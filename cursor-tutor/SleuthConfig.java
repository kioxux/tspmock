package com.fastcnt.credit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import brave.propagation.Propagation;

@Configuration
public class SleuthConfig {

    @Bean
    public Propagation.Factory customPropagationFactory() {
        return new CustomPropagationFactory();
    }
}
