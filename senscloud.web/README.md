<h1>耕云设备健康云</h1>

## 主要功能
 1. 数据库：Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能。 数据库密码加密，加密方式请查看PropertiesUtil，decryptProperties属性配置需要解密的key。
 2. 进行DDL(create,alter,drop)操作，利用MyBatis对数据库进行DDL操作(注册公司时->+<-通过传参过来公司前缀的表前缀来新建表)
### 架构图想法图：
![改进后的MVC模式](picture/改进后的MVC模式.jpg)
![前后端分离](picture/前后端分离.jpg)