package com.gengyun.senscloud;

import com.gengyun.senscloud.interceptor.IllegalInterceptor;
import com.gengyun.senscloud.interceptor.LoginInterceptor;
import com.gengyun.senscloud.util.DataChangeUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import javax.annotation.Resource;
import java.util.List;

/**
 * Spring 配置类
 */
@Configuration
public class WebAppConfigurer extends WebMvcConfigurerAdapter {
    @Resource
    LoginInterceptor loginInterceptor;
    @Resource
    IllegalInterceptor illegalInterceptor;

    /**
     * 定义拦截器
     *
     * @param registry 注册信息
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("resource_key");
        registry.addInterceptor(localeChangeInterceptor);
        String[] staticUrls = new String[]{"/cdn/**", "/css/**", "/img/**", "/js/**"};
//        List<String> specUrls = DataChangeUtil.scdArrayToListStr(new String[]{"/index.html", "/doGoSns"});
        List<String> specUrls = DataChangeUtil.scdArrayToListStr(new String[]{"/wxMsg","/index.html", "/doGoSns", "/searchLoginData", "/doNFCLogin", "/searchLoginImage", "/swagger-resources/**", "/v2/api-docs-ext"});
        specUrls.addAll(DataChangeUtil.scdArrayToListStr(staticUrls));
        String[] excludeUrls = specUrls.toArray(new String[]{});

        // 检查用户是否已经登录
        // 注册登录拦截器
        InterceptorRegistration loginReg = registry.addInterceptor(loginInterceptor);
        loginReg.addPathPatterns("/**");
        loginReg.excludePathPatterns(DataChangeUtil.mergeStrArray(excludeUrls, new String[]{"/sendVerificationCodeByPhone", "/checkVerificationCode", "/editPsdByPhone"}));

        // 注册权限拦截器
        InterceptorRegistration authReg = registry.addInterceptor(illegalInterceptor);
        authReg.addPathPatterns("/**");
        authReg.excludePathPatterns(excludeUrls);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
        //converters.add(mappingJackson2HttpMessageConverter());
        converters.add(bufferedImageHttpMessageConverter());
    }

    @Bean
    public BufferedImageHttpMessageConverter bufferedImageHttpMessageConverter() {
        return new BufferedImageHttpMessageConverter();
    }
}
