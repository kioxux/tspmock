package com.gengyun.senscloud.auth;

import org.springframework.stereotype.Service;

@Service
public class AuthService {
//    public static boolean logout(HttpServletRequest request) {
//        try {
//            request.getSession().removeAttribute(Constants.SESSION_KEY_USER);
//            request.getSession().removeAttribute(Constants.SESSION_KEY_DPL);
//            request.getSession().removeAttribute(Constants.SESSION_KEY_COMPANY);
//            request.getSession().removeAttribute(Constants.SESSION_KEY_UKEY);
//            request.getSession().removeAttribute(Constants.SESSION_KEY_NO_COMPANY);
//            request.getSession().removeAttribute(Constants.SESSION_KEY_TIME_ZONE_ID);
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }
//
//    public static boolean systemUserLogout(HttpServletRequest request) {
//        try {
//            request.getSession().removeAttribute(Constants.SESSION_KEY_SYSTEM_USER);
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }
//
//    public static Company getCompany(HttpServletRequest request) {
//        try {
//            Company company = (Company) request.getSession().getAttribute(Constants.SESSION_KEY_COMPANY);
//            return company;
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    public static Company saveCompany(HttpServletRequest request, Company company) {
//        if (getCompany(request) != null) {
//            return getCompany(request);
//        }
//        try {
//            request.getSession(true).setAttribute(Constants.SESSION_KEY_COMPANY, company);
//            return company;
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    public static boolean isLogined(HttpServletRequest request) {
//        if (request.getSession().getAttribute(Constants.SESSION_KEY_USER) == null) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//
//    /**
//     * 已废弃，请用getCompany代替
//     *
//     * @param request
//     * @return
//     */
//    @Deprecated
//    public Company getLoginCompany(HttpServletRequest request) {
//        try {
//            Company company = (Company) request.getSession().getAttribute(Constants.SESSION_KEY_USER);
//            return company;
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    public SystemUser getSystemLoginUser(HttpServletRequest request) {
//        try {
//            SystemUser systemUser = (SystemUser) request.getSession().getAttribute(Constants.SESSION_KEY_SYSTEM_USER);
//            return systemUser;
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    public static User getLoginUser(HttpServletRequest request) {
//        try {
//            User user = (User) request.getSession().getAttribute(Constants.SESSION_KEY_USER);
//            return user;
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    public static DomainPortUrlData getDomainPortUrlData(HttpServletRequest request) {
//        try {
//            return (DomainPortUrlData)request.getSession().getAttribute(Constants.SESSION_KEY_DPL);
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    /**
//     * 当前登录的用户是否有指定角色ID，角色ID可以传任意多个
//     *
//     * @param request
//     * @param roleId
//     * @return
//     */
//    public static boolean isUserHasRoleId(HttpServletRequest request, String... roleId) {
//        try {
//            User user = getLoginUser(request);
//            LinkedHashMap<String, IUserRole> roles = user.getRoles();
//            for (String rid : roleId) {
//                if (roles.keySet().contains(rid)) {
//                    return true;
//                }
//            }
//            return false;
//        } catch (Exception ex) {
//
//        }
//        return false;
//    }
//
//    /**
//     * 当前登录的用户是否有指定角色名称，角色名称可以传任意多个
//     *
//     * @param request
//     * @param roleName
//     * @return
//     */
//    public static boolean isUserHasRoleName(HttpServletRequest request, String... roleName) {
//        try {
//            User user = getLoginUser(request);
//            LinkedHashMap<String, IUserRole> roles = user.getRoles();
//            for (String rname : roleName) {
//                for (String key : roles.keySet()) {
//                    IUserRole userRole = roles.get(key);
//                    if (rname.equals(userRole.getName())) {
//                        return true;
//                    }
//                }
//            }
//            return false;
//        } catch (Exception ex) {
//
//        }
//        return false;
//    }
//
//    /**
//     * 当前登录的用户是否有指定key的权限
//     *
//     * @param request
//     * @param permissionKey
//     * @return
//     */
//    public static boolean isUserHasPermissionKey(HttpServletRequest request, String... permissionKey) {
//        try {
//            User user = getLoginUser(request);
//            LinkedHashMap<String, ISystemPermission> permissionLinkedHashMap = user.getPermissions();
//            for (String permission : permissionKey) {
//                for (String key : permissionLinkedHashMap.keySet()) {
//                    ISystemPermission systemPermission = permissionLinkedHashMap.get(key);
//                    if (permission.equals(systemPermission.getKey())) {
//                        return true;
//                    }
//                }
//            }
//            return false;
//        } catch (Exception ex) {
//
//        }
//        return false;
//    }
//
//
//    /**
//     * 当前登录的用户是否有指定位置ID，位置ID可以传任意多个
//     *
//     * @param request
//     * @param facilityId
//     * @return
//     */
//    public static boolean isUserHasFacilityId(HttpServletRequest request, String... facilityId) {
//        try {
//            User user = getLoginUser(request);
//            LinkedHashMap<String, IFacility> facilities = user.getFacilities();
//            for (String rid : facilityId) {
//                if (facilities.keySet().contains(rid)) {
//                    return true;
//                }
//            }
//            return false;
//        } catch (Exception ex) {
//
//        }
//        return false;
//    }
}
