package com.gengyun.senscloud.auth;

import java.lang.annotation.*;

/**
 * 菜单访问权限控制
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MenuPermission {

    /**
     * permission key(权限key)
     * @return
     */
//    String value() default "";

    /**
     * permission key(权限key)-多个权限控制
     * @return
     */
    String[] value() default {};

    /**
     * 菜单权限控制规则
     * @return
     */
    MenuPermissionRule rule() default MenuPermissionRule.AND;
}
