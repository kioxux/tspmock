package com.gengyun.senscloud.auth;

public class UserRoles {
    public final static String ROLE_SYS_ADMIN = "系统管理员";
    public final static String ROLE_COMPANY = "企业管理员";
    public final static String ROLE_COMPANY_STAFF = "企业员工";
}
