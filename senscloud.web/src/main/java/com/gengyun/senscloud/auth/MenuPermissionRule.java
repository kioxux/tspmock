package com.gengyun.senscloud.auth;

/**
 * 菜单访问权限控制-逻辑规则
 */
public enum MenuPermissionRule {
    OR,
    AND
}
