package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserModel implements Serializable {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "账号")
    private String account;
    @ApiModelProperty(value = "用户名")
    private String user_name;
    @ApiModelProperty(value = "工号")
    private String user_code;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "手机号")
    private String mobile;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ApiModelProperty(value = "正常，禁用")
    private Integer status;
    @ApiModelProperty(value = "是否付费用户")
    private Boolean is_charge;
    @ApiModelProperty(value = "是否付禁用 1启用0禁用")
    private String is_use;
    @ApiModelProperty(value = "默认语言")
    private String language_tag;
    @ApiModelProperty(value = "工时成本")
    private String hour_fee;
    @ApiModelProperty(value = "性别：1：男2：女")
    private String gender_tag;
    @ApiModelProperty(value = "入职日期")
    private String join_date;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "启用状态查询")
    private Integer isUseSearch;
    @ApiModelProperty(value = "岗位查询")
    private String positionSearch;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "岗位id集合")
    private String positionIds;
    @ApiModelProperty(value = "是否是系统用户 1是系统用户 2自定义用户")
    private String sys_user;
    @ApiModelProperty(value = "是否在上班 1是 -1不是")
    private String is_on_duty;
    @ApiModelProperty(value = "ids")
    private String ids;
    @ApiModelProperty(value = "nfc_code")
    private String nfc_code;


    public String getIs_on_duty() {
        return is_on_duty;
    }

    public void setIs_on_duty(String is_on_duty) {
        this.is_on_duty = is_on_duty;
    }

    public Integer getIsUseSearch() {
        return isUseSearch;
    }

    public void setIsUseSearch(Integer isUseSearch) {
        this.isUseSearch = isUseSearch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getIs_charge() {
        return is_charge;
    }

    public void setIs_charge(Boolean is_charge) {
        this.is_charge = is_charge;
    }

    public String getLanguage_tag() {
        return language_tag;
    }

    public void setLanguage_tag(String language_tag) {
        this.language_tag = language_tag;
    }

    public String getHour_fee() {
        return hour_fee;
    }

    public void setHour_fee(String hour_fee) {
        this.hour_fee = hour_fee;
    }

    public String getGender_tag() {
        return gender_tag;
    }

    public void setGender_tag(String gender_tag) {
        this.gender_tag = gender_tag;
    }

    public String getJoin_date() {
        return join_date;
    }

    public void setJoin_date(String join_date) {
        this.join_date = join_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPositionSearch() {
        return positionSearch;
    }

    public void setPositionSearch(String positionSearch) {
        this.positionSearch = positionSearch;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getPositionIds() {
        return positionIds;
    }

    public void setPositionIds(String positionIds) {
        this.positionIds = positionIds;
    }

    public String getSys_user() {
        return sys_user;
    }

    public void setSys_user(String sys_user) {
        this.sys_user = sys_user;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getNfc_code() {
        return nfc_code;
    }

    public void setNfc_code(String nfc_code) {
        this.nfc_code = nfc_code;
    }
}
