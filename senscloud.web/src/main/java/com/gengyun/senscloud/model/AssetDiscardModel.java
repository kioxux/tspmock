package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * @Author: Wudang Dong
 * @Description:设备报废实例类POJO
 * @Date: Create in 上午 11:15 2019/4/30 0030
 */
public class AssetDiscardModel implements Serializable {
//    private static final long serialVersionUID = 3102154456965271385L;
//    private String discard_code;
//    private String position_id;
//    private int cost;
//    private float leave_cost;
//    private int new_position_id;
//    private int status;
//    private String remark;
//    private String create_user_account;
//    private Timestamp create_time;
//    private boolean is_head;
//
//    private String schema_name;
//
//    private String strcode;
//    private String strname;
//    private String model_name;
//    private String asset_type;
//    private String supplier;
//    private String facilities_name;
//    private String asset_status_name;
//    private String username;
//    private String taskId;
//    private Object body_property;
//    private String source_sub_code;
//
//    public String getDiscard_code() {
//        return discard_code;
//    }
//
//    public void setDiscard_code(String discard_code) {
//        this.discard_code = discard_code;
//    }
//
//    public String getPosition_id() {
//        return position_id;
//    }
//
//    public void setPosition_id(String position_id) {
//        this.position_id = position_id;
//    }
//
//    public int getCost() {
//        return cost;
//    }
//
//    public void setCost(int cost) {
//        this.cost = cost;
//    }
//
//    public int getNew_position_id() {
//        return new_position_id;
//    }
//
//    public void setNew_position_id(int new_position_id) {
//        this.new_position_id = new_position_id;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public String getAsset_type() {
//        return asset_type;
//    }
//
//    public void setAsset_type(String asset_type) {
//        this.asset_type = asset_type;
//    }
//
//    public String getSupplier() {
//        return supplier;
//    }
//
//    public void setSupplier(String supplier) {
//        this.supplier = supplier;
//    }
//
//    public String getFacilities_name() {
//        return facilities_name;
//    }
//
//    public void setFacilities_name(String facilities_name) {
//        this.facilities_name = facilities_name;
//    }
//
//    public String getAsset_status_name() {
//        return asset_status_name;
//    }
//
//    public void setAsset_status_name(String asset_status_name) {
//        this.asset_status_name = asset_status_name;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getModel_name() {
//        return model_name;
//    }
//
//    public void setModel_name(String model_name) {
//        this.model_name = model_name;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public boolean isIs_head() {
//        return is_head;
//    }
//
//    public void setIs_head(boolean is_head) {
//        this.is_head = is_head;
//    }
//
//    public String getTaskId() {
//        return taskId;
//    }
//
//    public void setTaskId(String taskId) {
//        this.taskId = taskId;
//    }
//
//    public float getLeave_cost() {
//        return leave_cost;
//    }
//
//    public void setLeave_cost(float leave_cost) {
//        this.leave_cost = leave_cost;
//    }
//
//    public Object getBody_property() {
//        return body_property;
//    }
//
//    public void setBody_property(Object body_property) {
//        this.body_property = body_property;
//    }
//
//    public String getSource_sub_code() {
//        return source_sub_code;
//    }
//
//    public void setSource_sub_code(String source_sub_code) {
//        this.source_sub_code = source_sub_code;
//    }
}
