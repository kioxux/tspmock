package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(description = "设备列表查询参数")
public class AssetSearchParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "设备主键")
    private String id;
    @ApiModelProperty(value = "选中设备主键")
    private String ids;
    @ApiModelProperty(value = "资产状态")
    private String statusSearch;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "设备位置")
    private String[] positionsSearch;
    @ApiModelProperty(value = "设备类型")
    private String assetTypesSearch;
    @ApiModelProperty(value = "设备型号")
    private String assetModelSearch;
    @ApiModelProperty(value = "运行状态")
    private String runStatusSearch;
    @ApiModelProperty(value = "供应商")
    private String customerSearch;
    @ApiModelProperty(value = "制造商")
    private String manufacturerSearch;
    @ApiModelProperty(value = "重要级别")
    private String levelSearch;
    @ApiModelProperty(value = "开始日期")
    private String startDateSearch;
    @ApiModelProperty(value = "结束日期")
    private String endDateSearch;
    @ApiModelProperty(value = "开始日期-创建时间")
    private String startCreateDateSearch;
    @ApiModelProperty(value = "结束日期-创建时间")
    private String endCreateDateSearch;
    @ApiModelProperty(value = "保固状态")
    private String warrantiesTypeSearch;
    @ApiModelProperty(value = "设备组织")
    private String[] facilitiesSearch;
    @ApiModelProperty(value = "组织类型")
    private Integer orgTypeSearch;
    @ApiModelProperty(value = "重复编码")
    private Boolean duplicateAssetSearch;
    @ApiModelProperty(value = "根节点设备数据")
    private Boolean returnBootAssetOnly;
    @ApiModelProperty(value = "设备监控数据", notes = "是否仅仅获取监控设备,1：是；0：否")
    private Integer monitorDataOnly;
    @ApiModelProperty(value = "模块类型[bomIcon-设备图片,assetTop-顶部信息,assetBase-基本信息,assetDyInfo-规格参数(自定义字段),assetBuy-采购信息,assetInstall-安装信息]")
    private String sectionType;
    @ApiModelProperty(value = "要新增或删除的附件文档id")
    private String file_id;
    @ApiModelProperty(value = "设备编码")
    private String codeSearch;
    @ApiModelProperty(value = "文件类别")
    private Integer file_category_id;
    @ApiModelProperty(value = "文件类型")
    private Integer file_type_id;
    @ApiModelProperty(value = "厂商主键")
    private String org_id;
    @ApiModelProperty(value = "人员主键")
    private String user_id;
    @ApiModelProperty(value = "工单类型主键（职责主键）")
    private String duty_id;
    @ApiModelProperty(value = "备注，描述")
    private String remark;
    @ApiModelProperty(value = "设备关联人员id")
    private String duty_man_id;
    @ApiModelProperty(value = "备件id")
    private String bom_id;
    @ApiModelProperty(value = "数量")
    private String use_count;
    @ApiModelProperty(value = "设备备件id")
    private String asset_bom_id;
    @ApiModelProperty(value = "地图坐标")
    private String location;
    @ApiModelProperty(value = "字段信息")
    private List<String> fieldList;
    @ApiModelProperty(value = "设备类型标识")
    private String asssetTypeTag;
    @ApiModelProperty(value = "父节点id")
    private String parent_id;
    @ApiModelProperty(value = "当前选中id")
    private String select_id;

    public String getSelect_id() { return select_id; }

    public void setSelect_id(String select_id) { this.select_id = select_id; }

    public String getParent_id() {return parent_id;}

    public void setParent_id(String parent_id) { this.parent_id = parent_id;}

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getFile_type_id() {
        return file_type_id;
    }

    public void setFile_type_id(Integer file_type_id) {
        this.file_type_id = file_type_id;
    }

    public Integer getFile_category_id() {
        return file_category_id;
    }

    public void setFile_category_id(Integer file_category_id) {
        this.file_category_id = file_category_id;
    }

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String[] getPositionsSearch() {
        return positionsSearch;
    }

    public void setPositionsSearch(String[] positionsSearch) {
        this.positionsSearch = positionsSearch;
    }

    public String getAssetTypesSearch() {
        return assetTypesSearch;
    }

    public void setAssetTypesSearch(String assetTypesSearch) {
        this.assetTypesSearch = assetTypesSearch;
    }

    public String getAssetModelSearch() {
        return assetModelSearch;
    }

    public void setAssetModelSearch(String assetModelSearch) {
        this.assetModelSearch = assetModelSearch;
    }

    public String getRunStatusSearch() {
        return runStatusSearch;
    }

    public void setRunStatusSearch(String runStatusSearch) {
        this.runStatusSearch = runStatusSearch;
    }

    public String getCustomerSearch() {
        return customerSearch;
    }

    public void setManufacturerSearch(String manufacturerSearch) {
        this.manufacturerSearch = manufacturerSearch;
    }

    public String getManufacturerSearch() {
        return manufacturerSearch;
    }

    public void setCustomerSearch(String customerSearch) {
        this.customerSearch = customerSearch;
    }

    public String getLevelSearch() {
        return levelSearch;
    }

    public void setLevelSearch(String levelSearch) {
        this.levelSearch = levelSearch;
    }

    public String getStartDateSearch() {
        return startDateSearch;
    }

    public void setStartDateSearch(String startDateSearch) {
        this.startDateSearch = startDateSearch;
    }

    public String getEndDateSearch() {
        return endDateSearch;
    }

    public void setEndDateSearch(String endDateSearch) {
        this.endDateSearch = endDateSearch;
    }

    public String getWarrantiesTypeSearch() {
        return warrantiesTypeSearch;
    }

    public void setWarrantiesTypeSearch(String warrantiesTypeSearch) {
        this.warrantiesTypeSearch = warrantiesTypeSearch;
    }

    public String[] getFacilitiesSearch() {
        return facilitiesSearch;
    }

    public void setFacilitiesSearch(String[] facilitiesSearch) {
        this.facilitiesSearch = facilitiesSearch;
    }

    public Integer getOrgTypeSearch() {
        return orgTypeSearch;
    }

    public void setOrgTypeSearch(Integer orgTypeSearch) {
        this.orgTypeSearch = orgTypeSearch;
    }

    public Boolean getDuplicateAssetSearch() {
        return duplicateAssetSearch;
    }

    public void setDuplicateAssetSearch(Boolean duplicateAssetSearch) {
        this.duplicateAssetSearch = duplicateAssetSearch;
    }

    public Boolean getReturnBootAssetOnly() {
        return returnBootAssetOnly;
    }

    public void setReturnBootAssetOnly(Boolean returnBootAssetOnly) {
        this.returnBootAssetOnly = returnBootAssetOnly;
    }

    public Integer getMonitorDataOnly() {
        return monitorDataOnly;
    }

    public void setMonitorDataOnly(Integer monitorDataOnly) {
        this.monitorDataOnly = monitorDataOnly;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public String getCodeSearch() {
        return codeSearch;
    }

    public void setCodeSearch(String codeSearch) {
        this.codeSearch = codeSearch;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDuty_id() {
        return duty_id;
    }

    public void setDuty_id(String duty_id) {
        this.duty_id = duty_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDuty_man_id() {
        return duty_man_id;
    }

    public void setDuty_man_id(String duty_man_id) {
        this.duty_man_id = duty_man_id;
    }

    public String getBom_id() {
        return bom_id;
    }

    public void setBom_id(String bom_id) {
        this.bom_id = bom_id;
    }

    public String getUse_count() {
        return use_count;
    }

    public void setUse_count(String use_count) {
        this.use_count = use_count;
    }

    public String getAsset_bom_id() {
        return asset_bom_id;
    }

    public void setAsset_bom_id(String asset_bom_id) {
        this.asset_bom_id = asset_bom_id;
    }

    public List<String> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<String> fieldList) {
        this.fieldList = fieldList;
    }

    public String getAsssetTypeTag() { return asssetTypeTag; }

    public void setAsssetTypeTag(String asssetTypeTag) { this.asssetTypeTag = asssetTypeTag; }

    public String getStartCreateDateSearch() {
        return startCreateDateSearch;
    }

    public void setStartCreateDateSearch(String startCreateDateSearch) {
        this.startCreateDateSearch = startCreateDateSearch;
    }

    public String getEndCreateDateSearch() {
        return endCreateDateSearch;
    }

    public void setEndCreateDateSearch(String endCreateDateSearch) {
        this.endCreateDateSearch = endCreateDateSearch;
    }
}
