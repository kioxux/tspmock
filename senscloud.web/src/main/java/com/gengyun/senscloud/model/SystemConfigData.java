package com.gengyun.senscloud.model;

/*
* 系统配置项，执行可动态控制的业务，如：
* a) 上报问题是否可选择设备 : question_select_asset      0:不可选；1：可选；
* b) 保养是否可选择设备 ： maintain_select_asset      0:不可选；1：可选；
* c) 点检是否可选择设备 :
* d) 巡检是扫描设备，还是扫描区域 : inspection_select_area   asset:设备；area：区域；
* e) 巡检是否可选择设备 :
* f) 点检、巡检、保养项、保养确认项配置时，按设备类型来配置，增加一个通用类型，如果没有配置该类型，则获取通用类型的配置 :
* g) 保养任务自动生成服务，做成可配置是否开启，开启才开始生成 :
* h) 短信接口，做成可配置，是耕云短信(腾讯云)，还是客户自定义接口 :
* i) 是否开启设备监控短信发送 :
* exception_sms_interval : 设备异常，发送短信间隔
* alarm_sms1_interval: 设备报警，第1次和第2次发送短信间隔
* alarm_sms2_interval : 设备报警，第2次和第3次发送短信间隔
* alarm_sms3_interval : 设备报警，第3次和第4次发送短信间隔
* alarm_sms4_interval : 设备报警，第4次后，依次发送短信间隔
*/

import java.io.Serializable;

public class SystemConfigData implements Serializable {

    private static final long serialVersionUID = 5442770642979796940L;
    private String config_name;

    private String setting_value;

    private String config_title;

    private String group_title;

    private int control_type;

    private String source_data;

    private int order;

    private String remark;

    public String getConfigName() {
        return config_name;
    }

    public void setConfigName(String config_name) {
        this.config_name = config_name;
    }

    public String getSettingValue() {
        return setting_value;
    }

    public void setSettingValue(String setting_value) {
        this.setting_value = setting_value;
    }

    public String getConfig_title() {
        return config_title;
    }

    public void setConfig_title(String config_title) {
        this.config_title = config_title;
    }

    public String getGroup_title() {
        return group_title;
    }

    public void setGroup_title(String group_title) {
        this.group_title = group_title;
    }

    public int getControl_type() {
        return control_type;
    }

    public void setControl_type(int control_type) {
        this.control_type = control_type;
    }

    public String getSource_data() {
        return source_data;
    }

    public void setSource_data(String source_data) {
        this.source_data = source_data;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
