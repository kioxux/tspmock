package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 业务配置
 */
public class BussinessConfigModel {
    @ApiModelProperty(value = "id")
    private Integer id;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "业务配置类型 1:工单类型 2:维修类型 3:设备运行状态 4:设备级别 5:备件类型 6:位置类型 7:文档类型 8:单位管理 9:完修说明 10:货币种类")
    private Integer systemConfigType;
    @ApiModelProperty(value = "业务类型")
    private Integer business_type_id;
    @ApiModelProperty(value = "显示顺序")
    private Integer data_order;
    @ApiModelProperty(value = "是否启用 1启用0禁用")
    private String is_use;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "类型名称")
    private String type_name;
    @ApiModelProperty(value = "级别名称")
    private String level_name;
    @ApiModelProperty(value = "类型编码")
    private String type_code;
    @ApiModelProperty(value = "父id")
    private Integer parent_id;
    @ApiModelProperty(value = "单位名称")
    private String unit_name;
    @ApiModelProperty(value = "完修说明")
    private String finished_name;
    @ApiModelProperty(value = "是否完修 1是 2否")
    private Integer is_finished;
    @ApiModelProperty(value = "货币名称")
    private String currency_name;
    @ApiModelProperty(value = "货币编码")
    private String currency_code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSystemConfigType() {
        return systemConfigType;
    }

    public void setSystemConfigType(Integer systemConfigType) {
        this.systemConfigType = systemConfigType;
    }

    public Integer getBusiness_type_id() {
        return business_type_id;
    }

    public void setBusiness_type_id(Integer business_type_id) {
        this.business_type_id = business_type_id;
    }

    public Integer getData_order() {
        return data_order;
    }

    public void setData_order(Integer data_order) {
        this.data_order = data_order;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getType_code() {
        return type_code;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getFinished_name() {
        return finished_name;
    }

    public void setFinished_name(String finished_name) {
        this.finished_name = finished_name;
    }

    public Integer getIs_finished() {
        return is_finished;
    }

    public void setIs_finished(Integer is_finished) {
        this.is_finished = is_finished;
    }

    public String getCurrency_name() {
        return currency_name;
    }

    public void setCurrency_name(String currency_name) {
        this.currency_name = currency_name;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }
}
