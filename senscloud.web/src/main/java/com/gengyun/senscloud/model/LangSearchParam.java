package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "多语言模块参数")
public class LangSearchParam implements Serializable {
    @ApiModelProperty(value = "所属企业")
    private String companyIdSearch;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "键值")
    private String key;
    @ApiModelProperty(value = "用途描述")
    private String use_type;
    @ApiModelProperty(value = "选择企业")
    private String selectCompanyIds;

    public String getCompanyIdSearch() {
        return companyIdSearch;
    }

    public void setCompanyIdSearch(String companyIdSearch) {
        this.companyIdSearch = companyIdSearch;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUse_type() {
        return use_type;
    }

    public void setUse_type(String use_type) {
        this.use_type = use_type;
    }

    public String getSelectCompanyIds() {
        return selectCompanyIds;
    }

    public void setSelectCompanyIds(String selectCompanyIds) {
        this.selectCompanyIds = selectCompanyIds;
    }
}
