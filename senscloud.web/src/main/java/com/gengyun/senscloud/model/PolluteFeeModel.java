package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 缴费信息
 */
public class PolluteFeeModel {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "内部编码")
    private String inner_code;

    @ApiModelProperty(value = "厂商名称")
    private String title;

    @ApiModelProperty(value = "status")
    private Integer status;

    @ApiModelProperty(value = "开始日期")
    private String begin_date;

    @ApiModelProperty(value = "结束日期")
    private String end_date;

    @ApiModelProperty(value = "关键字")
    private String keywordSearch;

    @ApiModelProperty(value = "最终费用")
    private String final_amount;

    @ApiModelProperty(value = "修改人")
    private String modify_user_id;

    @ApiModelProperty(value = "审核人")
    private String auditor;

    @ApiModelProperty(value = "是否通过")
    private String pass;

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getModify_user_id() {
        return modify_user_id;
    }

    public void setModify_user_id(String modify_user_id) {
        this.modify_user_id = modify_user_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInner_code() {
        return inner_code;
    }

    public void setInner_code(String inner_code) {
        this.inner_code = inner_code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getFinal_amount() {
        return final_amount;
    }

    public void setFinal_amount(String final_amount) {
        this.final_amount = final_amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @ApiModelProperty(value = "备注")
    private String remark;
}
