//package com.gengyun.senscloud.model;
//
//import com.gengyun.senscloud.common.LangConstant;
//
//import java.io.Serializable;
//import java.sql.Timestamp;
//
//public class SystemPermission implements ISystemPermission,Serializable {
//    private static final long serialVersionUID = -5441743296490460918L;
//
//    public final static String PERMISSION_KEY_ASSET_DATA_MANAGEMENT_ALL_FACILITY = "asset_data_management_all_facility";
//    public final static String PERMISSION_KEY_ASSET_DATA_MANAGEMENT_IMPORT = "asset_data_management_import";
//    public final static String PERMISSION_KEY_ASSET_DATA_MANAGEMENT_EDIT = "asset_data_management_edit";
////    public final static String PERMISSION_KEY_ASSET_DATA_MANAGEMENT_EDIT_ALL_FACILITY = "asset_data_management_edit:all_facility";
//    public final static String PERMISSION_KEY_ASSET_DATA_MANAGEMENT_EXPORT = "asset_data_management_export";
//    //    public final static String PERMISSION_KEY_ASSET_DATA_MANAGEMENT_EXPORT_ALL_FACILITY = "asset_data_management_export:all_facility";
//    /*id bigint,
//    title text,
//    key text,
//    url text,
//    parent_id bigint,
//    available boolean,
//    createtime timestamp without time zone,
//    index integer,
//    icon character varying,
//    type integer*/
//    private String id;
//    private String name;
//    private String key;
//    private String url;
//    private String parent_id;
//    private boolean available;
//    private Timestamp createtime;
//    private int index;
//    private String icon;
//    private int type;
//    private String typeName;
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getKey() {
//        return key;
//    }
//
//    public void setKey(String key) {
//        this.key = key;
//    }
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getParent_id() {
//        return parent_id;
//    }
//
//    public void setParent_id(String parent_id) {
//        this.parent_id = parent_id;
//    }
//
//    public boolean isAvailable() {
//        return available;
//    }
//
//    public void setAvailable(boolean available) {
//        this.available = available;
//    }
//
//    public Timestamp getCreatetime() {
//        return createtime;
//    }
//
//    public void setCreatetime(Timestamp createtime) {
//        this.createtime = createtime;
//    }
//
//    public int getIndex() {
//        return index;
//    }
//
//    public void setIndex(int index) {
//        this.index = index;
//    }
//
//    public String getIcon() {
//        return icon;
//    }
//
//    public void setIcon(String icon) {
//        this.icon = icon;
//    }
//
//    public int getType() {
//        return type;
//    }
//
//    public void setType(int type) {
//        this.type = type;
//    }
//
//    public String getTypeName() {
//        switch (type) {
//            case 1:
//                return PermissionType.MENU.name;
//            case 2:
//                return PermissionType.FUNCTION.name;
//            case 3:
//                return PermissionType.MODULE.name;
//            default:
//                return "";
//        }
//    }
//
//    public String getTypeNameCN() {
//        switch (type) {
//            case 1:
//                return PermissionType.MENU.name_cn;
//            case 2:
//                return PermissionType.FUNCTION.name_cn;
//            case 3:
//                return PermissionType.MODULE.name_cn;
//            default:
//                return "";
//        }
//    }
//
//    public enum PermissionType {
//        MENU("menu", LangConstant.MENU_M, 1), //菜单
//        FUNCTION("function", LangConstant.FUNCTION_F, 2),//功能
//        MODULE("module", LangConstant.MODULE_M, 3);//模块
//        // 成员变量
//        private String name;
//        private String name_cn;
//        private int value;
//
//        private PermissionType(String name, String name_cn, int value) {
//            this.name = name;
//            this.name_cn = name_cn;
//            this.value = value;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public int getValue() {
//            return value;
//        }
//
//        public void setValue(int value) {
//            this.value = value;
//        }
//    }
//}
