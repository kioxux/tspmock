package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(description = "备件列表查询参数")
public class BomSearchParam implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "备件主键")
    private String id;
    @ApiModelProperty(value = "选中备件主键")
    private String ids;
//    @ApiModelProperty(value = "物料编码")
//    private String materialCodeSearch;
//    @ApiModelProperty(value = "规格参数")
//    private String bomModelSearch;
    @ApiModelProperty(value = "备件类型")
    private String[] typeIdsSearch;
    @ApiModelProperty(value = "供应商")
    private String[] suppliersSearch;
    @ApiModelProperty(value = "起始数量")
    private Integer startCountSearch;
    @ApiModelProperty(value = "截止数量")
    private Integer endCountSearch;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "是否启用 1启用 0禁用")
    private String isUseSearch;
    @ApiModelProperty(value = "模块类型[bomIcon-备件图片,bomTop-顶部信息]")
    private String sectionType;
    @ApiModelProperty(value = "字段信息")
    private List<String> fieldList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

//    public String getMaterialCodeSearch() {
//        return materialCodeSearch;
//    }
//
//    public void setMaterialCodeSearch(String materialCodeSearch) {
//        this.materialCodeSearch = materialCodeSearch;
//    }
//
//    public String getBomModelSearch() {
//        return bomModelSearch;
//    }
//
//    public void setBomModelSearch(String bomModelSearch) {
//        this.bomModelSearch = bomModelSearch;
//    }

    public String[] getTypeIdsSearch() {
        return typeIdsSearch;
    }

    public void setTypeIdsSearch(String[] typeIdsSearch) {
        this.typeIdsSearch = typeIdsSearch;
    }

    public String[] getSuppliersSearch() {
        return suppliersSearch;
    }

    public void setSuppliersSearch(String[] suppliersSearch) {
        this.suppliersSearch = suppliersSearch;
    }

    public Integer getStartCountSearch() {
        return startCountSearch;
    }

    public void setStartCountSearch(Integer startCountSearch) {
        this.startCountSearch = startCountSearch;
    }

    public Integer getEndCountSearch() {
        return endCountSearch;
    }

    public void setEndCountSearch(Integer endCountSearch) {
        this.endCountSearch = endCountSearch;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getIsUseSearch() {
        return isUseSearch;
    }

    public void setIsUseSearch(String isUseSearch) {
        this.isUseSearch = isUseSearch;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public List<String> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<String> fieldList) {
        this.fieldList = fieldList;
    }
}
