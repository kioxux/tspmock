package com.gengyun.senscloud.model;

import java.io.Serializable;

public class MetaDataAsset implements Serializable {
//    private static final long serialVersionUID = -3729569731996727647L;
//    //region 字段常量
//    public final static String ASSET_TYPE = "asset_type";
//    public final static String SYS_ID = "_id";
//    public final static String STRNAME = "strname";
//    public final static String STRCODE = "strcode";
//    public final static String STRMODEL = "strmodel";
//    public final static String ENABLE_TIME = "enable_time";
//    public final static String INTSTATUS = "intstatus";
//    public final static String INTSITEID = "intsiteid";
//    public final static String SUPPLIER = "supplier";
//    public final static String STRDESCRIPTION = "strdescription";
//    public final static String X = "x";
//    public final static String Y = "y";
//    public final static String PARENT_ID = "parent_id";
//    public final static String POWER = "_power";
//    public final static String CATEGORY_ID = "category_id";
//    public final static String IMPORTMENT_LEVEL_ID = "importment_level_id";
//    public final static String _COUNT = "_count";
//    public final static String UNIT_ID = "unit_id";
//    public final static String PRICE = "price";
//    public final static String TAX_RATE = "tax_rate";
//    public final static String TAX_PRICE = "tax_price";
//    public final static String CURRENCY = "currency";
//    public final static String ASSET_SUBJECT = "asset_subject";
//    public final static String POSITION_CODE = "position_code";
//    public final static String BUY_DATE = "buy_date";
//    public final static String INSTALL_DATE = "install_date";
//
//    //endregion
//
//    private String schema_name;
//    private String asset_name;
//    private String asset_alias;
//    private int index;
//    private String description;
//    private String fields;//用户自定义字段
//    private String icon;
//    private int id;
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    @Deprecated
//    private int totalAsset;
//
//    @Override
//    public String toString() {
//        return "MetaDataAsset{schema_name=" + schema_name + ",asset_name='" + asset_name + '\'' +
//                ",asset_alias='" + asset_alias + '\'' +
//                ",description='" + description + '\'' +
//                ",fields='" + fields + '\'' +
//                '}';
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public String getAsset_name() {
//        return asset_name;
//    }
//
//    public void setAsset_name(String asset_name) {
//        this.asset_name = asset_name;
//    }
//
//    public String getAsset_alias() {
//        if (RegexUtil.isNull(asset_alias)) {
//            return "";
//        }
//        return asset_alias;
//    }
//
//    public void setAsset_alias(String asset_alias) {
//        this.asset_alias = asset_alias;
//    }
//
//    public String getDescription() {
//        if (RegexUtil.isNull(description)) {
//            return "";
//        }
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public String getFields() {
//        return fields;
//    }
//
//    public JSONArray getFieldArray() {
//        JSONArray fieldArray = new JSONArray();
//        try {
//            fieldArray = JSONArray.fromObject(this.fields);
//        } catch (Exception ex) {
//            fieldArray = new JSONArray();
//        }
//        return fieldArray;
//    }
//
//    public void setFields(String fields) {
//        this.fields = fields;
//    }
//
//    public int getIndex() {
//        return index;
//    }
//
//    public void setIndex(int index) {
//        this.index = index;
//    }
//
//    public int getTotalAsset() {
//        return totalAsset;
//    }
//
//    public void setTotalAsset(int totalAsset) {
//        this.totalAsset = totalAsset;
//    }
//
//    //region 系统字段
//    public final static JSONArray getSystemFields() {
//        JSONArray jsonArray = new JSONArray();
//        try {
//            MetaDataField field = new MetaDataField();
//            field.setName("_id");
//            field.setAlias("ID号");
//            field.setValidation(FieldValidation.UUID);
//            field.setDescription("自动自动生成的ID号");
//            JSONObject jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("strname");
//            field.setAlias("设备名称");
//            field.setValidation(FieldValidation.NOT_VALIDATION);
//            field.setDescription("设备名称");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("strcode");
//            field.setAlias("设备代码");
//            field.setValidation(FieldValidation.NOT_VALIDATION);
//            field.setDescription("设备代码");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("strmodel");
//            field.setAlias("设备型号");
//            field.setValidation(FieldValidation.NOT_VALIDATION);
//            field.setDescription("设备型号");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("intstatus");
//            field.setAlias("设备状态");
//            field.setValidation(FieldValidation.INT);
//            field.setDescription("设备状态");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("intsiteid");
//            field.setAlias("设备所属位置ID");
//            field.setValidation(FieldValidation.INT);
//            field.setDescription("设备所属位置ID");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("supplier");
//            field.setAlias("供应商");
//            field.setValidation(FieldValidation.INT);
//            field.setDescription("供应商ID号");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("x");
//            field.setAlias("X坐标");
//            field.setValidation(FieldValidation.NUMERIC);
//            field.setDescription("X坐标");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName("y");
//            field.setAlias("Y坐标");
//            field.setValidation(FieldValidation.NUMERIC);
//            field.setDescription("Y坐标");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.ASSET_TYPE);
//            field.setAlias("设备模版");
//            field.setValidation(FieldValidation.NOT_VALIDATION);
//            field.setNullable(false);
//            field.setDescription("设备模版");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.CATEGORY_ID);
//            field.setAlias("设备类型");
//            field.setValidation(FieldValidation.INT);
//            field.setNullable(true);
//            field.setDescription("设备类型");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.ENABLE_TIME);
//            field.setAlias("启用日期");
//            field.setValidation(FieldValidation.DATE);
//            field.setNullable(true);
//            field.setDescription("启用日期");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.POWER);
//            field.setAlias("设备功率");
//            field.setValidation(FieldValidation.NUMERIC);
//            field.setNullable(true);
//            field.setDescription("设备功率");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.TAX_RATE);
//            field.setAlias("税率");
//            field.setValidation(FieldValidation.NUMERIC);
//            field.setNullable(true);
//            field.setDescription("税率");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.IMPORTMENT_LEVEL_ID);
//            field.setAlias("重要级别");
//            field.setValidation(FieldValidation.INT);
//            field.setNullable(true);
//            field.setDescription("重要级别");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.PRICE);
//            field.setAlias("税前单价");
//            field.setValidation(FieldValidation.NUMERIC);
//            field.setNullable(true);
//            field.setDescription("税前单价");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset._COUNT);
//            field.setAlias("数量");
//            field.setValidation(FieldValidation.INT);
//            field.setNullable(true);
//            field.setDescription("数量");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.UNIT_ID);
//            field.setName("unit_id");
//            field.setAlias("单位");
//            field.setValidation(FieldValidation.INT);
//            field.setNullable(true);
//            field.setDescription("单位");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.TAX_PRICE);
//            field.setAlias("税");
//            field.setValidation(FieldValidation.NUMERIC);
//            field.setNullable(true);
//            field.setDescription("税");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.CURRENCY);
//            field.setName("currency");
//            field.setAlias("货币");
//            field.setValidation(FieldValidation.INT);
//            field.setNullable(true);
//            field.setDescription("货币");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//
//            field = new MetaDataField();
//            field.setName(MetaDataAsset.ASSET_SUBJECT);
//            field.setName("asset_subject");
//            field.setAlias("资产类型");
//            field.setValidation(FieldValidation.INT);
//            field.setNullable(true);
//            field.setDescription("资产类型");
//            jsonObject = JSONObject.fromObject(field);
//            jsonArray.add(jsonObject);
//        } catch (Exception ex) {
//
//        }
//        return jsonArray;
//    }
//
//    public final static boolean isSystemField(String fieldName) {
//        JSONArray systemFieldArray = getSystemFields();
//        for (Object field : systemFieldArray) {
//            JSONObject fieldObj = (JSONObject) field;
//            MetaDataField field1 = (MetaDataField) JSONObject.toBean(fieldObj, MetaDataField.class);
//            String fieldName2 = field1.getName();
//            if (fieldName2.equalsIgnoreCase(fieldName)) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public String getIcon() {
//        return icon;
//    }
//
//    public void setIcon(String icon) {
//        this.icon = icon;
//    }
//endregion
}
