package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * 数据导入日志类
 */
public class ImportLog implements Serializable {
    private static final long serialVersionUID = -1076937532061156800L;
    private String num;
    private String level;
    private String message;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
