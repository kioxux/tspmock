package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "流程配置增加、修改信息")
public class WorkFlowTemplateAdd implements Serializable {
    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "节点的id/流程列表的id，流程时也塞id", required = true)
    private String node_id;
    @ApiModelProperty(value = "流程的id/流程列表的parent，流程的时候也塞id", required = true)
    private String pref_id;
    @ApiModelProperty(value = "流程、节点列表接口中的node_type字段", required = true)
    private String node_type;
    @ApiModelProperty(value = "流程名称：流程需要")
    private String flow_name;
    @ApiModelProperty(value = "流程显示名称：流程需要")
    private String flow_show_name;
    @ApiModelProperty(value = "节点显示名称：流程需要")
    private String node_show_name;
    @ApiModelProperty(value = "工单类型：流程需要")
    private String work_type_id;
    @ApiModelProperty(value = "工单模板code：流程需要")
    private String work_template_code;
    @ApiModelProperty(value = "适用对象：流程需要")
    private String relation_type;
    @ApiModelProperty(value = "流程code：流程需要")
    private String flow_code;
    @ApiModelProperty(value = "展示名称：流程、节点都需要")
    private String show_name;
    @ApiModelProperty(value = "节点名称：节点、子节点需要")
    private String node_name;
    @ApiModelProperty(value = "处理类型，数据字典：node_handle_type")
    private String deal_type_id;
    @ApiModelProperty(value = "处理角色")
    private String deal_role_id;
    @ApiModelProperty(value = "模板描述")
    private String remark;
    @ApiModelProperty(value = "节点表的主键")
    private String word_flow_node_id;
    @ApiModelProperty(value = "基本类型（处理）：base  详情：detail", required = true )
    private String page_type;
    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getPref_id() {
        return pref_id;
    }

    public void setPref_id(String pref_id) {
        this.pref_id = pref_id;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public String getFlow_name() {
        return flow_name;
    }

    public void setFlow_name(String flow_name) {
        this.flow_name = flow_name;
    }

    public String getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(String work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public String getRelation_type() {
        return relation_type;
    }

    public void setRelation_type(String relation_type) {
        this.relation_type = relation_type;
    }

    public String getFlow_code() {
        return flow_code;
    }

    public void setFlow_code(String flow_code) {
        this.flow_code = flow_code;
    }

    public String getShow_name() {
        return show_name;
    }

    public void setShow_name(String show_name) {
        this.show_name = show_name;
    }

    public String getNode_name() {
        return node_name;
    }

    public void setNode_name(String node_name) {
        this.node_name = node_name;
    }

    public String getDeal_type_id() {
        return deal_type_id;
    }

    public void setDeal_type_id(String deal_type_id) {
        this.deal_type_id = deal_type_id;
    }

    public String getDeal_role_id() {
        return deal_role_id;
    }

    public void setDeal_role_id(String deal_role_id) {
        this.deal_role_id = deal_role_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getWord_flow_node_id() {
        return word_flow_node_id;
    }

    public void setWord_flow_node_id(String word_flow_node_id) {
        this.word_flow_node_id = word_flow_node_id;
    }

    public String getPage_type() {
        return page_type;
    }

    public void setPage_type(String page_type) {
        this.page_type = page_type;
    }

    public String getFlow_show_name() {
        return flow_show_name;
    }

    public void setFlow_show_name(String flow_show_name) {
        this.flow_show_name = flow_show_name;
    }

    public String getNode_show_name() {
        return node_show_name;
    }

    public void setNode_show_name(String node_show_name) {
        this.node_show_name = node_show_name;
    }
}
