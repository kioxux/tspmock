package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 系统配置
 */
public class SystemConfigModel {
    @ApiModelProperty(value = "健值名称")
    private String config_name;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "配置值")
    private String setting_value;
    @ApiModelProperty(value = "显示名称")
    private String config_title;
    @ApiModelProperty(value = "分组标题")
    private String group_title;
    @ApiModelProperty(value = "数据源值")
    private String source_data;
    @ApiModelProperty(value = "类型  1输入框 2密码框 3下拉框单选 4开关 5富文本框 6下拉框多选")
    private Integer control_type;
    @ApiModelProperty(value = "排序")
    private Integer order_index;

    public String getConfig_name() {
        return config_name;
    }

    public void setConfig_name(String config_name) {
        this.config_name = config_name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSetting_value() {
        return setting_value;
    }

    public void setSetting_value(String setting_value) {
        this.setting_value = setting_value;
    }

    public String getConfig_title() {
        return config_title;
    }

    public void setConfig_title(String config_title) {
        this.config_title = config_title;
    }

    public String getGroup_title() {
        return group_title;
    }

    public void setGroup_title(String group_title) {
        this.group_title = group_title;
    }

    public String getSource_data() {
        return source_data;
    }

    public void setSource_data(String source_data) {
        this.source_data = source_data;
    }

    public Integer getControl_type() {
        return control_type;
    }

    public void setControl_type(Integer control_type) {
        this.control_type = control_type;
    }

    public Integer getOrder_index() {
        return order_index;
    }

    public void setOrder_index(Integer order_index) {
        this.order_index = order_index;
    }
}
