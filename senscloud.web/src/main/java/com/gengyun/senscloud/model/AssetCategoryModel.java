package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class AssetCategoryModel implements Serializable {
    @ApiModelProperty(value = "id")
    private Integer id;
    @ApiModelProperty(value = "设备类型id")
    private Integer asset_category_id;
    @ApiModelProperty(value = "被克隆的设备类型id")
    private Integer clone_asset_category_id;
    @ApiModelProperty(value = "是否开启 1开启 0未开启")
    private String isUseSearch;
    @ApiModelProperty(value = "种类")
    private String assetCategorySearch;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "类型名称")
    private String category_name;
    @ApiModelProperty(value = "种类")
    private Integer type;
    @ApiModelProperty(value = "父类型")
    private Integer parent_id;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "自定义字段")
    private String fields;
    @ApiModelProperty(value = "是否可用")
    private String is_use;
    @ApiModelProperty(value = "排序")
    private Integer data_order;
    @ApiModelProperty(value = "自逻辑编码")
    private String category_code;

    public Integer getClone_asset_category_id() {
        return clone_asset_category_id;
    }

    public void setClone_asset_category_id(Integer clone_asset_category_id) {
        this.clone_asset_category_id = clone_asset_category_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAsset_category_id() {
        return asset_category_id;
    }

    public void setAsset_category_id(Integer asset_category_id) {
        this.asset_category_id = asset_category_id;
    }

    public String getIsUseSearch() {
        return isUseSearch;
    }

    public void setIsUseSearch(String isUseSearch) {
        this.isUseSearch = isUseSearch;
    }

    public String getAssetCategorySearch() {
        return assetCategorySearch;
    }

    public void setAssetCategorySearch(String assetCategorySearch) {
        this.assetCategorySearch = assetCategorySearch;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public Integer getData_order() {
        return data_order;
    }

    public void setData_order(Integer data_order) {
        this.data_order = data_order;
    }

    public String getCategory_code() {
        return category_code;
    }

    public void setCategory_code(String category_code) {
        this.category_code = category_code;
    }
}
