package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "工单流程字段增加信息")
public class WorkFlowTemplateColumnAdd implements Serializable {
    private static final long serialVersionUID = -1L;


    @ApiModelProperty(value = "searchWorkflowInstDspList接口的parent字段,当为流程时，也传id字段",required = true)
    private String pref_id;
    @ApiModelProperty(value = "searchWorkflowInstDspList接口的id字段",required = true)
    private String node_id;
    @ApiModelProperty(value = "流程：flow,开始节点：start,过程节点：node,结束节点：end,子节点：subNode",required = true)
    private String node_type;
    @ApiModelProperty(value = "选中字段主键字符串", required = true)
    private String field_form_codes;
    @ApiModelProperty(value = "字段编码", required = true)
    private String field_code;
    @ApiModelProperty(value = "字段显示名称/key", required = true)
    private String field_name;
    @ApiModelProperty(value = "存储方式")
    private String save_type;
    @ApiModelProperty(value = "写入只读权限", required = true)
    private String field_right;
    @ApiModelProperty(value = "是否必填 1是 2否")
    private String is_required;
    @ApiModelProperty(value = "备注")
    private String field_remark;
    @ApiModelProperty(value = "控件类型", required = true)
    private String field_view_type;
    @ApiModelProperty(value = "所属模块", required = true)
    private String field_section_type;
    @ApiModelProperty(value = "特殊属性列表,格式详见note说明")
    private String extList;
    @ApiModelProperty(value = "字段联动列表,格式详见note说明")
    private String linkList;
    @ApiModelProperty(value = "节点字段表主键")
    private Integer id;
    @ApiModelProperty(value = "处理角色id")
    private String deal_role_id;
    @ApiModelProperty(value = "1：单人处理；2：会签；3：抢单；4：处理链；")
    private String deal_type_id;
    @ApiModelProperty(value = "base 基本页面   detail 详细页面")
    private String page_type;
    @ApiModelProperty(value = "选中主键字符串")
    private String ids;


    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public String getField_form_codes() {
        return field_form_codes;
    }

    public void setField_form_codes(String field_form_codes) {
        this.field_form_codes = field_form_codes;
    }

    public String getField_code() {
        return field_code;
    }

    public void setField_code(String field_code) {
        this.field_code = field_code;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getSave_type() {
        return save_type;
    }

    public void setSave_type(String save_type) {
        this.save_type = save_type;
    }

    public String getField_right() {
        return field_right;
    }

    public void setField_right(String field_right) {
        this.field_right = field_right;
    }

    public String getIs_required() {
        return is_required;
    }

    public void setIs_required(String is_required) {
        this.is_required = is_required;
    }

    public String getField_remark() {
        return field_remark;
    }

    public void setField_remark(String field_remark) {
        this.field_remark = field_remark;
    }

    public String getField_view_type() {
        return field_view_type;
    }

    public void setField_view_type(String field_view_type) {
        this.field_view_type = field_view_type;
    }

    public String getField_section_type() {
        return field_section_type;
    }

    public void setField_section_type(String field_section_type) {
        this.field_section_type = field_section_type;
    }


    public String getLinkList() {
        return linkList;
    }

    public void setLinkList(String linkList) {
        this.linkList = linkList;
    }

    public String getPref_id() {
        return pref_id;
    }

    public void setPref_id(String pref_id) {
        this.pref_id = pref_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeal_role_id() {
        return deal_role_id;
    }

    public void setDeal_role_id(String deal_role_id) {
        this.deal_role_id = deal_role_id;
    }

    public String getDeal_type_id() {
        return deal_type_id;
    }

    public void setDeal_type_id(String deal_type_id) {
        this.deal_type_id = deal_type_id;
    }

    public String getPage_type() {
        return page_type;
    }

    public void setPage_type(String page_type) {
        this.page_type = page_type;
    }

    public String getExtList() {
        return extList;
    }

    public void setExtList(String extList) {
        this.extList = extList;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }
}
