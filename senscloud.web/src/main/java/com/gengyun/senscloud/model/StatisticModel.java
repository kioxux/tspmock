package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 统计分析配置
 */
public class StatisticModel {
    @ApiModelProperty(value = "统计配置id")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
