package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * 抄表计量
 */
public class CheckMeterModel {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "内部编码")
    private String inner_code;

    @ApiModelProperty(value = "厂商名称")
    private String title;

    @ApiModelProperty(value = "status")
    private Integer status;

    @ApiModelProperty(value = "开始日期")
    private String begin_date;

    @ApiModelProperty(value = "结束日期")
    private String end_date;

    @ApiModelProperty(value = "关键字")
    private String keywordSearch;

    @ApiModelProperty(value = "设备id")
    private String asset_id;

    @ApiModelProperty(value = "当前读数")
    private BigDecimal indication;

    @ApiModelProperty(value = "上期行至")
    private BigDecimal last_indication;

    @ApiModelProperty(value = "客户id")
    private Integer facility_id;

    @ApiModelProperty(value = "抄表时间")
    private String check_time;

    @ApiModelProperty(value = "流量")
    private BigDecimal flow;

    @ApiModelProperty(value = "最终流量")
    private BigDecimal final_flow;

    @ApiModelProperty(value = "水质")
    private String water_quality;

    @ApiModelProperty(value = "抄表类型")
    private String check_type;

    @ApiModelProperty(value = "异常类型")
    private String unusual_type;

    @ApiModelProperty(value = "批次号")
    private String batch_no;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "创建人")
    private String create_user_id;

    @ApiModelProperty(value = "修改人")
    private String modify_user_id;

    @ApiModelProperty(value = "超标")
    private String exceed;

    @ApiModelProperty(value = "异常")
    private String unusual;

    @ApiModelProperty(value = "正常")
    private String normal;

    @ApiModelProperty(value = "所属日期")
    private String belong_date;

    public String getExceed() {
        return exceed;
    }

    public String getUnusual() {
        return unusual;
    }

    public String getNormal() {
        return normal;
    }

    public String getBelong_date() {
        return belong_date;
    }

    public void setBelong_date(String belong_date) {
        this.belong_date = belong_date;
    }


    public void setExceed(String exceed) {
        this.exceed = exceed;
    }


    public void setUnusual(String unusual) {
        this.unusual = unusual;
    }


    public void setNormal(String normal) {
        this.normal = normal;
    }


    public String getModify_user_id() {
        return modify_user_id;
    }

    public void setModify_user_id(String modify_user_id) {
        this.modify_user_id = modify_user_id;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public BigDecimal getFlow() {
        return flow;
    }

    public void setFlow(BigDecimal flow) {
        this.flow = flow;
    }

    public BigDecimal getFinal_flow() {
        return final_flow;
    }

    public void setFinal_flow(BigDecimal final_flow) {
        this.final_flow = final_flow;
    }

    public String getWater_quality() {
        return water_quality;
    }

    public void setWater_quality(String water_quality) {
        this.water_quality = water_quality;
    }

    public String getCheck_type() {
        return check_type;
    }

    public void setCheck_type(String check_type) {
        this.check_type = check_type;
    }

    public String getUnusual_type() {
        return unusual_type;
    }

    public void setUnusual_type(String unusual_type) {
        this.unusual_type = unusual_type;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSub_work_code() {
        return sub_work_code;
    }

    public void setSub_work_code(String sub_work_code) {
        this.sub_work_code = sub_work_code;
    }

    @ApiModelProperty(value = "工单号")
    private String sub_work_code;


    public String getCheck_time() {
        return check_time;
    }

    public void setCheck_time(String check_time) {
        this.check_time = check_time;
    }

    public BigDecimal getLast_indication() {
        return last_indication;
    }

    public void setLast_indication(BigDecimal last_indication) {
        this.last_indication = last_indication;
    }

    public BigDecimal getIndication() {
        return indication;
    }

    public void setIndication(BigDecimal indication) {
        this.indication = indication;
    }

    public Integer getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(Integer facility_id) {
        this.facility_id = facility_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getInner_code() {
        return inner_code;
    }

    public void setInner_code(String inner_code) {
        this.inner_code = inner_code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getAsset_id() {
        return asset_id;
    }

    public void setAsset_id(String asset_id) {
        this.asset_id = asset_id;
    }


}
