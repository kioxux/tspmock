package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 工单管理
 */
@ApiModel(description = "工单管理")
public class WorksModel {

    @ApiModelProperty(value = "所属工单号")
    private String work_code;
    @ApiModelProperty(value = "工单号")
    private String sub_work_code;
    @ApiModelProperty(value = "processDefinitionId")
    private String processDefinitionId;
    @ApiModelProperty(value = "所属位置筛选")
    private String positionCodeSearch;
    @ApiModelProperty(value = "设备类型筛选")
    private String assetCategoryIdSearch;
    @ApiModelProperty(value = "设备型号筛选")
    private String assetModelIdSearch;
    @ApiModelProperty(value = "制造商筛选")
    private String manufacturerIdSearch;
    @ApiModelProperty(value = "供应商筛选")
    private String supplierIdSearch;
    @ApiModelProperty(value = "库房筛选")
    private String stockIdSearch;
    @ApiModelProperty(value = "工单类型筛选")
    private String workTypeIdSearch;
    @ApiModelProperty(value = "工单状态筛选10;//草稿（维修保存未提交时）15;//待确认（工单请求确认）20;//待分配（维修未找到维修人时）30;//待接单（维修人未接单时）40;//处理中（维修人接单时，保养点击计时后）50;//待审核（维修完提交审核后）55;//完工报告（维修完提交审核后）60;//已完成（维修完成，保养完成，巡检任务完成）70;//待保养（保养单生成后）80;//待确认（保养完成，提交确认）90;//未提交（巡检任务分配未提交）100;//保养过期（过期后不允许再提交）110;//已关闭（维修完成，保养完成，巡检任务完成，服务单关闭）120;//线下关闭（线下完成，服务单关闭）130;//无效关闭（无效关闭，服务单关闭）140;//已拒绝（审核拒绝）900;//作废（维修和保养单废除后）")
    private String statusSearch;
    @ApiModelProperty(value = "开工时间")
    private String inTypeSearch;
    @ApiModelProperty(value = "入库类型")
    private String bomTypeIdSearch;
    @ApiModelProperty(value = "入库类型")
    private String beginTime;
    @ApiModelProperty(value = "完工时间")
    private String finishedTime;
    @ApiModelProperty(value = "user_assign_type")
    private String user_assign_type;
    @ApiModelProperty(value = "维修工id")
    private String receive_user_id;
    @ApiModelProperty(value = "业务编码")
    private String business_no;
    @ApiModelProperty(value = "工单类型")
    private Integer work_type_id;
    @ApiModelProperty(value = "模糊搜索关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "页面调用模块 工单列表：workList, 工单调度：workDispatch ")
    private String pageType;
    @ApiModelProperty(value = "计划开工时间，工单调度处使用")
    private String plan_begin_time;
    @ApiModelProperty(value = "开工时间，工单调度处使用")
    private String begin_time;
    @ApiModelProperty(value = "开始数量")
    private Integer start_quantity;
    @ApiModelProperty(value = "结束数量")
    private Integer end_quantity;
    @ApiModelProperty(value = "是否显示超时数据：  1是")
    private String overtimeSearch;
    @ApiModelProperty(value = "查询类型  reportToday:今日上报  completedToday:今日完成   timeoutTask:超时任务")
    private String sectionType;
    @ApiModelProperty(value = "开始日期-创建时间")
    private String startCreateDateSearch;
    @ApiModelProperty(value = "结束日期-创建时间")
    private String endCreateDateSearch;

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public Integer getStart_quantity() {
        return start_quantity;
    }

    public void setStart_quantity(Integer start_quantity) {
        this.start_quantity = start_quantity;
    }

    public Integer getEnd_quantity() {
        return end_quantity;
    }

    public void setEnd_quantity(Integer end_quantity) {
        this.end_quantity = end_quantity;
    }

    public String getInTypeSearch() {
        return inTypeSearch;
    }

    public void setInTypeSearch(String inTypeSearch) {
        this.inTypeSearch = inTypeSearch;
    }

    public String getBomTypeIdSearch() {
        return bomTypeIdSearch;
    }

    public void setBomTypeIdSearch(String bomTypeIdSearch) {
        this.bomTypeIdSearch = bomTypeIdSearch;
    }

    public String getStockIdSearch() {
        return stockIdSearch;
    }

    public void setStockIdSearch(String stockIdSearch) {
        this.stockIdSearch = stockIdSearch;
    }

    public String getAssetModelIdSearch() {
        return assetModelIdSearch;
    }

    public void setAssetModelIdSearch(String assetModelIdSearch) {
        this.assetModelIdSearch = assetModelIdSearch;
    }

    public String getManufacturerIdSearch() {
        return manufacturerIdSearch;
    }

    public void setManufacturerIdSearch(String manufacturerIdSearch) {
        this.manufacturerIdSearch = manufacturerIdSearch;
    }

    public String getSupplierIdSearch() {
        return supplierIdSearch;
    }

    public void setSupplierIdSearch(String supplierIdSearch) {
        this.supplierIdSearch = supplierIdSearch;
    }

    public Integer getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(Integer work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getWork_code() {
        return work_code;
    }

    public void setWork_code(String work_code) {
        this.work_code = work_code;
    }

    public String getSub_work_code() {
        return sub_work_code;
    }

    public void setSub_work_code(String sub_work_code) {
        this.sub_work_code = sub_work_code;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getPositionCodeSearch() {
        return positionCodeSearch;
    }

    public void setPositionCodeSearch(String positionCodeSearch) {
        this.positionCodeSearch = positionCodeSearch;
    }

    public String getAssetCategoryIdSearch() {
        return assetCategoryIdSearch;
    }

    public void setAssetCategoryIdSearch(String assetCategoryIdSearch) {
        this.assetCategoryIdSearch = assetCategoryIdSearch;
    }

    public String getWorkTypeIdSearch() {
        return workTypeIdSearch;
    }

    public void setWorkTypeIdSearch(String workTypeIdSearch) {
        this.workTypeIdSearch = workTypeIdSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(String finishedTime) {
        this.finishedTime = finishedTime;
    }

    public String getUser_assign_type() {
        return user_assign_type;
    }

    public void setUser_assign_type(String user_assign_type) {
        this.user_assign_type = user_assign_type;
    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public String getBusiness_no() {
        return business_no;
    }

    public void setBusiness_no(String business_no) {
        this.business_no = business_no;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPlan_begin_time() {
        return plan_begin_time;
    }

    public void setPlan_begin_time(String plan_begin_time) {
        this.plan_begin_time = plan_begin_time;
    }

    public String getOvertimeSearch() {
        return overtimeSearch;
    }

    public void setOvertimeSearch(String overtimeSearch) {
        this.overtimeSearch = overtimeSearch;
    }

    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public String getStartCreateDateSearch() {
        return startCreateDateSearch;
    }

    public void setStartCreateDateSearch(String startCreateDateSearch) {
        this.startCreateDateSearch = startCreateDateSearch;
    }

    public String getEndCreateDateSearch() {
        return endCreateDateSearch;
    }

    public void setEndCreateDateSearch(String endCreateDateSearch) {
        this.endCreateDateSearch = endCreateDateSearch;
    }
}
