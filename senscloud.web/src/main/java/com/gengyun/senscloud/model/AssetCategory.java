package com.gengyun.senscloud.model;

import java.io.Serializable;

public class AssetCategory implements IAssetCategory,Serializable {

//    private static final long serialVersionUID = -8376046194442996119L;
//    private Integer id;
//    private String category_name;
//    private Integer parent_id;
//    private Integer order;
//    private Integer isuse;
//    private String icon;
//    private String category_code;   //区分父子类型的编码：01，0101
//    private String fields;//用户自定义字段
//    private String schema_name;
//
//    @Override
//    public Integer getId() {
//        return id;
//    }
//    @Override
//    public void setId(Integer id) {
//        this.id = id;
//    }
//    @Override
//    public String getCategory_name() {
//        return category_name;
//    }
//    @Override
//    public void setCategory_name(String category_name) {
//        this.category_name = category_name;
//    }
//    @Override
//    public Integer getParent_id() {
//        return parent_id;
//    }
//    @Override
//    public void setParent_id(Integer parent_id) {
//        this.parent_id = parent_id;
//    }
//
//    public Integer getOrder() {
//        return order;
//    }
//
//    public void setOrder(Integer order) {
//        this.order = order;
//    }
//    @Override
//    public Integer getIsuse() {
//        return isuse;
//    }
//    @Override
//    public void setIsuse(Integer isuse) {
//        this.isuse = isuse;
//    }
//    @Override
//    public String getCategoryCode() {
//        return category_code;
//    }
//    @Override
//    public void setCategoryCode(String category_code) {
//        this.category_code = category_code;
//    }
//
//    public AssetCategory() {
//        super();
//    }
//
//    public String getIcon() {
//        return icon;
//    }
//
//    public void setIcon(String icon) {
//        this.icon = icon;
//    }
//
//    public AssetCategory(Integer id, String category_name, Integer parent_id, Integer order, Integer isuse, String icon) {
//        super();
//        this.id = id;
//        this.category_name = category_name;
//        this.parent_id = parent_id;
//        this.order = order;
//        this.isuse = isuse;
//        this.icon = icon;
//    }
//
//    public String getFields() {
//        return fields;
//    }
//
//    public JSONArray getFieldArray() {
//        JSONArray fieldArray = new JSONArray();
//        try {
//            fieldArray = JSONArray.fromObject(this.fields);
//        } catch (Exception ex) {
//            fieldArray = new JSONArray();
//        }
//        return fieldArray;
//    }
//
//    public void setFields(String fields) {
//        this.fields = fields;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//
//    /* 用于生成设备的模板*/
//    //region 字段常量
//    public final static String ASSET_TYPE = "asset_type";
//    public final static String SYS_ID = "_id";
//    public final static String STRNAME = "strname";
//    public final static String STRCODE = "strcode";
////    public final static String STRMODEL = "strmodel";
//    public final static String ENABLE_TIME = "enable_time";
//    public final static String INTSTATUS = "intstatus";
//    public final static String INTSITEID = "intsiteid";
//    public final static String SUPPLIER = "supplier";
//    public final static String MANUFACTURE = "manufacturer";
//    public final static String IOT_STATUS = "iot_status";
//    public final static String USE_ADD = "use_add";
//    public final static String STRDESCRIPTION = "strdescription";
//    public final static String X = "x";
//    public final static String Y = "y";
//    public final static String PARENT_ID = "parent_id";
//    public final static String PARENT_NAME = "parent_name";
//    public final static String POWER = "_power";
//    public final static String CATEGORY_ID = "category_id";
//    public final static String IMPORTMENT_LEVEL_ID = "importment_level_id";
//    public final static String _COUNT = "_count";
//    public final static String UNIT_ID = "unit_id";
//    public final static String PRICE = "price";
//    public final static String TAX_RATE = "tax_rate";
//    public final static String ASSET_MODEL_ID = "asset_model_id";
//    public final static String TAX_PRICE = "tax_price";
//    public final static String CURRENCY = "currency";
//    public final static String BUY_CURRENCY = "buy_currency";
//    public final static String INSTALL_CURRENCY = "install_currency";
//    public final static String ASSET_SUBJECT = "asset_subject";
//    public final static String POSITION_CODE = "position_code";
//    public final static String BUY_DATE = "buy_date";
//    public final static String INSTALL_DATE = "install_date";
//    public final static String OUT_CODE = "out_code";
//    public final static String IN_CODE = "in_code";
//    public final static String TEM_VALUE1 = "tem_value1";
//    public final static String TEM_VALUE2 = "tem_value2";
//    public final static String TEM_VALUE3 = "tem_value3";
//    public final static String TEM_VALUE4 = "tem_value4";
//    public final static String TEM_VALUE5 = "tem_value5";
//    public final static String TEM_VALUE9 = "tem_value9";
//    public final static String SOURCE_TYPE = "source_type";
//    public final static String INSTALL_PRICE = "install_price";
//    public final static String USE_YEAR = "use_year";
//    public final static String BY_CURRENCY_ID = "buy_currency_id";
//    public final static String INSTALL_CURRENCY_ID = "install_currency_id";
//
//    //endregion
//
//    private int index;
//    private String remark;
//
//    @Deprecated
//    private int totalAsset;
//
//    @Override
//    public String toString() {
//        return "AssetCategoryTemplate{schema_name=" + schema_name + ",id='" + id + '\'' +
//                ",category_name='" + category_name + '\'' +
//                ",remark='" + remark + '\'' +
//                ",fields='" + fields + '\'' +
//                '}';
//    }
//
//    public String getRemark() {
//        if (RegexUtil.isNull(remark)) {
//            return "";
//        }
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//
//    public int getIndex() {
//        return index;
//    }
//
//    public void setIndex(int index) {
//        this.index = index;
//    }
//
//    public int getTotalAsset() {
//        return totalAsset;
//    }
//
//    public void setTotalAsset(int totalAsset) {
//        this.totalAsset = totalAsset;
//    }

}
