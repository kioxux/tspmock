package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@ApiModel(description = "文件")
public class FileModel {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "文件ids")
    private String files_ids;
    @ApiModelProperty(value = "数据主键")
    private String data_id;
    @ApiModelProperty(value = "业务类型")
    private String business_no;
    @ApiModelProperty(value = "文件类型")
    private Integer file_category_id;
    @ApiModelProperty(value = "文件列表")
    private List<MultipartFile> files;
    @ApiModelProperty(value = "是否是logo")
    private Boolean is_logo;

    public Integer getFile_category_id() {
        return file_category_id;
    }

    public void setFile_category_id(Integer file_category_id) {
        this.file_category_id = file_category_id;
    }

    public List<MultipartFile> getFiles() {
        return files;
    }

    public void setFiles(List<MultipartFile> files) {
        this.files = files;
    }

    public String getFiles_ids() {
        return files_ids;
    }

    public void setFiles_ids(String files_ids) {
        this.files_ids = files_ids;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_no() {
        return business_no;
    }

    public void setBusiness_no(String business_no) {
        this.business_no = business_no;
    }

    public String getData_id() {
        return data_id;
    }

    public void setData_id(String data_id) {
        this.data_id = data_id;
    }

    public Boolean getIs_logo() {
        return is_logo;
    }

    public void setIs_logo(Boolean is_logo) {
        this.is_logo = is_logo;
    }
}
