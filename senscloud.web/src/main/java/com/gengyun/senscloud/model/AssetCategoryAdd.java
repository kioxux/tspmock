package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel(description = "新增设备类型")
public class AssetCategoryAdd implements Serializable {
//    private static final long serialVersionUID = -1;
//    @ApiModelProperty(value = "类型名称", required = true)
//    private String category_name;
//    @ApiModelProperty(value = "种类", required = true)
//    private String assetCategoryType;
//    @ApiModelProperty(value = "类型编码")
//    private String code;
//    @ApiModelProperty(value = "父类型")
//    private String parent_id;
//    @ApiModelProperty(value = "备注")
//    private String remark;
//    @ApiModelProperty(value = "自定义字段")
//    private String fields;
//    @ApiModelProperty(value = "是否可用")
//    private String isuse;
//    @ApiModelProperty(value = "排序")
//    private String order;
//    @ApiModelProperty(value = "图标id")
//    private String icon;
//    @ApiModelProperty(value = "自逻辑编码")
//    private String category_code;
//    @ApiModelProperty(value = "报警图标id")
//    private String alarm_icon;
//    @ApiModelProperty(value = "创建时间")
//    private String create_time;
//    @ApiModelProperty(value = "创建人")
//    private String create_user_account;
//
//    public String getCategory_name() {
//        return category_name;
//    }
//
//    public void setCategory_name(String category_name) {
//        this.category_name = category_name;
//    }
//
//    public String getAssetCategoryType() {
//        return assetCategoryType;
//    }
//
//    public void setAssetCategoryType(String assetCategoryType) {
//        this.assetCategoryType = assetCategoryType;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }
//
//    public String getParent_id() {
//        return parent_id;
//    }
//
//    public void setParent_id(String parent_id) {
//        this.parent_id = parent_id;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public String getFields() {
//        return fields;
//    }
//
//    public void setFields(String fields) {
//        this.fields = fields;
//    }
//
//    public String getIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(String isuse) {
//        this.isuse = isuse;
//    }
//
//    public String getOrder() {
//        return order;
//    }
//
//    public void setOrder(String order) {
//        this.order = order;
//    }
//
//    public String getIcon() {
//        return icon;
//    }
//
//    public void setIcon(String icon) {
//        this.icon = icon;
//    }
//
//    public String getCategory_code() {
//        return category_code;
//    }
//
//    public void setCategory_code(String category_code) {
//        this.category_code = category_code;
//    }
//
//    public String getAlarm_icon() {
//        return alarm_icon;
//    }
//
//    public void setAlarm_icon(String alarm_icon) {
//        this.alarm_icon = alarm_icon;
//    }
//
//    public String getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(String create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
}
