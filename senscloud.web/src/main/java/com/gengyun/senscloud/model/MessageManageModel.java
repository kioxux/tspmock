package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 消息管理
 */
@ApiModel(description = "消息管理")
public class MessageManageModel {
    @ApiModelProperty(value = "消息id")
    private Integer message_id;
    @ApiModelProperty(value = "消息内容")
    private String msg_content;
    @ApiModelProperty(value = "是否成功 1成功 2未成功")
    private String is_success;
    @ApiModelProperty(value = "业务类型")
    private String business_type;
    @ApiModelProperty(value = "业务编码")
    private String business_no;
    @ApiModelProperty(value = "发送人")
    private String send_user_id;
    @ApiModelProperty(value = "消息类型 1：短信 2邮箱 3：消息通知")
    private Integer type;
    @ApiModelProperty(value = "接收号码")
    private String receive_mobile;
    @ApiModelProperty(value = "接收人")
    private String receive_user_id;
    @ApiModelProperty(value = "是否主接收人 1是 -1不是")
    private String is_main_receiver;
    @ApiModelProperty(value = "是否已读 1是 -1不是")
    private String is_read;
    @ApiModelProperty(value = "关键字搜索")
    private String keywordSearch;
    @ApiModelProperty(value = "开始时间")
    private String startTime;
    @ApiModelProperty(value = "结束时间")
    private String endTime;

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getMessage_id() {
        return message_id;
    }

    public void setMessage_id(Integer message_id) {
        this.message_id = message_id;
    }

    public String getMsg_content() {
        return msg_content;
    }

    public void setMsg_content(String msg_content) {
        this.msg_content = msg_content;
    }

    public String getIs_success() {
        return is_success;
    }

    public void setIs_success(String is_success) {
        this.is_success = is_success;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getBusiness_no() {
        return business_no;
    }

    public void setBusiness_no(String business_no) {
        this.business_no = business_no;
    }

    public String getSend_user_id() {
        return send_user_id;
    }

    public void setSend_user_id(String send_user_id) {
        this.send_user_id = send_user_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getReceive_mobile() {
        return receive_mobile;
    }

    public void setReceive_mobile(String receive_mobile) {
        this.receive_mobile = receive_mobile;
    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public String getIs_main_receiver() {
        return is_main_receiver;
    }

    public void setIs_main_receiver(String is_main_receiver) {
        this.is_main_receiver = is_main_receiver;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }
}
