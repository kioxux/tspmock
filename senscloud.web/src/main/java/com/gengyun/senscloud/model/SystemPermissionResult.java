package com.gengyun.senscloud.model;

import java.util.List;

public class SystemPermissionResult {
    private String id;//企业id
    private String name;//名称
    private String key;//键 字段名（国际化key
    private String url;//默认链接(分厂)
    private String parent_id;//父菜单
    private String lang_key;//国际化lang_key
    private String icon;//样式名
    private int index;//排序
    private int type;//菜单还是功能:1:菜单2:功能3:操作
    private List<SystemPermissionResult> items; //权限子类集合

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLang_key() {
        return lang_key;
    }

    public void setLang_key(String lang_key) {
        this.lang_key = lang_key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<SystemPermissionResult> getItems() {
        return items;
    }

    public void setItems(List<SystemPermissionResult> items) {
        this.items = items;
    }
}
