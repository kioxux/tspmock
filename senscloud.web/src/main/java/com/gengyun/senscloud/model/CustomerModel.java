package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class CustomerModel extends FacilitiesModel {

    @ApiModelProperty(value = "重要等级")
    private String importantLevel;

    @ApiModelProperty(value = "客户类型")
    private String customerType;

    @ApiModelProperty(value = "并管排污")
    private String unloadingGroup;

    @ApiModelProperty(value = "是否已缴费")
    private boolean isPaid;

    @ApiModelProperty(value = "并管组")
    private int unloading_group_id;

    @ApiModelProperty(value = "所在片区")
    private String position_code;

    @ApiModelProperty(value = "管道编号")
    private String pipeline_code;

    @ApiModelProperty(value = "污水去向")
    private String sewage_destination;

    @ApiModelProperty(value = "主要产品")
    private String main_products;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "区")
    private String zone;

    @ApiModelProperty(value = "重要级别")
    private int important_level_id;

    @ApiModelProperty(value = "客户类型")
    private String customer_type_id;

    @ApiModelProperty(value = "组织机构主键")
    private int facility_id;

    @ApiModelProperty(value = "开始时间")
    private String begin_time;

    @ApiModelProperty(value = "结束时间")
    private String end_time;

    @ApiModelProperty(value = "污染因子")
    private String factor_id;

    @ApiModelProperty(value = "纳管限值")
    private BigDecimal limit_value;

    @ApiModelProperty(value = "纳管限值单位")
    private String limit_unit;

    @ApiModelProperty(value = "查询日期类型(year/month)")
    private String date_type;

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getDate_type() {
        return date_type;
    }

    public void setDate_type(String date_type) {
        this.date_type = date_type;
    }

    public String getImportantLevel() {
        return importantLevel;
    }

    public void setImportantLevel(String importantLevel) {
        this.importantLevel = importantLevel;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getUnloadingGroup() {
        return unloadingGroup;
    }

    public void setUnloadingGroup(String unloadingGroup) {
        this.unloadingGroup = unloadingGroup;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    public int getUnloading_group_id() {
        return unloading_group_id;
    }

    public void setUnloading_group_id(int unloading_group_id) {
        this.unloading_group_id = unloading_group_id;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getPipeline_code() {
        return pipeline_code;
    }

    public void setPipeline_code(String pipeline_code) {
        this.pipeline_code = pipeline_code;
    }

    public String getSewage_destination() {
        return sewage_destination;
    }

    public void setSewage_destination(String sewage_destination) {
        this.sewage_destination = sewage_destination;
    }

    public String getMain_products() {
        return main_products;
    }

    public void setMain_products(String main_products) {
        this.main_products = main_products;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public int getImportant_level_id() {
        return important_level_id;
    }

    public void setImportant_level_id(int important_level_id) {
        this.important_level_id = important_level_id;
    }

    public String getCustomer_type_id() {
        return customer_type_id;
    }

    public void setCustomer_type_id(String customer_type_id) {
        this.customer_type_id = customer_type_id;
    }

    public int getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(int facility_id) {
        this.facility_id = facility_id;
    }

    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getFactor_id() {
        return factor_id;
    }

    public void setFactor_id(String factor_id) {
        this.factor_id = factor_id;
    }

    public BigDecimal getLimit_value() {
        return limit_value;
    }

    public void setLimit_value(BigDecimal limit_value) {
        this.limit_value = limit_value;
    }

    public String getLimit_unit() {
        return limit_unit;
    }

    public void setLimit_unit(String limit_unit) {
        this.limit_unit = limit_unit;
    }
}
