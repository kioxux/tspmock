package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 设备位置
 */
public class AssetPositionModel {
    @ApiModelProperty(value = "设备位置编码")
    private String position_code;
    @ApiModelProperty(value = "设备位置名称")
    private String position_name;
    @ApiModelProperty(value = "所属位置编码 父编码")
    private String parent_code;
    @ApiModelProperty(value = "所属位置编码 父编码")
    private String parent_id;
    @ApiModelProperty(value = "坐标")
    private String location;
    @ApiModelProperty(value = "组织图层")
    private String layer_path;
    @ApiModelProperty(value = "位置类型id")
    private Integer position_type_id;
    @ApiModelProperty(value = "位置描述")
    private String remark;
    @ApiModelProperty(value = "平面图id")
    private String file_id;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "位置类型高级筛选")
    private String positionTypeIdSearch;
    @ApiModelProperty(value = "scada选项值,当系统配置需要scada时传")
    private String scada_config;
    @ApiModelProperty(value = "工单类型id")
    private Integer work_type_id;
    @ApiModelProperty(value = "任务模板集合")
    private String[] template_codes;

    public Integer getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(Integer work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String[] getTemplate_codes() {
        return template_codes;
    }

    public void setTemplate_codes(String[] template_codes) {
        this.template_codes = template_codes;
    }

    public String getPositionTypeIdSearch() {
        return positionTypeIdSearch;
    }

    public void setPositionTypeIdSearch(String positionTypeIdSearch) {
        this.positionTypeIdSearch = positionTypeIdSearch;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLayer_path() {
        return layer_path;
    }

    public void setLayer_path(String layer_path) {
        this.layer_path = layer_path;
    }

    public Integer getPosition_type_id() {
        return position_type_id;
    }

    public void setPosition_type_id(Integer position_type_id) {
        this.position_type_id = position_type_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getScada_config() {
        return scada_config;
    }

    public void setScada_config(String scada_config) {
        this.scada_config = scada_config;
    }
}
