package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "字段特殊属性信息")
public class WorkColumnExAdd implements Serializable {
    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "字段主键")
    private String field_form_code;
    @ApiModelProperty(value = "属性名称/taskRight", required = true)
    private String field_name;
    @ApiModelProperty(value = "属性类型/写入只读权限/日期类型/小数点位数/", required = true)
    private String field_write_type;
    @ApiModelProperty(value = "属性值")
    private String field_value;
    private Timestamp create_time;
    private String create_user_id;
    private String node_id;
    private Integer column_id;

    public String getField_form_code() {
        return field_form_code;
    }

    public void setField_form_code(String field_form_code) {
        this.field_form_code = field_form_code;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getField_write_type() {
        return field_write_type;
    }

    public void setField_write_type(String field_write_type) {
        this.field_write_type = field_write_type;
    }

    public String getField_value() {
        return field_value;
    }

    public void setField_value(String field_value) {
        this.field_value = field_value;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public Integer getColumn_id() {
        return column_id;
    }

    public void setColumn_id(Integer column_id) {
        this.column_id = column_id;
    }
}
