package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * 功能：博泽设备监控列表实例Model
 */
public class BroseAssetMonitorModel implements Serializable {
//    private static final long serialVersionUID = -6786315240018063483L;
//    private String _id;//设备编号
//    private String strcode;//设备编码
//    private String strname;//设备名称
//    private String intstatus;//设备状态
//    private String monitor_status;//监控状态
//    private String temperature;//温度
//    private String setting_hum_value;
//    private String setting_tem_value;
//    private String humidity;//湿度
//    private String errorInfo;//错误信息
//    private double x;//x
//    private double y;//y
//    private String gather_time;//采集时间
//
//    public String get_id() {
//        return _id;
//    }
//
//    public void set_id(String _id) {
//        this._id = _id;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public String getIntstatus() {
//        return intstatus;
//    }
//
//    public void setIntstatus(String intstatus) {
//        this.intstatus = intstatus;
//    }
//
//    public String getTemperature() {
//        return temperature;
//    }
//
//    public void setTemperature(String temperature) {
//        this.temperature = temperature;
//    }
//
//    public String getHumidity() {
//        return humidity;
//    }
//
//    public void setHumidity(String humidity) {
//        this.humidity = humidity;
//    }
//
//    public String getErrorInfo() {
//        return errorInfo;
//    }
//
//    public void setErrorInfo(String errorInfo) {
//        this.errorInfo = errorInfo;
//    }
//
//    public double getX() {
//        return x;
//    }
//
//    public void setX(double x) {
//        this.x = x;
//    }
//
//    public double getY() {
//        return y;
//    }
//
//    public void setY(double y) {
//        this.y = y;
//    }
//
//    public String getMonitor_status() {
//        return monitor_status;
//    }
//
//    public void setMonitor_status(String monitor_status) {
//        this.monitor_status = monitor_status;
//    }
//
//    public String getGather_time() {
//        return gather_time;
//    }
//
//    public void setGather_time(String gather_time) {
//        this.gather_time = gather_time;
//    }
//
//    public String getSetting_hum_value() {
//        return setting_hum_value;
//    }
//
//    public void setSetting_hum_value(String setting_hum_value) {
//        this.setting_hum_value = setting_hum_value;
//    }
//
//    public String getSetting_tem_value() {
//        return setting_tem_value;
//    }
//
//    public void setSetting_tem_value(String setting_tem_value) {
//        this.setting_tem_value = setting_tem_value;
//    }
}
