package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.util.List;

public class WorkSheetDistribution implements Serializable {
    private static final long serialVersionUID = -7354099313529945213L;
    private  String receive_name;//维修工姓名
    private  String duty_name;//职称
    private  String expertise;//专长
//    private  String receive_account;
    private  String receive_user_id;
    private int  workload;//任务量
    private int  overtimeCount;//超时任务量
    private List<WorkListModel> workListModel;

//    private List<WorkListDetailModel> workList;//具体任务


    public int getWorkload() {
        return workload;
    }

    public void setWorkload(int workload) {
        this.workload = workload;
    }

    public WorkSheetDistribution() {
        super();
    }

    public List<WorkListModel> getWorkListModel() {
        return workListModel;
    }

    public void setWorkListModel(List<WorkListModel> workListModel) {
        this.workListModel = workListModel;
    }

    public String getReceive_name() {
        return receive_name;
    }

    public void setReceive_name(String receive_name) {
        this.receive_name = receive_name;
    }

//    public String getReceive_account() {
//        return receive_account;
//    }
//
//    public void setReceive_account(String receive_account) {
//        this.receive_account = receive_account;
//    }

//    public List<WorkListDetailModel> getWorkList() {
//        return workList;
//    }
//
//    public void setWorkList(List<WorkListDetailModel> workList) {
//        this.workList = workList;
//    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public int getOvertimeCount() {
        return overtimeCount;
    }

    public void setOvertimeCount(int overtimeCount) {
        this.overtimeCount = overtimeCount;
    }

    public String getDuty_name() {
        return duty_name;
    }

    public void setDuty_name(String duty_name) {
        this.duty_name = duty_name;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }
}
