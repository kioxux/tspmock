package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 任务模板入参
 */
public class TaskTempLateModel {
    @ApiModelProperty(value = "模板任务项关联主键集合")
    private String task_ids;
    @ApiModelProperty(value = "模板编码集合")
    private String task_template_codes;
    @ApiModelProperty(value = "模板任务项关联主键")
    private String task_id;
    @ApiModelProperty(value = "模板编码")
    private String task_template_code;
    @ApiModelProperty(value = "模板名称")
    private String task_template_name;
    @ApiModelProperty(value = "启用状态")
    private String is_use;
    @ApiModelProperty(value = "详细描述")
    private String remark;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "是否启用筛选")
    private String isUseSearch;
    @ApiModelProperty(value = "任务项编码集合")
    private String task_item_codes;
    @ApiModelProperty(value = "任务项编码")
    private String task_item_code;
    @ApiModelProperty(value = "组名称")
    private String group_name;
    @ApiModelProperty(value = "原组名称")
    private String old_group_name;
    @ApiModelProperty(value = "移动方向  上up  下down")
    private String direction;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_ids() {
        return task_ids;
    }

    public void setTask_ids(String task_ids) {
        this.task_ids = task_ids;
    }

    public String getTask_template_code() {
        return task_template_code;
    }

    public String getTask_template_codes() {
        return task_template_codes;
    }

    public void setTask_template_codes(String task_template_codes) {
        this.task_template_codes = task_template_codes;
    }

    public void setTask_template_code(String task_template_code) {
        this.task_template_code = task_template_code;
    }

    public String getTask_template_name() {
        return task_template_name;
    }

    public void setTask_template_name(String task_template_name) {
        this.task_template_name = task_template_name;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getIsUseSearch() {
        return isUseSearch;
    }

    public void setIsUseSearch(String isUseSearch) {
        this.isUseSearch = isUseSearch;
    }

    public String getTask_item_codes() {
        return task_item_codes;
    }

    public void setTask_item_codes(String task_item_codes) {
        this.task_item_codes = task_item_codes;
    }

    public String getTask_item_code() {
        return task_item_code;
    }

    public void setTask_item_code(String task_item_code) {
        this.task_item_code = task_item_code;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getOld_group_name() {
        return old_group_name;
    }

    public void setOld_group_name(String old_group_name) {
        this.old_group_name = old_group_name;
    }
}
