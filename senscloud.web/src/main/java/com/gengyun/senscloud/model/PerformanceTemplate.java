package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * 绩效考核模板对象
 */
public class PerformanceTemplate implements Serializable {

//    private static final long serialVersionUID = -6363918662912165516L;
//    private long id;
//    private String template_name;//模板名称
//    private int period;//周期：1、月度 2、年度
//    private int assessment_type;//考核类型：1、个人 2、团队
//    private String remark;//备注
//    private boolean isuse;//状态 t、启用 f、禁用
//    private String create_user_account;
//    private Timestamp create_time;
//    private String schema_name;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public String getTemplate_name() {
//        return template_name;
//    }
//
//    public void setTemplate_name(String template_name) {
//        this.template_name = template_name;
//    }
//
//    public int getPeriod() {
//        return period;
//    }
//
//    public void setPeriod(int period) {
//        this.period = period;
//    }
//
//    public int getAssessment_type() {
//        return assessment_type;
//    }
//
//    public void setAssessment_type(int assessment_type) {
//        this.assessment_type = assessment_type;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public boolean isIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(boolean isuse) {
//        this.isuse = isuse;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
}
