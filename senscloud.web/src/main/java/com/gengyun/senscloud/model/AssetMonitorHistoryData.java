package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class AssetMonitorHistoryData implements Serializable {

    private static final long serialVersionUID = -8055270096256929382L;
    private int id;
    private String asset_code;
    private String monitor_code;
    private String monitor_name;
    private String monitor_value;
    private Timestamp gather_time;
    private int is_deal_tag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAsset_code() {
        return asset_code;
    }

    public void setAsset_code(String asset_code) {
        this.asset_code = asset_code;
    }

    public String getMonitor_code() {
        return monitor_code;
    }

    public void setMonitor_code(String monitor_code) {
        this.monitor_code = monitor_code;
    }

    public String getMonitor_name() {
        return monitor_name;
    }

    public void setMonitor_name(String monitor_name) {
        this.monitor_name = monitor_name;
    }

    public String getMonitor_value() {
        return monitor_value;
    }

    public void setMonitor_value(String monitor_value) {
        this.monitor_value = monitor_value;
    }

    public Timestamp getGather_time() {
        return gather_time;
    }

    public void setGather_time(Timestamp gather_time) {
        this.gather_time = gather_time;
    }

    public int getIs_deal_tag() {
        return is_deal_tag;
    }

    public void setIs_deal_tag(int is_deal_tag) {
        this.is_deal_tag = is_deal_tag;
    }
}
