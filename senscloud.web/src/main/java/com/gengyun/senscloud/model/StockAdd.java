package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "新增库房信息")
public class StockAdd implements Serializable {
    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "库房编码")
    private String stock_code;
    @ApiModelProperty(value = "库房名称", required = true)
    private String stock_name;
    @ApiModelProperty(value = "管理员id", required = true)
    private String manager_user_id;
    @ApiModelProperty(value = "所在地址名称")
    private String address;
    @ApiModelProperty(value = "所在地址坐标点")
    private Object location;
    @ApiModelProperty(value = "货币单位id")
    private Integer currency_id;
    @ApiModelProperty(value = "详细描述")
    private String remark;
    @ApiModelProperty(value = "是否启用 1启用 0禁用")
    private String is_use;

    public String getStock_code() {
        return stock_code;
    }

    public void setStock_code(String stock_code) {
        this.stock_code = stock_code;
    }

    public String getStock_name() {
        return stock_name;
    }

    public void setStock_name(String stock_name) {
        this.stock_name = stock_name;
    }

    public String getManager_user_id() {
        return manager_user_id;
    }

    public void setManager_user_id(String manager_user_id) {
        this.manager_user_id = manager_user_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public Integer getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(Integer currency_id) {
        this.currency_id = currency_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }
}
