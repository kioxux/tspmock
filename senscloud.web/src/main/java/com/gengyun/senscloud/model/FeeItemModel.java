package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class FeeItemModel {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "编码")
    private String fee_code;

    @ApiModelProperty(value = "名称")
    private String fee_name;

    @ApiModelProperty(value = "开始日期")
    private String begin_date;

    @ApiModelProperty(value = "结束日期")
    private String end_date;

    @ApiModelProperty(value = "超标次数起")
    private Integer begin_over_times;

    @ApiModelProperty(value = "超标次数止")
    private Integer end_over_times;

    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    @ApiModelProperty(value = "超标系数类型")
    private String ratio_cal_type;

    @ApiModelProperty(value = "重复测量单价")
    private BigDecimal repeat_price;

    @ApiModelProperty(value = "启用（1,2）")
    private String is_use;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "关键字")
    private String keywordSearch;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFee_Code() {
        return fee_code;
    }

    public void setFee_Code(String fee_code) {
        this.fee_code = fee_code;
    }

    public String getFee_Name() {
        return fee_name;
    }

    public void setFee_Name(String fee_name) {
        this.fee_name = fee_name;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Integer getBegin_over_times() {
        return begin_over_times;
    }

    public void setBegin_over_times(Integer begin_over_times) {
        this.begin_over_times = begin_over_times;
    }

    public Integer getEnd_over_times() {
        return end_over_times;
    }

    public void setEnd_over_times(Integer end_over_times) {
        this.end_over_times = end_over_times;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getRatio_cal_type() {
        return ratio_cal_type;
    }

    public void setRatio_cal_type(String ratio_cal_type) {
        this.ratio_cal_type = ratio_cal_type;
    }

    public void setRepeat_price(BigDecimal repeat_price) {
        this.repeat_price = repeat_price;
    }

    public BigDecimal getRepeat_price() {
        return repeat_price;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }
}
