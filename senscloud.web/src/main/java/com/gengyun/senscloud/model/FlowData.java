package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.util.List;

public class FlowData implements Serializable {
    private static final long serialVersionUID = -1671755417529883651L;
    private String name;
    private String formKey;
    private Integer order;
    private String receive_account;
    private String sub_work_code;
    private String deadline_time;
    private String facility_id;
    private List<String> candidateUsers;
    private List<String> candidateGroups;
    private String create_time;
    private String request_param;
    private String sub_token;
    private String pref_id;
    private String work_code;
    private String main_sub_work_code;
    private String position_code;
    private String work_type_id;
    private String business_type_id;
    private String node_id;
    private String priority_level;
    private String title_page;
    private String receive_user_id;

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public String getTitle_page() {
        return title_page;
    }

    public void setTitle_page(String title_page) {
        this.title_page = title_page;
    }

    public String getPriority_level() {
        return priority_level;
    }

    public void setPriority_level(String priority_level) {
        this.priority_level = priority_level;
    }

    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(String work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getBusiness_type_id() {
        return business_type_id;
    }

    public void setBusiness_type_id(String business_type_id) {
        this.business_type_id = business_type_id;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getWork_code() {
        return work_code;
    }

    public void setWork_code(String work_code) {
        this.work_code = work_code;
    }

    public String getMain_sub_work_code() {
        return main_sub_work_code;
    }

    public void setMain_sub_work_code(String main_sub_work_code) {
        this.main_sub_work_code = main_sub_work_code;
    }

    public String getPref_id() {
        return pref_id;
    }

    public void setPref_id(String pref_id) {
        this.pref_id = pref_id;
    }

    public String getSub_token() {
        return sub_token;
    }

    public void setSub_token(String sub_token) {
        this.sub_token = sub_token;
    }

    public String getRequest_param() {
        return request_param;
    }

    public void setRequest_param(String request_param) {
        this.request_param = request_param;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getReceive_account() {
        return receive_account;
    }

    public void setReceive_account(String receive_account) {
        this.receive_account = receive_account;
    }

    public String getSub_work_code() {
        return sub_work_code;
    }

    public void setSub_work_code(String sub_work_code) {
        this.sub_work_code = sub_work_code;
    }

    public String getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(String facility_id) {
        this.facility_id = facility_id;
    }

    public String getDeadline_time() {
        return deadline_time;
    }

    public void setDeadline_time(String deadline_time) {
        this.deadline_time = deadline_time;
    }

    public List<String> getCandidateUsers() {
        return candidateUsers;
    }

    public void setCandidateUsers(List<String> candidateUsers) {
        this.candidateUsers = candidateUsers;
    }

    public List<String> getCandidateGroups() {
        return candidateGroups;
    }

    public void setCandidateGroups(List<String> candidateGroups) {
        this.candidateGroups = candidateGroups;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
