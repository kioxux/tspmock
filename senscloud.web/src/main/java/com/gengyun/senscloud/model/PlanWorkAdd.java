package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 功能：计划工单
 * Created by Dong wudang on 2018/11/24.
 */
@ApiModel(description = "计划新增信息")
public class PlanWorkAdd implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "计划主键")
    private String plan_code;//程序定义，编码
    @ApiModelProperty(value = "选中的计划主键，逗号拼接")
    private String planCodes;
    @ApiModelProperty(value = "任务名称", required = true)
    private String plan_name;//任务名称
    @ApiModelProperty(value = "工单类型id", required = true)
    private String work_type_id;//工单类型id
    @ApiModelProperty(value = "开始日期", required = true)
    private String begin_time;//开始日期
    @ApiModelProperty(value = "截止日期", required = true)
    private String end_time;//截止日期
    @ApiModelProperty(value = "紧急程度", required = true)
    private String priority_level;//紧急程度
    @ApiModelProperty(value = "是否启用", required = true)
    private String is_use;//是否启用
//    @ApiModelProperty(value = "任务项")
//    private String taskList;//任务项
    @ApiModelProperty(value = "维保对象", required = true)
    private String maintenanceObject;//维保对象
    @ApiModelProperty(value = "参与团队", required = true)
    private String assistList;//参与团队
    @ApiModelProperty(value = "计划触发条件(时间)", required = true )
    private String triggleInfo;//计划触发条件
    @ApiModelProperty(value = "计划触发条件（条件）", required = true )
    private String conditionTriggle;
    @ApiModelProperty(value = "维护资源" )
    private String maintainResources;//维护资源
    @ApiModelProperty(value = "参考附件" )
    private String attachment;//参考附件
    @ApiModelProperty(value = "风险评估" )
    private String riskAssessment;//风险评估
    @ApiModelProperty(value = "任务项，风险评估等页签传参成json字符串格式，详细格式见note备注", required = true)
    private String planJson;//风险评估
    @ApiModelProperty(value = "是否创建计划，1是 0否", required = true)
    private String isCreateSchedule;//风险评估
    @ApiModelProperty(value = "是否自动生成工单： 1：生成； 2：不生成；", required = true)
    private String auto_generate_bill;//是否自动生成工单
    @ApiModelProperty(value = "触发条件：1：所有条件满足执行；2：任一条件满足即执行；", required = true)
    private String do_type;// 触发条件：1：所有条件满足执行；2：任一条件满足即执行；

    @ApiModelProperty(value = "开始时间" )
    private String occur_time;
    @ApiModelProperty(value = "截止时间" )
    private String deadline_time;
    @ApiModelProperty(value = "计划行事历code" )
    private String work_calendar_code;
    @ApiModelProperty(value = "接收人" )
    private String receive_user_id;
    @ApiModelProperty(value = "部门id" )
    private String group_id;

    @ApiModelProperty(value = "自动生成工单时间点" )
    private String generate_time_point;

//    private String relation_type;//关联对象：1：位置2：设备3：点巡检点9：其他；


//    private int facility_id;//所属位置
//    private String pool_id;//工单池
//    private Map<String, Object> task_list;//任务列表
//    private String task_list_word;//任务列表字符串
//    private float plan_hour;//预计工时
//    private int period;//周期0：一次性任务；>0：周期值；
//    private int pre_day;//提前生成天

//    private String remark;//备注
//    private int status;
//    private Timestamp create_time;//创建时间
//    private String create_user_account;//创建人
//    private int allocate_receiver_type;//分配人员配置
//    private int generate_bill_type;//生成工单配置
//    private String facility_name;
//    private String executor;//执行人
//    private String assist;//外协
//    /*扩展字段*/
//    private String work_type_name;
//    //工单类型对应的工单模板，是否有子工单,1:
//    private int is_have_sub;
//    //业务类型id
//    private int business_type_id;
//    private String business_name;
//    private String flow_template_code;
//    private String work_template_code;


    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(String work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getPriority_level() {
        return priority_level;
    }

    public void setPriority_level(String priority_level) {
        this.priority_level = priority_level;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

//    public String getTaskList() {
//        return taskList;
//    }
//
//    public void setTaskList(String taskList) {
//        this.taskList = taskList;
//    }

    public String getMaintenanceObject() {
        return maintenanceObject;
    }

    public void setMaintenanceObject(String maintenanceObject) {
        this.maintenanceObject = maintenanceObject;
    }

    public String getAssistList() {
        return assistList;
    }

    public void setAssistList(String assistList) {
        this.assistList = assistList;
    }

    public String getTriggleInfo() {
        return triggleInfo;
    }

    public void setTriggleInfo(String triggleInfo) {
        this.triggleInfo = triggleInfo;
    }

    public String getMaintainResources() {
        return maintainResources;
    }

    public void setMaintainResources(String maintainResources) {
        this.maintainResources = maintainResources;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getRiskAssessment() {
        return riskAssessment;
    }

    public void setRiskAssessment(String riskAssessment) {
        this.riskAssessment = riskAssessment;
    }

    public String getPlanCodes() {
        return planCodes;
    }

    public void setPlanCodes(String planCodes) {
        this.planCodes = planCodes;
    }

    public String getPlanJson() {
        return planJson;
    }

    public void setPlanJson(String planJson) {
        this.planJson = planJson;
    }

    public String getPlan_code() {
        return plan_code;
    }

    public void setPlan_code(String plan_code) {
        this.plan_code = plan_code;
    }

    public String getIsCreateSchedule() {
        return isCreateSchedule;
    }

    public void setIsCreateSchedule(String isCreateSchedule) {
        this.isCreateSchedule = isCreateSchedule;
    }

    public String getAuto_generate_bill() {
        return auto_generate_bill;
    }

    public void setAuto_generate_bill(String auto_generate_bill) {
        this.auto_generate_bill = auto_generate_bill;
    }

    public String getDo_type() {
        return do_type;
    }

    public void setDo_type(String do_type) {
        this.do_type = do_type;
    }

    public String getConditionTriggle() {
        return conditionTriggle;
    }

    public void setConditionTriggle(String conditionTriggle) {
        this.conditionTriggle = conditionTriggle;
    }


    public String getOccur_time() {
        return occur_time;
    }

    public void setOccur_time(String occur_time) {
        this.occur_time = occur_time;
    }

    public String getDeadline_time() {
        return deadline_time;
    }

    public void setDeadline_time(String deadline_time) {
        this.deadline_time = deadline_time;
    }

    public String getWork_calendar_code() {
        return work_calendar_code;
    }

    public void setWork_calendar_code(String work_calendar_code) {
        this.work_calendar_code = work_calendar_code;
    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGenerate_time_point() {
        return generate_time_point;
    }

    public void setGenerate_time_point(String generate_time_point) {
        this.generate_time_point = generate_time_point;
    }
}

