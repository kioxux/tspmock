package com.gengyun.senscloud.model;

import java.io.Serializable;

public class RepairData implements Serializable {

//    private static final long serialVersionUID = -7244322188646196914L;
//    private String repair_code;
//
//    public String getRepairCode() {
//        return repair_code;
//    }
//
//    public void setRepairCode(String repair_code) {
//        this.repair_code = repair_code;
//    }
//
//    private String asset_type;
//
//    public String getAssetType() {
//        return asset_type;
//    }
//
//    public void setAssetType(String asset_type) {
//        this.asset_type = asset_type;
//    }
//
//    private String asset_name;
//
//    public String getAssetName() {
//        return asset_name;
//    }
//
//    public void setAssetName(String asset_name) {
//        this.asset_name = asset_name;
//    }
//
//    private String asset_code;
//
//    public String getAssetCode() {
//        return asset_code;
//    }
//
//    public void setAssetCode(String asset_code) {
//        this.asset_code = asset_code;
//    }
//
//    private String asset_id;
//
//    public String getAssetId() {
//        return asset_id;
//    }
//
//    public void setAssetId(String asset_id) {
//        this.asset_id = asset_id;
//    }
//
//    private String properties;
//
//    public String getAssetProperties() {
//        return properties;
//    }
//
//    public void setAssetProperties(String properties) {
//        this.properties = properties;
//    }
//
//    private int asset_status;
//
//    public int getAssetStatus() {
//        return asset_status;
//    }
//
//    public void setAssetStatus(int asset_status) {
//        this.asset_status = asset_status;
//    }
//
//    private String asset_status_name;
//
//    public String getAssetStatusName() {
//        return asset_status_name;
//    }
//
//    public void setAssetStatusName(String asset_status_name) {
//        this.asset_status_name = asset_status_name;
//    }
//
//    private int fault_type;
//
//    public int getFaultType() {
//        return fault_type;
//    }
//
//    public void setFaultType(int fault_type) {
//        this.fault_type = fault_type;
//    }
//
//    private String fault_type_name;
//
//    public String getFaultTypeName() {
//        return fault_type_name == null ? "" : fault_type_name;
//    }
//
//    public void setFaultTypeName(String fault_type_name) {
//        this.fault_type_name = fault_type_name;
//    }
//
//    private int priority_level;
//
//    public int getPriorityLevel() {
//        return priority_level;
//    }
//
//    public void setPriorityLevel(int priority_level) {
//        this.priority_level = priority_level;
//    }
//
//    private Timestamp occur_time;
//
//    public Timestamp getOccurtime() {
//        return occur_time;
//    }
//
//    public void setOccurtime(Timestamp occur_time) {
//        this.occur_time = occur_time;
//    }
//
//    private Timestamp deadline_time;
//
//    public Timestamp getDeadlineTime() {
//        return deadline_time;
//    }
//
//    public void setDeadlineTime(Timestamp deadline_time) {
//        this.deadline_time = deadline_time;
//    }
//
//    private String fault_note;
//
//    public String getFaultNote() {
//        return fault_note;
//    }
//
//    public void setFaultNote(String fault_note) {
//        this.fault_note = fault_note;
//    }
//
//    private String fault_img;
//
//    public String getFaultImage() {
//        return fault_img;
//    }
//
//    public void setFaultImage(String fault_img) {
//        this.fault_img = fault_img;
//    }
//
//    private int repair_type;
//
//    public int getRepairType() {
//        return repair_type;
//    }
//
//    public void setRepairType(int repair_type) {
//        this.repair_type = repair_type;
//    }
//
//    private String repair_type_name;
//
//    public String getRepairTypeName() {
//        return repair_type_name;
//    }
//
//    public void setRepairTypeName(String repair_type_name) {
//        this.repair_type_name = repair_type_name;
//    }
//
//    private String distribute_account;
//
//    public String getDistributeAccount() {
//        return distribute_account;
//    }
//
//    public void setDistributeAccount(String distribute_account) {
//        this.distribute_account = distribute_account;
//    }
//
//    private String distribute_name;
//
//    public String getDistributeName() {
//        return distribute_name;
//    }
//
//    public void setDistributeName(String distribute_name) {
//        this.distribute_name = distribute_name;
//    }
//
//    private Timestamp distribute_time;
//
//    public Timestamp getDistributeTime() {
//        return distribute_time;
//    }
//
//    public void setDistributeTime(Timestamp distribute_time) {
//        this.distribute_time = distribute_time;
//    }
//
//    private String receive_account;
//
//    public String getReceiveAccount() {
//        return receive_account;
//    }
//
//    public void setReceiveAccount(String receive_account) {
//        this.receive_account = receive_account;
//    }
//
//    private String receive_name;
//
//    public String getReceiveName() {
//        return receive_name;
//    }
//
//    public void setReceiveName(String receive_name) {
//        this.receive_name = receive_name;
//    }
//
//    private Timestamp receive_time;
//
//    public Timestamp getReveiveTime() {
//        return receive_time;
//    }
//
//    public void setReveiveTime(Timestamp receive_time) {
//        this.receive_time = receive_time;
//    }
//
//    private Timestamp repair_begin_time;
//
//    public Timestamp getRepairBeginTime() {
//        return repair_begin_time;
//    }
//
//    public void setRepairBeginTime(Timestamp repair_begin_time) {
//        this.repair_begin_time = repair_begin_time;
//    }
//
//    private float spend_hour;
//
//    public float getSpendHour() {
//        return spend_hour;
//    }
//
//    public void setSpendHour(float spend_hour) {
//        this.spend_hour = spend_hour;
//    }
//
//    private float total_work_hour;
//
//    public float getTotalWorkHour() {
//        return total_work_hour;
//    }
//
//    public void setTotalWorkHour(float total_work_hour) {
//        this.total_work_hour = total_work_hour;
//    }
//
//    private String before_img;
//
//    public String getBeforeImage() {
//        return before_img;
//    }
//
//    public void setBeforeImage(String before_img) {
//        this.before_img = before_img;
//    }
//
//    private String repair_note;
//
//    public String getRepairNote() {
//        return repair_note == null ? "" : repair_note;
//    }
//
//    public void setRepairNote(String repair_note) {
//        this.repair_note = repair_note;
//    }
//
//    private String after_img;
//
//    public String getAfterImage() {
//        return after_img;
//    }
//
//    public void setAfterImage(String after_img) {
//        this.after_img = after_img;
//    }
//
//    private Timestamp finished_time;
//
//    public Timestamp getFinishedTime() {
//        return finished_time;
//    }
//
//    public void setFinishedTime(Timestamp finished_time) {
//        this.finished_time = finished_time;
//    }
//
//    private String audit_account;
//
//    public String getAuditAccount() {
//        return audit_account;
//    }
//
//    public void setAuditAccount(String audit_account) {
//        this.audit_account = audit_account;
//    }
//
//    private String audit_account_name;
//
//    public String getAuditAccountusName() {
//        return audit_account_name;
//    }
//
//    public void setAuditAccountName(String audit_account_name) {
//        this.audit_account_name = audit_account_name;
//    }
//
//    private Timestamp audit_time;
//
//    public Timestamp getAuditTime() {
//        return audit_time;
//    }
//
//    public void setAuditTime(Timestamp audit_time) {
//        this.audit_time = audit_time;
//    }
//
//    private String audit_word;
//
//    public String getAuditWord() {
//        return audit_word;
//    }
//
//    public void setAuditWord(String audit_word) {
//        this.audit_word = audit_word;
//    }
//
//    private int status;
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    private String status_name;
//
//    public String getStatusName() {
//        return status_name;
//    }
//
//    public void setStatusName(String status_name) {
//        this.status_name = status_name;
//    }
//
//    private int facility_id;
//
//    public int getFacilityId() {
//        return facility_id;
//    }
//
//    public void setFacilityId(int facility_id) {
//        this.facility_id = facility_id;
//    }
//
//    private String facility_name;
//
//    public String getFacilityName() {
//        return facility_name;
//    }
//
//    public void setFacilityName(String facility_name) {
//        this.facility_name = facility_name;
//    }
//
//    private String parent_name;
//
//    public String getParentName() {
//        return parent_name;
//    }
//
//    public void setParentName(String parent_name) {
//        this.parent_name = parent_name;
//    }
//
//    private Timestamp createtime;
//
//    public Timestamp getCreatetime() {
//        return createtime;
//    }
//
//    public void setCreatetime(Timestamp createtime) {
//        this.createtime = createtime;
//    }
//
//    private String create_user_account;
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    private String create_user_name;
//
//    public String getCreate_user_name() {
//        return create_user_name;
//    }
//
//    public void setCreate_user_name(String create_user_name) {
//        this.create_user_name = create_user_name;
//    }
//
//    private String suppilerName;
//
//    public String getSuppilerName() {
//        return suppilerName;
//    }
//
//    public void setSuppilerName(String suppilerName) {
//        this.suppilerName = suppilerName;
//    }
//
//    private String remark;
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    //来源单号
//    private String from_code;
//
//    public String getFromCode() {
//        return from_code;
//    }
//
//    public void setFromCode(String from_code) {
//        this.from_code = from_code;
//    }
//
//    private List<RepairBomData> bomList;
//
//    public List<RepairBomData> getBomList() {
//        return bomList;
//    }
//
//    public void setBomList(List<RepairBomData> bomList) {
//        this.bomList = bomList;
//    }
//
//    private List<RepairWorkHoursData> workHourList;
//
//    public List<RepairWorkHoursData> getWorkHourList() {
//        return workHourList;
//    }
//
//    public void setWorkHourList(List<RepairWorkHoursData> workHourList) {
//        this.workHourList = workHourList;
//    }
//
//    private String operators;
//
//    public String getOperators() {
//        return operators;
//    }
//
//    public void setOperators(String operators) {
//        this.operators = operators;
//    }
//
//    //历史记录
//    private List<LogsData> historyList;
//
//    public List<LogsData> getHistoryList() {
//        return historyList;
//    }
//
//    public void setHistoryList(List<LogsData> historyList) {
//        this.historyList = historyList;
//    }
//
//    //是否完成
//    private boolean isFinished;
//
//    public boolean getIsFinished() {
//        return isFinished;
//    }
//
//    public void setIsFinished(boolean isFinished) {
//        this.isFinished = isFinished;
//    }
//
//    private List<StockData> bomStock;
//
//    public List<StockData> getBomStock() {
//        return bomStock;
//    }
//
//    public void setBomStock(List<StockData> bomStock) {
//        this.bomStock = bomStock;
//    }
//
//    private int waiting;
//
//    public int getWaiting() {
//        return waiting;
//    }
//
//    public void setWaiting(int waiting) {
//        this.waiting = waiting;
//    }
//
//    private String bom_app_result;
//
//    public String getBomAppResult() {
//        return bom_app_result;
//    }
//
//    public void setBomAppResult(String bom_app_result) {
//        this.bom_app_result = bom_app_result;
//    }
//
//
//    private String mobile;
//
//    public String getMobile() {
//        return mobile;
//    }
//
//    public void setMobile(String mobile) {
//        this.mobile = mobile;
//    }
//
//    private String need_bom_application;
//
//    public String getNeed_bom_application() {
//        return need_bom_application;
//    }
//
//    public void setNeed_bom_application(String need_bom_application) {
//        this.need_bom_application = need_bom_application;
//    }
}
