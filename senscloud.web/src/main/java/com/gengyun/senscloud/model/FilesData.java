package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class FilesData implements Serializable {
    private static final long serialVersionUID = 3998151286468987478L;
    private Integer id;
    private String file_name;
    private Integer file_type_id;
    private Double file_size;
    private String file_original_name;
    private String file_url;
    private String remark;
    private String is_use;
    private Timestamp create_time;
    private String create_user_account;
    private String create_user_id;
    private Integer file_width;
    private Integer file_height;
    private String create_time_str;
    private Integer file_category_id;
    private String business_no;
    private String business_no_id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public Integer getFile_type_id() {
        return file_type_id;
    }

    public void setFile_type_id(Integer file_type_id) {
        this.file_type_id = file_type_id;
    }

    public Double getFile_size() {
        return file_size;
    }

    public void setFile_size(Double file_size) {
        this.file_size = file_size;
    }

    public String getFile_original_name() {
        return file_original_name;
    }

    public void setFile_original_name(String file_original_name) {
        this.file_original_name = file_original_name;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public Integer getFile_width() {
        return file_width;
    }

    public void setFile_width(Integer file_width) {
        this.file_width = file_width;
    }

    public Integer getFile_height() {
        return file_height;
    }

    public void setFile_height(Integer file_height) {
        this.file_height = file_height;
    }

    public String getCreate_time_str() {
        return create_time_str;
    }

    public void setCreate_time_str(String create_time_str) {
        this.create_time_str = create_time_str;
    }

    public Integer getFile_category_id() {
        return file_category_id;
    }

    public void setFile_category_id(Integer file_category_id) {
        this.file_category_id = file_category_id;
    }

    public String getBusiness_no() {
        return business_no;
    }

    public void setBusiness_no(String business_no) {
        this.business_no = business_no;
    }

    public String getBusiness_no_id() {
        return business_no_id;
    }

    public void setBusiness_no_id(String business_no_id) {
        this.business_no_id = business_no_id;
    }
}
