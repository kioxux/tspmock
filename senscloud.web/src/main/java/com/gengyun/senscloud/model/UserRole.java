//package com.gengyun.senscloud.model;
//
//import com.gengyun.senscloud.common.LangConstant;
//
//import java.io.Serializable;
//import java.sql.Timestamp;
//import java.util.LinkedHashMap;
//
//public class UserRole extends UserRoleBase implements Serializable {
//    public final static String ROLE_ID_COMPANY_ADMIN = "2000";
//    public final static String ROLE_NAME_COMPANY_ADMIN = LangConstant.ENTERPRISE_ADMINISTRATOR;//企业管理员
//
//    public final static String ROLE_ID_SYSTEM_ADMIN = "1000";
//    public final static String ROLE_NAME_SYSTEM_ADMIN = LangConstant.SYSTEM_ADMINISTRATOR;//系统管理员
//
//    public final static String ROLE_ID_REPAIR_FREE = "500";
//    public final static String ROLE_NAME_REPAIR_FREE= LangConstant.CREATE_REPAIR_USER;//维修上报员
//    private static final long serialVersionUID = -4749281993858591104L;
//
//
//    private String description; // 角色描述,UI界面显示使用
//    private Timestamp createtime;
//
//    private LinkedHashMap<String, ISystemPermission> permissions;//一个角色可以有多个权限
//
//    public String getDescription() {
//        return description == null ? "" : description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public Timestamp getCreatetime() {
//        return createtime;
//    }
//
//    public void setCreatetime(Timestamp createtime) {
//        this.createtime = createtime;
//    }
//
//    public LinkedHashMap<String, ISystemPermission> getPermissions() {
//        return permissions;
//    }
//
//    public void setPermissions(LinkedHashMap<String, ISystemPermission> permissions) {
//        this.permissions = permissions;
//    }
//}
