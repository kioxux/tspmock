package com.gengyun.senscloud.model;

import java.io.Serializable;

public class WorkSheet implements Serializable {
//    private static final long serialVersionUID = 3927018131444559574L;
//    private String work_code;               //工单编号
//    private String type_name;               //工单类型
//    private String title;                   //工单描述
//    private String receive_name;            //接收人
//    private int status;                     //工单状态
//    private String asset_name;              //设备名称
//    private int priority_level;             //优先级
//    private Timestamp deadline_time;        //截止时间
//    private int work_type_id;               //类型ID
//    private String relation_id;
//    private String receive_account;
//    private Timestamp occur_time;           //发送
//    private String sub_work_code;           //子工单id
//    private String facility_name;           //位置
//    private int facility_id;                //位置id
//    private String work_template_code;      //工单模板编码
//    private String parent_code;             //所属工单模板；
//    private String pool_id;                //工单池id
//    private Integer relation_type;          //关联对象
//    private Timestamp create_time;          //创建时间
//    private String status_name;             //分配状态
//    private Timestamp receive_time;         //接受
//    private String problem_note;            //问题
//    private String problem_img;            //问题图片
//    private String strcode;//
//    private String flow_code;               //模版code
//    private int business_type_id;           //对应的类型
//    private Boolean is_handle;              //当前用户是否有处理权限
//    private Timestamp begin_time;           //开始时间
//    private Timestamp distribute_time;           //开始时间
//    private Timestamp finished_time;         //完成时间
//    private Timestamp audit_time;           //确认时间
//    private String customer_name;             //分配状态
//    private String running_status;            // 设备运行状态
//
//    private float fee_amount;            // 合计
//    private float fee_1;            // 工时费
//    private float fee_2;            // 物料费
//    private float fee_9;            // 其他费用
//    private String fee_currency;    // 货币
//    private String fee_amount_currency;    // 按货币单位合计
//    private int service_score;
//    private String fault_code_name;
//    private String fault_code;
//    private String interval; // 时效
//    // 修改工单时的模板编号
//    private String update_code;
//    private String create_user_account;//创建人账号
//    private String create_user_name;//创建人姓名
//
//    public String getFault_code() {
//        return fault_code;
//    }
//
//    public void setFault_code(String fault_code) {
//        this.fault_code = fault_code;
//    }
//
//    public String getFault_code_name() {
//        return fault_code_name;
//    }
//
//    public void setFault_code_name(String fault_code_name) {
//        this.fault_code_name = fault_code_name;
//    }
//
//    public int getService_score() {
//        return service_score;
//    }
//
//    public void setService_score(int service_score) {
//        this.service_score = service_score;
//    }
//
//    public float getFee_amount() {
//        return fee_amount;
//    }
//
//    public void setFee_amount(float fee_amount) {
//        this.fee_amount = fee_amount;
//    }
//
//    public String getRunning_status() {
//        return running_status;
//    }
//
//    public void setRunning_status(String running_status) {
//        this.running_status = running_status;
//    }
//
//    public Timestamp getDistribute_time() {
//        return distribute_time;
//    }
//
//    public void setDistribute_time(Timestamp distribute_time) {
//        this.distribute_time = distribute_time;
//    }
//
//    private String fault_name;              //故障类型
//    private String repair_type_name;        //维修类型
//    private String parent_name;             //父场地名称
//    private String result_note;             //处理结果
//
//    public Boolean getIs_handle() {
//        return is_handle;
//    }
//
//    public void setIs_handle(Boolean is_handle) {
//        this.is_handle = is_handle;
//    }
//
//    private String asset_type;
//
//    public String getAsset_type() {
//        return asset_type;
//    }
//
//    public void setAsset_type(String asset_type) {
//        this.asset_type = asset_type;
//    }
//
//    public int getBusiness_type_id() {
//        return business_type_id;
//    }
//
//    public void setBusiness_type_id(int business_type_id) {
//        this.business_type_id = business_type_id;
//    }
//
//    public String getFlow_code() {
//        return flow_code;
//    }
//
//    public void setFlow_code(String flow_code) {
//        this.flow_code = flow_code;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getProblem_note() {
//        return problem_note;
//    }
//
//    public void setProblem_note(String problem_note) {
//        this.problem_note = problem_note;
//    }
//
//    public String getProblem_img() {
//        return problem_img;
//    }
//
//    public void setProblem_img(String problem_img) {
//        this.problem_img = problem_img;
//    }
//
//    public Timestamp getReceive_time() {
//        return receive_time;
//    }
//
//    public void setReceive_time(Timestamp receive_time) {
//        this.receive_time = receive_time;
//    }
//
//    public String getStatus_name() {
//        return status_name;
//    }
//
//    public void setStatus_name(String status_name) {
//        this.status_name = status_name;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public String getCreate_user_name() {
//        return create_user_name;
//    }
//
//    public void setCreate_user_name(String create_user_name) {
//        this.create_user_name = create_user_name;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public Integer getRelation_type() {
//        return relation_type;
//    }
//
//    public void setRelation_type(Integer relation_type) {
//        this.relation_type = relation_type;
//    }
//
//    public String getPool_id() {
//        return pool_id;
//    }
//
//    public void setPool_id(String pool_id) {
//        this.pool_id = pool_id;
//    }
//
//    public String getParent_code() {
//        return parent_code;
//    }
//
//    public void setParent_code(String parent_code) {
//        this.parent_code = parent_code;
//    }
//
//    public String getWork_template_code() {
//        return work_template_code;
//    }
//
//    public void setWork_template_code(String work_template_code) {
//        this.work_template_code = work_template_code;
//    }
//
//    public String getFacility_name() {
//        return facility_name;
//    }
//
//    public void setFacility_name(String facility_name) {
//        this.facility_name = facility_name;
//    }
//
//    public int getFacility_id() {
//        return facility_id;
//    }
//
//    public void setFacility_id(int facility_id) {
//        this.facility_id = facility_id;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    private String remark;
//
//    public String getSub_work_code() {
//        return sub_work_code;
//    }
//
//    public void setSub_work_code(String sub_work_code) {
//        this.sub_work_code = sub_work_code;
//    }
//
//    public Timestamp getOccur_time() {
//        return occur_time;
//    }
//
//    public void setOccur_time(Timestamp occur_time) {
//        this.occur_time = occur_time;
//    }
//
//    public String getReceive_name() {
//        return receive_name;
//    }
//
//    public void setReceive_name(String receive_name) {
//        this.receive_name = receive_name;
//    }
//
//    public String getReceive_account() {
//        return receive_account;
//    }
//
//    public void setReceive_account(String receive_account) {
//        this.receive_account = receive_account;
//    }
//
//    public String getRelation_id() {
//        return relation_id;
//    }
//
//    public void setRelation_id(String relation_id) {
//        this.relation_id = relation_id;
//    }
//
//    public int getWork_type_id() {
//        return work_type_id;
//    }
//
//    public void setWork_type_id(int work_type_id) {
//        this.work_type_id = work_type_id;
//    }
//
//    public int getPriority_level() {
//        return priority_level;
//    }
//
//    public void setPriority_level(int priority_level) {
//        this.priority_level = priority_level;
//    }
//
//    public Timestamp getDeadline_time() {
//        return deadline_time;
//    }
//
//    public void setDeadline_time(Timestamp deadline_time) {
//        this.deadline_time = deadline_time;
//    }
//
//    public String getWork_code() {
//        return work_code;
//    }
//
//    public String getAsset_name() {
//        return asset_name;
//    }
//
//    public void setAsset_name(String asset_name) {
//        this.asset_name = asset_name;
//    }
//
//    public void setWork_code(String work_code) {
//        this.work_code = work_code;
//    }
//
//    public String getType_name() {
//        return type_name;
//    }
//
//    public void setType_name(String type_name) {
//        this.type_name = type_name;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public Timestamp getBegin_time() {
//        return begin_time;
//    }
//
//    public void setBegin_time(Timestamp begin_time) {
//        this.begin_time = begin_time;
//    }
//
//    public Timestamp getFinished_time() {
//        return finished_time;
//    }
//
//    public void setFinished_time(Timestamp finished_time) {
//        this.finished_time = finished_time;
//    }
//
//    public Timestamp getAudit_time() {
//        return audit_time;
//    }
//
//    public void setAudit_time(Timestamp audit_time) {
//        this.audit_time = audit_time;
//    }
//
//    public String getFault_name() {
//        return fault_name;
//    }
//
//    public void setFault_name(String fault_name) {
//        this.fault_name = fault_name;
//    }
//
//    public String getRepair_type_name() {
//        return repair_type_name;
//    }
//
//    public void setRepair_type_name(String repair_type_name) {
//        this.repair_type_name = repair_type_name;
//    }
//
//    public String getParent_name() {
//        return parent_name;
//    }
//
//    public void setParent_name(String parent_name) {
//        this.parent_name = parent_name;
//    }
//
//    public String getResult_note() {
//        return result_note;
//    }
//
//    public void setResult_note(String result_note) {
//        this.result_note = result_note;
//    }
//
//    public String getCustomer_name() {
//        return customer_name;
//    }
//
//    public void setCustomer_name(String customer_name) {
//        this.customer_name = customer_name;
//    }
//
//    public WorkSheet() {
//        super();
//    }
//
//    public String getInterval() {
//        return interval;
//    }
//
//    public void setInterval(String interval) {
//        this.interval = interval;
//    }
//
//    public float getFee_1() {
//        return fee_1;
//    }
//
//    public void setFee_1(float fee_1) {
//        this.fee_1 = fee_1;
//    }
//
//    public float getFee_2() {
//        return fee_2;
//    }
//
//    public void setFee_2(float fee_2) {
//        this.fee_2 = fee_2;
//    }
//
//    public float getFee_9() {
//        return fee_9;
//    }
//
//    public void setFee_9(float fee_9) {
//        this.fee_9 = fee_9;
//    }
//
//    public String getFee_currency() {
//        return fee_currency;
//    }
//
//    public void setFee_currency(String fee_currency) {
//        this.fee_currency = fee_currency;
//    }
//
//    public String getUpdate_code() {
//        return update_code;
//    }
//
//    public void setUpdate_code(String update_code) {
//        this.update_code = update_code;
//    }
//
//    public String getFee_amount_currency() {
//        return fee_amount_currency;
//    }
//
//    public void setFee_amount_currency(String fee_amount_currency) {
//        this.fee_amount_currency = fee_amount_currency;
//    }
}
