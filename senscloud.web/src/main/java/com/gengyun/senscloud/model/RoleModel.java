package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

@ApiModel(description = "新增角色信息")
public class RoleModel {
    @ApiModelProperty(value = "角色名称")
    private String name;
    @ApiModelProperty(value = "角色描述")
    private String description;
    @ApiModelProperty(value = "是否系统角色")
    private String sys_role;
    @ApiModelProperty(value = "是否免费角色")
    private Boolean is_free;
    @ApiModelProperty(value = "是否可用")
    private Boolean available;
    @ApiModelProperty(value = "创建时间")
    private Timestamp create_time;
    @ApiModelProperty(value = "创建人")
    private String create_user_account;
    @ApiModelProperty(value = "角色id")
    private String id;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "功能权限id列表")
    private String permissionIds;
    @ApiModelProperty(value = "设备位置权限id列表")
    private String assetPositionIds;
    @ApiModelProperty(value = "设备类型权限id列表")
    private String assetCategoryIds;
    @ApiModelProperty(value = "工单类型权限id列表")
    private String workTypeRoleIds;
    @ApiModelProperty(value = "创建工单权限id列表")
    private String newWorkRoleIds;
    @ApiModelProperty(value = "库房权限id列表")
    private String stockRoleIds;

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }

    public String getStockRoleIds() {
        return stockRoleIds;
    }

    public void setStockRoleIds(String stockRoleIds) {
        this.stockRoleIds = stockRoleIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSys_role() {
        return sys_role;
    }

    public void setSys_role(String sys_role) {
        this.sys_role = sys_role;
    }

    public Boolean getIs_free() {
        return is_free;
    }

    public void setIs_free(Boolean is_free) {
        this.is_free = is_free;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(String permissionIds) {
        this.permissionIds = permissionIds;
    }

    public String getAssetPositionIds() {
        return assetPositionIds;
    }

    public void setAssetPositionIds(String assetPositionIds) {
        this.assetPositionIds = assetPositionIds;
    }

    public String getAssetCategoryIds() {
        return assetCategoryIds;
    }

    public void setAssetCategoryIds(String assetCategoryIds) {
        this.assetCategoryIds = assetCategoryIds;
    }

    public String getWorkTypeRoleIds() {
        return workTypeRoleIds;
    }

    public void setWorkTypeRoleIds(String workTypeRoleIds) {
        this.workTypeRoleIds = workTypeRoleIds;
    }

    public String getNewWorkRoleIds() {
        return newWorkRoleIds;
    }

    public void setNewWorkRoleIds(String newWorkRoleIds) {
        this.newWorkRoleIds = newWorkRoleIds;
    }
}
