package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "备件供应商信息")
public class BomSupplierInfo implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "物料编码", required = true)
    private String material_code;
    @ApiModelProperty(value = "供应商id")
    private Integer supplier_id;
    @ApiModelProperty(value = "供货周期（天）")
    private String supply_period;
    @ApiModelProperty(value = "备件主键id")
    private Integer bom_id;
    @ApiModelProperty(value = "备件供应商主键id")
    private Integer id;
    @ApiModelProperty(value = "是否启用 1启用 0禁用")
    private String is_use;

    public String getMaterial_code() {
        return material_code;
    }

    public void setMaterial_code(String material_code) {
        this.material_code = material_code;
    }

    public Integer getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(Integer supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getSupply_period() {
        return supply_period;
    }

    public void setSupply_period(String supply_period) {
        this.supply_period = supply_period;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBom_id() {
        return bom_id;
    }

    public void setBom_id(Integer bom_id) {
        this.bom_id = bom_id;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }
}
