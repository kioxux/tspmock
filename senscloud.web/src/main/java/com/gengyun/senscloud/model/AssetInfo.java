package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "设备明细信息")
public class AssetInfo extends AssetAdd implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "设备图片", required = true)
    private String asset_icon;
    @ApiModelProperty(value = "运行状态", required = true)
    private String running_status;
    @ApiModelProperty(value = "资产状态", required = true)
    private String status;
    @ApiModelProperty(value = "保固状态", required = true)
    private String guarantee_status;
    @ApiModelProperty(value = "运行状态名称")
    private String running_status_name;
    @ApiModelProperty(value = "资产状态名称")
    private String status_name;
    @ApiModelProperty(value = "保固状态名称")
    private String guarantee_status_name;
    @ApiModelProperty(value = "所在位置名称")
    private String position_name;
    @ApiModelProperty(value = "设备型号名称")
    private String model_name;
    @ApiModelProperty(value = "设备类型名称")
    private String category_name;
    @ApiModelProperty(value = "重要级别名称")
    private String importment_level_name;
    @ApiModelProperty(value = "供应商名称")
    private String supplier_name;
    @ApiModelProperty(value = "制造商名称")
    private String manufacturer_name;
    @ApiModelProperty(value = "购买价格货币单位名称")
    private String buy_currency;
    @ApiModelProperty(value = "安装价格货币单位名称")
    private String install_currency;
    @ApiModelProperty(value = "自定义字段")
    private String properties;

    public String getAsset_icon() {
        return asset_icon;
    }

    public void setAsset_icon(String asset_icon) {
        this.asset_icon = asset_icon;
    }

    public String getRunning_status() {
        return running_status;
    }

    public void setRunning_status(String running_status) {
        this.running_status = running_status;
    }

    public String getGuarantee_status() {
        return guarantee_status;
    }

    public void setGuarantee_status(String guarantee_status) {
        this.guarantee_status = guarantee_status;
    }

    public String getRunning_status_name() {
        return running_status_name;
    }

    public void setRunning_status_name(String running_status_name) {
        this.running_status_name = running_status_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGuarantee_status_name() {
        return guarantee_status_name;
    }

    public void setGuarantee_status_name(String guarantee_status_name) {
        this.guarantee_status_name = guarantee_status_name;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImportment_level_name() {
        return importment_level_name;
    }

    public void setImportment_level_name(String importment_level_name) {
        this.importment_level_name = importment_level_name;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getManufacturer_name() {
        return manufacturer_name;
    }

    public void setManufacturer_name(String manufacturer_name) {
        this.manufacturer_name = manufacturer_name;
    }

    public String getBuy_currency() {
        return buy_currency;
    }

    public void setBuy_currency(String buy_currency) {
        this.buy_currency = buy_currency;
    }

    public String getInstall_currency() {
        return install_currency;
    }

    public void setInstall_currency(String install_currency) {
        this.install_currency = install_currency;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }
}
