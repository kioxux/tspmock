package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "新增日常报告")
public class SuezReportAdd implements Serializable {

    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "报告名称", required = true)
    private String report_name;

    @ApiModelProperty(value = "工艺单元")
    private String position_code;

    @ApiModelProperty(value = "设备")
    private String searchAsset;

    @ApiModelProperty(value = "工单类型")
    private String work_type_ids;

    @ApiModelProperty(value = "开始时间", required = true)
    private String begin_time;

    @ApiModelProperty(value = "截止时间", required = true)
    private String end_time;

    public String getReport_name() {
        return report_name;
    }

    public void setReport_name(String report_name) {
        this.report_name = report_name;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getSearchAsset() {
        return searchAsset;
    }

    public void setSearchAsset(String searchAsset) {
        this.searchAsset = searchAsset;
    }

    public String getWork_type_ids() {
        return work_type_ids;
    }

    public void setWork_type_ids(String work_type_ids) {
        this.work_type_ids = work_type_ids;
    }

    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
