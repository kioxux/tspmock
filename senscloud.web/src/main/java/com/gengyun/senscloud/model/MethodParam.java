package com.gengyun.senscloud.model;

import com.gengyun.senscloud.common.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 接口公共参数（禁止使用嵌套bean）
 */
@ApiModel(description = "接口公共参数")
public class MethodParam implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "token", required = true)
    private String token;
    @ApiModelProperty(value = "每页展示行数", required = true)
    private Integer pageSize;
    @ApiModelProperty(value = "当前页码", required = true)
    private Integer pageNumber;

    @ApiModelProperty(value = "数据库", hidden = true)
    private String schemaName;
    @ApiModelProperty(value = "企业编号", hidden = true)
    private Long companyId;
    @ApiModelProperty(value = "公司名字", hidden = true)
    private String companyName;
    @ApiModelProperty(value = "是否打开短信功能", hidden = true)
    private Boolean isOpenSms;
    @ApiModelProperty(value = "是否需要抄送", hidden = true)
    private Boolean isNeedCC;
    @ApiModelProperty(value = "公司系统名称", hidden = true)
    private String companySystemName;
    @ApiModelProperty(value = "公司Logo地址", hidden = true)
    private String companyLogo;
    @ApiModelProperty(value = "登录时端口", hidden = true)
    private String loginPort;
    @ApiModelProperty(value = "登录时域名", hidden = true)
    private String loginDomain;
    @ApiModelProperty(value = "登录用户主键", hidden = true)
    private String userId;
    @ApiModelProperty(value = "登录用户账号", hidden = true)
    private String account;
    @ApiModelProperty(value = "登录用户名称", hidden = true)
    private String userName;
    @ApiModelProperty(value = "登录用户国际化语言配置", hidden = true)
    private String userLang;
    @ApiModelProperty(value = "登录用户绑定小程序open_id", hidden = true)
    private String miniProgramOpenId;
    @ApiModelProperty(value = "登录用户绑定微信公众号open_id", hidden = true)
    private String officialOpenId;
    @ApiModelProperty(value = "登录用户绑定微信union_id", hidden = true)
    private String unionId;
    @ApiModelProperty(value = "登录用户绑定微信号", hidden = true)
    private String weiXinCode;
    @ApiModelProperty(value = "登录用户首个部门岗位名称", hidden = true)
    private String userGpName;
    @ApiModelProperty(value = "登录用户部门岗位列表", hidden = true)
    private Map<String, List<Map<String, Object>>> userGpList;
    @ApiModelProperty(value = "登录用户手机号", hidden = true)
    private String userPhone;
    @ApiModelProperty(value = "客户端类型", hidden = true)
    private String clientName;
    @ApiModelProperty(value = "是否需要分页", hidden = true)
    private Boolean isNeedPagination;
    @ApiModelProperty(value = "查询明细数据类型", hidden = true)
    private String searchDtlType;
    @ApiModelProperty(value = "国际化转换类型", hidden = true)
    private String langChangeType = Constants.LANG_CHANGE;
    @ApiModelProperty(value = "系统国际化语言配置", hidden = true)
    private String systemLang;
    @ApiModelProperty(value = "登录用户菜单权限", hidden = true)
    private List<String> userPermissionList;
    @ApiModelProperty(value = "登录用户模块权限", hidden = true)
    private Map<String, Map<String, Boolean>> userModelPrmInfo;
    @ApiModelProperty(value = "当前请求地址", hidden = true)
    private String uri;
    @ApiModelProperty(value = "数据主键", hidden = true)
    private String ids;
    @ApiModelProperty(value = "数据主键【单个/首个】", hidden = true)
    private String dataId;
    @ApiModelProperty(value = "数据主键【数组格式】", hidden = true)
    private String[] dataIdArray;
    @ApiModelProperty(value = "是否批量处理", hidden = true)
    private Boolean isBatchDeal = false;
    @ApiModelProperty(value = "文件临时信息", hidden = true)
    private Map<String, Object> fileTmpInfo;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getOpenSms() {
        return isOpenSms;
    }

    public void setOpenSms(Boolean openSms) {
        isOpenSms = openSms;
    }

    public Boolean getNeedCC() {
        return isNeedCC;
    }

    public void setNeedCC(Boolean needCC) {
        isNeedCC = needCC;
    }

    public String getCompanySystemName() {
        return companySystemName;
    }

    public void setCompanySystemName(String companySystemName) {
        this.companySystemName = companySystemName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getLoginPort() {
        return loginPort;
    }

    public void setLoginPort(String loginPort) {
        this.loginPort = loginPort;
    }

    public String getLoginDomain() {
        return loginDomain;
    }

    public void setLoginDomain(String loginDomain) {
        this.loginDomain = loginDomain;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Boolean getNeedPagination() {
        return isNeedPagination;
    }

    public void setNeedPagination(Boolean needPagination) {
        isNeedPagination = needPagination;
    }

    public String getSearchDtlType() {
        return searchDtlType;
    }

    public void setSearchDtlType(String searchDtlType) {
        this.searchDtlType = searchDtlType;
    }

    public String getLangChangeType() {
        return langChangeType;
    }

    public void setLangChangeType(String langChangeType) {
        this.langChangeType = langChangeType;
    }

    public String getSystemLang() {
        return systemLang;
    }

    public void setSystemLang(String systemLang) {
        this.systemLang = systemLang;
    }

    public List<String> getUserPermissionList() {
        return userPermissionList;
    }

    public void setUserPermissionList(List<String> userPermissionList) {
        this.userPermissionList = userPermissionList;
    }

    public Map<String, Map<String, Boolean>> getUserModelPrmInfo() {
        return userModelPrmInfo;
    }

    public void setUserModelPrmInfo(Map<String, Map<String, Boolean>> userModelPrmInfo) {
        this.userModelPrmInfo = userModelPrmInfo;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String[] getDataIdArray() {
        return dataIdArray;
    }

    public void setDataIdArray(String[] dataIdArray) {
        this.dataIdArray = dataIdArray;
    }

    public Boolean getBatchDeal() {
        return isBatchDeal;
    }

    public void setBatchDeal(Boolean batchDeal) {
        isBatchDeal = batchDeal;
    }

    public Map<String, Object> getFileTmpInfo() {
        return fileTmpInfo;
    }

    public void setFileTmpInfo(Map<String, Object> fileTmpInfo) {
        this.fileTmpInfo = fileTmpInfo;
    }

    public String getMiniProgramOpenId() {
        return miniProgramOpenId;
    }

    public void setMiniProgramOpenId(String miniProgramOpenId) {
        this.miniProgramOpenId = miniProgramOpenId;
    }

    public String getOfficialOpenId() {
        return officialOpenId;
    }

    public void setOfficialOpenId(String officialOpenId) {
        this.officialOpenId = officialOpenId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getWeiXinCode() {
        return weiXinCode;
    }

    public void setWeiXinCode(String weiXinCode) {
        this.weiXinCode = weiXinCode;
    }

    public String getUserGpName() {
        return userGpName;
    }

    public void setUserGpName(String userGpName) {
        this.userGpName = userGpName;
    }

    public Map<String, List<Map<String, Object>>> getUserGpList() {
        return userGpList;
    }

    public void setUserGpList(Map<String, List<Map<String, Object>>> userGpList) {
        this.userGpList = userGpList;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
