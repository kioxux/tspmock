package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * @Author: Wudang Dong
 * @Description:邮件发送人配置实例model
 * @Date: Create in 下午 2:55 2019/5/23 0023
 */
public class MailSenderConfigModel implements Serializable {
//    private static final long serialVersionUID = 4248677516001666145L;
//    private int id;
//    private String obj_name;
//    private String obj_type;
//    private String device_id;
//    private String receiver_account;
//    private String cc_account;
//    private boolean status;//offline
//    private boolean error_status;//error
//
//    /*衍生字段名扩充*/
//    private String device_name;//对象名称
//    private String receiver_name;//邮件接收人
//    private String cc_name;//邮件抄送人
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getObj_name() {
//        return obj_name;
//    }
//
//    public void setObj_name(String obj_name) {
//        this.obj_name = obj_name;
//    }
//
//    public String getObj_type() {
//        return obj_type;
//    }
//
//    public void setObj_type(String obj_type) {
//        this.obj_type = obj_type;
//    }
//
//    public String getDevice_id() {
//        return device_id;
//    }
//
//    public void setDevice_id(String device_id) {
//        this.device_id = device_id;
//    }
//
//    public String getReceiver_account() {
//        return receiver_account;
//    }
//
//    public void setReceiver_account(String receiver_account) {
//        this.receiver_account = receiver_account;
//    }
//
//    public String getCc_account() {
//        return cc_account;
//    }
//
//    public void setCc_account(String cc_account) {
//        this.cc_account = cc_account;
//    }
//
//    public boolean isStatus() {
//        return status;
//    }
//
//    public void setStatus(boolean status) {
//        this.status = status;
//    }
//
//
//    public String getDevice_name() {
//        return device_name;
//    }
//
//    public void setDevice_name(String device_name) {
//        this.device_name = device_name;
//    }
//
//    public String getReceiver_name() {
//        return receiver_name;
//    }
//
//    public void setReceiver_name(String receiver_name) {
//        this.receiver_name = receiver_name;
//    }
//
//    public String getCc_name() {
//        return cc_name;
//    }
//
//    public void setCc_name(String cc_name) {
//        this.cc_name = cc_name;
//    }
//
//    public boolean isError_status() {
//        return error_status;
//    }
//
//    public void setError_status(boolean error_status) {
//        this.error_status = error_status;
//    }
}
