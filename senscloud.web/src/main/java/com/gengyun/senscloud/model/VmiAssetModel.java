package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * VMI设备
 */
public class VmiAssetModel implements Serializable {
//
//    private static final long serialVersionUID = 4706741647757999279L;
//    private long facilitiesId;//客户ID
//    private String short_title; //客户名称
//    private String facilitycode;//客户编码
//    private String assetId;//设备ID
//    private String strname;//设备名称
//    private String strcode;//设备编码
//    private String model_name;//设备型号
//    private String category_name;//设备类型
//    private String category_id;//设备类型
//
//    public String getCategory_id() {
//        return category_id;
//    }
//
//    public void setCategory_id(String category_id) {
//        this.category_id = category_id;
//    }
//
//    private String capacity;//储罐容量
//    private String tank_volume;//当前储罐总存量
//    private String total_receive_actual;//入库总量
//    private String colorlist;//设备颜色配置表
//
//    public String getShort_title() {
//        return short_title;
//    }
//
//    public void setShort_title(String short_title) {
//        this.short_title = short_title;
//    }
//
//    public String getFacilitycode() {
//        return facilitycode;
//    }
//
//    public void setFacilitycode(String facilitycode) {
//        this.facilitycode = facilitycode;
//    }
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getModel_name() {
//        return model_name;
//    }
//
//    public void setModel_name(String model_name) {
//        this.model_name = model_name;
//    }
//
//    public long getFacilitiesId() {
//        return facilitiesId;
//    }
//
//    public void setFacilitiesId(long facilitiesId) {
//        this.facilitiesId = facilitiesId;
//    }
//
//    public String getAssetId() {
//        return assetId;
//    }
//
//    public void setAssetId(String assetId) {
//        this.assetId = assetId;
//    }
//
//    public String getCategory_name() {
//        return category_name;
//    }
//
//    public void setCategory_name(String category_name) {
//        this.category_name = category_name;
//    }
//
//    public String getCapacity() {
//        return capacity;
//    }
//
//    public void setCapacity(String capacity) {
//        this.capacity = capacity;
//    }
//
//    public String getTank_volume() {
//        return tank_volume;
//    }
//
//    public void setTank_volume(String tank_volume) {
//        this.tank_volume = tank_volume;
//    }
//
//    public String getTotal_receive_actual() {
//        return total_receive_actual;
//    }
//
//    public void setTotal_receive_actual(String total_receive_actual) {
//        this.total_receive_actual = total_receive_actual;
//    }
//
//    public String getColorlist() {
//        return colorlist;
//    }
//
//    public void setColorlist(String colorlist) {
//        this.colorlist = colorlist;
//    }
}
