package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "字典接口公共参数")
public class SelectModel implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "字典类型", required = true)
    private String selectKey;
    @ApiModelProperty(value = "查询条件", required = true)
    private String selectExtKey;
    @ApiModelProperty(value = "选中项值")
    private String selectValue;
    @ApiModelProperty(value = "选中项属性")
    private String querySelectName;

    public String getSelectKey() {
        return selectKey;
    }

    public void setSelectKey(String selectKey) {
        this.selectKey = selectKey;
    }

    public String getSelectValue() {
        return selectValue;
    }

    public void setSelectValue(String selectValue) {
        this.selectValue = selectValue;
    }

    public String getQuerySelectName() {
        return querySelectName;
    }

    public void setQuerySelectName(String querySelectName) {
        this.querySelectName = querySelectName;
    }

    public String getSelectExtKey() {
        return selectExtKey;
    }

    public void setSelectExtKey(String selectExtKey) {
        this.selectExtKey = selectExtKey;
    }
}
