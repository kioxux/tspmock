package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class StockData implements Serializable {

    private static final long serialVersionUID = -1607189798969731879L;
    private int id;

    private String stock_code;

    private String stock_name;

    private String facility_ids;

    private String group_ids;

    private String org_names;

    private String group_names;

    private String manager_account;
    //管理人姓名
    private String manager_account_name;

    private String address;

    private int status;

    private Timestamp createtime;

    private String create_user_account;

    private int currency_id;

    private String currency_code;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }

    public String getStock_code() {
        return stock_code;
    }

    public void setStock_code(String stock_code) {
        this.stock_code = stock_code;
    }

    public String getStock_name() {
        return stock_name;
    }

    public void setStock_name(String stock_name) {
        this.stock_name = stock_name;
    }

    public String getFacility_ids() {
        return facility_ids;
    }

    public void setFacility_ids(String facility_ids) {
        this.facility_ids = facility_ids;
    }

    public String getGroup_ids() {
        return group_ids;
    }

    public void setGroup_ids(String group_ids) {
        this.group_ids = group_ids;
    }

    public String getOrg_names() {
        return org_names;
    }

    public void setOrg_names(String org_names) {
        this.org_names = org_names;
    }

    public String getGroup_names() {
        return group_names;
    }

    public void setGroup_names(String group_names) {
        this.group_names = group_names;
    }

    public String getManager_account() {
        return manager_account;
    }

    public void setManager_account(String manager_account) {
        this.manager_account = manager_account;
    }

    public String getManager_account_name() {
        return manager_account_name;
    }

    public void setManager_account_name(String manager_account_name) {
        this.manager_account_name = manager_account_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

//    //使用位置
//    private List<Facility> facilitiesList;
//
//    public List<Facility> getFacilitiesList() {
//        return facilitiesList;
//    }

//    public void setFacilitiesList(List<Facility> facilitiesList) {
//        this.facilitiesList = facilitiesList;
//    }

    //所属组织
    private List<UserGroupData> userGroupDataList;

    public List<UserGroupData> getUserGroupDataList() {
        return userGroupDataList;
    }

    public void setUserGroupDataList(List<UserGroupData> userGroupDataList) {
        this.userGroupDataList = userGroupDataList;
    }

    public int getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(int currency_id) {
        this.currency_id = currency_id;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }
}
