package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "新增设备信息")
public class AssetAdd implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "设备编号", required = true)
    private String asset_code;
    @ApiModelProperty(value = "设备名称", required = true)
    private String asset_name;
    @ApiModelProperty(value = "所在位置", required = true)
    private String position_code;
    @ApiModelProperty(value = "设备型号", required = true)
    private String asset_model_id;
    @ApiModelProperty(value = "设备类型", required = true)
    private String category_id;
    @ApiModelProperty(value = "重要级别")
    private String importment_level_id;
    @ApiModelProperty(value = "启用日期")
    private String enable_time;
    @ApiModelProperty(value = "设备寿命")
    private String use_year;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "购买日期")
    private String buy_date;
    @ApiModelProperty(value = "供应商")
    private String supplier_id;
    @ApiModelProperty(value = "制造商")
    private String manufacturer_id;
    @ApiModelProperty(value = "税率")
    private String tax_rate;
    @ApiModelProperty(value = "税前价格")
    private String price;
    @ApiModelProperty(value = "税费")
    private String tax_price;
    @ApiModelProperty(value = "安装日期")
    private String install_date;
    @ApiModelProperty(value = "安装价格")
    private String install_price;
    @ApiModelProperty(value = "购买价格货币单位")
    private String buy_currency_id;
    @ApiModelProperty(value = "安装价格货币单位")
    private String install_currency_id;

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getAsset_model_id() {
        return asset_model_id;
    }

    public void setAsset_model_id(String asset_model_id) {
        this.asset_model_id = asset_model_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getImportment_level_id() {
        return importment_level_id;
    }

    public void setImportment_level_id(String importment_level_id) {
        this.importment_level_id = importment_level_id;
    }

    public String getEnable_time() {
        return enable_time;
    }

    public void setEnable_time(String enable_time) {
        this.enable_time = enable_time;
    }

    public String getUse_year() {
        return use_year;
    }

    public void setUse_year(String use_year) {
        this.use_year = use_year;
    }

    public String getBuy_date() {
        return buy_date;
    }

    public void setBuy_date(String buy_date) {
        this.buy_date = buy_date;
    }

    public String getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(String tax_rate) {
        this.tax_rate = tax_rate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax_price() {
        return tax_price;
    }

    public void setTax_price(String tax_price) {
        this.tax_price = tax_price;
    }

    public String getInstall_date() {
        return install_date;
    }

    public void setInstall_date(String install_date) {
        this.install_date = install_date;
    }

    public String getInstall_price() {
        return install_price;
    }

    public void setInstall_price(String install_price) {
        this.install_price = install_price;
    }

    public String getBuy_currency_id() {
        return buy_currency_id;
    }

    public void setBuy_currency_id(String buy_currency_id) {
        this.buy_currency_id = buy_currency_id;
    }

    public String getInstall_currency_id() {
        return install_currency_id;
    }

    public void setInstall_currency_id(String install_currency_id) {
        this.install_currency_id = install_currency_id;
    }

    public String getAsset_code() {
        return asset_code;
    }

    public void setAsset_code(String asset_code) {
        this.asset_code = asset_code;
    }

    public String getAsset_name() {
        return asset_name;
    }

    public void setAsset_name(String asset_name) {
        this.asset_name = asset_name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getManufacturer_id() {
        return manufacturer_id;
    }

    public void setManufacturer_id(String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }
}
