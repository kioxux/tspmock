package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "新增备件信息")
public class BomInfo implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "1：一类一码；2：一物一码", required = true)
    private String code_classification;
    @ApiModelProperty(value = "备件名称", required = true)
    private String bom_name;
    @ApiModelProperty(value = "物料编码", required = true)
    private String material_code;
    @ApiModelProperty(value = "型号规格")
    private String bom_model;
    @ApiModelProperty(value = "备件类型id", required = true)
    private Integer type_id;
    @ApiModelProperty(value = "单位id")
    private Integer unit_id;
    @ApiModelProperty(value = "备件单价")
    private BigDecimal show_price;
    @ApiModelProperty(value = "供应商id")
    private Integer supplier_id;
    @ApiModelProperty(value = "供货周期（天）")
    private String supply_period;
    @ApiModelProperty(value = "制造商id")
    private Integer manufacturer_id;
    @ApiModelProperty(value = "使用寿命（天）")
    private Integer service_life;
    @ApiModelProperty(value = "货币单位")
    private Integer currency_id;
    @ApiModelProperty(value = "备件主键id")
    private String id;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "图标id")
    private String icon;
    @ApiModelProperty(value = "是否启用 1启用 0禁用")
    private String is_use;

    public String getCode_classification() {
        return code_classification;
    }

    public void setCode_classification(String code_classification) {
        this.code_classification = code_classification;
    }

    public String getBom_name() {
        return bom_name;
    }

    public void setBom_name(String bom_name) {
        this.bom_name = bom_name;
    }

    public String getMaterial_code() {
        return material_code;
    }

    public void setMaterial_code(String material_code) {
        this.material_code = material_code;
    }

    public String getBom_model() {
        return bom_model;
    }

    public void setBom_model(String bom_model) {
        this.bom_model = bom_model;
    }

    public Integer getType_id() {
        return type_id;
    }

    public void setType_id(Integer type_id) {
        this.type_id = type_id;
    }

    public Integer getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(Integer unit_id) {
        this.unit_id = unit_id;
    }

    public BigDecimal getShow_price() {
        return show_price;
    }

    public void setShow_price(BigDecimal show_price) {
        this.show_price = show_price;
    }

    public Integer getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(Integer supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getSupply_period() {
        return supply_period;
    }

    public void setSupply_period(String supply_period) {
        this.supply_period = supply_period;
    }

    public Integer getManufacturer_id() {
        return manufacturer_id;
    }

    public void setManufacturer_id(Integer manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }

    public Integer getService_life() {
        return service_life;
    }

    public void setService_life(Integer service_life) {
        this.service_life = service_life;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(Integer currency_id) {
        this.currency_id = currency_id;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
