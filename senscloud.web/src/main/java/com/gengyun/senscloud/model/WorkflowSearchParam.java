package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(description = "工作流查询参数")
public class WorkflowSearchParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "关键字", hidden = true)
    private String keywordSearch;
    @ApiModelProperty(value = "是否显示最新", hidden = true)
    private Boolean showLatest;
    @ApiModelProperty(value = "流程编号", hidden = true)
    private String flow_code;
    @ApiModelProperty(value = "选中的工单编号", hidden = true)
    private List<String> subWorkCodesSearch;
    @ApiModelProperty(value = "只显示我的待办", hidden = true)
    private Boolean mine_only;
    @ApiModelProperty(value = "只查询待办数量", hidden = true)
    private Boolean count_only;
    @ApiModelProperty(value = "开始时间", hidden = true)
    private String start_time;
    @ApiModelProperty(value = "结束时间", hidden = true)
    private String end_time;
    @ApiModelProperty(value = "节点开始时间", hidden = true)
    private String process_start_time;
    @ApiModelProperty(value = "节点结束时间", hidden = true)
    private String process_end_time;
    @ApiModelProperty(value = "选中工单类型", hidden = true)
    private List<String> work_type_id;

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public Boolean getShowLatest() {
        return showLatest;
    }

    public void setShowLatest(Boolean showLatest) {
        this.showLatest = showLatest;
    }

    public String getFlow_code() {
        return flow_code;
    }

    public void setFlow_code(String flow_code) {
        this.flow_code = flow_code;
    }

    public List<String> getSubWorkCodesSearch() {
        return subWorkCodesSearch;
    }

    public void setSubWorkCodesSearch(List<String> subWorkCodesSearch) {
        this.subWorkCodesSearch = subWorkCodesSearch;
    }

    public Boolean getMine_only() {
        return mine_only;
    }

    public void setMine_only(Boolean mine_only) {
        this.mine_only = mine_only;
    }

    public Boolean getCount_only() {
        return count_only;
    }

    public void setCount_only(Boolean count_only) {
        this.count_only = count_only;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public List<String> getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(List<String> work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getProcess_start_time() {
        return process_start_time;
    }

    public void setProcess_start_time(String process_start_time) {
        this.process_start_time = process_start_time;
    }

    public String getProcess_end_time() {
        return process_end_time;
    }

    public void setProcess_end_time(String process_end_time) {
        this.process_end_time = process_end_time;
    }
}
