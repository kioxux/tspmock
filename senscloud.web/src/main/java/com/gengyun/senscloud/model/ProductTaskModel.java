package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 生产作业单
 */
public class ProductTaskModel {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "模糊搜索关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "生产作业编码")
    private String product_task_code;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "生产作业下井编码")
    private String task_well_code;
    @ApiModelProperty(value = "任务单号")
    private String task_code;
    @ApiModelProperty(value = "井号")
    private String well_no;
    @ApiModelProperty(value = "最高井温（℃）")
    private Float max_temperature;
    @ApiModelProperty(value = "井底压力（Mpc）")
    private Float bottom_pressure;
    @ApiModelProperty(value = "下井次数")
    private Integer go_down_count;
    @ApiModelProperty(value = "出库单号，多个逗号隔开")
    private String asset_out_code;
    @ApiModelProperty(value = "生产作业下井次数编码")
    private String task_well_item_code;
    @ApiModelProperty(value = "下井时间")
    private String go_down_time;
    @ApiModelProperty(value = "出井时间")
    private String come_out_time;
    @ApiModelProperty(value = "设备id")
    private String asset_id;
    @ApiModelProperty(value = "设备id")
    private String product_task_well_item_asset_list;
    @ApiModelProperty(value = "是否含硫化氢")
    private String is_contain_hydrogen_sulfide;
    @ApiModelProperty(value = "电缆接头位置")
    private String cable_joint_location;
    @ApiModelProperty(value = "车辆行驶总公里数")
    private String total_vehicle_kilometers;
    @ApiModelProperty(value = "遇卡总张力")
    private String total_tension_jamming;
    @ApiModelProperty(value = "遇卡净张力")
    private String net_tension_jamming;
    @ApiModelProperty(value = "遇卡深度")
    private String depth_of_encounter;

    public String getIs_contain_hydrogen_sulfide() {
        return is_contain_hydrogen_sulfide;
    }

    public void setIs_contain_hydrogen_sulfide(String is_contain_hydrogen_sulfide) {
        this.is_contain_hydrogen_sulfide = is_contain_hydrogen_sulfide;
    }

    public String getCable_joint_location() {
        return cable_joint_location;
    }

    public void setCable_joint_location(String cable_joint_location) {
        this.cable_joint_location = cable_joint_location;
    }

    public String getTotal_vehicle_kilometers() {
        return total_vehicle_kilometers;
    }

    public void setTotal_vehicle_kilometers(String total_vehicle_kilometers) {
        this.total_vehicle_kilometers = total_vehicle_kilometers;
    }

    public String getTotal_tension_jamming() {
        return total_tension_jamming;
    }

    public void setTotal_tension_jamming(String total_tension_jamming) {
        this.total_tension_jamming = total_tension_jamming;
    }

    public String getNet_tension_jamming() {
        return net_tension_jamming;
    }

    public void setNet_tension_jamming(String net_tension_jamming) {
        this.net_tension_jamming = net_tension_jamming;
    }

    public String getDepth_of_encounter() {
        return depth_of_encounter;
    }

    public void setDepth_of_encounter(String depth_of_encounter) {
        this.depth_of_encounter = depth_of_encounter;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getProduct_task_code() {
        return product_task_code;
    }

    public void setProduct_task_code(String product_task_code) {
        this.product_task_code = product_task_code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTask_well_code() {
        return task_well_code;
    }

    public void setTask_well_code(String task_well_code) {
        this.task_well_code = task_well_code;
    }

    public String getTask_code() {
        return task_code;
    }

    public void setTask_code(String task_code) {
        this.task_code = task_code;
    }

    public String getWell_no() {
        return well_no;
    }

    public void setWell_no(String well_no) {
        this.well_no = well_no;
    }

    public Float getMax_temperature() {
        return max_temperature;
    }

    public void setMax_temperature(Float max_temperature) {
        this.max_temperature = max_temperature;
    }

    public Float getBottom_pressure() {
        return bottom_pressure;
    }

    public void setBottom_pressure(Float bottom_pressure) {
        this.bottom_pressure = bottom_pressure;
    }

    public Integer getGo_down_count() {
        return go_down_count;
    }

    public void setGo_down_count(Integer go_down_count) {
        this.go_down_count = go_down_count;
    }

    public String getAsset_out_code() {
        return asset_out_code;
    }

    public void setAsset_out_code(String asset_out_code) {
        this.asset_out_code = asset_out_code;
    }

    public String getTask_well_item_code() {
        return task_well_item_code;
    }

    public void setTask_well_item_code(String task_well_item_code) {
        this.task_well_item_code = task_well_item_code;
    }

    public String getGo_down_time() {
        return go_down_time;
    }

    public void setGo_down_time(String go_down_time) {
        this.go_down_time = go_down_time;
    }

    public String getCome_out_time() {
        return come_out_time;
    }

    public void setCome_out_time(String come_out_time) {
        this.come_out_time = come_out_time;
    }

    public String getAsset_id() {
        return asset_id;
    }

    public void setAsset_id(String asset_id) {
        this.asset_id = asset_id;
    }

    public String getProduct_task_well_item_asset_list() {
        return product_task_well_item_asset_list;
    }

    public void setProduct_task_well_item_asset_list(String product_task_well_item_asset_list) {
        this.product_task_well_item_asset_list = product_task_well_item_asset_list;
    }
}
