package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 任务项入参
 */
public class TaskItemModel {
    @ApiModelProperty(value = "任务编号")
    private String task_item_code;
    @ApiModelProperty(value = "任务名称")
    private String task_item_name;
    @ApiModelProperty(value = "任务描述")
    private String requirements;
    @ApiModelProperty(value = "结果类型")
    private Integer result_type;
    @ApiModelProperty(value = "结果校验")
    private String result_verification;
    @ApiModelProperty(value = "附件ids")
    private String file_ids;
    @ApiModelProperty(value = "关键字搜索")
    private String keywordSearch;
    @ApiModelProperty(value = "任务模板编号")
    private String task_template_code;
    @ApiModelProperty(value = "附件id")
    private String file_id;
    @ApiModelProperty(value = "任务编号集合")
    private String task_item_codes;

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public String getTask_template_code() {
        return task_template_code;
    }

    public void setTask_template_code(String task_template_code) {
        this.task_template_code = task_template_code;
    }

    public String getResult_verification() {
        return result_verification;
    }

    public void setResult_verification(String result_verification) {
        this.result_verification = result_verification;
    }

    public String getTask_item_code() {
        return task_item_code;
    }

    public void setTask_item_code(String task_item_code) {
        this.task_item_code = task_item_code;
    }

    public String getTask_item_name() {
        return task_item_name;
    }

    public void setTask_item_name(String task_item_name) {
        this.task_item_name = task_item_name;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Integer getResult_type() {
        return result_type;
    }

    public void setResult_type(Integer result_type) {
        this.result_type = result_type;
    }

    public String getFile_ids() {
        return file_ids;
    }

    public void setFile_ids(String file_ids) {
        this.file_ids = file_ids;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getTask_item_codes() {
        return task_item_codes;
    }

    public void setTask_item_codes(String task_item_codes) {
        this.task_item_codes = task_item_codes;
    }
}
