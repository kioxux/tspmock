package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "新增角色信息")
public class Role {
    @ApiModelProperty(value = "角色名称", required = true)
    private String name;
    @ApiModelProperty(value = "角色描述")
    private String description;
    @ApiModelProperty(value = "是否系统角色", required = true)
    private String sys_role;
    @ApiModelProperty(value = "是否免费角色", required = true)
    private String is_free;
    @ApiModelProperty(value = "是否可用", required = true)
    private String available;
    @ApiModelProperty(value = "创建时间", required = true)
    private String createtime;
    @ApiModelProperty(value = "创建人", required = true)
    private String creater_account;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSys_role() {
        return sys_role;
    }

    public void setSys_role(String sys_role) {
        this.sys_role = sys_role;
    }

    public String getIs_free() {
        return is_free;
    }

    public void setIs_free(String is_free) {
        this.is_free = is_free;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getCreater_account() {
        return creater_account;
    }

    public void setCreater_account(String creater_account) {
        this.creater_account = creater_account;
    }
}
