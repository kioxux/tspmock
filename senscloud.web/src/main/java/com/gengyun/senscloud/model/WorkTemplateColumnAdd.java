package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "工单模板字段增加信息")
public class WorkTemplateColumnAdd implements Serializable {
    private static final long serialVersionUID = -1L;


//    @ApiModelProperty(value = "模板主键")
//    private String node_id;
    @ApiModelProperty(value = "模板主键", required = true)
    private String work_template_code;
    @ApiModelProperty(value = "选中字段主键字符串", required = true)
    private String field_form_codes;
    @ApiModelProperty(value = "字段库的字段主键", required = true)
    private String field_form_code;
    @ApiModelProperty(value = "字段编码", required = true)
    private String field_code;
    @ApiModelProperty(value = "字段显示名称/key", required = true)
    private String field_name;
    @ApiModelProperty(value = "存储方式")
    private String save_type;
    @ApiModelProperty(value = "写入只读权限 readonly 只读 ； edit 写入", required = true)
    private String field_right;
    @ApiModelProperty(value = "是否必填 1是 2否")
    private String is_required;
    @ApiModelProperty(value = "备注")
    private String field_remark;
    @ApiModelProperty(value = "控件类型", required = true)
    private String field_view_type;
    @ApiModelProperty(value = "所属模块", required = true)
    private String field_section_type;
    @ApiModelProperty(value = "特殊属性列表,格式详见note说明")
    private String extList;
    @ApiModelProperty(value = "字段联动列表,格式详见note说明")
    private String linkList;

    @ApiModelProperty(value = "是否显示： show 显示， hide 隐藏")
    private String field_is_show;
    @ApiModelProperty(value = "数据库主键")
    private Integer id;
    @ApiModelProperty(value = "类型 1、所属区块 2、是否显示 3、只读、写入权限 4、上移 5、下移")
    private String type;
    @ApiModelProperty(value = "数据源")
    private String data_key;
    @ApiModelProperty(value = "默认值")
    private String field_value;


    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public String getField_form_codes() {
        return field_form_codes;
    }

    public void setField_form_codes(String field_form_codes) {
        this.field_form_codes = field_form_codes;
    }

    public String getField_code() {
        return field_code;
    }

    public void setField_code(String field_code) {
        this.field_code = field_code;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getSave_type() {
        return save_type;
    }

    public void setSave_type(String save_type) {
        this.save_type = save_type;
    }

    public String getField_right() {
        return field_right;
    }

    public void setField_right(String field_right) {
        this.field_right = field_right;
    }

    public String getIs_required() {
        return is_required;
    }

    public void setIs_required(String is_required) {
        this.is_required = is_required;
    }

    public String getField_remark() {
        return field_remark;
    }

    public void setField_remark(String field_remark) {
        this.field_remark = field_remark;
    }

    public String getField_view_type() {
        return field_view_type;
    }

    public void setField_view_type(String field_view_type) {
        this.field_view_type = field_view_type;
    }

    public String getField_section_type() {
        return field_section_type;
    }

    public void setField_section_type(String field_section_type) {
        this.field_section_type = field_section_type;
    }

    public String getExtList() {
        return extList;
    }

    public void setExtList(String extList) {
        this.extList = extList;
    }

    public String getLinkList() {
        return linkList;
    }

    public void setLinkList(String linkList) {
        this.linkList = linkList;
    }

    public String getField_is_show() {
        return field_is_show;
    }

    public void setField_is_show(String field_is_show) {
        this.field_is_show = field_is_show;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getField_form_code() {
        return field_form_code;
    }

    public void setField_form_code(String field_form_code) {
        this.field_form_code = field_form_code;
    }

    public String getData_key() {
        return data_key;
    }

    public void setData_key(String data_key) {
        this.data_key = data_key;
    }

    public String getField_value() {
        return field_value;
    }

    public void setField_value(String field_value) {
        this.field_value = field_value;
    }
}
