package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;


/**
 * 厂商管理
 */
public class FacilitiesModel {
    @ApiModelProperty(value = "编辑类型 facilitiesBase：更新基础信息，facilitiesTop：更新厂商头部名称和编码")
    private String sectionType;
    @ApiModelProperty(value = "id")
    private Integer id;
    @ApiModelProperty(value = "ids")
    private String ids;
    @ApiModelProperty(value = "厂商名称")
    private String title;
    @ApiModelProperty(value = "上级厂商id")
    private String parent_id;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "坐标")
    private String location;
    @ApiModelProperty(value = "内部编码")
    private String inner_code;
    @ApiModelProperty(value = "地址")
    private String address;
    @ApiModelProperty(value = "组织图层")
    private String layer_path;
    @ApiModelProperty(value = "是否启用")
    private String is_use;
    @ApiModelProperty(value = "组织类型")
    private String org_type_id;
    @ApiModelProperty(value = "简称")
    private String short_title;
    @ApiModelProperty(value = "货币单位")
    private Integer currency_id;
    @ApiModelProperty(value = "附件文档id")
    private Integer file_id;
    @ApiModelProperty(value = "厂商id")
    private Integer org_id;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "是否启用高级搜索")
    private String isUseSearch;
    @ApiModelProperty(value = "厂商类型高级搜索")
    private String orgTypeSearch;
    @ApiModelProperty(value = "联系人名称")
    private String contact_name;
    @ApiModelProperty(value = "联系人手机号")
    private String contact_mobile;
    @ApiModelProperty(value = "联系人邮箱")
    private String contact_email;
    @ApiModelProperty(value = "联系人微信")
    private String wei_xin;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getInner_code() {
        return inner_code;
    }

    public void setInner_code(String inner_code) {
        this.inner_code = inner_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLayer_path() {
        return layer_path;
    }

    public void setLayer_path(String layer_path) {
        this.layer_path = layer_path;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }

    public String getOrg_type_id() {
        return org_type_id;
    }

    public void setOrg_type_id(String org_type_id) {
        this.org_type_id = org_type_id;
    }

    public String getShort_title() {
        return short_title;
    }

    public void setShort_title(String short_title) {
        this.short_title = short_title;
    }

    public Integer getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(Integer currency_id) {
        this.currency_id = currency_id;
    }

    public Integer getFile_id() {
        return file_id;
    }

    public void setFile_id(Integer file_id) {
        this.file_id = file_id;
    }

    public Integer getOrg_id() {
        return org_id;
    }

    public void setOrg_id(Integer org_id) {
        this.org_id = org_id;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getIsUseSearch() {
        return isUseSearch;
    }

    public void setIsUseSearch(String isUseSearch) {
        this.isUseSearch = isUseSearch;
    }

    public String getOrgTypeSearch() {
        return orgTypeSearch;
    }

    public void setOrgTypeSearch(String orgTypeSearch) {
        this.orgTypeSearch = orgTypeSearch;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_mobile() {
        return contact_mobile;
    }

    public void setContact_mobile(String contact_mobile) {
        this.contact_mobile = contact_mobile;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getWei_xin() {
        return wei_xin;
    }

    public void setWei_xin(String wei_xin) {
        this.wei_xin = wei_xin;
    }
}
