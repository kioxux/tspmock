package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class Statistic implements Serializable {
    private static final long serialVersionUID = -1L;
    private Integer id;
    private String title;
    private String description;
    private Map<String, Object> options;
    private List<Map<String, Object>> query_conditions;
    private String query;
    private String table_query;
    private List<Map<String, Object>> table_columns;
    private String create_user_id;
    private Timestamp create_time;
    private int is_show_dashboard;
    private String client_page; //客户定制页面
    private String group_name; //分组
    private String group_method_name; //分组

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Object> getOptions() {
        return options;
    }

    public void setOptions(Map<String, Object> options) {
        this.options = options;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public List<Map<String, Object>> getQuery_conditions() {
        return query_conditions;
    }

    public void setQuery_conditions(List<Map<String, Object>> query_conditions) {
        this.query_conditions = query_conditions;
    }

    public String getTable_query() {
        return table_query;
    }

    public void setTable_query(String table_query) {
        this.table_query = table_query;
    }

    public List<Map<String, Object>> getTable_columns() {
        return table_columns;
    }

    public void setTable_columns(List<Map<String, Object>> table_columns) {
        this.table_columns = table_columns;
    }

    public int getIs_show_dashboard() {
        return is_show_dashboard;
    }

    public void setIs_show_dashboard(int is_show_dashboard) {
        this.is_show_dashboard = is_show_dashboard;
    }

    public String getClient_page() {
        return client_page;
    }

    public void setClient_page(String client_page) {
        this.client_page = client_page;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_method_name() {
        return group_method_name;
    }

    public void setGroup_method_name(String group_method_name) {
        this.group_method_name = group_method_name;
    }
}
