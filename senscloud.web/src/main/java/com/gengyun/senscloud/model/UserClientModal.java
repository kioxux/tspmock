package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * @Author: Wudang Dong
 * @Description: _sc_client_user 客户用户表实体
 * @Date: Create in 下午 1:56 2019/7/18 0018
 */
public class UserClientModal implements Serializable {
//
//    private static final long serialVersionUID = -8280127431539010958L;
//    private int id;
//    private String app_code;
//    private String user_name;
//    private String user_account;
//    private String user_mobile;
//    private int client_id;
//    private String remark;
//    private Timestamp create_time;
//    private String create_user_account;
//    private Timestamp audit_time;
//    private String audit_person;
//    private String audit_content;
//    private String client_name;
//    private int status;
//    private String status_name;
//
//    public String getApp_code() {
//        return app_code;
//    }
//
//    public void setApp_code(String app_code) {
//        this.app_code = app_code;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getUser_name() {
//        return user_name;
//    }
//
//    public void setUser_name(String user_name) {
//        this.user_name = user_name;
//    }
//
//    public String getUser_account() {
//        return user_account;
//    }
//
//    public void setUser_account(String user_account) {
//        this.user_account = user_account;
//    }
//
//    public String getUser_mobile() {
//        return user_mobile;
//    }
//
//    public void setUser_mobile(String user_mobile) {
//        this.user_mobile = user_mobile;
//    }
//
//    public int getClient_id() {
//        return client_id;
//    }
//
//    public void setClient_id(int client_id) {
//        this.client_id = client_id;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public Timestamp getAudit_time() {
//        return audit_time;
//    }
//
//    public void setAudit_time(Timestamp audit_time) {
//        this.audit_time = audit_time;
//    }
//
//    public String getAudit_person() {
//        return audit_person;
//    }
//
//    public void setAudit_person(String audit_person) {
//        this.audit_person = audit_person;
//    }
//
//    public String getAudit_content() {
//        return audit_content;
//    }
//
//    public void setAudit_content(String audit_content) {
//        this.audit_content = audit_content;
//    }
//
//    public String getClient_name() {
//        return client_name;
//    }
//
//    public void setClient_name(String client_name) {
//        this.client_name = client_name;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public String getStatus_name() {
//        return status_name;
//    }
//
//    public void setStatus_name(String status_name) {
//        this.status_name = status_name;
//    }
}
