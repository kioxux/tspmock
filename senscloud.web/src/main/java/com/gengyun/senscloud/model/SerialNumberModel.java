package com.gengyun.senscloud.model;

import java.io.Serializable;

public class SerialNumberModel implements Serializable {

    private static final long serialVersionUID = -3155333499900016169L;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String serial_pre;

    public String getSerialPre() {
        return serial_pre;
    }

    public void setSerialPre(String serial_pre) {
        this.serial_pre = serial_pre;
    }

    private String currentdate;

    public String getCurrentDate() {
        return currentdate;
    }

    public void setCurrentDate(String currentdate) {
        this.currentdate = currentdate;
    }

    private int reset_type;

    public int getResetType() {
        return reset_type;
    }

    public void setResetType(int reset_type) {
        this.reset_type = reset_type;
    }

    private boolean is_need_pre;

    public boolean getIsNeedPre() {
        return is_need_pre;
    }

    public void setIsNeedPre(boolean is_need_pre) {
        this.is_need_pre = is_need_pre;
    }

    private int max_number;

    public int getMaxNumber() {
        return max_number;
    }

    public void setMaxNumber(int max_number) {
        this.max_number = max_number;
    }
}
