package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 功能：工单派工配置实例
 * Created by Dong wudang  on 2018/11/20.
 */
public class WorkSheetPoolConfigModel implements Serializable {
//
//    private static final long serialVersionUID = -3741580912658591420L;
//    private  String id;//主键
//    private  String  pool_name;//工单池名称
//    private  String  role_id;//负责人
//    private  int distribute_type;//1：派单 2：抢单
//    private  int distribute_sms_minutes;//分配短信提醒分钟
//    private  int receive_sms_minute;//接受短信提醒分钟
//    private Timestamp create_time;//创建时间
//    private String create_user_account;//创建人
//    private boolean isuse;//是否启用
//
//    /*******************一下为扩展字段******************************/
//    private String role_name;//负责角色名称
//    private String facility_name;//创建人名称
//    private List<WorkSheetPoolConfigFacilityModel> facilityModels;
//    private List<WorkSheetPoolConfigRoleModel> roleModels;
//
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getPool_name() {
//        return pool_name;
//    }
//
//    public void setPool_name(String pool_name) {
//        this.pool_name = pool_name;
//    }
//
//    public String getRole_id() {
//        return role_id;
//    }
//
//    public void setRole_id(String role_id) {
//        this.role_id = role_id;
//    }
//
//    public int getDistribute_type() {
//        return distribute_type;
//    }
//
//    public void setDistribute_type(int distribute_type) {
//        this.distribute_type = distribute_type;
//    }
//
//    public int getDistribute_sms_minutes() {
//        return distribute_sms_minutes;
//    }
//
//    public void setDistribute_sms_minutes(int distribute_sms_minutes) {
//        this.distribute_sms_minutes = distribute_sms_minutes;
//    }
//
//    public int getReceive_sms_minute() {
//        return receive_sms_minute;
//    }
//
//    public void setReceive_sms_minute(int receive_sms_minute) {
//        this.receive_sms_minute = receive_sms_minute;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public boolean isIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(boolean isuse) {
//        this.isuse = isuse;
//    }
//
//    public String getFacility_name() {
//        return facility_name;
//    }
//
//    public void setFacility_name(String facility_name) {
//        this.facility_name = facility_name;
//    }
//
//    public List<WorkSheetPoolConfigFacilityModel> getFacilityModels() {
//        return facilityModels;
//    }
//
//    public void setFacilityModels(List<WorkSheetPoolConfigFacilityModel> facilityModels) {
//        this.facilityModels = facilityModels;
//    }
//
//    public List<WorkSheetPoolConfigRoleModel> getRoleModels() {
//        return roleModels;
//    }
//
//    public void setRoleModels(List<WorkSheetPoolConfigRoleModel> roleModels) {
//        this.roleModels = roleModels;
//    }
//
//    public String getRole_name() {
//        return role_name;
//    }
//
//    public void setRole_name(String role_name) {
//        this.role_name = role_name;
//    }
}
