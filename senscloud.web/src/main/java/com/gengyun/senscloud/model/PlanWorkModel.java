package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * 功能：计划工单
 * Created by Dong wudang on 2018/11/24.
 */
public class PlanWorkModel implements Serializable {
//    private static final long serialVersionUID = 257177317653961448L;
//    private String plan_code;//程序定义，编码
//    private String plan_name;//任务名称
//    private int work_type_id;//工单类型id
//    private int facility_id;//所属位置
//    private String pool_id;//工单池
//    private int relation_type;//关联对象：1：位置2：设备3：点巡检点9：其他；
//    private Map<String, Object> task_list;//任务列表
//    private String task_list_word;//任务列表字符串
//    private float plan_hour;//预计工时
//    private int do_type;// 触发条件：1：所有条件满足执行；2：任一条件满足即执行；
//    private int period;//周期0：一次性任务；>0：周期值；
//    private Timestamp begin_time;//开始日期
//    private Timestamp end_time;//截止日期
//    private int pre_day;//提前生成天
//    private boolean isuse;//是否启用
//    private String remark;//备注
//    private int status;
//    private Timestamp create_time;//创建时间
//    private String create_user_account;//创建人
//    private int allocate_receiver_type;//分配人员配置
//    private int generate_bill_type;//生成工单配置
//    private String facility_name;
//    private String executor;//执行人
//    private String assist;//外协
//
//    /*扩展字段*/
//    private String work_type_name;
//
//    public String getPlan_code() {
//        return plan_code;
//    }
//
//    public void setPlan_code(String plan_code) {
//        this.plan_code = plan_code;
//    }
//
//    public String getPlan_name() {
//        return plan_name;
//    }
//
//    public void setPlan_name(String plan_name) {
//        this.plan_name = plan_name;
//    }
//
//    public int getWork_type_id() {
//        return work_type_id;
//    }
//
//    public void setWork_type_id(int work_type_id) {
//        this.work_type_id = work_type_id;
//    }
//
//    public int getFacility_id() {
//        return facility_id;
//    }
//
//    public void setFacility_id(int facility_id) {
//        this.facility_id = facility_id;
//    }
//
//    public int getRelation_type() {
//        return relation_type;
//    }
//
//    public void setRelation_type(int relation_type) {
//        this.relation_type = relation_type;
//    }
//
//    public Map<String, Object> getTask_list() {
//        return task_list;
//    }
//
//    public void setTask_list(Map<String, Object> task_list) {
//        this.task_list = task_list;
//    }
//
//    public String getTask_list_word() {
//        return task_list_word;
//    }
//
//    public void setTask_list_word(String task_list_word) {
//        this.task_list_word = task_list_word;
//    }
//
//    public float getPlan_hour() {
//        return plan_hour;
//    }
//
//    public void setPlan_hour(float plan_hour) {
//        this.plan_hour = plan_hour;
//    }
//
//    public int getDo_type() {
//        return do_type;
//    }
//
//    public void setDo_type(int do_type) {
//        this.do_type = do_type;
//    }
//
//    public int getPeriod() {
//        return period;
//    }
//
//    public void setPeriod(int period) {
//        this.period = period;
//    }
//
//    public Timestamp getBegin_time() {
//        return begin_time;
//    }
//
//    public void setBegin_time(Timestamp begin_time) {
//        this.begin_time = begin_time;
//    }
//
//    public Timestamp getEnd_time() {
//        return end_time;
//    }
//
//    public void setEnd_time(Timestamp end_time) {
//        this.end_time = end_time;
//    }
//
//    public int getPre_day() {
//        return pre_day;
//    }
//
//    public void setPre_day(int pre_day) {
//        this.pre_day = pre_day;
//    }
//
//    public boolean isIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(boolean isuse) {
//        this.isuse = isuse;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public String getWork_type_name() {
//        return work_type_name;
//    }
//
//    public void setWork_type_name(String work_type_name) {
//        this.work_type_name = work_type_name;
//    }
//
//    public String getPool_id() {
//        return pool_id;
//    }
//
//    public void setPool_id(String pool_id) {
//        this.pool_id = pool_id;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    //工单类型对应的工单模板，是否有子工单,1:
//    private int is_have_sub;
//
//    public int getIs_have_sub() {
//        return is_have_sub;
//    }
//
//    public void setIs_have_sub(int is_have_sub) {
//        this.is_have_sub = is_have_sub;
//    }
//
//    //业务类型id
//    private int business_type_id;
//
//    public int getBusiness_type_id() {
//        return business_type_id;
//    }
//
//    public void setBusiness_type_id(int business_type_id) {
//        this.business_type_id = business_type_id;
//    }
//
//    private String business_name;
//
//    public String getBusiness_name() {
//        return business_name;
//    }
//
//    public void setBusiness_name(String business_name) {
//        this.business_name = business_name;
//    }
//
//    private String flow_template_code;
//
//    private String work_template_code;
//
//    public String getFlow_template_code() {
//        return flow_template_code;
//    }
//
//    public void setFlow_template_code(String flow_template_code) {
//        this.flow_template_code = flow_template_code;
//    }
//
//    public String getWork_template_code() {
//        return work_template_code;
//    }
//
//    public void setWork_template_code(String work_template_code) {
//        this.work_template_code = work_template_code;
//    }
//
//    public int getAllocate_receiver_type() {
//        return allocate_receiver_type;
//    }
//
//    public void setAllocate_receiver_type(int allocate_receiver_type) {
//        this.allocate_receiver_type = allocate_receiver_type;
//    }
//
//    public int getGenerate_bill_type() {
//        return generate_bill_type;
//    }
//
//    public void setGenerate_bill_type(int generate_bill_type) {
//        this.generate_bill_type = generate_bill_type;
//    }
//
//    public String getFacility_name() {
//        return facility_name;
//    }
//
//    public void setFacility_name(String facility_name) {
//        this.facility_name = facility_name;
//    }
//
//    public String getExecutor() {
//        return executor;
//    }
//
//    public String getAssist() {
//        return assist;
//    }
}
