package com.gengyun.senscloud.model;

import java.io.Serializable;

public class InspectionData implements Serializable {
//    private static final long serialVersionUID = -7549396482473381706L;
//    private String inspection_code;
//    private String schema_name;
//    private String area_code;
//    private String area_name;
//    private String inspection_account;
//    private String inspection_account_name;
//    private Timestamp inspection_time;
//    private int status;
//    private int fault_number;
//    private int facility_id ;
//    private String remark;
//    private String create_user_account;
//    private Timestamp createtime;
//    private String inspection_result;
//    private String strcode;
//    private String strname;
//    private int inspection_no;
//    private List<InspectionItemData> inspectionItemList;
//    private InspectionAreaData inspectionArea;
//    private boolean is_abnormal;
//    private String abnormal_files;
//    //上报问题产生的维修单
//    private List<RepairData> repairDataList;
//
//    public String getInspection_code() {
//        return inspection_code;
//    }
//
//    public void setInspection_code(String inspection_code) {
//        this.inspection_code = inspection_code;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public String getInspection_account() {
//        return inspection_account;
//    }
//
//    public void setInspection_account(String inspection_account) {
//        this.inspection_account = inspection_account;
//    }
//
//    public void setInspectionAccountName(String inspection_account_name) {
//        this.inspection_account_name = inspection_account_name;
//    }
//
//    public String getInspectionAccountName() {
//        return inspection_account_name;
//    }
//
//    public Timestamp getInspection_time() {
//        return inspection_time;
//    }
//
//    public void setInspection_time(Timestamp inspection_time) {
//        this.inspection_time = inspection_time;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public int getFault_number() {
//        return fault_number;
//    }
//
//    public void setFault_number(int fault_number) {
//        this.fault_number = fault_number;
//    }
//
//    public int getFacility_id() {
//        return facility_id;
//    }
//
//    public void setFacility_id(int facility_id) {
//        this.facility_id = facility_id;
//    }
//
//    private String facility_name;
//
//    public String getFacilityName() {
//        return facility_name;
//    }
//
//    public void setFacilityName(String facility_name) {
//        this.facility_name = facility_name;
//    }
//
//    private String parent_name;
//
//    public String getParentName() {
//        return parent_name;
//    }
//
//    public void setParentName(String parent_name) {
//        this.parent_name = parent_name;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public Timestamp getCreatetime() {
//        return createtime;
//    }
//
//    public void setCreatetime(Timestamp createtime) {
//        this.createtime = createtime;
//    }
//
//    public String getInspection_result() {
//        return inspection_result;
//    }
//
//    public void setInspection_result(String inspection_result) {
//        this.inspection_result = inspection_result;
//    }
//
//    public List<InspectionItemData> getInspectionItemList() {
//        return inspectionItemList;
//    }
//
//    public void setInspectionItemList(List<InspectionItemData> inspectionItemList) {
//        this.inspectionItemList = inspectionItemList;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public String getArea_code() {
//        return area_code;
//    }
//
//    public void setArea_code(String area_code) {
//        this.area_code = area_code;
//    }
//
//    public String getArea_name() {
//        return area_name;
//    }
//
//    public void setArea_name(String area_name) {
//        this.area_name = area_name;
//    }
//
//    public InspectionAreaData getInspectionAreaData() {
//        return inspectionArea;
//    }
//
//    public void setInspectionAreaData(InspectionAreaData inspectionArea) {
//        this.inspectionArea = inspectionArea;
//    }
//
//    private  Timestamp begin_time;
//
//    public Timestamp getBeginTime() {
//        return begin_time;
//    }
//
//    public void setBeginTime(Timestamp begin_time) {
//        this.begin_time = begin_time;
//    }
//
//    public int getInspection_no() {
//        return inspection_no;
//    }
//
//    public void setInspection_no(int inspection_no) {
//        this.inspection_no = inspection_no;
//    }
//
//    public List<RepairData> getRepairDataList() {
//        return repairDataList;
//    }
//
//    public void setRepairDataList(List<RepairData> repairDataList) {
//        this.repairDataList = repairDataList;
//    }
//
//    public boolean isIs_abnormal() {
//        return is_abnormal;
//    }
//
//    public void setIs_abnormal(boolean is_abnormal) {
//        this.is_abnormal = is_abnormal;
//    }
//
//    public String getAbnormal_files() {
//        return abnormal_files;
//    }
//
//    public void setAbnormal_files(String abnormal_files) {
//        this.abnormal_files = abnormal_files;
//    }
}
