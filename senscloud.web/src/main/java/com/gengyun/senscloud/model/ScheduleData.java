package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class ScheduleData implements Serializable {
    private static final long serialVersionUID = 6355700398809896025L;
    private Integer id;
    private String shedule_name;
    private String task_class;
    private Integer task_type;
    private String expression;
    private String is_use;
    private Boolean reschedule;
    private Boolean is_running;
    private Timestamp createtime;
    private String create_user_id;
    private Integer run_channel;

    public String getShedule_name() {
        return shedule_name;
    }

    public void setShedule_name(String shedule_name) {
        this.shedule_name = shedule_name;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
    }

    public String getCreate_user_id() { return create_user_id; }

    public void setCreate_user_id(String create_user_id) { this.create_user_id = create_user_id; }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask_class() {
        return task_class;
    }

    public void setTask_class(String task_class) {
        this.task_class = task_class;
    }

    public Integer getTask_type() {
        return task_type;
    }

    public void setTask_type(Integer task_type) {
        this.task_type = task_type;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getIs_use() { return is_use; }

    public void setIs_use(String is_use) { this.is_use = is_use; }

    public Boolean getReschedule() {
        return reschedule;
    }

    public void setReschedule(Boolean reschedule) {
        this.reschedule = reschedule;
    }

    public Boolean getIs_running() {
        return is_running;
    }

    public void setIs_running(Boolean is_running) {
        this.is_running = is_running;
    }

    public Integer getRun_channel() {
        return run_channel;
    }

    public void setRun_channel(Integer run_channel) {
        this.run_channel = run_channel;
    }
}
