//package com.gengyun.senscloud.model;
//
//import org.springframework.data.annotation.Id;
//
//import java.io.Serializable;
//
//public class UserRoleBase implements IUserRole,Serializable {
//    private static final long serialVersionUID = -6789047583929361302L;
//    @Id
//    private String id; // 编号
//    private String name; // 角色标识程序中判断使用,如"admin",这个是唯一的:
//
//    private boolean available = true; // 是否可用,如果不可用将不会添加给用户
//    private boolean sys_role = false; // 是否系统定义角色，如果是，不允许企业管理员用户删除或修改
//
//    private boolean is_free = false;
//
//    @Override
//    public String getId() {
//        return id;
//    }
//
//    @Override
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    @Override
//    public String getName() {
//        return name;
//    }
//
//    @Override
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public boolean isAvailable() {
//        return available;
//    }
//
//    public void setAvailable(boolean available) {
//        this.available = available;
//    }
//
//    public boolean isSys_role() {
//        return sys_role;
//    }
//
//    public void setSys_role(boolean sys_role) {
//        this.sys_role = sys_role;
//    }
//
//    public boolean isIs_free() {
//        return is_free;
//    }
//
//    public void setIs_free(boolean is_free) {
//        this.is_free = is_free;
//    }
//}
