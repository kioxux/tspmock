package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@ApiModel(description = "新增统计配置信息")
public class StatisticAdd implements Serializable {
    private static final long serialVersionUID = -1;
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "标题", required = true)
    private String title;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "图表配置")
    private String options;
    @ApiModelProperty(value = "表格查询列")
    private String query_conditions;
    @ApiModelProperty(value = "查询语句")
    private String query;
    @ApiModelProperty(value = "表格查询语句")
    private String table_query;
    @ApiModelProperty(value = "查询条件")
    private String table_columns;
    @ApiModelProperty(value = "显示看板 0不显示  1显示")
    private String is_show_dashboard;
    @ApiModelProperty(value = "定制页面")
    private String client_page; //客户定制页面
    @ApiModelProperty(value = "分组")
    private String group_name; //客户定制页面
    @ApiModelProperty(value = "导出地址")
    private String export_url; //导出地址

    //    private String create_user_account;
//    private Timestamp createtime;

    public String getExport_url() {
        return export_url;
    }

    public void setExport_url(String export_url) {
        this.export_url = export_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getTable_query() {
        return table_query;
    }

    public void setTable_query(String table_query) {
        this.table_query = table_query;
    }

    public String getClient_page() {
        return client_page;
    }

    public void setClient_page(String client_page) {
        this.client_page = client_page;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getQuery_conditions() {
        return query_conditions;
    }

    public void setQuery_conditions(String query_conditions) {
        this.query_conditions = query_conditions;
    }

    public String getTable_columns() {
        return table_columns;
    }

    public void setTable_columns(String table_columns) {
        this.table_columns = table_columns;
    }

    public String getIs_show_dashboard() {
        return is_show_dashboard;
    }

    public void setIs_show_dashboard(String is_show_dashboard) {
        this.is_show_dashboard = is_show_dashboard;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
