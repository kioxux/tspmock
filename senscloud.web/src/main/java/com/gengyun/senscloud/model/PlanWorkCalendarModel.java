package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class PlanWorkCalendarModel implements Serializable {
    private static final long serialVersionUID = -1L;

    private String work_calendar_code;//工单行事历编号
    private int work_type_id;//工单类型id
    private String work_template_code;//工单模板编码
    private String title;//主题
    private String position_code;//设备位置
    private int relation_type;//关联对象：1：位置 2：设备 3：点巡检点 9：其他；
    private String relation_id;//关联对象编码
    private String problem_note;//描述（问题）
    private Timestamp occur_time;//发生时间
    private Timestamp deadline_time;//截止时间
    private Float plan_deal_time;//预计工时（分）
    private int status;//状态：1：未转工单；2：已转工单；
    private String receive_user_id;//负责人id
    private int group_id;//负责团队
    private String plan_code;//计划编号
    private int priority_level;//紧急程度:0:无；1:低；2:中；3:高；
    private String remark;//备注
    private Timestamp create_time;//创建时间
    private String create_user_id;//创建人id:手动生成时的人员id；自动生成为system
    private int auto_generate_bill;//是否自动生成工单：1：生成；2：不生成；
    private int generate_time_point;//自动生成工单触发时间点
    private String body_property;
    private String plan_work_property;//计划工单的自定义参数

    public String getWork_calendar_code() {
        return work_calendar_code;
    }

    public void setWork_calendar_code(String work_calendar_code) {
        this.work_calendar_code = work_calendar_code;
    }

    public int getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(int work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public int getRelation_type() {
        return relation_type;
    }

    public void setRelation_type(int relation_type) {
        this.relation_type = relation_type;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public String getProblem_note() {
        return problem_note;
    }

    public void setProblem_note(String problem_note) {
        this.problem_note = problem_note;
    }

    public Timestamp getOccur_time() {
        return occur_time;
    }

    public void setOccur_time(Timestamp occur_time) {
        this.occur_time = occur_time;
    }

    public Timestamp getDeadline_time() {
        return deadline_time;
    }

    public void setDeadline_time(Timestamp deadline_time) {
        this.deadline_time = deadline_time;
    }

    public Float getPlan_deal_time() {
        return plan_deal_time;
    }

    public void setPlan_deal_time(Float plan_deal_time) {
        this.plan_deal_time = plan_deal_time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getPlan_code() {
        return plan_code;
    }

    public void setPlan_code(String plan_code) {
        this.plan_code = plan_code;
    }

    public int getPriority_level() {
        return priority_level;
    }

    public void setPriority_level(int priority_level) {
        this.priority_level = priority_level;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public int getAuto_generate_bill() {
        return auto_generate_bill;
    }

    public void setAuto_generate_bill(int auto_generate_bill) {
        this.auto_generate_bill = auto_generate_bill;
    }

    public String getPlan_work_property() {
        return plan_work_property;
    }

    public void setPlan_work_property(String plan_work_property) {
        this.plan_work_property = plan_work_property;
    }

    public String getBody_property() {
        return body_property;
    }

    public void setBody_property(String body_property) {
        this.body_property = body_property;
    }

    public int getGenerate_time_point() {
        return generate_time_point;
    }

    public void setGenerate_time_point(int generate_time_point) {
        this.generate_time_point = generate_time_point;
    }
}
