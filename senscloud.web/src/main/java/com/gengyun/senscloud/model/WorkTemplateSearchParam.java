package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "模板参数")
public class WorkTemplateSearchParam implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "模板主键")
    private String work_template_code;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "字段主键")
    private String field_form_code;
    @ApiModelProperty(value = "控件类型")
    private String selectKey;
    @ApiModelProperty(value = "字段联动条件主键")
    private Integer id;
    @ApiModelProperty(value = "字段控件类型")
    private String field_view_types;
    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getField_form_code() {
        return field_form_code;
    }

    public void setField_form_code(String field_form_code) {
        this.field_form_code = field_form_code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSelectKey() {
        return selectKey;
    }

    public void setSelectKey(String selectKey) {
        this.selectKey = selectKey;
    }

    public String getField_view_types() {
        return field_view_types;
    }

    public void setField_view_types(String field_view_types) {
        this.field_view_types = field_view_types;
    }
}
