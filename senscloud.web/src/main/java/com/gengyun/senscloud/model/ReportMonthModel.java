package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

@ApiModel(description = "月度报告实体")

public class ReportMonthModel {
    @ApiModelProperty(value = "报告名称")
    private String report_name;
    @ApiModelProperty(value = "生成时间")
    private Timestamp generation_time;
    @ApiModelProperty(value = "报告年份")
    private String year;
    @ApiModelProperty(value = "报告月份")
    private String month;

//    @ApiModelProperty(value = "关键字")
//    private String keywordSearch;
    @ApiModelProperty(value = "开始时间")
    private String beginDate;
    @ApiModelProperty(value = "结束时间")
    private String endDate;

    public String getReport_name() {
        return report_name;
    }

    public void setReport_name(String report_name) {
        this.report_name = report_name;
    }

    public Timestamp getGeneration_time() {
        return generation_time;
    }

    public void setGeneration_time(Timestamp generation_time) {
        this.generation_time = generation_time;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

//    public String getKeywordSearch() {
//        return keywordSearch;
//    }
//
//    public void setKeywordSearch(String keywordSearch) {
//        this.keywordSearch = keywordSearch;
//    }
}
