package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 *绩效模板-考核项-考核标准对象
 */
public class PerformanceTemplateOptionsEquation implements Serializable {
//
//    private static final long serialVersionUID = -1350070445274479157L;
//    private long template_id;//绩效模板主键ID
//    private long template_option_id;//绩效模板考核项主键ID
//    private int greater;//大于
//    private int less;//小于
//    private BigDecimal greater_value;//大于分数
//    private BigDecimal less_value;//小于分数
//    private String equation;//计算公式
//
//    public long getTemplate_id() {
//        return template_id;
//    }
//
//    public void setTemplate_id(long template_id) {
//        this.template_id = template_id;
//    }
//
//    public long getTemplate_option_id() {
//        return template_option_id;
//    }
//
//    public void setTemplate_option_id(long template_option_id) {
//        this.template_option_id = template_option_id;
//    }
//
//    public int getGreater() {
//        return greater;
//    }
//
//    public void setGreater(int greater) {
//        this.greater = greater;
//    }
//
//    public int getLess() {
//        return less;
//    }
//
//    public void setLess(int less) {
//        this.less = less;
//    }
//
//    public BigDecimal getGreater_value() {
//        return greater_value;
//    }
//
//    public void setGreater_value(BigDecimal greater_value) {
//        this.greater_value = greater_value;
//    }
//
//    public BigDecimal getLess_value() {
//        return less_value;
//    }
//
//    public void setLess_value(BigDecimal less_value) {
//        this.less_value = less_value;
//    }
//
//    public String getEquation() {
//        return equation;
//    }
//
//    public void setEquation(String equation) {
//        this.equation = equation;
//    }
}
