package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "其它配置接口公共参数")
public class OtherCfgModel implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "缓存名称", required = true)
    private String name;
    @ApiModelProperty(value = "缓存键值", required = true)
    private String key;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "开始日期")
    private String startDateSearch;
    @ApiModelProperty(value = "结束日期")
    private String endDateSearch;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getStartDateSearch() {
        return startDateSearch;
    }

    public void setStartDateSearch(String startDateSearch) {
        this.startDateSearch = startDateSearch;
    }

    public String getEndDateSearch() {
        return endDateSearch;
    }

    public void setEndDateSearch(String endDateSearch) {
        this.endDateSearch = endDateSearch;
    }
}
