package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 字典表
 */
public class CloudDataModel {
    @ApiModelProperty(value = "主键id")
    private Integer id;
    @ApiModelProperty(value = "类型编码")
    private String data_type;
    @ApiModelProperty(value = "类型名称")
    private String data_type_nam;
    @ApiModelProperty(value = "是否需要翻译：1：不翻译，2：翻译")
    private String is_lang;
    @ApiModelProperty(value = "排序")
    private String data_order;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "编号")
    private String code;
    @ApiModelProperty(value = "值")
    private String name;
    @ApiModelProperty(value = "类型备注")
    private String type_remark;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getData_type_nam() {
        return data_type_nam;
    }

    public void setData_type_nam(String data_type_nam) {
        this.data_type_nam = data_type_nam;
    }

    public String getIs_lang() {
        return is_lang;
    }

    public void setIs_lang(String is_lang) {
        this.is_lang = is_lang;
    }

    public String getData_order() {
        return data_order;
    }

    public void setData_order(String data_order) {
        this.data_order = data_order;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType_remark() {
        return type_remark;
    }

    public void setType_remark(String type_remark) {
        this.type_remark = type_remark;
    }
}
