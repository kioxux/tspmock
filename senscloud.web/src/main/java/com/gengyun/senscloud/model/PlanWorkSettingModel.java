package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * 计划工单的配置
 */
public class PlanWorkSettingModel implements Serializable {
    private static final long serialVersionUID = -1L;
    private String schema_name;
    private String plan_code;//程序定义，编码
    private long id;//主键ID

    public String getPlan_code() {
        return plan_code;
    }

    public void setPlan_code(String plan_code) {
        this.plan_code = plan_code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSchema_name() {
        return schema_name;
    }

    public void setSchema_name(String schema_name) {
        this.schema_name = schema_name;
    }
}
