package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * 职安健
 */
public class SecurityModel {
    @ApiModelProperty(value = "职安健编码")
    private String security_item_code;
    @ApiModelProperty(value = "风险评估")
    private String security_item_name;
    @ApiModelProperty(value = "预防措施")
    private String rask_note;
    @ApiModelProperty(value = "职安健审批：1：是；2：否；")
    private String result_type;
    @ApiModelProperty(value = "标准")
    private String standard;

    public String getSecurity_item_code() {
        return security_item_code;
    }

    public void setSecurity_item_code(String security_item_code) {
        this.security_item_code = security_item_code;
    }

    public String getSecurity_item_name() {
        return security_item_name;
    }

    public void setSecurity_item_name(String security_item_name) {
        this.security_item_name = security_item_name;
    }

    public String getRask_note() {
        return rask_note;
    }

    public void setRask_note(String rask_note) {
        this.rask_note = rask_note;
    }

    public String getResult_type() {
        return result_type;
    }

    public void setResult_type(String result_type) {
        this.result_type = result_type;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }
}
