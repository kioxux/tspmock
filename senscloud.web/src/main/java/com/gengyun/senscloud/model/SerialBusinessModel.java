package com.gengyun.senscloud.model;

import java.io.Serializable;

public class SerialBusinessModel implements Serializable {

    private static final long serialVersionUID = 4082166423888338564L;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String business_type;

    public String getBusinessType() {
        return business_type;
    }

    public void setBusinessType(String business_type) {
        this.business_type = business_type;
    }

    private String serial_pre;

    public String getSerialPre() {
        return serial_pre;
    }

    public void setSerialPre(String serial_pre) {
        this.serial_pre = serial_pre;
    }

    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
