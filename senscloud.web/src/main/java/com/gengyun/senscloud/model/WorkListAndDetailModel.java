package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.util.List;

public class WorkListAndDetailModel implements Serializable {
    private static final long serialVersionUID = -1;
    private WorkListModel workListModel;
    private List<WorkListDetailModel> workListDetailModel;

    public WorkListModel getWorkListModel() {
        return workListModel;
    }

    public void setWorkListModel(WorkListModel workListModel) {
        this.workListModel = workListModel;
    }

    public List<WorkListDetailModel> getWorkListDetailModel() {
        return workListDetailModel;
    }

    public void setWorkListDetailModel(List<WorkListDetailModel> workListDetailModel) {
        this.workListDetailModel = workListDetailModel;
    }
}
