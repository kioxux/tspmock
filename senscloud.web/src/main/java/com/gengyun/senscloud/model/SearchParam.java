package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "查询参数")
public class SearchParam implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "维保计划编码集合")
    private String calendar_codes;
    @ApiModelProperty(value = "是否启用 1启用 0禁用")
    private String isUseSearch;
    @ApiModelProperty(value = "工单类型id字符串")
    private String workTypeIds;
    @ApiModelProperty(value = "工单类型id")
    private String workTypeId;
    @ApiModelProperty(value = "货币种类id")
    private String currencyIds;
    @ApiModelProperty(value = "开始日期")
    private String startDateSearch;
    @ApiModelProperty(value = "结束日期")
    private String endDateSearch;
    @ApiModelProperty(value = "设备位置")
    private String positionCode;
    @ApiModelProperty(value = "设备类型ids")
    private String assetTypeSearch;
    @ApiModelProperty(value = "设备型号ids")
    private String assetModelSearch;
    @ApiModelProperty(value = "启用日期")
    private String beginDateSearch;
    @ApiModelProperty(value = "勾选：true （全部选择）不勾选：false （手动选择设备）")
    private String isAllAsset;
    @ApiModelProperty(value = "任务模板字符串")
    private String taskTemplateCodes;
    @ApiModelProperty(value = "排序 asc 正序  desc 倒序")
    private String descStatusSearch;
    @ApiModelProperty(value = "子工单编号字符串")
    private String subWorkCodes;
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "分组类型id-1,2,3")
    private String groupName;
    @ApiModelProperty(value = "部门ids")
    private String groupIds;
    @ApiModelProperty(value = "显示类型：0、设备 1、任务", required = true)
    private String showType;
    @ApiModelProperty(value = "设备类型：0、全部设备  1、物联设备")
    private String assetType;
    @ApiModelProperty(value = "任务类型：0、全部任务 1、维修 2、计划 ")
    private String workType;
    @ApiModelProperty(value = "设备、位置类型 asset、设备 position、位置")
    private String type;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "设备类型ids")
    private String asset_category_id;
    @ApiModelProperty(value = "设备型号ids")
    private String asset_model_id;
    @ApiModelProperty(value = "启用日期")
    private String enable_begin_date;
    @ApiModelProperty(value = "结束日期")
    private String enable_end_date;
    @ApiModelProperty(value = "勾选：true （全部选择）不勾选：false （手动选择设备）")
    private String is_all_asset;

    public String getCalendar_codes() {
        return calendar_codes;
    }

    public void setCalendar_codes(String calendar_codes) {
        this.calendar_codes = calendar_codes;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getIsUseSearch() {
        return isUseSearch;
    }

    public void setIsUseSearch(String isUseSearch) {
        this.isUseSearch = isUseSearch;
    }

    public String getWorkTypeIds() {
        return workTypeIds;
    }

    public void setWorkTypeIds(String workTypeIds) {
        this.workTypeIds = workTypeIds;
    }

    public String getStartDateSearch() {
        return startDateSearch;
    }

    public void setStartDateSearch(String startDateSearch) {
        this.startDateSearch = startDateSearch;
    }

    public String getEndDateSearch() {
        return endDateSearch;
    }

    public void setEndDateSearch(String endDateSearch) {
        this.endDateSearch = endDateSearch;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCodes(String positionCode) {
        this.positionCode = positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public String getAssetTypeSearch() {
        return assetTypeSearch;
    }

    public void setAssetTypeSearch(String assetTypeSearch) {
        this.assetTypeSearch = assetTypeSearch;
    }

    public String getAssetModelSearch() {
        return assetModelSearch;
    }

    public void setAssetModelSearch(String assetModelSearch) {
        this.assetModelSearch = assetModelSearch;
    }

    public String getBeginDateSearch() {
        return beginDateSearch;
    }

    public void setBeginDateSearch(String beginDateSearch) {
        this.beginDateSearch = beginDateSearch;
    }

    public String getIsAllAsset() {
        return isAllAsset;
    }

    public void setIsAllAsset(String isAllAsset) {
        this.isAllAsset = isAllAsset;
    }

    public String getTaskTemplateCodes() {
        return taskTemplateCodes;
    }

    public void setTaskTemplateCodes(String taskTemplateCodes) {
        this.taskTemplateCodes = taskTemplateCodes;
    }

    public String getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(String workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getDescStatusSearch() {
        return descStatusSearch;
    }

    public void setDescStatusSearch(String descStatusSearch) {
        this.descStatusSearch = descStatusSearch;
    }

    public String getSubWorkCodes() {
        return subWorkCodes;
    }

    public void setSubWorkCodes(String subWorkCodes) {
        this.subWorkCodes = subWorkCodes;
    }

    public String getCurrencyIds() {
        return currencyIds;
    }

    public void setCurrencyIds(String currencyIds) {
        this.currencyIds = currencyIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAsset_category_id() {
        return asset_category_id;
    }

    public void setAsset_category_id(String asset_category_id) {
        this.asset_category_id = asset_category_id;
    }

    public String getAsset_model_id() {
        return asset_model_id;
    }

    public void setAsset_model_id(String asset_model_id) {
        this.asset_model_id = asset_model_id;
    }

    public String getEnable_begin_date() {
        return enable_begin_date;
    }

    public void setEnable_begin_date(String enable_begin_date) {
        this.enable_begin_date = enable_begin_date;
    }

    public String getEnable_end_date() {
        return enable_end_date;
    }

    public void setEnable_end_date(String enable_end_date) {
        this.enable_end_date = enable_end_date;
    }

    public String getIs_all_asset() {
        return is_all_asset;
    }

    public void setIs_all_asset(String is_all_asset) {
        this.is_all_asset = is_all_asset;
    }

    public String getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }
}
