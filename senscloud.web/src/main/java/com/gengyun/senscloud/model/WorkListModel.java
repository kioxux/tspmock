package com.gengyun.senscloud.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 功能：工单模板实例
 * Created by Dong wudang on 2018/11/2.
 */
public class WorkListModel implements Serializable {
    private static final long serialVersionUID = -1;
    private String work_code;//工单号
    private Integer work_type_id;//工单类型id
    private String work_template_code;//工单模板编码
    private String parent_code;//所属工单模板；
//    private String pool_id;//工单池id
//    private Integer facility_id;//所属位置
    private String position_code;
    private Integer relation_type;//关联对象
    private Integer is_main;//关联对象
    private String relation_id;//关联对象编码
    private Integer status;//状态
    private String status_name;//状态名称
    private String remark;//备注
    private String create_user_id;//创建人
    private String works_title;//主题名称
    private String work_code_bak;
    private String title;
//    private String business_type_id;
    private String receive_user_id;
    private String duty_user_id;
    private String duty_user_name;
    private String user_name;
    private String mobile;
    private String phone;
    private String work_type_name;
    private String type_name;
    private String position_name;
    private String priority_level_name;
    private Integer priority_level;
    private Integer category_id;
    private String category_name;
    private String str_sub_work_code;
    private String sub_work_code;
    private String asset_code;
    private String asset_name;
    private String business_name;
    private Integer business_type_id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp occur_time;//发生时间

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp deadline_time;//截止时间
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String create_time;
//    private Timestamp create_time;//创建时间

    private Timestamp receive_time;//接受时间
    private Timestamp distribute_time;//分配时间（最后一次时间，跟负责人修改同步更新）
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String begin_time;
//    private Timestamp begin_time;//开始时间

    private Timestamp finished_time;//完成时间
    private Timestamp plan_arrive_time;//预计到达时间
    private Timestamp plan_begin_time;//计划开工时间

    public String getWork_code() {
        return work_code;
    }

    public void setWork_code(String work_code) {
        this.work_code = work_code;
    }

    public Integer getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(Integer work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public Integer getRelation_type() {
        return relation_type;
    }

    public void setRelation_type(Integer relation_type) {
        this.relation_type = relation_type;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public String getWorks_title() {
        return works_title;
    }

    public void setWorks_title(String works_title) {
        this.works_title = works_title;
    }

    public String getWork_code_bak() {
        return work_code_bak;
    }

    public void setWork_code_bak(String work_code_bak) {
        this.work_code_bak = work_code_bak;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getStr_sub_work_code() {
        return str_sub_work_code;
    }

    public void setStr_sub_work_code(String str_sub_work_code) {
        this.str_sub_work_code = str_sub_work_code;
    }

    public Timestamp getOccur_time() {
        return occur_time;
    }

    public void setOccur_time(Timestamp occur_time) {
        this.occur_time = occur_time;
    }

    public Timestamp getDeadline_time() {
        return deadline_time;
    }

    public void setDeadline_time(Timestamp deadline_time) {
        this.deadline_time = deadline_time;
    }

//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }


    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Timestamp getReceive_time() {
        return receive_time;
    }

    public void setReceive_time(Timestamp receive_time) {
        this.receive_time = receive_time;
    }

    public Timestamp getDistribute_time() {
        return distribute_time;
    }

    public void setDistribute_time(Timestamp distribute_time) {
        this.distribute_time = distribute_time;
    }

//    public Timestamp getBegin_time() {
//        return begin_time;
//    }
//
//    public void setBegin_time(Timestamp begin_time) {
//        this.begin_time = begin_time;
//    }


    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public Timestamp getFinished_time() {
        return finished_time;
    }

    public void setFinished_time(Timestamp finished_time) {
        this.finished_time = finished_time;
    }

    public Timestamp getPlan_arrive_time() {
        return plan_arrive_time;
    }

    public void setPlan_arrive_time(Timestamp plan_arrive_time) {
        this.plan_arrive_time = plan_arrive_time;
    }

    public Timestamp getPlan_begin_time() {
        return plan_begin_time;
    }

    public void setPlan_begin_time(Timestamp plan_begin_time) {
        this.plan_begin_time = plan_begin_time;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public String getPriority_level_name() {
        return priority_level_name;
    }

    public void setPriority_level_name(String priority_level_name) {
        this.priority_level_name = priority_level_name;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getDuty_user_id() {
        return duty_user_id;
    }

    public void setDuty_user_id(String duty_user_id) {
        this.duty_user_id = duty_user_id;
    }

    public String getDuty_user_name() {
        return duty_user_name;
    }

    public void setDuty_user_name(String duty_user_name) {
        this.duty_user_name = duty_user_name;
    }

    public String getWork_type_name() {
        return work_type_name;
    }

    public void setWork_type_name(String work_type_name) {
        this.work_type_name = work_type_name;
    }

    public Integer getPriority_level() {
        return priority_level;
    }

    public void setPriority_level(Integer priority_level) {
        this.priority_level = priority_level;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getSub_work_code() {
        return sub_work_code;
    }

    public void setSub_work_code(String sub_work_code) {
        this.sub_work_code = sub_work_code;
    }

    public String getAsset_code() {
        return asset_code;
    }

    public void setAsset_code(String asset_code) {
        this.asset_code = asset_code;
    }

    public String getAsset_name() {
        return asset_name;
    }

    public void setAsset_name(String asset_name) {
        this.asset_name = asset_name;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public Integer getBusiness_type_id() {
        return business_type_id;
    }

    public void setBusiness_type_id(Integer business_type_id) {
        this.business_type_id = business_type_id;
    }

    public Integer getIs_main() {
        return is_main;
    }

    public void setIs_main(Integer is_main) {
        this.is_main = is_main;
    }
}
