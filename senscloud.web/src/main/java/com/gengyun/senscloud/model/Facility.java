//package com.gengyun.senscloud.model;
//
//import java.io.Serializable;
//import java.sql.Timestamp;
//
//public class Facility extends FacilityBase implements Serializable {
//    private static final long serialVersionUID = 6247403983682307907L;
//    private String schema_name;
//    private String parentName;
//    private Timestamp create_time;
//    private String create_user_account;
//    private String remark;
//    private boolean isuse;
//    private double lng;                 //经度
//    private double lat;                 //维度
//    private String location;
//    private String maptype;
//    private int org_type;
//    private int address_id;
//    private String facilitycode;
//    private String other_code;
//    private String address;
//    private String address_name;
//    private String type_name;                       //2019-04-02添加做维组织类型
//    private String areacode;
//    private String layerpath;
//    private int status = 0;                         //设备状态，0为正常状态，-1000为删除状态。
//    private String short_title;             // 简称
//    private Integer org_level;              // 组织级别
//    private Integer child_count;            // 子位置个数
//    private int currency_id;            // 货币
//
//    public String getAddress_name() {
//        return address_name;
//    }
//
//    public int getParentId() {
//        return parentId;
//    }
//
//    public void setParentId(int parentId) {
//        this.parentId = parentId;
//    }
//
//    public void setAddress_name(String address_name) {
//        this.address_name = address_name;
//    }
//
//    public int getAddress_id() {
//        return address_id;
//    }
//
//    public void setAddress_id(int address_id) {
//        this.address_id = address_id;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public String getType_name() {
//        return type_name;
//    }
//
//    public void setType_name(String type_name) {
//        this.type_name = type_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public String getparentName() {
//        return parentName;
//    }
//
//    public void setparentName(String parentName) {
//        this.parentName = parentName;
//    }
//
//    public Timestamp getCreateTime() {
//        return create_time;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.create_time = createTime;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public boolean isIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(boolean isuse) {
//        this.isuse = isuse;
//    }
//
//    public double getLng() {
//        return lng;
//    }
//
//    public void setLng(double lng) {
//        this.lng = lng;
//    }
//
//    public double getLat() {
//        return lat;
//    }
//
//    public void setLat(double lat) {
//        this.lat = lat;
//    }
//
//    public String getMaptype() {
//        return maptype;
//    }
//
//    public void setMaptype(String maptype) {
//        this.maptype = maptype;
//    }
//
//    public int getOrg_type() {
//        return org_type;
//    }
//
//    public void setOrg_type(int org_type) {
//        this.org_type = org_type;
//    }
//
//    public String getFacilitycode() {
//        return facilitycode;
//    }
//
//    public void setFacilitycode(String facilitycode) {
//        this.facilitycode = facilitycode;
//    }
//
//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    public String getAreacode() {
//        return areacode;
//    }
//
//    public void setAreacode(String areacode) {
//        this.areacode = areacode;
//    }
//
//    public String getLayerpath() {
//        return layerpath;
//    }
//
//    public void setLayerpath(String layerpath) {
//        this.layerpath = layerpath;
//    }
//
//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public String getOther_code() {
//        return other_code;
//    }
//
//    public void setOther_code(String other_code) {
//        this.other_code = other_code;
//    }
//
//    public String getShort_title() {
//        return short_title;
//    }
//
//    public void setShort_title(String short_title) {
//        this.short_title = short_title;
//    }
//
//    public Integer getOrg_level() {
//        return org_level;
//    }
//
//    public void setOrg_level(Integer org_level) {
//        this.org_level = org_level;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public Integer getChild_count() {
//        return child_count;
//    }
//
//    public Integer getCurrency_id() {
//        return currency_id;
//    }
//
//    public void setCurrency_id(int currency_id) {
//        this.currency_id = currency_id;
//    }
//
//    public void setChild_count(Integer child_count) {
//        this.child_count = child_count;
//    }
//
//    //    public String getFacility_no() {
////        return facility_no;
////    }
////
////    public void setFacility_no(String facility_no) {
////        this.facility_no = facility_no;
////    }
//}
