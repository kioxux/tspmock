package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * 绩效考核模板-考核项对象
 */
public class PerformanceTemplateOptions implements Serializable {
//
//    private static final long serialVersionUID = -1802408519040075823L;
//    private long id;
//    private long template_id;//绩效模板主键ID
//    private int type;//指标类型 1、客观指标 2、主观指标
//    private String assessment_latitude;//考核维度
//    private long dashboard_id;//定义
//    private String inspection_items;//考核项
//    private BigDecimal challenge_value;//挑战值
//    private BigDecimal target_value;//目标值
//    private BigDecimal minimum_value;//保底值
//    private BigDecimal weightiness;//权重
//    private boolean can_fill_in;//是否可填写 t、是  f、否
//    private String schema_name;
//    private List<PerformanceTemplateOptionsEquation> performanceTemplateOptionsEquations;
//
//    private String definition;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public long getTemplate_id() {
//        return template_id;
//    }
//
//    public void setTemplate_id(long template_id) {
//        this.template_id = template_id;
//    }
//
//    public int getType() {
//        return type;
//    }
//
//    public void setType(int type) {
//        this.type = type;
//    }
//
//    public String getAssessment_latitude() {
//        return assessment_latitude;
//    }
//
//    public void setAssessment_latitude(String assessment_latitude) {
//        this.assessment_latitude = assessment_latitude;
//    }
//
//    public long getDashboard_id() {
//        return dashboard_id;
//    }
//
//    public void setDashboard_id(long dashboard_id) {
//        this.dashboard_id = dashboard_id;
//    }
//
//    public String getInspection_items() {
//        return inspection_items;
//    }
//
//    public void setInspection_items(String inspection_items) {
//        this.inspection_items = inspection_items;
//    }
//
//    public BigDecimal getChallenge_value() {
//        return challenge_value;
//    }
//
//    public void setChallenge_value(BigDecimal challenge_value) {
//        this.challenge_value = challenge_value;
//    }
//
//    public BigDecimal getTarget_value() {
//        return target_value;
//    }
//
//    public void setTarget_value(BigDecimal target_value) {
//        this.target_value = target_value;
//    }
//
//    public BigDecimal getMinimum_value() {
//        return minimum_value;
//    }
//
//    public void setMinimum_value(BigDecimal minimum_value) {
//        this.minimum_value = minimum_value;
//    }
//
//    public BigDecimal getWeightiness() {
//        return weightiness;
//    }
//
//    public void setWeightiness(BigDecimal weightiness) {
//        this.weightiness = weightiness;
//    }
//
//    public boolean isCan_fill_in() {
//        return can_fill_in;
//    }
//
//    public void setCan_fill_in(boolean can_fill_in) {
//        this.can_fill_in = can_fill_in;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public List<PerformanceTemplateOptionsEquation> getPerformanceTemplateOptionsEquations() {
//        return performanceTemplateOptionsEquations;
//    }
//
//    public void setPerformanceTemplateOptionsEquations(List<PerformanceTemplateOptionsEquation> performanceTemplateOptionsEquations) {
//        this.performanceTemplateOptionsEquations = performanceTemplateOptionsEquations;
//    }
//
//    public String getDefinition() {
//        return definition;
//    }
//
//    public void setDefinition(String definition) {
//        this.definition = definition;
//    }
}
