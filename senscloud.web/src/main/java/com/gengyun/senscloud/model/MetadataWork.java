package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;

/**
 * 工单模板
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
public class MetadataWork implements Serializable {
    private static final long serialVersionUID = 1658776769057707894L;
    private String workTemplateCode; // 模板编号
    private String workTemplateName; // 模板名称
    private int relationType; // 关联对象
    private String parentCode; // 父级编号
    private String bodyProperty; // 自定义字段
    private String isuse; // 是否启用
    private String remark; // 备注
    private Timestamp createTime; // 创建时间
    private String createUserAccount; // 创建人
    private String isCommonTemplate; // 是否公共模板
    private String contentViewType; // 展示类型
    private String isHaveSub; // 是否有子工单
    private int templateType; // 模版用途
    private int workTypeId;//工单类型
    private Map<String, Object> extInfo;//扩展信息

    public int getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(int workTypeId) {
        this.workTypeId = workTypeId;
    }

    public int getTemplateType() {
        return templateType;
    }

    public void setTemplateType(int templateType) {
        this.templateType = templateType;
    }

    public String getWorkTemplateCode() {
        return workTemplateCode;
    }

    public void setWorkTemplateCode(String workTemplateCode) {
        this.workTemplateCode = workTemplateCode;
    }

    public String getWorkTemplateName() {
        return workTemplateName;
    }

    public void setWorkTemplateName(String workTemplateName) {
        this.workTemplateName = workTemplateName;
    }

    public int getRelationType() {
        return relationType;
    }

    public void setRelationType(int relationType) {
        this.relationType = relationType;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getBodyProperty() {
        return bodyProperty;
    }

    public void setBodyProperty(String bodyProperty) {
        this.bodyProperty = bodyProperty;
    }

    public String getIsuse() {
        return isuse;
    }

    public void setIsuse(String isuse) {
        this.isuse = isuse;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserAccount() {
        return createUserAccount;
    }

    public void setCreateUserAccount(String createUserAccount) {
        this.createUserAccount = createUserAccount;
    }

    public String getIsCommonTemplate() {
        return isCommonTemplate;
    }

    public void setIsCommonTemplate(String isCommonTemplate) {
        this.isCommonTemplate = isCommonTemplate;
    }

    public String getContentViewType() {
        return contentViewType;
    }

    public void setContentViewType(String contentViewType) {
        this.contentViewType = contentViewType;
    }

    public String getIsHaveSub() {
        return isHaveSub;
    }

    public void setIsHaveSub(String isHaveSub) {
        this.isHaveSub = isHaveSub;
    }

    public Map<String, Object> getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(Map<String, Object> extInfo) {
        this.extInfo = extInfo;
    }
}
