package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class AssetBomData implements Serializable {

    private static final long serialVersionUID = 1786050295530342189L;
    private int id;
    private String asset_name;
    private String asset_code;
    private String asset_id;
    private Timestamp createtime;
    private String create_user_account;
    private String bom_model;
    private String supplier_name;
    private String bom_name;
    private String brand_name;
    private String bom_code;
    private String bom_type_name;
    private Float price;
    private String material_code;
    private int is_from_service_supplier;
    private java.math.BigDecimal use_count;
    private int total_num;
    private Integer bom_source_type;
    private Timestamp finished_time;
    //    private Long part_id;
//    private String part_name;
    private String currency_name;
    private String consume_fee;
    private float use_days;
    private float maintenance_days;
    private int unit_type;
    private Timestamp maintenance_finished_time;

    public AssetBomData() {};
    public AssetBomData(String asset_code, String create_user_account, String bom_code, String material_code, float use_days, float maintenance_days, int unit_type) {
        this.asset_code = asset_code;
        this.create_user_account = create_user_account;
        this.bom_code = bom_code;
        this.material_code = material_code;
        this.use_days = use_days;
        this.maintenance_days = maintenance_days;
        this.unit_type = unit_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAsset_name() {
        return asset_name;
    }

    public void setAsset_name(String asset_name) {
        this.asset_name = asset_name;
    }

    public String getAsset_code() {
        return asset_code;
    }

    public void setAsset_code(String asset_code) {
        this.asset_code = asset_code;
    }

    public String getAsset_id() {
        return asset_id;
    }

    public void setAsset_id(String asset_id) {
        this.asset_id = asset_id;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }

    public String getBom_model() {
        return bom_model;
    }

    public void setBom_model(String bom_model) {
        this.bom_model = bom_model;
    }

    public String getBom_type_name() {
        return bom_type_name;
    }

    public void setBom_type_name(String bom_type_name) {
        this.bom_type_name = bom_type_name;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getBom_name() {
        return bom_name;
    }

    public void setBom_name(String bom_name) {
        this.bom_name = bom_name;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBom_code() {
        return bom_code;
    }

    public void setBom_code(String bom_code) {
        this.bom_code = bom_code;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getMaterial_code() {
        return material_code;
    }

    public void setMaterial_code(String material_code) {
        this.material_code = material_code;
    }

    public int getIs_from_service_supplier() {
        return is_from_service_supplier;
    }

    public void setIs_from_service_supplier(int is_from_service_supplier) {
        this.is_from_service_supplier = is_from_service_supplier;
    }

    public BigDecimal getUse_count() {
        return use_count;
    }

    public void setUse_count(BigDecimal use_count) {
        this.use_count = use_count;
    }

    public int getTotal_num() {
        return total_num;
    }

    public void setTotal_num(int total_num) {
        this.total_num = total_num;
    }

    public Timestamp getFinished_time() {
        return finished_time;
    }

    public void setFinished_time(Timestamp finished_time) {
        this.finished_time = finished_time;
    }

//    public Long getPart_id() {
//        return part_id;
//    }
//
//    public void setPart_id(Long part_id) {
//        this.part_id = part_id;
//    }
//
//    public String getPart_name() {
//        return part_name;
//    }
//
//    public void setPart_name(String part_name) {
//        this.part_name = part_name;
//    }

    public float getUse_days() {
        return use_days;
    }

    public void setUse_days(float use_days) {
        this.use_days = use_days;
    }

    public float getMaintenance_days() {
        return maintenance_days;
    }

    public void setMaintenance_days(float maintenance_days) {
        this.maintenance_days = maintenance_days;
    }

    public int getUnit_type() {
        return unit_type;
    }

    public void setUnit_type(int unit_type) {
        this.unit_type = unit_type;
    }

    public Timestamp getMaintenance_finished_time() {
        return maintenance_finished_time;
    }

    public void setMaintenance_finished_time(Timestamp maintenance_finished_time) {
        this.maintenance_finished_time = maintenance_finished_time;
    }

    public Integer getBom_source_type() {
        return bom_source_type;
    }

    public void setBom_source_type(Integer bom_source_type) {
        this.bom_source_type = bom_source_type;
    }

    public String getCurrency_name() {
        return currency_name;
    }

    public void setCurrency_name(String currency_name) {
        this.currency_name = currency_name;
    }

    public String getConsume_fee() {
        return consume_fee;
    }

    public void setConsume_fee(String consume_fee) {
        this.consume_fee = consume_fee;
    }
}
