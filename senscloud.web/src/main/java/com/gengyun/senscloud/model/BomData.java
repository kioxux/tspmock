package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/28.
 */
public class BomData implements Serializable {
//    private static final long serialVersionUID = -8473967303455274480L;
//    private int id;
//    private String bom_name;
//    private String bom_model;
//    private int quantity;
//    private String stock_code;
//    private Float price;
//    private String remark;
//    private Boolean isuse;
//    private String bom_code;
//    private Float security_quantity;
//    private int type_id;
//    private String type_name;
//    private int maintainCount;
//    private Timestamp create_time;
//    private String title;
//    private String short_title;
//    private String stock_name;
//    private int supplier;
//    private String supplier_name;
//    private String customer_name;
//    private int unit_id;
//    private String brand_name;
//    private String unit_name;
//    private int repairCount;
//    private String material_code;
//    private int is_from_service_supplier;
//    private String bom_page_key;
//    private int service_life;
//    private String specification;
//    private int bom_use;
//    private String bom_use_name;
//    private BigDecimal show_price;
//    private int currency_id;
//    private String bom_currency;
//    private int stock_currency_id;
//
//    public String getBom_use_name() {
//        return bom_use_name;
//    }
//
//    public void setBom_use_name(String bom_use_name) {
//        this.bom_use_name = bom_use_name;
//    }
//
//    public int getBom_use() {
//        return bom_use;
//    }
//
//    public void setBom_use(int bom_use) {
//        this.bom_use = bom_use;
//    }
//
//    public int getService_life() {
//        return service_life;
//    }
//
//    public void setService_life(int service_life) {
//        this.service_life = service_life;
//    }
//
//    public int getMaintainCount() {
//        return maintainCount;
//    }
//
//    public void setMaintainCount(int maintainCount) {
//        this.maintainCount = maintainCount;
//    }
//
//    public int getRepairCount() {
//        return repairCount;
//    }
//
//    public void setRepairCount(int repairCount) {
//        this.repairCount = repairCount;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    private String create_user_account;
//
//    public String getType_name() {
//        return type_name;
//    }
//
//    public void setType_name(String type_name) {
//        this.type_name = type_name;
//    }
//
//    public String getBom_code() {
//        return bom_code;
//    }
//
//    public void setBom_code(String bom_code) {
//        this.bom_code = bom_code;
//    }
//
//    public Float getSecurity_quantity() {
//        return security_quantity;
//    }
//
//    public void setSecurity_quantity(Float security_quantity) {
//        this.security_quantity = security_quantity;
//    }
//
//    public int getType_id() {
//        return type_id;
//    }
//
//    public void setType_id(int type_id) {
//        this.type_id = type_id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getStock_name() {
//        return stock_name;
//    }
//
//    public void setStock_name(String stock_name) {
//        this.stock_name = stock_name;
//    }
//
//
//    public String getUnit_name() {
//        return unit_name;
//    }
//
//    public void setUnit_name(String unit_name) {
//        this.unit_name = unit_name;
//    }
//
//    public int getUnit_id() {
//        return unit_id;
//    }
//
//    public void setUnit_id(int unit_id) {
//        this.unit_id = unit_id;
//    }
//
//    public String getBrand_name() {
//        return brand_name;
//    }
//
//    public void setBrand_name(String brand_name) {
//        this.brand_name = brand_name;
//    }
//
//    public String getCustomer_name() {
//        return customer_name;
//    }
//
//    public void setCustomer_name(String customer_name) {
//        this.customer_name = customer_name;
//    }
//
//    public int getSupplier() {
//        return supplier;
//    }
//
//    public void setSupplier(int supplier) {
//        this.supplier = supplier;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getBom_name() {
//        return bom_name;
//    }
//
//    public void setBom_name(String bom_name) {
//        this.bom_name = bom_name;
//    }
//
//    public String getBom_model() {
//        return bom_model;
//    }
//
//    public void setBom_model(String bom_model) {
//        this.bom_model = bom_model;
//    }
//
//    public int getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }
//
//    public String getStock_code() {
//        return stock_code;
//    }
//
//    public void setStock_code(String stock_code) {
//        this.stock_code = stock_code;
//    }
//
//    public Float getPrice() {
//        return price;
//    }
//
//    public void setPrice(Float price) {
//        this.price = price;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public Boolean getIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(Boolean isuse) {
//        this.isuse = isuse;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getMaterial_code() {
//        return material_code;
//    }
//
//    public void setMaterial_code(String material_code) {
//        this.material_code = material_code;
//    }
//
//    public int getIs_from_service_supplier() {
//        return is_from_service_supplier;
//    }
//
//    public void setIs_from_service_supplier(int is_from_service_supplier) {
//        this.is_from_service_supplier = is_from_service_supplier;
//    }
//
//    public String getBom_page_key() {
//        return bom_page_key;
//    }
//
//    public void setBom_page_key(String bom_page_key) {
//        this.bom_page_key = bom_page_key;
//    }
//
//    public String getSpecification() {
//        return specification;
//    }
//
//    public void setSpecification(String specification) {
//        this.specification = specification;
//    }
//
//    public BigDecimal getShow_price() {
//        return show_price;
//    }
//
//    public void setShow_price(BigDecimal show_price) {
//        this.show_price = show_price;
//    }
//
//    public int getCurrency_id() {
//        return currency_id;
//    }
//
//    public void setCurrency_id(int currency_id) {
//        this.currency_id = currency_id;
//    }
//
//    public String getBom_currency() {
//        return bom_currency;
//    }
//
//    public void setBom_currency(String bom_currency) {
//        this.bom_currency = bom_currency;
//    }
//
//    public String getShort_title() {
//        return short_title;
//    }
//
//    public void setShort_title(String short_title) {
//        this.short_title = short_title;
//    }
//
//    public String getSupplier_name() {
//        return supplier_name;
//    }
//
//    public void setSupplier_name(String supplier_name) {
//        this.supplier_name = supplier_name;
//    }
//
//    public int getStock_currency_id() {
//        return stock_currency_id;
//    }
//
//    public void setStock_currency_id(int stock_currency_id) {
//        this.stock_currency_id = stock_currency_id;
//    }
}
