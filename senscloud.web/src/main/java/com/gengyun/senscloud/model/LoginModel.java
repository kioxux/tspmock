package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "登录接口公共参数")
public class LoginModel implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "用户名、手机号等登录账号", required = true)
    private String u;
    @ApiModelProperty(value = "密码", required = true)
    private String p;
    @ApiModelProperty(value = "选择的企业号", required = true)
    private Long c_id;
    @ApiModelProperty(value = "语言类型", required = true)
    private String lang;
    @ApiModelProperty(value = "手机号", required = true)
    private String phone;
    @ApiModelProperty(value = "验证码", required = true)
    private String vfCode;
    @ApiModelProperty(value = "新密码", required = true)
    private String newPsd;
    @ApiModelProperty(value = "确认密码", required = true)
    private String cfmPsw;
    @ApiModelProperty(value = "员工工号", required = true)
    private String user_code;
    @ApiModelProperty(value = "员工NFC卡号", required = false)
    private String nfc_code;
    @ApiModelProperty(value = "微信code")
    private String userWxCode;

    public String getU() {
        return u;
    }

    public void setU(String u) {
        this.u = u;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public Long getC_id() {
        return c_id;
    }

    public void setC_id(Long c_id) {
        this.c_id = c_id;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVfCode() {
        return vfCode;
    }

    public void setVfCode(String vfCode) {
        this.vfCode = vfCode;
    }

    public String getNewPsd() {
        return newPsd;
    }

    public void setNewPsd(String newPsd) {
        this.newPsd = newPsd;
    }

    public String getCfmPsw() {
        return cfmPsw;
    }

    public void setCfmPsw(String cfmPsw) {
        this.cfmPsw = cfmPsw;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public String getUserWxCode() {
        return userWxCode;
    }

    public void setUserWxCode(String userWxCode) {
        this.userWxCode = userWxCode;
    }

    public String getNfc_code() {
        return nfc_code;
    }

    public void setNfc_code(String nfc_code) {
        this.nfc_code = nfc_code;
    }
}
