package com.gengyun.senscloud.model;

import java.io.Serializable;

public class MaintainData implements Serializable {
//    private static final long serialVersionUID = -3083649724475515436L;
//    private String suppilerName;
//
//    public String getSuppilerName() {
//        return suppilerName;
//    }
//
//    public void setSuppilerName(String suppilerName) {
//        this.suppilerName = suppilerName;
//    }
//
//    private String maintain_code;
//
//    public String getMaintainCode() {
//        return maintain_code;
//    }
//
//    public void setMaintainCode(String maintain_code) {
//        this.maintain_code = maintain_code;
//    }
//
//    private String asset_type;
//
//    public String getAssetType() {
//        return asset_type;
//    }
//
//    public void setAssetType(String asset_type) {
//        this.asset_type = asset_type;
//    }
//
//    private String asset_name;
//
//    public String getAssetName() {
//        return asset_name;
//    }
//
//    public void setAssetName(String asset_name) {
//        this.asset_name = asset_name;
//    }
//
//    private String asset_code;
//
//    public String getAssetCode() {
//        return asset_code;
//    }
//
//    public void setAssetCode(String asset_code) {
//        this.asset_code = asset_code;
//    }
//
//    private String asset_id;
//
//    public String getAssetId() {
//        return asset_id;
//    }
//
//    public void setAssetId(String asset_id) {
//        this.asset_id = asset_id;
//    }
//
//    private String properties;
//
//    public String getAssetProperties() {
//        return properties;
//    }
//
//    public void setAssetProperties(String properties) {
//        this.properties = properties;
//    }
//
//    private String maintain_account;
//
//    public String getMaintainAccount() {
//        return maintain_account;
//    }
//
//    public void setMaintainAccount(String maintain_account) {
//        this.maintain_account = maintain_account;
//    }
//
//    private String maintain_name;
//
//    public String getMaintainName() {
//        return maintain_name;
//    }
//
//    public void setMaintainName(String maintain_name) {
//        this.maintain_name = maintain_name;
//    }
//
//    private Timestamp deadline_time;
//
//    public Timestamp getDeadlineTime() {
//        return deadline_time;
//    }
//
//    public void setDeadlineTime(Timestamp deadline_time) {
//        this.deadline_time = deadline_time;
//    }
//
//    private Timestamp receive_time;
//
//    public Timestamp getReveiveTime() {
//        return receive_time;
//    }
//
//    public void setReveiveTime(Timestamp receive_time) {
//        this.receive_time = receive_time;
//    }
//
//    private Timestamp begin_time;
//
//    public Timestamp getBeginTime() {
//        return begin_time;
//    }
//
//    public void setBeginTime(Timestamp begin_time) {
//        this.begin_time = begin_time;
//    }
//
//    private float spend_hour;
//
//    public float getSpendHour() {
//        return spend_hour;
//    }
//
//    public void setSpendHour(float spend_hour) {
//        this.spend_hour = spend_hour;
//    }
//
//    private float total_work_hour;
//
//    public float getTotalWorkHour() {
//        return total_work_hour;
//    }
//
//    public void setTotalWorkHour(float total_work_hour) {
//        this.total_work_hour = total_work_hour;
//    }
//
//    private String before_img;
//
//    public String getBeforeImage() {
//        return before_img;
//    }
//
//    public void setBeforeImage(String before_img) {
//        this.before_img = before_img;
//    }
//
//    private String maintain_note;
//
//    public String getMaintainNote() {
//        return maintain_note;
//    }
//
//    public void setMaintainNote(String maintain_note) {
//        this.maintain_note = maintain_note;
//    }
//
//    private String after_img;
//
//    public String getAfterImage() {
//        return after_img;
//    }
//
//    public void setAfterImage(String after_img) {
//        this.after_img = after_img;
//    }
//
//    private Timestamp finished_time;
//
//    public Timestamp getFinishedTime() {
//        return finished_time;
//    }
//
//    public void setFinishedTime(Timestamp finished_time) {
//        this.finished_time = finished_time;
//    }
//
//    private String audit_account;
//
//    public String getAuditAccount() {
//        return audit_account;
//    }
//
//    public void setAuditAccount(String audit_account) {
//        this.audit_account = audit_account;
//    }
//
//    private String audit_name;
//
//    public String getAuditName() {
//        return audit_name;
//    }
//
//    public void setAuditName(String audit_name) {
//        this.audit_name = audit_name;
//    }
//
//    private Timestamp audit_time;
//
//    public Timestamp getAuditTime() {
//        return audit_time;
//    }
//
//    public void setAuditTime(Timestamp audit_time) {
//        this.audit_time = audit_time;
//    }
//
//    private String audit_word;
//
//    public String getAuditWord() {
//        return audit_word;
//    }
//
//    public void setAuditWord(String audit_word) {
//        this.audit_word = audit_word;
//    }
//
//    private int status;
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    private String status_name;
//
//    public String getStatusName() {
//        return status_name;
//    }
//
//    public void setStatusName(String status_name) {
//        this.status_name = status_name;
//    }
//
//    private int facility_number;
//
//    public int getFacilityNumber() {
//        return facility_number;
//    }
//
//    public void setFacilityNumber(int facility_number) {
//        this.facility_number = facility_number;
//    }
//
//    public int fault_number;
//
//    public int getFaultNumber() {
//        return fault_number;
//    }
//
//    public void setFaultNumber(int fault_number) {
//        this.fault_number = fault_number;
//    }
//
//    private int facility_id;
//
//    public int getFacilityId() {
//        return facility_id;
//    }
//
//    public void setFacilityId(int facility_id) {
//        this.facility_id = facility_id;
//    }
//
//    private String facility_name;
//
//    public String getFacilityName() {
//        return facility_name;
//    }
//
//    public void setFacilityName(String facility_name) {
//        this.facility_name = facility_name;
//    }
//
//    private String parent_name;
//
//    public String getParentName() {
//        return parent_name;
//    }
//
//    public void setParentName(String parent_name) {
//        this.parent_name = parent_name;
//    }
//
//    private Timestamp createtime;
//
//    public Timestamp getCreatetime() {
//        return createtime;
//    }
//
//    public void setCreatetime(Timestamp createtime) {
//        this.createtime = createtime;
//    }
//
//    private String create_user_account;
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    private String create_user_name;
//
//    public String getCreate_user_name() {
//        return create_user_name;
//    }
//
//    public void setCreate_user_name(String create_user_name) {
//        this.create_user_name = create_user_name;
//    }
//
//    private String maintain_result;
//
//    public String getMaintainResult() {
//        return maintain_result;
//    }
//
//    public void setMaintainResult(String maintainResult) {
//        this.maintain_result = maintainResult;
//    }
//
//    private String check_result;
//
//    public String getCheckResult() {
//        return check_result;
//    }
//
//    public void setCheckResult(String checkResult) {
//        this.check_result = checkResult;
//    }
//
//    private String remark;
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    private String operators;
//
//    public String getOperators() {
//        return operators;
//    }
//
//    public void setOperators(String operators) {
//        this.operators = operators;
//    }
//
//    private int cycle_count;
//
//    public int getCycleCount() {
//        return cycle_count;
//    }
//
//    public void setCycleCount(int cycle_count) {
//        this.cycle_count = cycle_count;
//    }
//
//    //保养开始日期
//    private Date maintain_begin_time;
//
//    public Date getMaintainBeginTime() {
//        return maintain_begin_time;
//    }
//
//    public void setMaintainBeginTime(Date maintain_begin_time) {
//        this.maintain_begin_time = maintain_begin_time;
//    }
//
//    private int asset_status;
//
//    public int getAssetStatus() {
//        return asset_status;
//    }
//
//    public void setAssetStatus(int asset_status) {
//        this.asset_status = asset_status;
//    }
//
//    private List<MaintainBomData> bomList;
//
//    public List<MaintainBomData> getBomList() {
//        return bomList;
//    }
//
//    public void setBomList(List<MaintainBomData> bomList) {
//        this.bomList = bomList;
//    }
//
//    private List<MaintainWorkHourData> workHourList;
//
//    public List<MaintainWorkHourData> getWorkHourList() {
//        return workHourList;
//    }
//
//    public void setWorkHourList(List<MaintainWorkHourData> workHourList) {
//        this.workHourList = workHourList;
//    }
//
//    private List<MaintainItemData> itemList;
//
//    public List<MaintainItemData> getMaintainItemList() {
//        return itemList;
//    }
//
//    public void setMaintainItemList(List<MaintainItemData> itemList) {
//        this.itemList = itemList;
//    }
//
//    private List<MaintainCheckItemData> checkItemList;
//
//    public List<MaintainCheckItemData> getMaintainCheckItemList() {
//        return checkItemList;
//    }
//
//    public void setMaintainCheckItemList(List<MaintainCheckItemData> checkItemList) {
//        this.checkItemList = checkItemList;
//    }
//
//    private List<LogsData> historyList;
//
//    public List<LogsData> getHistoryList() {
//        return historyList;
//    }
//
//    public void setHistoryList(List<LogsData> historyList) {
//        this.historyList = historyList;
//    }
//
//    //是否完成
//    private boolean isFinished;
//
//    public boolean getIsFinished() {
//        return isFinished;
//    }
//
//    public void setIsFinished(boolean isFinished) {
//        this.isFinished = isFinished;
//    }
//
//    private Timestamp return_time;
//
//    public Timestamp getReturnTime() {
//        return return_time;
//    }
//
//    public void setReturnTime(Timestamp return_time) {
//        this.return_time = return_time;
//    }
//
//    private List<StockData> bomStock;
//
//    public List<StockData> getBomStock() {
//        return bomStock;
//    }
//
//    public void setBomStock(List<StockData> bomStock) {
//        this.bomStock = bomStock;
//    }
//
//    private int waiting;
//
//    public int getWaiting() {
//        return waiting;
//    }
//
//    public void setWaiting(int waiting) {
//        this.waiting = waiting;
//    }
//
//    private String bom_app_result;
//
//    public String getBomAppResult() {
//        return bom_app_result;
//    }
//
//    public void setBomAppResult(String bom_app_result) {
//        this.bom_app_result = bom_app_result;
//    }
//
//    private String need_bom_application;
//
//    public String getNeed_bom_application() {
//        return need_bom_application;
//    }
//
//    public void setNeed_bom_application(String need_bom_application) {
//        this.need_bom_application = need_bom_application;
//    }
}
