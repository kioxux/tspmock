package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "字段信息")
public class WorkColumnAdd implements Serializable {
    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "字段主键")
    private String field_form_code;
    @ApiModelProperty(value = "字段编码", required = true)
    private String field_code;
    @ApiModelProperty(value = "字段显示名称/key", required = true)
    private String field_name;
    @ApiModelProperty(value = "存储方式")
    private String save_type;
    @ApiModelProperty(value = "写入只读权限", required = true)
    private String field_right;
    @ApiModelProperty(value = "是否必填 1是 2否")
    private String is_required;
    @ApiModelProperty(value = "备注")
    private String field_remark;
    @ApiModelProperty(value = "数据源")
    private String data_key;
    @ApiModelProperty(value = "默认值")
    private String field_value;
    @ApiModelProperty(value = "控件类型", required = true)
    private String field_view_type;
    @ApiModelProperty(value = "所属模块", required = true)
    private String field_section_type;
    @ApiModelProperty(value = "特殊属性列表,格式详见note说明")
    private String extList;
    //    @ApiModelProperty(value = "字段类型")
    //    private Integer change_type;
    //    @ApiModelProperty(value = "默认值")
    //    private BigDecimal field_value;
    //    @ApiModelProperty(value = "是否是表主键")
    //    private String is_table_key;
    //    @ApiModelProperty(value = "表类型")
    //    private Integer db_table_type;
    //    @ApiModelProperty(value = "接口数据源")
    //    private Integer field_data_base;
    //    @ApiModelProperty(value = "校验")
    //    private String field_validate_method;

    public String getField_form_code() {
        return field_form_code;
    }

    public void setField_form_code(String field_form_code) {
        this.field_form_code = field_form_code;
    }

    public String getField_code() {
        return field_code;
    }

    public void setField_code(String field_code) {
        this.field_code = field_code;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getSave_type() {
        return save_type;
    }

    public void setSave_type(String save_type) {
        this.save_type = save_type;
    }

    public String getField_right() {
        return field_right;
    }

    public void setField_right(String field_right) {
        this.field_right = field_right;
    }

    public String getIs_required() {
        return is_required;
    }

    public void setIs_required(String is_required) {
        this.is_required = is_required;
    }

    public String getField_remark() {
        return field_remark;
    }

    public void setField_remark(String field_remark) {
        this.field_remark = field_remark;
    }

    public String getField_view_type() {
        return field_view_type;
    }

    public void setField_view_type(String field_view_type) {
        this.field_view_type = field_view_type;
    }

    public String getField_section_type() {
        return field_section_type;
    }

    public void setField_section_type(String field_section_type) {
        this.field_section_type = field_section_type;
    }

    public String getExtList() {
        return extList;
    }

    public void setExtList(String extList) {
        this.extList = extList;
    }

    public String getData_key() {
        return data_key;
    }

    public void setData_key(String data_key) {
        this.data_key = data_key;
    }

    public String getField_value() {
        return field_value;
    }

    public void setField_value(String field_value) {
        this.field_value = field_value;
    }
}
