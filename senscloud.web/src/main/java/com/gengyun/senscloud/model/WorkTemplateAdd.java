package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/28.
 */
@ApiModel(description = "工单模板增加信息")
public class WorkTemplateAdd implements Serializable {
    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "模板名称", required = true)
    private String work_template_name;
    @ApiModelProperty(value = "模板编码", required = true)
    private String work_template_code;
    @ApiModelProperty(value = "备注")
    private String remark;

    public String getWork_template_name() {
        return work_template_name;
    }

    public void setWork_template_name(String work_template_name) {
        this.work_template_name = work_template_name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }
}
