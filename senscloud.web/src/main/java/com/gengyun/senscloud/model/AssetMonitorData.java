package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * 功能：设备监控
 * reconstruct yzj 2020-04-08
 */
public class AssetMonitorData implements Serializable {
    private static final long serialVersionUID = -6827266634256458786L;
    private String asset_code;
    private String monitor_code;
    private String monitor_name;
    // 采集值类型：1：连续量；2：开关量；3：文字；
    private int monitor_type;
    private int unit_id;
    private String unit_name;
    private float exception_up;
    private float exception_down;
    private float alarm_up;
    private float alarm_down;
    private float value_up;
    private float value_down;
    private Timestamp create_time;
    private String create_user_id;
    private String monitor_value;
    private Timestamp gather_time;

    public String getAsset_code() {
        return asset_code;
    }

    public void setAsset_code(String asset_code) {
        this.asset_code = asset_code;
    }

    public String getMonitor_code() {
        return monitor_code;
    }

    public void setMonitor_code(String monitor_code) {
        this.monitor_code = monitor_code;
    }

    public String getMonitor_name() {
        return monitor_name;
    }

    public void setMonitor_name(String monitor_name) {
        this.monitor_name = monitor_name;
    }

    public int getMonitor_type() {
        return monitor_type;
    }

    public void setMonitor_type(int monitor_type) {
        this.monitor_type = monitor_type;
    }

    public int getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(int unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public float getException_up() {
        return exception_up;
    }

    public void setException_up(float exception_up) {
        this.exception_up = exception_up;
    }

    public float getException_down() {
        return exception_down;
    }

    public void setException_down(float exception_down) {
        this.exception_down = exception_down;
    }

    public float getAlarm_up() {
        return alarm_up;
    }

    public void setAlarm_up(float alarm_up) {
        this.alarm_up = alarm_up;
    }

    public float getAlarm_down() {
        return alarm_down;
    }

    public void setAlarm_down(float alarm_down) {
        this.alarm_down = alarm_down;
    }

    public float getValue_up() {
        return value_up;
    }

    public void setValue_up(float value_up) {
        this.value_up = value_up;
    }

    public float getValue_down() {
        return value_down;
    }

    public void setValue_down(float value_down) {
        this.value_down = value_down;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public String getMonitor_value() {
        return monitor_value;
    }

    public void setMonitor_value(String monitor_value) {
        this.monitor_value = monitor_value;
    }

    public Timestamp getGather_time() {
        return gather_time;
    }

    public void setGather_time(Timestamp gather_time) {
        this.gather_time = gather_time;
    }
}
