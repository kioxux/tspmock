package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class WorkFlowTemplateSearchParam implements Serializable {

    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "流程的id/流程列表的parent，流程的时候也塞id",required = true)
    private String pref_id;
    @ApiModelProperty(value = "节点的id/流程列表的id，流程时也塞id",required = true)
    private String node_id;
    @ApiModelProperty(value = "流程：flow,开始节点：start,过程节点：node,结束节点：end,子节点：subNode ",required = true)
    private String node_type;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "字段主键")
    private String field_form_code;
    @ApiModelProperty(value = "基本类型（处理）：base 详情：detail")
    private String page_type;
    @ApiModelProperty(value = "数据库主键")
    private Integer id;
    @ApiModelProperty(value = "字段控件类型")
    private String field_view_types;
    public String getPref_id() {
        return pref_id;
    }

    public void setPref_id(String pref_id) {
        this.pref_id = pref_id;
    }

    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getField_form_code() {
        return field_form_code;
    }

    public void setField_form_code(String field_form_code) {
        this.field_form_code = field_form_code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPage_type() {
        return page_type;
    }

    public void setPage_type(String page_type) {
        this.page_type = page_type;
    }

    public String getField_view_types() {
        return field_view_types;
    }

    public void setField_view_types(String field_view_types) {
        this.field_view_types = field_view_types;
    }
}
