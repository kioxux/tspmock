package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "字段库列表查询参数")
public class WorkColumnSearchParam implements Serializable {
    private static final long serialVersionUID = -1L;
    @ApiModelProperty(value = "字段主键")
    private String field_form_code;
    @ApiModelProperty(value = "选中字段主键")
    private String field_form_codes;
    @ApiModelProperty(value = "字段控件类型")
    private String field_view_types;
//    @ApiModelProperty(value = "备件类型")
//    private String[] typeIdsSearch;
//    @ApiModelProperty(value = "供应商")
//    private String[] suppliersSearch;
//    @ApiModelProperty(value = "起始数量")
//    private Integer startCountSearch;
//    @ApiModelProperty(value = "截止数量")
//    private Integer endCountSearch;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "工单模板主键,工单模板添加字段弹框查询附带")
    private String work_template_code;
//    @ApiModelProperty(value = "是否启用 1启用 0禁用")
//    private String isUseSearch;
//    @ApiModelProperty(value = "模块类型[bomIcon-备件图片,bomTop-顶部信息]")
//    private String sectionType;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getIds() {
//        return ids;
//    }
//
//    public void setIds(String ids) {
//        this.ids = ids;
//    }
//
//    public String[] getTypeIdsSearch() {
//        return typeIdsSearch;
//    }
//
//    public void setTypeIdsSearch(String[] typeIdsSearch) {
//        this.typeIdsSearch = typeIdsSearch;
//    }
//
//    public String[] getSuppliersSearch() {
//        return suppliersSearch;
//    }
//
//    public void setSuppliersSearch(String[] suppliersSearch) {
//        this.suppliersSearch = suppliersSearch;
//    }
//
//    public Integer getStartCountSearch() {
//        return startCountSearch;
//    }
//
//    public void setStartCountSearch(Integer startCountSearch) {
//        this.startCountSearch = startCountSearch;
//    }
//
//    public Integer getEndCountSearch() {
//        return endCountSearch;
//    }
//
//    public void setEndCountSearch(Integer endCountSearch) {
//        this.endCountSearch = endCountSearch;
//    }


    public String getField_form_code() {
        return field_form_code;
    }

    public void setField_form_code(String field_form_code) {
        this.field_form_code = field_form_code;
    }

    public String getField_form_codes() {
        return field_form_codes;
    }

    public void setField_form_codes(String field_form_codes) {
        this.field_form_codes = field_form_codes;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public String getField_view_types() {
        return field_view_types;
    }

    public void setField_view_types(String field_view_types) {
        this.field_view_types = field_view_types;
    }

    //    public String getIsUseSearch() {
//        return isUseSearch;
//    }
//
//    public void setIsUseSearch(String isUseSearch) {
//        this.isUseSearch = isUseSearch;
//    }
//
//    public String getSectionType() {
//        return sectionType;
//    }
//
//    public void setSectionType(String sectionType) {
//        this.sectionType = sectionType;
//    }
}
