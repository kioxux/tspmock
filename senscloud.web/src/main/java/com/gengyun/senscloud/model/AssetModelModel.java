package com.gengyun.senscloud.model;

import io.swagger.annotations.ApiModelProperty;

public class AssetModelModel {
    @ApiModelProperty(value = "编辑类型 modelIcon：更新设备型号图片，modelTop：更新设备型号头部名称和类型，modelBase：更新设备型号基础信息，modelInfo：更新设备型号自定义字段")
    private String sectionType;
    @ApiModelProperty(value = "id")
    private Integer id;
    @ApiModelProperty(value = "企业id")
    private Integer org_id;
    @ApiModelProperty(value = "ids")
    private String ids;
    @ApiModelProperty(value = "型号名称")
    private String model_name;
    @ApiModelProperty(value = "设备类型id")
    private Integer category_id;
    @ApiModelProperty(value = "图标id")
    private Integer icon;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "自定义字段:{'field_code':'字段id','field_name':'显示名称','field_remark':'备注','field_source':'数据来源','field_view_type':'显示控件','field_value':'选中值'}")
    private String properties;
    @ApiModelProperty(value = "关键字")
    private String keywordSearch;
    @ApiModelProperty(value = "设备类型筛选")
    private String categorySearch;
    @ApiModelProperty(value = "设备类型筛选")
    private String orgSearch;
    @ApiModelProperty(value = "附件文档id")
    private Integer file_id;
    @ApiModelProperty(value = "附件类型id")
    private Integer file_type;

    public Integer getFile_type() {
        return file_type;
    }

    public void setFile_type(Integer file_type) {
        this.file_type = file_type;
    }

    public void setOrgSearch(String orgSearch) {
        this.orgSearch = orgSearch;
    }

    public Integer getFile_id() {
        return file_id;
    }

    public void setFile_id(Integer file_id) {
        this.file_id = file_id;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public String getCategorySearch() {
        return categorySearch;
    }

    public void setCategorySearch(String categorySearch) {
        this.categorySearch = categorySearch;
    }

    public String getOrgSearch() {
        return orgSearch;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getKeywordSearch() {
        return keywordSearch;
    }

    public void setKeywordSearch(String keywordSearch) {
        this.keywordSearch = keywordSearch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel_name() {
        return model_name;
    }

    public Integer getOrg_id() {
        return org_id;
    }

    public void setOrg_id(Integer org_id) {
        this.org_id = org_id;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
