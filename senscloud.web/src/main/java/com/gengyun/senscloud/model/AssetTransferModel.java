package com.gengyun.senscloud.model;

import java.io.Serializable;

/**
 * @Author: Wudang Dong
 * @Description:设备迁移实体类POJO
 * @Date: Create in 上午 11:16 2019/4/30 0030
 */
public class AssetTransferModel implements Serializable {
//    private static final long serialVersionUID = 7711738419858711635L;
//    private String transfer_code;
//    private String position_id;
//    private String new_position_id;
//    private int status;
//    private String remark;
//    private String create_user_account;
//    private Timestamp create_time;
//    private boolean is_head;
//
//    private String schema_name;
//
//    private String strcode;
//    private String strname;
//    private String model_name;
//    private String asset_type;
//    private String supplier;
//    private String facilities_name;
//    private String new_facilities_name;
//    private String asset_status_name;
//    private String username;
//    private String taskId;
//    private String position_name;
//    private String new_position_name;
//    private String send_file_ids; //调出附件ID
//    private String receive_file_ids; //接受附件ID
//    private Object body_property;//工单属性字段
//    private String source_sub_code;//来源-设备盘点子表编码（设备盘点调整发起的调拨申请）
//
//    public String getTransfer_code() {
//        return transfer_code;
//    }
//
//    public void setTransfer_code(String transfer_code) {
//        this.transfer_code = transfer_code;
//    }
//
//    public String getPosition_id() {
//        return position_id;
//    }
//
//    public void setPosition_id(String position_id) {
//        this.position_id = position_id;
//    }
//
//    public String getNew_position_id() {
//        return new_position_id;
//    }
//
//    public void setNew_position_id(String new_position_id) {
//        this.new_position_id = new_position_id;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public String getAsset_type() {
//        return asset_type;
//    }
//
//    public void setAsset_type(String asset_type) {
//        this.asset_type = asset_type;
//    }
//
//    public String getSupplier() {
//        return supplier;
//    }
//
//    public void setSupplier(String supplier) {
//        this.supplier = supplier;
//    }
//
//    public String getFacilities_name() {
//        return facilities_name;
//    }
//
//    public void setFacilities_name(String facilities_name) {
//        this.facilities_name = facilities_name;
//    }
//
//    public String getAsset_status_name() {
//        return asset_status_name;
//    }
//
//    public void setAsset_status_name(String asset_status_name) {
//        this.asset_status_name = asset_status_name;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getModel_name() {
//        return model_name;
//    }
//
//    public void setModel_name(String model_name) {
//        this.model_name = model_name;
//    }
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public boolean isIs_head() {
//        return is_head;
//    }
//
//    public void setIs_head(boolean is_head) {
//        this.is_head = is_head;
//    }
//
//    public String getNew_facilities_name() {
//        return new_facilities_name;
//    }
//
//    public void setNew_facilities_name(String new_facilities_name) {
//        this.new_facilities_name = new_facilities_name;
//    }
//
//    public String getTaskId() {
//        return taskId;
//    }
//
//    public void setTaskId(String taskId) {
//        this.taskId = taskId;
//    }
//
//    public String getPosition_name() {
//        return position_name;
//    }
//
//    public void setPosition_name(String position_name) {
//        this.position_name = position_name;
//    }
//
//    public String getNew_position_name() {
//        return new_position_name;
//    }
//
//    public void setNew_position_name(String new_position_name) {
//        this.new_position_name = new_position_name;
//    }
//
//    public String getSend_file_ids() {
//        return send_file_ids;
//    }
//
//    public void setSend_file_ids(String send_file_ids) {
//        this.send_file_ids = send_file_ids;
//    }
//
//    public String getReceive_file_ids() {
//        return receive_file_ids;
//    }
//
//    public void setReceive_file_ids(String receive_file_ids) {
//        this.receive_file_ids = receive_file_ids;
//    }
//
//    public Object getBody_property() {
//        return body_property;
//    }
//
//    public void setBody_property(Object body_property) {
//        this.body_property = body_property;
//    }
//
//    public String getSource_sub_code() {
//        return source_sub_code;
//    }
//
//    public void setSource_sub_code(String source_sub_code) {
//        this.source_sub_code = source_sub_code;
//    }
}
