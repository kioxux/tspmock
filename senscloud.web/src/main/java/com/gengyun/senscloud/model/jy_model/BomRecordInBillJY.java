package com.gengyun.senscloud.model.jy_model;

import java.io.Serializable;

/**
 * Created by Dong_wudang on 2018/9/27.
 * 功能：备件使用记录接口，传递json转换对象
 */
public class BomRecordInBillJY implements Serializable {
//    private static final long serialVersionUID = -2313227918876217408L;
//    private String user_account;//用户账号
//    private String valid_key;//加密key
//    private String token;//token
//    private String bill_code;//类型
//    private String is_pass;//0：未通过；1：通过；
//    private String result_note;
//    private String client_customer_code;
//    private List<BomBillJY> bom_list;
//
//    public String getUser_account() {
//        return user_account;
//    }
//
//    public void setUser_account(String user_account) {
//        this.user_account = user_account;
//    }
//
//    public String getValid_key() {
//        return valid_key;
//    }
//
//    public void setValid_key(String valid_key) {
//        this.valid_key = valid_key;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token) {
//        this.token = token;
//    }
//
//    public String getClient_customer_code() {
//        return client_customer_code;
//    }
//
//    public void setClient_customer_code(String client_customer_code) {
//        this.client_customer_code = client_customer_code;
//    }
//
//    public String getBill_code() {
//        return bill_code;
//    }
//
//    public void setBill_code(String bill_code) {
//        this.bill_code = bill_code;
//    }
//
//    public List<BomBillJY> getBom_list() {
//        return bom_list;
//    }
//
//    public void setBom_list(List<BomBillJY> bom_list) {
//        this.bom_list = bom_list;
//    }
//
//    public String getIs_pass() {
//        return is_pass;
//    }
//
//    public void setIs_pass(String is_pass) {
//        this.is_pass = is_pass;
//    }
//
//    public String getResult_note() {
//        return result_note;
//    }
//
//    public void setResult_note(String result_note) {
//        this.result_note = result_note;
//    }
}
