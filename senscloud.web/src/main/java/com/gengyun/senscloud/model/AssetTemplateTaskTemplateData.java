package com.gengyun.senscloud.model;

import java.io.Serializable;

//设备模板对应工单类型的任务模板
//用于进行点巡检提交

public class AssetTemplateTaskTemplateData implements Serializable {
//    private static final long serialVersionUID = 4567168601191258949L;
//    /**
//     * 设备模板
//     */
//    private String asset_template_code;
//    /**
//     * 工单类型id
//     */
//    private int work_type_id;
//    /**
//     * 工单类型名称
//     */
//    private String work_type_name;
//    /**
//     * 任务模板编码
//     */
//    private String task_template_code;
//    /**
//     * 任务模板名称
//     */
//    private String task_template_name;
//    /**
//     * 创建时间
//     */
//    private Timestamp create_time;
//    /**
//     * 创建人
//     */
//    private String create_user_account;
//
//    public String getAsset_template_code() {
//        return asset_template_code;
//    }
//
//    public void setAsset_template_code(String asset_template_code) {
//        this.asset_template_code = asset_template_code;
//    }
//
//    public int getWork_type_id() {
//        return work_type_id;
//    }
//
//    public void setWork_type_id(int work_type_id) {
//        this.work_type_id = work_type_id;
//    }
//
//    public String getWork_type_name() {
//        return work_type_name;
//    }
//
//    public void setWork_type_name(String work_type_name) {
//        this.work_type_name = work_type_name;
//    }
//
//    public String getTask_template_code() {
//        return task_template_code;
//    }
//
//    public void setTask_template_code(String task_template_code) {
//        this.task_template_code = task_template_code;
//    }
//
//    public String getTask_template_name() {
//        return task_template_name;
//    }
//
//    public void setTask_template_name(String task_template_name) {
//        this.task_template_name = task_template_name;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getCreate_user_account() {
//        return create_user_account;
//    }
//
//    public void setCreate_user_account(String create_user_account) {
//        this.create_user_account = create_user_account;
//    }
}
