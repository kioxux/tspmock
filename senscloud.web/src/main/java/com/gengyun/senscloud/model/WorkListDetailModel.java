package com.gengyun.senscloud.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 功能：工单详情
 * Created by Dong wudang on 2018/11/3.
 */
public class WorkListDetailModel implements Serializable {
    private static final long serialVersionUID = -1;
    private String sub_work_code;//工单号
    private String work_code;//所属工单号
    private Integer work_type_id;//工单类型id
    private String work_template_code;//工单模板编码
    private Integer relation_type;//关联对象
    private String relation_id;//关联对象编码
    private String title;//主题
    private String problem_note;//描述（问题）
    private String problem_img;//照片（问题）
    private Integer running_status_id;//设备运行状态
    private Integer fault_type_id;//故障类型
    private Integer repair_type_id;//维修类别
    private Integer priority_level;//紧急程度
    private Integer status;//状态
    private String remark;//备注
    private String from_code;//来源单号
    private Integer waiting;//0：默认；10:备件申领中，单据冻结；20：备件申领拒绝；60：备件申请通过；
    private String bom_app_result;//备件申领结果
    private String receive_user_id;//接收人
    private Integer fault_number;//问题个数
    private String status_name;//接收人
    private String receive_name;
    private int is_main;

    /*扩展字段*/

    private String user_name;
    private String phone;
    private String asset_name;
    private String works_title;
    private String type_name;
    private Integer business_type_id;
    private String flow_code;//模版code
    private Timestamp occur_time;//发生时间
    private Timestamp deadline_time;//截止时间
    private Timestamp create_time;//创建时间
    private Timestamp receive_time;//接受时间
    private Timestamp distribute_time;//分配时间（最后一次时间，跟负责人修改同步更新）
    private Timestamp begin_time;//开始时间
    private Timestamp finished_time;//完成时间
    private Timestamp plan_arrive_time;//预计到达时间
    private Timestamp plan_begin_time;//计划开工时间

    private String finished_time_interval; // 完成时效

    public String getSub_work_code() {
        return sub_work_code;
    }

    public void setSub_work_code(String sub_work_code) {
        this.sub_work_code = sub_work_code;
    }

    public String getWork_code() {
        return work_code;
    }

    public void setWork_code(String work_code) {
        this.work_code = work_code;
    }

    public Integer getWork_type_id() {
        return work_type_id;
    }

    public void setWork_type_id(Integer work_type_id) {
        this.work_type_id = work_type_id;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public Integer getRelation_type() {
        return relation_type;
    }

    public void setRelation_type(Integer relation_type) {
        this.relation_type = relation_type;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProblem_note() {
        return problem_note;
    }

    public void setProblem_note(String problem_note) {
        this.problem_note = problem_note;
    }

    public String getProblem_img() {
        return problem_img;
    }

    public void setProblem_img(String problem_img) {
        this.problem_img = problem_img;
    }

    public Integer getRunning_status_id() {
        return running_status_id;
    }

    public void setRunning_status_id(Integer running_status_id) {
        this.running_status_id = running_status_id;
    }

    public Integer getFault_type_id() {
        return fault_type_id;
    }

    public void setFault_type_id(Integer fault_type_id) {
        this.fault_type_id = fault_type_id;
    }

    public Integer getRepair_type_id() {
        return repair_type_id;
    }

    public void setRepair_type_id(Integer repair_type_id) {
        this.repair_type_id = repair_type_id;
    }

    public Integer getPriority_level() {
        return priority_level;
    }

    public void setPriority_level(Integer priority_level) {
        this.priority_level = priority_level;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFrom_code() {
        return from_code;
    }

    public void setFrom_code(String from_code) {
        this.from_code = from_code;
    }

    public Integer getWaiting() {
        return waiting;
    }

    public void setWaiting(Integer waiting) {
        this.waiting = waiting;
    }

    public String getBom_app_result() {
        return bom_app_result;
    }

    public void setBom_app_result(String bom_app_result) {
        this.bom_app_result = bom_app_result;
    }

    public String getReceive_user_id() {
        return receive_user_id;
    }

    public void setReceive_user_id(String receive_user_id) {
        this.receive_user_id = receive_user_id;
    }

    public Integer getFault_number() {
        return fault_number;
    }

    public void setFault_number(Integer fault_number) {
        this.fault_number = fault_number;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getReceive_name() {
        return receive_name;
    }

    public void setReceive_name(String receive_name) {
        this.receive_name = receive_name;
    }

    public int getIs_main() {
        return is_main;
    }

    public void setIs_main(int is_main) {
        this.is_main = is_main;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAsset_name() {
        return asset_name;
    }

    public void setAsset_name(String asset_name) {
        this.asset_name = asset_name;
    }

    public String getWorks_title() {
        return works_title;
    }

    public void setWorks_title(String works_title) {
        this.works_title = works_title;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public Integer getBusiness_type_id() {
        return business_type_id;
    }

    public void setBusiness_type_id(Integer business_type_id) {
        this.business_type_id = business_type_id;
    }

    public String getFlow_code() {
        return flow_code;
    }

    public void setFlow_code(String flow_code) {
        this.flow_code = flow_code;
    }

    public Timestamp getOccur_time() {
        return occur_time;
    }

    public void setOccur_time(Timestamp occur_time) {
        this.occur_time = occur_time;
    }

    public Timestamp getDeadline_time() {
        return deadline_time;
    }

    public void setDeadline_time(Timestamp deadline_time) {
        this.deadline_time = deadline_time;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Timestamp getReceive_time() {
        return receive_time;
    }

    public void setReceive_time(Timestamp receive_time) {
        this.receive_time = receive_time;
    }

    public Timestamp getDistribute_time() {
        return distribute_time;
    }

    public void setDistribute_time(Timestamp distribute_time) {
        this.distribute_time = distribute_time;
    }

    public Timestamp getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(Timestamp begin_time) {
        this.begin_time = begin_time;
    }

    public Timestamp getFinished_time() {
        return finished_time;
    }

    public void setFinished_time(Timestamp finished_time) {
        this.finished_time = finished_time;
    }

    public Timestamp getPlan_arrive_time() {
        return plan_arrive_time;
    }

    public void setPlan_arrive_time(Timestamp plan_arrive_time) {
        this.plan_arrive_time = plan_arrive_time;
    }

    public Timestamp getPlan_begin_time() {
        return plan_begin_time;
    }

    public void setPlan_begin_time(Timestamp plan_begin_time) {
        this.plan_begin_time = plan_begin_time;
    }

    public String getFinished_time_interval() {
        return finished_time_interval;
    }

    public void setFinished_time_interval(String finished_time_interval) {
        this.finished_time_interval = finished_time_interval;
    }
}
