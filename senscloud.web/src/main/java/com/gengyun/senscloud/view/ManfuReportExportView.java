package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.config.SpringContextHolder;
//import com.gengyun.senscloud.mapper.TaskTemplateMapper;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.service.system.SystemConfigService;
//import com.gengyun.senscloud.util.HttpRequestUtils;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ooxml.POIXMLDocumentPart;
//import org.apache.poi.ss.usermodel.*;
//import org.apache.poi.ss.util.CellRangeAddress;
//import org.apache.poi.xssf.usermodel.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.net.URLEncoder;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * 曼孚报告导出视图解析器
// */
public class ManfuReportExportView{
//public class ManfuReportExportView extends AbstractXlsxStreamingView {
//
//    private static final Logger logger = LoggerFactory.getLogger(ManfuReportExportView.class);
//
//    /**
//     * 模板文件路径-系统配置名
//     */
//    private String SYSTEM_CONFIG_NAME = "work_item_export";
//
//    /**
//     * 导出文件名
//     */
//    private String fileName = "manfu_report_";
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    TaskTemplateMapper taskTemplateMapper;
//
//    /**
//     * Application-provided subclasses must implement this method to populate
//     * the Excel workbook document, given the model.
//     *
//     * @param model    the model Map
//     * @param workbook the Excel workbook to populate
//     * @param request  in case we need locale etc. Shouldn't look at attributes.
//     * @param response in case we need to set cookies. Shouldn't write to it.
//     */
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        OutputStream output = null;
//        try {
//            //读取模板文件路径
//            String excelPath = getExcelPathBySystemConfig();
//            //获取模板文件
//            workbook = getWorkBookFileHandle(excelPath);
//            //业务数据填充
//            dataHandle(model, workbook);
//            //设置响应头
//            setResponseHead(request, response, fileName + new SimpleDateFormat(Constants.DATE_FMT_SS).format(new Date()) + ".xlsx");
//            output = response.getOutputStream();
//            //导出excel
//            workbook.write(output);
//        } catch (Exception e) {
//            logger.error("", e);
//        } finally {
//            if (output != null)
//                output.close();
//        }
//    }
//
//    /**
//     * 从系统配置表读取模板文件路径
//     *
//     * @return
//     */
//    private String getExcelPathBySystemConfig() {
//        Company company = AuthService.getCompany(HttpRequestUtils.getRequest());
//        SystemConfigService systemConfigService = SpringContextHolder.getBean(SystemConfigService.class);
//        SystemConfigData data = systemConfigService.getSystemConfigData(company.getSchema_name(), SYSTEM_CONFIG_NAME);
//        if (data != null)
//            return data.getSettingValue();
//
//        return null;
//    }
//
//    /**
//     * 表单数据填充
//     *
//     * @param model
//     * @param workbook
//     */
//    private void dataHandle(Map<String, Object> model, Workbook workbook) {
//        Sheet sheet = workbook.getSheetAt(0);//取第一个sheet
//        sheet.getRow(7).getCell(14).setCellValue(new SimpleDateFormat(Constants.DATE_FMT);.format(new Date()));//Date项，填上日期
//        List<Row> logoRows = getRowlist(sheet, 0, 8);//取页眉logo行；
//        List<Row> pageRows = getRowlist(sheet, 78, 79);//取页尾页码行；
//
//        List<Map<String, Object>> itemList1 = (List<Map<String, Object>>) model.get("template1");
//        List<Map<String, Object>> itemList2 = (List<Map<String, Object>>) model.get("template2");
//        List<Map<String, Object>> itemList3 = (List<Map<String, Object>>) model.get("template3");
//        Map<String, Integer> map = new HashMap<>();
//        map.put("dataCount", 0);
//        int itemDataRowIndex1 = 19;//任务项1 数据起始行索引
//        itemDataRowIndex1 = insertRowData(workbook, map, itemList1, sheet, itemDataRowIndex1);
//        int itemDataRowIndex2 = itemDataRowIndex1 + 4;//任务项2 数据起始行索引
//        itemDataRowIndex2 = insertRowData(workbook, map, itemList2, sheet, itemDataRowIndex2);
//        int itemDataRowIndex3 = itemDataRowIndex2 + 9;//任务项3 数据起始行索引
//        insertRowData(workbook, map, itemList3, sheet, itemDataRowIndex3);
//
//        //补上页眉、页尾
//        insertLogoAndPageRows(workbook, sheet, logoRows, pageRows);
//    }
//
//    /**
//     * 补充页眉页尾
//     *
//     * @param sheet
//     * @param logoRows
//     * @param pageRows
//     */
//    private void insertLogoAndPageRows(Workbook workbook, Sheet sheet, List<Row> logoRows, List<Row> pageRows) {
//        if (logoRows == null || logoRows.size() == 0 || pageRows == null || pageRows.size() == 0)
//            return;
//
//        int lastRowNum = sheet.getLastRowNum();
//        int pageSize = 47;//每页显示行数（注意：模板文件row的高度调整后，每页条数需要调整，否则影响计算页眉、页尾位置）
//        int pageNum = 2;//记录总页数，起始为2页
//        int index = 54;//第一页添加页尾的位置索引（注意：模板文件调整后，该索引需要调整）
//        index = insertLogoAndPage(workbook, sheet, logoRows, pageRows, index);
//        while (lastRowNum > pageSize + index - ((pageNum - 1) * (logoRows.size() + pageRows.size()))) {
//            index = insertLogoAndPage(workbook, sheet, logoRows, pageRows, index + pageSize);
//            pageNum++;
//        }
//        //填上页码数字
//        int pageIndex = 1;
//        int pageStr = 55;//第一个page页码项位置索引
//        for (int i = 0; i < pageNum; i++) {
//            Row row;
//            if (pageNum == (i + 1)) {
//                row = sheet.getRow(sheet.getLastRowNum() - 1);
//            } else {
//                row = sheet.getRow(pageStr + (i * (pageSize + logoRows.size() + pageRows.size())));
//            }
//            Cell cell = row.getCell(10);
//            cell.setCellValue(new StringBuffer(cell.getStringCellValue()).insert(22, pageNum).insert(11, pageIndex++).toString());
//        }
//    }
//
//    /**
//     * 插入logo、页码
//     *
//     * @param workbook
//     * @param sheet
//     * @param logoRows
//     * @param pageRows
//     * @param index
//     * @return
//     */
//    private int insertLogoAndPage(Workbook workbook, Sheet sheet, List<Row> logoRows, List<Row> pageRows, int index) {
//        insertRow(sheet, index, logoRows.size() + pageRows.size());//插入行
//        for (int i = 0; i < pageRows.size(); i++) {
//            Row row = sheet.createRow(index++);//在新增的空白行创建新的row
//            copyRow(workbook, sheet, pageRows.get(i), row);//拷贝模板行格式到新增行
//        }
//        for (int i = 0; i < logoRows.size(); i++) {
//            Row row = sheet.createRow(index++);//在新增的空白行创建新的row
//            copyRow(workbook, sheet, logoRows.get(i), row);//拷贝模板行格式到新增行
//        }
//        return index;
//    }
//
//    /**
//     * 获取指定行的row
//     *
//     * @param sheet
//     * @param end
//     * @return
//     */
//    private List<Row> getRowlist(Sheet sheet, int start, int end) {
//        List<Row> rowList = new ArrayList<>();
//        for (; start <= end; start++) {
//            rowList.add(sheet.getRow(start));
//        }
//        return rowList;
//    }
//
//    /**
//     * 结果分析-对勾；叉
//     *
//     * @param value
//     * @return
//     */
//    private String getBooleanValue(Object value, int result_type) {
//        if (result_type == 1) {
//            int[] code = {0x2713, 0x2717};//1、对勾；2、叉
//            if (value != null && value instanceof Boolean && (Boolean) value == true)
//                return new String(code, 0, 1);
//
//            return new String(code, 1, 1);
//        }
//        return "";
//    }
//
//    /**
//     * 插入空白行
//     *
//     * @param sheet
//     * @param rowIndex    插入位置行索引
//     * @param insertCount 插入几行
//     * @return
//     */
//    private void insertRow(Sheet sheet, Integer rowIndex, int insertCount) {
//        if (sheet.getRow(rowIndex) != null) {
//            //获取原始的合并单元格集合
//            List<CellRangeAddress> originMerged = sheet.getMergedRegions();
//            int lastRowNo = sheet.getLastRowNum();
//            sheet.shiftRows(rowIndex, lastRowNo, insertCount, true, false);//索引行到最后一行的rows往下移动一行
//
//            //解决shiftRows后，合并单元格丢失的问题
//            for (CellRangeAddress cellRangeAddress : originMerged) {
//                //这里的8是插入行的index，表示这行之后才重新合并
//                if (cellRangeAddress.getFirstRow() > (rowIndex + insertCount - 1)) {
//                    //你插入了几行就加几，我这里插入了一行，加1
//                    int firstRow = cellRangeAddress.getFirstRow() + insertCount;
//                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(firstRow, (firstRow + (cellRangeAddress
//                            .getLastRow() - cellRangeAddress.getFirstRow())), cellRangeAddress.getFirstColumn(),
//                            cellRangeAddress.getLastColumn());
//                    sheet.addMergedRegion(newCellRangeAddress);
//                }
//            }
//            //解决shiftRows后，图片不同步移动的问题
//            for (POIXMLDocumentPart dr : ((XSSFSheet) sheet).getRelations()) {
//                if (dr instanceof XSSFDrawing) {
//                    XSSFDrawing drawing = (XSSFDrawing) dr;
//                    List<XSSFShape> shapes = drawing.getShapes();
//                    for (XSSFShape shape : shapes) {
//                        XSSFPicture pic = (XSSFPicture) shape;
//                        XSSFClientAnchor anchor = pic.getClientAnchor();
//                        if (anchor.getRow1() >= rowIndex - 1) {//从插入行到最后一行的所有图片往下移动一行
//                            anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
//                            anchor.setRow1(anchor.getRow1() + insertCount);
//                            anchor.setRow2(anchor.getRow2() + insertCount);
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 复制行
//     *
//     * @param srcRow
//     * @param desRow
//     */
//    private void copyRow(Workbook workbook, Sheet sheet, Row srcRow, Row desRow) {
//        Iterator<Cell> it = srcRow.cellIterator();
//        while (it.hasNext()) {
//            Cell srcCell = it.next();
//            Cell desCell = desRow.createCell(srcCell.getColumnIndex());
//            copyCell(srcCell, desCell);
//        }
//        desRow.setHeight(srcRow.getHeight());
//        desRow.setRowStyle(srcRow.getRowStyle());
//        //复制单元格合并
//        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
//            CellRangeAddress cellRangeAddress = sheet.getMergedRegion(i);
//            if (cellRangeAddress.getFirstRow() == srcRow.getRowNum()) {
//                CellRangeAddress newCellRangeAddress = new CellRangeAddress(desRow.getRowNum(), (desRow.getRowNum() +
//                        (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow())), cellRangeAddress
//                        .getFirstColumn(), cellRangeAddress.getLastColumn());
//                sheet.addMergedRegionUnsafe(newCellRangeAddress);
//            }
//        }
//        //复制图片
//        for (POIXMLDocumentPart dr : ((XSSFSheet) sheet).getRelations()) {
//            if (dr instanceof XSSFDrawing) {
//                XSSFDrawing drawing = (XSSFDrawing) dr;
//                List<XSSFShape> shapes = drawing.getShapes();
//                for (XSSFShape shape : shapes) {
//                    XSSFPicture pic = (XSSFPicture) shape;
//                    XSSFClientAnchor anchor = pic.getClientAnchor();
//                    if (anchor.getRow1() == srcRow.getRowNum()) {
//                        XSSFClientAnchor newAnchor = new XSSFClientAnchor(anchor.getDx1(), anchor.getDy1(), anchor.getDx2(), anchor.getDy2(),
//                                anchor.getCol1(), anchor.getRow1() + (desRow.getRowNum() - srcRow.getRowNum()),
//                                anchor.getCol2(), anchor.getRow2() + (desRow.getRowNum() - srcRow.getRowNum()));
//                        newAnchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
//                        drawing.createPicture(newAnchor, workbook.addPicture(pic.getPictureData().getData(), HSSFWorkbook.PICTURE_TYPE_JPEG));
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 复制单元格(样式、评论、内容等，完全拷贝)
//     */
//    public static void copyCell(Cell srcCell, Cell desCell) {
//        //复制样式
//        desCell.setCellStyle(srcCell.getCellStyle());
//        //复制评论
//        if (srcCell.getCellComment() != null) {
//            desCell.setCellComment(srcCell.getCellComment());
//        }
//        //复制内容
//        desCell.setCellType(srcCell.getCellTypeEnum());
//        switch (srcCell.getCellTypeEnum()) {
//            case STRING:
//                desCell.setCellValue(srcCell.getStringCellValue());
//                break;
//            case NUMERIC:
//                desCell.setCellValue(srcCell.getNumericCellValue());
//                break;
//            case FORMULA:
//                desCell.setCellFormula(srcCell.getCellFormula());
//                break;
//            case BOOLEAN:
//                desCell.setCellValue(srcCell.getBooleanCellValue());
//                break;
//            case ERROR:
//                desCell.setCellValue(srcCell.getErrorCellValue());
//                break;
//            case BLANK:
//                //nothing to do
//                break;
//            default:
//                break;
//        }
//    }
//
//    /**
//     * 得到一个已有的工作薄的POI对象
//     *
//     * @return
//     */
//    private XSSFWorkbook getWorkBookFileHandle(String excelPath) {
//        XSSFWorkbook wb = null;
//        FileInputStream fis = null;
//        File f = new File(excelPath);
//        try {
//            if (f != null) {
//                fis = new FileInputStream(f);
//                wb = new XSSFWorkbook(fis);
//            }
//        } catch (Exception e) {
//            logger.error("", e);
//            return null;
//        } finally {
//            if (fis != null) {
//                try {
//                    fis.close();
//                } catch (IOException e) {
//                    logger.error("", e);
//                }
//            }
//        }
//        return wb;
//    }
//
//    /**
//     * 设置响应头
//     *
//     * @param request
//     * @param response
//     * @param fileName
//     * @throws IOException
//     */
//    private void setResponseHead(HttpServletRequest request, HttpServletResponse response, String fileName) throws IOException {
//        String userAgent = request.getHeader("user-agent").toLowerCase();
//        if (userAgent != null) {
//            if (userAgent.contains("firefox")) {
//                // firefox有默认的备用字符集是西欧字符集
//                fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
//            } else if (userAgent.contains("webkit") && (userAgent.contains("chrome") || userAgent.contains("safari"))) {
//                // webkit核心的浏览器,主流的有chrome,safari,360
//                fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
//            } else {
//                // 新老版本的IE都可直接用URL编码工具编码后输出正确的名称,无乱码
//                fileName = URLEncoder.encode(fileName, "UTF-8");
//            }
//        }
//        //响应头信息
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/ms-excel; charset=UTF-8");
//        response.setHeader("Content-disposition", "attachment; filename=" + fileName);
//    }
//
//    /**
//     * 在指定索引处插入数据
//     *
//     * @param map              索引参数集合
//     * @param itemList         数据列表
//     * @param sheet
//     * @param itemDataRowIndex 任务项数据起始行索引
//     * @return
//     */
//    int insertRowData(Workbook workbook, Map<String, Integer> map, List<Map<String, Object>> itemList, Sheet sheet, int itemDataRowIndex) {
//        if (itemList != null && !itemList.isEmpty()) {
//            Row originalRow = sheet.getRow(itemDataRowIndex);
//            for (int i = 0; i < itemList.size(); i++) {
//                Map<String, Object> item = itemList.get(i);
//                Row row;
//                if (i > 0) {
//                    insertRow(sheet, ++itemDataRowIndex, 1);//插入行
//                    row = sheet.createRow(itemDataRowIndex);//在新增的空白行创建新的row
//                    copyRow(workbook, sheet, originalRow, row);//拷贝模板行格式到新增行
//                } else {
//                    row = originalRow;
//                }
//                int dataCount = map.get("dataCount");
//                try {
//                    row.getCell(0).setCellValue(++dataCount);
//                    row.getCell(1).setCellValue((String) item.get("task_item_name"));
//                } catch (Exception e) {
//                    logger.error("", e);
//                }
//                map.put("dataCount", dataCount);
//            }
//        }
//        return itemDataRowIndex;
//    }

}