package com.gengyun.senscloud.view;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.util.RegexUtil;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import com.gengyun.senscloud.service.system.SelectOptionService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class UserConfigDataExportView extends AbstractXlsxStreamingView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            String fileName = "user_list_export_" + sdf.format(new Date()) + ".xlsx";
            response.setHeader("Content-Disposition", "attachment; filename= " + fileName);
            List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("userConfigDataList");
            SelectOptionService selectOptionService = (SelectOptionService) model.get("selectOptionService");

            // create excel xls sheet
            Sheet sheet = workbook.createSheet("人员");
            sheet.setDefaultColumnWidth(24);
            // create header row
            Row header = sheet.createRow(0);
            int i = 0;
            String[] titleFields = {"姓名", "账号", "工号", "用户电话", "部门", "邮箱","NFC"};
            for (int k = 0; k < titleFields.length; k++) {
                String title = titleFields[k];
                header.createCell(i).setCellValue(title);
                i++;
            }

            int rowCount = 1;
            for (Map<String, Object> data : list) {
                Row courseRow = sheet.createRow(rowCount++);
                courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("user_name"),""));
                courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(data.get("account"),""));
                courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(data.get("user_code"),""));
                courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(data.get("mobile"),""));
                courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(data.get("groupPositionName"),""));
                courseRow.createCell(5).setCellValue(RegexUtil.optStrOrVal(data.get("email"),""));
                courseRow.createCell(6).setCellValue(RegexUtil.optStrOrVal(data.get("nfc_code"),""));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
