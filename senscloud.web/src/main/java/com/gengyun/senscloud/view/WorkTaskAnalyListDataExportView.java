package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class WorkTaskAnalyListDataExportView{
//public class WorkTaskAnalyListDataExportView extends AbstractXlsxStreamingView {
//
//    private static final Logger logger = LoggerFactory.getLogger(WorkTaskAnalyListDataExportView.class);
//
//    /**
//     * Application-provided subclasses must implement this method to populate
//     * the Excel workbook document, given the model.
//     *
//     * @param model    the model Map
//     * @param workbook the Excel workbook to populate
//     * @param request  in case we need locale etc. Shouldn't look at attributes.
//     * @param response in case we need to set cookies. Shouldn't write to it.
//     */
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "work_task_analy_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("workTaskAnalyList");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String taskTitle = (String)model.get("taskTitle");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.WORK_TASK);//工作任务
//
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(sheetName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WORK_TASK));//工作任务
//            header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.USER_GROUP));//用户组
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.STAFF_K));//员工
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.JOB_ID));//工号
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.JOB_ROLE));//职务角色
//            header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PROCESSING_A));//处理中
//            header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.COM_QUA));//已完成数量
//            header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.DELAY_SUBMISSION));//延迟提交
//            header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.CERTIFICATE)+selectOptionService.getLanguageInfo(LangConstant.QTY_A));//合格数量
//            header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NOT_CERTIFICATE)+selectOptionService.getLanguageInfo(LangConstant.QTY_A));//不合格数量
//            header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.QUALIFICATION_RATE));//合格率
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                for (Map<String, Object> data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(taskTitle);
//                    courseRow.createCell(1).setCellValue((String)data.get("group_name"));
//                    courseRow.createCell(2).setCellValue((String)data.get("username"));
//                    courseRow.createCell(3).setCellValue((String)data.get("user_code"));
//                    courseRow.createCell(4).setCellValue((String)data.get("roles_name"));
//                    courseRow.createCell(5).setCellValue(String.valueOf(data.get("ongoing_times")));
//                    BigDecimal finishedTimes = new BigDecimal(String.valueOf(data.get("finished_times")));
//                    courseRow.createCell(6).setCellValue(finishedTimes.toString());
//                    courseRow.createCell(7).setCellValue(String.valueOf(data.get("delay_times")));
//                    BigDecimal qualifiedTimes = new BigDecimal(String.valueOf(data.get("qualified_times")));
//                    courseRow.createCell(8).setCellValue(qualifiedTimes.toString());
//                    courseRow.createCell(9).setCellValue(String.valueOf(data.get("unqualified_times")));
//                    BigDecimal qualificationRate = BigDecimal.ZERO;
//                    if(finishedTimes.compareTo(BigDecimal.ZERO) > 0){
//                        qualificationRate = qualifiedTimes.multiply(new BigDecimal(100)).divide(finishedTimes, 2, BigDecimal.ROUND_HALF_UP);
//                    }
//                    courseRow.createCell(10).setCellValue(qualificationRate.toString()+"%");
//                }
//            }
//        } catch (Exception ex) {
//            logger.error("", ex);
//        }
//    }
}
