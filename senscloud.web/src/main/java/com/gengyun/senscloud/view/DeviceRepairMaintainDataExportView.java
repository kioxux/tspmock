package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysFacilityRepairMaintainResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
public class DeviceRepairMaintainDataExportView {
//public class DeviceRepairMaintainDataExportView extends AbstractXlsxStreamingView {
//
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "device_repair_export_" + sdf.format(new Date()) + ".xlsx";
//
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<AnalysFacilityRepairMaintainResult> list = (List<AnalysFacilityRepairMaintainResult>) model.get("repairMaintainAnalys");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_REPAIR_DATA));
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_C));
//                header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_A));
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.DEVICE_QUANT));
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.REPAIR_MINUTE));
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_NOF));
//                header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_TOTAL_FAIL_MIN));
//                header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_RUN_REPAIR_MIN));
//                header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_TOTLA_TIMES));
//                header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_REPIAR_TOTAL_TIME));
//                header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.AVERAGE_MAINTENANCE_LIMIT));
//                header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NUM_MC));
//                header.createCell(11).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MAIN_ALL_MIN));
//                header.createCell(12).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OVER_MAIN_TIMES));
//                header.createCell(13).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_ELECT_NUM));
//                header.createCell(14).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_ELECT_TOTAL_TIME));
//                header.createCell(15).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MOTOR_FAIL));
//                header.createCell(16).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MOTOR_FAIL_TIME));
//                header.createCell(17).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BELT_FAIL));
//                header.createCell(18).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BELT_FAIL_TIME));
//                header.createCell(19).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_DRUM_FAIL));
//                header.createCell(20).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_DRUM_FAIL_TIME));
//                header.createCell(21).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BEARING_FAIL));
//                header.createCell(22).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BEARING_FAIL_TIME));
//                header.createCell(23).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OTHER_FAIL));
//                header.createCell(24).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OTHER_FAIL_TIME));
//                header.createCell(25).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_EFFEC_MAIN));
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (AnalysFacilityRepairMaintainResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getAnalysName());
//                    courseRow.createCell(1).setCellValue(data.getStrname());
//                    courseRow.createCell(2).setCellValue(data.getAsset_count());
//                    courseRow.createCell(3).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalRepairMinutePreTime())));
//                    courseRow.createCell(4).setCellValue(data.getRepairTimes());
//                    courseRow.createCell(5).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalRepairMinute())));
//                    courseRow.createCell(6).setCellValue(Double.parseDouble(decimalFormat.format(data.getRunTimeRepairMinutePreTime())));
//                    courseRow.createCell(7).setCellValue(data.getRunTimeRepairTimes());
//                    courseRow.createCell(8).setCellValue(Double.parseDouble(decimalFormat.format(data.getRunTimeTotalRepairMinute())));
//                    courseRow.createCell(9).setCellValue(Double.parseDouble(decimalFormat.format(data.getMaintainMinutePreTime())));
//                    courseRow.createCell(10).setCellValue(data.getFinishedMaintainTimes());
//                    courseRow.createCell(11).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalMaintainMinute())));
//                    courseRow.createCell(12).setCellValue(data.getNoFinishedMaintainTimes());
//                    courseRow.createCell(13).setCellValue(data.getDevice_fault_times());
//                    courseRow.createCell(14).setCellValue(Double.parseDouble(decimalFormat.format(data.getDevice_fault_minutes())));
//                    courseRow.createCell(15).setCellValue(data.getEngine_fault_times());
//                    courseRow.createCell(16).setCellValue(Double.parseDouble(decimalFormat.format(data.getEngine_fault_minutes())));
//                    courseRow.createCell(17).setCellValue(data.getBelt_fault_times());
//                    courseRow.createCell(18).setCellValue(Double.parseDouble(decimalFormat.format(data.getBelt_fault_minutes())));
//                    courseRow.createCell(19).setCellValue(data.getRoll_fault_times());
//                    courseRow.createCell(20).setCellValue(Double.parseDouble(decimalFormat.format(data.getRoll_fault_minutes())));
//                    courseRow.createCell(21).setCellValue(data.getBearing_fault_times());
//                    courseRow.createCell(22).setCellValue(Double.parseDouble(decimalFormat.format(data.getBearing_fault_minutes())));
//                    courseRow.createCell(23).setCellValue(data.getOther_fault_times());
//                    courseRow.createCell(24).setCellValue(Double.parseDouble(decimalFormat.format(data.getOther_fault_minutes())));
//                    courseRow.createCell(25).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalMaintainOfRepairRate() * 100)));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
