package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
///**
// * BomDataExportView class 备件导出模板类
// *
// * @author Zys
// * @date 2020/03/19
// */
public class BomInventoryDataExportView{
//public class BomInventoryDataExportView extends AbstractXlsxStreamingView {
//
//    /**
//     * 初始化备件导出模板类
//     *
//     * @param model
//     * @param workbook
//     * @param request
//     * @param response
//     * @return
//     */
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "bom_inventory_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<Map<String,Object>> list = (List<Map<String,Object>>) model.get("bomList");
//            SelectOptionService selectOptionService = (SelectOptionService) model.get("selectOptionService");
//            //sheet页名称
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.LIST_D);
//            if (list != null && list.size() > 0) {
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                Row header = sheet.createRow(0);
//                // 备件编码
//                header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PART_NUM ));
//                // 备件名称
//                header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PART_NAME));
//                //备件型号
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PART_MODEL));
//                //物料编码
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PART_CODE));
//                //备件类型
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PART_CATEGORY));
//                //单位
//                header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.UNIT_U));
//                //所属库房
//                header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WAR_A));
//                //库存数量
//                header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.STOCK_QUANTITY));
//                //最小库存
//                header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MIN_INV));
//                //最大库存
//                header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAX_INV));
//                //所属组
//                header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SUBORDINATE_GROUP));
//                int rowCount = 1;
//                for (Map<String,Object> data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(String.valueOf(data.get("bom_code")));
//                    courseRow.createCell(1).setCellValue(String.valueOf(data.get("bom_name")));
//                    courseRow.createCell(2).setCellValue(String.valueOf(data.get("bom_model")));
//                    courseRow.createCell(3).setCellValue(String.valueOf(data.get("material_code")));
//                    courseRow.createCell(4).setCellValue(String.valueOf(data.get("type_name")));
//                    courseRow.createCell(5).setCellValue(String.valueOf(data.get("unit_name")));
//                    courseRow.createCell(6).setCellValue(String.valueOf(data.get("stock_name")));
//                    courseRow.createCell(7).setCellValue(String.valueOf(data.get("quantity")));
//                    courseRow.createCell(8).setCellValue(String.valueOf(data.get("security_quantity")));
//                    courseRow.createCell(9).setCellValue(String.valueOf(data.get("max_security_quantity")));
//                    courseRow.createCell(10).setCellValue(String.valueOf(data.get("facility_name")));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
