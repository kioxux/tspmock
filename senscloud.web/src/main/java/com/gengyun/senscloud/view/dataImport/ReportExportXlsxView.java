package com.gengyun.senscloud.view.dataImport;

import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONObject;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.flowable.common.engine.impl.util.CollectionUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.List;
import java.util.Map;

@Component
public class ReportExportXlsxView extends AbstractXlsxStreamingView {
    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

    @Override
    protected void buildExcelDocument(Map<String, Object> result, Workbook workbook,
                                      HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            String xlsTitle = (String) result.get("xlsTitle");
            xlsTitle = RegexUtil.optStrOrVal(xlsTitle, "日常报告下载");
            String fileName = xlsTitle + ".xlsx";
            String downloadFileName = URLEncoder.encode(fileName, "UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);
            List<Map<String, Object>> firstList = (List<Map<String, Object>>) result.get("one");
            List<Map<String, Object>> twoList = (List<Map<String, Object>>) result.get("two");
            List<Map<String, Object>> threeList = (List<Map<String, Object>>) result.get("three");//备件
            List<Map<String, Object>> fourList = (List<Map<String, Object>>) result.get("four");//材料
            List<Map<String, Object>> fiveList = (List<Map<String, Object>>) result.get("five");//其他
            List<Map<String, Object>> unDoList = (List<Map<String, Object>>) result.get("six");//未做
            List<Map<String, Object>> taskList = (List<Map<String, Object>>) result.get("task");//任务项
            // create excel xls sheet
            Sheet sheet = workbook.createSheet("维护事项综合报告");
            sheet.setDefaultColumnWidth(24);
            // create header row
            Row header = sheet.createRow(0);
            int i = 0;
            String[] titleFields = {"报表日期（年/月）", "公司名称（全称）", "水厂/厂外泵站名称", "业务类型", "工单号", "设备编码", "设备名称",
                    "工艺单元代码", "工艺单元名称", "设备功能类型代码", "设备功能类型名称", "设备专业类型代码", "设备专业类型名称", "设备关键度等级代码", "设备关键度等级",
                    "维护类型（主项）（代码）", "维护类型（主项）", "维护类型代码（子项）", "维护类型（子项）", "工作组", "工作范围分类", "工作描述",
                    "症状描述", "故障类型大项代码", "故障类型大项描述", "维修症状描述","故障类型代码", "故障类型描述", "故障原因代码", "故障原因描述", "实施措施代码",
                    "实施措施代码描述", "实施措施描述", "设备状况代码", "设备状况", "工单优先级代码", "工单优先级", "负责部门", "工单状态",
                    "工单生成日期/报修日期（年/月/日）", "计划开工日期", "计划完工日期", "计划维修时间（Hrs）", "实际开工日期（年/月/日 时）",
                    "实际完成日期（年/月/日 时）", "维修负责人（工号）", "实际维修时间（Hrs）", "设备不可用停机时间（Hrs）", "维修方式（自修/外委）"
//                    ,"外委/JV公司名称","维修费用合计", "公司内部材料/工具费用", "公司内部备件费用", "公司内部人工费用", "公司内部其它费用（元）", "外委材料/工具费用", "外委备件费用", "外委人工费用", "外委其它费用（元）"
            };
            for (int k = 0; k < titleFields.length; k++) {
                String title = titleFields[k];
                header.createCell(i).setCellValue(title);
                i++;
            }
            // Create data cells
            int rowCount = 1;
            for (Map<String, Object> firstMap : firstList) {
                JSONObject jsonObject = JSONObject.fromObject(firstMap);
                Row courseRow = sheet.createRow(rowCount++);
                courseRow.createCell(0).setCellValue(jsonObject.optString("report_day", ""));//报表日期（年/月）A
                courseRow.createCell(1).setCellValue(jsonObject.optString("org_title", ""));//公司名称（全称）B
                courseRow.createCell(2).setCellValue(jsonObject.optString("poc_title", ""));//水厂/厂外泵站名称C
                courseRow.createCell(3).setCellValue(jsonObject.optString("duty_type_name", ""));//业务类型D
                courseRow.createCell(4).setCellValue(jsonObject.optString("work_code", ""));//工单号E
                courseRow.createCell(5).setCellValue(jsonObject.optString("asset_code", ""));//设备编码F
                courseRow.createCell(6).setCellValue(jsonObject.optString("asset_name", ""));//设备名称G
                courseRow.createCell(7).setCellValue(jsonObject.optString("inner_code", ""));//工艺单元（中文）H
                courseRow.createCell(8).setCellValue(jsonObject.optString("position_name", ""));//工艺单元（字母）I
                courseRow.createCell(9).setCellValue(jsonObject.optString("category_code", ""));//设备功能类型（字母）J
                courseRow.createCell(10).setCellValue(jsonObject.optString("category_name", ""));//设备功能类型（中文）K
                String asset_prf_type = jsonObject.optString("asset_prf_type", "");
                String asset_prf_type_code = "";
                if (asset_prf_type.contains("(")) {
                    asset_prf_type_code = asset_prf_type.substring(asset_prf_type.indexOf("(") + 1, asset_prf_type.indexOf(")"));
                }
                courseRow.createCell(11).setCellValue(asset_prf_type_code);//设备专业类型代码 L
                courseRow.createCell(12).setCellValue(asset_prf_type);//设备专业类型名称 M
                String level_name = jsonObject.optString("level_name", "");
                String name = "";
                String code = "";
                if (level_name.contains("(")) {
                    name = level_name.substring(0, level_name.indexOf("("));
                    code = level_name.substring(level_name.indexOf("(") + 1, level_name.indexOf(")"));
                } else {
                    name = level_name;
                }
                courseRow.createCell(13).setCellValue(code);//设备关键度等级代码 N
                courseRow.createCell(14).setCellValue(name);//设备关键度等级 O
                String business_name = jsonObject.optString("business_name", "");
                String business_name_code = "";
                switch (business_name) {
                    case "纠正性维护":
                        business_name_code = "CM";
                        break;
                    case "预防性维护":
                        business_name_code = "PM";
                        break;
                    case "改进性维护":
                        business_name_code = "MI";
                        break;
                    case "资本性项目":
                        business_name_code = "CAP";
                        break;
                    default:
                        break;
                }

                courseRow.createCell(15).setCellValue(business_name_code);//维护类型（主项）（代码）P
                courseRow.createCell(16).setCellValue(business_name);//维护类型（主项）Q
                String type_name = jsonObject.optString("type_name", "");
                if (type_name.contains("(")) {
                    name = type_name.substring(0, type_name.indexOf("("));
                    code = type_name.substring(type_name.indexOf("(") + 1, type_name.indexOf(")"));
                } else {
                    name = type_name;
                    code = "";
                }
                courseRow.createCell(17).setCellValue(code);//维护类型代码（子项）R
                courseRow.createCell(18).setCellValue(name);//维护类型（子项）S
                courseRow.createCell(19).setCellValue(asset_prf_type);//工作组 T
                courseRow.createCell(20).setCellValue(asset_prf_type);//工作范围分类 U
                courseRow.createCell(21).setCellValue(jsonObject.optString("result_note", ""));//工作描述 V
                courseRow.createCell(22).setCellValue(jsonObject.optString("problem_note", ""));//症状描述 W
                String parent_fault_code = jsonObject.optString("parent_fault_code_name", "");
                String parent_fault_code_name = "";
                String parent_fault_code_code = "";
                if (parent_fault_code.contains("【")) {
                    parent_fault_code_name = parent_fault_code.substring(0, parent_fault_code.indexOf("【"));
                    parent_fault_code_code = parent_fault_code.substring(parent_fault_code.indexOf("【") + 1, parent_fault_code.indexOf("】"));
                } else {
                    parent_fault_code_name = parent_fault_code;
                    parent_fault_code_code = "";
                }
                courseRow.createCell(23).setCellValue(parent_fault_code_code);//故障类型大项代码 X
                courseRow.createCell(24).setCellValue(parent_fault_code_name);//故障类型大项描述 Y
                courseRow.createCell(25).setCellValue(jsonObject.optString("repair_note", ""));//维修症状描述
                String fault_code = jsonObject.optString("fault_code_name", "");
                String fault_code_name = "";
                String fault_code_code = "";
                if (fault_code.contains("【")) {
                    fault_code_name = fault_code.substring(0, fault_code.indexOf("【"));
                    fault_code_code = fault_code.substring(fault_code.indexOf("【") + 1, fault_code.indexOf("】"));
                } else {
                    fault_code_name = fault_code;
                    fault_code_code = "";
                }
                courseRow.createCell(26).setCellValue(fault_code_code);//故障类型代码 Z
                courseRow.createCell(27).setCellValue(fault_code_name);//故障类型描述 AA
                String problem_cau = jsonObject.optString("problem_cau");
                int leftIndex = problem_cau.lastIndexOf("(");
                int rightIndex = problem_cau.lastIndexOf(")");
                String pcL = "";
                String pcR = "";
                if (leftIndex > 0) {
                    pcL = problem_cau.substring(0, leftIndex - 1);
                    if (rightIndex > 0) {
                        pcR = problem_cau.substring(leftIndex + 1, rightIndex);
                    } else {
                        pcR = problem_cau.substring(leftIndex);
                    }
                } else {
                    pcL = problem_cau;
                }
                courseRow.createCell(28).setCellValue(pcR);//故障原因代码 AB
                courseRow.createCell(29).setCellValue(pcL);//故障原因描述 AC
                String measure_name = jsonObject.optString("measure_name");
                int lIndex = measure_name.lastIndexOf("(");
                int rIndex = measure_name.lastIndexOf(")");
                String mL = "";
                String mR = "";
                if (lIndex > 0) {
                    mL = measure_name.substring(0, lIndex - 1);
                    if (rIndex > 0) {
                        mR = measure_name.substring(lIndex + 1, rIndex);
                    } else {
                        mR = measure_name.substring(lIndex);
                    }
                } else {
                    mL = measure_name;
                }
                courseRow.createCell(30).setCellValue(mR);//实施措施代码 AD
                courseRow.createCell(31).setCellValue(mL);//实施措施代码描述 AE
                courseRow.createCell(32).setCellValue(jsonObject.optString("measures_note", ""));//实施措施描述 AF

                String equipment_situation = jsonObject.optString("equipment_situation", "");
                String equipment_code = "";
                String equipment_name = "";
                if (equipment_situation.contains("(")) {
                    equipment_name = equipment_situation.substring(0, equipment_situation.indexOf("("));
                    equipment_code = equipment_situation.substring(equipment_situation.indexOf("(") + 1, equipment_situation.indexOf(")"));
                } else {
                    equipment_name = equipment_situation;
                    equipment_code = "";
                }
                courseRow.createCell(33).setCellValue(equipment_code);//设备状况代码 AG
                courseRow.createCell(34).setCellValue(equipment_name);//设备状况 AH
                courseRow.createCell(35).setCellValue(jsonObject.optString("repair_priority_level", ""));//工单优先级（代码）AI
                courseRow.createCell(36).setCellValue(jsonObject.optString("repair_priority", ""));//工单优先级（中文）AJ
                courseRow.createCell(37).setCellValue(jsonObject.optString("department_name", ""));//负责部门 AK
                courseRow.createCell(38).setCellValue(jsonObject.optString("status", ""));//工单状态 AL
                courseRow.createCell(39).setCellValue(jsonObject.optString("create_time", ""));//工单生成日期/报修日期（年/月/日） AM
                courseRow.createCell(40).setCellValue(jsonObject.optString("create_time", ""));//计划开工日期 AN
                courseRow.createCell(41).setCellValue("");//计划完工日期 AO
                courseRow.createCell(42).setCellValue("");//计划维修时间 （h） AP
                courseRow.createCell(43).setCellValue(jsonObject.optString("begin_time", ""));//实际开工日期（年/月/日 时）AQ
                courseRow.createCell(44).setCellValue(jsonObject.optString("finished_time", ""));//实际完成日期（年/月/日 时） AR
                courseRow.createCell(45).setCellValue(jsonObject.optString("user_code", ""));//维修负责人（工号）AS
                courseRow.createCell(46).setCellValue(jsonObject.optString("repair_time", ""));//实际维修时间（Hrs） AT
                courseRow.createCell(47).setCellValue(jsonObject.optString("downtime", ""));//设备不可用停机时间（Hrs）AU
                courseRow.createCell(48).setCellValue(jsonObject.optString("repair_type", ""));//维修方式（自修/外委） AV
            }
            Sheet sheet2 = workbook.createSheet("人工时报告");
            sheet2.setDefaultColumnWidth(24);
            // create header row
            Row header2 = sheet2.createRow(0);
            int x = 0;
            String[] titleFields2 = {"报表日期（年/月）", "工单号", "工艺单元", "设备编码",
                    "设备名称", "工作组代码", "工作组", "维修人员（工号）", "维修人员（用户名）", "维修人工时（Hrs）", "人工单价（元/小时）", "人工费合计（元）",
                    "费用类别", "费用属性", "维修方式", "外委/JV公司名称"};
            for (int k = 0; k < titleFields2.length; k++) {
                String title = titleFields2[k];
                header2.createCell(x).setCellValue(title);
                x++;
            }
            int rowCount2 = 1;
            if (CollectionUtil.isNotEmpty(twoList) && twoList.get(0) != null) {
                for (Map<String, Object> twoMap : twoList) {
                    JSONObject jsonObject = JSONObject.fromObject(twoMap);
                    Row courseRow = sheet2.createRow(rowCount2++);
                    courseRow.createCell(0).setCellValue(jsonObject.optString("report_day", ""));//报表日期（年/月）
                    courseRow.createCell(1).setCellValue(jsonObject.optString("work_code", ""));//工单号
                    courseRow.createCell(2).setCellValue(jsonObject.optString("position_name", ""));//工艺单元
                    courseRow.createCell(3).setCellValue(jsonObject.optString("asset_code", ""));//设备编码
                    courseRow.createCell(4).setCellValue(jsonObject.optString("asset_name", ""));//设备名称
                    String asset_prf_type = jsonObject.optString("asset_prf_type", "");
                    String asset_prf_type_code = "";
                    if (asset_prf_type.contains("(")) {
                        asset_prf_type_code = asset_prf_type.substring(asset_prf_type.indexOf("(") + 1, asset_prf_type.indexOf(")"));
                    }

                    courseRow.createCell(5).setCellValue(asset_prf_type_code);//工作组代码
                    courseRow.createCell(6).setCellValue(asset_prf_type);//工作组
                    courseRow.createCell(7).setCellValue(jsonObject.optString("user_code", ""));//维修人员（工号）
                    courseRow.createCell(8).setCellValue(jsonObject.optString("user_name", ""));//维修人员（用户名）
                    courseRow.createCell(9).setCellValue(jsonObject.optString("real_hour", ""));//维修人工时（Hrs）
                    courseRow.createCell(10).setCellValue(jsonObject.optString("price", ""));//人工单价（元/小时）
                    courseRow.createCell(11).setCellValue(jsonObject.optString("totalPrice", ""));//人工费合计（元）
                    courseRow.createCell(12).setCellValue(jsonObject.optString("fee_belong_name", ""));//费用类别
                    courseRow.createCell(13).setCellValue(jsonObject.optString("fee_attribute_name", ""));//费用属性
                    courseRow.createCell(14).setCellValue(jsonObject.optString("is_out", ""));//维修方式
                    courseRow.createCell(15).setCellValue(jsonObject.optString("text", ""));//外委/JV公司名称
//                    if (jsonObject.optString("repair_type", "").equals("委外") || jsonObject.optString("repair_type", "").equals("外委")) {
//                        courseRow.createCell(11).setCellValue(jsonObject.optString("name_a", ""));//
//                    } else {
//                        courseRow.createCell(11).setCellValue("");//
//                    }
                }
            }
            Sheet sheet3 = workbook.createSheet("备件消耗报告");
            sheet3.setDefaultColumnWidth(24);
            // create header row
            Row header3 = sheet3.createRow(0);
            int w = 0;
            String[] titleFields3 = {"报表日期（年/月）", "工单号", "设备编码", "设备名称", "费用类别", "费用属性", "备件编码", "备件类型", "备件名称",
                    "备件型号", "备件关键度等级", "备件数量（套）", "备件当前单价（元）", "备件费用合计（元）"};
            for (int k = 0; k < titleFields3.length; k++) {
                String title = titleFields3[k];
                header3.createCell(w).setCellValue(title);
                w++;
            }
            int rowCount3 = 1;
            if (CollectionUtil.isNotEmpty(threeList) && threeList.get(0) != null) {
                for (Map<String, Object> threeMap : threeList) {
                    JSONObject jsonObject = JSONObject.fromObject(threeMap);
                    Row courseRow = sheet3.createRow(rowCount3++);
                    courseRow.createCell(0).setCellValue(jsonObject.optString("report_day", ""));//报表日期（年/月）
                    courseRow.createCell(1).setCellValue(jsonObject.optString("work_code", ""));//工单号
                    courseRow.createCell(2).setCellValue(jsonObject.optString("asset_code", ""));//设备编码
                    courseRow.createCell(3).setCellValue(jsonObject.optString("asset_name", ""));//设备名称
                    courseRow.createCell(4).setCellValue(jsonObject.optString("fee_belong_name", ""));//费用类别
                    courseRow.createCell(5).setCellValue(jsonObject.optString("fee_attribute_name", ""));//费用属性
                    courseRow.createCell(6).setCellValue(jsonObject.optString("object_code", ""));//备件编码
                    courseRow.createCell(7).setCellValue("");//备件类型
                    courseRow.createCell(8).setCellValue(jsonObject.optString("object_name", ""));//备件名称
                    courseRow.createCell(9).setCellValue(jsonObject.optString("object_model", ""));//备件型号
                    courseRow.createCell(10).setCellValue("");//备件关键度等级
                    courseRow.createCell(11).setCellValue(jsonObject.optString("use_count", ""));//备件数量（套）
                    courseRow.createCell(12).setCellValue(jsonObject.optString("price", ""));//备件当前单价（元）
                    courseRow.createCell(13).setCellValue(jsonObject.optString("totalPrice", ""));//备件费用合计（元）
                }
            }

            Sheet sheet4 = workbook.createSheet("材料消耗报告");
            sheet4.setDefaultColumnWidth(24);
            // create header row
            Row header4 = sheet4.createRow(0);
            int q = 0;
            String[] titleFields4 = {"报表日期（年/月）", "工单号", "设备编码", "设备名称", "费用类别", "费用属性", "材料名称", "材料数量", "材料单价（元）", "材料费用合计"};
            for (int k = 0; k < titleFields4.length; k++) {
                String title = titleFields4[k];
                header4.createCell(q).setCellValue(title);
                q++;
            }
            int rowCount4 = 1;
            if (CollectionUtil.isNotEmpty(fourList) && fourList.get(0) != null) {
                for (Map<String, Object> fourMap : fourList) {
                    JSONObject jsonObject = JSONObject.fromObject(fourMap);
                    Row courseRow = sheet4.createRow(rowCount4++);
                    courseRow.createCell(0).setCellValue(jsonObject.optString("report_day", ""));//报表日期（年/月）
                    courseRow.createCell(1).setCellValue(jsonObject.optString("work_code", ""));//工单号
                    courseRow.createCell(2).setCellValue(jsonObject.optString("asset_code", ""));//设备编码
                    courseRow.createCell(3).setCellValue(jsonObject.optString("asset_name", ""));//设备名称
                    courseRow.createCell(4).setCellValue(jsonObject.optString("fee_belong_name", ""));//费用类别
                    courseRow.createCell(5).setCellValue(jsonObject.optString("fee_attribute_name", ""));//费用属性
                    courseRow.createCell(6).setCellValue(jsonObject.optString("object_name", ""));//材料名称
                    courseRow.createCell(7).setCellValue(jsonObject.optString("use_count", ""));//材料数量
                    courseRow.createCell(8).setCellValue(jsonObject.optString("price", ""));//材料单价（元）
                    courseRow.createCell(9).setCellValue(jsonObject.optString("totalPrice", ""));//材料费用合计
                }
            }
            Sheet sheet5 = workbook.createSheet("其他消耗报告");
            sheet5.setDefaultColumnWidth(24);
            // create header row
            Row header5 = sheet5.createRow(0);
            int r = 0;
            String[] titleFields5 = {"报表日期（年/月）", "工单号", "设备编码", "设备名称", "费用类别", "费用属性", "名称", "描述", "材料数量", "材料单价（元）", "材料费用合计"};
            for (int d = 0; d < titleFields5.length; d++) {
                String title = titleFields5[d];
                header5.createCell(r).setCellValue(title);
                r++;
            }
            int rowCount5 = 1;
            if (CollectionUtil.isNotEmpty(fiveList) && fiveList.get(0) != null) {
                for (Map<String, Object> fiveMap : fiveList) {
                    JSONObject jsonObject = JSONObject.fromObject(fiveMap);
                    Row courseRow = sheet5.createRow(rowCount5++);
                    courseRow.createCell(0).setCellValue(jsonObject.optString("report_day", ""));//报表日期（年/月）
                    courseRow.createCell(1).setCellValue(jsonObject.optString("work_code", ""));//工单号
                    courseRow.createCell(2).setCellValue(jsonObject.optString("asset_code", ""));//设备编码
                    courseRow.createCell(3).setCellValue(jsonObject.optString("asset_name", ""));//设备名称
                    courseRow.createCell(4).setCellValue(jsonObject.optString("fee_belong_name", ""));//费用类别
                    courseRow.createCell(5).setCellValue(jsonObject.optString("fee_attribute_name", ""));//费用属性
                    courseRow.createCell(6).setCellValue(jsonObject.optString("object_name", ""));//名称
                    courseRow.createCell(7).setCellValue(jsonObject.optString("remark", ""));//描述
                    courseRow.createCell(8).setCellValue(jsonObject.optString("use_count", ""));//材料数量
                    courseRow.createCell(9).setCellValue(jsonObject.optString("price", ""));//材料单价（元）
                    courseRow.createCell(10).setCellValue(jsonObject.optString("totalPrice", ""));//材料费用合计
                }
            }


            Sheet sheet6 = workbook.createSheet("维护事项未完成报告");
            sheet6.setDefaultColumnWidth(24);
            // create header row
            Row header6 = sheet6.createRow(0);
            int z = 0;
            for (int k = 0; k < titleFields.length; k++) {
                String title = titleFields[k];
                header6.createCell(z).setCellValue(title);
                z++;
            }
            // Create data cells
            int rowCount6 = 1;
            for (Map<String, Object> unDoMap : unDoList) {
                JSONObject jsonObject = JSONObject.fromObject(unDoMap);
                Row courseRow = sheet6.createRow(rowCount6++);
                courseRow.createCell(0).setCellValue(jsonObject.optString("report_day", ""));//报表日期（年/月）A
                courseRow.createCell(1).setCellValue(jsonObject.optString("org_title", ""));//公司名称（全称）B
                courseRow.createCell(2).setCellValue(jsonObject.optString("poc_title", ""));//水厂/厂外泵站名称C
                courseRow.createCell(3).setCellValue(jsonObject.optString("duty_type_name", ""));//业务类型D
                courseRow.createCell(4).setCellValue(jsonObject.optString("work_code", ""));//工单号E
                courseRow.createCell(5).setCellValue(jsonObject.optString("asset_code", ""));//设备编码F
                courseRow.createCell(6).setCellValue(jsonObject.optString("asset_name", ""));//设备名称G
                courseRow.createCell(7).setCellValue(jsonObject.optString("inner_code", ""));//工艺单元（中文）H
                courseRow.createCell(8).setCellValue(jsonObject.optString("position_name", ""));//工艺单元（字母）I
                courseRow.createCell(9).setCellValue(jsonObject.optString("category_code", ""));//设备功能类型（字母）J
                courseRow.createCell(10).setCellValue(jsonObject.optString("category_name", ""));//设备功能类型（中文）K
                String asset_prf_type = jsonObject.optString("asset_prf_type", "");
                String asset_prf_type_code = "";
                if (asset_prf_type.contains("(")) {
                    asset_prf_type_code = asset_prf_type.substring(asset_prf_type.indexOf("(") + 1, asset_prf_type.indexOf(")"));
                }
                courseRow.createCell(11).setCellValue(asset_prf_type_code);//设备专业类型代码 L
                courseRow.createCell(12).setCellValue(asset_prf_type);//设备专业类型名称 M
                String level_name = jsonObject.optString("level_name", "");
                String name = "";
                String code = "";
                if (level_name.contains("(")) {
                    name = level_name.substring(0, level_name.indexOf("("));
                    code = level_name.substring(level_name.indexOf("(") + 1, level_name.indexOf(")"));
                } else {
                    name = level_name;
                }
                courseRow.createCell(13).setCellValue(code);//设备关键度等级代码 N
                courseRow.createCell(14).setCellValue(name);//设备关键度等级 O
                String business_name = jsonObject.optString("business_name", "");
                String business_name_code = "";
                switch (business_name) {
                    case "纠正性维护":
                        business_name_code = "CM";
                        break;
                    case "预防性维护":
                        business_name_code = "PM";
                        break;
                    case "改进性维护":
                        business_name_code = "MI";
                        break;
                    case "资本性项目":
                        business_name_code = "CAP";
                        break;
                    default:
                        break;
                }

                courseRow.createCell(15).setCellValue(business_name_code);//维护类型（主项）（代码）P
                courseRow.createCell(16).setCellValue(business_name);//维护类型（主项）Q
                String type_name = jsonObject.optString("type_name", "");
                if (type_name.contains("(")) {
                    name = type_name.substring(0, type_name.indexOf("("));
                    code = type_name.substring(type_name.indexOf("(") + 1, type_name.indexOf(")"));
                } else {
                    name = type_name;
                    code = "";
                }
                courseRow.createCell(17).setCellValue(code);//维护类型代码（子项）R
                courseRow.createCell(18).setCellValue(name);//维护类型（子项）S
                courseRow.createCell(19).setCellValue(asset_prf_type);//工作组 T
                courseRow.createCell(20).setCellValue(asset_prf_type);//工作范围分类 U
                courseRow.createCell(21).setCellValue(jsonObject.optString("result_note", ""));//工作描述 V
                courseRow.createCell(22).setCellValue(jsonObject.optString("problem_note", ""));//症状描述 W
                String parent_fault_code = jsonObject.optString("parent_fault_code_name", "");
                String parent_fault_code_name = "";
                String parent_fault_code_code = "";
                if (parent_fault_code.contains("【")) {
                    parent_fault_code_name = parent_fault_code.substring(0, parent_fault_code.indexOf("【"));
                    parent_fault_code_code = parent_fault_code.substring(parent_fault_code.indexOf("【") + 1, parent_fault_code.indexOf("】"));
                } else {
                    parent_fault_code_name = parent_fault_code;
                    parent_fault_code_code = "";
                }
                courseRow.createCell(23).setCellValue(parent_fault_code_code);//故障类型大项代码 X
                courseRow.createCell(24).setCellValue(parent_fault_code_name);//故障类型大项描述 Y


                String fault_code = jsonObject.optString("fault_code_name", "");
                String fault_code_name = "";
                String fault_code_code = "";
                if (fault_code.contains("【")) {
                    fault_code_name = fault_code.substring(0, fault_code.indexOf("【"));
                    fault_code_code = fault_code.substring(fault_code.indexOf("【") + 1, fault_code.indexOf("】"));
                } else {
                    fault_code_name = fault_code;
                    fault_code_code = "";
                }

                courseRow.createCell(25).setCellValue(fault_code_code);//故障类型代码 Z
                courseRow.createCell(26).setCellValue(fault_code_name);//故障类型描述 AA
                String problem_cau = jsonObject.optString("problem_cau");
                int leftIndex = problem_cau.lastIndexOf("(");
                int rightIndex = problem_cau.lastIndexOf(")");
                String pcL = "";
                String pcR = "";
                if (leftIndex > 0) {
                    pcL = problem_cau.substring(0, leftIndex - 1);
                    if (rightIndex > 0) {
                        pcR = problem_cau.substring(leftIndex + 1, rightIndex);
                    } else {
                        pcR = problem_cau.substring(leftIndex);
                    }
                } else {
                    pcL = problem_cau;
                }
                courseRow.createCell(27).setCellValue(pcR);//故障原因代码 AB
                courseRow.createCell(28).setCellValue(pcL);//故障原因描述 AC
                String measure_name = jsonObject.optString("measure_name");
                int lIndex = measure_name.lastIndexOf("(");
                int rIndex = measure_name.lastIndexOf(")");
                String mL = "";
                String mR = "";
                if (lIndex > 0) {
                    mL = measure_name.substring(0, lIndex - 1);
                    if (rIndex > 0) {
                        mR = measure_name.substring(lIndex + 1, rIndex);
                    } else {
                        mR = measure_name.substring(lIndex);
                    }
                } else {
                    mL = measure_name;
                }
                courseRow.createCell(29).setCellValue(mR);//实施措施代码 AD
                courseRow.createCell(30).setCellValue(mL);//实施措施代码描述 AE
                courseRow.createCell(31).setCellValue(jsonObject.optString("measures_note", ""));//实施措施描述 AF

                String equipment_situation = jsonObject.optString("equipment_situation", "");
                String equipment_code = "";
                String equipment_name = "";
                if (equipment_situation.contains("(")) {
                    equipment_name = equipment_situation.substring(0, equipment_situation.indexOf("("));
                    equipment_code = equipment_situation.substring(equipment_situation.indexOf("(") + 1, equipment_situation.indexOf(")"));
                } else {
                    equipment_name = equipment_situation;
                    equipment_code = "";
                }
                courseRow.createCell(32).setCellValue(equipment_code);//设备状况代码 AG
                courseRow.createCell(33).setCellValue(equipment_name);//设备状况 AH
                courseRow.createCell(34).setCellValue(jsonObject.optString("repair_priority_level", ""));//工单优先级（代码）AI
                courseRow.createCell(35).setCellValue(jsonObject.optString("repair_priority", ""));//工单优先级（中文）AJ
                courseRow.createCell(36).setCellValue(jsonObject.optString("department_name", ""));//负责部门 AK
                courseRow.createCell(37).setCellValue(jsonObject.optString("status", ""));//工单状态 AL
                courseRow.createCell(38).setCellValue(jsonObject.optString("create_time", ""));//工单生成日期/报修日期（年/月/日） AM
                courseRow.createCell(39).setCellValue(jsonObject.optString("create_time", ""));//计划开工日期 AN
                courseRow.createCell(40).setCellValue("");//计划完工日期 AO
                courseRow.createCell(41).setCellValue("");//计划维修时间 （h） AP
                courseRow.createCell(42).setCellValue(jsonObject.optString("begin_time", ""));//实际开工日期（年/月/日 时）AQ
                courseRow.createCell(43).setCellValue(jsonObject.optString("finished_time", ""));//实际完成日期（年/月/日 时） AR
                courseRow.createCell(44).setCellValue(jsonObject.optString("user_code", ""));//维修负责人（工号）AS
                courseRow.createCell(45).setCellValue(jsonObject.optString("repair_time", ""));//实际维修时间（Hrs） AT
                courseRow.createCell(46).setCellValue(jsonObject.optString("downtime", ""));//设备不可用停机时间（Hrs）AU
                courseRow.createCell(47).setCellValue(jsonObject.optString("repair_type", ""));//维修方式（自修/外委） AV
            }
            Sheet sheet7 = workbook.createSheet("任务项");
            sheet7.setDefaultColumnWidth(24);
//            sheet7.setColumnWidth(1, 30 * 256);
            // create header row
            Row header7 = sheet7.createRow(0);
            int y = 0;
            String[] titleFields7 = {"报表日期（年/月）", "工单号", "任务模板名称", "任务组名称", "任务项名称", "任务结果"};
            for (int k = 0; k < titleFields7.length; k++) {
                String title = titleFields7[k];
                header7.createCell(y).setCellValue(title);
                y++;
            }
            int rowCount7 = 1;
            if (RegexUtil.optIsPresentList(taskList) && taskList.get(0) != null) {
                for (Map<String, Object> taskMap : taskList) {
                    Row courseRow = sheet7.createRow(rowCount7++);
                    courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(taskMap.get("report_day"), ""));//报表日期（年/月）
                    courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(taskMap.get("work_code"), ""));//工单号
                    courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(taskMap.get("task_template_name"), ""));//设备编码
                    courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(taskMap.get("group_name"), ""));//设备名称
                    courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(taskMap.get("task_item_name"), ""));//费用类别
                    String result_type = RegexUtil.optStrOrBlank(taskMap.get("result_type"));
                    if ("4".equals(result_type)) {
                        List<String> imgs = (List<String>) taskMap.get("value");
                        if (imgs == null || imgs.size() == 0) {
                            courseRow.createCell(5).setCellValue("");
                        } else {
                            courseRow.setHeightInPoints(150);
                            int cellColumn = 5;
                            for (int d = 0; d < imgs.size(); d++) {
                                sheet7.setColumnWidth(cellColumn, 35 * 256);
                                String fileNames = imgs.get(d);
                                Drawing patriarch = sheet7.createDrawingPatriarch();
                                CreationHelper helper = sheet7.getWorkbook().getCreationHelper();
                                ClientAnchor anchor = helper.createClientAnchor();
                                anchor.setCol1(cellColumn);
                                anchor.setRow1(rowCount7 - 1);
                                InputStream is = new FileInputStream(fileNames);
                                byte[] bytes = IOUtils.toByteArray(is);
                                int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                                Picture pict = patriarch.createPicture(anchor, pictureIdx);
                                pict.resize(1,1);
                                cellColumn++;
                            }
                        }
                    } else {
                        courseRow.createCell(5).setCellValue(RegexUtil.optStrOrVal(taskMap.get("value"), ""));//费用属性
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
