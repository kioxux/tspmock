package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.apache.poi.hssf.usermodel.DVConstraint;
//import org.apache.poi.hssf.usermodel.HSSFDataValidation;
//import org.apache.poi.ss.usermodel.*;
//import org.apache.poi.ss.util.CellRangeAddressList;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.view.document.AbstractXlsView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
public class MetadataExcelTemplate{
//public class MetadataExcelTemplate extends AbstractXlsView {
//    public static String SHEET_NAME_FACILITIES = "位置";
//    public static String SHEET_NAME_SUPPLIERS = "供应商";
//    public static String SHEET_NAME_CATEGORY = "设备类型";
//    public static String SHEET_NAME_MODEL = "设备型号";
//    public static String SHEET_NAME_POSITION = "位置编码";
//    public static String SHEET_NAME_SOURCE = "设备来源";
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        try {
//            String asset_name = (String) map.get("category_name");
//            String category_id = (String) map.get("category_id");
//            JSONArray fields = (JSONArray) map.get("fields");
//            SelectOptionService selectOptionService = (SelectOptionService) map.get("selectOptionService");
//            String excelName = asset_name + ".xls";
//            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(excelName, "utf-8"));
//            response.setContentType("application/ms-excel; charset=UTF-8");
//            response.setCharacterEncoding("UTF-8");
//
//            Sheet sheet = workbook.createSheet(asset_name + "(" + category_id + ")");
//
//            Sheet sheet_facilities = workbook.createSheet(selectOptionService.getLanguageInfo(LangConstant.POS_A));
//
//            Sheet sheet_suppliers = workbook.createSheet(selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A));
//
//            Sheet sheet_category = workbook.createSheet(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_C));
//
//            Sheet sheet_model = workbook.createSheet(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_D));
//
//            Sheet sheet_position = workbook.createSheet(selectOptionService.getLanguageInfo(LangConstant.POSITION_CODE));
//
//            sheet.setDefaultColumnWidth(30);
//            CellStyle style = workbook.createCellStyle();
//            Font font = workbook.createFont();
//            font.setFontName("Arial");
//            font.setBold(true);
//            style.setFont(font);
//
//            Row header = sheet.createRow(0);
//            int intsiteid_index = -1;
//            int intstatus_index = -1;
//            int supplier_index = -1;
//            int category_index = -1;
//            int importment_index = -1;
//            int unit_index = -1;
//            int subject_index = -1;
//            int currency_index = -1;
//            int asset_monitor_type_index = -1;
//            int source_type_index = -1;
//            int asset_model_id_index = -1;
//            int manufacturer_index = -1;
//            int position_index = -1;
//
//            for (int i = 0; i < fields.size(); i++) {
//                JSONObject field = fields.optJSONObject(i);
//                MetaDataField metaDataField = (MetaDataField) JSONObject.toBean(field, MetaDataField.class);
//
//                String name = metaDataField.getName();
//                if (AssetCategory.INTSITEID.equalsIgnoreCase(name)) {
//                    intsiteid_index = i;
//                }
//                if (AssetCategory.INTSTATUS.equalsIgnoreCase(name)) {
//                    intstatus_index = i;
//                }
//                if (AssetCategory.SUPPLIER.equalsIgnoreCase(name)) {
//                    supplier_index = i;
//                }
//                if (AssetCategory.CATEGORY_ID.equalsIgnoreCase(name)) {
//                    category_index = i;
//                }
//                if (AssetCategory.IMPORTMENT_LEVEL_ID.equalsIgnoreCase(name)) {
//                    importment_index = i;
//                }
//                if (AssetCategory.UNIT_ID.equalsIgnoreCase(name)) {
//                    unit_index = i;
//                }
//                if (AssetCategory.ASSET_SUBJECT.equalsIgnoreCase(name)) {
//                    subject_index = i;
//                }
//                if (AssetCategory.CURRENCY.equalsIgnoreCase(name)) {
//                    currency_index = i;
//                }
//                if (AssetCategory.IOT_STATUS.equalsIgnoreCase(name)) {
//                    asset_monitor_type_index = i;
//                }
//                if (AssetCategory.SOURCE_TYPE.equalsIgnoreCase(name)) {
//                    source_type_index = i;
//                }
//                if (AssetCategory.ASSET_MODEL_ID.equalsIgnoreCase(name)) {
//                    asset_model_id_index = i;
//                }
//                if (AssetCategory.MANUFACTURE.equalsIgnoreCase(name)) {
//                    manufacturer_index = i;
//                }
//                if (AssetCategory.POSITION_CODE.equalsIgnoreCase(name)) {
//                    position_index = i;
//                }
//                String alias = metaDataField.getAlias();
//                header.createCell(i).setCellValue(name + "[" + alias + "]");
//                style.setLocked(true);
//                header.getCell(i).setCellStyle(style);
//                if(map.containsKey(name)){
//                    if (map.get(name) != null) {
//                        try {
//                            List<Map<String, Object>> fieldList = (List<Map<String, Object>>) map.get(name);
//                            String[] currencyList2 = new String[fieldList.size()];
//                            for (int j = 0; j < fieldList.size(); j++) {
//                                String text = fieldList.get(j).get("desc") + "";
//                                currencyList2[j] = text;
//                            }
//                            setHSSFValidation(sheet, currencyList2, 1, Integer.MAX_VALUE, i, i);
//                        } catch (Exception ex) {
//
//                        }
//                    }
//                }
//            }
//            sheet.createFreezePane(0, 1, 0, 1);
//
//            if (map.get("facilityList") != null) {
//                try {
//                    List<Facility> facilityList = (List<Facility>) map.get("facilityList");
//                    List<String> facilityList2 = new ArrayList<String>();
//                    facilityList2.add(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//                    Row row = sheet_facilities.createRow(0);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//
//                    for (int i = 0; i < facilityList.size(); i++) {
//                        Facility facility = facilityList.get(i);
//                        String text = facility.getTitle().trim() + "";
//                        facilityList2.add(text);
//                        row = sheet_facilities.createRow(i + 1);
//                        cell = row.createCell(0);
//                        cell.setCellValue(text);
//                    }
//
//                    Name namedCell = workbook.createName();
//                    namedCell.setNameName(selectOptionService.getLanguageInfo(LangConstant.POS_A));//位置列表
//                    namedCell.setRefersToFormula(sheet_facilities.getSheetName() + "!$A$1:$A$" + facilityList2.size());
//                    //加载数据,将名称为hidden的
//                    DVConstraint constraint = DVConstraint.createFormulaListConstraint(selectOptionService.getLanguageInfo(LangConstant.POS_A));//位置列表
//
//                    // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
//                    CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, Integer.MAX_VALUE, intsiteid_index, intsiteid_index);
//                    HSSFDataValidation validation = new HSSFDataValidation(cellRangeAddressList, constraint);
//
//                    //将第二个sheet设置为隐藏
//                    //workbook.setSheetHidden(workbook.getSheetIndex(SHEET_NAME_FACILITIES), true);
//                    sheet.addValidationData(validation);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            List<Map<String, Object>> iotSetting = new ArrayList<Map<String, Object>>();
//            for (int x = 1; x <= 3; x++) {
//                Map<String, Object> map_iot_status = new HashMap<String, Object>();
//                if (x == 1) {
//                    map_iot_status.put("iot_status", selectOptionService.getLanguageInfo(LangConstant.NO_IOT_N));//无物联
//                } else if (x == 2) {
//                    map_iot_status.put("iot_status", selectOptionService.getLanguageInfo(LangConstant.IOT_ONLINE));//物联在线
//                } else if (x == 3) {
//                    map_iot_status.put("iot_status", selectOptionService.getLanguageInfo(LangConstant.IOT_OFFLINE));//物联离线
//                }
//                iotSetting.add(map_iot_status);
//            }
//            if (iotSetting != null) {
//                try {
//                    String[] iotSetting2 = new String[iotSetting.size()];
//                    for (int i = 0; i < iotSetting.size(); i++) {
//                        String text = iotSetting.get(i).get("iot_status") + "";
//                        iotSetting2[i] = text;
//                    }
//                    setHSSFValidation(sheet, iotSetting2, 1, Integer.MAX_VALUE, asset_monitor_type_index, asset_monitor_type_index);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            if (map.get("categoryList") != null) {
//                try {
//                    List<AssetCategory> categoryList = (List<AssetCategory>) map.get("categoryList");
//                    List<String> categoryList2 = new ArrayList<String>();
//                    categoryList2.add(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//                    Row row = sheet_category.createRow(0);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//
//                    for (int i = 0; i < categoryList.size(); i++) {
//                        AssetCategory category = categoryList.get(i);
//                        String text = category.getCategory_name().trim() + "";
////                    String text = "(" + category.getId() + ")" + category.getTitle().trim();
//                        categoryList2.add(text);
//                        row = sheet_category.createRow(i + 1);
//                        cell = row.createCell(0);
//                        cell.setCellValue(text);
//                    }
//
//                    Name namedCell = workbook.createName();
//                    namedCell.setNameName(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_C));//设备类型列表
//                    namedCell.setRefersToFormula(sheet_category.getSheetName() + "!$A$1:$A$" + categoryList2.size());
//                    //加载数据,将名称为hidden的
//                    DVConstraint constraint = DVConstraint.createFormulaListConstraint(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_C));//设备类型列表
//                    // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
//                    CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, Integer.MAX_VALUE, category_index, category_index);
//                    HSSFDataValidation validation = new HSSFDataValidation(cellRangeAddressList, constraint);
//                    //将第二个sheet设置为隐藏
//                    //workbook.setSheetHidden(workbook.getSheetIndex(SHEET_NAME_FACILITIES), true);
//                    sheet.addValidationData(validation);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            if (map.get("assetStatusList") != null) {
//                try {
//                    List<AssetStatus> assetStatusList = (List<AssetStatus>) map.get("assetStatusList");
//                    String[] assetStatusList2 = new String[assetStatusList.size()];
//                    for (int i = 0; i < assetStatusList.size(); i++) {
//                        AssetStatus assetStatus = assetStatusList.get(i);
//                        String text = assetStatus.getStatus() + "";
//                        assetStatusList2[i] = text;
//                    }
//                    setHSSFValidation(sheet, assetStatusList2, 1, Integer.MAX_VALUE, intstatus_index, intstatus_index);
//                } catch (Exception ex) {
//
//                }
//            }
//
//            if (map.get("sourceTypes") != null) {
//                try {
//                    List<Map<String, Object>> sourceTypeList = (List<Map<String, Object>>) map.get("sourceTypes");
//                    String[] sourceTypeList2 = new String[sourceTypeList.size()];
//                    for (int i = 0; i < sourceTypeList.size(); i++) {
//                        String text = sourceTypeList.get(i).get("desc") + "";
//                        sourceTypeList2[i] = text;
//                    }
//                    setHSSFValidation(sheet, sourceTypeList2, 1, Integer.MAX_VALUE, source_type_index, source_type_index);
//                } catch (Exception ex) {
//
//                }
//            }
//
//            if (map.get("modelList") != null) {
//                try {
//                    List<Map<String, Object>> modelList = (List<Map<String, Object>>) map.get("modelList");
//                    List<String> modelList2 = new ArrayList<String>();
//                    modelList2.add(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//                    Row row = sheet_model.createRow(0);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//
//                    for (int i = 0; i < modelList.size(); i++) {
//                        String text = modelList.get(i).get("model_name") + "";
//                        modelList2.add(text.trim());
//                        row = sheet_model.createRow(i + 1);
//                        cell = row.createCell(0);
//                        cell.setCellValue(text);
//                    }
//
//                    Name namedCell = workbook.createName();
//                    namedCell.setNameName(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_D));//设备型号列表
//                    namedCell.setRefersToFormula(sheet_model.getSheetName() + "!$A$1:$A$" + modelList2.size());
//                    //加载数据,将名称为hidden的
//                    DVConstraint constraint = DVConstraint.createFormulaListConstraint(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_D));//设备型号列表
//                    // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
//                    CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, Integer.MAX_VALUE, asset_model_id_index, asset_model_id_index);
//                    HSSFDataValidation validation = new HSSFDataValidation(cellRangeAddressList, constraint);
//                    //将第二个sheet设置为隐藏
//                    //workbook.setSheetHidden(workbook.getSheetIndex(SHEET_NAME_FACILITIES), true);
//                    sheet.addValidationData(validation);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            if (map.get("assetPositionsList") != null) {
//                try {
//                    List<AssetPosition> assetPositionList = (List<AssetPosition>) map.get("assetPositionsList");
//                    List<String> assetPositionsList2 = new ArrayList<String>();
//                    assetPositionsList2.add(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//                    Row row = sheet_position.createRow(0);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//
//                    for (int i = 0; i < assetPositionList.size(); i++) {
//                        AssetPosition assetPosition = assetPositionList.get(i);
//                        String text = assetPosition.getPosition_name().trim() + "";
//                        assetPositionsList2.add(text);
//                        row = sheet_position.createRow(i + 1);
//                        cell = row.createCell(0);
//                        cell.setCellValue(text);
//                    }
//                    Name namedCell = workbook.createName();
//                    namedCell.setNameName(selectOptionService.getLanguageInfo(LangConstant.POSITION_CODE));//位置编码列表
//                    namedCell.setRefersToFormula(sheet_position.getSheetName() + "!$A$1:$A$" + assetPositionsList2.size());
//                    //加载数据,将名称为hidden的
//                    DVConstraint constraint = DVConstraint.createFormulaListConstraint(selectOptionService.getLanguageInfo(LangConstant.POSITION_CODE));//位置编码列表
//                    // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
//                    CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, Integer.MAX_VALUE, position_index, position_index);
//                    HSSFDataValidation validation = new HSSFDataValidation(cellRangeAddressList, constraint);
//                    //将第二个sheet设置为隐藏
//                    //workbook.setSheetHidden(workbook.getSheetIndex(SHEET_NAME_FACILITIES), true);
//                    sheet.addValidationData(validation);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            if (map.get("manufacturerList") != null) {
//                try {
//                    List<Map<String, Object>> manufacturerList = (List<Map<String, Object>>) map.get("manufacturerList");
//                    String[] manufacturerList2 = new String[manufacturerList.size()];
//                    for (int i = 0; i < manufacturerList.size(); i++) {
//                        String text = manufacturerList.get(i).get("desc") + "";
//                        manufacturerList2[i] = text;
//                    }
//                    setHSSFValidation(sheet, manufacturerList2, 1, Integer.MAX_VALUE, manufacturer_index, manufacturer_index);
//                } catch (Exception ex) {
//
//                }
//            }
//
//            if (map.get("unitList") != null) {
//                try {
//                    List<Unit> UnitList = (List<Unit>) map.get("unitList");
//                    String[] UnitList2 = new String[UnitList.size()];
//                    for (int i = 0; i < UnitList.size(); i++) {
//                        Unit Unit = UnitList.get(i);
//                        String text = Unit.getUnit_name() + "";
//                        UnitList2[i] = text;
//                    }
//                    setHSSFValidation(sheet, UnitList2, 1, Integer.MAX_VALUE, unit_index, unit_index);
//                } catch (Exception ex) {
//
//                }
//            }
//
//            if (map.get("currencyList") != null) {
//                try {
//                    List<Map<String, Object>> currencyList = (List<Map<String, Object>>) map.get("currencyList");
//                    String[] currencyList2 = new String[currencyList.size()];
//                    for (int i = 0; i < currencyList.size(); i++) {
//                        String text = currencyList.get(i).get("desc") + "";
//                        currencyList2[i] = text;
//                    }
//                    setHSSFValidation(sheet, currencyList2, 1, Integer.MAX_VALUE, currency_index, currency_index);
//                } catch (Exception ex) {
//
//                }
//            }
//            List<Map<String, Object>> asset_subject = null;
//            if (map.get("asset_subject") != null) {
//                try {
//                    asset_subject = (List<Map<String, Object>>) map.get("asset_subject");
//                    String[] asset_subject2 = new String[asset_subject.size()];
//                    for (int i = 0; i < asset_subject.size(); i++) {
//                        for (String key : asset_subject.get(i).keySet()) {
//                            String text = asset_subject.get(i).get(key) + "";
//                            asset_subject2[i] = text;
//                        }
//                    }
//                    setHSSFValidation(sheet, asset_subject2, 1, Integer.MAX_VALUE, subject_index, subject_index);
//                } catch (Exception ex) {
//
//                }
//            }
////        List<AssetCategory> categoryList = null;
////        if (map.get("categoryList") != null) {
////            try {
////                categoryList = (List<AssetCategory>) map.get("categoryList");
////                String[] assetCategoryList2 = new String[categoryList.size()];
////                for (int i = 0; i < categoryList.size(); i++) {
////                    AssetCategory assetCategory = categoryList.get(i);
////                    String text = "(" + assetCategory.getId() + ")" + assetCategory.getCategory_name();
////                    assetCategoryList2[i] = text;
////                }
////                setHSSFValidation(sheet, assetCategoryList2, 1, Integer.MAX_VALUE, category_index, category_index);
////            } catch (Exception ex) {
////                ex.printStackTrace();
////                return;
////            }
////        }
//
//            if (map.get("importmentLevelList") != null) {
//                try {
//                    List<ImportmentLevel> importmentLevelList = (List<ImportmentLevel>) map.get("importmentLevelList");
//                    String[] importmentLevelList2 = new String[importmentLevelList.size()];
//                    for (int i = 0; i < importmentLevelList.size(); i++) {
//                        ImportmentLevel importmentLevel = importmentLevelList.get(i);
//                        String text = importmentLevel.getLevel_name() + "";
//                        importmentLevelList2[i] = text;
//                    }
//                    setHSSFValidation(sheet, importmentLevelList2, 1, Integer.MAX_VALUE, importment_index, importment_index);
//                } catch (Exception ex) {
//
//                }
//            }
//
//            // yzj 0830，清除customer表，导致bug，大家各自修改  --  修改好删除此注释
//            if (map.get("supplierList") != null) {
//                try {
//                    List<Map<String, Object>> supplierList = (List<Map<String, Object>>) map.get("supplierList");
//                    List<String> supplierList2 = new ArrayList<String>();
//                    supplierList2.add(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//                    Row row = sheet_suppliers.createRow(0);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(selectOptionService.getLanguageInfo(LangConstant.TEXT_AO));//未知
//
//                    for (int i = 0; i < supplierList.size(); i++) {
//                        Map<String, Object> supplier = supplierList.get(i);
//                        String text = supplier.get("desc") + "";
//                        supplierList2.add(text);
//                        row = sheet_suppliers.createRow(i + 1);
//                        cell = row.createCell(0);
//                        cell.setCellValue(text);
//                    }
//
//                    Name namedCell = workbook.createName();
//                    namedCell.setNameName(selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A));//供应商列表
//                    namedCell.setRefersToFormula(sheet_suppliers.getSheetName() + "!$A$1:$A$" + supplierList2.size());
//                    //加载数据,将名称为hidden的
//                    DVConstraint constraint = DVConstraint.createFormulaListConstraint(selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A));//供应商列表
//
//                    // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
//                    CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, Integer.MAX_VALUE, supplier_index, supplier_index);
//                    HSSFDataValidation validation = new HSSFDataValidation(cellRangeAddressList, constraint);
//
//                    //将第二个sheet设置为隐藏
//                    //workbook.setSheetHidden(workbook.getSheetIndex(SHEET_NAME_SUPPLIERS), true);
//                    sheet.addValidationData(validation);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    /**
//     * 设置某些列的值只能输入预制的数据,显示下拉框.
//     *
//     * @param sheet    要设置的sheet.
//     * @param textlist 下拉框显示的内容
//     * @param firstRow 开始行
//     * @param endRow   结束行
//     * @param firstCol 开始列
//     * @param endCol   结束列
//     * @return 设置好的sheet.
//     */
//    public static Sheet setHSSFValidation(Sheet sheet,
//                                          String[] textlist, int firstRow, int endRow, int firstCol,
//                                          int endCol) {
//        // 加载下拉列表内容
//        DVConstraint constraint = DVConstraint.createExplicitListConstraint(textlist);
//        // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
//        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
//        // 数据有效性对象
//        HSSFDataValidation data_validation_list = new HSSFDataValidation(regions, constraint);
//        sheet.addValidationData(data_validation_list);
//        return sheet;
//    }
//
//    /**
//     * 设置单元格上提示
//     *
//     * @param sheet         要设置的sheet.
//     * @param promptTitle   标题
//     * @param promptContent 内容
//     * @param firstRow      开始行
//     * @param endRow        结束行
//     * @param firstCol      开始列
//     * @param endCol        结束列
//     * @return 设置好的sheet.
//     */
//    public static Sheet setHSSFPrompt(Sheet sheet, String promptTitle,
//                                      String promptContent, int firstRow, int endRow, int firstCol, int endCol) {
//        // 构造constraint对象
//        DVConstraint constraint = DVConstraint.createCustomFormulaConstraint("BB1");
//        // 四个参数分别是：起始行、终止行、起始列、终止列
//        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
//        // 数据有效性对象
//        HSSFDataValidation data_validation_view = new HSSFDataValidation(regions, constraint);
//        data_validation_view.createPromptBox(promptTitle, promptContent);
//        sheet.addValidationData(data_validation_view);
//        return sheet;
//    }
}
