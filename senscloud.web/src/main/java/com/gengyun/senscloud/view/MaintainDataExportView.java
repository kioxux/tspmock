package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.MaintainData;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class MaintainDataExportView{
//public class MaintainDataExportView extends AbstractXlsxStreamingView {
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "maintain_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<MaintainData> list = (List<MaintainData>) model.get("maintains");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.MAINT_DATA);
//
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAIN_NUM));
//                header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ASSET_CODE));
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_A));
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_PARENT_LOCAL));
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.POS_A));
//                header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_USER));
//                header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TASK_STATUS));
//                header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.START_TIME));
//                header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.COMP_TIME));
//                header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MAIN_TIME_OFF));
//
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (MaintainData data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getMaintainCode());
//                    courseRow.createCell(1).setCellValue(data.getAssetCode());
//                    courseRow.createCell(2).setCellValue(data.getAssetName());
//                    courseRow.createCell(3).setCellValue(data.getParentName());
//                    courseRow.createCell(4).setCellValue(data.getFacilityName());
//                    courseRow.createCell(5).setCellValue(data.getMaintainName());
//                    courseRow.createCell(6).setCellValue(data.getStatusName());
//                    if (data.getBeginTime() != null) {
//                        courseRow.createCell(7).setCellValue(sdf.format(new Date(data.getBeginTime().getTime())));
//                    } else {
//                        courseRow.createCell(7).setCellValue("");
//                    }
//                    if (data.getFinishedTime() != null) {
//                        courseRow.createCell(8).setCellValue(sdf.format(new Date(data.getFinishedTime().getTime())));
//                    } else {
//                        courseRow.createCell(8).setCellValue("");
//                    }
//                    if (data.getFinishedTime() != null && data.getBeginTime() != null) {
//                        Long tv = (data.getFinishedTime().getTime() - data.getBeginTime().getTime())
//                                / (1000 * 60);
//                        courseRow.createCell(9).setCellValue(Double.parseDouble(decimalFormat.format(tv)));
//                    } else {
//                        courseRow.createCell(9).setCellValue("");
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}