package com.gengyun.senscloud.view;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WorkFlowDataExportView extends AbstractXlsxStreamingView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            String fileName = "work_flow_export_" + sdf.format(new Date()) + ".xlsx";
            response.setHeader("Content-Disposition", "attachment; filename= " + fileName);
            List<Map<String, Object>> work_flow_info = RegexUtil.optNotNullListOrNew(model.get("work_flow_info"));
            Sheet sheet = workbook.createSheet("工单流程信息");
            sheet.setDefaultColumnWidth(24);
            Row header = sheet.createRow(0);
            if (!work_flow_info.isEmpty()) {
                int i = 0;
                Set<String> key = work_flow_info.get(0).keySet();
                String[] titleFields = key.toArray(new String[0]);
                for (int k = 0; k < titleFields.length; k++) {
                    String title = titleFields[k];
                    header.createCell(i).setCellValue(title);
                    i++;
                }
                int rowCount = 1;
                for (Map<String, Object> data : work_flow_info) {
                    Row courseRow = sheet.createRow(rowCount++);
                    for (int j = 0; j < titleFields.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields[j]), ""));
                    }
                }
            }
            List<Map<String, Object>> work_flow_node = RegexUtil.optNotNullListOrNew(model.get("work_flow_node"));
            // create excel xls sheet
            Sheet sheet2 = workbook.createSheet("工单流程节点信息");
            sheet2.setDefaultColumnWidth(24);
            // create header row
            Row header2 = sheet2.createRow(0);
            if (!work_flow_node.isEmpty()) {
                int a = 0;
                Set<String> key = work_flow_node.get(0).keySet();
                String[] titleFields1 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields1.length; k++) {
                    String title = titleFields1[k];
                    header2.createCell(a).setCellValue(title);
                    a++;
                }
                int cowCount = 1;
                for (Map<String, Object> data : work_flow_node) {
                    Row courseRow = sheet2.createRow(cowCount++);
                    for (int j = 0; j < titleFields1.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields1[j]), ""));
                    }
                }
            }
            List<Map<String, Object>> work_flow_node_colum = RegexUtil.optNotNullListOrNew(model.get("work_flow_node_colum"));
            Sheet sheet3 = workbook.createSheet("工单流程及节点字段信息");
            sheet3.setDefaultColumnWidth(24);
            Row header3 = sheet3.createRow(0);
            if (!work_flow_node_colum.isEmpty()) {
                int z = 0;
                Set<String> key = work_flow_node_colum.get(0).keySet();
                String[] titleFields3 = key.toArray(new String[0]);
                for (int x = 0; x < titleFields3.length; x++) {
                    String title = titleFields3[x];
                    header3.createCell(z).setCellValue(title);
                    z++;
                }
                int rowCount3 = 1;
                for (Map<String, Object> data : work_flow_node_colum) {
                    Row courseRow = sheet3.createRow(rowCount3++);
                    for (int j = 0; j < titleFields3.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields3[j]), ""));
                    }
                }
            }
            List<Map<String, Object>> work_flow_node_colum_ext = RegexUtil.optNotNullListOrNew(model.get("work_flow_node_colum_ext"));
            Sheet sheet4 = workbook.createSheet("工单流程及节点字段特殊属性信息");
            sheet4.setDefaultColumnWidth(24);
            Row header4 = sheet4.createRow(0);
            if (!work_flow_node_colum_ext.isEmpty()) {
                int d = 0;
                Set<String> key = work_flow_node_colum_ext.get(0).keySet();
                String[] titleFields4 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields4.length; k++) {
                    String title = titleFields4[k];
                    header4.createCell(d).setCellValue(title);
                    d++;
                }
                int rowCount4 = 1;
                for (Map<String, Object> data : work_flow_node_colum_ext) {
                    Row courseRow = sheet4.createRow(rowCount4++);
                    for (int j = 0; j < titleFields4.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields4[j]), ""));
                    }
                }
            }
            List<Map<String, Object>> work_flow_node_colum_linkage = RegexUtil.optNotNullListOrNew(model.get("work_flow_node_colum_linkage"));
            Sheet sheet5 = workbook.createSheet("工单流程及节点字段联动信息");
            sheet5.setDefaultColumnWidth(24);
            Row header5 = sheet5.createRow(0);
            if (!work_flow_node_colum_linkage.isEmpty()) {
                int e = 0;
                Set<String> key = work_flow_node_colum_linkage.get(0).keySet();
                String[] titleFields5 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields5.length; k++) {
                    String title = titleFields5[k];
                    header5.createCell(e).setCellValue(title);
                    e++;
                }
                int rowCount5 = 1;
                for (Map<String, Object> data : work_flow_node_colum_linkage) {
                    Row courseRow = sheet5.createRow(rowCount5++);
                    for (int j = 0; j < titleFields5.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields5[j]), ""));
                    }
                }
            }
            List<Map<String, Object>> work_flow_node_colum_linkage_codition = RegexUtil.optNotNullListOrNew(model.get("work_flow_node_colum_linkage_codition"));
            Sheet sheet6 = workbook.createSheet("工单流程及节点字段联动条件信息");
            sheet6.setDefaultColumnWidth(24);
            Row header6 = sheet6.createRow(0);
            if (!work_flow_node_colum_linkage_codition.isEmpty()) {
                int f = 0;
                Set<String> key = work_flow_node_colum_linkage_codition.get(0).keySet();
                String[] titleFields6 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields6.length; k++) {
                    String title = titleFields6[k];
                    header6.createCell(f).setCellValue(title);
                    f++;
                }
                int rowCount6 = 1;
                for (Map<String, Object> data : work_flow_node_colum_linkage_codition) {
                    Row courseRow = sheet6.createRow(rowCount6++);
                    for (int j = 0; j < titleFields6.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields6[j]), ""));
                    }
                }
            }

            List<Map<String, Object>> work_template = RegexUtil.optNotNullListOrNew(model.get("work_template"));
            Sheet sheet7 = workbook.createSheet("工单模板信息");
            sheet7.setDefaultColumnWidth(24);
            Row header7 = sheet7.createRow(0);
            if (!work_template.isEmpty()) {
                int g = 0;
                Set<String> key = work_template.get(0).keySet();
                String[] titleFields7 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields7.length; k++) {
                    String title = titleFields7[k];
                    header7.createCell(g).setCellValue(title);
                    g++;
                }
                int rowCount7 = 1;
                for (Map<String, Object> data : work_template) {
                    Row courseRow = sheet7.createRow(rowCount7++);
                    for (int j = 0; j < titleFields7.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields7[j]), ""));
                    }
                }
            }

            List<Map<String, Object>> work_template_colum_linkage_condition = RegexUtil.optNotNullListOrNew(model.get("work_template_colum_linkage_condition"));
            Sheet sheet8 = workbook.createSheet("工单模板字段联动条件信息");
            sheet8.setDefaultColumnWidth(24);
            Row header8 = sheet8.createRow(0);
            if (!work_template_colum_linkage_condition.isEmpty()) {
                int h = 0;
                Set<String> key = work_template_colum_linkage_condition.get(0).keySet();
                String[] titleFields8 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields8.length; k++) {
                    String title = titleFields8[k];
                    header8.createCell(h).setCellValue(title);
                    h++;
                }
                int rowCount8 = 1;
                for (Map<String, Object> data : work_template_colum_linkage_condition) {
                    Row courseRow = sheet8.createRow(rowCount8++);
                    for (int j = 0; j < titleFields8.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields8[j]), ""));
                    }
                }
            }

            List<Map<String, Object>> work_template_colum_linkage = RegexUtil.optNotNullListOrNew(model.get("work_template_colum_linkage"));
            Sheet sheet9 = workbook.createSheet("工单模板字段联动信息");
            sheet9.setDefaultColumnWidth(24);
            Row header9 = sheet9.createRow(0);
            if (!work_template_colum_linkage.isEmpty()) {
                int i = 0;
                Set<String> key = work_template_colum_linkage.get(0).keySet();
                String[] titleFields9 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields9.length; k++) {
                    String title = titleFields9[k];
                    header9.createCell(i).setCellValue(title);
                    i++;
                }
                int rowCount9 = 1;
                for (Map<String, Object> data : work_template_colum_linkage) {
                    Row courseRow = sheet9.createRow(rowCount9++);
                    for (int j = 0; j < titleFields9.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields9[j]), ""));
                    }
                }
            }

            List<Map<String, Object>> work_template_colum_ext = RegexUtil.optNotNullListOrNew(model.get("work_template_colum_ext"));
            Sheet sheet10 = workbook.createSheet("工单模板字段特殊属性信息");
            sheet10.setDefaultColumnWidth(24);
            Row header10 = sheet10.createRow(0);
            if (!work_template_colum_ext.isEmpty()) {
                int i = 0;
                Set<String> key = work_template_colum_ext.get(0).keySet();
                String[] titleFields10 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields10.length; k++) {
                    String title = titleFields10[k];
                    header10.createCell(i).setCellValue(title);
                    i++;
                }
                int rowCount10 = 1;
                for (Map<String, Object> data : work_template_colum_ext) {
                    Row courseRow = sheet10.createRow(rowCount10++);
                    for (int j = 0; j < titleFields10.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields10[j]), ""));
                    }
                }
            }

            List<Map<String, Object>> work_template_colum = RegexUtil.optNotNullListOrNew(model.get("work_template_colum"));
            Sheet sheet11 = workbook.createSheet("工单模板字段信息");
            sheet11.setDefaultColumnWidth(24);
            Row header11 = sheet11.createRow(0);
            if (!work_template_colum.isEmpty()) {
                int l = 0;
                Set<String> key = work_template_colum.get(0).keySet();
                String[] titleFields11 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields11.length; k++) {
                    String title = titleFields11[k];
                    header11.createCell(l).setCellValue(title);
                    l++;
                }
                int rowCount11 = 1;
                for (Map<String, Object> data : work_template_colum) {
                    Row courseRow = sheet11.createRow(rowCount11++);
                    for (int j = 0; j < titleFields11.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields11[j]), ""));
                    }
                }
            }

            List<Map<String, Object>> data_source = RegexUtil.optNotNullListOrNew(model.get("data_source"));
            Sheet sheet12 = workbook.createSheet("工单数据源信息");
            sheet12.setDefaultColumnWidth(24);
            Row header12 = sheet12.createRow(0);
            if (!data_source.isEmpty()) {
                int m = 0;
                Set<String> key = data_source.get(0).keySet();
                String[] titleFields12 = key.toArray(new String[0]);
                for (int k = 0; k < titleFields12.length; k++) {
                    String title = titleFields12[k];
                    header12.createCell(m).setCellValue(title);
                    m++;
                }
                int rowCount12 = 1;
                for (Map<String, Object> data : data_source) {
                    Row courseRow = sheet12.createRow(rowCount12++);
                    for (int j = 0; j < titleFields12.length; j++) {
                        courseRow.createCell(j).setCellValue(RegexUtil.optStrOrVal(data.get(titleFields12[j]), ""));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
