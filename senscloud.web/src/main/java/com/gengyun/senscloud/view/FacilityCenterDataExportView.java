package com.gengyun.senscloud.view;

//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysFacilityResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class FacilityCenterDataExportView {
//public class FacilityCenterDataExportView extends AbstractXlsxStreamingView {
//
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "facility_center_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String excelName = selectOptionService.getLanguageInfo(LangConstant.MSG_LOCAL_DATA);
//            List<AnalysFacilityResult> list = (List<AnalysFacilityResult>) model.get("facilityAnalys");
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(excelName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.L_CODE));
//                header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.LOCATION_LON));
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.FAILUR_RATE));
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_EFF));
//
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAINT_QUANT));//计划保养总数
//                header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NUM_MC));//保养完成次数
//                header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MC_R)+ "(%)");//保养完成率
//
//                header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.AVG_TIME_FAILURE));
//                header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.FAULT_HAND_CAPA));
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (AnalysFacilityResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getFacilityCode());
//                    courseRow.createCell(1).setCellValue(data.getFacilityName());
//                    courseRow.createCell(2).setCellValue(data.getFailureRate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getFailureRate() * 100)));
//                    courseRow.createCell(3).setCellValue(data.getMaintainRate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getMaintainRate() * 100)));
//
//                    courseRow.createCell(4).setCellValue(data.getPlanMaintainTotal());
//                    courseRow.createCell(5).setCellValue(data.getFinishedMaintainTotal());
//                    courseRow.createCell(6).setCellValue(data.getMaintainFinishedRate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getMaintainFinishedRate() * 100)));
//
//                    courseRow.createCell(7).setCellValue(data.getTimeBetweenFailures() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getTimeBetweenFailures()/60)));
//                    courseRow.createCell(8).setCellValue(data.getAveRepairTime() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getAveRepairTime()/60)));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
