package com.gengyun.senscloud.view;

//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysFacilityRepairMaintainResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class FacilitySupplierRepairMaintianDataExportView {
//public class FacilitySupplierRepairMaintianDataExportView extends AbstractXlsxStreamingView {
//
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "repair_maintain_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String excelName = selectOptionService.getLanguageInfo(LangConstant.MSG_MANAGE_LOCAL);
//            String preType = selectOptionService.getLanguageInfo(LangConstant.POS_A);
//            List<AnalysFacilityRepairMaintainResult> list = (List<AnalysFacilityRepairMaintainResult>) model.get("repairAnalys");
//            String analysType = (String) model.get("analysType");
//            switch (analysType) {
//                case "supplier":
//                    excelName = selectOptionService.getLanguageInfo(LangConstant.MSG_MANAGE_SUPP);
//                    preType = selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A);
//                    break;
//            }
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(excelName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(preType + selectOptionService.getLanguageInfo(LangConstant.NAME_A));
//            header.createCell(1).setCellValue(preType + selectOptionService.getLanguageInfo(LangConstant.CODE_CODE));
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_A));
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_NUMBER_PLAT));
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.AVERAGE_MAINTENANCE_LIMIT));
//            header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NUM_MC));
//            header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MAIN_ALL_MIN));
//            header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OVER_MAIN_NUMS));
//            header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_RUN_REPAIR_MIN));
//            header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_TOTLA_TIMES));
//            header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_REPIAR_TOTAL_TIME));
//            header.createCell(11).setCellValue(selectOptionService.getLanguageInfo(LangConstant.REPAIR_MINUTE));
//            header.createCell(12).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_NOF));
//            header.createCell(13).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_TOTAL_FAIL_MIN));
//            header.createCell(14).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_EFFEC_MAIN));
//
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (AnalysFacilityRepairMaintainResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getAnalysName());
//                    courseRow.createCell(1).setCellValue(data.getAnalysCode());
//                    courseRow.createCell(2).setCellValue(data.getStrname());
//                    courseRow.createCell(3).setCellValue(data.getAsset_count());
//                    courseRow.createCell(4).setCellValue(Double.parseDouble(decimalFormat.format(data.getMaintainMinutePreTime())));
//                    courseRow.createCell(5).setCellValue(data.getFinishedMaintainTimes());
//                    courseRow.createCell(6).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalMaintainMinute())));
//                    courseRow.createCell(7).setCellValue(data.getNoFinishedMaintainTimes());
//                    courseRow.createCell(8).setCellValue(Double.parseDouble(decimalFormat.format(data.getRunTimeRepairMinutePreTime())));
//                    courseRow.createCell(9).setCellValue(data.getRunTimeRepairTimes());
//                    courseRow.createCell(10).setCellValue(Double.parseDouble(decimalFormat.format(data.getRunTimeTotalRepairMinute())));
//                    courseRow.createCell(11).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalRepairMinutePreTime())));
//                    courseRow.createCell(12).setCellValue(data.getRepairTimes());
//                    courseRow.createCell(13).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalRepairMinute())));
//                    courseRow.createCell(14).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalMaintainOfRepairRate() * 100)));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
