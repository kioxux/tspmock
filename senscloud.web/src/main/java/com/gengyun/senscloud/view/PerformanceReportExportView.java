package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class PerformanceReportExportView{
//public class PerformanceReportExportView extends AbstractXlsxStreamingView {
//
//    /**
//     * Application-provided subclasses must implement this method to populate
//     * the Excel workbook document, given the model.
//     *
//     * @param model    the model Map
//     * @param workbook the Excel workbook to populate
//     * @param request  in case we need locale etc. Shouldn't look at attributes.
//     * @param response in case we need to set cookies. Shouldn't write to it.
//     */
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "performance_report_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("dataList");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.PERFORMANCE_REPORT_R);//绩效报告
//
//            // create excel xls sheet
//            Sheet sheet = StringUtils.isBlank(sheetName)?workbook.createSheet():workbook.createSheet(sheetName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.USER_GROUP));//用户组
//            header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PERFORMANCE_PEOPLE_COUNTING));//考核人数
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SUBJECTIVE_AVG_SCORE));//主观平均分
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.OBJECTIVE_AVG_SCORE));//客观平均分
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_AVG_POINTS));//平均分
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                for (Map<String, Object> data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue((String)data.get("group_name"));
//                    courseRow.createCell(1).setCellValue((Long)data.get("num"));
//                    courseRow.createCell(2).setCellValue(((BigDecimal)data.get("subjective_avg_score")).toString());
//                    courseRow.createCell(3).setCellValue(((BigDecimal)data.get("objective_avg_score")).toString());
//                    courseRow.createCell(4).setCellValue(((BigDecimal)data.get("total_avg_points")).toString());
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
