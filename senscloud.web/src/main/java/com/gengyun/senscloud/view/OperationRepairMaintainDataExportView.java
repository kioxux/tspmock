package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysFacilityRepairMaintainResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class OperationRepairMaintainDataExportView{
//public class OperationRepairMaintainDataExportView extends AbstractXlsxStreamingView {
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            SelectOptionService selectOptionService = (SelectOptionService) model.get("selectOptionService");
//            String fileName = "employee_repair_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            String excelName = selectOptionService.getLanguageInfo(LangConstant.DATA_FOR_USER);//员工维护数据
//            List<AnalysFacilityRepairMaintainResult> list = (List<AnalysFacilityRepairMaintainResult>) model.get("repairMaintainAnalys");
//
//
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(excelName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NAME_NAME));//姓名
//                header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.JOB_ID));//工号
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SUBORDINATE_GROUP));//所属组
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.REPAIR_MINUTE));//平均维修时效
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_NUM_OF_RE));//维修总次数
//                header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_MAIN_TIME_A));//维修总时间
//                header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.AVERAGE_MAINTENANCE_LIMIT));//平均保养时效
//                header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NUM_MC));//保养完成次数
//                header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MAIN_ALL_MIN));//保养总用时
//                header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OVER_MAIN_TIMES));//超期未保养次数
//                header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAINT_QUANT));//计划保养总数
//                header.createCell(11).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MC_R) + "(%)");//保养完成率
//                header.createCell(12).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TIME_LAP));//到场时效
//                header.createCell(13).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WORK_SAT));//工作饱和度
//                header.createCell(14).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_EFFEC_MAIN));//保养时效占比
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (AnalysFacilityRepairMaintainResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getUsername());
//                    courseRow.createCell(1).setCellValue(data.getAnalysCode());
//                    courseRow.createCell(2).setCellValue(data.getTitle());
//                    courseRow.createCell(3).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalRepairMinutePreTime())));
//                    courseRow.createCell(4).setCellValue(data.getRepairTimes());
//                    courseRow.createCell(5).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalRepairMinute())));
//                    courseRow.createCell(6).setCellValue(Double.parseDouble(decimalFormat.format(data.getMaintainMinutePreTime())));
//                    courseRow.createCell(7).setCellValue(data.getFinishedMaintainTimes());
//                    courseRow.createCell(8).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalMaintainMinute())));
//                    courseRow.createCell(9).setCellValue(data.getNoFinishedMaintainTimes());
//                    courseRow.createCell(10).setCellValue(data.getPlanMaintainTimes());
//                    courseRow.createCell(11).setCellValue(Double.parseDouble((decimalFormat.format(data.getMaintainFinishedRate() * 100))));
//                    courseRow.createCell(12).setCellValue(Double.parseDouble(decimalFormat.format(data.getArriveMinutePreTime())));
//                    courseRow.createCell(13).setCellValue(Double.parseDouble(decimalFormat.format(data.getTotalMinute())));
//                    courseRow.createCell(14).setCellValue(Double.parseDouble((decimalFormat.format(data.getTotalMaintainOfRepairRate() * 100))));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
