package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.BomData;
//import com.gengyun.senscloud.response.AnalysRepairWorkResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by Administrator on 2018/6/13.
// */
public class AnalysBomDataView {
//public class AnalysBomDataView extends AbstractXlsxStreamingView {
//
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "bom_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<BomData> list = (List<BomData>) model.get("bomAnalys");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.MSG_STATIC_BOM_DATA);
//            String bom_model = selectOptionService.getLanguageInfo(LangConstant.PART_CATEGORY);
//            String bom_name = selectOptionService.getLanguageInfo(LangConstant.PART_NAME);
//
//            if (list != null && list.size() > 0) {
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(bom_model);
//                header.createCell(1).setCellValue(bom_name);
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.STOCK_QUANTITY));
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_USE_NUM));
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MAINTAIN_USE_NUM));
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (BomData data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getBom_model());
//                    courseRow.createCell(1).setCellValue(data.getBom_name());
//                    courseRow.createCell(2).setCellValue(data.getQuantity());
//                    courseRow.createCell(3).setCellValue(data.getRepairCount());
//                    courseRow.createCell(4).setCellValue(data.getMaintainCount());
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
