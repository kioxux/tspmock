package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysSupplierResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class SupplierDataExportView{
//public class SupplierDataExportView extends AbstractXlsxStreamingView {
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "supplier_export_" + sdf.format(new Date()) + ".xlsx";
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            String excelName = selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A);//供应商数据
//            List<AnalysSupplierResult> list = (List<AnalysSupplierResult>) model.get("supplierAnalys");
//
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(excelName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.VMI_CU_CODE));//供应商代码
//            header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_NAME));//供应商名称
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_FAIL_RATE));//总的故障率
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PANAR_LINE_FAIL_RATE));//平面线故障率
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.EXPAN_MACH_FAIL_RATE));//伸缩机故障率
//            header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TURN_MAC_FAIL_RATE));//转弯机故障率
//            header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.CROSS_BAND_FAIL_RATE));//交叉带故障率
//            header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MOTOR_FR)+"(%)");//电机故障率
//            header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ELE_FR)+"(%)");//电器故障率
//            header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ROLLER_FR)+"(%)");//滚筒故障率
//            header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.BEAR_FR)+"(%)");//轴承故障率
//            header.createCell(11).setCellValue(selectOptionService.getLanguageInfo(LangConstant.BELT_FR)+"(%)");//皮带故障率
//            DecimalFormat decimalFormat = new DecimalFormat("#.00");
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                for (AnalysSupplierResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getSupplier());
//                    courseRow.createCell(1).setCellValue(data.getSupplier_name());
//                    courseRow.createCell(2).setCellValue(data.getFault_rate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getFault_rate() * 100)));
//                    courseRow.createCell(3).setCellValue(data.getPmx_fault_rate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getPmx_fault_rate() * 100)));
//                    courseRow.createCell(4).setCellValue(data.getSsj_fault_rate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getSsj_fault_rate() * 100)));
//                    courseRow.createCell(5).setCellValue(data.getZwj_fault_rate() == null ? 0 : Double.parseDouble(decimalFormat.format(data.getZwj_fault_rate() * 100)));
//                    courseRow.createCell(6).setCellValue(data.getJcd_fault_rate() == null ? 0: Double.parseDouble(decimalFormat.format(data.getJcd_fault_rate() * 100)));
//                    courseRow.createCell(7).setCellValue(data.getEngine_fault_rate() == null ? 0: Double.parseDouble(decimalFormat.format(data.getEngine_fault_rate() * 100)));
//                    courseRow.createCell(8).setCellValue(data.getDevice_fault_rate() == null ? 0: Double.parseDouble(decimalFormat.format(data.getDevice_fault_rate() * 100)));
//                    courseRow.createCell(9).setCellValue(data.getRoll_fault_rate() == null ? 0: Double.parseDouble(decimalFormat.format(data.getRoll_fault_rate() * 100)));
//                    courseRow.createCell(10).setCellValue(data.getBearing_fault_rate() == null ? 0: Double.parseDouble(decimalFormat.format(data.getBearing_fault_rate() * 100)));
//                    courseRow.createCell(11).setCellValue(data.getBelt_fault_rate() == null ? 0: Double.parseDouble(decimalFormat.format(data.getBelt_fault_rate() * 100)));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
