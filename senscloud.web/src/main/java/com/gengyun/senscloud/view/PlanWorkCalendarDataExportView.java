package com.gengyun.senscloud.view;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PlanWorkCalendarDataExportView extends AbstractXlsxStreamingView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            String fileName = "plan_work_calendar_export_" + sdf.format(new Date()) + ".xlsx";
            response.setHeader("Content-Disposition", "attachment; filename= " + fileName);
            List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew (model.get("rows"));
            SelectOptionService selectOptionService = SpringContextHolder.getBean("selectOptionServiceImpl");
            PlanWorkService planWorkService = SpringContextHolder.getBean("planWorkServiceImpl");
            MethodParam methodParam = (MethodParam) model.get("methodParam");
            Sheet sheet = workbook.createSheet("维保计划");
            sheet.setDefaultColumnWidth(24);
            Row header = sheet.createRow(0);
            int i = 0;
            String[] titleFields = {"行事历编号", "维保对象", "工单类型", "紧急程度", "计划开始时间", "计划截止时间", "自动生成工单",
                    "工单生成时间点", "负责人", "计划名称", "编号", "位置", "维护频率", "工作优先级"};
            for (int k = 0; k < titleFields.length; k++) {
                String title = titleFields[k];
                header.createCell(i).setCellValue(title);
                i++;
            }
            int rowCount = 1;
            for (Map<String, Object> data : list) {
                Row courseRow = sheet.createRow(rowCount++);
                Map<String, Object> info = RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(methodParam.getSchemaName(), RegexUtil.optStrOrNull(data.get("work_calendar_code"))));
                Map<String, Object> bodyPropertyMap = DataChangeUtil.scdObjToMapOrNew(info.get("body_property"));
                if (bodyPropertyMap.containsKey("repair_rate")) {
                    data.put("repair_rate", bodyPropertyMap.get("repair_rate"));
                }
                if (bodyPropertyMap.containsKey("work_level")) {
                    data.put("work_level", bodyPropertyMap.get("work_level"));
                }
                courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(data.get("asset_name"), ""));
                courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(data.get("work_type_name"), ""));
                courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(data.get("priority_level_name"), ""));
                courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(data.get("occur_time"), ""));
                courseRow.createCell(5).setCellValue(RegexUtil.optStrOrVal(data.get("deadline_time"), ""));
                courseRow.createCell(6).setCellValue(RegexUtil.optStrOrVal(data.get("auto_generate_bill_name"), ""));
                courseRow.createCell(7).setCellValue(RegexUtil.optStrOrVal(data.get("create_time"), ""));
                courseRow.createCell(8).setCellValue(RegexUtil.optStrOrVal(data.get("user_name"), ""));
                courseRow.createCell(9).setCellValue(RegexUtil.optStrOrVal(data.get("title"), ""));
                courseRow.createCell(10).setCellValue(RegexUtil.optStrOrVal(data.get("plan_code"), ""));
                courseRow.createCell(11).setCellValue(RegexUtil.optStrOrVal(data.get("position_name"), ""));
                courseRow.createCell(12).setCellValue(RegexUtil.optStrOrVal(selectOptionService.getSelectOptionAttrByCode(methodParam, "repair_rate", RegexUtil.optStrOrVal(data.get("repair_rate"), "is_no_value"), "text"), ""));
                courseRow.createCell(13).setCellValue(RegexUtil.optStrOrVal(selectOptionService.getSelectOptionAttrByCode(methodParam, "work_level", RegexUtil.optStrOrVal(data.get("work_level"), "is_no_value"), "text"), ""));
            }
            List<Map<String, Object>> cows_list = (List<Map<String, Object>>) model.get("cows");
            // create excel xls sheet
            Sheet sheet2 = workbook.createSheet("任务项");
            sheet2.setDefaultColumnWidth(24);
            // create header row
            Row header2 = sheet2.createRow(0);
            int a = 0;
            String[] titleFields1 = {"行事历编号", "任务项编号", "任务项名称", "结果类型", "组名称", "模板编号", "任务描述"};
            for (int k = 0; k < titleFields1.length; k++) {
                String title = titleFields1[k];
                header2.createCell(a).setCellValue(title);
                a++;
            }
            int cowCount = 1;
            if (RegexUtil.optNotNull(cows_list).isPresent()) {
                for (Map<String, Object> data : cows_list) {
                    if (RegexUtil.optIsPresentStr(data.get("body_property"))) {
                        JSONObject body_property = JSONObject.fromObject(data.get("body_property"));
                        if (body_property.containsKey("task_list")) {
                            JSONArray task_list = body_property.getJSONArray("task_list");
                            if (RegexUtil.optNotNull(task_list).isPresent() && task_list.size() > 0) {
                                for (int l = 0; l < task_list.size(); l++) {
                                    Row courseRow = sheet2.createRow(cowCount++);
                                    JSONObject arr = task_list.getJSONObject(l);
                                    courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                                    courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(arr.get("task_item_code"), ""));
                                    courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(arr.get("task_item_name"), ""));
                                    courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(arr.get("result_type_name"), ""));
                                    courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(arr.get("group_name"), ""));
                                    courseRow.createCell(5).setCellValue(RegexUtil.optStrOrVal(arr.get("task_template_code"), ""));
                                    courseRow.createCell(6).setCellValue(RegexUtil.optStrOrVal(arr.get("requirements"), ""));
                                }
                            }
                        }
                    }
                }
            }
            List<Map<String, Object>> dows_List = (List<Map<String, Object>>) model.get("dows");
            Sheet sheet3 = workbook.createSheet("日志");
            sheet3.setDefaultColumnWidth(24);
            Row header3 = sheet3.createRow(0);
            int z = 0;
            String[] titleFields3 = {"行事历编号", "编号", "时间", "内容", "操作人"};
            for (int x = 0; x < titleFields3.length; x++) {
                String title = titleFields3[x];
                header3.createCell(z).setCellValue(title);
                z++;
            }
            int rowCount3 = 1;
            for (Map<String, Object> data : dows_List) {
                Row courseRow = sheet3.createRow(rowCount3++);
                courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(data.get("plan_code"), ""));
                courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(data.get("create_time"), ""));
                courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(data.get("remark"), ""));
                courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(data.get("create_user_name"), ""));
            }

            Sheet sheet4 = workbook.createSheet("职安健");
            sheet4.setDefaultColumnWidth(24);
            Row header4 = sheet4.createRow(0);
            int d = 0;
            String[] titleFields4 = {"行事历编号", "编号", "风险评估", "预防措施", "职安健审批", "创建时间"};
            for (int k = 0; k < titleFields4.length; k++) {
                String title = titleFields4[k];
                header4.createCell(d).setCellValue(title);
                d++;
            }
            int rowCount4 = 1;
            if (RegexUtil.optNotNull(cows_list).isPresent()) {
                for (Map<String, Object> data : cows_list) {
                    if (RegexUtil.optIsPresentStr(data.get("body_property"))) {
                        JSONObject body_property = JSONObject.fromObject(data.get("body_property"));
                        if (body_property.containsKey("safe_list")) {
                            JSONArray safe_list = body_property.getJSONArray("safe_list");
                            if (RegexUtil.optNotNull(safe_list).isPresent() && safe_list.size() > 0) {
                                for (int l = 0; l < safe_list.size(); l++) {
                                    Row courseRow = sheet4.createRow(rowCount4++);
                                    JSONObject arr = safe_list.getJSONObject(l);
                                    courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                                    courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(arr.get("security_item_code"), ""));
                                    courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(arr.get("security_item_name"), ""));
                                    courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(arr.get("rask_note"), ""));
                                    courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(arr.get("result_type_name"), ""));
                                    courseRow.createCell(5).setCellValue(RegexUtil.optStrOrVal(arr.get("create_time"), ""));
                                }
                            }
                        }
                    }
                }
            }

            Sheet sheet5 = workbook.createSheet("所需资源-备件材料");
            sheet5.setDefaultColumnWidth(24);
            Row header5 = sheet5.createRow(0);
            int e = 0;
            String[] titleFields5 = {"行事历编号", "备件名称", "备件类型", "备件型号", "物料编码", "数量"};
            for (int k = 0; k < titleFields5.length; k++) {
                String title = titleFields5[k];
                header5.createCell(e).setCellValue(title);
                e++;
            }
            int rowCount5 = 1;
            if (RegexUtil.optNotNull(cows_list).isPresent()) {
                for (Map<String, Object> data : cows_list) {
                    if (RegexUtil.optIsPresentStr(data.get("body_property"))) {
                        JSONObject body_property = JSONObject.fromObject(data.get("body_property"));
                        if (body_property.containsKey("bom_list")) {
                            JSONArray bom_list = body_property.getJSONArray("bom_list");
                            if (RegexUtil.optNotNull(bom_list).isPresent() && bom_list.size() > 0) {
                                for (int l = 0; l < bom_list.size(); l++) {
                                    Row courseRow = sheet5.createRow(rowCount5++);
                                    JSONObject arr = bom_list.getJSONObject(l);
                                    courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                                    courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(arr.get("bom_name"), ""));
                                    courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(arr.get("type_name"), ""));
                                    courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(arr.get("bom_model"), ""));
                                    courseRow.createCell(4).setCellValue(RegexUtil.optStrOrVal(arr.get("material_code"), ""));
                                    courseRow.createCell(5).setCellValue(RegexUtil.optStrOrVal(arr.get("count"), ""));
                                }
                            }
                        }
                    }
                }
            }

            Sheet sheet6 = workbook.createSheet("所需资源-工具");
            sheet6.setDefaultColumnWidth(24);
            Row header6 = sheet6.createRow(0);
            int f = 0;
            String[] titleFields6 = {"行事历编号", "工具名称", "工具类型", "数量"};
            for (int k = 0; k < titleFields6.length; k++) {
                String title = titleFields6[k];
                header6.createCell(f).setCellValue(title);
                f++;
            }
            int rowCount6 = 1;
            if (RegexUtil.optNotNull(cows_list).isPresent()) {
                for (Map<String, Object> data : cows_list) {
                    if (RegexUtil.optIsPresentStr(data.get("body_property"))) {
                        JSONObject body_property = JSONObject.fromObject(data.get("body_property"));
                        if (body_property.containsKey("tools_list")) {
                            JSONArray tools_list = body_property.getJSONArray("tools_list");
                            if (RegexUtil.optNotNull(tools_list).isPresent() && tools_list.size() > 0) {
                                for (int l = 0; l < tools_list.size(); l++) {
                                    Row courseRow = sheet6.createRow(rowCount6++);
                                    JSONObject arr = tools_list.getJSONObject(l);
                                    courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                                    courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(arr.get("tools_name"), ""));
                                    courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(arr.get("type_name"), ""));
                                    courseRow.createCell(3).setCellValue(RegexUtil.optStrOrVal(arr.get("count"), ""));
                                }
                            }
                        }
                    }
                }
            }

            Sheet sheet7 = workbook.createSheet("所需资源-费用");
            sheet7.setDefaultColumnWidth(24);
            Row header7 = sheet7.createRow(0);
            int g = 0;
            String[] titleFields7 = {"行事历编号", "费用类别", "金额"};
            for (int k = 0; k < titleFields7.length; k++) {
                String title = titleFields7[k];
                header7.createCell(g).setCellValue(title);
                g++;
            }
            int rowCount7 = 1;
            if (RegexUtil.optNotNull(cows_list).isPresent()) {
                for (Map<String, Object> data : cows_list) {
                    if (RegexUtil.optIsPresentStr(data.get("body_property"))) {
                        JSONObject body_property = JSONObject.fromObject(data.get("body_property"));
                        if (body_property.containsKey("fee_list")) {
                            JSONArray fee_list = body_property.getJSONArray("fee_list");
                            if (RegexUtil.optNotNull(fee_list).isPresent() && fee_list.size() > 0) {
                                for (int l = 0; l < fee_list.size(); l++) {
                                    Row courseRow = sheet7.createRow(rowCount7++);
                                    JSONObject arr = fee_list.getJSONObject(l);
                                    courseRow.createCell(0).setCellValue(RegexUtil.optStrOrVal(data.get("work_calendar_code"), ""));
                                    courseRow.createCell(1).setCellValue(RegexUtil.optStrOrVal(selectOptionService.getSelectOptionAttrByCode(methodParam, "fee_category", RegexUtil.optStrOrVal(arr.get("type_name"), "is_no_value"), "text"), ""));
                                    courseRow.createCell(2).setCellValue(RegexUtil.optStrOrVal(arr.get("fee"), ""));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
