package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysRepairWorkResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//public class AnalysMaintainDataExportView extends AbstractXlsxStreamingView {
public class AnalysMaintainDataExportView{
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "maintain_export_" + sdf.format(new Date()) + ".xlsx";
//
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<AnalysRepairWorkResult> list = (List<AnalysRepairWorkResult>) model.get("maintainAnalys");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String analysType = (String) model.get("analysType");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.MSG_MAINTAIN_STATIC);
//            String analysName = selectOptionService.getLanguageInfo(LangConstant.POS_A);
//            String analysCode = selectOptionService.getLanguageInfo(LangConstant.POSITION_CODE);
//
//            switch (analysType) {
//                case "device":
//                    sheetName += selectOptionService.getLanguageInfo(LangConstant.MSG_BRACKETS_ASSET);
//                    analysName = selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_A);
//                    analysCode = selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_AE);
//                    break;
//                case "user":
//                    sheetName += selectOptionService.getLanguageInfo(LangConstant.MSG_BRACKETS_MAIN_MAN);
//                    analysName = selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_USER);
//                    analysCode = selectOptionService.getLanguageInfo(LangConstant.MSG_MAINTAIN_CODE);
//                    break;
//                case "facility":
//                    sheetName += selectOptionService.getLanguageInfo(LangConstant.MSG_BRACKETS_SITE);
//                    analysName = selectOptionService.getLanguageInfo(LangConstant.POS_A);
//                    analysCode = selectOptionService.getLanguageInfo(LangConstant.POSITION_CODE);
//                    break;
//            }
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(analysName);
//                header.createCell(1).setCellValue(analysCode);
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.AVERAGE_MAINTENANCE_LIMIT));
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.AUDIT_MINUTE));
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_TIME));
//
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (AnalysRepairWorkResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getAnalysName());
//                    courseRow.createCell(1).setCellValue(data.getAnalysCode());
//                    courseRow.createCell(2).setCellValue(Double.parseDouble(decimalFormat.format(data.getDealMinute())));
//                    courseRow.createCell(3).setCellValue(Double.parseDouble(decimalFormat.format(data.getAuditMinute())));
//                    courseRow.createCell(4).setCellValue(data.getDoTimes());
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
