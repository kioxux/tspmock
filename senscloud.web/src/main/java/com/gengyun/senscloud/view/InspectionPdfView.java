package com.gengyun.senscloud.view;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.AssetPosition;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.itextpdf.barcodes.BarcodeQRCode;
//import com.itextpdf.text.Chunk;
//import com.itextpdf.text.Element;
//import com.itextpdf.text.PageSize;
//import com.itextpdf.text.Phrase;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import org.springframework.web.servlet.view.AbstractView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.awt.*;
//import java.util.List;
//import java.util.Map;
//
public class InspectionPdfView{
//public class InspectionPdfView extends AbstractView {
//
//    public static PdfPCell getPdfPCell(Phrase phrase, Integer horizontalAlignment) {
//        PdfPCell cell = null;
//        if (phrase != null) {
//            cell = new PdfPCell(phrase);
//        } else {
//            cell = new PdfPCell();
//        }
//        if (horizontalAlignment != null) {
//            cell.setHorizontalAlignment(horizontalAlignment);
//        }
//        cell.setBorder(0);
//        return cell;
//    }
//    //自定义table方法
//    public static PdfPTable getPdfPTable(int column, int[] tableWidth) throws Exception {
//        PdfPTable table = new PdfPTable(column);
//        table.setWidths(tableWidth);
//        table.getDefaultCell().setBorder(0);
//        table.setWidthPercentage(100);
//        return table;
//    }
//    //自定义长段落cell方法
//    public static PdfPCell getPdfPCell(Phrase phrase) {
//        return getPdfPCell(phrase, null);
//    }
//
//    @Override
//    protected void renderMergedOutputModel(Map<String, Object> model,
//                                           HttpServletRequest request,
//                                           HttpServletResponse response) throws Exception {
//        response.setContentType("application/x-download");//下面三行是关键代码，处理乱码问题
//        response.setCharacterEncoding("utf-8");
//        response.setHeader("Content-Disposition", "attachment; filename=Inspection.pdf");
//        //IText API
//        com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4);
//        com.itextpdf.text.pdf.PdfWriter.getInstance(document, response.getOutputStream());
//        try {
//
//            //设置字体
//            com.itextpdf.text.pdf.BaseFont bfChinese = com.itextpdf.text.pdf.BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", com.itextpdf.text.pdf.BaseFont.NOT_EMBEDDED);
//            com.itextpdf.text.Font FontChinese24 = new com.itextpdf.text.Font(bfChinese, 24, com.itextpdf.text.Font.BOLD);
//            com.itextpdf.text.Font FontChinese18 = new com.itextpdf.text.Font(bfChinese, 18, com.itextpdf.text.Font.BOLD);
//            com.itextpdf.text.Font FontChinese16 = new com.itextpdf.text.Font(bfChinese, 16, com.itextpdf.text.Font.NORMAL);
//            com.itextpdf.text.Font FontChinese12 = new com.itextpdf.text.Font(bfChinese, 12, com.itextpdf.text.Font.NORMAL);
//            com.itextpdf.text.Font FontChinese11Bold = new com.itextpdf.text.Font(bfChinese, 11, com.itextpdf.text.Font.BOLD);
//            com.itextpdf.text.Font FontChinese11 = new com.itextpdf.text.Font(bfChinese, 11, com.itextpdf.text.Font.ITALIC);
//            com.itextpdf.text.Font FontChinese11Normal = new com.itextpdf.text.Font(bfChinese, 10, com.itextpdf.text.Font.NORMAL);
//            com.itextpdf.text.Font FontChinese11Normal2 = new com.itextpdf.text.Font(bfChinese, 8, com.itextpdf.text.Font.NORMAL);
//
//            List<AssetPosition> AssetPositionDataList = (List<AssetPosition>) model.get("assetPositionList");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            //左右表格计数器，
//            int index =2;
//            //计数器（最后未满1000条数据用到）;
//            int i=1;
//            //换行用空段落
//            com.itextpdf.text.Paragraph blankRow1 = new com.itextpdf.text.Paragraph(18f, " ", FontChinese18);
//            //打开
//            document.open();
//            //左边总表格
//            PdfPTable tableall1 = new PdfPTable(1);
//            //整体大表格
//            PdfPTable tableall =  getPdfPTable(3, new int[]{48,4,48 });
//            //右边大表格
//            PdfPTable tableall2 = new PdfPTable(1);
//            //设置左边边表格宽度占所有空白宽度的百分比
//            tableall1.setWidthPercentage(45);
//            //设置右边表格宽度占所有空白宽度的百分比
//            tableall2.setWidthPercentage(45);
//            //设置大表格表格宽度占所有空白宽度的百分比
//            tableall.setWidthPercentage(100);
//
//            for (AssetPosition ap : AssetPositionDataList) {
//                //二维码模板的抬头表 存放公司名称
//                PdfPTable table1 = new PdfPTable(1);
//                //二维码模板的中间表 存放二维码和设备信息
//                PdfPTable table2 = getPdfPTable(2, new int[]{40,60 });
//                //二维码模板最下方表 存放位置信息
//                PdfPTable table3 = new PdfPTable(4);
//                //此表功能是将上面所有表放到一个表里
//                PdfPTable table4 = new PdfPTable(1);
//                //设置表的宽度百分比
//                table4.setWidthPercentage(30);
//                PdfPCell cell11 = null;
//                String code = ap.getPosition_code();
//                if (RegexUtil.isNull(code)) {
//                    cell11 = new PdfPCell(new com.itextpdf.text.Paragraph("无效的编码", FontChinese24));
//                    table1.addCell(cell11);
//
//                    continue;
//                }else {
//                    //不为空将公司名称写入cell
//                    cell11 = new PdfPCell(new com.itextpdf.text.Paragraph(AuthService.getCompany(request).getCompany_name(), FontChinese16));
//                    //设置段落垂直居中
//                    cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                    //设置段落水平居中
//                    cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    //设置段落无边框
//                    cell11.setBorder(0);
//                    //设置段落高度
//                    cell11.setFixedHeight(25);
//                    //将cell添加到抬头表中
//                    table1.getDefaultCell().setBorder(0);
//                    table1.addCell(cell11);
//                }
//                //将编号信息生成二维码图片流
//                BarcodeQRCode barcodeQRCode = new BarcodeQRCode(code);
//                //将图片流转换成pdf图片格式
//                com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(barcodeQRCode.createAwtImage(Color.BLACK, Color.WHITE), Color.WHITE);
//                //设置二维码图片的宽高
//                image.scaleAbsolute(80, 80);
//                //将二维码图片放入段落
//                PdfPCell table7C1 = new PdfPCell(image);
//                table7C1.setHorizontalAlignment(Element.ALIGN_CENTER);
//                table7C1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                table7C1.setBorder(0);
//                //将设备编码信息和名称放入段落
//                Phrase table7C2P = new Phrase();
//                table7C2P.add(new Chunk("点巡检编码Inspection Code:", FontChinese11Normal));
//                table7C2P.add(Chunk.NEWLINE);
//                table7C2P.add(new Chunk(code, FontChinese11Normal));
//                table7C2P.add(Chunk.NEWLINE);
//                table7C2P.add(Chunk.NEWLINE);
//                table7C2P.add(new Chunk("点巡检名称Inspection Name:", FontChinese11Normal));
//                table7C2P.add(Chunk.NEWLINE);
//                table7C2P.add(new Chunk(ap.getPosition_name(), FontChinese11Normal));
//
//                PdfPCell table7C2 = getPdfPCell(table7C2P);
//                table7C2.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                //将图片和信息放入中间表
//                table2.addCell(table7C1);
//                table2.addCell(table7C2);
//
//                //设置最低表三列的宽度占比
//                int width21[] = {6,56,8,30};
//                table3.setWidths(width21);
//                PdfPCell cell20 = new PdfPCell(new com.itextpdf.text.Paragraph(""));
//                cell20.setBorder(0);
//                //位置写入列
//                PdfPCell cell21 = new PdfPCell(new com.itextpdf.text.Paragraph(selectOptionService.getLanguageInfo(LangConstant.POS_A)+"：" +ap.getFacilities_title(), FontChinese11Normal2));
//                cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell21.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cell21.setBorder(0);
//                //加入空列
//                cell21.setFixedHeight(15);
//                PdfPCell cell22 = new PdfPCell(new Phrase(""));
//                cell22.setBorder(0);
//                //最后字段
//                PdfPCell cell23 = new PdfPCell(new com.itextpdf.text.Paragraph(selectOptionService.getLanguageInfo(LangConstant.QR_FOR_MARP), FontChinese11Normal2));
//                cell23.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell23.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell23.setBorder(0);
//                cell23.setFixedHeight(20);
//                table3.addCell(cell20);
//                table3.addCell(cell21);
//                table3.addCell(cell22);
//                table3.addCell(cell23);
//                //将表 1 2 3 添加成table4的cell
//                PdfPCell cell1 = new PdfPCell(table1);
//                cell1.setBorder(0);
//                PdfPCell cell2 = new PdfPCell(table2);
//                cell2.setBorder(0);
//                PdfPCell cell3 = new PdfPCell(table3);
//                cell3.setBorder(0);
//                table4.addCell(cell1);
//                table4.addCell(cell2);
//                table4.addCell(cell3);
//                //将表格4变成cell这样就可以在一个设备所有信息放完之后加上边框
//                PdfPCell cell4 = new PdfPCell(table4);
//                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                //加上空段落换行
//                Phrase tab= new Phrase();
//                tab.add(Chunk.NEWLINE);
//
//                PdfPCell cell5 = getPdfPCell(tab);
//                //判断奇偶数
//                if(index%2==1) {//奇数放入左边大表
//                    tableall1.addCell(cell4);
//                    tableall1.addCell(cell5);
//
//                }else{//偶数放入右边大表
//                    tableall2.addCell(cell4);
//                    tableall2.addCell(cell5);
//
//                }
//                //因为pdftable有内存极限，所以当表格个数达到一千的时候先写入pdf
//                if(index>=100){
//
//                    PdfPCell cellall2 = new PdfPCell(tableall2);
//
//                    cellall2.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cellall2.setVerticalAlignment(Element.ALIGN_TOP);
//                    cellall2.setBorder(0);
//                    PdfPCell cellall1 = new PdfPCell(tableall1);
//                    cellall1.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cellall1.setVerticalAlignment(Element.ALIGN_TOP);
//                    cellall1.setBorder(0);
//                    //清空表格
//
//
//
//
//                    PdfPCell cellall3=new PdfPCell(new Phrase(""));
//                    cellall3.setBorder(0);
//                    tableall.addCell(cellall2);
//                    tableall.addCell(cellall3);
//                    tableall.addCell(cellall1);
//                    //写入
//                    document.add(tableall);
//
//                    tableall1.deleteBodyRows();
//                    tableall2.deleteBodyRows();
//                    tableall.deleteBodyRows();
//                    //重置计数器
//                    index=1;
//
//
//                }
//
//
//
//                index++;
//            }
//            //当打印数量不满1000或者前面打完后面剩余不满以前的时候直接打印
//
//            PdfPCell cellall2 = new PdfPCell(tableall2);
//
//            cellall2.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cellall2.setVerticalAlignment(Element.ALIGN_TOP);
//            cellall2.setBorder(0);
//            PdfPCell cellall1 = new PdfPCell(tableall1);
//            cellall1.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cellall1.setVerticalAlignment(Element.ALIGN_TOP);
//            cellall1.setBorder(0);
//
//
//            PdfPCell cellall3=new PdfPCell(new Phrase(""));
//            cellall3.setBorder(0);
//            tableall.addCell(cellall2);
//            tableall.addCell(cellall3);
//            tableall.addCell(cellall1);
//            document.add(tableall);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            document.close();
//        }
//
//
//    }
}
