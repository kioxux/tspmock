package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.model.RepairData;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class RepairDataExportView{
//public class RepairDataExportView extends AbstractXlsxStreamingView {
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "repair_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<RepairData> list = (List<RepairData>) model.get("repairs");
//
//            String sheetName = "维修数据";
//
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue("维修单号");
//                header.createCell(1).setCellValue("设备编号");
//                header.createCell(2).setCellValue("设备名称");
//                header.createCell(3).setCellValue("供应商");
//                header.createCell(4).setCellValue("父位置");
//                header.createCell(5).setCellValue("位置");
//                header.createCell(6).setCellValue("发生时间");
//                header.createCell(7).setCellValue("设备状态");
//                header.createCell(8).setCellValue("维修类型");
//                header.createCell(9).setCellValue("故障类型");
//                header.createCell(10).setCellValue("紧急程度");
//                header.createCell(11).setCellValue("上报人员");
//                header.createCell(12).setCellValue("维修人员");
//                header.createCell(13).setCellValue("状态");
//                header.createCell(14).setCellValue("开始时间");
//                header.createCell(15).setCellValue("完成时间");
//                header.createCell(16).setCellValue("时效(分钟)");
//                header.createCell(17).setCellValue("问题来源");
//                header.createCell(18).setCellValue("问题描述");
//                header.createCell(19).setCellValue("维修说明");
//
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (RepairData data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getRepairCode());
//                    courseRow.createCell(1).setCellValue(data.getAssetCode());
//                    courseRow.createCell(2).setCellValue(data.getAssetName());
//                    courseRow.createCell(3).setCellValue(data.getSuppilerName());
//                    courseRow.createCell(4).setCellValue(data.getParentName());
//                    courseRow.createCell(5).setCellValue(data.getFacilityName());
//                    if (data.getOccurtime() != null) {
//                        courseRow.createCell(6).setCellValue(sdf.format(new Date(data.getOccurtime().getTime())));
//                    } else {
//                        courseRow.createCell(6).setCellValue("");
//                    }
//                    courseRow.createCell(7).setCellValue(data.getAssetStatusName());
//                    courseRow.createCell(8).setCellValue(data.getRepairTypeName());
//                    courseRow.createCell(9).setCellValue(data.getFaultTypeName());
//                    courseRow.createCell(10).setCellValue(data.getPriorityLevel());
//                    courseRow.createCell(11).setCellValue(data.getCreate_user_name());
//                    courseRow.createCell(12).setCellValue(data.getReceiveName());
//                    courseRow.createCell(13).setCellValue(data.getStatusName());
//                    if (data.getRepairBeginTime() != null) {
//                        courseRow.createCell(14).setCellValue(sdf.format(new Date(data.getRepairBeginTime().getTime())));
//                    } else {
//                        courseRow.createCell(14).setCellValue("");
//                    }
//                    if (data.getFinishedTime() != null) {
//                        courseRow.createCell(15).setCellValue(sdf.format(new Date(data.getFinishedTime().getTime())));
//                    } else {
//                        courseRow.createCell(15).setCellValue("");
//                    }
//                    if (data.getFinishedTime() != null && data.getRepairBeginTime() != null) {
//                        Long tv = (data.getFinishedTime().getTime() - data.getRepairBeginTime().getTime())
//                                / (1000 * 60);
//                        courseRow.createCell(16).setCellValue(Double.parseDouble(decimalFormat.format(tv)));
//                    } else {
//                        courseRow.createCell(16).setCellValue("");
//                    }
//                    courseRow.createCell(17).setCellValue(data.getFromCode());
//                    courseRow.createCell(18).setCellValue(data.getFaultNote());
//                    courseRow.createCell(19).setCellValue(data.getRepairNote());
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}