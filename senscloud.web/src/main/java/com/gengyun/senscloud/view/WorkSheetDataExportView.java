package com.gengyun.senscloud.view;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.WorkSheet;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class WorkSheetDataExportView{
//public class WorkSheetDataExportView extends AbstractXlsxStreamingView {
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "work_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<WorkSheet> list = (List<WorkSheet>) model.get("WorkSheet");
//            SelectOptionService selectOptionService = (SelectOptionService) model.get("selectOptionService");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.W_O);//工单数据
//
//            if (list != null && list.size() > 0) {
//                Boolean hasFeeFlag = (Boolean) model.get("hasFeeFlag"); // 工单费用显示权限
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WM_WONA));//工单号
//                header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.POS_A));//位置
//                header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A));//供应商
//                header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ASSET_CATEGORY));//设备类型
//                header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_I));//设备
//                header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_AE));//设备编码
//                header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WO_TYPE));//工单类型
//                header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PRIORITY_A));//优先级
//                header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.REPORTER_A));//上报人
//                header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO));//负责人
//                header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.FAULT_TYPE));//故障类型
//                header.createCell(11).setCellValue(selectOptionService.getLanguageInfo(LangConstant.REP_TYPE));//维修类型
//                header.createCell(12).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ERROR_CODE));//故障代码
//                header.createCell(13).setCellValue(selectOptionService.getLanguageInfo(LangConstant.FAULT_DESCRIPTION));//故障代码描述
//                header.createCell(14).setCellValue(selectOptionService.getLanguageInfo(LangConstant.OCCURRENCE_TIME));//发生时间
//                header.createCell(15).setCellValue(selectOptionService.getLanguageInfo(LangConstant.DEADLINE_A));//截至时间
//                header.createCell(16).setCellValue(selectOptionService.getLanguageInfo(LangConstant.START_TIME));//开始时间
//                header.createCell(17).setCellValue(selectOptionService.getLanguageInfo(LangConstant.COMP_TIME));//完成时间
//                header.createCell(18).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TIME_LAP));//到场时效(分钟)
//                header.createCell(19).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WM_CTM));//完成时效(分钟)
//                header.createCell(20).setCellValue(selectOptionService.getLanguageInfo(LangConstant.PROBLEM_DESCRIPTION));//问题描述
//                header.createCell(21).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WM_RD));//结果说明
//                header.createCell(22).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SUBJECT_A));//主题
//                header.createCell(23).setCellValue(selectOptionService.getLanguageInfo(LangConstant.STATUS_A));//状态
//                if (hasFeeFlag) {
//                    header.createCell(24).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MANHOUR_COST)); // 工时费
//                    header.createCell(25).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MATER_COST)); // 物料费
//                    header.createCell(26).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ITEM_OTHER)); // 其他
//                    header.createCell(27).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TOTAL_T)); // 合计
//                    header.createCell(28).setCellValue(selectOptionService.getLanguageInfo(LangConstant.CURRENCY)); // 货币
//                }
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (WorkSheet data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getWork_code());
//                    courseRow.createCell(1).setCellValue(data.getFacility_name());
//                    courseRow.createCell(2).setCellValue(data.getCustomer_name());
//                    courseRow.createCell(3).setCellValue(data.getAsset_type());
//                    courseRow.createCell(4).setCellValue(data.getAsset_name());
//                    courseRow.createCell(5).setCellValue(data.getStrcode());
//                    courseRow.createCell(6).setCellValue(data.getType_name());
//                    if (data.getPriority_level() == 0) {
//                        courseRow.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.NONE_A));//无
//                    } else if (data.getPriority_level() == 1) {
//                        courseRow.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.LOW_A));//低
//                    } else if (data.getPriority_level() == 2) {
//                        courseRow.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MIDDLE_A));//中
//                    } else {
//                        courseRow.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.HIGH_A));//高
//                    }
//                    courseRow.createCell(8).setCellValue(data.getCreate_user_name());
//                    courseRow.createCell(9).setCellValue(data.getReceive_name());
//                    courseRow.createCell(10).setCellValue(data.getFault_name());
//                    courseRow.createCell(11).setCellValue(data.getRepair_type_name());
//                    courseRow.createCell(12).setCellValue(data.getFault_code());
//                    courseRow.createCell(13).setCellValue(data.getFault_code_name());
//
//                    if (data.getOccur_time() != null) {
//                        courseRow.createCell(14).setCellValue(sdf.format(new Date(data.getOccur_time().getTime())));
//                    } else {
//                        courseRow.createCell(14).setCellValue("");
//                    }
//                    if (data.getDeadline_time() != null) {
//                        courseRow.createCell(15).setCellValue(sdf.format(new Date(data.getDeadline_time().getTime())));
//                    } else {
//                        courseRow.createCell(15).setCellValue("");
//                    }
//                    if (data.getBegin_time() != null) {
//                        courseRow.createCell(16).setCellValue(sdf.format(new Date(data.getBegin_time().getTime())));
//                    } else {
//                        courseRow.createCell(16).setCellValue("");
//                    }
//                    if (data.getFinished_time() != null) {
//                        courseRow.createCell(17).setCellValue(sdf.format(new Date(data.getFinished_time().getTime())));
//                    } else {
//                        courseRow.createCell(17).setCellValue("");
//                    }
//                    if (data.getBegin_time() != null && data.getDistribute_time() != null) {
//                        Long tv = (data.getBegin_time().getTime() - data.getDistribute_time().getTime())
//                                / (1000 * 60);
//                        courseRow.createCell(18).setCellValue(Double.parseDouble(decimalFormat.format(tv)));
//                    } else {
//                        courseRow.createCell(18).setCellValue("");
//                    }
//                    if (data.getBegin_time() != null && data.getFinished_time() != null) {
//                        Long tv = (data.getFinished_time().getTime() - data.getBegin_time().getTime())
//                                / (1000 * 60);
//                        courseRow.createCell(19).setCellValue(Double.parseDouble(decimalFormat.format(tv)));
//                    } else {
//                        courseRow.createCell(19).setCellValue("");
//                    }
//                    courseRow.createCell(20).setCellValue(data.getProblem_note());
//                    courseRow.createCell(21).setCellValue(data.getResult_note());
//                    courseRow.createCell(22).setCellValue(data.getTitle());
//                    courseRow.createCell(23).setCellValue(data.getStatus_name());
//                    if (hasFeeFlag) {
//                        courseRow.createCell(24).setCellValue(Double.parseDouble(decimalFormat.format(data.getFee_1())));
//                        courseRow.createCell(25).setCellValue(Double.parseDouble(decimalFormat.format(data.getFee_2())));
//                        courseRow.createCell(26).setCellValue(Double.parseDouble(decimalFormat.format(data.getFee_9())));
//                        courseRow.createCell(27).setCellValue(Double.parseDouble(decimalFormat.format(data.getFee_amount())));
//                        courseRow.createCell(28).setCellValue(data.getFee_currency());
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
