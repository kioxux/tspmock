package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.model.InspectionData;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class InspectionDataExportView{
//public class InspectionDataExportView extends AbstractXlsxStreamingView {
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "inspecction_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<InspectionData> list = (List<InspectionData>) model.get("InspectionList");
//            String sheetName = "巡检数据";
//
//            if (list != null && list.size() > 0) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                //设置excel的标题，并依次为标题赋值
//                // create header row
//                Row header = sheet.createRow(0);
//                header.createCell(0).setCellValue("巡检单号");
//                header.createCell(1).setCellValue("所属区域");
//                header.createCell(2).setCellValue("父位置");
//                header.createCell(3).setCellValue("所属位置");
//                header.createCell(4).setCellValue("巡检人员");
//                header.createCell(5).setCellValue("问题个数");
//                header.createCell(6).setCellValue("开始时间");
//                header.createCell(7).setCellValue("完成时间");
//                header.createCell(8).setCellValue("巡检时效(分钟)");
//
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (InspectionData data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getInspection_code());
//                    courseRow.createCell(1).setCellValue(data.getArea_name());
//                    courseRow.createCell(2).setCellValue(data.getParentName());
//                    courseRow.createCell(3).setCellValue(data.getFacilityName());
//                    courseRow.createCell(4).setCellValue(data.getInspectionAccountName());
//                    courseRow.createCell(5).setCellValue(data.getFault_number());
//                    if (data.getBeginTime() != null) {
//                        courseRow.createCell(6).setCellValue(sdf.format(new Date(data.getBeginTime().getTime())));
//                    } else {
//                        courseRow.createCell(6).setCellValue("");
//                    }
//                    if (data.getInspection_time() != null) {
//                        courseRow.createCell(7).setCellValue(sdf.format(new Date(data.getInspection_time().getTime())));
//                    } else {
//                        courseRow.createCell(7).setCellValue("");
//                    }
//
//                    if (data.getBeginTime() != null && data.getInspection_time() != null) {
//                        Long tv = (data.getInspection_time().getTime() - data.getBeginTime().getTime())
//                                / (1000 * 60);
//                        courseRow.createCell(8).setCellValue(Double.parseDouble(decimalFormat.format(tv)));
//                    } else {
//                        courseRow.createCell(8).setCellValue("");
//                    }
//
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
