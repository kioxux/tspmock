package com.gengyun.senscloud.view;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.config.SpringContextHolder;
//import com.gengyun.senscloud.model.Asset;
//import com.gengyun.senscloud.model.AssetCategory;
//import com.gengyun.senscloud.model.FieldValidation;
//import com.gengyun.senscloud.service.ExcelImportService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@Component
public class AssetDataExportXlsxStreamingViewV2{
//public class AssetDataExportXlsxStreamingViewV2 extends AbstractXlsxStreamingView {
//    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model,
//                                      Workbook workbook, HttpServletRequest request,
//                                      HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            String schema_name = AuthService.getCompany(request).getSchema_name();
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "asset_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<AssetCategory> metaDataAssetList = (List<AssetCategory>) model.get("assetCategoryList");
//            List<Asset> assetList = (List<Asset>) model.get("assetList");
//            SelectOptionService selectOptionService = (SelectOptionService) model.get("selectOptionService");
//            List<Map<String, Object>> staticSelect = selectOptionService.getStaticSelectList("source_type");
//            Boolean amFeeShow = (Boolean) model.get("amFeeShow"); // 设备费用显示权限
//            Map<Integer, String> sourceTypeMap = new HashMap<>();
//            for (Map<String, Object> sta : staticSelect) {
//                if (sta.get("code") != null) {
//                    sourceTypeMap.put(Integer.parseInt(sta.get("code").toString()), sta.get("desc").toString());
//                }
//            }
//
//            // 单位
//            List<Map<String, Object>> UnitList = selectOptionService.SelectOptionList(schema_name, null, "unit", "", null);
//
//            //自定义字段下拉框
//            Map<String, Object> selectMap = new HashMap<String, Object>();
//
//            // 货币单位
////            List<Map<String, Object>> currencyList = selectOptionService.SelectOptionList(schema_name, null, "currency", "", null);
////            // 只取系统当前配置的货币，其他货币进入系统没有意义
////            // 1:￥ ;2:$ ; 3:€;
////            int currencyValue = 1;
////            SystemConfigData systemData = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.CURRENT_CURRENCY);
////            if (null != systemData && null != systemData.getSettingValue() && !systemData.getSettingValue().isEmpty()) {
////                currencyValue = Integer.parseInt(systemData.getSettingValue());
////            }
////            if (null != currencyList && currencyList.size() > 0) {
////                for (Iterator y = currencyList.iterator(); y.hasNext(); ) {
////                    Map<String, Object> item = (Map<String, Object>) y.next();
////                    if (null == item.get("code") || null == item.get("code").toString() || item.get("code").toString().isEmpty()
////                            || currencyValue != Integer.parseInt(item.get("code").toString())) {
////                        y.remove();
////                    }
////                }
////            }
//
////            // 资产类型
////            List<Map<String, Object>> asset_subject = new ArrayList<Map<String, Object>>();
////            Map asset_subjectmap = new HashMap();
////            Map asset_subjectmap2 = new HashMap();
////            asset_subjectmap.put("1", selectOptionService.getLanguageInfo(LangConstant.INVESTMENT_A));//资产
////            asset_subject.add(asset_subjectmap);
////            asset_subjectmap2.put("2", selectOptionService.getLanguageInfo(LangConstant.FEE_F));//费用
////            asset_subject.add(asset_subjectmap2);
////
////            // 物联状态
////            Map<String, Object> map_iot_status = new HashMap<String, Object>();
////            for (int x = 1; x <= 3; x++) {
////                if (x == 1) {
////                    map_iot_status.put("iot_status", selectOptionService.getLanguageInfo(LangConstant.NO_IOT_N));//无物联
////                } else if (x == 2) {
////                    map_iot_status.put("iot_status", selectOptionService.getLanguageInfo(LangConstant.IOT_ONLINE));//物联在线
////                } else if (x == 3) {
////                    map_iot_status.put("iot_status", selectOptionService.getLanguageInfo(LangConstant.IOT_OFFLINE));//物联离线
////                }
////            }
//
//            ExcelImportService excelImportService = SpringContextHolder.getBean("excelImportServiceImpl");
//            for (AssetCategory metaDataAsset : metaDataAssetList) {
//                // create excel xls sheet
//                Sheet sheet = workbook.createSheet(metaDataAsset.getCategory_name() + "(" + metaDataAsset.getCategoryCode() + ")");
//                sheet.setDefaultColumnWidth(30);
//
//                // create header row
//                Row header = sheet.createRow(0);
//                int i = 0;
//                //系统字段 主，主要
//                JSONArray systemCommonFields = excelImportService.getAssetSystemCommonFields();
//                if (systemCommonFields != null && systemCommonFields.size() > 0) {
//                    for (int k = 0; k < systemCommonFields.size(); k++) {
//                        JSONObject field = systemCommonFields.getJSONObject(k);
//                        String title = field.getString("name") + "[" + field.optString("alias", "") + "]";
//                        header.createCell(i).setCellValue(title);
//                        i++;
//                    }
//                }
//                JSONArray customFields = JSONArray.fromObject(metaDataAsset.getFields());
//                if (customFields != null && customFields.size() > 0) {
//                    for (int k = 0; k < customFields.size(); k++) {
//                        JSONObject field = customFields.getJSONObject(k);
//                        if (null == field || field.size() <= 0) {
//                            continue;
//                        }
//                        String alias = field.optString("alias", "");
//                        if (field.containsKey("fieldViewType") &&
//                                ("2".equals(field.getString("fieldViewType")) || "3".equals(field.getString("fieldViewType")))
//                                && field.containsKey("fieldDataBase")) {
//                            selectMap.put(field.getString("name"), field.getString("fieldDataBase"));
//                        }
//                        String title = field.getString("name") + "[" + alias + "]";
//                        header.createCell(i).setCellValue(title);
//                        i++;
//                    }
//                }
//                //系统字段 副，次要
//                JSONArray systemUsualFields = excelImportService.getAssetSystemUsualFields();
//                if (systemUsualFields != null && systemUsualFields.size() > 0) {
//                    for (int k = 0; k < systemUsualFields.size(); k++) {
//                        JSONObject field = systemUsualFields.getJSONObject(k);
//                        String fieldName = field.getString("name");
//                        if (!amFeeShow) {
//                            if (AssetCategory.PRICE.equals(fieldName) || AssetCategory.TAX_RATE.equals(fieldName)
//                                    || AssetCategory.TAX_PRICE.equals(fieldName) || AssetCategory.CURRENCY.equals(fieldName)
//                                    || AssetCategory.BUY_CURRENCY.equals(fieldName) || AssetCategory.INSTALL_CURRENCY.equals(fieldName)
//                                    || AssetCategory.INSTALL_PRICE.equals(fieldName)) {
//                                continue;
//                            }
//                        }
//                        String title = fieldName + "[" + field.optString("alias", "") + "]";
//                        header.createCell(i).setCellValue(title);
//                        i++;
//                    }
//                }
//
//                /*header.createCell(0).setCellValue("ID[_id]");
//                header.createCell(1).setCellValue("设备名称[strname]");
//                header.createCell(2).setCellValue("设备编码[strcode]");*/
//                // Create data cells
//                int rowCount = 1;
//
//                for (Asset asset : assetList) {
//                    JSONObject jsonObject = JSONObject.fromObject(asset);
//                    Integer categoryId = Integer.parseInt(jsonObject.optString("category_id", ""));
//                    if (metaDataAsset.getId() != categoryId) {
//                        continue;
//                    }
//
//                    Row courseRow = sheet.createRow(rowCount++);
//                    i = 0;
//                    // 解析通用字段，主，重要字段
//                    if (systemCommonFields != null && systemCommonFields.size() > 0) {
//                        for (int k = 0; k < systemCommonFields.size(); k++) {
//                            JSONObject systemField = systemCommonFields.getJSONObject(k);
//                            String fieldName = systemField.getString("name");
//                            //单独处理category_id字段
//                            if (AssetCategory.CATEGORY_ID.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getCategory_id() + ")" + asset.getCategory_name();
//                                String value = asset.getCategory_name();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理设备型号
//                            if (AssetCategory.ASSET_MODEL_ID.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (asset.getAsset_model_id() != null) {
////                                    value = "(" + asset.getAsset_model_id() + ")" + asset.getModel_name();
//                                    value = asset.getModel_name();
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理site字段
//                            if (AssetCategory.INTSITEID.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getIntsiteid() + ")" + asset.getSite_name();
//                                String value = asset.getSite_name();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理position——code字段
//                            if (AssetCategory.POSITION_CODE.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getIntsiteid() + ")" + asset.getSite_name();
//                                String value = asset.getPosition_name();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理status字段
//                            if (AssetCategory.INTSTATUS.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getIntstatus() + ")" + asset.getStatus();
//                                String value = asset.getStatus();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理importment_level_id字段
//                            if (AssetCategory.IMPORTMENT_LEVEL_ID.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getImportment_level_id() + ")" + asset.getImportment_level_name();
//                                String value = asset.getImportment_level_name();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理supplier字段
//                            if (AssetCategory.SUPPLIER.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getSupplier() + ")" + asset.getSupplier_name();
//                                String value = asset.getSupplier_name();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理manufacutre字段
//                            if (AssetCategory.MANUFACTURE.equalsIgnoreCase(fieldName)) {
////                                String value = "(" + asset.getSupplier() + ")" + asset.getSupplier_name();
//                                String value = asset.getManufacturer_name();
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理source_type字段
//                            if (AssetCategory.SOURCE_TYPE.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (asset.getSource_type() != null) {
////                                    value = "(" + asset.getSource_type() + ")" + sourceTypeMap.get(asset.getSource_type());
//                                    value = sourceTypeMap.get(asset.getSource_type());
//                                }
//
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理Unit字段
//                            if (AssetCategory.UNIT_ID.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (asset.getUnit_id() != null) {
//                                    if (null != UnitList && UnitList.size() > 0) {
//                                        for (Iterator y = UnitList.iterator(); y.hasNext(); ) {
//                                            Map<String, Object> item = (Map<String, Object>) y.next();
//                                            if (null != item.get("code") && null != item.get("code").toString() &&
//                                                    !item.get("code").toString().isEmpty() &&
//                                                    asset.getUnit_id() == Integer.parseInt(item.get("code").toString())) {
//                                                value = item.get("desc").toString();
//                                                break;
//                                            }
//                                        }
//
//
//                                    }
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            String validation = systemField.optString("validation", "");
//                            switch (validation) {
//                                case FieldValidation.DATE:
//                                    try {
//                                        long time = jsonObject.getJSONObject(fieldName).getLong("time");
//                                        Date date = new Date(time);
//                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//                                        String value = simpleDateFormat.format(date);
//                                        courseRow.createCell(i).setCellValue(value);
//                                    } catch (Exception ex) {
//                                        courseRow.createCell(i).setCellValue("");
//                                    }
//                                    break;
//                                default:
//                                    courseRow.createCell(i).setCellValue(jsonObject.optString(fieldName, ""));
//                            }
//                            i++;
//                        }
//                    }
//
//                    // 解析自定义字段
//                    if (null != jsonObject.opt("properties")) {
//                        if (customFields != null && customFields.size() > 0) {
//                            Object obj = jsonObject.opt("properties");
//                            JSONObject properties = null;
//                            if (null != obj && null != obj.toString() && !obj.toString().isEmpty()) {
//                                properties = JSONObject.fromObject(obj);
//                            }
//                            for (int m = 0; m < customFields.size(); m++) {
//                                JSONObject customField = customFields.getJSONObject(m);
//                                if (null == customField || customField.size() <= 0) {
//                                    continue;
//                                }
//                                if (null == properties) {
//                                    courseRow.createCell(i).setCellValue("");
//                                } else {
//                                    String fieldName = customField.getString("name");
//                                    String validation = customField.optString("validation", "");
//                                    switch (validation) {
//                                        case FieldValidation.DATE:
//                                            try {
//                                                long time = properties.getJSONObject(fieldName).getLong("time");
//                                                Date date = new Date(time);
//                                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//                                                String value = simpleDateFormat.format(date);
//                                                courseRow.createCell(i).setCellValue(value);
//                                            } catch (Exception ex) {
//                                                courseRow.createCell(i).setCellValue("");
//                                            }
//                                            break;
//                                        default:
//                                            String cell_value = properties.optString(fieldName, "");
//                                            if (selectMap.containsKey(fieldName) && null != selectMap.get(fieldName)) {
//                                                cell_value = selectOptionService.getOptionNameByCode(schema_name, (String) selectMap.get(fieldName), cell_value, "desc");
//                                            }
//                                            courseRow.createCell(i).setCellValue(cell_value);
//                                    }
//                                }
//                                i++;
//                            }
//                        }
//                    }
//
//                    // 解析通用字段，副，次要字段
//                    if (systemUsualFields != null && systemUsualFields.size() > 0) {
//                        for (int k = 0; k < systemUsualFields.size(); k++) {
//                            JSONObject systemField = systemUsualFields.getJSONObject(k);
//                            String fieldName = systemField.getString("name");
//                            if (!amFeeShow) {
//                                if (AssetCategory.PRICE.equals(fieldName) || AssetCategory.TAX_RATE.equals(fieldName)
//                                        || AssetCategory.TAX_PRICE.equals(fieldName) || AssetCategory.CURRENCY.equals(fieldName)
//                                        || AssetCategory.BUY_CURRENCY.equals(fieldName) || AssetCategory.INSTALL_CURRENCY.equals(fieldName)
//                                        || AssetCategory.INSTALL_PRICE.equals(fieldName)) {
//                                    continue;
//                                }
//                            }
//                            //单独处理asset_subject字段
//                            if (AssetCategory.ASSET_SUBJECT.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (asset.getAsset_subject() != null && asset.getAsset_subject().equals("1")) {
//                                    value = selectOptionService.getLanguageInfo(LangConstant.INVESTMENT_A);
//                                } else if (asset.getAsset_subject() != null && asset.getAsset_subject().equals("2")) {
//                                    if (amFeeShow) {
//                                        value = selectOptionService.getLanguageInfo(LangConstant.FEE_F);
//                                    } else {
//                                        continue;
//                                    }
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//                            //单独处理iot_status字段
//                            if (AssetCategory.IOT_STATUS.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (asset.getIot_status() == 1) {
//                                    value = selectOptionService.getLanguageInfo(LangConstant.NO_IOT_N);
//                                } else if (asset.getIot_status() == 2) {
//                                    value = selectOptionService.getLanguageInfo(LangConstant.IOT_ONLINE);
//                                } else if (asset.getIot_status() == 3) {
//                                    value = selectOptionService.getLanguageInfo(LangConstant.IOT_OFFLINE);
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//
//                            //单独处理currency字段
//                            if (AssetCategory.CURRENCY.equalsIgnoreCase(fieldName)) {
//                                String value = "";
////                                if (asset.getCurrency() != null) {
////                                    if (null != currencyList && currencyList.size() > 0) {
////                                        for (Iterator y = currencyList.iterator(); y.hasNext(); ) {
////                                            Map<String, Object> item = (Map<String, Object>) y.next();
////                                            if (null != item.get("code") && null != item.get("code").toString() &&
////                                                    !item.get("code").toString().isEmpty() &&
////                                                    asset.getCurrency() == Integer.parseInt(item.get("code").toString())) {
////                                                value = item.get("desc").toString();
////                                                break;
////                                            }
////                                        }
////
////
////                                    }
////                                }
//                                if (RegexUtil.isNotNull(asset.getAsset_currency())) {
//                                    value = asset.getAsset_currency();
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//
//                            // 单独处理currency字段
//                            if (AssetCategory.BUY_CURRENCY.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (RegexUtil.isNotNull(asset.getBuy_currency())) {
//                                    value = asset.getBuy_currency();
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//
//                            // 单独处理currency字段
//                            if (AssetCategory.INSTALL_CURRENCY.equalsIgnoreCase(fieldName)) {
//                                String value = "";
//                                if (RegexUtil.isNotNull(asset.getInstall_currency())) {
//                                    value = asset.getInstall_currency();
//                                }
//                                courseRow.createCell(i).setCellValue(value);
//                                i++;
//                                continue;
//                            }
//
//                            String validation = systemField.optString("validation", "");
//                            switch (validation) {
//                                case FieldValidation.DATE:
//                                    try {
//                                        long time = jsonObject.getJSONObject(fieldName).getLong("time");
//                                        Date date = new Date(time);
//                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//                                        String value = simpleDateFormat.format(date);
//                                        courseRow.createCell(i).setCellValue(value);
//                                    } catch (Exception ex) {
//                                        courseRow.createCell(i).setCellValue("");
//                                    }
//                                    break;
//                                default:
//                                    courseRow.createCell(i).setCellValue(jsonObject.optString(fieldName, ""));
//                            }
//                            i++;
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
