package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.PlanWorkModel;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class PlanWorkListDataExportView{
//public class PlanWorkListDataExportView extends AbstractXlsxStreamingView {
//
//    /**
//     * Application-provided subclasses must implement this method to populate
//     * the Excel workbook document, given the model.
//     *
//     * @param model    the model Map
//     * @param workbook the Excel workbook to populate
//     * @param request  in case we need locale etc. Shouldn't look at attributes.
//     * @param response in case we need to set cookies. Shouldn't write to it.
//     */
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "plan_work_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<PlanWorkModel> list = (List<PlanWorkModel>) model.get("planWorkModelList");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.PLAN_SCHEDULE);//维保计划
//
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(sheetName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.SCHEDULE_NAME));//计划名称
//            header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.POS_A));//客户
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.WO_TYPE));//工单类型
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.START_DATE));//开始日期
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.DEADLINE_A));//截至日期
//            header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.EXECUTOR_E));//执行人
//            header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.ASSIST_A));//协助
//            header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.STATUS_A));//状态
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                for (PlanWorkModel data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getPlan_name());
//                    courseRow.createCell(1).setCellValue(data.getFacility_name());
//                    courseRow.createCell(2).setCellValue(data.getWork_type_name());
//                    courseRow.createCell(3).setCellValue(data.getBegin_time());
//                    courseRow.createCell(4).setCellValue(data.getEnd_time());
//                    courseRow.createCell(5).setCellValue(data.getExecutor());
//                    String assist = handlerAssist(data.getAssist(), selectOptionService);
//                    courseRow.createCell(6).setCellValue(assist);
//                    courseRow.createCell(7).setCellValue(data.isIsuse()==true?
//                        selectOptionService.getLanguageInfo(LangConstant.OPEN_K):
//                        selectOptionService.getLanguageInfo(LangConstant.CLOSE_C));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    /**
//     * 拼接外协组织信息
//     * @param assistStr
//     * @return
//     */
//    private String handlerAssist(String assistStr, SelectOptionService selectOptionService) {
//        if(StringUtils.isBlank(assistStr))
//            return "";
//
//        StringBuffer assistBf = new StringBuffer();
//        String[] assistArray = assistStr.split(",");
//        for(String assist : assistArray) {
//            if(StringUtils.isNotBlank(assist)){
//                String[] assists = assist.split("_");
//                assistBf.append((assistBf.length()==0?"":",")+assists[0]+(StringUtils.isBlank(assists[1])?"":("（"+assists[1]+selectOptionService.getLanguageInfo(LangConstant.PEOPLE_P)+"）")));
//            }
//        }
//        return assistBf.toString();
//    }
}
