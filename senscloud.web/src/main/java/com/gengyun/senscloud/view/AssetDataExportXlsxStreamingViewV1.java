package com.gengyun.senscloud.view;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.util.Map;

public class AssetDataExportXlsxStreamingViewV1 extends AbstractXlsxStreamingView {


    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "asset_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            MetaDataAsset metaDataAsset = (MetaDataAsset) model.get("metaDataAsset");
//            List<Map<String, Object>> assetList = (List<Map<String, Object>>) model.get("assetList");
//
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(metaDataAsset.getAsset_name() + "(" + metaDataAsset.getAsset_alias() + ")");
//            sheet.setDefaultColumnWidth(40);
//
//            // create header row
//            Row header = sheet.createRow(0);
//            JSONArray systemFields = MetaDataAsset.getSystemFields();
//            JSONArray customFields = JSONArray.fromObject(metaDataAsset.getFields());
//            int i = 0;
//            if (systemFields != null && systemFields.size() > 0) {
//                for (int k = 0; k < systemFields.size(); k++) {
//                    JSONObject field = systemFields.getJSONObject(k);
//                    String title = field.getString("name") + "[" + field.optString("alias", "") + "]";
//                    header.createCell(i).setCellValue(title);
//                    i++;
//                }
//            }
//            if (customFields != null && customFields.size() > 0) {
//                for (int k = 0; k < customFields.size(); k++) {
//                    JSONObject field = customFields.getJSONObject(k);
//                    String title = field.getString("name") + "[" + field.optString("alias", "") + "]";
//                    header.createCell(i).setCellValue(title);
//                    i++;
//                }
//            }
//            // Create data cells
//            int rowCount = 1;
//
//            for (Map<String, Object> asset : assetList) {
//                JSONObject jsonObject = JSONObject.fromObject(asset);
//                Row courseRow = sheet.createRow(rowCount++);
//                i = 0;
//                //String properties = (String) asset.get("properties");
//                if (systemFields != null && systemFields.size() > 0) {
//                    for (int k = 0; k < systemFields.size(); k++) {
//                        JSONObject systemField = systemFields.getJSONObject(k);
//                        courseRow.createCell(i).setCellValue(jsonObject.optString(systemField.getString("name"), ""));
//                        i++;
//                    }
//                }
//
//                if (customFields != null && customFields.size() > 0) {
//                    for (int m = 0; m < customFields.size(); m++) {
//                        JSONObject customField = customFields.getJSONObject(m);
//                        courseRow.createCell(i).setCellValue(jsonObject.optString(customField.getString("name"), ""));
//                        i++;
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }
}
