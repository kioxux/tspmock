package com.gengyun.senscloud.view;

//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.AnalysFacilityRepairMaintainResult;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class FacilitySupplierRepairTypeDataExportView {
//public class FacilitySupplierRepairTypeDataExportView extends AbstractXlsxStreamingView {
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "repair_type_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String excelName = selectOptionService.getLanguageInfo(LangConstant.MSG_LOCAL_ERR_DATA);
//            String preType = selectOptionService.getLanguageInfo(LangConstant.POS_A);
//            List<AnalysFacilityRepairMaintainResult> list = (List<AnalysFacilityRepairMaintainResult>) model.get("repairAnalys");
//            String analysType = (String) model.get("analysType");
//
//            switch (analysType) {
//                case "supplier":
//                    excelName = selectOptionService.getLanguageInfo(LangConstant.MSG_SUPP_ERR_DATA);
//                    preType = selectOptionService.getLanguageInfo(LangConstant.SUPPLIER_A);
//                    break;
//            }
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(excelName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(preType + selectOptionService.getLanguageInfo(LangConstant.NAME_A));
//            header.createCell(1).setCellValue(preType + selectOptionService.getLanguageInfo(LangConstant.CODE_CODE));
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_A));
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_NUMBER_PLAT));
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_ELECT_NUM));
//            header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_ELECT_TOTAL_TIME));
//            header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MOTOR_FAIL));
//            header.createCell(7).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_MOTOR_FAIL_TIME));
//            header.createCell(8).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BELT_FAIL));
//            header.createCell(9).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BELT_FAIL_TIME));
//            header.createCell(10).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_DRUM_FAIL));
//            header.createCell(11).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_DRUM_FAIL_TIME));
//            header.createCell(12).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BEARING_FAIL));
//            header.createCell(13).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_BEARING_FAIL_TIME));
//            header.createCell(14).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OTHER_FAIL));
//            header.createCell(15).setCellValue(selectOptionService.getLanguageInfo(LangConstant.MSG_OTHER_FAIL_TIME));
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                DecimalFormat decimalFormat = new DecimalFormat("#.00");
//                for (AnalysFacilityRepairMaintainResult data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getAnalysName());
//                    courseRow.createCell(1).setCellValue(data.getAnalysCode());
//                    courseRow.createCell(2).setCellValue(data.getStrname());
//                    courseRow.createCell(3).setCellValue(data.getAsset_count());
//                    courseRow.createCell(4).setCellValue(data.getDevice_fault_times());
//                    courseRow.createCell(5).setCellValue(Double.parseDouble(decimalFormat.format(data.getDevice_fault_minutes())));
//                    courseRow.createCell(6).setCellValue(data.getEngine_fault_times());
//                    courseRow.createCell(7).setCellValue(Double.parseDouble(decimalFormat.format(data.getEngine_fault_minutes())));
//                    courseRow.createCell(8).setCellValue(data.getBelt_fault_times());
//                    courseRow.createCell(9).setCellValue(Double.parseDouble(decimalFormat.format(data.getBelt_fault_minutes())));
//                    courseRow.createCell(10).setCellValue(data.getRoll_fault_times());
//                    courseRow.createCell(11).setCellValue(Double.parseDouble(decimalFormat.format(data.getRoll_fault_minutes())));
//                    courseRow.createCell(12).setCellValue(data.getBearing_fault_times());
//                    courseRow.createCell(13).setCellValue(Double.parseDouble(decimalFormat.format(data.getBearing_fault_minutes())));
//                    courseRow.createCell(14).setCellValue(data.getOther_fault_times());
//                    courseRow.createCell(15).setCellValue(Double.parseDouble(decimalFormat.format(data.getOther_fault_minutes())));
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
