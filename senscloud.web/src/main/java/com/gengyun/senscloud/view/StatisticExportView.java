package com.gengyun.senscloud.view;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.JsonUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.math.BigDecimal;
//import java.math.BigInteger;
//import java.sql.Timestamp;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
public class StatisticExportView{
//public class StatisticExportView extends AbstractXlsxStreamingView {
//    private SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//    private DecimalFormat decimalFormat = new DecimalFormat("#.00");
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            String fileName = "statistic_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<Map<String, Object>> list = ( List<Map<String, Object>>) model.get("statistic");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String sheetName = (String)model.get("title");
//            if (StringUtils.isEmpty(sheetName)) {
//                sheetName = selectOptionService.getLanguageInfo(LangConstant.VMI_EXCEL_DATA);//导出统计数据
//            }
//            String column = (String)model.get("column");
//            List<Map> columns = new ArrayList<>();
//            if (StringUtils.isNotEmpty(column)) {
//                columns = JsonUtils.jsonToList(column, Map.class);
//            }
//
//            if (list != null && list.size() > 0) {
//                Sheet sheet = workbook.createSheet(sheetName);
//                sheet.setDefaultColumnWidth(40);
//                Row header = sheet.createRow(0);
//                Map<String, Integer> columnMap = new HashMap<>();
//                boolean isHeaderSet = false;
//                if (columns != null && columns.size() > 0) {
//                    int index = 0;
//                    for (Map map : columns) {
//                        String field = (String)map.getOrDefault("field", "");
//                        String title = (String)map.getOrDefault("title", "");
//                        header.createCell(index).setCellValue(title);
//                        columnMap.put(field, index++);
//                    }
//                    isHeaderSet = true;
//                }
//
//                int rowCount = 1;
//                for (Map<String, Object> data : list) {
//                    Row courseRow = sheet.createRow(rowCount);
//                    if (data != null && data.size() > 0) {
//                        int headerIndex = 0;
//                        for (Map.Entry<String, Object> entry : data.entrySet()) {
//                            String field = entry.getKey();
//                            if (!isHeaderSet && rowCount == 1) {
//                                header.createCell(headerIndex).setCellValue(field);
//                                columnMap.put(field, headerIndex++);
//                            }
//                            Integer index = columnMap.get(field);
//                            if (index != null) {
//                                fillRow(courseRow, index, entry.getValue());
//                            }
//                        }
//                    }
//                    rowCount++;
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    private void fillRow(Row courseRow, int index, Object value) {
//        if (value instanceof Timestamp) {
//            courseRow.createCell(index).setCellValue(sdf.format(new Date(((Timestamp)value).getTime())));
//        } else if (value instanceof Integer){
//            courseRow.createCell(index).setCellValue((Integer)value);
//        } else if (value instanceof Double) {
//            courseRow.createCell(index).setCellValue(Double.parseDouble(decimalFormat.format(value)));
//        } else if (value instanceof Float) {
//            courseRow.createCell(index).setCellValue((Float) value);
//        } else if (value instanceof Short) {
//            courseRow.createCell(index).setCellValue((Short) value);
//        } else if (value instanceof Byte) {
//            courseRow.createCell(index).setCellValue((Byte) value);
//        } else if (value instanceof Long) {
//            courseRow.createCell(index).setCellValue((Long) value);
//        } else if (value instanceof BigDecimal) {
//            courseRow.createCell(index).setCellValue(Double.parseDouble(decimalFormat.format(value)));
//        } else if (value instanceof BigInteger) {
//            courseRow.createCell(index).setCellValue(((BigInteger) value).longValue());
//        } else {
//            courseRow.createCell(index).setCellValue(value.toString());
//        }
//    }
}