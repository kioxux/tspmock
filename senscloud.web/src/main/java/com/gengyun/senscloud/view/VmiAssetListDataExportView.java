package com.gengyun.senscloud.view;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.VmiAssetModel;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
public class VmiAssetListDataExportView{
//public class VmiAssetListDataExportView extends AbstractXlsxStreamingView {
//
//    /**
//     * Application-provided subclasses must implement this method to populate
//     * the Excel workbook document, given the model.
//     *
//     * @param model    the model Map
//     * @param workbook the Excel workbook to populate
//     * @param request  in case we need locale etc. Shouldn't look at attributes.
//     * @param response in case we need to set cookies. Shouldn't write to it.
//     */
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        // change the file name
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "vmi_asset_list_export_" + sdf.format(new Date()) + ".xlsx";
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//            List<VmiAssetModel> list = (List<VmiAssetModel>) model.get("vmiAssetList");
//            SelectOptionService selectOptionService=(SelectOptionService)model.get("selectOptionService");
//            String sheetName = selectOptionService.getLanguageInfo(LangConstant.VMI_ASSET_DATA_A);//VMI设备数据
//
//            // create excel xls sheet
//            Sheet sheet = workbook.createSheet(sheetName);
//            sheet.setDefaultColumnWidth(40);
//            //设置excel的标题，并依次为标题赋值
//            // create header row
//            Row header = sheet.createRow(0);
//            header.createCell(0).setCellValue(selectOptionService.getLanguageInfo(LangConstant.LOCATION_NAME));//客户名称
//            header.createCell(1).setCellValue(selectOptionService.getLanguageInfo(LangConstant.CUS_NUM));//客户编码
//            header.createCell(2).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_A));//设备名称
//            header.createCell(3).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_AE));//设备编码
//            header.createCell(4).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_D));//设备型号
//            header.createCell(5).setCellValue(selectOptionService.getLanguageInfo(LangConstant.TANK_CAP));//储罐容量
//            header.createCell(6).setCellValue(selectOptionService.getLanguageInfo(LangConstant.CURR_STOCK));//当前存量
//            if (list != null && list.size() > 0) {
//                int rowCount = 1;
//                for (VmiAssetModel data : list) {
//                    Row courseRow = sheet.createRow(rowCount++);
//                    courseRow.createCell(0).setCellValue(data.getShort_title());
//                    courseRow.createCell(1).setCellValue(data.getFacilitycode());
//                    courseRow.createCell(2).setCellValue(data.getStrname());
//                    courseRow.createCell(3).setCellValue(data.getStrcode());
//                    courseRow.createCell(4).setCellValue(data.getModel_name());
//                    courseRow.createCell(5).setCellValue(data.getCapacity());
//
//                    BigDecimal percent = BigDecimal.ZERO;
//                    if(StringUtils.isNotBlank(data.getCapacity()) && StringUtils.isNotBlank(data.getTank_volume()) && !data.getCapacity().equals("0")){
//                        BigDecimal capacity = new BigDecimal(data.getCapacity());
//                        BigDecimal tankVolume = new BigDecimal(data.getTank_volume());
//                        percent = tankVolume.divide(capacity, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2);
//                    }
//                    courseRow.createCell(6).setCellValue(percent.toString() + "%");
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
