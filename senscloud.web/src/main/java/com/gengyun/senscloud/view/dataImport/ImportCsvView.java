package com.gengyun.senscloud.view.dataImport;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.system.CommonUtilService;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

/**
 * 导入返回处理
 */
public class ImportCsvView extends AbstractCsvView {

    @Override
    protected void buildCsvDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        MethodParam methodParam = (MethodParam) model.get("methodParam");
        String[] header = {"num", "level", "message"};
        String[] dataType = header;
        CommonUtilService commonUtilService = SpringContextHolder.getBean("commonUtilServiceImpl");
        ImportLog alias = new ImportLog();
        alias.setNum(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AG_G)); // 行号
        response.setHeader("Content-Disposition", "attachment; filename=\"data-import-log.csv\"");
        Writer writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8");
        writer.write('\uFEFF'); // BOM for UTF-*
        ICsvBeanWriter csvWriter = new CsvBeanWriter(writer, CsvPreference.STANDARD_PREFERENCE);
        try {
            csvWriter.writeHeader(header);
            alias.setLevel(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_CATEGORY_AP)); // 日志类型
            alias.setMessage(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_CM)); // 详细信息
            csvWriter.write(alias, dataType);
            List<ImportLog> logList = (List<ImportLog>) model.get("logList");
            for (ImportLog importLog : logList) {
                csvWriter.write(importLog, dataType);
            }
        } catch (Exception e) {

        }
        csvWriter.close();
    }
}
