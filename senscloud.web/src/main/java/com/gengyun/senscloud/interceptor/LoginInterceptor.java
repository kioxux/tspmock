package com.gengyun.senscloud.interceptor;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.login.ClientTokenService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.ScdSesRecordUtil;
import com.gengyun.senscloud.util.SenscloudException;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;

/**
 * 用户登录拦截器,监测用户是否已经登录。
 * 这里直接把用户信息放到session里面。将来如果需要在多个服务器间共享session，可以通过Redis实现。
 * 详情请参考http://blog.csdn.net/u011493599/article/details/53940363
 */
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Resource
    ClientTokenService clientTokenService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        try {
            String url = request.getRequestURI();
            if ("/error".equals(url)) {
                request.getRequestDispatcher("/index.html").forward(request, response);
                return false;
            }
            String reLoginErr = "reLogin";
            String token = RegexUtil.optNotBlankStrOrExp(request.getParameter("token"), reLoginErr);
            Object tmpToken = ScdSesRecordUtil.getScdSesDataByKey(token, Constants.SCD_SES_KEY_LOGIN_TOKEN);
            if (RegexUtil.optNotNull(tmpToken).isPresent()) {
                if (("/checkCompanies".equals(url) || "/doLogin".equals(url)) && token.equals(tmpToken)) {
                    Instant loginTime = (Instant) RegexUtil.optNotNullOrExp(ScdSesRecordUtil.getScdSesDataByKey(token, Constants.SCD_SES_KEY_LOGIN_TIME), reLoginErr);
                    if (Duration.between(loginTime, Instant.now()).getSeconds() > 300) {
                        throw new SenscloudException(reLoginErr);
                    }
                } else {
                    throw new SenscloudException(reLoginErr);
                }
            } else {
                String schemaName = SenscloudUtil.getSchemaByCompId(RegexUtil.optNotBlankStrOrExp(ScdSesRecordUtil.getScdSesDataByKey(token, Constants.SCD_SES_KEY_COMPANY_ID), reLoginErr));
                int tokenCnt = clientTokenService.getTokenCacheCount(schemaName, RegexUtil.optStrOrVal(request.getParameter("clientType"), SensConstant.PC_CLIENT_NAME), token);
                if (tokenCnt == 0) {
                    throw new SenscloudException(reLoginErr);
                }
            }
        } catch (SenscloudException tokenExp) {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter pw = response.getWriter();
            pw.write(JSONObject.fromObject(ResponseModel.reLogin()).toString());
            pw.flush();
            pw.close();
            return false;
        }
        return true;
    }
}