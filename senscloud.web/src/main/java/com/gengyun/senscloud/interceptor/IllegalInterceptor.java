package com.gengyun.senscloud.interceptor;


import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.ScdSesRecordUtil;
import com.gengyun.senscloud.util.SenscloudException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 非法请求拦截器
 */
@Component
public class IllegalInterceptor implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(IllegalInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        int errorCode = ResponseConstant.ERROR_ILLEGAL_CODE;
        try {
            // 从 HTTP 头中取得 Referer 值，判断 Referer 是否以域名开头
            String referer = request.getHeader("Referer");
            String token = RegexUtil.optStrOrBlank(request.getParameter("token"));
            boolean isIllegal = RegexUtil.optNotBlankStrOpt(referer).map(String::trim).map(rf -> {
                List<String> domainList = RegexUtil.optNotNullListStr(DataChangeUtil.scdObjToListStr(ScdSesRecordUtil.getScdSesDataByKey(token, "domainListAll")))
                        .orElseGet(() -> {
                            CommonUtilService commonUtilService = SpringContextHolder.getBean("commonUtilServiceImpl");
                            List<String> domainListAll = commonUtilService.getAllDomain();
                            ScdSesRecordUtil.setScdSesDataByKey(token, "domainListAll", domainListAll);
                            return domainListAll;
                        });
                return RegexUtil.optNotNullListStr(domainList).map(dl -> {
                    String rfStr = rf.substring(request.getScheme().length() + 3);
                    for (String domain : domainList) {
                        if (rfStr.startsWith(domain)) {
                            rfStr = null;
                            break;
                        }
                    }
                    return RegexUtil.optNotBlankStrOpt(rfStr).isPresent();
                }).orElse(true);
            }).orElse(true);
            if (isIllegal) {
//                logger.error(ResponseConstant.ERROR_RIGHT_CODE + "-referer:" + referer);
                throw new SenscloudException(errorCode);
            }
            String method = request.getMethod();
            RegexUtil.optNotBlankStrOpt(method).filter(m -> "GET".equals(m) || "POST".equals(m) || "HEAD".equals(m)).map(m -> true).orElseGet(() -> {
                logger.error(ResponseConstant.ERROR_ILLEGAL_CODE + "-method:" + method);
                throw new SenscloudException(errorCode);
            });
        } catch (Exception e) {
            // TODO 上线时使用白名单功能
//            response.setCharacterEncoding("UTF-8");
//            response.setContentType("application/json;charset=UTF-8");
//            PrintWriter pw = response.getWriter();
//            pw.write(JSONObject.fromObject(ResponseModel.errorIllegal()).toString()); // HTTP动词篡改导致的认证旁路
//            pw.flush();
//            pw.close();
//            return false;
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) {

    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) {

    }
}
