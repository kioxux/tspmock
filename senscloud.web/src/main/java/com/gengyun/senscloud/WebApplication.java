package com.gengyun.senscloud;

import org.flowable.spring.boot.ProcessEngineAutoConfiguration;
import org.flowable.spring.boot.app.AppEngineAutoConfiguration;
import org.flowable.spring.boot.app.AppEngineServicesAutoConfiguration;
import org.flowable.spring.boot.dmn.DmnEngineAutoConfiguration;
import org.flowable.spring.boot.dmn.DmnEngineServicesAutoConfiguration;
import org.flowable.spring.boot.form.FormEngineAutoConfiguration;
import org.flowable.spring.boot.form.FormEngineServicesAutoConfiguration;
import org.flowable.spring.boot.idm.IdmEngineAutoConfiguration;
import org.flowable.spring.boot.idm.IdmEngineServicesAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan({"com.gengyun.senscloud.mapper"})
@EnableScheduling //运行程序定期执行
@EnableTransactionManagement //支持事务处理
@ServletComponentScan   //扫描Servlet
@EnableRetry //开启retry重试机制
//@SpringBootApplication
//@EnableCaching
//禁用自动配置数据源，支持多数据源
@SpringBootApplication(exclude = {IdmEngineAutoConfiguration.class,
        IdmEngineServicesAutoConfiguration.class, DmnEngineServicesAutoConfiguration.class,
        DmnEngineAutoConfiguration.class, FormEngineAutoConfiguration.class,
        FormEngineServicesAutoConfiguration.class, AppEngineAutoConfiguration.class,
        AppEngineServicesAutoConfiguration.class, ProcessEngineAutoConfiguration.class})
public class WebApplication extends SpringBootServletInitializer {
    public WebApplication() {
        super();
        setRegisterErrorPageFilter(false);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebApplication.class, args);
    }
}
