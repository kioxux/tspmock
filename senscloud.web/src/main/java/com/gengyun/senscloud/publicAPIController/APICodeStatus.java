package com.gengyun.senscloud.publicAPIController;

/**
 * Created by Dong_wudang on 2018/9/27.
 * 功能：标注接口code返回状态
 */
public interface APICodeStatus {
    String REPAIR = "RP";//维修
    String MAINTAIN = "MA";//保养
    int SUCCESS_CODE = 1;//成功
    int USERID_ERROR_CODE = 2;//用户身份验证错误
    int EXCEPTION_CODE = 8;//接口异常接口
    int FAULT_CODE = 9;//失败
    String JY_APPLAY_BOM = "http://mrotest.verymro.com/api/Private/bom_use_in_bill";//嘉岩
}

