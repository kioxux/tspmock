package com.gengyun.senscloud.publicAPIController;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.MaintainService;
//import com.gengyun.senscloud.service.RepairService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.HttpHelper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Map;
//
///**
// * Created by Dong_wudang on 2018/9/29.
// */
//@RestController
//@RequestMapping("/use_api")
public class UseAPIController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private RepairService repairService;
//
//    @Autowired
//    private MaintainService maintainService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/bom_use_in_bill")
//    public ResponseModel bom_use_in_bill(String jsonApp ,HttpServletResponse response, HttpServletRequest request){
//        ResponseModel responseModel = new ResponseModel();
//        String result = null;
//        Boolean applyStatus = false;
//        try{
////            result = HttpHelper.httpPost(APICodeStatus.JY_APPLAY_BOM, URLEncoder.encode(jsonApp,"utf-8"));//获取返回结果json串
//            result = HttpHelper.httpPost(APICodeStatus.JY_APPLAY_BOM, jsonApp);//获取返回结果json串
//            Map resultObject = (Map) com.alibaba.fastjson.JSON.parse(result);
//            if(resultObject.size()>0){
//                for (Object obj : resultObject.keySet()){
//                    System.out.println("key为："+obj+"值为："+resultObject.get(obj));
//                    if(obj.equals("code")){
//                        responseModel.setCode((Integer) resultObject.get(obj));
//                    }else if(obj.equals("content")){
//                        responseModel.setContent(resultObject.get(obj));
//                    }else if(obj.equals("msg")){
//                        responseModel.setMsg(resultObject.get(obj)+"");
//                    }
//                }
//            }
//            return responseModel;
//        }catch (Exception e){
//            responseModel.setCode(-1);
//            responseModel.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_APPLY_EXECP));
//            responseModel.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_APPLY_EXECP));
//            return responseModel;
//        }
//
//    }
//    @RequestMapping("/updateApplyBomRepairOrMaintian")
//    public ResponseModel updateApplyBomRepairOrMaintian(String code ,String source, HttpServletRequest request, HttpServletResponse response){
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try{
//            int update = 0;
//                if(source!=null&&source.equals("repair")){
//                    //修改维修数据
//                    update = repairService.updateRepairApplyStatus(schema_name,code);
//                }else if(source!=null&&source.equals("maintain")){
//                    //修改保养单据状态
//                    update = maintainService.updateMaintainApplyStaus(schema_name,code);
//                }
//                if(update>0){
//                    result.setCode(1);
//                    result.setContent("");
//                    result.setMsg("");
//                }else {
//                    result.setCode(0);
//                    result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_APPLY_FAIL));
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_APPLY_FAIL));
//                }
//                return  result;
//        }catch (Exception e){
//            result.setCode(-1);
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_APPLY_EXEC));
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_APPLY_EXEC));
//            return  result;
//        }
//    }
}
