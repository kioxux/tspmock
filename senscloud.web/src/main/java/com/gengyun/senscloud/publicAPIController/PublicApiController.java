package com.gengyun.senscloud.publicAPIController;
//
//import com.alibaba.fastjson.JSON;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.BomData;
//import com.gengyun.senscloud.model.MaintainBomData;
//import com.gengyun.senscloud.model.MoreCompany;
//import com.gengyun.senscloud.model.RepairBomData;
//import com.gengyun.senscloud.model.jy_model.BomBillJY;
//import com.gengyun.senscloud.model.jy_model.BomRecordInBillJY;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.sun.org.apache.xpath.internal.operations.Bool;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
///**
// * Created by Dong_wudang on 2018/9/27.
// */
//@RestController
//@RequestMapping("/api")
public class PublicApiController {
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    BomService bomService;
//
//    @Autowired
//    MoreCompanyService moreCompanyService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping(value = "/bom/bom_record_in_bill",method = RequestMethod.POST)
//    @Transactional
//    public String jy_use_bom_record_in_bill(@RequestBody BomRecordInBillJY bomRecordInBillJY){
//        ResponseModel result = new ResponseModel();
//        try {
//            int code = 0;
//            int total = 0;
//            String msg = "";
//            String content = "";
//            Boolean userIDStatus = false;//企业用户角色是否合法
//            Boolean updateStatus = false;//同步状态
//            String schema_name = "";
//
//            String user_account = bomRecordInBillJY.getUser_account();//用户账号
//            String valid_key = bomRecordInBillJY.getValid_key();//加密key
//            String token = bomRecordInBillJY.getToken();//token
//            String bill_code = bomRecordInBillJY.getBill_code();//类型
//            String is_pass = bomRecordInBillJY.getIs_pass();//是否通过
//            String client_customer_code = bomRecordInBillJY.getClient_customer_code();
//            List<BomBillJY> bomBillJIES = bomRecordInBillJY.getBom_list();
//
//
//            //判断企业账号信息的准确性
//            MoreCompany moreCompany = moreCompanyService.selectCountCompany(client_customer_code);//查询用户身份是否是合法企业账号信息
//            if(moreCompany!=null){
//                userIDStatus = true;
//                schema_name = "sc_com_"+ moreCompany.get_id();//获取schema，前往多租户的schema更新数据
//            }
//            if(userIDStatus){
//                if(is_pass!=null&&is_pass.equals("0")){//不通过修改单据为
//                    //修改维修或者保养状态为不通过 为20
//                    if(bill_code!=null&&!bill_code.equals("")&&bill_code.contains(APICodeStatus.MAINTAIN)) {//保养
//                        maintainService.updateOpenMaintainApplyStaus(schema_name,bill_code,20);//保养不通过
//                        maintainService.udpateMaintainBomBymaterial(schema_name,bill_code);//删除外部厂商的物料申请信息
//                        code = APICodeStatus.SUCCESS_CODE;//操作成功标注
//                        msg = selectOptionService.getLanguageInfo(LangConstant.MSG_APPENT_REFUSE);
//                        content = selectOptionService.getLanguageInfo(LangConstant.MSG_APPENT_REFUSE);
//                    }else if(bill_code!=null&&!bill_code.equals("")&&bill_code.contains(APICodeStatus.REPAIR)){
//                        repairService.updateOpenRepairApplyStatus(schema_name,bill_code,20);//维修不通过
//                        repairService.updateRepairBomByMaterCode(schema_name,bill_code);//删除外部厂商的物料申请信息
//                        code = APICodeStatus.SUCCESS_CODE;//操作成功标注
//                        msg = selectOptionService.getLanguageInfo(LangConstant.MSG_APPENT_REFUSE);
//                        content = selectOptionService.getLanguageInfo(LangConstant.MSG_APPENT_REFUSE);
//                    }
//                }else if(is_pass!=null&&is_pass.equals("1")){//通过
//
//                    //修改维修或者保养状态为不通过 为60
//                    if(bomBillJIES!=null&&bomBillJIES.size()>0){
//                        if(bill_code!=null&&!bill_code.equals("")&&bill_code.contains(APICodeStatus.MAINTAIN)) {//保养
//                            maintainService.udpateMaintainBomBymaterial(schema_name,bill_code);//删除外部厂商的物料申请信息
//                        }else if(bill_code!=null&&!bill_code.equals("")&&bill_code.contains(APICodeStatus.REPAIR)){
//                            repairService.updateRepairBomByMaterCode(schema_name,bill_code);//删除外部厂商的物料申请信息
//                        }
//                        for(BomBillJY bom:bomBillJIES){
//                            String bom_code = "";
//                            String bom_modal = "";
//                            String material = bom.getMaterial_code();//物料编码
//
//                            int facilityId = 0;
//                            int isFromServiceSupplier = 0;
//                            float useCount = 0f;
//                            if(bom.getFacility_id()!=null && !bom.getFacility_id().equals("")){
//                                 facilityId = Integer.parseInt( bom.getFacility_id());//位置编号
//                            }
//                            if(bom.getIs_from_service_supplier()!=null && !bom.getIs_from_service_supplier().equals("")){
//                                isFromServiceSupplier = Integer.parseInt(bom.getIs_from_service_supplier());
//                            }
//                            if(bom.getQuantity()!=null && !bom.getQuantity().equals("")){
//                                useCount = Float.parseFloat(bom.getQuantity());
//                            }
//                            String stockCode = bom.getStock_code();//库房编号
//                            BomData bomData = bomService.findByMaterial_code(schema_name,material);
//                            if(bomData!=null){
//                                bom_modal = bomData.getBom_model();
//                                bom_code = bomData.getBom_code();
//                            }else{
//                                bom_modal = bom.getBom_model();//实际情况是否存在备件编号和备件型号不存在情况
//                                bom_code = bom.getBom_code();
//                            }
//
//                            if(bill_code!=null&&!bill_code.equals("")&&bill_code.contains(APICodeStatus.MAINTAIN)){//保养
//                                //查询保养单据是否存在物料单据数据信息，存在删除并更新，不存在直接保存
//                                //查询保养备件表如果存在数据就更新，不存在就新增
////                                Boolean isMaintainBom = false;//标记是否存在保养备件信息
////                                List<MaintainBomData> maintainBomDataList = maintainService.selectMaintainByMaterialCode(schema_name,bill_code);//根据订单号和物料编号查询是否存在保养备件信息
////                                if(maintainBomDataList!=null&&maintainBomDataList.size()>0){
////                                    isMaintainBom = true;
////                                }
////                                if(isMaintainBom){//存在保养备件记录
////                                int i = maintainService.udpateMaintainBomBymaterial(schema_name,bill_code);//删除外部厂商的物料申请信息
////                                if(i>0){//保存第三方审核返回的数据
//                                    MaintainBomData maintainBomData = new MaintainBomData();
//                                    maintainBomData.setBomCode(bom_code);
//                                    maintainBomData.setBomModel(bom_modal);
//                                    maintainBomData.setUseCount(useCount);
//                                    maintainBomData.setMaintainCode(bill_code);
//                                    maintainBomData.setMaterial_code(material);
//                                    maintainBomData.setFetchMan(user_account);
//                                    maintainBomData.setFacilityId(facilityId);
//                                    maintainBomData.setStockCode(stockCode);
//                                    maintainBomData.setIs_from_service_supplier(isFromServiceSupplier);
//                                    int w = maintainService.saveMaintainBom(schema_name,maintainBomData);
//                                    if(w>0){
//                                        updateStatus = true;//标记保存状态
//                                        total++;//记录保存成功条数
//                                    }
////                                }
//                                if(updateStatus){//更新单据状态 60正常
//                                    maintainService.updateOpenMaintainApplyStaus(schema_name,bill_code,60);
//                                }
//
//                            }else if(bill_code!=null&&!bill_code.equals("")&&bill_code.contains(APICodeStatus.REPAIR)){//维修
////                                Boolean isRepairBom =false;
////                                List<RepairBomData> repairBomDataList = repairService.selectRepairByMaterialCode(schema_name,bill_code);//根据订单号和物料编号查询维修备件列表信息
////                                if(repairBomDataList!=null&&repairBomDataList.size()>0){
////                                    isRepairBom = true;
////                                }
////                                if(isRepairBom){//更新维修备件
////                                int i = repairService.updateRepairBomByMaterCode(schema_name,bill_code);//删除外部厂商的物料申请信息
////                                if(i>0){//保存第三方审核返回的数据
//                                    updateStatus = true;
//                                    RepairBomData repairBomData = new RepairBomData();
//                                    repairBomData.setBomCode(bom_code);
//                                    repairBomData.setBomModel(bom_modal);
//                                    repairBomData.setFetchMan(user_account);
//                                    repairBomData.setMaterial_code(material);
//                                    repairBomData.setUseCount(useCount);
//                                    repairBomData.setRepairCode(bill_code);
//                                    repairBomData.setFacilityId(facilityId);
//                                    repairBomData.setIs_from_service_supplier(isFromServiceSupplier);
//                                    repairBomData.setStockCode(stockCode);
//                                    int w = repairService.insertRepairBomJY(schema_name,repairBomData);
//                                    if(w>0){
//                                        updateStatus = true;//标记保存状态
//                                        total++;//记录保存成功条数
//                                    }
////                                }
//                                if(updateStatus){//更新单据状态 60正常
//                                    repairService.updateOpenRepairApplyStatus(schema_name,bill_code,60);//释放保养申领状态
//                                }
//                            }
//                        }
//                        //删除相同数据并删除重复数据并添加新的数据
//                        if(updateStatus){//同步成功
//                            code = APICodeStatus.SUCCESS_CODE;//操作成功标注
//                            msg = selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//                            content = selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_SUCC)+total+selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_INFO);
//                        }else{//同步失败
//                            code = APICodeStatus.FAULT_CODE;
//                            msg = selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_ERROR);
//                            content = selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_DATA_EXEC);
//                        }
//                    }
//
//                }
//            }else{//企业身份信息非法
//                code = APICodeStatus.USERID_ERROR_CODE;//用户身份有误
//                msg = selectOptionService.getLanguageInfo(LangConstant.MSG_VALI_USER_REFUSE);
//                content = selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR);
//            }
//            result.setCode(code);
//            result.setMsg(msg);
//            result.setContent(content);
//            String json = JSON.toJSONString(result);
//            return  json;
//        }catch (Exception e){
//            result.setCode(APICodeStatus.EXCEPTION_CODE);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INTERFACE_EXEC));
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_INTERFACE_EXEC));
//            return  JSON.toJSONString(result);
//        }
//
//    }
}
