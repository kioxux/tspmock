package com.gengyun.senscloud.service.system.impl;


import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.cache.impl.CacheUtilServiceImpl;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.model.WorkFlowTemplateSearchParam;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.ScdSesRecordUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 字典处理
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
@Service
public class SelectOptionServiceImpl implements SelectOptionService {
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    PagePermissionMapper pagePermissionMapper;
    @Resource
    WorkColumnMapper workColumnMapper;
    @Resource
    AssetMapper assetMapper;
    @Resource
    WorkflowService workflowService;
    @Resource
    CacheUtilServiceImpl cacheUtilService;
    @Resource
    CompanyService companyService;
    @Resource
    WorkFlowTemplateMapper workFlowTemplateMapper;
    @Resource
    WorkFlowTemplateService workFlowTemplateService;

    /**
     * 根据类型查询字典
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param extParam    扩展参数
     * @return 字典
     */
    @Override
    public List<Map<String, Object>> getSelectOptionList(MethodParam methodParam, String selectKey, Map<String, Object> extParam) {
        String key = RegexUtil.optStrOrError(selectKey, methodParam, ErrorConstant.EC_SELECT_1);
        if ("asset_type".equals(key)) {
            return pagePermissionMapper.findPrmAssetCategoryList(methodParam.getSchemaName(), methodParam.getUserId());
        } else if ("asset_position_remark".equals(key)) {
            return pagePermissionMapper.findAssetPositionRemarkList(methodParam.getSchemaName(), methodParam.getUserId()); // 设备地址描述
        } else if ("login_user_info".equals(key)) {
            List<Map<String, Object>> list = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            map.put(Constants.SELECT_CODE_NAME, "1");
            map.put(Constants.SELECT_NAME_NAME, methodParam.getUserName());
            map.put("user_id", methodParam.getUserId());
            map.put("account", methodParam.getAccount());
            list.add(map);
            return list;
        } else if ("auto_generate_bill".equals(key)) {
            List<Map<String, Object>> list = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            map.put("1", LangConstant.LOG_E);
            map.put("0", LangConstant.LOG_F);
            list.add(map);
            return list;
        } else if ("currency".equals(key)) {
            return pagePermissionMapper.findCurrencyList(methodParam.getSchemaName());
        } else if ("asset_category_permission".equals(key)) {
            return pagePermissionMapper.findAssetCategoryPermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 带权限的设备类型
            // 带权限的设备类型(去除自己和子集)
        } else if ("asset_category_with_id".equals(key) && RegexUtil.optNotNull(extParam.get("id")).isPresent()) {
            Integer id = RegexUtil.optIntegerOrExpParam(extParam.get("id"), LangConstant.MSG_A);
            return pagePermissionMapper.findAssetCategoryList(methodParam.getSchemaName(), id);
            //去除自己和子集的备件类型
        } else if ("bom_type_with_id".equals(key) && RegexUtil.optNotNull(extParam.get("id")).isPresent()) {
            Integer id = RegexUtil.optIntegerOrExpParam(extParam.get("id"), LangConstant.MSG_A);
            return pagePermissionMapper.findBomTypeList(methodParam.getSchemaName(), id);
            // 根据设备类型获取设备型号下拉框
        } else if ("asset_model_category_with_id".equals(key) && RegexUtil.optNotNull(extParam.get("id")).isPresent()) {
            Integer id = RegexUtil.optIntegerOrExpParam(extParam.get("id"), LangConstant.MSG_A);
            return pagePermissionMapper.findAssetModelCategoryList(methodParam.getSchemaName(), id);
            // 去除自己和子集的设备位置
        } else if ("asset_position_with_id".equals(key) && RegexUtil.optNotNull(extParam.get("id")).isPresent()) {
            String id = RegexUtil.optStrOrExpNotNull(extParam.get("id"), LangConstant.MSG_A);
            List<String> postionList = pagePermissionMapper.findAssetPositionList(methodParam.getSchemaName(), id);
            return pagePermissionMapper.findNotInAssetPositionList(methodParam.getSchemaName(), postionList);
        } else if ("asset_position_permission".equals(key)) {
            return pagePermissionMapper.findAssetPositionPermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 带权限的设备位置
            // 去除自己和子集的厂商
        } else if ("all_facilities_with_id".equals(key) && RegexUtil.optNotNull(extParam.get("id")).isPresent()) {
            String id = RegexUtil.optStrOrExpNotNull(extParam.get("id"), LangConstant.MSG_A);
            return pagePermissionMapper.findFacilitiesList(methodParam.getSchemaName(), id);
        } else if ("work_type_permission".equals(key)) {
            return pagePermissionMapper.findWorkTypePermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 带权限的工单类型
        } else if ("work_list_type_permission".equals(key)) {
            return pagePermissionMapper.findWorkListTypePermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 带权限的普通工单类型(工单列表用)
        } else if ("task_list_type_permission".equals(key)) {
            return pagePermissionMapper.findTaskListTypePermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 带权限的流程类型(待办列表用)
        } else if ("works_type_permission".equals(key)) {
            return pagePermissionMapper.findWorksTypePermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 根据权限查询可创建的工单类型列表
        } else if ("works_type_permission_with_customer".equals(key)) {
            return pagePermissionMapper.findWorksTypePermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 根据权限查询可创建的工单类型列表
        } else if ("works_type_permission_with_asset".equals(key)) {
            return pagePermissionMapper.findWorksTypeWithAssetPermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 设备出入库可创建的工单
        } else if ("works_type_permission_with_bom".equals(key)) {
            return pagePermissionMapper.findWorksTypeWithBomPermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 备件可创建的工单
        } else if ("work_type_permission_with_bom".equals(key)) {
            return pagePermissionMapper.findWorkTypeWithBomPermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 备件工单筛选下拉框
        } else if ("multilingual".equals(key)) {
//            return selectOptionsMapper.findResource(methodParam.getCompanyId(), "WHERE field like 'title%' or field like 'btn%'"); // 获取多语言title开头和btn开头的数据
            return findResource(methodParam.getCompanyId(), "WHERE field like 'title%' or field like 'btn%'"); // 获取多语言title开头和btn开头的数据
        } else if ("lang_all_sources".equals(key)) {
            return findResource(methodParam.getCompanyId(), StringUtils.EMPTY); // 获取多语言title开头和btn开头的数据
        } else if ("data_type_all".equals(key)) {
            return cacheUtilService.findAllCloudDataType(methodParam.getSchemaName(), methodParam.getUserLang(), methodParam.getCompanyId()); // 获取cloud_data_type表的所有数据
            // 根据设备类型获取设备型号下拉框
        } else if ("asset_model_category".equals(key) && RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
            Integer id = RegexUtil.optIntegerOrExpParam(extParam.get("selectExtKey"), LangConstant.MSG_A);
            return pagePermissionMapper.findAssetModelCategoryList(methodParam.getSchemaName(), id);
            // 根据角色id获取用户列表
        } else if ("users_by_role".equals(key) && RegexUtil.optNotNull(extParam.get("role_id")).isPresent()) {
            String role_id = RegexUtil.optNotBlankStrOrExp(extParam.get("role_id"), LangConstant.MSG_A);
            return pagePermissionMapper.findUsersByRoleId(methodParam.getSchemaName(), role_id);
            // 测试模糊查询设备
        } else if ("test_asset_list".equals(key)) {
            String words = RegexUtil.optNotBlankStrOrExp(extParam.get("selectExtKey"), LangConstant.MSG_A);
            return pagePermissionMapper.testAssetList(methodParam.getSchemaName(), words);
        } else if ("source".equals(key)) {
            return cacheUtilService.findAllCloudDataType(methodParam.getSchemaName(), methodParam.getUserLang(), methodParam.getCompanyId());
        } else if ("works_log".equals(key)) {
            return findWorksLog(methodParam.getCompanyId());
        } else if ("asset_position_facility".equals(key)) {
            return cacheUtilService.findAssetOrg(methodParam.getSchemaName(), methodParam.getCompanyId(), methodParam.getUserLang()); // 根据工单设备组织表，获取使用的组织
//            return selectOptionsMapper.findAssetPositionFacility(methodParam.getSchemaName());
        } else if ("scada_config".equals(key)) {
            return selectOptionsMapper.findScadaConfig(methodParam.getSchemaName());
            // 根据设备类型 工单类型  设备位置查询维修工
        } else if ("receive_user_with_permission".equals(key)) {
            String position_code = null;
            Integer work_type_id = null;
            Integer category_id = null;
            if (RegexUtil.optNotNull(extParam.get("position_code")).isPresent()) {
                position_code = extParam.get("position_code").toString();
            }
            if (RegexUtil.optNotNull(extParam.get("work_type_id")).isPresent()) {
                work_type_id = Integer.valueOf(extParam.get("work_type_id").toString());
            }
            if (RegexUtil.optNotNull(extParam.get("category_id")).isPresent()) {
                category_id = Integer.valueOf(extParam.get("category_id").toString());
            }
            return selectOptionsMapper.findReceiveUserWithPermission(methodParam.getSchemaName(), position_code, category_id, work_type_id);
            // 可借出的设备类表
        } else if ("asset_out_for_search".equals(key)) {
            String selectExtKey = null;
            Integer category_id = null;
            if (RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
                selectExtKey = extParam.get("selectExtKey").toString();
            }
            if (RegexUtil.optNotNull(extParam.get("category_id")).isPresent()) {
                category_id = Integer.valueOf(extParam.get("category_id").toString());
            }
            if (RegexUtil.optNotNull(extParam.get("bak_value1")).isPresent()) {
                selectExtKey = extParam.get("bak_value1").toString();
            }
            return selectOptionsMapper.findAssetOutForSearch(methodParam.getSchemaName(), selectExtKey, category_id);
            // 可归还的设备列表
        } else if ("asset_input_for_search".equals(key)) {
            String selectExtKey = null;
            Integer category_id = null;
            String bak_value1 = null;
            String out_code = null;
            if (RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
                selectExtKey = extParam.get("selectExtKey").toString();
            }
            if (RegexUtil.optNotNull(extParam.get("category_id")).isPresent()) {
                category_id = Integer.valueOf(extParam.get("category_id").toString());
            }
            if (RegexUtil.optNotNull(extParam.get("bak_value1")).isPresent()) {
                bak_value1 = extParam.get("bak_value1").toString();
            }
            if (RegexUtil.optNotNull(extParam.get("out_code")).isPresent()) {
                out_code = extParam.get("out_code").toString();
            }
            return selectOptionsMapper.findAssetInputForSearch(methodParam.getSchemaName(), selectExtKey, category_id, bak_value1, out_code);
            // 设备调拨设备列表
        } else if ("asset_allocation_for_search".equals(key)) {
            return RegexUtil.optNotBlankStrOpt(RegexUtil.optStrOrNull(extParam.get("position_code"))).map(p -> selectOptionsMapper.findAssetAllocationForSearch(methodParam.getSchemaName(), RegexUtil.optStrOrNull(extParam.get("selectExtKey")), RegexUtil.optIntegerOrNull(extParam.get("category_id")), p)).orElse(null);
        } else if ("asset_out_code_by_well_no".equals(key) && RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
            String selectExtKey = extParam.get("selectExtKey").toString();
            return pagePermissionMapper.findAssetOutCodeByWellNo(methodParam.getSchemaName(), selectExtKey);
            //根据扫码获取设备编码获取设备信息
//        if ("asset_info_by_scan".equals(key) && RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
//            String selectExtKey = extParam.get("selectExtKey").toString();
//            return pagePermissionMapper.findAssetOutCodeByWellNo(methodParam.getSchemaName(), selectExtKey);
//        }
            // 获取设备工单井号下拉框
        } else if ("asset_works_well_no".equals(key)) {
            String selectExtKey = null;
            if (RegexUtil.optNotNull(extParam.get(selectExtKey)).isPresent()) {
                extParam.get("selectExtKey").toString();
            }
            return pagePermissionMapper.findAssetWorksWellNo(methodParam.getSchemaName(), selectExtKey);
            // 获取所有启用厂商
        } else if ("all_facility_with_type".equals(key)) {
            String asset_id = RegexUtil.optStrOrVal(extParam.get("selectExtKey"), "0");
            String keyword = RegexUtil.optStrOrNull(extParam.get("keyword"));
            String orgType = RegexUtil.optStrOrNull(extParam.get("orgType"));
            return cacheUtilService.findAllFacilityWithType(methodParam.getSchemaName(), methodParam.getCompanyId(), methodParam.getUserLang(), asset_id, keyword, orgType);
            // 根据设备id查询有权限的人
        } else if ("only_customer".equals(key)) {
            return pagePermissionMapper.findOnlyCustomer(methodParam.getSchemaName(), methodParam.getUserId());
        } else if ("user_with_asset_permission".equals(key) && RegexUtil.optIsPresentStr(extParam.get("selectExtKey"))) {
            String asset_id = RegexUtil.optStrOrVal(extParam.get("selectExtKey"), "0");
            String keyword = RegexUtil.optStrOrNull(extParam.get("keyword"));
            return pagePermissionMapper.findUserWithAssetPermission(methodParam.getSchemaName(), asset_id, keyword);
            // 获取所有内部厂商（组织）
        } else if ("inner_facility_with_type".equals(key)) {
//            String asset_id = RegexUtil.optStrOrVal(extParam.get("selectExtKey"), "0");
//            String keyword = RegexUtil.optStrOrNull(extParam.get("keyword"));
            List<Map<String, Object>> innerFacilityWithType = cacheUtilService.findInnerFacilityWithType(methodParam.getSchemaName(), methodParam.getCompanyId(), methodParam.getUserLang());
            for (Map<String, Object> map : innerFacilityWithType) {
                map.put("schema_name", methodParam.getSchemaName());
            }
            return innerFacilityWithType;
            // 判案人员
        } else if ("user_list_by_permission".equals(key)) {
            String role_id = RegexUtil.optStrOrNull(extParam.get("role_id"));
            Integer work_type_id = RegexUtil.optIntegerOrNull(extParam.get("work_type_id"), LangConstant.TITLE_CATEGORY_X);
            if (null == role_id || null == work_type_id) {
                return new ArrayList<>();
            }
            Map<String, Object> assetMap = RegexUtil.optMapOrNew(RegexUtil.optNotBlankStrOpt(RegexUtil.optStrAndValOrNull(extParam.get("asset_id"), extParam.get("relation_id")))
                    .map(s -> assetMapper.findAssetById(methodParam.getSchemaName(), s)).orElse(null));
            String position_code = RegexUtil.optStrAndValOrNull(extParam.get("position_code"), assetMap.get("position_code"));
            Integer category_id = RegexUtil.optIntegerAndValOrNull(extParam.get("category_id"), assetMap.get("category_id"), LangConstant.TITLE_ASSET_C);
            Integer stock_id = RegexUtil.optIntegerOrNull(extParam.get("stock_id"), LangConstant.TITLE_AJ_Q);
            return pagePermissionMapper.findUserByPermission(methodParam.getSchemaName(), role_id, position_code, work_type_id, stock_id, category_id);
            // 设备扫码
        } else if ("asset_info_by_scan".equals(key)) {
            return RegexUtil.optNotBlankStrOpt(extParam.get("selectExtKey")).map(s -> selectOptionsMapper.findAssetInfoForByScan(methodParam.getSchemaName(), s)).orElse(null);
            // 设备位置扫码
        } else if ("asset_position_info_by_scan".equals(key)) {
            return RegexUtil.optNotBlankStrOpt(extParam.get("selectExtKey")).map(s -> selectOptionsMapper.findAssetPositionInfoForByScan(methodParam.getSchemaName(), s)).orElse(null);
        } else if ("bom_stock_list".equals(key)) {
            Integer stock_id = RegexUtil.optIntegerOrNull(extParam.get("stock_id"));
            return selectOptionsMapper.findBomListByBomIdAndStockId(methodParam.getSchemaName(), stock_id);
        } else if ("bom_stock_out_list".equals(key)) {
            String selectExtKey = RegexUtil.optStrOrNull(extParam.get("selectExtKey"));
            Integer stock_id = RegexUtil.optIntegerOrNull(extParam.get("stock_id"));
            String type_id = RegexUtil.optStrOrNull(extParam.get("bom_type_id"));
            String supplier_id = RegexUtil.optStrOrNull(extParam.get("supplier_id"));
            String manufacturer_id = RegexUtil.optStrOrNull(extParam.get("manufacturer_id"));
            if (null == supplier_id && null == manufacturer_id && null == type_id) {
                return new ArrayList<>();
            }
            return selectOptionsMapper.findStockBomList(methodParam.getSchemaName(), selectExtKey, stock_id, type_id, supplier_id, manufacturer_id);
        } else if ("bom_sup_man_list".equals(key)) {
            Integer stock_id = RegexUtil.optIntegerOrNull(extParam.get("stock_id"));
            String supplier_id = RegexUtil.optStrOrNull(extParam.get("supplier_id"));
            String manufacturer_id = RegexUtil.optStrOrNull(extParam.get("manufacturer_id"));
            if (null == supplier_id && null == manufacturer_id) {
                return new ArrayList<>();
            }
            return selectOptionsMapper.findBomListByBomIdAndSupId(methodParam.getSchemaName(), stock_id, supplier_id, manufacturer_id);
        } else if ("bom_list_with_type".equals(key)) {
            String selectExtKey = null;
            if (RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
                selectExtKey = extParam.get("selectExtKey").toString();
            }
            return selectOptionsMapper.findBomListWithType(methodParam.getSchemaName(), selectExtKey);
            // 设备盘点位置列表
        } else if ("asset_position_list_with_inventory".equals(key)) {
            String selectExtKey = null;
            if (RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
                selectExtKey = extParam.get("selectExtKey").toString();
            }
            String categoryIdStr = RegexUtil.optStrOrNull(extParam.get("category_id"));
            Map<String, Object> pm = new HashMap<>();
            WorkFlowTemplateSearchParam bParam = new WorkFlowTemplateSearchParam();
            pm.put("prefId", "Asset_check");
            pm.put("nodeType", "subNode");
            String nodeId = workFlowTemplateMapper.findNodeId(methodParam.getSchemaName(), pm);
            bParam.setNode_id(nodeId);
            bParam.setPref_id("Asset_check");
            Map<String, Object> workFlowInfo = workFlowTemplateService.getWorkFlowTemplateInfo(methodParam, bParam);
            Map<String, Object> handleMap = (Map<String, Object>) workFlowInfo.get("handle");
            String dealRoleId = RegexUtil.optStrOrBlank(handleMap.get("deal_role_id"));

            if (RegexUtil.optIsBlankStr(categoryIdStr)) {
                return selectOptionsMapper.findAssetPositionListWithInventory(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), categoryIdStr, dealRoleId);
            } else {
                List<Map<String, Object>> listRow = selectOptionsMapper.findAssetPositionListWithInventory(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), null, dealRoleId);

                List<Map<String, Object>> list = selectOptionsMapper.findAssetPositionListWithInventory(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), categoryIdStr, dealRoleId);
                List<Map<String, Object>> listNewRow = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) listRow);
                List<Map<String, Object>> listNew = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) list);
                RegexUtil.optNotNullList(listRow).ifPresent(l -> {
                    if (RegexUtil.optIsPresentList(list)) {
                        l.forEach(r -> {
                            list.forEach(e -> {
                                if (Objects.equals(e.get("position_code"), r.get("position_code"))) {
                                    listNewRow.remove(r);
                                }
                            });
                        });
                    }
                    listNewRow.forEach(r -> {
                        r.put("stock_count", 0);
                        r.put("category_id", categoryIdStr);
                        listNew.add(r);
                    });
                });
                return listNew;
            }
            // 设备盘点设备列表（根据设备位置获取设备列表）
        } else if ("asset_list_by_position_with_inventory".equals(key)) {
            String selectExtKey = null;
            if (RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
                selectExtKey = extParam.get("selectExtKey").toString();
            }
            String relation_id = RegexUtil.optStrOrNull(extParam.get("relation_id"));
            String category_id = RegexUtil.optStrOrNull(extParam.get("category_id"));
            return selectOptionsMapper.findAssetListByPositionWithInventory(methodParam.getSchemaName(),relation_id,category_id,selectExtKey);
            // 备件采购需求列表
        } else if ("bom_need_by_list".equals(key)) {
            List<Map<String, Object>> bom_list = new ArrayList<>();
            String selectExtKey = null;
            if (RegexUtil.optNotNull(extParam.get("selectExtKey")).isPresent()) {
                selectExtKey = extParam.get("selectExtKey").toString();
                methodParam.setNeedPagination(true);
                String json = selectOptionsMapper.findBomNeedByList(methodParam.getSchemaName(), selectExtKey);
                if (RegexUtil.optIsPresentStr(json)) {
                    JSONArray jsonArray = JSONArray.fromObject(json);
                    JSONObject jsonObject = JSONObject.fromObject(jsonArray.get(0));
                    Iterator iter = jsonObject.entrySet().iterator();
                    Map<String, Object> field_codes = new HashMap<>();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        Map<String, Object> column = workColumnMapper.findWorkColumnByFieldFormCode(methodParam.getSchemaName(), entry.getKey().toString());
                        if (RegexUtil.optNotNull(column).isPresent()) {
                            String field_code = column.get("field_code").toString();
                            field_codes.put(entry.getKey().toString(), field_code);
                        }
                    }
                    for (int i = 0; i < jsonArray.size(); i++) {
                        Map<String, Object> bom = new HashMap<>();
                        JSONObject json1 = JSONObject.fromObject(jsonArray.get(i));
                        Iterator iter1 = json1.entrySet().iterator();
                        while (iter1.hasNext()) {
                            Map.Entry entry = (Map.Entry) iter1.next();
                            String key1 = entry.getKey().toString();
                            JSONObject.fromObject(jsonArray.get(i)).remove(key1);
                            String value = entry.getValue().toString();
                            key1 = field_codes.get(key1).toString();
                            bom.put(key1, value);
                        }
                        bom_list.add(bom);
                    }
                }
                return bom_list;
            } else {
                return new ArrayList<>();
            }
            // 备件采购需求列表
        } else if ("bom_need_code_list".equals(key)) {
            return selectOptionsMapper.findBomNeedWorkCodes(methodParam.getSchemaName());
            // 库房列表支持模糊搜索
        } else if("area".equals(key)){
            if(!RegexUtil.optIsPresentStr(extParam.get("selectExtKey")))
                return new ArrayList<>();
            String selectExtKey = extParam.get("selectExtKey").toString();
            if(RegexUtil.optIsPresentStr(selectExtKey)){
                extParam.put("selectExtKey","and parent_code='"+selectExtKey+"'");
            }
        } else if("sub_work_list".equals(key)){
            String selectExtKey = extParam.get("selectExtKey").toString();
            return selectOptionsMapper.findSubWorkList(methodParam.getSchemaName(),selectExtKey);
        } else if("customer_asset_list".equals(key)){
            if(!RegexUtil.optIsPresentStr(extParam.get("selectExtKey")))
                return new ArrayList<>();
            String selectExtKey = extParam.get("selectExtKey").toString();
            /*if(RegexUtil.optIsPresentStr(selectExtKey)){
                extParam.put("selectExtKey","and b.org_id='"+selectExtKey+"'::int");
            }*/
            return pagePermissionMapper.findCustomerAssetList(methodParam.getSchemaName(), selectExtKey,methodParam.getUserId());

        } else if("asset_customer".equals(key)){
            if(!RegexUtil.optIsPresentStr(extParam.get("selectExtKey")))
                return new ArrayList<>();
            String selectExtKey = extParam.get("selectExtKey").toString();
            if(RegexUtil.optIsPresentStr(selectExtKey)){
                extParam.put("selectExtKey","and b.asset_id='"+selectExtKey+"'");
            }
        } else if("customer_contact_all".equals(key)){
            if(!RegexUtil.optIsPresentStr(extParam.get("id")))
                return new ArrayList<>();
            String id = extParam.get("id").toString();
            extParam.put("selectExtKey",id);
        } else if ("cc_all_user".equals(key)) {
            return RegexUtil.optNotBlankStrOpt(extParam.get("org_id")).map((org_id) -> {
                List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew(pagePermissionMapper.findCCAllUser(methodParam.getSchemaName(), new HashMap<String, Object>() {{put("org_id", org_id);}}));
                return list.stream().filter(e -> !Objects.equals(e.get("value"), methodParam.getUserId())).collect(Collectors.toList());
            }).orElseGet(ArrayList::new);
        } else if ("stock_list_with_like".equals(key)) {
            String selectExtKey = RegexUtil.optStrOrNull(extParam.get("selectExtKey"));
            return RegexUtil.optNotBlankStrOpt(extParam.get("work_type_id")).map(Integer::valueOf).map(workTypeId -> pagePermissionMapper.findBwPrmStockList(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), workTypeId))
                    .orElseGet(ArrayList::new);
            // 备件盘点库房列表
        } else if ("bom_stock_list_with_inventory".equals(key)) {
            String selectExtKey = RegexUtil.optStrOrNull(extParam.get("selectExtKey"));
            String bomTypeIdStr = RegexUtil.optStrOrNull(extParam.get("bom_type_id"));
            Map<String, Object> pm = new HashMap<>();
            WorkFlowTemplateSearchParam bParam = new WorkFlowTemplateSearchParam();
            pm.put("prefId", "bom_check");
            pm.put("nodeType", "subNode");
            String nodeId = workFlowTemplateMapper.findNodeId(methodParam.getSchemaName(), pm);
            bParam.setNode_id(nodeId);
            bParam.setPref_id("bom_check");
            Map<String, Object> workFlowInfo = workFlowTemplateService.getWorkFlowTemplateInfo(methodParam, bParam);
            Map<String, Object> handleMap = (Map<String, Object>) workFlowInfo.get("handle");
            String dealRoleId = RegexUtil.optStrOrBlank(handleMap.get("deal_role_id"));
            if (RegexUtil.optIsBlankStr(bomTypeIdStr)) {
                return RegexUtil.optNotBlankStrOpt(extParam.get("work_type_id")).map(Integer::valueOf).map(workTypeId -> pagePermissionMapper.findBwPrmStockBomList(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), workTypeId, bomTypeIdStr, dealRoleId))
                        .orElseGet(ArrayList::new);
            } else {
                List<Map<String, Object>> listRow = RegexUtil.optNotBlankStrOpt(extParam.get("work_type_id")).map(Integer::valueOf).map(workTypeId -> pagePermissionMapper.findBwPrmStockBomList(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), workTypeId, null, dealRoleId))
                        .orElseGet(ArrayList::new);

                List<Map<String, Object>> list = RegexUtil.optNotBlankStrOpt(extParam.get("work_type_id")).map(Integer::valueOf).map(workTypeId -> pagePermissionMapper.findBwPrmStockBomList(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), workTypeId, bomTypeIdStr, dealRoleId))
                        .orElseGet(ArrayList::new);
                List<Map<String, Object>> listNewRow = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) listRow);
                List<Map<String, Object>> listNew = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) list);
                RegexUtil.optNotNullList(listRow).ifPresent(l -> {
                    if (RegexUtil.optIsPresentList(list)) {
                        l.forEach(r -> {
                            list.forEach(e -> {
                                if (Objects.equals(e.get("relation_id"), r.get("relation_id"))) {
                                    listNewRow.remove(r);
                                }
                            });
                        });
                    }
                    listNewRow.forEach(r -> {
                        r.put("stock_count", 0);
                        r.put("bom_type_id", bomTypeIdStr);
                        listNew.add(r);
                    });
                });
                return listNew;
            }
            // 备件盘点备件列表
        } else if ("bom_list_by_stock_with_inventory".equals(key)) {
            String selectExtKey = RegexUtil.optStrOrNull(extParam.get("selectExtKey"));
            String bomTypeId = RegexUtil.optStrOrNull(extParam.get("bom_type_id"));
            return RegexUtil.optNotBlankStrOpt(extParam.get("bom_id")).map(Integer::valueOf).map(bomId -> pagePermissionMapper.findStockBomListByStockAndType(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), bomId, bomTypeId))
                    .orElseGet(ArrayList::new);
        } else if ("bom_in_list".equals(key)) {
            String selectExtKey = RegexUtil.optStrOrNull(extParam.get("selectExtKey"));
            String bomTypeId = RegexUtil.optStrOrNull(extParam.get("bom_type_id"));
            String supplierId = RegexUtil.optStrOrNull(extParam.get("supplier_id"));
            String manufacturerId = RegexUtil.optStrOrNull(extParam.get("manufacturer_id"));
            Integer stockId = RegexUtil.optIntegerOrNull(extParam.get("stock_id"));
            if (null == supplierId && null == manufacturerId && null == bomTypeId) {
                return new ArrayList<>();
            }
            return pagePermissionMapper.findBomInListByStockAndType(methodParam.getSchemaName(), selectExtKey, methodParam.getUserId(), stockId, bomTypeId, supplierId, manufacturerId);
        } else if ("bom_position_permission".equals(key)) {
            return pagePermissionMapper.findBomPositionPermissionList(methodParam.getSchemaName(), methodParam.getUserId()); // 带权限的设备位置
        } else if ("root_asset_position_name".equals(key)) {
            return selectOptionsMapper.getRootAssetPositionName(methodParam.getSchemaName());
        } else if ("second_asset_position_name".equals(key)) {
            return selectOptionsMapper.getSecondAssetPositionName(methodParam.getSchemaName());
            // 按照选中的设备位置获取设备列表
        } else if ("assetByPositions".equals(key)) {
            return RegexUtil.optNotBlankStrOpt(extParam.get("selectExtKey")).map(s -> s.split(",")).map(ss -> selectOptionsMapper.findAssetByPositions(methodParam.getSchemaName(), ss)).orElseGet(ArrayList::new);
        } else if ("company_all".equals(key)) {
            return DataChangeUtil.scdStringToList(companyService.requestNoParam("companyAll"));
        }
        List<Map<String, Object>> list;
        String sv = RegexUtil.optNotNullMap(extParam).map(ep -> RegexUtil.optStrOrBlank(ep.get("selectExtKey"))).orElse("");
        String sc = RegexUtil.optNotNullMap(extParam).map(ep -> RegexUtil.optStrOrBlank(ep.get("selectSearchKey"))).orElse("");
        if ("".equals(sv)) {
            list = RegexUtil.optNotBlankStrOpt(SqlConstant.SELECTS.get(key))
                    .map(s -> s.replace(SqlConstant.SCHEMA_NAME, methodParam.getSchemaName())).map(s -> {
                        if ("bom_list".equals(key)) {
                            s = s.replace(SqlConstant.SQL_WHERE_NAME, "");
                        } else if ("asset_with_position".equals(key)) {
                            s = s.replace(SqlConstant.SQL_WHERE_LIKE, "");
                        }
                        return s;
                    })
                    .map(sql -> selectOptionsMapper.findSelectOptionList(sql)).orElseGet(() -> this.getStaticSelectList(methodParam, key));
        } else {
            // 根据流程号获取节点下拉框
            if ("flow_node_list_by_id".equals(key)) {
                return workflowService.getWorkflowNodeListById(methodParam, sv);
            }
            list = RegexUtil.optNotBlankStrOpt(SqlConstant.SELECTS.get(key)).map(s -> s.replace(SqlConstant.SCHEMA_NAME, methodParam.getSchemaName())).map(s -> {
                if ("bom_list".equals(key)) {
                    String ts = " and ( b.bom_name like '%" + SqlConstant.SQL_WHERE_NAME + "%' or b.material_code like '%" + SqlConstant.SQL_WHERE_NAME
                            + "%' or b.bom_model like '%" + SqlConstant.SQL_WHERE_NAME + "%' or b.brand_name like '%" + SqlConstant.SQL_WHERE_NAME + "%' ) ";
                    ts = ts.replace(SqlConstant.SQL_WHERE_NAME, sv);
                    s = s.replace(SqlConstant.SQL_WHERE_NAME, ts);
                } else if ("asset_with_position".equals(key)) {
                    if (RegexUtil.optIsPresentStr(sc)) {
                        String ts = " and ( t1.asset_name like '%" + SqlConstant.SQL_WHERE_LIKE + "%' ) ";
                        ts = ts.replace(SqlConstant.SQL_WHERE_LIKE, sc);
                        s = s.replace(SqlConstant.SQL_WHERE_LIKE, ts);
                    } else {
                        s = s.replace(SqlConstant.SQL_WHERE_LIKE, "");
                    }
                }
                return s;
            }).map(s -> s.replace(SqlConstant.SQL_WHERE_NAME, sv)).map(sql -> selectOptionsMapper.findSelectOptionList(sql)).orElseGet(() ->
                    cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), selectKey, sv, methodParam.getUserLang(), methodParam.getCompanyId()));
        }
        return list;
    }

    /**
     * 根据code取字典选中项信息（默认取名称）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param selectValue     选中项
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    @Override
    public String getSelectOptionAttrByCode(MethodParam methodParam, String selectKey, String selectValue, String querySelectName) {
        return RegexUtil.optNotNullMap(this.getSelectOptionByCode(methodParam, selectKey, selectValue, null))
                .map(i -> i.get(RegexUtil.optStrOrVal(querySelectName, Constants.SELECT_NAME_NAME))).map(RegexUtil::optStrOrNull).orElse(null);
    }

    /**
     * 根据类型查询字典（【缓存模式】）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @return 选中项信息
     */
    @Override
    public List<Map<String, Object>> getSelectOptionListWithCache(MethodParam methodParam, String selectKey) {
        Object dl = ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_DATA + selectKey);
        if (null == dl) {
            List<Map<String, Object>> list = this.getSelectOptionList(methodParam, selectKey, null);
            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_DATA + selectKey, list);
            return list;
        } else {
            return DataChangeUtil.scdObjToList(dl);
        }
    }

    /**
     * 根据code取字典选中项信息（默认取名称【缓存模式】）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param selectValue     选中项
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    @Override
    public String getSelectOptionAttrByCodeWithCache(MethodParam methodParam, String selectKey, String selectValue, String querySelectName) {
        return RegexUtil.optNotNullMap(RegexUtil.optNotNullList(getSelectOptionListWithCache(methodParam, selectKey)).map(l -> l.stream()
                .filter(m -> RegexUtil.optEquals(m.get(Constants.SELECT_CODE_NAME), selectValue)).findFirst().orElse(null)).orElse(null))
                .map(i -> i.get(RegexUtil.optStrOrVal(querySelectName, Constants.SELECT_NAME_NAME))).map(RegexUtil::optStrOrNull).orElse(null);
    }

    /**
     * 根据Text取字典选中项信息（默认取名称【缓存模式】）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param selectText     选中项
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    @Override
    public String getSelectOptionAttrByTextWithCache(MethodParam methodParam, String selectKey, String selectText, String querySelectName) {
        return RegexUtil.optNotNullMap(RegexUtil.optNotNullList(getSelectOptionListWithCache(methodParam, selectKey)).map(l -> l.stream()
                .filter(m -> RegexUtil.optEquals(m.get(Constants.SELECT_NAME_NAME), selectText)).findFirst().orElse(null)).orElse(null))
                .map(i -> i.get(RegexUtil.optStrOrVal(querySelectName, Constants.SELECT_CODE_NAME))).map(RegexUtil::optStrOrNull).orElse(null);
    }

    /**
     * 根据code取字典选中项名称（默认取名称【缓存模式】）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @return 选中项信息
     */
    @Override
    public String getSelectOptionTextByCodeWithCache(MethodParam methodParam, String selectKey, String selectValue) {
        return this.getSelectOptionAttrByCodeWithCache(methodParam, selectKey, selectValue, null);
    }

    /**
     * 根据Text取字典选中项名称（默认取名称【缓存模式】）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectText 选中项
     * @return 选中项信息
     */
    @Override
    public String getSelectOptionCodeByTextWithCache(MethodParam methodParam, String selectKey, String selectText) {
        return this.getSelectOptionAttrByTextWithCache(methodParam, selectKey, selectText, null);
    }

    /**
     * 取字典第一个选项信息（默认取名称【缓存模式】）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    @Override
    public String getSelectFirstOptionAttrByCodeWithCache(MethodParam methodParam, String selectKey, String querySelectName) {
        return RegexUtil.optNotBlankStrOpt(ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_FIRST_DATA + selectKey + querySelectName)).orElseGet(() -> {
            String s = RegexUtil.optNotNullMap(RegexUtil.optNotNullList(getSelectOptionListWithCache(methodParam, selectKey)).map(l -> l.stream().findFirst().orElse(null)).orElse(null))
                    .map(i -> i.get(RegexUtil.optStrOrVal(querySelectName, Constants.SELECT_NAME_NAME))).map(RegexUtil::optStrOrNull).orElse(null);
            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_FIRST_DATA + selectKey + querySelectName, s);
            return s;
        });
    }

    /**
     * 根据code取字典选中项名称
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @return 选中项信息
     */
    @Override
    public String getSelectOptionTextByCode(MethodParam methodParam, String selectKey, String selectValue) {
        return this.getSelectOptionAttrByCode(methodParam, selectKey, selectValue, null);
    }

    /**
     * 根据code取字典选中项信息
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @return 选中项信息
     */
    @Override
    public Map<String, Object> getSelectOptionByCode(MethodParam methodParam, String selectKey, String selectValue, Map<String, Object> paramMap) {
        if (!RegexUtil.optIsPresentStr(selectKey)) {
            return null;
        } else if ("asset_info_by_scan".equals(selectKey)) {
            return RegexUtil.optNotNullList(selectOptionsMapper.findAssetInfoForByScan(methodParam.getSchemaName(), selectValue)).map(l -> l.get(0)).orElse(null); // 设备扫码
        } else if ("asset_info_by_scan_condition".equals(selectKey)) {
            String category_id = RegexUtil.optStrOrNull(paramMap.get("category_id"));
            String position_code = RegexUtil.optStrOrExpNotNull(paramMap.get("position_code"), LangConstant.TITLE_ASSET_G);
            return RegexUtil.optNotNullList(selectOptionsMapper.findAssetInfoForByScanCondition(methodParam.getSchemaName(), selectValue, category_id, position_code)).map(l -> l.get(0)).orElse(null); // 设备扫码
        } else if ("asset_position_info_by_scan".equals(selectKey)) {
            return RegexUtil.optNotNullList(selectOptionsMapper.findAssetPositionInfoForByScan(methodParam.getSchemaName(), selectValue)).map(l -> l.get(0)).orElse(null); // 设备位置扫码
        } else if ("bom_info_by_scan".equals(selectKey)) {
            String bom_id = RegexUtil.optStrOrExpNotNull(paramMap.get("bom_id"), LangConstant.TITLE_ABAA_T);
            Integer stock_id = RegexUtil.optIntegerOrExpParam(paramMap.get("stock_id"), LangConstant.TITLE_AJ_Q);
            return selectOptionsMapper.findBomInfoForByScan(methodParam.getSchemaName(), bom_id, stock_id);
        } else if ("bom_info_by_scan_stock_id".equals(selectKey)) {
            String bom_id = RegexUtil.optStrOrExpNotNull(paramMap.get("bom_id"), LangConstant.TITLE_ABAA_T);
            Integer stock_id = RegexUtil.optIntegerOrExpParam(paramMap.get("stock_id"), LangConstant.TITLE_AJ_Q);
            return selectOptionsMapper.findBomInfoForByScanAndStockId(methodParam.getSchemaName(), methodParam.getUserId(), bom_id, stock_id);
        } else if ("login_user_info".equals(selectKey)) {
            Map<String, Object> map = new HashMap<>();
            map.put(Constants.SELECT_CODE_NAME, "1");
            map.put(Constants.SELECT_NAME_NAME, methodParam.getUserName());
            map.put("user_id", methodParam.getUserId());
            map.put("account", methodParam.getAccount());
            return map;
        }
        String checkKey = "," + selectKey + ",";
        if (",validUser,group_role_user,user_with_role_sub_work_code,group_role_user_by_plan_code,users_by_role,receive_user_with_permission,user_with_asset_permission,user_list_by_permission,".contains(checkKey)) {
            selectKey = "user";
        } else if (",work_type_with_relation_type,work_type_all,work_type_permission,work_list_type_permission,works_type_permission,".contains(checkKey)) {
            selectKey = "work_type";
        } else if ("allUserGroups".equals(selectKey)) {
            selectKey = "userGroups";
        } else if (",customerFacilities,insideFacilities,service_provider,customer,all_facilities,manufacturer,all_facility_with_type,inner_facility_with_type,".contains(checkKey)) {
            selectKey = "onlyFacilities";
        } else if (",asset_with_position,test_asset_list,asset_out_for_search,asset_input_for_search,asset_allocation_for_search,asset_position_list_with_inventory,asset_list_by_position_with_inventory,".contains(checkKey)) {
            selectKey = "asset";
        } else if ("category_fault_code".equals(selectKey)) {
            selectKey = "fault_code";
        } else if (",asset_type_app,asset_type_all,asset_category_permission,asset_category_with_id,".contains(checkKey)) {
            selectKey = "asset_type";
        } else if (",asset_position_by_parent_code,asset_position_with_asset,asset_position_permission,asset_position_with_id,".contains(checkKey)) {
            selectKey = "asset_position";
        } else if (",asset_position_remark,".contains(checkKey)) {
            selectKey = "asset_position_remark";
        } else if (",stock_bom,bom_out_list,bom_detail_all_info,bom_list_with_type,".contains(checkKey)) {
            selectKey = "bom";
        } else if (",bom_type_all,bom_type_with_id,".contains(checkKey)) {
            selectKey = "bom_type";
        } else if ("onlyStock".equals(selectKey) || "stock_list_with_like".equals(selectKey)) {
            selectKey = "stock_list";
        } else if ("part_list".equals(selectKey)) {
            selectKey = "asset_part";
        } else if ("group_with_type".equals(selectKey)) {
            selectKey = "userGroups";
        } else if ("asset_running_status_with_asset".equals(selectKey)) {
            selectKey = "asset_status";
        } else if (",asset_model_category_with_id,asset_model_category,".contains(checkKey)) {
            selectKey = "asset_model_all";
        } else if (",all_facilities_with_id,asset_position_facility,".contains(checkKey)) {
            selectKey = "onlyFacilities";
        }
        String key = RegexUtil.optStrOrError(selectKey, methodParam, ErrorConstant.EC_SELECT_2);
        String sv = RegexUtil.optStrOrPrmError(selectValue, methodParam, ErrorConstant.EC_SELECT_3, selectKey);
        return RegexUtil.optNotBlankStrOpt(SqlConstant.SELECTS_FOR_DETAIL.get(key)).map(s -> s.replace(SqlConstant.SCHEMA_NAME, methodParam.getSchemaName()))
                .map(s -> s.replace(SqlConstant.SQL_WHERE_NAME, sv)).map(sql -> selectOptionsMapper.findSelectOption(sql))
                .orElseGet(() -> this.getStaticSelectOption(methodParam, key, sv));
    }

    /**
     * 静态字典选中项查询
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param code        选中项值
     * @return 选中项信息
     */
    @Override
    public Map<String, Object> getStaticSelectOption(MethodParam methodParam, String selectKey, String code) {
        return RegexUtil.optEqualsOpt(methodParam.getLangChangeType(), Constants.LANG_CHANGE_PAGE).map(info -> selectOptionsMapper.findUnLangStaticOption(methodParam.getSchemaName(), selectKey, code))
                .orElseGet(() -> cacheUtilService.findLangStaticOption(methodParam.getSchemaName(), methodParam.getCompanyId(), methodParam.getUserLang(), selectKey, code));
    }

    /**
     * 静态字典查询f
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @return 字典信息
     */
    @Override
    public List<Map<String, Object>> getStaticSelectList(MethodParam methodParam, String selectKey) {
        return RegexUtil.optEqualsOpt(methodParam.getLangChangeType(), Constants.LANG_CHANGE_PAGE)
                .map(info -> selectOptionsMapper.findUnLangStaticDataByType(methodParam.getSchemaName(), selectKey))
                .orElseGet(() -> cacheUtilService.findLangStaticDataByType(methodParam.getSchemaName(), methodParam.getCompanyId(), methodParam.getUserLang(), selectKey));
    }

    /**
     * 静态字典查询（对象形式）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @return 字典信息
     */
    @Override
    public Map<String, Object> getStaticSelectForInfo(MethodParam methodParam, String selectKey) {
        Object dl = ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_STATIC_INFO + selectKey);
        if (null == dl) {
            Map<String, Object> map = RegexUtil.optEqualsOpt(methodParam.getLangChangeType(), Constants.LANG_CHANGE_PAGE).map(info -> DataChangeUtil.scdListToMap(selectOptionsMapper.findUnLangStaticDataByType(methodParam.getSchemaName(), selectKey),
                    Constants.SELECT_CODE_NAME, Constants.SELECT_NAME_NAME)).orElseGet(() -> DataChangeUtil.scdListToMap(cacheUtilService.findLangStaticDataByType(methodParam.getSchemaName(), methodParam.getCompanyId(),
                    methodParam.getUserLang(), selectKey), Constants.SELECT_CODE_NAME, Constants.SELECT_NAME_NAME));
            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_STATIC_INFO + selectKey, map);
            return map;
        } else {
            return DataChangeUtil.scdToMap(dl);
        }
    }

    /**
     * 根据业务id获取目标表列表
     */
    @Override
    public List<Map<String, Object>> getTargetTableList(MethodParam methodParam, String data_type, String parent_code) {
        return selectOptionsMapper.findTargetTableList(methodParam.getSchemaName(), data_type, parent_code);
    }

    /**
     * 根据工单类型id查询工单类型信息
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 工单类型信息
     */
    @Override
    public Map<String, Object> getWorkTypeById(MethodParam methodParam, Integer id) {
        return selectOptionsMapper.findWorkTypeById(methodParam.getSchemaName(), id);
    }

    /**
     * 根据key和extKey获取下拉框数据
     *
     * @param methodParam  入参
     * @param selectKey    入参
     * @param selectExtKey 入参
     * @return 根据key和extKey获取下拉框数据
     */
    @Override
    public List<Map<String, Object>> getSelectOptionByKeyAndExtKey(MethodParam methodParam, String selectKey, String selectExtKey) {
        String key = RegexUtil.optStrOrError(selectKey, methodParam, ErrorConstant.EC_SELECT_2);
        String sv = RegexUtil.optStrOrPrmError(selectExtKey, methodParam, ErrorConstant.EC_SELECT_3, selectKey);
        return RegexUtil.optNotBlankStrOpt(SqlConstant.SELECTS.get(key)).map(s -> s.replace(SqlConstant.SCHEMA_NAME, methodParam.getSchemaName()))
                .map(s -> s.replace(SqlConstant.SQL_WHERE_NAME, sv)).map(sql -> selectOptionsMapper.findSelectOptionList(sql))
                .orElseGet(() -> cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), selectKey, selectExtKey, methodParam.getUserLang(), methodParam.getCompanyId()));
    }

    /**
     * 根据key和code获取静态字典参数数据
     *
     * @param methodParam 系统参数
     * @param selectValue 选中项
     * @return 静态字典参数数据
     */
    @Override
    public List<Map<String, Object>> getSelectOptionInfoByKeyAndCode(MethodParam methodParam, String selectValue) {
//        return selectOptionsMapper.findSelectOptionInfoByKeyAndCode(methodParam.getSchemaName(), selectValue);
        return cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), "interface_address_param", selectValue, methodParam.getUserLang(), methodParam.getCompanyId());
    }

    /**
     * 聚合json字段数据
     *
     * @param schemaName        入参
     * @param tableName         入参
     * @param keyName           数据主键名称
     * @param keyValue          数据主键值
     * @param columnName        数据json字段名称
     * @param jsonColumnName    json待处理字段名称
     * @param jsonColumnValue   json待处理字段值
     * @param polymerizationStr 聚合类型
     * @param fieldColumnName   聚合字段名
     */
    @Override
    public Double selectPolymerizationJsonColumnData(String schemaName, String tableName, String keyName, String keyValue, String columnName, String jsonColumnName, String jsonColumnValue, String polymerizationStr, String fieldColumnName) {
        return selectOptionsMapper.selectPolymerizationJsonColumnData(schemaName, tableName, keyName, keyValue, columnName, jsonColumnName, jsonColumnValue, polymerizationStr, fieldColumnName);
    }

    /**
     * 根据字段名和value值获取名称
     *
     * @param methodParam 入参
     * @param name        入参
     * @param value       入参
     * @return 获取名称
     */
    @Override
    public Map<String, Object> getInfoByValue(MethodParam methodParam, String name, String value) {
        if (!RegexUtil.optIsBlankStr(value)) {
            if ("status".equals(name)) {
                return selectOptionsMapper.findStatusInfoByValue(methodParam.getSchemaName(), value);
            } else if ("supplier_id".equals(name)) {
                return selectOptionsMapper.findFacilitiesInfoByValue(methodParam.getSchemaName(), value);
            } else if ("manufacturer_id".equals(name)) {
                return selectOptionsMapper.findFacilitiesInfoByValue(methodParam.getSchemaName(), value);
            } else if ("unit_id".equals(name)) {
                return selectOptionsMapper.findUnitInfoByValue(methodParam.getSchemaName(), value);
            } else if ("category_id".equals(name)) {
                return selectOptionsMapper.findCategoryInfoByValue(methodParam.getSchemaName(), value);
            } else if ("importment_level_id".equals(name)) {
                return selectOptionsMapper.findImportmentInfoByValue(methodParam.getSchemaName(), value);
            } else if ("currency_id".equals(name)) {
                return selectOptionsMapper.findCurrencyInfoByValue(methodParam.getSchemaName(), value);
            } else if ("position_code".equals(name)) {
                return selectOptionsMapper.findAssetPositionInfoByValue(methodParam.getSchemaName(), value);
            } else if ("asset_model_id".equals(name)) {
                return selectOptionsMapper.findAssetModelInfoByValue(methodParam.getSchemaName(), value);
            } else if ("install_currency_id".equals(name)) {
                return selectOptionsMapper.findCurrencyInfoByValue(methodParam.getSchemaName(), value);
            } else if ("buy_currency_id".equals(name)) {
                return selectOptionsMapper.findCurrencyInfoByValue(methodParam.getSchemaName(), value);
            } else if ("guarantee_status_id".equals(name)) {
                return selectOptionsMapper.findCloudDataInfoByValue(methodParam.getSchemaName(), "warranties_type", value);
            } else if ("running_status_id".equals(name)) {
                return selectOptionsMapper.findRunningStatusInfoByValue(methodParam.getSchemaName(), value);
            } else if ("asset_status".equals(name)) {
                return selectOptionsMapper.findAssetStatusInfoByValue(methodParam.getSchemaName(), value);
            } else if ("asset_icon".equals(name)) {
                return selectOptionsMapper.findFilesInfoByValue(methodParam.getSchemaName(), value);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 查询设备基本字段信息（【缓存模式】）
     *
     * @param methodParam 系统参数
     * @return 设备基本字段信息
     */
    @Override
    public List<String> getAssetCommonFieldList(MethodParam methodParam) {
        Object dl = ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_ASSET_COM_FIELD);
        if (null == dl) {
            List<String> list = selectOptionsMapper.findAssetCommonFieldList(methodParam.getSchemaName());
            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_SELECT_ASSET_COM_FIELD, list);
            return list;
        } else {
            return DataChangeUtil.scdObjToListStr(dl);
        }
    }

    public List<Map<String, Object>> findResource(Long company_id, String condition) {
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamTwo(company_id, condition, "company_id", "condition", "findResource"));
        return result;
    }

    public List<Map<String, Object>> findWorksLog(Long company_id) {
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestLang(company_id, "company_id", "findWorksLog"));
        return result;
    }
}
