package com.gengyun.senscloud.service.dynamic.impl;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.dynamic.ProductTaskService;
import com.gengyun.senscloud.service.job.WorkOrderJobService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 生产作业单
 */
@Service
public class ProductTaskServiceImpl implements ProductTaskService {
    @Resource
    AssetMapper assetMapper;
    @Resource
    ProductTaskMapper productTaskMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    LogsService logService;
    @Resource
    AssetMonitorCurrentMapper assetMonitorCurrentMapper;
    @Resource
    PlanWorkMapper planWorkMapper;
    @Resource
    WorksMapper worksMapper;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    private CommonUtilService commonUtilService;

    @Override
    public Map<String, Map<String, Boolean>> getProductTaskPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[21]);
    }

    /**
     * 新增生产作业单
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void newProductTask(MethodParam methodParam, Map<String, Object> pm) {
        productTaskMapper.insertProductTask(methodParam.getSchemaName(), pm);
    }

    /**
     * 新增生产作业下井
     *
     * @param methodParam 入参
     * @param pms         入参
     */
    @Override
    public void newProductTaskWell(MethodParam methodParam, List<Map<String, Object>> pms) {
        productTaskMapper.insertProductTaskWell(methodParam.getSchemaName(), pms);
    }

    /**
     * 新增生产作业下井次数
     *
     * @param methodParam 入参
     * @param pms         入参
     */
    @Override
    public void newProductTaskWellItem(MethodParam methodParam, List<Map<String, Object>> pms) {
        productTaskMapper.insertProductTaskWellItem(methodParam.getSchemaName(), pms);
    }

    /**
     * 查询生产作业单列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业单列表
     */
    @Override
    public Map<String, Object> findProductTaskPage(MethodParam methodParam, Map<String, Object> pm) {
        Map<String, Object> result = new HashMap<>();
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = productTaskMapper.findProductTaskPageCount(methodParam.getSchemaName(), pm);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        pm.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = productTaskMapper.findProductTaskPage(methodParam.getSchemaName(), pm);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 查询生产作业单详情
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业单详情
     */
    @Override
    public Map<String, Object> findProductTaskInfo(MethodParam methodParam, Map<String, Object> pm) {
        String product_task_code = RegexUtil.optStrOrExpNotNull(pm.get("product_task_code"), LangConstant.TITLE_B_O);
        return productTaskMapper.findProductTaskInfo(methodParam.getSchemaName(), product_task_code);
    }

    /**
     * 查询生产作业单明细列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业单明细列表
     */
    @Override
    public List<Map<String, Object>> findProductTaskWellList(MethodParam methodParam, Map<String, Object> pm) {
        String product_task_code = RegexUtil.optStrOrExpNotNull(pm.get("product_task_code"), LangConstant.TITLE_B_O);
        return productTaskMapper.findProductTaskWellList(methodParam.getSchemaName(), product_task_code);
    }

    /**
     * 更新生产作业信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void modifyProductTask(MethodParam methodParam, Map<String, Object> pm) {
        String product_task_code = RegexUtil.optStrOrExpNotNull(pm.get("product_task_code"), LangConstant.TITLE_B_O);
        //获取更新前的数据
        Map<String, Object> oldProductTask = productTaskMapper.findProductTaskInfo(methodParam.getSchemaName(), product_task_code);
        if (RegexUtil.optNotNull(oldProductTask).isPresent()) {
            productTaskMapper.updateProductTask(methodParam.getSchemaName(), pm);
            JSONArray loger = LangUtil.compareMap(pm, oldProductTask);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10003, product_task_code,
                    LangUtil.doSetLogArray("更新了产作业单信息", LangConstant.TEXT_AJ,
                            new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CIK, loger.toString()}));
        }

    }

    /**
     * 更新生产作业下井信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void modifyProductTaskWell(MethodParam methodParam, Map<String, Object> pm) {
        String id = RegexUtil.optStrOrExpNotNull(pm.get("id"), LangConstant.MSG_BI);
//        String task_well_code = RegexUtil.optStrOrExpNotNull(pm.get("task_well_code"), LangConstant.TITLE_CGH);
        RegexUtil.optStrOrExpNotNull(pm.get("is_contain_hydrogen_sulfide"), LangConstant.TITLE_CGH);//是否含硫化氢
        RegexUtil.optStrOrExpNotNull(pm.get("cable_joint_location"), LangConstant.TITLE_CGH);//电缆接头位置
        RegexUtil.optStrOrExpNotNull(pm.get("total_vehicle_kilometers"), LangConstant.TITLE_CGH);//车辆行驶总公里数
        RegexUtil.optStrOrExpNotNull(pm.get("asset_out_code"), LangConstant.TITLE_CGN);//出库单号
        Float max_temperature = RegexUtil.optFloatOrNull(pm.get("max_temperature"), LangConstant.TITLE_CGK);
        Float bottom_pressure = RegexUtil.optFloatOrNull(pm.get("bottom_pressure"), LangConstant.TITLE_CGL);
        //车辆行驶总公里数
//        String total_vehicle_kilometers = RegexUtil.optStrOrExpNotNull(pm.get("total_vehicle_kilometers"), LangConstant.TITLE_CGH);
//        String temperature = RegexUtil.optStrOrExpNotNull(max_temperature, LangConstant.TITLE_CGK);
//        String pressure = RegexUtil.optStrOrExpNotNull(bottom_pressure, LangConstant.TITLE_CGL);
        pm.put("schema_name", methodParam.getSchemaName());
        //获取更新前的数据
        Map<String, Object> oldProductTaskWell = productTaskMapper.findProductTaskWellInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldProductTaskWell).isPresent()) {
//            Date now = new Date(); // 创建一个Date对象，获取当前时间
            // 指定格式化格式
//            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd HH-mm-ss");
//            pm.put("gather_time", f.format(now));
            productTaskMapper.updateProductTaskWell(methodParam.getSchemaName(), pm);
            //查询子工单设备，修改监控压力，温度
//            List<Map<String, Object>> assets = productTaskMapper.findProductTaskWellItemAssetByTaskWellCode(methodParam.getSchemaName(), oldProductTaskWell.get("id").toString());
//            if (RegexUtil.optIsPresentList(assets)) {
//                for (Map<String, Object> asset : assets) {
//                    pm.put("asset_code", asset.get("asset_id"));
//                    pm.put("monitor_name", "最高井温");
//                    pm.put("monitor_value_type", 9);
//                    pm.put("monitor_value_unit", "℃");
//                    pm.put("monitor_value", temperature);
//                    assetMonitorCurrentMapper.updateAssetMonitorCurrentByAssetIdAndMonitorName(pm);
//                    pm.put("monitor_name", "井低压力");
//                    pm.put("monitor_value_type", 9);
//                    pm.put("monitor_value_unit", "Mpc");
//                    pm.put("monitor_value", pressure);
//                    assetMonitorCurrentMapper.updateAssetMonitorCurrentByAssetIdAndMonitorName(pm);
//                }
//            }
            //根据生产下井作业编码获取所有的设备
//            List<Map<String, Object>> productTaskWellItemAssetList = productTaskMapper.findProductTaskWellItemAssetList(methodParam.getSchemaName(), id);
            //根据出库单号获取所有设备
            List<Map<String, Object>> productTaskWellItemAssetList = productTaskMapper.findAssetListByAssetOutCode(methodParam.getSchemaName(), pm);
            if (RegexUtil.optIsPresentList(productTaskWellItemAssetList)) {
                List<Map<String, Object>> columns = selectOptionService.getSelectOptionList(methodParam, "plan_work_column_key", null);
                //查询设备列表是否触发了维保计划  如果触发了 就生成工单
                triggleAssetList(methodParam, pm, productTaskWellItemAssetList, oldProductTaskWell, 1, columns);
            }
            JSONArray loger = LangUtil.compareMap(pm, oldProductTaskWell);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10003, id,
                    LangUtil.doSetLogArray("更新了生产作业下井信息", LangConstant.TEXT_AJ,
                            new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CHK, loger.toString()}));
        }
    }

    /**
     * 判断该设备是否是该维保计划的维保对象
     *
     * @param methodParam   入参
     * @param plan_work_map 入参
     * @param assetMap      入参
     * @return 是否
     */
    private boolean checkPlanWithAsset(MethodParam methodParam, Map<String, Object> plan_work_map, Map<String, Object> assetMap) {
        String plan_code = plan_work_map.get("plan_code").toString();
        String asset_id = assetMap.get("id").toString();
        if (RegexUtil.optNotNull(planWorkMapper.findPlanWorkAssetByPlanCode(methodParam.getSchemaName(), plan_code, asset_id)).isPresent()) {
            return true;
        }
        String position_code = null;
        Integer category_id = null;
        Integer asset_model_id = null;
        if (RegexUtil.optIsPresentStr(assetMap.get("position_code"))) {
            position_code = assetMap.get("position_code").toString();
        }
        if (RegexUtil.optNotNull(assetMap.get("category_id")).isPresent()) {
            category_id = Integer.valueOf(assetMap.get("category_id").toString());
        }
        if (RegexUtil.optNotNull(assetMap.get("asset_model_id")).isPresent()) {
            asset_model_id = Integer.valueOf(assetMap.get("asset_model_id").toString());
        }
        //获取该计划的所有计划工单的配置
        List<Map<String, Object>> plan_work_setting_lisList = planWorkMapper.findPlanWorkSettingList(methodParam.getSchemaName(), plan_code);
        //设备启用时间
        String enable_time = null;
        DateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT);
        if (RegexUtil.optIsPresentStr(assetMap.get("enable_time"))) {
            enable_time = assetMap.get("enable_time").toString();
        }
        for (Map<String, Object> setting : plan_work_setting_lisList) {
            boolean isCloud = true;
            if (true == (boolean) setting.get("is_all_asset")) {
                try {
                    if (RegexUtil.optIsPresentStr(setting.get("enable_begin_date"))) {//启用开始日期
                        if (!RegexUtil.optIsPresentStr(enable_time)) {
                            isCloud = false;
                        } else {
                            Date enable_date = sdf.parse(enable_time);
                            Date enable_begin_date = sdf.parse(setting.get("enable_begin_date").toString());
                            if (enable_date.getTime() < enable_begin_date.getTime()) {
                                isCloud = false;
                            }
                        }
                    }
                    if (RegexUtil.optIsPresentStr(setting.get("enable_end_date"))) {//启用截止日期
                        if (!RegexUtil.optIsPresentStr(enable_time)) {
                            isCloud = false;
                        } else {
                            Date enable_date = sdf.parse(enable_time);
                            Date enable_end_date = sdf.parse(setting.get("enable_end_date").toString());
                            if (enable_date.getTime() > enable_end_date.getTime()) {
                                isCloud = false;
                            }
                        }
                    }
                } catch (Exception e) {
//                    System.out.println(e);
                }
                if (RegexUtil.optIsPresentStr(setting.get("position_code"))) {
                    if (!RegexUtil.optIsPresentStr(position_code)) {//计划工单设备位置不为空  设备自身设备为空 则不属于维保对象
                        isCloud = false;
                    }
                    if (!setting.get("position_code").toString().equals(position_code)) {//计划工单设备位置与设备自身设备不等 则不属于维保对象
                        isCloud = false;
                    }
                }
                if (RegexUtil.optNotNull(setting.get("asset_category_id")).isPresent()) {
                    if (!RegexUtil.optNotNull(category_id).isPresent()) {//计划工单设备类型不为空  设备自身设备类型为空 则不属于维保对象
                        isCloud = false;
                    }
                    if (!Integer.valueOf(setting.get("asset_category_id").toString()).equals(category_id)) {//计划工单设备类型不等于设备自身设备类型 则不属于维保对象
                        isCloud = false;
                    }
                }
                if (RegexUtil.optNotNull(setting.get("asset_model_id")).isPresent()) {
                    if (!RegexUtil.optNotNull(asset_model_id).isPresent()) {//计划工单设备型号不为空  设备自身设备型号为空 则不属于维保对象
                        isCloud = false;
                    }
                    if (!Integer.valueOf(setting.get("asset_model_id").toString()).equals(asset_model_id)) {//计划工单设备型号不等于设备自身设备型号 则不属于维保对象
                        isCloud = false;
                    }
                }
                if (isCloud) {
                    return true;
                }
//                return isCloud;
            }
        }
        return false;
    }

    /**
     * 查询设备列表是否触发了维保计划  如果触发了 就生成工单
     *
     * @param methodParam        入参
     * @param params             生产作业单数据
     * @param assetList          入参
     * @param oldProductTaskWell
     * @param type               1主数据   2子数据
     */
    private void triggleAssetList(MethodParam methodParam, Map<String, Object> params, List<Map<String, Object>> assetList, Map<String, Object> oldProductTaskWell, int type, List<Map<String, Object>> columns) {
        //获取按照条件触发的所有条件
        List<Map<String, Object>> planWorkTrigglelist = planWorkMapper.findAllColumnKeyListTriggleList(methodParam.getSchemaName());
        for (Map<String, Object> productTaskWellItemAsset : assetList) {
            Map<String, Object> endplanWorkTriggle = new HashMap<>();
            JSONArray jsonArray = null;
            if (null == productTaskWellItemAsset.get("properties")) {
                jsonArray = new JSONArray();
                addDefaultParams(jsonArray);
            } else {
                jsonArray = JSONArray.fromObject(productTaskWellItemAsset.get("properties"));
                if (null == jsonArray) {
                    jsonArray = new JSONArray();
                }
            }
            for (Map<String, Object> column : columns) {
                String reserve1 = RegexUtil.optStrOrVal(column.get("reserve1"), "no");
                String value = RegexUtil.optStrOrVal(column.get("value"), "");
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String field_code = jsonObject.optString("field_code", "");
                    String field_value = null;
                    String db_field_value = null;
                    if (value.equals(field_code)) {
                        if ("total_vehicle_kilometers".equals(field_code)) {
                            String old_total_vehicle_kilometers = RegexUtil.optStrOrVal(oldProductTaskWell.get(value), "0");//生产作业单db记录值
                            String maximum_kilometers = RegexUtil.optStrOrVal(params.get(value), "0");//生产作业单传值
                            db_field_value = jsonObject.optString("field_value", "0");//设备详情记录监控值
                            float total_meter = Float.parseFloat(db_field_value) - Float.parseFloat(old_total_vehicle_kilometers) + Float.parseFloat(maximum_kilometers);
                            field_value = String.valueOf(total_meter);
                        } else if ("well_occupation_time".equals(field_code) || "total_time".equals(field_code)) {
                            if (null == oldProductTaskWell.get("asset_out_code")) {
                                String old_well_occupation_time = RegexUtil.optStrOrVal(oldProductTaskWell.get(value), "0");
                                db_field_value = jsonObject.optString("field_value", "0");//设备详情记录监控值
                                float total_usage_time = Float.parseFloat(db_field_value) + Float.parseFloat(old_well_occupation_time);
                                field_value = String.valueOf(total_usage_time);
                            }
                        } else if ("well_times".equals(field_code)) {//井次
                            if (productTaskWellItemAsset.containsKey("isAdd") && (boolean) productTaskWellItemAsset.get("isAdd")) {
                                db_field_value = jsonObject.optString("field_value", "0");//设备详情记录监控值
                                field_value = String.valueOf(Integer.valueOf(db_field_value) + 1);
                            }
                            if (productTaskWellItemAsset.containsKey("isDelete") && (boolean) productTaskWellItemAsset.get("isDelete")) {
                                db_field_value = jsonObject.optString("field_value", "0");//设备详情记录监控值
                                if (Integer.valueOf(db_field_value) > 0) {
                                    field_value = String.valueOf(Integer.valueOf(db_field_value) + 1);
                                } else {
                                    field_value = db_field_value;
                                }
                            }
                        } else {
                            if (null == RegexUtil.optStrOrNull(params.get(value))) {
                                field_value = jsonObject.optString("field_value", "");
                            } else {
                                field_value = RegexUtil.optStrOrVal(params.get(value), "0");//生产作业单传值
                            }
                        }
                        //生产作业单值需存在，并且设备的监控值也许存在
                        if (RegexUtil.optIsPresentStr(field_value)) {
                            //根据设备id查询设备详情
                            Map<String, Object> assetMap = assetMapper.findAssetById(methodParam.getSchemaName(), productTaskWellItemAsset.get("asset_id").toString());
                            //查询是否要触发维保工单计划
                            for (Map<String, Object> planWorkTriggle : planWorkTrigglelist) {
                                if (RegexUtil.optIsPresentStr(planWorkTriggle.get("column_key"))
                                        && RegexUtil.optIsPresentStr(planWorkTriggle.get("column_condition"))
                                        && RegexUtil.optIsPresentStr(planWorkTriggle.get("column_value"))
                                        && field_code.equals(planWorkTriggle.get("column_key"))) {
                                    String column_condition = planWorkTriggle.get("column_condition").toString();
                                    String column_value = planWorkTriggle.get("column_value").toString();
                                    Float field_value_float = Float.parseFloat(field_value);
                                    Float column_value_float = Float.parseFloat(column_value);
                                    String plan_code = planWorkTriggle.get("plan_code").toString();
                                    if (">=".equals(column_condition) && field_value_float >= column_value_float) {
                                        if (RegexUtil.optNotNull(endplanWorkTriggle.get(plan_code)).isPresent()) {
                                            endplanWorkTriggle.put(plan_code, Integer.valueOf(endplanWorkTriggle.get(plan_code).toString()) + 1);
                                        } else {
                                            endplanWorkTriggle.put(plan_code, 1);
                                        }
                                    } else if ("<=".equals(column_condition) && field_value_float <= column_value_float) {
                                        if (RegexUtil.optNotNull(endplanWorkTriggle.get(plan_code)).isPresent()) {
                                            endplanWorkTriggle.put(plan_code, Integer.valueOf(endplanWorkTriggle.get(plan_code).toString()) + 1);
                                        } else {
                                            endplanWorkTriggle.put(plan_code, 1);
                                        }
                                    } else if ("==".equals(column_condition) && field_value_float.equals(column_value_float)) {
                                        if (RegexUtil.optNotNull(endplanWorkTriggle.get(plan_code)).isPresent()) {
                                            endplanWorkTriggle.put(plan_code, Integer.valueOf(endplanWorkTriggle.get(plan_code).toString()) + 1);
                                        } else {
                                            endplanWorkTriggle.put(plan_code, 1);
                                        }
                                    }
                                }
                            }
                            //根据满足的触发条件的计划编码
                            Set<String> keySet = endplanWorkTriggle.keySet();
                            boolean is_reset_field_value = false;
                            for (String plan_code : keySet) {
                                Map<String, Object> plan_work_map = planWorkMapper.findPlanScheduleInfoByCode(methodParam.getSchemaName(), plan_code);
                                if (RegexUtil.optNotNull(plan_work_map).isPresent()) {
                                    boolean is_do_plan_work = false;
                                    //触发条件
                                    int do_type = Integer.valueOf(plan_work_map.get("do_type").toString());
                                    if (1 == do_type) {//1：所有条件满足执行
                                        int planTriggleCount = planWorkMapper.findCountTriggleByPlanCode(methodParam.getSchemaName(), plan_code);
                                        int count = Integer.valueOf(endplanWorkTriggle.get(plan_code).toString());
                                        if (planTriggleCount == count) {
                                            //判断该设备和该维保计划是否有关联
                                            is_do_plan_work = checkPlanWithAsset(methodParam, plan_work_map, assetMap);
                                            //触发维保计划
                                        }
                                    } else if (2 == do_type) {//2：任一条件满足即执行
                                        //判断该设备和该维保计划是否有关联
                                        is_do_plan_work = checkPlanWithAsset(methodParam, plan_work_map, assetMap);
                                    }
                                    if (is_do_plan_work) {
                                        is_reset_field_value = true;
                                        //判断该设备和维保计划是否还有未处理的工单  如果有则不触发工单
                                        Integer work_type_id = Integer.valueOf(plan_work_map.get("work_type_id").toString());
                                        String relation_id = assetMap.get("id").toString();
                                        if (!RegexUtil.optNotNull(worksMapper.noEndWorksByWorkTypeANdAssetId(methodParam.getSchemaName(), work_type_id, relation_id)).isPresent()) {
                                            Map<String, Object> plan_work_property = planWorkMapper.findPlanWorkPropertyByPlanCode(methodParam.getSchemaName(), plan_code);
                                            PlanWorkCalendarModel planWorkCalendarModel = new PlanWorkCalendarModel();
                                            planWorkCalendarModel.setPlan_code(plan_code);
                                            planWorkCalendarModel.setRelation_type(2);
                                            planWorkCalendarModel.setRelation_id(assetMap.get("id").toString());
                                            if (RegexUtil.optNotNull(plan_work_property).isPresent()
                                                    && RegexUtil.optNotNull(plan_work_property.get("body_property")).isPresent()) {
                                                planWorkCalendarModel.setPlan_work_property(plan_work_property.get("body_property").toString());
                                            }
                                            planWorkCalendarModel.setWork_type_id(Integer.valueOf(plan_work_map.get("work_type_id").toString()));
                                            commonUtilService.createWorkOrderByPlanWorkCalendar(methodParam, planWorkCalendarModel);
                                        }
                                    }
                                }
                            }
                            if (is_reset_field_value && "add".equals(reserve1)) {
                                jsonObject.put("field_value", "0");
                            } else {
                                jsonObject.put("field_value", field_value);
                            }
                            //修改设备参数放最后可生成工单时做
                            assetMap.put("properties", jsonArray.toString());
                            //修改设备的自定义字段监控值
                            assetMapper.updateAssetProperties(methodParam.getSchemaName(), assetMap);
                        }
                    }
                }
            }
        }
    }

    private void addDefaultParams(JSONArray jsonArray) {
        JSONObject object = new JSONObject();
        object.put("field_code", "well_times");
        object.put("field_name", "井次");
        object.put("field_value", 0);
        object.put("field_remark", "井次");
        object.put("field_source", "");
        object.put("field_view_type", "input");
        jsonArray.add(object);
        JSONObject object2 = new JSONObject();
        object2.put("field_code", "max_temperature");
        object2.put("field_name", "最大承受温度");
        object2.put("field_value", 0);
        object2.put("field_remark", "最大承受温度");
        object2.put("field_source", "");
        object2.put("field_view_type", "input");
        jsonArray.add(object2);
        JSONObject object3 = new JSONObject();
        object3.put("field_code", "bottom_pressure");
        object3.put("field_name", "最大承受压力");
        object3.put("field_value", 0);
        object3.put("field_remark", "最大承受压力");
        object3.put("field_source", "");
        object3.put("field_view_type", "input");
        jsonArray.add(object3);
        JSONObject object4 = new JSONObject();
        object4.put("field_code", "net_tension_jamming");
        object4.put("field_name", "最大承受拉力");
        object4.put("field_value", 0);
        object4.put("field_remark", "最大承受拉力");
        object4.put("field_source", "");
        object4.put("field_view_type", "input");
        jsonArray.add(object4);
        JSONObject object5 = new JSONObject();
        object5.put("field_code", "total_vehicle_kilometers");
        object5.put("field_name", "最大公里数");
        object5.put("field_value", 0);
        object5.put("field_remark", "最大公里数");
        object5.put("field_source", "");
        object5.put("field_view_type", "input");
        jsonArray.add(object5);
        JSONObject object6 = new JSONObject();
        object6.put("field_code", "well_occupation_time");
        object6.put("field_name", "使用时间");
        object6.put("field_value", 0);
        object6.put("field_remark", "使用时间");
        object6.put("field_source", "");
        object6.put("field_view_type", "input");
        jsonArray.add(object6);
        JSONObject object7 = new JSONObject();
        object7.put("field_code", "total_time");
        object7.put("field_name", "仪器使用时间");
        object7.put("field_value", 0);
        object7.put("field_remark", "仪器使用时间");
        object7.put("field_source", "");
        object7.put("field_view_type", "input");
        jsonArray.add(object7);
    }

    /**
     * 获取设备自定义字段中的监控项的额定值
     *
     * @param jsonArray
     * @param db_field_code
     * @return
     */
    private String getDbFieldValue(JSONArray jsonArray, String db_field_code) {
        String db_field_value = "0";
        for (int x = 0; x < jsonArray.size(); x++) {
            JSONObject jsonObject1 = jsonArray.getJSONObject(x);
            String fieldCode = jsonObject1.optString("field_code");
            if (db_field_code.equals(fieldCode)) {
                db_field_value = jsonObject1.optString("field_value");
                break;
            }
        }
        return db_field_value;
    }

    /**
     * 更新生产作业下井次数信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void modifyProductTaskWellItem(MethodParam methodParam, Map<String, Object> pm) {
        String task_well_item_code = RegexUtil.optStrOrExpNotNull(pm.get("id"), LangConstant.TITLE_B_O);
        String go_down_time = RegexUtil.optStrOrExpNotNull(pm.get("go_down_time"), LangConstant.TITLE_CHM);//下井时间
        String come_out_time = RegexUtil.optStrOrExpNotNull(pm.get("come_out_time"), LangConstant.TITLE_CHN);//出井时间
        if (RegexUtil.optIsPresentStr(pm.get("net_tension_jamming")) && !RegexUtil.optIsBlankStr(pm.get("net_tension_jamming"))) {
            RegexUtil.falseExp(RegexUtil.isNumeric(pm.get("net_tension_jamming").toString()), LangConstant.TEXT_AF, new String[]{"遇卡净张力", LangConstant.TITLE_AAT_X}); // {d}【{0}】格式错误，应该是{1}格式{d}
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date go_down_date = null;
        Date come_out_date = null;
        try {
            go_down_date = sdf.parse(go_down_time);
            come_out_date = sdf.parse(come_out_time);
        } catch (Exception e) {

        }
        //如果下井时间大于出井时间  则提示错误  出井时间不能小于下井时间
        if (go_down_date.getTime() > come_out_date.getTime()) {
            throw new SenscloudException(LangConstant.MSG_N);//
        }
        //查询生产作业下井设备列表
        productTaskMapper.updateProductTaskWellItem(methodParam.getSchemaName(), pm);
        //获取更新前的数据
//        Map<String, Object> oldProductTaskWellItem = productTaskMapper.findProductTaskWellInfo(methodParam.getSchemaName(), task_well_item_code);
        Map<String, Object> oldProductTaskWell = productTaskMapper.findProductTaskWellInfoByTaskWellItemCode(methodParam.getSchemaName(), task_well_item_code);
        if (RegexUtil.optNotNull(oldProductTaskWell).isPresent()) {
            List<Map<String, Object>> assetList = new ArrayList<>();
//            productTaskMapper.updateProductTaskWell(methodParam.getSchemaName(), pm);
            /// String max_temperature = oldProductTaskWell.get("max_temperature").toString();
            /// String bottom_pressure = oldProductTaskWell.get("bottom_pressure").toString();
            JSONArray loger = new JSONArray();
///            Date now = new Date(); // 创建一个Date对象，获取当前时间
            // 指定格式化格式
///            SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd HH-mm-ss");
///         Map<String, Object> current = new HashMap<>();
///            current.put("gather_time", f.format(now));
///            current.put("schema_name", methodParam.getSchemaName());
///            current.put("create_time", SenscloudUtil.getNowTime());
            if (!RegexUtil.optIsBlankStr(pm.get("product_task_well_item_asset_list"))) {
//                List<Map<String, Object>> product_task_well_item_asset_list = (List<Map<String, Object>>) pm.get("product_task_well_item_asset_list");
                String assetStr = RegexUtil.optStrOrExpNotNull(pm.get("product_task_well_item_asset_list"), LangConstant.TITLE_ASSET_I);// 设备不能为空
                List<Map<String, Object>> product_task_well_item_asset_list = (List<Map<String, Object>>) JSON.parse(assetStr);
                List<Map<String, Object>> old_product_task_well_item_asset_list =
                        productTaskMapper.findProductTaskWellItemAssetByCode(methodParam.getSchemaName(), task_well_item_code);
//                assetList.addAll(old_product_task_well_item_asset_list);
                //新增的设备
                for (Map<String, Object> product_task_well_item_asset : product_task_well_item_asset_list) {
                    boolean isAdd = true;
                    String asset_id = product_task_well_item_asset.get("asset_id").toString();
                    product_task_well_item_asset.put("task_well_item_code", task_well_item_code);
///                    current.put("asset_code", asset_id);
                    for (Map<String, Object> old_product_task_well_item_asset : old_product_task_well_item_asset_list) {
                        if (product_task_well_item_asset.get("asset_id").equals(old_product_task_well_item_asset.get("asset_id"))) {
                            isAdd = false;
                            break;
                        }
                    }
                    Map<String, Object> assetMap = assetMapper.findAssetById(methodParam.getSchemaName(), asset_id);
                    if (isAdd) {
//                        String asset_id = pm.get("asset_id").toString();
//                        Map<String, Object> assetMap = assetMapper.findAssetById(methodParam.getSchemaName(), asset_id);
                        assetMap.put("isAdd", true);
                        assetList.add(assetMap);
                        productTaskMapper.insertProductTaskWellItemAsset(methodParam.getSchemaName(), product_task_well_item_asset);
                        loger.add(LangUtil.doSetLog("新增生产作业下井次数信息", LangConstant.TEXT_AJ,
                                new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_CHL, assetMap.get("asset_name").toString()}));//新增生产作业下井次数信息设备
                    } else {
                        assetMap.put("isAdd", false);
                        assetList.add(assetMap);
                    }
///                    current.put("monitor_name", "最高井温");
///                    current.put("monitor_value_type", 9);
///                    current.put("monitor_value_unit", "℃");
///                    current.put("monitor_value", max_temperature);
///                    Map<String, Object> dbCurrent = assetMonitorCurrentMapper.queryByAssetIdAndMonitorName(current);
///                    if (null != dbCurrent) {
///                        assetMonitorCurrentMapper.updateAssetMonitorCurrentByAssetIdAndMonitorName(current);
///                    } else {
///                        assetMonitorCurrentMapper.insertAssetMonitorCurrent(current);
///                    }
///                    current.put("monitor_name", "井低压力");
///                    current.put("monitor_value_type", 9);
///                    current.put("monitor_value_unit", "Mpc");
///                    current.put("monitor_value", bottom_pressure);
///                    dbCurrent = assetMonitorCurrentMapper.queryByAssetIdAndMonitorName(current);
///                    if (null != dbCurrent) {
///                        assetMonitorCurrentMapper.updateAssetMonitorCurrentByAssetIdAndMonitorName(current);
///                    } else {
///                        assetMonitorCurrentMapper.insertAssetMonitorCurrent(current);
///                    }
///                    current.put("monitor_name", "下井次数");
///                    current.put("monitor_value_type", 9);
///                    current.put("monitor_value_unit", "");
///                    dbCurrent = assetMonitorCurrentMapper.queryByAssetIdAndMonitorName(current);
///                    if (isAdd) {
///                        if (null != dbCurrent) {
///                            Integer monitor_value = Integer.valueOf(dbCurrent.get("monitor_value").toString());
///                            current.put("monitor_value", String.valueOf(monitor_value + 1));
///                            assetMonitorCurrentMapper.updateAssetMonitorCurrentByAssetIdAndMonitorName(current);
///                        } else {
///                            current.put("monitor_value", "1");
///                            assetMonitorCurrentMapper.insertAssetMonitorCurrent(current);
///                        }
///                    }
                }
                for (Map<String, Object> old_product_task_well_item_asset : old_product_task_well_item_asset_list) {
                    boolean isDelete = true;
                    String asset_id = old_product_task_well_item_asset.get("asset_id").toString();
///                    current.put("asset_code", asset_id);
                    for (Map<String, Object> product_task_well_item_asset : product_task_well_item_asset_list) {
                        if (product_task_well_item_asset.get("asset_id").equals(old_product_task_well_item_asset.get("asset_id"))) {
                            isDelete = false;
                            break;
                        }
                    }
                    if (isDelete) {
                        Map<String, Object> assetMap = assetMapper.findAssetById(methodParam.getSchemaName(), asset_id);
                        productTaskMapper.deleteProductTaskWellItemAssetById(methodParam.getSchemaName(), task_well_item_code, asset_id);
                        loger.add(LangUtil.doSetLog("删除生产作业下井次数信息", LangConstant.TEXT_AJ,
                                new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CHL, assetMap.get("asset_name").toString()}));//删除生产作业下井次数信息设备
///                        current.put("monitor_name", "下井次数");
///                        current.put("monitor_value_type", 9);
///                        current.put("monitor_value_unit", "");
                        assetMap.put("isDelete", true);
                        assetList.add(assetMap);
///                        Map<String, Object> dbCurrent = assetMonitorCurrentMapper.queryByAssetIdAndMonitorName(current);
///                        Integer monitor_value = Integer.valueOf(dbCurrent.get("monitor_value").toString());
///                        int i = monitor_value - 1;
//                        if(i==0){
//                            assetMonitorCurrentMapper.deleteAssetMonitorCurrentByAssetIdAndMonitorName(current);
//                        }else {
///                        current.put("monitor_value", String.valueOf(i));
//                            assetMonitorCurrentMapper.updateAssetMonitorCurrentByAssetIdAndMonitorName(current);
//                        }
                    }
                }
            }
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10003, task_well_item_code,
                    LangUtil.doSetLogArray("更新生产作业下井次数信息", LangConstant.TEXT_AJ,
                            new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CHL, loger.toString()}));
            if (assetList.size() > 0) {
                List<Map<String, Object>> columns = selectOptionService.getSelectOptionList(methodParam, "plan_work_column_key", null);
                triggleAssetList(methodParam, pm, assetList, oldProductTaskWell, 2, columns);
            }
        }
    }

    /**
     * 获取生产作业下井详情
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业下井详情
     */
    @Override
    public Map<String, Object> findProductTaskWellInfo(MethodParam methodParam, Map<String, Object> pm) {
        String id = RegexUtil.optStrOrExpNotNull(pm.get("id"), LangConstant.TITLE_B_O);
        return productTaskMapper.findProductTaskWellInfo(methodParam.getSchemaName(), id);
    }

    /**
     * 获取生产作业下井次数详情
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业下井次数详情
     */
    @Override
    public Map<String, Object> findProductTaskWellItemInfo(MethodParam methodParam, Map<String, Object> pm) {
        String id = RegexUtil.optStrOrExpNotNull(pm.get("id"), LangConstant.TITLE_B_O);
        Map<String, Object> resultMap = productTaskMapper.findProductTaskWellItemInfo(methodParam.getSchemaName(), id);
        List<Map<String, Object>> assetList = productTaskMapper.findProductTaskWellItemAssetByCode(methodParam.getSchemaName(), id);
        resultMap.put("product_task_well_item_asset_list", assetList);
        return resultMap;
    }

    /**
     * 删除生产作业单
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void cutProductTask(MethodParam methodParam, Map<String, Object> pm) {
        String product_task_code = RegexUtil.optStrOrExpNotNull(pm.get("product_task_code"), LangConstant.TITLE_B_O);
        Map<String, Object> oldProductTask = productTaskMapper.findProductTaskInfo(methodParam.getSchemaName(), product_task_code);
        if (RegexUtil.optNotNull(oldProductTask).isPresent()) {
            //删除相关联的设备
            productTaskMapper.deleteProductTaskWellItemAssetByProductTaskCode(methodParam.getSchemaName(), product_task_code);
            //删除生产作业下井次数信息
            productTaskMapper.deleteProductTaskWellItemByProductTaskCode(methodParam.getSchemaName(), product_task_code);
            //删除生产作业下井信息
            productTaskMapper.deleteProductTaskWellByProductTaskCode(methodParam.getSchemaName(), product_task_code);
            //删除生产单信息
            productTaskMapper.deleteProductTask(methodParam.getSchemaName(), product_task_code);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10003, product_task_code,
                    LangUtil.doSetLogArray("删除了生产作业单", LangConstant.TEXT_AJ,
                            new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CIK, product_task_code}));
        }

    }

    /**
     * 删除生产作业单下井信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void cutProductTaskWell(MethodParam methodParam, Map<String, Object> pm) {
        String id = RegexUtil.optStrOrExpNotNull(pm.get("id"), LangConstant.TITLE_B_O);
        Map<String, Object> oldProductTaskWell = productTaskMapper.findProductTaskWellInfo(methodParam.getSchemaName(), id);
        Map<String, Object> productTaskWellItem = productTaskMapper.findProductTaskWellItemByTaskWellCode(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(productTaskWellItem).isPresent()) {
            throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_CHK,
                    LangConstant.TITLE_CHL, LangConstant.TITLE_CHK});//生产作业下井信息还有生产作业下井次数信息，不能删除生产作业下井信息
        }
        if (RegexUtil.optNotNull(oldProductTaskWell).isPresent()) {
            productTaskMapper.deleteProductTaskWell(methodParam.getSchemaName(), id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10003, id,
                    LangUtil.doSetLogArray("删除了生产作业单下井信息", LangConstant.TEXT_AJ,
                            new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CHK, id}));
        }

    }

    /**
     * 删除生产作业单下井次数信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void cutProductTaskWellItem(MethodParam methodParam, Map<String, Object> pm) {
        String id = RegexUtil.optStrOrExpNotNull(pm.get("id"), LangConstant.TITLE_B_O);
        Map<String, Object> oldProductTaskWellItem = productTaskMapper.findProductTaskWellItemInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldProductTaskWellItem).isPresent()) {
            productTaskMapper.deleteProductTaskWellItem(methodParam.getSchemaName(), id);
            productTaskMapper.deleteProductTaskWellItemAsset(methodParam.getSchemaName(), id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10003, id,
                    LangUtil.doSetLogArray("删除了生产作业单下井次数信息", LangConstant.TEXT_AJ,
                            new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CHL, id}));
        }
    }

    /**
     * 根据任务单号查询是否已经有任务
     *
     * @param methodParam 入参
     * @param task_code   入参
     * @return 任务单号查询是否已经有任务
     */
    @Override
    public Map<String, Object> findProductTaskWellByTaskCode(MethodParam methodParam, String task_code) {
        return productTaskMapper.findProductTaskWellByTaskCode(methodParam.getSchemaName(), task_code);
    }

    /**
     * 查询出库单下设备列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 设备列表
     */
    @Override
    public List<Map<String, Object>> findAssetListByAssetOutCode(MethodParam methodParam, Map<String, Object> pm) {
        RegexUtil.optStrOrExpNotNull(pm.get("asset_out_code"), LangConstant.TITLE_CGN);
        return productTaskMapper.findAssetListByAssetOutCode(methodParam.getSchemaName(), pm);

    }
}
