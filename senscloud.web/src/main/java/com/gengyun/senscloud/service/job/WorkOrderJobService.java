package com.gengyun.senscloud.service.job;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;

public interface WorkOrderJobService {

    /**
     * 根据行事历生成相应的工单 —— 同步执行
     * @param schemaName
     */
    void cronJobToGenerateWorkOrderByCalendar(String schemaName);
}
