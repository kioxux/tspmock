package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.AssetMonitorsValueMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.asset.AssetMonitorValueService;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AssetMonitorValueServiceImpl implements AssetMonitorValueService {

    @Resource
    AssetMonitorsValueMapper mapper;

//    @Override
//    public List<AssetMonitorHistoryData> getAssetMonitorValues(String schema_name){
//        return mapper.getAssetMonitorValues(schema_name);
//    }
//
//    @Override
//    public int insertAssetMonitorValue(String schema_name,AssetMonitorHistoryData monitor){
//        return mapper.insertAssetMonitorValue(schema_name,monitor);
//    }
//
//    @Override
//    public   List<AssetMonitorData> getAssetMonitorValuesByName(String schema_name, String assetcode, String monitorname , String begtime, String endtime){
//        return mapper.getAssetMonitorValuesByName( schema_name,  assetcode,  monitorname ,  begtime,  endtime);
//    }
//
//    @Override
//    public  List<AssetMonitorData> getAssetMonitorName(String schema_name, String assetcode){
//        return mapper.getAssetMonitorName( schema_name,  assetcode);
//
//    }


    @Override
    public List<Map<String, Object>> searchIotAssetPoints(MethodParam methodParam, Map<String, Object> paramMap) {
        String positionCode = RegexUtil.optNotBlankStrOrExp(paramMap.get("position_code"), ""); // 设备位置未选择
        String categoryId = RegexUtil.optNotBlankStrOrExp(paramMap.get("category_id"), ""); // 设备类型未选择
//        String positionCode = RegexUtil.optNotBlankStrOrExp(paramMap.get("position_code"), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_G}); // 设备位置未选择
//        String categoryId = RegexUtil.optNotBlankStrOrExp(paramMap.get("category_id"), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_C}); // 设备类型未选择
        List<Map<String, Object>> points = mapper.findIotAssetPoints(methodParam.getSchemaName(), positionCode, categoryId);
        if (points == null || points.isEmpty()) {
            return null;
        }

        List<Map<String, Object>> assetPointsList = new ArrayList<>();
        Map<String, Object> assetMap = new HashMap<>();
        Map<String, List<Map<String, Object>>> pointsMap = new HashMap<>();
        points.forEach(p -> {
            System.out.print(points);
            String assetId = String.valueOf(p.get("id"));
            if (!assetMap.containsKey(assetId)) {
                assetMap.put(assetId, p.get("asset_name"));
                List<Map<String, Object>> pointList = new ArrayList<>();
                pointList.add(p);
                pointsMap.put(assetId, pointList);
            } else {
                pointsMap.get(assetId).add(p);
            }
        });
        RegexUtil.optNotNullMap(assetMap).ifPresent(am ->
                assetMap.forEach((k, v) -> {
                    Map<String, Object> assetPoints = new HashMap<>();
                    assetPoints.put("asset_id", k);
                    assetPoints.put("asset_name", v);
                    assetPoints.put("monitor", pointsMap.get(k));
                    assetPoints.put("category", v);
                    assetPointsList.add(assetPoints);
                })
        );
        return assetPointsList;
    }

    @Override
    public Map<String, Object> searchAssetCurrentMonitorListPage(MethodParam methodParam, Map<String, Object> paramMap) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getAssetCurrentMonitorWhereString(paramMap);

        int total = mapper.findCountAssetCurrentMonitorList(schemaName, searchWord); // 获取设备的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by m.gather_time desc";
            searchWord += pagination;
            List<Map<String, Object>> planScheduleList = null;
            planScheduleList = mapper.findAssetCurrentMonitorList(schemaName, searchWord);
            result.put("rows", planScheduleList);
        }
        result.put("total", total);
        return result;
    }

    @Override
    public Map<String, Object> searchAssetHistoryMonitorListPage(MethodParam methodParam, Map<String, Object> paramMap) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getAssetHistoryMonitorWhereString(paramMap);

        int total = mapper.findCountAssetHistoryMonitorList(schemaName, searchWord); // 获取设备的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by m.gather_time desc";
            searchWord += pagination;
            List<Map<String, Object>> planScheduleList = null;
            planScheduleList = mapper.findAssetHistoryMonitorList(schemaName, searchWord);
            result.put("rows", planScheduleList);
        }
        result.put("total", total);
        return result;
    }

    @Override
    public Map<String, Object> updateAssetMonitorCurrentInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        String asset_id = RegexUtil.optNotBlankStrOrExp(paramMap.get("asset_id"), "");
        String monitor_name = RegexUtil.optNotBlankStrOrExp(paramMap.get("monitor_name"), "");
        String monitor_value = RegexUtil.optNotBlankStrOrExp(paramMap.get("monitor_value"), "");
//        if(monitor_name.equals("switchChoose")){
//            monitor_name = "Power";
//        }
        List<Map<String, Object>> result = new ArrayList<>();
        Map<String, Object> ResponseParamMap = new HashMap<String, Object>();
        String url = null;

        result = mapper.findIotAssetsPointsByAssetId(methodParam.getSchemaName(), asset_id, monitor_name);
        for (int i = 0; i < result.size(); i++) {

            String pointId = String.valueOf(result.get(i).get("point_id"));
            String is_state = String.valueOf(result.get(i).get("is_state"));
            String deploymentId = String.valueOf(result.get(i).get("poc_deployment_id"));
            String orgId = String.valueOf(result.get(i).get("org_id"));
            if (!"2".equals(orgId)) {
                continue;
            }
            String controlGroupId = result.get(i).get("poc_controller_id") == null ? "" : String.valueOf(result.get(i).get("poc_controller_id"));
            //测试用
            url = "https://sdiot.stg.starbucks.net/api/v1/organizations/" + orgId
                    + "/deployments/" + deploymentId + "/controllers/" + controlGroupId + "/points/" + pointId + "/set_state/";

            //正式用
//            url = "https://sdiot.starbucks.net/api/v1/organizations/" + orgId
//                    + "/deployments/" + deploymentId + "/controllers/" + controlGroupId + "/points/" + pointId + "/set_state/";
            if ("f".equals(is_state)) {
                ResponseParamMap.put("setpoint", monitor_value);
            } else {
                ResponseParamMap.put("state_id", monitor_value);
            }

            JSONObject jsonData = JSONObject.fromObject(ResponseParamMap);
            HttpRequestUtils.sendHtpps1(url, jsonData);
//            String jsonData = JSONObject.fromObject(ResponseParamMap).toString();
//            HttpRequestUtils.sendHttps(url, jsonData);

        }


//        String deviceId = "55bfa83b-32d3-4deb-84c9-022176743c09";
//        String orgId = "2";
//        String url = "https://sdiot.stg.starbucks.net/api/v1/organizations/" + orgId +
//                "/deployments/"+ deviceId + "/controllers/115/points/128/set_state/";
//        String point = "22.5";
//        int state_id = 220;
//
//        if(state_id != 0){
//            ResponseParamMap.put("state_id",point);
//        }else{
//            ResponseParamMap.put("setpoint",point);
//        }
//        String jsonData = JSONObject.fromObject(ResponseParamMap).toString();
        return null;
    }

    /**
     * 获取查询设备实时监控列表的条件
     *
     * @param param 请求参数
     * @return 拼接sql
     */
    private String getAssetCurrentMonitorWhereString(Map<String, Object> param) {
        StringBuffer whereString = new StringBuffer(" where m.asset_code = '" + param.get("asset_code") + "' ");
        RegexUtil.optNotBlankStrLowerOpt(param.get("keywordSearch"))
                .map(e -> "'%" + e.toLowerCase() + "%'")
                .ifPresent(e -> whereString.append(" and (lower(m.monitor_name) like ").append(e).append(")"));
        return whereString.toString();
    }

    /**
     * 获取查询设备实时监控列表的条件
     *
     * @param param 请求参数
     * @return 拼接sql
     */
    private String getAssetHistoryMonitorWhereString(Map<String, Object> param) {
        StringBuffer whereString = new StringBuffer(" where m.asset_code = '" + param.get("asset_code") + "' ");
        RegexUtil.optNotBlankStrOpt(param.get("startDateSearch")).ifPresent(e -> whereString.append(" and (to_date(m.gather_time::text, '" + SqlConstant.SQL_DATE_FMT + "') >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "'))"));
        RegexUtil.optNotBlankStrOpt(param.get("endDateSearch")).ifPresent(e -> whereString.append(" and (to_date(m.gather_time::text, '" + SqlConstant.SQL_DATE_FMT + "') <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "'))"));
        RegexUtil.optNotBlankStrLowerOpt(param.get("keywordSearch"))
                .map(e -> "'%" + e.toLowerCase() + "%'")
                .ifPresent(e -> whereString.append(" and (lower(m.monitor_name) like ").append(e).append(")"));
        return whereString.toString();
    }

}
