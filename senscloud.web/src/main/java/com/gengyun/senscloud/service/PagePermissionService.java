package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 页面权限用接口
 */
public interface PagePermissionService {
    /**
     * 用户菜单-功能权限查询
     *
     * @param methodParam 系统参数
     */
    void getUserPrmListByInfo(MethodParam methodParam);

    /**
     * 用户权限验证
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     */
    void applyCheckUserPrm(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 根据权限类型验证功能权限
     *
     * @param methodParam 系统参数
     * @param prmKey      请求地址
     */
    void applyCheckFunPrmByKey(MethodParam methodParam, String prmKey);

    /**
     * 根据权限类型验证功能权限（无异常，返回结果）
     *
     * @param methodParam 系统参数
     * @param prmKey      请求地址
     */
    boolean applyCheckFunPrmByKeyNoExp(MethodParam methodParam, String prmKey);

    /**
     * 根据权限类型获取并验证模块权限
     *
     * @param methodParam 系统参数
     * @param prmKey      请求地址
     */
    Map<String, Map<String, Boolean>> getModelPrmByKey(MethodParam methodParam, String prmKey);

    /**
     * 根据权限类型获取并验证模块权限
     *
     * @param methodParam 系统参数
     * @param prmKeys     请求地址
     */
    Map<String, Map<String, Boolean>> getModelPrmByKeyArray(MethodParam methodParam, String... prmKeys);

    /**
     * 获取有安群库存短信接收权限的人
     *
     * @param schemaName 系统参数
     */
    List<Map<String, Object>> getBomSmsPermissionUserList(String schemaName, int stockId);

}
