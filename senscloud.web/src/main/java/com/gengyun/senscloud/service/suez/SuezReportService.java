package com.gengyun.senscloud.service.suez;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

public interface SuezReportService {


    Map<String, Object> getSuezReportList(MethodParam methodParam, Map<String, Object> searchParam);

    Long newSuezReport(MethodParam methodParam, Map<String, Object> paramMap);

    void cutSuezReport(MethodParam methodParam);

    void getImportSuezReport(MethodParam methodParam, SearchParam param);

    String getImportSuezReportNew(MethodParam methodParam, SearchParam param);

    ModelAndView downLoadSuezReport(MethodParam methodParam, SearchParam param);

    boolean SuezReportExistNT(MethodParam methodParam, SearchParam param);

    Long newSuezReportNT(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> getSuezReportListNT(MethodParam methodParam, Map<String, Object> paramMap);

    void cutSuezReportNT(MethodParam methodParam);
}
