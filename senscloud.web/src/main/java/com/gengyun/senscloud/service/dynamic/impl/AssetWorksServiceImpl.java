package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.AssetWorksMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.dynamic.AssetWorksService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 设备工单管理
 */
@Service
public class AssetWorksServiceImpl implements AssetWorksService {
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    private AssetWorksMapper assetWorksMapper;

    /**
     * 获取按钮功能权限信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getAwListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[30]);
    }

    /**
     * 分页获取设备工单列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 设备工单列表
     */
    @Override
    public Map<String, Object> getAssetWorksList(MethodParam methodParam, Map<String, Object> pm) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam); // 分页参数
        String nowUserId = methodParam.getUserId();
        pm.put("user_id", nowUserId);
        pm.put("pagination", pagination);
        if (RegexUtil.optNotNull(pm.get("workTypeIdSearch")).isPresent()) {
            String[] workTypeIdSearch = RegexUtil.optNotBlankStrOpt(pm.get("workTypeIdSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("workTypeIdSearch", workTypeIdSearch);
        }
        if (RegexUtil.optNotNull(pm.get("statusSearch")).isPresent()) {
            String[] statusSearch = RegexUtil.optNotBlankStrOpt(pm.get("statusSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("statusSearch", statusSearch);
        }
        Integer total = assetWorksMapper.findAssetWorksCount(methodParam.getSchemaName(), pm);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        List<Map<String, Object>> rows = assetWorksMapper.findAssetWorksPage(methodParam.getSchemaName(), pm);
        for (Map<String, Object> result : rows) {
            List<String> menuList = methodParam.getUserPermissionList();
            result.put("isEdit", false);
            result.put("isDetail", false);
            result.put("delete", false);
            result.put("isDistribution", false);
            int status = RegexUtil.optIntegerOrVal(result.get("status"), StatusConstant.NULL_STATUS, methodParam, ErrorConstant.EC_LIST_002, RegexUtil.optStrOrBlank(result.get("status")));
            RegexUtil.optNotBlankStrOpt(result.get("duty_user_id")).ifPresent(d -> {
                if (menuList.contains("asset_io_edit") && d.equals(nowUserId) && status < StatusConstant.COMPLETED) {
                    result.put("isEdit", true); // 判断是否有处理权限
                }
            });

            // 设备盘点
            RegexUtil.optNotBlankStrOpt(result.get("create_user_id")).ifPresent(d -> {
                RegexUtil.optNotNullIntegerOpt(result.get("is_have_sub"), "0").ifPresent(s -> {
                    if (menuList.contains("asset_io_edit") && (d.equals(nowUserId) && status == StatusConstant.DRAFT || s == 1)) {
                        result.put("isEdit", true); // 判断是否有处理权限
                    }
                });
            });

            // 判断是否有作废权限
            if (menuList.contains("asset_io_delete") && status < StatusConstant.COMPLETED) {
                result.put("isDelete", true);
            } else if (status <= StatusConstant.DRAFT && RegexUtil.optEquals(result.get("create_user_id"), nowUserId)) {
                result.put("isDelete", true);
            }

            // 判断是否有查看详情权限
            if (menuList.contains("asset_io_detail")) {
                result.put("isDetail", true);
            }
            // 判断是否有分配权限
            if (menuList.contains("work_sheet_distribution")) {
                result.put("isDistribution", true);
            }
            // 如果状态为已关闭 已完成 已作废  则只能查看
            if (status == StatusConstant.COMPLETED_CLOSE
                    || status == StatusConstant.COMPLETED
                    || status == StatusConstant.CANCEL) {
                result.put("isEdit", false);
                result.put("isDelete", false);
                result.put("isDistribution", false);
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }
}
