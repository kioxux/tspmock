package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MetaDataAssetService;
import org.springframework.stereotype.Service;

@Service
public class MetaDataAssetServiceImpl implements MetaDataAssetService {
//    @Autowired
//    MetaDataAssetMapper metaDataAssetMapper;
//
////    @Override
////    @Deprecated
////    public void insertMetaDataAsset(String schema_name, String asset_name, String table_alias, String description, String fields) {
////        metaDataAssetMapper.insertMetaDataAsset(schema_name, asset_name, table_alias, description, fields);
////    }
////
////    @Override
////    @Transactional //支持事务
////    public void insertMetaDataAsset(MetaDataAsset metaDataAsset) throws Exception {
////        metaDataAssetMapper.insertMetaDataAsset2(metaDataAsset);
////        //metaDataAssetMapper.createAssetTable(metaDataAsset);
////        /*try {
////            String fields = metaDataAsset.getFields();
////            JSONArray fieldsArray = JSONArray.fromObject(fields);
////            for (int i = 0; i < fieldsArray.size(); i++) {
////                JSONObject item = fieldsArray.getJSONObject(i);
////                metadataAddField(metaDataAsset.getSchema_name(), metaDataAsset.getAsset_name(), item);
////            }
////        } catch (Exception ex) {
////            throw ex;
////        }*/
////    }
//
////    @Override
////    public void updateAssetIcon(MetaDataAsset metaDataAsset) throws Exception {
////        metaDataAssetMapper.updateAssetIcon(metaDataAsset);
////    }
////
////    @Override
////    public String getAssetIcon(MetaDataAsset metaDataAsset) throws Exception {
////        return metaDataAssetMapper.getAssetIcon(metaDataAsset);
////    }
////
////    @Override
////    public void updateMetaDataAssetBaseInfo(MetaDataAsset metaDataAsset) {
////        metaDataAssetMapper.updateMetaDataAssetBaseInfo(metaDataAsset);
////    }
////
////    @Transactional
////    @Deprecated
////    void createAssetTable(MetaDataAsset metaDataAsset) {
////        metaDataAssetMapper.createAssetTable(metaDataAsset);
////    }
////
//////    @Override
//////    public List<MetaDataAsset> findAllMetaDataAsset(String schema_name) {
//////        return metaDataAssetMapper.findAllMetaDataAsset(schema_name);
//////    }
////
////    @Override
////    public List<String> findAllMetaDataAssetName(String schema_name) {
////        return metaDataAssetMapper.findAllMetaDataAssetName(schema_name);
////    }
////
//////    @Override
//////    public MetaDataAsset findMetaDataAsset(String schema_name, int category_id) {
//////        MetaDataAsset metaDataAsset = metaDataAssetMapper.findMetaDataAsset(schema_name, category_id);
//////        metaDataAsset.setSchema_name(schema_name);
//////        return metaDataAsset;
//////    }
////
////    @Override
////    @Transactional
////    public void deleteMetaDataAsset(String schema_name, String asset_name) {
////        metaDataAssetMapper.deleteMetaDataAsset(schema_name, asset_name);
////    }
////
////    @Transactional
////    void dropMetaDataAssetTable(String schema_name, String asset_name) {
////        metaDataAssetMapper.dropMetaDataAssetTable(schema_name, asset_name);
////    }
////
////    @Transactional
////    public void metadataAddField(String schema_name, String asset_name, JSONArray fields, JSONObject newField) throws Exception {
////        metaDataAssetMapper.metadataUpdateField(schema_name, asset_name, fields.toString());
////        StringBuffer field_sql = new StringBuffer();
////        try {
////            field_sql.append(" ADD COLUMN " + newField.getString("name"));
////            String field_type = newField.optString("type", "");
////            switch (field_type) {
////                case DataType.TYPE_TEXT:
////                    field_sql.append(" TEXT ");
////                    break;
////                /*case DataType.TYPE_TEXT_AREA:
////                case DataType.TYPE_RICH_TEXT:
////                    field_sql.append(" TEXT ");
////                    break;*/
////                /*case DataType.TYPE_BIGINT:
////                    field_sql.append(" BIGINT ");
////                    break;*/
////                case DataType.TYPE_BOOLEAN:
////                    field_sql.append(" BOOLEAN ");
////                    break;
////                /*case DataType.TYPE_CHECKBOX:
////                    field_sql.append(" TEXT ");
////                    break;
////                case DataType.TYPE_DATETTIME:
////                    field_sql.append(" timestamp with time zone ");
////                    break;*/
////                case DataType.TYPE_DOUBLE:
////                    /*case DataType.TYPE_NUMERIC:*/
////                    field_sql.append(" numeric ");
////                    break;
////                /*case DataType.TYPE_GEOMETRIC:
////                    break;*/
////                /*case DataType.TYPE_IDENTITY_CARD:
////                case DataType.TYPE_LICENSE_PLATE:
////                    field_sql.append(" text ");
////                    break;
////                case DataType.TYPE_INT:
////                    field_sql.append(" int ");
////                    break;
////                case DataType.TYPE_RADIO:
////                case DataType.TYPE_DROPLIST:
////                    field_sql.append(" text ");
////                    break;
////                case DataType.TYPE_UUID:
////                    field_sql.append(" uuid ");
////                    break;*/
////                default:
////            }
////            /*if (newField.getBoolean("can_null") == false) {
////                field_sql.append(" NOT NULL");
////            }*/
////            metaDataAssetMapper.addField(schema_name, asset_name, field_sql.toString());
////        } catch (Exception ex) {
////            throw ex;
////        }
////    }
////
////    @Transactional
////    public void metadataAddField(String schema_name, String asset_name, JSONObject field) throws Exception {
////        StringBuffer field_sql = new StringBuffer();
////        try {
////            String field_name = field.optString("name", "");
////            String field_type = field.optString("type", "");
////            Boolean can_null = field.optBoolean("can_null", true);
////            field_sql.append(" ADD COLUMN " + field_name);
////            switch (field_type) {
////                case DataType.TYPE_TEXT:
////                    field_sql.append(" TEXT ");
////                    break;
////                /*case DataType.TYPE_TEXT_AREA:
////                case DataType.TYPE_RICH_TEXT:
////                    field_sql.append(" TEXT ");
////                    break;*/
////                /*case DataType.TYPE_BIGINT:
////                    field_sql.append(" BIGINT ");
////                    break;*/
////                case DataType.TYPE_BOOLEAN:
////                    field_sql.append(" BOOLEAN ");
////                    break;
////                /*case DataType.TYPE_CHECKBOX:
////                    field_sql.append(" TEXT ");
////                    break;*/
////                case DataType.TYPE_DATE:
////                    field_sql.append(" date ");
////                    break;
////                /*case DataType.TYPE_DATETTIME:
////                    field_sql.append(" timestamp with time zone ");
////                    break;*/
////                case DataType.TYPE_DOUBLE:
////                    /*case DataType.TYPE_NUMERIC:*/
////                    field_sql.append(" numeric ");
////                    break;
////                /*case DataType.TYPE_GEOMETRIC:
////                    break;*/
////                /*case DataType.TYPE_IDENTITY_CARD:
////                case DataType.TYPE_LICENSE_PLATE:
////                    field_sql.append(" text ");
////                    break;*/
////                case DataType.TYPE_INT:
////                    field_sql.append(" int ");
////                    break;
////                /*case DataType.TYPE_RADIO:
////                case DataType.TYPE_DROPLIST:
////                    field_sql.append(" text ");
////                    break;*/
////                /*case DataType.TYPE_UUID:
////                    field_sql.append(" uuid ");
////                    break;*/
////                default:
////            }
////            /*if (can_null == false) {
////                field_sql.append(" NOT NULL");
////            }*/
////            metaDataAssetMapper.addField(schema_name, asset_name, field_sql.toString());
////        } catch (Exception ex) {
////            throw ex;
////        }
////    }
////
////    @Override
////    @Transactional
////    public void metadataUpdateField(String schema_name, String asset_name, String old_field_name, JSONArray fields, JSONObject newField) throws Exception {
////        metaDataAssetMapper.metadataUpdateField(schema_name, asset_name, fields.toString());
////        try {
////            String new_field_name = newField.optString("name", "");
////            if (old_field_name.equalsIgnoreCase(new_field_name) == false) {
////                //需要改字段名
////                metaDataAssetMapper.metadataRenameField(schema_name, asset_name, old_field_name, new_field_name);
////            }
////            String type = newField.optString("type", "");
////            switch (type) {
////                case DataType.TYPE_TEXT:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "text");
////                    break;
////                /*case DataType.TYPE_TEXT_AREA:
////                case DataType.TYPE_RICH_TEXT:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "text");
////                    break;*/
////                /*case DataType.TYPE_BIGINT:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "bigint");
////                    break;*/
////                case DataType.TYPE_BOOLEAN:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "boolean");
////                    break;
////                /*case DataType.TYPE_CHECKBOX:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "text");
////                    break;*/
////                case DataType.TYPE_DATE:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "date");
////                    break;
////                /*case DataType.TYPE_DATETTIME:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "timestamp with time zone");
////                    break;*/
////                case DataType.TYPE_DOUBLE:
////                    /*case DataType.TYPE_NUMERIC:*/
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "numeric");
////                    //sql.append("ALTER TABLE " + table_name + " ALTER COLUMN " + new_field_name + " TYPE numeric;");
////                    break;
////                /*case DataType.TYPE_GEOMETRIC:
////                    break;*/
////                /*case DataType.TYPE_IDENTITY_CARD:
////                case DataType.TYPE_LICENSE_PLATE:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "text");
////                    //sql.append("ALTER TABLE " + table_name + " ALTER COLUMN " + new_field_name + " TYPE text;");
////                    break;*/
////                case DataType.TYPE_INT:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "int");
////                    //sql.append("ALTER TABLE " + table_name + " ALTER COLUMN " + new_field_name + " TYPE int;");
////                    break;
////                /*case DataType.TYPE_RADIO:
////                case DataType.TYPE_DROPLIST:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "text");
////                    //sql.append("ALTER TABLE " + table_name + " ALTER COLUMN " + new_field_name + " TYPE text;");
////                    break;*/
////                /*case DataType.TYPE_UUID:
////                    metaDataAssetMapper.metadataAlterFieldType(schema_name, asset_name, new_field_name, "uuid");
////                    //sql.append("ALTER TABLE " + table_name + " ALTER COLUMN " + new_field_name + " TYPE uuid;");
////                    break;*/
////                default:
////            }
////        } catch (Exception ex) {
////            throw ex;
////        }
////    }
//
//    /*@Override
//    @Transactional
//    public void metadataDeleteField(String schema_name, String asset_name, String field_name) throws Exception {
//        MetaDataAsset metaDataAsset = findMetaDataAsset(schema_name, asset_name);
//        String fields = metaDataAsset.getFields();
//        JSONArray fieldsArray = JSONArray.fromObject(fields);
//        for (int i = 0; i < fieldsArray.size(); i++) {
//            JSONObject item = fieldsArray.getJSONObject(i);
//            if (field_name.equalsIgnoreCase(item.optString("name", ""))) {
//                fieldsArray.remove(i);
//                break;
//            }
//        }
//        metaDataAssetMapper.metadataUpdateField(schema_name, asset_name, fieldsArray.toString());
//    }*/
//
////    @Override
////    public void metadataUpdateFields(String schema_name, String asset_name, String fields) throws Exception {
////        metaDataAssetMapper.metadataUpdateField(schema_name, asset_name, fields);
////    }
//
//

}
