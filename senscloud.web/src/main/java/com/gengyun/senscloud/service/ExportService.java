package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 导出接口
 */
public interface ExportService {
    /**
     * 设备导出
     *
     * @param methodParam 系统参数
     * @param assetList   数据信息
     * @return 文件码
     */
    String doDownloadAssetInfo(MethodParam methodParam, List<Map<String, Object>> assetList);

    /**
     * 任务模板导出
     *
     * @param methodParam 系统参数
     * @param dataList    数据信息
     * @return 文件码
     */
    String doDownloadTaskTempInfo(MethodParam methodParam, List<Map<String, Object>> dataList);

    /**
     * 任务项导出
     *
     * @param methodParam 系统参数
     * @param dataList    数据信息
     * @return 文件码
     */
    String doDownloadTaskItem(MethodParam methodParam, List<Map<String, Object>> dataList);

    /**
     * 知识库导出
     *
     * @param methodParam 系统参数
     * @param dataList   数据信息
     */
    void doDownloadKnowledgeBaseInfo(MethodParam methodParam, List<Map<String, Object>> dataList, HttpServletResponse response);

    /**
     * 导入日志文件导出
     *
     * @param methodParam 系统参数
     * @param logList     数据信息
     * @param importType  导入类型
     * @return 文件码
     */
    String doDownloadLogInfo(MethodParam methodParam, List<Map<String, Object>> logList, String importType);

    /**
     * 客户导出
     *
     * @param methodParam 系统参数
     * @param customerList   数据信息
     * @return 文件码
     */
    String doDownloadCustomerInfo(MethodParam methodParam, List<Map<String, Object>> customerList);

}
