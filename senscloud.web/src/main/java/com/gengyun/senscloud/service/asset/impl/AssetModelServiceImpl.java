package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.AssetModelMapper;
import com.gengyun.senscloud.model.AssetCategoryModel;
import com.gengyun.senscloud.model.AssetModelModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.asset.AssetCategoryService;
import com.gengyun.senscloud.service.asset.AssetModelService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 设备型号
 */
@Service
public class AssetModelServiceImpl implements AssetModelService {
    @Resource
    AssetModelMapper assetModelMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    LogsService logService;
    @Resource
    FilesService filesService;
    @Resource
    AssetCategoryService AssetCategoryService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getAssetModelListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[5]);
    }

    /**
     * 新增设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     */
    @Override
    public void newAssetModel(MethodParam methodParam, AssetModelModel assetModelModel) {
        int category_id = RegexUtil.optSelectOrExpParam(assetModelModel.getCategory_id(), LangConstant.TITLE_ASSET_C); // 设备类型未选择
        String model_name = RegexUtil.optStrOrExpNotNull(assetModelModel.getModel_name(), LangConstant.TITLE_NAME_AB_O); // 型号名称不能为空
        //查询设备型号名称是否已被使用
        if (RegexUtil.optNotNull(assetModelMapper.findAssetModelByModelName(methodParam.getSchemaName(), model_name)).isPresent()) {
            throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_NAME_AB_O, model_name});//设备型号名称已存在
        }
        Map<String, Object> pm = new HashMap<>();
        pm.put("category_id", category_id);
        pm.put("model_name", model_name);
        pm.put("remark", assetModelModel.getRemark());
        pm.put("create_user_id", methodParam.getUserId());
        pm.put("properties", assetModelModel.getProperties());
        pm.put("is_use", 1);
        pm.put("level_id", 0);
        assetModelMapper.insertAssetModel(methodParam.getSchemaName(), pm);
        //新增设备型号企业关联表
        if (RegexUtil.optNotNull(assetModelModel.getOrg_id()).isPresent()) {
            Map<String, Object> modelCustom = new HashMap<>();
            modelCustom.put("create_user_id", methodParam.getUserId());
            modelCustom.put("model_id", pm.get("id"));
            modelCustom.put("org_id", assetModelModel.getOrg_id());
            assetModelMapper.insertModelCustomer(methodParam.getSchemaName(), modelCustom);
        }
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7009, pm.get("id").toString(), LangUtil.doSetLogArray("添加设备型号", LangConstant.TEXT_ADD_A, new String[]{LangConstant.TAB_ASSET_A,}));//添加设备型号
    }

    /**
     * 批量删除设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     */
    @Override
    public void cutAssetModel(MethodParam methodParam, AssetModelModel assetModelModel) {
        String ids = RegexUtil.optStrOrExpNotNull(assetModelModel.getIds(), LangConstant.MSG_A);
        List<String> names = assetModelMapper.findNamesByIds(methodParam.getSchemaName(), ids);
        //查询这些型号是否绑定设备 如果绑定设备则不能删除
        Map<String, Object> asset = assetModelMapper.findAssetByModelIds(methodParam.getSchemaName(), ids);
        if (RegexUtil.optNotNull(asset).isPresent()) {
            throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TAB_ASSET_A, LangConstant.TITLE_ASSET_I, LangConstant.TAB_ASSET_A});//设备型号下还有备件类型，不能删除设备型号
        }
        if (RegexUtil.optIsPresentList(names)) {
            //删除设备型号
            assetModelMapper.deleteAssetModel(methodParam.getSchemaName(), ids);
            //删除设备类型和企业关联表
            assetModelMapper.deleteModelCustomer(methodParam.getSchemaName(), ids);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7009, ids, LangUtil.doSetLogArray("删除了设备型号", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_ASSET_A, names.toString()}));//删除了设备型号
        }
    }

    /**
     * 编辑设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     */
    @Override
    public void modifyAssetModel(MethodParam methodParam, AssetModelModel assetModelModel) {
        Integer id = RegexUtil.optSelectOrExpParam(assetModelModel.getId(), LangConstant.TITLE_ASSET_D); // 设备型号未选择
        String sectionType = RegexUtil.optStrOrError(assetModelModel.getSectionType(), methodParam, ErrorConstant.EC_ASSET_2, id.toString(), LangConstant.MSG_BI);
        Map<String, Object> oldPm = assetModelMapper.findById(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldPm).isPresent()) {
            Map<String, Object> pm = new HashMap<>();
            pm.put("id", id);
            //更新设备型号图片
            if ("modelIcon".equals(sectionType)) {
                pm.put("icon", assetModelModel.getIcon());
            }
            //更新设备型号头部名称和类型
            if ("modelTop".equals(sectionType)) {
                pm.put("model_name", assetModelModel.getModel_name());
                //判断型号名称是否改变
                if (!assetModelModel.getModel_name().equals(oldPm.get("model_name"))
                        && RegexUtil.optNotNull(assetModelMapper.findAssetModelByModelName(methodParam.getSchemaName(), assetModelModel.getModel_name())).isPresent()) {
                    throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_NAME_AB_O, assetModelModel.getModel_name()});//设备型号名称已存在
                }
                pm.put("category_id", assetModelModel.getCategory_id());
                //如果设备类型不为空且和以前的设备类型不一样  更新成新的设备类型 先要确保该设备型号没有被设备关联
                if (RegexUtil.optNotNull(assetModelModel.getCategory_id()).isPresent() && !assetModelModel.getCategory_id().toString().equals(oldPm.get("category_id").toString())) {
                    Map<String, Object> asset = assetModelMapper.findAssetByModelId(methodParam.getSchemaName(), id);
                    if (RegexUtil.optNotNull(asset).isPresent()) {
                        throw new SenscloudException(LangConstant.MSG_CI, new String[]{LangConstant.TAB_ASSET_A, LangConstant.TITLE_ASSET_I, LangConstant.TAB_ASSET_A});//设备型号下还有设备，不能编辑该设备型号
                    }
                    AssetCategoryModel assetCategoryModel = new AssetCategoryModel();
                    assetCategoryModel.setAsset_category_id(assetModelModel.getCategory_id());
                    JSONArray assetCategoryCustomFields = getAssetCategoryCustomFields(methodParam, assetCategoryModel);
                    pm.put("properties", assetCategoryCustomFields.toString());
                }
            }
            //更新设备型号基础信息
            if ("modelBase".equals(sectionType)) {
                pm.put("org_id", assetModelModel.getOrg_id());
                pm.put("remark", assetModelModel.getRemark());
                //更新设备型号企业关联信息
                if (RegexUtil.optNotNull(assetModelModel.getOrg_id()).isPresent()) {
                    Map<String, Object> assetModel = assetModelMapper.findModelCustomer(methodParam.getSchemaName(), id);
                    if (RegexUtil.optNotNull(assetModel).isPresent()) {
                        assetModelMapper.updateModelCustomer(methodParam.getSchemaName(), id, assetModelModel.getOrg_id());
                    } else {
                        Map<String, Object> modelCustom = new HashMap<>();
                        modelCustom.put("create_user_id", methodParam.getUserId());
                        modelCustom.put("model_id", id);
                        modelCustom.put("org_id", assetModelModel.getOrg_id());
                        assetModelMapper.insertModelCustomer(methodParam.getSchemaName(), modelCustom);
                    }
                } else {
                    //删除制造商关联
                    assetModelMapper.deleteModelCustomerById(methodParam.getSchemaName(), id);
                }
            }
            //更新设备型号自定义字段
            if ("modelInfo".equals(sectionType)) {
                pm.put("properties", assetModelModel.getProperties());
            }
            assetModelMapper.updateAssetModel(methodParam.getSchemaName(), pm);

            JSONArray loger = LangUtil.compareMap(pm, oldPm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7009, id.toString(), LangUtil.doSetLogArray("编辑了设备型号", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TAB_ASSET_A, loger.toString()}));
        }
    }

    /**
     * 分页查询设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     * @return 设备型号分页列表
     */
    @Override
    public Map<String, Object> getAssetModelListPage(MethodParam methodParam, AssetModelModel assetModelModel) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        Map<String, Object> params = new HashMap<>();
        params.put("keywordSearch", assetModelModel.getKeywordSearch());
        params.put("categorySearch", assetModelModel.getCategorySearch());
        params.put("orgSearch", assetModelModel.getOrgSearch());
        //查询该条件下的总记录数
        int total = assetModelMapper.findAssetModelListTotal(methodParam.getSchemaName(), params);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        params.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = assetModelMapper.findAssetModelListPage(methodParam.getSchemaName(), params);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 查询设备型号详情
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     * @return 查询设备型号详情
     */
    @Override
    public Map<String, Object> getAssetModelInfo(MethodParam methodParam, AssetModelModel assetModelModel) {
        Integer id = RegexUtil.optSelectOrExpParam(assetModelModel.getId(), LangConstant.TITLE_ASSET_D); // 设备型号未选择
        return getAssetModelInfoById(methodParam, id);
    }

    /**
     * 查询设备型号详情
     *
     * @param methodParam 入参
     * @param id          数据主键
     * @return 查询设备型号详情
     */
    @Override
    public Map<String, Object> getAssetModelInfoById(MethodParam methodParam, Integer id) {
        return RegexUtil.optNotNullMap(assetModelMapper.findById(methodParam.getSchemaName(), id)).map(m -> {
            m.put("properties", RegexUtil.optNotBlankStrOpt(m.get("properties")).filter(RegexUtil::optIsPresentStr).map(JSONArray::fromObject).orElseGet(JSONArray::new));
            return m;
        }).orElseGet(HashMap::new);
    }

    /**
     * 分页查询附件文档新消息
     */
    @Override
    public Map<String, Object> getAssetModelDocPage(MethodParam methodParam, AssetModelModel assetModelModel) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        Map<String, Object> params = new HashMap<>();
        params.put("model_id", assetModelModel.getId());
        params.put("keywordSearch", assetModelModel.getKeywordSearch());
        //查询该条件下的总记录数
        int total = assetModelMapper.findAssetModelDocsListTotal(methodParam.getSchemaName(), params);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        params.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = assetModelMapper.findAssetModelDocsList(methodParam.getSchemaName(), params);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 删除设备类型附件文档
     */
    @Override
    public void cutAssetModelDocs(MethodParam methodParam, AssetModelModel assetModelModel) {
        Integer id = RegexUtil.optSelectOrExpParam(assetModelModel.getId(), LangConstant.TITLE_ASSET_D); // 设备型号未选择
        Integer fileId = RegexUtil.optSelectOrExpParam(assetModelModel.getFile_id(), LangConstant.TITLE_AAAA_Q); // 文档未选择
        Map<String, Object> fieldMap = filesService.findById(methodParam.getSchemaName(), fileId);
        if (RegexUtil.optNotNull(fieldMap).isPresent()) {
            assetModelMapper.deleteAssetModelDocs(methodParam.getSchemaName(), id, fileId);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7009, id.toString(), LangUtil.doSetLogArray("删除了附件文档", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_DOC_A, fieldMap.get("file_original_name").toString()}));
        }
    }

    /**
     * 新增设备类型附件文档
     */
    @Override
    public void newAssetModelDocs(MethodParam methodParam, AssetModelModel assetModelModel) {
        Integer id = RegexUtil.optSelectOrExpParam(assetModelModel.getId(), LangConstant.TITLE_ASSET_D); // 设备型号未选择
        Integer fileId = RegexUtil.optSelectOrExpParam(assetModelModel.getFile_id(), LangConstant.TITLE_AAAA_Q); // 文档未选择
        Map<String, Object> pm = new HashMap<>();
        pm.put("model_id", id);
        pm.put("file_id", fileId);
        pm.put("remark", assetModelModel.getRemark());
        pm.put("create_user_id", methodParam.getUserId());
        Map<String, Object> fieldMap = filesService.findById(methodParam.getSchemaName(), fileId);
        if (RegexUtil.optNotNull(fieldMap).isPresent()) {
            assetModelMapper.insertAssetModelDocs(methodParam.getSchemaName(), pm);
            if (RegexUtil.optNotNull(assetModelModel.getFile_type()).isPresent()) {
                filesService.updateFileType(methodParam.getSchemaName(), Integer.valueOf(fileId), assetModelModel.getFile_type());
            }
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7009, id.toString(), LangUtil.doSetLogArray("添加了附件文档", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TAB_DOC_A, fieldMap.get("file_original_name").toString()}));
        }
    }

    /**
     * 分页查询故障履历
     *
     * @param methodParam     系统入参
     * @param assetModelModel 入参
     * @return 故障履历
     */
    @Override
    public Map<String, Object> getFaultRecordListPage(MethodParam methodParam, AssetModelModel assetModelModel) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        Map<String, Object> params = new HashMap<>();
        params.put("id", assetModelModel.getId());
        params.put("keywordSearch", assetModelModel.getKeywordSearch());
        //查询该条件下的总记录数
        int total = assetModelMapper.findFaultRecordListTotal(methodParam.getSchemaName(), params);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        params.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = assetModelMapper.findFaultRecordList(methodParam.getSchemaName(), params);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 根据型号id获取自定义字段
     *
     * @param methodParam 系统入参
     * @param modelId     入参
     * @return 定义字段
     */
    @Override
    public String getPropertiesById(MethodParam methodParam, Integer modelId) {
        return assetModelMapper.findPropertiesById(methodParam.getSchemaName(), modelId);
    }

    /**
     * 验证设备型号是否存在
     *
     * @param schemaName
     * @param assetModelList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryAssetModelByCode(String schemaName, List<String> assetModelList) {
        return assetModelMapper.queryAssetModelByCode(schemaName, assetModelList);
    }

    @Override
    public JSONArray getAssetCategoryCustomFields(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        return RegexUtil.optNotNullMap(AssetCategoryService.getAssetCategoryInfo(methodParam, assetCategoryModel)).map(m -> m.get("fields")).filter(RegexUtil::optIsPresentStr).map(JSONArray::fromObject).orElseGet(JSONArray::new);
    }
}
