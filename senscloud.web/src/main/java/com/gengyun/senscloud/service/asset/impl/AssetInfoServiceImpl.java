package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.mapper.AssetInfoMapper;
import com.gengyun.senscloud.model.AssetBomData;
import com.gengyun.senscloud.service.asset.AssetInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class AssetInfoServiceImpl implements AssetInfoService {
    @Resource
    AssetInfoMapper mapper;

    /**
     * 验证设备编码是否存在
     *
     * @param schemaName
     * @param assetCodeList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryAssetCodeByCode(String schemaName, List<String> assetCodeList) {
        return mapper.queryAssetCodeByCode(schemaName, assetCodeList);
    }

    /**
     * 验证设备名称是否存在
     *
     * @param schemaName
     * @param assetNameList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryAssetNameByName(String schemaName, List<String> assetNameList) {
        return mapper.queryAssetNameByName(schemaName, assetNameList);
    }
}

