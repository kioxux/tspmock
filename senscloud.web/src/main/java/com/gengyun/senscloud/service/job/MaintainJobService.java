package com.gengyun.senscloud.service.job;

public interface MaintainJobService {

    /**
     * 根据新增、修改的维保计划生成相应的行事历 —— 同步执行
     * @param schemaName
     */
    void cronJobToGeneratePlanWorkWithNewModel(String schemaName);

    /**
     * 行事历过期数据处理
     * @param schemaName
     */
    void expirePlanWorkCalendar(String schemaName);
}
