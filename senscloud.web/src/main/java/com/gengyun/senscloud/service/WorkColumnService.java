package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.BomSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkColumnSearchParam;

import java.util.List;
import java.util.Map;

public interface WorkColumnService {


    /**
     * 获取字段库列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getWorkColumnListForPage(MethodParam methodParam, WorkColumnSearchParam param);

    /**
     * 获取字段主键
     *
     * @return
     */
    String getWorkColumnKey();

    /**
     * 新增字段
     *
     * @param methodParam
     * @param paramMap
     */
    void newWorkColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑字段
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除选中字段
     *
     * @param methodParam
     * @param field_form_codes
     */
    void cutWorkColumnByFieldFormCode(MethodParam methodParam, String field_form_codes);

    /**
     * 获取字段明细信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkColumnInfoByFieldFormCode(MethodParam methodParam, WorkColumnSearchParam bParam);

    /**
     * 克隆字段
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkColumnCloneByFieldFormCode(MethodParam methodParam, WorkColumnSearchParam bParam);

//    List<Map<String, Object>> getSpecialPropertyList(MethodParam methodParam, String column_type);

    /**
     * 查询新增字段查询字段库字段列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String, Object>> getWorkColumnListForTemplate(MethodParam methodParam, WorkColumnSearchParam param);
}
