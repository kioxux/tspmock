package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.OtherCfgModel;

import java.util.List;
import java.util.Map;

/**
 * 日志信息
 */
public interface LogsService {
    /**
     * 新增日志信息
     *
     * @param methodParam 系统参数
     * @param log_type    模块类别
     * @param business_no 业务数据编号
     * @param remark      日志信息
     */
    void newLog(MethodParam methodParam, String log_type, String business_no, String remark);

    /**
     * 查询日志
     *
     * @param methodParam 系统参数
     * @param log_type    模块类别
     * @param business_no 业务数据编号
     * @return 日志列表
     */
    List<Map<String, Object>> getLog(MethodParam methodParam, String log_type, String business_no);

    /**
     * 查询整个模块的日志信息
     *
     * @param methodParam 系统参数
     * @param log_type    模块类别
     * @param paramMap    参数
     * @return 日志列表
     */
    List<Map<String, Object>> getLogByLogType(MethodParam methodParam, String log_type, Map<String, Object> paramMap);

    /**
     * 新增错误日志信息
     *
     * @param schemaName 数据库
     * @param error_code 错误编号
     * @param userId     当前登录用户ID
     * @param remark     备注
     */
    void newErrorLog(String schemaName, String error_code, String userId, String remark);

    /**
     * 查询错误日志列表
     *
     * @param methodParam 系统参数
     * @param ocModel     请求参数
     * @return 错误日志列表
     */
    Map<String, Object> getErrorLogList(MethodParam methodParam, OtherCfgModel ocModel);
}
