package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SystemConfigData;
import com.gengyun.senscloud.model.SystemConfigModel;

import java.util.List;
import java.util.Map;

/**
 * 系统配置信息处理
 */
public interface SystemConfigService {

    /**
     * 获取系统默认语言
     *
     * @param methodParam 系统参数
     * @return 多语言转换值
     */
    String getSysLang(MethodParam methodParam);

    /**
     * 根据系统配置名获取配置值
     *
     * @param schema_name 数据库
     * @param config_name 系统配置名
     * @return 配置值
     */
    String getSysCfgValueByCfgName(String schema_name, String config_name);

    /**
     * 查询系统配置基础配置列表
     *
     * @param methodParam       入参
     * @param systemConfigModel 入参
     * @return 系统配置基础配置列表
     */
    List<Map<String, Object>> getSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel);

    /**
     * 查询地图类型系统配置基础配置
     *
     * @param methodParam       入参
     * @param systemConfigModel 入参
     * @return 地图类型系统配置基础配
     */
    Map<String, Object> getMapTypeSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel);

    /**
     * 查询顶层地图类型系统配置基础配置
     *
     * @param methodParam       入参
     * @param systemConfigModel 入参
     * @return 地图类型系统配置基础配
     */
    Map<String, Object> getTopMapTypeSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel);

    /**
     * 更新系统配置
     *
     * @param methodParam 入参
     * @param params      入参
     * @return 系统配置基础配置列表
     */
    void modifySysConfigList(MethodParam methodParam, Map<String, Object> params);


    /**
     * 查询系统配置
     *
     * @param schema_name 入参
     * @param config_name
     * @return
     */
    SystemConfigData getSystemConfigData(String schema_name, String config_name);

    /**
     * 查询系统配置
     *
     * @param schema_name 入参
     * @param config_name
     * @return
     */
    Map<String, Object> getMapSystemConfigData(String schema_name, String config_name);

    /**
     * 查询系统配置组名称列表
     *
     * @param schema_name 数据库
     * @return 组名称列表
     */
    String[] getGroupTitleList(String schema_name);

}
