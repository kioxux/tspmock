package com.gengyun.senscloud.service.asset;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

public interface AssetMonitorValueService {
//    List<AssetMonitorHistoryData> getAssetMonitorValues(String schema_name);
//
//    int insertAssetMonitorValue(String schema_name,AssetMonitorHistoryData monitor);
//
//    List<AssetMonitorData> getAssetMonitorValuesByName(String schema_name, String assetcode, String monitorname , String begtime, String endtime);
//
//    List<AssetMonitorData> getAssetMonitorName(String schema_name,String assetcode);

    List<Map<String, Object>> searchIotAssetPoints(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> searchAssetCurrentMonitorListPage(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> searchAssetHistoryMonitorListPage(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> updateAssetMonitorCurrentInfo(MethodParam methodParam, Map<String, Object> paramMap);

}
