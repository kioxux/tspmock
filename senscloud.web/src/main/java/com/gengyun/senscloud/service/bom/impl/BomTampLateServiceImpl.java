package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomTampLateService;
import org.springframework.stereotype.Service;

@Service
public class BomTampLateServiceImpl implements BomTampLateService {
//    @Autowired
//    BomTampLateMapper bomTampLateMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Override
//    public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName, String sortOrder) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        if (StringUtils.isNotEmpty(sortName)) {
//            if (StringUtils.isEmpty(sortOrder)) {
//                sortOrder = "asc";
//            }
//            orderBy = sortName + " " + sortOrder;
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = bomTampLateMapper.queryBomTampLateList(schemaName, orderBy, pageSize, begin, searchKey);
//        int total = bomTampLateMapper.countByCondition(schemaName, searchKey);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel addBomTampLate(String schemaName, Map<String, Object> paramMap) {
//        paramMap.put("schema_name", schemaName);
//        String bomModel = (String) paramMap.get("bom_model");
//        if (RegexUtil.isNull(bomModel)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_MODEL_NULL));//备件型号必填！
//        }
//        Integer id = bomTampLateMapper.selectByBomModel(schemaName, bomModel);
//        if (RegexUtil.isNotNull(id)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_MODEL_EXIST));//备件型号已存在！
//        }
//        int count = bomTampLateMapper.insert(paramMap);
//        if (!RegexUtil.isNull((String) paramMap.get("files_id"))) {
//            String file_ids = ((String) paramMap.get("files_id")).trim();
//            bomTampLateMapper.update_doc(schemaName, "(" + file_ids + ")", Integer.parseInt(paramMap.get("id").toString()));
//        }
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//
//    }
//
//    @Override
//    public Map<String, Object> findById(String schemaName, String bom_model) {
//        return bomTampLateMapper.selectByBomID(schemaName,bom_model);
//    }
//
//    @Override
//    public ResponseModel editBomTampLate(String schemaName, Map<String, Object> paramMap) {
//        String oldId = (String) paramMap.get("id");
//        if (RegexUtil.isNull(oldId)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_WRONG));//数据异常，操作没有执行！
//        }
//        int count = bomTampLateMapper.update(schemaName, paramMap);
//        if (count > 0) {
//            bomTampLateMapper.updateBomNameByModel(schemaName, oldId);
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel deleteBomTampLate(String schemaName, Integer id) {
//        int count = bomTampLateMapper.delete(schemaName, id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public JSONObject query_doc(String schemaName, String model_id) {
//        JSONObject pageResult = new JSONObject();
//        List<Map<String, Object>> result = bomTampLateMapper.query_doc(schemaName, model_id);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel insert_modal_doc(String schemaName, Map<String, Object> paramMap) {
//        int count = 0;
//        String[] file_ids = null;
//        String file_id = paramMap.get("file_id").toString().trim();
//        if (!RegexUtil.isNull(file_id) && !"".equals(file_id)) {
//            file_ids = file_id.split(",");
//            for (String id : file_ids) {
//                paramMap.put("file_id", Integer.parseInt(id));
//                count += bomTampLateMapper.insert_model_doc(schemaName, paramMap);
//            }
//        }
//        if (null != file_ids && count == file_ids.length) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel delete_model_doc(String schemaName, Integer file_id) {
//        int count = bomTampLateMapper.delete_model_doc(schemaName, file_id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
}
