package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.WorkTaskService;
import org.springframework.stereotype.Service;

@Service
public class WorkTaskServiceImpl implements WorkTaskService {
//    @Autowired
//    WorkTaskMapper workTaskMapper;
//
//    @Autowired
//    LogsMapper loggerMapper;
//
//    @Autowired
//    UserMapper userMapper;
//
//    @Autowired
//    FacilitiesMapper facilityMapper;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    ToolMapper toolMapper;
//
//    @Override
//    public int getWorkTaskListCount(String schema_name, String condition) {
//        return workTaskMapper.getWorkTaskListCount(schema_name, condition);
//    }
//
//    @Override
//    public List<WorkTask> getWorkTaskList(String schema_name, String condition, int pageSize, int begin) {
//        return workTaskMapper.getWorkTaskList(schema_name, condition, pageSize, begin);
//    }
//
//    public JSONObject queryWorkTaskList() {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        Map<String, Object> dbInfo = commonUtilService.doGetDbBaseInfo();
//        String condition = "";
//        String keyWord = request.getParameter("keyWord");
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (upper(t.task_item_code) like upper('%" + keyWord + "%') or upper(t.task_item_name) like upper('%" + keyWord + "%') )";
//        }
//        String check_method = request.getParameter("check_method");
//        if (StringUtils.isNotBlank(check_method)) {
//            condition += " and t.check_method like '%" + check_method + "%' ";
//        }
//        String schemaName = (String) dbInfo.get("schemaName");
//        int pageNumber = Integer.valueOf(RegexUtil.optStrOrVal(request.getParameter("pageNumber"), "0"));
//        int pageSize = Integer.valueOf(RegexUtil.optStrOrVal(request.getParameter("pageSize"), "15"));
//        int begin = pageSize * pageNumber;
//        List<WorkTask> workTaskList = this.getWorkTaskList(schemaName, condition, pageSize, begin);
//        int total = this.getWorkTaskListCount(schemaName, condition);
//        JSONObject result = new JSONObject();
//        result.put("rows", workTaskList);
//        result.put("total", total);
//        return result;
//    }
//
//    /**
//     * 新增、编辑任务项
//     *
//     * @param schema_name
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel addAndUpdateWorkTask(String schema_name, HttpServletRequest request) {
//        User user = authService.getLoginUser(request);
//        String task_item_code = request.getParameter("task_item_code");//任务项编号
//        String standard = request.getParameter("standard");//标准
//        String task_item_name = request.getParameter("task_item_name");//任务名称
//        String check_method = request.getParameter("check_method");//检查方法
//        String result_type = request.getParameter("result_type");//结果类型
//        String file_ids = request.getParameter("file_ids");//指导图片
//        String requirements = request.getParameter("requirements");//任务要求
//        String work_hours = request.getParameter("work_hours");//预计工时
//        String order = request.getParameter("order");//排序序号
//        String isuse = request.getParameter("isuse");//排序序号
//        if (work_hours == null || "".equals(work_hours)) {
//            work_hours = "0.0";
//        }
//        WorkTask workTask = new WorkTask();
//        workTask.setCheck_method(check_method);
//        workTask.setCheck_way(0);
//        workTask.setStandard(standard);
//        workTask.setFile_ids(file_ids);
//        workTask.setOrder(Integer.parseInt(order));
//        workTask.setResult_type(Integer.parseInt(result_type));
//        workTask.setTask_item_name(task_item_name);
//        workTask.setRequirements(requirements);
//        workTask.setWork_hours(Float.parseFloat(work_hours));
//        workTask.setTask_item_code(task_item_code);
//        workTask.setIsuse(isuse.equals("1"));
//        int result = 0;
//        if (StringUtils.isBlank(task_item_code)) {
//            workTask.setTask_item_code(serialNumberService.generateMaxBusinessCode(schema_name, "work_task"));
//            workTask.setCreate_time(UtilFuns.sysTimestamp());
//            workTask.setCreate_user_account(user.getAccount());
//            result = workTaskMapper.addWorkTask(schema_name, workTask);
//        } else {
//            result = workTaskMapper.updateworktask(schema_name, workTask);
//        }
//        if (result > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SAVED_A));
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL));
//        }
//    }
//
//    @Override
//    public WorkTask findByID(String schema_name, String task_item_code) {
//        return workTaskMapper.findByID(schema_name, task_item_code);
//    }
//
//    @Override
//    public int updateworktask(String schema_name, WorkTask workTask) {
//        return workTaskMapper.updateworktask(schema_name, workTask);
//    }
//
//    @Override
//    public List<WorkTask> getAllWorkTaskList(String schema_name, String condition) {
//        return workTaskMapper.getAllWorkTaskList(schema_name, condition);
//
//
//    }
//
//    @Override
//    public List<WorkTask> getAllTemplateTaskList(String schema_name, String condition) {
//        return workTaskMapper.getAllTemplateTaskList(schema_name, condition);
//    }
//
//    /**
//     * 查询任务项bom列表
//     *
//     * @param schema_name
//     * @param taskItemCode
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryTaskItemBomList(String schema_name, String taskItemCode) {
//        return workTaskMapper.getTaskItemBomList(schema_name, taskItemCode);
//    }
//
//    /**
//     * 新增任务项备件
//     *
//     * @param schema_name
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel addTaskItemBom(String schema_name, HttpServletRequest request) {
//        String bomModel = request.getParameter("bom_mode");
//        String bomCode = request.getParameter("bom_code");
//        String materialCode = request.getParameter("material_code");
//        String taskItemCode = request.getParameter("taskItemCode");
//        if (RegexUtil.isNull(bomCode) || RegexUtil.isNull(materialCode))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_NO_CH));
//
//        if (StringUtils.isBlank(taskItemCode))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//        User user = authService.getLoginUser(request);
//        int result = workTaskMapper.addTaskItemBom(schema_name, taskItemCode, bomModel, bomCode, materialCode, UtilFuns.sysTimestamp(), user.getAccount());
//        if (result > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_AD_BOM_SU));//新增备件成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_AD_BOM_DE));//新增备件失败
//        }
//    }
//
//    /**
//     * 通过code查询任务项详情
//     *
//     * @param schema_name
//     * @param taskItemCode
//     * @return
//     */
//    @Override
//    public WorkTask getTaskItemDetailByCode(String schema_name, String taskItemCode) {
//        return workTaskMapper.getTaskItemDetailByCode(schema_name, taskItemCode);
//    }
//
//    /**
//     * 删除任务项bom
//     *
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Override
//    public int deleteTaskItemBom(String schema_name, long id) {
//        return workTaskMapper.deleteTaskItemBom(schema_name, id);
//    }
//
//    /**
//     * 查询任务项工具列表
//     *
//     * @param schema_name
//     * @param taskItemCode
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryTaskItemToolList(String schema_name, String taskItemCode) {
//        return workTaskMapper.getTaskItemToolList(schema_name, taskItemCode);
//    }
//
//    /**
//     * 新增工具
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel addTool(String schemaName, HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String toolName = request.getParameter("toolName");
//        if (StringUtils.isBlank(toolName))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        ToolData toolData = new ToolData();
//        toolData.setTool_name(toolName);
//        toolData.setCreate_user_account(user.getAccount());
//        toolData.setCreatetime(UtilFuns.sysTimestamp());
//        toolData.setSchema_name(schemaName);
//        //保存部位
//        int result = toolMapper.addTool(toolData);
//        if (result > 0)
//            return ResponseModel.ok(toolData.getId());//操作成功
//        else
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败
//    }
//
//    /**
//     * 新增任务项工具
//     *
//     * @param schema_name
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel addTaskItemTool(String schema_name, HttpServletRequest request) {
//        String toolId = request.getParameter("toolId");
//        String taskItemCode = request.getParameter("taskItemCode");
//        if (StringUtils.isBlank(toolId))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.PLEASE_CHOOSE) + selectOptionService.getLanguageInfo(LangConstant.TOOLS));//请选择工具
//
//        if (StringUtils.isBlank(taskItemCode))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//        User user = authService.getLoginUser(request);
//        int result = workTaskMapper.addTaskItemTool(schema_name, taskItemCode, Long.parseLong(toolId), UtilFuns.sysTimestamp(), user.getAccount());
//        if (result > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_AD_BOM_SU));//新增备件成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_AD_BOM_DE));//新增备件失败
//        }
//    }
//
//    /**
//     * 删除任务项工具
//     *
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Override
//    public int deleteTaskItemTool(String schema_name, long id) {
//        return workTaskMapper.deleteTaskItemTool(schema_name, id);
//    }
}
