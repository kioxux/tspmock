package com.gengyun.senscloud.service.dynamic;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 生产作业单
 */
public interface ProductTaskService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getProductTaskPermission(MethodParam methodParam);

    /**
     * 新增生产作业单
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void newProductTask(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 新增生产作业下井
     *
     * @param methodParam 入参
     * @param pms         入参
     */
    void newProductTaskWell(MethodParam methodParam, List<Map<String, Object>> pms);

    /**
     * 新增生产作业下井次数
     *
     * @param methodParam 入参
     * @param pms         入参
     */
    void newProductTaskWellItem(MethodParam methodParam, List<Map<String, Object>> pms);

    /**
     * 查询生产作业单列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业单列表
     */
    Map<String, Object> findProductTaskPage(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 查询生产作业单详情
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业单详情
     */
    Map<String, Object> findProductTaskInfo(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 查询生产作业单明细列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业单明细列表
     */
    List<Map<String, Object>> findProductTaskWellList(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 更新生产作业信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void modifyProductTask(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 更新生产作业下井信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void modifyProductTaskWell(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 更新生产作业下井次数信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void modifyProductTaskWellItem(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 获取生产作业下井详情
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业下井详情
     */
    Map<String, Object> findProductTaskWellInfo(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 获取生产作业下井次数详情
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 生产作业下井次数详情
     */
    Map<String, Object> findProductTaskWellItemInfo(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除生产作业单
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void cutProductTask(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除生产作业单下井信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void cutProductTaskWell(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除生产作业单下井次数信息
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void cutProductTaskWellItem(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 根据任务单号查询是否已经有任务
     * @param methodParam 入参
     * @param task_code 入参
     * @return 任务单号查询是否已经有任务
     */
    Map<String, Object> findProductTaskWellByTaskCode(MethodParam methodParam, String task_code);

    /**
     * 查询出库单下设备列表列表
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    List<Map<String, Object>> findAssetListByAssetOutCode(MethodParam methodParam, Map<String, Object> paramMap);
}
