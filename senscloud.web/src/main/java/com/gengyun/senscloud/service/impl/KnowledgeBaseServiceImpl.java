package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.KnowledgeBaseMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.system.KnowledgeBaseService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "知识库")
@Service
public class KnowledgeBaseServiceImpl implements KnowledgeBaseService {
    private Logger logger = LoggerFactory.getLogger(KnowledgeBaseServiceImpl.class);
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    KnowledgeBaseMapper knowledgeBaseMapper;
    @Resource
    ExportService exportService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getKnowledgeBaseListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[32]);
    }

    /**
     * 获取查询知识库信息的条件
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 拼接sql
     */
    private String getKnowledgeBaseWhereString(MethodParam methodParam, Map<String, Object> paramMap) {
        String schemaName = methodParam.getSchemaName();
        String business_no = RegexUtil.optNotNullMap(paramMap).map(p -> RegexUtil.optStrOrNull(p.get("business_no"))).orElse(null);
        StringBuffer whereString = new StringBuffer(" where f.is_use = '1' ");
        RegexUtil.optNotBlankStrOpt(paramMap.get("file_category_id")).ifPresent(e -> whereString.append(" and f.file_category_id::varchar in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(paramMap.get("ids")).ifPresent(e -> whereString.append(" and f.id::varchar in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        if (SensConstant.BUSINESS_NO_2000.equals(business_no)) {
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(t1.asset_name) like ")
                    .append(e).append(" or lower(t1.asset_code) like ").append(e).append(" or lower(f.file_original_name) like ").append(e).append(")"));
        } else if (SensConstant.BUSINESS_NO_3000.equals(business_no)) {
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(bm.bom_name) like ")
                    .append(e).append(" or lower(bm.material_code) like ").append(e).append(" or lower(f.file_original_name) like ").append(e).append(")"));
        } else if (SensConstant.BUSINESS_NO_6001.equals(business_no)) {
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(fc.short_title) like ")
                    .append(e).append(" or lower(fc.inner_code) like ").append(e).append(" or lower(f.file_original_name) like ").append(e).append(")"));
        } else if (SensConstant.BUSINESS_NO_7009.equals(business_no)) {
            RegexUtil.optNotBlankStrOpt(paramMap.get("category_id")).ifPresent(e -> whereString.append(" and amd.category_id::varchar in (").append(DataChangeUtil.joinByStr(e)).append(") "));
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(amd.model_name) like ")
                    .append(e).append(" or lower(f.file_original_name) like ").append(e).append(")"));
        } else if (SensConstant.BUSINESS_NO_7004.equals(business_no)) {
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(tsk.task_item_name) like ")
                    .append(e).append(" or lower(tsk.task_item_code) like ").append(e).append(" or lower(f.file_original_name) like ").append(e).append(")"));
        } else if (SensConstant.BUSINESS_NO_10001.equals(business_no)) {
            RegexUtil.optNotBlankStrOpt(paramMap.get("work_type_id")).ifPresent(e -> whereString.append(" and f.work_type_id::varchar in (").append(DataChangeUtil.joinByStr(e)).append(") "));
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(f.work_code) like ")
                    .append(e).append(" or lower(f.file_original_name) like ").append(e).append(")"));
        } else if (SensConstant.BUSINESS_NO_9000.equals(business_no)) {
            whereString.append(" and f.id not in ( ").append(SqlConstant.OTHER_KNOWLEDGE_BASE.replaceAll("schema_name", schemaName)).append(" )");
            RegexUtil.optNotBlankStrLowerOpt(paramMap.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(f.file_original_name) like ")
                    .append(e).append(")"));
        }
        return whereString.toString();
    }

    /**
     * 获取知识库列表
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> getKnowledgeBaseListForPage(MethodParam methodParam, Map<String, Object> paramMap) {
        String schemaName = methodParam.getSchemaName();
        String userId = methodParam.getUserId();
        String business_no = RegexUtil.optNotNullMap(paramMap).map(p -> RegexUtil.optStrOrNull(p.get("business_no"))).orElse(null);
        // 获取按条件拼接的sql
        String searchWord = this.getKnowledgeBaseWhereString(methodParam, paramMap);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        int total = knowledgeBaseMapper.countKnowledgeBaseList(schemaName, searchWord, userId, business_no, userId);
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by f.create_time desc ";
            searchWord += pagination;
            List<Map<String, Object>> knowledgeBaseList = null;
            String searchDtlType = methodParam.getSearchDtlType();
            if (RegexUtil.optIsPresentStr(searchDtlType)) {
                if (Constants.SEARCH_DTL_TYPE.equals(searchDtlType)) {
                    knowledgeBaseList = knowledgeBaseMapper.findExportKnowledgeBaseList(schemaName, searchWord, userId, business_no, userId);
                }
            } else {
                knowledgeBaseList = knowledgeBaseMapper.findKnowledgeBaseList(schemaName, searchWord, userId, business_no, userId);
            }
            result.put("rows", knowledgeBaseList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 获取知识库明细信息
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> getKnowledgeBaseDetailById(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optNotNullOrExp(paramMap.get("id"), LangConstant.MSG_A, new String[]{LangConstant.TITLE_ACJ}); // 主键不能为空
        Map<String, Object> knowledgeBaseInfo = RegexUtil.optMapOrExpNullInfo(knowledgeBaseMapper.findknowledgeBaseDetailById(methodParam.getSchemaName(), RegexUtil.optStrOrNull(paramMap.get("id"))), LangConstant.TITLE_KF); // 知识库信息不存在
        return knowledgeBaseInfo;
    }

    /**
     * 获取知识库明细信息列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @param listType    列表查询类型
     * @return 设备信息列表
     */
    private List<Map<String, Object>> getKnowledgeBaseCodeDtlListByParam(MethodParam methodParam, Map<String, Object> paramMap, String listType) {
        methodParam.setSearchDtlType(listType);
        Map<String, Object> info = this.getKnowledgeBaseListForPage(methodParam, paramMap); // 数据权限判断
        return RegexUtil.optObjToListOrExp(RegexUtil.optMapOrExpNullInfo(info, LangConstant.TITLE_KF).get("rows"), LangConstant.TITLE_KF); // 知识库信息不存在
    }

    /**
     * 根据选中主键下载知识库信息
     *
     * @param methodParam
     * @param paramMap
     */
    @Override
    public void getExportKnowledgeBaseCode(MethodParam methodParam, Map<String, Object> paramMap, HttpServletResponse response) {
        exportService.doDownloadKnowledgeBaseInfo(methodParam, this.getKnowledgeBaseCodeDtlListByParam(methodParam, paramMap, Constants.SEARCH_DTL_TYPE), response);
    }
}
