package com.gengyun.senscloud.service.snc;

import com.gengyun.senscloud.model.MethodParam;

import java.util.Map;

public interface SncDataImportService {

    /**
     * 维修工单打印pdf
     *
     * @param methodParam
     * @param paramMap
     */
    void exportWorksPdf(MethodParam methodParam, Map<String, Object> paramMap);
}
