package com.gengyun.senscloud.service.business.impl;

import com.gengyun.senscloud.service.business.AddressManageService;
import org.springframework.stereotype.Service;

/**
 * 地址管理
 * User: sps
 * Date: 2018/11/20
 * Time: 上午11:20
 */
@Service
public class AddressManageServiceImpl implements AddressManageService {
//    @Autowired
//    AddressManageMapper addressManageMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @return
//     */
//    @Override
//    public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber,
//                            String sortName, String sortOrder) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        if (StringUtils.isNotEmpty(sortName)) {
//            if (StringUtils.isEmpty(sortOrder)) {
//                sortOrder = "asc";
//            }
//            orderBy = sortName + " " + sortOrder;
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = addressManageMapper.query(schemaName, orderBy, pageSize, begin, searchKey);
//        int total = addressManageMapper.countByCondition(schemaName, searchKey);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel addAddress(HttpServletRequest request, User user, String schemaName, Map<String, Object> paramMap) {
//        paramMap.put("schema_name",schemaName);
//        int count = addressManageMapper.insert(paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(paramMap);//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel updateAddress(HttpServletRequest request, User user, String schemaName, Map<String, Object> paramMap) {
//        int count = addressManageMapper.update(schemaName, paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel deleteById(User user, String schemaName, HttpServletRequest request) {
//        Integer id = Integer.valueOf((String) request.getParameter("id"));
//        int count = addressManageMapper.delete(schemaName, id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
}
