package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

/**
 * 用户个人配置
 */
public interface UserInfoEditService {
    /**
     * 更新用户常用语言类型
     *
     * @param methodParam 系统参数
     * @param lang        语言类型
     */
    void modifyUserConfig(MethodParam methodParam, String lang);
}
