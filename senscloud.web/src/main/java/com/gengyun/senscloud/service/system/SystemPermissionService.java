package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SystemPermissionResult;

import java.util.List;

public interface SystemPermissionService {
    /**
     * 根据用户id获取当前用户的菜单列表
     * @param methodParam 入参
     * @return 菜单列表
     */
    List<SystemPermissionResult> getByUserId(MethodParam methodParam) ;
}