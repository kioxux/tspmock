package com.gengyun.senscloud.service.bom;

/**
 * 备件入库
 */
public interface BomInStockService {
//    /**
//     * 获取备件入库列表
//     *
//     * @param request
//     * @return
//     */
//    String findBomInStockList(HttpServletRequest request);
//
//    /**
//     * 根据主键查询备件入库数据
//     *
//     * @param schemaName
//     * @param inCode
//     * @return
//     */
//    Map<String, Object> queryBomInStockById(String schemaName, String inCode);
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @param pageType
//     * @return
//     * @throws Exception
//     */
//    ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception;
//
//    /**
//     * 生成备件入库数据、流程
//     *
//     * @param schema_name
//     * @param doFlowKey
//     * @param user
//     * @param processDefinitionId
//     * @param roleIds
//     * @param bomInStock
//     * @param bomInStockDetails
//     * @param source
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel addBomInStock(String schema_name, String doFlowKey, User user, String processDefinitionId, String roleIds, Map<String, Object> bomInStock,
//                                       JSONArray bomInStockDetails, String source, boolean isNew, boolean hasAudit) throws Exception;
//
//    /**
//     * 入库申请作废
//     *
//     * @param schema_name
//     * @param user
//     * @param in_code
//     * @return
//     */
//    ResponseModel cancel(String schema_name, User user, String in_code);
//
//    /**
//     * 备件入库审核
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param user
//     * @param isFinish    审核通过后是否完成入库
//     * @return
//     */
//    public ResponseModel inStockAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, boolean isFinish) throws Exception;
//
//    /**
//     * 随机取一个库房管理人
//     *
//     * @param schema_name
//     * @param roleIds
//     * @param stock_code
//     * @return
//     */
//    public String getBomStockUserRandom(String schema_name, String roleIds, String stock_code);
//
//    /**
//     * 修改备件对外价格及货币单位
//     *
//     * @param schema_name
//     * @param bomStockData
//     * @return
//     */
//    int synchronizePrice(String schema_name, Map<String, Object> bomStockData);
}
