package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetAnalysByMapService;
import org.springframework.stereotype.Service;

@Service
public class AssetAnalysByMapServiceImpl implements AssetAnalysByMapService {
//    @Autowired
//    AssetAnalysByMapMapper mapper;
//
//    @Autowired
//    FacilitiesMapper facilitiesMapper;
//
////    //根据位置id，获取位置的信息
////    @Override
////    public FacilitiesResult GetFacilitiesResultById(String schema_name, Integer facilityId) {
////        return mapper.GetFacilitiesResultById(schema_name, facilityId);
////    }
//
//    //根据位置id，获取位置的信息
//    @Override
//    public List<FacilitiesResult> GetFacilitiesCustomResultById(String schema_name, String type, String assetType, Integer facilityId) {
//        String filed = "";
//        if("0".equals(type) || ("1".equals(type) && ("0".equals(assetType) || "1".equals(assetType) ))) {
//            //地图显示位置、设备-保障时，查询待维修设备数量
//            //地图显示设备-计划保养时，查询待保养设备数量
//            int businessType = Constants.BUSINESS_TYPE_REPAIR_TYPE;
//            if("1".equals(type) && "1".equals(assetType))
//                businessType = Constants.BUSINESS_TYPE_MAINTAIN_TYPE;
//
//            filed = ",(SELECT COUNT(DISTINCT a._id ) FROM "+schema_name+"._sc_asset a  " +
//                    "INNER JOIN "+schema_name+"._sc_works_detail wd ON wd.relation_id = a._id " +
//                    "INNER JOIN "+schema_name+"._sc_asset_organization ao ON ao.asset_id = a._id " +
//                    "INNER JOIN "+schema_name+"._sc_work_type wt ON wt.id = wd.work_type_id " +
//                    "INNER JOIN (" +
//                    "   SELECT cf.* FROM "+schema_name+"._sc_facilities pf " +
//                    "   INNER JOIN "+schema_name+"._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
//                    "   WHERE pf.id = fac.ID " +
//                    ") f ON ao.org_id = f.id " +
//                    "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.COMPLETED +
//                    " AND wt.business_type_id = "+ businessType +" AND wd.relation_type = 2 ) as repairMaintainAssetCount ";
//        }else if("1".equals(type) && ("-1".equals(assetType) || "2".equals(assetType) || "3".equals(assetType)) ) {
//            //地图显示设备-物联时，查询物联设备数量
//            //地图显示设备-离线物联时，查询物联设备在线、离线数量
//            String tmp = "SELECT COUNT(DISTINCT a._id) FROM "+schema_name+"._sc_asset a " +
//                    "INNER JOIN "+schema_name+"._sc_asset_organization ao ON ao.asset_id = a._id " +
//                    "INNER JOIN " +
//                    "( " +
//                    "SELECT cf.* FROM "+schema_name+"._sc_facilities pf " +
//                    "INNER JOIN "+schema_name+"._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
//                    "WHERE pf.id = fac.ID " +
//                    ") f ON ao.org_id = f.id " +
//                    "LEFT JOIN "+schema_name+"._sc_asset_iot_setting i ON a._id = i.asset_id ";
//            if("-1".equals(assetType)){//全部设备数量
//                filed = ",(" + tmp + " ) as assetCount ";
//            }else if("2".equals(assetType)) {//物联设备数量
//                filed = ",(" + tmp + " WHERE i.iot_status in ("+Constants.ASSET_IOT_SETTING_STATUS_ONLINE+","+Constants.ASSET_IOT_SETTING_STATUS_OFFLINE+") ) as iotAssetCount ";
//            }else if("3".equals(assetType)){//在线、离线数量
//                filed = ",(" + tmp + " WHERE i.iot_status = "+Constants.ASSET_IOT_SETTING_STATUS_ONLINE+") as onlineIotAssetCount " +
//                        ",(" + tmp + " WHERE i.iot_status = "+Constants.ASSET_IOT_SETTING_STATUS_OFFLINE+") as offlineIotAssetCount ";
//            }
//        }else{
//            return null;
//        }
//        if(facilityId == null)//查询全部组织
//            return mapper.GetAllParentFacilitiesCustomResultList(schema_name, filed);
//
//        if("0".equals(type)){//如果是位置，且指定了某个位置，则需要查询选中位置以及它的子位置
//            return mapper.GetFacilityAndChildrenCustomResultById(schema_name, filed, facilityId);
//        }else {
//            return mapper.GetFacilitiesCustomResultById(schema_name, filed, facilityId);
//        }
//    }
//
////    //根据条件，获取所有的位置，如果为0，返回所有的第一级，否则，按父节点查询
////    //修改为，当为0时，返回所有的地图为百度地图的位置，2018-08-11
////    @Override
////    public List<FacilitiesResult> GetAllFacilitiesResultList(String schema_name, Integer facilityId) {
////        if (facilityId > 0) {
////            return mapper.GetAllFacilitiesResultList(schema_name, facilityId);
////        } else {
////            return mapper.GetAllParentFacilitiesResultList(schema_name);
////        }
////    }
////
////    //场地监控，根据位置id，查询所有的设备
////    @Override
////    public List<Asset> GetFacilityMonitorAssetByFacilityId(String schema_name, Integer facilityId, String status) {
////        return mapper.GetFacilityMonitorAssetByFacilityId(schema_name, facilityId, status);
////    }
////
////    //根据位置id，监控设备
////    @Override
////    public List<Asset> GetAssetByFacilityAndMonitor(String schema_name, Integer facilityId) {
////        return mapper.GetAssetByFacilityAndMonitor(schema_name, facilityId);
////    }
////
////    /**
////     * 根据组织ID，查询设备数量
////     * @param schema_name
////     * @param type  1、维修 2、保养
////     * @param facilityId
////     * @return
////     */
////    @Override
////    public int countAssetByFacilityId(String schema_name, int type, Long facilityId) {
////        return mapper.countAssetByFacilityId(schema_name, type, facilityId);
////    }
//
//    /**
//     * 根据组织ID，查询设备列表
//     * @param schema_name
//     * @param type  1、维修 2、保养
//     * @param facilityNo
//     * @return
//     */
//    @Override
//    public List<Asset> getAssetByFacilityId(String schema_name, Integer type, String facilityNo) {
//        return mapper.getAssetByFacilityId(schema_name, type, facilityNo);
//    }
//
//    /**
//     * 根据组织ID，查询过期的设备数量
//     *
//     * @param schema_name
//     * @param facilityNo
//     * @return
//     */
//    @Override
//    public int countExpireAssetByFacilityId(String schema_name, String facilityNo) {
//        return mapper.countExpireAssetByFacilityId(schema_name, facilityNo);
//    }
//
//    /**
//     * 获取子组织（含有监控设备的组织）坐标信息以及监控设备数量
//     *
//     * @param schema_name
//     * @param facilityId
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getChildFacilityWithMonitor(String schema_name, Integer facilityId) {
//        return mapper.getChildFacilityWithMonitor(schema_name, facilityId);
//    }
}
