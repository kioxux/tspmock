package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.UserRegisterService;
import org.springframework.stereotype.Service;

@Service
public class UserRegisterServiceImpl implements UserRegisterService {
//    private static final Logger logger = LoggerFactory.getLogger(UserRegisterServiceImpl.class);
//    @Autowired
//    UserRegisterMapper userRegisterMapper;
//    @Autowired
//    MetadataWorkMapper metadataWorkMapper;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    SerialNumberService serialNumberService;
//    @Autowired
//    MessageService messageService;
//    @Autowired
//    VerificationCodeService verificationCodeService;
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    /**
//     * 用户注册提交
//     *
//     * @param request
//     * @param schema_name
//     * @param userRegister
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public int addUserRegister(HttpServletRequest request, String schema_name, UserRegisrerData userRegister) throws Exception {
//        int result = 0;
//        if (StringUtil.isEmpty(userRegister.getUser_mobile()) || StringUtil.isEmpty(userRegister.getUser_name()) || StringUtil.isEmpty(userRegister.getPassword())) {
//            return 1; //手机，密码和用户名必填
//        }
//        userRegister.setStatus(StatusConstant.TO_BE_AUDITED);
//        // 检查用户表中是否有重复的电话号码，此时的电话号码，同时做了用户工号和用户账号,
//        int checkResult = userRegisterMapper.checkUserReduplicate(schema_name, userRegister.getUser_mobile());
//        if (checkResult > 0) {
//            return 3;  // 用户已经存在
//        }
//
//        // 检查用户是否提交过申请
//        checkResult = userRegisterMapper.checkUserRegister(schema_name, userRegister.getUser_mobile());
//        if (checkResult > 0) {
//            return 2; // 已经提交过申请
//        }
//
//
//        String appCode = dynamicCommonService.getDataCode(schema_name, 25);
//        userRegister.setApp_code(appCode);
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        userRegister.setCreate_time(now);
//
//        //添加用户注册信息
//        int doAdd = userRegisterMapper.addUserRegister(schema_name, userRegister);
//        if (doAdd > 0) {
//            //发起流程
//            String titleTodo = userRegister.getUser_name();
//            String title = userRegister.getUser_name() + selectOptionService.getLanguageInfo(LangConstant.APPLY_A) + selectOptionService.getLanguageInfo(LangConstant.USER_REGISTER);
//            //获取一个facility，去找一个用户审核角色的人
//
//            Map<String, String> map = new HashMap<String, String>();
//            map.put("title_page", titleTodo);
//            map.put("sub_work_code", appCode);
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            long tomorrowMill = now.getTime() + 24 * 3600 * 1000;
//            Date tomorrowDate = new Date(tomorrowMill);
//            map.put("create_time", nowDateWord);
//            map.put("deadline_time", simleDateFormat.format(tomorrowDate));
//            // 获取新模板
//            Map<String, Object> template = metadataWorkMapper.getWdTemplateByBusinessType(schema_name, Integer.valueOf(SensConstant.BUSINESS_NO_63), 2);
//            // 获取工单类型id
//            String workTypeId = null == template.get("work_type_id") ? "" : template.get("work_type_id").toString();
//            map.put("businessType", workTypeId);
//            JSONArray arrays = JSONArray.fromObject(JSONObject.fromObject(template.get("body_property")).get("value"));
//            JSONObject data = null;
//            Map<String, Object> templateMap = new HashMap<String, Object>();
//            String fieldCode = null;
//            for (Object object : arrays) {
//                data = JSONObject.fromObject(object);
//                fieldCode = data.get("fieldCode").toString();
//                templateMap.put(fieldCode, data.get("fieldValue"));
//            }
//            String receive_account = commonUtilService.getRandomUserAllByRole(schema_name, null, map, templateMap);
//            map.put("receive_account", receive_account);
//            String flowId = dynamicCommonService.getWorkFlowId(schema_name, SensConstant.BUSINESS_NO_63);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, flowId, "system", map);
//            if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.USER_REGISTER) + "system");
//            }
//
//            String[] contentParam = {userRegister.getUser_name(), selectOptionService.getLanguageInfo(LangConstant.USER_REGISTER)};
//            messageService.beginMsgWithOutRequest(schema_name, contentParam, qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_63, userRegister.getApp_code(), "system", null);
//
//            workProcessService.saveWorkProccess(schema_name, appCode, StatusConstant.TO_BE_AUDITED, "system", Constants.PROCESS_CREATE_TASK);//工单进度记录
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_63), appCode, title, "system");//记录历史
//            result = 200;
//        }
//
//        return result;
//    }
//
//
//    /**
//     * 用户注册审核
//     *
//     * @param request
//     * @param schema_name
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public int auditUserRegister(HttpServletRequest request, String schema_name, long companyId, Map<String, Object> paramMap) throws Exception {
//        String account = (String) paramMap.get("account");
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> businessInfo = (Map<String, Object>) dataInfo.get("_sc_user_register");
//        String code = (String) businessInfo.get("app_code");
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> dbDataInfo = userRegisterMapper.queryUserRegisterByCode(schema_name, code);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, code);
//        String logWord = selectOptionService.getLanguageInfo(LangConstant.MSG_AUDIT_PASS);//审核通过
//        String remark = (String) businessInfo.get("audit_content");
//        remark = (null == remark || remark.isEmpty()) ? "" : remark;
//        Map map = new HashMap();
//        int status = StatusConstant.COMPLETED;
//        Boolean is_submit = false; // 是否提交
//        businessInfo.put("audit_time", new Timestamp(System.currentTimeMillis()));
//        businessInfo.put("audit_person", account);
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {//同意按钮
//            is_submit = true;//提交事件
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            is_submit = true;//提交事件
//            status = StatusConstant.REFUSE;
//            logWord = selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK);
//        }
//        businessInfo.put("status", status);
//        userRegisterMapper.updateUserRegisterByCode(schema_name, businessInfo, code); // 更新信息
//        boolean isAgree = false;
//        int doResult = 0;
//        // 同意按钮
//        if (null != do_flow_key && !"".equals(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {
//            userService.doAddUserByRegister(businessInfo, schema_name, companyId); // 用户信息转移
//            isAgree = true;
//        }
//        if (is_submit) {
//            String taskId = request.getParameter("task_id");
//            map.put("status", status);
//            map.put("sub_work_code", code);
//            map.put("do_flow_key", do_flow_key);
//            map.put("create_user_account", account); // 发送短信人
//            String receive_account = (String) dbDataInfo.get("create_user_account");
//            map.put("receive_account", receive_account); // 接受人
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, code);
//            Boolean workFlowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workFlowResult) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK));
//            }
//            workProcessService.saveWorkProccess(schema_name, code, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_63), code, logWord + "：" + remark, account);//记录历史
//            doResult = 1;
//        }
//        SystemConfigData wxNameInfo = systemConfigService.getSystemConfigData(schema_name, "wx_name");
//        String wxName = Constants.SYSTEM_NAME;
//        if (null != wxNameInfo && RegexUtil.isNotNull(wxNameInfo.getSettingValue())) {
//            wxName = wxNameInfo.getSettingValue();
//        }
//        String[] contentParam;
//        String msgTemplate = qcloudsmsConfig.SMS_10002004; // 拒绝模板
//        if (isAgree) {
//            contentParam = new String[]{wxName, wxName, (String) businessInfo.get("user_mobile")};
//            msgTemplate = qcloudsmsConfig.SMS_10001003;
//        } else {
//            contentParam = new String[]{wxName, selectOptionService.getLanguageInfo(LangConstant.USER_REGISTER)};
//        }
//        messageService.doSendMsg(schema_name, contentParam, msgTemplate, (String) businessInfo.get("user_mobile"), SensConstant.BUSINESS_NO_63, (String) businessInfo.get("app_code"), "system", null, (String) businessInfo.get("user_mobile"));
//        return doResult;
//    }
//
//    /**
//     * 获取用户注册列表
//     *
//     * @param request
//     */
//    @Override
//    public String findUserRegisterList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 20;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 20;
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String keyWord = request.getParameter("keyWord");
//        //首页条件
//        StringBuffer condition = new StringBuffer("");
//
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition.append(" and (");
//            condition.append(" t.user_name like '%" + keyWord + "%'");
//            condition.append(" t.user_mobile like '%" + keyWord + "%'");
//            condition.append(" or t.audit_person like '%" + keyWord + "%'");
//            condition.append(" or au.username like '%" + keyWord + "%'");
//            condition.append(" or s.status like '%" + keyWord + "%'");
//            condition.append(" ) ");
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = userRegisterMapper.findUserRegisterList(schemaName, condition.toString(), pageSize, begin);
//        User user = AuthService.getLoginUser(request);
//        String account = user.getAccount();
//        List<String> roles = new ArrayList<>(user.getRoles().keySet());
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("app_code")));
//        if (subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schemaName, "", null, subWorkCodes, account, roles, false, "", "", "", pageSize, pageNumber);
//
//        for (Map<String, Object> dataMap : dataList) {
//            String appCode = (String) dataMap.get("app_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(appCode);
//            if (task != null && null != task.getAssignee()) {
//                dataMap.put("is_handle", task.getAssignee().equals(account));
//                continue;
//            }
//        }
//        int total = userRegisterMapper.countUserResgisterList(schemaName, condition.toString());
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 根据主键查询用户注册数据
//     *
//     * @param schemaName
//     * @param code
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryUserRegisterByCode(String schemaName, String code) {
//        return userRegisterMapper.queryUserRegisterByCode(schemaName, code);
//    }
//
//    /**
//     * 详情用户注册获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    @Override
//    public ModelAndView getUserRegisterDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String result = this.findUserRegisterList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("app_code");
//
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        permissionInfo.put("user_register_audit", "isDistribute");
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "user_register");
//
//        Map<String, Object> data = userRegisterMapper.queryUserRegisterByCode(schemaName, code);
//        Integer status = (Integer) data.get("status"); // 工单类型
//        if (StatusConstant.TO_BE_AUDITED != status) {
//            mav.getModel().put("isDistribute", false);
//        }
//
//        Map<String, Object> template = metadataWorkMapper.getWdTemplateByBusinessType(schemaName, Integer.valueOf(SensConstant.BUSINESS_NO_63), 2); // 获取模板
//        mav.addObject("workFormKey", template.get("work_template_code"));
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/user_register/findSingleUserRegisterInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 用户注册APP端审核和分配的信息获取
//     */
//    public JSONObject getUserRegisterHandleInfo(String schemaName, String account, String workTemplateCode, String subWorkCode, String businessNo, HttpServletRequest request) throws Exception {
//        MetadataWork template = metadataWorkMapper.queryById(schemaName, workTemplateCode); // 获取新模板
//        Map<String, Object> oldData = null; // 获取原工单数据
//        JSONObject result = new JSONObject();
//        if (null != subWorkCode && !"".equals(subWorkCode) && !"undefined".equals(subWorkCode)) {
//            // 用户注册
//            if (SensConstant.BUSINESS_NO_63.equals(businessNo)) {
//                oldData = userRegisterMapper.queryUserRegisterByCode(schemaName, subWorkCode); // 获取原数据
//            }
//
//            // 获取历史数据
//            try {
//                List<LogsData> historyList = commonUtilService.queryLogByBusinessNo(schemaName, subWorkCode);
//                //多时区处理
//                SCTimeZoneUtil.responseObjectListDataHandler(historyList, new String[]{"create_time"});
//                result.put("historyData", historyList);
//            } catch (Exception exp) {
//                logger.warn(selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_HIS_DATA) + subWorkCode);//历史信息未处理
//            }
//        }
//
//        Boolean isDistribute = false;
//        String isDistributeStr = request.getParameter("isDistribute");
//        if (RegexUtil.isNotNull(isDistributeStr)) {
//            isDistribute = Boolean.valueOf(isDistributeStr);
//        }
//        dynamicCommonService.getPageInfo(schemaName, request, account, result, template, oldData, false, isDistribute, subWorkCode);
//        return result;
//    }
//
//    //发送验证码
//    @Override
//    public ResponseModel sendUserRegisterVerificationCode(String schema_name, String phoneNumber) {
//        if (RegexUtil.isNull(schema_name)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//        if (RegexUtil.isNull(phoneNumber) || !RegexUtil.isMobile(phoneNumber)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_USER_MOBILE_WRONG));
//        }
//
//        // 检查用户表中是否有重复的电话号码，此时的电话号码，同时做了用户工号和用户账号,
//        int checkResult = userRegisterMapper.checkUserReduplicate(schema_name, phoneNumber);
//        if (checkResult > 0) {
//            // 用户已经存在
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_USER_MOBILE_ALREADY_USE));
//        }
//
//        // 检查用户是否提交过申请
//        checkResult = userRegisterMapper.checkUserRegister(schema_name, phoneNumber);
//        if (checkResult > 0) {
//            // 已经提交过申请
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_USER_MOBILE_ALREADY_USE));
//        }
//
//        try {
//            String VerificationCode = verificationCodeService.getNewestVerificationCode(schema_name, phoneNumber);
//            HttpServletRequest request = HttpRequestUtils.getRequest();
//            request.setAttribute("account", "system");
//            String[] contentParam = {VerificationCode, "1"};
//            messageService.doSendMsg(schema_name, contentParam, qcloudsmsConfig.SMS_10001002, phoneNumber, SensConstant.BUSINESS_NO_63, "user_register", "system", null, phoneNumber);
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_VERINONEMIN));
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//    }
}
