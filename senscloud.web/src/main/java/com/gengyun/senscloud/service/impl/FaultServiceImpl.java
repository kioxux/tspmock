package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.FaultService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
//@CacheConfig(cacheNames = "fault")
@Service
@Component
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class FaultServiceImpl implements FaultService {
//
//    @Autowired
//    SetMonitorMapper setMonitorMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Cacheable
//    @Override
//    public Map<String, Object> getFault(String schema_name) {
//        return this.faultData(schema_name);
//    }
//
//    @CachePut
//    @Override
//    public Map<String, Object> refreshFault(String schema_name) {
//        return this.faultData(schema_name);
//    }
//    public Map<String, Object> faultData(String schema_name){
//        try {
//            Map<String,Object> resultMap=new HashMap<>();
//            List<Map<String, Object>> assetCategoryList = selectOptionService.SelectOptionList(schema_name,null,"asset_type_app",null,null);
//            String asset_category_id = "";
//            for (Map<String, Object> assetCategory : assetCategoryList) {
//                asset_category_id = String.valueOf(assetCategory.get("code"));
//                resultMap.put(asset_category_id, this.faultDataById(asset_category_id,schema_name));
//            }
//            return resultMap;
//        }catch (Exception e){
//            e.printStackTrace();
//            return null;
//        }
//
//    }
//    private Map<String, Object> faultDataById(String asset_category_id,String schema_name){
//        try{
//            Map<String,Object> resultMap=new HashMap<>();
//            List<Map<String, Object>> faultData=setMonitorMapper.findFaultData(schema_name,asset_category_id);
//            for(Map<String,Object> fdata:faultData){
//                resultMap.put(String.valueOf(fdata.get("fault_code")),(String)fdata.get("code_name"));
//            }
//            return resultMap;
//        }catch (Exception e){
//            e.printStackTrace();
//            return null;
//        }
//    }
}
