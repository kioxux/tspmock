package com.gengyun.senscloud.service.dynamic;

/**
 * 工单业务处理
 * User: sps
 * Date: 2018/12/28
 * Time: 上午11:20
 */
public interface WorkSheetHandleBusinessService {
//
//    /**
//     * 维修确认操（审核）作
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel confirmRepairProblem(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 维修确认操作（1级审核）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel confirmFirstRepairProblem(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 维修执行事件（处理提交）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel doRepairProblem(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 子页面数据保存
//     *
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    ResponseModel doSaveSubPage(Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 接单
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel receiveRepairProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 问题确认（提交、关闭）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel confirmProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 店长确认（重新提交、关闭）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel reconfirmRepairProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 维修问题确认（主管转派、接单、退回）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel confirmRepairProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 巡检任务完成提交
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel finishedInspectioncheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 巡检任务确认
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel auditInspectioncheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 点检任务完成提交
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel finishedSpotcheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 点检任务确认
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel auditSpotcheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 分配事件
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel distribution(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 工单请求确认
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    ResponseModel confirmWsq(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 更新字段值
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    ResponseModel doUpdateByData(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception;
//
//    /**
//     * 问题上报
//     *
//     * @param schema_name
//     * @param account
//     * @param accountName
//     * @param fromCode
//     * @param pageData
//     * @param paramMap
//     * @param workTmp
//     * @param relationId
//     * @param deviceCodes
//     * @throws Exception
//     */
//    void repairWorkData(String schema_name, String account, String accountName, String title, String fromCode,
//                        Map<String, Object> pageData, Map<String, Object> paramMap, Map<String, Object> workTmp,
//                        String relationId, List<Map<String, Object>> deviceCodes) throws Exception;
}
