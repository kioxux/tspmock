package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;

import java.util.Map;

/**
 * Created by Administrator on 2018/4/25.
 */
public interface StockService {

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getStockListPermission(MethodParam methodParam);

    /**
     * 查询库房列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getStockList(MethodParam methodParam, SearchParam param);

    /**
     * 新增库房
     *
     * @param methodParam
     * @param paramMap
     */
    void newStock(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑库房
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyStock(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取库房明细信息
     *
     * @param methodParam
     * @param id
     * @return
     */
    Map<String, Object> getStockDetailById(MethodParam methodParam, String id);

    /**
     * 启用禁用库房
     *
     * @param methodParam
     * @param data
     */
    void modifyStockUse(MethodParam methodParam, Map<String, Object> data);

    /**
     * 删除选中库房
     *
     * @param methodParam
     */
    void cutStockByIds(MethodParam methodParam);

    /**
     * 查询库房库存清单列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getStockBomList(MethodParam methodParam, SearchParam param);

    /**
     * 设置库房安全库存
     *
     * @param methodParam
     * @param paramMap
     */
    void modifySafeStock(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 查询库房出入库记录列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getStockInAndOutList(MethodParam methodParam, SearchParam param);

    /**
     * 查询库房备件导入记录列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getStockBomImportList(MethodParam methodParam, SearchParam param);
//
//    //按权限，查询库房列表
//    List<StockData> getStockListByUserPermission(String schema_name, String condition, String account);
//
//    //查询库房列表，按库房名称、使用组织、负责客户
//    List<StockData> findStockListByKey(String schema_name, String keySearch, String stockPermissionCondition, int pageSize, int begin);
//
//    //查询库房总数量，按库房名称、使用组织、负责客户
//    int findStockListCountByKey(String schema_name, String keySearch, String stockPermissionCondition);
//
//    //根据ID查找库房信息
//    StockData findByStockCode(String schema_name, String stock_code);
//
//    //删除库房
//    int deleteStockData(String schema_name, String stock_code, int status);
//
//    //新增库房
//    int insertStockData(String schema_name, StockData stockData);
//
//    //更新库房信息
//    int updateStockData(String schema_name, StockData stockData);
}
