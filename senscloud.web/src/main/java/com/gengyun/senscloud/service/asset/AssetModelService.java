package com.gengyun.senscloud.service.asset;

import com.gengyun.senscloud.model.AssetCategoryModel;
import com.gengyun.senscloud.model.AssetModelModel;
import com.gengyun.senscloud.model.MethodParam;
import net.sf.json.JSONArray;

import java.util.List;
import java.util.Map;

/**
 * 设备型号
 */
public interface AssetModelService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAssetModelListPermission(MethodParam methodParam);

    /**
     * 新增设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     */
    void newAssetModel(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 删除设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     */
    void cutAssetModel(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 编辑设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     */
    void modifyAssetModel(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 分页查询设备型号
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     * @return 设备型号分页列表
     */
    Map<String, Object> getAssetModelListPage(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 查询设备型号详情
     *
     * @param methodParam     入参
     * @param assetModelModel 入参
     * @return 查询设备型号详情
     */
    Map<String, Object> getAssetModelInfo(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 查询设备型号详情
     *
     * @param methodParam 入参
     * @param id          数据主键
     * @return 查询设备型号详情
     */
    Map<String, Object> getAssetModelInfoById(MethodParam methodParam, Integer id);

    /**
     * 分页查询附件文档新消息
     */
    Map<String, Object> getAssetModelDocPage(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 删除设备类型附件文档
     */
    void cutAssetModelDocs(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 新增设备类型附件文档
     */
    void newAssetModelDocs(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 分页查询故障履历
     *
     * @param methodParam     系统入参
     * @param assetModelModel 入参
     * @return 故障履历
     */
    Map<String, Object> getFaultRecordListPage(MethodParam methodParam, AssetModelModel assetModelModel);

    /**
     * 根据型号id获取自定义字段
     *
     * @param methodParam 系统入参
     * @param modelId     入参
     * @return 定义字段
     */
    String getPropertiesById(MethodParam methodParam, Integer modelId);

    /**
     * 验证设备型号是否存在
     *
     * @param schemaName
     * @param assetModelList
     * @return
     */
    Map<String, Map<String, Object>> queryAssetModelByCode(String schemaName, List<String> assetModelList);

    JSONArray getAssetCategoryCustomFields(MethodParam methodParam, AssetCategoryModel assetCategoryModel);
}
