package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.WorkSheetHandleBusinessService;
import org.springframework.stereotype.Service;

/**
 * 工单业务处理
 * User: sps
 * Date: 2018/12/28
 * Time: 上午11:20
 */
@Service
public class WorkSheetHandleBusinessServiceImpl implements WorkSheetHandleBusinessService {
//    private static final Logger logger = LoggerFactory.getLogger(WorkSheetHandleBusinessServiceImpl.class);
//    @Autowired
//    UserMapper userMapper;
//    @Autowired
//    AssetMapper assetMapper;
//    @Autowired
//    UserService userService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    WorkScheduleService workScheduleService;
//    @Autowired
//    WorkListService workListService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    MetadataWorkService metadataWorkService;
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    WorkSheetRequestService workSheetRequestService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    MessageService messageService;
//    @Autowired
//    ExecutorService executorService;
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    WorkRecordHourService workRecordHourService;
//    @Autowired
//    PlanWorkService planWorkService;
//
//    /**
//     * 维修确认操作（最终审核）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel confirmRepairProblem(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        String subWorkCode = "";
//        int status = StatusConstant.COMPLETED;
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        String account = user.getAccount();
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) works_tmp.get("work_code"));
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType + selectOptionService.getLanguageInfo(LangConstant.AUDIT_A);//进行了 ， 审核
//        String receive_account = (String) works_detaile_tmp.get("receive_account");
//        String remark = (String) works_detaile_tmp.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        works_tmp.put("status", StatusConstant.PENDING);
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属位置
//        String msgCode = "";
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {//同意按钮
//            is_submit = true;//提交事件
//            status = StatusConstant.COMPLETED;
//            works_detaile_tmp.put("audit_time", new Timestamp(System.currentTimeMillis()));
//            workSheetHandleService.doCheckBomData(schema_name, SensConstant.BUSINESS_NO_1, subWorkCode, receive_account, false);
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            is_submit = true;//提交事件
//            status = StatusConstant.PENDING;
//            String nextStatus = (String) map_object.get("next_status");
//            if (null != nextStatus && !"".equals(nextStatus)) {
//                status = Integer.valueOf(nextStatus);
//                receive_account = null;
//            }
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
////            works_detaile_tmp.put("varchar@finished_time", "finished_time");
//            works_detaile_tmp.put("finished_time", null);
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_AGAIN) + doType;//任务，需重新
//        }
//        map.put("status", String.valueOf(status));
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        map.put("receive_account", receive_account);//接受人
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
//
//        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, workRequestCode, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            if (status == StatusConstant.COMPLETED) {
//                // 更新根据PM生成的工单的实时值
//                planWorkService.updatePlanWorkAssetTriggleValueByWorkCode(schema_name, subWorkCode);
//                if (RegexUtil.isNotNull(workRequestCode)) {
//                    workflowresult = workflowService.deleteInstancesBySubWorkCode(schema_name, workRequestCode, selectOptionService.getLanguageInfo(LangConstant.LOG_SERVICE_CLOSE)); // 服务单关闭
//                    if (!workflowresult) {
//                        throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.PP_F)); // 流程处理失败
//                    }
//                    workSheetRequestService.doUpdateWsqStatus(schema_name, workRequestCode, StatusConstant.COMPLETED_CLOSE);
//                    workProcessService.saveWorkProccess(schema_name, workRequestCode, StatusConstant.COMPLETED_CLOSE, account, taskSid);//工单进度记录
////                    logService.AddLog(schema_name, pageType, workRequestCode, logOperation + selectOptionService.getLanguageInfo(LangConstant.LOG_SERVICE_CLOSE) + remark, account);//记录历史
//                }
//            }
//            messageService.msgPreProcess(msgCode, receive_account, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 维修确认操作（1级审核）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel confirmFirstRepairProblem(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        String wkPageType = (String) map_object.get("wkPageType");
//        if (RegexUtil.isNull(wkPageType) || "1".equals(wkPageType)) {
//            return this.doConfirmFirstRepairProblem(schema_name, user, map_object);
//        }
//        if ("2".equals(wkPageType)) {
//            return this.goToApproveWork(schema_name, user, map_object);
//        }
//        if ("3".equals(wkPageType)) {
//            return this.confirmWsqWorkOrClose(schema_name, user, map_object);
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.LOG_ERROR_IN_WORKSHEET));//当前工单信息有错误
//    }
//
//    /**
//     * 维修确认操作（1级审核）
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel doConfirmFirstRepairProblem(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        String msgCode = null; // 消息编码
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        String subWorkCode = "";
//        int status = StatusConstant.TO_BE_AUDITED;
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        String account = user.getAccount();
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workCode = (String) map_object.get("work_code");
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 取数据库原数据
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType + selectOptionService.getLanguageInfo(LangConstant.AUDIT_A);//进行了 ， 审核
//        String receive_account = (String) works_detaile_tmp.get("receive_account");
//        String sendAccount = account;
//        String sendName = user.getUsername();
//        String remark = (String) works_detaile_tmp.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属位置
//        map.put("position_code", works_tmp.get("position_code")); // 所属位置
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {//同意按钮
//            receive_account = (String) map_object.get("next_operator");
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
//            msgCode = qcloudsmsConfig.SMS_10003010;
//            sendAccount = (String) works_detaile_tmp.get("receive_account");
//            sendName = selectOptionService.getOptionNameByCode(schema_name, "user", sendAccount, null);
//            map.put("relation_id", works_detaile_tmp.get("relation_id"));
//            receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//            if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//确认了 任务
//            } else {
//                String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.WM_DWP) + "：" + receiveAccountName;//确认了 任务，处理人：
//            }
//            is_submit = true;//提交事件
//            status = StatusConstant.TO_BE_AUDITED;
//            works_detaile_tmp.put("audit_time", new Timestamp(System.currentTimeMillis()));
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            is_submit = true;//提交事件
//            status = StatusConstant.PENDING;
////            works_detaile_tmp.put("varchar@finished_time", "finished_time");
//            works_detaile_tmp.put("finished_time", null);
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_AGAIN) + doType;//退回了，需更新
//        }
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        map.put("receive_account", receive_account);//接受人
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            messageService.msgPreProcess(msgCode, receive_account, businessTypeId, subWorkCode,
//                    sendAccount, sendName, (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 维修确认操作（最终审核）
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel confirmWsqWorkOrClose(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        String subWorkCode = "";
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        String account = user.getAccount();
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) worksDetailTmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String status = (String) works_tmp.get("status");
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) works_tmp.get("work_code"));
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType + selectOptionService.getLanguageInfo(LangConstant.AUDIT_A);//进行了 ， 审核
//        String receive_account = (String) worksDetailTmp.get("receive_account");
//        String remark = (String) worksDetailTmp.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属位置
//        String msgCode = "";
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {//同意按钮
//            is_submit = true;//提交事件
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            is_submit = true;//提交事件
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
////            worksDetailTmp.put("varchar@finished_time", "finished_time");
//            worksDetailTmp.put("finished_time", null);
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_AGAIN) + doType;//任务，需重新
//        }
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        map.put("receive_account", receive_account);//接受人
//        dataInfo.put("_sc_works_detail", worksDetailTmp);
//        map_object.put("dataInfo", dataInfo);
//
//        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, workRequestCode, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            messageService.msgPreProcess(msgCode, receive_account, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, Integer.valueOf(status), account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 维修确认操作
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel goToApproveWork(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        String subWorkCode = "";
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        String account = user.getAccount();
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) worksDetailTmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String status = (String) works_tmp.get("status");
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) works_tmp.get("work_code"));
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType + selectOptionService.getLanguageInfo(LangConstant.AUDIT_A);//进行了 ， 审核
//        String receive_account = (String) worksDetailTmp.get("receive_account");
//        String remark = (String) worksDetailTmp.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属位置
//        String msgCode = "";
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {//同意按钮
//            is_submit = true;//提交事件
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
//            msgCode = qcloudsmsConfig.SMS_10003008;
//            map.put("relation_id", worksDetailTmp.get("relation_id"));
//            receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//            if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//确认了 任务
//            } else {
//                String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.WM_DWP) + "：" + receiveAccountName;//确认了 任务，处理人：
//            }
//            worksDetailTmp.put("audit_time", new Timestamp(System.currentTimeMillis()));
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            is_submit = true;//提交事件
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
////            worksDetailTmp.put("varchar@finished_time", "finished_time");
//            worksDetailTmp.put("finished_time", null);
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_AGAIN) + doType;//任务，需重新
//        }
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        map.put("receive_account", receive_account);//接受人
//        dataInfo.put("_sc_works_detail", worksDetailTmp);
//        map_object.put("dataInfo", dataInfo);
//
//        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, workRequestCode, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            messageService.msgPreProcess(msgCode, receive_account, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, Integer.valueOf(status), account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 维修执行事件（处理提交）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel doRepairProblem(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String msgCode = null; // 消息编码
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Boolean workflowresult = false;//流程提交是否成功
//        String taskId = request.getParameter("task_id");//taskId
//        String account = user.getAccount();
//        String accountName = user.getUsername();
//        String subWorkCode = "";
//        Boolean is_submit = false;//是否提交
//        int status = 0;
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        Map<String, Object> dtlInfo = workSheetHandleService.queryBusinessDetailById(schema_name, subWorkCode);
//        String workRequestCode = "";
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) works_tmp.get("work_code"));
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(dtlInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String urlType = (String) paramMap.get("pc_submit_right");
//        if (!checkExecuteType(schema_name, user.getId(), businessTypeId, urlType)) {
//            return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_NO_PC_SUBMIT_RIGHT);
//        }
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType + selectOptionService.getLanguageInfo(LangConstant.SAVE_S);//进行了 保存
//        String receive_account = (String) works_detaile_tmp.get("receive_account");
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            is_submit = false;//提交事件
//            status = StatusConstant.PENDING;
//        }
//
//        // 页面【开始时间】为空时，自动赋值
//        if (null == dtlInfo.get("begin_time") || "".equals(dtlInfo.get("begin_time"))) {
//            works_detaile_tmp.put("varchar@begin_time", "begin_time");
//            works_detaile_tmp.remove("null@begin_time");
//            works_detaile_tmp.put("begin_time", now);
//        }
//
//        String relationId = (String) works_detaile_tmp.get("relation_id");
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按
//            is_submit = true;//提交事件
//            status = StatusConstant.TO_BE_AUDITED;
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.DONE_D) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//完成了，。任务
//
//            String nextStatus = (String) map_object.get("next_status");
//            if (null != nextStatus && !"".equals(nextStatus)) {
//                status = Integer.valueOf(nextStatus);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.SUBMIT) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//提交了，。任务
//                msgCode = qcloudsmsConfig.SMS_10003008;
//            }
//            if (StatusConstant.TO_BE_AUDITED == status) {
//                works_detaile_tmp.put("varchar@finished_time", "finished_time");
//                works_detaile_tmp.put("finished_time", now);
//                msgCode = qcloudsmsConfig.SMS_10003010;
//            }
//
////            map.put("title_page", works_tmp.get("title"));
//            map.put("sub_work_code", subWorkCode);
//            map.put("create_user_account", account);//发送短信人
//            map.put("businessType", businessTypeId);//工单类型
//            map.put("deadline_time", works_tmp.get("deadline_time"));
//            map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属组织
//            map.put("position_code", works_tmp.get("position_code")); // 所属位置
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                String tmpAccount = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(tmpAccount) && RegexUtil.isNotNull(workRequestCode)) {
//                    tmpAccount = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//                map.put("receive_account", tmpAccount);//接受人
//            } else if (StatusConstant.PENDING == status) {
//                msgCode = null;
//                map.put("receive_account", dtlInfo.get("receive_account"));//接受人
//            } else {
//                map.put("relation_id", works_detaile_tmp.get("relation_id"));
//                String receiver = commonUtilService.getRandomUserByInfoForWork(schema_name, null, map, map_object);
//                map.put("receive_account", receiver);//接受人
//            }
//        }
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        dataInfo.put("_sc_works", works_tmp);
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        Object returnType = "ok"; // 非保存
//        if (is_submit) {
//            workSheetHandleService.doCheckBomData(schema_name, SensConstant.BUSINESS_NO_1, subWorkCode, receive_account, true);
//            workSheetHandleService.insertWorkReceiveGroup(schema_name, subWorkCode, receive_account); // 工单负责人组处理
//            map.put("status", status);
//            String workRequestType = (String) map_object.get("work_request_type");
//            map.put("work_request_type", workRequestType);
//            if (RegexUtil.isNotNull(relationId)) {
//                if (null != map_object.get("is_repair") && "1".equals(map_object.get("is_repair").toString())) {
//                    this.repairWorkData(schema_name, account, accountName, null, subWorkCode, map_object, paramMap, works_tmp, relationId, null);
//                }
//            }
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            if (StatusConstant.TO_BE_AUDITED == status) {
//                workRecordHourService.updateWorkRecordHour(schema_name, subWorkCode, account, now);
//                workRecordHourService.updateWorkTotalHour(schema_name, subWorkCode);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        } else {
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnType = returnContent; // 保存
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 子页面数据保存
//     *
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel doSaveSubPage(Map<String, Object> paramMap) throws Exception {
//        Map<String, Object> dbInfo = commonUtilService.doGetDbBaseInfo();
//        String schema_name = (String) dbInfo.get("schemaName");
//        String account = (String) dbInfo.get("account");
//        String accountName = (String) dbInfo.get("userName");
//        String userId = (String) dbInfo.get("userId");
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String subWorkCode = "";
//        Boolean is_submit = false;//是否提交
//        int status = 0;
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        String workCode = (String) works_tmp.get("work_code");
//        String subPage = (String) paramMap.get("subPage");
//        if (RegexUtil.isNull(subPage)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_WRONG)); // 数据异常，操作没有执行！
//        }
//        Map<String, Object> dtlInfo = null;
//        String workRequestCode = "";
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> detailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode); // 取数据库原数据
//            String flowCode = (String) detailInfo.get("sub_work_code");
//            dtlInfo = workSheetHandleService.queryBusinessDetailById(schema_name, subWorkCode);
//            if (RegexUtil.isNull(dtlInfo)) {
//                dtlInfo = works_detaile_tmp;
//            }
//            dynamicCommonService.doCheckFlowInfo(schema_name, account, flowCode);
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(dtlInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String urlType = (String) paramMap.get("pc_submit_right");
//        if (!checkExecuteType(schema_name, userId, businessTypeId, urlType)) {
//            return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_NO_PC_SUBMIT_RIGHT);
//        }
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType + selectOptionService.getLanguageInfo(LangConstant.SAVE_S);//进行了 保存
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            is_submit = false;//提交事件
//            status = StatusConstant.PENDING;
//        }
//
//        // 页面【开始时间】为空时，自动赋值
//        if (null == dtlInfo.get("begin_time") || "".equals(dtlInfo.get("begin_time"))) {
//            works_detaile_tmp.put("varchar@begin_time", "begin_time");
//            works_detaile_tmp.remove("null@begin_time");
//            works_detaile_tmp.put("begin_time", now);
//        }
//
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按
//            is_submit = true;//提交事件
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.DONE_D) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//完成了，。任务
//            status = StatusConstant.COMPLETED;
//        }
//        works_detaile_tmp.put("status", status);
//        dataInfo.put("_sc_works", works_tmp);
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        Object returnType = "ok"; // 非保存
//        if (is_submit) {
//            String receive_account = (String) dtlInfo.get("receive_account");
//            workSheetHandleService.doCheckBomData(schema_name, SensConstant.BUSINESS_NO_1, subWorkCode, receive_account, true);
//            workSheetHandleService.insertWorkReceiveGroup(schema_name, subWorkCode, receive_account); // 工单负责人组处理
//            String relationId = (String) works_detaile_tmp.get("relation_id");
//            if (RegexUtil.isNotNull(relationId)) {
//                if (null != map_object.get("is_repair") && "1".equals(map_object.get("is_repair").toString())) {
//                    this.repairWorkData(schema_name, account, accountName, null, subWorkCode, map_object, paramMap, works_tmp, relationId, null);
//                }
//            }
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workRecordHourService.updateWorkRecordHour(schema_name, subWorkCode, account, now);
//            workRecordHourService.updateWorkTotalHour(schema_name, subWorkCode);
//        } else {
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("subWorkCode", subWorkCode);
//            returnType = returnContent; // 保存
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 接单
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel receiveRepairProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String msgCode = null; // 消息编码
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        int status = StatusConstant.PENDING;
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        String account = user.getAccount();
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        String subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workCode = (String) map_object.get("work_code");
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 取数据库原数据
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.DISTRIBUTION_A) + doType + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO);//分配了 负责人
//        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属位置
//        String distributeAccount = null;
//        if (do_flow_key != null && !do_flow_key.equals("")) {
//            // 接单
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//                is_submit = true;//提交事件
//                status = StatusConstant.PENDING;
//                works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                works_detaile_tmp.put("receive_time", new Timestamp(System.currentTimeMillis()));
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.MSG_RECEIVED_ORDERS);//已接单
//                // 退回
//            } else if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//                if (!works_detaile_tmp.containsKey("remark") || null == works_detaile_tmp.get("remark") ||
//                        "".equals(works_detaile_tmp.get("remark"))) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.WM_FTF) + SensConstant.REQUIRED_MSG);//退回原因
//                }
//                msgCode = qcloudsmsConfig.SMS_10003009;
//                is_submit = true;//提交事件
//                status = StatusConstant.PENDING_ORDER;
//                String nextStatus = (String) map_object.get("next_status");
//                if (null != nextStatus && !"".equals(nextStatus)) {
//                    status = Integer.valueOf(nextStatus);
//                }
//                distributeAccount = (String) dtlInfo.get("distribute_account");
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    distributeAccount = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(distributeAccount) && RegexUtil.isNotNull(workRequestCode)) {
//                        distributeAccount = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                String distributeAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", distributeAccount, null);
//                works_detaile_tmp.put("varchar@receive_account", "receive_account");
//                works_detaile_tmp.put("receive_account", null);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_R) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_NEED) + distributeAccountName + selectOptionService.getLanguageInfo(LangConstant.LOG_REASSIGN_PERSON);//拒绝了 任务，需 重新分配维修负责人
//                map.put("receive_account", distributeAccount);//接受人
//                String remark = (String) works_detaile_tmp.get("remark");
//                remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//                logOperation = logOperation + remark;
//            }
//        }
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            messageService.msgPreProcess(msgCode, distributeAccount, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 问题确认（提交、关闭）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel confirmProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String msgCode = null; // 消息编码
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        String account = user.getAccount();
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        String subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workCode = (String) map_object.get("work_code");
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 取数据库原数据
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = "";
//        String remark = (String) works_detaile_tmp.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属组织
//        map.put("position_code", String.valueOf(works_tmp.get("position_code"))); // 所属位置
//        int status = StatusConstant.TO_BE_CONFIRM;
//        if (do_flow_key != null && !do_flow_key.equals("")) {
//            // 关闭
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_CLOSE))) {
//                is_submit = true;//提交事件
//                status = StatusConstant.COMPLETED_CLOSE;
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CLOSE_C) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//关闭了 任务
//                msgCode = qcloudsmsConfig.SMS_10003016;
//                map.put("receive_account", String.valueOf(workInfo.get("create_user_account")));//接收人账号
//                // 提交
//            } else if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//                is_submit = true;//提交事件
//                String receiveAccount = (String) works_detaile_tmp.get("receive_account");
//                if (RegexUtil.isNull(receiveAccount) || "-1".equals(receiveAccount)) {
//                    String receive_account = (String) map_object.get("next_operator");
//                    String nextOperatorTask = (String) map_object.get("next_operator_task");
//                    if (RegexUtil.isNotNull(nextOperatorTask)) {
//                        receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                        if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                            receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                        }
//                    }
//                    map.put("relation_id", works_detaile_tmp.get("relation_id"));
//                    receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//                    if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                        msgCode = qcloudsmsConfig.SMS_10003006;
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//确认了 任务
//                    } else {
//                        msgCode = qcloudsmsConfig.SMS_10003008;
//                        map.put("receive_account", receive_account);//接收人账号
//                        String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.WM_DWP) + "：" + receiveAccountName;//确认了 任务，处理人：
//                    }
//                } else {
//                    status = StatusConstant.PENDING;
//                    msgCode = qcloudsmsConfig.SMS_10003007;
//                    map.put("receive_account", receiveAccount);//接收人账号
//                    if (RegexUtil.isNull(works_detaile_tmp)) {
//                        works_detaile_tmp.put("varchar@distribute_account", "distribute_account");
//                        works_detaile_tmp.put("distribute_account", account);
//                        works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                        works_detaile_tmp.put("receive_time", new Timestamp(System.currentTimeMillis()));
//                        works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                        works_detaile_tmp.put("distribute_time", new Timestamp(System.currentTimeMillis()));
//                    }
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receiveAccount, null);
//                    logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;//确认了 任务，负责人：
//                }
//                // 退回
//            } else if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//                is_submit = true;//提交事件
//                msgCode = qcloudsmsConfig.SMS_10003012;
//                String nextStatus = (String) map_object.get("next_status");
//                if (null == nextStatus || "".equals(nextStatus)) {
//                    status = StatusConstant.PENDING;
//                    Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
//                    map.put("receive_account", dtlInfo.get("receive_account"));//接收人账号
//                } else {
//                    status = Integer.valueOf(nextStatus);
//                    map.put("relation_id", works_detaile_tmp.get("relation_id"));
//                    String receiver = commonUtilService.getRandomUserByInfoForWork(schema_name, null, map, map_object);
//                    map.put("receive_account", receiver);//接受人
//                }
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_AGAIN) + doType;//退回了，需更新
//            }
//        }
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            String hideTaskId = (String) map_object.get("hideTaskId");
//            String nextOperator = (String) map_object.get("next_operator");
//            if (StatusConstant.PENDING == status && RegexUtil.isNotNull(hideTaskId) && RegexUtil.isNotNull(nextOperator)) {
//                workProcessService.saveWorkProccess(schema_name, subWorkCode, status, nextOperator, hideTaskId);//工单进度记录
//            }
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 店长确认（重新提交、关闭）、维修班长确认（提交）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel reconfirmRepairProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String account = user.getAccount();
//        String msgCode = null; // 消息编码
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        int status = (Integer) paramMap.get("baseStatus");
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        String subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workCode = (String) map_object.get("work_code");
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 取数据库原数据
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = "";
//        String receive_account = (String) works_detaile_tmp.get("receive_account");
//        String remark = (String) works_detaile_tmp.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属组织
//        map.put("position_code", works_tmp.get("position_code")); // 所属位置
//        if (do_flow_key != null && !do_flow_key.equals("")) {
//            // 关闭
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_CLOSE))) {
//                is_submit = true;//提交事件
//                status = StatusConstant.COMPLETED_CLOSE;
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CLOSE_C) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//关闭了 任务
//                msgCode = qcloudsmsConfig.SMS_10003016;
//                map.put("receive_account", String.valueOf(workInfo.get("create_user_account")));//接收人账号
//                // 重新提交
//            } else if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//                //获取当前时间
//                Timestamp now = new Timestamp(System.currentTimeMillis());
//                is_submit = true;//提交事件
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                        receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                if (receive_account != null && !"-1".equals(receive_account)) {
//                    status = StatusConstant.PENDING_ORDER;
//                    msgCode = qcloudsmsConfig.SMS_10003007;
//                    String nextStatus = (String) map_object.get("next_status");
//                    if (RegexUtil.isNull(nextStatus) || StatusConstant.PENDING == Integer.valueOf(nextStatus)) {
//                        if (RegexUtil.isNotNull(nextStatus)) {
//                            works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                            works_detaile_tmp.put("receive_time", now);
//                            status = Integer.valueOf(nextStatus);
//                        }
//                        works_detaile_tmp.put("varchar@distribute_account", "distribute_account");
//                        works_detaile_tmp.put("distribute_account", account);
//                        works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                        works_detaile_tmp.put("distribute_time", now);
//                    } else {
//                        status = Integer.valueOf(nextStatus);
//                        msgCode = qcloudsmsConfig.SMS_10003008;
//                    }
//                    map.put("receive_account", receive_account);//接收人账号
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
////                    if (StatusConstant.PENDING_ORDER == status) {
////                        logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_RS) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;//重新提交了 任务，负责人
////                    } else {
//                    logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;//确认了 任务，负责人：
////                    }
//                } else {
//                    msgCode = qcloudsmsConfig.SMS_10003005;
//                    if (StatusConstant.DRAFT == status) {
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_RS) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//重新提交了 任务
//                    } else {
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//确认了 任务
//                    }
//                    map.put("relation_id", works_detaile_tmp.get("relation_id"));
//                    receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//                    map.put("receive_account", receive_account);//接受人
//                    status = StatusConstant.UNDISTRIBUTED;
//                }
//            }
//        }
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 维修问题确认（主管转派、接单、退回）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel confirmRepairProblemInfo(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String account = user.getAccount();
//        String msgCode = null; // 消息编码
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//taskId
//        int status = StatusConstant.PENDING_ORDER;
//        Boolean is_submit = false;//是否提交
//        Boolean workflowresult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        String subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workCode = (String) map_object.get("work_code");
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 取数据库原数据
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        Map<String, Object> detailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode); // 取数据库原数据
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.MSG_CA) + selectOptionService.getLanguageInfo(LangConstant.LOG_WHETHER) + selectOptionService.getLanguageInfo(LangConstant.LOG_NEED) + doType;//
//        String receive_account = (String) works_detaile_tmp.get("receive_account");
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(works_tmp.get("facility_id"))); // 所属位置
//        if (do_flow_key != null && !do_flow_key.equals("")) {
//            // 接单
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//                is_submit = true;//提交事件
//                status = StatusConstant.PENDING;
//                if (!works_detaile_tmp.containsKey("plan_arrive_time") || null == works_detaile_tmp.get("plan_arrive_time") ||
//                        "".equals(works_detaile_tmp.get("plan_arrive_time"))) {
//                    return ResponseModel.errorMsg("预计到达时间" + SensConstant.REQUIRED_MSG);
//                }
//                works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                works_detaile_tmp.put("receive_time", new Timestamp(System.currentTimeMillis()));
//                String dbDistributeTime = (String) detailInfo.get("distribute_time");
//                if (RegexUtil.isNull(dbDistributeTime)) {
//                    works_detaile_tmp.put("varchar@distribute_account", "distribute_account");
//                    works_detaile_tmp.put("distribute_account", account);
//                    works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                    works_detaile_tmp.put("distribute_time", new Timestamp(System.currentTimeMillis()));
//                }
//                works_detaile_tmp.put("varchar@receive_account", "receive_account");
//                works_detaile_tmp.put("receive_account", account);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.MSG_RECEIVED_ORDERS); // "已接单";
//                // 退回
//            } else if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//                if (!works_detaile_tmp.containsKey("remark") || null == works_detaile_tmp.get("remark") ||
//                        "".equals(works_detaile_tmp.get("remark"))) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.WM_FTF) + SensConstant.REQUIRED_MSG);//退回原因
//                }
//                msgCode = qcloudsmsConfig.SMS_10003012;
//                is_submit = true;//提交事件
//                status = StatusConstant.DRAFT;
//                String nextStatus = (String) map_object.get("next_status");
//                if (null != nextStatus && !"".equals(nextStatus)) {
//                    status = Integer.valueOf(nextStatus);
//                }
//                String createUserAccount = (String) workInfo.get("create_user_account");
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    createUserAccount = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(createUserAccount) && RegexUtil.isNotNull(workRequestCode)) {
//                        createUserAccount = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                String createUserName = selectOptionService.getOptionNameByCode(schema_name, "user", createUserAccount, null);
//                map.put("receive_account", createUserAccount);//接受人
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + selectOptionService.getLanguageInfo(LangConstant.LOG_NEED) + createUserName + selectOptionService.getLanguageInfo(LangConstant.LOG_CONF_WHETHER_ND);//退回了 任务，需 确认是否需要
//                String remark = (String) works_detaile_tmp.get("remark");
//                remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//                logOperation = logOperation + remark;
//                // 转派
//            } else if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_DISTRIBUTION))) {
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                        receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                if (RegexUtil.isNull(receive_account) || account.equals(receive_account)) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.LOG_RD_NO_MS));
//                }
//                msgCode = qcloudsmsConfig.SMS_10003007;
//                is_submit = true;//提交事件
//                status = StatusConstant.PENDING_ORDER;
//                String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                logOperation = user.getUsername() + selectOptionService.getLanguageInfo(LangConstant.LOG_GIVE) + receiveAccountName + selectOptionService.getLanguageInfo(LangConstant.WM_T) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_WAIT_RECEIPT);//转派了 任务，等待接单
//                map.put("receive_account", receive_account);//接受人
//                works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                works_detaile_tmp.put("distribute_time", new Timestamp(System.currentTimeMillis()));
//                works_detaile_tmp.put("varchar@distribute_account", "distribute_account");
//                works_detaile_tmp.put("distribute_account", account);
//            }
//        }
//        works_detaile_tmp.put("status", status);
//        works_tmp.put("status", status);
//        map.put("status", status);
//        map.put("sub_work_code", subWorkCode);
//        map.put("do_flow_key", do_flow_key);
//        dataInfo.put("_sc_works", works_tmp);
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId); //发送短信内容
//        map.put("deadline_time", works_tmp.get("deadline_time"));
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowresult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowresult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, subWorkCode,
//                    account, user.getUsername(), (String) works_tmp.get("title"), doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 巡检任务完成提交
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel finishedInspectioncheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String urlType = (String) paramMap.get("pc_submit_right");
//        if (!checkExecuteType(schema_name, user.getId(), SensConstant.BUSINESS_NO_3, urlType)) {
//            return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_NO_PC_SUBMIT_RIGHT);
//        }
//        String account = user.getAccount();
//        String msgCode = null; // 消息编码
//        Object returnType = "ok"; // 非保存
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Boolean is_submit = false;//是否提交
//        Boolean workflowResult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> work = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workDetail = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        String subWorkCode = paramMap.get("sub_work_code").toString();
//        //给流程的key
//        Map map = new HashMap();
//        map.put("sub_work_code", subWorkCode);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workRequestCode = null;
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) work.get("work_code"));
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + ButtonConstant.BTN_SUBMIT);
//        }
//
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        //巡检单状态
//        int status = StatusConstant.PENDING;
//
//        // 页面【开始时间】为空时，自动赋值
//        if (null == workDetail.get("begin_time") || "".equals(workDetail.get("begin_time"))) {
//            workDetail.put("varchar@begin_time", "begin_time");
//            workDetail.remove("null@begin_time");
//            workDetail.put("begin_time", now);
//        }
//
//        // 第一条处理的时候，更新主工单开始时间
//        String workCode = (String) work.get("work_code");
//        this.updateMainDetailBeginTime(schema_name, workCode);
//
//        //提交按钮
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_SUBMIT + ""))) {
//            is_submit = true;//提交事件
//            workDetail.put("varchar@finished_time", "finished_time");
//            workDetail.put("finished_time", now);
//            status = StatusConstant.COMPLETED;
////            status = StatusConstant.TO_BE_AUDITED;
////            String content = "";
////            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {//韵达短信发送平台
////                content = MessageFormat.format(SensConstant.CONFIRM_INFO_WORK, user.getUsername(), (String) work.get("title"));
////            }else {//腾讯短信发送平台
////                String pattern = "{0,number,#}::{1}::{2}";
////                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_CONFIRM_INFO_WORK, user.getUsername(), (String) work.get("title"));
////            }
////            map.put("create_user_account", account);//发送短信人
////            map.put("receive_account", null);
//        }
//        //保存按钮
//        else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_SAVE + ""))) {
//            workDetail.put("finished_time", null);
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnType = returnContent; // 保存
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_FAULT_CODE) + "：S404");
//        }
//
//        //处理概要表和详情表状态
////        work.put("status", status);
//        workDetail.put("status", status);
//
//        //重新绑定值
//        dataInfo.put("_sc_works", work);
//        dataInfo.put("_sc_works_detail", workDetail);
//        map_object.put("dataInfo", dataInfo);
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            workSheetHandleService.insertWorkReceiveGroup(schema_name, subWorkCode, account); // 工单负责人组处理
//            //保存工单数据成功，调用工作流，启动巡检流程
//            map.put("facility_id", String.valueOf(work.get("facility_id"))); // 所属组织
//            map.put("position_code", work.get("position_code")); // 所属位置
//            // 没有未处理的子工单的时，发送确认短信（流程会判断是否有确认流程、要不要发短信）
//            String workRequestType = (String) map_object.get("work_request_type");
//            int waitWorkCount = workSheetHandleService.countWaitWorkByWorkCode(schema_name, workCode);
//            if (waitWorkCount == 0 && RegexUtil.isNotNull(workRequestType) && "inspectionConfirm".equals(workRequestType)) {
//                msgCode = qcloudsmsConfig.SMS_10003010;
//                map.put("create_user_account", account);//发送短信人
//                String receiver = null;
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receiver = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(receiver) && RegexUtil.isNotNull(workRequestCode)) {
//                        receiver = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                receiver = commonUtilService.getRandomUserByInfoForWork(schema_name, receiver, map, map_object);
//                map.put("receive_account", receiver);//接受人
//            }
//            map.put("status", String.valueOf(status));
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowResult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            // 发送短信
//            if (RegexUtil.isNotNull(msgReceiveAccount) && RegexUtil.isNotNull(msgCode)) {
//                // 主明细数据
//                Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode);
//                String mainSubWorkCode = (String) mainDetailInfo.get("sub_work_code");
//                messageService.msgPreProcess(msgCode, msgReceiveAccount, SensConstant.BUSINESS_NO_3, mainSubWorkCode,
//                        account, user.getUsername(), (String) work.get("title"), null); // 发送短信
//            }
//            //记录工单处理过程
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_3), subWorkCode, selectOptionService.getLanguageInfo(LangConstant.LOG_SUBMIT_INS), account);//记录历史
//
//            // 维修上报
//            try {
//                List<Map<String, Object>> deviceCodes = (List<Map<String, Object>>) map_object.get("deviceCodes");
//                if (null != deviceCodes && deviceCodes.size() > 0) {
//                    this.repairWorkData(schema_name, account, user.getUsername(), null, subWorkCode, map_object, paramMap, work, null, deviceCodes);
//                }
//            } catch (Exception dce) {
//                logger.info(selectOptionService.getLanguageInfo(LangConstant.LOG_INS_REP_UNTREATED) + subWorkCode + dce.getMessage());
//            }
//        } else {
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_3), subWorkCode, selectOptionService.getLanguageInfo(LangConstant.LOG_SAVE_MA_RESULT), account);//记录历史
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 巡检任务确认
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel auditInspectioncheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String msgCode = null; // 消息编码
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String logWord = selectOptionService.getLanguageInfo(LangConstant.LOG_COM_INS_FINISH);//完成巡检确认
//
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> work = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workDetail = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        String subWorkCode = paramMap.get("sub_work_code").toString();
//        String account = user.getAccount();
//        String remark = (String) workDetail.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        //给流程的key
//        Map map = new HashMap();
////        map.put("title_page", work.get("title"));
//        map.put("sub_work_code", subWorkCode);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        Boolean workflowresult = false;//流程提交是否成功
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        //巡检单状态
//        int status = StatusConstant.PENDING;
//        String workRequestCode = null;
//
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) work.get("work_code"));
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//        //同意按钮
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_AGREE + ""))) {
////            workDetail.put("varchar@audit_time", "audit_time"); 流程回调函数中有处理
////            workDetail.put("audit_time", now);
//            status = StatusConstant.COMPLETED;
//        }
//        //退回按钮
//        else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_RETURN + ""))) {
////            workDetail.put("varchar@finished_time", "finished_time"); 流程回调函数中有处理
////            workDetail.put("finished_time", null);
//            map_object.put("subReturnStatus", StatusConstant.PENDING);
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            map.put("create_user_account", account);//发送短信人
//            String receive_account = (String) workDetail.get("receive_account");
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
//            map.put("receive_account", receive_account);
////            try {
////                Map<String, Object> roleInfo = (Map) map_object.get("roleInfo");
////                if (roleInfo.containsKey("mergeRole")) {
////                    map_object.put("roleIds", roleInfo.get("mergeRole"));
////                }
////            } catch (Exception roleExp) {
////            }
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////            String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
//            logWord = selectOptionService.getLanguageInfo(LangConstant.MSG_BACK_RE_INSPECT);//退回了巡检结果，需重新巡检
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_FAULT_CODE) + "：S405");//页面数据有问题，问题代码
//        }
////        map.put("roleIds", map_object.get("roleIds"));
//        map.put("do_flow_key", do_flow_key);
//        //处理概要表和详情表状态
//        work.put("status", status);
//        workDetail.put("status", status);
//
//        //重新绑定值
//        dataInfo.put("_sc_works", work);
//        dataInfo.put("_sc_works_detail", workDetail);
//        map_object.put("dataInfo", dataInfo);
//        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        map.put("status", String.valueOf(status));
//        //保存工单数据成功，调用工作流，启动巡检流程
//        map.put("facility_id", String.valueOf(work.get("facility_id"))); // 所属位置
//        String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//        dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//        workflowresult = workflowService.complete(schema_name, taskId, account, map);
//        if (!workflowresult) {
//            throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//        }
//        String msgReceiveAccount = (String) map.get("receive_account");
//        // 发送短信
//        if (RegexUtil.isNotNull(msgReceiveAccount) && RegexUtil.isNotNull(msgCode)) {
//            // 主明细数据
//            Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode);
//            String mainSubWorkCode = (String) mainDetailInfo.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, SensConstant.BUSINESS_NO_3, mainSubWorkCode,
//                    account, user.getUsername(), (String) work.get("title"), null); // 发送短信
//        }
//        //记录工单处理过程
//        workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_3), subWorkCode, logWord + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 点检任务完成提交
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel finishedSpotcheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String urlType = (String) paramMap.get("pc_submit_right");
//        if (!checkExecuteType(schema_name, user.getId(), SensConstant.BUSINESS_NO_4, urlType)) {
//            return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_NO_PC_SUBMIT_RIGHT);
//        }
//        String account = user.getAccount();
//        String msgCode = null; // 消息编码
//        Object returnType = "ok"; // 非保存
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Boolean is_submit = false;//是否提交
//        Boolean workflowResult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> work = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workDetail = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        String subWorkCode = paramMap.get("sub_work_code").toString();
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workRequestCode = null;
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) work.get("work_code"));
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + ButtonConstant.BTN_SUBMIT);
//        }
//        //给流程的key
//        Map map = new HashMap();
//        map.put("sub_work_code", subWorkCode);
//
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        //点检单状态
//        int status = StatusConstant.PENDING;
//
//        // 页面【开始时间】为空时，自动赋值
//        if (null == workDetail.get("begin_time") || "".equals(workDetail.get("begin_time"))) {
//            workDetail.put("varchar@begin_time", "begin_time");
//            workDetail.remove("null@begin_time");
//            workDetail.put("begin_time", now);
//        }
//
//        // 第一条处理的时候，更新主工单开始时间
//        String workCode = (String) work.get("work_code");
//        this.updateMainDetailBeginTime(schema_name, workCode);
//
//        //提交按钮
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_SUBMIT + ""))) {
//            is_submit = true;//提交事件
//            workDetail.put("varchar@finished_time", "finished_time");
//            workDetail.put("finished_time", now);
//            status = StatusConstant.COMPLETED;
////            status = StatusConstant.TO_BE_AUDITED;
////            String content = "";
////            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {//韵达短信发送平台
////                content = MessageFormat.format(SensConstant.CONFIRM_INFO_WORK, user.getUsername(), (String) work.get("title"));
////            }else {//腾讯短信发送平台
////                String pattern = "{0,number,#}::{1}::{2}";
////                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_CONFIRM_INFO_WORK, user.getUsername(), (String) work.get("title"));
////            }
//            map.put("create_user_account", account);//发送短信人
//        }
//        //保存按钮
//        else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_SAVE + ""))) {
//            workDetail.put("finished_time", null);
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnType = returnContent; // 保存
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_FAULT_CODE) + "：S405");//页面数据有问题，问题代码
//        }
//
//        //处理概要表和详情表状态
////        work.put("status", status);
//        workDetail.put("status", status);
//
//        //重新绑定值
//        dataInfo.put("_sc_works", work);
//        dataInfo.put("_sc_works_detail", workDetail);
//        map_object.put("dataInfo", dataInfo);
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            workSheetHandleService.insertWorkReceiveGroup(schema_name, subWorkCode, account); // 工单负责人组处理
//            map.put("facility_id", String.valueOf(work.get("facility_id"))); // 所属组织
//            map.put("position_code", String.valueOf(work.get("position_code"))); // 所属位置
//            // 没有未处理的子工单的时，发送确认短信（流程会判断是否有确认流程、要不要发短信）
//            String workRequestType = (String) map_object.get("work_request_type");
//            int waitWorkCount = workSheetHandleService.countWaitWorkByWorkCode(schema_name, workCode);
//            if (waitWorkCount == 0 && RegexUtil.isNotNull(workRequestType) && "spotConfirm".equals(workRequestType)) {
//                msgCode = qcloudsmsConfig.SMS_10003010;
//                map.put("create_user_account", account);//发送短信人
//                String receiver = null;
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receiver = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(receiver) && RegexUtil.isNotNull(workRequestCode)) {
//                        receiver = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                receiver = commonUtilService.getRandomUserByInfoForWork(schema_name, receiver, map, map_object);
//                map.put("receive_account", receiver);//接受人
//
//            }
//            map.put("status", String.valueOf(status));
//            //保存工单数据成功，调用工作流，启动点检流程
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            workflowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workflowResult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            // 发送短信
//            if (RegexUtil.isNotNull(msgReceiveAccount) && RegexUtil.isNotNull(msgCode)) {
//                // 主明细数据
//                Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode);
//                String mainSubWorkCode = (String) mainDetailInfo.get("sub_work_code");
//                messageService.msgPreProcess(msgCode, msgReceiveAccount, SensConstant.BUSINESS_NO_4, mainSubWorkCode,
//                        account, user.getUsername(), (String) work.get("title"), null); // 发送短信
//            }
//            //记录工单处理过程
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_4), subWorkCode, selectOptionService.getLanguageInfo(LangConstant.LOG_SUBMIT_SPOT), account);//记录历史 提交了点检任务结果
//
//            // 维修上报
//            try {
//                List<Map<String, Object>> deviceCodes = (List<Map<String, Object>>) map_object.get("deviceCodes");
//                if (null != deviceCodes && deviceCodes.size() > 0) {
//                    this.repairWorkData(schema_name, account, user.getUsername(), null, subWorkCode, map_object, paramMap, work, null, deviceCodes);
//                }
//            } catch (Exception dce) {
//                logger.info(selectOptionService.getLanguageInfo(LangConstant.LOG_INS_REP_UNTREATED) + subWorkCode + dce.getMessage());
//            }
//        } else {
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_4), subWorkCode, selectOptionService.getLanguageInfo(LangConstant.LOG_SAVE_SPOT_REL), account);//记录历史 保存了一次点检任务结果
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 点检任务确认
//     *
//     * @param schema_name
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel auditSpotcheck(String schema_name, String taskId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String msgCode = null; // 消息编码
//        String doType = selectOptionService.getLanguageInfo(LangConstant.POINT_INSPECTION);//点检
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Boolean workflowResult = false;//流程提交是否成功
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> work = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workDetail = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        String subWorkCode = paramMap.get("sub_work_code").toString();
//        String account = user.getAccount();
//        String remark = (String) workDetail.get("remark");
//        remark = (null == remark || remark.isEmpty()) ? "" : "：" + remark;
//        //给流程的key
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(work.get("facility_id"))); // 所属组织
//        map.put("position_code", work.get("position_code")); // 所属位置
////        map.put("title_page", work.get("title"));
//        map.put("sub_work_code", subWorkCode);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, subWorkCode);
//        String workRequestCode = null;
//        String logWord = selectOptionService.getLanguageInfo(LangConstant.LOG_CONF_SPOT_REL);//确认点检任务结果
//        int status = StatusConstant.PENDING;
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) work.get("work_code"));
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//
//        //同意按钮
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_AGREE + ""))) {
////            workDetail.put("varchar@audit_time", "audit_time"); 流程回调函数中有处理
////            workDetail.put("audit_time", now);
//            status = StatusConstant.COMPLETED;
//            String nextStatus = (String) map_object.get("next_status");
//            if (null != nextStatus && !"".equals(nextStatus)) {
//                status = Integer.valueOf(nextStatus);
//            }
//            if (StatusConstant.COMPLETED != status) {
//                String receive_account = (String) map_object.get("next_operator");
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                    if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                        receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                    }
//                }
//                receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//                map.put("receive_account", receive_account);//接收人账号
//                msgCode = qcloudsmsConfig.SMS_10003010;
//                if (RegexUtil.isNotNull(receive_account) && !"-1".equals(receive_account)) {
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                    logWord = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + "，" + selectOptionService.getLanguageInfo(LangConstant.WM_DWP) + "：" + receiveAccountName;//确认了 任务，处理人：
//                }
//            }
//        }
//        //退回按钮
//        else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals((ButtonConstant.BTN_RETURN + ""))) {
////            workDetail.put("varchar@finished_time", "finished_time"); 流程回调函数中有处理
////            workDetail.put("finished_time", null);
//            map_object.put("subReturnStatus", status);
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            map.put("create_user_account", account);//发送短信人
//            String receive_account = (String) workDetail.get("receive_account");
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, subWorkCode, nextOperatorTask);
//                if (RegexUtil.isNull(receive_account) && RegexUtil.isNotNull(workRequestCode)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//            }
//            map.put("receive_account", receive_account);
////            try {
////                Map<String, Object> roleInfo = (Map) map_object.get("roleInfo");
////                if (roleInfo.containsKey("mergeRole")) {
////                    map_object.put("roleIds", roleInfo.get("mergeRole"));
////                }
////            } catch (Exception roleExp) {
////            }
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////            String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
//            logWord = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK_RE_SPOT);//退回了点检结果，需重新点检
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_FAULT_CODE) + "：S505");//页面数据有问题，问题代码
//        }
////        map.put("roleIds", map_object.get("roleIds"));
//        map.put("do_flow_key", do_flow_key);
//        //处理概要表和详情表状态
//        work.put("status", status);
//        workDetail.put("status", status);
//
//        //重新绑定值
//        dataInfo.put("_sc_works", work);
//        dataInfo.put("_sc_works_detail", workDetail);
//        map_object.put("dataInfo", dataInfo);
//        String workCode = (String) map_object.get("work_code");
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        map.put("status", String.valueOf(status));
//        //保存工单数据成功，调用工作流，启动点检流程
//        String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//        dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//        workflowResult = workflowService.complete(schema_name, taskId, account, map);
//        if (!workflowResult) {
//            throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//        }
//        String msgReceiveAccount = (String) map.get("receive_account");
//        // 发送短信
//        if (RegexUtil.isNotNull(msgReceiveAccount) && RegexUtil.isNotNull(msgCode)) {
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, SensConstant.BUSINESS_NO_4, msgSubWorkCode,
//                    account, user.getUsername(), (String) work.get("title"), null); // 发送短信
//        }
//        //记录工单处理过程
//        workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, taskSid);//工单进度记录
//        logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_4), subWorkCode, logWord + remark, account);//记录历史
//        return ResponseModel.ok("ok");
////        } catch (Exception exp) {
////            workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 分配事件
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel distribution(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String account = user.getAccount();
//        String userAssignType = (String) paramMap.get("user_assign_type");
//        // 工单问题上报、工单处理负责人
//        if (null == userAssignType || !"flowNextOperatorAssign".equals(userAssignType)) {
//            return this.distributionWork(schema_name, paramMap, request, account, user.getUsername());
//        }
//        // 工单请求、下一步操作人
//        if ("flowNextOperatorAssign".equals(userAssignType)) {
//            return this.distributionRequest(schema_name, paramMap, account, user.getUsername());
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.LOG_ERROR_IN_WORKSHEET));//当前工单信息有错误
//    }
//
//    /**
//     * 分配事件（工单）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel distributionWork(String schema_name, Map<String, Object> paramMap, HttpServletRequest request, String account, String userName) throws Exception {
//        String pageTmpType = (String) paramMap.get("pageTmpType");
//        String receive_account = (String) paramMap.get("next_operator");
//        if (null == receive_account || "-1".equals(receive_account)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PL_DIST_MAN));//请分配一个负责人
//        }
//        String customerIdStr = (String) paramMap.get("customer_id");
//        Integer customerId = null;
//        if (RegexUtil.isNotNull(customerIdStr)) {
//            customerId = Integer.valueOf(customerIdStr); // 委外服务商
//        }
//
//        String subWorkCode = (String) paramMap.get("sub_work_code");
//        if (null == subWorkCode || subWorkCode.isEmpty()) {
//            return null;
//        }
//        String workCode = null;
//        Map<String, Object> dtlInfo = workSheetHandleService.queryBusinessDetailById(schema_name, subWorkCode); // 明细数据
//        Map<String, Object> workInfo = null; // 主数据
//        Map<String, Object> mainDetailInfo = null; // 主明细数据
//        Timestamp receiveTime = new Timestamp(System.currentTimeMillis());
//        Timestamp deadlineTime = null;
//        if (RegexUtil.isNotNull(pageTmpType)) {
//            if ("workPool".equals(pageTmpType)) {
//                String time = request.getParameter("time");
//                if (RegexUtil.isNotNull(time)) {
//                    deadlineTime = Timestamp.valueOf(time + " 23:59:59");
//                }
//                if (null == dtlInfo || dtlInfo.size() == 0) {
//                    workCode = (String) paramMap.get("sub_work_code");
//                    workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
//                    mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode);
//                    subWorkCode = (String) mainDetailInfo.get("sub_work_code");
//                } else {
//                    workCode = (String) dtlInfo.get("work_code");
//                    workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
//                }
//            }
//        } else {
//            workCode = (String) dtlInfo.get("work_code");
//            workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 主数据
//        }
//
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            if (StatusConstant.TO_BE_CONFIRM > (Integer) workInfo.get("status") ||
//                    StatusConstant.PENDING < (Integer) workInfo.get("status")) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            request.setAttribute("work_code", workCode);
//            if ("".equals(workSheetService.findWorkSheetList(request))) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//            }
//        } catch (Exception e) {
//            logger.info(selectOptionService.getLanguageInfo(LangConstant.WO_DISTRIBUTION) + subWorkCode);//工单分配
//        }
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//        String log = selectOptionService.getLanguageInfo(LangConstant.DISTRIBUTION_A) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + selectOptionService.getLanguageInfo(LangConstant.LOG_GIVE) + receiveAccountName;
//        // 保存工单主表，更新状态 "分配了" + doType + "任务，处理人：" + receiveAccountName;
//        Map<String, Object> dataInfo = new HashMap<String, Object>();
//        dataInfo.put("work_code", workCode);
//        Integer status = (Integer) workInfo.get("status");
//        String userAssignType = (String) paramMap.get("user_assign_type");
//        // 处理中时只更新负责人
//        if (StatusConstant.UNDISTRIBUTED == status) {
//            status = StatusConstant.PENDING;
//            String nextStatus = (String) paramMap.get("next_status");
//            if (null != nextStatus && !"".equals(nextStatus)) {
//                status = Integer.valueOf(nextStatus);
//            }
//            dataInfo.put("int@status", "status");
//            dataInfo.put("status", status);//保存状态
//            if (RegexUtil.isNotNull(customerId)) {
//                dataInfo.put("int@customer_id", customerId);
//                dataInfo.put("customer_id", customerId);
//            }
//            if (null != deadlineTime) {
//                dataInfo.put("varchar@deadline_time", "deadline_time");
//                dataInfo.put("deadline_time", deadlineTime);
//            }
//            workSheetHandleService.updateWorkByWorkCode(schema_name, dataInfo);
//            // 工单问题上报、工单处理负责人
//            if (null == userAssignType || "workHandleAssign".equals(userAssignType)) {
//                dataInfo.remove("customer_id");
//                dataInfo.remove("int@customer_id");
//                dataInfo.remove("deadline_time");
//                dataInfo.remove("varchar@deadline_time");
//                // 保存工单明细表，更新分配人、状态、时间等信息
//                dataInfo.put("varchar@receive_time", "receive_time");
//                dataInfo.put("receive_time", receiveTime);
//                dataInfo.put("varchar@distribute_time", "distribute_time");
//                dataInfo.put("distribute_time", receiveTime);
//                dataInfo.put("varchar@distribute_account", "distribute_account");
//                dataInfo.put("distribute_account", account);
//            }
//            workSheetHandleService.updateDetailByWorkCode(schema_name, dataInfo, "work_code");
//        } else {
//            if (RegexUtil.isNotNull(customerId)) {
//                dataInfo.put("int@customer_id", customerId);
//                dataInfo.put("customer_id", customerId);
//                workSheetHandleService.updateWorkByWorkCode(schema_name, dataInfo);
//            }
//        }
//
//        // 工单问题上报、工单处理负责人
//        if (null == userAssignType || "workHandleAssign".equals(userAssignType)) {
//            if ("workPool".equals(pageTmpType)) {
//                if (null != deadlineTime) {
//                    dataInfo.clear();
//                    dataInfo.put("work_code", workCode);
//                    dataInfo.put("varchar@deadline_time", "deadline_time");
//                    dataInfo.put("deadline_time", deadlineTime);
//                    workSheetHandleService.updateWorkByWorkCode(schema_name, dataInfo);
//                }
//                dataInfo.remove("deadline_time");
//                dataInfo.remove("varchar@deadline_time");
//                dataInfo.put("varchar@receive_time", "receive_time");
//                dataInfo.put("receive_time", receiveTime);
//                dataInfo.put("varchar@distribute_time", "distribute_time");
//                dataInfo.put("distribute_time", receiveTime);
//                dataInfo.put("varchar@receive_account", "distribute_account");
//                dataInfo.put("distribute_account", account);
//                workSheetHandleService.updateDetailByWorkCode(schema_name, dataInfo, "work_code");
//            } // 保存工单明细表，更新分配人等信息
//            workSheetHandleService.updateWaitWorkDetailByWorkCode(schema_name, receive_account, workCode);
//        }
//
//        String title = (String) workInfo.get("title");
//        Map map = new HashMap();
//        map.put("receive_account", receive_account); // 短信接收人
//        map.put("status", String.valueOf(status));
//        map.put("create_user_account", account);//发送短信人
//        map.put("businessType", businessTypeId);//发送短信内容
//        map.put("deadline_time", String.valueOf(workInfo.get("deadline_time")));
//        map.put("facility_id", String.valueOf(workInfo.get("facility_id"))); // 所属位置
//        Boolean isSuccess = false;
//        // 明细表所有数据
//        List<Map<String, Object>> dataList = workSheetHandleService.querySubWorkCodeListByWorkCode(schema_name, workCode);
//        if (null != dataList && dataList.size() > 1) {
//            if (null == mainDetailInfo || mainDetailInfo.size() == 0) {
//                mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode); // 主明细数据
//            }
//            subWorkCode = (String) mainDetailInfo.get("sub_work_code");
//            List<String> subWorkCodes = new ArrayList<String>();
//            for (Map<String, Object> codes : dataList) {
//                Integer tmpStatus = (Integer) codes.get("status");
//                if (StatusConstant.COMPLETED != tmpStatus) {
//                    subWorkCodes.add(codes.get("sub_work_code").toString());
//                }
//            }
//            subWorkCodes.remove(subWorkCode);
//            map.put("work_code", subWorkCode); // 发短信用
//            isSuccess = workflowService.parentAssign(schema_name, subWorkCodes, receive_account, map); // receive_account：被分配人
//        } else {
//            map.put("sub_work_code", subWorkCode);
//            isSuccess = workflowService.assignWithSWC(schema_name, subWorkCode, receive_account, map);
//        }
//        if (!isSuccess) {
//            throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//        }
//        messageService.msgPreProcess(qcloudsmsConfig.SMS_10003007, receive_account, businessTypeId, subWorkCode,
//                account, userName, title, doType); // 发送短信
//        logService.AddLog(schema_name, pageType, subWorkCode, log, account);//记录历史
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 分配事件（工单请求）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param account
//     * @param userName
//     * @return
//     */
//    public ResponseModel distributionRequest(String schema_name, Map<String, Object> paramMap, String account, String userName) {
//        String nextOperator = (String) paramMap.get("next_operator");
//        if (null == nextOperator || "-1".equals(nextOperator)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PL_DIST_MAN));//请分配一个负责人
//        }
//        String subWorkCode = (String) paramMap.get("subWorkCode");
//        // 根据数据库中状态判断是否为重复操作
////        try {
////            if ("".equals(workSheetRequestService.findWsqList(request))) {
////                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
////            }
////        } catch (Exception e) {
////            logger.info(assignCode + "：" + do_flow_key);
////        }
//        String workTypeId = (String) paramMap.get("work_type_id");
//        String title = (String) paramMap.get("title");
//        Map map = new HashMap();
//        map.put("receive_account", nextOperator); // 短信接收人
//        map.put("create_user_account", account);//发送短信人
////        map.put("facility_id", facilityId); // 所属位置
//        String businessTypeId = (String) paramMap.get("business_no");
//        if (RegexUtil.isNotNull(workTypeId)) {
//            businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        }
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        if (RegexUtil.isNull(title)) {
//            title = doType;
//        }
//        String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", nextOperator, null);
//        String log = selectOptionService.getLanguageInfo(LangConstant.DISTRIBUTION_A) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + selectOptionService.getLanguageInfo(LangConstant.LOG_GIVE) + receiveAccountName;
//        Boolean isSuccess = workflowService.assignWithSWC(schema_name, subWorkCode, nextOperator, map);
//        if (!isSuccess) {
//            return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//        }
//        String msgCode = qcloudsmsConfig.SMS_10003008;
//        if (subWorkCode.startsWith("WR")) {
//            msgCode = qcloudsmsConfig.SMS_10003001;
//        } else if (SensConstant.BUSINESS_NO_41.equals(businessTypeId)) {
//            msgCode = qcloudsmsConfig.SMS_10002003;
//        } else if (Integer.valueOf(businessTypeId) > 20) {
//            msgCode = qcloudsmsConfig.SMS_10002007;
//        }
//        messageService.msgPreProcess(msgCode, nextOperator, businessTypeId, subWorkCode,
//                account, userName, title, doType); // 发送短信
//        logService.AddLog(schema_name, pageType, subWorkCode, log, account);//记录历史
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 工单请求确认
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel confirmWsq(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//
//        String msgCode = null; // 消息编码
//        String account = user.getAccount();
//        Object returnType = "ok"; // 非保存
//
//        Boolean is_submit = false;//是否提交
//        String taskId = request.getParameter("task_id");//taskId
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> worksTmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        Object facilityId = worksTmp.get("facility_id");
//        String workRequestCode = (String) worksTmp.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, workRequestCode);
//        String workAssignType = (String) map_object.get("work_assign_type");
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workRequestInfo = workSheetRequestService.queryWsqInfoById(schema_name, workRequestCode);
//        String workTypeId = String.valueOf(workRequestInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_T) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);
//
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        String title = (String) worksTmp.get("title");
//        map.put("title_page", title);
//        String subWorkCode = (String) worksDetailTmp.get("sub_work_code");
//        String workCode = (String) worksTmp.get("work_code");
//        map.put("sub_work_code", subWorkCode);
//        String receive_account = (String) worksDetailTmp.get("receive_account");
//        String relationId = (String) worksDetailTmp.get("relation_id"); // 对象
//        int status = StatusConstant.TO_BE_CONFIRM;//状态
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workCode", workCode);
//            returnContent.put("subWorkCode", subWorkCode);
//            returnContent.put("workRequestCode", workRequestCode);
//            returnType = returnContent; // 保存
//            workRequestTmp.remove("int@status");
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            // 创建工单
//            if ("1".equals(workAssignType)) {
//                workRequestTmp.put("status", StatusConstant.PENDING);
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//                String nextStatus = (String) map_object.get("next_status");
//                // 处理接收人和接受时间
//                if (!StringUtil.isEmpty(receive_account) && !"-1".equals(receive_account)) {
//                    status = StatusConstant.PENDING;//保存状态
//                    if (null != nextStatus && !"".equals(nextStatus)) {
//                        status = Integer.valueOf(nextStatus);
//                    }
//                    map.put("receive_account", receive_account);
//                    worksDetailTmp.put("varchar@receive_time", "receive_time");
//                    worksDetailTmp.put("receive_time", now);
//                    worksDetailTmp.put("varchar@distribute_time", "distribute_time");
//                    worksDetailTmp.put("distribute_time", now);
//                    worksDetailTmp.put("varchar@distribute_account", "distribute_account");
//                    worksDetailTmp.put("distribute_account", account);
//                    msgCode = qcloudsmsConfig.SMS_10003007;
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                    // 维修时
//                    logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_T) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;//分配 成功，负责人
//                } else {
//                    msgCode = qcloudsmsConfig.SMS_10003005;
//                    status = StatusConstant.UNDISTRIBUTED;//无分配人下一步开始分配
//                    if (null != nextStatus && !"".equals(nextStatus)) {
//                        status = Integer.valueOf(nextStatus);
//                    }
//                    receive_account = (String) map_object.get("next_operator");
//                    if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                        receive_account = null;
//                    }
//                    map.put("receive_account", receive_account);
//                }
//            } else if ("2".equals(workAssignType)) {
//                // 完成关闭
//                do_flow_key = String.valueOf(ButtonConstant.BTN_CLOSE);
//                status = StatusConstant.OFFLINE_COMPLETED_CLOSE;
//                workRequestTmp.put("status", status);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CLOSE_C) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//关闭了 任务
//                msgCode = qcloudsmsConfig.SMS_10003004;
//                map.put("receive_account", String.valueOf(workRequestInfo.get("create_user_account")));//接收人账号
//            } else if ("3".equals(workAssignType)) {
//                // 无效关闭
//                do_flow_key = String.valueOf(ButtonConstant.BTN_CLOSE);
//                status = StatusConstant.INVALID_CLOSE;
//                workRequestTmp.put("status", status);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CLOSE_C) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//关闭了 任务
//                msgCode = qcloudsmsConfig.SMS_10003004;
//                map.put("receive_account", String.valueOf(workRequestInfo.get("create_user_account")));//接收人账号
//            }
//            map.put("businessType", businessTypeId);     //工单
//            map.put("deadline_time", worksTmp.get("deadline_time"));
//            map.put("status", String.valueOf(status));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//            map.put("create_user_account", account);//发送短信人
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 页面【发生时间】为空时，自动赋值
//        if (worksTmp.containsKey("occur_time")) {
//            if (null == worksTmp.get("occur_time") || "".equals(worksTmp.get("occur_time"))) {
//                worksTmp.put("varchar@occur_time", "occur_time");
//                worksTmp.remove("null@occur_time");
//                worksTmp.put("occur_time", now);
//            } else {
//                map.put("create_time", worksTmp.get("occur_time"));
//            }
//        }
//        worksTmp.put("status", status);
//        worksTmp.put("create_user_account", workRequestInfo.get("create_user_account"));
//        //处理工单表的创建人和创建时间
//        worksTmp.put("varchar@create_time", "create_time");
//        worksTmp.put("create_time", now);
//        worksDetailTmp.put("status", status);//无分配人下一步开始分配
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//        if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//            try {
//                Integer fid = Integer.valueOf((String) facilityId);
//            } catch (Exception fidExp) {
//                worksTmp.put("position_code", facilityId);
//                worksTmp.remove("facility_id");
//                workRequestTmp.put("position_code", facilityId);
//                workRequestTmp.remove("facility_id");
//            }
//        }
//        // 重新绑定值
//        if (null != do_flow_key && !"".equals(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//            dataInfo.put("_sc_works", worksTmp);
//            dataInfo.put("_sc_works_detail", worksDetailTmp);
//        } else {
//            Map<String, String> keys = new HashMap<String, String>();
//            keys.put("_sc_work_request", "work_request_code");
//            map_object.put("keys", keys);
//            dataInfo = new HashMap<String, Object>();
//            dataInfo.put("_sc_work_request", workRequestTmp);
//        }
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("saveSubType", "request");
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", worksTmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", worksTmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        String flow_data = workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            // 多个对象
//            if (RegexUtil.isNotNull(relationId)) {
//                if ("1".equals(workAssignType)) {
//                    workSheetHandleService.insertWorkAssetOrg(schema_name, subWorkCode, relationId); // 工单设备组织处理
//                }
//                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
//                String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
//                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
//                    map.put("categoryId", categoryIdStr);
//                }
//            }
//            if (StringUtil.isNotEmpty(receive_account) && StringUtil.isNotEmpty(flow_data)) {
//                map.put("flow_data", flow_data);
//            }
//            map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//            map.put("do_flow_key", do_flow_key);
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, workRequestCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, workRequestCode, map_object);
//            boolean workFlowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workFlowResult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                    account, user.getUsername(), title, doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, workRequestCode, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
////        } else {
////            logOperation = "进行了一次" + doType + "保存";
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workInfo && null != dtlInfo) {
////                workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            }
////            if (null != workRequestInfo) {
////                workSheetRequestService.doRollBackData(schema_name, workRequestCode, workRequestInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        if (null != map_object && map_object.size() > 0) {
//            String workRequestType = (String) map_object.get("work_request_type");
//            if (null == workRequestType || !"confirm".equals(workRequestType)) {
//                return this.saveWorkData(schema_name, processDefinitionId, map_object, do_flow_key, user);
//            }
//            if ("confirm".equals(workRequestType)) {
//                return this.saveWorkRequest(schema_name, processDefinitionId, map_object, do_flow_key, user, workRequestType);
//            }
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.LOG_ERROR_IN_WORKSHEET));//当前工单信息有错误
//    }
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param map_object
//     * @param do_flow_key
//     * @param user
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel saveWorkData(String schema_name, String processDefinitionId, Map<String, Object> map_object, String do_flow_key, User user) throws Exception {
//        String account = user.getAccount();
//        String accountName = user.getUsername();
//        Object returnType = "ok"; // 非保存
//
//        String msgCode = null; // 消息编码
//        Boolean is_submit = false;//是否提交
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        Object facilityId = works_tmp.get("facility_id");
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        String title = (String) works_tmp.get("title");
//        map.put("title_page", title);
//        map.put("work_request_type", (String) map_object.get("work_request_type"));
//        String subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        String workCode = (String) works_tmp.get("work_code");
//        try {
//            // 根据数据库中状态判断是否为重复操作
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
//            if ((Integer) workInfo.get("status") > StatusConstant.DRAFT || !account.equals(workInfo.get("create_user_account"))) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(works_detaile_tmp.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.NEW_K) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);
//        if (SensConstant.BUSINESS_NO_1.equals(businessTypeId)) {
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_ESCALATE_A_PROBLEM);//上报一个问题
//        }
//        map.put("sub_work_code", subWorkCode);
//        String receive_account = (String) map_object.get("next_operator");
//        if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//            receive_account = (String) works_detaile_tmp.get("receive_account");
//        }
//        if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//            receive_account = null;
//        }
//        String relationId = (String) works_detaile_tmp.get("relation_id"); // 对象
//        int status = StatusConstant.DRAFT;//状态
//        Integer nextStatus = null;
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//        if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//            try {
//                Integer fid = Integer.valueOf((String) facilityId);
//            } catch (Exception fidExp) {
//                works_tmp.put("position_code", facilityId);
//                works_tmp.remove("facility_id");
//            }
//        }
//        map.put("facility_id", String.valueOf(facilityId)); // 所属组织
//        map.put("position_code", works_tmp.get("position_code")); // 所属位置
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workCode", workCode);
//            returnContent.put("subWorkCode", subWorkCode);
//            returnType = returnContent; // 保存
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            try {
//                nextStatus = Integer.valueOf((String) map_object.get("next_status"));
//            } catch (Exception statusExp) {
//
//            }
//            // 处理接收人和接受时间
//            if (null != nextStatus || !StringUtil.isEmpty(receive_account) && !"-1".equals(receive_account)) {
//                if (null == nextStatus) {
//                    status = StatusConstant.PENDING;//保存状态
//                    works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                    works_detaile_tmp.put("receive_time", now);
//                    works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                    works_detaile_tmp.put("distribute_time", now);
//                    works_detaile_tmp.put("varchar@distribute_account", "distribute_account");
//                    works_detaile_tmp.put("distribute_account", account);
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                    // 维修时
//                    if (SensConstant.BUSINESS_NO_1.equals(businessTypeId)) {
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_ESCALATE_A_PROBLEM) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;
//                    } else {
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.NEW_K) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;
//                    }
//                    msgCode = qcloudsmsConfig.SMS_10003007;
//                } else {
//                    status = nextStatus;
//                    map.put("relation_id", works_detaile_tmp.get("relation_id"));
//                    receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//                    msgCode = qcloudsmsConfig.SMS_10003008;
//                }
//            } else {
//                msgCode = qcloudsmsConfig.SMS_10003005;
//                status = StatusConstant.UNDISTRIBUTED;//无分配人下一步开始分配
//            }
//            map.put("receive_account", receive_account);
//            map.put("create_user_account", account);//发送短信人
//            map.put("businessType", workTypeId);     //工单
//            map.put("deadline_time", works_tmp.get("deadline_time"));
//            map.put("status", String.valueOf(status));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 页面【发生时间】为空时，自动赋值
//        if (works_tmp.containsKey("occur_time")) {
//            if (null == works_tmp.get("occur_time") || "".equals(works_tmp.get("occur_time"))) {
//                works_tmp.put("varchar@occur_time", "occur_time");
//                works_tmp.remove("null@occur_time");
//                works_tmp.put("occur_time", now);
//            } else {
//                map.put("create_time", works_tmp.get("occur_time"));
//            }
//        }
//        works_tmp.put("status", status);
//        works_tmp.put("create_user_account", account);
//        //处理工单表的创建人和创建时间
//        works_tmp.put("varchar@create_time", "create_time");
//        works_tmp.put("create_time", now);
//        works_detaile_tmp.put("status", status);//无分配人下一步开始分配
//        // 重新绑定值
//        dataInfo.put("_sc_works", works_tmp);
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
//
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", works_tmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", works_tmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        String flow_data = workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        // 多个对象
//        if (RegexUtil.isNotNull(relationId)) {
//            workSheetHandleService.insertWorkAssetOrg(schema_name, subWorkCode, relationId); // 工单设备组织处理
//        }
//        if (StringUtil.isNotEmpty(flow_data)) {
//            map.put("flow_data", flow_data);
//            try {
//                Map<String, Object> roleInfo = (Map) map_object.get("roleInfo");
//                if (roleInfo.containsKey("mergeRole")) {
//                    map_object.put("roleIds", roleInfo.get("mergeRole"));
//                }
//            } catch (Exception roleExp) {
//            }
//        }
//        if (is_submit) {
//            this.saveWorkRequest(schema_name, processDefinitionId, map_object, do_flow_key, user, null);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//            if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                throw new SenscloudException(doType + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//流程启动失败
//            }
//            String isSendMsg = (String) map_object.get("isSendMsg");
//            if (!RegexUtil.optIsPresentStr(isSendMsg) || !"1".equals(isSendMsg)) {
//                String msgReceiveAccount = (String) map.get("receive_account");
//                String msgSubWorkCode = (String) map.get("sub_work_code");
//                messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                        account, accountName, title, doType); // 发送短信
//            }
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//            logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
////        } else {
////            logOperation = "进行了一次" + doType + "保存";
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workInfo && null != dtlInfo) {
////                workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 保存请求事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param map_object
//     * @param do_flow_key
//     * @param user
//     * @param workRequestType
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel saveWorkRequest(String schema_name, String processDefinitionId, Map<String, Object> map_object, String do_flow_key, User user, String workRequestType) throws Exception {
//        String account = user.getAccount();
//        String accountName = user.getUsername();
//        Object returnType = "ok"; // 非保存
//
//        String msgCode = null; // 消息编码
//        Boolean is_submit = false;//是否提交
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        String workRequestCode = null;
//        // 工单流程
//        if (null == workRequestType || !"confirm".equals(workRequestType)) {
//            Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//            Map<String, Object> worksTmp = (Map<String, Object>) dataInfo.get("_sc_works");
//            workRequestTmp = new HashMap<String, Object>();
//            workRequestTmp.putAll(worksTmp);
//            workRequestTmp.putAll(worksDetailTmp);
//            workRequestCode = dynamicCommonService.getWorkRequestCode(schema_name);
//            workRequestTmp.put("work_request_code", workRequestCode);
//            Map<String, Object> workInfo = new HashMap<String, Object>();
//            workInfo.put("varchar@work_request_code", "work_request_code");
//            workInfo.put("work_request_code", workRequestCode);
//            workInfo.put("work_code", worksTmp.get("work_code"));
//            workSheetHandleService.updateWorkByWorkCode(schema_name, workInfo);
//            map_object.put("saveSubType", "updateRequest");
//        } else {
//            workRequestCode = (String) workRequestTmp.get("work_request_code"); // 工单请求流程
//            map_object.put("saveSubType", "request");
//        }
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        map.put("work_request_type", workRequestType);
//        Object facilityId = workRequestTmp.get("facility_id");
//        SystemConfigData systemConfigData = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//        if (null == systemConfigData || RegexUtil.isNull(systemConfigData.getSettingValue()) || systemConfigData.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//            try {
//                Integer fid = Integer.valueOf((String) facilityId);
//            } catch (Exception fidExp) {
//                workRequestTmp.put("position_code", facilityId);
//                workRequestTmp.remove("facility_id");
//            }
//        }
//        String title = (String) workRequestTmp.get("title");
//        map.put("title_page", title);
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workRequestInfo = workSheetRequestService.queryWsqInfoById(schema_name, workRequestCode);
//            if ((Integer) workRequestInfo.get("status") > StatusConstant.DRAFT || !account.equals(workRequestInfo.get("create_user_account"))) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//        } catch (Exception e) {
//            logger.info(workRequestCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(workRequestTmp.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.NEW_K) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);//创建 成功
//        if (SensConstant.BUSINESS_NO_1.equals(businessTypeId)) {
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_ESCALATE_A_PROBLEM);//上报一个问题
//        }
//        map.put("sub_work_code", workRequestCode);
//        int status = StatusConstant.DRAFT;//状态
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            workRequestTmp.put("create_user_account", account);
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workRequestCode", workRequestCode);
//            returnType = returnContent; // 保存
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            status = StatusConstant.TO_BE_CONFIRM; // 提交状态
//            map.put("facility_id", facilityId); // 所属位置
//            map.put("position_code", workRequestTmp.get("position_code")); // 所属位置
//            String nextStatus = (String) map_object.get("next_status");
//            if (null != nextStatus && !"".equals(nextStatus)) {
//                status = Integer.valueOf(nextStatus);
//            }
//            msgCode = qcloudsmsConfig.SMS_10003001;
//            String receive_account = (String) map_object.get("next_operator");
//            if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                map.put("relation_id", (String) workRequestTmp.get("relation_id"));
//                receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//            }
//            map.put("receive_account", receive_account);
//            map.put("create_user_account", account);//发送短信人
//            map.put("businessType", workTypeId);     //工单
//            map.put("deadline_time", workRequestTmp.get("deadline_time"));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//            // 页面【发生时间】为空时，自动赋值
//            if (workRequestTmp.containsKey("occur_time")) {
//                if (null == workRequestTmp.get("occur_time") || "".equals(workRequestTmp.get("occur_time"))) {
//                    workRequestTmp.put("varchar@occur_time", "occur_time");
//                    workRequestTmp.remove("null@occur_time");
//                    workRequestTmp.put("occur_time", now);
//                } else {
//                    map.put("create_time", workRequestTmp.get("occur_time"));
//                }
//            }
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        if (null == workRequestType || !"confirm".equals(workRequestType)) {
//            status = StatusConstant.PENDING; // 保存状态
//        }
//        map.put("status", String.valueOf(status));
//        workRequestTmp.put("status", status);
//        workRequestTmp.put("create_user_account", account);
//        //处理工单表的创建人和创建时间
//        workRequestTmp.put("varchar@create_time", "create_time");
//        workRequestTmp.put("create_time", now);
//        // 重新绑定值
//        dataInfo.put("_sc_work_request", workRequestTmp);
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("work_request_code", workRequestCode);
//
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", workRequestTmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", workRequestTmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        try {
//        if (null == workRequestType || !"confirm".equals(workRequestType)) {
//            List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//            dataList.add(workRequestTmp);
//            selectOptionService.doBatchInsertSql(schema_name, "_sc_work_request", SqlConstant._sc_work_request_columns, dataList);
//            workSheetHandleService.updateWorkDutymanHourInfoByCode(schema_name, (String) map_object.get("subWorkCode"), workRequestCode);
//        } else {
//            workSheetHandleService.doSaveData(schema_name, map_object);
//        }
//        if (null != workRequestType && "confirm".equals(workRequestType) && is_submit) {
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, null, workRequestCode, map_object);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//            if (!workflowStartResult.isStarted()) {
//                throw new SenscloudException(doType + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//流程启动失败
//            }
//            String isSendMsg = (String) map_object.get("isSendMsg");
//            if (!RegexUtil.optIsPresentStr(isSendMsg) || !"1".equals(isSendMsg)) {
//                String msgReceiveAccount = (String) map.get("receive_account");
//                String msgSubWorkCode = (String) map.get("sub_work_code");
//                messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                        account, accountName, title, doType); // 发送短信
//            }
//            workProcessService.saveWorkProccess(schema_name, workRequestCode, status, account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workRequestInfo) {
////                workSheetRequestService.doRollBackData(schema_name, workRequestCode, workRequestInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
//
//
//    /**
//     * 小程序/PC执行节点权限验证
//     *
//     * @param schema_name
//     * @param userId
//     * @param parentType
//     * @param urlType
//     * @return
//     */
//    private boolean checkExecuteType(String schema_name, String userId, String parentType, String urlType) {
//        boolean executeRight = false;
//        // PC端时权限验证
//        if (null != urlType && !"".equals(urlType)) {
//            if (SensConstant.BUSINESS_NO_1.equals(parentType)) {
//                urlType = "repair_do_pc";
//                parentType = "repair";
//            }
//            if (SensConstant.BUSINESS_NO_2.equals(parentType)) {
//                urlType = "maintain_result_submit_pc";
//                parentType = "maintain";
//            }
//            if (SensConstant.BUSINESS_NO_3.equals(parentType)) {
//                urlType = "inspection_add_pc";
//                parentType = "inspection";
//            }
//            if (SensConstant.BUSINESS_NO_4.equals(parentType)) {
//                urlType = "spot_add_pc";
//                parentType = "spotcheck";
//            }
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, userId, parentType);
//            if (null != userFunctionList && userFunctionList.size() > 0) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    if (urlType.equals(functionData.getFunctionName())) {
//                        executeRight = true;
//                    }
//                }
//            }
//        } else {
//            executeRight = true;
//        }
//        return executeRight;
//    }
//
//
//    /**
//     * 更新字段值
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel doUpdateByData(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        String subWorkCode = (String) paramMap.get("subWorkCode");
//        Map<String, Object> dtlInfo = workSheetHandleService.queryBusinessDetailById(schema_name, subWorkCode); // 明细数据
//        String workCode = (String) dtlInfo.get("work_code");
//        request.setAttribute("work_code", workCode);
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode); // 主数据
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String account = user.getAccount();
////        Integer status = (Integer) workInfo.get("status");
//        String fieldCode = (String) paramMap.get("fieldCode");
////        // 根据数据库中状态判断是否为重复操作
////        try {
////            String createAccount = (String) workInfo.get("create_user_account");
////            if (!createAccount.equals(account)) {
////                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
////            }
////            Object fieldDbValue = dtlInfo.get(fieldCode);
////            if (StatusConstant.COMPLETED != status || (null != fieldDbValue && !"".equals(fieldDbValue))) {
////                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
////            }
////            if ("".equals(workSheetService.findWorkSheetList(request))) {
////                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
////            }
////        } catch (Exception e) {
////            logger.info(selectOptionService.getLanguageInfo(LangConstant.MSG_FIELD_HANDLE) + subWorkCode);//字段处理中
////        }
//        String logContent = (String) paramMap.get("logContent");
//        if (RegexUtil.isNull(logContent)) {
//            logContent = selectOptionService.getLanguageInfo(LangConstant.MSG_REFRESH_DATA);//更新了数据
//        }
//        String workTypeId = String.valueOf(workInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//
//        String jsonData = (String) paramMap.get("jsonData");
//        if (RegexUtil.isNull(jsonData)) {
//            // 保存工单主表，更新状态
//            Map<String, Object> dataInfo = new HashMap<String, Object>();
//            String changeType = (String) paramMap.get("changeType");
//            // 保存工单明细表，更新分配人、状态、时间等信息
//            dataInfo.put(changeType + "@" + fieldCode, fieldCode);
//            dataInfo.put(fieldCode, paramMap.get("fieldValue"));
//            dataInfo.put("sub_work_code", subWorkCode);
//            workSheetHandleService.updateDetailByWorkCode(schema_name, dataInfo, "sub_work_code");
//
//        } else {
//            Map<String, Object> dataInfo = null;
//            String isCalculation = "";
//            String fieldCodeNew = "";
//            JSONObject jsonObject = JSONObject.fromObject(jsonData);
//            Object fvrDtl = jsonObject.get("fieldViewRelationDetail");
//            String fieldViewType = (String) jsonObject.get("fieldViewType");
//            if (null != fvrDtl) {
//                dataInfo = new HashMap<String, Object>();
//                JSONObject jo = JSONObject.fromObject(fvrDtl);
//                isCalculation = jo.getString("isCalculation");
//                String changeType = jo.getString("changeType");
//                fieldCodeNew = jo.getString("fieldCode");
//                // 保存工单明细表，更新分配人、状态、时间等信息
//                dataInfo.put(changeType + "@" + fieldCodeNew, fieldCodeNew);
//                dataInfo.put("sub_work_code", subWorkCode);
//            }
//            selectOptionService.doUpdateJsonColumnData(schema_name, SqlConstant.WORK_DB_TABLES[1], "sub_work_code", subWorkCode, "body_property", "fieldValue", fieldCode, jsonData);
//            if (RegexUtil.isNotNull(isCalculation)) {
//                Double score = selectOptionService.selectPolymerizationJsonColumnData(schema_name, SqlConstant.WORK_DB_TABLES[1], "sub_work_code", subWorkCode, "body_property", "fieldViewType", fieldViewType, "AVG", "fieldValue");
//                dataInfo.put(fieldCodeNew, score);
//                workSheetHandleService.updateDetailByWorkCode(schema_name, dataInfo, "sub_work_code");
//            }
//
//        }
//        logService.AddLog(schema_name, pageType, subWorkCode, logContent, account);//记录历史
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 问题上报
//     *
//     * @param schema_name
//     * @param account
//     * @param accountName
//     * @param fromCode
//     * @param pageData
//     * @param paramMap
//     * @param workTmp
//     * @param relationId
//     * @param deviceCodes
//     * @throws Exception
//     */
//    public void repairWorkData(String schema_name, String account, String accountName, String title, String fromCode,
//                               Map<String, Object> pageData, Map<String, Object> paramMap, Map<String, Object> workTmp,
//                               String relationId, List<Map<String, Object>> deviceCodes) throws Exception {
//        if (RegexUtil.isNotNull(relationId)) {
//            Map<String, Object> assetInfo = assetMapper.findOne(schema_name, relationId);
//            deviceCodes = new ArrayList<Map<String, Object>>();
//            deviceCodes.add(assetInfo);
//        }
//        boolean isWork = true;
//        Integer workTypeId = null;
//        String flow_id = null;
//        String businessTypeDesc = null;
//        String fromWorkTypeId = "";
//        MetadataWork mateWork = null;
//        String msgCode = null; // 消息编码
//        if (RegexUtil.isNull(title)) {
//            try {
//                fromWorkTypeId = (String) workTmp.get("work_type_id");
//                businessTypeDesc = selectOptionService.getOptionNameByCode(schema_name, "work_type", fromWorkTypeId, "desc");
//                businessTypeDesc = "【" + businessTypeDesc + "】" + selectOptionService.getLanguageInfo(LangConstant.MSG_REPORT_REPAIR);
//            } catch (Exception wt) {
//                logger.warn(wt.getMessage());
//                throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_INVENTORY_FLOW_ID_WRONG));
//            }
//        }
//        try {
//            String user_id = null;
//            if (RegexUtil.isNull(account)) {
//                User user = userMapper.findByAccount(schema_name, account);
//                if (null != user) {
//                    user_id = user.getId();
//                }
//            }
//
//
//            Map<String, Object> flowMap = (Map) pageData.get("roleInfo");
//            List<Map<String, Object>> workList = null;
//            Map<String, Object> workInfo = new HashMap<String, Object>();
//            if (null != flowMap) {
//                workList = (List<Map<String, Object>>) flowMap.get("workInfo");
//                if (null != workList && workList.size() > 0) {
//                    String tmpCode = null;
//                    for (Map<String, Object> work : workList) {
//                        tmpCode = (String) work.get("code");
//                        if ("work_type_id".equals(tmpCode)) {
//                            workTypeId = Integer.valueOf((String) workInfo.get("work_type_id"));
//                        } else {
//                            workInfo.put(tmpCode, work);
//                        }
//                    }
//                }
//            }
//
//
//            String formKey = null;
//            if (RegexUtil.isNull(workTypeId)) {
//                Map<String, Object> workMap = dynamicCommonService.serviceEntry(schema_name, user_id);
//                workTypeId = (int) workMap.get("work_type_id");
//                flow_id = (String) workMap.get("flow_id");
//                formKey = (String) workMap.get("formKey");
//            } else {
//                flow_id = selectOptionService.getOptionNameByCode(schema_name, "work_type", String.valueOf(workTypeId), "relation");
//                formKey = workflowService.getStartFormKey(schema_name, flow_id);
//            }
//
//            mateWork = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//        } catch (Exception wt) {
//            logger.warn(wt.getMessage());
//            throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_INVENTORY_FLOW_ID_WRONG));
//        }
//        String body_property = mateWork.getBodyProperty();//自定义字段号集合
//        List<Object> list = new ArrayList<Object>();//转换城集合村数据库
//        String facilityId = String.valueOf(workTmp.get("facility_id"));
//        String poolId = String.valueOf(workTmp.get("pool_id"));
//        Map map = new HashMap();
//        Map<String, Object> map_object = new HashMap<String, Object>();
//        Map<String, Object> work_request_tmp = new HashMap<String, Object>();
//        Map<String, Object> works_detaile_tmp = new HashMap<String, Object>();
//        Map<String, Object> works_tmp = new HashMap<String, Object>();
//        Map<String, Object> keys = new HashMap<String, Object>();
//        Map<String, Object> dataInfo = new HashMap<String, Object>();
//        String asset_status = null;
//        int asset_running_status = 0;
//        int priority_level = 0;
//        //查找所有的设备状态，按状态设置问题的优先级
//        List<AssetRunningData> assetRunningDataList = maintenanceSettingsService.getAssetRunningDataList(schema_name);
//        String site_name = "";
//
//        if (null != assetRunningDataList && assetRunningDataList.size() > 0) {
//            asset_running_status = assetRunningDataList.get(0).getId();
//            priority_level = assetRunningDataList.get(0).getPriorityLevel();
//        }
//        // 获取当前设备
//        for (Map<String, Object> assetModel : deviceCodes) {
//            if (null != assetModel.get("site_name")) {
//                site_name = (String) assetModel.get("site_name");
//            }
//            if (RegexUtil.isNull(title)) {
//                title = businessTypeDesc + site_name + (String) assetModel.get("strname");
//            }
//            String changeType = "";
//            String saveType = "";
//            //生成新订单编号
//            Timestamp now = new Timestamp(System.currentTimeMillis()); // 获取当前时间
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(now);
//            calendar.add(Calendar.DATE, 1);
//            Timestamp deadline_time = new Timestamp(calendar.getTimeInMillis());
//            String rolePermissionKey = null;
//            User receiveAccountInfo = null;
//            int status = StatusConstant.PENDING; // 处理中
//
//            try {
//                if (null != body_property && !"".equals(body_property)) {
//                    JSONArray arrays = JSONArray.fromObject(body_property);
//                    JSONObject data = null;
//                    Map<String, Object> flowMap = (Map) pageData.get("roleInfo");
//                    List<Map<String, Object>> workList = null;
//                    Map<String, Object> workInfo = new HashMap<String, Object>();
//                    if (null != flowMap) {
//                        workList = (List<Map<String, Object>>) flowMap.get("workInfo");
//                        if (null != workList && workList.size() > 0) {
//                            for (Map<String, Object> work : workList) {
//                                workInfo.put((String) work.get("code"), work);
//                            }
//                        }
//                    }
//                    String fieldFormCode = null;
//                    String fieldCode = null;
//                    for (Object object : arrays) {
//                        data = JSONObject.fromObject(object);
//                        changeType = data.get("changeType").toString();
//                        saveType = data.get("saveType").toString();
//                        fieldFormCode = (String) data.get("fieldFormCode");
//                        fieldCode = (String) data.get("fieldCode");
//                        data.put(changeType + "@" + fieldCode, fieldCode);
//                        if (workTmp.containsKey(fieldCode)) {
//                            data.put("fieldValue", workTmp.get(fieldCode));
//                        }
//                        if (workInfo.containsKey(fieldFormCode)) {
//                            try {
//                                Map<String, Object> info = (Map<String, Object>) workInfo.get(fieldFormCode);
//                                if (info.containsKey("type")) {
//                                    if ("1".equals(info.get("type"))) {
//                                        data.put("fieldValue", paramMap.get(info.get("key")));
//                                    }
//                                } else {
//                                    data.put("fieldValue", info.get("key"));
//                                }
//                            } catch (Exception wiExp) {
//                                data.put("fieldValue", "");
//                            }
//                        }
//                        if ("roleInfo".equals(fieldCode)) {
//                            data.put("fieldValue", pageData.get("roleInfo"));
//                        }
//                        if ("work_template_code".equals(fieldCode)) {
//                            changeType = "varchar";
//                            data.put("fieldValue", mateWork.getWorkTemplateCode());
//                        }
//                        if ("create_user_account".equals(fieldCode)) {
//                            data.put("fieldValue", account);
//                        }
//                        if ("fromCode".equals(fieldCode)) {
//                            data.put("fieldValue", fromCode);
//                        }
//                        if ("title".equals(fieldCode)) {
//                            data.put("fieldValue", title);
//                        }
//                        if ("relation_id".equals(fieldCode)) {
//                            data.put("fieldValue", assetModel.get("_id"));
//                        }
//                        if ("relation_type".equals(fieldCode)) {
//                            data.put("fieldValue", mateWork.getRelationType());
//                        }
//                        if ("work_template_code".equals(fieldCode)) {
//                            data.put("fieldValue", mateWork.getWorkTemplateCode());
//                        }
//                        if ("asset_running_status".equals(fieldCode)) {
//                            data.put("fieldValue", asset_running_status);
//                        }
//                        if ("priority_level".equals(fieldCode)) {
//                            data.put("fieldValue", priority_level);
//                        }
//                        if ("work_type_id".equals(fieldCode)) {
//                            data.put("fieldValue", workTypeId);
//                        }
//                        if ("next_status".equals(fieldCode)) {
//                            String nextStatus = (String) data.get("fieldValue");
//                            if (null != nextStatus && !"".equals(nextStatus)) {
//                                status = Integer.valueOf(nextStatus);
//                            }
//                        }
//                        if ("work_request_type".equals(fieldCode) || "roleIds".equals(fieldCode)) {
//                            map.put(fieldCode, data.get("fieldValue"));
//                            if ("confirm".equals(data.get("fieldValue"))) {
//                                isWork = false;
//                            }
//                        }
//
//                        // 关联字段
//                        if (data.containsKey("fieldViewRelation") && !"".equals(data.get("fieldViewRelation"))) {
//                            // 条件参数
//                            if ("3".equals(data.get("fieldViewRelation"))) {
//                                if ("receive_account".equals(fieldCode)) {
//                                    rolePermissionKey = (String) data.get("fieldViewRelationDetail");
//                                }
//                            }
//                        }
//                        Object tmpValue = data.get("fieldValue");
//                        if ("create_time".equals(fieldCode) || "occur_time".equals(fieldCode)) {
//                            changeType = "varchar";
//                            data.put("fieldValue", now);
//                            tmpValue = now;
//                        }
//                        if ("deadline_time".equals(fieldCode)) {
//                            changeType = "varchar";
//                            data.put("fieldValue", deadline_time);
//                            tmpValue = now;
//                        }
//                        if ("1".equals(data.get("dbTableType")) && ("1".equals(saveType) || "2".equals(saveType))) {
//                            works_tmp.put(changeType + "@" + fieldCode, fieldCode);
//                            works_tmp.put(fieldCode, tmpValue);
//                        }
//                        if ("2".equals(data.get("dbTableType")) && ("1".equals(saveType) || "2".equals(saveType))) {
//                            works_detaile_tmp.put(changeType + "@" + fieldCode, fieldCode);
//                            works_detaile_tmp.put(fieldCode, tmpValue);
//                        }
//                        if ("6".equals(data.get("dbTableType")) && ("1".equals(saveType) || "2".equals(saveType))) {
//                            work_request_tmp.put(changeType + "@" + fieldCode, fieldCode);
//                            work_request_tmp.put(fieldCode, tmpValue);
//                        }
//                        list.add(data);
//                    }
//
//                }
//            } catch (Exception e) {
//                logger.warn(e.getMessage());
//                throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.T_LOAD_FAIL));
//            }
//            String work_code = "";
//            String sub_work_code = "";
//            String workRequestCode = "";
//            work_request_tmp.put("jsonb@body_property", "body_property");
//            work_request_tmp.put("body_property", JSONArray.fromObject(list).toString());
//            works_detaile_tmp.put("jsonb@body_property", "body_property");
//            works_detaile_tmp.put("body_property", JSONArray.fromObject(list).toString());
//            try {
//                HttpServletRequest request = HttpRequestUtils.getRequest();
//                request.setAttribute("poolId", poolId); // 工单池
//                request.setAttribute("workTypeId", String.valueOf(workTypeId)); // 工单类型
//                request.setAttribute("facilityId", facilityId); // 场地
//                request.setAttribute("roleIds", map.get("roleIds")); // 角色权限
//                request.setAttribute("rolePermissionKey", rolePermissionKey); // 特殊角色权限
//                if (RegexUtil.isNotNull(relationId)) {
//                    request.setAttribute("relationId", relationId); // 设备
//                }
//                receiveAccountInfo = this.getPermissionUser(schema_name);
//            } catch (Exception e) {
//                //默认设备位置主导派案策略
//                boolean isPositionGuide = true;
//                //默认位置
//                String guideKey = "position_code";
//                //去系统配置派案策略 设备组织还是设备位置主导
//                SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//                if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//                    guideKey = "facility_id";
//                    isPositionGuide = false;
//                }
//                List<User> userList = userService.findUsersByRoles(schema_name, map.get("roleIds").toString(), String.valueOf(workTmp.get(guideKey)), String.valueOf(assetModel.get("category_id")), isPositionGuide);
//                if (!userList.isEmpty()) {
//                    int max = userList.size();
//                    int getInt = (int) (new Random().nextFloat() * max);
//                    receiveAccountInfo = userList.get(getInt);
//                }
//            }
//            map.put("facility_id", facilityId); // 所属位置
//            //判断状态
//            if (StatusConstant.UNDISTRIBUTED != status && (StatusConstant.PENDING != status ||
//                    (null != receiveAccountInfo && RegexUtil.isNotNull(receiveAccountInfo.getAccount())))) {
//                if (null != receiveAccountInfo && RegexUtil.isNotNull(receiveAccountInfo.getAccount())) {
//                    if (StatusConstant.PENDING == status) {
//                        works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                        works_detaile_tmp.put("distribute_time", now);
//                        works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                        works_detaile_tmp.put("receive_time", now);
//                        works_detaile_tmp.put("varchar@receive_account", "receive_account");
//                        works_detaile_tmp.put("receive_account", receiveAccountInfo.getAccount());
//                        works_detaile_tmp.put("varchar@distribute_account", "distribute_account");
//                        works_detaile_tmp.put("distribute_account", account);
//                        msgCode = qcloudsmsConfig.SMS_10003007;
//                    } else if (isWork) {
//                        msgCode = qcloudsmsConfig.SMS_10003008;
//                    } else {
//                        msgCode = qcloudsmsConfig.SMS_10003001;
//                    }
//                    map.put("receive_account", receiveAccountInfo.getAccount());
//                } else {
//                    map.put("receive_account", null);
//                }
//            } else {
//                status = StatusConstant.UNDISTRIBUTED;   //待分配
//                map.put("receive_account", null);
//            }
//
//            map.put("create_user_account", account);//发送短信人
//            map.put("title_page", title);
//            map.put("businessType", String.valueOf(workTypeId)); //工单
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            map.put("deadline_time", simleDateFormat.format(deadline_time));
//            map.put("create_time", simleDateFormat.format(now));
//            map.put("status", String.valueOf(status));
//            try {
//                if (isWork) {
//                    work_code = dynamicCommonService.getWorkCode(schema_name);//工单号
//                    sub_work_code = dynamicCommonService.getSubWorkCode(schema_name);//工单详情单号
//                    works_tmp.put("work_code", work_code);
//                    works_tmp.put("status", status);
//                    works_detaile_tmp.put("status", status);
//                    works_detaile_tmp.put("work_code", work_code);
//                    works_detaile_tmp.put("int@is_main", "is_main");
//                    works_detaile_tmp.put("is_main", 1);
//                    works_detaile_tmp.put("sub_work_code", sub_work_code);
//                    keys.put("_sc_works", SqlConstant.IS_NEW_DATA + "work_code");
//                    keys.put("_sc_works_detail", SqlConstant.IS_NEW_DATA + "sub_work_code");
//                    dataInfo.put("_sc_works", works_tmp);
//                    dataInfo.put("_sc_works_detail", works_detaile_tmp);
//                    map_object.put("work_code", work_code);
//                    map_object.put("subWorkCode", sub_work_code);
//                } else {
//                    workRequestCode = dynamicCommonService.getWorkRequestCode(schema_name);
//                    work_request_tmp.put("status", status);
//                    map_object.put("saveSubType", "request");
//                    work_request_tmp.put("work_request_code", workRequestCode);
//                    map_object.put("work_request_code", workRequestCode);
//                    keys.put("_sc_work_request", SqlConstant.IS_NEW_DATA + "work_request_code");
//                    dataInfo.put("_sc_work_request", work_request_tmp);
//                }
//                map_object.put("keys", keys);
//                map_object.put("dataInfo", dataInfo);
//                map.put("sub_work_code", isWork ? sub_work_code : workRequestCode);
//                workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//                int z = workSheetHandleService.insertWorkAssetOrg(schema_name, sub_work_code, (String) assetModel.get("_id"));
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, flow_id, account, map);
//                if (!workflowStartResult.isStarted()) {
//                    throw new SenscloudException(businessTypeDesc);//上报维修，流程启动失败
//                }
//                logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_1), isWork ? sub_work_code : workRequestCode, businessTypeDesc, account);
//                workProcessService.saveWorkProccess(schema_name, isWork ? sub_work_code : workRequestCode, status, account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//
//            } catch (SenscloudException expMsg) {
//                logger.warn(expMsg.getMessage());
//                throw new SenscloudException(businessTypeDesc + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//上报维修，流程启动失败
//            } catch (Exception exp) {
//                logger.warn(exp.getMessage());
//                if (isWork) {
//                    workSheetHandleService.deleteByWorkCode(schema_name, work_code, "_sc_works");
//                    workSheetHandleService.deleteByWorkCode(schema_name, work_code, "_sc_works_detail");
//                } else {
//                    workSheetHandleService.deleteByWorkRequestCode(schema_name, workRequestCode, "_sc_work_request");
//                }
//                throw new Exception(exp);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, SensConstant.BUSINESS_NO_1, msgSubWorkCode,
//                    account, accountName, (String) map.get("title_page"), null); // 发送短信
//        }
//    }
//
//    /**
//     * 取随机负责人
//     *
//     * @param schemaName
//     * @return
//     */
//    private User getPermissionUser(String schemaName) {
//        List<User> list = executorService.getConditionalUsers(schemaName, null);
//        if (null != list && list.size() > 0) {
//            int max = list.size();
//            int getInt = (int) (new Random().nextFloat() * max);
//            return list.get(getInt);
//        }
//        return null;
//    }
//
//    /**
//     * 更新主工单开始时间
//     *
//     * @param schema_name
//     * @param workCode
//     */
//    private void updateMainDetailBeginTime(String schema_name, String workCode) {
//        // 主工单明细表数据处理
//        Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode);
//        // 第一条处理的时候，更新主工单开始时间
//        if (null == mainDetailInfo.get("begin_time") || "".equals(mainDetailInfo.get("begin_time"))) {
//            String mainDetailSubWorkCode = (String) mainDetailInfo.get("sub_work_code");
//            Map<String, Object> tmpInfo = new HashMap<String, Object>();
//            tmpInfo.put("sub_work_code", mainDetailSubWorkCode);
//            tmpInfo.put("varchar@begin_time", "begin_time");
//            tmpInfo.put("begin_time", new Timestamp(System.currentTimeMillis()));
//            workSheetHandleService.updateDetailByWorkCode(schema_name, tmpInfo, "sub_work_code");
//        }
//    }
}
