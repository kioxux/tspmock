package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MessageRuleService;
import org.springframework.stereotype.Service;

/**
 * MessageRuleService interface 消息规则业务接口
 *
 * @author Zys
 * @date 2020/04/10
 */
@Service
public class MessageRuleServiceImpl implements MessageRuleService {

//    private static final String MINUTE = "1";
//    private static final String HOUR = "2";
//    private static final String DAY = "3";
//    private static final String MONTH = "4";
//
//    private static final String IS_SMS = "1";
//    private static final String IS_EMAIL = "2";
//    private static final String IS_WECHEAT = "3";
//    private static final String IS_PC = "4";
//    private static final String IS_APP = "5";
//
//    @Autowired
//    MessageRuleMapper messageRuleMapper;
//
//    @Autowired
//    BomStockListMapper bomStockListMapper;
//
//    @Autowired
//    MessageCenterService messageCenterService;
//
//    @Autowired
//    FlowBusinessService flowBusinessService;
//
//    @Autowired
//    WorkProcessMapper workProcessMapper;
//
//    @Autowired
//    SelectOptionCustomMapper selectOptionCustomMapper;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    MessageService messageService;
//
//    /**
//     * 查询所有的消息规则
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryMessageRules(String schemaName) {
//        return messageRuleMapper.queryMessageRules(schemaName);
//    }
//
//    /**
//     * 判断是否满足规则
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public void messageRuleBusiness(String schemaName, Map<String, Object> paramMap) {
//        //是否满足规则
//        boolean isNeedSend;
//        //是否提醒当前操作人
//        boolean isSelfMsg = false;
//        //规则id
//        int ruleId;
//        //第一次判定时间间隔,触发之后每次判定时间间隔,截止时间间隔
//        Integer generateMinute,intervalMinute,deadlineMinute;
//        //表名,字段名,表单号,业务编号
//        String businessTable,judgeColumn,formKey,subWorkCode,intervalType;
//        Timestamp judgeTime;
//        List<Map<String, Object>> judgeTimeList;
//        String bomStock="_sc_bom_stock",selfMessageRole="0",selfCycle="_sc_self_cycle",workProcess="_sc_work_process";
//        try {
//            businessTable = String.valueOf(paramMap.get("business_table"));
//            intervalType=String.valueOf(paramMap.get("interval_type"));
//            ruleId = (Integer) (paramMap.get("id"));
//            generateMinute = (Integer) (paramMap.get("generate_minute"));
//            intervalMinute = (Integer) (paramMap.get("interval_minute"));
//            deadlineMinute = (Integer) (paramMap.get("deadline_minute"));
//            //无效的消息规则
//            if (RegexUtil.isNull(businessTable)) {
//                return;
//            }
//
//            //库存预警处理
//            if (bomStock.equals(businessTable)) {
//                List<Map<String, Object>> warningStocks = bomStockListMapper.queryWarningStocks(schemaName);
//                for (Map<String, Object> stock : warningStocks) {
//                    String BomStockId = String.valueOf(stock.get("id"));
//                    //判定规则是否满足
//                    isNeedSend = judgeMessageRule(schemaName, generateMinute, intervalMinute, deadlineMinute, ruleId,  BomStockId, null,intervalType);
//                    if (!isNeedSend) {
//                        continue;
//                    }
//                    insertMessageCenter(schemaName, paramMap, stock, false, BomStockId,  null,intervalType);
//                }
//                return;
//            }
//            judgeColumn = String.valueOf(paramMap.get("judge_column"));
//            if(RegexUtil.isNull(judgeColumn)){
//                return;
//            }
//
//            //非其他判定固定循环
//            if (selfCycle.equals(businessTable)) {
//                isNeedSend = judgeMessageRule(schemaName, generateMinute, intervalMinute, deadlineMinute, ruleId,  selfCycle, Timestamp.valueOf(judgeColumn), intervalType);
//                if (!isNeedSend) {
//                   return;
//                }
//                insertMessageCenter(schemaName, paramMap, new Hashtable<>(), false, selfCycle,  null, intervalType);
//                return;
//            }
//
//            //业务相关提醒
//            formKey = String.valueOf(paramMap.get("form_key"));
//            String roleId = String.valueOf(paramMap.get("role_id"));
//            if (selfMessageRole.equals(roleId)) {
//                isSelfMsg = true;
//            }
//
//            //工单超时提醒
//            if (workProcess.equals(workProcess)) {
//                //判定字段和节点为空规则无效
//                if (RegexUtil.isNull(formKey)) {
//                    return;
//                }
//                List<Map<String, Object>> messageWorks = flowBusinessService.queryFlowBusinessDataByFromKey(schemaName, formKey);
//                for (Map<String, Object> mWork : messageWorks) {
//                    subWorkCode = String.valueOf(mWork.get("sub_work_code"));
//                    String processId = String.valueOf(workProcessMapper.getIdByTaskId(schemaName, subWorkCode));
//                    judgeTimeList = messageRuleMapper.queryJudgeColumn(schemaName, judgeColumn, businessTable, "sub_work_code", subWorkCode);
//                    //时间判定为空或多个无效
//                    if (judgeTimeList.isEmpty() || judgeTimeList.size() > 1) {
//                        continue;
//                    }
//                    //判定字段类型错误 无效
//                    try {
//                        judgeTime = (Timestamp) judgeTimeList.get(0).get("judgecolumn");
//                    } catch (Exception e) {
//                        continue;
//                    }
//                    //判定规则是否满足
//                    isNeedSend = judgeMessageRule(schemaName, generateMinute, intervalMinute, deadlineMinute, ruleId, processId, judgeTime,intervalType);
//                    if (!isNeedSend) {
//                        continue;
//                    }
//                    Long timeInterval = (System.currentTimeMillis() - judgeTime.getTime()) / 60000;
//                    insertMessageCenter(schemaName, paramMap, mWork, isSelfMsg, processId, timeInterval,intervalType);
//            }
//            }
//        } catch (Exception e) {
//            return;
//        }
//    }
//
//    /**
//     * 查询未发送的消息
//     *
//     * @param schemaName
//     * @param generateTime
//     * @param intervalTime
//     * @param deadlineTime
//     * @param ruleId
//     * @param processId
//     * @param judgeTime
//     * @return
//     */
//    private boolean judgeMessageRule(String schemaName, int generateTime, int intervalTime, int deadlineTime, int ruleId, String processId, Timestamp judgeTime,String intervalType) {
//        try {
//            //查询此业务规则历史消息
//            List<Map<String, Object>> ruleMessage = messageRuleMapper.queryMessageByRuleId(schemaName, ruleId, processId);
//            //历史消息为空且判定时间为空,库房预警第一次触发
//            if (ruleMessage.isEmpty() && null == judgeTime) {
//                return true;
//            }
//            // 第一次触发时间间隔
//            long generateTimeInterval;
//            // 截止时间时间间隔
//            long deadlineTimeInterval;
//            // 循环时间间隔
//            long timeInterval;
//            // 默认时间单位为分钟
//            long timeUnit=60*1000;
//            // 是否启用月单位
//            boolean monthUnit=false;
//            switch (intervalType) {
//                case MINUTE:
//                    timeUnit=60*1000;
//                    break;
//                case HOUR:
//                    timeUnit=60*1000*60;
//                    break;
//                case DAY:
//                    timeUnit=60*1000*60*24;
//                    break;
//                case MONTH:
//                    monthUnit=true;
//                    break;
//                    default:
//            }
//            //历史消息为空,判定时间不为空,查看当前时间间隔是否大于第一次触发时间间隔
//            if (ruleMessage.isEmpty() && null != judgeTime) {
//               if(monthUnit){
//                   generateTimeInterval=getMonthDiff(new Date(),new Date(judgeTime.getTime()));
//               }else{
//                   generateTimeInterval = (System.currentTimeMillis() - judgeTime.getTime()) / timeUnit;
//               }
//                if (generateTimeInterval < generateTime) {
//                    return false;
//                } else {
//                    return true;
//                }
//            }
//            //历史消息不为空,且往后触发时间间隔小于0 只触发一次直接返回
//            if (!ruleMessage.isEmpty() && intervalTime <= 0) {
//                return false;
//            }
//            //库房预警 判定时间为空 不是第一次触发,判定时间为历史消息第一条发送时间
//            if (null == judgeTime) {
//                judgeTime = Timestamp.valueOf((String)ruleMessage.get(ruleMessage.size() - 1).get("createtime")) ;
//            }
//            //判断提醒是否已经过期
//            if(monthUnit){
//                deadlineTimeInterval=getMonthDiff(new Date(),new Date(judgeTime.getTime()));
//            }else{
//                deadlineTimeInterval = (System.currentTimeMillis() - judgeTime.getTime()) / timeUnit;
//            }
//            if (deadlineTimeInterval >=deadlineTime) {
//                return false;
//            }
//            //没有过期则那最新的消息发送时间作为判定时间
//            Timestamp lastTime =  Timestamp.valueOf((String)ruleMessage.get(0).get("createtime"));
//
//            //判断提醒是否已经过期
//            if(monthUnit){
//                timeInterval=getMonthDiff(new Date(),new Date(judgeTime.getTime()));
//            }else{
//                timeInterval = (System.currentTimeMillis() - lastTime.getTime()) / timeUnit;
//            }
//            if (timeInterval <intervalTime) {
//                return false;
//            }
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 组装消息
//     *
//     * @param messageRule
//     * @param mWork
//     * @return
//     */
//    private void insertMessageCenter(String schemaName, Map<String, Object> messageRule, Map<String, Object> mWork, boolean isSelfMsg, String processId, Long timeInterval,String intervalType) {
//        Map<String, Object> message = new HashMap(16);
//        //TODO 没有request 没有session取不到中英文 暂时先写死为中文的
//        SimpleDateFormat sf1=new SimpleDateFormat(Constants.DATE_FMT_SS);
//        SimpleDateFormat sf=new SimpleDateFormat(Constants.DATE_FMT_FILE);
//        Date now=new Date();
//        String timeUnit="分钟";
//        switch (intervalType) {
//            case MINUTE:
//                timeUnit="分钟";
//                break;
//            case HOUR:
//                timeUnit="小时";
//                break;
//            case DAY:
//                timeUnit="天";
//                break;
//            case MONTH:
//                timeUnit="月";
//                break;
//                default:
//        }
//        try {
//            String specParam = String.valueOf(messageRule.get("sms_reserve1")),
//                    pattern = String.valueOf(messageRule.get("sms_pattern")),
//                    pattern_description = String.valueOf(messageRule.get("pattern_description")),
//                    sendWay = String.valueOf(messageRule.get("send_channel")),
//                    businessTable= String.valueOf(messageRule.get("business_table")),
//                    provider = String.valueOf(messageRule.get("provider"));
//            String bomStock="_sc_bom_stock",selfCycle="_sc_self_cycle",workProcess="_sc_work_process";
//            if(RegexUtil.isNull(sendWay)){
//                return;
//            }
//            boolean is_sms_content=false;
//            boolean is_is_weixin_content = false;
//            List<String> sendWays=Arrays.asList(sendWay.split(","));
//            message.put("is_sms", false);
//            message.put("is_email", false);
//            message.put("is_weixin", false);
//            message.put("is_pc", false);
//            message.put("is_app", false);
//            message.put("createtime", sf1.format(now));
//            for(String way:sendWays){
//                if(IS_SMS.equals(way)){
//                    is_sms_content=true;
//                    message.put("is_sms", true);
//                }else if(IS_EMAIL.equals(way)){
//                    message.put("is_email", true);
//                }else if(IS_WECHEAT.equals(way)){
//                    is_is_weixin_content=true;
//                    message.put("is_weixin", true);
//                }else if(IS_PC.equals(way)){
//                    message.put("is_pc", true);
//                }else if(IS_APP.equals(way)){
//                    message.put("is_app", true);
//                }
//            }
//            message.put("business_type", messageRule.get("work_type_id"));
//            message.put("rule_id", messageRule.get("id"));
//            message.put("is_send",false);
//            message.put("process_id",processId);
//            String smsCode = (String)messageRule.get("msg_content_id");
//            Long messageId;
//            Map<String,Object> messageReceiver=new HashMap<>();
//
//            //备件库存预警
//            if(bomStock.equals(businessTable)){
//                String[] contentParamBom = {String.valueOf(mWork.get("bom_name")), String.valueOf(mWork.get("bom_code")), String.valueOf(mWork.get("stock_code"))};
//                if(is_sms_content){
//                    message.put("real_msg_content", this.getMsConfigInfo(contentParamBom, specParam, pattern, provider));
//                }
//                if(is_is_weixin_content){
//                    message.put("real_wx_oa_msg_content", messageService.getWxoaContent(schemaName, smsCode, contentParamBom));
//                }
//                message.put("msg_content", this.getMsConfigInfo(contentParamBom, pattern_description));
//                message.put("business_no", mWork.get("stock_code")+sf.format(now));
//                messageCenterService.insertMessageCenterColumn(schemaName,message);
//                messageId=(Long)message.get("id");
//                messageReceiver.put("message_id", messageId);
//                List<Map<String, Object>> userMapList = selectOptionCustomMapper.findStockUserListByRoleIdAndStockCode(schemaName, String.valueOf(messageRule.get("role_id")), String.valueOf(mWork.get("stock_code")));
//                for (Map<String, Object> userMap : userMapList) {
//                    messageReceiver.put("receive_account", userMap.get("code"));
//                    messageReceiver.put("is_read", false);
//                    messageReceiver.put("is_main_receiver", false);
//                    messageCenterService.insertMessageReceiverColumn(schemaName, messageReceiver);
//                }
//                return;
//            }
//
//            boolean isPositionGuide = true;
//
//            //默认位置
//            String guideKey = "position_code";
//            //去系统配置派案策略 设备组织还是设备位置主导
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schemaName, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//            if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//                guideKey = "facility_id";
//                isPositionGuide = false;
//            }
//
//            String roleIdStr = String.valueOf(messageRule.get("role_id"));
//            String facilityIdStr=null;
//            //工单提醒
//            if(workProcess.equals(businessTable)){
//                //组装消息
//                String workCode = String.valueOf(mWork.get("work_code"));
//                message.put("business_no", mWork.get("sub_work_code")+sf.format(now));
//                String dutyName ="";
//                String dutyAccount=null;
//                User user=null;
//                if (RegexUtil.isNotNull(String.valueOf(mWork.get("duty_account")))) {
//                    dutyAccount=String.valueOf(mWork.get("duty_account"));
//                    user = userService.getUserByAccount(schemaName, dutyAccount);
//                    dutyName=user.getUsername();
//                }
//                String[] contentParam = {dutyName, workCode, String.valueOf(timeInterval)+timeUnit};
//                if(is_sms_content){
//                    message.put("real_msg_content", this.getMsConfigInfo(contentParam, specParam, pattern, provider));
//                }
//                if(is_is_weixin_content){
//                    message.put("real_wx_oa_msg_content", messageService.getWxoaContent(schemaName, smsCode, contentParam));
//                }
//                message.put("msg_content", this.getMsConfigInfo(contentParam, pattern_description));
//                messageCenterService.insertMessageCenterColumn(schemaName,message);
//                messageId=(Long)message.get("id");
//                messageReceiver.put("message_id", messageId);
//                //如果提醒处理人自己
//                if (isSelfMsg) {
//                    if(RegexUtil.isNull(dutyAccount)||user==null){
//                        return;
//                    }
//                    messageReceiver.put("receive_account",dutyAccount);
//                    messageReceiver.put("is_read", false);
//                    messageReceiver.put("is_main_receiver", false);
//                    messageCenterService.insertMessageReceiverColumn(schemaName, messageReceiver);
//                    return;
//                }
//                //提醒角色下所有人
//                String workTypeId = String.valueOf(messageRule.get("work_type_id"));
//                String tableName = SensConstant.TABLE_TYPE_INFO.get(workTypeId);
//                tableName = tableName.replace("@", schemaName);
//                message.put("business_no", mWork.get("sub_work_code"));
//
//                //工单业务
//                List<Map<String, Object>> tabs = messageRuleMapper.queryBusiness(schemaName, SensConstant.TABLE_COLUMN_TYPE_INFO.get(workTypeId),
//                        tableName, SensConstant.TABLE_CONDITION_TYPE_INFO.get(workTypeId), String.valueOf(mWork.get("sub_work_code")));
//                if (tabs.isEmpty() || tabs.size() > 1) {
//                    return;
//                }
//
//                //组织或位置
//                 facilityIdStr = String.valueOf(tabs.get(0).get(guideKey));
//                 roleIdStr = String.valueOf(messageRule.get("role_id"));
//
//
//            }
//            if (selfCycle.equals(businessTable)) {
//                String businessNo=messageRule.get("id")+sf.format(now);
//                message.put("business_no", businessNo);
//                String[] contentParam = {};
//                if(is_sms_content){
//                    message.put("real_msg_content", this.getMsConfigInfo(contentParam, specParam, pattern, provider));
//                }
//                if(is_is_weixin_content){
//                    message.put("real_wx_oa_msg_content", messageService.getWxoaContent(schemaName, smsCode, contentParam));
//                }
//                message.put("msg_content", this.getMsConfigInfo(contentParam, pattern_description));
//                messageCenterService.insertMessageCenterColumn(schemaName,message);
//                messageId=(Long)message.get("id");
//                messageReceiver.put("message_id", messageId);
//            }
//
//            //循环遍历
//            List<User> userList = userService.findUsersByRoles(schemaName, roleIdStr, facilityIdStr, null, isPositionGuide);
//            if (userList.isEmpty()) {
//                return;
//            }
//
//            //遍历能接收到消息的人插入消息表
//            for (User receiveUser : userList) {
//                messageReceiver.put("receive_account",receiveUser.getAccount());
//                messageReceiver.put("is_read", false);
//                messageReceiver.put("is_main_receiver", false);
//                messageCenterService.insertMessageReceiverColumn(schemaName, messageReceiver);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    /**
//     * 内容解析 暂时发送短信现将详细转换成对应的短信格式
//     *
//     * @param contentParam
//     * @param specParam
//     * @param pattern
//     * @param provider
//     * @return
//     */
//    private String getMsConfigInfo(Object[] contentParam, String specParam, String pattern, String provider) {
//        if (RegexUtil.isNull(specParam) || RegexUtil.isNull(pattern)) {
//            return null;
//        }
//        if (RegexUtil.isNull(provider) || Constants.SENSCLOUD_SMS_DEFAULT.equals(provider)) {
//            List<Object> list = new ArrayList<Object>();
//            list.add(Integer.valueOf(specParam));
//            list.addAll(Arrays.asList(contentParam));
//            return MessageFormat.format(pattern, list.toArray());
//        }
//        return MessageFormat.format(pattern, contentParam);
//    }
//
//    /**
//     * 内容解析 暂时发送短信现将详细转换成对应的短信格式
//     *
//     * @param contentParam
//     * @param specParam
//     * @return
//     */
//    private String getMsConfigInfo(Object[] contentParam, String specParam) {
//        if (RegexUtil.isNull(specParam)) {
//            return null;
//        }
//        List  contentList= new ArrayList();
//        contentList.add("");
//        for(Object o:contentParam){
//            contentList.add(o);
//        }
//        MessageFormat messageFormat = new MessageFormat(specParam);
//        String message = messageFormat.format(contentList.toArray());
//        return message;
//    }
//
//    /**
//     * 获取两个日期相差的月数
//     *
//     * @param d1
//     * @param d2
//     */
//    public static int getMonthDiff(Date d1, Date d2) {
//        Calendar c1 = Calendar.getInstance();
//        Calendar c2 = Calendar.getInstance();
//        c1.setTime(d1);
//        c2.setTime(d2);
//        int year1 = c1.get(Calendar.YEAR);
//        int year2 = c2.get(Calendar.YEAR);
//        int month1 = c1.get(Calendar.MONTH);
//        int month2 = c2.get(Calendar.MONTH);
//        int day1 = c1.get(Calendar.DAY_OF_MONTH);
//        int day2 = c2.get(Calendar.DAY_OF_MONTH);
//        // 获取年的差值 
//        int yearInterval = year1 - year2;
//        // 如果 d1的 月-日 小于 d2的 月-日 那么 yearInterval-- 这样就得到了相差的年数
//        if (month1 < month2 || month1 == month2 && day1 < day2) {
//            yearInterval--;
//        }
//        // 获取月数差值
//        int monthInterval = (month1 + 12) - month2;
//        if (day1 < day2) {
//            monthInterval--;
//        }
//        monthInterval %= 12;
//        int monthsDiff = Math.abs(yearInterval * 12 + monthInterval);
//        return monthsDiff;
//    }
}
