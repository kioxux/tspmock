package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkColumnSearchParam;
import com.gengyun.senscloud.model.WorkTemplateSearchParam;

import java.util.List;
import java.util.Map;

public interface WorkTemplateService {

    /**
     * 获取工单模板列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String, Object>>  getWorkTemplateListForPage(MethodParam methodParam, WorkTemplateSearchParam param);

    /**
     * 新增模板基础信息
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    String newWorkTemplate(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取模板基本信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkTemplateInfoByWorkTemplateCode(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 获取模板字段信息列表
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    List<Map<String, Object>> getWorkTemplateColumnListByWorkTemplateCode(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 删除工单模板
     *
     * @param methodParam
     * @param work_template_code
     */
    void cutWorkTemplateByWorkTemplateCode(MethodParam methodParam, String work_template_code);

    /**
     * 删除工单模板字段
     *
     * @param methodParam
     * @param param
     */
    void cutWorkTemplateColumnByWorkTemplateCode(MethodParam methodParam, WorkTemplateSearchParam param);

    /**
     * 新增工单模板字段
     *
     * @param methodParam
     * @param paramMap
     */
    void newWorkTemplateColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 是否处理工单模板字段
     *
     * @param methodParam
     * @param paramMap
     */
    void isHandleWorkTemplateColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑工单模板字段
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkTemplateColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取工单模板字段明细信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkTemplateColumnInfo(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 获取工单模板字段关联条件明细信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkTemplateColumnLinkageInfo(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 克隆字段
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    String newWorkTemplateClone(MethodParam methodParam, WorkColumnSearchParam bParam);

//    Map<String,Object> getWorkTemplateColumnSpecialProperty(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 编辑工单模板基本信息
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkTemplate(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 根据控件类型获取特殊属性key
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    List<Map<String, Object>> getSpecialPropertyByWidgetType(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 根据字段id获取基本及特殊属性
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    List<Map<String, Object>> getColumnProperty(MethodParam methodParam, WorkTemplateSearchParam bParam);

    /**
     * 通过work_template_code获取模板基本属性
     *
     * @param methodParam
     * @param work_template_code
     * @return
     */
    Map<String, Object> getWorkTemplateDetailByCode(MethodParam methodParam, String work_template_code);

    /**
     * 编辑模板字段是否显示
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkTemplateColumnUse(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑字段 上移、下移信息
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkTemplateOrder(MethodParam methodParam, Map<String, Object> paramMap);
}
