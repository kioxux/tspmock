package com.gengyun.senscloud.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gengyun.senscloud.common.BussinessConfigConstant;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.BussinessConfigMapper;
import com.gengyun.senscloud.model.BussinessConfigModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.BussinessConfigService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 业务配置
 */
@Service
public class BussinessConfigServiceImpl implements BussinessConfigService {
    @Resource
    BussinessConfigMapper businessConfigMapper;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;

    /**
     * 新增系统维护配置
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void newSystemConfig(MethodParam methodParam, Map<String, Object> pm) {
        //1:工单类型 2:维修类型 3:设备运行状态 4:设备级别 5:备件类型 6:位置类型 7:文档类型 8:单位管理 9:完修说明 10:货币种类
        int systemConfigType = RegexUtil.optIntegerOrExp(pm.get("systemConfigType"), methodParam, ErrorConstant.EC_BUSINESS_CONFIG_2, LangConstant.MSG_BI);
        pm.put("create_user_id", methodParam.getUserId());
        if (systemConfigType == 1) {//工单类型  业务类型下拉框key 工单业务类型
            businessConfigMapper.insertWorkType(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("工单类型新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_CATEGORY_X, pm.get("type_name").toString()}));//工单类型新建
        } else if (systemConfigType == 2) {//维修类型
            businessConfigMapper.insertRepairType(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("维修类型新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_CATEGORY_AX, pm.get("type_name").toString()}));//维修类型新建
        } else if (systemConfigType == 3) {//设备运行状态
            businessConfigMapper.insertAssetRunningStatus(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("设备运行状态新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_ASSET_BA, pm.get("running_status").toString()}));//设备运行状态新建
        } else if (systemConfigType == 4) {//设备级别
            businessConfigMapper.insertImportmentLevel(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("设备级别新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_ASSET_AS, pm.get("level_name").toString()}));//设备级别新建
        } else if (systemConfigType == 5) {//备件类型
            if (!RegexUtil.optNotNull(pm.get("parent_id")).isPresent()) {
                pm.put("parent_id", 0);
            }
            businessConfigMapper.insertBomType(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("备件类型新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_CATEGORY_C, pm.get("type_name").toString()}));//备件类型新建
        } else if (systemConfigType == 6) {//位置类型
            businessConfigMapper.insertFacilityType(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("位置类型新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_CATEGORY_AZ, pm.get("type_name").toString()}));//位置类型新建
        } else if (systemConfigType == 7) {//文档类型
            businessConfigMapper.insertFileType(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("文档类型新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_CATEGORY_AB_A, pm.get("type_name").toString()}));//文档类型新建
        } else if (systemConfigType == 8) {//单位管理
            businessConfigMapper.insertUnit(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("单位管理新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_PB, pm.get("unit_name").toString()}));//单位管理新建
        } else if (systemConfigType == 9) {//完修说明
            businessConfigMapper.insertWorkFinishedType(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("完修说明新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_AAX_Z, pm.get("finished_name").toString()}));//完修说明新建
        } else if (systemConfigType == 10) {//货币种类
            businessConfigMapper.insertCurrency(methodParam.getSchemaName(), pm);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(), LangUtil.doSetLogArray("货币种类新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_PC, pm.get("currency_name").toString()}));//货币种类新建
        }
    }

    /**
     * 更新系统维护配置
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void modifySystemConfig(MethodParam methodParam, Map<String, Object> pm) {
        //1:工单类型 2:维修类型 3:设备运行状态 4:设备级别 5:备件类型 6:位置类型 7:文档类型 8:单位管理 9:完修说明 10:货币种类
        String tableName = "";
        int systemConfigType = RegexUtil.optIntegerOrExp(pm.get("systemConfigType"), methodParam, ErrorConstant.EC_BUSINESS_CONFIG_1, pm.get("id").toString(), LangConstant.MSG_BI);
        // Integer systemConfigType = RegexUtil.optIntegerOrExp(pm.get("systemConfigType"), LangConstant.MSG_BI);
        Map<String, Object> old;
        if (systemConfigType == 1) {//工单类型 工单模板下拉框key work_template 流程模板下拉框key flow_template 业务类型下拉框key 工单业务类型
            tableName = "_sc_work_type";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("工单类型更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CATEGORY_X, jsonArray.toString()}));//工单类型更新
        } else if (systemConfigType == 2) {//维修类型
            tableName = "_sc_repair_type";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("修类型更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CATEGORY_AX, jsonArray.toString()}));//维修类型更新
        } else if (systemConfigType == 3) {//设备运行状态
            tableName = "_sc_asset_running_status";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("备运行状态更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_ASSET_BA, jsonArray.toString()}));//设备运行状态更新
        } else if (systemConfigType == 4) {//设备级别
            tableName = "_sc_importment_level";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("设备级别更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_ASSET_AS, jsonArray.toString()}));//设备级别更新
        } else if (systemConfigType == 5) {//备件类型
            tableName = "_sc_bom_type";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("备件类型更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CATEGORY_C, jsonArray.toString()}));//备件类型更新
        } else if (systemConfigType == 6) {//位置类型
            tableName = "_sc_facility_type";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("位置类型更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CATEGORY_AZ, jsonArray.toString()}));//位置类型更新
        } else if (systemConfigType == 7) {//文档类型
            tableName = "_sc_file_type";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("文档类型更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_CATEGORY_AB_A, jsonArray.toString()}));//文档类型更新
        } else if (systemConfigType == 8) {//单位管理
            tableName = "_sc_unit";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("单位管理更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_PB, jsonArray.toString()}));//单位管理更新
        } else if (systemConfigType == 10) {//货币种类
            tableName = "_sc_currency";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("货币种类更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_PC, jsonArray.toString()}));//货币种类更新
        } else if (systemConfigType == 9) {//完修说明
            tableName = "_sc_work_finished_type";
            old = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, RegexUtil.changeToInteger(pm.get("id").toString(), LangConstant.TITLE_AAW_T));
            JSONArray jsonArray = LangUtil.compareMap(pm, old);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, pm.get("id").toString(),
                    LangUtil.doSetLogArray("完修说明更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAX_Z, jsonArray.toString()}));//完修说明更新
        }
        businessConfigMapper.updateById(methodParam.getSchemaName(), tableName, pm);
    }

    /**
     * 查询系统维护配置列表
     *
     * @param methodParam          入参
     * @param bussinessConfigModel 入参
     * @return 系统维护配置列表
     */
    @Override
    public List<Map<String, Object>> getSystemConfigList(MethodParam methodParam, BussinessConfigModel bussinessConfigModel) {
        Map<String, Object> pm = new HashMap<>();
        pm.put("keywordSearch", bussinessConfigModel.getKeywordSearch());
        int systemConfigType = RegexUtil.optIntegerOrExp(bussinessConfigModel.getSystemConfigType(), methodParam, ErrorConstant.EC_BUSINESS_CONFIG_3, LangConstant.MSG_BI);
        //1:工单类型 2:维修类型 3:设备运行状态 4:设备级别 5:备件类型 6:位置类型 7:文档类型 8:单位管理 9:完修说明 10:货币种类
        if (systemConfigType == 1) {//工单类型
            return businessConfigMapper.findWorkTypeList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 2) {//维修类型
            return businessConfigMapper.findRepairTypeList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 3) {//设备运行状态
            return businessConfigMapper.findAssetRuningStatusList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 4) {//设备级别
            return businessConfigMapper.findImportmentLevelList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 5) {//备件类型
            return businessConfigMapper.findBomTypeList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 6) {//位置类型
            return businessConfigMapper.findFacilityTypeList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 7) {//文档类型
            return businessConfigMapper.findFileTypeList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 8) {//单位管理
            return businessConfigMapper.findUnitList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 9) {//完修说明
            return businessConfigMapper.findWorkFinishedTypeList(methodParam.getSchemaName(), pm);
        } else if (systemConfigType == 10) {//货币种类
            return businessConfigMapper.findCurrencyList(methodParam.getSchemaName(), pm);
        }

        return new ArrayList<>();
    }

    /**
     * 删除系统维护配置
     *
     * @param methodParam          入参
     * @param BussinessConfigModel 入参
     */
    @Override
    public void cutSystemConfigById(MethodParam methodParam, BussinessConfigModel BussinessConfigModel) {
        Integer id = RegexUtil.optSelectOrExpParam(BussinessConfigModel.getId(), LangConstant.TITLE_AAT_P); // 数据未选择
        String idStr = id.toString();
        int systemConfigType = RegexUtil.optIntegerOrExp(BussinessConfigModel.getSystemConfigType(), methodParam, ErrorConstant.EC_BUSINESS_CONFIG_4, idStr, LangConstant.MSG_BI);
        String tableName = "";
        Map<String, Object> map;
        //1:工单类型 2:维修类型 3:设备运行状态 4:设备级别 5:备件类型 6:位置类型 7:文档类型 8:单位管理 9:完修说明 10:货币种类
        if (systemConfigType == 1) {//工单类型 工单模板下拉框key work_template 流程模板下拉框key flow_template 业务类型下拉框key 工单业务类型
            tableName = "_sc_work_type";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除工单类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CATEGORY_X, map.get("type_name").toString()}));//删除了工单类型
        } else if (systemConfigType == 2) {//维修类型
            tableName = "_sc_repair_type";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除维修类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CATEGORY_AX, map.get("type_name").toString()}));//删除了维修类型
        } else if (systemConfigType == 3) {//设备运行状态
            tableName = "_sc_asset_running_status";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除设备运行状态", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ASSET_BA, map.get("running_status").toString()}));//删除了设备运行状态
        } else if (systemConfigType == 4) {//设备级别
            tableName = "_sc_importment_level";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了设备级别", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ASSET_AS, map.get("level_name").toString()}));//删除了设备级别
        } else if (systemConfigType == 5) {//备件类型
            tableName = "_sc_bom_type";
            Map<String, Object> childMap = businessConfigMapper.findBomTypeChildren(methodParam.getSchemaName(), id);
            if (RegexUtil.optNotNull(childMap).isPresent()) {
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_PM, LangConstant.TITLE_PM, LangConstant.TITLE_PM});//备件类型下还有备件类型，不能删除备件类型
            }
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了备件类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CATEGORY_C, map.get("type_name").toString()}));//删除了备件类型
        } else if (systemConfigType == 6) {//位置类型
            tableName = "_sc_facility_type";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了位置类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CATEGORY_AZ, map.get("type_name").toString()}));//删除了位置类型
        } else if (systemConfigType == 7) {//文档类型
            tableName = "_sc_file_type";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了文档类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_CATEGORY_AB_A, map.get("type_name").toString()}));//删除了文档类型
        } else if (systemConfigType == 8) {//单位管理
            tableName = "_sc_unit";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了单位管理", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_PB, map.get("unit_name").toString()}));//删除了单位管理
        } else if (systemConfigType == 10) {//货币种类
            tableName = "_sc_currency";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了货币种类", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_PC, map.get("currency_name").toString()}));//删除了货币种类
        } else if (systemConfigType == 9) {//完修说明
            tableName = "_sc_work_finished_type";
            map = businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7002, idStr, LangUtil.doSetLogArray("删除了完修说明", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAX_Z, map.get("finished_name").toString()}));//删除了完修说明
        }
        businessConfigMapper.deleteById(methodParam.getSchemaName(), tableName, id);
    }

    @Override
    public Object getSystemConfigField(MethodParam methodParam, BussinessConfigModel bussinessConfigModel) {
        int systemConfigType = RegexUtil.optIntegerOrExp(bussinessConfigModel.getSystemConfigType(), methodParam, ErrorConstant.EC_BUSINESS_CONFIG_3, LangConstant.MSG_BI);
        JSONObject jsonObject = new JSONObject();
        //1:工单类型 2:维修类型 3:设备运行状态 4:设备级别 5:备件类型 6:位置类型 7:文档类型 8:单位管理 9:完修说明 10:货币种类
        if (systemConfigType == 1) {//工单类型 工单模板下拉框key work_template 流程模板下拉框key flow_template 业务类型下拉框key 工单业务类型
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.WORK_TYPE_FIELD);
        } else if (systemConfigType == 2) {//维修类型
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.REPAIR_TYPE_FIELD);
        } else if (systemConfigType == 3) {//设备运行状态
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.ASSET_RUNNING_STATUS_FIELD);
        } else if (systemConfigType == 4) {//设备级别
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.IMPORTMENT_LEVEL_FIELD);
        } else if (systemConfigType == 5) {//备件类型
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.BOM_TYPE_FIELD);
        } else if (systemConfigType == 6) {//位置类型
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.FACILITY_TYPE_FIELD);
        } else if (systemConfigType == 7) {//文档类型
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.FILE_TYPE_FIELD);
        } else if (systemConfigType == 8) {//单位管理
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.UNIT_FIELD);
        } else if (systemConfigType == 10) {//货币种类
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.CURRENCY_FIELD);
        } else if (systemConfigType == 9) {//完修说明
            jsonObject = JSONObject.parseObject(BussinessConfigConstant.WORK_FINISHED_TYPE_FIELD);
        }
        return jsonObject;
    }

    @Override
    public Map<String, Object> getSystemConfigInfoById(MethodParam methodParam, String tableName, Integer id) {
        return businessConfigMapper.findSystemById(methodParam.getSchemaName(), tableName, id);
    }

    /**
     * 根据id获取业务类型详情
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 业务类型详情
     */
    @Override
    public Map<String, Object> getBusinessById(MethodParam methodParam, Integer id) {
        return businessConfigMapper.findBusinessById(methodParam.getSchemaName(), id);
    }

    /**
     * 根据业务类型获取id最小的工单类型
     */
    @Override
    public Map<String, Object> getWorkTypeByBusiness(MethodParam methodParam, Integer business_id) {
        return businessConfigMapper.findWorkTypeByBusiness(methodParam.getSchemaName(), business_id);
    }

    /**
     * 根据业务类型获取启用的工单类型ids
     *
     * @param methodParam 入参
     * @param business_id 入参
     * @return 工单类型ids
     */
    @Override
    public List<String> getWorkTypeIdsByBusinessId(MethodParam methodParam, Integer business_id) {
        return businessConfigMapper.findWorkTypeIdsByBusinessId(methodParam.getSchemaName(), business_id);
    }
}
