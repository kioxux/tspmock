package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.service.job.WorkOrderAsyncJobService;
import com.gengyun.senscloud.service.job.WorkOrderJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class WorkOrderAsyncJobServiceImpl implements WorkOrderAsyncJobService {
    private static final Logger logger = LoggerFactory.getLogger(WorkOrderAsyncJobServiceImpl.class);

    private static boolean isRunning = false;

    @Resource
    private WorkOrderJobService workOrderJobService;

    /**
     * 根据新增、修改的维保计划生成相应的行事历 —— 异步执行
     * @param schemaName
     */
    @Override
    @Async
    public void asyncCronJobToGenerateWorkOrderByCalendar(String schemaName) {
        if(!isRunning) {
            try {
                //加锁，防止定时器重复执行，
                isRunning = true;
                workOrderJobService.cronJobToGenerateWorkOrderByCalendar(schemaName);
            } catch (Exception e) {
                logger.error("WorkOrderJobServiceImpl.asyncCronJobToGerenateWorkOrderByCalendar fail ~ ", e);
            } finally {
                //运行完后释放锁
                isRunning = false;
            }
        }
    }
}
