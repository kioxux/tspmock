package com.gengyun.senscloud.service.login;

import com.gengyun.senscloud.entity.LoginLogEntity;
import com.gengyun.senscloud.model.MethodParam;

/**
 * 登录token处理
 * User: Wudang Dong
 * Date: 2019/07/15
 * Time: 上午11:42
 */
public interface ClientTokenService {
    /**
     * 新增登录记录
     *
     * @param methodParam 系统参数
     * @param loginLog    登录记录
     * @return 成功数量
     */
    int newLoginLog(MethodParam methodParam, LoginLogEntity loginLog);

    /**
     * token验证（已缓存）
     *
     * @param schemaName 企业库
     * @param clientName 客户端
     * @param token      token
     * @return 数量
     */
    int getTokenCacheCount(String schemaName, String clientName, String token);

    /**
     * 根据token查找用户账号
     *
     * @param methodParam 系统参数
     * @return 账号
     */
    String getLoginAccountByToken(MethodParam methodParam);
}
