package com.gengyun.senscloud.service.asset.impl;


import com.gengyun.senscloud.service.asset.AssetInventoryService;
import org.springframework.stereotype.Service;

/**
 * 设备盘点管理
 */
@Service
public class AssetInventoryServiceImpl implements AssetInventoryService {
//
//    private static final Logger logger = LoggerFactory.getLogger(AssetInventoryServiceImpl.class);
//
//    @Autowired
//    AssetInventoryMapper assetInventoryMapper;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private CommonUtilService commonUtilService;
//
//    @Autowired
//    private DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    private WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    private LogsService logService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    @Autowired
//    private WorkProcessService workProcessService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private WorkSheetHandleMapper workSheetHandleMapper;
//
////    @Autowired
////    private AssetStockListMapper bomStockListMapper;
//
//    @Autowired
//    private PagePermissionService pagePermissionService;
//
////    @Autowired
////    private AssetInStockService bomInStockService;
////
////    @Autowired
////    private AssetDiacardService bomDiacardService;
//
//    @Autowired
//    private FacilitiesMapper facilitiesMapper;
//
//    @Autowired
//    private AssetMapper assetMapper;
//
//    @Autowired
//    private AssetTransferService assetTransferService;
//
//    @Autowired
//    private AssetDiscardService assetDiscardService;
//
//    @Autowired
//    private MetadataWorkService metadataWorkService;
//
//    @Autowired
//    private MessageService messageService;
//
//
//    /**
//     * 根据code查询设备盘点数据
//     *
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryAssetInventoryByCode(String schemaName, String inventoryCode) {
//        return assetInventoryMapper.queryAssetInventoryByCode(schemaName, inventoryCode);
//    }
//
//    /**
//     * 根据code查询盘点位置数据
//     *
//     * @param schemaName
//     * @param subInventoryCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryAssetInventoryStockByCode(String schemaName, String subInventoryCode) {
//        Map<String, Object> bomInventoryStock = assetInventoryMapper.queryAssetInventoryOrgByCode(schemaName, subInventoryCode);
//        Map<String, Object> bomInventory = assetInventoryMapper.queryAssetInventoryByCode(schemaName, (String) bomInventoryStock.get("inventory_code"));
//        bomInventoryStock.put("inventory_name", bomInventory.get("inventory_name"));
//        bomInventoryStock.put("begin_time", bomInventory.get("begin_time"));
//        bomInventoryStock.put("deadline_time", bomInventory.get("deadline_time"));
//        return bomInventoryStock;
//    }
//
//    /**
//     * 查询设备盘点列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findAssetInventoryList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        //首页条件
//        String condition = "";
//        String startTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("startTime");
//        String endTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("endTime");
//        SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT);
//        if (StringUtils.isNotBlank(startTime)) {
//            try {
//                condition += " and bi.begin_time >= '" + df.format(df.parse(startTime)) + " 00:00:00' ";
//            } catch (ParseException e) {
//                logger.error("", e);
//            }
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            try {
//                Calendar cl = Calendar.getInstance();
//                cl.setTime(new SimpleDateFormat(Constants.DATE_FMT_SS).parse(endTime));
//                cl.add(Calendar.DAY_OF_MONTH, 1);
//                condition += " and bi.begin_time <'" + df.format(cl.getTime()) + " 00:00:00' ";
//            } catch (ParseException e) {
//            }
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = assetInventoryMapper.getAssetInventoryList(schema_name, condition, pageSize, begin);
//        int total = assetInventoryMapper.countAssetInventoryList(schema_name, condition);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 设备盘点报废
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel invalidInventory(HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String inventoryCode = request.getParameter("inventoryCode");
//        Map<String, Object> map = assetInventoryMapper.queryAssetInventoryByCode(schemaName, inventoryCode);
//        if (map == null || (Integer) map.get("status") == StatusConstant.COMPLETED || (Integer) map.get("status") == StatusConstant.CANCEL) {
//            ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_INVENTORY_DATA_ERROR));//设备盘点信息异常，报废失败
//        }
//        int result = assetInventoryMapper.updateAssetInventoryStatus(schemaName, StatusConstant.CANCEL, inventoryCode, null);
//        if (result > 0) {
//            String condition = " and inventory_code = '" + inventoryCode + "' ";
//            result = assetInventoryMapper.updateAssetInventoryOrgStatus(schemaName, StatusConstant.CANCEL, null, condition);
//            if (result > 0) {
//                //关闭流程(主流程以及所有子流程)
//                List<Map<String, Object>> orgList = assetInventoryMapper.queryAssetInventoryOrgListByCode(schemaName, inventoryCode);
//                if (orgList != null) {
//                    for (Map<String, Object> org : orgList) {
//                        String sub_inventory_code = (String) org.get("sub_inventory_code");
//                        boolean isSuccess = workflowService.deleteInstancesBySubWorkCode(schemaName, sub_inventory_code, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_CANCEL));//盘点作废
//                        if (!isSuccess) {
//                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_CANCEL_FLOW_ERROR));//盘点作废流程调用失败
//                        }
//                        logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), sub_inventory_code, selectOptionService.getLanguageInfo(LangConstant.INV_FINISHED), user.getAccount());//记录历史；盘点完成
//                    }
//                }
//                //关闭流程
//                boolean isSuccess = workflowService.deleteInstancesBySubWorkCode(schemaName, inventoryCode, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_CANCEL));//盘点作废
//                if (!isSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_CANCEL_FLOW_ERROR));//盘点作废流程调用失败
//                }
//                logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), inventoryCode, selectOptionService.getLanguageInfo(LangConstant.INV_FINISHED), user.getAccount());//记录历史；盘点完成
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_OBSOLETE_SUCCESS));//作废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            }
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DISCARD_FAIL));//报废失败
//    }
//
//    /**
//     * 查询设备盘点位置列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findInventoryOrgList(HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        String inventoryCode = request.getParameter("inventoryCode");
//        String assetOrg = request.getParameter("assetOrg");
//        String keyword = request.getParameter("keyword");
//        if (StringUtils.isBlank(inventoryCode))
//            return "{}";
//
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        //首页条件
//        String condition = "";
//        if (StringUtils.isNotBlank(assetOrg)) {
//            String[] orgIds = assetOrg.split(",");
//            condition += " AND f.id in (";
//            for (String orgId : orgIds) {
//                condition += orgId + ",";
//            }
//            condition = condition.substring(0, condition.length() - 1);
//            condition += ") ";
//        }
//        if (StringUtils.isNotBlank(keyword)) {
//            condition += " AND u.username LIKE '%" + keyword + "%' ";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = assetInventoryMapper.getAssetInventoryOrgList(schema_name, inventoryCode, condition, pageSize, begin);
//        int total = assetInventoryMapper.countAssetInventoryOrgList(schema_name, inventoryCode, condition);
//        //查询获取流程taskId、按钮权限等数据
//        handlerFlowData(schema_name, user.getAccount(), dataList);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();//记录每条盘点数据对应的task
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("sub_inventory_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        //把taskId、权限数据补充到列表数据中去
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("sub_inventory_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//    /**
//     * 查询盘点设备列表
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public String findInventoryOrgDetailList(HttpServletRequest request, String schema_name) {
//        String subInventoryCode = request.getParameter("subInventoryCode");
//        String keyword = request.getParameter("keyword");
//        String status = request.getParameter("status");//1、在库 2、盈余 3、亏损（未盘点）
//        if (StringUtils.isBlank(subInventoryCode))
//            return "{}";
//
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        JSONObject result = new JSONObject();
//        //首页条件
//        String condition = "";
//        if (StringUtils.isNotBlank(keyword)) {
//            condition += " AND (u.username LIKE '%" + keyword + "%' OR iod.asset_name LIKE '%" + keyword + "%') ";
//        }
//        if ("1".equals(status) || "2".equals(status) || "3".equals(status)) {
//            condition += " AND iod.in_status = " + status + " ";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = assetInventoryMapper.getAssetInventoryOrgDetailList(schema_name, subInventoryCode, condition, pageSize, begin);
//        int total = assetInventoryMapper.countAssetInventoryOrgDetailList(schema_name, subInventoryCode, condition);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 启动盘点
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel addInventory(String schemaName, HttpServletRequest request, String processDefinitionId, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        int status = StatusConstant.PENDING; //默认待盘点状态
//        Timestamp createTime = new Timestamp(System.currentTimeMillis());
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schemaName, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> asset_inventory_org_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_inventory_org");
//        Map<String, Object> asset_inventory_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_inventory");
//        String inventoryCode = (String) asset_inventory_tmp.get("inventory_code");
//
//        //解析处理设备类型
//        Object bodyProperty = paramMap.get("body_property"); // 页面所有模板相关信息
//        JSONArray arrays = JSONArray.fromObject(bodyProperty);
//        String fieldCode = null;
//        String assetCategoryIds = "";
//        for (Object object : arrays) {
//            net.sf.json.JSONObject data = net.sf.json.JSONObject.fromObject(object);
//            fieldCode = data.get("fieldCode").toString();
//            if ("asset_category_id".equals(fieldCode)) {
//                assetCategoryIds = data.getString("fieldValue");
//                break;
//            }
//        }
//
//        //解析处理分拨中心
//        net.sf.json.JSONArray facilityArrays = (net.sf.json.JSONArray) map_object.get("bomContent24");
//        StringBuffer facilityIdStr = new StringBuffer();
//        Map<Long, String> facilityIdToDutyManMap = new HashMap<>();
//        if (do_flow_key != null && !do_flow_key.equals("")) {
//            for (int i = 0; i < facilityArrays.size(); i++) {
//                net.sf.json.JSONObject facility = facilityArrays.getJSONObject(i);
//                if (facilityIdStr.length() > 0)
//                    facilityIdStr.append(",");
//
//                facilityIdStr.append(facility.getString("code"));
//                String dutyMan = facility.optString("duty_man", null);
//                if (StringUtils.isNotBlank(dutyMan))
//                    facilityIdToDutyManMap.put(facility.getLong("code"), dutyMan);
//            }
//            //查询分拨中心是否已经在盘点中
//            int doingCount = assetInventoryMapper.countDoingAssetInventoryOrg(schemaName, facilityIdStr.toString());
//            if (doingCount > 0) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FACILITY_INVENTORY_REPEAT));//分拨中心已经在盘点中，不能重复盘点
//            }
//            //提交启动盘点表单数据
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//                if (!asset_inventory_tmp.containsKey("begin_time") || StringUtils.isBlank((String) asset_inventory_tmp.get("begin_time"))) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.START_DATE) + SensConstant.REQUIRED_MSG);//开始日期
//                }
//                if (!asset_inventory_tmp.containsKey("deadline_time") || StringUtils.isBlank((String) asset_inventory_tmp.get("deadline_time"))) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.CLOSING_DATE) + SensConstant.REQUIRED_MSG);//截止日期
//                }
//                SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT_SS);
//                asset_inventory_tmp.put("begin_time", new Timestamp(df.parse((String) asset_inventory_tmp.get("begin_time")).getTime()));
//                asset_inventory_tmp.put("deadline_time", new Timestamp(df.parse((String) asset_inventory_tmp.get("deadline_time")).getTime()));
//                asset_inventory_tmp.put("create_time", createTime);
//                asset_inventory_tmp.put("create_user_account", user.getAccount());
//                asset_inventory_tmp.put("status", status);
//                asset_inventory_tmp.put("inventory_count", 0);
//                asset_inventory_tmp.put("record_count", 0);//更新设备盘点总设备数量
//                asset_inventory_tmp.put("balance_count", 0);
//                asset_inventory_org_tmp.put("inventory_code", asset_inventory_tmp.get("inventory_code"));
//                asset_inventory_org_tmp.put("create_time", createTime);
//                asset_inventory_org_tmp.put("create_user_account", user.getAccount());
//                asset_inventory_org_tmp.put("inventory_count", 0);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//            }
//        }
//        //查询解析 盘点位置、盘点设备列表数据
//        boolean result = handlerAssetInventoryOrgDetail(schemaName, dataInfo, asset_inventory_tmp, asset_inventory_org_tmp, inventoryCode, user.getAccount(), createTime, facilityIdStr.toString(), assetCategoryIds, facilityIdToDutyManMap);
//        if (!result)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FACILITY_NOT_NEED_INVENTORY));//分拨中心下没有查询到任何设备，无需盘点
//
//        dataInfo.put("_sc_asset_inventory", asset_inventory_tmp);
//        map_object.put("dataInfo", dataInfo);
//        try {
//            String flow_data = doSaveData(schemaName, map_object);//保存新建工单
//
//            //流程数据
//            Map map = new HashMap();
//            if (StringUtils.isNotEmpty(flow_data)) {
//                map.put("flow_data", flow_data);
//            }
//            map.put("status", status);
//            map.put("sub_work_code", inventoryCode);
//            map.put("workCode", inventoryCode);
//            map.put("do_flow_key", do_flow_key);
//            map.put("create_user_account", user.getAccount());
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schemaName, processDefinitionId, user.getAccount(), map);
//            if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_START_ASSET_INVENTORY_FLOW_FAIL));//启动设备盘点流程启动失败
//            }
//            if (StringUtil.isNotEmpty(flow_data)) {
//                org.json.JSONArray flowDatas = new org.json.JSONArray(flow_data);
//                if(flowDatas.length() > 0){
//                    for(int i=0;i<flowDatas.length();i++){
//                        JSONObject flowData = flowDatas.getJSONObject(i);
//                        String receive_account = flowData.optString("receive_account");
//                        messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_24, flowData.optString("sub_work_code"),
//                                user.getAccount(), user.getUsername(), null, null); // 发送短信
//                    }
//                }
//            }
//            List<Map<String, Object>> inventoryOrgList = (List<Map<String, Object>>) dataInfo.get("asset_inventory_org_list_tmp");
//            for (Map<String, Object> inventoryOrg : inventoryOrgList) {
//                String subInventoryCode = (String) inventoryOrg.get("sub_inventory_code");
//                logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), subInventoryCode, selectOptionService.getLanguageInfo(LangConstant.START_ASSET_INVENTORY), user.getAccount());//记录历史；启动设备盘点
//            }
//            logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), inventoryCode, selectOptionService.getLanguageInfo(LangConstant.START_ASSET_INVENTORY), user.getAccount());//记录历史；启动设备盘点
//            return ResponseModel.ok("ok");
//        } catch (Exception exp) {
//            throw new Exception(exp);
//        }
//    }
//
//    /**
//     * PC盘点
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel pcInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String inventoryCode = null;
//        String subInventoryCode = null;
//        Integer orgId = null;
//        JSONArray assetArray = null;
//        Object bodyProperty = paramMap.get("body_property"); // 页面所有模板相关信息
//        JSONArray arrays = JSONArray.fromObject(bodyProperty);
//        String fieldCode = null;
//        for (Object object : arrays) {
//            net.sf.json.JSONObject data = net.sf.json.JSONObject.fromObject(object);
//            fieldCode = data.get("fieldCode").toString();
//            if ("relation_id".equals(fieldCode)) {
//                assetArray = (JSONArray)data.get("relationContent");
//            } else if ("inventory_code".equals(fieldCode)) {
//                inventoryCode = data.getString("fieldValue");
//            } else if ("sub_inventory_code".equals(fieldCode)) {
//                subInventoryCode = data.getString("fieldValue");
//            } else if ("facility_id".equals(fieldCode)) {
//                orgId = Integer.valueOf(data.getString("fieldValue"));
//            }
//        }
//        if (assetArray == null || assetArray.size() == 0) {
//            return ResponseModel.ok("ok");
//        }
//        if (StringUtils.isBlank(inventoryCode) || StringUtils.isBlank(subInventoryCode) || orgId == null || orgId.intValue() <= 0)
////        if (StringUtils.isBlank(inventoryCode) || StringUtils.isBlank(subInventoryCode) || orgId == null || orgId.intValue() <= 0 || assetArray == null || assetArray.size() == 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PC_INVENTORY_FAIL));//PC盘点失败
//
//        //查询分拨中心信息
//        Facility facility = facilitiesMapper.FacilitiesById(schemaName, orgId);
//        if (facility == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FACILITY_DATA_ERROR_INVENTORY_FAIL));//分拨中心数据异常，PC盘点失败
//
//        Map<String, Object> org = assetInventoryMapper.queryAssetInventoryOrgByCode(schemaName, subInventoryCode);
//        if (org == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_INVENTORY_FAIL));//数据异常，PC盘点失败
//
//        if (!user.getAccount().equals(org.get("duty_man")))//只有登录账户是盘点位置表中的盘点人时，才能进行盘点操作
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_PERMISSION_INVENTORY));//当前登录账户没有盘点权限，PC盘点失败
//
//        BigDecimal totalCount = BigDecimal.ZERO;//本次盘点设备总数
//        List<Map<String, Object>> newInventoryOrgDetailList = new ArrayList<>();
//        for (int i = 0; i < assetArray.size(); i++) {
//            net.sf.json.JSONObject assetJson = assetArray.getJSONObject(i);
//            String assetCode = assetJson.getString("strcode");
//            String assetName = assetJson.getString("strname");
//            List<Asset> assets = assetMapper.findByCode(schemaName, assetCode);
//            if (assets == null || assets.size() == 0 || assets.get(0).getIntstatus() <= 0) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOT_EXIST), assetName));//设备“%s”不存在，请先录入到系统再进行盘点
//            }
//            Asset asset = assets.get(0);
//            //查询设备盘点明细表
//            Map<String, Object> map = assetInventoryMapper.queryAssetInventoryOrgDetailByCode(schemaName, subInventoryCode, orgId, asset.get_id());
//            if (map == null) {
//                List<Long> assetOrgIds = facilitiesMapper.getFacilitiesByAssetId(schemaName, asset.get_id());
//                if (!assetOrgIds.contains(Long.valueOf(orgId))) {//如果设备盘点明细表中没有此设备，且该设备不是这个组织下的，则添加一条设备盘点明细数据，并标记盘点状态为“盈余”
//                    Map<String, Object> newInventoryOrgDetail = new HashMap<>();
//                    newInventoryOrgDetail.put("inventory_code", inventoryCode);
//                    newInventoryOrgDetail.put("sub_inventory_code", subInventoryCode);
//                    newInventoryOrgDetail.put("in_status", 2);//盈余
//                    newInventoryOrgDetail.put("deal_result", 0);
//                    newInventoryOrgDetail.put("create_time", new Date());
//                    newInventoryOrgDetail.put("create_user_account", user.getAccount());
//                    newInventoryOrgDetail.put("status", StatusConstant.PENDING);//待盘点
//                    newInventoryOrgDetail.put("facility_id", orgId);
//                    newInventoryOrgDetail.put("asset_id", asset.get_id());
//                    newInventoryOrgDetail.put("asset_code", asset.getStrcode());
//                    newInventoryOrgDetail.put("asset_name", asset.getStrname());
//                    newInventoryOrgDetailList.add(newInventoryOrgDetail);
//                }
//            } else {
//                if ((Integer) map.get("in_status") != 3) {//非默认状态的设备已经盘点过，无需再处理
//                    continue;
//                }
//                if ((Integer) map.get("deal_result") != 0) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_ALREADY_INVENTORY), map.get("asset_code")));//设备(%s)已经盘点并处理完毕，不能再次盘点
//                }
//                map.put("finished_time", new Date());
//                map.put("in_status", 1);//在库
//                int result = assetInventoryMapper.updateAssetInventoryOrgDetail(schemaName, map);
//                if (result == 0) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PC_INVENTORY_FAIL));//PC盘点失败
//                }
//            }
//            totalCount = totalCount.add(new BigDecimal(1));
//        }
//        //批量导入设备盘点明细表数据
//        selectOptionService.batchInsertDatas(schemaName, newInventoryOrgDetailList, "_sc_asset_inventory_org_detail", SqlConstant._sc_asset_inventory_org_detail_columns);
//        //同步更新设备盘点表、盘点位置表的累计盘点数量
//        assetInventoryMapper.updateAssetInventoryCount(schemaName, inventoryCode, totalCount);
//        assetInventoryMapper.updateAssetInventoryOrgCount(schemaName, subInventoryCode, totalCount);
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 盘点分配
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel inventoryAllot(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//
//        String next_operator = request.getParameter("next_operator");//按钮类型
//        String subInventoryCode = request.getParameter("subWorkCode");
//        if (StringUtils.isBlank(next_operator) || "-1".equals(next_operator))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATOR_IS_NULL));//操作人不能为空，盘点分配失败
//
//        Map<String, Object> org = assetInventoryMapper.queryAssetInventoryOrgByCode(schemaName, subInventoryCode);
//        if (org == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_INVENTORY_ALLOT_FAIL));//数据异常，盘点分配失败
//
//        if ((Integer) org.get("status") == StatusConstant.COMPLETED)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FACILITY_INVENTORY_FINISHED_ALLOT_FAIL));//分拨中心盘点已完成，盘点分配失败
//
//        int result = assetInventoryMapper.updateAssetOrgDutyMan(schemaName, next_operator, StatusConstant.PENDING, subInventoryCode);
//        if (result > 0) {
//            //更新流程中的分配人信息
//            boolean isSuccess = workflowService.assignWithSWC(schemaName, subInventoryCode, next_operator, new HashMap());
//            if (!isSuccess) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_ALLOT_FLOW_FAIL));//盘点分配流程调用失败
//            }
//            logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), subInventoryCode, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_ALLOT), user.getAccount());//记录历史;盘点分配
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_ALLOT_FAIL));//盘点分配失败
//        }
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 查询解析 盘点位置、盘点设备列表数据
//     *
//     * @param schemaName
//     * @param asset_inventory_org_tmp
//     * @param inventoryCode
//     * @param userAccount
//     * @param createTime
//     * @return
//     */
//    private boolean handlerAssetInventoryOrgDetail(String schemaName, Map<String, Object> dataInfo, Map<String, Object> asset_inventory_tmp, Map<String, Object> asset_inventory_org_tmp,
//                                                   String inventoryCode, String userAccount, Timestamp createTime, String facilityStr, String assetCategoryIds, Map<Long, String> facilityIdToDutyManMap) {
//        Map<Long, Object> facilityIdToSubInventoryCodeMap = new HashMap<>();//分拨中心code、盘点位置表code关系映射
//        Map<Long, String> facilityIdToFacilityNameMap = new HashMap<>();//分拨中心code、分拨中心名称关系映射
//        List<Map<String, Object>> asset_inventory_org_list_tmp = new ArrayList<>();//盘点位置表数据
//        String[] facilityIds = facilityStr.split(",");
//        for (int i = 0; i < facilityIds.length; i++) {
//            //每个分拨中心生成一条盘点子表数据
//            Long facilityId = Long.valueOf(facilityIds[i]);
//            HashMap<String, Object> asset_inventory_org_map = new HashMap<>();
//            asset_inventory_org_map.putAll(asset_inventory_org_tmp);
//            asset_inventory_org_map.put("facility_id", facilityId);
//            if (i > 0)//多个分拨中心时，要给除第一条意外的设备分拨中心表补上主键code
//                asset_inventory_org_map.put("sub_inventory_code", dynamicCommonService.getSubAssetInventoryCode(schemaName));
//
//            facilityIdToSubInventoryCodeMap.put(facilityId, asset_inventory_org_map.get("sub_inventory_code"));//记录分拨中心对应的盘点位置表主键code
//            asset_inventory_org_list_tmp.add(asset_inventory_org_map);
//        }
//        //解析设备类型
//        String condition = "";
//        List<Map<String, Object>> asset_category_list_tmp = new ArrayList<>();//盘点设备类别表数据
//        if (StringUtils.isNotBlank(assetCategoryIds)) {
//            condition = " AND a.category_id IN (" + assetCategoryIds + ")";
//            String[] assetCategoryIdArray = assetCategoryIds.split(",");
//            for (String assetCategoryId : assetCategoryIdArray) {
//                Map<String, Object> asset_category_tmp = new HashMap<>();
//                asset_category_tmp.put("asset_category_id", assetCategoryId);
//                asset_category_tmp.put("inventory_code", inventoryCode);
//                asset_category_list_tmp.add(asset_category_tmp);
//            }
//        }
//        //根据分拨中心code（多选），查询分拨中心设备列表
//        List<Map<String, Object>> results = assetInventoryMapper.queryAssetByFacilityIds(schemaName, facilityStr, condition);
//        if (results == null || results.size() == 0)
//            return false;
//
//        int totalRecordCount = 0;//盘点设备总数
//        Map<Long, Integer> orgRecordCountMap = new HashMap<>();//分拨中心设备总数映射表
//        List<Map<String, Object>> asset_inventory_org_detail_list_tmp = new ArrayList<>();//盘点设备表数据
//        for (Map<String, Object> result : results) {
//            Long facility_id = (Long) result.get("facility_id");
//            result.put("inventory_code", inventoryCode);
//            result.put("sub_inventory_code", facilityIdToSubInventoryCodeMap.get(facility_id));
//            result.put("in_status", 3);//默认3（亏损）
//            result.put("deal_result", 0);
//            result.put("create_time", createTime);
//            result.put("create_user_account", userAccount);
//            result.put("status", StatusConstant.PENDING);//待盘点
//            facilityIdToFacilityNameMap.put(facility_id, (String) result.get("short_title"));//记录分拨中心对应的名称
//            asset_inventory_org_detail_list_tmp.add(result);
//            //累计计算每个分拨中心的设备总数
//            if (orgRecordCountMap.containsKey(facility_id)) {
//                orgRecordCountMap.put(facility_id, orgRecordCountMap.get(facility_id) + 1);
//            } else {
//                orgRecordCountMap.put(facility_id, 1);
//            }
//            totalRecordCount += 1;//累计总设备数
//        }
//
//        Iterator<Map<String, Object>> iter = asset_inventory_org_list_tmp.iterator();
//        while (iter.hasNext()) {
//            Map<String, Object> asset_inventory_org = iter.next();
//            Long facilityId = (Long) asset_inventory_org.get("facility_id");
//            if (!orgRecordCountMap.containsKey(facilityId)) {//如果分拨中心中没有任何设备，则该分拨中心无需加到盘点中去，剔除该分拨中心
//                iter.remove();
//                continue;
//            }
//            Object count = orgRecordCountMap.get(facilityId);
//            //更新盘点位置表的分拨中心设备总数
//            asset_inventory_org.put("record_count", count);
//            asset_inventory_org.put("balance_count", count);
//            asset_inventory_org.put("short_title", facilityIdToFacilityNameMap.get(facilityId));
//            //补上分拨中心盘点人
//            String dutyMan = facilityIdToDutyManMap.get(facilityId);
//            if (StringUtils.isBlank(dutyMan)) {
//                asset_inventory_org.put("status", StatusConstant.PENDING_ORDER);
//                asset_inventory_org.put("duty_man", "");
//            } else {
//                asset_inventory_org.put("status", StatusConstant.PENDING);
//                asset_inventory_org.put("duty_man", dutyMan);
//            }
//        }
//        asset_inventory_tmp.put("record_count", totalRecordCount);//更新设备盘点总设备数量
//        asset_inventory_tmp.put("balance_count", totalRecordCount);
//        dataInfo.put("asset_category_list_tmp", asset_category_list_tmp);
//        dataInfo.put("asset_inventory_org_list_tmp", asset_inventory_org_list_tmp);
//        dataInfo.put("asset_inventory_org_detail_list_tmp", asset_inventory_org_detail_list_tmp);
//        return true;
//    }
//
//    /**
//     * 保存数据
//     *
//     * @param schemaName
//     * @param map        : keys：数据主键，dataInfo：数据
//     */
//    private String doSaveData(String schemaName, Map<String, Object> map) {
//        Map<String, Map<String, Object>> dataInfo = (Map<String, Map<String, Object>>) map.get("dataInfo");
//        Map<String, Object> assetInventory = dataInfo.get("_sc_asset_inventory");
//        dynamicCommonService.handleTableColumnTypes(assetInventory);//补充插入数据字段类型
//        workSheetHandleMapper.insert(schemaName, assetInventory, "_sc_asset_inventory"); //保存设备盘点主表信息
//
//        List<Map<String, Object>> assetCategoryList = (List<Map<String, Object>>) dataInfo.get("asset_category_list_tmp");
//        List<Map<String, Object>> inventoryOrgList = (List<Map<String, Object>>) dataInfo.get("asset_inventory_org_list_tmp");
//        List<Map<String, Object>> inventoryOrgDetailList = (List<Map<String, Object>>) dataInfo.get("asset_inventory_org_detail_list_tmp");
//        //批量导入设备盘点位置表数据
//        selectOptionService.batchInsertDatas(schemaName, inventoryOrgList, "_sc_asset_inventory_org", SqlConstant._sc_asset_inventory_org_columns);
//        //批量导入设备盘点明细表数据
//        selectOptionService.batchInsertDatas(schemaName, inventoryOrgDetailList, "_sc_asset_inventory_org_detail", SqlConstant._sc_asset_inventory_org_detail_columns);
//        //批量导入设备盘点设备类型表数据
//        selectOptionService.batchInsertDatas(schemaName, assetCategoryList, "_sc_asset_inventory_asset_category", SqlConstant._sc_asset_inventory_asset_category_columns);
//        if (inventoryOrgList != null) {
//            List<FlowData> flowDataList = new ArrayList<FlowData>(); // 流程数据集合
//            for (Map<String, Object> inventoryOrg : inventoryOrgList) {
//                // 流程数据
//                FlowData fd = new FlowData();
//                fd.setName(inventoryOrg.get("short_title") + selectOptionService.getLanguageInfo(LangConstant.INVEN_ATORY)); // title;盘点
//                if (StringUtils.isNotBlank((String) inventoryOrg.get("duty_man")) && !"-1".equals(inventoryOrg.get("duty_man"))) {
//                    fd.setReceive_account((String) inventoryOrg.get("duty_man")); // 用户
//                }
//                fd.setSub_work_code((String) inventoryOrg.get("sub_inventory_code")); // 主键
//                fd.setOrder(-1); // 并行
//                fd.setFacility_id(String.valueOf(inventoryOrg.get("facility_id"))); // 所属位置
//                fd.setDeadline_time(assetInventory.get("deadline_time").toString()); // 截止时间
//                fd.setCreate_time(assetInventory.get("begin_time").toString()); // 发生时间
//                flowDataList.add(fd);
//            }
//            return JSON.toJSONString(flowDataList);
//        }
//        return null;
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String subInventoryCode = request.getParameter("sub_inventory_code");
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        Map<String, Object> assetInventoryOrg = assetInventoryMapper.queryAssetInventoryOrgByCode(schemaName, subInventoryCode);
//        int status = (Integer) assetInventoryOrg.get("status");
//        if (StatusConstant.COMPLETED != status && StatusConstant.CANCEL != status) {//未完成的盘点单可以分配
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_24, "code");
//        String whereString = "and(template_type=3 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/asset_inventory/findSingleAidInfo")
//                .addObject("workCode", subInventoryCode)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", subInventoryCode)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 盘点盈余设备的调整
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel assetTransfer(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        String subInventoryCode = request.getParameter("sub_inventory_code");
//        String facilityId = request.getParameter("facilityId");
//        String oldFacilityId = request.getParameter("oldFacilityId");
//        String assetId = request.getParameter("asset_id");
//
//        Map<String, Object> asset = assetMapper.getAssetByAssetId(schemaName, assetId);
//        if (asset == null)
//            ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOT_EXIST_TRANS_FAIL));//设备不存在，设备调拨失败
//
//        Map<String, Object> asset_transfer_tmp = new HashMap<>();
//        Map<String, Object> asset_transfer_detail_tmp = new HashMap<>();
//        asset_transfer_tmp.put("transfer_code", dynamicCommonService.getAssetTransferCode(schemaName));
//        asset_transfer_tmp.put("new_position_id", facilityId);
//        asset_transfer_tmp.put("position_id", oldFacilityId.split(",")[0]);
//        asset_transfer_detail_tmp.put("relation_id", assetId);
//
//        String work_request_type = "";
//        //获取body_property、flow_id、roleIds数据
//        List<Object> body_property = null;
//        String flow_id = null;
//        Object roleIds = null;
//        String inv_position_id = (String) asset_transfer_tmp.get("position_id");
//        try {
//            Map<String, Object> workTypes = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_22);//这里查询的是设备调拨的流程模板
//            flow_id = (String) workTypes.get("relation");
//        } catch (Exception e) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_INVENTORY_FLOW_ID_ERROR));//设备盘点业务类型获取失败
//        }
//        String formKey = workflowService.getStartFormKey(schemaName, flow_id);
//        MetadataWork mateWork = (MetadataWork) metadataWorkService.queryById(schemaName, formKey).getContent();
//        body_property = JSON.parseArray(mateWork.getBodyProperty());//自定义字段号集合
//        for (Object bp : body_property) {
//            com.alibaba.fastjson.JSONObject property = (com.alibaba.fastjson.JSONObject) bp;
//            String fieldCode = property.getString("fieldCode");
//            paramMap.put(fieldCode, property.get("fieldValue")); // TODO 取值待优化
//            if ("relation_id".equals(fieldCode)) {
//                String fieldFormCode = property.getString("fieldFormCode");
//                com.alibaba.fastjson.JSONArray jsonArray = new com.alibaba.fastjson.JSONArray();
//                com.alibaba.fastjson.JSONObject json = new com.alibaba.fastjson.JSONObject();
//                json.put("_id", assetId);
//                json.put("strcode", asset.get("strcode"));
//                json.put("strname", asset.get("strname"));
//                json.put("site_name", asset.get("site_name"));
//                json.put("model_name", asset.get("model_name"));
//                json.put("enable_time", asset.get("enable_time"));
//                json.put("nowModalKey", fieldFormCode);
//                json.put("category_name", asset.get("category_name"));
//                json.put("int_status_name", asset.get("int_status_name"));
//                json.put("asset_code_new", request.getParameter("assetCodeNew"));
//                jsonArray.add(json);
//                property.put("relationContent", jsonArray);
//                paramMap.put("relationIdContent", jsonArray);
//            } else if ("inv_position_id".equals(fieldCode)) {
//                property.put("fieldValue", inv_position_id);
//            } else if ("work_request_type".equals(fieldCode)) {
//                work_request_type = property.getString("fieldValue");
//            }
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        return assetTransferService.addTransfer(schemaName, String.valueOf(ButtonConstant.BTN_SUBMIT), user, flow_id, paramMap, asset_transfer_tmp,
//                asset_transfer_detail_tmp, body_property.toString(), inv_position_id, subInventoryCode, true, hasAudit);
//    }
//
//    /**
//     * 盘点盈余设备的报废
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel assetDiscard(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        String subInventoryCode = request.getParameter("sub_inventory_code");
//        String facilityId = request.getParameter("facilityId");
//        String assetId = request.getParameter("asset_id");
//        String leave_cost = request.getParameter("leaveCost");
//        Map<String, Object> asset = assetMapper.getAssetByAssetId(schemaName, assetId);
//        if (asset == null)
//            ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOT_EXIST_DISCARD_FAIL));//设备不存在，设备报废失败
//
//        Map<String, Object> asset_discard_tmp = new HashMap<>();
//        Map<String, Object> asset_discard_detail_tmp = new HashMap<>();
//        asset_discard_tmp.put("discard_code", dynamicCommonService.getAssetDiscardCode(schemaName));
//        asset_discard_tmp.put("position_id", facilityId);
//        asset_discard_detail_tmp.put("relation_id", assetId);
//
//        String work_request_type = "";
//        //获取body_property、flow_id、roleIds数据
//        List<Object> body_property = null;
//        String flow_id = null;
//        try {
//            Map<String, Object> workTypes = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_23);//这里查询的是设备报废的流程模板
//            flow_id = (String) workTypes.get("relation");
//        } catch (Exception e) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_INVENTORY_FLOW_ID_ERROR));//设备盘点业务类型获取失败
//        }
//        String formKey = workflowService.getStartFormKey(schemaName, flow_id);
//        MetadataWork mateWork = (MetadataWork) metadataWorkService.queryById(schemaName, formKey).getContent();
//        body_property = JSON.parseArray(mateWork.getBodyProperty());//自定义字段号集合
//        for (Object bp : body_property) {
//            com.alibaba.fastjson.JSONObject property = (com.alibaba.fastjson.JSONObject) bp;
//            String fieldCode = property.getString("fieldCode");
//            paramMap.put(fieldCode, property.get("fieldValue")); // TODO 取值待优化
//            if ("relation_id".equals(fieldCode)) {
//                String fieldFormCode = property.getString("fieldFormCode");
//                com.alibaba.fastjson.JSONArray jsonArray = new com.alibaba.fastjson.JSONArray();
//                com.alibaba.fastjson.JSONObject json = new com.alibaba.fastjson.JSONObject();
//                json.put("_id", assetId);
//                json.put("strcode", asset.get("strcode"));
//                json.put("strname", asset.get("strname"));
//                json.put("site_name", asset.get("site_name"));
//                json.put("leave_cost", leave_cost);
//                json.put("enable_time", asset.get("enable_time"));
//                json.put("nowModalKey", fieldFormCode);
//                json.put("category_name", asset.get("category_name"));
//                json.put("int_status_name", asset.get("int_status_name"));
//                json.put("currency", paramMap.get("currency"));
//                json.put("asset_currency", paramMap.get("asset_currency"));
//                asset.put("currency", paramMap.get("currency"));
//                asset.put("asset_currency", paramMap.get("asset_currency"));
//                jsonArray.add(json);
//                property.put("relationContent", jsonArray);
//
//                asset.put("leave_cost", leave_cost);
//                asset.put("nowModalKey", fieldFormCode);
//            } else if ("work_request_type".equals(fieldCode)) {
//                work_request_type = property.getString("fieldValue");
//            }
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        List<Map<String, Object>> assetList = new ArrayList<>();
//        assetList.add(asset);
//        return assetDiscardService.addDiscard(schemaName, String.valueOf(ButtonConstant.BTN_SUBMIT), user, flow_id, paramMap, paramMap, asset_discard_tmp, asset_discard_detail_tmp,
//                assetList, body_property.toString(), subInventoryCode, true, hasAudit);
//    }
//
//    /**
//     * 盘点完成
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel finishInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) {
//        User user = AuthService.getLoginUser(request);
//        String inventoryCode = request.getParameter("inventoryCode");
//        String subInventoryCode = request.getParameter("subInventoryCode");
//        String taskId = request.getParameter("taskId");
//        if (StringUtils.isBlank(inventoryCode) || StringUtils.isBlank(subInventoryCode) || StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_INVENTORY_FINISH_FAIL));//参数异常，盘点完成操作失败
//
//        Map<String, Object> org = assetInventoryMapper.queryAssetInventoryOrgByCode(schemaName, subInventoryCode);
//        if (org == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_FACILITY_ERROR_FINISH_FAIL));//盘点位置信息异常，盘点完成操作失败
//
//        if ((Integer) org.get("status") == StatusConstant.COMPLETED || (Integer) org.get("status") == StatusConstant.CANCEL)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FACILITY_ALREADY_INVENTORY));//分拨中心已经完成盘点或者作废，不能重复操作
//
//        List<Map<String, Object>> orgDetails = assetInventoryMapper.queryAssetInventoryOrgDetailBySubInventoryCode(schemaName, subInventoryCode);
//        if (orgDetails == null || orgDetails.size() == 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_FINISH_FAIL));//盘点完成操作失败
//
//        boolean isFinish = true; //标识整个分拨中心是否已完成盘点
//        for (Map<String, Object> orgDetail : orgDetails) {
//            Timestamp finishedTime = (Timestamp) orgDetail.get("finished_time");
//            int inStatus = (Integer) orgDetail.get("in_status");
//            int dealResult = (Integer) orgDetail.get("deal_result");
//            if (!(inStatus == 1 || dealResult == 2 || dealResult == 3)
//                    || finishedTime == null) {//在库状态为“在库”或者处理结果为“已调整”、“已报废”,且完成时间不能为空时，才能完成盘点
//                isFinish = false; //只要有一个设备没有完成盘点处理，整个分拨中心就不能算盘点完成
//                break;
//            }
//        }
//        if (isFinish) {
//            Timestamp finishedTime = new Timestamp(System.currentTimeMillis());
//            String condition = " and sub_inventory_code = '" + subInventoryCode + "' ";
//            assetInventoryMapper.updateAssetInventoryOrgStatus(schemaName, StatusConstant.COMPLETED, finishedTime, condition);
//            //查询是否还有未处理完成的分拨中心
//            int unFinishedCount = assetInventoryMapper.countUnfinishedAssetInventoryOrg(schemaName, inventoryCode);
//            if (unFinishedCount == 0) {
//                assetInventoryMapper.updateAssetInventoryStatus(schemaName, StatusConstant.COMPLETED, inventoryCode, finishedTime);
//                logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), inventoryCode, selectOptionService.getLanguageInfo(LangConstant.INV_FINISHED), user.getAccount());//记录历史；盘点完成
//            }
//            Map<String, String> map = new HashMap<>();
//            map.put("sub_work_code", subInventoryCode);
//            map.put("title_page", selectOptionService.getLanguageInfo(LangConstant.FACILITY_INVENTORY_SUCCESS));//分拨中心盘点成功
//            //完成流程
//            boolean isSuccess = workflowService.complete(schemaName, taskId, user.getAccount(), map);
//            if (!isSuccess) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_FINISH_FLOW_FAIL));//盘点完成流程调用失败
//            }
//            logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_24), subInventoryCode, selectOptionService.getLanguageInfo(LangConstant.INV_FINISHED), user.getAccount());//记录历史;盘点完成
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOT_FINISH_FACILITY_FINISH_FAIL));//分拨中心设备还没有处理完，分拨中心不能标记为盘点完成
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_INVENTORY_FINISH));//设备盘点完成
//    }
//
//    /**
//     * 查询分拨中心下，需要盘点的设备总数
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel getAssetInventoryQuantity(String schemaName, HttpServletRequest request) {
//        String facilityIds = request.getParameter("facilityIds");
//        String assetCategoryIds = request.getParameter("assetCategoryIds");
//        if (StringUtils.isBlank(facilityIds))
//            return ResponseModel.ok(0);
//
//        String condition = "";
//        if (StringUtils.isNotBlank(assetCategoryIds)) {
//            condition = " AND a.category_id IN (" + assetCategoryIds + ")";
//        }
//        int quantity = assetInventoryMapper.getAssetInventoryQuantity(schemaName, facilityIds, condition);
//        return ResponseModel.ok(quantity);
//    }
//
//    /**
//     * 查询设备详情
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel getAssetFacilitiesById(String schemaName, HttpServletRequest request) {
//        String assetId = request.getParameter("assetId");
//        Map<String, Object> map = assetMapper.getAssetFacilitiesById(schemaName, assetId);
//        //多时区
//        SCTimeZoneUtil.responseMapDataHandler(map);
//        return ResponseModel.ok(map);
//    }
}
