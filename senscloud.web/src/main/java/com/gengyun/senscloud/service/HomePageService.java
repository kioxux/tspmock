package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

public interface HomePageService {
    /**
     * 获取首页的待办任务，今日上报，今日完成，超时任务数
     *
     * @param methodParam 入参
     * @return 获取首页的待办任务，今日上报，今日完成，超时任务数
     */
    Map<String, Object> getHomeWorksCount(MethodParam methodParam);

    /**
     * 首页的日历待办任务数
     *
     * @param methodParam 入参
     * @param month       入参
     * @return 首页的日历待办任务数
     */
    List<Map<String, Object>> getCalendarWorksCount(MethodParam methodParam, String month);

    /**
     * 首页显示的图表
     *
     * @param methodParam 入参
     * @return 首页显示的图表
     */
    List<Map<String, Object>> getStatisticList(MethodParam methodParam);

}
