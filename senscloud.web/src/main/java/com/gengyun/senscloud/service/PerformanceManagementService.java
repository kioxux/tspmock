package com.gengyun.senscloud.service;

public interface PerformanceManagementService {
//
//    /**
//     * 查询个人绩效列表
//     * @param request
//     * @return
//     */
//    public String findPerformanceList(HttpServletRequest request);
//
//    /**
//     * 查询个人绩效列表-获取考核项字段（bootstraptable动态列columns）
//     * @param request
//     * @return
//     */
//    public ResponseModel findPerformanceOptionList(HttpServletRequest request);
//
//    /**
//     * 评分页面-通过ID，查询个人绩效列表-获取考核项字段（bootstraptable动态列columns）注意：这里的分数是没有经过公式折算的原始评分分数
//     * @param request
//     * @return
//     */
//    public ResponseModel findPerformanceOptionListById(HttpServletRequest request);
//
//    /**
//     * 查询绩效详情
//     * @param request
//     * @return
//     */
//    public Map<String, Object> findPerformanceDetail(HttpServletRequest request);
//
//    /**
//     * 查询个人评分项列表
//     * @param request
//     * @return
//     */
//    public String findPerformanceItemsById(HttpServletRequest request);
//
//    /**
//     * 查询绩效模板列表
//     * @param request
//     * @return
//     */
//    public String findPerformanceTemplateList(HttpServletRequest request);
//
//    /**
//     * 查询绩效报告列表
//     * @param request
//     * @return
//     */
//    public String findPerformanceReportList(HttpServletRequest request);
//
//    /**
//     * 查询绩效报告用户列表
//     * @param request
//     * @return
//     */
//    public String findPerformanceReportUserList(HttpServletRequest request);
//
//    /**
//     * 绩效报告导出
//     * @param request
//     * @return
//     */
//    public ModelAndView performanceReportExport(HttpServletRequest request);
//
//    /**
//     * 绩效模板启用、禁用
//     * @param schemaName
//     * @param id
//     * @param status
//     * @return
//     */
//    public ResponseModel enableOrDisablePerformanceTemplateById(String schemaName, long id, String status);
//
//    /**
//     * 新增、编辑绩效考核模板
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    public ResponseModel doAddPerformanceTemplate(String schemaName, HttpServletRequest request);
//
//    /**
//     * 通过ID查询绩效模板详情
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    public ResponseModel getTemplateDetailById(String schemaName, long id);
//
//    /**
//     * 绩效评分
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    public ResponseModel doAddPerformanceScore(String schemaName, HttpServletRequest request);
//
//    /**
//     * 查询绩效模板列表
//     * @param request
//     * @return
//     */
//    public String findPerformancePlanList(HttpServletRequest request);
//
//    /**
//     * 绩效模板启用、禁用
//     * @param schemaName
//     * @param id
//     * @param status
//     * @return
//     */
//    public ResponseModel enableOrDisablePerformancePlanById(String schemaName, String id, String status);
//
//    /**
//     * 查询绩效计划详情
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    public Map<String, Object> queryPlanById(String schemaName, String subWorkCode);
//
//    /**
//     * 考核计划处理方法
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @param pageType
//     * @return
//     * @throws Exception
//     */
//    ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception;
//
//    /**
//     * 绩效计划执行处理方法
//     * @param schema_name
//     */
//    void performancePlanDeal(String schema_name);
//
//    /**
//     * 使用公式计算总分
//     * @param schema_name
//     * @param request
//     * @return
//     */
//    ResponseModel equationExecute(String schema_name, HttpServletRequest request);
}
