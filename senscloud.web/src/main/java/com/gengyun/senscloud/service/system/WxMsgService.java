package com.gengyun.senscloud.service.system;

import net.sf.json.JSONObject;

import java.util.Map;

/**
 * 微信消息管理
 * User: sps
 * Date: 2020/05/29
 * Time: 上午11:20
 */
public interface WxMsgService {
    /**
     * 微信消息发送
     *
     * @param schemaName
     * @param configInfo
     * @param openId
     * @param templateId
     * @param contentInfo
     */
    void sendWxMsg(String schemaName, Map<String, String> configInfo, String openId, String templateId, Map<String, Object> contentInfo);

    /**
     * 更新微信Token（定时任务）
     *
     * @param schemaName
     * @return
     */
    String checkWxAccessToken(String schemaName);

    /**
     * 获取小程序session_key
     *
     * @param schemaName
     * @param code
     */
    JSONObject getSessionKeyByCode(String schemaName, String code);

    /**
     * 更新用户微信主键
     *
     * @param schemaName 数据库
     * @param userId     用户主键
     * @param nickName   微信昵称
     * @param openid     微信小程序主键
     * @param unionId    微信用户主键
     */
    void updateUserWxOpenId(String schemaName, String userId, String nickName, String openid, String unionId);

    /**
     * 删除用户微信主键
     *
     * @param schemaName 数据库
     * @param userId     用户主键
     * @param openid     微信小程序主键
     */
    void removeUserWxOpenId(String schemaName, String userId, String openid);

}
