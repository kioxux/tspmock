package com.gengyun.senscloud.service.pollute;

import com.gengyun.senscloud.model.FeeItemModel;
import com.gengyun.senscloud.model.MethodParam;

import java.util.Map;

public interface FeeItemService {
    Map<String, Object> findPolluteFeeItemList(MethodParam methodParam, Map<String, Object> paramMap);

    void newPolluteFeeItem(MethodParam methodParam, Map<String, Object> paramMap);

    void modifyPolluteFeeItem(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> findById(MethodParam methodParam, FeeItemModel feeItemModel);

    void deletePolluteFeeItem(MethodParam methodParam, FeeItemModel feeItemModel);

    void modifyUsePolluteFeeItem(MethodParam methodParam, FeeItemModel feeItemModel);
}
