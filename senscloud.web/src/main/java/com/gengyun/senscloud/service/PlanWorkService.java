package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.model.SearchParam;

import java.util.List;
import java.util.Map;

/**
 * 功能：计划工单service
 * Created by Dong wudang on 2018/11/24.
 */
public interface PlanWorkService {

    /**
     * 获取设备位置树列表-带用户权限查询
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String,Object>> getAssetPositionTree(MethodParam methodParam, SearchParam param);

    /**
     * 新建、编辑维保计划功能
     *
     * @param methodParam
     * @param param
     * @param isEditor
     * @return
     */
    void newPlanSchedule(MethodParam methodParam, Map<String,Object> param, boolean isEditor);

    /**
     * 根据id查询维修计划详情
     *
     * @param methodParam
     * @param plan_code
     * @return
     */
    Map<String,Object> getPlanScheduleInfo(MethodParam methodParam, String plan_code);

    /**
     * 查询维修计划列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String,Object> getPlanScheduleListForPage(MethodParam methodParam, SearchParam param);

    /**
     * 启用禁用维修计划
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyPlanScheduleUse(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除选中维保计划（支持多选，逗号拼接）
     *
     * @param methodParam
     * @param paramMap
     */
    void cutSelectPlanSchedule(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 维保计划查询设备对象根据设备位置、时间、类型、型号
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String,Object> getPlanWorkAssetByPositionCodeAndAssetType(MethodParam methodParam, SearchParam param);

    /**
     * 维保计划查询任务模板列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String,Object>> getPlanWorkTaskTemplateList(MethodParam methodParam, SearchParam param);

    /**
     * 维保计划查询任务模板任务项列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String,Object> getPlanWorkTaskTemplateItemList(MethodParam methodParam, SearchParam param);

    /**
     * 标准任务项选择列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String,Object>> getPlanWorkTaskItemList(MethodParam methodParam, SearchParam param);

    /**
     * 查询维保计划的触发条件列表
     * @param schemaName
     * @param plan_code
     * @return
     */
    List<Map<String, Object>> getPlanWorkTriggleByPlanCode(String schemaName, String plan_code);

    /**
     * 查询维保计划的任务列表
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    List<String> getTaskListByPlanWorkCode(String schemaName, String planWorkCode);

    /**
     * 查询计划任务，开始根据配置的任务，进行工单生成
     * @param schemaName
     * @return
     */
    List<Map<String, Object>> getExecutePlanWork(String schemaName, int limit);

    /**
     * 批量修改维保计划的任务状态
     * @param schemaName
     * @param planWorkCodes
     * @param jobStatus
     */
    void updatePlanWorkJobStatusByPlanWorkCodeList(String schemaName, String planWorkCodes, int jobStatus);

    /**
     * 修改维保计划的任务状态
     * @param schemaName
     * @param planWorkCode
     * @param jobStatus
     */
    void updatePlanWorkJobStatusByPlanWorkCode(String schemaName, String planWorkCode, int jobStatus);

    /**
     * 查询维保计划的所有维保对象规则列表
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    List<Map<String, Object>> getPlanWorkSettingCondition(String schemaName, String planWorkCode);

    /**
     * 获取符合筛选条件的设备列表（维保计划）
     * @param schemaName
     * @param condition
     * @return
     */
    List<Map<String, Object>> getAssetPlanByCondition(String schemaName, String condition);

    /**
     * 获取菜单信息
     *
     * @param methodParam 入参
     * @return
     */
    Map<String, Map<String, Boolean>> getPlanWorkPermission(MethodParam methodParam);

    /**
     * 根据维保计划编号获取设备列表（维保计划）
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    List<Map<String, Object>> getAssetPlanByPlanWorkCode(String schemaName, String planWorkCode);

    /**
     * 批量插入维保行事历
     * @param schemaName
     * @param planWorkCalendars
     */
    int insertBatchPlanWorkCalendars(String schemaName, List<PlanWorkCalendarModel> planWorkCalendars);

    /**
     * 新增维保行事历
     * @param schemaName
     * @param planWorkCalendar
     */
    int insertPlanWorkCalendar(String schemaName, PlanWorkCalendarModel planWorkCalendar);

    /**
     * 修改维保行事历的任务项
     * @param schemaName
     * @param taskItems
     * @return
     */
    int updatePlanWorkCalendarTaskItems(String schemaName, List<Map<String, Object>> taskItems);

    /**
     * 查询计划行事历列表
     * @param schemaName
     * @param limit
     * @return
     */
    List<PlanWorkCalendarModel> getPlanWorkCalendarListByOccurTime(String schemaName, int autoGenerateBill, int limit);

    /**
     * 批量修改行事历状态
     * @param schemaName
     * @param workCalendarCodes
     * @param status
     */
    void updatePlanWorkCalendarStatusByWorkCalendarCodeList(String schemaName, String workCalendarCodes, int status);

    /**
     * 删除计划生成的还未产生工单的计划行事历
     * @param schemaName
     * @param planWorkCode
     */
    void deletePlanWorkCalendarByPlanWorkCode(String schemaName, String planWorkCode);

    /**
     * 查询指定设备某一天的行事历数量
     * @param schemaName
     * @param planWorkCode
     * @param assetId
     * @param date
     */
    int countPlanWorkCalendarByAssetAndDate(String schemaName, String planWorkCode, String assetId, String date);

    Map<String, Object> findPlanWorkPropertyInfo(String schemaName, String work_calendar_code);
}
