package com.gengyun.senscloud.service.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.RoleMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.RoleModel;
import com.gengyun.senscloud.model.SystemPermissionResult;
import com.gengyun.senscloud.response.*;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.RoleService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    RoleMapper roleMapper;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    LogsService logService;
    @Value("${senscloud.com.url}")
    private String url;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getRoleListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[2]);
    }

    /**
     * 新增角色
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    @Override
    public void newRole(MethodParam methodParam, RoleModel roleModel) {
        String schema_name = methodParam.getSchemaName();
        Map<String, Object> role = new HashMap<>();
        String roleId = SenscloudUtil.generateUUIDStr();
        //获取当前最大序号
        int maxOrder = roleMapper.findMaxOrder(schema_name);
        role.put("id", roleId);
        role.put("data_order", maxOrder + 1);
        role.put("name", roleModel.getName());
        role.put("description", roleModel.getDescription());
        role.put("create_time", new Timestamp(System.currentTimeMillis()));
        role.put("create_user_id", methodParam.getUserId());
        roleMapper.insert(schema_name, role);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7006, roleId, LangUtil.doSetLogArray("角色新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AM_Y}));//角色新建
    }

    /**
     * 更新角色基本信息
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    @Override
    public void modifyRoleById(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        Map<String, Object> role = new HashMap<>();
        role.put("id", roleModel.getId());
        role.put("name", roleModel.getName());
        role.put("description", roleModel.getDescription());
        Map<String, Object> oldRole = roleMapper.findByRoleId(methodParam.getSchemaName(), roleModel.getId());
        roleMapper.update(methodParam.getSchemaName(), role);
        JSONArray loger = LangUtil.compareMap(role, oldRole);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7006, roleModel.getId(), LangUtil.doSetLogArray("编辑了角色", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AM_Y, loger.toString()}));//编辑了角色
    }

    /**
     * 更新角色功能权限信息
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    @Override
    public void modifyPermissionRoleById(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);//角色不能为空
        String schema_name = methodParam.getSchemaName();
        JSONArray logArr = new JSONArray();

        //功能权限id列表
        if (RegexUtil.optIsPresentStr(roleModel.getPermissionIds())) {
            String[] permissionIds = RegexUtil.optNotBlankStrOpt(roleModel.getPermissionIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            //获取要更新所有的菜单名称
//            List<String> newPermissionNames = roleMapper.findPermissionNamesByIds(schema_name, permissionIds);
            List<String> newPermissionNames = findPermissionNamesByIds(permissionIds);
            //获取该角色现有的菜单列表
            List<String> oldPermissionNames = findPermissionNamesByRoleId(schema_name, roleModel.getId());
            //新增的角色菜单列表
            List<String> addPermissionName = LangUtil.compareList(newPermissionNames, oldPermissionNames);
            //删除的角色菜单列表
            List<String> removePermissionName = LangUtil.compareList(oldPermissionNames, newPermissionNames);
            if (RegexUtil.optIsPresentList(addPermissionName)) {
                for (String s : addPermissionName
                ) {
                    logArr.add(LangUtil.doSetLogArray("添加了角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AL_A, s}));//添加了角色权限
                }
            }
            if (RegexUtil.optIsPresentList(removePermissionName)) {
                for (String s : addPermissionName
                ) {
                    logArr.add(LangUtil.doSetLogArray("删除了角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AL_A, s}));//添加了角色权限
                }
            }
            List<Map<String, Object>> list = new ArrayList<>();
            //去重
            List<String> permissionIdStream = Arrays.asList(permissionIds).stream().distinct().collect(Collectors.toList());
            for (String permissionId : permissionIdStream
            ) {
                Map<String, Object> map = new HashMap<>();
                map.put("role_id", roleModel.getId());
                map.put("permission_id", permissionId);
                list.add(map);
            }
            roleMapper.deleteRolePermission(schema_name, roleModel.getId());
            commonUtilService.batchInsertDataList(schema_name, list, "_sc_role_permission", "role_id,permission_id");
        } else {
            roleMapper.deleteRolePermission(schema_name, roleModel.getId());
            List<String> oldPermissionNames = findPermissionNamesByRoleId(schema_name, roleModel.getId());
            if (RegexUtil.optNotNull(oldPermissionNames).isPresent()) {
                //删除的角色列表
                if (RegexUtil.optIsPresentList(oldPermissionNames)) {
                    for (String s : oldPermissionNames
                    ) {
                        logArr.add(LangUtil.doSetLogArray("删除了角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AL_A, s}));//添加了角色权限
                    }
                }
            }
        }
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7006, roleModel.getId(), LangUtil.doSetLogArray("编辑了角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AL_A, logArr.toString()}));
    }

    /**
     * 编辑角色数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    @Override
    public void modifyDataPermissionRoleById(MethodParam methodParam, RoleModel roleModel) {
        String roleId = RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);//角色不能为空
        String schema_name = methodParam.getSchemaName();
        JSONArray logArr = new JSONArray();
        //库房id列表
        if (RegexUtil.optIsPresentStr(roleModel.getStockRoleIds())) {
            String[] stockRoleIds = RegexUtil.optNotBlankStrOpt(roleModel.getStockRoleIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            //获取新的设备位置名称列表
            List<String> newStockNames = roleMapper.findStockNamesByIds(schema_name, stockRoleIds);
            //获取该角色原有的设备位置名称列表
            List<String> oldStockNames = roleMapper.findStockNamesByRoleId(schema_name, roleId);
            //新增的设备位置名称列表
            List<String> addStockNames = LangUtil.compareList(newStockNames, oldStockNames);
            //删除的设备位置名称列表
            List<String> removeStockNames = LangUtil.compareList(oldStockNames, newStockNames);
            if (RegexUtil.optIsPresentList(addStockNames)) {
                for (String s : addStockNames) {
                    logArr.add(LangUtil.doSetLogArray("添加了库房角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_BKL, s}));//添加了库房权限
                }
            }
            if (RegexUtil.optIsPresentList(removeStockNames)) {
                for (String s : removeStockNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了库房角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_BKL, s}));//删除了库房权限
                }
            }
            roleMapper.deleteRoleStockPosition(schema_name, roleId);
            List<Map<String, Object>> list = new ArrayList<>();
            //去重
            List<String> streamList = Arrays.asList(stockRoleIds).stream().distinct().collect(Collectors.toList());
            for (String stock_id : streamList
            ) {
                Map<String, Object> map = new HashMap<>();
                map.put("role_id", roleId);
                map.put("stock_id", stock_id);
                list.add(map);
            }
            commonUtilService.batchInsertDataList(schema_name, list, "_sc_role_stock", "role_id,stock_id");
        } else {
            //获取该角色原有的设备位置名称列表
            List<String> oldStockNames = roleMapper.findStockNamesByRoleId(schema_name, roleId);
            if (RegexUtil.optIsPresentList(oldStockNames)) {
                for (String s : oldStockNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了库房角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_BKL, s}));//删除了库房权限
                }
            }
            roleMapper.deleteRoleStockPosition(schema_name, roleId);
        }

        //设备位置权限id列表
        if (RegexUtil.optIsPresentStr(roleModel.getAssetPositionIds())) {
            String[] assetPositionIds = RegexUtil.optNotBlankStrOpt(roleModel.getAssetPositionIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            //获取新的设备位置名称列表
            List<String> newAssetPositioNames = roleMapper.findAssetPositonNamesByIds(schema_name, assetPositionIds);
            //获取该角色原有的设备位置名称列表
            List<String> oldAssetPositioNames = roleMapper.findAssetPositonNamesByRoleId(schema_name, roleId);
            //新增的设备位置名称列表
            List<String> addAssetPositioNames = LangUtil.compareList(newAssetPositioNames, oldAssetPositioNames);
            //删除的设备位置名称列表
            List<String> removeAssetPositioNames = LangUtil.compareList(oldAssetPositioNames, newAssetPositioNames);
            if (RegexUtil.optIsPresentList(addAssetPositioNames)) {
                for (String s : addAssetPositioNames) {
                    logArr.add(LangUtil.doSetLogArray("添加了设备位置角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_HIN, s}));//添加了设备位置权限
                }
            }
            if (RegexUtil.optIsPresentList(removeAssetPositioNames)) {
                for (String s : removeAssetPositioNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了设备位置角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_HIN, s}));//删除了设备位置权限
                }
            }
            roleMapper.deleteRoleAssetPosition(schema_name, roleId);
            List<Map<String, Object>> list = new ArrayList<>();
            //去重
            List<String> streamList = Arrays.asList(assetPositionIds).stream().distinct().collect(Collectors.toList());
            for (String assetPositionId : streamList) {
                Map<String, Object> map = new HashMap<>();
                map.put("role_id", roleId);
                map.put("asset_position_code", assetPositionId);
                list.add(map);
            }
            commonUtilService.batchInsertDataList(schema_name, list, "_sc_role_asset_position", "role_id,asset_position_code");
        } else {
            //获取该角色原有的设备位置名称列表
            List<String> oldAssetPositioNames = roleMapper.findAssetPositonNamesByRoleId(schema_name, roleId);
            if (RegexUtil.optIsPresentList(oldAssetPositioNames)) {
                for (String s : oldAssetPositioNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了设备位置角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_HIN, s}));//删除了设备位置权限
                }
            }
            roleMapper.deleteRoleAssetPosition(schema_name, roleId);
        }
        //设备类型权限id列表
        if (RegexUtil.optIsPresentStr(roleModel.getAssetCategoryIds())) {
            String[] assetCategoryIds = RegexUtil.optNotBlankStrOpt(roleModel.getAssetCategoryIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            //获取新的设备类型名称列表
            List<String> newAssetCategoryNames = roleMapper.findAssetCategoryNamesByIds(schema_name, assetCategoryIds);
            //获取该角色原有的设备类型名称列表
            List<String> oldAssetCategoryNames = roleMapper.findAssetCategoryNamesByRoleId(schema_name, roleId);
            //新增的设备类型名称列表
            List<String> addAssetCategoryNames = LangUtil.compareList(newAssetCategoryNames, oldAssetCategoryNames);
            //删除的设备类型名称列表
            List<String> removeAssetCategoryNames = LangUtil.compareList(oldAssetCategoryNames, newAssetCategoryNames);
            if (RegexUtil.optIsPresentList(addAssetCategoryNames)) {
                for (String s : addAssetCategoryNames) {
                    logArr.add(LangUtil.doSetLogArray("添加了设备类型角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.COM_B, s}));//添加了设备类型权限
                }
            }
            if (RegexUtil.optIsPresentList(removeAssetCategoryNames)) {
                for (String s : removeAssetCategoryNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了设备类型角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.COM_B, s}));//删除了设备类型权限
                }
            }
            roleMapper.deleteRoleAssetCategory(schema_name, roleId);
            List<Map<String, Object>> list = new ArrayList<>();
            //去重
            List<String> streamList = Arrays.asList(assetCategoryIds).stream().distinct().collect(Collectors.toList());
            for (String assetCategoryId : streamList
            ) {
                Map<String, Object> map = new HashMap<>();
                map.put("role_id", roleId);
                map.put("category_id", assetCategoryId);
                list.add(map);
            }
            commonUtilService.batchInsertDataList(schema_name, list, "_sc_role_asset_type", "role_id,category_id");
        } else {
            //获取该角色原有的设备类型名称列表
            List<String> oldAssetCategoryNames = roleMapper.findAssetCategoryNamesByRoleId(schema_name, roleId);
            if (RegexUtil.optIsPresentList(oldAssetCategoryNames)) {
                for (String s : oldAssetCategoryNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了设备类型角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.COM_B, s}));//删除了设备类型权限
                }
            }
            roleMapper.deleteRoleAssetCategory(schema_name, roleId);
        }
        //工单类型权限id列表
        if (RegexUtil.optIsPresentStr(roleModel.getWorkTypeRoleIds())) {
            String[] workTypeRoleIds = RegexUtil.optNotBlankStrOpt(roleModel.getWorkTypeRoleIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            //获取新的工单类型名称列表
            List<String> newWorkTypeNames = roleMapper.findWorkTypeNamesByIds(schema_name, workTypeRoleIds);
            //获取该角色原有的工单类型名称列表
            List<String> oldWorkTypeNames = roleMapper.findWorkTypeNamesByRoleId(schema_name, roleId);
            //新增的工单类型名称列表
            List<String> addWorkTypeNames = LangUtil.compareList(newWorkTypeNames, oldWorkTypeNames);
            //删除的工单类型名称列表
            List<String> removeWorkTypeNames = LangUtil.compareList(oldWorkTypeNames, newWorkTypeNames);
            if (RegexUtil.optIsPresentList(addWorkTypeNames)) {
                for (String s : addWorkTypeNames) {
                    logArr.add(LangUtil.doSetLogArray("添加了工单类型角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.COM_C, s}));//添加了工单类型权限
                }
            }
            if (RegexUtil.optIsPresentList(removeWorkTypeNames)) {
                for (String s : removeWorkTypeNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了工单类型角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.COM_C, s}));//删除了工单类型权限
                }
            }
            roleMapper.deleteRoleWorkType(schema_name, roleId);
            List<Map<String, Object>> list = new ArrayList<>();
            //去重
            List<String> streamList = Arrays.asList(workTypeRoleIds).stream().distinct().collect(Collectors.toList());
            for (String workTypeRoleId : streamList) {
                Map<String, Object> map = new HashMap<>();
                map.put("role_id", roleId);
                map.put("work_type_id", workTypeRoleId);
                list.add(map);
            }
            commonUtilService.batchInsertDataList(schema_name, list, "_sc_role_work_type", "role_id,work_type_id");
        } else {
            //获取该角色原有的工单类型名称列表
            List<String> oldWorkTypeNames = roleMapper.findWorkTypeNamesByRoleId(schema_name, roleId);
            if (RegexUtil.optIsPresentList(oldWorkTypeNames)) {
                for (String s : oldWorkTypeNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了工单类型角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.COM_C, s}));//删除了工单类型权限
                }
            }
            roleMapper.deleteRoleWorkType(schema_name, roleId);
        }

        // 创建工单权限id列表
        if (RegexUtil.optIsPresentStr(roleModel.getNewWorkRoleIds())) {
            String[] workTypeRoleIds = RegexUtil.optNotBlankStrOpt(roleModel.getNewWorkRoleIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            // 获取新的工单类型名称列表
            List<String> newWorkTypeNames = roleMapper.findWorkTypeNamesByIds(schema_name, workTypeRoleIds);
            // 获取该角色原有的工单类型名称列表
            List<String> oldWorkTypeNames = roleMapper.findNewWorkNamesByRoleId(schema_name, roleId);
            // 新增的工单类型名称列表
            List<String> addWorkTypeNames = LangUtil.compareList(newWorkTypeNames, oldWorkTypeNames);
            // 删除的工单类型名称列表
            List<String> removeWorkTypeNames = LangUtil.compareList(oldWorkTypeNames, newWorkTypeNames);
            if (RegexUtil.optIsPresentList(addWorkTypeNames)) {
                for (String s : addWorkTypeNames) {
                    logArr.add(LangUtil.doSetLogArray("添加了创建工单角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.COM_D, s}));//添加了创建工单权限
                }
            }
            if (RegexUtil.optIsPresentList(removeWorkTypeNames)) {
                for (String s : removeWorkTypeNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了创建工单角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.COM_D, s}));//删除了创建工单权限
                }
            }
            roleMapper.deleteRoleNewWork(schema_name, roleId);
            List<Map<String, Object>> list = new ArrayList<>();
            // 去重
            Arrays.stream(workTypeRoleIds).distinct().forEach(wtrId -> {
                Map<String, Object> map = new HashMap<>();
                map.put("role_id", roleId);
                map.put("data_id", wtrId);
                map.put("data_type", Constants.ROLE_DATA_PRM_TYPE);
                list.add(map);
            });
            commonUtilService.batchInsertDataList(schema_name, list, "_sc_role_data_permission", "role_id,data_id,data_type");
        } else {
            // 获取该角色原有的工单类型名称列表
            List<String> oldWorkTypeNames = roleMapper.findNewWorkNamesByRoleId(schema_name, roleId);
            if (RegexUtil.optIsPresentList(oldWorkTypeNames)) {
                for (String s : oldWorkTypeNames) {
                    logArr.add(LangUtil.doSetLogArray("删除了创建工单角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.COM_D, s}));//删除了创建工单权限
                }
            }
            roleMapper.deleteRoleNewWork(schema_name, roleId);
        }

        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7006, roleId, LangUtil.doSetLogArray("编辑了角色权限", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AL_A, logArr.toString()}));

    }

    /**
     * 删除角色
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    @Override
    public void cutRoleById(MethodParam methodParam, RoleModel roleModel) {
        String roleId = RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        String schema_name = methodParam.getSchemaName();
        Map<String, Object> oldMap = roleMapper.findByRoleId(schema_name, roleId);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            //删除角色表信息
            roleMapper.deleteById(schema_name, roleId);
            //删除角色菜单权限关联信息
            roleMapper.deleteRolePermission(schema_name, roleId);
            //删除角色设备位置关联信息
            roleMapper.deleteRoleAssetPosition(schema_name, roleId);
            //删除角色设备类型关联信息
            roleMapper.deleteRoleAssetCategory(schema_name, roleId);
            //删除角色工单类型关联信息
            roleMapper.deleteRoleWorkType(schema_name, roleId);
            //删除岗位角色关联表
            roleMapper.deletePositionRole(schema_name, roleId);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7006, roleId, LangUtil.doSetLogArray("删除了角色", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AM_Y, oldMap.get("name").toString()}));
        }
    }

    /**
     * 分页获取角色信息列表
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 获取角色信息列表
     */
    @Override
    public Map<String, Object> getRoleList(MethodParam methodParam, RoleModel roleModel) {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> sysRoleList = roleMapper.findSysRoleList(methodParam.getSchemaName(), roleModel.getKeywordSearch());
        List<Map<String, Object>> noSysRoleList = roleMapper.findNoSysRoleList(methodParam.getSchemaName(), roleModel.getKeywordSearch());
        result.put("sysRoleList", sysRoleList);
        result.put("noSysRoleList", noSysRoleList);
        return result;
    }

    /**
     * 获取角色详情
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色详情
     */
    @Override
    public Map<String, Object> getRoleInfo(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        return roleMapper.findRoleInfo(methodParam.getSchemaName(), roleModel.getId());
    }

    /**
     * 查询角色功能权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色功能权限
     */
    @Override
    public List<PermissionRoleResult> getRolePermission(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        //查询该角色存不存在
        Map<String, Object> roleMap = roleMapper.findByRoleId(methodParam.getSchemaName(), roleModel.getId());
        if (!RegexUtil.optNotNull(roleMap).isPresent()) {
            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AM_Y});//角色信息不存在
        }
        List<PermissionRoleResult> permissionRoleList =
                findByCompanyIdAndRoleId(methodParam.getSchemaName(), methodParam.getCompanyId(), null, roleModel.getId());
        permissionRoleList = TreeUtil.convert(permissionRoleList, "id", "parent_id", "childrens");
        permissionRoleList = newPermissionRole(permissionRoleList);
        return permissionRoleList;
    }

    private List<PermissionRoleResult> newPermissionRole(List<PermissionRoleResult> permissionRoleList) {
        for (PermissionRoleResult permissionRoleResult : permissionRoleList
        ) {
            if (permissionRoleResult.getType() == 1 || permissionRoleResult.getType() == 2) {
                if (RegexUtil.optNotNull(permissionRoleResult.getChildrens()).isPresent()) {
                    newPermissionRole(permissionRoleResult.getChildrens());
                }
                //新建一个虚拟的本身菜单对象
                PermissionRoleResult newPermissionResult = new PermissionRoleResult();
                newPermissionResult.setType(3);
                newPermissionResult.setId(permissionRoleResult.getId());
                newPermissionResult.setParent_id(permissionRoleResult.getId());
                newPermissionResult.setMenu_name("模块启用");
                newPermissionResult.setIsSelect(permissionRoleResult.getIsSelect());
                newPermissionResult.setKey(permissionRoleResult.getKey());
                newPermissionResult.setUrl(permissionRoleResult.getUrl());
                if (!RegexUtil.optNotNull(permissionRoleResult.getChildrens()).isPresent()) {
                    List<PermissionRoleResult> childrens = new ArrayList<>();
                    childrens.add(newPermissionResult);
                    permissionRoleResult.setChildrens(childrens);
                } else {
                    permissionRoleResult.getChildrens().add(0, newPermissionResult);
                }
            }
        }
        return permissionRoleList;
    }

    /**
     * 查询角色设备位置数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色设备位置数据权限
     */
    @Override
    public List<AssetPositionRoleResult> getRoleAssetPositionPermission(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        List<AssetPositionRoleResult> assetPositionList = roleMapper.findAssetPositionRoleList(methodParam.getSchemaName(), roleModel.getId());
        return TreeUtil.convert(assetPositionList, "position_code", "parent_code", "childrens");
    }

    /**
     * 查询角色设备类型数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色设备类型数据权限
     */
    @Override
    public List<AssetCategoryRoleResult> getRoleAssetTypePermission(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        List<AssetCategoryRoleResult> assetCategoryList = roleMapper.findAssetCategoryRoleList(methodParam.getSchemaName(), roleModel.getId());
        return TreeUtil.convert(assetCategoryList, "id", "parent_id", "childrens");
    }

    /**
     * 查询角色工单类型数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色工单类型数据权限
     */
    @Override
    public List<WorkTypeRoleResult> getRoleWorkTypePermission(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        return roleMapper.findWorkTypeRoleList(methodParam.getSchemaName(), roleModel.getId());
    }

    /**
     * 查询角色创建工单类型数据权限
     *
     * @param methodParam 系统参数
     * @param roleModel   角色参数
     * @return 创建工单类型角色列表
     */
    @Override
    public List<Map<String, Object>> getNewWorkTypePermission(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        return roleMapper.findNewWorkTypeRoleList(methodParam.getSchemaName(), roleModel.getId());
    }

    /**
     * 查询角色库房数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色库房数据权限
     */
    @Override
    public List<StockRoleResult> getRoleStockPermission(MethodParam methodParam, RoleModel roleModel) {
        RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        return roleMapper.findStockRoleList(methodParam.getSchemaName(), roleModel.getId());
    }

    /**
     * 克隆角色
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    @Override
    public void cloneRole(MethodParam methodParam, RoleModel roleModel) {
        String oldRoleId = RegexUtil.optStrOrExpNotNull(roleModel.getId(), LangConstant.TITLE_AM_Y);
        String schema_name = methodParam.getSchemaName();
        Map<String, Object> oldRole = roleMapper.findByRoleId(schema_name, oldRoleId);
        if (RegexUtil.optNotNull(oldRole).isPresent()) {
            //生成一个新的角色id
            String newRoleId = SenscloudUtil.generateUUIDStr();
            roleMapper.cloneRole(schema_name, newRoleId, oldRoleId);
            roleMapper.cloneRoleAssetPosition(schema_name, newRoleId, oldRoleId);
            roleMapper.cloneRoleAssetType(schema_name, newRoleId, oldRoleId);
            roleMapper.cloneRolePermission(schema_name, newRoleId, oldRoleId);
            roleMapper.cloneRoleStock(schema_name, newRoleId, oldRoleId);
            roleMapper.cloneRoleWorkType(schema_name, newRoleId, oldRoleId);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7006, newRoleId, LangUtil.doSetLogArray("克隆了角色", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AM_Y, oldRole.get("name").toString()}));//克隆了角色
        }
    }

    /**
     * 根据角色id列表获取角色名称列表
     *
     * @param schema_name 入参
     * @param roleIds     入参
     * @return 角色名称列表
     */
    @Override
    public List<String> getRoleNamesByRoleIds(String schema_name, List<String> roleIds) {
        return roleMapper.findRoleNamesByRoleIds(schema_name, roleIds);
    }

    /**
     * 根据角色id获取角色信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 角色信息
     */
    @Override
    public Map<String, Object> getByRoleId(String schema_name, String roleId) {
        RegexUtil.optStrOrExpNotNull(roleId, LangConstant.TITLE_AM_Y);
        return roleMapper.findByRoleId(schema_name, roleId);
    }

    public List<String> findPermissionNamesByIds(String[] ids) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ids", ids);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findPermissionNamesByIds");
        List<String> result = Arrays.asList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
    }

    public List<String> findPermissionNamesByRoleId(String schema_name, String id) {
        List<String> oldPermissionNamesList = roleMapper.findPermissionNamesByRoleId(schema_name, id);
        JSONObject jsonObject = new JSONObject();
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findAllPermissionNoCondition");
        List<Map<String, Object>> requestResult = DataChangeUtil.scdStringToList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        Set<String> param = new HashSet<>();
        requestResult.forEach(r -> {
            oldPermissionNamesList.forEach(e -> {
                if (RegexUtil.optStrOrNull(e).equals(RegexUtil.optStrOrBlank(r.get("id")))) {
                    param.add(RegexUtil.optStrOrBlank(r.get("key")));
                }
            });
        });
        List<String> paramResult = new ArrayList<>(param);
        return paramResult;
    }

    public List<PermissionRoleResult> findByCompanyIdAndRoleId(String schema_name, Long company_id, Integer type, String roleId) {
        List<PermissionRoleResult> result = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("company_id", company_id);
        jsonObject.put("type", type);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findByCompanyIdAndRoleId");
        List<PermissionRoleResult> resMap = JSON.parseArray(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), PermissionRoleResult.class);
        List<Map<String, Object>> list = roleMapper.findByCompanyIdAndRoleId(schema_name, roleId);
        if (RegexUtil.optIsPresentList(resMap)) {
            resMap.forEach(e -> {
                list.forEach(l -> {
                    if (Objects.equals(e.getId(), l.get("permission_id"))) {
                        if (RegexUtil.optIsPresentStr(l.get("role_id"))) {
                            e.setIsSelect(1);
                        }
                    }
                });
                result.add(e);
            });
        }
        return result.stream().sorted(new Comparator<PermissionRoleResult>() {
            @Override
            public int compare(PermissionRoleResult o1, PermissionRoleResult o2) {
                return o1.getIndex() - o2.getIndex();
            }
        }).collect(Collectors.toList());
    }
}
