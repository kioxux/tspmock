package com.gengyun.senscloud.service.asset;

/**
 * 设备盘点
 */
public interface AssetInventoryService {
//
//    /**
//     * 根据code查询设备盘点数据
//     *
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    Map<String, Object> queryAssetInventoryByCode(String schemaName, String inventoryCode);
//
//    /**
//     * 根据code查询盘点位置数据
//     *
//     * @param schemaName
//     * @param subInventoryCode
//     * @return
//     */
//    Map<String, Object> queryAssetInventoryStockByCode(String schemaName, String subInventoryCode);
//
//    /**
//     * 查询设备盘点列表
//     *
//     * @param request
//     * @return
//     */
//    String findAssetInventoryList(HttpServletRequest request);
//
//    /**
//     * 设备盘点报废
//     * @param request
//     * @return
//     */
//    ResponseModel invalidInventory(HttpServletRequest request);
//
//    /**
//     * 查询设备盘点位置列表
//     * @param request
//     * @return
//     */
//    String findInventoryOrgList(HttpServletRequest request);
//
//    /**
//     * 查询盘点设备列表
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    String findInventoryOrgDetailList(HttpServletRequest request, String schema_name);
//
//    /**
//     * 启动盘点
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    ResponseModel addInventory(String schemaName, HttpServletRequest request, String processDefinitionId, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * PC盘点
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    ResponseModel pcInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 盘点分配
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    ResponseModel inventoryAllot(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
//
//    /**
//     * 盘点盈余设备的调整
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    public ResponseModel assetTransfer(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 盘点盈余设备的报废
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    public ResponseModel assetDiscard(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 盘点完成
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    public ResponseModel finishInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap);
//
//    /**
//     * 查询分拨中心下，需要盘点的设备总数
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    public ResponseModel getAssetInventoryQuantity(String schemaName, HttpServletRequest request);
//
//    /**
//     * 查询设备详情
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    public ResponseModel getAssetFacilitiesById(String schemaName, HttpServletRequest request);
}
