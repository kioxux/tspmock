package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.mapper.StatisticMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkflowSearchParam;
import com.gengyun.senscloud.service.HomePageService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.system.WorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class HomePageServiceImpl implements HomePageService {
    private static final Logger logger = LoggerFactory.getLogger(HomePageServiceImpl.class);
    @Resource
    WorksService worksService;
    @Resource
    WorkflowService workflowService;
    @Resource
    StatisticMapper statisticMapper;

    /**
     * 获取首页的待办任务，今日上报，今日完成，超时任务数
     *
     * @param methodParam 入参
     * @return 获取首页的待办任务，今日上报，今日完成，超时任务数
     */
    @Override
    public Map<String, Object> getHomeWorksCount(MethodParam methodParam) {
        Map<String, Object> result = new HashMap<>();
        //代办任务数(查看所有当前用户有权限看到的数据)
        WorkflowSearchParam wfParam = new WorkflowSearchParam();
        //今日上报数 仅维修工单
        Integer reportWorksCount = worksService.todayReportWorksCount(methodParam);
        //今日完成数
        Integer finishedWorksCount = worksService.todayFinishedWorksCount(methodParam);
        //超时任务
        Integer overtimeWorksCount = worksService.todayOvertimeWorksCount(methodParam);
        result.put("tasksCount", workflowService.getTasksCount(methodParam, wfParam));
        result.put("reportWorksCount", reportWorksCount);
        result.put("finishedWorksCount", finishedWorksCount);
        result.put("overtimeWorksCount", overtimeWorksCount);
        return result;
    }

    /**
     * 首页的日历待办任务数
     *
     * @param methodParam 入参
     * @param month       入参
     * @return 首页的日历待办任务数
     */
    @Override
    public List<Map<String, Object>> getCalendarWorksCount(MethodParam methodParam, String month) {
        Date date;
        try {
            date = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON_2).parse(month);
        } catch (ParseException e) {
            logger.error("", e);
            date = new Date();
        }
        Calendar cl = Calendar.getInstance();
        cl.setTime(date);
        //先把时间调到1日的00:00:00
        cl.set(Calendar.DAY_OF_MONTH, 1);
        cl.set(Calendar.HOUR_OF_DAY, 0);
        cl.set(Calendar.MINUTE, 0);
        cl.set(Calendar.SECOND, 0);
        //日历要查询指定月份前后15天的时间范围
        cl.add(Calendar.DAY_OF_YEAR, -15);
        Date start = cl.getTime();
        cl.add(Calendar.DAY_OF_YEAR, 15);
        cl.add(Calendar.MONTH, 1);
        cl.add(Calendar.DAY_OF_MONTH, 15);
        Date end = cl.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
        WorkflowSearchParam wfParam = new WorkflowSearchParam();
        wfParam.setMine_only(true);
        wfParam.setStart_time(sdf.format(start));
        wfParam.setEnd_time(sdf.format(end));
        return workflowService.getFlowTaskGroupList(methodParam, wfParam);
    }

    /**
     * 首页显示的图表
     *
     * @param methodParam 入参
     * @return 首页显示的图表
     */
    @Override
    public List<Map<String, Object>> getStatisticList(MethodParam methodParam) {
        return statisticMapper.findStatisticShowDashboardList(methodParam.getSchemaName());
    }
}
