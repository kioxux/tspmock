package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.ScheduleData;

import java.util.List;

public interface ScheduleService {
    void insert(String schema_name, ScheduleData scheduleData);

    void update(String schema_name, ScheduleData ScheduleData);

    void delete(String schema_name, Integer id);

    List<ScheduleData> find(String schema_name, Integer id, String shedule_name, String is_use, Boolean reschedule, Boolean is_running, Integer run_channel);

    boolean checkRunning(String schema_name, Integer id);
}
