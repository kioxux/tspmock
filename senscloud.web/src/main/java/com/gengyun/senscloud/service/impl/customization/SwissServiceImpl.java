package com.gengyun.senscloud.service.impl.customization;

import com.gengyun.senscloud.service.customization.SwissService;
import org.springframework.stereotype.Service;

@Service
public class SwissServiceImpl implements SwissService {
//
//    private static final Logger logger = LoggerFactory.getLogger(SwissServiceImpl.class);
//
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//
//    @Autowired
//    WorkSheetRequestMapper workSheetRequestMapper;
//
//    @Autowired
//    FilesService filesService;
//
//    /**
//     * 跳转到完工报告页面
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ModelAndView worksheetDetailCompletionReport(HttpServletRequest request, String schema_name) {
//        User user = AuthService.getLoginUser(request);
//        String subWorkCode = request.getParameter("subWorkCode");
//        String code = request.getParameter("code");
//        String sep = System.getProperty("file.separator");
//        String submit = request.getParameter("submit");
//
//        ModelAndView modelAndView = new ModelAndView("swiss_report/completion_report")
//                .addObject("today", new SimpleDateFormat(Constants.DATE_FMT);.format(new Date()))
//                .addObject("submit", "true".equals(submit)? true: false)
//                .addObject("executor", user.getUsername())
//                .addObject("code", code)
//                .addObject("subWorkCode", subWorkCode);
//
//        Map<String, Object> workInfo = null;
//        //判断请求、工单
//        if (subWorkCode.startsWith("WR")) {
//            workInfo = workSheetRequestMapper.queryWqInfoById(schema_name, subWorkCode);
//        }else{
//            workInfo = workSheetHandleMapper.queryWorkInfoBySubWorkCode(schema_name, subWorkCode);
//            // 获取备件数据
//            List<Map<String, Object>> bomList = workSheetHandleMapper.queryWbsListBySubWorkCode(schema_name, subWorkCode);
//            modelAndView.addObject("bomList", bomList);
//        }
//        modelAndView.addObject("isRequest", subWorkCode.startsWith("WR")?true:false)
//                .addObject("report_code", subWorkCode.startsWith("WR")?subWorkCode:workInfo.get("work_code"))
//                .addObject("create_time", new SimpleDateFormat(Constants.DATE_FMT_SS).format(workInfo.get("create_time")))
//                .addObject("username", workInfo.get("username"))
//                .addObject("mobile", workInfo.get("mobile"))
//                .addObject("short_title", workInfo.get("short_title"))
//                .addObject("remark", workInfo.get("remark"))
//                .addObject("result_note", workInfo.get("result_note"));
//        try {
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailCompletionReport" + sep + subWorkCode + ".json";
//            modelAndView.addObject("data", FileUtil.getMfReportData(filePath))
//                    .addObject("subWorkCode", subWorkCode);
//        } catch (Exception ex) {
//            logger.error("", ex);
//        }
//        return modelAndView;
//    }
//
//    /**
//     * 保存完工报告数据
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ResponseModel doSaveWorksheetDetailCompletionReport(HttpServletRequest request, String schema_name) {
//        User user = AuthService.getLoginUser(request);
//        String sep = System.getProperty("file.separator");
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            String reportData = request.getParameter("reportData");
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailCompletionReport" + sep + subWorkCode;
//            //生成json文件并保存
//            FileUtil.generateJsonFile(filePath, reportData);
//            Map<String, Object> exportData = new HashMap<String, Object>();
//            if (RegexUtil.isNotNull(reportData)) {
//                exportData = JSONObject.fromObject(reportData);
//            }
//            Object timeArray = exportData.get("template1Array");
//            if (null == timeArray || ((List) timeArray).size() == 0) {
//                exportData.put("template1Array", JSONArray.fromObject("[{}]"));
//            }
//
//            Object bomList = exportData.get("bom_list");
//            if (null == bomList || ((List) bomList).size() == 0) {
//                exportData.put("bom_list", JSONArray.fromObject("[{}]"));
//            }
//            String fileType = ".doc";
//            filePath = filePath + fileType;
//            WordExportUtil.createWordByData(exportData, "swissWorkFinishedReport.ftl", "work", file_upload_dir + sep + schema_name, filePath, sep);
//            //保存文件路径到附件表
//            FilesData attachment = new FilesData();
//            attachment.setFileName(subWorkCode + fileType);
//            attachment.setFileOriginalName(subWorkCode + fileType);
//            attachment.setFileUrl(filePath);
//            attachment.setFileType(2);//文件
//            attachment.setCreateUserAccount(user.getAccount());
//            int fileId = filesService.Add(schema_name, attachment);
//            Map<String, Object> result = new HashMap<>();
//            result.put("fileId", fileId);
//            result.put("isRequest", subWorkCode.startsWith("WR") ? true : false);
//            return ResponseModel.ok(result);
//        } catch (Exception e) {
//            logger.error("", e);
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到预防性维护完工报告页面
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ModelAndView worksheetDetailMaintenanceCompletionReport(HttpServletRequest request, String schema_name) {
//        User user = AuthService.getLoginUser(request);
//        String subWorkCode = request.getParameter("subWorkCode");
//        String code = request.getParameter("code");
//        String sep = System.getProperty("file.separator");
//        String submit = request.getParameter("submit");
//
//        Map<String, Object> workInfo = workSheetHandleMapper.queryWorkInfoBySubWorkCode(schema_name, subWorkCode);
//        String work_code = (String)workInfo.get("work_code");
//        String tableName = "_sc_works_detail";
//        String sw_begin_date = workSheetHandleMapper.getBodyPropertyFieldValueBySubWorkCode(schema_name, tableName, subWorkCode, "sw_begin_date");
//        String sw_end_date = workSheetHandleMapper.getBodyPropertyFieldValueBySubWorkCode(schema_name, tableName, subWorkCode, "sw_end_date");
//        String asset_category = workSheetHandleMapper.getBodyPropertyFieldValueBySubWorkCode(schema_name, tableName, subWorkCode, "asset_category");
//        List<Map<String, Object>> assetList = workSheetHandleMapper.querySwissSubWorkSheetAsset(schema_name, work_code);
//        Map<String, Object> paramMap = new HashMap<>();
//        paramMap.put("work_code", work_code);
//        paramMap.put("search_is_main", Constants.WORKSHEET_IS_MAIN_ASSET);
//        List<Map<String, Object>> subPageList = workSheetHandleMapper.querySubPageListByInfo(schema_name, paramMap);
//        if(StringUtils.isNotBlank(sw_begin_date)){
//            try {
//                sw_begin_date = new SimpleDateFormat(Constants.DATE_FMT);.format(new SimpleDateFormat(Constants.DATE_FMT_SS).parse(sw_begin_date));
//            } catch (ParseException e) {
//            }
//        }
//        if(StringUtils.isNotBlank(sw_end_date)){
//            try {
//                sw_end_date = new SimpleDateFormat(Constants.DATE_FMT);.format(new SimpleDateFormat(Constants.DATE_FMT_SS).parse(sw_end_date));
//            } catch (ParseException e) {
//            }
//        }
//        ModelAndView modelAndView = new ModelAndView("swiss_report/maintenance_completion_report")
//                .addObject("submit", "true".equals(submit)? true: false)
//                .addObject("executor", user.getUsername())
//                .addObject("code", code)
//                .addObject("subWorkCode", subWorkCode)
//                .addObject("report_code", work_code)
//                .addObject("username", workInfo.get("username"))
//                .addObject("sw_begin_date", sw_begin_date)
//                .addObject("sw_end_date", sw_end_date)
//                .addObject("short_title", workInfo.get("short_title"))
//                .addObject("assetList", assetList == null?new ArrayList<Map<String, Object>>():assetList)
//                .addObject("subPageList", subPageList== null?new ArrayList<Map<String, Object>>(): subPageList)
//                .addObject("asset_category", asset_category)
//                .addObject("remark", workInfo.get("remark"))
//                .addObject("result_note", workInfo.get("result_note"));
//        try {
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailMaintenanceCompletionReport" + sep + subWorkCode + ".json";
//            modelAndView.addObject("data", FileUtil.getMfReportData(filePath))
//                    .addObject("subWorkCode", subWorkCode);
//        } catch (Exception ex) {
//            logger.error("", ex);
//        }
//        return modelAndView;
//    }
//
//    /**
//     * 保存预防性维护完工报告数据
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ResponseModel doSaveWorksheetDetailMaintenanceCompletionReport(HttpServletRequest request, String schema_name) {
//        User user = AuthService.getLoginUser(request);
//        String sep = System.getProperty("file.separator");
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            String reportData = request.getParameter("reportData");
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailMaintenanceCompletionReport" + sep + subWorkCode;
//            //生成json文件并保存
//            FileUtil.generateJsonFile(filePath, reportData);
//            Map<String, Object> exportData = new HashMap<String, Object>();
//            if (RegexUtil.isNotNull(reportData)) {
//                exportData = JSONObject.fromObject(reportData);
//            }
//            Object timeArray = exportData.get("template1Array");
//            if (null == timeArray || ((List) timeArray).size() == 0) {
//                exportData.put("template1Array", JSONArray.fromObject("[{}]"));
//            }
//
//            Object bomList = exportData.get("bom_list");
//            if (null == bomList || ((List) bomList).size() == 0) {
//                exportData.put("bom_list", JSONArray.fromObject("[{}]"));
//            }
//            String fileType = ".doc";
//            filePath = filePath + fileType;
//            WordExportUtil.createWordByData(exportData, "swissMaintenanceWorkFinishedReport.ftl", "work", file_upload_dir + sep + schema_name, filePath, sep);
//            //保存文件路径到附件表
//            FilesData attachment = new FilesData();
//            attachment.setFileName(subWorkCode + fileType);
//            attachment.setFileOriginalName(subWorkCode + fileType);
//            attachment.setFileUrl(filePath);
//            attachment.setFileType(2);//文件
//            attachment.setCreateUserAccount(user.getAccount());
//            int fileId = filesService.Add(schema_name, attachment);
//            Map<String, Object> result = new HashMap<>();
//            result.put("fileId", fileId);
//            return ResponseModel.ok(result);
//        } catch (Exception e) {
//            logger.error("", e);
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 巡检内容清单导出功能
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public void exportInspectionContentsReport(HttpServletRequest request, HttpServletResponse response, String schema_name) {
//        String subWorkCode = request.getParameter("subWorkCode");
//        Map<String, Object> paramMap = new HashMap<>();
//        paramMap.put("sub_work_code", subWorkCode);
//        paramMap.put("search_is_main", Constants.WORKSHEET_IS_MAIN_ASSET);
//        List<Map<String, Object>> subPageList = workSheetHandleMapper.querySubPageListBySubWorkInfo(schema_name, paramMap);
//        List<Map<String, Object>> dataList = new ArrayList<>();
//        String tableName = "_sc_works_detail";
//        if(subPageList != null && subPageList.size() > 0){
//            for(Map<String, Object> map : subPageList){
//                String sub_work_code = (String)map.get("sub_work_code");
//                String bodyPropertys = workSheetHandleMapper.getBodyPropertyFieldColumnBySubWorkCode(schema_name, tableName, sub_work_code, "bom_maintain","bomContent");
//                if(bodyPropertys != null) {
//                    JSONArray jsonArray = JSONArray.fromObject(bodyPropertys);
//                    if(jsonArray != null && jsonArray.size() > 0) {
//                        for (int i = 0; i < jsonArray.size(); i++) {
//                            JSONObject jsonObject = jsonArray.getJSONObject(i);
//                            if (jsonObject != null) {
//                                map.put("bom_name", jsonObject.optString("bom_name"));
//                            }
//                            //多个备件，需要生成多条数据，map拷贝
//                            Map<String, Object> resultMap = new HashMap<>();
//                            Iterator it = map.entrySet().iterator();
//                            while(it.hasNext()) {
//                                Map.Entry entry=(Map.Entry)it.next();
//                                Object key=entry.getKey();
//                                if(key!=null && map.get(key)!=null) {
//                                    resultMap.put(key.toString(), map.get(key));
//                                }
//                            }
//                            dataList.add(resultMap);
//                        }
//                    }else{
//                        dataList.add(map);
//                    }
//                }else{
//                    dataList.add(map);
//                }
//            }
//        }
//        String sep = System.getProperty("file.separator");
//        String filePath = file_upload_dir + sep + schema_name + sep + "template" + sep + "work" + sep + "swisslog_inspection_contents_report.xlsx";
//        File targetFile = new File(filePath);
//        FileInputStream is = null;
//        OutputStream os = null;
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String fileName = "inspection_contents_report_" + sdf.format(new Date()) + ".xlsx";
//            // 下面几行是为了解决文件名乱码的问题
//            response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(), "iso-8859-1"));
//            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
//            response.setHeader("Pragma", "no-cache");
//            response.setHeader("Cache-Control", "no-cache");
//            response.setDateHeader("Expires", 0);
//            os = response.getOutputStream();
//            is = new FileInputStream(targetFile);
//            Workbook wb = new XSSFWorkbook(is);
//            Sheet hssfSheet = wb.getSheetAt(0);
//            if(hssfSheet != null){
//                SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT_SS);
//                CellStyle style = wb.createCellStyle();
//                style.setBorderLeft(BorderStyle.THIN);
//                style.setBorderRight(BorderStyle.THIN);
//                style.setBorderTop(BorderStyle.THIN);
//                style.setBorderBottom(BorderStyle.THIN);
//                style.setVerticalAlignment(VerticalAlignment.CENTER);
//                style.setAlignment(HorizontalAlignment.CENTER);
//                CellStyle leftStyle = wb.createCellStyle();
//                leftStyle.setBorderLeft(BorderStyle.THIN);
//                leftStyle.setBorderRight(BorderStyle.THIN);
//                leftStyle.setBorderTop(BorderStyle.THIN);
//                leftStyle.setBorderBottom(BorderStyle.THIN);
//                leftStyle.setVerticalAlignment(VerticalAlignment.CENTER);
//                leftStyle.setAlignment(HorizontalAlignment.LEFT);
//                int length = dataList.size();
//                for(int i = 0;i< length; i++){
//                    Map<String, Object> subPage = dataList.get(i);
//                    Row row = hssfSheet.getRow(i + 2);
//                    if(row == null)
//                        row = hssfSheet.createRow(i + 2);
//
//                    int k = 0;
//                    while(k < 7){
//                        Cell cell = row.getCell(k);
//                        if(cell == null)
//                            cell = row.createCell(k);
//
//                        cell.setCellStyle(k > 3?leftStyle:style);
//                        k++;
//                    }
//                    row.getCell(0).setCellValue(i+1);
//                    row.getCell(1).setCellValue(subPage.get("strname") != null?String.valueOf(subPage.get("strname")):"");
//                    row.getCell(2).setCellValue(subPage.get("bom_name") != null?String.valueOf(subPage.get("bom_name")):"");
//                    row.getCell(3).setCellValue(subPage.get("problem_note") != null?String.valueOf(subPage.get("problem_note")):"");
//                    row.getCell(4).setCellValue(subPage.get("result_note") != null?String.valueOf(subPage.get("result_note")):"");
//                    row.getCell(5).setCellValue(subPage.get("finished_time") != null?df.format(subPage.get("finished_time")):"");
//                    row.getCell(6).setCellValue(subPage.get("remark") != null?String.valueOf(subPage.get("remark")):"");
//                }
//            }
//            wb.write(os);
//            //关闭流
//            wb.close();
//            os.close();
//        } catch (FileNotFoundException e) {
//            logger.error("", e);
//        } catch (IOException e) {
//            logger.error("", e);
//        } finally {
//            if (null != is) {
//                try {
//                    is.close();
//                } catch (Exception e) {
//                    logger.error("", e);
//                }
//            }
//            if (null != os){
//                try {
//                    os.close();
//                } catch (IOException e) {
//                    logger.error("", e);
//                }
//            }
//        }
//    }
}
