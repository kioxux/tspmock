package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetTransferService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 上午 11:15 2019/4/30 0030
 */
@Service
public class AssetTransferServiceImpl implements AssetTransferService {
//
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    AssetTransferMapper assetTransferMapper;
//
//    @Autowired
//    AssetPositionMapper assetPositionMapper;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    LogsService logsService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    AssetInventoryMapper assetInventoryMapper;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    MessageService messageService;
//
////    /**
////     * @Description:查看详情
////     * @param schema_name
////     * @param id
////     * @return
////     */
////    @Override
////    public AssetTransferModel selectDetailById(String schema_name, int id) {
////        AssetTransferModel model = assetTransferMapper.selectDetailById(schema_name,id);
////        return model;
////    }
//
//    /**
//     * @param schena_name
//     * @param condition
//     * @return
//     * @Description：查看列表
//     */
//    @Override
//    public List<Map<String, Object>> selectAllDiscard(String schena_name, String condition, User loginUser) {
//        List<Map<String, Object>> all = assetTransferMapper.selectAllDiscard(schena_name, condition);
//        //查询获取流程taskId、按钮权限等数据
//        handlerFlowData(schena_name, loginUser.getAccount(), all);
//        return all;
//    }
//
//    @Override
//    public int selectAllDiscardNum(String schena_name, String condition) {
//        int num = assetTransferMapper.selectAllDiscardNum(schena_name, condition);
//        return num;
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("transfer_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("transfer_code");
//            //获取流程信息
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//    /**
//     * 设备调拨-调出审核
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param user
//     * @param auditNode   审核节点
//     * @return
//     */
//    @Override
//    public ResponseModel transferAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, AssetTransferAuditNode auditNode) throws Exception {
//        String title = selectOptionService.getLanguageInfo(auditNode.getVlaue());
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_DATA_WRONG));//当前设备调拨信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> asset_transfer_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_transfer");
//        String body_property = (String) asset_transfer_tmp.get("body_property");
//        String transfer_code = (String) asset_transfer_tmp.get("transfer_code");
//        String send_file_ids = (String) asset_transfer_tmp.get("send_file_ids");//调出附件ID
//        String receive_file_ids = (String) asset_transfer_tmp.get("receive_file_ids");//接收附件ID
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String approve_remark = (String) map_object.get("approve_remark");//意见
//        String inv_position_id = (String) map_object.get("inv_position_id");//位置
//        if (StringUtils.isBlank(transfer_code) || StringUtils.isBlank(do_flow_key))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//
//        if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key) && StringUtils.isBlank(approve_remark))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_REMARK_NULL_AUDIT_FAIL), title));//意见不能为空，%s审核失败
//
//        Map<String, Object> assetTransfer = assetTransferMapper.selectDetailByTransferCode(schema_name, transfer_code);
//        if (assetTransfer == null)
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_ERROR_AUDIT_FAIL), title));//设备调拨信息异常，%s审核失败
//
//        if (!String.valueOf(assetTransfer.get("status")).equals(String.valueOf(StatusConstant.TO_BE_AUDITED)))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_STATUS_ERROR_AUDIT_FAIL), title));//设备调拨状态异常，%s审核失败
//
//        String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, transfer_code);
//        if (StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_FLOW_ERROR_AUDIT_FAIL), title));//设备调拨流程信息异常，%s审核失败
//
//        int result = 0;
//        int status = 0;
//        if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {//如果审核同意，则更新调拨申请附件
//            if (auditNode == AssetTransferAuditNode.TRANSFER_IN_ASSET) {//接收设备
//                status = StatusConstant.COMPLETED;
//                result = assetTransferMapper.updateReceiveFile(schema_name, receive_file_ids, status, body_property, transfer_code);//更新附件，修改状态为“已完成”
//                if (result > 0) {
//                    //接收审核通过时，变更设备位置
//                    String positionId = (String) assetTransfer.get("position_id");
//                    String newPositionId = (String) assetTransfer.get("new_position_id");
//                    String assetIds = (String) assetTransfer.get("relation_id");
//                    String positionName = selectOptionService.getOptionNameByCode(schema_name, "facilities", positionId, null);
//                    String newPositionName = selectOptionService.getOptionNameByCode(schema_name, "facilities", newPositionId, null);
//                    String source_sub_code = (String) assetTransfer.get("source_sub_code");
//                    boolean isSource = false;
//                    if (StringUtils.isNotBlank(source_sub_code) && source_sub_code.startsWith("AID")) {
//                        isSource = true;
//                    }
//                    Map<String, Map<String, Object>> assetListInfo = doSetAssetUpdateInfo(schema_name, transfer_code, assetIds, map_object, null);
//                    changeAssetPositionByString(schema_name, positionId, newPositionId, positionName, newPositionName, assetIds, assetListInfo, user.getAccount(), isSource);
//
//                    //如果是设备盘点发起的调拨申请，则调拨完后同步更新盘点数据
//                    if (isSource) {
//                        updateAssetInventoryStatus(schema_name, assetIds, source_sub_code, user.getAccount(), positionName, newPositionName, assetListInfo);
//                    }
//                }
//            } else if (auditNode == AssetTransferAuditNode.TRANSFER_OUT_ASSET) {//调出设备
//                result = assetTransferMapper.updateSendFile(schema_name, send_file_ids, body_property, transfer_code);//更新附件
//            } else if (auditNode == AssetTransferAuditNode.TRANSFER_OUT_AUDIT || auditNode == AssetTransferAuditNode.TRANSFER_IN_AUDIT) {//调出审核、调入审核
//                result = assetTransferMapper.updateBodyProperty(schema_name, body_property, transfer_code);//更新bodyProperty
//            } else {
//                throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_FLOW_ERROR_AUDIT_FAIL), title));//设备调拨流程信息异常，%s审核失败
//            }
//        } else if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {//如果审核退回，则调拨申请状态变更为“处理中”
//            status = StatusConstant.PENDING;
//            result = assetTransferMapper.updateStatus(schema_name, status, transfer_code);
//        } else {
//            throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_FLOW_ERROR_AUDIT_FAIL), title));//设备调拨流程信息异常，%s审核失败
//        }
//        if (result > 0) {
//            String title_page = "";
//            String logRemark = "";
//            if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//                title_page = selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_BACK);//设备调拨退回
//                logRemark = String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_AUDIT_BACK), title);//设备%s审核退回
//            } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//                title_page = String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_AUDIT_SUCCESS), title);//设备%s审核成功
//                logRemark = String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_AUDIT_SUCCESS), title);//设备%s审核成功
//            }
//            if (StringUtils.isNotBlank(approve_remark))
//                logRemark += "（" + selectOptionService.getLanguageInfo(LangConstant.WM_OA) + "：" + approve_remark + "）";//意见
//
//            //完成流程
//            Map<String, String> map = new HashMap<>();
//            map.put("sub_work_code", transfer_code);
//            map.put("title_page", title_page);
//            map.put("do_flow_key", do_flow_key);
//            map.put("facility_id", inv_position_id); // 所属位置
//            if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//                //如果是审核退回，则退回给申请人
//                map.put("receive_account", (String) assetTransfer.get("create_user_account"));
//            } else {
//                map.put("receive_account", commonUtilService.getRandomUserByInfo(schema_name, null, map, map_object));
//            }
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, transfer_code);
//            boolean isSuccess = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//            if (isSuccess) {
//                if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)
//                        || (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key) && auditNode == AssetTransferAuditNode.TRANSFER_IN_ASSET))
//                    workProcessService.saveWorkProccess(schema_name, transfer_code, status, user.getAccount(), taskSid);//工单进度记录
//
//                messageService.msgPreProcess(ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)?qcloudsmsConfig.SMS_10002006:qcloudsmsConfig.SMS_10002005,
//                        map.get("receive_account"), SensConstant.BUSINESS_NO_22, transfer_code, user.getAccount(), user.getUsername(), null, null); // 发送短信
//                logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_22), transfer_code, logRemark, user.getAccount());
//                return ResponseModel.ok("ok");
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_AUDIT_ERROR), title));//设备%s审核出现错误，请重试
//            }
//        } else {
//            throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作出现错误，请重试
//        }
//    }
//
//    /**
//     * 变更设备位置
//     *
//     * @param schema_name
//     * @param positionId
//     * @param newPositionId
//     * @param positionName
//     * @param newPositionName
//     * @param assetIds
//     * @param assetListInfo
//     * @param account
//     * @param isSource
//     */
//    private void changeAssetPositionByString(String schema_name, String positionId, String newPositionId, String positionName,
//                                             String newPositionName, String assetIds, Map<String, Map<String, Object>> assetListInfo,
//                                             String account, boolean isSource) {
//        if (StringUtils.isNotBlank(assetIds)) {
//            String[] assetIdArray = assetIds.split(",");
//            changeAssetPosition(schema_name, positionId, newPositionId, positionName, newPositionName, assetIdArray, assetListInfo, account, isSource);
//        }
//    }
//
//    /**
//     * 变更设备位置
//     *
//     * @param schema_name
//     * @param positionId
//     * @param newPositionId
//     * @param positionName
//     * @param newPositionName
//     * @param assetIds
//     * @param assetListInfo
//     * @param account
//     * @param isSource
//     */
//    private void changeAssetPosition(String schema_name, String positionId, String newPositionId, String positionName,
//                                     String newPositionName, String[] assetIds, Map<String, Map<String, Object>> assetListInfo,
//                                     String account, boolean isSource) {
//        for (String asset_id : assetIds) {
//            if (positionId.startsWith("P")) {
//                //如果设备原先在position，则需要先清空位置code，然后把设备组织关联表数据也清楚
//                List<String> orgIds = assetPositionMapper.getOrganizationList(schema_name, positionId);
//                StringBuffer str = new StringBuffer();
//                for (String orgId : orgIds) {
//                    if (str.length() != 0)
//                        str.append(",");
//
//                    str.append(orgId);
//                }
//                assetTransferMapper.discardAssetFacility(schema_name, "", asset_id);
//                assetTransferMapper.deleteAssetOrgs(schema_name, str.toString(), asset_id);
//            } else {
//                assetTransferMapper.deleteAssetOrg(schema_name, Long.valueOf(positionId), asset_id);
//            }
//            if (newPositionId.startsWith("P")) {
//                //如果调拨后的位置是position，则更新设备position_code，同时还要把position所属的组织也添加到设备组织关联表
//                List<String> orgIds = assetPositionMapper.getOrganizationList(schema_name, newPositionId);
//                assetTransferMapper.discardAssetFacility(schema_name, newPositionId, asset_id);
//                for (String orgId : orgIds) {
//                    assetTransferMapper.addAssetOrg(schema_name, Long.valueOf(orgId), asset_id);
//                }
//            } else {
//                assetTransferMapper.addAssetOrg(schema_name, Long.valueOf(newPositionId), asset_id);
//            }
//            if (!isSource) {
//                doSetAssetUpdateLog(schema_name, positionName, newPositionName, asset_id, assetListInfo, account, LangConstant.LOG_TRANSFER_A);
//            }
//        }
//    }
//
//    /**
//     * 调度申请报废
//     *
//     * @param schema_name
//     * @param user
//     * @param transfer_code
//     * @return
//     */
//    @Override
//    public ResponseModel cancel(String schema_name, User user, String transfer_code) {
//        int doUpdate = assetTransferMapper.updateStatus(schema_name, StatusConstant.CANCEL, transfer_code);
//        if (doUpdate > 0) {
//            boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, transfer_code, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            if (deleteflow) {
//                workProcessService.saveWorkProccess(schema_name, transfer_code, StatusConstant.CANCEL, user.getAccount(), Constants.PROCESS_CANCEL_TASK);//工单进度记录
//                logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_22), transfer_code, selectOptionService.getLanguageInfo(LangConstant.ASSET_TRANS_CANCEL), user.getAccount());//设备调度报废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_DISCARD_SUCCESS));//报废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_CANCEL_FLOW_FAIL));//设备调拨报废流程调用失败
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_CANCEL_FAIL));//设备调拨报废失败
//        }
//    }
//
//    /**
//     * 申请设备调拨
//     *
//     * @param schema_name
//     * @param request
//     * @param user
//     * @param processDefinitionId
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel add(String schema_name, HttpServletRequest request, User user, String processDefinitionId, Map<String, Object> paramMap) throws Exception {
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_DATA_WRONG));//当前设备调拨信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> asset_transfer_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_transfer");
//        Map<String, Object> asset_transfer_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_transfer_detail");
//        Map<String, Object> keys = (Map<String, Object>) map_object.get("keys");
//        Object body_property = paramMap.get("body_property");
//        String key = (String) keys.get("_sc_asset_transfer");
//        String inv_position_id = (String) map_object.get("inv_position_id");//位置
//        String work_request_type = "";
//        try {
//            work_request_type = (String) map_object.get("work_request_type");
//        } catch (Exception statusExp) {
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        boolean isNew = true;
//        if (hasAudit) {
//            //判断是否是新增
//            if (!key.startsWith(SqlConstant.IS_NEW_DATA))
//                isNew = false;
//        }
//        return addTransfer(schema_name, do_flow_key, user, processDefinitionId, map_object, asset_transfer_tmp, asset_transfer_detail_tmp, body_property.toString(), inv_position_id, null, isNew, hasAudit);
//    }
//
//    /**
//     * 生成设备调拨以及调拨流程
//     *
//     * @param schema_name
//     * @param do_flow_key
//     * @param user
//     * @param processDefinitionId
//     * @param map_object
//     * @param asset_transfer_tmp
//     * @param asset_transfer_detail_tmp
//     * @param body_property
//     * @param inv_position_id
//     * @param source                    来源：盘点子表编码（盘点调整发起的调拨申请）
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel addTransfer(String schema_name, String do_flow_key, User user, String processDefinitionId, Map<String, Object> map_object,
//                                     Map<String, Object> asset_transfer_tmp, Map<String, Object> asset_transfer_detail_tmp, String body_property,
//                                     String inv_position_id, String source, boolean isNew, boolean hasAudit) throws Exception {
//        String title = hasAudit ? (isNew ? selectOptionService.getLanguageInfo(LangConstant.ADD_K) : selectOptionService.getLanguageInfo(LangConstant.MOD_A)) : "";//新增；修改
//        String asset_ids = (String) asset_transfer_detail_tmp.get("relation_id");
//        String newPositionId = (String) asset_transfer_tmp.get("new_position_id");
//        String positionId = (String) asset_transfer_tmp.get("position_id");
//        String remark = (String) asset_transfer_tmp.get("remark");
//        String subWorkCode = (String) asset_transfer_tmp.get("transfer_code");
//        String account = user.getAccount();
//
//        if (StringUtils.isBlank(positionId) || "-1".equals(positionId))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_FACILITY_NULL));//设备位置不能为空
//
//        if (StringUtils.isBlank(newPositionId) || "-1".equals(newPositionId))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TO_FACILITY_NULL));//调往位置不能为空
//
//        if (positionId.equals(newPositionId))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_FACILITY_IS_SAME));//设备位置、调往位置不能相同
//
//        if (StringUtils.isBlank(asset_ids)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_IS_NULL));//设备不能为空
//        }
//        String[] assetIds = asset_ids.split(",");
//        List<Map<String, Object>> assets = assetMapper.getAssetDetailByIds(schema_name, assetIds);
//        Map<String, Map<String, Object>> assetListDbInfo = null;
//        if (assets != null && assets.size() > 0) {
//            for (Map<String, Object> asset : assets) {
//                if (asset == null) {
//                    return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_ERROR));//调拨单出现错误
//                } else if ((Integer) asset.get("intstatus") == StatusConstant.ASSET_INTSTATUS_DISCARD) {
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARDED_TRANS_FAIL), asset.get("strname")));//设备“%s”已经报废，无法申请调拨
//                }
//                //校验设备是否意见在调往的位置下
//                if (newPositionId.startsWith("P") && newPositionId.equals((String) asset.get("position_code"))) {
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_AT_FACILITY_TRANS_FAIL), asset.get("strname")));//设备“%s”已经在调往位置下，申请调拨失败
//                } else if (!newPositionId.startsWith("P")) {
//                    String[] orgIds = ((String) asset.get("org_id")).split(",");
//                    for (String orgId : orgIds) {
//                        if (newPositionId.equals(orgId))
//                            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_AT_FACILITY_TRANS_FAIL), asset.get("strname")));//设备“%s”已经在调往位置下，申请调拨失败
//                    }
//                }
//                if (null == assetListDbInfo) {
//                    assetListDbInfo = new HashMap<String, Map<String, Object>>();
//                }
//                assetListDbInfo.put((String) asset.get("_id"), asset);
//            }
//        } else {
//            return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_ERROR));//调拨单出现错误
//        }
//        //查询设备申请调拨情况
//        List<Map<String, Object>> assetTransferStatusList = assetMapper.getAssetTransferStatusByIds(schema_name, assetIds);
//        if (assetTransferStatusList != null && assetTransferStatusList.size() > 0) {
//            for (Map<String, Object> asset : assetTransferStatusList) {
//                if (isNew || (!isNew && !subWorkCode.equals(asset.get("transfer_code")))) {
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_APPLY_REPEAT), asset.get("strname")));//设备“%s”正在调拨处理中，无法重复申请调拨
//                }
//            }
//        }
//        AssetTransferModel assetTransferModel = new AssetTransferModel();
//        assetTransferModel.setTransfer_code(subWorkCode);
//        assetTransferModel.setPosition_id(positionId);
//        assetTransferModel.setSchema_name(schema_name);
//        assetTransferModel.setRemark(remark);
//        assetTransferModel.setNew_position_id(newPositionId);
//        assetTransferModel.setSource_sub_code(source);//来源
//        if (hasAudit)
//            assetTransferModel.setStatus(StatusConstant.TO_BE_AUDITED);//待审核状态
//        else
//            assetTransferModel.setStatus(StatusConstant.COMPLETED);//如果没有审核流程，则直接变更为“已完成”状态
//
//        assetTransferModel.setBody_property(body_property);
//        if (isNew) {
//            assetTransferModel.setCreate_user_account(account);
//            assetTransferModel.setCreate_time(new Timestamp(System.currentTimeMillis()));
//        }
//
//        int doAdd = 0;
//        if (ButtonConstant.BTN_SUBMIT == Integer.valueOf(do_flow_key)) {//提交调拨申请
//            if (isNew)
//                doAdd = assetTransferMapper.add(assetTransferModel);
//            else
//                doAdd = assetTransferMapper.updateAssetTransfer(assetTransferModel);
//
//            if (doAdd > 0) {
////                if (!isNew)//如果是修改，则要把旧的设备调拨详情表记录清空
////                    assetTransferMapper.deleteAssetTransferDetail(schema_name, subWorkCode);
//
//                Map<String, Map<String, Object>> assetListInfo = doSetAssetUpdateInfo(schema_name, subWorkCode, asset_ids, map_object, assetListDbInfo);
////                doAdd = assetTransferMapper.addAssetTransferDetails(schema_name, assetTransferDetails);
////                if (doAdd == 0) {
////                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
////                    return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_ERROR));//调拨单出现错误
////                } else {
//                if (!hasAudit) {//流程如果没有审核节点，则直接审核通过，并变更设备位置
//                    String positionName = selectOptionService.getOptionNameByCode(schema_name, "facilities", positionId, null);
//                    String newPositionName = selectOptionService.getOptionNameByCode(schema_name, "facilities", newPositionId, null);
//                    boolean isSource = false;
//                    if (StringUtils.isNotBlank(source) && source.startsWith("AID")) {
//                        isSource = true;
//                    }
//                    changeAssetPosition(schema_name, positionId, newPositionId, positionName, newPositionName, assetIds, assetListInfo, account, isSource);
//                    //如果是设备盘点发起的调拨申请，则调拨完后同步更新盘点数据
//                    if (isSource) {
//                        updateAssetInventoryStatus(schema_name, assetIds[0], source, account, positionName, newPositionName, assetListInfo);
//                    }
//                }
////                }
//            }
//        }
//        if (doAdd > 0) {
//            //流程数据，跳转到审核流程
//            Map map = new HashMap<>();
//            map.put("title_page", selectOptionService.getLanguageInfo(LangConstant.ASSET_TRANSFER));//设备调拨
//            map.put("sub_work_code", subWorkCode);
//            map.put("workCode", subWorkCode);
//            map.put("do_flow_key", do_flow_key);
//            map.put("status", String.valueOf(assetTransferModel.getStatus()));
//            map.put("create_user_account", account);
//            map.put("create_time", UtilFuns.sysTime());
//            map.put("facility_id", inv_position_id); // 所属位置
//            map.put("receive_account", commonUtilService.getRandomUserByInfo(schema_name, null, map, map_object));
//            if (isNew) {
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_FLOW_START_FAIL));//设备调拨流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, subWorkCode, assetTransferModel.getStatus(), account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//            } else {
//                String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, subWorkCode);
//                if (StringUtils.isBlank(taskId))
//                    throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_FLOW_ERROR_APPLY_FAIL), title));//设备调拨流程信息异常，%s设备调拨申请失败
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//                boolean isSuccess = workflowService.complete(schema_name, taskId, account, map);
//                if (!isSuccess) {
//                    throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_FLOW_START_FAIL));//设备调拨流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, subWorkCode, assetTransferModel.getStatus(), account, taskSid);//工单进度记录
//            }
//            if (hasAudit) {
//                messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, (String) map.get("receive_account"), SensConstant.BUSINESS_NO_22, subWorkCode, account, user.getUsername(), null, null); // 发送短信
//            }
//            title = title + selectOptionService.getLanguageInfo(SensConstant.LOG_TYPE_NAME.get(SensConstant.BUSINESS_NO_22)) + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);
//            logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_22), subWorkCode, title, account);//记录历史;成功
//            if (StringUtils.isNotBlank(source))
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANSFER_APPLY_SUCC));//设备调拨申请成功
//            else
//                return ResponseModel.ok("ok");
//        } else {
//            throw new SenscloudException(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_ERROR));//调拨单出现错误
//        }
//    }
//
//    /**
//     * 更新设备盘点状态
//     *
//     * @param schemaName
//     * @param assetId
//     * @param sourceSubCode
//     * @param account
//     * @param positionName
//     * @param newPositionName
//     * @param assetListInfo
//     */
//    private void updateAssetInventoryStatus(String schemaName, String assetId, String sourceSubCode, String account, String positionName, String newPositionName, Map<String, Map<String, Object>> assetListInfo) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("asset_id", assetId);
//        map.put("deal_result", 2);
//        map.put("sub_inventory_code", sourceSubCode);
//        assetInventoryMapper.updateAssetInventoryOrgDetailDealResult(schemaName, map);
//        doSetAssetUpdateLog(schemaName, positionName, newPositionName, assetId, assetListInfo, account, LangConstant.LOG_TRANSFER_B);
//    }
//
//    /**
//     * 根据code查询设备调拨数据
//     *
//     * @param schemaName
//     * @param transferCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryAssetTransferByCode(String schemaName, String transferCode) {
//        return assetTransferMapper.selectDetailByTransferCode(schemaName, transferCode);
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("transfer_code");
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        Map<String, Object> assetTransfer = assetTransferMapper.selectDetailByTransferCode(schemaName, code);
//        long status = (Long) assetTransfer.get("status");
//        if (StatusConstant.COMPLETED != status && StatusConstant.CANCEL != status) {//未完成的调拨单可以分配
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_22, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/assetTransfer/findSingleAtInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//
//    /**
//     * 更新设备信息使用（提交时使用，保存不可使用）
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @param assetIds
//     * @param map_object
//     * @param assetListDbInfo
//     * @return
//     * @throws Exception
//     */
//    private Map<String, Map<String, Object>> doSetAssetUpdateInfo(String schemaName, String subWorkCode, String assetIds, Map<String, Object> map_object, Map<String, Map<String, Object>> assetListDbInfo) throws Exception {
//        //assetExecution 1、重置明细表 2、更新编码
//        if (map_object.containsKey("assetExecution") && map_object.containsKey("relationIdContent")) {
//            String assetExecution = (String) map_object.get("assetExecution");
//            if (RegexUtil.isNotNull(assetExecution)) {
//                Map<String, Map<String, Object>> assetListInfo = null;
//                List<Map<String, Object>> assetList = (List<Map<String, Object>>) map_object.get("relationIdContent");
//                if (null != assetList && assetList.size() > 0) {
//                    if (null == assetListDbInfo || assetListDbInfo.size() == 0) {
//                        String[] astIds = assetIds.split(",");
//                        List<Map<String, Object>> assets = assetMapper.getAssetDetailByIds(schemaName, astIds);
//                        if (assets != null && assets.size() > 0) {
//                            for (Map<String, Object> asset : assets) {
//                                if (null == assetListDbInfo) {
//                                    assetListDbInfo = new HashMap<String, Map<String, Object>>();
//                                }
//                                assetListDbInfo.put((String) asset.get("_id"), asset);
//                            }
//                        } else {
//                            throw new SenscloudException(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_TRANS_ERROR));//调拨单出现错误
//                        }
//                    }
//
//                    Map<String, String> newAssetCode = new HashMap<String, String>();
//                    Set<String> assetCodeNewInfo = new HashSet<String>();
//                    for (Map<String, Object> assetMap : assetList) {
//                        String pageAssetId = (String) assetMap.get("_id");
//                        String assetCodeNew = (String) assetMap.get("asset_code_new");
//                        if (RegexUtil.isNotNull(pageAssetId) && RegexUtil.isNotNull(assetCodeNew)) {
//                            if (assetCodeNewInfo.contains(assetCodeNew)) {
//                                // 设备编码重复，请重新编辑？
//                                throw new SenscloudException("【" + assetCodeNew + "】" +
//                                        selectOptionService.getLanguageInfo(LangConstant.ASSET_CODE_REPEAT_TO_RESET));
//                            }
//                            assetCodeNewInfo.add(assetCodeNew);
//                            newAssetCode.put(pageAssetId, assetCodeNew);
//                        }
//                    }
//                    for (Map<String, Object> assetMap : assetList) {
//                        String pageAssetId = (String) assetMap.get("_id");
//                        assetMap.put("asset_id", pageAssetId);
//                        assetMap.put("transfer_code", subWorkCode);
//                        if (RegexUtil.isNotNull(pageAssetId)) {
//                            if (null != assetListDbInfo && assetListDbInfo.size() > 0 && assetListDbInfo.containsKey(pageAssetId)) {
//                                boolean isUpdateAsset = false;
//                                Map<String, Object> assetDbInfo = assetListDbInfo.get(pageAssetId);
//                                if (assetMap.containsKey("asset_code_new") && assetDbInfo.containsKey("strcode")) {
//                                    String assetCode = (String) assetDbInfo.get("strcode");
//                                    String assetCodeNew = (String) assetMap.get("asset_code_new");
//                                    if (RegexUtil.isNotNull(assetCodeNew) && RegexUtil.isNotNull(assetCode) && !assetCode.equals(assetCodeNew)) {
//                                        List<Asset> dbAssetList = assetDataServiceV2.findByCode(schemaName, assetCodeNew);
//                                        if (null != dbAssetList && dbAssetList.size() > 0) {
//                                            String asId = dbAssetList.get(0).get_id();
//                                            if (!pageAssetId.equals(asId)) {
//                                                if (!newAssetCode.containsKey(asId) || assetCodeNew.equals(newAssetCode.get(asId))) {
//                                                    // 设备编码重复，请重新编辑？
//                                                    throw new SenscloudException("【" + assetCodeNew + "】" +
//                                                            selectOptionService.getLanguageInfo(LangConstant.ASSET_CODE_REPEAT_TO_RESET));
//                                                }
//                                            }
//                                        }
//                                        if (!"1".equals(assetExecution)) {
////                                if (!hasAudit) {
//                                            assetMap.put("varchar@strcode", "strcode");
//                                            assetMap.put("strcode", assetCodeNew);
//                                            assetMap.put("asset_code", assetCode);
//                                            isUpdateAsset = true;
////                                }
//                                        }
//                                    }
//                                }
//                                if (isUpdateAsset) {
//                                    if (null == assetListInfo) {
//                                        assetListInfo = new HashMap<String, Map<String, Object>>();
//                                    }
//                                    assetListInfo.put(pageAssetId, assetMap);
//                                }
//                            }
//                        }
//                    }
//                }
//                if (!"2".equals(assetExecution)) {
//                    assetTransferMapper.deleteAssetTransferDetail(schemaName, subWorkCode);
//                    if (map_object.containsKey("relationIdContent")) {
//                        selectOptionService.batchInsertDatas(schemaName, assetList, SqlConstant.WORK_DB_TABLES[21], SqlConstant._sc_asset_allocation_detail_columns);
////                } else {
////                    String[] astIds = assetIds.split(",");
////                    // 盘点时的调拨，不处理编码变更等特殊逻辑
////                    List<AssetTransferDetailModel> assetTransferDetails = new ArrayList<>();
////                    for (String assetId : astIds) {
////                        AssetTransferDetailModel assetDetail = new AssetTransferDetailModel();
////                        assetDetail.setTransfer_code(subWorkCode);
////                        assetDetail.setAsset_id(assetId);
////                        assetTransferDetails.add(assetDetail);
////                    }
////                    assetTransferMapper.addAssetTransferDetails(schemaName, assetTransferDetails);
//                    }
//                }
//                if (!"1".equals(assetExecution)) {
//                    return assetListInfo;
//                }
//            }
//        }
//        return null;
//    }
//
//    /**
//     * 设备更新日志
//     *
//     * @param schema_name
//     * @param positionName
//     * @param newPositionName
//     * @param assetId
//     * @param assetListInfo
//     * @param account
//     * @param logContent
//     */
//    private void doSetAssetUpdateLog(String schema_name, String positionName,
//                                     String newPositionName, String assetId, Map<String, Map<String, Object>> assetListInfo,
//                                     String account, String logContent) {
//        logContent = positionName + selectOptionService.getLanguageInfo(logContent) + newPositionName;
//        if (null != assetListInfo && assetListInfo.size() > 0
//                && assetListInfo.containsKey(assetId)) {
//            Map<String, Object> assetMap = assetListInfo.get(assetId);
//            workSheetHandleMapper.update(schema_name, assetMap, "_sc_asset", "_id"); // 更新字段
//            logContent = logContent + "，" + selectOptionService.getLanguageInfo(LangConstant.TITLE_ASSET_AE)
//                    + "【" + assetMap.get("asset_code") + "】" + selectOptionService.getLanguageInfo(LangConstant.MSG_BE_EDIT)
//                    + "【" + assetMap.get("strcode") + "】";
//        }
//        logsService.AddLog(schema_name, "asset", assetId, logContent, account);
//    }
}
