package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.BatchReportService;
import org.springframework.stereotype.Service;

/**
 * 批报查询
 */
@Service
public class BatchReportServiceImpl implements BatchReportService {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private BatchReportMapper batchReportMapper;
//
//    /**
//     * 查询BTS类型设备的收货量批报
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBTSReceiveBatchReportList(HttpServletRequest request) {
//        String[] columns = new String[]{Constants.ASSET_MONITOR_RECEIVE_START_TIME, Constants.ASSET_MONITOR_RECEIVE_END_TIME,Constants.ASSET_MONITOR_RECEIVE_SHIPMENT,
//                Constants.ASSET_MONITOR_RECEIVE_ACTUAL,Constants.ASSET_MONITOR_RECEIVE_DEVIATION,Constants.ASSET_MONITOR_RECEIVE_RUNNING_TIME};
//        return queryBatchReportList(request, columns, Constants.ASSET_CATEGORY_NAME_BTS, Constants.MONITOR_CATEGORY_STORAGE_A);
//    }
//
//    /**
//     * 查询BTS类型设备的出库量批报
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBTSLoadingBatchReportList(HttpServletRequest request) {
//        String[] columns = new String[]{Constants.ASSET_MONITOR_LOADING_START_TIME,Constants.ASSET_MONITOR_LOADING_END_TIME,Constants.ASSET_MONITOR_LOADING_TOTAL};
//        return queryBatchReportList(request, columns, Constants.ASSET_CATEGORY_NAME_BTS, Constants.MONITOR_CATEGORY_OV_A);
//    }
//
//    /**
//     * 查询BTS类型设备的调拨量批报
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBTSAllocationBatchReportList(HttpServletRequest request) {
//        String[] columns = new String[]{Constants.ASSET_MONITOR_ALLOCATION_START_TIME,Constants.ASSET_MONITOR_ALLOCATION_END_TIME,
//                Constants.ASSET_MONITOR_ALLOCATION_TARGET,Constants.ASSET_MONITOR_ALLOCATION_ACTUAL};
//        return queryBatchReportList(request, columns, Constants.ASSET_CATEGORY_NAME_BTS, Constants.MONITOR_CATEGORY_TRANSFER_A);
//    }
//
//    /**
//     * 查询PPLA类型设备批报
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPPLABatchReportList(HttpServletRequest request) {
//        String[] columns = new String[]{Constants.ASSET_MONITOR_PPLA_BATCH_START_TIME,Constants.ASSET_MONITOR_PPLA_BATCH_END_TIME,
//                Constants.ASSET_MONITOR_PPLA_BATCH_LIQA_TARGET_TOTAL,Constants.ASSET_MONITOR_PPLA_BATCH_LIQA_ACTUAL_TOTAL,
//                Constants.ASSET_MONITOR_PPLA_BATCH_LIQA_DEVIATION_RATE,Constants.ASSET_MONITOR_PPLA_BATCH_LIQB_TARGET_TOTAL,
//                Constants.ASSET_MONITOR_PPLA_BATCH_LIQB_ACTUAL_TOTAL,Constants.ASSET_MONITOR_PPLA_BATCH_LIQB_DEVIATION_RATE,
//                Constants.ASSET_MONITOR_PPLA_BATCH_WATER_TOTAL,Constants.ASSET_MONITOR_PPLA_BATCH_LIQA_INCLUSION_RATE,
//                Constants.ASSET_MONITOR_PPLA_BATCH_LIQB_INCLUSION_RATE,Constants.ASSET_MONITOR_PPLA_BATCH_WATER_INCLUSION_RATE,
//                Constants.ASSET_MONITOR_PPLA_BATCH_RUNNING_TIME,Constants.ASSET_MONITOR_PPLA_BATCH_SOLIDFLOW_AVG_FLOWRATE};
//        return queryBatchReportList(request, columns, Constants.ASSET_CATEGORY_NAME_PPLA, null);
//    }
//
//    /**
//     * 查询MLA类型设备批报
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findMLABatchReportList(HttpServletRequest request) {
//        String[] columns = new String[]{Constants.ASSET_MONITOR_MLA_BATCH_START_TIME,Constants.ASSET_MONITOR_MLA_BATCH_END_TIME,
//                Constants.ASSET_MONITOR_MLA_BATCH_TARGET,Constants.ASSET_MONITOR_MLA_BATCH_ACTUAL,
//                Constants.ASSET_MONITOR_MLA_BATCH_DOSING_TIME,Constants.ASSET_MONITOR_MLA_BATCH_DEVIATION,
//                Constants.ASSET_MONITOR_MLA_BATCH_DEVIATION_RATE,Constants.ASSET_MONITOR_MLA_BATCH_AVG_ADDITION_RATE};
//        return queryBatchReportList(request, columns, Constants.ASSET_CATEGORY_NAME_AT88MLA, null);
//    }
//
//    private String splitJointCondition(String facilities){
//        String condition = "";
//        if (null != facilities && !facilities.isEmpty()) {
//            String[] fs = facilities.split(",");
//            StringBuffer fsb = new StringBuffer();
//            for (String value : fs) {
//                fsb.append(" or f.facility_no like '" + value + "%' ");
//            }
//            String nfs = fsb.toString();
//            if (!nfs.isEmpty() && !"".equals(nfs)) {
//                nfs = nfs.substring(3);
//                condition += " and ( " + nfs + ") ";
//            }
//        }
//        return condition;
//    }
//
//    /**
//     * 查询批报数据
//     * @param request
//     * @return
//     */
//    private String queryBatchReportList(HttpServletRequest request, String[] columns , String categoryName, Integer monitorCategory) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//
//        //首页条件
//        String condition = "";
//        String facilities = request.getParameter("facilities");
//        String searchKey = request.getParameter("searchKey");
//        String strcode = request.getParameter("strcode");
//        if(StringUtils.isNotBlank(categoryName)){
//            condition += " and ac.category_name = '"+categoryName+"' ";
//        }
//        if(monitorCategory != null){
//            condition += String.format(" and mm.monitor_category = %d ", monitorCategory);
//        }
//        if (StringUtils.isNotBlank(searchKey)) {
//            condition += " and ( asset.strname like '%" + searchKey + "%' or upper(asset.strcode) like upper('%" + searchKey + "%') ) ";
//        }
//        condition += splitJointCondition(facilities);
//
//        if(StringUtils.isNotBlank(strcode)){
//            condition += String.format(" and asset.strcode = '%s' ", strcode);
//        }
//
//        String startTime = request.getParameter("startTime");
//        String endTime = request.getParameter("endTime");
//        if (StringUtils.isNotBlank(startTime)) {
//            condition += " and am.gather_time >= '" + startTime + " 00:00:00' ";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            try {
//                Calendar cl = Calendar.getInstance();
//                cl.setTime(new SimpleDateFormat(Constants.DATE_FMT);.parse(endTime));
//                cl.add(Calendar.DAY_OF_MONTH, 1);
//                condition += " and am.gather_time <'" + new SimpleDateFormat(Constants.DATE_FMT);.format(cl.getTime()) + " 00:00:00' ";
//            } catch (ParseException e) {
//            }
//        }
//        String queryColumns = "";
//        if(columns != null && columns.length > 0){
//            String subCondition = "";
//            for(String column : columns){
//                subCondition += (subCondition==""?"":",") + "'"+column+"'";
//                queryColumns += " MAX(CASE am.monitor_name WHEN '"+ column+"' THEN am.monitor_value ELSE '' END) AS "+column+", ";
//            }
//            condition += " AND am.monitor_name IN (" + subCondition + ") ";
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = batchReportMapper.findBTSReceiveBatchReportList(schema_name, queryColumns, condition, pageSize, begin);
//        int total = batchReportMapper.countBTSReceiveBatchReportList(schema_name, queryColumns, condition);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
}
