package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.mapper.WorkScheduleMapper;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.service.dynamic.WorkScheduleService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Administrator on 2018/5/9.
 */
@Service
public class WorkScheduleServiceImpl implements WorkScheduleService {

    @Resource
    WorkScheduleMapper workScheduleMapper;
    @Resource
    UserMapper userMapper;
    //    @Autowired
//    UserService userService;
//
//    @Autowired
//    UserMapper userMapper;
//
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    CompanyService companyService;

    //
//    @Override
//    public int insertWorkSchedule(String schema_name, WorkScheduleData workScheduleData) {
//        return workScheduleMapper.insertWorkSchedule(schema_name, workScheduleData);
//    }
//
//    @Override
//    public int deleteWorkSchedule(String schema_name, int id) {
//        return workScheduleMapper.deleteWorkSchedule(schema_name, id);
//    }
//
//    @Override
//    public int updateWorlSchedule(String schema_name, WorkScheduleData workScheduleData) {
//        return workScheduleMapper.updateWorkSchedule(schema_name, workScheduleData);
//    }
//
//    @Override
//    public WorkScheduleData findById(String schema_name, int id) {
//        return workScheduleMapper.findById(schema_name, id);
//    }
//
//    @Override
//    public List<WorkScheduleData> getWorkScheduleList(String schema_name) {
//        return workScheduleMapper.getWorkScheduleList(schema_name);
//    }
//
//    //按用户组织和日期，获取排班数据
//    @Override
//    public List<WorkScheduleData> getWorkScheduleListByGroup(String schema_name, String condition) {
//        return workScheduleMapper.getWorkScheduleListByGroup(schema_name, condition);
//    }
//
//    //查询id下的排班信息
//    @Override
//    public WorkScheduleData getWorkScheduleListByFacilityById(String schema_name, int id) {
//        return workScheduleMapper.getWorkScheduleListByFacilityById(schema_name, id);
//    }
//
//    @Override
//    public int updateDate(String schema_name, int id, Timestamp begin_time, Timestamp end_time) {
//        return workScheduleMapper.updateDate(schema_name, id, begin_time, end_time);
//    }
//
////    @Override
////    public List<User> getWorkScheduleBySiteAndTime(String schema_name, String facilityId, String functionKey) {
////        return workScheduleMapper.getWorkScheduleBySiteAndTime(schema_name, facilityId, functionKey);
////    }
////
////    @Override
////    public List<User> getWorkScheduleBySiteAndTimeInterval(String schema_name, String facilityId, String functionKey) {
////        return workScheduleMapper.getWorkScheduleBySiteAndTimeInterval(schema_name, facilityId, functionKey);
////    }
//
//    @Override
//    public List<WorkScheduleData> findByFacilities(String schema_name, String facilities) {
//        return workScheduleMapper.findByFacilities(schema_name, facilities);
//    }
//
//    //获取当天排班联系负责人
//    @Override
//    public List<LeadingOfficialResult> getWorkScheduleLeadingOfficial(String schema_name, Timestamp moningTime, Timestamp afternoonTime) {
//        return workScheduleMapper.getWorkScheduleLeadingOfficial(schema_name, moningTime, afternoonTime);
//    }
//
//    //根据位置和人员权限查询对应排班人员,在固定时间上班的人员
//    //@Override
////    public List<User> getWorkScheduleBySiteAndTimeOnDuty(String schema_name, String facility_id, String functionKey, Timestamp beginDateTime, Timestamp endDateTime) {
////        return workScheduleMapper.getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(schema_name, facility_id, functionKey, beginDateTime, endDateTime);
////    }
////
////    //根据工单池、位置和人员权限查询对应排班人员,在固定时间上班的人员
////    //@Override
////    public List<User> getWorkScheduleByPoolAndSiteAndTimeOnDuty(String schema_name, String facility_id, String pool_id, String functionKey, Timestamp beginDateTime, Timestamp endDateTime) {
////        return workScheduleMapper.getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(schema_name, facility_id, pool_id, functionKey, beginDateTime, endDateTime);
////    }
//
//    //按工单类型，获取有操作权限的用户
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //rolePermissionKey: 固定角色权限key
////    @Override
////    public List<User> getDoTaskUserByWorkTypeRoleIds(String schema, String facilityId, String
////            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, int distributionOrExecute, String rolePermissionKey) {
////        String permissionKey = this.getDoTaskUserPermissionKey(businessTypeId, distributionOrExecute, rolePermissionKey);
////
////        //从工单池中获取用户，按位置和排班
////        List<User> scheduleList = getWorkScheduleByPoolAndSiteAndTimeOnDuty(schema, facilityId, poolId, permissionKey, beginDateTime, endDateTime);
////        if (scheduleList.size() == 0) {
////            //按角色从位置中获取 排班 用户
////            scheduleList = getWorkScheduleBySiteAndTimeOnDuty(schema, facilityId, permissionKey, beginDateTime, endDateTime);
////            if (scheduleList.size() == 0) {
////                //还是没有人，则按角色找
////                scheduleList = userService.findUserByFunctionStrKey(schema, permissionKey, facilityId.toString());
////            }
////            //找到的人如果没有配置，则不分配保养人员
////            if (scheduleList == null || scheduleList.isEmpty()) {
////                scheduleList = new ArrayList<>();
////            }
////        }
////
////        return scheduleList;
////    }
//
//    // LSP工单处理变更-20190529-sps start
//    //按工单类型，获取有操作权限的用户
//    //facilityNO（场地NO）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //roleIds: 角色id
//    @Override
//    public List<User> getDoTaskUserByWorkTypeRoleIds(String schema, String facilityNo, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, String roleIds, int category_id) {
//        if (null == facilityNo || facilityNo.isEmpty() || null == roleIds || roleIds.isEmpty()) {
//            return null;
//        }
//        List<String> roleIdList = new ArrayList<>();
//        String[] roleIdInfo = roleIds.split(",");
//        for (String roleId : roleIdInfo) {
//            roleIdList.add(roleId);
//        }
//        //从工单池中获取用户，按位置和排班
//        List<User> scheduleList = workScheduleMapper.getWorkScheduleUserByRoleAndPoolAndSiteAndTimeOnDuty(schema, facilityNo, poolId, roleIdList, beginDateTime, endDateTime, category_id);
//        if (scheduleList.size() == 0) {
//            //按角色从位置中获取 排班 用户
//            scheduleList = workScheduleMapper.getWorkScheduleUserByRoleAndSiteAndTimeOnDuty(schema, facilityNo, roleIdList, beginDateTime, endDateTime, category_id);
//            if (scheduleList.size() == 0) {
//                //还是没有人，则按角色找
//                scheduleList = userMapper.findUsersByRoles(schema, roleIdList, facilityNo, category_id);
//            }
//            //找到的人如果没有配置，则不分配保养人员
//            if (scheduleList == null || scheduleList.isEmpty()) {
//                scheduleList = new ArrayList<>();
//            }
//        }
//
//        return scheduleList;
//    }
//
//    //facilityNO（场地NO）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //rolePermissionKey: 固定角色权限key
//    @Override
//    public List<User> getDoTaskUserByWorkTypePermission(String schema, String facilityNo, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, int distributionOrExecute, String rolePermissionKey, int category_id) {
//        if (null == facilityNo || facilityNo.isEmpty()) {
//            return null;
//        }
//        String permissionKey = this.getDoTaskUserPermissionKey(businessTypeId, distributionOrExecute, rolePermissionKey);
//        if (null != permissionKey && !"".equals(permissionKey)) {
//            //从工单池中获取用户，按位置和排班
//            List<User> scheduleList = workScheduleMapper.getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(schema, facilityNo, poolId, permissionKey, beginDateTime, endDateTime, category_id);
//            if (scheduleList.size() == 0) {
//                //按角色从位置中获取 排班 用户
//                scheduleList = workScheduleMapper.getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(schema, facilityNo, permissionKey, beginDateTime, endDateTime, category_id);
//                if (scheduleList.size() == 0) {
//                    //还是没有人，则按角色权限key找人员
//                    scheduleList = userService.findUserByFunctionStrKey(schema, permissionKey, facilityNo, category_id);
//                }
//                //找到的人如果没有配置，则不分配保养人员
//                if (scheduleList == null || scheduleList.isEmpty()) {
//                    scheduleList = new ArrayList<>();
//                }
//            }
//            return scheduleList;
//        }
//        return null;
//    }
//
//    //按工单类型，获取有操作权限的用户（单个设备）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //roleIds: 角色id
//    @Override
//    public List<User> getDoTaskUserByAssetIdRoles(String schema, String assetId, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, String roleIds) {
//        if (null == assetId || assetId.isEmpty() || null == roleIds || roleIds.isEmpty()) {
//            return null;
//        }
//        //从工单池中获取用户，按位置和排班
//        List<User> scheduleList = workScheduleMapper.getWorkScheduleUserByRoleAndPoolAndAssetAndTimeOnDuty(schema, assetId, poolId, roleIds, beginDateTime, endDateTime);
//        if (scheduleList.size() == 0) {
//            //按角色从位置中获取 排班 用户
//            scheduleList = workScheduleMapper.getWorkScheduleUserByRoleAndAssetAndTimeOnDuty(schema, assetId, roleIds, beginDateTime, endDateTime);
//            if (scheduleList.size() == 0) {
//                //还是没有人，则按角色找
//                scheduleList = workScheduleMapper.findUsersByAssetIdAndRoles(schema, roleIds, assetId);
//            }
//            //找到的人如果没有配置，则不分配保养人员
//            if (scheduleList == null || scheduleList.isEmpty()) {
//                scheduleList = new ArrayList<>();
//            }
//        }
//
//        return scheduleList;
//    }
//
//
//    //按工单类型，获取有操作权限的用户（单个设备）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //rolePermissionKey: 固定角色权限key
//    @Override
//    public List<User> getDoTaskUserByAssetIdPermission(String schema, String assetId, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, int distributionOrExecute, String rolePermissionKey) {
//        if (null == assetId || assetId.isEmpty()) {
//            return null;
//        }
//        String permissionKey = this.getDoTaskUserPermissionKey(businessTypeId, distributionOrExecute, rolePermissionKey);
//        if (null != permissionKey && !"".equals(permissionKey)) {
//            //从工单池中获取用户，按位置和排班
//            List<User> scheduleList = workScheduleMapper.getWorkScheduleUserByPermissionKeyAndPoolAndAssetAndTimeOnDuty(schema, assetId, poolId, permissionKey, beginDateTime, endDateTime);
//            if (scheduleList.size() == 0) {
//                //按角色从位置中获取 排班 用户
//                scheduleList = workScheduleMapper.getWorkScheduleUserByPermissionKeyAndAssetAndTimeOnDuty(schema, assetId, permissionKey, beginDateTime, endDateTime);
//                if (scheduleList.size() == 0) {
//                    //还是没有人，则按角色权限key找人员
//                    scheduleList = workScheduleMapper.findUserByAssetIdAndFunctionStrKey(schema, permissionKey, assetId);
//                }
//                //找到的人如果没有配置，则不分配保养人员
//                if (scheduleList == null || scheduleList.isEmpty()) {
//                    scheduleList = new ArrayList<>();
//                }
//            }
//
//            return scheduleList;
//        }
//        return null;
//    }
//
    //找设备专门配置的维保执行人员
    @Override
    public List<Map<String, Object>> getDoExecuteUserByAssetId(String schema, String assetId, Integer workTypeId, String roleIds, Timestamp now) {
        if (null == assetId || assetId.isEmpty()) {
            return null;
        }
        List<String> roleIdList = null;
        if (RegexUtil.optNotNull(roleIds).isPresent()) {
            roleIdList = new ArrayList<>();
            String[] roleIdInfo = roleIds.split(",");
            for (String roleId : roleIdInfo) {
                roleIdList.add(roleId);
            }
        }
        //增加关联人员的排班判断，如果排班没有人，则再不按排班找关联人员
        List<Map<String, Object>> doUserList = workScheduleMapper.getDoExecuteScheduleUserByAssetId(schema, assetId, workTypeId, roleIdList, now);
        if (null == doUserList || doUserList.size() <= 0) {
            doUserList = workScheduleMapper.getDoExecuteUserByAssetId(schema, assetId, workTypeId, roleIdList);
        }
        return doUserList;
    }

    //
    //按工单类型，获取有操作权限的用户（场地字符串型）
    //distributionOrExecute: 1:执行；2：分配；
    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
    //rolePermissionKey: 固定角色权限key
    private String getDoTaskUserPermissionKey(Integer businessTypeId, int distributionOrExecute, String rolePermissionKey) {
        // 保养分配：maintain_distribute 巡检分配：inspection_distribute 点检分配：spot_distribute
        // 保养执行：maintain_result_submit 巡检执行：inspection_do 点检执行：spot_do
        String permissionKey = "";
        // 页面配置特定权限后，使用该角色权限进行过滤
        if (null == rolePermissionKey || "".equals(rolePermissionKey)) {
            switch (businessTypeId) {
                case 1:
                    permissionKey = (1 == distributionOrExecute) ? "repair_do" : "repair_distribute";
                    break;
                case 2:
                    permissionKey = (1 == distributionOrExecute) ? "maintain_result_submit" : "maintain_distribute";
                    break;
                case 3:
                    permissionKey = (1 == distributionOrExecute) ? "inspection_do" : "inspection_distribute";
                    break;
                case 4:
                    permissionKey = (1 == distributionOrExecute) ? "spot_do" : "spot_distribute";
                    break;
            }
        } else {
            permissionKey = rolePermissionKey;
        }
        return permissionKey;
    }
//
//    //将逗号隔开的角色id，转成List
//    private List<String> getRoleFromSourceSplitWord(String roleIds) {
//        List<String> roleWord = null;
//        if (null != roleIds && !"".equals(roleIds)) {
//            roleWord = new ArrayList<String>();
//            String[] pkStr = roleIds.split(",");
//            for (String pk : pkStr) {
//                roleWord.add(pk);
//            }
//        }
//        return roleWord;
//    }
//
//
//    // LSP工单处理变更-20190529-sps end
//
//
//    //派案策略变更 -zys 2020/03/25
//

    /**
     * 按工单类型，获取有操作权限的用户 派案策略变更新方法 加入设备位置为参数作为派案中间连接条件 -zys 2020/03/25
     *
     * @param schema
     * @param positionCode
     * @param poolId
     * @param businessTypeId
     * @param beginDateTime
     * @param endDateTime
     * @param roleIds
     * @param category_id
     * @return
     */
    @Override
    public List<Map<String, Object>> getExecutorByWorkTypeAndRoleIds(String schema, String positionCode, String poolId,
                                                                     Integer businessTypeId, Timestamp beginDateTime,
                                                                     Timestamp endDateTime, String roleIds, int category_id) {
        if (RegexUtil.optNotNull(positionCode).isPresent() || RegexUtil.optNotNull(roleIds).isPresent()) {
            return null;
        }
        //是否是设备位置主导
        boolean isPositionGuide = true;
        //去系统配置派案策略 设备组织还是设备位置主导
        Map<String, Object> dataList = systemConfigService.getMapSystemConfigData(schema, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
        if (null == dataList || RegexUtil.optNotNull(dataList.get("setting_value")).isPresent() || dataList.get("setting_value").equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
            isPositionGuide = false;
        }
        List<String> roleIdList = Arrays.asList(roleIds.split(","));
        List<Map<String, Object>> scheduleList;
        //从工单池中获取用户，按位置和排班
        if (isPositionGuide) {
            scheduleList = workScheduleMapper.getWorkScheduleExecutorByRoleAndPoolAndSiteAndTimeOnDuty(schema, positionCode, poolId, roleIdList, beginDateTime, endDateTime, category_id);
        } else {
            scheduleList = workScheduleMapper.getWorkScheduleUserByRoleAndPoolAndSiteAndTimeOnDuty(schema, positionCode, poolId, roleIdList, beginDateTime, endDateTime, category_id);
        }
        if (RegexUtil.optNotNull(scheduleList).isPresent()) {
            return scheduleList;
        }
        //按角色从位置中获取 排班 用户
        if (isPositionGuide) {
            scheduleList = workScheduleMapper.getWorkScheduleExecutorByRoleAndSiteAndTimeOnDuty(schema, positionCode, roleIdList, beginDateTime, endDateTime, category_id);
        } else {
            scheduleList = workScheduleMapper.getWorkScheduleUserByRoleAndSiteAndTimeOnDuty(schema, positionCode, roleIdList, beginDateTime, endDateTime, category_id);
        }
        if (RegexUtil.optNotNull(scheduleList).isPresent()) {
            return scheduleList;
        }
        //还是没有人，则按角色找
        if (isPositionGuide) {
            scheduleList = userMapper.getUsersByRoles(schema, roleIdList, positionCode, category_id);
        } else {
            scheduleList = userMapper.findUsersByRoles(schema, roleIdList, positionCode, category_id);
        }
        if (RegexUtil.optNotNull(scheduleList).isPresent()) {
            return scheduleList;
        }
        //找到的人如果没有配置，则不分配保养人员
        return new ArrayList<>();
    }

    /**
     * 根据rolePermissionKey 固定角色权限key，获取有操作权限的用户 派案策略变更新方法 加入设备位置为参数作为派案中间连接条件 -zys 2020/03/25
     *
     * @param schema
     * @param positionCode
     * @param poolId
     * @param businessTypeId
     * @param beginDateTime
     * @param endDateTime
     * @param distributionOrExecute
     * @param rolePermissionKey
     * @param category_id
     * @return
     */
    @Override
    public List<Map<String, Object>> getExecutorByWorkTypeAndPermission(String schema, String positionCode, String poolId,
                                                                        Integer businessTypeId, Timestamp beginDateTime,
                                                                        Timestamp endDateTime, int distributionOrExecute,
                                                                        String rolePermissionKey, int category_id) {
        if (!RegexUtil.optNotNull(positionCode).isPresent()) {
            return null;
        }
        String permissionKey = this.getDoTaskUserPermissionKey(businessTypeId, distributionOrExecute, rolePermissionKey);
        if (!RegexUtil.optNotNull(permissionKey).isPresent()) {
            return null;
        }
        //是否是设备位置主导
        boolean isPositionGuide = true;
        //去系统配置派案策略 设备组织还是设备位置主导
        Map<String, Object> dataList = systemConfigService.getMapSystemConfigData(schema, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
        if (null == dataList || !RegexUtil.optNotNull(dataList.get("setting_value")).isPresent() || dataList.get("setting_value").equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
            isPositionGuide = false;
        }
        List<Map<String, Object>> scheduleList;
        //从工单池中获取用户，按位置和排班
        if (isPositionGuide) {
            scheduleList = getWorkScheduleExecutorByPermissionKeyAndPoolAndSiteAndTimeOnDuty(schema, positionCode, poolId, permissionKey, beginDateTime, endDateTime, category_id);
        } else {
            scheduleList = getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(schema, positionCode, poolId, permissionKey, beginDateTime, endDateTime, category_id);
        }

        if (RegexUtil.optNotNull(scheduleList).isPresent()) {
            return scheduleList;
        }
        //按角色从位置中获取 排班 用户
        if (isPositionGuide) {
            scheduleList = getWorkScheduleExecutorByPermissionKeyAndSiteAndTimeOnDuty(schema, positionCode, permissionKey, beginDateTime, endDateTime, category_id);
        } else {
            scheduleList = getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(schema, positionCode, permissionKey, beginDateTime, endDateTime, category_id);
        }
        if (RegexUtil.optNotNull(scheduleList).isPresent()) {
            return scheduleList;
        }
        //还是没有人，则按角色权限key找人员
        if (isPositionGuide) {
            scheduleList = getUserByFunctionStrKey(schema, permissionKey, positionCode, category_id);
        } else {
            scheduleList = findUserByFunctionStrKey(schema, permissionKey, positionCode, category_id);
        }
        if (RegexUtil.optNotNull(scheduleList).isPresent()) {
            return scheduleList;
        }
        //找到的人如果没有配置，则不分配保养人员
        return new ArrayList<>();
    }

    public List<Map<String, Object>> getUserByFunctionStrKey(String schema_name, String functionKey, String positionCode, int categoryId) {
        List<Map<String, Object>> scheduleList = userMapper.getUserByFunctionStrKey(schema_name, functionKey, positionCode, categoryId);
        List<Map<String, Object>> requestResult = DataChangeUtil.scdStringToList(companyService.requestNoParam("findAllPermissionNoCondition"));
        List<Map<String, Object>> param = new ArrayList<>();
        requestResult.forEach(r -> {
            scheduleList.forEach(e -> {
                if (RegexUtil.optStrOrNull(e.get("permission_id")).equals(RegexUtil.optStrOrBlank(r.get("id")))) {
                    param.add(e);
                }
            });
        });
        return param;
    }

    public List<Map<String, Object>> findUserByFunctionStrKey(String schema_name, String functionKey, String positionCode, int categoryId) {
        List<Map<String, Object>> scheduleList = userMapper.getUserByFunctionStrKey(schema_name, functionKey, positionCode, categoryId);
        List<Map<String, Object>> requestResult = DataChangeUtil.scdStringToList(companyService.requestNoParam("findAllPermissionNoCondition"));
        List<Map<String, Object>> param = new ArrayList<>();
        requestResult.forEach(r -> {
            scheduleList.forEach(e -> {
                if (RegexUtil.optStrOrNull(e.get("permission_id")).equals(RegexUtil.optStrOrBlank(r.get("id")))) {
                    param.add(e);
                }
            });
        });
        return param;
    }

    public List<Map<String, Object>> getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(String schema_name, String facility_no, String pool_id, String functionKey, Timestamp beginDateTime, Timestamp endDateTime, int category_id) {
        List<Map<String, Object>> param = new ArrayList<>();
        List<Map<String, Object>> list = workScheduleMapper.getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(schema_name, facility_no, pool_id , beginDateTime, endDateTime, category_id);
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamOne(functionKey, "key", "findPermissiontByKey"));
        if (RegexUtil.optIsPresentList(list)) {
            list.forEach(e -> {
                result.forEach(l -> {
                    if (Objects.equals(RegexUtil.optStrOrBlank(e.get("permission_id")), RegexUtil.optStrOrBlank(l.get("id")))) {
                        param.add(e);
                    }
                });
            });
        }
        return param;
    }

    public List<Map<String, Object>> getWorkScheduleExecutorByPermissionKeyAndPoolAndSiteAndTimeOnDuty(String schema_name, String facility_no, String pool_id, String functionKey, Timestamp beginDateTime, Timestamp endDateTime, int category_id) {
        List<Map<String, Object>> param = new ArrayList<>();
        List<Map<String, Object>> list = workScheduleMapper.getWorkScheduleExecutorByPermissionKeyAndPoolAndSiteAndTimeOnDuty(schema_name, facility_no, pool_id , beginDateTime, endDateTime, category_id);
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamOne(functionKey, "key", "findPermissiontByKey"));
        if (RegexUtil.optIsPresentList(list)) {
            list.forEach(e -> {
                result.forEach(l -> {
                    if (Objects.equals(RegexUtil.optStrOrBlank(e.get("permission_id")), RegexUtil.optStrOrBlank(l.get("id")))) {
                        param.add(e);
                    }
                });
            });
        }
        return param;
    }

    public List<Map<String, Object>> getWorkScheduleExecutorByPermissionKeyAndSiteAndTimeOnDuty(String schema_name, String positionCode, String functionKey, Timestamp beginDateTime, Timestamp endDateTime, int category_id) {
        List<Map<String, Object>> param = new ArrayList<>();
        List<Map<String, Object>> list = workScheduleMapper.getWorkScheduleExecutorByPermissionKeyAndSiteAndTimeOnDuty(schema_name, positionCode , beginDateTime, endDateTime, category_id);
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamOne(functionKey, "key", "findPermissiontByKey"));
        if (RegexUtil.optIsPresentList(list)) {
            list.forEach(e -> {
                result.forEach(l -> {
                    if (Objects.equals(RegexUtil.optStrOrBlank(e.get("permission_id")), RegexUtil.optStrOrBlank(l.get("id")))) {
                        param.add(e);
                    }
                });
            });
        }
        return param;
    }

    public List<Map<String, Object>> getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(String schema_name, String positionCode, String functionKey, Timestamp beginDateTime, Timestamp endDateTime, int category_id) {
        List<Map<String, Object>> param = new ArrayList<>();
        List<Map<String, Object>> list = workScheduleMapper.getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(schema_name, positionCode , beginDateTime, endDateTime, category_id);
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamOne(functionKey, "key", "findPermissiontByKey"));
        if (RegexUtil.optIsPresentList(list)) {
            list.forEach(e -> {
                result.forEach(l -> {
                    if (Objects.equals(RegexUtil.optStrOrBlank(e.get("permission_id")), RegexUtil.optStrOrBlank(l.get("id")))) {
                        param.add(e);
                    }
                });
            });
        }
        return param;
    }
//
//    //派案策略变更 -zys 2020/03/25
}

