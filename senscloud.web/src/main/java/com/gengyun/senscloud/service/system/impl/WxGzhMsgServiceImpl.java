package com.gengyun.senscloud.service.system.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.bean.wxGzh.TemplateMessage;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.mapper.MessageMapper;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.service.MessageService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.WxGzhMsgService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import com.gengyun.senscloud.util.wx.gzh.WxGzhUtil;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信公众号消息管理
 * User: sps
 * Date: 2020/05/29
 * Time: 上午11:20
 */
@Service
public class WxGzhMsgServiceImpl implements WxGzhMsgService {

    private static final Logger logger = LoggerFactory.getLogger(WxGzhMsgServiceImpl.class);
    @Resource
    MessageService messageService;
    @Resource
    LogsService logService;
    @Resource
    UserMapper userMapper;
    @Resource
    MessageMapper messageMapper;
    @Value("${senscloud.com.url}")
    private String url;

    /**
     * 微信消息发送
     *
     * @param schemaName  数据库
     * @param senderId    发送人id
     * @param openId      openId
     * @param templateId  templateId
     * @param contentInfo 消息内容
     */
    @Override
    public void sendWxMsg(String schemaName, String senderId, String openId, String templateId, Map<String, Object> contentInfo) {
        RegexUtil.optNotBlankStrOrExp(templateId, ErrorConstant.EC_WX_GZH_008);
        WxGzhUtil.sendTemplateMessage(getAccessTokenByDb(schemaName), new TemplateMessage(openId, templateId, contentInfo).toString()); // 消息发送
    }

    /**
     * 更新微信Token（定时任务）
     *
     * @param schemaName 数据库
     */
    @Override
    public synchronized void checkWxAccessToken(String schemaName) {
        logger.info("wxGzhAppTokenCheck"); // reserve1  accessToken
        Map<String, String> wxTokenConfig = RegexUtil.optMapStrOrExp(messageService.getWxTokenConfig(schemaName, Constants.USUAL_CONFIG_ID_WX_GZH_TOKEN), ErrorConstant.EC_WX_GZH_011);
        String secret = RegexUtil.optNotBlankStrOrExp(wxTokenConfig.get("reserve5"), ErrorConstant.EC_WX_GZH_013);
        String type = RegexUtil.optNotBlankStrOrExp(wxTokenConfig.get("reserve6"), ErrorConstant.EC_WX_GZH_012);
        // 只能设置一家企业刷新token，其它企业都是仅get，防止重复获取token导致失效
        if (!type.startsWith("get")) {
            if (!"1".equals(type)) {
                type = type.concat(secret);
                String rightStr = RegexUtil.optMapStrOrExp(messageService.getGlobalInfo(type), ErrorConstant.EC_WX_GZH_014).get("reserve2");
                RegexUtil.trueExp(!RegexUtil.optIsPresentStr(rightStr) || !rightStr.equals(schemaName), "wxGzhAppTokenCheck-globalRightRestrict");
            }
            String timeInterval = wxTokenConfig.get("time_interval"); // token间隔
            RegexUtil.trueExp(RegexUtil.optIsPresentStr(timeInterval) && Integer.valueOf(timeInterval) < 0, "wxGzhAppTokenCheck-ok"); // 校验是否过期

            logger.info("wxGzhAppTokenCheck-overtime");
            String appId = RegexUtil.optNotBlankStrOrExp(wxTokenConfig.get("reserve4"), ErrorConstant.EC_WX_GZH_015);
            try {
                String appToken = WxGzhUtil.getAccessToken(appId, secret);
                int rst = messageService.modifyWxToken(schemaName, appToken, Constants.USUAL_CONFIG_ID_WX_GZH_TOKEN);
                if (rst > 0) {
                    logger.info("wxGzhAppTokenCheck-update：o:" + appToken);
                } else {
                    logService.newErrorLog(schemaName, ErrorConstant.EC_WX_GZH_018, Constants.SYSTEM_USER, appToken);
                }
                if (!"1".equals(type)) {
                    rst = messageService.modifyGlobalWxToken(appToken, type);
                    if (rst > 0) {
                        logger.info("wxGzhAppTokenCheck-goUpdate：go:" + appToken);
                    } else {
                        logService.newErrorLog(schemaName, ErrorConstant.EC_WX_GZH_019, Constants.SYSTEM_USER, appToken);
                    }
                }
            } catch (Exception e) {
                logService.newErrorLog(schemaName, ErrorConstant.EC_WX_GZH_020, Constants.SYSTEM_USER, e.getMessage());
            }
        }
    }

    /**
     * 获取微信Token
     *
     * @param schemaName 数据库
     * @return 微信Token
     */
    @Override
    public String getAccessTokenByDb(String schemaName) {
        Map<String, String> wxTokenConfig = RegexUtil.optMapStrOrExp(messageService.getWxTokenConfig(schemaName, Constants.USUAL_CONFIG_ID_WX_GZH_TOKEN), ErrorConstant.EC_WX_GZH_003);
        String secret = RegexUtil.optNotBlankStrOrExp(wxTokenConfig.get("reserve5"), ErrorConstant.EC_WX_GZH_004);
        String type = RegexUtil.optNotBlankStrOrExp(wxTokenConfig.get("reserve6"), ErrorConstant.EC_WX_GZH_005); // 只能设置一家企业刷新token，其它企业都是仅get，防止重复获取token导致失效
        String appToken = RegexUtil.optNotBlankStrOrExp(RegexUtil.optNotBlankStrOpt(type).filter(t -> "1".equals(t) || !t.startsWith("get")).map(t -> wxTokenConfig.get("reserve1")).orElseGet(() ->
                RegexUtil.optMapStrOrExp(messageService.getGlobalInfo(type.substring(3).concat(secret)), ErrorConstant.EC_WX_GZH_006).get("reserve1")), ErrorConstant.EC_WX_GZH_009);
        logger.info("wxGzhMsg-appToken：" + appToken);
        return appToken;
    }

    /**
     * 新增关注的公众号用户
     *
     * @param schemaName 数据库
     * @param wxCode     微信号
     * @param openId     微信公众号open_id
     * @param unionId    微信union_id
     */
    @Override
    public void newWxGzhUser(String schemaName, String wxCode, String openId, String unionId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("wx_code", wxCode);
        paramMap.put("open_id", openId);
//        int cnt = userMapper.countWxGzhUser(schemaName, paramMap);
        int cnt = countWxGzhUser(paramMap);
        // 已经关注过的用户，不再新增记录
        if (cnt == 0) {
            paramMap.put("union_id", unionId);
            paramMap.put("id", SenscloudUtil.generateUUIDStr());
//            userMapper.insertWxGzhUser(schemaName, paramMap);
            insertWxGzhUser(paramMap);
        }
    }

    /**
     * 删除关注的公众号用户
     *
     * @param wxCode 微信号
     * @param openId 微信公众号open_id
     */
    @Override
    public void cutWxGzhUser(String wxCode, String openId) {
        deleteWxGzhUser(wxCode, openId);
//        userMapper.deleteWxGzhUser(wxCode, openId);
    }


    /**
     * 更新用户微信公众号openid
     *
     * @param schemaName 数据库
     * @param unionId    微信union_id
     * @param openId     微信公众号open_id
     * @return 成功数量
     */
    @Override
    public int modifyUserWxGzhOpenId(String schemaName, String unionId, String openId) {
        return userMapper.updateUserWxGzhOpenId(schemaName, unionId, openId);
    }

    /**
     * 清空用户微信公众号openid
     *
     * @param schemaName 数据库
     * @param openId     微信公众号open_id
     * @return 成功数量
     */
    @Override
    public int cutUserWxGzhOpenId(String schemaName, String openId) {
        return userMapper.deleteUserWxGzhOpenId(schemaName, openId);
    }

    /**
     * 根据条件查询全局数据
     *
     * @param weixinCode
     * @return
     */
    @Override
    public List<Map<String, Object>> queryGlobalInfoListByWeixinCode(String weixinCode) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("weixinCode", weixinCode);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("queryGlobalInfoListByWeixinCode");
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
//        return messageMapper.queryGlobalInfoListByWeixinCode(weixinCode);
    }

    public int countWxGzhUser(Map<String, Object> paramMap) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("paramMap", paramMap);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("countWxGzhUser");
        int result = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), int.class);
        return result;
    }

    public int insertWxGzhUser(Map<String, Object> paramMap) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("paramMap", paramMap);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("insertWxGzhUser");
        int result = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), int.class);
        return result;
    }

    public int deleteWxGzhUser(String wxCode, String openId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("wxCode", wxCode);
        jsonObject.put("openId", openId);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("deleteWxGzhUser");
        int result = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), int.class);
        return result;
    }
}
