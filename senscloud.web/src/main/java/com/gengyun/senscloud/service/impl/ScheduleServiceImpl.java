package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.mapper.ScheduleMapper;
import com.gengyun.senscloud.model.ScheduleData;
import com.gengyun.senscloud.service.ScheduleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Resource
    ScheduleMapper scheduleMapper;

    @Override
    public void insert(String schema_name, ScheduleData scheduleData) {
        scheduleMapper.insert(schema_name, scheduleData);
    }

    @Override
    public void update(String schema_name, ScheduleData ScheduleData) {
        scheduleMapper.update(schema_name, ScheduleData);
    }

    @Override
    public void delete(String schema_name, Integer id) {
        scheduleMapper.delete(schema_name, id);
    }

    @Override
    public List<ScheduleData> find(String schema_name, Integer id, String shedule_name, String is_use, Boolean reschedule, Boolean is_running, Integer run_channel) {
        return scheduleMapper.find(schema_name, id, shedule_name, is_use, reschedule, is_running, run_channel);
    }

    @Override
    @Transactional
    public boolean checkRunning(String schema_name, Integer id) {
        List<ScheduleData> scheduleDataList = scheduleMapper.find(schema_name, id, null,"t", null, false,1);
        if (scheduleDataList == null || scheduleDataList.size() != 1) {
            return false;
        } else {
            ScheduleData scheduleData = scheduleDataList.get(0);
            ScheduleData scheduleData1 = new ScheduleData();
            scheduleData1.setIs_running(true);
            scheduleData1.setId(scheduleData.getId());
            try {
                scheduleMapper.update(schema_name, scheduleData1);
            } catch (Exception e) {
                return false;
            }
            return true;
        }
    }
}
