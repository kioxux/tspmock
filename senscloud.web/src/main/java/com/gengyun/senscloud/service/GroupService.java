package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.GroupModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.GroupResult;
import com.gengyun.senscloud.response.PositionResult;

import java.util.List;
import java.util.Map;

/**
 * 部门管理
 */
public interface GroupService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getGroupListPermission(MethodParam methodParam);

    /**
     * 新增部门
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    void newGroup(MethodParam methodParam, GroupModel groupModel);

    /**
     * 更新部门
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    void modifyGroup(MethodParam methodParam, GroupModel groupModel);

    /**
     * 删除部门
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    void cutGroup(MethodParam methodParam, GroupModel groupModel);

    /**
     * 获取部门列表
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    List<GroupResult> getGroupList(MethodParam methodParam, GroupModel groupModel);

    /**
     * 查询部门列表下拉框
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    List<GroupResult> getGroupSelectList(MethodParam methodParam, GroupModel groupModel);

    /**
     * 获取部门详情
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    GroupResult getGroupInfo(MethodParam methodParam, GroupModel groupModel);

    /**
     * 新增岗位
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    void newPosition(MethodParam methodParam, GroupModel groupModel);

    /**
     * 更新岗位
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    void modifyPosition(MethodParam methodParam, GroupModel groupModel);

    /**
     * 删除岗位
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    void cutPosition(MethodParam methodParam, GroupModel groupModel);

    /**
     * 获取岗位详情
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    PositionResult getPositionInfo(MethodParam methodParam, GroupModel groupModel);

    /**
     * 新增岗位角色
     */
    void newPositionRole(MethodParam methodParam, GroupModel groupModel);

    /**
     * 删除岗位角色
     */
    void cutPositionRole(MethodParam methodParam, GroupModel groupModel);

    /**
     * 查询岗位角列表
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    List<Map<String, Object>> getPositionRoleList(MethodParam methodParam, GroupModel groupModel);

    /**
     * 新增岗位角色的可选角色列表
     *
     * @param methodParam 入参
     * @param groupModel  入参
     * @return 可选角色列表
     */
    List<Map<String, Object>> getAddPositionRoleList(MethodParam methodParam, GroupModel groupModel);

    /**
     * 根据用户id获取岗位列表
     *
     * @param methodParam 入参
     * @param userId      入参
     * @return 岗位列表
     */
    List<PositionResult> getByUserId(MethodParam methodParam, String userId);

    /**
     * 根据岗位id获取他的部门级联字符串
     */
    StringBuilder getGrouNameString(MethodParam methodParam, PositionResult position);

    Map<String, Map<String, Object>> getGroupListMap(String schemaName);

    Map<String, Map<String, Object>> getPositionListMap(String schemaName);
}
