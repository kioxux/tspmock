package com.gengyun.senscloud.service.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.PagePermissionMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.PermissionRoleResult;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 页面权限用接口
 */
@Service
public class PagePermissionServiceImpl implements PagePermissionService {
    @Resource
    PagePermissionMapper pagePermissionMapper;
    @Resource
    CompanyService companyService;
    @Value("${senscloud.com.url}")
    private String url;

    /**
     * 用户菜单-功能权限查询
     *
     * @param methodParam 系统参数
     */
    @Override
    public void getUserPrmListByInfo(MethodParam methodParam) {
        Object list = ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), "loginUserMenuPrm");
        if (null == list) {
            List<String> resultList = pagePermissionMapper.findUserPrmListByInfo(methodParam.getSchemaName(), methodParam.getUserId());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("company_id", methodParam.getCompanyId());
            List<PermissionRoleResult> resMap = JSON.parseArray(SenscloudUtil.requestPublicJsonByPost(url.concat("findByCompanyIdAndRoleId"), jsonObject.toString()), PermissionRoleResult.class);
            Set<String> param = new HashSet<>();
            resMap.forEach(r -> resultList.forEach(e -> RegexUtil.optEqualsOpt(e, RegexUtil.optStrOrBlank(r.getId())).map(t -> r.getKey()).ifPresent(param::add)));
            List<String> paramResult = new ArrayList<>(param);
            methodParam.setUserPermissionList(paramResult);
            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), "loginUserMenuPrm", "loginUserMenuPrm");
        }
    }

    /**
     * 用户权限验证
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     */
    @Override
    public void applyCheckUserPrm(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optNotBlankStrOpt(methodParam.getUri()).ifPresent(huri -> {
            String uri = SensConstant.URL_PRM.containsKey(huri) ? huri : SensConstant.URL_PRM.containsKey(huri.substring(1)) ? huri.substring(1) : huri;
            RegexUtil.optNotBlankStrOpt(SensConstant.URL_PRM.get(uri)).ifPresent(k -> {
                List<String> prmList = RegexUtil.optListStrOrExp(methodParam.getUserPermissionList(), LangConstant.MSG_CA); // 权限不足！
                if (SensConstant.WORK_URL_PRM.containsKey(uri)) {
                    String[] uris = SensConstant.WORK_URL_PRM.get(uri);
                    boolean hasRight = false;
                    for (String u : uris) {
                        boolean tmpRgt = true;
                        try {
                            u = RegexUtil.optNotBlankStrOrExp(SensConstant.URL_PRM.get(u), LangConstant.MSG_CA);
                            List<String> pks = RegexUtil.optListStrOrExp(findParentPrmListById(u), LangConstant.MSG_CA); // 权限不足！
                            for (String key : pks) {
                                if (!prmList.contains(key)) {
                                    throw new Exception(key); // 权限不足！
                                }
                            }
                        } catch (Exception e) {
                            tmpRgt = false;
                        }
                        if (tmpRgt) {
                            hasRight = true;
                            break;
                        }
                    }
                    RegexUtil.falseExp(hasRight, LangConstant.MSG_CA); // 权限不足！
                } else {
                    applyCheckFunPrmByKey(methodParam, k);
                }
            }); // 功能权限验证
            applyCheckDataPrm(methodParam, uri, paramMap);
        });
    }

    /**
     * 根据权限类型验证功能权限
     *
     * @param methodParam 系统参数
     * @param prmKey      请求地址
     */
    @Override
    public void applyCheckFunPrmByKey(MethodParam methodParam, String prmKey) {
        RegexUtil.optNotNullListStr(findParentPrmListById(prmKey)).ifPresent(l -> {
            List<String> prmList = RegexUtil.optListStrOrExp(methodParam.getUserPermissionList(), LangConstant.MSG_CA); // 权限不足！
            for (String key : l) {
                RegexUtil.falseExp(prmList.contains(key), LangConstant.MSG_CA); // 权限不足！
            }
        });
    }

    /**
     * 根据权限类型验证功能权限（无异常，返回结果）
     *
     * @param methodParam 系统参数
     * @param prmKey      请求地址
     */
    @Override
    public boolean applyCheckFunPrmByKeyNoExp(MethodParam methodParam, String prmKey) {
        return RegexUtil.optNotNullListStr(findParentPrmListById(prmKey))
                .map(pks -> RegexUtil.optNotNullListStr(methodParam.getUserPermissionList()).map(prmList -> {
                    boolean isAuth = true;
                    for (String key : pks) {
                        if (!prmList.contains(key)) {
                            isAuth = false;
                            break;
                        }
                    }
                    return isAuth;
                }).orElse(false)).orElse(false);
    }

    /**
     * 根据权限类型获取并验证模块权限
     *
     * @param methodParam 系统参数
     * @param prmKey      请求地址
     */
    @Override
    public Map<String, Map<String, Boolean>> getModelPrmByKey(MethodParam methodParam, String prmKey) {
        applyCheckFunPrmByKey(methodParam, prmKey);
        List<String> sks = RegexUtil.optListStrOrExp(findSubPrmListById(prmKey), LangConstant.MSG_CA); // 权限不足！
        Map<String, Map<String, Boolean>> prmInfo = new HashMap<>();
        Optional<List<String>> lo = RegexUtil.optNotNullListStr(methodParam.getUserPermissionList());
        for (String key : sks) {
            lo.filter(l -> l.contains(key)).ifPresent(l -> prmInfo.put(key, PermissionUtil.getModelPermission(methodParam, key)));
        }
        return prmInfo;
    }

    /**
     * 根据权限类型获取并验证模块权限
     *
     * @param methodParam 系统参数
     * @param prmKeys     请求地址
     */
    @Override
    public Map<String, Map<String, Boolean>> getModelPrmByKeyArray(MethodParam methodParam, String... prmKeys) {
        Map<String, Map<String, Boolean>> prmInfo = new HashMap<>();
        for (String prmKey : prmKeys) {
            try {
                prmInfo.putAll(getModelPrmByKey(methodParam, prmKey));//把通过权限验证的添加到map权限组里，没通过验证的跳过
            } catch (Exception e) {
            }
        }
        return prmInfo;
    }

    /**
     * 数据权限验证
     *
     * @param methodParam 系统参数
     * @param uri         请求地址
     * @param paramMap    请求参数
     */
    private void applyCheckDataPrm(MethodParam methodParam, String uri, Map<String, Object> paramMap) {
        String oldUri = uri;
        uri = "," + uri + ",";
        String assetUris = ",/deleteSelectAsset,/showAssetQRCode,/exportAssetQRCode,/addAssetParent,/getAssetParentList,/removeAssetParent,/getExportAssetCode,/editAsset,/getAssetInfo,/getAssetWorkInfo,/getAssetHistoryWork,/getAssetFileList,/getAssetLogList,/getAssetRepairInfo,/removeAssetFile,/addAssetFile,";
        String assetCodeUris = ",/searchAssetIdByCode,";
        String assetPositionCodeUris = ",/showAssetPositionQRCode,/exportAssetPositionQRCode,";
        String bomUris = ",/deleteSelectBom,/showBomQRCode,/exportBomQRCode,";
//        String bomUris = ",/removeSelectBom,/searchBomInfo,/searchBomStockInfo,/searchBomInAndOutList,/editBomSupplier,/editUseBomSupplier,/searchBomMaterialQRCode,/editBom,/editUseBom,";
        String fileUris = ",/exportFile,/getImg,/addFile,";
        String customerUris =",/getCustomerLogList,";
        String total = assetPositionCodeUris + assetCodeUris + assetUris + bomUris + fileUris + customerUris;
        if (total.contains(uri)) {
            String ids = RegexUtil.optStrAndValOrNull(paramMap.get("id"), paramMap.get("ids")); // 数据主键
            String businessNo = "";
            if (fileUris.contains(uri)) {
                businessNo = RegexUtil.optStrOrBlank(paramMap.get("business_no"));
                ids = RegexUtil.optStrOrNull(paramMap.get("data_id"));
            } else if (assetCodeUris.contains(uri)) {
                ids = RegexUtil.optStrOrNull(paramMap.get("codeSearch"));
            }
            if (SensConstant.BUSINESS_NO_AND_MENU_NO_DATA.containsKey(businessNo)) {
                applyCheckFunPrmByKey(methodParam, SensConstant.BUSINESS_NO_AND_MENU_NO_DATA.get(businessNo));
            } else if (",/getImg,".equals(uri) && RegexUtil.optBool(paramMap.get("is_logo"))) {
                RegexUtil.optListStrOrErr(methodParam.getUserPermissionList(), methodParam, ErrorConstant.EC_PRIMARY_INVALID_3);
            } else {
                String[] dataIds = applyCheckIds(ids, methodParam, oldUri); // 数据主键
                int len = dataIds.length;
                int cnt = 0;
                if (assetUris.contains(uri) || SensConstant.BUSINESS_NO_2000.equals(businessNo)) {
                    cnt = pagePermissionMapper.dealCheckAssetDataPrm(methodParam.getSchemaName(), dataIds, methodParam.getUserId());
                } else if (assetCodeUris.contains(uri)) {
                    //传入的是设备id
                    cnt = pagePermissionMapper.dealCheckAssetDataPrmByCode(methodParam.getSchemaName(), dataIds, methodParam.getUserId());
                } else if (bomUris.contains(uri)) {
//                    cnt = pagePermissionMapper.dealCheckAssetDataPrm(methodParam.getSchemaName(), dataIds, methodParam.getUserId());
                    cnt = pagePermissionMapper.dealCheckBomDataPrmByCode(methodParam.getSchemaName(), dataIds, methodParam.getUserId());
                    ;
                } else if (assetPositionCodeUris.contains(uri)) {
                    cnt = len;
                } else if (customerUris.contains(uri)) {
                    cnt = len;
                }
                if (1 > len || cnt != len) {
                    if ("".equals(businessNo)) {
                        throw new SenscloudException(LangConstant.MSG_CA); // 权限不足！
                    } else {
                        throw new SenscloudError(methodParam, ErrorConstant.EC_PRIMARY_INVALID_2, "url:" + uri + "ids:" + ids, LangConstant.MSG_BK);
                    }
                }
                methodParam.setDataIdArray(dataIds);
                methodParam.setBatchDeal(dataIds.length > 1);
                methodParam.setIds(DataChangeUtil.joinByStrArrayWithoutCheck(dataIds));
                methodParam.setDataId(dataIds[0]);
            }
        }
    }

    /**
     * 校验主键是否有效
     *
     * @param ids 主键
     * @return 主键
     */
    private String[] applyCheckIds(String ids, MethodParam methodParam, String uri) {
        return RegexUtil.optNotBlankStrOpt(ids).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudError(methodParam, ErrorConstant.EC_PRIMARY_INVALID_1, "url:" + uri + "ids:" + ids, LangConstant.MSG_BI)); // 失败：缺少必要条件！
    }

    /**
     * 获取有安群库存短信接收权限的人
     *
     * @param schemaName 系统参数
     */
    @Override
    public List<Map<String, Object>> getBomSmsPermissionUserList(String schemaName, int stockId) {
        return findBomSmsPermissionUserList(schemaName, stockId);
    }

    public List<Map<String, Object>> findBomSmsPermissionUserList(String schemaName, int stockId) {
        List<Map<String, Object>> bomSmsPermissionUserList = pagePermissionMapper.findBomSmsPermissionUserList(schemaName, stockId);
        List<Map<String, Object>> requestResult = DataChangeUtil.scdStringToList(companyService.requestNoParam("findAllPermissionNoCondition"));
        List<Map<String, Object>> param = new ArrayList<>();
        requestResult.forEach(r -> {
            bomSmsPermissionUserList.forEach(e -> {
                if (RegexUtil.optStrOrNull(e.get("permission_id")).equals(RegexUtil.optStrOrBlank(r.get("id"))) && "bom_limit_sms".equals(RegexUtil.optStrOrBlank(r.get("key")))) {
                    param.add(e);
                }
            });
        });
        return param;
    }

    public List<String> findParentPrmListById(String key) {
        List<String> result = JSON.parseArray(companyService.requestParamOne(key, "key", "findParentPrmListById"), String.class);
        return result;
    }

    public List<String> findSubPrmListById(String key) {
        List<String> result = JSON.parseArray(companyService.requestParamOne(key, "key", "findSubPrmListById"), String.class);
        return result;
    }
}
