package com.gengyun.senscloud.service.language;

import com.gengyun.senscloud.model.LangSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import io.swagger.annotations.Api;

import java.util.Map;

@Api(tags = "多语言")
public interface MultipleLanguagesService {
    /**
     * 获取菜单信息
     * methodParam 系统参数
     *
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAssetListPermission(MethodParam methodParam);

    /**
     * 获取多语言列表
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @return 列表数据
     */
    Map<String, Object> getLanguagesListForPage(MethodParam methodParam, LangSearchParam lgParam);

    /**
     * 获取多语言列表（企业）
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @return 列表数据
     */
    Map<String, Object> getLangListForCompanyForPage(MethodParam methodParam, LangSearchParam lgParam);

    /**
     * 新增多语言
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @param paramMap    页面参数
     * @param isSelf      是否是单企业编辑
     */
    void newLanguage(MethodParam methodParam, LangSearchParam lgParam, Map<String, Object> paramMap, boolean isSelf);

    /**
     * 编辑多语言
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @param paramMap    页面参数
     * @param isSelf      是否是单企业编辑
     */
    void editLanguages(MethodParam methodParam, LangSearchParam lgParam, Map<String, Object> paramMap, boolean isSelf);

    /**
     * 发布多语言
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     */
    void releaseLang(MethodParam methodParam, LangSearchParam lgParam);

    /**
     * 发布多语言（企业）
     *
     * @param methodParam 系统参数
     */
    void releaseLangByCompany(MethodParam methodParam);
}
