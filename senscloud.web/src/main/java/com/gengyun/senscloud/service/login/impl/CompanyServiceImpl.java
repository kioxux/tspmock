package com.gengyun.senscloud.service.login.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.entity.CompanyEntity;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.login.HandleCacheService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 企业信息处理
 */
@CacheConfig(cacheNames = "ehcCompanyInfo")
@Service
public class CompanyServiceImpl implements CompanyService {
    @Resource
    HandleCacheService handleCacheService;
    @Value("${senscloud.com.url}")
    private String url;
    @Value("${senscloud.com.license}")
    private String license;

    /**
     * 通过企业编号获得企业信息
     *
     * @param id 企业编号
     * @return 企业信息
     */
    @Override
    public CompanyEntity getCompanyById(long id) {
        CompanyEntity companyEntity = JSON.parseObject(handleCacheService.requestCompany(id, "companyId", "findCompanyById"), CompanyEntity.class);
        //return companyMapper.findCompanyById(id);
        return companyEntity;
    }

    /**
     * 根据登录信息获取企业信息
     *
     * @param user 登录信息
     * @return 企业信息
     */
    //    @Cacheable(key = "'user_cpm_list_' + #user", unless = "#result.size() == 0")
    @Override
    @Cacheable(key = "'ehc_user_cpm_list' + #user + #license")
    public List<Map<String, Object>> getCompanyListByUser(String user) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", user);
        jsonObject.put("license", license);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findCompanyListByUserAndLicense");
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
    }

    /**
     * 根据用户删除公司
     *
     * @param user
     */
    @Override
    @CacheEvict(key = "'ehc_user_cpm_list' + #user + #license")
    public void deleteCompanyListByUserAndLicense(String user) {
    }

    /**
     * 根据用户删除公司
     *
     * @param user
     */
    @Override
    @CacheEvict(key = "'ehc_user_cpm_list' + #user")
    public void deleteCompanyListByUser(String user) {
    }

    /**
     * 将用户信息同步到公共表中
     *
     * @param param 参数
     */
    @Override
    @CacheEvict(key = "'ehc_user_cpm_list' + #param.get('account') + #license")
    public void newUserTenantRelation(Map<String, Object> param) {
        param.put("license", license);
        JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(param), JSONObject.class);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("insertUserTenantRelation");
        SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
//        companyMapper.insertUserTenantRelation(param);
    }

    /**
     * 根据手机号获取企业信息（后台用）
     *
     * @param phone 手机号
     * @return 企业信息
     */
    @Override
    @Cacheable(key = "'phone_cpm_list_' + #phone")
    public List<Map<String, Object>> getCompanyListByPhone(String phone) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findCompanyListByPhone");
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
//        return companyMapper.findCompanyListByPhone(phone);
    }

    /**
     * 根据手机号获取企业信息（返回客户端）
     *
     * @param phone 手机号
     * @return 企业信息
     */
    @Override
    @Cacheable(key = "'page_phone_cpm_list_' + #phone")
    public List<Map<String, Object>> getCompanyListForPageByPhone(String phone) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findCompanyListForPageByPhone");
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
//        return companyMapper.findCompanyListForPageByPhone(phone);
    }

    /**
     * 根据登录用户信息查询企业数量
     *
     * @param account   账号
     * @param companyId 企业编号
     * @return 企业数量
     */
    @Override
    @Cacheable(key = "'user_cpm_cnt_' + #companyId + '_ac_' + #account")
    public int cntCompanyListByUser(String account, long companyId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("account", account);
        jsonObject.put("companyId", companyId);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("countCompanyListForLogin");
        int result = Integer.valueOf(RegexUtil.optStrOrBlank(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString)).trim());
        return result;
        //return companyMapper.countCompanyListForLogin(account, companyId);
    }

    @Override
    @Cacheable(key = "'request_lang_public' + #companyId + '_key_' + #key + '_path_' + #path")
    public String requestLang(Long companyId, String key, String path) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(key, companyId);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

    @Override
    @Cacheable(key = "'request_param_one_public' + '_condition_' + #condition + '_key_' + #key + '_path_' + #path")
    public String requestParamOne(String condition, String key, String path) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(key, condition);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

    @Override
    @Cacheable(key = "'request_param_two_public' + #companyId + '_condition_' + #condition + '_key1_' + #key1 + '_key2_' + #key2 + '_path_' + #path")
    public String requestParamTwo(Long companyId, String condition, String key1, String key2, String path) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(key1, companyId);
        jsonObject.put(key2, condition);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

    @Override
    @Cacheable(key = "'request_no_param_public' + '_path_' + #path")
    public String requestNoParam(String path) {
        JSONObject jsonObject = new JSONObject();
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

//    /**
//     * 获取全部有效企业列表
//     *
//     * @return 企业列表
//     */
//    @Override
//    public List<Map<String, Object>> findAll() {
//        return companyMapper.findAll();
//    }
}
