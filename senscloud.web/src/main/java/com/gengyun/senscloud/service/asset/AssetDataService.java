package com.gengyun.senscloud.service.asset;

import com.gengyun.senscloud.model.AssetSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import io.swagger.annotations.Api;

import java.util.List;
import java.util.Map;

@Api(tags = "设备管理")
public interface AssetDataService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAssetListPermission(MethodParam methodParam);

    /**
     * 获取设备列表
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    Map<String, Object> getAssetListForPage(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 根据主键批量删除设备（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    void deleteAssetByIds(MethodParam methodParam);

    /**
     * 根据主键展示设备二维码（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    void doShowAssetQRCode(MethodParam methodParam);

    /**
     * 根据选中主键下载设备二维码
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    void doExportAssetQRCode(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获取设备导入模板文件码（文件码）
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    String getImportAssetCode(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 根据选中主键下载设备信息（文件码）
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    String getExportAssetCode(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 添加设备
     *
     * @param methodParam 系统参数
     * @param data        数据信息
     */
    void doAddAsset(MethodParam methodParam, Map<String, Object> data);

    /**
     * 编辑设备
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param data        数据信息
     */
    void doEditAsset(MethodParam methodParam, AssetSearchParam asParam, Map<String, Object> data);

    /**
     * 设备地图打点
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param data        数据信息
     */
    void editAssetMap(MethodParam methodParam, AssetSearchParam asParam, Map<String, Object> data);

    /**
     * 获取设备明细信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 设备信息
     */
    Map<String, Object> getAssetDetailById(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 根据设备编码获取设备明细信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 设备信息
     */
    Map<String, Object> getAssetDetailByCode(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 根据设备编码获取设备主键
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 设备信息
     */
    String getAssetIdByCode(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获得设备故障履历列表
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    List<Map<String, Object>> getFaultRecordList(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获取设备下一步工作信息
     *
     * @param methodParam 系统参数
     * @return 列表数据
     */
    List<Map<String, Object>> getAssetWorkInfo(MethodParam methodParam);

    /**
     * 获取设备历史工作信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    List<Map<String, Object>> getAssetHistoryWorkInfo(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获取设备附件文档信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    List<Map<String, Object>> getAssetFileList(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 设备附件处理
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param isAdd       是否新增
     */
    void doModifyAssetFile(MethodParam methodParam, AssetSearchParam asParam, boolean isAdd);

    /**
     * 根据设备id获取设备详情
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 设备详情
     */
    Map<String, Object> getAssetInfo(MethodParam methodParam, String id);

    /**
     * 批量修改设备信息
     *
     * @param methodParam
     * @param assetList
     */
    void updateAssetBatch(MethodParam methodParam, List<Map<String, Object>> assetList);

    /**
     * 设备厂商新增
     *
     * @param methodParam
     * @param asParam
     * @param b
     */
    void doModifyAssetFacilities(MethodParam methodParam, AssetSearchParam asParam, boolean b);

    /**
     * 获得设备厂商列表
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    List<Map<String, Object>> getAssetFacilityList(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 设备厂商删除
     *
     * @param methodParam
     * @param asParam
     */
    void cutAssetFacility(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获得设备关联人员列表
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    List<Map<String, Object>> getAssetDutyManList(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 设备关联人员新增
     *
     * @param methodParam
     * @param asParam
     */
    void newAssetDutyMan(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 设备关联人员删除
     *
     * @param methodParam
     * @param asParam
     */
    void cutAssetDutyMan(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获得设备的备件列表
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    List<Map<String, Object>> getAssetBomList(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 设备关联备件新增
     *
     * @param methodParam
     * @param asParam
     */
    void newAssetBom(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 设备关联备件删除
     *
     * @param methodParam
     * @param asParam
     */
    void removeAssetBom(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 获得设备的备件使用记录列表
     *
     * @param methodParam
     * @param bom_id
     * @return
     */
    Map<String, Object> getBomUsageRecordList(MethodParam methodParam, String bom_id);

    /**
     * 获得备件导入记录列表
     *
     * @param methodParam
     * @param bom_id
     * @return
     */
    Map<String, Object> getBomImportRecordList(MethodParam methodParam, String bom_id);

    /**
     * 批量新增设备组织
     *
     * @param methodParam
     * @param addAssetList
     */
    void addAssetOrgBatch(MethodParam methodParam, List<Map<String, Object>> addAssetList);

    /**
     * 获取运行的设备运行状态的id
     *
     * @param schemaName
     * @return
     */
    String searchAssetRunningStatusForRun(String schemaName);

    /**
     * 根据设备id或者编码查询设备信息
     *
     * @param methodParam
     * @param id_or_code
     * @return
     */
    Map<String, Object> getAssetByIdOrCode(MethodParam methodParam, String id_or_code);

    /**
     * 获取指定设备类型
     * @param methodParam
     * @param asParam
     * @return
     */
    String getAssetTypeIds(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 子设备新增
     *
     * @param methodParam 系统变量
     * @param asParam  请求参数
     */
    void newAssetParent(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 子设备删除
     *
     * @param methodParam 系统变量
     * @param asParam   请求参数
     */
    void removeAssetParent(MethodParam methodParam, AssetSearchParam asParam);

    /**
     * 查询子设备信息列表
     *
     * @param methodParam 系统变量
     * @param asParam   请求参数
     * @return 子设备信息列表
     */
    List<Map<String, Object>> getAssetParentList(MethodParam methodParam, AssetSearchParam asParam);
}