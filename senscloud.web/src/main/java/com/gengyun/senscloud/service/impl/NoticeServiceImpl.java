package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.NoticeService;
import org.springframework.stereotype.Service;

@Service
public class NoticeServiceImpl implements NoticeService {
//    @Autowired
//    NoticeMapper noticeMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Override
//    public JSONObject queryForApp(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName, String sortOrder) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        if (StringUtils.isNotEmpty(sortName)) {
//            orderBy = sortName + " " + sortOrder;
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = noticeMapper.queryForApp(schemaName, orderBy, pageSize, begin, searchKey);
//        int total = noticeMapper.countByConditionForApp(schemaName, searchKey);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    /**
//     * 根据ID查询公告详情
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel getNoticeDetailById(String schemaName, HttpServletRequest request) {
//        Integer id = Integer.valueOf((String) request.getParameter("id"));
//        return ResponseModel.ok(noticeMapper.findById(schemaName, id));
//    }
//
//    @Override
//    public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName, String sortOrder, String beginTime, String endTime) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        if (StringUtils.isNotEmpty(sortName)) {
//            orderBy = sortName + " " + sortOrder;
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = noticeMapper.query(schemaName, orderBy, pageSize, begin, searchKey, beginTime, endTime);
//        int total = noticeMapper.countByCondition(schemaName, searchKey, beginTime, endTime);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public Map<String, Object> queryNoticeById(String schemaName, int id) {
//        return noticeMapper.queryNoticeById(schemaName, id);
//    }
//
//    @Override
//    public ResponseModel addNotice(String schemaName, Map<String, Object> paramMap) {
//        int count = noticeMapper.insert(schemaName, paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel updateNotice(String schemaName, int id, boolean is_use) {
//        if (is_use) {
//            noticeMapper.updateUse(schemaName);
//        }
//        Map<String, Object> noticeMap = this.queryNoticeById(schemaName, id);
//        noticeMap.put("is_use", is_use);
//        int count = noticeMapper.update(schemaName, noticeMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//
//    }
//
//    @Override
//    public ResponseModel updateNotice(String schemaName, Map<String, Object> paramMap) {
//        int count = noticeMapper.update(schemaName, paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel deleteById(String schemaName, HttpServletRequest request) {
//        Integer id = Integer.valueOf((String) request.getParameter("id"));
//        int count = noticeMapper.delete(schemaName, id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
}
