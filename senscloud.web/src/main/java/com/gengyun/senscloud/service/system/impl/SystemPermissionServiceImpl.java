package com.gengyun.senscloud.service.system.impl;


import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.mapper.SystemPermissionMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SystemPermissionResult;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.SystemPermissionService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SystemPermissionServiceImpl implements SystemPermissionService {
    @Resource
    SystemPermissionMapper systemPermissionMapper;
    @Resource
    CompanyService companyService;

    /**
     * 根据用户id获取当前用户的菜单列表
     *
     * @param methodParam 入参
     * @return 菜单列表
     */
    @Override
    public List<SystemPermissionResult> getByUserId(MethodParam methodParam) {
        return handleSystemPermissionResult(methodParam);
    }

    public List<SystemPermissionResult> handleSystemPermissionResult(MethodParam methodParam) {
        List<SystemPermissionResult> result = new ArrayList<>();
        List<Map<String, Object>> systemPermissionResult = findSystemPermissionResult();
        List<String> companyMenu = findCompanyMenu(methodParam.getCompanyId());
        List<String> permissionId = systemPermissionMapper.findByUserId(methodParam.getSchemaName(), methodParam.getUserId());
        if (RegexUtil.optIsPresentList(permissionId)) {
            permissionId.forEach(pId -> {
                if (RegexUtil.optIsPresentList(companyMenu) && companyMenu.contains(pId)) {
                    systemPermissionResult.forEach(sp -> {
                        if (Objects.equals(RegexUtil.optStrOrBlank(sp.get("id")), pId)) {
                            result.add(JSON.parseObject(JSON.toJSONString(sp),SystemPermissionResult.class));
                        }
                    });
                }
            });
        }
        return result.stream().sorted(new Comparator<SystemPermissionResult>() {
            @Override
            public int compare(SystemPermissionResult o1, SystemPermissionResult o2) {
                return o1.getIndex() - o2.getIndex();
            }
        }).collect(Collectors.toList());
    }

    public List<Map<String, Object>> findSystemPermissionResult() {
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestNoParam("findSystemPermissionResult"));
        return result;
    }

    public List<String> findCompanyMenu(long companyId) {
        List<String> result = JSON.parseArray(companyService.requestLang(companyId, "companyId", "findCompanyMenu"), String.class);
        return result;
    }
}
