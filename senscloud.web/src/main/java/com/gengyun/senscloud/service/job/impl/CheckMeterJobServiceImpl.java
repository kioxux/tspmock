package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.mapper.PolluteFeeMapper;
import com.gengyun.senscloud.service.job.CheckMeterJobService;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class CheckMeterJobServiceImpl implements CheckMeterJobService {
    private static final Logger logger = LoggerFactory.getLogger(CheckMeterJobServiceImpl.class);

    @Resource
    PolluteFeeMapper polluteFeeMapper;
    @Resource
    AutoCheckMeterImpl autoCheckMeterImpl;

    @Override
    @Retryable(value = Exception.class, maxAttempts = 1, backoff = @Backoff(delay = 1000L, multiplier = 0))
    public void cronJobToGenerateCheckMeter(String schemaName) {
        autoCheckMeterImpl.autoCheckMeter(schemaName);
    }

    @Recover
    public void recover(String schemaName) {
        List<Map<String, Object>> checkMeterSeting = RegexUtil.optNotNullListOrNew(polluteFeeMapper.polluteFeeMapper(schemaName, "自动抄表配置"));
        Map<String, Object> checkMeterSetingInfo = autoCheckMeterImpl.toMapByList(checkMeterSeting, new HashMap<String, Object>(){{
            put("try_retry_times",1);
        }});
        int try_retry_times = NumberUtils.toInt(RegexUtil.optStrOrVal(checkMeterSetingInfo.get("try_retry_times"), "0"));
        try_retry_times--;
        while (try_retry_times >0) {
            try {
                autoCheckMeterImpl.autoCheckMeter(schemaName);
                try_retry_times = 0;
            } catch (Exception e) {
                logger.error("自动抄表异常信息:{}",e);
                try_retry_times--;
            }
        }
        logger.info("cronJobToGenerateCheckMeter------重试{}次后依然失败",NumberUtils.toInt(RegexUtil.optStrOrVal(checkMeterSetingInfo.get("try_retry_times"), "0")));
    }
}
