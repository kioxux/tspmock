package com.gengyun.senscloud.service.job;


public interface MaintainAsyncJobService {

    /**
     * 根据新增、修改的维保计划生成相应的行事历 —— 异步执行
     * @param schemaName
     */
    void asyncCronJobToGeneratePlanWorkWithNewModel(String schemaName);
}
