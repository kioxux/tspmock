package com.gengyun.senscloud.service.login.impl;


import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.entity.CompanyEntity;
import com.gengyun.senscloud.entity.DomainPortUrlEntity;
import com.gengyun.senscloud.entity.LoginLogEntity;
import com.gengyun.senscloud.entity.UserEntity;
import com.gengyun.senscloud.mapper.CommonUtilMapper;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.model.LoginModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.OtherCfgModel;
import com.gengyun.senscloud.model.SystemPermissionResult;
import com.gengyun.senscloud.service.MessageService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.login.ClientTokenService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.login.LoginService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.service.system.SystemPermissionService;
import com.gengyun.senscloud.service.system.WxMsgService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

/**
 * 登录用
 */
@Service
public class LoginServiceImpl implements LoginService {
    private Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
    @Value("${senscloud.com.url}")
    private String url;
    @Resource
    CompanyService companyService;
    @Resource
    UserService userService;
    @Resource
    ClientTokenService clientTokenService;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    SystemPermissionService systemPermissionService;
    @Resource
    MessageService messageService;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    CommonUtilMapper commonUtilMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    WxMsgService wxMsgService;

    /**
     * 根据域名获取登录页信息
     *
     * @param methodParam 系统参数
     * @return 登录用户信息
     */
    @Override
    public Map<String, Object> getLoginData(MethodParam methodParam) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("domainName", RegexUtil.optNotBlankStrOrExp(methodParam.getLoginDomain(), LangConstant.MSG_CB));
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("getDomainEntityByName");
        DomainPortUrlEntity domain = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), DomainPortUrlEntity.class);
        //DomainPortUrlEntity domain = commonUtilMapper.getDomainEntityByName(RegexUtil.optNotBlankStrOrExp(methodParam.getLoginDomain(), LangConstant.MSG_CB));
        RegexUtil.optNotNullOrExp(domain, "");
        Map<String, Object> result = new HashMap<>();
        result.put("loginTitle", RegexUtil.optStrOrBlank(domain.getLogin_title())); // 登录页文字
        result.put("loginLogo", RegexUtil.optEqualsOpt(methodParam.getClientName(), SensConstant.PC_CLIENT_NAME).map(c -> domain.getBak_url8()).orElse(RegexUtil.optStrOrBlank(domain.getLogin_logo())));
        result.put("loginBackground", RegexUtil.optStrOrBlank(domain.getBak_url7())); // 登录页背景图地址
        result.put("isNeedTechSupport", domain.getIs_need_tech_support()); // 是否需要技术支持框
        result.put("loginRsvAa", domain.getBak_url6()); // 备用信息
        return result;
    }

    /**
     * 登录验证
     *
     * @param methodParam 系统参数
     * @param loginModel  页面参数
     * @return token
     */
    @Override
    public String checkLogin(MethodParam methodParam, LoginModel loginModel) {
        String infoErr = LangConstant.MSG_BC; // 帐号或密码错误，请重试！
        String userName = RegexUtil.optNotBlankStrOrExp(AESUtils.decryptAES(RegexUtil.optStrOrExpNotNull(loginModel.getU(), LangConstant.TITLE_JQ)), infoErr); // 账号
        String password = RegexUtil.optNotBlankStrOrExp(MD5.getStrByMd5(RegexUtil.optNotBlankStrOrExp(AESUtils.decryptAES(RegexUtil.optStrOrExpNotNull(loginModel.getP(), LangConstant.TITLE_AAH_F)), infoErr)), infoErr); // 密码
        List<Map<String, Object>> companyList = companyService.getCompanyListByUser(userName);
        RegexUtil.optListOrExp(companyList, infoErr);
        String account = null;
        for (Map<String, Object> company : companyList) {
            account = RegexUtil.optNotNull(company.get("id")).map(ci -> userService.getUserAccountForLogin(SenscloudUtil.getSchemaByCompId(ci), userName, password)).orElse(null);
            if (RegexUtil.optNotNull(account).isPresent()) {
                break;
            }
        }
        RegexUtil.optNotBlankStrOrExp(account, infoErr);
        String token = SenscloudUtil.getToken(methodParam.getClientName(), account); // 生成token并保存到用户表中，并返回给前端
        ScdSesRecordUtil.setLoginSesData(token, account, password);
        return token;
    }

    /**
     * 获取企业列表
     *
     * @param methodParam 系统参数
     * @return 企业列表
     */
    @Override
    public List<Map<String, Object>> getCompanyList(MethodParam methodParam) {
        String token = methodParam.getToken();
        String account = RegexUtil.optNotBlankStrOpt(ScdSesRecordUtil.getLoginAccount(token)).orElseGet(() -> RegexUtil.optNotBlankStrOrExp(clientTokenService.getLoginAccountByToken(methodParam), LangConstant.MSG_BC)); // 帐号或密码错误，请重试！
        List<Map<String, Object>> companyList = companyService.getCompanyListByUser(account);
        return RegexUtil.optListOrExp(companyList.stream().filter(company -> RegexUtil.optNotBlankStrOpt(company.get("id"))
                .map(ci -> userService.getUserAccountForLogin(SenscloudUtil.getSchemaByCompId(ci), ScdSesRecordUtil.getLoginAccount(token), ScdSesRecordUtil.getLoginPassword(token)))
                .filter(RegexUtil::optIsPresentStr).isPresent()).collect(Collectors.toList()), LangConstant.MSG_AD); // 该账号无关联企业,请与管理员申请开通服务！
    }

    /**
     * 进入登录
     *
     * @param methodParam 系统参数
     * @param c_id        企业编号
     */
    @Override
    public void applyLogin(MethodParam methodParam, Long c_id) {
        String token = methodParam.getToken();
        RegexUtil.optNotBlankStrOpt(ScdSesRecordUtil.getLoginAccount(token)).ifPresent(account -> {
            RegexUtil.optNotBlankStrOrExp(userService.getUserAccountForLogin(SenscloudUtil.getSchemaByCompId(c_id), account, ScdSesRecordUtil.getLoginPassword(token)), LangConstant.MSG_BC); // 帐号或密码错误，请重试！
            String infoErr = LangConstant.MSG_AD; // 该账号无关联企业,请与管理员申请开通服务！
            RegexUtil.optNotBlankStrOpt(c_id).map(companyId -> {
                RegexUtil.intExp(companyService.cntCompanyListByUser(account, c_id), infoErr);
                methodParam.setCompanyId(c_id);
                methodParam.setSchemaName(SenscloudUtil.getSchemaByCompId(c_id));
                LoginLogEntity loginLog = new LoginLogEntity();
                loginLog.setAccount(account);
                loginLog.setClient_name(methodParam.getClientName());
                loginLog.setLogin_time(SenscloudUtil.getNowTime());
                loginLog.setToken(methodParam.getToken());
                HttpRequestUtils.clientInfo(loginLog);
                RegexUtil.intExp(clientTokenService.newLoginLog(methodParam, loginLog), infoErr);
                methodParam.setSystemLang(systemConfigService.getSysLang(methodParam));
                logger.info(String.format(commonUtilService.getLanguageInfoBySysLang(methodParam, LangConstant.TITLE_BAAB_Y), account)); // {d}登录系统
                ScdSesRecordUtil.setAfterLoginSesData(methodParam.getToken(), c_id);
                return c_id;
            }).orElseThrow(() -> new SenscloudException(infoErr));
        });
    }

    /**
     * 进入登录(NFC)
     *
     * @param methodParam 页面参数
     * @param c_id        企业编号
     * @param nfc_code    员工NFC卡号
     */
    @Override
    public Map<String, Object> applyNFCLogin(MethodParam methodParam, Long c_id, String nfc_code) {
        CompanyEntity company = companyService.getCompanyById(c_id);
        RegexUtil.optNotNullOrExp(company, LangConstant.MSG_F);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("domainName", RegexUtil.optNotBlankStrOrExp(methodParam.getLoginDomain(), LangConstant.MSG_F));
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("getDomainEntityByName");
        DomainPortUrlEntity domain = (DomainPortUrlEntity) DataChangeUtil.scdStringToMap(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        //DomainPortUrlEntity domain = commonUtilMapper.getDomainEntityByName(RegexUtil.optNotBlankStrOrExp(methodParam.getLoginDomain(), LangConstant.MSG_F));
        RegexUtil.optNotNullOrExp(domain, LangConstant.MSG_F);
        RegexUtil.notEqualsExp(domain.getBak_url3(), "1", LangConstant.MSG_F);
        methodParam.setCompanyId(c_id);
        methodParam.setSchemaName(SenscloudUtil.getSchemaByCompId(c_id));
        String nfcCode = RegexUtil.optNotBlankStrOrExp(AESUtils.decryptAES(RegexUtil.optStrOrExpNotNull(nfc_code, LangConstant.TITLE_BAAAB_H)), LangConstant.MSG_M); // NFC卡号
        UserEntity userEntity = userService.getUserAccountForNfcCode(methodParam.getSchemaName(), nfcCode);
        RegexUtil.optNotNullOrExp(userEntity, LangConstant.MSG_M);
        RegexUtil.optNotBlankStrOrExp(userEntity.getAccount(), LangConstant.MSG_M);
        String token = SenscloudUtil.getToken(methodParam.getClientName(), userEntity.getAccount()); // 生成token并保存到用户表中，并返回给前端
        ScdSesRecordUtil.setLoginSesData(token, userEntity.getAccount(), null);//登录时登录信息记录到系统信息中

        LoginLogEntity loginLog = new LoginLogEntity();
        loginLog.setAccount(userEntity.getAccount());
        loginLog.setClient_name(methodParam.getClientName());
        loginLog.setLogin_time(SenscloudUtil.getNowTime());
        loginLog.setToken(token);
        HttpRequestUtils.clientInfo(loginLog);
        RegexUtil.intExp(clientTokenService.newLoginLog(methodParam, loginLog), LangConstant.MSG_M);
        methodParam.setSystemLang(systemConfigService.getSysLang(methodParam));
        logger.info(String.format(commonUtilService.getLanguageInfoBySysLang(methodParam, LangConstant.TITLE_BAAB_Y), userEntity.getAccount())); // {d}登录系统
        ScdSesRecordUtil.setAfterLoginSesData(token, c_id);
        Map<String, Object> result = new HashMap<>();
        result.put("token", token);
        result.put("user_name", userEntity.getUser_name());
        result.put("position", userMapper.findGroupPositionNameString(methodParam.getSchemaName(), userEntity.getId()));
        return result;
    }

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 菜单信息
     */
    @Override
    public List<SystemPermissionResult> getMenuList(MethodParam methodParam) {
        return systemPermissionService.getByUserId(methodParam);
    }

    /**
     * 获取登录用户信息
     *
     * @param methodParam 系统参数
     * @return 登录用户信息
     */
    @Override
    public Map<String, Object> getLoginUserInfo(MethodParam methodParam) {
        Map<String, Object> result = new HashMap<>();
        result.put("userId", methodParam.getUserId());
        result.put("account", methodParam.getAccount());
        result.put("userName", methodParam.getUserName());
        result.put("timeZoneId", TimeZone.getDefault().toZoneId().getId());
        result.put("companyName", methodParam.getCompanyName());
        RegexUtil.optNotNullMap(userService.queryUserWxLoginInfoById(methodParam.getSchemaName(), methodParam.getUserId()))
                .filter(wxi -> RegexUtil.optIsPresentStr(wxi.get("mini_program_open_id"))).ifPresent(wxi -> {
            result.put("isBinding", true);
            result.put("nickName", RegexUtil.optStrOrBlank(wxi.get("nick_name")));
        });
        result.put("userGpName", methodParam.getUserGpName());
        result.put("userGpList", methodParam.getUserGpList());
        String phone = RegexUtil.optStrOrVal(methodParam.getUserPhone(), "1");
        int len = phone.length();
        String str = "";
        if (len > 7) {
            str = phone.substring(0, 3).concat("*").concat(phone.substring(len - 4));
        } else {
            str = phone.substring(0, 1).concat("*").concat(phone.substring(len - 1));
        }
        result.put("userTmpPhone", str);
        result.put("companySystemName", methodParam.getCompanySystemName());
        result.put("companyLogo", methodParam.getCompanyLogo());
        result.put("phoneChooseAlbum", systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.PHONE_CHOOSE_ALBUM));
        return result;
    }

    /**
     * 发送验证码（忘记密码）
     *
     * @param phone 手机号码
     */
    @Override
    public void sendVfCodeByPhone(String phone) {
        RegexUtil.optStrOrExpNotNull(phone, LangConstant.TITLE_AAT_F); // 手机号码不能为空
        List<Map<String, Object>> companyList = RegexUtil.optListOrExpNullInfo(companyService.getCompanyListByPhone(phone), LangConstant.TITLE_AAT_F); // 手机号码信息不存在
        String cid = "";
        for (Map<String, Object> company : companyList) {
            cid = RegexUtil.optNotBlankStrOpt(company.get("id")).filter(ci -> RegexUtil.optBool(company.get("is_open_sms"))).orElse("");
            if (RegexUtil.optNotNull(cid).isPresent()) {
                break;
            }
        }
        RegexUtil.optNotBlankStrOrExp(cid, LangConstant.MSG_CF, new String[]{LangConstant.TITLE_PH}); // 不支持短信功能
        if (!RegexUtil.optNotBlankStrOpt(cacheUtilService.getVfCode(phone)).isPresent()) {
            MethodParam methodParam = new MethodParam();
            methodParam.setCompanyId(Long.valueOf(cid));
            methodParam.setSchemaName(SenscloudUtil.getSchemaByCompId(cid));
            methodParam.setAccount(Constants.SYSTEM_USER);
            methodParam.setUserId(Constants.SYSTEM_USER);
            methodParam.setOpenSms(true);
            methodParam.setSystemLang(systemConfigService.getSysLang(methodParam));
            UserEntity user = userService.getUserByPhone(methodParam.getSchemaName(), phone);
            RegexUtil.optNotNullOrExpNullInfo(user, LangConstant.TITLE_AAT_F); // 手机号码信息不存在
            methodParam.setUserLang(RegexUtil.optStrAndValOrNull(user.getLanguage_tag(), methodParam.getSystemLang()));
            methodParam.setUserName(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BH));
            messageService.doSendSmsMsg(methodParam, new String[]{cacheUtilService.newVfCode(phone), "1"}, SensConstant.SMS_10001002, user.getId(), SensConstant.BUSINESS_NO_9000, phone);
        }
    }

    /**
     * 验证短信验证码并返回企业列表（忘记密码用）
     *
     * @param loginModel 页面参数
     * @return 企业列表
     */
    @Override
    public List<Map<String, Object>> getCompanyListByPhone(LoginModel loginModel) {
        String phone = RegexUtil.optStrOrExpNotNull(loginModel.getPhone(), LangConstant.TITLE_AAT_F); // 手机号码不能为空
        String vfCode = RegexUtil.optStrOrExpNotNull(loginModel.getVfCode(), LangConstant.TITLE_PI); // 验证码不能为空
        String localVfCode = RegexUtil.optNotBlankStrOrExp(cacheUtilService.getVfCode(phone), LangConstant.MSG_CH); // 验证码错误
        RegexUtil.falseExp(vfCode.equals(localVfCode), LangConstant.MSG_CH); // 验证码错误
        List<Map<String, Object>> companyList = RegexUtil.optListOrExpNullInfo(companyService.getCompanyListForPageByPhone(phone), LangConstant.TITLE_AAT_F); // 手机号码信息不存在
        return RegexUtil.optListOrExpNullInfo(companyList.stream().filter(cm -> RegexUtil.optIsPresentStr(cm.get("id")))
                .collect(Collectors.toList()), LangConstant.TITLE_AAT_F); // 手机号码信息不存在
    }

    /**
     * 修改密码（忘记密码用）
     *
     * @param loginModel 页面参数
     */
    @Override
    public void modifyPsdByPhone(LoginModel loginModel) {
        String schemaName = SenscloudUtil.getSchemaByCompId(RegexUtil.optNotBlankStrOrExp(loginModel.getC_id(), LangConstant.MSG_CG)); // 验证失效
        String phone = RegexUtil.optNotBlankStrOrExp(loginModel.getPhone(), LangConstant.MSG_CG); // 验证失效
        String vfCode = RegexUtil.optNotBlankStrOrExp(loginModel.getVfCode(), LangConstant.MSG_CG); // 验证失效
        String localVfCode = RegexUtil.optNotBlankStrOrExp(cacheUtilService.getVfCode(phone), LangConstant.MSG_CG); // 验证失效
        RegexUtil.falseExp(vfCode.equals(localVfCode), LangConstant.MSG_CG); // 验证失效
        String newPsd = RegexUtil.optStrOrExpNotNull(loginModel.getNewPsd(), LangConstant.TITLE_DA); // 新密码不能为空
        String tmpPsw = RegexUtil.optStrOrExpNotNull(AESUtils.decryptAES(newPsd), LangConstant.TITLE_DA); // 新密码不能为空
        String cfmPsw = RegexUtil.optStrOrExpNotNull(loginModel.getCfmPsw(), LangConstant.TITLE_AAR_A); // 确认密码不能为空
        RegexUtil.falseExp(newPsd.equals(cfmPsw), LangConstant.TITLE_DB);
        String psd = RegexUtil.optNotBlankStrOrExp(MD5.getStrByMd5(tmpPsw), LangConstant.TITLE_DA); // 新密码不能为空
        UserEntity user = userService.getUserByPhone(schemaName, phone);
        RegexUtil.optNotNullOrExpNullInfo(user, LangConstant.TITLE_AAT_F); // 手机号码信息不存在
        String id = RegexUtil.optNotBlankStrOpt(user.getId()).orElseThrow(() -> new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AAT_F})); // 手机号码信息不存在
        // 检查密码是否为强验证密码
        RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.IS_VALIDATE_STRONG_PASSWORD)).filter("1"::equals).ifPresent(s -> RegexUtil.isStrongPwd(tmpPsw));
        String account = user.getAccount();
        MethodParam methodParam = new MethodParam();
        methodParam.setSchemaName(schemaName);
        methodParam.setAccount(account);
        methodParam.setUserId(id);
        userService.modifyPassword(methodParam, id, psd);
    }

    /**
     * 发送验证码
     *
     * @param methodParam 系统参数
     */
    @Override
    public void sendVfCode(MethodParam methodParam) {
        RegexUtil.falseExp(methodParam.getOpenSms(), LangConstant.MSG_CF, new String[]{LangConstant.TITLE_PH}); // 不支持短信功能
        String phone = RegexUtil.optNotBlankStrOrExp(methodParam.getUserPhone(), LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AAT_F}); // 手机号码信息不存在
        if (!RegexUtil.optNotBlankStrOpt(cacheUtilService.getVfCode(phone)).isPresent()) {
            messageService.doSendSmsMsg(methodParam, new String[]{cacheUtilService.newVfCode(phone), "1"}, SensConstant.SMS_10001002, methodParam.getUserId(), SensConstant.BUSINESS_NO_9000, phone);
        }
    }

    /**
     * 修改密码
     *
     * @param methodParam 系统参数
     * @param loginModel  页面参数
     */
    @Override
    public void modifyPwd(MethodParam methodParam, LoginModel loginModel) {
        String phone = RegexUtil.optNotBlankStrOrExp(methodParam.getUserPhone(), LangConstant.MSG_CG); // 验证失效
        String vfCode = RegexUtil.optNotBlankStrOrExp(loginModel.getVfCode(), LangConstant.MSG_CG); // 验证失效
        String localVfCode = RegexUtil.optNotBlankStrOrExp(cacheUtilService.getVfCode(phone), LangConstant.MSG_CG); // 验证失效
        RegexUtil.falseExp(vfCode.equals(localVfCode), LangConstant.MSG_CG); // 验证失效
        String newPsd = RegexUtil.optStrOrExpNotNull(loginModel.getNewPsd(), LangConstant.TITLE_DA); // 新密码不能为空
        String tmpPsw = RegexUtil.optStrOrExpNotNull(AESUtils.decryptAES(newPsd), LangConstant.TITLE_DA); // 新密码不能为空
        String cfmPsw = RegexUtil.optStrOrExpNotNull(loginModel.getCfmPsw(), LangConstant.TITLE_AAR_A); // 确认密码不能为空
        RegexUtil.falseExp(newPsd.equals(cfmPsw), LangConstant.TITLE_DB);
        String psd = RegexUtil.optNotBlankStrOrExp(MD5.getStrByMd5(tmpPsw), LangConstant.TITLE_DA); // 新密码不能为空
        // 检查密码是否为强验证密码
        RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.IS_VALIDATE_STRONG_PASSWORD)).filter("1"::equals).ifPresent(s -> RegexUtil.isStrongPwd(tmpPsw));
        userService.modifyPassword(methodParam, methodParam.getUserId(), psd);
    }

    /**
     * 获取用户常用语言包/系统默认语言包
     *
     * @param methodParam 系统参数
     * @return 语言包
     */
    @Override
    public Map<String, Object> getDefaultLangInfo(MethodParam methodParam) {
        Map<String, Object> result = new HashMap<>();
        String lang = methodParam.getUserLang();
        result.put(lang, cacheUtilService.getDefaultLangDtl(methodParam, lang));
        return result;
    }

    /**
     * 按照旧密码修改新密码
     *
     * @param methodParam 系统参数
     * @param loginModel  页面参数
     */
    @Override
    public void modifyPwdWithoutVfCode(MethodParam methodParam, LoginModel loginModel) {
        String userId = RegexUtil.optNotBlankStrOrExp(methodParam.getUserId(), LangConstant.MSG_CG); // 验证失效
        String oldPsw = RegexUtil.optStrOrExpNotNull(loginModel.getP(), LangConstant.TITLE_AK_A); // 旧密码不能为空
        String tmpOldPsw = RegexUtil.optStrOrExpNotNull(AESUtils.decryptAES(oldPsw), LangConstant.TITLE_AK_A); // 旧密码不能为空
        String oldPsd = RegexUtil.optNotBlankStrOrExp(MD5.getStrByMd5(tmpOldPsw), LangConstant.TITLE_AK_A); // 旧密码不能为空
        String newPsd = RegexUtil.optStrOrExpNotNull(loginModel.getNewPsd(), LangConstant.TITLE_DA); // 新密码不能为空
        String tmpPsw = RegexUtil.optStrOrExpNotNull(AESUtils.decryptAES(newPsd), LangConstant.TITLE_DA); // 新密码不能为空
        String cfmPsw = RegexUtil.optStrOrExpNotNull(loginModel.getCfmPsw(), LangConstant.TITLE_AAR_A); // 确认密码不能为空
        RegexUtil.falseExp(newPsd.equals(cfmPsw), LangConstant.TITLE_DB);
        String psd = RegexUtil.optNotBlankStrOrExp(MD5.getStrByMd5(tmpPsw), LangConstant.TITLE_DA); // 新密码不能为空
        // 检查密码是否为强验证密码
        RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.IS_VALIDATE_STRONG_PASSWORD)).filter("1"::equals).ifPresent(s -> RegexUtil.isStrongPwd(tmpPsw));
        int count = userService.getUserInfoByUserIdAndPwd(methodParam, userId, oldPsd);
        RegexUtil.falseExp(count > 0, LangConstant.MSG_BC);
        userService.modifyPassword(methodParam, methodParam.getUserId(), psd);
    }

    @Override
    public Map<String, Object> getWeiXinInfo(MethodParam methodParam, LoginModel loginModel) {
        Map<String, Object> result = new HashMap<>();
        JSONObject wxUser = wxMsgService.getSessionKeyByCode(methodParam.getSchemaName(), loginModel.getUserWxCode());
        result.put("session_key", wxUser != null ? wxUser.optString("session_key", "") : "");
        result.put("openId", wxUser != null ? wxUser.optString("openid", "") : "");
        result.put("unionId", wxUser != null ? wxUser.optString("unionid", "") : "");
        return result;
    }

    @Override
    public List<Map<String, Object>> getAllCache(MethodParam methodParam) {
        OtherCfgModel ocModel = new OtherCfgModel();
        List<Map<String, Object>> cacheList = cacheUtilService.getCacheListForPage(ocModel);
        return cacheList;
    }

    @Override
    public void clearAllCache(MethodParam methodParam, Map param) {
        String type = RegexUtil.optStrOrNull(param.get("type"));
        OtherCfgModel ocModel = new OtherCfgModel();
        List<Map<String, Object>> cacheList = cacheUtilService.getCacheListForPage(ocModel);
        if ("all".equals(type)) {
            RegexUtil.optNotNullList(cacheList).ifPresent(c -> {
                c.forEach(e -> {
                    cacheUtilService.evictCacheByName(RegexUtil.optStrOrNull(e.get("cache_name")));
                });
            });
        } else if ("one".equals(type)) {
            cacheUtilService.evictCacheByName(RegexUtil.optStrOrNull(param.get("cache_name")));
        } else if ("two".equals(type)) {
            cacheUtilService.evictCacheByKey(RegexUtil.optStrOrNull(param.get("cache_name")), RegexUtil.optStrOrNull(param.get("key")));
        }

    }
}
