package com.gengyun.senscloud.service.system.impl;

import com.gengyun.senscloud.common.ButtonConstant;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.mapper.CommonUtilMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.regex.Matcher;

/**
 * 公共工具用接口
 */
@Service
public class CommonUtilServiceImpl implements CommonUtilService {
    private static final Logger logger = LoggerFactory.getLogger(CommonUtilServiceImpl.class);
    @Resource
    CommonUtilMapper commonUtilMapper;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    private WorksService worksService;
    @Resource
    private WorkFlowTemplateService workFlowTemplateService;
    @Resource
    LogsService logsService;
    @Resource
    CompanyService companyService;

    /**
     * 根据系统默认语言进行国际化转换
     *
     * @param methodParam 系统参数
     * @param langKey     多语言key
     * @return 多语言转换值
     */
    @Override
    public String getLanguageInfoBySysLang(MethodParam methodParam, String langKey) {
        return RegexUtil.optStrOrVal(cacheUtilService.getDefaultLangDtl(methodParam, methodParam.getSystemLang()).get(langKey), langKey);
    }

    /**
     * 系统记录日志
     *
     * @param methodParam        系统变量
     * @param dataFieldStr       日志配置信息
     * @param newMap             页面数据
     * @param dbMap              数据库数据
     * @param logArr             打印日志数组
     * @param work_calendar_code 计划主键
     * @param businessNo         业务编号
     * @param remark             日志操作
     */
    @Override
    public void doSetLogInfo(MethodParam methodParam, String dataFieldStr, Map<String, Object> newMap, Map<String, Object> dbMap, JSONArray logArr, String work_calendar_code, String businessNo, String remark) {
        JSONArray dataFields = JSONArray.fromObject(dataFieldStr);
        for (int i = 0; i < dataFields.size(); i++) {
            JSONObject field = dataFields.getJSONObject(i);
            String checkType = RegexUtil.optStrOrBlank(field.get("checkType"));
            String name = RegexUtil.optStrOrBlank(field.get("name"));
            String typeKey = RegexUtil.optStrOrBlank(field.get("typeKey"));
            String dbValue = RegexUtil.optStrOrBlank(dbMap.get(name));
            String newValue = RegexUtil.optStrOrBlank(newMap.get(name));
            //输入框
            if ("1".equals(checkType)) {
                RegexUtil.notEqualsOpt(dbValue, newValue).ifPresent(a ->
                        logArr.add(LangUtil.doSetLogArray(remark, LangConstant.LOG_D, new String[]{typeKey, dbValue, newValue}))
                );
            }
            //下拉框
            else if ("2".equals(checkType) || "4".equals(checkType)) {
                RegexUtil.notEqualsOrStrOpt(dbValue, newValue).ifPresent(a -> {
                    String selectKey = RegexUtil.optStrOrBlank(field.get("selectKey"));
                    String getDbName = selectOptionService.getSelectOptionTextByCodeWithCache(methodParam, selectKey, RegexUtil.optStrOrVal(dbValue, "0"));
                    String getNewName = selectOptionService.getSelectOptionTextByCodeWithCache(methodParam, selectKey, RegexUtil.optStrOrVal(newValue, "0"));
                    logArr.add(LangUtil.doSetLogArray(remark, LangConstant.LOG_D, new String[]{typeKey, getDbName, getNewName}));
                });
            }
            //集合
            else if ("3".equals(checkType)) {
                RegexUtil.optStrToArraySplit((field.get("joinPrm"))).ifPresent(l -> {
                    Map<String, String> keyInfoMap = DataChangeUtil.scdObjToMapStr(field.get("keyInfo"));
                    List<String> list1 = SenscloudUtil.dateTypeChange(newValue, l);
                    List<String> list2 = SenscloudUtil.dateTypeChange(dbValue, l);
                    this.checkAbListForLog(list1, list2, logArr, remark, LangConstant.TEXT_AJ, LangConstant.BTN_ADD_A, typeKey, keyInfoMap, methodParam);
                    this.checkAbListForLog(list2, list1, logArr, remark, LangConstant.TEXT_AJ, LangConstant.BTN_DELETE_A, typeKey, keyInfoMap, methodParam);
                });
            }
        }
        if (logArr.size() != 0) {
            logsService.newLog(methodParam, businessNo, work_calendar_code, logArr.toString());

        }
    }

    /**
     * 根据用户默认语言进行国际化转换
     *
     * @param methodParam 系统参数
     * @param langKey     多语言key
     * @return 多语言转换值
     */
    @Override
    public String getLanguageInfoByUserLang(MethodParam methodParam, String langKey) {
        return RegexUtil.optStrOrVal(cacheUtilService.getDefaultLangDtl(methodParam, methodParam.getUserLang()).get(langKey), langKey);
    }

    /**
     * 分批批量插入表格数据
     *
     * @param dataList   数据列表
     * @param schemaName 数据库
     * @param tableName  表明
     * @param columns    需要插入的字段
     */
    @Override
    public void batchInsertDataList(String schemaName, List<Map<String, Object>> dataList, String tableName, String columns) {
        this.batchInsertDataListWithNum(schemaName, dataList, tableName, columns, Constants.BATCH_INSERT_NUM);
    }

    /**
     * 分批批量插入表格数据（指定条数）
     *
     * @param dataList   数据列表
     * @param schemaName 数据库
     * @param tableName  表明
     * @param columns    需要插入的字段
     * @param num        每批插入条数
     */
    @Override
    public void batchInsertDataListWithNum(String schemaName, List<Map<String, Object>> dataList, String tableName, String columns, int num) {
        if (dataList == null || dataList.size() == 0 || num == 0) {
            return;
        }
        // 分批批量插入
        int size = dataList.size();
        int p = size / num + (size % num == 0 ? 0 : 1);
        for (int i = 0; i < p; i++) {
            int begin = i * num;
            int end = (i + 1) * num - 1;
            end = (end < (size - 1) ? end : (size - 1));
            this.doBatchInsertSql(schemaName, tableName, columns, dataList.subList(begin, end + 1));
        }
    }

    /**
     * 查询所有的域名
     *
     * @return 所有的域名
     */
    @Override
    public List<String> getAllDomain() {
        List<String> result = DataChangeUtil.scdObjToListStr(companyService.requestNoParam("getAllDomain"));
        return result;
        // return commonUtilMapper.getAllDomain();
    }

    /**
     * 批量存储数据
     *
     * @param schemaName 数据库
     * @param tableName  表名
     * @param columns    列信息
     * @param dataList   数据
     */
    @Override
    @Deprecated
    public void doBatchInsertSql(String schemaName, String tableName, String columns, List<Map<String, Object>> dataList) {
        String[] columnNames = columns.split(",");
        StringBuilder row = new StringBuilder();
        Object value;
        for (Map<String, Object> data : dataList) {
            // 列
            StringBuilder columnSb = new StringBuilder();
            for (String column : columnNames) {
                value = data.get(column);
                if (null != value && !"".equals(value) && !"null".equals(value) && !JSONNull.getInstance().equals(value)) {
                    columnSb.append(",'").append(data.get(column)).append("'");
                } else {
                    columnSb.append(",null");
                }
            }
            // 行
            if (columnSb.length() > 0) {
                row.append(",(").append(columnSb.substring(1)).append(")");
            }
        }
        if (row.length() > 0) {
            commonUtilMapper.insertBatch("INSERT INTO " + schemaName + "." + tableName + " ( " + columns + ") values " + row.substring(1));
        }
    }

    /**
     * 根据父id，按顺序生成一个位置编号
     *
     * @param schema_name     数据库
     * @param no              默认编号
     * @param id              主键
     * @param parentId        父id值
     * @param tableName       表名
     * @param fieldName       编号字段名
     * @param parentFieldName 父编号字段名
     * @param keyFieldName    主键编号字段名
     * @param prefix          编号前缀
     * @param firstNo         第一串编号
     * @return 返回按顺序生成的一个编号
     */
    @Override
    public String getMaxNoByParentId(String schema_name, String no, Object id, Object parentId, String tableName, String fieldName, String parentFieldName, String keyFieldName, String prefix, String firstNo) {
        if (null == keyFieldName || "".equals(keyFieldName)) {
            keyFieldName = "id";
        }
        // 更新时判断是否需要重新获取
        if (null != id && !"".equals(id)) {
            String dbNo = commonUtilMapper.getNoByData(schema_name, id, parentId, tableName, fieldName, parentFieldName);
            if (null != dbNo && !"".equals(dbNo)) {
                return dbNo;
            }
        }
        // 获取编号
        if (null == no || "".equals(no)) {
            no = "001";
        }
        String maxNo = commonUtilMapper.getMaxNoByParentId(schema_name, parentId, tableName, fieldName, parentFieldName);
        // 非空时，增加1
        if (null != maxNo && !maxNo.isEmpty()) {
            if (null != prefix && !"".equals(prefix)) {
                maxNo = maxNo.substring(prefix.length());
            }
            int maxNoLen = maxNo.length();
            if (maxNoLen > 6) {
                String beginNo = maxNo.substring(0, maxNo.length() - 3);
                String endNo = maxNo.substring(maxNoLen - 3, maxNoLen);
                int noInt = Integer.parseInt(endNo);
                noInt++;
                String realNo = String.valueOf(noInt);
                while (realNo.length() < endNo.length()) {
                    realNo = "0".concat(realNo);
                }
                no = beginNo + realNo;
            } else {
                int noInt = Integer.parseInt(maxNo);
                noInt++;
                String realNo = String.valueOf(noInt);
                //补全前面的0
                while (realNo.length() < maxNoLen) {
                    realNo = "0".concat(realNo);
                }
                no = realNo;
            }
        } else {
            //没有子节点，为第一个子节点，编号为父编号+no
            if (null != parentId && !"".equals(parentId) && !"0".equals(parentId.toString())) {
                String dbNo = commonUtilMapper.getNoById(schema_name, parentId, tableName, fieldName, keyFieldName);
                if (null != dbNo && !"".equals(dbNo)) {
                    no = dbNo + no;
                }
            } else if (RegexUtil.optNotNull(firstNo).isPresent()) {
                no = firstNo;
            }
        }
        if (null != prefix && !"".equals(prefix) && !no.startsWith(prefix)) {
            no = prefix + no;
        }
        return no;
    }

    /**
     * 设备  设备位置 备件 二维码导出
     *
     * @param methodParam 入参
     * @param type        1设备 2设备位置 3备件
     * @return 二维码配置
     */
    @Override
    public Map<String, Object> qrCodeExport(MethodParam methodParam, Integer type) {
        Map<String, Object> map = new HashMap<>();
        String str = null;
        if (3 == type) {
            str = "bom_qr_code_export";
        } else {
            str = null == type || 1 == type ? "asset_qr_code_export" : "asset_position_qr_code_export";
        }
        List<Map<String, Object>> cloud_data_list = selectOptionService.getStaticSelectList(methodParam, str);
        Map<String, Object> reserve1 = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (Map<String, Object> cloud_data : cloud_data_list) {
            String reserve = RegexUtil.optStrOrBlank(cloud_data.get("reserve1")); // 占位符
            map.put(reserve, getLanguageInfoByUserLang(methodParam, RegexUtil.optStrOrBlank(cloud_data.get("text")))); // 占位符-显示名称
            String fieldCode = RegexUtil.optStrOrBlank(cloud_data.get("value")); // 字段名
            if ("company_name".equals(fieldCode)) {
                map.put(RegexUtil.optStrOrBlank(cloud_data.get("reserve1")), methodParam.getCompanyName()); // 企业名称
            }
            reserve1.put(reserve, fieldCode);
            map.put("fieldInfo", reserve1);
            list.add(fieldCode);
        }
        map.put("fieldCodes", list);
        return map;
    }

    /**
     * 替换模板中的占位符，输出填完值后的字符串
     *
     * @param param
     * @param patternStr
     * @return
     */
    @Override
    public String stringFormat(Object[] param, String patternStr) {
        try {
            Matcher m = Constants.SCD_PT_INT.matcher(patternStr);
            String format = patternStr;
            int count = 0;
            List<Object> params = new ArrayList<>();
            while (m.find()) {
                String tag = m.group();
                try {
                    format = format.replace(tag, "%s");
                    params.add(param[count]);
                } catch (Exception e) {
                    params.add(StringUtils.EMPTY);
                }
                count++;
            }
            return String.format(format, params.toArray());
        } catch (NumberFormatException e) {
            return "";
        }
    }

    /**
     * 更新序列开始值
     *
     * @param methodParam 系统参数
     * @param seq         序列名称
     * @param value       列名称
     */
    @Override
    public void modifySeqStartByTbl(MethodParam methodParam, String seq, int value) {
        commonUtilMapper.updateSeqStartByTbl(methodParam.getSchemaName(), seq, value);
    }

    /**
     * 更新序列（默认列）
     *
     * @param methodParam 系统参数
     * @param seq         序列名称
     * @param table       表名称
     */
    @Override
    public void modifySeqByTblForDefault(MethodParam methodParam, String seq, String table) {
        this.modifySeqByTbl(methodParam, seq, "id", table);
    }

    /**
     * 更新序列
     *
     * @param methodParam 系统参数
     * @param seq         序列名称
     * @param col         列名称
     * @param table       表名称
     */
    @Override
    public void modifySeqByTbl(MethodParam methodParam, String seq, String col, String table) {
        commonUtilMapper.updateSeqByTbl(methodParam.getSchemaName(), seq, col, table);
    }

    /**
     * 取表字段最大值
     *
     * @param methodParam 系统参数
     * @param col         列名称
     * @param table       表名称
     * @return 字段最大值
     */
    @Override
    public String getMaxId(MethodParam methodParam, String col, String table) {
        return commonUtilMapper.findMaxId(methodParam.getSchemaName(), col, table);
    }

    /**
     * 根据维保计划生成一条工单
     *
     * @param methodParam 系统参数
     * @param c           方法参数
     */
    @Transactional
    @Override
    public void createWorkOrderByPlanWorkCalendar(MethodParam methodParam, PlanWorkCalendarModel c) {
        Map<String, Object> flowStartNode = workFlowTemplateService.getWorkFlowStartNodeByWorkTypeId(methodParam, String.valueOf(c.getWork_type_id()));//获取流程的开始节点
        if (!RegexUtil.optIsPresentMap(flowStartNode)) {
            return;
        }

        String prefId = RegexUtil.optStrOrBlank(flowStartNode.get("pref_id"));
        String pageType = RegexUtil.optStrOrBlank(flowStartNode.get("page_type"));
        String nodeId = RegexUtil.optStrOrBlank(flowStartNode.get("node_id"));
        if (!RegexUtil.optIsPresentStr(prefId) || !RegexUtil.optIsPresentStr(pageType) || !RegexUtil.optIsPresentStr(nodeId)) {
            return;
        }

        Map<String, Object> workFlowNodeTemplateInfo = workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, prefId, nodeId, pageType);
        if (!RegexUtil.optIsPresentMap(workFlowNodeTemplateInfo)) {
            return;
        }

        worksService.worksSubmit(methodParam, getParamMap(methodParam, new Date(), c, flowStartNode, workFlowNodeTemplateInfo, pageType, prefId));
    }

    /**
     * 判断两集合包含关系，输出日志
     *
     * @param left    左集合
     * @param right   右集合
     * @param logArr  日志集合
     * @param remark  日志备注
     * @param logKey  日志多语言key
     * @param logPrmA 日志参数
     * @param logPrmB 日志参数
     */
    private void checkAbListForLog(List<String> left, List<String> right, JSONArray logArr, String remark, String logKey, String logPrmA, String logPrmB, Map<String, String> keyInfoMap, MethodParam methodParam) {
        left.stream()
                .filter(s -> !right.contains(s))
                .map(s -> {
                    if ("-".equals(String.valueOf(s.charAt(0))) || "-".equals(String.valueOf(s.charAt(s.length() - 1)))) {
                        s = s.substring(1, s.length() - 1);
                    }
                    return s;
                })
                .map(s -> null == keyInfoMap || keyInfoMap.size() == 0 ? s : spiltArrToSelectOption(methodParam, s, keyInfoMap))
                .map(s -> "【" + s + "】")
                .map(s -> LangUtil.doSetLogArray(remark, logKey, new String[]{logPrmA, logPrmB, s}))
                .forEach(logArr::add);
    }

    /**
     * 根据配置信息转换下拉框日志信息
     *
     * @param methodParam      系统变量
     * @param joinParam        值拼接
     * @param selectKeyInfoMap 特殊处理类型属性
     * @return 转换后日志内容
     */
    private String spiltArrToSelectOption(MethodParam methodParam, String joinParam, Map<String, String> selectKeyInfoMap) {
        String[] arr = joinParam.split("-");
        StringBuffer strBuf = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            String selectKey = selectKeyInfoMap.get(String.valueOf(i));
            arr[i] = RegexUtil.optNotBlankStrOpt(arr[i]).filter(m -> RegexUtil.optIsPresentStr(selectKey))
                    .map(s -> selectOptionService.getSelectOptionTextByCodeWithCache(methodParam, selectKey, s))
                    .orElse(RegexUtil.optStrOrBlank(arr[i]));

            RegexUtil.optNotBlankStrOpt(arr[i]).ifPresent(s -> {
                strBuf.append(s);
                strBuf.append("-");
            });
        }
        if ('-' == strBuf.charAt(strBuf.length() - 1)) {
            strBuf.deleteCharAt(strBuf.length() - 1);
        }
        return strBuf.toString();
    }


    /**
     * 拼接新建工单的请求参数
     *
     * @param methodParam
     * @param now
     * @param calendar
     * @param flowStartNode
     * @param workFlowNodeTemplateInfo
     * @param pageType
     * @param prefId
     * @return
     */
    private Map<String, Object> getParamMap(MethodParam methodParam, Date now, PlanWorkCalendarModel calendar, Map<String, Object> flowStartNode,
                                            Map<String, Object> workFlowNodeTemplateInfo, String pageType, String prefId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("relation_id", calendar.getRelation_id());
        paramMap.put("do_flow_key", ButtonConstant.BTN_SUBMIT);
        paramMap.put("type_name", workFlowNodeTemplateInfo.get("type_name"));
        paramMap.put("deal_role_id", flowStartNode.get("deal_role_id"));
        paramMap.put("create_time", now.getTime());
        paramMap.put("deal_type_id", flowStartNode.get("deal_type_id"));
        paramMap.put("page_type", pageType);
        paramMap.put("node_name", flowStartNode.get("node_name"));
        paramMap.put("work_template_code", workFlowNodeTemplateInfo.get("work_template_code"));
        paramMap.put("flow_name", workFlowNodeTemplateInfo.get("flow_name"));
        paramMap.put("relation_type", workFlowNodeTemplateInfo.get("relation_type"));
        paramMap.put("relation_type_name", workFlowNodeTemplateInfo.get("relation_type_name"));
        paramMap.put("flow_code", workFlowNodeTemplateInfo.get("flow_code"));
        paramMap.put("work_type_id", calendar.getWork_type_id());
        paramMap.put("word_flow_node_id", flowStartNode.get("id"));
        paramMap.put("node_type", flowStartNode.get("node_type"));
        paramMap.put("work_template_name", workFlowNodeTemplateInfo.get("work_template_name"));
        paramMap.put("pref_id", prefId);
        paramMap.put("node_show_name", flowStartNode.get("show_name"));
        paramMap.put("flow_show_name", workFlowNodeTemplateInfo.get("flow_show_name"));
        paramMap.put("id", workFlowNodeTemplateInfo.get("id"));
        paramMap.put("node_id", flowStartNode.get("node_id"));
        paramMap.put("create_user_id", methodParam.getUserId());
        paramMap.put("work_calendar_code", calendar.getWork_calendar_code());
        paramMap.put("next_user_id", calendar.getReceive_user_id());
        paramMap.put("occur_time", calendar.getOccur_time());
        paramMap.put("deadline_time", calendar.getDeadline_time());
        //任务项处理-取维保计划里配置的任务项
        String plan_work_property = RegexUtil.optIsPresentStr(calendar.getPlan_work_property()) ? calendar.getPlan_work_property() : calendar.getBody_property();
        if (RegexUtil.optIsPresentStr(plan_work_property)) {
            JSONObject json = JSONObject.fromObject(plan_work_property);
            // 职安健、备件、费用、工具
            Set keys = json.keySet();
            for (Object key : keys) {
                RegexUtil.optNotBlankStrOpt(key).ifPresent(k -> paramMap.put(k, json.get(k)));
            }
            // 任务项
            if (json.containsKey("task_list")) {
                paramMap.put("task_item", json.get("task_list"));
            }
            // 文件
            if (json.containsKey("file_list")) {
                String taskStr = json.getString("file_list");
                JSONArray jsonArray = JSONArray.fromObject(taskStr);
                List<String> l = new ArrayList<>();
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject j = jsonArray.getJSONObject(i);
                    RegexUtil.optNotBlankStrOpt(j.get("file_id")).ifPresent(l::add);
                }
                paramMap.put("file_list", String.join(",", l));
            }
        }
        return paramMap;
    }
}
