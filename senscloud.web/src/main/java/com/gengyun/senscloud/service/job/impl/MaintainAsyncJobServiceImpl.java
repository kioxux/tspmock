package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.service.job.MaintainAsyncJobService;
import com.gengyun.senscloud.service.job.MaintainJobService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MaintainAsyncJobServiceImpl implements MaintainAsyncJobService {
    private static final Logger logger = LoggerFactory.getLogger(MaintainAsyncJobServiceImpl.class);

    private static boolean isRunning = false;

    @Resource
    private MaintainJobService maintainJobService;

    /**
     * 根据新增、修改的维保计划生成相应的行事历 —— 异步执行
     * @param schemaName
     */
    @Override
    @Async
    public void asyncCronJobToGeneratePlanWorkWithNewModel(String schemaName) {
        logger.info("MaintainJobServiceImpl ~ start-" + RegexUtil.optStrOrBlank(schemaName));
        if(!isRunning) {
            try {
                //加锁，防止定时器重复执行，
                isRunning = true;
                logger.info("MaintainJobServiceImpl ~ start-sub-" + RegexUtil.optStrOrBlank(schemaName));
                maintainJobService.cronJobToGeneratePlanWorkWithNewModel(schemaName);
                logger.info("MaintainJobServiceImpl ~ end-sub-" + RegexUtil.optStrOrBlank(schemaName));
            } catch (Exception e) {
                logger.error("MaintainJobServiceImpl.asyncCronJobToGerenatePlanWorkWithNewModel fail ~ ", e);
            } finally {
                //运行完后释放锁
                isRunning = false;
            }
        }
        logger.info("MaintainJobServiceImpl ~ end-" + RegexUtil.optStrOrBlank(schemaName));
    }
}
