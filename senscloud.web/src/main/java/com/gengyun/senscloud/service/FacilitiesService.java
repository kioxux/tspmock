package com.gengyun.senscloud.service;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface FacilitiesService {

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getFacilitiesPermission(MethodParam methodParam);

    /**
     * 新增厂商
     *
     * @param methodParam 入参
     * @param param       入参
     */
    void newFacilities(MethodParam methodParam, Map<String, Object> param);

    /**
     * 更新厂商
     *
     * @param methodParam 入参
     * @param param       入参
     */
    void modifyFacilities(MethodParam methodParam, Map<String, Object> param);

    /**
     * 启用禁用厂商
     *
     * @param methodParam 入参
     * @param param       入参
     */
    void changeUseFacilities(MethodParam methodParam, Map<String, Object> param);

    /**
     * 删除厂商
     *
     * @param methodParam     入参
     * @param facilitiesModel 入参
     */
    void cutFacilities(MethodParam methodParam, FacilitiesModel facilitiesModel);
    /**
     * 批量删除厂商
     *
     * @param methodParam     入参
     * @param facilitiesModel 入参
     */
    void cutFacilitiesList(MethodParam methodParam, FacilitiesModel facilitiesModel);

    /**
     * 获取厂商详情
     *
     * @param methodParam     入参
     * @param facilitiesModel 入参
     * @return 厂商详情
     */
    Map<String, Object> findById(MethodParam methodParam, FacilitiesModel facilitiesModel);

    /**
     * 获取厂商列表
     *
     * @param methodParam 入参
     * @param param       入参
     * @return 厂商列表
     */
    Map<String, Object> findFacilitiesList(MethodParam methodParam, Map<String, Object> param);

    /**
     * 新增厂商附件
     *
     * @param methodParam 入参
     * @param param       入参
     */
    void newOrganizationDoc(MethodParam methodParam, Map<String, Object> param);

    /**
     * 删除厂商附件
     *
     * @param methodParam 入参
     * @param param       入参
     */
    void cutOrganizationDoc(MethodParam methodParam, Map<String, Object> param);

    /**
     * 获取厂商附件列表
     *
     * @param methodParam 入参
     * @param param       入参
     * @return 厂商附件列表
     */
    List<Map<String, Object>> getOrganizationDocList(MethodParam methodParam, Map<String, Object> param);

    /**
     * 新增厂商联系人
     */
    void newOrganizationContact(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 编辑厂商联系人
     */
    void modifyOrganizationContact(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除厂商联系人
     */
    void cutOrganizationContact(MethodParam methodParam, FacilitiesModel facilitiesModel);

    /**
     * 查询厂商联系人列表
     */
    List<Map<String, Object>> getOrganizationContactList(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 获取外部组织
     *
     * @param schemaName
     * @return
     */
    Map<String, Map<String, Object>> queryOuterSuppliesList(String schemaName);

    Map<String, Map<String, Object>> queryOuterSupplierList(String schemaName);

    Map<String, Map<String, Object>> queryOuterManufacturerList(String schemaName);

    /**
     * 根据编码查询厂商详情
     *
     * @param schemaName 入参
     * @param code        入参
     * @return 厂商详情
     */
    Map<String, Object> findByCode(String schemaName,  String code);
}