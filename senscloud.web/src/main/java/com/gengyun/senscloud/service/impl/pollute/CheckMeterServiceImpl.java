package com.gengyun.senscloud.service.impl.pollute;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.CheckMeterModel;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.CustomersService;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.impl.CustomersServiceImpl;
import com.gengyun.senscloud.service.job.CheckMeterJobService;
import com.gengyun.senscloud.service.job.SewageRecordsJobService;
import com.gengyun.senscloud.service.pollute.CheckMeterService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CheckMeterServiceImpl implements CheckMeterService {
    private static final Logger logger = LoggerFactory.getLogger(CheckMeterServiceImpl.class);

    private static boolean isRunning = false;
    private static boolean isSewageRunning = false;

    @Resource
    CheckMeterMapper checkMeterMapper;

    @Resource
    PolluteSurveyRecordMapper polluteSurveyRecordMapper;

    @Resource
    CustomersService customersService;

    @Resource
    ExportService exportService;

    @Resource
    LogsService logService;

    @Resource
    CustomersServiceImpl customersServiceImpl;

    @Resource
    PolluteFeeMapper polluteFeeMapper;

    @Resource
    SystemConfigMapper systemConfigMapper;

    @Resource
    CheckMeterJobService checkMeterJobService;

    @Resource
    SewageRecordsJobService sewageRecordsJobService;

    @Resource
    LogsMapper logsMapper;

    /**
     * 查询排污记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findCheckMeterList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = checkMeterMapper.findCheckMetersListCount(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        paramMap.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = checkMeterMapper.findCheckMetersList(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 抄表计量统计数据
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findCheckMeterStatistics(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        Map<String, Object> result = RegexUtil.optMapOrNew(checkMeterMapper.findCheckMeterStatistics(methodParam.getSchemaName(),methodParam.getUserId(), paramMap));
        List<Map<String, Object>> list = checkMeterMapper.findIsCreatePollute(methodParam.getSchemaName(), paramMap);
        result.put("fac", list);
        return result;
    }

    /**
     * 设备抄表记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findAssetCheckMeterList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = checkMeterMapper.findAssetCheckMeterListCount(methodParam.getSchemaName(), paramMap);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        paramMap.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = checkMeterMapper.findAssetCheckMeterList(methodParam.getSchemaName(), paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 排污详情
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findCheckMeterDetailInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_B_O);//不能为空
        Map<String, Object> map = RegexUtil.optMapOrNew(checkMeterMapper.findCheckMeterById(methodParam.getSchemaName(), Integer.valueOf(String.valueOf(paramMap.get("id")))));
        List<Map<String, Object>> list = polluteSurveyRecordMapper.findPolluteSurveyRecordByCheckMeterId(methodParam.getSchemaName(), Integer.valueOf(String.valueOf(paramMap.get("id"))));
        map.put("pollute_surver_records", list);
        return map;
    }

    /**
     * 手工抄表
     *
     * @param methodParam
     * @param model
     */
    @Override
    public void newCheckMeter(MethodParam methodParam, CheckMeterModel model) {
        RegexUtil.optStrOrExpNotNull(model.getFacility_id(), LangConstant.TITLE_AJ_G);//客户不能为空
        RegexUtil.optStrOrExpNotNull(model.getAsset_id(), LangConstant.TITLE_ASSET_AC_P);//设备不能为空
        RegexUtil.optStrOrExpNotNull(model.getIndication(), LangConstant.TITLE_BBA_D);//读数不能为空
        RegexUtil.optStrOrExpNotNull(model.getLast_indication(), LangConstant.TITLE_BBA_D);//读数不能为空
        RegexUtil.optStrOrExpNotNull(model.getCheck_time(), LangConstant.TITLE_DATE_V);//时间不能为空
        FacilitiesModel facilitiesModel = new FacilitiesModel();
        facilitiesModel.setId(model.getFacility_id());
        //校验客服
        Map<String,Object> customer = RegexUtil.optMapOrExp(customersService.findById(methodParam,facilitiesModel),LangConstant.TITLE_AJ_G);
        Map<String,Object> param = new HashMap();
        param.put("id",model.getFacility_id());
        //校验设备
        List<Map<String,Object>> assetList = customersService.findRelationAssetList(methodParam,param);
        List<Map<String,Object>> containList = assetList.stream().filter(e->model.getAsset_id().equals(e.get("asset_id"))).collect(Collectors.toList());
        RegexUtil.optListOrExp(containList,LangConstant.TITLE_ASSET_AC_P);
        param = new HashMap();
        param.put("asset_id",model.getAsset_id());
        param.put("check_time",model.getCheck_time());
        Map<String,Object> old = RegexUtil.optMapOrNew(findLastCheckMeter(methodParam,param));
        Map<String,Object> after = RegexUtil.optMapOrNew(findAfterCheckMeter(methodParam,param));
        BigDecimal old_indecation = RegexUtil.optIsPresentStr(old.get("indication"))?new BigDecimal(old.get("indication").toString()):BigDecimal.ZERO;
        BigDecimal after_indecation = RegexUtil.optIsPresentStr(after.get("indication"))?new BigDecimal(after.get("indication").toString()):BigDecimal.ZERO;
        String old_check_time = RegexUtil.optIsPresentStr(old.get("check_time"))?String.valueOf(old.get("check_time")):"1900-01-01 00:00:01";
        //校验数据是否合理
        if(model.getLast_indication().compareTo(old_indecation)!=0 || model.getIndication().compareTo(old_indecation)<0 || (after_indecation.compareTo(BigDecimal.ZERO)==0 ? false : model.getIndication().compareTo(after_indecation)>0))
            RegexUtil.trueExp(true,LangConstant.MSG_BU);
        model.setCheck_type(CheckMeterTypeEnum.MANUAL.getKey()); //手工抄表
        model.setWater_quality(WaterQualityEnum.NORMAL.getKey());//水质正常
        model.setStatus(StatusConstant.CHECK_METER_CHECKED); //已抄
        model.setUnusual_type(UnusualTypeEnum.NORMAL.getKey()); //异常状态正常
        model.setCreate_user_id(methodParam.getUserId());
        model.setFlow(model.getIndication().subtract(model.getLast_indication()==null? BigDecimal.ZERO:model.getLast_indication()));
        model.setFinal_flow(model.getFlow());
        model.setBelong_date(model.getCheck_time().substring(0,10));
        Integer id = checkMeterMapper.insert(methodParam.getSchemaName(),model);
        logsMapper.insertLog(methodParam.getSchemaName(), SensConstant.BUSINESS_NO_6003, RegexUtil.optStrOrNull(id), LangUtil.doSetLogArray("新增手动抄表记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_B_U, "记录："
                .concat(RegexUtil.optStrOrBlank(customer.get("short_title")))
                .concat("，流量计编号：").concat(RegexUtil.optStrOrBlank(containList.get(0).get("asset_code")))
                .concat("，当前读数：").concat(RegexUtil.optStrOrBlank(model.getIndication()))}), methodParam.getUserId(),
                RegexUtil.optNotBlankStrOpt(model.getCheck_time()).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT)).map(d -> new Timestamp(d.getTime())).orElse(SenscloudUtil.getNowTime()));
        //同步更新往后的当前排水量
        List<Map<String, Object>> afterCheckMeter = RegexUtil.optNotNullListOrNew(checkMeterMapper.findAfterCheckMeter(methodParam.getSchemaName(), model));
        if (!afterCheckMeter.isEmpty()) {
            checkMeterMapper.updateCheckMeter(methodParam.getSchemaName(), afterCheckMeter);
        }
    }

    /**
     * 设备最近一笔抄表记录
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findLastCheckMeter(MethodParam methodParam, Map<String, Object> paramMap) {
//        RegexUtil.optStrOrExpNotNull(paramMap.get("asset_id"), LangConstant.TITLE_ASSET_AC_P);//客户不能为空
        Map<String,Object> defaultMap = new HashMap();
        defaultMap.put("indication",0);
        Map<String,Object> lastMap = checkMeterMapper.findLastAssetCheckMeter(methodParam.getSchemaName(),paramMap);
        if(RegexUtil.optIsPresentMap(lastMap))
            return lastMap;
        return defaultMap;
    }

    /**
     * 设备最近后的一笔抄表记录
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findAfterCheckMeter(MethodParam methodParam, Map<String, Object> paramMap) {
        Map<String,Object> defaultMap = new HashMap();
        defaultMap.put("indication",0);
        Map<String,Object> afterMap = checkMeterMapper.findAfterAssetCheckMeter(methodParam.getSchemaName(),paramMap);
        if(RegexUtil.optIsPresentMap(afterMap))
            return afterMap;
        return defaultMap;
    }

    /**
     * 抄表日历数据
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findCheckMeterCalendar(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        return checkMeterMapper.findCheckMeterCalendar(methodParam.getSchemaName(),methodParam.getUserId(),paramMap);

    }



    /**
     * 编辑抄表记录
     * @param methodParam
     * @param model
     */
    @Override
    public void modifyCheckMeter(MethodParam methodParam, CheckMeterModel model) {
        RegexUtil.optIntegerOrExpParam(model.getId(), LangConstant.TITLE_ACJ);//不能为空
        model.setModify_user_id(methodParam.getUserId());
        Map<String, Object> oldMap = checkMeterMapper.findCheckMeterById(methodParam.getSchemaName(), model.getId());
        Map<String, Object> pm = new HashMap();
        pm.put("asset_id",model.getAsset_id());
        pm.put("check_time",model.getCheck_time());
        Map<String,Object> old = RegexUtil.optMapOrNew(findLastCheckMeter(methodParam,pm));
        Map<String,Object> after = RegexUtil.optMapOrNew(findAfterCheckMeter(methodParam,pm));
        BigDecimal old_indecation = RegexUtil.optIsPresentStr(old.get("indication"))?new BigDecimal(old.get("indication").toString()):BigDecimal.ZERO;
        BigDecimal after_indecation = RegexUtil.optIsPresentStr(after.get("indication"))?new BigDecimal(after.get("indication").toString()):BigDecimal.ZERO;
        //校验数据是否合理
        if(model.getIndication().compareTo(old_indecation)<0 || (after_indecation.compareTo(BigDecimal.ZERO)==0 ? false : model.getIndication().compareTo(after_indecation)>0)) {
            RegexUtil.trueExp(true,LangConstant.MSG_BU);
        }
        checkMeterMapper.modifyCheckMeterFinalFlow(methodParam.getSchemaName(),model);
        Map<String, Object> param = JSON.parseObject(JSON.toJSONString(model),Map.class);
        if (!Objects.equals(oldMap.get("remark"),model.getRemark()) || RegexUtil.optBigDecimalOrVal(oldMap.get("indication"),"0",null).compareTo(model.getIndication())!=0) {
            StringBuffer log = new StringBuffer("，：").append(RegexUtil.optStrOrBlank(oldMap.get("short_title"))).append("，：")
                    .append(RegexUtil.optStrOrBlank(oldMap.get("asset_code")));
            if (RegexUtil.optBigDecimalOrVal(oldMap.get("indication"),"0",null).compareTo(model.getIndication())!=0) {
                log.append("，【当前读数】").append(RegexUtil.optStrOrBlank(oldMap.get("indication"))).append("修改为").append(RegexUtil.optStrOrBlank(model.getIndication()));
            }
            if (!Objects.equals(oldMap.get("remark"),model.getRemark())) {
                log.append("，【备注】").append(RegexUtil.optStrOrBlank(oldMap.get("remark"))).append("修改为").append(RegexUtil.optStrOrBlank(model.getRemark()));
            }
            logsMapper.insertLog(methodParam.getSchemaName(), SensConstant.BUSINESS_NO_6003, param.get("id").toString(), LangUtil.doSetLogArray("编辑了抄表记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_B_U, log.toString()}), methodParam.getUserId(),
                    RegexUtil.optNotBlankStrOpt(model.getCheck_time()).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT_SS)).map(d -> new Timestamp(d.getTime())).orElse(SenscloudUtil.getNowTime()));
        }
        //同步更新往后的当前排水量
        List<Map<String, Object>> afterCheckMeter = RegexUtil.optNotNullListOrNew(checkMeterMapper.findAfterCheckMeter(methodParam.getSchemaName(), model));
        if (!afterCheckMeter.isEmpty()) {
            checkMeterMapper.updateCheckMeter(methodParam.getSchemaName(), afterCheckMeter);
        }
    }

    /**
     * 根据选中主键下载抄表记录信息
     *
     * @param methodParam 系统参数
     * @param model     请求参数
     */
    @Override
    public String getExportCheckMeter(MethodParam methodParam, CheckMeterModel model) {
        return exportService.doDownloadAssetInfo(methodParam, this.getCheckMeterListByParam(methodParam, model, Constants.SEARCH_DTL_TYPE));
    }

    @Override
    public Map<String, Object> findCheckMeterMonthList(MethodParam methodParam, Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = checkMeterMapper.findCheckMetersMonthListCount(methodParam.getSchemaName(), paramMap);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        paramMap.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = checkMeterMapper.findCheckMetersMonthList(methodParam.getSchemaName(), paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 获取抄表记录信息列表
     *
     * @param methodParam 系统参数
     * @param model     请求参数
     * @param listType    列表查询类型
     * @return 抄表记录信息列表
     */
    public List<Map<String, Object>> getCheckMeterListByParam(MethodParam methodParam, CheckMeterModel model, String listType) {
        methodParam.setSearchDtlType(listType);
        Map<String, Object> info = this.getCheckMeterListForPage(methodParam, model); // 数据权限判断
        return RegexUtil.optObjToListOrExp(RegexUtil.optMapOrExpNullInfo(info, LangConstant.TITLE_ASSET_I).get("rows"), LangConstant.TITLE_ASSET_I); // 设备信息不存在
    }

    public Map<String, Object> getCheckMeterListForPage(MethodParam methodParam, CheckMeterModel model) {
        RegexUtil.optStrOrExpNotNull(model.getBegin_date(), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(model.getEnd_date(), LangConstant.TITLE_DATE_D);//日期不能为空
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("begin_date", model.getBegin_date());
        paramMap.put("end_date", model.getEnd_date());
        //查询该条件下的总记录数
        int total = checkMeterMapper.findCheckMetersListCount(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", null);
            return result;
        }
        //查询数据列表
        List<Map<String, Object>> rows = checkMeterMapper.findCheckMetersList(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    @Override
    public List<Map<String, Object>> findCheckMeterInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        String group_title = RegexUtil.optStrOrExpNotNull(paramMap.get("group_title"), LangConstant.TITLE_AAZ_P);
        return polluteFeeMapper.polluteFeeMapper(methodParam.getSchemaName(), group_title);
    }

    @Override
    public void autoCheckMeterSeting(MethodParam methodParam, Map<String, Object> paramMap) {
        JSONArray jsonArray = JSONArray.fromObject(paramMap.get("sysConfigs"));
        for (Object object : jsonArray) {
            Map<String, Object> map = (Map<String, Object>) JSONObject.fromObject(object);
            systemConfigMapper.updateSysConfig(methodParam.getSchemaName(), map);

            if (Objects.equals(map.get("config_name"), "check_meter_shedule_hour")) {
                Map<String, Object> param = new HashMap<>();
                param.put("task_class", "com.gengyun.senscloud.job.CheckMeterJobBean");
                // 0 0 10 * * ?
                String cron = "0 0 @day_hour@ * * ?";
                String upCron = cron.replaceAll("@day_hour@", RegexUtil.optStrOrVal(map.get("setting_value"), "0"));
                param.put("expression", upCron);
                polluteFeeMapper.modfiySysShedule(methodParam.getSchemaName(), param);
            }
        }
    }

    @Override
    @Async
    public void asyncCronJobToGenerateCheckMeter(String schemaName) {
        logger.info("CheckMeterServiceImpl ~ start-" + RegexUtil.optStrOrBlank(schemaName));
        if (!isRunning) {
            try {
                //加锁，防止定时器重复执行，
                isRunning = true;
                logger.info("CheckMeterServiceImpl ~ start-sub-" + RegexUtil.optStrOrBlank(schemaName));
                checkMeterJobService.cronJobToGenerateCheckMeter(schemaName);
                logger.info("CheckMeterServiceImpl ~ end-sub-" + RegexUtil.optStrOrBlank(schemaName));
            } catch (Exception e) {
                logger.error("CheckMeterServiceImpl.cronJobToGenerateCheckMeter fail ~ ", e);
            } finally {
                //运行完后释放锁
                isRunning = false;
            }
        }
        logger.info("CheckMeterServiceImpl ~ end-" + RegexUtil.optStrOrBlank(schemaName));
    }

    @Override
    @Async
    public void asyncCronJobToGenerateSewageRecords(String schemaName) {
        logger.info("CheckMeterServiceImpl ~ start-" + RegexUtil.optStrOrBlank(schemaName));
        if (!isSewageRunning) {
            try {
                //加锁，防止定时器重复执行，
                isSewageRunning = true;
                logger.info("CheckMeterServiceImpl ~ start-sub-" + RegexUtil.optStrOrBlank(schemaName));
                sewageRecordsJobService.cronJobToGenerateSewageRecords(schemaName);
                logger.info("CheckMeterServiceImpl ~ end-sub-" + RegexUtil.optStrOrBlank(schemaName));
            } catch (Exception e) {
                logger.error("CheckMeterServiceImpl.asyncCronJobToGenerateSewageRecords fail ~ ", e);
            } finally {
                //运行完后释放锁
                isSewageRunning = false;
            }
        }
        logger.info("CheckMeterServiceImpl ~ end-" + RegexUtil.optStrOrBlank(schemaName));
    }
}
