package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.AssetMapper;
import com.gengyun.senscloud.mapper.AssetModelMapper;
import com.gengyun.senscloud.mapper.BomStockListMapper;
import com.gengyun.senscloud.mapper.WorksMapper;
import com.gengyun.senscloud.model.AssetSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.asset.AssetCategoryService;
import com.gengyun.senscloud.service.asset.AssetDataService;
import com.gengyun.senscloud.service.asset.AssetModelService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.*;
import io.swagger.annotations.Api;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "设备管理")
@Service
public class AssetDataServiceImpl implements AssetDataService {
    private Logger logger = LoggerFactory.getLogger(AssetDataServiceImpl.class);
    @Resource
    AssetMapper assetMapper;
    @Resource
    AssetModelMapper assetModelMapper;
    @Resource
    WorksMapper worksMapper;
    @Resource
    LogsService logService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    ExportService exportService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    AssetCategoryService assetCategoryService;
    @Resource
    AssetModelService assetModelService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    FilesService filesService;
    @Resource
    BomStockListMapper bomStockListMapper;
    @Resource
    CompanyService companyService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getAssetListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[0]);
    }

    /**
     * 获取设备列表
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getAssetListForPage(MethodParam methodParam, AssetSearchParam asParam) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        // TODO 时区处理
//        asParam.setStartDateSearch(SCTimeZoneUtil.requestParamHandler("startDateSearch")); // 开始日期
//        asParam.setEndDateSearch(SCTimeZoneUtil.requestParamHandler("endDateSearch")); // 开始日期

        String schemaName = methodParam.getSchemaName();
        // 获取按条件拼接的sql
        String searchWord = this.getAssertWhereString(methodParam, asParam);
        int total = assetMapper.countAssetList(schemaName, searchWord, methodParam.getUserId()); // 获取设备的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by  t1.asset_code,t1.parent_id ";
            searchWord += pagination;

            List<Map<String, Object>> assetList = null;
            String searchDtlType = methodParam.getSearchDtlType();
            String userId = methodParam.getUserId();
            if (RegexUtil.optIsPresentStr(searchDtlType)) {
                if (Constants.SEARCH_DTL_TYPE.equals(searchDtlType)) {
                    assetList = assetMapper.findExportAssetList(schemaName, searchWord, userId);
                } else if (Constants.SEARCH_IMPORT_TYPE.equals(searchDtlType)) {
                    assetList = assetMapper.findExportAssetList(schemaName, searchWord, userId);
                    // 需要取自定义字段信息
                } else if (Constants.SEARCH_BODY_TYPE.equals(searchDtlType)) {
                    assetList = assetMapper.findAssetList(schemaName, searchWord, userId);
                    RegexUtil.optNotNullList(assetList).ifPresent(l -> {
                        // 自定义字段信息处理
                        RegexUtil.optNotNullListStr(asParam.getFieldList()).ifPresent(fl -> {
                            StringBuffer s = new StringBuffer("");
                            List<String> fieldList = new ArrayList<>();
                            for (String str : fl) {
                                if (RegexUtil.optIsPresentStr(str)) {
                                    if (!"".equals(s.toString())) {
                                        s.append(" or ");
                                    }
                                    s.append(" pp ->> 'field_code' = '").append(str).append("' ");
                                    fieldList.add(str);
                                }
                            }
                            if (RegexUtil.optIsPresentStr(s)) {
                                String whereString = " where " + s.toString();
                                // 需要获取数据的设备id
                                List<String> nl = l.stream().filter(io -> RegexUtil.optIsPresentStr(io.get("id"))).map(io -> io.get("id").toString()).collect(Collectors.toList());
                                RegexUtil.optNotNullListStr(nl).ifPresent(n -> {
                                    int len = n.size();
                                    String[] ss = n.toArray(new String[len]);
                                    // 获取自定义字段数据信息
                                    Map<String, Map<String, Object>> extFieldInfo = assetMapper.findAssetBodyInfo(schemaName, whereString, ss);
                                    if (null != extFieldInfo && extFieldInfo.size() > 0) {
                                        l.forEach(data -> {
                                            for (String str : fieldList) {
                                                if (!data.containsKey(str)) {
                                                    String id = RegexUtil.optStrOrBlank(data.get("id"));
                                                    RegexUtil.optNotNullMap(extFieldInfo.get(id + str)).ifPresent(ti -> {
                                                        String field_view_type = RegexUtil.optStrOrBlank(ti.get("field_view_type"));
                                                        String field_value = RegexUtil.optStrOrBlank(ti.get("field_value"));
                                                        if ("select".equals(field_view_type)) {
                                                            RegexUtil.optNotBlankStrOpt(ti.get("field_source")).ifPresent(fs -> {
                                                                data.put(str, RegexUtil.optNotBlankStrOpt(field_value).map(fv -> selectOptionService.getSelectOptionTextByCodeWithCache(methodParam, fs, fv)).orElse(""));
                                                                data.put(str + "_code", field_value);
                                                            });
                                                        } else {
                                                            data.put(str, field_value);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                }
            } else {
                assetList = assetMapper.findAssetList(schemaName, searchWord, userId);
            }
            // TODO 时区处理
//        SCTimeZoneUtil.responseObjectListDataHandler(assetList);
            Map<Integer, Map<String, Object>> acInfo = assetCategoryService.getCategoryNameString(methodParam.getSchemaName());
            if (null != acInfo && acInfo.size() > 0) {
                RegexUtil.optNotNullList(assetList).ifPresent(l -> l.forEach(info ->
                                RegexUtil.optNotBlankStrOpt(info.get("category_id")).ifPresent(cid ->
                                        RegexUtil.optNotNullMap(acInfo.get(Integer.valueOf(cid))).ifPresent(m -> info.put("category_name", m.get("all_name")))
                                )
                        )
                );
            }
            result.put("rows", assetList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 根据主键批量删除设备（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    @Override
    public void deleteAssetByIds(MethodParam methodParam) {
        String[] idStr = methodParam.getDataIdArray();
        int len = idStr.length;
        RegexUtil.falseExp(len > 0, LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        RegexUtil.falseExp(assetMapper.setSelectStatus(methodParam.getSchemaName(), idStr, StatusConstant.STATUS_DELETEED) == len, LangConstant.MSG_CD); // 失败：请获取最新数据后重试！
        boolean isBatchDel = RegexUtil.booleanCheck(methodParam.getBatchDeal());
        String remark = isBatchDel ? LangUtil.doSetLogArrayNoParam("批量删除了设备！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了设备！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ASSET_I});
        for (String id : idStr) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, id, remark);
        }
    }

    /**
     * 根据主键展示设备二维码（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    @Override
    public void doShowAssetQRCode(MethodParam methodParam) {
        String id = methodParam.getDataId();
        Map<String, Object> asset = assetMapper.findAssetById(methodParam.getSchemaName(), id);
        RegexUtil.optNotNullOrExp(asset, LangConstant.TEXT_K); // 信息不存在！
        QRCodeUtil.doShowQRCode(RegexUtil.optStrOrPrmError(asset.get("id"), methodParam, ErrorConstant.EC_ASSET_1, id));
    }

    /**
     * 根据选中主键下载设备二维码
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    @Override
    public void doExportAssetQRCode(MethodParam methodParam, AssetSearchParam asParam) {
        Map<String, Object> map = commonUtilService.qrCodeExport(methodParam, 1);
        asParam.setFieldList(DataChangeUtil.scdObjToListStr(map.get("fieldCodes")));
        List<Map<String, Object>> assetList = this.getAssetDtlListByParam(methodParam, asParam, Constants.SEARCH_BODY_TYPE); // 数据权限判断，获取数据
        map.put("dataList", assetList);
        map.put("companyId", methodParam.getCompanyId());
        map.put("companyName", methodParam.getCompanyName());
        String schemaName = methodParam.getSchemaName();
        map.put("qrCodePrefix", RegexUtil.optStrOrBlank(systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.QR_CODE_PREFIX)));
        String ctVal = systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.CODE_TEMPLATE);
        if (RegexUtil.optEqualsOpt(ctVal, "2").isPresent()) {
            QRCodeUtil.doDownloadBRCodePdf(map);
        } else {
            QRCodeUtil.doDownloadQRCodePdf(map);
        }
    }

    /**
     * 获取设备导入模板文件码（文件码）
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    @Override
    public String getImportAssetCode(MethodParam methodParam, AssetSearchParam asParam) {
        return exportService.doDownloadAssetInfo(methodParam, this.getAssetDtlListByParam(methodParam, asParam, Constants.SEARCH_IMPORT_TYPE));
    }

    /**
     * 根据选中主键下载设备信息（文件码）
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    @Override
    public String getExportAssetCode(MethodParam methodParam, AssetSearchParam asParam) {
        return exportService.doDownloadAssetInfo(methodParam, this.getAssetDtlListByParam(methodParam, asParam, Constants.SEARCH_DTL_TYPE));
    }

    /**
     * 添加设备
     *
     * @param methodParam 系统参数
     * @param data        数据信息
     */
    @Override
    public void doAddAsset(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_ASSET_AE}); // 设备编码不能为空
        String asset_code = RegexUtil.optStrOrExpNotNull(data.get("asset_code"), LangConstant.TITLE_ASSET_AE); // 设备编码不能为空
        int categoryId = RegexUtil.optSelectOrExpParam(data.get("category_id"), LangConstant.TITLE_ASSET_C); // 设备类型未选择
        RegexUtil.optNotNullOrExpNullInfo(assetCategoryService.getAssetCategoryInfoById(methodParam, categoryId), LangConstant.TITLE_ASSET_C); // 设备类型不存在
        int assetModelId = RegexUtil.optSelectOrExpParam(data.get("asset_model_id"), LangConstant.TITLE_ASSET_D); // 设备型号未选择
        String schemaName = methodParam.getSchemaName();
        RegexUtil.trueExp(assetMapper.countAssetById(schemaName, asset_code) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_ASSET_AE}); // 设备编码重复，请修改
        data.put("running_status_id", RegexUtil.optIntegerOrNull(selectOptionService.getSelectFirstOptionAttrByCodeWithCache(methodParam, "asset_status", "value"), methodParam, ErrorConstant.CODE_ASSET_301)); // 获取默认的运行状态  下拉框第一个
        data.put("currency_id", RegexUtil.optNotBlankStrOpt(data.get("currency_id")).orElseGet(() -> RegexUtil.optStrOrNull(systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.CURRENT_CURRENCY))));
        RegexUtil.optNotBlankStrOpt(assetModelMapper.findAssetModelFileIdsById(schemaName, assetModelId)).ifPresent(afi -> data.put("file_ids", afi)); // 设备型号关联文档集合
        //{'name': 'x','nameKey': 'title_aaab_x','defaultValue': '0.0','checkType': '3'},{'name': 'y','nameKey': 'title_aaab_w','defaultValue': '0.0','checkType': '3'},{'name': 'asset_subject','nameKey': 'title_asset_ac_a','checkType': '4'},{'name': 'source_type','nameKey': 'title_asset_aw','checkType': '4'},
        String dataFieldStr = "[{'name': 'tax_price','nameKey': 'title_tax_b','checkType': '2'},{'name': 'tax_rate','nameKey': 'title_tax_a','checkType': '2'}," +
                "{'name': 'price','nameKey': 'title_price_a','checkType': '2'},{'name': 'install_price','nameKey': 'title_price_b','checkType': '2'}," +
                "{'name': 'use_year','nameKey': 'title_aau_b','checkType': '2'},{'name': 'iot_status','nameKey': 'title_aaad_y','checkType': '4'}," +
                "{'name': 'manufacturer_id','nameKey': 'title_mfr_a','checkType': '4'},{'name': 'supplier_id','nameKey': 'title_supplier_a','checkType': '4'}," +
                "{'name': 'importment_level_id','nameKey': 'title_asset_as','checkType': '4'},{'name': 'install_currency_id','nameKey': 'title_aaaab_d','checkType': '4'}," +
                "{'name': 'buy_currency_id','nameKey': 'title_cz','checkType': '4'},{'name': 'currency_id','nameKey': 'title_ag_t','checkType': '4'}," +
                "{'name': 'unit_id','nameKey': 'title_baab_a','checkType': '4'},{'name': 'use_add','nameKey': 'title_gj','checkType': '4'}," +
                "{'name': 'category_id','nameKey': 'title_asset_c','checkType': '4'},{'name': 'asset_model_id','nameKey': 'title_asset_d','checkType': '4'}," +
                "{'name': 'quantity','nameKey': 'title_asset_ab_f','defaultValue': '1','checkType': '5'},{'name': 'status','nameKey': 'title_status_b','defaultValue': '1','checkType': '5'}," +
                "{'name': 'asset_name','nameKey': 'title_asset_a','checkType': '6'},{'name': 'remark','nameKey': 'title_aah_h','checkType': '6'}," +
                "{'name': 'position_code','nameKey': 'title_asset_g','checkType': '6'},{'name': 'parent_id','nameKey': 'title_asset_ac_e','checkType': '6'}," +
                "{'name': 'enable_time','nameKey': 'title_aal_m','checkType': '7'},{'name': 'install_date','nameKey': 'title_install_b','checkType': '7'}," +
                "{'name': 'buy_date','nameKey': 'title_date_b','checkType': '7'}]";
        this.doSetAssetOtherInfo(methodParam, dataFieldStr, data, null, null, false);
        // 遍历设备类型自定义字段设定默认值
        RegexUtil.optNotNullMap(assetCategoryService.getAssetCategoryInfoById(methodParam, categoryId)).map(m -> RegexUtil.optStrOrBlank(m.get("properties"))).filter(RegexUtil::optIsPresentStr).map(JSONArray::fromObject).filter(a -> a.size() > 0).ifPresent(acArray -> {
            Map<String, Object> dbAfData = new HashMap<>(); // 遍历设备型号自定义字段用作缓存
            RegexUtil.optNotNullMap(assetModelService.getAssetModelInfoById(methodParam, assetModelId)).map(m -> RegexUtil.optStrOrBlank(m.get("properties"))).filter(RegexUtil::optIsPresentStr).ifPresent(properties -> SenscloudUtil.doCacheAssetFieldVal("new", properties, dbAfData, ErrorConstant.CODE_ASSET_302, ErrorConstant.CODE_ASSET_303, ErrorConstant.CODE_ASSET_304));
            RegexUtil.optNotNullMap(dbAfData).ifPresent(m -> {
                for (int i = 0; i < acArray.size(); i++) {
                    JSONObject jsonObject = acArray.getJSONObject(i);
                    RegexUtil.optNotBlankStrOpt(jsonObject.get("field_code")).ifPresent(k ->
                            jsonObject.put("field_value", RegexUtil.optStrOrBlank(m.get(k)))
                    );
                }
                data.put("properties", acArray.toString());
            });
        });
        data.put("schemaName", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        data.put("create_time", SenscloudUtil.getNowTime());
        String id = SenscloudUtil.generateUUIDStr();
        data.put("id", id);
        RegexUtil.intExp(assetMapper.insertAsset(data), LangConstant.MSG_BK); // 未知错误！
//        this.doSetAssetBomByModel(schemaName, assetModelId, asset_code, id, account); // 设备型号关联备件集合
//        doSetAssetIotInfo(schemaName, id, data, null, false); // 设置设备物联信息

        List<String> list = new ArrayList<>();
        RegexUtil.optNotBlankStrOpt(data.get("manufacturer_id")).ifPresent(list::add);
        RegexUtil.optNotBlankStrOpt(data.get("supplier_id")).ifPresent(list::add);
        RegexUtil.optNotNullListStr(list).ifPresent(l -> assetMapper.insertAssetOrgByList(schemaName, id, l));
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, id, LangUtil.doSetLogArray("设备新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_ASSET_I}));
    }

    /**
     * 编辑设备
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param data        数据信息
     */
    @Override
    public void doEditAsset(MethodParam methodParam, AssetSearchParam asParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        String id = asParam.getId();
        String sectionType = RegexUtil.optStrOrError(data.get("sectionType"), methodParam, ErrorConstant.EC_ASSET_2, id, LangConstant.MSG_BI);
        Map<String, Object> dbAssetInfo = this.getAssetDtlInfoByParam(methodParam);
        String schemaName = methodParam.getSchemaName();
        JSONArray logArr = new JSONArray();
        boolean isTop = "assetIcon".equals(sectionType) || "assetTop".equals(sectionType);
        if ("assetIcon".equals(sectionType)) {
            this.doSetAssetOtherInfo(methodParam, "[{'name': 'asset_icon', 'checkType': '6', 'nameKey': 'title_asset_ab_k'}]", data, dbAssetInfo, null, true);
            logArr.add(LangUtil.doSetLog("设备属性更新", LangConstant.TAB_DEVICE_A, new String[]{LangConstant.TITLE_ASSET_AB_K}));
        }
        if ("assetTop".equals(sectionType)) {
            String asset_code = RegexUtil.optStrOrExpNotNull(data.get("asset_code"), LangConstant.TITLE_ASSET_AE); // 设备编码不能为空
            String dbValue = RegexUtil.optStrOrBlank(dbAssetInfo.get("asset_code"));
            if (!asset_code.equals(dbValue)) {
                data.put("varchar@asset_code", "asset_code");
                RegexUtil.trueExp(assetMapper.countAssetByInfo(schemaName, asset_code, id) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_ASSET_AE}); // 设备编码重复，请修改
                logArr.add(LangUtil.doSetLog("设备属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_ASSET_AE, dbValue, asset_code}));
            }
            String dataFieldStr = "{'name': 'asset_name', 'checkType': '6','nameKey': 'title_asset_a'}," +
                    " {'name': 'running_status_id', 'defaultValue': '1', 'checkType': '5', 'selectKey': 'asset_status','nameKey': 'title_status_a'}, " +
                    "{'name': 'parent_id', 'checkType': '6', 'selectKey': 'asset','nameKey': 'title_asset_ac_e'}," +
                    "{'name': 'status', 'defaultValue': '1', 'checkType': '5', 'selectKey': 'asset_info_status','nameKey': 'title_status_b'}, " +
                    "{'name': 'guarantee_status_id', 'defaultValue': '1', 'checkType': '5', 'selectKey': 'warranties_type','nameKey': 'title_status_c'}";
            this.doSetAssetOtherInfo(methodParam, "[" + dataFieldStr + "]", data, dbAssetInfo, logArr, true);
        }
        if (!isTop) {
            String defaultSection = RegexUtil.optStrOrVal(selectOptionService.getSelectFirstOptionAttrByCodeWithCache
                    (methodParam, "asset_section_type", Constants.SELECT_CODE_NAME), Constants.ASSET_DEFAULT_SECTION);
            // 设备基础字段分组信息
            JSONArray baseInfoArray;
            Object sa = ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_ASSET_FIELD_SECTION_INFO + sectionType);
            if (null == sa) {
                Map<String, JSONArray> baseFieldAllInfo = new HashMap<>();
                RegexUtil.optNotNullListStr(selectOptionService.getAssetCommonFieldList(methodParam)).ifPresent(bl -> {
                    bl.forEach(bp -> {
                        try {
                            JSONObject json = JSONObject.fromObject(bp);
                            String section = RegexUtil.optStrOrVal(json.get("field_section_type"), defaultSection);
                            RegexUtil.optNotBlankStrOpt(json.get("field_code")).ifPresent(c -> {
                                JSONArray tmpInfoList = RegexUtil.optJaOrNew(baseFieldAllInfo.get(section));
                                tmpInfoList.add(json);
                                baseFieldAllInfo.put(section, tmpInfoList);
                            });
                        } catch (Exception e) {
                            logger.error(ErrorConstant.CODE_ASSET_201, id + e);
                        }
                    });
                    baseFieldAllInfo.forEach((key, value) -> ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_ASSET_FIELD_SECTION_INFO + key, value));
                });
                baseInfoArray = RegexUtil.objToJaOrNew(baseFieldAllInfo.get(sectionType));
            } else {
                baseInfoArray = RegexUtil.objToJaOrNew(sa);
            }
            // 修改基础字段
            boolean isChangeModel = false;
            int categoryId = RegexUtil.optIntegerOrVal(dbAssetInfo.get("category_id"), -1, LangConstant.TITLE_ASSET_C);
            int assetModelIdDb = RegexUtil.optIntegerOrVal(dbAssetInfo.get("asset_model_id"), -1, LangConstant.TITLE_ASSET_D);
            int assetModelId = assetModelIdDb;
            if (baseInfoArray.size() > 0) {
                for (int i = 0; i < baseInfoArray.size(); i++) {
                    JSONObject jsonObject = baseInfoArray.getJSONObject(i);
                    String k = RegexUtil.optStrOrBlank(jsonObject.get("field_code"));
                    if (!"".equals(k)) {
                        String dbValue = RegexUtil.optStrOrBlank(dbAssetInfo.get(k));
                        String newValue = RegexUtil.optStrOrBlank(data.get(k));
                        if (!newValue.equals(dbValue)) {
                            if ("category_id".equals(k)) {
                                categoryId = RegexUtil.optSelectOrExpParam(data.get("category_id"), LangConstant.TITLE_ASSET_C); // 设备类型未选择
                                RegexUtil.optNotNullOrExpNullInfo(assetCategoryService.getAssetCategoryInfoById(methodParam, categoryId), LangConstant.TITLE_ASSET_C); // 设备类型不存在
                            }
                            if ("asset_model_id".equals(k)) {
                                isChangeModel = true;
                                assetModelId = RegexUtil.optSelectOrExpParam(data.get("asset_model_id"), LangConstant.TITLE_ASSET_D);
                            }
                            String dataType = "varchar@";
                            if (",category_id,asset_model_id,importment_level_id,".contains("," + k + ",")) {
                                dataType = "int@";
                            } else if (",install_price,tax_rate,price,tax_price,use_year,".contains("," + k + ",")) {
                                dataType = "float@";
                            } else if (",enable_time,install_date,buy_date,".contains("," + k + ",")) {
                                dataType = "dateTime@";
                            }
                            data.put(dataType + k, k); // 特殊字段类型处理
                            this.doSetAssetEditLog(methodParam, logArr, jsonObject, k, "field_name", "field_source", dbValue, newValue);
                        }
                    }
                }
            }

            // 特殊字段类型处理
            if (data.containsKey("buy_currency_id")) {
                this.doSetAssetOtherInfo(methodParam, "[{'name': 'buy_currency_id','nameKey': 'title_cz','checkType': '4', 'selectKey': 'currency'}]", data, dbAssetInfo, logArr, true);
            }
            // 特殊字段类型处理
            if (data.containsKey("install_currency_id")) {
                this.doSetAssetOtherInfo(methodParam, "[{'name': 'install_currency_id','nameKey': 'title_aaaab_d','checkType': '4', 'selectKey': 'currency'}]", data, dbAssetInfo, logArr, true);
            }

            // 型号变动，自定义字段不再变动保持原值，附件需要同步
            if (isChangeModel) {
                // 设备型号关联文档集合
                List<String> fileIdList = RegexUtil.optNotBlankStrOpt(dbAssetInfo.get("file_ids")).map(fs -> Arrays.stream(fs.split(",")).filter(RegexUtil::optIsPresentStr).collect(Collectors.toList())).filter(RegexUtil::optIsPresentList).map(fsl -> {
                    String oldIds = assetModelMapper.findAssetModelFileIdsById(schemaName, assetModelIdDb);
                    return RegexUtil.optNotBlankStrOpt(oldIds).map(ois ->
                            RegexUtil.optNotNullListStr(Arrays.stream(ois.split(",")).filter(RegexUtil::optIsPresentStr).collect(Collectors.toList()))
                                    .map(afiList -> fsl.stream().filter(fid -> !afiList.contains(fid)).collect(Collectors.toList())).orElse(fsl)
                    ).orElse(fsl);
                }).orElseGet(ArrayList::new);
                String docIds = assetModelMapper.findAssetModelFileIdsById(schemaName, assetModelId);
                RegexUtil.optNotBlankStrOpt(docIds).flatMap(ids -> RegexUtil.optNotNullListStr(Arrays.stream(ids.split(",")).filter(RegexUtil::optIsPresentStr).collect(Collectors.toList()))).ifPresent(fileIdList::addAll);
                data.put("file_ids", fileIdList.stream().distinct().collect(Collectors.joining(",")));
                data.put("varchar@file_ids", "file_ids");
//                assetMapper.deleteAssetBom(schemaName, Integer.valueOf(assetModelIdDb), id);
//                this.doSetAssetBomByModel(schemaName, assetModelId, asset_code, id, account); // 设备型号关联备件集合
            }
            // 遍历设备自定义字段用作缓存
            Map<String, Object> dbAfData = new HashMap<>();
            RegexUtil.optNotBlankStrOpt(dbAssetInfo.get("properties")).ifPresent(properties -> SenscloudUtil.doCacheAssetFieldVal(id, properties, dbAfData, ErrorConstant.CODE_ASSET_204, ErrorConstant.CODE_ASSET_205, ErrorConstant.CODE_ASSET_206));
            // 设备类型字段
            JSONArray afArray = RegexUtil.optNotNullIntegerOpt(categoryId, methodParam, ErrorConstant.EC_ASSET_4, id)
                    .map(d -> assetCategoryService.getAssetCategoryInfoById(methodParam, d)).map(m -> m.get("fields")).filter(RegexUtil::optIsPresentStr).map(JSONArray::fromObject).filter(a -> a.size() > 0).orElseGet(JSONArray::new);
            if (afArray.size() > 0) {
                JSONArray newArray = new JSONArray();
                boolean isEditField = false;
                for (int i = 0; i < afArray.size(); i++) {
                    try {
                        JSONObject jsonObject = afArray.getJSONObject(i);
                        String k = RegexUtil.optStrOrBlank(jsonObject.get("field_code"));
                        if (!"".equals(k)) {
                            String dbValue = RegexUtil.optStrOrBlank(dbAfData.get(k));
                            // 页面分组内传递字段
                            if (data.containsKey(k)) {
                                String newValue = RegexUtil.optStrOrBlank(data.get(k));
                                if (!newValue.equals(dbValue)) {
                                    isEditField = true;
                                    this.doSetAssetEditLog(methodParam, logArr, jsonObject, k, "field_name", "field_source", dbValue, newValue);
                                }
                                jsonObject.put("field_value", newValue); // 使用页面值
                            } else {
                                jsonObject.put("field_value", dbValue); // 使用数据库值
                            }
                            newArray.add(jsonObject);
                        }
                    } catch (Exception e) {
                        logger.error(ErrorConstant.CODE_ASSET_203, id + e);
                    }
                }
                if (isEditField) {
                    data.put("jsonb@properties", "properties");
                    data.put("properties", newArray.toString());
                }
            }
        }
        // 未处理字段：[{'name': 'x', 'defaultValue': '0.0', 'checkType': '3'}, {'name': 'y', 'defaultValue': '0.0', 'checkType': '3'},  {'name': 'iot_status', 'checkType': '4'}, {'name': 'asset_subject', 'checkType': '4'},  {'name': 'currency', 'checkType': '4'}, {'name': 'unit_id', 'checkType': '4'}, {'name': 'source_type', 'checkType': '4'}, {'name': 'use_add', 'checkType': '4'}, {'name': 'quantity', 'defaultValue': '1', 'checkType': '5'}]
//        doSetAssetIotInfo(schemaName, id, data, dbAssetInfo, true); // 设置设备物联信息
        data.put("id", id);
        // 执行修改，记录修改日志
        RegexUtil.optNotNullJa(logArr).ifPresent(l -> {
            worksMapper.update(schemaName, data, "_sc_asset", "id");
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, id, l.toString());
        });
    }

    /**
     * 设备地图打点
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param data        数据信息
     */
    @Override
    public void editAssetMap(MethodParam methodParam, AssetSearchParam asParam, Map<String, Object> data) {
        String dataId = RegexUtil.optStrOrBlank(data.get("id"));
        methodParam.setDataId(dataId);
        Map<String, Object> dbAssetInfo = this.getAssetDtlInfoByParam(methodParam);
        String id = RegexUtil.optStrOrBlank(dbAssetInfo.get("id"));
        String location = RegexUtil.optStrOrBlank(data.get("location"));
        assetMapper.updateAssetMapLocation(methodParam.getSchemaName(), id, location);
    }

    /**
     * 获取设备明细信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 设备信息
     */
    @Override
    public Map<String, Object> getAssetDetailById(MethodParam methodParam, AssetSearchParam asParam) {
        String id = methodParam.getDataId();
        Map<String, Object> assetInfo = RegexUtil.optMapOrExpNullInfo(assetMapper.findAssetDetailById(methodParam.getSchemaName(), id), LangConstant.TITLE_ASSET_I); // 设备信息不存在
        JSONArray array = new JSONArray();
        RegexUtil.optNotNullListStr(selectOptionService.getAssetCommonFieldList(methodParam)).ifPresent(array::addAll); // 设备基础字段
        // 设备类型字段
        RegexUtil.optNotNullIntegerOpt(assetInfo.get("category_id"), methodParam, ErrorConstant.EC_ASSET_4, id)
                .map(d -> assetCategoryService.getAssetCategoryInfoById(methodParam, d)).map(m -> m.get("fields")).filter(RegexUtil::optIsPresentStr).map(JSONArray::fromObject).filter(a -> a.size() > 0).ifPresent(array::addAll);
        // 设备、设备型号
        String properties = RegexUtil.optNotBlankStrOpt(assetInfo.get("properties")).orElseGet(() ->
                RegexUtil.optNotNullIntegerOpt(assetInfo.get("asset_model_id"), methodParam, ErrorConstant.EC_ASSET_5, id).map(d -> assetModelService.getAssetModelInfoById(methodParam, d)).map(m -> RegexUtil.optStrOrBlank(m.get("properties"))).orElse("")
        );
        SenscloudUtil.doCacheAssetFieldVal(id, properties, assetInfo, ErrorConstant.CODE_ASSET_101, ErrorConstant.CODE_ASSET_103, ErrorConstant.CODE_ASSET_104); // 遍历设备自定义字段用作缓存
        int len = array.size();
        if (len > 0) {
            Map<String, JSONArray> map = new HashMap<>();
            String defaultSection = RegexUtil.optStrOrVal(selectOptionService.getSelectFirstOptionAttrByCodeWithCache
                    (methodParam, "asset_section_type", Constants.SELECT_CODE_NAME), Constants.ASSET_DEFAULT_SECTION);
            // 遍历字段补全字段值，分组转换
            for (int i = 0; i < len; i++) {
                try {
                    JSONObject json = array.getJSONObject(i);
                    RegexUtil.optNotBlankStrOpt(json.get("field_code")).ifPresent(c -> {
                        if ("category_id".equals(c) || "importment_level_id".equals(c)) {
                            json.put("field_value", RegexUtil.initIntegerWithRemark(assetInfo.get(c), methodParam, ErrorConstant.CODE_ASSET_102, id + "_" + c));
                        } else {
                            json.put("field_value", RegexUtil.optStrOrBlank(assetInfo.get(c)));
                        }
                        RegexUtil.optNotBlankStrOpt(json.get("field_select_code")).ifPresent(fc -> {
                            if ("buy_currency_id".equals(fc) || "install_currency_id".equals(fc)) {
                                json.put("field_select_value", RegexUtil.initIntegerWithRemark(assetInfo.get(fc), methodParam, ErrorConstant.CODE_ASSET_102, id + "_" + fc));
                            } else {
                                json.put("field_select_value", RegexUtil.optStrOrBlank(assetInfo.get(fc)));
                            }
                        });
                    });
                    String section = RegexUtil.optStrOrVal(json.get("field_section_type"), defaultSection);
                    json.put("field_section_type", section);
                    JSONArray jsonArray = RegexUtil.optJaOrNew(map.get(section));
                    jsonArray.add(json);
                    map.put(section, jsonArray);
                } catch (Exception e) {
                    logger.error(ErrorConstant.CODE_ASSET_105, id + e);
                }
            }
            RegexUtil.optNotNullList(selectOptionService.getSelectOptionListWithCache(methodParam, "asset_section_type")).ifPresent(l -> {
                List<Map<String, Object>> list = new ArrayList<>();
                l.forEach((m) ->
                        RegexUtil.optNotBlankStrOpt(m.get("value")).filter(map::containsKey).ifPresent(v -> {
                            Map<String, Object> info = new HashMap<>();
                            info.put("field_section_type", v);
                            info.put("list", map.get(v));
                            list.add(info);
                        })
                );
                assetInfo.put("properties", list);
            });
        }
        assetInfo.put("guarantee_status_name", RegexUtil.optNotBlankStrOpt(assetInfo.get("guarantee_status_id")).map(e -> selectOptionService.getSelectOptionTextByCode(methodParam, "warranties_type", e)).orElse(null));
        return assetInfo;
    }

    /**
     * 根据设备编码获取设备明细信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 设备信息
     */
    @Override
    public Map<String, Object> getAssetDetailByCode(MethodParam methodParam, AssetSearchParam asParam) {
        methodParam.setDataId(assetMapper.findAssetIdByCode(methodParam.getSchemaName(), asParam.getCodeSearch()));
        return getAssetDetailById(methodParam, asParam);
    }

    /**
     * 根据设备编码获取设备主键
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 设备信息
     */
    @Override
    public String getAssetIdByCode(MethodParam methodParam, AssetSearchParam asParam) {
        return assetMapper.findAssetIdByCode(methodParam.getSchemaName(), asParam.getCodeSearch());
    }

    /**
     * 获取并校验设备明细信息
     *
     * @param methodParam 系统参数
     * @return 设备信息
     */
    private Map<String, Object> getAssetDtlInfoByParam(MethodParam methodParam) {
        return RegexUtil.optMapOrExpNullInfo(assetMapper.findAssetById(methodParam.getSchemaName(), methodParam.getDataId()), LangConstant.TITLE_ASSET_I); // 设备信息不存在
    }


    /**
     * 获取设备明细信息列表
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param listType    列表查询类型
     * @return 设备信息列表
     */
    private List<Map<String, Object>> getAssetDtlListByParam(MethodParam methodParam, AssetSearchParam asParam, String listType) {
        methodParam.setSearchDtlType(listType);
        Map<String, Object> info = this.getAssetListForPage(methodParam, asParam); // 数据权限判断
        return RegexUtil.optObjToListOrExp(RegexUtil.optMapOrExpNullInfo(info, LangConstant.TITLE_ASSET_I).get("rows"), LangConstant.TITLE_ASSET_I); // 设备信息不存在
    }
//
//    /**
//     * 根据设备型号设置设备备件
//     *
//     * @param schemaName   schemaName
//     * @param assetModelId 设备型号
//     * @param asset_code      设备编码
//     * @param assetId      设备主键
//     * @param account      当前用户
//     */
//    private void doSetAssetBomByModel(String schemaName, String assetModelId, String asset_code, String assetId, String account) {
//        // 设备型号关联备件集合
//        List<Map<String, Object>> bomList = assetModelMapper.query_model_bom(schemaName, assetModelId);
//        if (RegexUtil.optIsPresentList(bomList)) {
//            for (Map<String, Object> bomModel : bomList) {
//                float useDays = RegexUtil.optBigDecimalOrVal(bomModel.get("use_days"), "0").floatValue(); // 替换周期，或寿命
//                float maintenanceDays = RegexUtil.optBigDecimalOrVal(bomModel.get("maintenance_days"), "0").floatValue(); // 保养周期
//                assetInfoService.AddAssetBom(schemaName, asset_code, assetId, RegexUtil.optStrOrBlank(bomModel.get("bom_model")),
//                        RegexUtil.optStrOrBlank(bomModel.get("bom_code")), RegexUtil.optStrOrBlank(bomModel.get("material_code")),
//                        useDays, maintenanceDays, RegexUtil.optIntegerOrVal(bomModel.get("unit_type"), 2), account); // 1:小时；2：天
//            }
//        }
//    }
//

    /**
     * 设置设备其他字段信息
     *
     * @param methodParam  系统参数
     * @param dataFieldStr 字段信息
     * @param data         页面信息
     * @param dbData       数据库数据信息
     * @param logArr       日志信息
     * @param isEdit       是否编辑
     */
    private void doSetAssetOtherInfo(MethodParam methodParam, String dataFieldStr, Map<String, Object> data, Map<String, Object> dbData, JSONArray logArr, boolean isEdit) {
        JSONArray dataFields = JSONArray.fromObject(dataFieldStr);
        for (int i = 0; i < dataFields.size(); i++) {
            JSONObject field = dataFields.getJSONObject(i);
            String checkType = field.get("checkType").toString();
            String name = field.get("name").toString();
            String nameKey = RegexUtil.optStrOrVal(field.get("nameKey"), name);
            String dataType = "";
            if ("2".equals(checkType)) {
                data.put(name, RegexUtil.optBigDecimalOrNull(data.get(name), nameKey));
                dataType = "float@";
            } else if ("3".equals(checkType)) {
                data.put(name, RegexUtil.optBigDecimalOrVal(data.get(name), field.get("defaultValue").toString(), nameKey));
                dataType = "float@";
            } else if ("4".equals(checkType)) {
                data.put(name, RegexUtil.optIntegerOrNull(data.get(name), nameKey));
                dataType = "int@";
            } else if ("5".equals(checkType)) {
                data.put(name, RegexUtil.optIntegerOrVal(data.get(name), RegexUtil.optIntegerOrNull(field.get("defaultValue"), nameKey), nameKey));
                dataType = "int@";
            } else if ("6".equals(checkType)) {
                data.put(name, RegexUtil.optStrOrBlank(data.get(name)));
                dataType = "varchar@";
            }
//            } else if ("7".equals(checkType)) {
//                data.put(name, RegexUtil.optDateChangeOrNull(data.get(name), nameKey, Constants.DATA_FMT));
//            }
            if (isEdit) {
                String tmpDy = dataType + name;
                String newValue = RegexUtil.optStrOrBlank(data.get(name));
                String dbValue = RegexUtil.optStrOrBlank(dbData.get(name));
                RegexUtil.notEqualsOpt(newValue, dbValue).ifPresent(b -> {
                    data.put(tmpDy, name);
                    if (null != logArr) {
                        this.doSetAssetEditLog(methodParam, logArr, field, name, "nameKey", "selectKey", dbValue, newValue);
                    }
                });
            }
        }
    }


//    /**
//     * 设置设备物联字段信息
//     *
//     * @param schemaName schemaName
//     * @param assetId    设备主键
//     * @param data       页面信息
//     * @param dbData     数据库数据信息
//     * @param isEdit     是否编辑
//     */
//    private void doSetAssetIotInfo(String schemaName, String assetId, Map<String, Object> data, Map<String, Object> dbData, boolean isEdit) {
//        // 设置设备物联信息
//        String outCode = RegexUtil.optStrOrBlank(data.get("out_code"));
//        String assetIotStatus = RegexUtil.optStrOrBlank(data.get("asset_iot_status"));
//        if (!"".equals(outCode) && !"".equals(assetIotStatus)) {
//            Map<String, Object> aisMap = new HashMap<>();
//            aisMap.put("asset_id", assetId);
//            aisMap.put("iot_status", assetIotStatus);
//            aisMap.put("out_code", outCode);
//            aisMap.put("in_code", RegexUtil.optStrOrBlank(data.get("in_code")));
//            aisMap.put("use_add", RegexUtil.optIntegerOrNull(data.get("use_add")));
//            aisMap.put("tem_value1", RegexUtil.optStrOrVal(data.get("tem_value1"), "0"));
//            aisMap.put("tem_value2", RegexUtil.optStrOrVal(data.get("tem_value2"), "0"));
//            aisMap.put("tem_value3", RegexUtil.optStrOrVal(data.get("tem_value3"), "0"));
//            aisMap.put("tem_value4", RegexUtil.optStrOrVal(data.get("tem_value4"), "0"));
//            aisMap.put("tem_value5", RegexUtil.optStrOrVal(data.get("tem_value5"), "0"));
//            aisMap.put("tem_value9", RegexUtil.optStrOrVal(data.get("tem_value9"), "0"));
//            if (isEdit) {
//                assetIotSettingService.updateAssetIotSetting(schemaName, assetId, aisMap);
//            } else {
//                assetIotSettingService.addAssetIotSetting(schemaName, aisMap);
//            }
//        }
//    }
//

    /**
     * 获取查询设备信息的条件
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 拼接sql
     */
    private String getAssertWhereString(MethodParam methodParam, AssetSearchParam asParam) {
        String schemaName = methodParam.getSchemaName();
        StringBuffer whereString = new StringBuffer(" where t1.status > " + StatusConstant.STATUS_DELETEED);
        RegexUtil.optNotBlankStrOpt(asParam.getStatusSearch()).ifPresent(e -> whereString.append(" and t1.status in (").append(e).append(") "));
        RegexUtil.optNotBlankStrOpt(asParam.getId()).ifPresent(e -> whereString.append(" and t1.id = '").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(asParam.getIds()).ifPresent(e -> whereString.append(" and t1.id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        if (RegexUtil.booleanCheck(asParam.getReturnBootAssetOnly())) {
            whereString.append(" and (t1.parent_id is null or t1.parent_id='' or t1.parent_id='-1') ");
        }
        RegexUtil.optNotBlankStrOpt(asParam.getAssetTypesSearch()).ifPresent(e -> whereString.append(" AND t1.category_id IN ( " +
                " WITH RECURSIVE re (id, parent_id) AS (" +
                " SELECT t.id, t.parent_id FROM " + schemaName + "._sc_asset_category t where t.id IN (")
                .append(e).append(" ) UNION ALL " +
                        " SELECT t2.id, t2.parent_id FROM " + schemaName + "._sc_asset_category t2 , re e WHERE e.id = t2.parent_id" +
                        " ) select id from re ").append(" ) "));
        RegexUtil.optNotBlankStrOpt(asParam.getAssetModelSearch()).ifPresent(e -> whereString.append(" AND t1.asset_model_id IN (").append(e).append(") "));
        RegexUtil.optNotBlankStrOpt(asParam.getRunStatusSearch()).ifPresent(e -> whereString.append(" AND t1.running_status_id in (").append(e).append(") "));
        RegexUtil.optNotBlankStrOpt(asParam.getCustomerSearch()).ifPresent(e -> whereString.append(" AND t1.supplier_id in (").append(e).append(") "));
        RegexUtil.optNotBlankStrOpt(asParam.getLevelSearch()).ifPresent(e -> whereString.append(" AND t1.importment_level_id in (").append(e).append(") "));
        RegexUtil.optNotBlankStrOpt(asParam.getWarrantiesTypeSearch()).ifPresent(e -> whereString.append(" AND t1.guarantee_status_id in (").append(e).append(") "));
        RegexUtil.optNotBlankStrOpt(asParam.getStartDateSearch()).ifPresent(e -> whereString.append(" and t1.enable_time >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
        RegexUtil.optNotBlankStrOpt(asParam.getEndDateSearch()).ifPresent(e -> whereString.append(" and t1.enable_time <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
        RegexUtil.optNotBlankStrOpt(asParam.getStartCreateDateSearch()).ifPresent(e -> whereString.append(" and t1.create_time >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_TIME_FMT + "') "));
        RegexUtil.optNotBlankStrOpt(asParam.getEndCreateDateSearch()).ifPresent(e -> whereString.append(" and t1.create_time <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_TIME_FMT + "') "));

        // 拼接设备位置筛选条件
        RegexUtil.optNotNullArrayOpt(asParam.getPositionsSearch()).ifPresent(e -> {
            StringBuffer positionStr = new StringBuffer();
            for (String position : e) {
                RegexUtil.optNotBlankStrOpt(position).ifPresent(im -> positionStr.append(" or t1.position_code like '").append(im).append("%' "));
            }
            RegexUtil.optNotBlankStrOpt(positionStr).ifPresent(ps -> whereString.append(" AND ( ").append(ps.substring(4)).append(") "));
        });

        // 查询重复的设备
        if (RegexUtil.booleanCheck(asParam.getDuplicateAssetSearch())) {
            whereString.append(" and t1.asset_code in (select asset_code from ").append(schemaName).append("._sc_asset ").
                    append(" where status > " + StatusConstant.STATUS_DELETEED).append(" group by asset_code having count(asset_code) > 1) ");
        }
        // 只查监控设备
        RegexUtil.optEqualsOpt(asParam.getMonitorDataOnly(), "1").ifPresent(e -> whereString.append(" AND EXISTS (SELECT t10.asset_id FROM ")
                .append(schemaName).append("._sc_asset_iot_setting t10 WHERE t10.asset_id = t1.id AND t10.iot_status > 1) "));
        RegexUtil.optNotBlankStrLowerOpt(asParam.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(t1.asset_name) like ")
                .append(e).append(" or lower(t1.asset_code) like ").append(e).append(")"));

        return whereString.toString();
    }

    /**
     * 获得设备故障履历列表
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getFaultRecordList(MethodParam methodParam, AssetSearchParam asParam) {
        return assetMapper.findFaultRecordList(methodParam.getSchemaName(), asParam.getId(), asParam.getKeywordSearch());
    }

    /**
     * 获取设备下一步工作信息
     *
     * @param methodParam 系统参数
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getAssetWorkInfo(MethodParam methodParam) {
        return assetMapper.findAssetWorkInfo(methodParam.getSchemaName(), methodParam.getDataId());
    }

    /**
     * 获取设备附件文档信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getAssetFileList(MethodParam methodParam, AssetSearchParam asParam) {
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> asset = assetMapper.findAssetById(schemaName, methodParam.getDataId());
        return RegexUtil.optNotBlankStrOpt(asset.get("file_ids")).map(ids -> assetMapper.findAssetFileList(schemaName, ids, asParam.getKeywordSearch())).orElse(new ArrayList<>());
    }

    /**
     * 获取设备历史工作信息
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getAssetHistoryWorkInfo(MethodParam methodParam, AssetSearchParam asParam) {
        return assetMapper.findAssetHistoryWorkInfo(methodParam.getSchemaName(), methodParam.getDataId(), asParam.getKeywordSearch(), RegexUtil.optStrOrNull(asParam.getStartDateSearch()), RegexUtil.optStrOrNull(asParam.getEndDateSearch()));
    }

    /**
     * 设备
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     * @param isAdd       是否新增
     */
    @Override
    public void doModifyAssetFile(MethodParam methodParam, AssetSearchParam asParam, boolean isAdd) {
        String file_id = RegexUtil.optNotBlankStrOrExp(asParam.getFile_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAAA_Q}); // 文档未选择
        String assetId = methodParam.getDataId();
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> assetMap = assetMapper.findAssetById(schemaName, assetId);
        if (isAdd || RegexUtil.optIsPresentStr(assetMap.get("file_ids"))) {
            Map<String, Object> asset = new HashMap<>();
            asset.put("id", assetId);
            asset.put("schemaName", schemaName);
            asset.put("fileIdsChange", 1);
            asset.put("file_ids", isAdd ? RegexUtil.optNotBlankStrOpt(assetMap.get("file_ids")).map(s -> s.concat(",").concat(file_id)).orElse(file_id) :
                    RegexUtil.optNotBlankStrOpt(assetMap.get("file_ids")).map(s -> DataChangeUtil.removeStrForStrings(s, file_id)).orElse(null));
            assetMapper.updateAsset(asset);
        }
        List<String> file_original_names = filesService.getFileNamesByIds(methodParam.getSchemaName(), asParam.getFile_id());
        if (RegexUtil.optNotNull(asParam.getFile_type_id()).isPresent()) {
            filesService.updateFileType(methodParam.getSchemaName(), Integer.valueOf(file_id), asParam.getFile_type_id());
        }
        if (isAdd) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("添加了附件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TAB_DOC_A, String.join(",", file_original_names)}));
        } else {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("删除了附件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_DOC_A, String.join(",", file_original_names)}));
        }
    }

    /**
     * 根据设备id获取设备详情
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 设备详情
     */
    @Override
    public Map<String, Object> getAssetInfo(MethodParam methodParam, String id) {
        return assetMapper.findAssetById(methodParam.getSchemaName(), id);
    }

    /**
     * 批量修改设备信息
     *
     * @param methodParam 入参
     * @param assetList   入参
     */
    @Override
    public void updateAssetBatch(MethodParam methodParam, List<Map<String, Object>> assetList) {
        assetMapper.updateAssetBatch(methodParam.getSchemaName(), assetList);
    }

    /**
     * 设备厂商新增
     *
     * @param methodParam
     * @param asParam
     * @param isAdd
     */
    @Override
    public void doModifyAssetFacilities(MethodParam methodParam, AssetSearchParam asParam, boolean isAdd) {
        String org_id = RegexUtil.optNotBlankStrOrExp(asParam.getOrg_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_B_O}); // 厂商未选择
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String schemaName = methodParam.getSchemaName();
        if (isAdd) {
            String[] org_ids = org_id.split(",");
            assetMapper.insertAssetOrg(schemaName, assetId, org_ids);
            for (String orgId : org_ids) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("新增了设备厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_B_O, assetMapper.findFacilitiesTitleById(schemaName, Integer.valueOf(orgId))}));
            }
        }
    }

    /**
     * 获得设备厂商列表
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    @Override
    public List<Map<String, Object>> getAssetFacilityList(MethodParam methodParam, AssetSearchParam asParam) {
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String schemaName = methodParam.getSchemaName();

        return RegexUtil.optNotNullListOrNew(findAssetFacilityList(schemaName, assetId, asParam.getKeywordSearch(), methodParam.getCompanyId(), methodParam.getUserLang(), asParam.getKeywordSearch()));
    }

    /**
     * 设备厂商删除
     *
     * @param methodParam
     * @param asParam
     */
    @Override
    public void cutAssetFacility(MethodParam methodParam, AssetSearchParam asParam) {
        String schemaName = methodParam.getSchemaName();
        String org_id = RegexUtil.optNotBlankStrOrExp(asParam.getOrg_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_B_O}); // 厂商未选择
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        assetMapper.deleteAssetFacility(schemaName, assetId, org_id);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("删除了设备厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_B_O, assetMapper.findFacilitiesTitleById(schemaName, Integer.valueOf(org_id))}));
    }

    /**
     * 获得设备关联人员列表
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    @Override
    public List<Map<String, Object>> getAssetDutyManList(MethodParam methodParam, AssetSearchParam asParam) {
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String schemaName = methodParam.getSchemaName();
        return RegexUtil.optNotNullListOrNew(assetMapper.findAssetDutyManList(schemaName, assetId, asParam.getKeywordSearch(), methodParam.getUserId()));

    }

    /**
     * 设备关联人员新增
     *
     * @param methodParam
     * @param asParam
     */
    @Override
    public void newAssetDutyMan(MethodParam methodParam, AssetSearchParam asParam) {
        String user_id = RegexUtil.optNotBlankStrOrExp(asParam.getUser_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAW_K}); // 维保人未选择
        String duty_id = RegexUtil.optNotBlankStrOrExp(asParam.getDuty_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_KU}); // 职责未选择
        String asset_id = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> data = new HashMap<>();
        data.put("schema_name", schemaName);
        data.put("user_id", user_id);
        data.put("duty_id", duty_id);
        data.put("asset_id", asset_id);
        data.put("is_use", "1");
        data.put("create_user_id", methodParam.getUserId());
        data.put("create_time", SenscloudUtil.getNowTime());
        data.put("remark", asParam.getRemark());
        assetMapper.addAssetDutyMan(data);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, asset_id, LangUtil.doSetLogArray("新增了设备关联人员", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AH_R, user_id}));
    }

    /**
     * 设备关联人员删除
     *
     * @param methodParam
     * @param asParam
     */
    @Override
    public void cutAssetDutyMan(MethodParam methodParam, AssetSearchParam asParam) {
        String schemaName = methodParam.getSchemaName();
        String duty_man_id = RegexUtil.optNotBlankStrOrExp(asParam.getDuty_man_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AH_R}); // 关联人员未选择
        String asset_id = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        assetMapper.deleteAssetDutyMan(schemaName, asset_id, duty_man_id);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, asset_id, LangUtil.doSetLogArray("删除了设备关联人员", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AH_R, duty_man_id}));

    }

    /**
     * 获得设备的备件列表
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    @Override
    public List<Map<String, Object>> getAssetBomList(MethodParam methodParam, AssetSearchParam asParam) {
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String schemaName = methodParam.getSchemaName();
        return RegexUtil.optNotNullListOrNew(assetMapper.findAssetBomList(schemaName, assetId, asParam.getKeywordSearch(), methodParam.getUserId()));
    }

    /**
     * 设备关联备件新增
     *
     * @param methodParam
     * @param asParam
     */
    @Override
    public void newAssetBom(MethodParam methodParam, AssetSearchParam asParam) {
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String bom_ids = RegexUtil.optNotBlankStrOrExp(asParam.getBom_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAAAY}); // 备件未选择
        String use_count = RegexUtil.optStrOrVal(asParam.getUse_count(), "0");
        String schemaName = methodParam.getSchemaName();
        String[] bom_id_arr = bom_ids.split(",");
        Map<String, Object> map = new HashMap<>();
        map.put("schema_name", methodParam.getSchemaName());
        map.put("create_user_id", methodParam.getUserId());
        map.put("create_time", SenscloudUtil.getNowTime());
        map.put("asset_id", assetId);
        map.put("use_count", use_count);
        map.put("bom_code", null);


        for (String bom_id : bom_id_arr) {
            if (RegexUtil.optIsPresentStr(bom_id)) {
                map.put("bom_id", bom_id);
                List<Map<String, Object>> bomCodeList = bomStockListMapper.findBomCodeByBomIdWithStockPermission(schemaName, bom_id, methodParam.getUserId());
                if (RegexUtil.optIsPresentList(bomCodeList)) {
                    for (Map<String, Object> data : bomCodeList) {
                        String bom_code = RegexUtil.optStrOrNull(data.get("bom_code"));
                        map.put("bom_code", bom_code);
                        assetMapper.insertAssetBom(map);
                        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("新增了设备关联备件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AAAAY, bom_code}));

                    }
                } else {
                    assetMapper.insertAssetBom(map);
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("新增了设备关联备件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AAAAY, bom_id}));

                }
            }
        }

    }

    /**
     * 设备关联备件删除
     *
     * @param methodParam
     * @param asParam
     */
    @Override
    public void removeAssetBom(MethodParam methodParam, AssetSearchParam asParam) {
        String schemaName = methodParam.getSchemaName();
        String asset_bom_id = RegexUtil.optNotBlankStrOrExp(asParam.getAsset_bom_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAAAY}); // 关联人员未选择
        String asset_id = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        assetMapper.deleteAssetBom(schemaName, asset_id, asset_bom_id);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, asset_id, LangUtil.doSetLogArray("删除了设备关联备件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAAAY, asset_bom_id}));

    }

    /**
     * 获得设备的备件使用记录列表
     *
     * @param methodParam
     * @param bom_id
     * @return
     */
    @Override
    public Map<String, Object> getBomUsageRecordList(MethodParam methodParam, String bom_id) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        Map<String, Object> result = new HashMap<>();
        int total = assetMapper.findCountBomUsageRecordList(methodParam.getSchemaName(), bom_id, methodParam.getUserId());
        List<Map<String, Object>> list = null;
        if (total > 0) {
            list = assetMapper.findBomUsageRecordList(methodParam.getSchemaName(), bom_id, methodParam.getUserId(), pagination);
        }
        result.put("total", total);
        result.put("rows", RegexUtil.optNotNullListOrNew(list));
        return result;
    }

    /**
     * 获得备件导入记录列表
     *
     * @param methodParam
     * @param bom_id
     * @return
     */
    @Override
    public Map<String, Object> getBomImportRecordList(MethodParam methodParam, String bom_id) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        Map<String, Object> result = new HashMap<>();
        int total = assetMapper.findCountBomImportRecordList(methodParam.getSchemaName(), bom_id, methodParam.getUserId());
        List<Map<String, Object>> list = null;
        if (total > 0) {
            list = assetMapper.findBomImportRecordList(methodParam.getSchemaName(), bom_id, methodParam.getUserId(), pagination);
        }
        result.put("total", total);
        result.put("rows", RegexUtil.optNotNullListOrNew(list));
        return result;
    }

    /**
     * 批量新增设备组织
     *
     * @param methodParam
     * @param addAssetList
     */
    @Override
    public void addAssetOrgBatch(MethodParam methodParam, List<Map<String, Object>> addAssetList) {
        List<Map<String, Object>> list = new ArrayList<>();
        String[] array = new String[]{"manufacturer_id", "supplier_id"};
        String schemaName = methodParam.getSchemaName();
        for (Map<String, Object> map : addAssetList) {
            RegexUtil.optNotBlankStrOpt(map.get("id")).ifPresent(ai -> {
                assetMapper.deleteAssetOrg(schemaName, ai);
                for (String n : array) {
                    RegexUtil.optNotBlankStrOpt(map.get(n)).filter(s -> !RegexUtil.optEquals(s, "0")).map(s -> new HashMap<String, Object>()).ifPresent(data -> {
                        data.put("asset_id", ai);
                        data.put("org_id", map.get(n));
                        list.add(data);
                    });
                }
            });
        }
        RegexUtil.optNotNullList(list).ifPresent(l -> assetMapper.insertAssetOrgBatch(schemaName, l));
    }

    /**
     * 获取运行的设备运行状态的id
     *
     * @param schemaName
     * @return
     */
    @Override
    public String searchAssetRunningStatusForRun(String schemaName) {
        return RegexUtil.optStrOrNull(assetMapper.findAssetRunningStatusForRun(schemaName));
    }

    /**
     * 根据设备id或者编码查询设备信息
     *
     * @param methodParam
     * @param id_or_code
     * @return
     */
    @Override
    public Map<String, Object> getAssetByIdOrCode(MethodParam methodParam, String id_or_code) {
        return assetMapper.findAssetByIdOrCode(methodParam.getSchemaName(), id_or_code);
    }

    /**
     * 设置设备编辑日志
     *
     * @param methodParam 系统参数
     * @param logArr      日志信息
     * @param jsonObject  字段信息
     * @param k           字段名称
     * @param fieldName   字段名称定义名
     * @param fieldSource 字段下拉框定义名
     * @param dbValue     数据库值
     * @param newValue    页面值
     */
    private void doSetAssetEditLog(MethodParam methodParam, JSONArray logArr, JSONObject jsonObject, String k, String fieldName, String fieldSource, String dbValue, String newValue) {
        String nameKey = RegexUtil.optStrAndValOrBlank(jsonObject.get(fieldName), k);
        String log = RegexUtil.optNotBlankStrOpt(jsonObject.get(fieldSource)).map(sk -> {
            String dbDesc = "".equals(dbValue) ? "" : selectOptionService.getSelectOptionTextByCode(methodParam, sk, dbValue);
            String newDesc = "".equals(newValue) ? "" : selectOptionService.getSelectOptionTextByCode(methodParam, sk, newValue);
            return LangUtil.doSetLog("设备属性更新", LangConstant.LOG_A, new String[]{nameKey, dbDesc, newDesc});
        }).orElseGet(() -> LangUtil.doSetLog("设备属性更新", LangConstant.LOG_A, new String[]{nameKey, dbValue, newValue}));
        logArr.add(log);
    }

    /**
     * 子设备新增
     *
     * @param methodParam 系统变量
     * @param asParam     请求参数
     */
    @Override
    public void newAssetParent(MethodParam methodParam, AssetSearchParam asParam) {
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String schemaName = methodParam.getSchemaName();
        String[] idStr = RegexUtil.optStrToArrayNotSelf(asParam.getSelect_id(), assetId, LangConstant.TITLE_ASSET_AC_W);
        assetMapper.insertAssetParent(schemaName, assetId, idStr);
        String strAssetName = RegexUtil.optNotBlankStrOpt(assetMapper.selectAddAssetName(schemaName, idStr)).map(s -> "【".concat(s).concat("】")).orElse("");
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("绑定了子设备", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_AL, LangConstant.TITLE_ASSET_AC_W, strAssetName}));

    }

    /**
     * 获取指定设备类型
     *
     * @param methodParam
     * @param asParam
     * @return
     */
    @Override
    public String getAssetTypeIds(MethodParam methodParam, AssetSearchParam asParam) {
        String asssetTypeTag = RegexUtil.optNotBlankStrOrExp(asParam.getAsssetTypeTag(), LangConstant.MSG_B, new String[]{LangConstant.TEXT_BM}); // 设备状态参数为空
        List<Map<String, Object>> assetTypeList = selectOptionService.getSelectOptionList(methodParam, "asset_type_all", null);
        List<Map<String, Object>> specialAssetTypeList = selectOptionService.getSelectOptionList(methodParam, "special_asset_type", null);
        Set<String> set = new HashSet();
        specialAssetTypeList.forEach(o -> {
            if (o.get("value").equals("flowmeter")) {
                set.addAll(Arrays.asList(((String) o.get("text")).split(",")));
            }
        });
        StringBuilder sb = new StringBuilder();
        assetTypeList.forEach(o -> {
            if (set.contains(o.get("relation"))) {
                if (RegexUtil.optIsPresentStr(sb.toString()))
                    sb.append(",");
                sb.append(o.get("value"));

            }
        });
        return sb.toString();
    }

    /**
     * 子设备删除
     *
     * @param methodParam 系统变量
     * @param asParam     请求参数
     */
    @Override
    public void removeAssetParent(MethodParam methodParam, AssetSearchParam asParam) {
        String assetId = RegexUtil.optNotBlankStrOrExp(asParam.getId(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I}); // 设备未选择
        String idStr = RegexUtil.optNotBlankStrOrExp(asParam.getSelect_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_AC_W});// 子设备未选择
        String schemaName = methodParam.getSchemaName();
        assetMapper.deleteAssetParent(schemaName, idStr);
        String strAssetName = RegexUtil.optNotBlankStrOpt(assetMapper.selectDeleteAssetName(schemaName, idStr)).map(s -> "【".concat(s).concat("】")).orElse("");
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetId, LangUtil.doSetLogArray("解绑了子设备", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_BI, LangConstant.TITLE_ASSET_AC_W, strAssetName}));
    }

    /**
     * 获得子设备信息列表
     *
     * @param methodParam 系统参数
     * @param asParam     页面参数
     * @return 子设备信息列表
     */
    @Override
    public List<Map<String, Object>> getAssetParentList(MethodParam methodParam, AssetSearchParam asParam) {
        return RegexUtil.optNotBlankStrOpt(asParam.getId()).map(i -> assetMapper.findAssetParentList(methodParam.getSchemaName(), i, asParam.getKeywordSearch())).orElseGet(ArrayList::new);
    }

    public List<Map<String, Object>> findAssetFacilityList(String schemaName, String dataId, String keywordSearch, Long companyId, String langKey, String condition) {
        List<Map<String, Object>> assetFacilityLis = assetMapper.findAssetFacilityList(schemaName, dataId, keywordSearch, condition);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentList(assetFacilityLis)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                assetFacilityLis.forEach(sd -> {
                    if (dd.containsKey(sd.get("org_type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("org_type_name"))).get(langKey));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("org_type_name", value);
                        }
                    }
                });
            }
        }
        return assetFacilityLis;
    }
}