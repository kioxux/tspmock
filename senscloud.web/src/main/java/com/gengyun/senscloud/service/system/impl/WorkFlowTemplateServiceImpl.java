package com.gengyun.senscloud.service.system.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.SelectOptionsMapper;
import com.gengyun.senscloud.mapper.WorkFlowTemplateMapper;
import com.gengyun.senscloud.mapper.WorkTemplateMapper;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.service.BussinessConfigService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CopyCallbackService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.service.system.WorkTemplateService;
import com.gengyun.senscloud.util.*;
import io.swagger.annotations.Api;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "流程配置")
@Service
public class WorkFlowTemplateServiceImpl implements WorkFlowTemplateService {

    @Resource
    WorkFlowTemplateMapper workFlowTemplateMapper;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    BussinessConfigService bussinessConfigService;
    @Resource
    WorkTemplateMapper workTemplateMapper;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    WorkTemplateService workTemplateService;
    @Resource
    CompanyService companyService;
    @Resource
    CopyCallbackServiceImpl callbackService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getWorkFlowTemplateListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[13]);
    }

    /**
     * 通过pref_id获取流程end节点的全部信息
     *
     * @param methodParam
     * @param pref_id
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowEndNodeTemplateInfo(MethodParam methodParam, String pref_id) {
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> nodeInfo = this.getBaseNodeInfoByNodeTypeAndPrefId(schemaName, pref_id, Constants.WORK_FLOW_NODE_TYPE_END);
        return getNodeColumnsInfo(schemaName, nodeInfo);
    }

    @Override
    public Map<String, Object> getWorkFlowStartNodeTemplateInfo(MethodParam methodParam, String pref_id) {
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> nodeInfo = this.getBaseNodeInfoByNodeTypeAndPrefId(schemaName, pref_id, Constants.WORK_FLOW_NODE_TYPE_START);
        return getNodeColumnsInfo(schemaName, nodeInfo);
    }

    /**
     * 根据流程、节点信息获取全部详细信息
     *
     * @param methodParam
     * @param pref_id
     * @param node_id
     * @param page_type
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowNodeTemplateInfo(MethodParam methodParam, String pref_id, String node_id, String page_type) {
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> nodeInfo = this.getNodeInfo(schemaName, pref_id, node_id, page_type);
        return getNodeColumnsInfo(schemaName, nodeInfo);
    }

    /**
     * 根据流程、节点信息获取全部详细信息
     *
     * @param methodParam      请求参数
     * @param workTemplateCode 工单模板编号
     * @return
     */
    @Override
    public Map<String, Object> getSubPageWorkFlowNodeTemplateInfo(MethodParam methodParam, String workTemplateCode) {
        Map<String, Object> workTemplateDetail = workTemplateService.getWorkTemplateDetailByCode(methodParam, workTemplateCode);
        List<Map<String, Object>> columnList = workFlowTemplateMapper.findColumnListByNodeId(methodParam.getSchemaName(), workTemplateCode);
        return handleColumnList(methodParam.getSchemaName(), workTemplateDetail, columnList);
    }

    /**
     * 获取流程模板基本信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowTemplateInfo(MethodParam methodParam, WorkFlowTemplateSearchParam bParam) {
//        String node_type = bParam.getNode_type();
//        String page_type = bParam.getPage_type();
//        String page_type = RegexUtil.optStrOrExpNotNull(bParam.getPage_type(), LangConstant.TITLE_ABC);
        Map<String, Object> info = new HashMap<>();
//        if (Constants.WORK_FLOW_PAGE_TYPE_BASE.equals(page_type)) {//流程的基本详情
        Map<String, Object> base = this.getNodeInfo(methodParam.getSchemaName(), bParam.getPref_id(), bParam.getNode_id(), Constants.WORK_FLOW_PAGE_TYPE_BASE);
        info.put("base", base);
        if (RegexUtil.optIsPresentMap(base) && RegexUtil.optIsPresentStr(base.get("node_type"))) {
            if (!Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(base.get("node_type").toString())) {
                info.put("handle", base);
            }
            if (Constants.WORK_FLOW_NODE_TYPE_SUB_NODE.equals(base.get("node_type").toString())) {
                Map<String, Object> sub = this.getNodeInfo(methodParam.getSchemaName(), bParam.getPref_id(), bParam.getNode_id(), Constants.WORK_FLOW_PAGE_TYPE_DETAIL);
                info.put("sub", sub);
            }
        }

//        if(RegexUtil.optIsPresentMap(base)){
//            info.put("base", base);
//            if (RegexUtil.optIsPresentStr(base.get("node_type"))) {
//                if (!Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(base.get("node_type").toString())) {
//                    info.put("handle", base);
//                }
//                if (Constants.WORK_FLOW_NODE_TYPE_SUB_NODE.equals(base.get("node_type").toString())) {
//                    Map<String, Object> sub = this.getNodeInfo(methodParam.getSchemaName(), bParam.getPref_id(), bParam.getNode_id(), Constants.WORK_FLOW_PAGE_TYPE_DETAIL);
//                    info.put("sub", sub);
//                }
//            }
//        }else {
//            info.put("base",new HashMap<>());
//            info.put("sub",new HashMap<>());
//            info.put("handle",new HashMap<>());
//        }
        return info;
    }

    /**
     * 新增/编辑流程模板基础信息
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkFlowTemplate(MethodParam methodParam, Map<String, Object> data, CopyCallbackService copyCallbackService) {
        String schemaName = methodParam.getSchemaName();
        data.put("schema_name", schemaName);
        Timestamp nowTime = SenscloudUtil.getNowTime();
        String node_type = RegexUtil.optStrOrExpNotNull(data.get("node_type"), LangConstant.TITLE_AN);
        String page_type = RegexUtil.optStrOrExpNotNull(data.get("page_type"), LangConstant.TITLE_ABC);
        if (!Constants.WORK_FLOW_PAGE_TYPE_BASE.equals(page_type) && !Constants.WORK_FLOW_PAGE_TYPE_DETAIL.equals(page_type)) {
            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ABC});// 页面类型不存在
        }
        boolean isAdd = false;
        JSONArray log = new JSONArray();
        String pref_id = RegexUtil.optStrOrExpNotNull(data.get("pref_id"), LangConstant.TITLE_AAI_J);// 流程id不能为空
        String node_id = RegexUtil.optStrOrExpNotNull(data.get("node_id"), LangConstant.TITLE_AAI_J);// 节点id不能为空
        callbackService.setResult(new HashMap<>());
        if (Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(node_type)) {
            Map<String, Object> base = this.getFlowInfo(methodParam.getSchemaName(), data.get("pref_id").toString());
            RegexUtil.optStrOrExpNotNull(data.get("flow_name"), LangConstant.TITLE_NAME_Z);// 流程名称不能为空
            RegexUtil.optStrOrExpNotNull(data.get("flow_show_name"), LangConstant.TITLE_NAME_C);// 显示名称不能为空
            int work_type_id = RegexUtil.optSelectOrExpParam(data.get("work_type_id"), LangConstant.TITLE_CATEGORY_C);// 工单类型不能为空
            RegexUtil.optNotNullOrExpNullInfo(bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id), LangConstant.TITLE_CATEGORY_C); // 工单类型不存在
            String work_template_code = RegexUtil.optStrOrExpNotNull(data.get("work_template_code"), LangConstant.TITLE_AC_V);// 工单模板不能为空
            RegexUtil.optNotNullOrExpNullInfo(workTemplateMapper.findWorkTemplateInfoByCode(schemaName, work_template_code), LangConstant.TITLE_AC_V); // 工单模板不存在
            String relation_type = RegexUtil.optStrOrExpNotNull(data.get("relation_type"), LangConstant.TITLE_AAT_A);// 适用对象不能为空
            RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "relationTypes", relation_type), LangConstant.TITLE_AAT_A); // 适用对象不存在
            RegexUtil.optStrOrExpNotNull(data.get("flow_code"), LangConstant.TITLE_AAI_K);// 流程key不能为空
            RegexUtil.trueExp(workFlowTemplateMapper.findCountWorkType(schemaName, work_type_id, pref_id) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_CATEGORY_X}); // 工单类型重复，请修改
            data.put("node_name", data.get("flow_name"));
            if (RegexUtil.optIsPresentMap(base)) {
                data.put("show_name", data.get("flow_show_name"));
                workFlowTemplateMapper.updateWorkFlowInfo(data);
                workFlowTemplateMapper.updateWorkFlowNode(data);
                if (RegexUtil.optNotNull(base).isPresent()) {
                    log = LangUtil.compareMap(data, base);
                }
                if (!work_template_code.equals(base.get("work_template_code").toString())) {
                    List<Map<String, Object>> cpWorkTemplateCloumList = workTemplateService.getWorkTemplateColumnListByWorkTemplateCode(methodParam, new WorkTemplateSearchParam(){{setWork_template_code(base.get("work_template_code").toString());}});
                    List<Map<String, Object>> orWorkTemplateCloumList = workTemplateService.getWorkTemplateColumnListByWorkTemplateCode(methodParam, new WorkTemplateSearchParam(){{setWork_template_code(work_template_code);}});
                    handleWorkTemplateCloums(methodParam, copyCallbackService, work_template_code, cpWorkTemplateCloumList, orWorkTemplateCloumList);
                    // workFlowTemplateMapper.deleteColumnLinkageConditionsByPrefIdForWorkTemplate(schemaName, pref_id);
                    // workFlowTemplateMapper.deleteColumnLinkagesByPrefIdForWorkTemplate(schemaName, pref_id);
                    // workFlowTemplateMapper.deleteColumnExtByPrefIdForWorkTemplate(schemaName, pref_id);
                    // workFlowTemplateMapper.deleteColumnsByPrefIdForWorkTemplate(schemaName, pref_id);
                }
            } else {
                isAdd = true;
                data.put("create_time", nowTime);
                data.put("create_user_id", methodParam.getUserId());
                data.put("node_id", data.get("pref_id"));
                data.put("show_name", data.get("flow_name"));
                workFlowTemplateMapper.insertWorkFlowInfo(data);
                workFlowTemplateMapper.insertWorkFlowNode(data);
            }
        } else {
//            if (Constants.WORK_FLOW_NODE_TYPE_START.equals(node_type) || Constants.WORK_FLOW_NODE_TYPE_END.equals(node_type)) {
            // 启动，结束节点若存在，更改字段的node_id值为新的
//                if (workFlowTemplateMapper.findCountNodeTypeByNodeIdAndPrefId(schemaName, node_id, pref_id, node_type) > 0) {
            Map<String, Object> dbStartInfo = this.getBaseNodeInfoByNodeTypeAndPrefId(schemaName, pref_id, node_type);
//                    if (RegexUtil.optIsPresentStr(dbStartInfo.get("word_flow_node_id"))) {
//                        workFlowTemplateMapper.updateWorkFlowColumnLinkage(schemaName, node_id, dbStartInfo.get("word_flow_node_id").toString());
//                        workFlowTemplateMapper.updateWorkFlowColumnExt(schemaName, node_id, dbStartInfo.get("word_flow_node_id").toString());
//                        workFlowTemplateMapper.updateWorkFlowColumn(schemaName, node_id, dbStartInfo.get("word_flow_node_id").toString());
//                        workFlowTemplateMapper.updateWorkFlowNodeById(schemaName, node_id, dbStartInfo.get("word_flow_node_id").toString());
//                    }
//                }
//            }

            Map<String, Object> info = this.getNodeInfo(methodParam.getSchemaName(), data.get("pref_id").toString(), data.get("node_id").toString(), data.get("page_type").toString());
            if (!RegexUtil.optIsPresentMap(info)) {
                throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ABE});//流程节点信息不存在
            }
            data.put("show_name", data.get("node_show_name"));
            if (RegexUtil.optIsPresentStr(info.get("word_flow_node_id"))) {
                workFlowTemplateMapper.updateWorkFlowNode(data);
                if (RegexUtil.optNotNull(info).isPresent()) {
                    log = LangUtil.compareMap(data, info);
                }
            } else {
                isAdd = true;
                data.put("create_time", nowTime);
                data.put("create_user_id", methodParam.getUserId());
                workFlowTemplateMapper.insertWorkFlowNode(data);
            }
        }
        if (isAdd) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, data.get("node_id").toString(), LangUtil.doSetLogArray("配置流程新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_AAI_I, LangConstant.TITLE_AAN_F}));
        } else {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, data.get("node_id").toString(), LangUtil.doSetLogArray("配置流程编辑", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAN_F, log.toString()}));
        }
    }

    private void handleWorkTemplateCloums(MethodParam methodParam, CopyCallbackService copyCallbackService, String work_template_code, List<Map<String, Object>> cpWorkTemplateCloumList, List<Map<String, Object>> orWorkTemplateCloumList) {
        List<Map<String, Object>> isNeedHandWorkTemplateCloumList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) cpWorkTemplateCloumList);
        cpWorkTemplateCloumList.forEach(c -> {
            for (Map<String, Object> item : orWorkTemplateCloumList) {
                if (Objects.equals(c.get("field_form_code"), item.get("field_form_code")) && Objects.equals(c.get("field_code"), item.get("field_code"))) {
                    isNeedHandWorkTemplateCloumList.remove(c);
                    break;
                }
            }
        });
        List<Map<String, Object>> isNeedAddWorkTemplateCloumList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) isNeedHandWorkTemplateCloumList);
        List<Map<String, Object>> isChooseWorkTemplateCloumList = new ArrayList<>();
        isNeedHandWorkTemplateCloumList.forEach(h -> {
            for (Map<String, Object> item : orWorkTemplateCloumList) {
                if (Objects.equals(h.get("field_form_code"), item.get("field_form_code")) || Objects.equals(h.get("field_code"), item.get("field_code"))) {
                    isNeedAddWorkTemplateCloumList.remove(h);
                    h.put("hd_work_template_code", work_template_code);
                    isChooseWorkTemplateCloumList.add(h);
                    break;
                }
            }
        });
        if (!isNeedAddWorkTemplateCloumList.isEmpty()) {
            String field_form_codes = StringUtils.join(isNeedAddWorkTemplateCloumList.stream().map(e -> e.get("field_form_code")).collect(Collectors.toList()),",");
            workTemplateService.newWorkTemplateColumn(methodParam, new HashMap<String, Object>(){{put("work_template_code", work_template_code);put("field_form_codes",field_form_codes);}});
        }
        if (!isChooseWorkTemplateCloumList.isEmpty()) {
            copyCallbackService.callBackResult(isChooseWorkTemplateCloumList);
        }
    }

    /**
     * 查询流程、节点字段信息列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkFlowTemplateColumnListByNodeIdAndNodeType(MethodParam methodParam, WorkFlowTemplateSearchParam param) {
        String schemaName = methodParam.getSchemaName();
        // 获取按条件拼接的sql
        String searchWord = this.getWorkFlowTemplateColumnWhereString(methodParam, param);
        searchWord += " order by wfnc.data_order asc,wfnc.id asc ";
        List<Map<String, Object>> workColumnList = null;
        workColumnList = findWorkFlowTemplateColumnList(schemaName, searchWord, param.getPref_id(), param.getNode_id(), param.getPage_type(), methodParam.getCompanyId(), methodParam.getUserLang(), RegexUtil.optStrOrNull(param.getKeywordSearch()));
        return workColumnList;
    }

    /**
     * 新增流程、节点模板字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void newWorkFlowTemplateColumn(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_NG}); // 字段不能为空
        String pref_id = RegexUtil.optStrOrExpNotNull(data.get("pref_id"), LangConstant.TITLE_AAI_J);// 工单模板不能为空
        String node_id = RegexUtil.optStrOrExpNotNull(data.get("node_id"), LangConstant.TITLE_AK);// 工单模板不能为空
//        String node_type = RegexUtil.optStrOrExpNotNull(data.get("node_type"), LangConstant.TITLE_CATEGORY_M);// 存储类型不能为空
        String page_type = RegexUtil.optStrOrExpNotNull(data.get("page_type"), LangConstant.TITLE_ABC);// 存储类型不能为空
        String field_form_codes = RegexUtil.optStrOrExpNotNull(data.get("field_form_codes"), LangConstant.TITLE_NG);// 字段不能为空
        Map<String, Object> dbNodeInfo = this.getNodeInfo(schemaName, pref_id, node_id, page_type);
        if (!RegexUtil.optIsPresentMap(dbNodeInfo)) {
            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ABE});//流程节点信息不存在
        } else {
            if (!RegexUtil.optIsPresentStr(dbNodeInfo.get("word_flow_node_id"))) {
                throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ABE});//流程节点信息不存在
            }
        }
        String work_template_code = dbNodeInfo.get("work_template_code").toString();
        String word_flow_node_id = dbNodeInfo.get("word_flow_node_id").toString();
        if (!RegexUtil.optIsPresentStr(word_flow_node_id)) {
            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AK});//节点id信息不存在
        }
//        Map<String, Object> nodeInfo = this.getNodeInfo(schemaName, pref_id, node_id, page_type);

        Integer dataOrder = workFlowTemplateMapper.findMaxDataOrderByNode(schemaName, pref_id, node_id, page_type); // 获取流程字段最大排序
        if (dataOrder == null) {
            dataOrder = 0;
        }
        data.put("work_template_code", work_template_code);
        String[] fieldFormCodes = field_form_codes.split(",");
        data.put("create_time", nowTime);
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        data.put("word_flow_node_id", word_flow_node_id);
        int result = 0;
        for (String fieldFormCode : fieldFormCodes) {
            dataOrder = dataOrder + 1;
            data.put("field_form_code", fieldFormCode);
            data.put("sort", dataOrder);
            result += workFlowTemplateMapper.insertWorkFlowColumn(data);
            workFlowTemplateMapper.insertWorkFlowColumnExt(data);
            List<Map<String, Object>> linkList = workFlowTemplateMapper.findWorkTemplateColumnLinkageAndConditionListByNodeIdAndFieldFormCode(methodParam.getSchemaName(), work_template_code, fieldFormCode);
            if (RegexUtil.optNotNull(linkList).isPresent()) {
                for (Map<String, Object> linkage : linkList) {
                    linkage.put("create_time", nowTime);
                    linkage.put("create_user_id", methodParam.getUserId());
                    linkage.put("schema_name", schemaName);
                    String condition = (String) linkage.get("condition");
                    com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(condition);
//                    JSONObject jsonObject = JSONObject.fromObject(condition);
                    linkage.put("condition", jsonObject);
                    linkage.put("node_id", node_id);
                    linkage.put("column_id", data.get("id"));
                    workTemplateMapper.insertWorkTemplateColumnLinkage(linkage);
                    workTemplateMapper.insertWorkTemplateColumnLinkageCondition(linkage);
                }
            }
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        for (String id : fieldFormCodes) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, node_id, LangUtil.doSetLogArray("新增流程配置字段！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_NG, id}));
        }
    }

    /**
     * 修改字段区块、显示、权限信息
     *
     * @param methodParam
     * @param data        type:类型 1、所属区块 2、是否显示 3、只读、写入权限
     */
    @Override
    public void modifyWorkFlowColumnUse(MethodParam methodParam, Map<String, Object> data) {
        Map<String, Object> dbColumnInfo = this.getWorkFlowColumnDetailById(methodParam.getSchemaName(), Integer.valueOf(data.get("id").toString()));
        data.put("schema_name", methodParam.getSchemaName());
        if (!RegexUtil.optIsPresentStr(data.get("field_section_type")) && !RegexUtil.optIsPresentStr(data.get("field_is_show")) && !RegexUtil.optIsPresentStr(data.get("field_right"))) {
            throw new SenscloudException(LangConstant.MSG_BI);
        }
        if (RegexUtil.optIsPresentStr(data.get("field_is_show")) && "hide".equals(data.get("field_is_show").toString())) {
            data.put("is_required", "2");
        }
        workTemplateMapper.updateWorkTemplateColumnUse(data);
//        workTemplateMapper.updateWorkTemplateColumn(data);
        if (RegexUtil.optNotNull(dbColumnInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbColumnInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, dbColumnInfo.get("node_id").toString(), LangUtil.doSetLogArray("编辑累成配置字段使用", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_NG, log.toString()}));
        }
    }

    /**
     * 修改字段上移、下移 信息
     *
     * @param methodParam
     * @param data        4、上移 5、下移
     */
    @Override
    public void modifyWorkFlowOrder(MethodParam methodParam, Map<String, Object> data) {
        String type = data.get("type").toString();
        data.put("schema_name", methodParam.getSchemaName());
        Map<String, Object> info = workFlowTemplateMapper.findColumnInfoById(methodParam.getSchemaName(), Integer.valueOf(data.get("id").toString()));
        if ("4".equals(type)) {// 上移
            List<Map<String, Object>> upList = workFlowTemplateMapper.findColumnListForDescOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(), RegexUtil.optStrOrVal(info.get("word_flow_node_id"), null));
            if (RegexUtil.optIsPresentStr(info.get("data_order"))) {
                if (upList.size() > 0) {
                    if (RegexUtil.optEquals(info.get("data_order").toString(), upList.get(0).get("data_order").toString())) {
                        info.put("schema_name", methodParam.getSchemaName());
                        workFlowTemplateMapper.reduceOrderByData(info);
                        upList = workFlowTemplateMapper.findColumnListForDescOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(), RegexUtil.optStrOrVal(info.get("word_flow_node_id"), null));
                    }
//                    else {
                    upList.get(0).put("schema_name", methodParam.getSchemaName());
                    upList.get(0).put("down_order", info.get("data_order"));
                    data.put("top_order", upList.get(0).get("data_order"));
                    workFlowTemplateMapper.upOrder(data);
                    workFlowTemplateMapper.downOrder(upList.get(0));
//                    }
                }
            } else {
                //该字段无排序---后续考虑
            }
        } else {// 下移
            List<Map<String, Object>> downList = workFlowTemplateMapper.findColumnListForAscOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(), RegexUtil.optStrOrVal(info.get("word_flow_node_id"), null));
            if (RegexUtil.optIsPresentStr(info.get("data_order"))) {
                if (downList.size() > 0) {
                    if (RegexUtil.optEquals(info.get("data_order").toString(), downList.get(0).get("data_order").toString())) {
                        info.put("schema_name", methodParam.getSchemaName());
                        workFlowTemplateMapper.plusOrderByData(info);
                        downList = workFlowTemplateMapper.findColumnListForAscOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(), RegexUtil.optStrOrVal(info.get("word_flow_node_id"), null));
                    }
//                    else {
                    downList.get(0).put("schema_name", methodParam.getSchemaName());
                    downList.get(0).put("top_order", info.get("data_order"));
                    data.put("down_order", downList.get(0).get("data_order"));
                    workFlowTemplateMapper.downOrder(data);
                    workFlowTemplateMapper.upOrder(downList.get(0));
//                    }
                }
            } else {
                //该字段无排序---后续考虑
            }
        }
    }

    /**
     * 编辑流程配置字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkFlowTemplateColumn(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        String schemaName = methodParam.getSchemaName();
        Integer id = Integer.valueOf(data.get("id").toString());
        String field_form_code = data.get("field_form_code").toString();
        Map<String, Object> dbNodeInfo = workFlowTemplateMapper.findNodeInfoByColumnId(schemaName, id);
        String node_id = dbNodeInfo.get("node_id").toString();
        String pref_id = dbNodeInfo.get("pref_id").toString();
        String node_type = dbNodeInfo.get("node_type").toString();
        JSONArray logArr = new JSONArray();
//        Map<String, Object> dbWorkColumnInfo = this.getWorkFlowColumnDetailById(methodParam.getSchemaName(), id);
        WorkTemplateSearchParam param = new WorkTemplateSearchParam();
        param.setId(id);
        Map<String, Object> dbWorkColumnInfo = this.getWorkFlowColumnInfoById(methodParam, param);
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optStrOrExpNotNull(data.get("field_name"), LangConstant.TITLE_NAME_C);   // 显示名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("field_code"), LangConstant.TITLE_NAME_AB_R); // 字段名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("field_right"), LangConstant.TITLE_B_C); // 操作权限不能为空
        String field_section_type = RegexUtil.optStrOrExpNotNull(data.get("field_section_type"), LangConstant.TITLE_QE);// 所在区块不能为空
        String field_view_type = RegexUtil.optStrOrExpNotNull(data.get("field_view_type"), LangConstant.TITLE_QF);// 控件类型不能为空
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "block", field_section_type), LangConstant.TITLE_QE); // 所在区块不存在
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "work_column", field_view_type), LangConstant.TITLE_QF); // 控件类型不存在
        if (RegexUtil.optEquals(dbWorkColumnInfo.get("field_is_show"), "hide") && RegexUtil.optEquals(data.get("is_required"), "1")) {
            throw new SenscloudException(LangConstant.MSG_C);//隐藏字段不可设为必填
        }

        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            String extList1 = data.get("extList").toString();
            JSONArray extList = JSONArray.fromObject(extList1);
            for (int x = 0; x < extList.size(); x++) {
                JSONObject extObject = extList.getJSONObject(x);
                extObject.remove("create_time");
            }
            List<WorkColumnExAdd> list = JSONArray.toList(extList, WorkColumnExAdd.class);
            if (list.size() > 0) {
                for (WorkColumnExAdd workColumnExAdd : list) {
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_name(), LangConstant.TITLE_AAT_P);   // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_write_type(), LangConstant.TITLE_AAT_P); // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_value(), workColumnExAdd.getField_name()); // 特殊属性的名称值不能为空
                    workColumnExAdd.setField_form_code(field_form_code);
                    workColumnExAdd.setCreate_time(nowTime);
                    workColumnExAdd.setCreate_user_id(methodParam.getUserId());
                    workColumnExAdd.setNode_id(node_id);
                    workColumnExAdd.setColumn_id(id);
//                    if (Constants.COLUMN_EXT_FIELD_VALUE.equals(workColumnExAdd.getField_name())) {
//                        data.put("field_value", workColumnExAdd.getField_value());
//                    }
                }
                data.put("extList", list);
            } else {
                data.remove("extList");
            }
        }
        if (RegexUtil.optNotNull(data.get("linkList")).isPresent()) {
            String linkList = data.get("linkList").toString();
            com.alibaba.fastjson.JSONArray linkLists = com.alibaba.fastjson.JSONArray.parseArray(linkList);
//            JSONArray linkLists = JSONArray.fromObject(linkList);
            if (linkLists.size() > 0) {
                for (int x = 0; x < linkLists.size(); x++) {
                    com.alibaba.fastjson.JSONObject linkObject = linkLists.getJSONObject(x);
                    RegexUtil.optStrOrExpNotNull(linkObject.get("linkage_type"), LangConstant.TITLE_AG);   // 来源方式为空
                    linkObject.put("create_time", nowTime.toString());
                    linkObject.put("create_user_id", methodParam.getUserId());
                    linkObject.put("field_form_code", field_form_code);
                    linkObject.put("node_id", node_id);
                    linkObject.put("schema_name", schemaName);
                    linkObject.put("column_id", id);
                    if (linkObject.containsKey("condition")) {
                    } else {
                        linkObject.put("conditions", "{}");
                    }
                }
                data.put("linkList", linkLists);
            }
        }
        data.put("schema_name", schemaName);
//        data.put("field_code",dbWorkColumnInfo.get("field_code"));
        int result = workTemplateMapper.updateWorkTemplateColumn(data);
        workFlowTemplateMapper.deleteFlowColumnExtByColumnId(schemaName, id);
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            List<WorkColumnExAdd> extList = (List<WorkColumnExAdd>) data.get("extList");
            workTemplateMapper.insertWorkTemplateColumnExBatch(schemaName, extList);
        }
        workFlowTemplateMapper.deleteFlowColumnLinkageConditionByColumnId(schemaName, id);
        workFlowTemplateMapper.deleteFlowColumnLinkageByColumnId(schemaName, id);
        if (RegexUtil.optNotNull(data.get("linkList")).isPresent()) {
            com.alibaba.fastjson.JSONArray linkList = (com.alibaba.fastjson.JSONArray) data.get("linkList");
            for (int i = 0; i < linkList.size(); i++) {
                com.alibaba.fastjson.JSONObject linkObject = linkList.getJSONObject(i);
                workTemplateMapper.insertWorkTemplateColumnLinkage(linkObject);
                // todo 条件字段未作处理
                workTemplateMapper.insertWorkTemplateColumnLinkageCondition(linkObject);
            }
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        if (RegexUtil.optNotNull(dbWorkColumnInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbWorkColumnInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, node_id, LangUtil.doSetLogArray("编辑了流程配置字段", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_NG, log.toString()}));
        }
    }

    /**
     * 编辑流程模板操作人信息
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkFlowTemplateHandle(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
//        String node_type = data.get("node_type").toString();
//        if (!Constants.WORK_FLOW_TYPE_FLOW.equals(node_type) && !Constants.WORK_FLOW_TYPE_NODE.equals(node_type) && !Constants.WORK_FLOW_TYPE_SUB_NODE.equals(node_type)) {
//            new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AK});// 流程节点不存在
//        }
        String pref_id = RegexUtil.optStrOrExpNotNull(data.get("pref_id"), LangConstant.TITLE_AAI_J);// 流程id不能为空
        String node_id = RegexUtil.optStrOrExpNotNull(data.get("node_id"), LangConstant.TITLE_AAI_J);// 流程id不能为空
//        String page_type = RegexUtil.optStrOrExpNotNull(data.get("page_type"), LangConstant.TITLE_ABC);// 流程id不能为空
        RegexUtil.optStrOrExpNotNull(data.get("deal_type_id"), LangConstant.TITLE_CATEGORY_AH);// 类型不能为空
        RegexUtil.optStrOrExpNotNull(data.get("deal_role_id"), LangConstant.TITLE_AM_Y);// 角色不能为空
        Map<String, Object> base = this.getNodeInfo(methodParam.getSchemaName(), pref_id, node_id, Constants.WORK_FLOW_PAGE_TYPE_BASE);
        if (!RegexUtil.optIsPresentStr(base.get("word_flow_node_id"))) {
            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AK});//节点id信息不存在
        }
        data.put("schema_name", schemaName);
        data.put("page_type", Constants.WORK_FLOW_PAGE_TYPE_BASE);
        workFlowTemplateMapper.updateWorkFlowNode(data);
        JSONArray logArr = new JSONArray();
        String dbDeal_type_id = RegexUtil.optStrOrBlank(base.get("deal_type_id"));
        String newDeal_type_id = RegexUtil.optStrOrBlank(data.get("deal_type_id"));
        if (!newDeal_type_id.equals(dbDeal_type_id)) {
            String log = LangUtil.doSetLog("属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_CATEGORY_AH, dbDeal_type_id, newDeal_type_id});
            logArr.add(log);
        }
        String dbDeal_role_id = RegexUtil.optStrOrBlank(base.get("deal_role_id"));
        String newDeal_role_id = RegexUtil.optStrOrBlank(data.get("deal_role_id"));
        if (!newDeal_role_id.equals(dbDeal_role_id)) {
            String log = LangUtil.doSetLog("属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_AM_Y, dbDeal_role_id, newDeal_role_id});
            logArr.add(log);
        }
        if (RegexUtil.optIsPresentList(logArr)) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, node_id, logArr.toString());
        }
    }

    /**
     * 删除流程模板字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void cutWorkFlowColumnById(MethodParam methodParam, WorkFlowTemplateColumnAdd data) {
        String schemaName = methodParam.getSchemaName();
        Integer id = data.getId();
        Map<String, Object> dbWorkColumnInfo = this.getWorkFlowColumnDetailById(methodParam.getSchemaName(), id);
        String word_flow_node_id = dbWorkColumnInfo.get("word_flow_node_id").toString();
        String field_form_code = dbWorkColumnInfo.get("field_form_code").toString();
        // todo 子页面关联的work_template_code也需要校验一下，暂查询的_sc_work_flow_node_column_ext表的field_value字段
        RegexUtil.trueExp(workFlowTemplateMapper.findColumnCountUseByWordFlowNodeId(schemaName, word_flow_node_id, field_form_code) > 0, LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AAI_I, LangConstant.TITLE_NG, LangConstant.TITLE_NG}); // 流程下还有字段，不能删除该字段
        workFlowTemplateMapper.deleteTemplateColumnById(schemaName, id);
        workFlowTemplateMapper.deleteTemplateColumnExtByColumnId(schemaName, id);
        workFlowTemplateMapper.deleteTemplateColumnLinkageConditionByColumnId(schemaName, id);
        workFlowTemplateMapper.deleteTemplateColumnLinkageByColumnId(schemaName, id);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, dbWorkColumnInfo.get("node_id").toString(), LangUtil.doSetLogArray("流程配置字段删除", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAN_F, LangConstant.TITLE_NG}));
    }

    /**
     * 查询新增字段查询工单模板字段列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkFlowColumnListForAdd(MethodParam methodParam, WorkFlowTemplateSearchParam param) {
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> flowInfo = this.getFlowInfo(schemaName, param.getPref_id());
        List<Map<String, Object>> workColumnList = null;
        if (RegexUtil.optIsPresentMap(flowInfo)) {
            // 获取按条件拼接的sql
            String searchWord = this.getWorkFlowTemplateColumnWhereString(methodParam, param);
            StringBuilder whereString = new StringBuilder();
            if (RegexUtil.optIsPresentStr(param.getNode_id()) && RegexUtil.optIsPresentStr(param.getPref_id()) && RegexUtil.optIsPresentStr(param.getPage_type())) {
                whereString.append(" AND not exists(select 1 FROM ").append(methodParam.getSchemaName()).append("._sc_work_flow_node n JOIN ").append(methodParam.getSchemaName()).append("._sc_work_flow_node_column c ON n.id=c.word_flow_node_id WHERE n.node_id = '").append(param.getNode_id()).append("' AND n.pref_id = '").append(param.getPref_id()).append("' AND n.page_type='").append(param.getPage_type()).append("' and wfnc.field_form_code = c.field_form_code)");
                searchWord += whereString.toString();
            }
            searchWord += " ORDER BY wfnc.field_form_code ";

            workColumnList = findWorkTemplateColumnList(schemaName, searchWord, flowInfo.get("work_template_code").toString(), methodParam.getCompanyId(), methodParam.getUserLang(), param.getKeywordSearch());
        } else {
            workColumnList = new ArrayList<>();
        }
        return workColumnList;
    }

    /**
     * 根据流程、节点类型获取克隆对应模板
     *
     * @param schemaName
     * @param flow_code  flow_code
     * @param node_id    新增节点的id
     * @param page_type  页面类型
     * @return
     */
    private Map<String, Object> getNodeInfoWithFlowCode(String schemaName, String flow_code, String node_id, String page_type) {
        return workFlowTemplateMapper.findNodeInfoWithFlowCode(schemaName, flow_code, node_id, page_type);

    }

    /**
     * 根据流程、节点类型获取克隆对应模板
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkFlowTemplateListForClone(MethodParam methodParam, WorkFlowTemplateSearchParam param) {
        String schemaName = methodParam.getSchemaName();
        String node_type = param.getNode_type();
        String node_type_condition = " ";
        if (Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(node_type)) {
            node_type_condition += " and wfn.node_type = '" + Constants.WORK_FLOW_NODE_TYPE_FLOW + "' ";
        }
//        String node_type_condition = " and wfn.node_type in ('" + Constants.WORK_FLOW_NODE_TYPE_FLOW + "','" + node_type + "')";
//        if (Constants.WORK_FLOW_TYPE_FLOW.equals(node_type)) {
//            node_type_condition += ")";
//        } else if (Constants.WORK_FLOW_TYPE_NODE.equals(node_type)) {
//            node_type_condition += ",'2') AND not exists (SELECT 1 FROM " + schemaName + "._sc_work_flow_node n WHERE n.pref_id=wfn.pref_id AND n.node_id = wfn.node_id AND n.node_type = '3')";
//        } else if (Constants.WORK_FLOW_TYPE_SUB_NODE.equals(node_type)) {
//            node_type_condition += ",'3')";
//        }
        return workFlowTemplateMapper.getWorkFlowTemplateListForClone(schemaName, node_type_condition);
    }

    /**
     * 克隆流程配置
     *
     * @param methodParam
     * @param bParam
     */
    @Override
    public void newWorkFlowNodeClone(MethodParam methodParam, WorkFlowTemplateAdd bParam, CopyCallbackService copyCallbackService) {
        String word_flow_node_id = bParam.getWord_flow_node_id();//模板配置id
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        String node_type = bParam.getNode_type();//需copy的流程配置类型
        String pref_id = bParam.getPref_id();//需copy的流程配置流程pref_id
        String node_id = bParam.getNode_id();//需copy的流程配置节点node_id
        String page_type = bParam.getPage_type();
        callbackService.setResult(new HashMap<>());
//        RegexUtil.optStrOrExpNotNull(process_type, LangConstant.TITLE_NAME_Z);// 过程不能为空
        if (Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(node_type)) {
            RegexUtil.optStrOrExpNotNull(bParam.getFlow_name(), LangConstant.TITLE_NAME_Z);// 流程名称不能为空
            RegexUtil.optStrOrExpNotNull(bParam.getFlow_code(), LangConstant.TITLE_AAI_K);// 流程key不能为空
        } else {
            RegexUtil.optStrOrExpNotNull(bParam.getNode_name(), LangConstant.TITLE_NAME_AB_B);//  名称不能为空
        }
        Map<String, Object> dbToDeleteNodeInfo = this.getNodeInfo(schemaName, pref_id, node_id, Constants.WORK_FLOW_PAGE_TYPE_BASE);
        if (RegexUtil.optIsPresentMap(dbToDeleteNodeInfo)) {//若需copy数据已存在，需删除后再copy
            if (Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(node_type)) {
                workFlowTemplateMapper.deleteWorkFlowByPrefId(schemaName, pref_id);
            }
            if (RegexUtil.optIsPresentStr(dbToDeleteNodeInfo.get("word_flow_node_id"))) {
                //根据查询出来的需删除数据的node表id删除数据
                this.deleteByWordFlowNodeId(schemaName, dbToDeleteNodeInfo.get("word_flow_node_id").toString());
            }
        }
        Map<String, Object> data = new HashMap<>();
        data.put("word_flow_node_id", word_flow_node_id);
        data.put("create_time", nowTime);
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        data.put("node_type", node_type);
        data.put("pref_id", pref_id);
        data.put("node_id", node_id);
        data.put("node_type", node_type);
        data.put("work_template_code", dbToDeleteNodeInfo.get("work_template_code"));
//        data.put("process_type", process_type);
        if (Constants.WORK_FLOW_NODE_TYPE_FLOW.equals(node_type)) {
            data.put("flow_code", bParam.getFlow_code());
            data.put("flow_name", bParam.getFlow_name());
            data.put("node_name", bParam.getFlow_name());
            workFlowTemplateMapper.insertWorkFlowClone(data);
        } else {
            data.put("node_name", bParam.getNode_name());
        }
        workFlowTemplateMapper.insertWorkFlowNodeClone(data);
        //copy数据
        this.cloneWorkFlowTemplate(methodParam, schemaName, nowTime, word_flow_node_id, node_id, data, copyCallbackService);

        //子节点时还需copy该子节点的节点数据
        if (Constants.WORK_FLOW_NODE_TYPE_SUB_NODE.equals(node_type)) {
            if (RegexUtil.optIsPresentMap(dbToDeleteNodeInfo) && RegexUtil.optIsPresentStr(dbToDeleteNodeInfo.get("word_flow_node_id"))) {
                // 需删除子数据的id
//                String delete_word_flow_node_id = dbToDeleteNodeInfo.get("word_flow_node_id").toString();
                String db_node_id = dbToDeleteNodeInfo.get("node_id").toString();
                String db_pref_id = dbToDeleteNodeInfo.get("pref_id").toString();
                // 需删除子节点数据的节点数据
                Map<String, Object> dbDeleteNodeInfo = this.getNodeInfo(schemaName, db_pref_id, db_node_id, Constants.WORK_FLOW_PAGE_TYPE_DETAIL);
//                Map<String, Object> dbDeleteNodeInfo = workFlowTemplateMapper.findWorkFlowNodeInfoBySub(schemaName, delete_word_flow_node_id);
                // 需删除子节点数据的节点数据
                if (RegexUtil.optIsPresentMap(dbDeleteNodeInfo) && RegexUtil.optIsPresentStr(dbDeleteNodeInfo.get("word_flow_node_id"))) {
                    String id = dbDeleteNodeInfo.get("word_flow_node_id").toString();
                    // 当数据库有数据时，先删除数据
                    this.deleteByWordFlowNodeId(schemaName, id);
                }
            }
            // 被copy子节点的节点数据
            Map<String, Object> dbDetailNodeInfo = workFlowTemplateMapper.findWorkFlowNodeInfoBySub(schemaName, word_flow_node_id);
            // 被copy子节点的节点id
            String id = dbDetailNodeInfo.get("id").toString();
            data.put("word_flow_node_id", id);
            data.put("page_type", "detail");
            workFlowTemplateMapper.insertWorkFlowNodeClone(data);
            this.cloneWorkFlowTemplate(methodParam, schemaName, nowTime, id, node_id, data, copyCallbackService);
        }
//        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, node_id, LangUtil.doSetLogArray("流程配置克隆", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AAN_F}));
    }

    /**
     * 通过node表id，删除node对应的字段所有信息
     *
     * @param schemaName
     * @param word_flow_node_id
     */
    private void deleteByWordFlowNodeId(String schemaName, String word_flow_node_id) {
        workFlowTemplateMapper.deleteWorkNodeById(schemaName, word_flow_node_id);
        workFlowTemplateMapper.deleteColumnsExtsByWordFlowNodeId(schemaName, word_flow_node_id);
        workFlowTemplateMapper.deleteColumnsLinkageConditionsByWordFlowNodeId(schemaName, word_flow_node_id);
        workFlowTemplateMapper.deleteColumnsLinkagesByWordFlowNodeId(schemaName, word_flow_node_id);
        workFlowTemplateMapper.deleteColumnsByWordFlowNodeId(schemaName, word_flow_node_id);
    }

    /**
     * 克隆新增字段及额外字段信息
     *
     * @param methodParam
     * @param schemaName
     * @param nowTime
     * @param word_flow_node_id 被copy数据的node表主键
     * @param node_id           新增节点的id
     * @param data              存储node表的数据
     */
    private void cloneWorkFlowTemplate(MethodParam methodParam, String schemaName, Timestamp nowTime, String word_flow_node_id, String node_id, Map<String, Object> data, CopyCallbackService copyCallbackService) {
        String work_template_code = RegexUtil.optStrOrExpNotExist(data.get("work_template_code"), LangConstant.TITLE_AAH_L);
        List<Map<String, Object>> columnList = workFlowTemplateMapper.findColumnListByWordFlowNodeId(schemaName, word_flow_node_id);
        List<Map<String, Object>> orWorkTemplateCloumList = workTemplateService.getWorkTemplateColumnListByWorkTemplateCode(methodParam, new WorkTemplateSearchParam(){{setWork_template_code(work_template_code);}});
        this.handleWorkTemplateCloums(methodParam, copyCallbackService, work_template_code, columnList, orWorkTemplateCloumList);
        for (Map<String, Object> dbColumn : columnList) {
            dbColumn.put("node_id", node_id);
            dbColumn.put("word_flow_node_id", data.get("id"));
            dbColumn.put("create_time", nowTime);
            dbColumn.put("schema_name", schemaName);
            dbColumn.put("create_user_id", methodParam.getUserId());
            workFlowTemplateMapper.insertWorkFlowColumnAdd(dbColumn);
            workFlowTemplateMapper.insertWorkFlowColumnExtAdd(dbColumn);
            //根据被copy数据的字段id查询联动条件
            List<Map<String, Object>> linkList = workFlowTemplateMapper.findWorkTemplateColumnLinkageAndConditionListByColumnId(methodParam.getSchemaName(), dbColumn.get("id").toString());
            if (RegexUtil.optNotNull(linkList).isPresent()) {
                for (Map<String, Object> linkage : linkList) {
                    linkage.put("create_time", nowTime);
                    linkage.put("create_user_id", methodParam.getUserId());
                    linkage.put("schema_name", schemaName);
                    String condition = (String) linkage.get("condition");
                    com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(condition);
//                    JSONObject jsonObject = JSONObject.fromObject(condition);
                    linkage.put("condition", jsonObject);
                    linkage.put("node_id", node_id);
                    linkage.put("column_id", dbColumn.get("column_id"));
                    workTemplateMapper.insertWorkTemplateColumnLinkage(linkage);
                    workTemplateMapper.insertWorkTemplateColumnLinkageCondition(linkage);
                }
            }
        }
    }

    /**
     * 获取流程配置字段详细信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowColumnInfoById(MethodParam methodParam, WorkTemplateSearchParam bParam) {
        Map<String, Object> info = this.getWorkFlowColumnDetailById(methodParam.getSchemaName(), bParam.getId());
        RegexUtil.optMapOrExpNullInfo(info, LangConstant.TITLE_NG); // 字段不存在
        List<Map<String, Object>> extList = this.getWorkFlowColumnExtById(methodParam.getSchemaName(), bParam.getId());
        if (RegexUtil.optIsPresentList(extList)) {
            info.put("extList", extList);
        } else {
            info.put("extList", null);
        }
        List<Map<String, Object>> linkList = this.getWorkFlowColumnLinkage(methodParam.getSchemaName(), bParam.getId());
        if (RegexUtil.optIsPresentList(linkList)) {
            for (Map<String, Object> condition : linkList) {
                com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(condition.get("condition").toString());
                condition.put("condition", jsonObject);
            }
            info.put("linkList", linkList);
        } else {
            info.put("linkList", null);
        }

        return info;
    }

//    /**
//     * 根据流程表主键id获取节点表start节点
//     *
//     * @param methodParam   系统参数
//     * @param id            流程表主键id
//     * @return
//     */
//    @Override
//    public Map<String, Object> getWorkFlowStartNode(MethodParam methodParam, String id) {
//        return workFlowTemplateMapper.findWorkFlowNodeByWorkFlowId(methodParam.getSchemaName(),id);
//    }

    /**
     * 根据工单类型id获取节点表start节点的主键id
     *
     * @param methodParam 系统参数
     * @param workTypeId  工单类型id
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowStartNodeByWorkTypeId(MethodParam methodParam, String workTypeId) {
        return workFlowTemplateMapper.findWorkFlowNodeByWorkTypeId(methodParam.getSchemaName(), workTypeId);
    }

    /**
     * 通过work_type_id查询流程节点信息
     *
     * @param methodParam
     * @param work_type_id
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowNodeTemplateInfoByWorkTypeId(MethodParam methodParam, Integer work_type_id) {
        return RegexUtil.optMapOrExpNullInfo(workFlowTemplateMapper.findWorkFlowNodeTemplateInfoByWorkTypeId(methodParam.getSchemaName(), work_type_id), LangConstant.TITLE_AAI_I); // 流程不存在
    }

    /**
     * 获取流程信息列表
     *
     * @param methodParam
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkFlowInfoList(MethodParam methodParam) {
        return workFlowTemplateMapper.findWorkFlowInfoList(methodParam.getSchemaName());
    }

    /**
     * 通过node信息，获取node字段的所有信息
     *
     * @param schemaName
     * @param nodeInfo
     * @return
     */
    private Map<String, Object> getNodeColumnsInfo(String schemaName, Map<String, Object> nodeInfo) {
        RegexUtil.optMapOrExpNullInfo(nodeInfo, LangConstant.TITLE_AAI_I); // 流程不存在
        String word_flow_node_id = nodeInfo.get("word_flow_node_id").toString();
        List<Map<String, Object>> columnList = workFlowTemplateMapper.findColumnListByWordFlowNodeId(schemaName, word_flow_node_id);
        return handleColumnList(schemaName, nodeInfo, columnList);
    }

    private Map<String, Object> handleColumnList(String schemaName, Map<String, Object> nodeInfo, List<Map<String, Object>> columnList) {
        for (Map<String, Object> column : columnList) {
            if (RegexUtil.optIsPresentStr(column.get("id"))) {
                List<Map<String, Object>> extList = this.getWorkFlowColumnExtById(schemaName, Integer.valueOf(column.get("id").toString()));
                if (RegexUtil.optIsPresentList(extList)) {
                    column.put("work_flow_node_column_ext_list", extList);
                }
                List<Map<String, Object>> linkList = this.getWorkFlowColumnLinkage(schemaName, Integer.valueOf(column.get("id").toString()));
                if (RegexUtil.optIsPresentList(linkList)) {
                    for (Map<String, Object> linkage : linkList) {
                        String conditionStr = RegexUtil.optStrOrNull(linkage.get("condition"));
                        Map<Object, Object> condition = new HashMap<>();
                        condition.put("linkage_id", linkage.get("linkage_id"));
                        condition.put("link_field_form_code", linkage.get("link_field_form_code"));
                        condition.put("interface_address", linkage.get("interface_address"));
                        condition.put("self_defined", linkage.get("self_defined"));
                        condition.put("condition",linkage.get("condition"));
                        linkage.remove("condition");
                        linkage.remove("linkage_id");
                        linkage.remove("link_field_form_code");
                        linkage.remove("interface_address");
                        linkage.remove("self_defined");
                        linkage.put("work_flow_node_column_linkage_condition", condition);
                        String linkage_type = RegexUtil.optStrOrNull(linkage.get("linkage_type"));
                        if ("4".equals(linkage_type)) {
                            String sub_page_template_code = RegexUtil.optStrOrNull(linkage.get("sub_page_template_code"));
                            if (RegexUtil.optIsPresentStr(conditionStr)) {
                                com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(conditionStr);
                                com.alibaba.fastjson.JSONArray page = jsonObject.getJSONArray("page");
                                for (int x = 0; x < page.size(); x++) {
                                    com.alibaba.fastjson.JSONObject jsonObject1 = page.getJSONObject(x);
                                    String fieldCode = jsonObject1.getString("field_code");
                                    String field_form_code = jsonObject1.getString("field_form_code");
                                    List<Map<String, Object>> workTemplateColumnExtByCode = workTemplateMapper.findWorkTemplateColumnExtByCode(schemaName, field_form_code, sub_page_template_code);
                                    jsonObject1.put("work_flow_node_column_ext_list", workTemplateColumnExtByCode);
                                }
                                condition.put("condition",jsonObject);
                            }

                        }
//                        else{
//                            condition.put("condition",conditionStr);
//                        }

                    }
                    column.put("work_flow_node_column_linkage_list", linkList);
                }
            }
        }
        nodeInfo.put("work_flow_node_column_list", columnList);
        return nodeInfo;
    }

    private Map<String, Object> getBaseNodeInfoByNodeTypeAndPrefId(String schemaName, String pref_id, String node_type) {
        return workFlowTemplateMapper.findBaseNodeInfoByNodeTypeAndPrefId(schemaName, pref_id, node_type);
    }

    /**
     * 获取字段联动信息
     *
     * @param schemaName
     * @param id
     * @return
     */
    private List<Map<String, Object>> getWorkFlowColumnLinkage(String schemaName, Integer id) {
        List<Map<String, Object>> conditions = workFlowTemplateMapper.findWorkFlowColumnLinkageByColumnId(schemaName, id);
        return conditions;
    }

    /**
     * 获取字段特殊属性信息
     *
     * @param schemaName
     * @param id
     * @return
     */
    private List<Map<String, Object>> getWorkFlowColumnExtById(String schemaName, Integer id) {
        return workFlowTemplateMapper.findWorkFlowColumnExtByColumnId(schemaName, id);

    }

    /**
     * 根据主键获取node表详情数据
     *
     * @param schemaName
     * @param id
     * @return
     */
    private Map<String, Object> getNodeInfoById(String schemaName, String id) {
        return workFlowTemplateMapper.findNodeInfoById(schemaName, id);
    }

    /**
     * 单独获取字段表数据
     *
     * @param schemaName
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowColumnDetailById(String schemaName, Integer id) {
        return RegexUtil.optMapOrExpNullInfo(workTemplateMapper.findWorkColumnById(schemaName, id), LangConstant.TITLE_NG); // 字段不存在
    }

    /**
     * 字段列表查询条件拼接
     *
     * @param methodParam
     * @param param
     * @return
     */
    private String getWorkFlowTemplateColumnWhereString(MethodParam methodParam, WorkFlowTemplateSearchParam param) {
        StringBuffer whereString = new StringBuffer();
//        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower((select (c.resource->>wfnc.field_name)::jsonb->>'").append(methodParam.getUserLang()).append("' from public.company_resource c where c.company_id = ").append(methodParam.getCompanyId()).append(")) like ").append(e)
//        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(wfnc.field_name) like ").append(e)
//                .append(" or lower(wfnc.field_code) like ").append(e).append(" or wfnc.field_remark like ").append(e).append(")"));
        RegexUtil.optNotBlankStrOpt(param.getField_view_types()).ifPresent(e -> whereString.append(" and wfnc.field_view_type in (").append(DataChangeUtil.joinByStr(e)).append(") "));

        return whereString.toString();
    }

    /**
     * 获取节点信息
     *
     * @param schemaName
     * @param pref_id
     * @param node_id
     * @param page_type
     * @return
     */
    private Map<String, Object> getNodeInfo(String schemaName, String pref_id, String node_id, String page_type) {
        return workFlowTemplateMapper.findNodeInfo(schemaName, pref_id, node_id, page_type);
    }

    /**
     * 获取流程基本信息
     *
     * @param schemaName
     * @param pref_id
     * @return
     */
    @Override
    public Map<String, Object> getFlowInfo(String schemaName, String pref_id) {
        return workFlowTemplateMapper.findFlowInfo(schemaName, pref_id);
    }


    /**
     * 通过pref_id获取流程end节点的信息
     *
     * @param methodParam
     * @param pref_id
     * @return
     */
    @Override
    public Map<String, Object> getWorkFlowInfoByPrefId(MethodParam methodParam, String pref_id) {
        return workFlowTemplateMapper.findWorkFlowInfoByPrefId(methodParam.getSchemaName(), pref_id);
    }

    /**
     * 通过工单模板编码查询所有的字段列表
     *
     * @param methodParam
     * @param workTemplateCode
     * @return
     */
    @Override
    public List<Map<String, Object>> getColumnListByWorkTemplateCode(MethodParam methodParam, String workTemplateCode) {
        return workFlowTemplateMapper.findColumnListByWorkTemplateCode(methodParam.getSchemaName(), workTemplateCode);
    }

    @Override
    public void cutWorkFlowColumnByIds(MethodParam methodParam, WorkFlowTemplateColumnAdd data) {
        String schemaName = methodParam.getSchemaName();
        String ids = data.getIds();
        String[] idArr = ids.split(",");
        String node_id = "";
        for (String idStr : idArr) {
            Integer id = Integer.valueOf(idStr);
            Map<String, Object> dbWorkColumnInfo = this.getWorkFlowColumnDetailById(methodParam.getSchemaName(), id);
            String word_flow_node_id = dbWorkColumnInfo.get("word_flow_node_id").toString();
            String field_form_code = dbWorkColumnInfo.get("field_form_code").toString();
            node_id = dbWorkColumnInfo.get("node_id").toString();
            int count = workFlowTemplateMapper.findColumnCountUseByWordFlowNodeId(schemaName, word_flow_node_id, field_form_code);
            RegexUtil.trueExp(count > 0, LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AAI_I, LangConstant.TITLE_NG, LangConstant.TITLE_NG}); // 流程下还有字段，不能删除该字段
        }
        for (String idStr : idArr) {
            Integer id = Integer.valueOf(idStr);
            workFlowTemplateMapper.deleteTemplateColumnById(schemaName, id);
            workFlowTemplateMapper.deleteTemplateColumnExtByColumnId(schemaName, id);
            workFlowTemplateMapper.deleteTemplateColumnLinkageConditionByColumnId(schemaName, id);
            workFlowTemplateMapper.deleteTemplateColumnLinkageByColumnId(schemaName, id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, node_id, LangUtil.doSetLogArray("流程配置字段批量删除", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAN_F, LangConstant.TITLE_NG}));
        }

    }

    @Override
    public List<Map<String, Object>> findWorkTemplateColumnList(String schemaName, String searchWord, String work_template_code, Long company_id, String userLang, String keywordSearch) {
        List<Map<String, Object>> list = workTemplateMapper.findWorkTemplateColumnList(schemaName,searchWord,work_template_code);
        List<Map<String, Object>> listNew = new ArrayList<>();
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    sd.put("field_name_key", sd.get("field_name"));
                    if (dd.containsKey(sd.get("field_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("field_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("field_name", value);
                        }
                    }
                });
            }
        }

        if (RegexUtil.optIsPresentList(list) && RegexUtil.optNotBlankStrLowerOpt(keywordSearch).isPresent()) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                List<Map<String, Object>> finalList = new ArrayList<>();
                list.forEach(sd -> {
                    String value = "";
                    if (RegexUtil.optIsPresentStr(dd.get(sd.get("field_name")))) {
                        value = RegexUtil.optStrOrBlank(JSONObject.fromObject(dd.get(sd.get("field_name"))).get(userLang));
                    }
                    if (value.toLowerCase().indexOf(RegexUtil.optStrOrBlank(keywordSearch).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("field_code")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(keywordSearch).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("field_remark")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(keywordSearch).toLowerCase()) > -1) {
                        finalList.add(sd);
                    }
                });
                listNew = finalList;
            }
        } else {
            listNew = list;
        }
        return listNew;
    }

    public List<Map<String, Object>> findWorkFlowTemplateColumnList(String schemaName, String searchWord, String pref_id, String node_id, String page_type, Long company_id, String userLang, String keywordSearch) {
        List<Map<String, Object>> list = workFlowTemplateMapper.findWorkFlowTemplateColumnList(schemaName, searchWord, pref_id, node_id, page_type);
        List<Map<String, Object>> listNew = new ArrayList<>();
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.get("field_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("field_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("field_name", value);
                        }
                    }
                });
            }
        }

        if (RegexUtil.optIsPresentList(list) && RegexUtil.optNotBlankStrLowerOpt(keywordSearch).isPresent()) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                List<Map<String, Object>> finalList = new ArrayList<>();
                list.forEach(sd -> {
                    String value = "";
                    if (RegexUtil.optIsPresentStr(dd.get(sd.get("field_name")))) {
                        value = RegexUtil.optStrOrBlank(JSONObject.fromObject(dd.get(sd.get("field_name"))).get(userLang));
                    }
                    if (value.toLowerCase().indexOf(RegexUtil.optStrOrBlank(keywordSearch).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("field_code")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(keywordSearch).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("field_remark")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(keywordSearch).toLowerCase()) > -1) {
                        finalList.add(sd);
                    }
                });
                listNew = finalList;
            }
        } else {
            listNew = list;
        }
        return listNew;
    }

    /**
     * 一键同步流程、节点
     *
     * @param methodParam
     * @param paramMap
     */
    @Override
    public void modifyWorkFlowNodeSync(MethodParam methodParam, Map<String, Object> paramMap) {
        if (RegexUtil.optIsPresentMap(paramMap)) {
            List<Map<String, Object>> flowList = DataChangeUtil.scdStringToList(RegexUtil.optStrOrNull(paramMap.get("flow")));
            List<Map<String, Object>> nodeList = DataChangeUtil.scdStringToList(RegexUtil.optStrOrNull(paramMap.get("node")));

            workFlowTemplateMapper.batchUpdateFlow(methodParam.getSchemaName(), flowList);
            workFlowTemplateMapper.batchUpdateNode(methodParam.getSchemaName(), nodeList);
        }
    }
}
