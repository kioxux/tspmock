package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.TaskReleaseFinishedService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 5:53 2019/9/17 0017
 */
@Service
public class TaskReleaseFinishedServiceImpl implements TaskReleaseFinishedService {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    TaskReleaseFinishedMapper taskReleaseFinishedMapper;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    private LogsService logService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//
//    @Override
//    public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName, String sortOrder, String groups,User loginUser) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        orderBy = "tf."+ sortName + " " + sortOrder;
//        int begin = pageSize * (pageNumber - 1);
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("taskReleasePublish");
//        List<Map<String, Object>> result = taskReleaseFinishedMapper.query(schemaName, orderBy, pageSize, begin, searchKey, groups,idPrefix);
//        handlerFlowData(schemaName, loginUser.getAccount(), result);
//        int total = taskReleaseFinishedMapper.countByCondition(schemaName, searchKey, groups);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    /**
//     * 根据任务编号获取用户组角色信息
//     *
//     * @param schemaName
//     * @param taskId
//     * @return
//     */
//    public List<Map<String, Object>> getGroupRoleListByTask(String schemaName, Integer taskId) {
//        return taskReleaseFinishedMapper.getGroupRoleListByTask(schemaName, taskId);
//    }
//
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//按钮类型
//
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//
//            Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//            if (null != map_object && map_object.size() > 0) {
//                String is_finished = "3";
//                Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//                Map<String, Object> work_taskInfo = (Map<String, Object>) dataInfo.get("_sc_work_task");
//                Map<String, Object> work_task_finishedInfo = (Map<String, Object>) dataInfo.get("_sc_work_task_finished");
//                String sub_work_code = work_task_finishedInfo.get("sub_work_code").toString();//工单编号
//                dynamicCommonService.doCheckFlowInfo(schema_name, user.getAccount(), sub_work_code);
//                Map<String, Object> workInfo = taskReleaseFinishedMapper.findSingleWtInfo(schema_name, sub_work_code);
//
//                String receive_account = workInfo.get("audit_man").toString();//跟进人
////                String finished_content = work_task_finishedInfo.get("finished_content").toString();//任务建议
//                String task_title = work_taskInfo.get("task_title").toString();//任务名称
//                Timestamp now = new Timestamp(System.currentTimeMillis());//获取当前时间
//                work_task_finishedInfo.put("finished_time",now);
//                work_task_finishedInfo.put("is_finished",is_finished);
//
//                Map<String, String> map = new HashMap<String, String>();
//                String title_page = task_title+selectOptionService.getLanguageInfo(LangConstant.MSG_TASK_SUBT);//任务已提交
//                map.put("sub_work_code", sub_work_code);
//                map.put("title_page", title_page);
//                map.put("receive_account", receive_account);
//                map.put("do_flow_key", doFlowKey);
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, sub_work_code);
//                boolean workflowResult = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//                if(workflowResult){
//                    taskReleaseFinishedMapper.updateSubmit(schema_name,work_task_finishedInfo);
//                }
//                workProcessService.saveWorkProccess(schema_name, sub_work_code, Integer.parseInt(is_finished), user.getAccount(), taskSid); // 进度记录
//                logService.AddLog(schema_name, pageType, sub_work_code, selectOptionService.getLanguageInfo(LangConstant.MSG_TASK_SUBT), user.getAccount()); // 记录历史
//                return ResponseModel.ok("ok");
//            }
//        }
//        return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    @Override
//    public ResponseModel doEvaluateTaskRelease(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        String taskId = request.getParameter("task_id");//按钮类型
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//
//            Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//            if (null != map_object && map_object.size() > 0) {
//                Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//                Map<String, Object> work_taskInfo = (Map<String, Object>) dataInfo.get("_sc_work_task");
//                Map<String, Object> work_task_finishedInfo = (Map<String, Object>) dataInfo.get("_sc_work_task_finished");
//                String sub_work_code = work_task_finishedInfo.get("sub_work_code").toString();//工单编号
//                String is_finished = work_task_finishedInfo.get("is_finished").toString();//状态——合格、不合格
//                String task_title = work_taskInfo.get("task_title").toString();//任务名称
//                Timestamp now = new Timestamp(System.currentTimeMillis());//获取当前时间
//                work_task_finishedInfo.put("audit_time",now);
//                work_task_finishedInfo.put("is_finished",is_finished);
//                dynamicCommonService.doCheckFlowInfo(schema_name, user.getAccount(), sub_work_code);
//
//                Map<String, String> map = new HashMap<String, String>();
//                String title_page = task_title+selectOptionService.getLanguageInfo(LangConstant.MSG_TASK_SUBT);
//                map.put("sub_work_code", sub_work_code);
//                map.put("title_page", title_page);
//                map.put("do_flow_key", doFlowKey);
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, sub_work_code);
//                boolean workflowResult = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//                if(workflowResult){
//                    taskReleaseFinishedMapper.updateEvaluate(schema_name,work_task_finishedInfo);
//                }
//                workProcessService.saveWorkProccess(schema_name, sub_work_code, Integer.parseInt(is_finished), user.getAccount(), taskSid); // 进度记录
//                logService.AddLog(schema_name, pageType, sub_work_code, selectOptionService.getLanguageInfo(LangConstant.MSG_TASK_CONF), user.getAccount()); // 任务评价
//                return ResponseModel.ok("ok");
//            }
//        }
//        return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    @Override
//    public boolean doSubmitTaskRelease(String schema_name, String id, boolean status){
//        int update = taskReleaseFinishedMapper.doSubmitTaskRelease(schema_name, id, status);
//        return  (update>0 ? true: false);
//    }
//
//    @Override
//    public Map<String, Object> queryTaskReleaseById(String schemaName, String code) {
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("bomStockList");
//        code = code.substring(idPrefix.length());
//        return taskReleaseFinishedMapper.queryTaskReleaseById(schemaName, Integer.valueOf(code));
//    }
//
//    @Override
//    public Map<String, Object> findSingleWtInfo(String schemaName, String subWorkCode) {
//        return taskReleaseFinishedMapper.findSingleWtInfo(schemaName, subWorkCode);
//    }
//    @Override
//    public int getUnfinishedTask(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        if (null != user) {
//            return taskReleaseFinishedMapper.getUnfinishedTask(schemaName,user.getAccount());
//        }else{
//            return 0;
//        }
//    }
//
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("subWorkCode");
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        permissionInfo.put("todo_list_distribute", "isDistribute");
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_41, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/task_release_finised/findSingleWtInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList){
//        if(dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("sub_work_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        for(Map<String, Object> data : dataList){
//            String subWorkCode = (String)data.get("sub_work_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            //获取流程信息
//            if(task != null){
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            }else{
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
}
