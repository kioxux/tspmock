package com.gengyun.senscloud.service.dynamic;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorksModel;
import org.apache.ibatis.annotations.Param;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;

import java.util.List;
import java.util.Map;

/**
 * 工单管理
 */
public interface WorksService {

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getWorksListPermission(MethodParam methodParam);


    /**
     * 查询工单详情
     *
     * @param methodParam 入参
     * @param worksModel  入参
     * @return 工单详情
     */
    Map<String, Object> getWorkDetailByCode(MethodParam methodParam, WorksModel worksModel);

    /**
     * 查询工单详情
     *
     * @param methodParam 入参
     * @param worksModel  入参
     * @return 工单详情
     */
    Map<String, Object> start(MethodParam methodParam, WorksModel worksModel);

    /**
     * 保存事件
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    void worksSubmit(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 分页查询工单列表
     *
     * @param methodParam 入参
     * @param paramMap    入参
     * @return 工单列表
     */
    Map<String, Object> getWorksList(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 工单作废
     *
     * @param methodParam 入参
     * @param worksModel  入参
     */
    void cancelWorks(MethodParam methodParam, WorksModel worksModel);

    /**
     * 工单分配
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    void worksAssignment(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 工单分配列表
     *
     * @param methodParam 入参
     * @param paramMap    入参
     * @return 工单分配列表
     */
    List<Map<String, Object>> worksAssignmentList(MethodParam methodParam, Map<String, Object> paramMap);


    /**
     * 今日上报数
     *
     * @param methodParam 入参
     * @return 今日上报数
     */
    Integer todayReportWorksCount(MethodParam methodParam);


    /**
     * 今日完成数
     *
     * @param methodParam 入参
     * @return 今日完成数
     */
    Integer todayFinishedWorksCount(MethodParam methodParam);


    /**
     * 超时任务数
     *
     * @param methodParam 入参
     * @return 超时任务数
     */
    Integer todayOvertimeWorksCount(MethodParam methodParam);


    /**
     * 流程回调
     *
     * @param methodParam 入参
     * @param taskEntity  流程节点实例
     * @param isEnd       是否是结束节点
     */
    void callBackStartWithForm(MethodParam methodParam, TaskEntity taskEntity, Map<String, String> endParams, boolean isEnd, FlowableEngineEventType flowableEngineEventType);

    /**
     * 检查验证数据
     *
     * @param methodParam   入参
     * @param sub_work_code 入参
     */
    void doCheckFlowInfo(MethodParam methodParam, String sub_work_code);

    /**
     * 工单回收
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    void worksRecovery(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 有子工单的主工单处理接口
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    List<Map<String, Object>> haveSubWorksDeal(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 扫码签到
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    Map<String, Object> codeScanningSign(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 查询紧急度
     *
     * @param schemaName
     * @param company_id
     * @param userLang
     * * @return
     */
    List<Map<String, Object>> queryCloudDataList(String schemaName, Long company_id, String userLang, String dataType);
}
