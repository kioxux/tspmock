package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.SettlementService;
import org.springframework.stereotype.Service;

@Service
public class SettlementServiceImpl implements SettlementService {
//    private static final Logger logger = LoggerFactory
//            .getLogger(SettlementServiceImpl.class);
//
//    @Autowired
//    SettlementMapper settlementMapper;
//
//    private static final Pattern patternIndex = Pattern.compile("\\([0-9]*\\)");
//    private static final Pattern patternDigit = Pattern.compile("[0-9]+");
//    private static final Pattern patternContent = Pattern.compile("\\{[\\S]*:[\\S]*}");
//    private static final Pattern patternData = Pattern.compile("[^\\{]*:[^}]*");
//
//    @Override
//    public void insert(String schema_name, Settlement settlement) {
//        settlementMapper.insert(schema_name, settlement);
//    }
//
//    @Override
//    public int audit(String schema_name, Settlement settlement) {
//        return settlementMapper.audit(schema_name, settlement);
//    }
//
//    @Override
//    public void delete(String schema_name, String id) {
//        settlementMapper.delete(schema_name, id);
//    }
//
//    @Override
//    public List<Settlement> find(String schema_name, String title, String id, String loginUser) {
//        return settlementMapper.find(schema_name, title, id, loginUser);
//    }
//
//    @Override
//    public List<Map<String, Object>> tableQuery(String schema_name, String table_query) {
//        return settlementMapper.query(schema_name, table_query);
//    }
//
//    @Override
//    public void insertSubBatch(String schema_name, Settlement settlement) {
//        settlementMapper.insertSubBatch(schema_name, settlement);
//    }
//
//    @Override
//    public List<SettlementItem> findSubList(@Param("schema_name") String schema_name, @Param("id") String id) {
//        return settlementMapper.findSubList(schema_name, id);
//    }
//
//
//    @SuppressWarnings({"unchecked"})
//    private void resolveData(Map<String, Object> map, Map<String, Object> outerMap, boolean isParentArray) {
//        map.forEach((key, value) -> {
//            Matcher indexMatcher = patternIndex.matcher(key);
//            Matcher contentMatcher = patternContent.matcher(key);
//            String index = "";
//            int digit = -1;
//            boolean isArray = false;
//            Map<String, Object> additionMap = new HashMap<>();
//            List<Object> nlist = null;
//            Map<String, Object> nmap = null;
//            if (indexMatcher.find()) {
//                index = indexMatcher.group();
//                if (StringUtils.isNotEmpty(index)) {
//                    Matcher digitMatcher = patternDigit.matcher(index);
//                    isArray = true;
//                    if (digitMatcher.find()) {
//                        String strDigit = digitMatcher.group();
//                        digit = Integer.parseInt(strDigit);
//                    }
//                }
//            }
//            String content = "";
//            if (contentMatcher.find()) {
//                content = contentMatcher.group();
//                Matcher dataMatcher = patternData.matcher(content);
//                if (dataMatcher.find()) {
//                    String[] keys = dataMatcher.group().split(":");
//                    additionMap.put(keys[0], keys[1]);
//                }
//            }
//            String keyRemain = key.replace(index, "").replace(content, "");
//            if (isArray) {
//                nlist = (List<Object>)outerMap.computeIfAbsent(keyRemain, k -> new ArrayList<>());
//                if (digit > -1) {
//                    if (nlist.size() - 1 < digit) {
//                        for (int i = nlist.size(); i < digit + 1; i++) {
//                            nlist.add(null);
//                        }
//                    }
//                }
//                if (value instanceof Map) {
//                    if (digit > -1) {
//                        nmap = (Map<String, Object>) nlist.get(digit);
//                        if (nmap == null) {
//                            nmap = new HashMap<>();
//                            nlist.set(digit, nmap);
//                        }
//                    } else {
//                        nmap = new HashMap<>();
//                        nlist.add(nmap);
//                    }
//                    if (additionMap.size() > 0) {
//                        nmap.putAll(additionMap);
//                    }
//                    resolveData((Map<String, Object>) value, nmap, digit <= -1);
//                } else {
//                    nlist.add(value);
//                }
//            } else {
//                if (value instanceof Map) {
//                    nmap = (Map<String, Object>) outerMap.computeIfAbsent(keyRemain, k -> new HashMap<>());
//                    resolveData((Map<String, Object>) value, nmap, false);
//                } else {
//                    if (isParentArray) {
//                        outerMap.put(key, value);
//                    } else {
//                        nlist = (List<Object>) outerMap.computeIfAbsent(keyRemain, k -> new ArrayList<>());
//                        nlist.add(value);
//                    }
//                }
//            }
//        });
//    }
//
//    @Override
//    public Map<String, Object> query(String schema_name, String query) {
//        Map<String, Object> result = new HashMap<>();
//        List<Map<String, Object>> maps = settlementMapper.query(schema_name, query);
//        maps.forEach((map) -> {
//            resolveData(map, result, false);
//        });
//        return result;
//    }
}
