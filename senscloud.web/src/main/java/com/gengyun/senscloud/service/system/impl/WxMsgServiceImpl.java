package com.gengyun.senscloud.service.system.impl;


import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.mapper.MessageMapper;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.service.MessageService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.system.WxMsgService;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信消息管理
 * User: sps
 * Date: 2020/05/29
 * Time: 上午11:20
 */
@Service
public class WxMsgServiceImpl implements WxMsgService {
    private static final Logger logger = LoggerFactory.getLogger(WxMsgServiceImpl.class);
    @Resource
    MessageService messageService;
    @Resource
    UserService userService;
    @Resource
    UserMapper userMapper;
    @Resource
    MessageMapper messageMapper;

    /**
     * 微信消息发送
     *
     * @param schemaName  入参
     * @param configInfo  入参
     * @param openId      入参
     * @param templateId  入参
     * @param contentInfo 入参
     */
    @Override
    public void sendWxMsg(String schemaName, Map<String, String> configInfo, String openId, String templateId, Map<String, Object> contentInfo) {
        String isSuccess = "";
        // 消息发送
        if (RegexUtil.optIsPresentStr(templateId) || RegexUtil.optIsPresentStr(openId)) {
            String appToken = getAccessTokenByDb(schemaName);
            if (!RegexUtil.optIsPresentStr(appToken)) {
                logger.warn("wxMsg-appTokenNull");
            } else {
                logger.info("wxMsg-appToken：" + appToken);
                logger.info("wxMsg-userOpenId：" + openId);
                Map<String, Object> paramMap = new HashMap<String, Object>();
//                String data = "{\"character_string1\": {\"value\": \"W202005290001\"},\"name2\": {\"value\": \"赵应生\"},\"character_string4\": {\"value\": \"T-C-A001\"} ,\"thing3\": {\"value\": \"台式机A001\"},\"thing5\": {\"value\": \"南京小长城天安门长江白宫大楼A栋606室\"}}";
                paramMap.put("touser", openId);
                paramMap.put("template_id", templateId);
                paramMap.put("page", configInfo.get("reserve3"));
                paramMap.put("miniprogram_state", configInfo.get("reserve4"));
                paramMap.put("data", contentInfo);
//                paramMap.put("data", JSONObject.fromObject(data));
                if (doSendMsg(appToken, paramMap)) {
                    isSuccess = "1";
                } else {
                    isSuccess = "-1";
                }
            }
        } else {
            logger.warn("wxMsg-paramError");
        }
        // 更新短信发送日志
        int id = Integer.valueOf(configInfo.get("msgDataId"));
        messageService.updateMessageData(schemaName, id, isSuccess);
    }

    /**
     * 更新微信Token（定时任务）
     *
     * @param schemaName 入参
     */
    @Override
    public synchronized String checkWxAccessToken(String schemaName) {
        logger.info("wxAppTokenCheck");
        Map<String, String> wxTokenConfig = messageService.getWxTokenConfig(schemaName, Constants.USUAL_CONFIG_ID_WXAPPTOKEN);
        if (!RegexUtil.optNotNull(wxTokenConfig).isPresent()) {
            logger.warn("wxAppTokenCheck-configNull");
            return null;
        }
        String type = wxTokenConfig.get("reserve6");
        if (!RegexUtil.optIsPresentStr(type)) {
            logger.warn("wxAppTokenCheck-typeNull");
            return null;
        }
        if (type.startsWith("get")) {
            logger.warn("wxAppTokenCheck-typeRestrict");
            return null;
        }

        String secret = wxTokenConfig.get("reserve5");
        if (!RegexUtil.optIsPresentStr(secret)) {
            logger.warn("wxAppTokenCheck-secretNull");
            return null;
        }
        if (!"1".equals(type)) {
            type = type.concat(secret);
            Map<String, String> globalInfo = messageService.getGlobalInfo(type);
            if (!RegexUtil.optIsPresentStr(globalInfo)) {
                logger.warn("wxAppTokenCheck-globalInfoNull");
                return null;
            } else {
                String rightStr = globalInfo.get("reserve2");
                if (!RegexUtil.optIsPresentStr(rightStr) || !rightStr.equals(schemaName)) {
                    logger.warn("wxAppTokenCheck-globalRightRestrict");
                    return null;
                }
            }
        }

        String timeInterval = wxTokenConfig.get("time_interval"); // token间隔
        // 校验是否过期
        if (RegexUtil.optIsPresentStr(timeInterval) && Integer.valueOf(timeInterval) < 0) {
            logger.info("wxAppTokenCheck-ok");
            return null;
        }
        logger.info("wxAppTokenCheck-overtime");
        String appId = wxTokenConfig.get("reserve4");
        if (!RegexUtil.optIsPresentStr(appId)) {
            logger.warn("wxAppTokenCheck-appIdNull");
            return null;
        }
        String appTokenInfo = getAccessToken(appId, secret);
        if (!RegexUtil.optIsPresentStr(appTokenInfo)) {
            return null;
        }
        JSONObject appTokenJo = JSONObject.fromObject(appTokenInfo);
        String appToken = (String) appTokenJo.get("access_token");
        String result = null;
        int rst = messageService.modifyWxToken(schemaName, appToken, Constants.USUAL_CONFIG_ID_WXAPPTOKEN);
        if (rst > 0) {
            result = "o:" + appToken;
            logger.info("wxAppTokenCheck-update：" + appToken);
        } else {
            result = "f:" + appToken;
            logger.warn("wxAppTokenCheck-updateFail");
        }
        if (!"1".equals(type)) {
            rst = messageService.modifyGlobalWxToken(appToken, type);
            if (rst > 0) {
                result = "go:" + appToken;
                logger.info("wxAppTokenCheck-goUpdate：" + appToken);
            } else {
                result = "gf:" + appToken;
                logger.warn("wxAppTokenCheck-gfUpdateFail");
            }
        }
        return result;
    }

    /**
     * 获取微信Token
     *
     * @param schemaName 入参
     * @return 微信Token
     */
    private String getAccessTokenByDb(String schemaName) {
        String appToken = null;
        Map<String, String> wxTokenConfig = messageService.getWxTokenConfig(schemaName, Constants.USUAL_CONFIG_ID_WXAPPTOKEN);
        if (!RegexUtil.optIsPresentStr(wxTokenConfig)) {
            logger.warn("wxMsg-appTokenConfigNull");
        } else {
            String secret = wxTokenConfig.get("reserve5");
            String type = wxTokenConfig.get("reserve6");
            if (!RegexUtil.optIsPresentStr(type) || !RegexUtil.optIsPresentStr(secret)) {
                logger.warn("wxMsg-appTokenParamNull");
            } else {
                if ("1".equals(type) || !type.startsWith("get")) {
                    appToken = wxTokenConfig.get("reserve1");
                } else {
                    type = type.substring(3).concat(secret);
                    Map<String, String> globalInfo = messageService.getGlobalInfo(type);
                    if (!RegexUtil.optNotNull(globalInfo).isPresent()) {
                        logger.warn("wxMsg-appToken-globalInfoNull");
                    } else {
                        appToken = globalInfo.get("reserve1");
                    }
                }
            }
        }
        return appToken;
    }

    /**
     * 更新用户微信主键
     *
     * @param schemaName
     * @param code
     */
    @Override
    public JSONObject getSessionKeyByCode(String schemaName, String code) {
        logger.debug("getSessionKeyByCode code:" + code);
        if (RegexUtil.optIsPresentStr(code)) {
            Map<String, String> wxTokenConfig = messageService.getWxTokenConfig(schemaName, Constants.USUAL_CONFIG_ID_WXAPPTOKEN);
            if (!RegexUtil.optNotNull(wxTokenConfig).isPresent()) {
                logger.warn("wxOpenId-configNull");
                return null;
            } else {
                String appId = wxTokenConfig.get("reserve4");
                String secret = wxTokenConfig.get("reserve5");
                String userClientInfo = getUserClientInfo(code, appId, secret);
                logger.debug("getSessionKeyByCode userClientInfo:" + userClientInfo);
                if (RegexUtil.optIsPresentStr(userClientInfo)) {
                    return JSONObject.fromObject(userClientInfo);
                }
            }
        }
        return null;
    }

    /**
     * 更新用户微信主键
     *
     * @param schemaName 数据库
     * @param userId     用户主键
     * @param nickName   微信昵称
     * @param openid     微信小程序主键
     * @param unionId    微信用户主键
     */
    @Override
    public void updateUserWxOpenId(String schemaName, String userId, String nickName, String openid, String unionId) {
        logger.debug("updateUserWxOpenId schemaName:" + schemaName + " userId:" + userId + " nickName:" + nickName
                + " openid:" + RegexUtil.optNotBlankStrOrExp(openid, ResponseConstant.RE_LOGIN_CODE) + " unionId:" + RegexUtil.optNotBlankStrOrExp(unionId, ResponseConstant.RE_LOGIN_CODE));
        Map<String, Object> userWxLoginInfo = userMapper.queryUserWxLoginInfo(schemaName, userId);
        boolean isOld = RegexUtil.optIsPresentMap(userWxLoginInfo);
        String wxOaOpenId = null;
        boolean updateWxoaOpenId = false;
        if (isOld) {
            wxOaOpenId = RegexUtil.optStrOrNull(userWxLoginInfo.get("official_open_id"));
            String oldOpenId = RegexUtil.optStrOrBlank(userWxLoginInfo.get("mini_program_open_id"));
            // 如果小程序openid换绑了，或者没有公众号的openid，则重新查询下公众号openid并绑定
            if (!oldOpenId.equals(openid) || !RegexUtil.optIsPresentStr(wxOaOpenId)) {
                updateWxoaOpenId = true;
            }
        } else {
            updateWxoaOpenId = true;
        }
        if (updateWxoaOpenId) {
            logger.debug("updateUserWxOpenId wxOaOpenId:" + wxOaOpenId);
            // 如果用户关注了公众号，获取公众号OPENID，把用户小程序跟公众号关联起来
            wxOaOpenId = RegexUtil.optNotNullMapString(messageMapper.findMsgConfig(schemaName, Constants.USUAL_CONFIG_ID_WX_GZH_TOKEN)).map(m -> m.get("reserve5")).map(wxOaStr ->
                    RegexUtil.optNotNullMap(userService.queryWxoaUserByUnionId(unionId, wxOaStr)).map(wi -> wi.get("open_id")).map(RegexUtil::optStrOrNull).orElse(null)
            ).orElse(null);
        }
        if (isOld) {
            logger.debug("updateUserWxOpenId update userId:" + userId + " openId:" + openid + " unionId:" + unionId + " wxOaOpenId:" + wxOaOpenId + " nickName:" + nickName);
            userService.updateUserWxOpenId(schemaName, userId, openid, unionId, wxOaOpenId, nickName);
        } else {
            logger.debug("updateUserWxOpenId insert userId:" + userId + " openId:" + openid + " unionId:" + unionId + " wxOaOpenId:" + wxOaOpenId + " nickName:" + nickName);
            userService.insertUserWxOpenId(schemaName, userId, openid, unionId, wxOaOpenId, nickName);
        }
    }

    /**
     * 删除用户微信主键
     *
     * @param schemaName 数据库
     * @param userId     用户主键
     * @param openid     微信小程序主键
     */
    @Override
    public void removeUserWxOpenId(String schemaName, String userId, String openid) {
        logger.debug("removeUserWxOpenId schemaName:" + schemaName + " userId:" + RegexUtil.optNotBlankStrOrExp(userId, ResponseConstant.RE_LOGIN_CODE)
                + " openid:" + RegexUtil.optNotBlankStrOrExp(openid, ResponseConstant.RE_LOGIN_CODE));
        String oldOpenId = RegexUtil.optNotNullMap(userMapper.queryUserWxLoginInfo(schemaName, userId)).map(m -> RegexUtil.optStrOrNull(m.get("mini_program_open_id"))).orElse(null);
        logger.debug("removeUserWxOpenId oldOpenId:" + oldOpenId + " openId:" + openid);
        // 如果小程序openid与账号绑定的小程序openid不一致，则不能解绑（只能绑定的小程序账号自己解绑）
        RegexUtil.optNotBlankStrOrExp(oldOpenId, ResponseConstant.RE_LOGIN_CODE);
        RegexUtil.falseExp(RegexUtil.optEquals(oldOpenId, openid), LangConstant.LOG_AD);
        userMapper.clearWxOpenIdAndOaOpenId(schemaName, userId);
    }

    /**
     * 获取微信用户信息
     *
     * @param code   入参
     * @param appId  入参
     * @param secret 入参
     * @return 微信用户信息
     */
    private static String getUserClientInfo(String code, String appId, String secret) {
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code";
        String info = null;
        try {
            info = HttpRequestUtils.requestByGet(url);
            if (RegexUtil.optIsPresentStr(info)) {
                JSONObject jo = JSONObject.fromObject(info);
                String errMsg = (String) jo.get("errmsg");
                if (RegexUtil.optIsPresentStr(errMsg)) {
                    logger.warn("wxMsg-usiError：" + errMsg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("wxMsg-usiExp：" + e.getMessage());
        }
        return info;
    }

    /**
     * 获取微信Token信息
     *
     * @param appId  入参
     * @param secret 入参
     * @return 微信Token信息
     */
    private static String getAccessToken(String appId, String secret) {
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret;
        String info = null;
        try {
            info = HttpRequestUtils.requestByGet(url);
            if (RegexUtil.optIsPresentStr(info)) {
                JSONObject jo = JSONObject.fromObject(info);
                String errMsg = (String) jo.get("errmsg");
                if (RegexUtil.optIsPresentStr(errMsg)) {
                    logger.warn("wxMsg-tokenError：" + errMsg);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("wxMsg-tokenExp：" + e.getMessage());
        }
        return info;
    }

    /**
     * 发送信息
     *
     * @param appToken 入参
     * @param paramMap 入参
     */
    private boolean doSendMsg(String appToken, Map<String, Object> paramMap) {
        boolean isSuccess = false;
        try {
            String url = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + appToken;
            String jsonData = JSONObject.fromObject(paramMap).toString();
            System.out.println(jsonData);
            String info = HttpRequestUtils.requestJsonByPost(url, jsonData);
            if (RegexUtil.optIsPresentStr(info)) {
                JSONObject jo = JSONObject.fromObject(info);
                Integer errorCode = (Integer) jo.get("errcode");
                if (RegexUtil.optIsPresentStr(errorCode)) {
                    if (0 == errorCode) {
                        isSuccess = true;
                    }
                }
                if (!isSuccess) {
                    String errMsg = (String) jo.get("errmsg");
                    if (RegexUtil.optIsPresentStr(errMsg) && !"ok".equals(errMsg)) {
                        logger.warn("wxMsg-sendMsgError：" + errMsg);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("wxMsg-sendMsgExp：" + e.getMessage());
        }
        return isSuccess;
    }
}
