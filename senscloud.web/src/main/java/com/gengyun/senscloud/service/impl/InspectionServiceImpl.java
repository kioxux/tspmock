package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.InspectionService;
import org.springframework.stereotype.Service;

@Service
public class InspectionServiceImpl implements InspectionService {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    InspectionMapper inspectionMapper;
//
//    @Override
//    public List<InspectionItemData> inspectionItemData(String schema_name) {
//
//        return inspectionMapper.inspectionItemData(schema_name);
//    }
//
//
//    @Override
//    public InspectionData inspectionData(String schema_name, String strcode, String account) {
//        return inspectionMapper.inspectionData(schema_name, strcode, account);
//    }
//
//    @Override
//    public int insertInspectionData(String schema_name, InspectionData inspectionData) {
//        return inspectionMapper.insertInspectionData(schema_name, inspectionData);
//    }
//
//    @Override
//    public int updateInspectionData(InspectionData inspectionData) {
//        return inspectionMapper.updateInspectionData(inspectionData);
//    }
//
//    @Override
//    public int EditInspectionBeginTime(String schema_name, InspectionData inspectionData) {
//        return inspectionMapper.EditInspectionBeginTime(schema_name, inspectionData);
//    }
//
//    @Override
//    public int EditInspectionMan(String schema_name, InspectionData inspectionData) {
//        return inspectionMapper.EditInspectionMan(schema_name, inspectionData);
//    }
//
//    @Override
//    public List<InspectionListAppResult> inspectionListAppResult(String schema_name, String condition, int pageSize, int begin) {
//        return inspectionMapper.inspectionListAppResult(schema_name, condition, pageSize, begin);
//    }
//
//    @Override
//    public List<InspectionData> InspectionDataByDate(String schema_name, String condition, String insdate) {
//        return inspectionMapper.InspectionDataByDate(schema_name, condition, insdate);
//    }
//
//    @Override
//    public List<InspectionData> InspectionDataList(String schema_name, String condition, String insdate, int pageSize, int begin) {
//        return inspectionMapper.InspectionDataList(schema_name, condition, insdate, pageSize, begin);
//    }
//
//    @Override
//    public InspectionData inspectionDataByNo(String schema_name, String inspection_code) {
//        return inspectionMapper.inspectionDataByNo(schema_name, inspection_code);
//    }
//
//    /////web//////////////////////////////////////
//    @Override
//    public int insertInspectionAreaData(InspectionAreaData inspectionAreaData) {
//        return inspectionMapper.insertInspectionAreaData(inspectionAreaData);
//    }
//
//
//    @Override
//    public InspectionAreaData InspectionAreaDatabyxy(String schema_name, Integer facility_id, double x, double y) {
//        return inspectionMapper.InspectionAreaDatabyxy(schema_name, facility_id, x, y);
//    }
//
//
//    @Override
//    public InspectionAreaData InspectionAreaDatabyarea(String schema_name, String area_code) {
//        return inspectionMapper.InspectionAreaDatabyarea(schema_name, area_code);
//    }
//
//
//    @Override
//    public List<InspectionAreaData> InspectionAreaDatabyfac(String schema_name, Integer facility_id) {
//        return inspectionMapper.InspectionAreaDatabyfac(schema_name, facility_id);
//    }
//
//    @Override
//    public List<InspectionAreaData> queryByPlanInspecctionAreaList(String schema_name, String condition, int pageSize, int begin) {
//        return inspectionMapper.queryByPlanInspecctionAreaList(schema_name, condition, pageSize, begin);
//    }
//
//    @Override
//    public List<InspectionAreaData> findAllInspectionAreaData(String schema_name) {
//        return inspectionMapper.findAllInspectionAreaData(schema_name);
//    }
//
//    @Override
//    public List<InspectionTimes> findAllInspectionTimes(String schema_name) {
//        return inspectionMapper.findAllInspectionTimes(schema_name);
//    }
//
//    @Override
//    public int closeInspectionForDue(String schema_name, Timestamp timeStamp) {
//        return inspectionMapper.closeInspectionForDue(schema_name, timeStamp);
//    }
//
//    @Override
//    public int updateInspectionAreaData(InspectionAreaData inspectionAreaData) {
//        return inspectionMapper.updateInspectionAreaData(inspectionAreaData);
//    }
//
//    /**
//     * 删除巡更点
//     */
//    @Override
//    public int DeleteionAreaDatabyfac(String schema_name, Integer facility_id, String area_code) {
//        return inspectionMapper.DeleteionAreaDatabyfac(schema_name, facility_id, area_code);
//    }
//
//    /**
//     * 更新巡更点表的xy
//     */
//    @Override
//    public int updateInspectionAreaxy(String schema_name, Integer facility_id, Double x, Double y, String Locationxy) {
//        return inspectionMapper.updateInspectionAreaxy(schema_name, facility_id, x, y, Locationxy);
//    }
//
//    @Override
//    public int countInspectionsTodayByNo(String schema_name, int inspectionNo) {
//        return inspectionMapper.countInspectionsTodayByNo(schema_name, inspectionNo);
//    }
//
//    @Override
//    public List<InspectionData> InspectionDataListweb(String schema_name, String condition, int pageSize, int begin) {
//        return inspectionMapper.InspectionDataListweb(schema_name, condition, pageSize, begin);
//    }
//
//    @Override
//    public int InspectionDataListwebtotal(String schema_name, String condition) {
//        return inspectionMapper.InspectionDataListwebtotal(schema_name, condition);
//    }
//
//    @Override
//    public InspectionData InspectionDatabycode(String schema_name, String inspection_code) {
//        return inspectionMapper.InspectionDatabycode(schema_name, inspection_code);
//    }
//
//    @Override
//    public int cancelInspection(String schema_name, String code) {
//        return inspectionMapper.cancelInspection(schema_name, code);
//
//    }
//
//    @Override
//    public FullScreenData getPlannedAndCompleteInspection(String schema_name) {
//        return inspectionMapper.getPlannedAndCompleteInspection(schema_name);
//    }
//
//    @Override
//    public FullScreenData getPlannedAndCompleteSpotInspection(String schema_name) {
//        return inspectionMapper.getPlannedAndCompleteSpotInspection(schema_name);
//    }
//
//    /**
//     * 校验数据库操作权限
//     *
//     * @param schema_name
//     * @param request
//     * @param area_code
//     * @throws Exception
//     */
//    public void checkUserPermission(String schema_name, HttpServletRequest request, String area_code) throws Exception {
//
//        String condition = null;
//        User user = authService.getLoginUser(request);
//        //根据人员的角色，判断获取值的范围
//        Boolean isAllFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection_config");
//        if (userFunctionList != null && !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("inspectionarea_all_facility")) {
//                    isAllFacility = true;
//                    break;
//                }
//            }
//        }
//        if (!isAllFacility) {
//            LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//            String facilityIds = "";
//            if (facilityList != null && !facilityList.isEmpty()) {
//                for (String key : facilityList.keySet()) {
//                    facilityIds += facilityList.get(key).getId() + ",";
//                }
//            }
//            int count = 0;
//            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                count = inspectionMapper.getInspectionAreaData(schema_name, facilityIds, area_code);
//            }
//
//            if (count < 1) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//            }
//        }
//    }
}
