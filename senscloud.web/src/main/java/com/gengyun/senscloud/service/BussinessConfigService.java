package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.BussinessConfigModel;
import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 业务配置
 */
public interface BussinessConfigService {
    /**
     * 新增系统维护配置
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void newSystemConfig(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除系统维护配置
     *
     * @param methodParam          入参
     * @param bussinessConfigModel 入参
     * @return 系统维护配置列表
     */
    void cutSystemConfigById(MethodParam methodParam, BussinessConfigModel bussinessConfigModel);

    /**
     * 查询系统维护配置列表
     *
     * @param methodParam          入参
     * @param bussinessConfigModel 入参
     * @return 系统维护配置列表
     */
    List<Map<String, Object>> getSystemConfigList(MethodParam methodParam, BussinessConfigModel bussinessConfigModel);

    /**
     * 更新系统维护配置
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void modifySystemConfig(MethodParam methodParam, Map<String, Object> pm);


    Object getSystemConfigField(MethodParam methodParam, BussinessConfigModel bussinessConfigModel);

    /**
     * 根据id获取系统信息
     *
     * @param methodParam 入参
     * @param tableName   表名
     * @param tableName   id
     */
    Map<String, Object> getSystemConfigInfoById(MethodParam methodParam, String tableName, Integer id);

    /**
     * 根据id获取业务类型详情
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 业务类型详情
     */
    Map<String, Object> getBusinessById(MethodParam methodParam, Integer id);

    /**
     * 根据业务类型获取id最小的工单类型
     *
     * @param methodParam 入参
     * @param business_id 入参
     * @return 业务类型详情
     */
    Map<String, Object> getWorkTypeByBusiness(MethodParam methodParam, Integer business_id);

    /**
     * 根据业务类型获取启用的工单类型ids
     *
     * @param methodParam 入参
     * @param business_id 入参
     * @return 工单类型ids
     */
    List<String> getWorkTypeIdsByBusinessId(MethodParam methodParam, Integer business_id);
}
