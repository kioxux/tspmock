package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MailSenderConfigService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 3:10 2019/5/23 0023
 */
@Service
public class MailSenderConfigServiceImpl implements MailSenderConfigService {
//    @Autowired
//    MailSenderConfigMapper mailSenderConfigMapper;
//
//    @Autowired
//    UserMapper userMapper;
//    @Override
//    public List<MailSenderConfigModel> all(String schema_name, String condition, int pageSize, int begin) {
//        List<MailSenderConfigModel> mailSenderConfigModels = mailSenderConfigMapper.all(schema_name , condition , pageSize , begin);
//        for(MailSenderConfigModel mailSender:mailSenderConfigModels){
//            String receiver_account = mailSender.getReceiver_account();
//            String cc_account = mailSender.getCc_account();
//            String receiver_name = "";
//            String cc_name = "";
//            if(receiver_account!=null && !receiver_account.equals("")){
//                String [] arrayReceive = receiver_account.split(",");
//                for(int i=0;i<arrayReceive.length;i++){
//
//                    User user = userMapper.findByAccount(schema_name,arrayReceive[i]);
//                    String user_name = user.getUsername()+",";
//                    receiver_name += user_name;
//                }
//            }
//
//            if(cc_account!=null && !cc_account.equals("")){
//                String [] arrayCc = cc_account.split(",");
//                for(int i=0;i<arrayCc.length;i++){
//
//                    User user = userMapper.findByAccount(schema_name,arrayCc[i]);
//                    String user_name = user.getUsername()+",";
//                    cc_name += user_name;
//                }
//            }
//            if(receiver_name!=null && !receiver_account.equals("")){
//                receiver_name = receiver_name.substring(0,receiver_name.length()-1);
//            }
//            if(cc_name!=null && !cc_name.equals("")){
//                cc_name = cc_name.substring(0,cc_name.length()-1);
//            }
//            mailSender.setReceiver_name(receiver_name);
//            mailSender.setCc_name(cc_name);
//        }
//        return mailSenderConfigModels;
//    }
//
//    @Override
//    public int allCount(String schema_name, String condition) {
//        return mailSenderConfigMapper.allCount(schema_name , condition);
//    }
//
//    @Override
//    public int add(String schema_name, MailSenderConfigModel mailSenderConfigModel) {
//        return mailSenderConfigMapper.add(schema_name,mailSenderConfigModel);
//    }
//
//    @Override
//    public int updateSenderMailConfig(String schema_name, boolean status_b, int id) {
//        return mailSenderConfigMapper.updateSenderMailConfig(schema_name,status_b,id);
//    }
//
//    public int updateSenderMailList(String schema_name, MailSenderConfigModel mailSenderConfigMode){
//        return mailSenderConfigMapper.updateSenderMailList(schema_name,mailSenderConfigMode);
//    }
}
