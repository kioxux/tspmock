package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.service.job.PolluteFreeJobService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PolluteFreeJobServiceImpl implements PolluteFreeJobService {
    @Resource
    PolluteFeeMapper polluteFeeMapper;
    @Resource
    CustomersMapper customersMapper;
    @Resource
    MeterMapper meterMapper;
    @Resource
    LogsMapper logsMapper;

    @Override
    @Transactional
    public void cronJobToGeneratePolluteFree(String schemaName) {
        /*
         * TODO: 定期缴费公式
         *   费用总金额 = 正常污水处理费用 + 超标污水处理补偿费用
         *   正常污水处理费用 = 正常污水处理量（A）*污水处理单价（B）  公式：A*B
         *   超标污水处理补偿费用 = 超标污水处理量（A）*污水处理单价（B）*各污染因子超标系数（C1+C2+...+Cn）+超标后复测次数（D）*复测采样分析费用（E） 公式: A*B*(C1+C2+...+Cn)+D*E D<3
         *   超标污水处理补偿费用 = 当月污水处理量（Q）*污水处理单价（B）*各污染因子超标系数（C1+C2+...+Cn）+超标后复测次数（D）*复测采样分析费用（E） 公式: Q*B*(C1+C2+...+Cn)+D*E D>=3
         *   单个污染因子超标系数 = 该因子的实测值/该因子的纳管标准限值（保留两位小数）
         * */

        //客户数据
        List<Map<String, Object>> customersList = RegexUtil.optNotNullListOrNew(customersMapper.findAllCustomers(schemaName));
        //获取缴费周期
        List<Map<String, Object>> polluteFeeSeting = RegexUtil.optNotNullListOrNew(polluteFeeMapper.polluteFeeMapper(schemaName, "自动计费配置"));
        Map<String, Object> cycleMap = this.toMapByList(polluteFeeSeting, "config_name", "setting_value");
        String belong_month = this.getPolluteCycleTime(null, null, true);
        String beginMonth = RegexUtil.optStrOrNull(cycleMap.get("pollute_begin_month"));
        String beginDay = RegexUtil.optStrOrNull(cycleMap.get("pollute_begin_day"));
        String endMonth = RegexUtil.optStrOrNull(cycleMap.get("pollute_end_month"));
        String endDay = RegexUtil.optStrOrNull(cycleMap.get("pollute_end_day"));
        String begin_date = this.getPolluteCycleTime(beginMonth, beginDay, false);
        String end_date = this.getPolluteCycleTime(endMonth, endDay, false);
        // 计算超标污水量 污水处理单价，复测采样分析费用等
        List<Map<String, Object>> pollutePriceSetList = RegexUtil.optNotNullListOrNew(polluteFeeMapper.findStaticDataByType(schemaName, "pollute_price_seting"));
        Map<String, Object> map = this.toMapByList(pollutePriceSetList, "reserve1", "value");
        // 污水处理单价
        BigDecimal price = new BigDecimal(RegexUtil.optStrOrVal(map.get("price"), "0"));
        // 基值
        BigDecimal base_value = new BigDecimal(RegexUtil.optStrOrVal(map.get("base_value"), "0"));
        // 复测采样分析费用
        BigDecimal repeat_price = new BigDecimal(RegexUtil.optStrOrVal(map.get("repeat_price"), "0"));
        // 计算超标的污水
        List<Map<String, Object>> cusMeterPollList = new ArrayList<>();
        customersList.forEach(cu -> {
            Map<String, Object> pm = new HashMap<>();
            pm.put("facility_id", RegexUtil.optStrOrNull(cu.get("facility_id")));
            pm.put("begin_time", begin_date);
            pm.put("end_time", end_date);
            // 获取该客户总排污量，异常排污量，复测次数
            Map<String, Object> cusMeterInfo = RegexUtil.optMapOrNew(meterMapper.findCusMeterInfo(schemaName, pm));
            // 超标系数
            BigDecimal exc_ratio = new BigDecimal(RegexUtil.optStrOrVal(meterMapper.findCusFacRatio(schemaName, pm), "0"));
            exc_ratio = exc_ratio.compareTo(BigDecimal.ONE) < 0 ? BigDecimal.ONE : exc_ratio;
            // 复测次数
            BigDecimal repeat_count = new BigDecimal(RegexUtil.optStrOrVal(cusMeterInfo.get("repeat_count"), "0"));
            // 超标次数
            BigDecimal exc_count = new BigDecimal(RegexUtil.optStrOrVal(meterMapper.findRatioCount(schemaName, pm), "0"));
            // 复测总费用
            BigDecimal repeat_fee = repeat_count.multiply(repeat_price).setScale(2,BigDecimal.ROUND_HALF_UP);
            // 总排污量
            BigDecimal total = new BigDecimal(RegexUtil.optStrOrVal(cusMeterInfo.get("total"), "0")).setScale(2,BigDecimal.ROUND_HALF_UP);
            // 异常排污量
            BigDecimal exc_total = new BigDecimal(RegexUtil.optStrOrVal(cusMeterInfo.get("exc"), "0")).setScale(2,BigDecimal.ROUND_HALF_UP);
            // 污水处理服务费
            BigDecimal normal_fee_total;
            if (total.compareTo(base_value) > 0) {
                normal_fee_total = total.multiply(price).setScale(2,BigDecimal.ROUND_HALF_UP);
            } else {
                normal_fee_total = base_value.multiply(price).setScale(2,BigDecimal.ROUND_HALF_UP);
            }
            // 处理服务补偿费
            BigDecimal exc_fee_total = BigDecimal.ZERO;
            if (repeat_count.compareTo(BigDecimal.valueOf(3)) < 0) {
                exc_fee_total = exc_total.multiply(price).multiply(exc_ratio).add(repeat_fee).setScale(2,BigDecimal.ROUND_HALF_UP);
            } else if (repeat_count.compareTo(BigDecimal.valueOf(3)) >= 0) {
                exc_fee_total = total.multiply(price).multiply(exc_ratio).add(repeat_fee).setScale(2,BigDecimal.ROUND_HALF_UP);
            }

            pm.put("price", price);
            pm.put("repeat_price", repeat_price);
            pm.put("repeat_count", repeat_count);
            pm.put("exc_ratio", exc_ratio);
            pm.put("total", total);
            pm.put("base_value", base_value);
            pm.put("exc_count", exc_count);
            pm.put("exc_total", exc_total);
            pm.put("normal_fee_total", normal_fee_total);
            pm.put("exc_fee_total", exc_fee_total);
            pm.put("fee_total", normal_fee_total.add(exc_fee_total));
            cusMeterPollList.add(pm);
        });
        // 对每个客户的污水处理费用指标保存业务数据
        for (Map<String, Object> item : cusMeterPollList) {
            Map<String, Object> param = new HashMap<>();
            Map<String, Object> paramNormalItem = new HashMap<>();
            Map<String, Object> paramExcItem = new HashMap<>();
            Map<String, Object> cusMap = RegexUtil.optMapOrNew(customersMapper.findByID(schemaName, RegexUtil.optIntegerOrNull(item.get("facility_id"))));
            param.put("facility_id", item.get("facility_id"));
            param.put("belong_month", belong_month);
            param.put("begin_date", begin_date);
            param.put("end_date", end_date);
            param.put("amount", item.get("fee_total"));
            param.put("final_amount", item.get("fee_total"));
            param.put("exceed_amount", item.get("exc_fee_total"));
            param.put("normal_amount", item.get("normal_fee_total"));
            param.put("pay_amount", item.get("fee_total"));
            param.put("final_flow", item.get("total"));
            param.put("payment_value", item.get("base_value"));
            param.put("exceed_times", item.get("exc_count"));
            param.put("create_user_id", "system");
            // 对每个客户的污水处理费用明细情况进行业务保存
            paramNormalItem.put("fee_type", "0");
            paramNormalItem.put("fee_item_id", 1);
            paramNormalItem.put("facility_id", item.get("facility_id"));
            paramNormalItem.put("belong_month", belong_month);
            if (new BigDecimal(RegexUtil.optStrOrVal(item.get("total"), "0")).compareTo(base_value) > 0) {
                paramNormalItem.put("final_flow",  item.get("total"));
            } else {
                paramNormalItem.put("final_flow",  base_value);
            }
            paramNormalItem.put("price", price);
            paramNormalItem.put("amount", item.get("normal_fee_total"));
            paramNormalItem.put("create_user_id", "system");

            paramExcItem.put("fee_type", "1");
            paramExcItem.put("fee_item_id", 2);
            paramExcItem.put("facility_id", item.get("facility_id"));
            paramExcItem.put("belong_month", belong_month);
            paramExcItem.put("final_flow", item.get("exc_total"));
            paramExcItem.put("price", price);
            paramExcItem.put("ratio", item.get("exc_ratio"));
            paramExcItem.put("repeat_count", item.get("repeat_count"));
            paramExcItem.put("repeat_fee", repeat_price);
            paramExcItem.put("amount", item.get("exc_fee_total"));
            paramExcItem.put("create_user_id", "system");
            int count = polluteFeeMapper.findPolluteFeeByCondition(schemaName, param);
            if (count == 0) {
                polluteFeeMapper.insertPolluteFee(schemaName, param);
                paramNormalItem.put("fee_id", param.get("id"));
                polluteFeeMapper.insertPolluteFeeNormalDetail(schemaName, paramNormalItem);

                paramExcItem.put("fee_id", param.get("id"));
                polluteFeeMapper.insertPolluteFeeExcDetail(schemaName, paramExcItem);
                logsMapper.insertLog(schemaName, SensConstant.BUSINESS_NO_6004, RegexUtil.optStrOrNull(param.get("id")), LangUtil.doSetLogArray("新增自动缴费信息", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_ADFG, "，客户："
                        .concat(RegexUtil.optStrOrBlank(cusMap.get("short_title")))
                        .concat("，").concat(belong_month)
                        .concat("月份缴费信息")}), "system", SenscloudUtil.getNowTime()); //新增自动缴费信息
            } else {
                int isUpdate = polluteFeeMapper.findPolluteFeeByUpdate(schemaName, param);
                if (isUpdate == 0) {
                    polluteFeeMapper.updatePolluteFee(schemaName, param);
                    polluteFeeMapper.updatePolluteFeeNormalDetail(schemaName, paramNormalItem);
                    polluteFeeMapper.updatePolluteFeeExcDetail(schemaName, paramExcItem);
                }
            }
        }
    }

    public String getPolluteCycleTime(String month, String day, boolean isMonth) {
        SimpleDateFormat format = null;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date); // 设置为当前时间
        if (isMonth) {
            format = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON_2);
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)); // 设置为本月
        } else {
            format = new SimpleDateFormat(Constants.DATE_FMT);
            if (Objects.equals(month, "0")) {
                calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1); // 设置为上一个月
            } else {
                calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)); // 设置为本月
            }
            calendar.set(Calendar.DAY_OF_MONTH, NumberUtils.toInt(day));
        }
        date = calendar.getTime();
        String accDate = format.format(date);
        return accDate;
    }

    public Map<String, Object> toMapByList(List<Map<String, Object>> list, String key, String value) {
        Map<String, Object> result = new HashMap<>();
        if (RegexUtil.optIsPresentList(list)) {
            for (Map<String, Object> item : list) {
                if (RegexUtil.optIsPresentStr(item.get(key))) {
                    result.put(item.get(key).toString(), item.get(value));
                }
            }
        }
        return result;
    }
}
