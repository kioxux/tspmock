package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.mapper.WorkProcessMapper;
import com.gengyun.senscloud.service.dynamic.WorkProcessService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;

/**
 * 功能：工单进度记录表
 * Created by Dong wudang on 2018/12/14.
 */
@Service
public class WorkProcessServiceImpl implements WorkProcessService {

    //    @Autowired
//    SerialNumberService serialNumberService;
//
    @Resource
    WorkProcessMapper workProcessMapper;

    /**
     * 工单进度记录
     *
     * @param schema_name
     * @param sub_work_code   工单编号
     * @param status          状态
     * @param operate_user_id 操作人id
     * @param task_id         流程节点编号
     * @return
     */
    @Override
    public Boolean saveWorkProccess(String schema_name, String sub_work_code, int status, String operate_user_id, String task_id) {
        try {
            Timestamp operate_time = new Timestamp(System.currentTimeMillis());
            Boolean result = false;
            int doSave = workProcessMapper.saveWorkProccess(schema_name, sub_work_code, status, task_id, operate_user_id, operate_time);
            if (doSave > 0) {
                result = true;
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
//

    /**
     * 按节点号取操作人
     *
     * @param schemaName  入参
     * @param subWorkCode 入参
     * @param nodeId      入参
     * @return 操作人
     */
    @Override
    public String getOperateUserIdByNodeId(String schemaName, String subWorkCode, String nodeId) {
        return workProcessMapper.findOperateUserIdByNodeId(schemaName, subWorkCode, nodeId);
    }
}
