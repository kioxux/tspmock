package com.gengyun.senscloud.service.cache;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.OtherCfgModel;
import com.gengyun.senscloud.model.WorkListModel;

import java.util.List;
import java.util.Map;

/**
 * 缓存工具接口
 */
public interface CacheUtilService {
    /**
     * 获取用户常用语言包/系统默认语言包（明细）
     *
     * @param methodParam 系统参数
     * @param lang        国际化类型
     * @return 语言包
     */
    Map<String, String> getDefaultLangDtl(MethodParam methodParam, String lang);

    /**
     * 获取验证码
     *
     * @param phone 电话号码
     * @return 验证码
     */
    String getVfCode(String phone);

    /**
     * 新建验证码并存入缓存
     *
     * @param phone 电话号码
     * @return 验证码
     */
    String newVfCode(String phone);

    /**
     * 查询业务编号生成规则配置信息
     *
     * @param schemaName   数据库
     * @param businessType 业务类型
     * @return 编号配置信息
     */
    Map<String, Object> getSerialByType(String schemaName, String businessType);

    /**
     * 获取系统缓存信息（替代session）
     *
     * @param token token
     * @return 系统缓存信息
     */
    Map<String, Object> cacheScdSesInfo(String token);

    /**
     * 退出登录
     *
     * @param methodParam 系统参数
     */
    void applyLogOut(MethodParam methodParam);

    /**
     * 获取缓存列表
     *
     * @param ocModel 请求参数
     * @return 缓存列表
     */
    List<Map<String, Object>> getCacheListForPage(OtherCfgModel ocModel);

    /**
     * 根据缓存名称清除缓存
     *
     * @param cacheName 缓存名称
     */
    void evictCacheByName(String cacheName);

    /**
     * 根据缓存名称和键值清除缓存
     *
     * @param name 缓存名称
     * @param key  缓存键值
     */
    void evictCacheByKey(String name, String key);

    /**
     * 添加个人信息缓存
     *
     * @param methodParam 入参
     * @param userId      入参
     * @return 个人信息缓存
     */
    Map<String, Object> getUserInfo(MethodParam methodParam, String userId);

    /**
     * 将用户信息同步到公共表中
     */

    void deleteSubToken(String sub_token);

    String getSubToken(MethodParam methodParam, String sub_token);

    /**
     * 删除用户信息缓存
     */

    void deleteUserInfo(String userId, String schemaName);

    Map<String, Object> findLangStaticOption(String schemaName, Long companyId, String userLang, String selectKey, String code);

    List<Map<String, Object>> findLangStaticDataByType(String schemaName, Long companyId, String userLang, String selectKey);

    Map<String, Object> findInfoById(String schemaName, Integer id, String userLang, Long companyId);

    List<Map<String, Object>> handleQueryCloudDataList(String schemaName, Long company_id, String userLang, String dataType);

    List<WorkListModel> findWorksheetList(String schemaName, String userId, String userLang, Long companyId, String condition, String pagination);

    List<WorkListModel> findWorkListDetailBySwc(String schemaName, String[] idStr, String user_lang, Long company_id);

    Map<String, Object> findAssetInfoForAssetMap(String schema_name, String id, String user_lang, Long companyId);

    List<Map<String, Object>> findAssetOrg(String schemaName, Long companyId, String langKey);

    List<Map<String, Object>> findAllCloudDataType(String schemaName, String langKey, Long company_id);

    List<Map<String, Object>> findAllFacilityWithType(String schemaName, Long companyId, String langKey, String asset_id, String keyword, String org_type);

    List<Map<String, Object>> findInnerFacilityWithType(String schemaName, Long companyId, String userLang);

    List<Map<String, Object>> findSpecialPropertyKey(String schemaName, String data_type, String parent_code, String langKey, Long company_id);

    List<Map<String, Object>> findAssistByPlanCode(String schemaName, String plan_code, String user_lang, Long company_id);
}