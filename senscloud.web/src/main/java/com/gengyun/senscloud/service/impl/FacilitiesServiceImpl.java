package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.FacilitiesMapper;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.FacilitiesService;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 厂商管理
 */
@Service
public class FacilitiesServiceImpl implements FacilitiesService {
    @Resource
    FacilitiesMapper facilitiesMapper;
    @Resource
    LogsService logService;
    @Resource
    FilesService filesService;
    @Resource
    PagePermissionService pagePermissionService;

    @Override
    public Map<String, Map<String, Boolean>> getFacilitiesPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[11]);
    }

    /**
     * 新增厂商
     *
     * @param methodParam 入参
     * @param param       入参
     */
    @Override
    public void newFacilities(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optStrOrExpNotNull(param.get("org_type_id"), LangConstant.TITLE_CATEGORY_AB_K);//组织类型不能为空
//        String title = RegexUtil.optStrOrExpNotNull(param.get("title"), LangConstant.TITLE_NAME_M);//厂商名称不能为空
//        String inner_code = RegexUtil.optStrOrExpNotNull(param.get("inner_code"), LangConstant.TITLE_ABAAA_T);//编码不能为空
//        String short_title = RegexUtil.optStrOrExpNotNull(param.get("short_title"), LangConstant.TITLE_AM_P);//简称不能为空
////        //判断厂商名称是否重复
//        if (RegexUtil.optNotNull(facilitiesMapper.findByTitle(methodParam.getSchemaName(), title)).isPresent()) {
//            throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_NAME_M, title});//厂商名称已存在
//        }
//        //判断厂商简称
//        if (RegexUtil.optNotNull(facilitiesMapper.findByShortTitle(methodParam.getSchemaName(), short_title)).isPresent()) {
//            throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_AM_P, short_title});//简称已存在
//        }
//        //判断厂商编码是否重复
//        if (RegexUtil.optNotNull(facilitiesMapper.findByCode(methodParam.getSchemaName(), inner_code)).isPresent()) {
//            throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_ABAAA_T, inner_code});//编码已存在
//        }
        param.put("is_use", '1');
        param.put("status", 1);
        param.put("create_user_id", methodParam.getUserId());
        facilitiesMapper.InsertFacilities(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, param.get("id").toString(), LangUtil.doSetLogArray("添加了厂商", LangConstant.TEXT_ADD_A, new String[]{LangConstant.TITLE_B_O}));//添加了厂商
    }

    /**
     * 更新厂商
     *
     * @param methodParam 入参
     * @param param       入参
     */
    @Override
    public void modifyFacilities(MethodParam methodParam, Map<String, Object> param) {

//        String sectionType = RegexUtil.optStrOrError(param.get("sectionType"), methodParam, ErrorConstant.EC_ASSET_2, param.get("id").toString(), LangConstant.MSG_BI);
        Integer id = RegexUtil.optSelectOrExpParam(param.get("id"), LangConstant.TITLE_B_O); // 厂商未选择
//        if ("facilitiesTop".equals(sectionType)) {
//            //判断厂商名称是否重复
//            Map<String, Object> titleMap = facilitiesMapper.findByTitle(methodParam.getSchemaName(), param.get("title").toString());
//            if (RegexUtil.optNotNull(titleMap).isPresent() && !titleMap.get("id").toString().equals(param.get("id").toString())) {
//                throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_NAME_M, param.get("title").toString()});//厂商名称已存在
//            }
//            //判断厂商编码是否重复
//            Map<String, Object> codeMap = facilitiesMapper.findByCode(methodParam.getSchemaName(), param.get("inner_code").toString());
//            if (RegexUtil.optNotNull(codeMap).isPresent() && !codeMap.get("id").toString().equals(param.get("id").toString())) {
//                throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_ABAAA_T, param.get("inner_code").toString()});//厂商编码已存在
//            }
//        }
//        if ("facilitiesBase".equals(sectionType)) {
//            //判断厂商简称是否重复
//            Map<String, Object> shortTitleMap = facilitiesMapper.findByShortTitle(methodParam.getSchemaName(), param.get("short_title").toString());
//            if (RegexUtil.optNotNull(shortTitleMap).isPresent() && !shortTitleMap.get("id").toString().equals(param.get("id").toString())) {
//                throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_AM_P, param.get("short_title").toString()});//简称已存在
//            }
//        }
        Map<String, Object> oldMap = RegexUtil.optMapOrExpNullInfo(facilitiesMapper.findByID(methodParam.getSchemaName(), id), LangConstant.TITLE_B_O);
        facilitiesMapper.updateFacilities(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, id.toString(), LangUtil.doSetLogArray("编辑了厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_B_O, LangUtil.compareMap(param, oldMap).toString()}));
    }

    /**
     * 启用禁用厂商
     *
     * @param methodParam 入参
     * @param param       入参
     */
    @Override
    public void changeUseFacilities(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_B_O);//厂商不能为空
        Map<String, Object> oldMap = facilitiesMapper.findByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            facilitiesMapper.changeIsUseFacilities(methodParam.getSchemaName(), param);
            JSONArray log = LangUtil.compareMap(param, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, param.get("id").toString(), LangUtil.doSetLogArray("编辑了厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_B_O, log.toString()}));
        }
    }

    /**
     * 删除厂商
     *
     * @param methodParam     入参
     * @param facilitiesModel 入参
     */
    @Override
    public void cutFacilities(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        //  Integer id = RegexUtil.optIntegerOrExpParam(facilitiesModel.getId(), LangConstant.TITLE_B_O);//厂商不能为空
//        Map<String, Object> oldMap = facilitiesMapper.findByID(methodParam.getSchemaName(), id);
//        if (RegexUtil.optNotNull(oldMap).isPresent()) {
//            facilitiesMapper.deleteFacilities(methodParam.getSchemaName(), id);
//            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, facilitiesModel.getId().toString(), LangUtil.doSetLogArray("删除了厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_B_O, oldMap.get("title").toString()}));//删除了厂商
//        }
        String ids = RegexUtil.optStrOrExpNotNull(facilitiesModel.getIds(), LangConstant.MSG_A);
        String[] idList = RegexUtil.optNotBlankStrOpt(ids).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
        List<String> nameList = facilitiesMapper.findNamesByIds(methodParam.getSchemaName(), idList);
        if (RegexUtil.optNotNull(nameList).isPresent() && nameList.size() > 0) {
            facilitiesMapper.deleteByIds(methodParam.getSchemaName(), idList);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, facilitiesModel.getIds(), LangUtil.doSetLogArray("删除了厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_B_O, nameList.toString()}));//删除了厂商
        }
    }

    /**
     * 批量删除厂商
     *
     * @param methodParam     入参
     * @param facilitiesModel 入参
     */
    @Override
    public void cutFacilitiesList(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        String ids = RegexUtil.optStrOrExpNotNull(facilitiesModel.getIds(), LangConstant.MSG_A);
        String[] idList = RegexUtil.optNotBlankStrOpt(ids).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
        List<String> nameList = facilitiesMapper.findNamesByIds(methodParam.getSchemaName(), idList);
        if (RegexUtil.optNotNull(nameList).isPresent() && nameList.size() > 0) {
            facilitiesMapper.deleteByIds(methodParam.getSchemaName(), idList);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, facilitiesModel.getIds(), LangUtil.doSetLogArray("删除了厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_B_O, nameList.toString()}));//删除了厂商
        }
    }

    /**
     * 获取厂商详情
     *
     * @param methodParam     入参
     * @param facilitiesModel 入参
     * @return 厂商详情
     */
    @Override
    public Map<String, Object> findById(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        Integer id = RegexUtil.optSelectOrExpParam(facilitiesModel.getId(), LangConstant.TITLE_B_O); // 厂商未选择
        return facilitiesMapper.findByID(methodParam.getSchemaName(), id);
    }

    /**
     * 获取厂商列表
     *
     * @param methodParam 入参
     * @param param       入参
     * @return 厂商列表
     */
    @Override
    public Map<String, Object> findFacilitiesList(MethodParam methodParam, Map<String, Object> param) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = facilitiesMapper.findFacilitiesListCount(methodParam.getSchemaName(), param);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        param.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = facilitiesMapper.findFacilitiesList(methodParam.getSchemaName(), param);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 新增厂商附件
     *
     * @param methodParam 入参
     * @param param       入参
     */
    @Override
    public void newOrganizationDoc(MethodParam methodParam, Map<String, Object> param) {
        Integer org_id = RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_B_O);//厂商不能为空
        Integer file_id = RegexUtil.optIntegerOrExpParam(param.get("file_id"), LangConstant.TAB_DOC_A);//附件文档不能为空
        Map<String, Object> file = filesService.findById(methodParam.getSchemaName(), file_id);
        if (RegexUtil.optNotNull(file).isPresent()) {
            param.put("create_user_id", methodParam.getUserId());
            facilitiesMapper.insertOrganizationDoc(methodParam.getSchemaName(), param);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, org_id.toString(), LangUtil.doSetLogArray("添加了附件文档", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TAB_DOC_A, file.get("file_original_name").toString()}));
        }
    }

    /**
     * 删除厂商附件
     *
     * @param methodParam 入参
     * @param param       入参
     */
    @Override
    public void cutOrganizationDoc(MethodParam methodParam, Map<String, Object> param) {
        Integer org_id = RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_B_O);//厂商不能为空
        Integer file_id = RegexUtil.optIntegerOrExpParam(param.get("file_id"), LangConstant.TAB_DOC_A);//附件文档不能为空
        Map<String, Object> file = filesService.findById(methodParam.getSchemaName(), file_id);
        if (RegexUtil.optNotNull(file).isPresent()) {
            facilitiesMapper.deleteOrganizationDoc(methodParam.getSchemaName(), org_id, file_id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, org_id.toString(), LangUtil.doSetLogArray("删除了附件文档", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_DOC_A, file.get("file_original_name").toString()}));
        }
    }

    /**
     * 获取厂商附件列表
     *
     * @param methodParam 入参
     * @param param       入参
     * @return 厂商附件列表
     */
    @Override
    public List<Map<String, Object>> getOrganizationDocList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_B_O);//厂商不能为空
        return facilitiesMapper.findOrganizationDocList(methodParam.getSchemaName(), param);
    }

    /**
     * 新增厂商联系人
     */
    @Override
    public void newOrganizationContact(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_B_O);//厂商不能为空
        param.put("create_user_id", methodParam.getUserId());
        facilitiesMapper.insertOrganizationContact(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, param.get("org_id").toString(), LangUtil.doSetLogArray("添加了厂商联系人", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AAJ_P, null}));

    }

    /**
     * 编辑厂商联系人
     *
     * @param methodParam 入参
     * @param param       入参
     */
    @Override
    public void modifyOrganizationContact(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_B_O);//厂商不能为空
        Map<String, Object> oldMap = facilitiesMapper.findOrganizationContactInfo(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            facilitiesMapper.updateOrganizationContact(methodParam.getSchemaName(), param);
            JSONArray log = LangUtil.compareMap(param, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, oldMap.get("org_id").toString(), LangUtil.doSetLogArray("编辑了厂商联系人", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAJ_P, log.toString()}));
        }
    }

    /**
     * 删除厂商联系人
     */
    @Override
    public void cutOrganizationContact(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        RegexUtil.optIntegerOrExpParam(facilitiesModel.getId(), LangConstant.TITLE_AAJ_P);//联系人不能为空
        Map<String, Object> organizationContact = facilitiesMapper.findOrganizationContactInfo(methodParam.getSchemaName(), facilitiesModel.getId());
        if (RegexUtil.optNotNull(organizationContact).isPresent()) {
            facilitiesMapper.deleteOrganizationContact(methodParam.getSchemaName(), facilitiesModel.getId());
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6001, facilitiesModel.getOrg_id().toString(), LangUtil.doSetLogArray("删除了厂商联系人", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAJ_P, organizationContact.get("contact_name").toString()}));
        }
    }

    /**
     * 查询厂商联系人列表
     */
    @Override
    public List<Map<String, Object>> getOrganizationContactList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_B_O);//厂商不能为空
        return facilitiesMapper.findOrganizationContactList(methodParam.getSchemaName(), param);
    }

    /**
     * 获取外部组织
     *
     * @param schemaName
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryOuterSuppliesList(String schemaName) {
        return facilitiesMapper.queryOuterSuppliesList(schemaName);
    }

    @Override
    public Map<String, Map<String, Object>> queryOuterSupplierList(String schemaName) {
        return facilitiesMapper.queryOuterSupplierList(schemaName);
    }

    @Override
    public Map<String, Map<String, Object>> queryOuterManufacturerList(String schemaName) {
        return facilitiesMapper.queryOuterManufacturerList(schemaName);
    }

    @Override
    public Map<String, Object> findByCode(String schemaName, String code) {
        return facilitiesMapper.findByCode(schemaName, code);
    }
}
