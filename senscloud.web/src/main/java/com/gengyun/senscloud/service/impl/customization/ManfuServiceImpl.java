package com.gengyun.senscloud.service.impl.customization;

import com.gengyun.senscloud.service.customization.ManfuService;
import org.springframework.stereotype.Service;

@Service
public class ManfuServiceImpl implements ManfuService {
//
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//
//    @Autowired
//    WorkSheetService workSheetService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    TaskTempLateService taskTempLateService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    FilesService filesService;
//
//    @Autowired
//    EmailUtil emailUtil;
//
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    /**
//     * 跳转到checklist模板页面
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ModelAndView worksheetDetailChecklistReport(HttpServletRequest request, String schema_name) {
//        String subWorkCode = request.getParameter("subWorkCode");
//        String toPdf = request.getParameter("toPdf");
//        String sep = System.getProperty("file.separator");
//        String clientType = request.getParameter("clientType");
//
//        Map<String, Object> workSheetDetail = workSheetService.findWorkSheetDetailBySubWorkCode(schema_name, subWorkCode);
//        Map<String, Object> workInfo = workSheetHandleMapper.queryWorkInfoBySubWorkCode(schema_name, subWorkCode);
//        String taskContent = workSheetHandleService.getWorkSheetTaskContentByWorkCode(schema_name, subWorkCode);
//        Map<String, List<Map<String, String>>> taskMap = new HashMap<>();
//        if(StringUtils.isNotBlank(taskContent)){//任务项处理
//            JSONArray taskItem = new org.json.JSONObject(taskContent).getJSONArray("data");
//            if(taskItem != null && taskItem.length() > 0){
//                Map<String, Boolean> codeToResultMap = new HashMap<>();
//                StringBuffer itemCodes = new StringBuffer();
//                for(int i=0;i<taskItem.length();i++){
//                    JSONObject item = taskItem.getJSONObject(i);
//                    String task_item_code = item.optString("task_item_code");
//                    String task_item_name = item.optString("task_item_name");
//                    Boolean lineValue = item.optBoolean("lineValue");
//                    itemCodes.append((itemCodes.length() == 0?"":",")+ "'"+task_item_code+"'");
//                    codeToResultMap.put(task_item_name, lineValue);
//                }
//                List<Map<String, String>> taskTemplateAndItems = taskTempLateService.findPlanTaskTemplateAndTaskItemList(schema_name, itemCodes.toString());
//                if(taskTemplateAndItems != null){
//                    for(Map<String, String> taskTemplateAndItem : taskTemplateAndItems){
//                        Boolean result = codeToResultMap.get(taskTemplateAndItem.get("task_item_name"));
//                        taskTemplateAndItem.put("lineValue", result == true?"true":"false");
//                        String key = taskTemplateAndItem.get("task_template_name");
//                        if(taskMap.containsKey(key)){
//                            List<Map<String, String>> list = taskMap.get(key);
//                            list.add(taskTemplateAndItem);
//                            taskMap.put(key, list);
//                        }else{
//                            taskMap.put(key, new ArrayList<>(Arrays.asList(taskTemplateAndItem)));
//                        }
//                    }
//                }
//            }
//        }
//        ModelAndView modelAndView = new ModelAndView("manfu_report/check_list")
//                .addObject("today", new SimpleDateFormat(Constants.DATE_FMT);.format(new Date()))
//                .addObject("isMobile", SensConstant.IOS_CLIENT_NAME.equalsIgnoreCase(clientType)|| SensConstant.APP_CLIENT_NAME.equalsIgnoreCase(clientType))
//                .addObject("companyId", schema_name.replace(SensConstant.SCHEMA_PREFIX, ""))
//                .addObject("subWorkCode", subWorkCode)
//                .addObject("workSheetDetail", workSheetDetail)
//                .addObject("workInfo", workInfo)
//                .addObject("save_s", selectOptionService.getLanguageInfo(LangConstant.SAVE_S))
//                .addObject("submit", selectOptionService.getLanguageInfo(LangConstant.SUBMIT))
//                .addObject("toPdf", toPdf)
//                .addObject("taskMap", taskMap);
//        try {
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "manfu_logo");
//            if (data != null)
//                modelAndView.addObject("logo", ImageUtils.getBase64Extension(data.getSettingValue()) + ImageUtils.getImgStrToBase64(data.getSettingValue()));
//
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "checklist" + sep + subWorkCode + ".json";
//            modelAndView.addObject("data", FileUtil.getMfReportData(filePath));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return modelAndView;
//    }
//
//    /**
//     * 保存、提交checklist报告
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ResponseModel doSaveWorksheetDetailCheckListReport(HttpServletRequest request, String schema_name) {
//        String sep = System.getProperty("file.separator");
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            String reportData = request.getParameter("reportData");
//            String isMobile = request.getParameter("isMobile");
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "checklist" + sep + subWorkCode;
//            //生成json文件并保存
//            FileUtil.generateJsonFile(filePath, reportData);
//            String url = HttpRequestUtils.getBaseUrl() + ("true".equals(isMobile)?"/service/work_":"/work/") + "worksheet-detail-checklist-report?subWorkCode=" + subWorkCode + "&toPdf=true&companyId="+schema_name.replace(SensConstant.SCHEMA_PREFIX, "");
//            //获取动态填写数据的html页面并生成文件
//            File html = HtmlToPdfUtils.getHtmlFile(request, url, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_HTML);
//            //html转pdf
//            HtmlToPdfUtils.htmlToPdf(html, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF, HttpRequestUtils.getBaseUrl(), true, true);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUBMIT_SUCESS));//提交成功
//    }
//
//    /**
//     * 跳转到Spray模板页面
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ModelAndView worksheetDetailSprayReport(HttpServletRequest request, String schema_name) {
//        String subWorkCode = request.getParameter("subWorkCode");
//        String toPdf = request.getParameter("toPdf");
//        String sep = System.getProperty("file.separator");
//        String clientType = request.getParameter("clientType");
//        ModelAndView modelAndView = new ModelAndView("manfu_report/spray_accuracy_test_report")
//                .addObject("subWorkCode", subWorkCode);
//        try {
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "manfu_logo");
//            if (data != null)
//                modelAndView.addObject("logo", ImageUtils.getBase64Extension(data.getSettingValue()) + ImageUtils.getImgStrToBase64(data.getSettingValue()));
//
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "spray" + sep + subWorkCode + ".json";
//            modelAndView.addObject("data", FileUtil.getMfReportData(filePath))
//                .addObject("isMobile", SensConstant.IOS_CLIENT_NAME.equalsIgnoreCase(clientType)|| SensConstant.APP_CLIENT_NAME.equalsIgnoreCase(clientType))
//                .addObject("companyId", schema_name.replace(SensConstant.SCHEMA_PREFIX, ""))
//                .addObject("toPdf", toPdf)
//                .addObject("save_s", selectOptionService.getLanguageInfo(LangConstant.SAVE_S))
//                .addObject("submit", selectOptionService.getLanguageInfo(LangConstant.SUBMIT));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return modelAndView;
//    }
//
//    /**
//     * 保存、提交Spray报告
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ResponseModel doSaveWorksheetDetailSprayReport(HttpServletRequest request, String schema_name) {
//        String sep = System.getProperty("file.separator");
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            String reportData = request.getParameter("reportData");
//            String isMobile = request.getParameter("isMobile");
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "spray" + sep + subWorkCode;
//            //生成json文件并保存
//            FileUtil.generateJsonFile(filePath, reportData);
//            String url = HttpRequestUtils.getBaseUrl() + ("true".equals(isMobile)?"/service/work_":"/work/") + "worksheet-detail-spray-report?subWorkCode=" + subWorkCode + "&toPdf=true&companyId="+schema_name.replace(SensConstant.SCHEMA_PREFIX, "");
//            //获取动态填写数据的html页面并生成文件
//            File html = HtmlToPdfUtils.getHtmlFile(request, url, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_HTML);
//            //html转pdf
//            HtmlToPdfUtils.htmlToPdf(html, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF, HttpRequestUtils.getBaseUrl(), true, true);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUBMIT_SUCESS));//提交成功
//    }
//
//    /**
//     * 跳转到photo模板页面
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ModelAndView worksheetDetailPhotoReport(HttpServletRequest request, String schema_name) {
//        String sep = System.getProperty("file.separator");
//        String subWorkCode = request.getParameter("subWorkCode");
//        String toPdf = request.getParameter("toPdf");
//        String preview = request.getParameter("preview");
//        String clientType = request.getParameter("clientType");
//        ModelAndView modelAndView = new ModelAndView("manfu_report/field_site_photos")
//                .addObject("subWorkCode", subWorkCode);
//        try {
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "manfu_logo");
//            if (data != null)
//                modelAndView.addObject("logo", ImageUtils.getBase64Extension(data.getSettingValue()) + ImageUtils.getImgStrToBase64(data.getSettingValue()));
//
//            Map<String, Object> map = workSheetService.findWorkSheetImgBySubWorkCode(schema_name, subWorkCode);
//            if (map != null) {
//                String beforeImg = (String) map.get("before_img");
//                String afterImg = (String) map.get("after_img");
//                List<String> fileIds = new ArrayList<>();
//                if (StringUtils.isNotBlank(beforeImg)) {
//                    String[] beforeImgArray = beforeImg.split(",");
//                    fileIds.addAll(Arrays.asList(Arrays.copyOf(beforeImgArray, beforeImgArray.length < 3 ? beforeImgArray.length : 3))); //默认取事前照片的前三张照片
//                }
//                if (StringUtils.isNotBlank(afterImg)) {
//                    String[] afterImgArray = afterImg.split(",");
//                    fileIds.addAll(Arrays.asList(Arrays.copyOf(afterImgArray, afterImgArray.length < 3 ? afterImgArray.length : 3))); //默认取事后照片的前三张照片
//                }
//                if (fileIds.size() > 0) {
//                    List<FilesData> fileDatas = filesService.FindAllFiles(schema_name, StringUtils.join(fileIds, ","));
//                    if (fileDatas != null && fileDatas.size() > 0) {
//                        List<Map<String, Object>> photoList = new ArrayList<>();
//                        for (FilesData filesData : fileDatas) {
//                            String fileUrl = filesData.getFileUrl();
//                            if (StringUtils.isNotBlank(fileUrl)) {
//                                try {
//                                    File file = new File(fileUrl);
//                                    StringBuffer fileName = new StringBuffer(file.getName());
//                                    fileName.insert(fileName.lastIndexOf("."), "_thumb").toString();
//                                    String thumbFileUrl = fileUrl.replace(file.getName(), fileName.toString());
//                                    File thumbFile = new File(thumbFileUrl);
//                                    if (!thumbFile.exists()) {
//                                        //对图片进行压缩，宽、高分别超过1300px、900px时，进行等比例缩放，小于这个尺寸，则保持宽、高不变
//                                        ImageUtil.createThumbnail(fileUrl, thumbFileUrl, 1300, 800, false);
//                                    }
//                                    Map<String, Object> photo = new HashMap<>();
//                                    BufferedImage bufferedImage = ImageIO.read(thumbFile);//获取图片宽高，指定img标签宽高值，解决itextpdf转pdf时图片宽高不对的问题
//                                    photo.put("width", bufferedImage.getWidth());
//                                    photo.put("height", bufferedImage.getHeight());
//                                    photo.put("fileUrl", "data:image/jpeg;base64," + ImageUtils.getImgStrToBase64(thumbFileUrl));
//                                    photoList.add(0, photo);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                        modelAndView.addObject("photoList", photoList);
//                    }
//                }
//            }
//            boolean readyOnly = false;
//            if ("true".equals(preview)) {
//                readyOnly = true;
//            } else {
//                String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "photo" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//                File jsonFile = new File(filePath);
//                if (jsonFile.exists())
//                    readyOnly = true;
//            }
//            modelAndView.addObject("readyOnly", readyOnly)
//                .addObject("isMobile", SensConstant.IOS_CLIENT_NAME.equalsIgnoreCase(clientType)|| SensConstant.APP_CLIENT_NAME.equalsIgnoreCase(clientType))
//                .addObject("companyId", schema_name.replace(SensConstant.SCHEMA_PREFIX, ""))
//                .addObject("toPdf", toPdf)
//                .addObject("save_s", selectOptionService.getLanguageInfo(LangConstant.SAVE_S))
//                .addObject("submit", selectOptionService.getLanguageInfo(LangConstant.SUBMIT));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return modelAndView;
//    }
//
//    /**
//     * 保存、提交photo报告
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ResponseModel doSaveWorksheetDetailPhotoReport(HttpServletRequest request, String schema_name) {
//        String sep = System.getProperty("file.separator");
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            String isMobile = request.getParameter("isMobile");
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "photo" + sep + subWorkCode;
//            String url = HttpRequestUtils.getBaseUrl() + ("true".equals(isMobile)?"/service/work_":"/work/") + "worksheet-detail-photo-report?preview=true&subWorkCode=" + subWorkCode+ "&toPdf=true&companyId="+schema_name.replace(SensConstant.SCHEMA_PREFIX, "");
//            //获取动态填写数据的html页面并生成文件
//            File html = HtmlToPdfUtils.getHtmlFile(request, url, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_HTML);
//            //html转pdf
//            HtmlToPdfUtils.htmlToPdf(html, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF, HttpRequestUtils.getBaseUrl(), true, true);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUBMIT_SUCESS));//提交成功
//    }
//
//    /**
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public ResponseModel doSendEmailForWorksheetReport(HttpServletRequest request, String schema_name, User loginUser) {
//        String sep = System.getProperty("file.separator");
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Boolean isChecklist = Boolean.valueOf(request.getParameter("checklist"));
//            Boolean isSpray = Boolean.valueOf(request.getParameter("spray"));
//            Boolean isPhoto = Boolean.valueOf(request.getParameter("photo"));
//            String emails = request.getParameter("email");
//            if (StringUtils.isBlank(emails))
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR));//没有获取参数
//
//            String[] emailArray = emails.split(",");
//            if (emailArray.length < 1) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.EMAIL) + selectOptionService.getLanguageInfo(LangConstant.NOT_NULL));//邮箱格式验证
//            }
//            Set<String> resultList = new HashSet();
//            for (String email : emailArray) {
//                if (RegexUtil.isEmail(email) == false)
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.EMAIL_ERROR));//邮箱格式验证
//                resultList.add(email);
//            }
//            emailArray = resultList.toArray(new String[resultList.size()]);
//            Map<String, Object>[] fileMap = new HashMap[3];
//            String checklistFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "checklist" + sep + subWorkCode;
//            String sprayFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "spray" + sep + subWorkCode;
//            String photoFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "photo" + sep + subWorkCode;
//            String checklistStr = FileUtil.getMfReportData(checklistFilePath + ".json");
//            if (StringUtils.isNotBlank(checklistStr) && isChecklist) {
//                JSONObject checklistJson = new JSONObject(checklistStr);
//                if (checklistJson != null && checklistJson.optBoolean("submit") == true) {
//                    Map<String, Object> map = new HashMap<>();
//                    map.put("file", new File(checklistFilePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF));
//                    map.put("fileNamePrefix", "checklist_");
//                    fileMap[0] = map;
//                }
//            }
//            String sprayStr = FileUtil.getMfReportData(sprayFilePath + ".json");
//            if (StringUtils.isNotBlank(sprayStr) && isSpray) {
//                JSONObject sprayJson = new JSONObject(sprayStr);
//                if (sprayJson != null && sprayJson.optBoolean("submit") == true) {
//                    Map<String, Object> map = new HashMap<>();
//                    map.put("file", new File(sprayFilePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF));
//                    map.put("fileNamePrefix", "spray_");
//                    fileMap[1] = map;
//                }
//            }
//            if (isPhoto) {
//                Map<String, Object> map = new HashMap<>();
//                map.put("file", new File(photoFilePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF));
//                map.put("fileNamePrefix", "photo_");
//                fileMap[2] = map;
//            }
//            //发送邮件给客户
//            emailUtil.sendEmailList(emailArray, loginUser.getEmail(), getSubject(schema_name,subWorkCode), Constants.MANFU_REPORT_EMAIL_CONTENT, fileMap);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUBMIT_SUCESS));//提交成功
//    }
//
//    /**
//     * 获取主题
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    private String getSubject(String schemaName,String subWorkCode) {
//        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_DATE_3, Locale.US);//日 月 年
//        WorkListDetailModel workDetail =  workSheetService.finddetilByID(schemaName, subWorkCode);
//        String orgTitle;
//        try{
//            Facility facility=facilitiesService.FacilitiesById(schemaName,Integer.parseInt((String)(workDetail.getFacility_id())) );
//            orgTitle=facility.getShort_title();
//        }catch (ClassCastException e){
//            orgTitle="";
//        }
//        return String.format("Adisseo liquid applicator service report %s %s %s %s", sdf.format(new Date()), subWorkCode,orgTitle,workDetail.getAsset_name());
//    }
}
