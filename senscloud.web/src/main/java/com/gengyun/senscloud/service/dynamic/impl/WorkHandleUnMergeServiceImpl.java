package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.WorkHandleUnMergeService;
import org.springframework.stereotype.Service;

/**
 * 工单业务处理（未合并、临时企业方案）
 * User: sps
 * Date: 2019/12/30
 * Time: 上午11:20
 */
@Service
public class WorkHandleUnMergeServiceImpl implements WorkHandleUnMergeService {
//    private static final Logger logger = LoggerFactory.getLogger(WorkHandleUnMergeServiceImpl.class);
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    WorkSheetRequestService workSheetRequestService;
//    @Autowired
//    MessageService messageService;
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        if (null != map_object && map_object.size() > 0) {
//            String workRequestType = (String) map_object.get("work_request_type");
//            if (RegexUtil.isNotNull(workRequestType)) {
//                if ("confirm".equals(workRequestType)) {
//                    return this.saveWorkRequest(schema_name, processDefinitionId, map_object, do_flow_key, user, workRequestType);
//                }
//            }
//            return this.saveWorkData(schema_name, processDefinitionId, map_object, do_flow_key, user);
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.LOG_ERROR_IN_WORKSHEET));//当前工单信息有错误
//    }
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param map_object
//     * @param do_flow_key
//     * @param user
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel saveWorkData(String schema_name, String processDefinitionId, Map<String, Object> map_object, String do_flow_key, User user) throws Exception {
//        String account = user.getAccount();
//        String accountName = user.getUsername();
//        Object returnType = "ok"; // 非保存
//
//        String msgCode = null; // 消息编码
//        Boolean is_submit = false;//是否提交
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        Object facilityId = works_tmp.get("facility_id");
//        map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//        map.put("position_code", works_tmp.get("position_code")); // 所属位置
//        String title = (String) works_tmp.get("title");
//        map.put("title_page", title);
//        map.put("work_request_type", (String) map_object.get("work_request_type"));
//        String subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        String workCode = (String) works_tmp.get("work_code");
//        try {
//            // 根据数据库中状态判断是否为重复操作
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
//            if ((Integer) workInfo.get("status") > StatusConstant.DRAFT || !account.equals(workInfo.get("create_user_account"))) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(works_detaile_tmp.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.NEW_K) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);
//        if (SensConstant.BUSINESS_NO_1.equals(businessTypeId)) {
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_ESCALATE_A_PROBLEM);//上报一个问题
//        }
//        map.put("sub_work_code", subWorkCode);
//        String receive_account = (String) map_object.get("next_operator");
//        if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//            receive_account = (String) works_detaile_tmp.get("receive_account");
//        }
//        if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//            receive_account = null;
//        }
//        String relationId = (String) works_detaile_tmp.get("relation_id"); // 对象
//        int status = StatusConstant.DRAFT;//状态
//        Integer nextStatus = null;
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workCode", workCode);
//            returnContent.put("subWorkCode", subWorkCode);
//            returnType = returnContent; // 保存
//        } else if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            try {
//                nextStatus = Integer.valueOf((String) map_object.get("next_status"));
//            } catch (Exception statusExp) {
//
//            }
//            // 处理接收人和接受时间
//            if (null != nextStatus || !StringUtil.isEmpty(receive_account) && !"-1".equals(receive_account)) {
//                if (null == nextStatus) {
//                    status = StatusConstant.PENDING;//保存状态
//                    works_detaile_tmp.put("varchar@receive_time", "receive_time");
//                    works_detaile_tmp.put("receive_time", now);
//                    works_detaile_tmp.put("varchar@distribute_time", "distribute_time");
//                    works_detaile_tmp.put("distribute_time", now);
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                    // 维修时
//                    if (SensConstant.BUSINESS_NO_1.equals(businessTypeId)) {
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_ESCALATE_A_PROBLEM) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;
//                    } else {
//                        logOperation = selectOptionService.getLanguageInfo(LangConstant.NEW_K) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;
//                    }
//                } else {
//                    status = nextStatus;
//                }
//                msgCode = qcloudsmsConfig.SMS_10003007;
//            } else {
//                msgCode = qcloudsmsConfig.SMS_10003005;
//                status = StatusConstant.UNDISTRIBUTED;//无分配人下一步开始分配
//            }
//            map.put("receive_account", receive_account);
//            map.put("create_user_account", account);//发送短信人
//            map.put("businessType", workTypeId);     //工单
//            map.put("deadline_time", works_tmp.get("deadline_time"));
//            map.put("status", String.valueOf(status));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 页面【发生时间】为空时，自动赋值
//        if (works_tmp.containsKey("occur_time")) {
//            if (null == works_tmp.get("occur_time") || "".equals(works_tmp.get("occur_time"))) {
//                works_tmp.put("varchar@occur_time", "occur_time");
//                works_tmp.remove("null@occur_time");
//                works_tmp.put("occur_time", now);
//            } else {
//                map.put("create_time", works_tmp.get("occur_time"));
//            }
//        }
//        works_tmp.put("status", status);
//        works_tmp.put("create_user_account", account);
//        //处理工单表的创建人和创建时间
//        works_tmp.put("varchar@create_time", "create_time");
//        works_tmp.put("create_time", now);
//        works_detaile_tmp.put("status", status);//无分配人下一步开始分配
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//        if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//            try {
//                Integer fid = Integer.valueOf((String) facilityId);
//            } catch (Exception fidExp) {
//                works_tmp.put("position_code", facilityId);
//                works_tmp.remove("facility_id");
//            }
//        }
//        // 重新绑定值
//        dataInfo.put("_sc_works", works_tmp);
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
//
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", works_tmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", works_tmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        String flow_data = workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        // 多个对象
//        if (RegexUtil.isNotNull(relationId)) {
//            workSheetHandleService.insertWorkAssetOrg(schema_name, subWorkCode, relationId); // 工单设备组织处理
//        }
//        if (StringUtil.isNotEmpty(flow_data)) {
//            map.put("flow_data", flow_data);
//            try {
//                Map<String, Object> roleInfo = (Map) map_object.get("roleInfo");
//                if (roleInfo.containsKey("mergeRole")) {
//                    map_object.put("roleIds", roleInfo.get("mergeRole"));
//                }
//            } catch (Exception roleExp) {
//            }
//        }
//        if (is_submit) {
//            String workRequestType = (String) map_object.get("work_request_type");
//            if (null == workRequestType || !"workOnly".equals(workRequestType)) {
//                this.saveWorkRequest(schema_name, processDefinitionId, map_object, do_flow_key, user, null);
//            }
////            map.put("roleIds", map_object.get("roleIds"));
////            if (RegexUtil.isNotNull(relationId)) {
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////                if (RegexUtil.isNotNull(categoryIdStr) && !"无".equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
////            }
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, null, map_object);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//            if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                throw new SenscloudException(doType + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//流程启动失败
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                    account, accountName, title, doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, subWorkCode, status, account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//            logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
////        } else {
////            logOperation = "进行了一次" + doType + "保存";
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workInfo && null != dtlInfo) {
////                workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 保存请求事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param map_object
//     * @param do_flow_key
//     * @param user
//     * @param workRequestType
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel saveWorkRequest(String schema_name, String processDefinitionId, Map<String, Object> map_object, String do_flow_key, User user, String workRequestType) throws Exception {
//        String account = user.getAccount();
//        String accountName = user.getUsername();
//        Object returnType = "ok"; // 非保存
//
//        String msgCode = null; // 消息编码
//        Boolean is_submit = false;//是否提交
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        String workRequestCode = null;
//        // 工单流程
//        if (null == workRequestType || !"confirm".equals(workRequestType)) {
//            Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//            Map<String, Object> worksTmp = (Map<String, Object>) dataInfo.get("_sc_works");
//            workRequestTmp = new HashMap<String, Object>();
//            workRequestTmp.putAll(worksTmp);
//            workRequestTmp.putAll(worksDetailTmp);
//            workRequestCode = dynamicCommonService.getWorkRequestCode(schema_name);
//            workRequestTmp.put("work_request_code", workRequestCode);
//            Map<String, Object> workInfo = new HashMap<String, Object>();
//            workInfo.put("varchar@work_request_code", "work_request_code");
//            workInfo.put("work_request_code", workRequestCode);
//            workInfo.put("work_code", worksTmp.get("work_code"));
//            workSheetHandleService.updateWorkByWorkCode(schema_name, workInfo);
//            map_object.put("saveSubType", "updateRequest");
//        } else {
//            workRequestCode = (String) workRequestTmp.get("work_request_code"); // 工单请求流程
//            map_object.put("saveSubType", "request");
//        }
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        map.put("work_request_type", workRequestType);
//        Object facilityId = workRequestTmp.get("facility_id");
//        map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//        map.put("position_code", workRequestTmp.get("position_code")); // 所属位置
//        try {
//            Integer fid = Integer.valueOf((String) facilityId);
//        } catch (Exception fidExp) {
//            workRequestTmp.put("position_code", facilityId);
//            workRequestTmp.remove("facility_id");
//        }
//        String title = (String) workRequestTmp.get("title");
//        map.put("title_page", title);
//        // 根据数据库中状态判断是否为重复操作
//        try {
//            Map<String, Object> workRequestInfo = workSheetRequestService.queryWsqInfoById(schema_name, workRequestCode);
//            if ((Integer) workRequestInfo.get("status") > StatusConstant.DRAFT || !account.equals(workRequestInfo.get("create_user_account"))) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//        } catch (Exception e) {
//            logger.info(workRequestCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(workRequestTmp.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.NEW_K) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);//创建 成功
//        if (SensConstant.BUSINESS_NO_1.equals(businessTypeId)) {
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_ESCALATE_A_PROBLEM);//上报一个问题
//        }
//        map.put("sub_work_code", workRequestCode);
//        int status = StatusConstant.DRAFT;//状态
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            workRequestTmp.put("create_user_account", account);
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workRequestCode", workRequestCode);
//            returnType = returnContent; // 保存
//        } else if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            status = StatusConstant.TO_BE_CONFIRM; // 提交状态
//            map.put("facility_id", facilityId); // 所属组织
//            map.put("position_code", workRequestTmp.get("position_code")); // 所属位置
//            String nextStatus = (String) map_object.get("next_status");
//            if (null != nextStatus && !"".equals(nextStatus)) {
//                status = Integer.valueOf(nextStatus);
//            }
//            msgCode = qcloudsmsConfig.SMS_10003001;
//            String receive_account = (String) map_object.get("next_operator");
//            if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                map.put("relation_id", (String) workRequestTmp.get("relation_id"));
//                receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//            }
//            map.put("receive_account", receive_account);
//            map.put("create_user_account", account);//发送短信人
//            map.put("businessType", workTypeId);     //工单
//            map.put("deadline_time", workRequestTmp.get("deadline_time"));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//            // 页面【发生时间】为空时，自动赋值
//            if (workRequestTmp.containsKey("occur_time")) {
//                if (null == workRequestTmp.get("occur_time") || "".equals(workRequestTmp.get("occur_time"))) {
//                    workRequestTmp.put("varchar@occur_time", "occur_time");
//                    workRequestTmp.remove("null@occur_time");
//                    workRequestTmp.put("occur_time", now);
//                } else {
//                    map.put("create_time", workRequestTmp.get("occur_time"));
//                }
//            }
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        if (null == workRequestType || !"confirm".equals(workRequestType)) {
//            status = StatusConstant.PENDING; // 保存状态
//        }
//        map.put("status", String.valueOf(status));
//        workRequestTmp.put("status", status);
//        workRequestTmp.put("create_user_account", account);
//        //处理工单表的创建人和创建时间
//        workRequestTmp.put("varchar@create_time", "create_time");
//        workRequestTmp.put("create_time", now);
//        // 重新绑定值
//        dataInfo.put("_sc_work_request", workRequestTmp);
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("work_request_code", workRequestCode);
//
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", workRequestTmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", workRequestTmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        try {
//        if (null == workRequestType || !"confirm".equals(workRequestType)) {
//            List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//            dataList.add(workRequestTmp);
//            selectOptionService.doBatchInsertSql(schema_name, "_sc_work_request", SqlConstant._sc_work_request_columns, dataList);
//            workSheetHandleService.updateWorkDutymanHourInfoByCode(schema_name, (String) map_object.get("subWorkCode"), workRequestCode);
//        } else {
//            workSheetHandleService.doSaveData(schema_name, map_object);
//        }
//        if (null != workRequestType && "confirm".equals(workRequestType) && is_submit) {
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, null, workRequestCode, map_object);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//            if (!workflowStartResult.isStarted()) {
//                throw new SenscloudException(doType + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//流程启动失败
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                    account, accountName, title, doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, workRequestCode, status, account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workRequestInfo) {
////                workSheetRequestService.doRollBackData(schema_name, workRequestCode, workRequestInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 工单请求确认
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel confirmWsq(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//
//        String wkPageType = (String) map_object.get("wkPageType");
//        if (RegexUtil.isNull(wkPageType) || "1".equals(wkPageType)) {
//            return this.confirmWsqWorkOrClose(schema_name, user, map_object);
//        }
//        if ("2".equals(wkPageType)) {
//            return this.goToApproveWsq(schema_name, user, map_object);
//        }
//        if ("3".equals(wkPageType)) {
//            return this.changeWsqOperator(schema_name, user, map_object);
//        }
//        if ("4".equals(wkPageType)) {
//            return this.confirmWsqWorkOrRtn(schema_name, user, map_object);
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.LOG_ERROR_IN_WORKSHEET));//当前工单信息有错误
//    }
//
//    /**
//     * 工单请求确认（生成工单、完成关闭、无效关闭）
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel confirmWsqWorkOrClose(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        String msgCode = null; // 消息编码
//        String account = user.getAccount();
//        Object returnType = "ok"; // 非保存
//
//        Boolean is_submit = false;//是否提交
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String taskId = request.getParameter("task_id");//taskId
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> worksTmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        String workRequestCode = (String) worksTmp.get("work_request_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, workRequestCode);
//        map_object.put("work_request_code", workRequestCode);
//        String workAssignType = (String) map_object.get("work_assign_type");
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workRequestInfo = workSheetRequestService.queryWsqInfoById(schema_name, workRequestCode);
//        String workTypeId = String.valueOf(workRequestInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_T) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);
//
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        Object facilityId = worksTmp.get("facility_id");
//        map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//        map.put("position_code", worksTmp.get("position_code")); // 所属位置
//        String title = (String) worksTmp.get("title");
//        map.put("title_page", title);
//        String subWorkCode = (String) worksDetailTmp.get("sub_work_code");
//        String workCode = (String) worksTmp.get("work_code");
//        String receive_account = (String) worksDetailTmp.get("receive_account");
//        String relationId = (String) worksDetailTmp.get("relation_id"); // 对象
//        int status = StatusConstant.TO_BE_CONFIRM;//状态
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workCode", workCode);
//            returnContent.put("subWorkCode", subWorkCode);
//            returnContent.put("workRequestCode", workRequestCode);
//            returnType = returnContent; // 保存
//            workRequestTmp.remove("int@status");
//        } else if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            // 创建工单
//            if ("1".equals(workAssignType)) {
//                workRequestTmp.put("status", StatusConstant.PENDING);
//                String nextOperatorTask = (String) map_object.get("next_operator_task");
//                if (RegexUtil.isNotNull(nextOperatorTask)) {
//                    receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//                }
//                String nextStatus = (String) map_object.get("next_status");
//                // 处理接收人和接受时间
//                if (!StringUtil.isEmpty(receive_account) && !"-1".equals(receive_account)) {
//                    status = StatusConstant.PENDING;//保存状态
//                    if (null != nextStatus && !"".equals(nextStatus)) {
//                        status = Integer.valueOf(nextStatus);
//                    }
//                    map.put("receive_account", receive_account);
//                    worksDetailTmp.put("varchar@receive_time", "receive_time");
//                    worksDetailTmp.put("receive_time", now);
//                    worksDetailTmp.put("varchar@distribute_time", "distribute_time");
//                    worksDetailTmp.put("distribute_time", now);
//                    msgCode = qcloudsmsConfig.SMS_10003007;
//                    String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                    // 维修时
//                    logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_T) + doType + selectOptionService.getLanguageInfo(LangConstant.SUCC_A) + "，" + selectOptionService.getLanguageInfo(LangConstant.ASSIGNED_TO) + "：" + receiveAccountName;//分配 成功，负责人
//                } else {
//                    msgCode = qcloudsmsConfig.SMS_10003005;
//                    status = StatusConstant.UNDISTRIBUTED;//无分配人下一步开始分配
//                    if (null != nextStatus && !"".equals(nextStatus)) {
//                        status = Integer.valueOf(nextStatus);
//                    }
//                    receive_account = (String) map_object.get("next_operator");
//                    if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                        receive_account = null;
//                    }
//                    map.put("receive_account", receive_account);
//                }
//                map.put("create_user_account", account);//发送短信人
//                map.put("roleIds", map_object.get("roleIds"));
//            } else if ("2".equals(workAssignType)) {
//                // 完成关闭
//                do_flow_key = String.valueOf(ButtonConstant.BTN_CLOSE);
//                status = StatusConstant.OFFLINE_COMPLETED_CLOSE;
//                workRequestTmp.put("status", status);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CLOSE_C) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//关闭了 任务
//                msgCode = qcloudsmsConfig.SMS_10003004;
//                map.put("receive_account", String.valueOf(workRequestInfo.get("create_user_account")));//接收人账号
//            } else if ("3".equals(workAssignType)) {
//                // 无效关闭
//                do_flow_key = String.valueOf(ButtonConstant.BTN_CLOSE);
//                status = StatusConstant.INVALID_CLOSE;
//                workRequestTmp.put("status", status);
//                logOperation = selectOptionService.getLanguageInfo(LangConstant.CLOSE_C) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA);//关闭了 任务
//                msgCode = qcloudsmsConfig.SMS_10003004;
//                map.put("receive_account", String.valueOf(workRequestInfo.get("create_user_account")));//接收人账号
//            }
//
//            map.put("businessType", String.valueOf(worksDetailTmp.get("work_type_id")));     //工单
//            map.put("deadline_time", worksTmp.get("deadline_time"));
//            map.put("status", String.valueOf(status));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 页面【发生时间】为空时，自动赋值
//        if (worksTmp.containsKey("occur_time")) {
//            if (null == worksTmp.get("occur_time") || "".equals(worksTmp.get("occur_time"))) {
//                worksTmp.put("varchar@occur_time", "occur_time");
//                worksTmp.remove("null@occur_time");
//                worksTmp.put("occur_time", now);
//            } else {
//                map.put("create_time", worksTmp.get("occur_time"));
//            }
//        }
//        worksTmp.put("status", status);
//        worksTmp.put("create_user_account", workRequestInfo.get("create_user_account"));
//        //处理工单表的创建人和创建时间
//        worksTmp.put("varchar@create_time", "create_time");
//        worksTmp.put("create_time", now);
//        worksDetailTmp.put("status", status);//无分配人下一步开始分配
//        try {
//            Integer fid = Integer.valueOf((String) facilityId);
//        } catch (Exception fidExp) {
//            worksTmp.put("position_code", facilityId);
//            worksTmp.remove("facility_id");
//            workRequestTmp.put("position_code", facilityId);
//            workRequestTmp.remove("facility_id");
//        }
//        // 重新绑定值
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//            dataInfo.put("_sc_works", worksTmp);
//            dataInfo.put("_sc_works_detail", worksDetailTmp);
//        } else {
//            Map<String, String> keys = new HashMap<String, String>();
//            keys.put("_sc_work_request", "work_request_code");
//            map_object.put("keys", keys);
//            dataInfo = new HashMap<String, Object>();
//            dataInfo.put("_sc_work_request", workRequestTmp);
//        }
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("saveSubType", "request");
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", worksTmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", worksTmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        String flow_data = workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            // 多个对象
//            if (RegexUtil.isNotNull(relationId)) {
//                if ("1".equals(workAssignType)) {
//                    workSheetHandleService.insertWorkAssetOrg(schema_name, subWorkCode, relationId); // 工单设备组织处理
//                }
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////                String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
//            }
//            if (StringUtil.isNotEmpty(receive_account) && StringUtil.isNotEmpty(flow_data)) {
//                map.put("flow_data", flow_data);
//            }
//            map.put("do_flow_key", do_flow_key);
//
//
////            boolean isWorkOnly = false;
//            if ("1".equals(workAssignType)) {
//                String workRequestType = (String) map_object.get("work_request_type");
//                if (null != workRequestType && "workOnly".equals(workRequestType)) {
//                    String processDefinitionId = selectOptionService.getOptionNameByCode(schema_name, "work_type", workTypeId, "relation");
//                    map.put("sub_work_code", subWorkCode);
//                    WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                    if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                        throw new SenscloudException(doType + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//流程启动失败
////                        isWorkOnly = true;
//                    }
//                }
//            }
//
////            if (!isWorkOnly) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, workRequestCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, workRequestCode, map_object);
//            map.put("sub_work_code", workRequestCode);
//            boolean workFlowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workFlowResult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                    account, user.getUsername(), title, doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, workRequestCode, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
////            }
////        } else {
////            logOperation = "进行了一次" + doType + "保存";
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workInfo && null != dtlInfo) {
////                workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            }
////            if (null != workRequestInfo) {
////                workSheetRequestService.doRollBackData(schema_name, workRequestCode, workRequestInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
//
//    /**
//     * 工单请求确认（前往生成工单等页）
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel goToApproveWsq(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        Object returnType = "ok"; // 非保存
//        Boolean is_submit = false;//是否提交
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        Object facilityId = workRequestTmp.get("facility_id");
//        try {
//            Integer fid = Integer.valueOf((String) facilityId);
//        } catch (Exception fidExp) {
//            workRequestTmp.put("position_code", facilityId);
//            workRequestTmp.remove("facility_id");
//        }
//        String account = user.getAccount();
//        String workRequestCode = (String) workRequestTmp.get("work_request_code");
//        map_object.put("work_request_code", workRequestCode);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, workRequestCode);
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workRequestInfo = workSheetRequestService.queryWsqInfoById(schema_name, workRequestCode);
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workRequestCode", workRequestCode);
//            returnType = returnContent; // 保存
//        } else if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 重新绑定值
//        dataInfo.put("_sc_work_request", workRequestTmp);
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("saveSubType", "request");
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            Map map = new HashMap();
//            map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//            map.put("position_code", workRequestTmp.get("position_code")); // 所属位置
//            String relationId = (String) workRequestTmp.get("relation_id"); // 对象
//            String receive_account = null;
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//            }
//            // 处理接收人和接受时间
//            if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                receive_account = (String) map_object.get("next_operator");
//                if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                    map.put("relation_id", relationId);
//                    receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//                }
//            }
//            String status = (String) workRequestTmp.get("status");
//            String workTypeId = String.valueOf(workRequestTmp.get("work_type_id"));
//            if (RegexUtil.isNull(workTypeId)) {
//                workTypeId = String.valueOf(workRequestInfo.get("work_type_id"));
//            }
//            map.put("receive_account", receive_account);
//            map.put("create_user_account", account);//发送短信人
//            map.put("roleIds", map_object.get("roleIds"));
//            map.put("businessType", workTypeId);     //工单
//            map.put("status", status);
//            map.put("work_assign_type", map_object.get("work_assign_type"));
//            map.put("deadline_time", workRequestTmp.get("deadline_time"));
//            // 多个对象
////            if (RegexUtil.isNotNull(relationId)) {
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////                String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
////            }
//            map.put("do_flow_key", do_flow_key);
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, workRequestCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, null, workRequestCode, map_object);
//            map.put("sub_work_code", workRequestCode);
//            String taskId = request.getParameter("task_id");//taskId
//            boolean workFlowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workFlowResult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//            String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//            String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//            doType = selectOptionService.getLanguageInfo(doType);
//            String remark = (String) map_object.get("remark");
//            remark = RegexUtil.isNull(remark) ? "" : "：" + remark;
//            String logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + remark;//确认了 任务
//            String disMsg = (String) map_object.get("disMsg");
//            if (RegexUtil.isNull(disMsg)) {
//                String msgReceiveAccount = (String) map.get("receive_account");
//                String title = (String) workRequestTmp.get("title");
//                messageService.msgPreProcess(qcloudsmsConfig.SMS_10003008, msgReceiveAccount, businessTypeId, workRequestCode,
//                        account, user.getUsername(), title, doType); // 发送短信
//            }
//            workProcessService.saveWorkProccess(schema_name, workRequestCode, Integer.valueOf(status), account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
//        }
//        return ResponseModel.ok(returnType);
//    }
//
//    /**
//     * 工单请求确认（转派、转专家）
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel changeWsqOperator(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        Object returnType = "ok"; // 非保存
//        Boolean is_submit = false;//是否提交
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        Object facilityId = workRequestTmp.get("facility_id");
//        try {
//            Integer fid = Integer.valueOf((String) facilityId);
//        } catch (Exception fidExp) {
//            workRequestTmp.put("position_code", facilityId);
//            workRequestTmp.remove("facility_id");
//        }
//        String account = user.getAccount();
//        String workRequestCode = (String) workRequestTmp.get("work_request_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, workRequestCode);
//        map_object.put("work_request_code", workRequestCode);
//        String nextOperator = (String) map_object.get("next_operator");
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SAVE))) {//保存按钮
//            Map<String, Object> returnContent = new HashMap<String, Object>(); // 保存时返回内容
//            returnContent.put("type", "editWork");
//            returnContent.put("workRequestCode", workRequestCode);
//            returnType = returnContent; // 保存
//        } else if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {//提交按钮
//            is_submit = true;//提交事件
//            if (null == nextOperator || "-1".equals(nextOperator)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PL_DIST_MAN));//请分配一个负责人
//            }
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 重新绑定值
//        dataInfo.put("_sc_work_request", workRequestTmp);
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("saveSubType", "request");
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            Map map = new HashMap();
//            String status = (String) workRequestTmp.get("status");
//            String workTypeId = String.valueOf(workRequestTmp.get("work_type_id"));
//            map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//            map.put("position_code", workRequestTmp.get("position_code")); // 所属位置
//            map.put("receive_account", nextOperator); // 短信接收人
//            map.put("create_user_account", account);//发送短信人
//            map.put("roleIds", map_object.get("roleIds"));
//            map.put("businessType", workTypeId);     //工单
//            map.put("status", status);
//            map.put("deadline_time", workRequestTmp.get("deadline_time"));
////            String relationId = (String) workRequestTmp.get("relation_id"); // 对象
//            // 多个对象
////            if (RegexUtil.isNotNull(relationId)) {
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////                String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
////            }
//            map.put("do_flow_key", do_flow_key);
//            map.put("sub_work_code", workRequestCode);
//
//            String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//            String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//            String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//            doType = selectOptionService.getLanguageInfo(doType);
//            String title = (String) workRequestTmp.get("title");
//            String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", nextOperator, null);
//            String logOperation = selectOptionService.getLanguageInfo(LangConstant.WM_T) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + selectOptionService.getLanguageInfo(LangConstant.LOG_GIVE) + receiveAccountName;
//            Boolean isSuccess = workflowService.assignWithSWC(schema_name, workRequestCode, nextOperator, map);
//            if (!isSuccess) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            messageService.msgPreProcess(qcloudsmsConfig.SMS_10003008, nextOperator, businessTypeId, workRequestCode,
//                    account, user.getUsername(), title, doType); // 发送短信
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
//        }
//        return ResponseModel.ok(returnType);
//    }
//
//    /**
//     * 工单请求确认（生成工单、退回）
//     *
//     * @param schema_name
//     * @param user
//     * @param map_object
//     * @return
//     * @throws Exception
//     */
//    private ResponseModel confirmWsqWorkOrRtn(String schema_name, User user, Map<String, Object> map_object) throws Exception {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String msgCode = null; // 消息编码
//        String account = user.getAccount();
//        Object returnType = "ok"; // 非保存
//        Boolean is_submit = false;//是否提交
//        String taskId = request.getParameter("task_id");//taskId
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> worksDetailTmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> worksTmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        Map<String, Object> workRequestTmp = (Map<String, Object>) dataInfo.get("_sc_work_request");
//        Object facilityId = worksTmp.get("facility_id");
//        String workRequestCode = (String) worksTmp.get("work_request_code");
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, workRequestCode);
//        map_object.put("work_request_code", workRequestCode);
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> workRequestInfo = workSheetRequestService.queryWsqInfoById(schema_name, workRequestCode);
//        String workTypeId = String.valueOf(workRequestInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        String logOperation = null;
//
//        //获取当前时间
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        Map map = new HashMap();
//        map.put("facility_id", String.valueOf(facilityId)); // 所属位置
//        map.put("position_code", worksTmp.get("position_code")); // 所属位置
//        String title = (String) worksTmp.get("title");
//        String subWorkCode = (String) worksDetailTmp.get("sub_work_code");
//        String workCode = (String) worksTmp.get("work_code");
//        String receive_account = (String) worksDetailTmp.get("receive_account");
//        String relationId = (String) worksDetailTmp.get("relation_id"); // 对象
//        int status = StatusConstant.TO_BE_CONFIRM;//状态
//        // 退回按钮
//        if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            is_submit = true;//提交事件
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//            }
//            msgCode = qcloudsmsConfig.SMS_10003012;
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_BACK) + doType + selectOptionService.getLanguageInfo(LangConstant.LOG_TASK_AGAIN) + selectOptionService.getLanguageInfo(LangConstant.HANDLE_A);//任务，需重新处理
//            // 同意按钮
//        } else if (RegexUtil.isNotNull(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {
//            is_submit = true;//提交事件
//            msgCode = qcloudsmsConfig.SMS_10003005;
//            String nextOperatorTask = (String) map_object.get("next_operator_task");
//            if (RegexUtil.isNotNull(nextOperatorTask)) {
//                receive_account = workProcessService.getOperateAccountByTaskId(schema_name, workRequestCode, nextOperatorTask);
//            }
//            // 处理接收人和接受时间
//            if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                receive_account = (String) map_object.get("next_operator");
//                if (RegexUtil.isNull(receive_account) || "-1".equals(receive_account)) {
//                    map.put("relation_id", relationId);
//                    receive_account = commonUtilService.getRandomUserByInfoForWork(schema_name, receive_account, map, map_object);
//                }
//            }
//            String remark = (String) map_object.get("remark");
//            remark = RegexUtil.isNull(remark) ? "" : "：" + remark;
//            logOperation = selectOptionService.getLanguageInfo(LangConstant.CONFIRM) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + remark;//确认了 任务
//        } else {
//            //表单数据有误，返回失败提示
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//        }
//        // 页面【发生时间】为空时，自动赋值
//        if (worksTmp.containsKey("occur_time")) {
//            if (null == worksTmp.get("occur_time") || "".equals(worksTmp.get("occur_time"))) {
//                worksTmp.put("varchar@occur_time", "occur_time");
//                worksTmp.remove("null@occur_time");
//                worksTmp.put("occur_time", now);
//            } else {
//                map.put("create_time", worksTmp.get("occur_time"));
//            }
//        }
//        worksTmp.put("status", status);
//        worksTmp.put("create_user_account", workRequestInfo.get("create_user_account"));
//        //处理工单表的创建人和创建时间
//        worksTmp.put("varchar@create_time", "create_time");
//        worksTmp.put("create_time", now);
//        worksDetailTmp.put("status", status);//无分配人下一步开始分配
//        try {
//            Integer fid = Integer.valueOf((String) facilityId);
//        } catch (Exception fidExp) {
//            worksTmp.put("position_code", facilityId);
//            worksTmp.remove("facility_id");
//            workRequestTmp.put("position_code", facilityId);
//            workRequestTmp.remove("facility_id");
//        }
//        // 重新绑定值
//        if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {
//            dataInfo.put("_sc_works", worksTmp);
//            dataInfo.put("_sc_works_detail", worksDetailTmp);
//        } else {
//            Map<String, String> keys = new HashMap<String, String>();
//            keys.put("_sc_work_request", "work_request_code");
//            map_object.put("keys", keys);
//            dataInfo = new HashMap<String, Object>();
//            dataInfo.put("_sc_work_request", workRequestTmp);
//        }
//        map_object.put("dataInfo", dataInfo);
//        map_object.put("saveSubType", "request");
//        // 是否会生成子工单，若无分配人则不生成
//        if (map_object.containsKey("is_have_sub") && is_submit) {
//            map_object.put("title_page", title);
//            map_object.put("facility_id", facilityId); // 所属位置
//            map_object.put("deadline_time", worksTmp.get("deadline_time")); // 截止时间
//            map_object.put("occur_time", worksTmp.get("occur_time")); // 发送时间
//        } else {
//            map_object.remove("is_have_sub");
//        }
////        Map<String, Object> dtlInfo = workSheetHandleService.queryDetailBySubWorkCode(schema_name, subWorkCode);
////        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, workCode);
////        try {
//        String flow_data = workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        if (is_submit) {
//            // 多个对象
//            if (RegexUtil.isNotNull(relationId)) {
//                workSheetHandleService.insertWorkAssetOrg(schema_name, subWorkCode, relationId); // 工单设备组织处理
////                String categoryIdStr = selectOptionService.getOptionNameByCode(schema_name, "asset", relationId, "category_id");
////                String noneStr = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////                if (RegexUtil.isNotNull(categoryIdStr) && !noneStr.equals(categoryIdStr)) {
////                    map.put("categoryId", categoryIdStr);
////                }
//            }
//            if (StringUtil.isNotEmpty(receive_account) && StringUtil.isNotEmpty(flow_data)) {
//                map.put("flow_data", flow_data);
//            }
//            map.put("do_flow_key", do_flow_key);
//            map.put("receive_account", receive_account);
//            map.put("create_user_account", account);//发送短信人
//            map.put("roleIds", map_object.get("roleIds"));
//            map.put("businessType", map_object.get("work_type_id"));     //工单
//            map.put("deadline_time", worksTmp.get("deadline_time"));
//            map.put("status", String.valueOf(status));
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String nowDateWord = simleDateFormat.format(now);
//            map.put("create_time", nowDateWord);
//            map.put("title_page", title);
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {
//                workTypeId = String.valueOf(worksDetailTmp.get("work_type_id"));
//                String processDefinitionId = selectOptionService.getOptionNameByCode(schema_name, "work_type", workTypeId, "relation");
//                map.put("sub_work_code", subWorkCode);
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new SenscloudException(doType + selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_FLOW));//流程启动失败
//                }
//            }
//
////            if (!isWorkOnly) {
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, workRequestCode);
//            dynamicCommonService.doWorkSubmitCommonWithRequest(schema_name, subWorkCode, workRequestCode, map_object);
//            map.put("sub_work_code", workRequestCode);
//            boolean workFlowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workFlowResult) {
//                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            String msgReceiveAccount = (String) map.get("receive_account");
//            String msgSubWorkCode = (String) map.get("sub_work_code");
//            messageService.msgPreProcess(msgCode, msgReceiveAccount, businessTypeId, msgSubWorkCode,
//                    account, user.getUsername(), title, doType); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, workRequestCode, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, pageType, workRequestCode, logOperation, account);//记录历史
////            }
////        } else {
////            logOperation = "进行了一次" + doType + "保存";
//        }
//        return ResponseModel.ok(returnType);
////        } catch (Exception exp) {
////            if (null != workInfo && null != dtlInfo) {
////                workSheetHandleService.doRollBackData(schema_name, workCode, subWorkCode, workInfo.get("status"), dtlInfo.get("status"));
////            }
////            if (null != workRequestInfo) {
////                workSheetRequestService.doRollBackData(schema_name, workRequestCode, workRequestInfo.get("status"));
////            }
////            throw new Exception(exp);
////        }
//    }
}
