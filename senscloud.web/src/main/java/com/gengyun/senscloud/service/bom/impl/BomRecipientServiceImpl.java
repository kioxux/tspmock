package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomRecipientService;
import org.springframework.stereotype.Service;

/**
 * 备件领用
 */
@Service
public class BomRecipientServiceImpl implements BomRecipientService {
//
//
//    @Autowired
//    BomAllotMapper bomAllotMapper;
//
//    @Autowired
//    private BomRecipientMapper bomRecipientMapper;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private CommonUtilService commonUtilService;
//
//    @Autowired
//    private DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    private LogsService logService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private BomStockListService bomStockListService;
//
//    @Autowired
//    private WorkProcessService workProcessService;
//
//    @Autowired
//    private PagePermissionService pagePermissionService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//
//    @Autowired
//    SelectOptionCustomMapper selectOptionCustomMapper;
//
//    @Autowired
//    BomInStockService bomInStockService;
//
//    @Autowired
//    MessageService messageService;
//
//    /**
//     * 获取备件领用列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBomRecipientList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String[] facilitySearch = {};
//        try {
//            facilitySearch = request.getParameterValues("facilities")[0].split(",");
//        } catch (Exception ex) {
//
//        }
//        String status = request.getParameter("status");
//        String keyWord = request.getParameter("keyWord");
//
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        User user = AuthService.getLoginUser(request);
//        //根据设备组织查询条件，查询用户可见的库房，用于查询备件
//        String stockPermissionCondition = dataPermissionForFacility.getStockPermissionByFacilityAndGroup(schema_name, user, facilitySearch, "stock", null);
//
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (s.stock_name like '%" + keyWord + "%' or upper(re.recipient_code) like upper('%" + keyWord +
//                    "%') or u.username like '%" + keyWord + "%' )";
//        }
//        if (StringUtils.isNotBlank(status)) {
//            condition += " and re.status = " + status;
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        dataList = bomRecipientMapper.getBomRecipientList(schema_name, condition, stockPermissionCondition, pageSize, begin);
//        int total = bomRecipientMapper.countBomRecipientList(schema_name, condition, stockPermissionCondition);
//        handlerFlowData(schema_name, user.getAccount(), dataList);
//        //多时区转换
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 根据主键查询备件领用数据
//     *
//     * @param schemaName
//     * @param recipientCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryBomRecipientById(String schemaName, String recipientCode) {
//        return bomRecipientMapper.queryBomRecipientById(schemaName, recipientCode);
//    }
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//            Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//            if (null != map_object && map_object.size() > 0) {
//                Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//                Map<String, Object> businessInfo = (Map<String, Object>) dataInfo.get("_sc_bom_recipient");
//                List<Map<String, Object>> recipientDetailList = (List<Map<String, Object>>) map_object.get("bomContent3");
//                JSONArray recipientDetails = JSONArray.fromObject(map_object.get("bomContent3"));
//                if (recipientDetailList == null || recipientDetailList.size() == 0) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SELECT_RECIPIENT_BOM));//请选择领用备件。
//                }
//                Map<String, Object> keys = (Map<String, Object>) map_object.get("keys");
//                String key = (String) keys.get("_sc_bom_recipient");
//                boolean isNew = key.startsWith(SqlConstant.IS_NEW_DATA) ? true : false;//判断是否是新增
//                String work_request_type = "";
//                try {
//                    work_request_type = (String) map_object.get("work_request_type");
//                } catch (Exception statusExp) {
//                }
//                boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//                String roleIds = (String) map_object.get("roleIds");
//
//                return addBomRecipient(schema_name, doFlowKey, user, processDefinitionId, roleIds, businessInfo, isNew, hasAudit, recipientDetailList, recipientDetails);
//            }
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    private ResponseModel addBomRecipient(String schema_name, String doFlowKey, User user, String processDefinitionId,
//                                          String roleIds, Map<String, Object> businessInfo, boolean isNew, boolean hasAudit, List<Map<String, Object>> recipientDetailList,
//                                          JSONArray recipientDetails) throws Exception {
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && Integer.valueOf(doFlowKey) == ButtonConstant.BTN_SUBMIT) {
//            int status;//状态
//            if (hasAudit) {
//                status = StatusConstant.TO_BE_AUDITED;
//            } else {
//                status = StatusConstant.COMPLETED;//如果流程不需要审核，则订单状态为“已完成”
//            }
//
//            String account = user.getAccount();
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            String code = (String) businessInfo.get("recipient_code");//领用编号
//            businessInfo.put("status", status);
//            businessInfo.put("create_user_account", account);
//            businessInfo.put("create_time", now);
//            List<Map<String, Object>> detailList = new ArrayList<Map<String, Object>>();
//            List<String> bomCodeList = new ArrayList<>();
//            if (code != null && !"".equals(code)) {
//                if (recipientDetailList != null && recipientDetailList.size() > 0) {
//                    for (Map<String, Object> rec : recipientDetailList) {
//                        Map<String, Object> detail = new HashMap<String, Object>();
//                        if (rec.get("use_count") == null || rec.get("use_count").equals("")) {
//                            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SELECT_BOM_RECIPIENT_COUNT));//请选择领用备件数量。
//                        }
//                        detail.put("price", rec.get("price"));
//                        detail.put("currency_id", rec.get("currency_id"));
//                        detail.put("recipient_code", code);
//                        detail.put("bom_code", rec.get("bom_code"));
//                        detail.put("material_code", rec.get("material_code"));
//                        detail.put("quantity", rec.get("use_count"));
//                        detailList.add(detail);
//                        bomCodeList.add((String) rec.get("bom_code"));
//                    }
//                }
//            }
//            String log_title = "";
//            if (isNew) {
//                log_title = selectOptionService.getLanguageInfo(LangConstant.ADD_BOM_RECIPIENT);//创建备件领用
//                //临时处理应对组织id不为数字--zys
//                if (businessInfo.containsKey("client_org_id")) {
//                    try {
//                        int client_org_id = (Integer) businessInfo.get("client_org_id");
//                    } catch (Exception e) {
//                        businessInfo.put("client_org_id", -1);
//                    }
//                }
//                bomRecipientMapper.insert(schema_name, businessInfo); //保存备件领用主表信息
//                selectOptionService.doBatchInsertSql(schema_name, "_sc_bom_recipient_detail", SqlConstant._sc_bom_recipient_detail_columns, detailList);//批量领用备件
//            } else {
//                log_title = selectOptionService.getLanguageInfo(LangConstant.UPDATE_BOM_RECIPIENT);//修改备件领用
//                bomRecipientMapper.update(schema_name, businessInfo); // 修改
//                bomRecipientMapper.deleRecipientDetail(schema_name, code);//删除备件编号对应的备件详情信息
//                //重新批量领用备件
//                selectOptionService.doBatchInsertSql(schema_name, "_sc_bom_recipient_detail", SqlConstant._sc_bom_recipient_detail_columns, detailList);
//            }
//            // 领用人是备件领用发起人；领用数量：
//            updateBomStockQuantity(schema_name, account, recipientDetails, businessInfo, hasAudit);
//
//            if (!hasAudit) {
//                int result = bomRecipientMapper.updateStatus(schema_name, status, code);//更改状态
//            }
//
//            String title = selectOptionService.getLanguageInfo(LangConstant.PART_REC); // 备件领用
//            Map map = new HashMap<>();
//            map.put("title_page", title);//备件领用
//            map.put("sub_work_code", code);
//            map.put("do_flow_key", doFlowKey);
//            map.put("status", status);
//            map.put("create_user_account", account);//发送短信人
//            map.put("create_time", UtilFuns.sysTime());
//            map.put("facility_id", null);
//            // 设置处理人（随机取一个库房管理人）
//            String receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) businessInfo.get("stock_code"));
//            map.put("receive_account", receive_account); // 短信接受人
//            if (isNew) {
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FLOW_START_FAIL));//备件入库流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, code, status, account, Constants.PROCESS_CREATE_TASK); // 进度记录
//            } else {
//                String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, code);
//                if (StringUtils.isBlank(taskId))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_RECIPIENT_FLOW_DATA_ERROR_APPLY_FAIL));//备件领用流程信息异常，备件领用申请失败
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, code);
//                boolean isSuccess = workflowService.complete(schema_name, taskId, account, map);
//                if (!isSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_RECIPIENT_FLOW_START_FAIL));//备件领用流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, code, status, account, taskSid); // 进度记录
//            }
//            messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_33, code,
//                    account, user.getUsername(), null, null); // 发送短信
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_33), code, log_title, account); // 记录历史
//            return ResponseModel.ok("ok");
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String result = this.findBomRecipientList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("recipient_code");
//
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        Map<String, Object> bomRecipient = bomRecipientMapper.queryBomRecipientById(schemaName, code);
//        if (!String.valueOf(StatusConstant.COMPLETED).equals(String.valueOf(bomRecipient.get("status")))) {//未完成的调拨单可以分配
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_33, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/bom_recipient/findSingleBrInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("recipient_code")));
//        if (subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        //把taskId、权限数据补充到列表数据中去
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("recipient_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//    @Override
//    public ResponseModel inStockRecipent(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, Boolean pass) throws Exception {
//        String title = "";
//        if (pass) {
//            title = selectOptionService.getLanguageInfo(LangConstant.BOM_RECIPIENT_STOCK_OUT);//备件领用出库
//        } else {
//            title = selectOptionService.getLanguageInfo(LangConstant.PART_REC);//备件领用
//        }
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_DATA_WRONG));//当前备件领用信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> bom_in_stock_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_recipient");
//        String approve_remark = (String) map_object.get("approve_remark");//意见
//        String recipient_code = (String) bom_in_stock_tmp.get("recipient_code");
//        String file_ids = (String) bom_in_stock_tmp.get("file_ids");//附件ID
//        String stock_code = (String) bom_in_stock_tmp.get("stock_code");//库房编号
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String body_property = request.getParameter("body_property");//按钮类型
//        String roleIds = (String) map_object.get("roleIds");
//        if (StringUtils.isBlank(recipient_code) || StringUtils.isBlank(do_flow_key))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//
//        Map<String, Object> bomRecipient = bomRecipientMapper.queryBomRecipientById(schema_name, recipient_code);
////                .queryBomInStockById(schema_name, recipient_code);
//        if (bomRecipient == null)
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_DATE_ERROR_AUDIT_FAIL), title, title));//%s信息异常，%s审核失败
//        if (pass && !String.valueOf(bomRecipient.get("status")).equals(String.valueOf(StatusConstant.TO_BE_AUDITED)) && !String.valueOf(bomRecipient.get("status")).equals(String.valueOf(StatusConstant.PENDING)))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_STATUS_ERROR_AUDIT_FAIL), title, title));//%s状态异常，%s审核失败
//
//        String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, recipient_code);
//        if (StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_FLOW_ERROR_AUDIT_FAIL), title, title));//%s流程信息异常，%s审核失败
//
//        int result = 0;
//        int status = 0;
//        if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {//如果审核同意，则更新调拨申请附件
//            //评审同样需要更新body_property 评审
//            List<Map<String, Object>> bom_detail = bomRecipientMapper.getAllBomRecipientDetail(schema_name, recipient_code);
//            if (pass) {
//                //审核确定
//                status = StatusConstant.COMPLETED;
//                result = bomRecipientMapper.updateBomRecipientStatus(schema_name, file_ids, status, body_property, recipient_code);//更新附件，修改状态为“已完成”
//                if (!bom_detail.isEmpty()) {
//                    for (Map<String, Object> bom : bom_detail) {
//                        float use_count_f = (float) bom.get("quantity");
//                        int use_count = (int) use_count_f;//备件申领数量
//                        String bom_code = (String) bom.get("bom_code");
//                        String material_code = (String) bom.get("material_code");
//                        Map<String, Object> businessInfo = new HashMap<String, Object>();
//                        businessInfo.put("bom_code", bom_code);
//                        businessInfo.put("material_code", material_code);
//                        businessInfo.put("stock_code", stock_code);
//                        businessInfo.put("isAddStock", "isAddStock");
//                        businessInfo.put("security_quantity", 0);
//                        businessInfo.put("max_security_quantity", 0);
//                        //领用时，入库数量为负数
//                        float quan = (float) bom.get("quantity");
//                        int quan_int = (int) quan;
//                        businessInfo.put("quantity", "-" + quan_int);
//                        String quantity = (String) businessInfo.get("quantity"); // 出库数量
//                        bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_33, businessInfo, (String) bomRecipient.get("create_user_account"), selectOptionService.getLanguageInfo(LangConstant.BOM_RECIPIENT_QUANTITY) + quantity.replace("-", "")); // 领用人是备件领用发起人；领用数量：
//                    }
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_RECIPIENT_INFO_WRONG));//无法确定领用备件信息
//                }
//            } else {
//                result = bomRecipientMapper.updateBomRecipientProperty(schema_name, file_ids, body_property, recipient_code);
//
//            }
//        } else if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {//如果审核退回，则调拨申请状态变更为“处理中”
//            status = StatusConstant.PENDING;
//            result = bomRecipientMapper.updateStatus(schema_name, status, recipient_code);
//        }
//
//        String title_page = "";
//        String logRemark = "";
//        if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//            title_page = title + selectOptionService.getLanguageInfo(LangConstant.RETURN_A);//退回
//            logRemark = title + selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK);//审核退回
//        } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//            title_page = title + selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);//成功
//            logRemark = title + selectOptionService.getLanguageInfo(LangConstant.AUDIT_SUCC);//审核成功
//        }
//        //完成流程
//        Map<String, String> map = new HashMap<>();
//        map.put("sub_work_code", recipient_code);
//        map.put("title_page", title_page);
//        map.put("do_flow_key", do_flow_key);
//
//        if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//            map.put("receive_account", (String) bomRecipient.get("create_user_account"));//短信接受人
//        } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//            // 设置处理人（随机取一个库房管理人）
//            String receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, stock_code);
//            map.put("receive_account", receive_account); // 短信接受人
//        }
//        if (approve_remark != null && !approve_remark.equals("")) {
//            logRemark = logRemark + "(" + selectOptionService.getLanguageInfo(LangConstant.WM_OA) + "：" + approve_remark + ")";//意见
//        }
//        String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, recipient_code);
//        boolean isSuccess = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//        if (isSuccess) {
//            workProcessService.saveWorkProccess(schema_name, recipient_code, status, user.getAccount(), taskSid);//工单进度记录
//            messageService.msgPreProcess(ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key) ? qcloudsmsConfig.SMS_10002006 : qcloudsmsConfig.SMS_10002005, map.get("receive_account"), SensConstant.BUSINESS_NO_33, recipient_code,
//                    user.getAccount(), user.getUsername(), null, null); // 发送短信
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_33), recipient_code, logRemark, user.getAccount());
//            return ResponseModel.ok("ok");
//        } else {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_AUDIT_ERROR));//审核出现错误，请重试
//        }
//    }
//
//    @Override
//    public ResponseModel cancel(String schema_name, User user, String in_code) {
//        int doUpdate = bomRecipientMapper.updateStatus(schema_name, StatusConstant.CANCEL, in_code);
//        if (doUpdate > 0) {
//            boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, in_code, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            if (deleteflow) {
//                String account = user.getAccount();
//                workProcessService.saveWorkProccess(schema_name, in_code, StatusConstant.CANCEL, account, Constants.PROCESS_CANCEL_TASK);//工单进度记录
//                logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31), in_code, selectOptionService.getLanguageInfo(LangConstant.SPARE_RECEIVE_CANCEL), account);//备件领用申请作废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SPARE_RECEIVE_CANCEL));//备件领用申请作废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_RECIPIENT_CANCEL_FLOW_START_FAIL));//备件领用作废流程调用失败
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_RECIPIENT_CANCEL_FAIL));//备件领用作废失败
//        }
//    }
//
//    /**
//     * 备件库房数量更新
//     *
//     * @param schema_name
//     * @param account
//     * @param bomInStockDetails
//     * @param bomInStock
//     * @param hasAudit
//     * @throws Exception
//     */
//    private void updateBomStockQuantity(String schema_name, String account, JSONArray bomInStockDetails, Map<String, Object> bomInStock, boolean hasAudit) throws Exception {
//        String security_quantity = (String) bomInStock.get("security_quantity");
//        String max_security_quantity = (String) bomInStock.get("max_security_quantity");
//        for (Object rowData : bomInStockDetails) {
//            net.sf.json.JSONObject bomRowJson = net.sf.json.JSONObject.fromObject(rowData);
//            updateBomStockQuantity(schema_name, account, "-" + bomRowJson.getString("use_count"), bomRowJson.getString("bom_code"), bomRowJson.getString("material_code"), (String) bomInStock.get("stock_code"), security_quantity, max_security_quantity, hasAudit);
//        }
//    }
//
//    /**
//     * 修改库存数量
//     *
//     * @param schema_name
//     * @param account
//     * @param quantity
//     * @param bom_code
//     * @param material_code
//     * @param stock_code
//     * @param security_quantity
//     * @param max_security_quantity
//     * @param hasAudit
//     * @throws Exception
//     */
//    private void updateBomStockQuantity(String schema_name, String account, String quantity, String bom_code, String material_code, String stock_code, String security_quantity, String max_security_quantity, boolean hasAudit) throws Exception {
//        Map<String, Object> bomRow = new HashMap<>();
//        bomRow.put("isAddStock", "isAddStock"); // 入库
//        bomRow.put("bom_code", bom_code);
//        bomRow.put("material_code", material_code);
//        bomRow.put("quantity", quantity);
//        bomRow.put("stock_code", stock_code);
//        bomRow.put("security_quantity", security_quantity);
//        bomRow.put("max_security_quantity", max_security_quantity);
//        if (hasAudit) {
//            bomStockListService.checkBomStockQuantity(schema_name, bomRow, null); // 更新备件库存数量;入库数量：
//        } else {
//            bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_33, bomRow, account, selectOptionService.getLanguageInfo(LangConstant.BOM_RECIPIENT_QUANTITY) + quantity.replace("-", "")); // 更新备件库存数量;入库数量：
//        }
//    }

}



