package com.gengyun.senscloud.service.business.impl;

import com.gengyun.senscloud.service.business.UserRoleService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {
//    @Autowired
//    UserRoleMapper userRoleMapper;
//    @Autowired
//    RolePermissionMapper rolePermissionMapper;
//    @Autowired
//    SystemPermissionService systemPermissionService;
//
//    /**
//     * 获取角色列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findRoleList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String keyWord = request.getParameter("searchKey");
//        StringBuffer condition = new StringBuffer();
//        if (null != keyWord && !"".equals(keyWord)) {
//            condition.append(" and name like '%");
//            condition.append(keyWord);
//            condition.append("%'");
//        }
//        Company company = AuthService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> userRoleList = userRoleMapper.getUserRoleList(schema_name, condition.toString(), pageSize, begin);
//        int total = userRoleMapper.getUserRoleCount(schema_name, condition.toString());
//        JSONObject result = new JSONObject();
//        result.put("rows", userRoleList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    @Override
//    public List<UserRole> findAll(String schema_name) {
//        return userRoleMapper.findAll(schema_name);
//    }
//
//    @Override
//    public List<UserRole> findAllCustomRole(String schema_name) {
//        return userRoleMapper.findAllCustomRole(schema_name);
//    }
//
//    @Override
//    public List<UserRole> findById(String schema_name, String roleId) {
//        List<UserRole> userRole = userRoleMapper.findById(schema_name, roleId);
//        if (null != userRole && !userRole.isEmpty() && userRole.size() > 0) {
//            for (int i = 0; i < userRole.size(); i++) {
//                LinkedHashMap<String, ISystemPermission> permissionSet = new LinkedHashMap<>();
//                String equalRole = "," + roleId + ",";
////        if (equalRole.indexOf(",'" + UserRole.ROLE_ID_SYSTEM_ADMIN + "',") >= 0) {
////            List<SystemPermission> list = systemPermissionService.findAll();
////            for (SystemPermission permission : list) {
////                permissionSet.put(permission.getId(), permission);
////            }
////        } else
//                if (equalRole.indexOf(",'" + UserRole.ROLE_ID_COMPANY_ADMIN + "',") >= 0 || equalRole.indexOf(",'" + UserRole.ROLE_ID_SYSTEM_ADMIN + "',") >= 0) {
//                    String strId = schema_name.replace("sc_com_", "");
//                    List<SystemPermission> list = systemPermissionService.findAllByCompanyId(Long.parseLong(strId), null);
//                    for (SystemPermission permission : list) {
//                        permissionSet.put(permission.getId(), permission);
//                    }
//                } else {
//                    String strId = schema_name.replace("sc_com_", "");
//                    List<Map<String, Object>> rolePermissons = rolePermissionMapper.getRolePermissions(schema_name, Long.parseLong(strId), roleId);
//                    for (Map<String, Object> permission : rolePermissons) {
//                        ISystemPermission permission1 = new SystemPermission();
//                        permission1.setId(permission.get("permissionid").toString());
//                        permission1.setName(permission.get("permissionname").toString());
//                        permission1.setKey(permission.get("permissionkey").toString());
//                        permissionSet.put(permission1.getId(), permission1);
//                    }
//                }
//                userRole.get(i).setPermissions(permissionSet);
//            }
//        }
//        return userRole;
//    }
//
//    /**
//     * 新增角色
//     *
//     * @param schema_name
//     * @param userRole
//     */
//    @Override
//    public void add(String schema_name, UserRole userRole) {
//        userRoleMapper.insertRole(schema_name, userRole);
//        rolePermissionMapper.deleteRolePermission(schema_name, userRole.getId());
//        userRoleMapper.deleteRoleAssetCategory(schema_name, userRole.getId());
//    }
//
//    /**
//     * 更新角色
//     *
//     * @param schema_name
//     * @param userRole
//     */
//    @Override
//    public void save(String schema_name, UserRole userRole) {
//        userRoleMapper.updateRole(schema_name, userRole);
//        rolePermissionMapper.deleteRolePermission(schema_name, userRole.getId());
//        userRoleMapper.deleteRoleAssetCategory(schema_name, userRole.getId());
//    }
//
//    /**
//     * 删除角色
//     *
//     * @param schema_name
//     * @param roleId
//     */
//    @Override
//    public void remove(String schema_name, String roleId) {
//        userRoleMapper.deleteRole(schema_name, roleId);
//        userRoleMapper.deleteUserRole(schema_name, roleId);
//        rolePermissionMapper.deleteRolePermission(schema_name, roleId);
//        userRoleMapper.deleteRoleAssetCategory(schema_name, roleId);
//    }
//
//    /**
//     * 查询没有用户的角色
//     *
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> checkUserByRole(String schema_name) {
//        return userRoleMapper.checkUserByRole(schema_name);
//    }
//
//    @Override
//    public String optionRole(List<UserRole> userRoleList, String schema_name, User user, String status) {
//        //查询已有的角色
//        List<Map<String, Object>> maps = userRoleMapper.checkUserIdByRole(schema_name, user);
//        if (status.equals("add")) {
//            maps = null;
//        }
//        StringBuffer stringBuffer = new StringBuffer();
//        if (userRoleList != null && userRoleList.size() > 0) {
//            for (int i = 0; i < userRoleList.size(); i++) {
//                stringBuffer.append("<option value=\"" + userRoleList.get(i).getId());
//                if (maps != null && maps.size() > 0) {
//                    for (Map<String, Object> map : maps) {
//                        if (map.get("id").equals(userRoleList.get(i).getId())) {
//                            stringBuffer.append("\" selected=\"selected");
//                            break;
//                        }
//                    }
//                }
//                stringBuffer.append("\">" + userRoleList.get(i).getName() + "</option>");
//            }
//        } else {
//            stringBuffer.append("<option value=\"-1\">无</option>");
//        }
//        return stringBuffer.toString();
//    }
//
//    /**
//     * 根据用户信息设定权限菜单和功能url
//     *
//     * @param schema_name
//     * @param roleId
//     * @param request
//     */
//    @Override
//    public UserRole setSessionPermissionUrlsById(String schema_name, String roleId, HttpServletRequest request) {
//        List<UserRole> userRole = userRoleMapper.findById(schema_name, "'" + roleId + "'");
//        LinkedHashMap<String, ISystemPermission> permissionSet = new LinkedHashMap<>();
//        Set<String> urlSet = null;
//        try {
//            urlSet = (Set<String>) request.getSession().getAttribute("urls");
//        } catch (Exception urlExp) {
//        }
//        if (null == urlSet || urlSet.size() == 0) {
//            urlSet = new HashSet<String>(); // 权限URL
//        }
//
//        Set<String> urlALLSet = null;
//        try {
//            urlALLSet = (Set<String>) request.getSession().getAttribute("allUrls");
//        } catch (Exception urlExp) {
//        }
//
//        if (null == urlALLSet || urlALLSet.size() == 0) {
//            urlALLSet = new HashSet<String>(); // 权限URL
//            List<SystemPermission> allList = systemPermissionService.findAll();
//            for (SystemPermission permission : allList) {
//                if (null != permission.getUrl() && !"".equals(permission.getUrl())) {
//                    urlALLSet.add(permission.getUrl()); // 权限URL
//                }
//            }
//            if (null != urlALLSet && urlALLSet.size() > 0) {
//                request.getSession(true).setAttribute("allUrls", urlALLSet);
//            }
//        }
//        if (roleId.equals(UserRole.ROLE_ID_SYSTEM_ADMIN)) {
//            List<SystemPermission> list = systemPermissionService.findAll();
//            for (SystemPermission permission : list) {
//                permissionSet.put(permission.getId(), permission);
//                if (null != permission.getUrl() && !"".equals(permission.getUrl())) {
//                    urlSet.add(permission.getUrl()); // 权限URL
//                }
//            }
//        } else if (roleId.equals(UserRole.ROLE_ID_COMPANY_ADMIN)) {
//            String strId = schema_name.replace("sc_com_", "");
//            List<SystemPermission> list = systemPermissionService.findAllByCompanyId(Long.parseLong(strId), null);
//            for (SystemPermission permission : list) {
//                permissionSet.put(permission.getId(), permission);
//                if (null != permission.getUrl() && !"".equals(permission.getUrl())) {
//                    urlSet.add(permission.getUrl()); // 权限URL
//                }
//            }
//        } else {
//            String strId = schema_name.replace("sc_com_", "");
//            List<Map<String, Object>> rolePermissons = rolePermissionMapper.getRolePermissions(schema_name, Long.parseLong(strId), "'" + roleId + "'");
//            for (Map<String, Object> permission : rolePermissons) {
//                ISystemPermission permission1 = new SystemPermission();
//                permission1.setId(permission.get("permissionid").toString());
//                permission1.setName(permission.get("permissionname").toString());
//                permission1.setKey(permission.get("permissionkey").toString());
//                permissionSet.put(permission1.getId(), permission1);
//                // 权限URL
//                if (null != permission.get("permission_url") && !"".equals(permission.get("permission_url"))) {
//                    urlSet.add(permission.get("permission_url").toString());
//                }
//            }
//        }
//
//        // 缓存权限URL
//        if (null != urlSet && urlSet.size() > 0) {
//            request.getSession(true).setAttribute("urls", urlSet);
//        }
//        if (null != userRole && userRole.size() > 0) {
//            userRole.get(0).setPermissions(permissionSet);
//            return userRole.get(0);
//        } else {
//            return null;
//        }
////        userRole.setPermissions(permissionSet);
////        return userRole;
//    }
//
//    /**
//     * 查询已有角设备类型
//     *
//     * @param schema_name
//     * @param role_id
//     * @return
//     */
//    @Override
//    public List<String> getRoleAcList(String schema_name, String role_id) {
//        return userRoleMapper.getRoleAcList(schema_name, role_id);
//    }
//
//    /**
//     * 查询已有角色权限
//     *
//     * @param schema_name
//     * @param companyId
//     * @param role_id
//     * @return
//     */
//    @Override
//    public List<String> getRolePermissionList(String schema_name, Long companyId, String role_id) {
//        return rolePermissionMapper.getRolePermissionIdList(schema_name, companyId, role_id);
//    }
}
