package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetGuaranteeService;
import org.springframework.stereotype.Service;

@Service
public class AssetGuaranteeServiceImpl implements AssetGuaranteeService {
//    @Autowired
//    AssetGuaranteeMapper mapper;
//
//    @Override
//    public List<AssetGuaranteeData> findAllAssetGuaranteeList(String schema_name, String asset_id) {
//        return mapper.findAllAssetGuaranteeList(schema_name, asset_id);
//    }
//
//    @Override
//    public int AddAssetGuarantee(String schema_name, String asset_id, Date buy_date, Date expired_date, String serial_no, Integer contract_type, Integer company_id, String create_user_account) {
//        return mapper.AddAssetGuarantee(schema_name, asset_id, buy_date, expired_date, serial_no, contract_type, company_id, create_user_account);
//    }
//
//    //修改资产保修信息
//    @Override
//    public int UpdateAssetGuarantee(String schema_name, Date buy_date, Date expired_date, String serial_no, Integer contract_type, Integer company_id, Integer id) {
//        return mapper.UpdateAssetGuarantee(schema_name, buy_date, expired_date, serial_no, contract_type, company_id, id);
//    }
//
//    //查找资产保修，按id查找，获取单个保修数据
//    @Override
//    public AssetGuaranteeData findAssetGuarantee(String schema_name, Integer id) {
//        return mapper.findAssetGuarantee(schema_name, id);
//    }
//
//    //删除资产保修信息
//    @Override
//    public int DeleteAssetGuarantee(String schema_name, Integer id, Boolean isuse) {
//        return mapper.DeleteAssetGuarantee(schema_name, id, isuse);
//    }
//
//    /**
//     * 获取资产保修公司信息
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public Integer findAssetPeriodInfo(String schemaName) {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        String assetId = request.getParameter("assetId");
//        String contractType = request.getParameter("contractType");
//        Integer result = findAssetPeriodCompanyId(schemaName, assetId, contractType);
//        return result;
//    }
//
//    // 获取资产保修公司信息
//    @Override
//    public Integer findAssetPeriodCompanyId(String schemaName, String assetId, String contractType) {
//        List<Map<String, Object>> periodList = mapper.findAssetPeriodInfo(schemaName, assetId, contractType);
//        Integer result = null;
//        if (null != periodList && periodList.size() > 0) {
//            try {
//                Date now = new Date();
//                for (Map<String, Object> data : periodList) {
//                    Date buyDate = (Date) data.get("buy_date");
//                    Date expiredDate = (Date) data.get("expired_date");
//                    if (now.after(buyDate) && now.before(expiredDate)) {
//                        result = ((Long) data.get("company_id")).intValue();
//                        break;
//                    }
//                }
//            } catch (Exception e) {
//
//            }
//        }
//        return result;
//    }
//
//    //自动同步设备的供应商和制造商到设备关联组织中去，韵达定制
//    @Override
//    public void autoUpdateSupplyInfoToDeviceOrganization(String schemaName) {
//        mapper.autoUpdateSupplyInfoToDeviceOrganization(schemaName);
//    }
}
