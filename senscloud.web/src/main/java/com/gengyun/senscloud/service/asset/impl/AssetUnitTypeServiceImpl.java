package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetUnitTypeService;
import org.springframework.stereotype.Service;

/**
 * 设备型号管理
 * User: zys
 * Date: 2019/04/16
 * Time: 上午17:20
 */
@Service
public class AssetUnitTypeServiceImpl implements AssetUnitTypeService {
//    @Autowired
//    AssetUnitTypeMapper assetUnitTypeMapper;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @return
//     */
//    @Override
//    public JSONObject query_modal(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName, String sortOrder) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        if (StringUtils.isNotEmpty(sortName)) {
//            if (StringUtils.isEmpty(sortOrder)) {
//                sortOrder = "asc";
//            }
//            orderBy = sortName + " " + sortOrder;
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = assetUnitTypeMapper.query_modal(schemaName, orderBy, pageSize, begin, searchKey);
//        int total = assetUnitTypeMapper.countByCondition_modal(schemaName, searchKey);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel insert_modal(String schemaName, Map<String, Object> paramMap) {
//        paramMap.put("schema_name", schemaName);
//        paramMap.put("isuse", 1);
//        int count = assetUnitTypeMapper.insert_modal(paramMap);
//        if (!RegexUtil.isNull((String) paramMap.get("bom_ids"))) {
//            String bom_ids = (String) paramMap.get("bom_ids");
//            assetUnitTypeMapper.update_bom(schemaName, "(" + bom_ids + ")", Integer.parseInt(paramMap.get("id").toString()));
//        }
//        if (!RegexUtil.isNull((String) paramMap.get("org_ids"))) {
//            String org_ids = (String) paramMap.get("org_ids");
//            assetUnitTypeMapper.update_customer(schemaName, "(" + org_ids + ")", Integer.parseInt(paramMap.get("id").toString()));
//        }
//        if (!RegexUtil.isNull((String) paramMap.get("file_ids"))) {
//            String file_ids = ((String) paramMap.get("file_ids")).trim();
//            String file_id = file_ids.substring(0, file_ids.length() - 1);
//            assetUnitTypeMapper.update_doc(schemaName, "(" + file_id + ")", Integer.parseInt(paramMap.get("id").toString()));
//        }
//        if (count > 0) {
//            return ResponseModel.ok(paramMap.get("id").toString());
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel update_model(String schemaName, Map<String, Object> paramMap) {
//        int count = assetUnitTypeMapper.update_model(schemaName, paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel delete_model(String schemaName, HttpServletRequest request) {
//        Integer id = Integer.valueOf((String) request.getParameter("id"));
//        int count = assetUnitTypeMapper.delete_model(schemaName, id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public JSONObject query_modal_bom(String schemaName, String model_id) {
//        JSONObject pageResult = new JSONObject();
//        List<Map<String, Object>> result = assetUnitTypeMapper.query_model_bom(schemaName, model_id);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel insert_modal_bom(String schemaName, List<Map<String, Object>> paramMaps, User user, String model_id) {
//        int count = 0;
//        List<String> bomIds = new ArrayList<String>();
//        List<Integer> oldIds = new ArrayList<Integer>();
//        Map resultMap = new HashMap();
//        int modelId = 0;
//        if (!RegexUtil.isNull(model_id) && !"".equals(modelId)) {
//            modelId = Integer.parseInt(model_id);
//        }
//        Integer old_id = 0;
//        for (Map<String, Object> paramMap : paramMaps) {
//            paramMap.put("create_user_account", user.getAccount());
//            paramMap.put("model_id", modelId);
//            paramMap.put("schema_name", schemaName);
//            old_id = Integer.parseInt(paramMap.get("id").toString());
//            oldIds.add(old_id);
//            paramMap.put("bom_id", old_id);
//            count += assetUnitTypeMapper.insert_model_bom(paramMap);
//            bomIds.add(String.valueOf(paramMap.get("id")));
//        }
//        if (count == paramMaps.size()) {
//            resultMap.put("bomIds", bomIds);
//            resultMap.put("selectBomList", paramMaps);
//            resultMap.put("oldIds", oldIds);
//            return ResponseModel.ok(resultMap);
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel delete_model_bom(String schemaName, Integer id) {
//        int count = assetUnitTypeMapper.delete_model_bom(schemaName, id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//
//    /**
//     * 更新设备型号的备件周期
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public ResponseModel updateAssetModelBomCycle(String schemaName, Integer id, float useDays, float maintenanceDays, int unitType) {
//        int count = assetUnitTypeMapper.updateAssetModelBomCycle(schemaName, id, useDays, maintenanceDays, unitType);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
////    @Override
////    public ResponseModel update_model_bom_part(String schemaName, Long bom_id,Long part_id) {
////        int count = assetUnitTypeMapper.update_bom_part(schemaName, bom_id,part_id);
////        if (count > 0) {
////            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
////        } else {
////            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
////        }
////    }
//
//
//    @Override
//    public JSONObject query_modal_doc(String schemaName, String model_id) {
//        JSONObject pageResult = new JSONObject();
//        List<Map<String, Object>> result = assetUnitTypeMapper.query_model_doc(schemaName, model_id);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel insert_modal_doc(String schemaName, Map<String, Object> paramMap) {
//        int count = 0;
//        String[] file_ids = null;
//        String file_id = paramMap.get("file_id").toString().trim();
//        if (!RegexUtil.isNull(file_id) && !"".equals(file_id)) {
//            file_ids = file_id.split(",");
//            for (String id : file_ids) {
//                paramMap.put("file_id", Integer.parseInt(id));
//                count += assetUnitTypeMapper.insert_model_doc(schemaName, paramMap);
//            }
//        }
//        if (null != file_ids && count == file_ids.length) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//
//    @Override
//    public ResponseModel delete_model_doc(String schemaName, Integer file_id) {
//        int count = assetUnitTypeMapper.delete_model_doc(schemaName, file_id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public JSONObject query_modal_customer(String schemaName, String model_id) {
//        JSONObject pageResult = new JSONObject();
//        List<Map<String, Object>> result = assetUnitTypeMapper.query_model_customer(schemaName, model_id);
//        pageResult.put("rows", result);
//        pageResult.put("code", 1);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel insert_modal_customer(String schemaName, List<Map<String, Object>> paramMaps, User user, String model_id) {
//        int count = 0;
//        List<String> customerIds = new ArrayList<String>();
//        List<Integer> oldIds = new ArrayList<Integer>();
//        int modelId = 0;
//        if (!RegexUtil.isNull(model_id) && !"".equals(modelId)) {
//            modelId = Integer.parseInt(model_id);
//        }
//        Map resultMap = new HashMap();
//        Integer old_id = 0;
//        for (Map<String, Object> paramMap : paramMaps) {
//            paramMap.put("create_user_account", user.getAccount());
//            paramMap.put("model_id", modelId);
//            paramMap.put("schema_name", schemaName);
//            old_id = Integer.parseInt(paramMap.get("id").toString());
//            oldIds.add(old_id);
//            paramMap.put("org_id", old_id);
//            count += assetUnitTypeMapper.insert_model_customer(paramMap);
//            customerIds.add(String.valueOf(paramMap.get("id")));
//        }
//        if (count == paramMaps.size()) {
//            resultMap.put("orgIds", customerIds);
//            resultMap.put("selectCustomerList", paramMaps);
//            resultMap.put("oldIds", oldIds);
//            return ResponseModel.ok(resultMap);
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel delete_model_customer(String schemaName, Integer id) {
//        int count = assetUnitTypeMapper.delete_model_customer(schemaName, id);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public JSONObject findBomList(String schemaName, HttpServletRequest request) {
//        JSONObject pageResult = new JSONObject();
//        String key = request.getParameter("keyWord");
//        String main_id = request.getParameter("main_id");
//        List<Map<String, Object>> resulold = new ArrayList<Map<String, Object>>();
//        String condition = "";
//        if (null != key && !"".equals(key)) {
//            condition += " and (b.bom_name like '%" + key + "%' or b.bom_model like '%" + key + "%' or b.brand_name like '%" + key + "%') ";
//        }
//        List<Map<String, Object>> resulall = assetUnitTypeMapper.find_bom_by_key(schemaName, condition);
//        if (null != main_id && !"".equals(main_id)) {
//            resulold = assetUnitTypeMapper.query_model_bom(schemaName, main_id);
//        }
//        pageResult.put("alldata", resulall);
//        pageResult.put("olddata", resulold);
//        pageResult.put("code", 1);
//        return pageResult;
//    }
//
//    @Override
//    public JSONObject findFacilitiesList(String schemaName, HttpServletRequest request) {
//        JSONObject pageResult = new JSONObject();
//        String key = request.getParameter("keyWord");
//        String main_id = request.getParameter("main_id");
//        String orgType = request.getParameter("org_type");
//        List<Map<String, Object>> resulold = new ArrayList<Map<String, Object>>();
//        String condition = "";
//        if (null != key && !"".equals(key)) {
//            condition += " and (a.title like upper('%" + key + "%') or a.facilitycode like upper('%" + key + "%')) ";
//        }
//        if (null != orgType && !"".equals(orgType) && "outer".equals(orgType)) {
//            condition += " and a.org_type in (" + SqlConstant.FACILITY_OUTSIDE + ") ";
//        }
//        List<Map<String, Object>> resulall = assetUnitTypeMapper.find_facilities_by_key(schemaName, condition);
//        if (null != main_id && !"".equals(main_id)) {
//            resulold = assetUnitTypeMapper.query_model_customer(schemaName, main_id);
//        }
//        pageResult.put("alldata", resulall);
//        pageResult.put("olddata", resulold);
//        pageResult.put("code", 1);
//        return pageResult;
//    }
//
//    @Override
//    public Map<String, Object> find_model_by_id(String schemaName, Integer id) {
//        return assetUnitTypeMapper.find_model_by_id(schemaName, id);
//    }
//
//    @Override
//    public List<Map<String, Object>> find_model_list(String schemaName) {
//        return assetUnitTypeMapper.find_model_list(schemaName);
//    }
//
//    //设备型号，新增工单类型的任务模板
//    @Override
//    public int addAssetModelTaskTemplate(String schema_name, AssetUnitTypeTaskTemplateData data) {
//        return assetUnitTypeMapper.addAssetModelTaskTemplate(schema_name, data);
//    }
//
//    //查询设备型号的工单类型的任务模板
//    @Override
//    public List<AssetUnitTypeTaskTemplateData> getAssetModelTaskTemplate(String schema_name, Integer model_id) {
//        return assetUnitTypeMapper.getAssetModelTaskTemplate(schema_name, model_id);
//    }
//
//    //删除设备型号的工单类型的任务模板
//    @Override
//    public int deletaAssetModelTaskTemplate(String schema_name, Integer model_id, int workTypeId, String taskTemplateCode) {
//        return assetUnitTypeMapper.deletaAssetModelTaskTemplate(schema_name, model_id, workTypeId, taskTemplateCode);
//    }
}
