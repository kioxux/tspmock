package com.gengyun.senscloud.service.system.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.SerialServiceMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudError;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 编号生成工具
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SerialNumberServiceImpl implements SerialNumberService {
    @Resource
    SerialServiceMapper serialMapper;
    @Resource
    CacheUtilService cacheUtilService;

    /**
     * 根据前缀等信息生成到一个最大的业务编号
     *
     * @param methodParam  系统参数
     * @param businessType 业务类型
     * @param serialPre    前缀
     * @param number       序号位数
     * @return 编号
     */
    @Override
    public String generateMaxBsCode(MethodParam methodParam, String businessType, String serialPre, int number) {
        String splitStr = SensConstant.LOG_SPLIT;
        String errRemark = RegexUtil.optStrOrVal(businessType, "businessType").concat(splitStr)
                .concat(RegexUtil.optStrOrVal(serialPre, "serialPre")).concat(splitStr)
                .concat(RegexUtil.optStrOrVal(number, "number")).concat(splitStr);
        RegexUtil.optStrOrError(businessType, methodParam, ErrorConstant.EC_SN_1, errRemark, LangConstant.MSG_BK);
        // 生成编号前面的日期串，拉出来，放在 synchronized 外
        Date date = new Date();
        SimpleDateFormat yearModel = new SimpleDateFormat(Constants.DATE_FMT_YEAR);
        String year = yearModel.format(date);
        SimpleDateFormat yearMonthModel = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON);
        String yearMonth = yearMonthModel.format(date);
        SimpleDateFormat yearMonthDayModel = new SimpleDateFormat(Constants.DATE_FMT_DATE);
        String yearMonthDay = yearMonthDayModel.format(date);
        String schema_name = methodParam.getSchemaName();
        synchronized (this) {
            // 再找业务编号记录，判断是否直接获取最大的，还是重置
            return RegexUtil.optNotNullMap(serialMapper.findSerialNumberByType(schema_name, serialPre)).map(si -> {
                // 1：按年进行重置;2按月进行重置;3：按天进行重置;4：不重置，一直增加；
                int resetType = RegexUtil.optIntegerOrExp(si.get("reset_type"), methodParam, ErrorConstant.EC_SN_2, errRemark, LangConstant.MSG_BK);
                int maxNumber = RegexUtil.optIntegerOrExp(si.get("max_number"), methodParam, ErrorConstant.EC_SN_3, errRemark, LangConstant.MSG_BK);
                String currentDate = RegexUtil.optStrOrBlank(si.get("current_date"));
                boolean isNeedPre = RegexUtil.optBool(si.get("is_need_pre"));
                int newNumber;
                String currentYearOrMonthOrDay = "";
                switch (resetType) {
                    case 1:
                        currentYearOrMonthOrDay = year;
                        break;
                    case 2:
                        currentYearOrMonthOrDay = yearMonth;
                        break;
                    case 3:
                        currentYearOrMonthOrDay = yearMonthDay;
                        break;
                }
                // 编号如包括日期，按日期，看是否是新的日期，如果是新的，需要重置
                if (currentYearOrMonthOrDay.equals(currentDate)) {
                    // 记录里已存在的日期，不需要重置
                    newNumber = maxNumber + 1;
                    // 再将数据库中的最大序号+1；
                    RegexUtil.intErr(serialMapper.updateSerialNumberByOne(schema_name, serialPre, newNumber), methodParam, ErrorConstant.EC_SN_4, errRemark, LangConstant.MSG_BK);
                    // 新的日期
                } else if (resetType == 4) {
                    // 规定了不重置，一直增加
                    newNumber = maxNumber + 1;
                    // 为编号更新新的年份，且不需要重置序号
                    // serialMapper.ResetSerialNumberOnlyCurrentDate(schema_name, serialPre, currentYearOrMonthOrDay);
                    // 再将数据库中的最大序号+1；
                    RegexUtil.intErr(serialMapper.updateSerialNumberByOne(schema_name, serialPre, newNumber), methodParam, ErrorConstant.EC_SN_4, errRemark, LangConstant.MSG_BK);
                } else {
                    newNumber = 1;
                    // 为编号更新新的年份，且重置序号为1
                    RegexUtil.intErr(serialMapper.resetSerialNumberForDate(schema_name, serialPre, currentYearOrMonthOrDay), methodParam, ErrorConstant.EC_SN_4, errRemark, LangConstant.MSG_BK);
                }
                String sn = isNeedPre ? serialPre : ""; // 前缀处理
                sn += currentYearOrMonthOrDay + String.format("%0" + number + "d", newNumber);
                return sn;
            }).orElseThrow(() -> new SenscloudError(methodParam, ErrorConstant.EC_SN_5, errRemark, LangConstant.MSG_BK));
        }
    }

    /**
     * 根据类型生成到一个最大的业务编号
     *
     * @param methodParam  系统参数
     * @param businessType 业务类型
     * @return 编号
     */
    @Override
    public String generateMaxBsCodeByType(MethodParam methodParam, String businessType) {
        String errRemark = RegexUtil.optStrOrVal(businessType, "businessType");
        RegexUtil.optStrOrError(businessType, methodParam, ErrorConstant.EC_SN_1, errRemark, LangConstant.MSG_BK);
        String schema_name = methodParam.getSchemaName();
        return RegexUtil.optNotNullMap(cacheUtilService.getSerialByType(schema_name, businessType)).map(sc -> {
            String serialPre = RegexUtil.optStrOrError(sc.get("serial_pre"), methodParam, ErrorConstant.EC_SN_7, errRemark, LangConstant.MSG_BK);
            int number = RegexUtil.optIntegerOrExp(sc.get("number"), methodParam, ErrorConstant.EC_SN_8, errRemark, LangConstant.MSG_BK);
            // 找到最大的序号
            return this.generateMaxBsCode(methodParam, businessType, serialPre, number);
        }).orElseThrow(() -> new SenscloudError(methodParam, ErrorConstant.EC_SN_6, errRemark, LangConstant.MSG_BK));
    }

}
