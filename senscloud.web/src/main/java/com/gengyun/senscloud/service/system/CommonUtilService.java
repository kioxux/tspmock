package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import net.sf.json.JSONArray;

import java.util.List;
import java.util.Map;

/**
 * 公共工具用接口
 */
public interface CommonUtilService {
    /**
     * 根据系统默认语言进行国际化转换
     *
     * @param methodParam 系统参数
     * @param langKey     多语言key
     * @return 多语言转换值
     */
    String getLanguageInfoBySysLang(MethodParam methodParam, String langKey);

    /**
     * 根据用户默认语言进行国际化转换
     *
     * @param methodParam 系统参数
     * @param langKey     多语言key
     * @return 多语言转换值
     */
    String getLanguageInfoByUserLang(MethodParam methodParam, String langKey);

    /**
     * 分批批量插入表格数据
     *
     * @param dataList   数据列表
     * @param schemaName 数据库
     * @param tableName  表明
     * @param columns    需要插入的字段
     */
    void batchInsertDataList(String schemaName, List<Map<String, Object>> dataList, String tableName, String columns);

    /**
     * 分批批量插入表格数据（指定条数）
     *
     * @param dataList   数据列表
     * @param schemaName 数据库
     * @param tableName  表明
     * @param columns    需要插入的字段
     * @param num        每批插入条数
     */
    void batchInsertDataListWithNum(String schemaName, List<Map<String, Object>> dataList, String tableName, String columns, int num);

    /**
     * 查询所有的域名
     *
     * @return 所有的域名
     */
    List<String> getAllDomain();

    /**
     * 系统记录日志
     *
     * @param methodParam        系统变量
     * @param dataFieldStr       日志配置信息
     * @param newMap             页面数据
     * @param dbMap              数据库数据
     * @param logArr             打印日志数组
     * @param work_calendar_code 计划主键
     * @param businessNo         业务编号
     * @param remark             日志操作
     */
    void doSetLogInfo(MethodParam methodParam, String dataFieldStr, Map<String, Object> newMap, Map<String, Object> dbMap, JSONArray logArr, String work_calendar_code, String businessNo, String remark);

    /**
     * 根据父id，按顺序生成一个位置编号
     *
     * @param schema_name     数据库
     * @param no              默认编号
     * @param id              主键
     * @param parentId        父id值
     * @param tableName       表名
     * @param fieldName       编号字段名
     * @param parentFieldName 父编号字段名
     * @param keyFieldName    主键编号字段名
     * @param prefix          编号前缀
     * @return 返回按顺序生成的一个编号
     */
    String getMaxNoByParentId(String schema_name, String no, Object id, Object parentId, String tableName, String fieldName, String parentFieldName, String keyFieldName, String prefix, String firstNo);

    /**
     * 批量存储数据
     *
     * @param schemaName 数据库
     * @param tableName  表名
     * @param columns    列信息
     * @param dataList   数据
     */
    @Deprecated
    void doBatchInsertSql(String schemaName, String tableName, String columns, List<Map<String, Object>> dataList);

    /**
     * 设备  设备位置 备件 二维码导出
     */
    Map<String, Object> qrCodeExport(MethodParam methodParam, Integer type);

    String stringFormat(Object[] params, String patternStr);

    /**
     * 根据维保计划生成一条工单
     *
     * @param methodParam 系统参数
     * @param c           方法参数
     */
    void createWorkOrderByPlanWorkCalendar(MethodParam methodParam, PlanWorkCalendarModel c);

    /**
     * 更新序列开始值
     *
     * @param methodParam 系统参数
     * @param seq         序列名称
     * @param value       列名称
     */
    void modifySeqStartByTbl(MethodParam methodParam, String seq, int value);

    /**
     * 更新序列（默认列）
     *
     * @param methodParam 系统参数
     * @param seq         序列名称
     * @param table       表名称
     */
    void modifySeqByTblForDefault(MethodParam methodParam, String seq, String table);

    /**
     * 更新序列
     *
     * @param methodParam 系统参数
     * @param seq         序列名称
     * @param col         列名称
     * @param table       表名称
     */
    void modifySeqByTbl(MethodParam methodParam, String seq, String col, String table);

    /**
     * 取表字段最大值
     *
     * @param methodParam 系统参数
     * @param col         列名称
     * @param table       表名称
     * @return 字段最大值
     */
    String getMaxId(MethodParam methodParam, String col, String table);
}
