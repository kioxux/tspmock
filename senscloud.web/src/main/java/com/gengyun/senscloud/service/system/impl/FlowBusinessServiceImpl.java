package com.gengyun.senscloud.service.system.impl;

import com.gengyun.senscloud.service.system.FlowBusinessService;
import org.springframework.stereotype.Service;

/**
 * FlowBusinessService class 业务数据和流程同步接口实现
 *
 * @author Zys
 * @date 2020/04/10
 */
@Service
public class FlowBusinessServiceImpl implements FlowBusinessService {
//
//    @Autowired
//    FlowBusinessMapper flowBusinessMapper;
//
//    /**
//     * 存在数据则更新 流程业务同步表,不存在则新增一条记录
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @param taskId
//     * @param formKey
//     * @param status
//     * @param dutyAccount
//     * @return
//     */
//    @Override
//    public void renovateFlowBusinessData(String schemaName, String subWorkCode, String taskId, String formKey, String status, String dutyAccount) {
//        flowBusinessMapper.insertOrUpdate(schemaName, subWorkCode, taskId, formKey, status, dutyAccount);
//    }
//
//    /**
//     * 删除已经完成,或者作废的工单的相关数据(不会再有后续操作的数据)
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public int deleteInvalidFlowBusinessData(String schemaName) {
//        int doCount = 0;
//        //查询出已经无意义的数据
//        String subWorkCodes = flowBusinessMapper.queryInvalidSubWorkCodes(schemaName);
//        if (RegexUtil.isNull(subWorkCodes)) {
//            return doCount;
//        }
//        subWorkCodes=subWorkCodes.replace("&","'");
//        //删除数据
//        doCount = flowBusinessMapper.deleteFlowBusinessDataByCodes(schemaName, subWorkCodes);
//
//        return doCount;
//    }
//
//    /**
//     * 按subWorkCode删除指定某条数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Override
//    public int deleteFlowBusinessDataByCode(String schemaName, String subWorkCode) {
//        int doCount = 0;
//        //删除数据
//        doCount = flowBusinessMapper.deleteFlowBusinessDataByCodes(schemaName, subWorkCode);
//        return doCount;
//    }
//
//    /**
//     * 按subWorkCodes查询数据
//     *
//     * @param schemaName
//     * @param subWorkCodes
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryFlowBusinessDataByCodes(String schemaName, String subWorkCodes) {
//        return flowBusinessMapper.queryFlowBusinessDataByCodes(schemaName, subWorkCodes);
//    }
//
//    /**
//     * 按subWorkCodes查询key:subWorkCode value:form_key数据
//     *
//     * @param schemaName
//     * @param subWorkCodes
//     * @param keySet
//     * @param valueSet
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryFlowBusinessMapByCodes(String schemaName, String subWorkCodes, String keySet, String valueSet) {
//        List<Map<String, Object>> flowBusinessDataList = flowBusinessMapper.queryFlowBusinessMapByCodes(schemaName, subWorkCodes, keySet, valueSet);
//        if (flowBusinessDataList.isEmpty()) {
//            return null;
//        }
//        Map<String, Object> resultMap = new HashMap<>();
//        try {
//            for (Map<String, Object> flowBusinessData : flowBusinessDataList) {
//                resultMap.put((String) flowBusinessData.get(keySet), flowBusinessData.get(valueSet));
//            }
//
//        } catch (NullPointerException e) {
//            resultMap = null;
//        }
//        return resultMap;
//    }
//
//    /**
//     * 根据formKey查询对应业务流程信息
//     *
//     * @param schemaName
//     * @param formKey
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryFlowBusinessDataByFromKey(String schemaName, String formKey) {
//        return flowBusinessMapper.queryFlowBusinessDataByFromKey(schemaName,formKey);
//    }
}
