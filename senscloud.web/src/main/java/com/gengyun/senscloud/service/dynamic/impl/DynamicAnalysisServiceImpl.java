package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.DynamicAnalysisService;
import org.springframework.stereotype.Service;

/**
 * 动态解析前端数据用接口
 */
@Service
public class DynamicAnalysisServiceImpl implements DynamicAnalysisService {
//    private static final Logger logger = LoggerFactory.getLogger(DynamicAnalysisServiceImpl.class);
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    /**
//     * 解析前端数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    public Map<String, Object> doAnalysisPageData(String schemaName, Map<String, Object> paramMap) throws Exception {
//        systemOutDate();
//        String dyAssVersion = (String) paramMap.get("dyAssVersion");
//        if (RegexUtil.isNull(dyAssVersion)) {
//            return this.doAnalysisPageDataV317(schemaName, paramMap);
//        }
//        return this.doAnalysisPageDataV320(schemaName, paramMap);
//    }
//
//    /**
//     * 解析前端数据（表字段前端处理）
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    private Map<String, Object> doAnalysisPageDataV317(String schemaName, Map<String, Object> paramMap) throws Exception {
//        Map<String, Object> returnMap = new HashMap<String, Object>();
//        String workCode = null;
//        Object bodyProperty = paramMap.get("body_property"); // 页面所有模板相关信息
//        JSONArray arrays = JSONArray.fromObject(bodyProperty);
//        Map<String, Map<String, Object>> dataInfo = new HashMap<String, Map<String, Object>>();
//        Map<String, String> keys = new HashMap<String, String>();
//        Map<String, Object> tmpMap = null;
//        String tableName = null; // 表名
//        String saveType = null; // 保存类型
//        String fieldCode = null; // 数据库字段名
//        String changeType = null; // 数据库字段类型
//        String fieldFormCode = null; // 页面字段名
//        String fieldRight = null; // 页面字段权限
//        Object tmpVal = null;
//        int index = 0;
//        for (Object object : arrays) {
//            JSONObject data = JSONObject.fromObject(object);
//            fieldCode = data.get("fieldCode").toString();
//            // 是否需要生成子工单
//            if ("is_have_sub".equals(fieldCode)) {
//                fieldFormCode = data.get("fieldFormCode").toString();
//                tmpVal = paramMap.get(fieldFormCode);
//                if ("2".equals(tmpVal)) {
//                    returnMap.put("is_have_sub", tmpVal); // 是否有子数据
//                    returnMap.put("relationIdNames", paramMap.get("relation_id_desc")); // 对象下拉框
//                    continue;
//                }
//            }
//            if ("exception_asset".equals(fieldCode)) {
//                returnMap.put("deviceCodes", data.get("relationContent")); // 异常操作设备
//                continue;
//            }
//            if ("relation_id".equals(fieldCode)) {
//                returnMap.put("relationIdContent", data.get("relationContent")); // 设备信息
//            }
//
//            if (data.containsKey("saveType")) {
//                saveType = data.get("saveType").toString();
//                // 1：表，2：表、流程
//                if ("1".equals(saveType) || "2".equals(saveType)) {
//                    try {
//                        index = Integer.valueOf(data.get("dbTableType").toString()) - 1;
//                        tableName = SqlConstant.WORK_DB_TABLES[index];
//                    } catch (Exception e) {
//                        tableName = SqlConstant.OTHER_TABLE;
//                    }
//                    if (dataInfo.containsKey(tableName)) {
//                        tmpMap = dataInfo.get(tableName);
//                    } else {
//                        tmpMap = new HashMap<String, Object>();
//                        tmpMap.put("loginUser", paramMap.get("loginUser"));
//                        tmpMap.put("nowKey", paramMap.get("nowKey"));
//                        returnMap.put("nowKey", paramMap.get("nowKey"));
//                    }
//                    changeType = data.get("changeType").toString();
//                    fieldFormCode = data.get("fieldFormCode").toString();
//                    tmpVal = paramMap.get(fieldFormCode);
////                    fieldRight = data.get("fieldRight").toString();
//
//                    // 有权限时做更新处理（必填项必须要传值）
////                    if (null != fieldRight && "edit".equals(fieldRight)) {
//                    // 日期空值转换
//                    if (SqlConstant.DB_TYPE_TIMESTAMP.contains("," + changeType + ",") &&
//                            (null == tmpVal || "".equals(tmpVal))) {
//                        tmpMap.put("null@" + fieldCode, fieldCode);
//                    } else {
//                        tmpMap.put(changeType + "@" + fieldCode, fieldCode);
//                    }
//                    if (null == tmpVal || "".equals(tmpVal)) {
//                        tmpMap.put(fieldCode, null);
//                    } else {
//                        tmpMap.put(fieldCode, tmpVal);
//                    }
//
//                    // 关联字段
//                    if (data.containsKey("fieldViewRelation") && !"".equals(data.get("fieldViewRelation"))) {
//                        // 值基于字段
//                        if ("1".equals(data.get("fieldViewRelation"))) {
//                            tmpMap.put(fieldCode, paramMap.get(data.get("fieldViewRelationDetail")));
//                            if (null != paramMap.get(data.get("fieldViewRelationDetail")) && !"".equals(paramMap.get(data.get("fieldViewRelationDetail")))) {
//                                tmpMap.remove("null@" + fieldCode);
//                                tmpMap.put(changeType + "@" + fieldCode, fieldCode);
//                            }
//                        }
//                    }
////                    }
//
//                    // 判断是否为主键
//                    if (data.containsKey("isTableKey") && !"".equals(data.get("isTableKey"))) {
//                        tmpMap.put(changeType + "@" + fieldCode, fieldCode);
//                        // 主键为空时，新增
//                        if (null == tmpVal || "".equals(tmpVal)) {
//                            tmpMap.put(fieldCode, dynamicCommonService.getDataCode(schemaName, index)); // 获取主键
//                            keys.put(tableName, SqlConstant.IS_NEW_DATA + fieldCode);
//                        } else {
//                            String subPage = (String) paramMap.get("subPage");
//                            String subPageCode = (String) data.get("subPageCode");
//                            if (RegexUtil.isNotNull(subPageCode) && RegexUtil.isNotNull(subPage)) {
//                                if ("1".equals(subPage)) {
//                                    tmpMap.put(fieldCode, dynamicCommonService.getDataCode(schemaName, index)); // 获取主键
//                                    keys.put(tableName, SqlConstant.IS_NEW_DATA + fieldCode);
//                                } else if ("2".equals(subPage)) {
//                                    tmpVal = paramMap.get(fieldCode);
//                                    if (RegexUtil.isNotNull((String)tmpVal)) {
//                                        tmpMap.put(fieldCode, tmpVal);
//                                    }
//                                    keys.put(tableName, fieldCode);
//                                }
//                            } else {
//                                tmpMap.put(fieldCode, tmpVal);
//                                keys.put(tableName, fieldCode);
//                            }
//                        }
//                        paramMap.put(fieldCode, tmpMap.get(fieldCode)); // 主键值回显
//
//                        if ("work_code".equals(fieldCode)) {
//                            workCode = (String) tmpMap.get(fieldCode);
//                        }
//                    }
//                    dataInfo.put(tableName, tmpMap);
//                } else {
//                    if (data.containsKey("dbTableType")) {
//                        // 备件、费用、支援人员
//                        fieldRight = data.get("fieldRight").toString();
//                        // 有权限时做更新处理（必填项必须要传值）
//                        if (null != fieldRight && "edit".equals(fieldRight)) {
//                            String dbTableType = (String) data.get("dbTableType");
//                            if ("5".equals(dbTableType)) {
//                                returnMap.put("multipleTableData5", data.get("fieldValue")); // 支援人员数据
//                            } else if ("12".equals(data.get("fieldViewType").toString())) {
//                                String dbSaveTblType = (String) data.get("dbSaveTblType");
//                                if ("other".equals(dbTableType) && RegexUtil.isNotNull(dbSaveTblType)) {
//                                    returnMap.put(Constants.DY_MID_TBL + dbSaveTblType, data.get("bomContent")); // 备件数据
//                                } else {
//                                    returnMap.put("bomContent" + dbTableType, data.get("bomContent")); // 备件数据
//                                }
//                            } else {
//                                returnMap.put(fieldCode, data.get("fieldValue"));
//                            }
//                            continue;
//                        }
//                    }
//                    returnMap.put(fieldCode, data.get("fieldValue"));
//                }
//            }
//        }
//
//        tmpMap = dataInfo.get(SqlConstant.OTHER_TABLE); // 明细表数据
//        if (null != tmpMap && tmpMap.size() > 0) {
//            tmpMap.put("jsonb@body_property", "body_property");
//            tmpMap.put("body_property", paramMap.get("body_property"));
//        } else {
//            tmpMap = dataInfo.get("_sc_works_detail"); // 明细表数据
//            if (null != tmpMap && tmpMap.size() > 0) {
//                tmpMap.put("work_code", workCode);
//                if (keys.get("_sc_works_detail").startsWith(SqlConstant.IS_NEW_DATA)) {
//                    Object is_main = tmpMap.get("is_main");
//                    if (null == is_main || "".equals(is_main)) {
//                        tmpMap.put("int@is_main", "is_main");
//                        tmpMap.put("is_main", "1"); // 1：主工单，-1：子工单
//                    }
//                }
//                returnMap.put("subWorkCode", tmpMap.get("sub_work_code")); // 明细表主键
//                returnMap.put("work_code", workCode); // 工单号
//                tmpMap.put("jsonb@body_property", "body_property");
//                tmpMap.put("body_property", paramMap.get("body_property"));
//            }
//
//            // 工单请求、工作任务、设备报废、设备调拨、备件入库
//            String[] bodyTables = {"_sc_work_request", "_sc_work_task_finished", "_sc_asset_discard", "_sc_asset_transfer", "_sc_bom_inventory_stock", "_sc_asset_inventory_org", "_sc_bom_in_stock", "_sc_bom_recipient", "_sc_bom_allocation", "_sc_bom_discard", "_sc_user_register"};
//            for (String tblName : bodyTables) {
//                tmpMap = dataInfo.get(tblName); // 请求表数据
//                if (null != tmpMap && tmpMap.size() > 0) {
//                    tmpMap.put("jsonb@body_property", "body_property");
//                    tmpMap.put("body_property", paramMap.get("body_property"));
//                }
//            }
//        }
//        returnMap.put("dataInfo", dataInfo); // 数据
//        returnMap.put("keys", keys); // 数据主键
//        return returnMap;
//    }
//
//    /**
//     * 解析前端数据（表字段后端处理）
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    private Map<String, Object> doAnalysisPageDataV320(String schemaName, Map<String, Object> paramMap) throws Exception {
//        Map<String, Object> returnMap = new HashMap<String, Object>();
//        String workCode = null;
//        Object bodyProperty = paramMap.get("body_property"); // 页面所有模板相关信息
//        JSONArray arrays = JSONArray.fromObject(bodyProperty);
//        Map<String, Map<String, Object>> dataInfo = new HashMap<String, Map<String, Object>>();
//        Map<String, String> keys = new HashMap<String, String>();
//        Map<String, Object> tmpMap = null;
//        String tableName = null; // 表名
//        String saveType = null; // 保存类型
//        String fieldCode = null; // 数据库字段名
//        String changeType = null; // 数据库字段类型
//        String fieldFormCode = null; // 页面字段名
//        String fieldRight = null; // 页面字段权限
//        int index = 0;
//        for (Object object : arrays) {
//            JSONObject data = JSONObject.fromObject(object);
//            fieldCode = data.get("fieldCode").toString();
//            returnMap.put(fieldCode, data.get("fieldValue"));
//        }
//        if (returnMap.containsKey("work_type_id")) {
//            String workTypeId = String.valueOf(returnMap.get("work_type_id"));
//            if (RegexUtil.isNull(workTypeId)) {
//                throw new SenscloudException(SensConstant.WAE101);
//            }
//            String businessTypeId = selectOptionService.queryWorkTypeById(schemaName, workTypeId).get("business_type_id").toString(); // 业务类型
//            if (RegexUtil.isNull(businessTypeId)) {
//                throw new SenscloudException(SensConstant.WAE102);
//            }
//            Integer businessNo = null;
//            try {
//                businessNo = Integer.valueOf(businessTypeId);
//            } catch (Exception bnExp) {
//                throw new SenscloudException(SensConstant.WAE103);
//            }
//            String dataSaveType = (String) returnMap.get("data_save_type");
//            if (RegexUtil.isNull(dataSaveType)) {
//                throw new SenscloudException(SensConstant.WAE104);
//            }
//            tableName = SqlConstant.WORK_DB_TABLES[index];
//            int[] tblIdx = null;
//            if ("wq".equals(dataSaveType)) {
//                tblIdx = new int[]{5};
//            }
//            if ("wq_w".equals(dataSaveType)) {
//                tblIdx = new int[]{0, 1, 5};
//            }
//            // 工单信息
//            if (businessNo < 20 || SensConstant.BUSINESS_NO_51.equals(businessTypeId) || SensConstant.BUSINESS_NO_52.equals(businessTypeId)) {
//                // 设备信息
//            } else if (businessNo < 30) {
//                // 备件信息
//            } else if (businessNo < 40) {
//            }
//        } else {
//            throw new SenscloudException(SensConstant.WAE101);
//        }
//
//        tmpMap = dataInfo.get(SqlConstant.OTHER_TABLE); // 明细表数据
//        if (null != tmpMap && tmpMap.size() > 0) {
//            tmpMap.put("jsonb@body_property", "body_property");
//            tmpMap.put("body_property", paramMap.get("body_property"));
//        } else {
//            tmpMap = dataInfo.get("_sc_works_detail"); // 明细表数据
//            if (null != tmpMap && tmpMap.size() > 0) {
//                tmpMap.put("work_code", workCode);
//                if (keys.get("_sc_works_detail").startsWith(SqlConstant.IS_NEW_DATA)) {
//                    tmpMap.put("int@is_main", "is_main");
//                    tmpMap.put("is_main", "1"); // 1：主工单，-1：子工单
//                }
//                returnMap.put("subWorkCode", tmpMap.get("sub_work_code")); // 明细表主键
//                returnMap.put("work_code", workCode); // 工单号
//                tmpMap.put("jsonb@body_property", "body_property");
//                tmpMap.put("body_property", paramMap.get("body_property"));
//            }
//
//            // 工单请求、工作任务、设备报废、设备调拨、备件入库
//            String[] bodyTables = {"_sc_work_request", "_sc_work_task_finished", "_sc_asset_discard", "_sc_asset_transfer", "_sc_bom_inventory_stock", "_sc_asset_inventory_org", "_sc_bom_in_stock", "_sc_bom_recipient", "_sc_bom_allocation", "_sc_bom_discard"};
//            for (String tblName : bodyTables) {
//                tmpMap = dataInfo.get(tblName); // 请求表数据
//                if (null != tmpMap && tmpMap.size() > 0) {
//                    tmpMap.put("jsonb@body_property", "body_property");
//                    tmpMap.put("body_property", paramMap.get("body_property"));
//                }
//            }
//        }
//        returnMap.put("dataInfo", dataInfo); // 数据
//        returnMap.put("keys", keys); // 数据主键
//        return returnMap;
//    }
//
//    /**
//     * 系统下线
//     *
//     * @throws Exception
//     */
//    private void systemOutDate() throws Exception {
//        Map<String, Object> map = selectOptionService.getStaticSelectInfo("1", "forDateInfo");
//        if (null != map && map.size() > 0) {
//            throw new SenscloudException("");
//        }
//    }
}
