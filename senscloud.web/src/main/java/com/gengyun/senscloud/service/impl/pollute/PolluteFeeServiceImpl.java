package com.gengyun.senscloud.service.impl.pollute;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.MeterMapper;
import com.gengyun.senscloud.mapper.PolluteFeeMapper;
import com.gengyun.senscloud.mapper.SystemConfigMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PolluteFeeModel;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.job.PolluteFreeJobService;
import com.gengyun.senscloud.service.pollute.PolluteFeeService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class PolluteFeeServiceImpl implements PolluteFeeService {
    private static final Logger logger = LoggerFactory.getLogger(PolluteFeeServiceImpl.class);

    private static boolean isRunning = false;

    @Resource
    PolluteFeeMapper polluteFeeMapper;
    @Resource
    MeterMapper meterMapper;
    @Resource
    SystemConfigMapper systemConfigMapper;
    @Resource
    PolluteFreeJobService polluteFreeJobService;
    @Resource
    PagePermissionService pagePermissionService;

    /**
     * 缴费信息列表
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findPolluteFeeList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = polluteFeeMapper.findPolluteFeeListCount(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        paramMap.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = polluteFeeMapper.findPolluteFeeList(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 客户排污统计信息
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findPolluteFeeStatistics(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        return polluteFeeMapper.findPolluteFeeStatistics(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
    }

    /**
     * 缴费日历
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findsearchPolluteFeeCalendar(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        return polluteFeeMapper.findsearchPolluteFeeCalendar(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
    }

    /**
     * 缴费明细
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findPolluteFeeDetailInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_B_O);//不能为空
        Map<String, Object> result = new HashMap();
        //缴费主表
        Map<String, Object> fee = RegexUtil.optMapOrNew(polluteFeeMapper.findCustomerPolluteFeeById(methodParam.getSchemaName(), paramMap));
        if (!RegexUtil.optIsPresentMap(fee))
            return result;
        result.put("fee", fee);
        String belong_month = String.valueOf(fee.get("belong_month"));
        paramMap.put("belong_month", belong_month);
        //缴费明细列表
        List<Map<String, Object>> feeDetails = RegexUtil.optNotNullListOrNew(polluteFeeMapper.findCustomerPolluteFeeDetailByFeeId(methodParam.getSchemaName(), paramMap));
        feeDetails.forEach(e -> {
            if ("0".equals(e.get("fee_type")))
                result.put("normal", e);
            else
                result.put("exceed", e);
        });
        //超标因子
        List<Map<String, Object>> surveys = RegexUtil.optNotNullListOrNew(meterMapper.findRatioList(methodParam.getSchemaName(), fee));
        Map<String, Map<String, Object>> map = new LinkedHashMap<>();
        surveys.forEach(e -> {
            String key = String.valueOf(e.get("date_time"));
            if (map.containsKey(key)) {
                ((List<Map<String, Object>>) (map.get(key).get("rows"))).add(e);
            } else {
                Map<String, Object> subMap = new HashMap();
                subMap.put("date_time", e.get("date_time"));
                List<Map<String, Object>> list = new ArrayList();
                list.add(e);
                subMap.put("rows", list);
                map.put(key, subMap);
            }
        });
        result.put("surveys", map.values());
        return result;
    }

    @Override
    public Map<String, Object> recalculatePolluteFeeDetailInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        Map<String, Object> polluteMap = new LinkedHashMap<>();
        Map<String, Object> feeMap = RegexUtil.optMapOrNew(JSON.parseObject(RegexUtil.optStrOrNull(paramMap.get("fee")), Map.class));
        Map<String, Object> normalMap = RegexUtil.optMapOrNew(JSON.parseObject(RegexUtil.optStrOrNull((paramMap.get("normal"))), Map.class));
        Map<String, Object> exceedMap = RegexUtil.optMapOrNew(JSON.parseObject(RegexUtil.optStrOrNull((paramMap.get("exceed"))), Map.class));
        List<Map<String, List<Map<String, Object>>>> surveysList = JSON.parseObject(RegexUtil.optStrOrNull(paramMap.get("surveys")), List.class);
        //重新计算响应的值
        /*
         * 缴费费用就2项：污水处理服务费、处理服务补偿费
         * 污水处理服务费：排污量在缴费信息列表里要显示实际值和缴费值（例如：低于500【基值】时实际值100、缴费值500，在缴费页面显示500）
         * 污水处理服务费：当期污水排放量为总排污量（正常+异常）
         * 缴费公式
         *   费用总金额 = 正常污水处理费用 + 超标污水处理补偿费用
         *   正常污水处理费用 = 正常污水处理量（A）*污水处理单价（B）  公式：A*B
         *   超标污水处理补偿费用 = 超标污水处理量（A）*污水处理单价（B）*各污染因子超标系数（C1+C2+...+Cn）+超标后复测次数（D）*复测采样分析费用（E） 公式: A*B*(C1+C2+...+Cn)+D*E D<3
         *   超标污水处理补偿费用 = 当月污水处理量（Q）*污水处理单价（B）*各污染因子超标系数（C1+C2+...+Cn）+超标后复测次数（D）*复测采样分析费用（E） 公式: Q*B*(C1+C2+...+Cn)+D*E D>=3
         *   单个污染因子超标系数 = 该因子的实测值/该因子的纳管标准限值（保留两位小数）
         * */
        //本期水量
        BigDecimal current_flow = new BigDecimal(RegexUtil.optStrOrVal(feeMap.get("final_flow"), "0"));
        //基值
        BigDecimal payment_value = new BigDecimal(RegexUtil.optStrOrVal(feeMap.get("payment_value"), "0"));
        //污水处理量
        BigDecimal normal_value = new BigDecimal(RegexUtil.optStrOrVal(normalMap.get("final_flow"), "0"));
        //超标水量
        BigDecimal exc_value = new BigDecimal(RegexUtil.optStrOrVal(exceedMap.get("final_flow"), "0"));
        //污水处理单价
        BigDecimal normal_price = new BigDecimal(RegexUtil.optStrOrVal(normalMap.get("price"), "0"));
        BigDecimal exc_price = new BigDecimal(RegexUtil.optStrOrVal(exceedMap.get("price"), "0"));
        //超标系数
        BigDecimal ratio = new BigDecimal(RegexUtil.optStrOrVal(exceedMap.get("ratio"), "0"));
        //复测次数
        BigDecimal repeat_times = new BigDecimal(RegexUtil.optStrOrVal(exceedMap.get("repeat_times"), "0"));
        //复测采样分析费用
        BigDecimal repeat_price = new BigDecimal(RegexUtil.optStrOrVal(exceedMap.get("repeat_fee"), "0"));
        // 复测总费用
        BigDecimal repeat_fee = repeat_times.multiply(repeat_price).setScale(2,BigDecimal.ROUND_HALF_UP);
        //污水处理服务费
        BigDecimal normal_fee;
        if (normal_value.compareTo(payment_value) > 0) {
            normal_fee = normal_value.multiply(normal_price).setScale(2,BigDecimal.ROUND_HALF_UP);
            normalMap.put("final_flow", normal_value);
        } else {
            normal_fee = payment_value.multiply(normal_price).setScale(2,BigDecimal.ROUND_HALF_UP);
            normalMap.put("final_flow", payment_value);
        }
        normalMap.put("amount", normal_fee);
        //处理服务补偿费
        BigDecimal exc_fee = BigDecimal.ZERO;
        if (repeat_times.compareTo(BigDecimal.valueOf(3)) < 0) {
            exc_fee = exc_value.multiply(exc_price).multiply(ratio).add(repeat_fee).setScale(2,BigDecimal.ROUND_HALF_UP);
        } else if (repeat_times.compareTo(BigDecimal.valueOf(3)) >= 0) {
            exc_fee = current_flow.multiply(exc_price).multiply(ratio).add(repeat_fee).setScale(2,BigDecimal.ROUND_HALF_UP);
            exceedMap.put("final_flow", current_flow);
        }
        exceedMap.put("amount", exc_fee);
        feeMap.put("normal_amount", normal_fee);
        feeMap.put("exceed_amount", exc_fee);
        feeMap.put("amount", normal_fee.add(exc_fee));
        polluteMap.put("fee", feeMap);
        polluteMap.put("normal", normalMap);
        polluteMap.put("exceed", exceedMap);
        polluteMap.put("surveys", surveysList);
        return polluteMap;
    }

    @Override
    public Map<String, Map<String, Object>> queryCusNameByName(String schemaName, List<String> cusNameList) {
        return polluteFeeMapper.queryCusNameByName(schemaName, cusNameList);
    }

    @Override
    public int findPolluteFeeByCondition(String schemaName, Map<String, Object> paramMap) {
        return polluteFeeMapper.findPolluteFeeByCondition(schemaName, paramMap);
    }

    @Override
    public void insertPolluteFee(String schemaName, Map<String, Object> paramMap) {
        polluteFeeMapper.insertPolluteFee(schemaName, paramMap);
    }

    @Override
    public void insertPolluteFeeNormalDetail(String schemaName, Map<String, Object> paramMap) {
        polluteFeeMapper.insertPolluteFeeNormalDetail(schemaName, paramMap);
    }

    @Override
    public void insertPolluteFeeExcDetail(String schemaName, Map<String, Object> paramMap) {
        polluteFeeMapper.insertPolluteFeeExcDetail(schemaName, paramMap);
    }

    @Override
    public int findPolluteFeeByUpdate(String schemaName, Map<String, Object> paramMap) {
        return polluteFeeMapper.findPolluteFeeByUpdate(schemaName, paramMap);
    }

    @Override
    public int updatePolluteFee(String schemaName, Map<String, Object> paramMap) {
        return polluteFeeMapper.updatePolluteFee(schemaName, paramMap);
    }

    @Override
    public int updatePolluteFeeNormalDetail(String schemaName, Map<String, Object> paramMap) {
        return polluteFeeMapper.updatePolluteFeeNormalDetail(schemaName, paramMap);
    }

    @Override
    public int updatePolluteFeeExcDetail(String schemaName, Map<String, Object> paramMap) {
        return polluteFeeMapper.updatePolluteFeeExcDetail(schemaName, paramMap);
    }

    @Override
    public Map<String, Map<String, Boolean>> getPolluteFeePermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[35]);
    }

    /**
     * 修改排污费
     *
     * @param methodParam
     * @param paramMap
     */
    @Override
    public void modifyPolluteFee(MethodParam methodParam, Map<String, Object> paramMap) {
        // 结单状态不能修改
        if (RegexUtil.optIsPresentStr(paramMap.get("isSubmit"))) {
            RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
            polluteFeeMapper.modifyPolluteFee(methodParam.getSchemaName(), paramMap);
            return;
        }
        Map<String, Object> feeMap = RegexUtil.optMapOrNew(JSON.parseObject(RegexUtil.optStrOrNull(paramMap.get("fee")), Map.class));
        Map<String, Object> normalMap = RegexUtil.optMapOrNew(JSON.parseObject(RegexUtil.optStrOrNull(paramMap.get("normal")), Map.class));
        Map<String, Object> exceedMap = RegexUtil.optMapOrNew(JSON.parseObject(RegexUtil.optStrOrNull(paramMap.get("exceed")), Map.class));
        if (RegexUtil.optIsPresentMap(feeMap)) {
            RegexUtil.optIntegerOrExpParam(feeMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
            String status = RegexUtil.optStrOrNull(feeMap.get("status"));
            RegexUtil.trueExp("2".equals(status), LangConstant.TITLE_MP);
            polluteFeeMapper.modifyPolluteFee(methodParam.getSchemaName(), feeMap);
        }
        if (RegexUtil.optIsPresentMap(normalMap)) {
            RegexUtil.optIntegerOrExpParam(normalMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
            polluteFeeMapper.modifyPolluteFeeDetailNormal(methodParam.getSchemaName(), normalMap);
        }
        if (RegexUtil.optIsPresentMap(exceedMap)) {
            RegexUtil.optIntegerOrExpParam(exceedMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
            polluteFeeMapper.modifyPolluteFeeDetailExceed(methodParam.getSchemaName(), exceedMap);
        }
    }

    /**
     * 审核缴费信息
     *
     * @param methodParam
     * @param model
     */
    @Override
    public void auditPolluteFee(MethodParam methodParam, PolluteFeeModel model) {
        RegexUtil.optIntegerOrExpParam(model.getId(), LangConstant.TITLE_B_O);//不能为空
        Map<String, Object> param = new HashMap();
        param.put("id", model.getId());
        String newStatus = RegexUtil.optStrOrBlank(model.getPass());
        model.setAuditor(methodParam.getUserId());
        if (newStatus.equals("1"))
            model.setStatus(1);  //这里改成枚举
        else
            model.setStatus(0);
        polluteFeeMapper.auditPolluteFee(methodParam.getSchemaName(), model);
    }

    @Override
    public void autoPolluteFeeSeting(MethodParam methodParam, Map<String, Object> paramMap) {
        JSONArray jsonArray = JSONArray.fromObject(paramMap.get("sysConfigs"));
        for (Object object : jsonArray) {
            Map<String, Object> map = (Map<String, Object>) JSONObject.fromObject(object);
            systemConfigMapper.updateSysConfig(methodParam.getSchemaName(), map);

            if (Objects.equals(map.get("config_name"), "pollute_shedule_begin_day")) {
                Map<String, Object> param = new HashMap<>();
                param.put("task_class", "com.gengyun.senscloud.job.PolluteFreeJobBean");
                // 0 0 0 1 * ?
                String cron = "0 0 0 @month_day@ * ?";
                String upCron = cron.replaceAll("@month_day@", RegexUtil.optStrOrVal(map.get("setting_value"), "1"));
                param.put("expression", upCron);
                polluteFeeMapper.modfiySysShedule(methodParam.getSchemaName(), param);
            }
        }
    }

    @Override
    public List<Map<String, Object>> findPolluteFeeSetingInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        String group_title = RegexUtil.optStrOrExpNotNull(paramMap.get("group_title"), LangConstant.TITLE_AAZ_P);
        return polluteFeeMapper.polluteFeeMapper(methodParam.getSchemaName(), group_title);
    }

    @Override
    @Async
    public void asyncCronJobToGeneratePolluteFree(String schemaName) {
        logger.info("PolluteFeeServiceImpl ~ start-" + RegexUtil.optStrOrBlank(schemaName));
        if (!isRunning) {
            try {
                //加锁，防止定时器重复执行，
                isRunning = true;
                logger.info("PolluteFeeServiceImpl ~ start-sub-" + RegexUtil.optStrOrBlank(schemaName));
                polluteFreeJobService.cronJobToGeneratePolluteFree(schemaName);
                logger.info("PolluteFeeServiceImpl ~ end-sub-" + RegexUtil.optStrOrBlank(schemaName));
            } catch (Exception e) {
                logger.error("PolluteFeeServiceImpl.asyncCronJobToGeneratePolluteFree fail ~ ", e);
            } finally {
                //运行完后释放锁
                isRunning = false;
            }
        }
        logger.info("PolluteFeeServiceImpl ~ end-" + RegexUtil.optStrOrBlank(schemaName));
    }
}
