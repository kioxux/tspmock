package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.BroseService;
import org.springframework.stereotype.Service;

@Service
public class BroseServiceImpl implements BroseService {
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    BroseMapper broseMapper;
//
//    @Override
//    @Transactional
//    public boolean dealBroseAssetMonitorStatus(String schema_name ,String id ,String dealStatus,String strcode) {
//        Timestamp time = new Timestamp(System.currentTimeMillis());
//        int status = 0;
//        if(dealStatus .equals("maintain")){
//            status = 4;
//        }else if(dealStatus .equals("deleteMaintain")){
//            //查询maintain之之前的状态
//            String str_status = broseMapper.selectBeforeStatus(schema_name,strcode);
//            if(str_status != null){
//                status = Integer.parseInt(str_status);
//            }else{
//                status = 2;
//            }
//
//        }
//        int update =  broseMapper.setSelectStatus(schema_name,id,status);
//        boolean result = false;
//        if(update>0){//修改状态成功，向设备监控插入maintain数据
//            int inser = broseMapper.addMaintain(schema_name ,strcode,time.toString(),status);
////                处理事时表
//                broseMapper.findNeedDealAssetMonitorListRealTime(schema_name);
//            if(inser>0){
//                result = true;
//            }
//        }
//        return result;
//    }
//
//    @Override
//    public List<BroseAssetMonitorModel> getAssetMonitorTable(String schema_name) {
//        List<BroseAssetMonitorModel> broseAssetMonitorModels = new ArrayList<BroseAssetMonitorModel>();
//        List<BroseMonitorModel> broseMonitorModels = new ArrayList<BroseMonitorModel>();
//        List<BroseAssetMonitorModel> broseAssetMonitorModelList = broseMapper.getAssetMonitorTable(schema_name);
//        broseMonitorModels = broseMapper.getMonitorData(schema_name);
//
//        for(BroseAssetMonitorModel broseAssetMonitorModel:broseAssetMonitorModelList){
//            String strcode = broseAssetMonitorModel.getStrcode();
//            String tem = "";
//            String hum = "";
//            String setting_tem = "";
//            String setting_hum = "";
//            String monitor_status = "";
//            String error_info = "";
//            for(BroseMonitorModel broseMonitorModel:broseMonitorModels){
//                if(strcode.equals(broseMonitorModel.getAsset_code())){
//                    String unit = broseMonitorModel.getUnit();
//                    if(unit!=null && unit.equals("℃")){
//                        tem = broseMonitorModel.getValue();
//                    }else if(unit !=null && unit.equals("%")){
//                        hum = broseMonitorModel.getValue();
//                    }else if(unit !=null && unit.equals("S")){
//                        monitor_status = broseMonitorModel.getValue();
//                        error_info = broseMonitorModel.getError_info();
//                    }
//                }
//            }
//            broseAssetMonitorModel.setTemperature(tem);
//            broseAssetMonitorModel.setHumidity(hum);
//
//            if(monitor_status!=null && monitor_status.equals("1")){
//                monitor_status = "Running";
//            }else if(monitor_status!=null && monitor_status.equals("2")){
//                monitor_status = "Preparation";
//            }else if(monitor_status!=null && monitor_status.equals("3")){
//                monitor_status = "Failure";
//            }else if(monitor_status!=null && monitor_status.equals("4")){
//                monitor_status = "Maintenance";
//            }else if(monitor_status!=null && monitor_status.equals("5")){
//                monitor_status = "Offline";
//            }
//            broseAssetMonitorModel.setMonitor_status(monitor_status);
//            broseAssetMonitorModel.setErrorInfo(error_info);
//            broseAssetMonitorModels.add(broseAssetMonitorModel);
//        }
//        return broseAssetMonitorModels;
//    }
//
//    @Override
//    public BroseAssetMonitorModel getAssetMonitorTableById(String schema_name ,String id) {
//        BroseAssetMonitorModel broseAssetMonitorModel = new BroseAssetMonitorModel();
//        broseAssetMonitorModel = broseMapper.getAssetMonitorTableById(schema_name,id);
//        return broseAssetMonitorModel;
//    }
//
//    @Override
//    public BroseTodayCurveModel getTemAndHumByAssetCode(String schema_name , String strcode , int i){
//        //根据设备编号查询数据采集表
//        BroseTodayCurveModel broseTodayCurveModel = new BroseTodayCurveModel();
//        broseTodayCurveModel.setAsset_code(strcode);
//        String temperature = "";
//        String humidity = "";
//        String temperature_setting = "";
//        String humidity_setting = "";
//        String gatherTime = "";
//        List <BroseMonitorModel> broseMonitorModelList =  broseMapper.getTemAndHumByAssetCode(schema_name , strcode , i);
//        for(BroseMonitorModel broseMonitorModel : broseMonitorModelList){
//            String unit = broseMonitorModel.getUnit();//监控单位
//            String tem_value = broseMonitorModel.getTem_value()+",";//温度实际值
//            String hum_value = broseMonitorModel.getHum_value()+",";//湿度实际值
//            String  gather = broseMonitorModel.getGather_time();
//            gather = gather.substring(0,19);
//            String gather_time = gather+",";//采集时间
//            String value_tem_setting = broseMonitorModel.getSetting_tem_value() +",";
//            String value_hum_setting = broseMonitorModel.getSetting_hum_value()+",";
////            if(unit != null && unit.equals("℃")){//温度
//            temperature = temperature + tem_value;
//            temperature_setting = temperature_setting + value_tem_setting;
////            }else if(unit !=null && unit.equals("%")){//湿度
//            humidity = humidity + hum_value;
//            humidity_setting = humidity_setting + value_hum_setting;
////            }
//            gatherTime = gatherTime + gather_time;
//        }
//        if(humidity!=null && !humidity.equals("")){
//            humidity = humidity.substring(0,humidity.length()-1);
//        }
//        if(temperature!=null && !temperature.equals("")){
//            temperature = temperature.substring(0,temperature.length()-1);
//        }
//        if(temperature_setting!=null && !temperature_setting.equals("")){
//            temperature_setting = temperature_setting.substring(0,temperature_setting.length()-1);
//        }
//        if(humidity_setting!=null && !humidity_setting.equals("")){
//            humidity_setting = humidity_setting.substring(0,humidity_setting.length()-1);
//        }
//
//        if( gatherTime!=null && !gatherTime.equals("")){
//            gatherTime = gatherTime.substring(0,gatherTime.length()-1);
//        }
//        broseTodayCurveModel.setHumidity(humidity);
//        broseTodayCurveModel.setTemperature(temperature);
//        broseTodayCurveModel.setGather_time(gatherTime);
//        broseTodayCurveModel.setTemperature_setting(temperature_setting);
//        broseTodayCurveModel.setHumidity_setting(humidity_setting);
//        return broseTodayCurveModel;
//    }
//
//    //查询13种设备的状态效率（建议使用缓存机制）
//    @Override
//    public List<BroseThreeDataModel> getAllAssetThreeMonth(String schema_name,String month){
//        List<BroseThreeDataModel> broseThreeDataModelList = broseMapper.getAllAssetThreeMonth(schema_name);
//        return broseThreeDataModelList;
//    }
//
//    //设备监控地图展示6月占比和统计结果数据查询(建议使用缓存机制)
//    @Override
//    public Map<String,String> getAllAssetSixMonth(String schema_name, String month){
//
//        Map<String,String> map = new HashMap<String , String>();
//        if(month!=null && !month.equals("")){
//            int mon_int = Integer.parseInt(month)-1;
//            String running = "";
//            String standby = "";
//            String failure = "";
//            String maintenance = "";
//            String monthString = "";
//            String offline = "";
//
//            SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON_2);
//            Calendar c = Calendar.getInstance();
//            for(int i=mon_int; i>=0;i--){
//                c.setTime(new Date());
//                c.add(Calendar.MONTH, -i);
//                Date m = c.getTime();
//                String mon_str = format.format(m);
//                List<BroseStatusRateMonthModel> broseStatusRateMonthModelList = broseMapper.getAllAssetSixMonth(schema_name,i);
//                if(broseStatusRateMonthModelList!=null && broseStatusRateMonthModelList.size()>0){
//                    String [] str = new String[broseStatusRateMonthModelList.size()];
//                    boolean run = false;
//                    boolean stan = false;
//                    boolean fai = false;
//                    boolean main =false;
//                    boolean off =false;
//                    float result_all = 0.0f;
//                    for(int k=0;k<broseStatusRateMonthModelList.size();k++){
//                        result_all = result_all + broseStatusRateMonthModelList.get(k).getResult_data();
//                    }
//                    for(int k=0;k<broseStatusRateMonthModelList.size();k++){//  BroseStatusRateMonthModel broseStatusRateMonthModel1:broseStatusRateMonthModelList
//                        String nape = broseStatusRateMonthModelList.get(k).getNape();
//                        float tip = broseStatusRateMonthModelList.get(k).getResult_data();
//                        NumberFormat numberFormat = NumberFormat.getInstance();
//                        numberFormat.setMaximumFractionDigits(2);
//                        String result_data = numberFormat.format((float) tip / (float) result_all * 100)+"";
//                        switch(nape){
//                            case "1":
//                                running = running + result_data+",";
//                                run = true;
//                                break;
//                            case "2":
//                                standby = standby +result_data+",";
//                                stan = true;
//                                break;
//                            case "3":
//                                failure = failure + result_data+",";
//                                fai = true;
//                                break;
//                            case "4":
//                                maintenance = maintenance +result_data+",";
//                                main = true;
//                                break;
//                            case "5":
//                                offline = offline +result_data+",";
//                                off = true;
//                                break;
////                            default :
////                                running = running + ",";
////                                standby = standby + ",";
////                                failure = failure + ",";
////                                maintenance = maintenance + ",";
////                                offline = offline + ",";
//                        }
//                        str[k] = nape;
//                    }
//                    if(!run){
//                        running = running + "0,";
//                    }
//                    if(!stan){
//                        standby = standby + "0,";
//                    }
//                    if(!fai){
//                        failure = failure + "0,";
//                    }
//                    if(!main){
//                        maintenance = maintenance + "0,";
//                    }
//                    if(!off){
//                        offline = offline + "0,";
//                    }
//                    monthString = monthString + mon_str + ",";
//
//                }else{
//                    running = running + "0,";
//                    standby = standby + "0,";
//                    failure = failure + "0,";
//                    maintenance = maintenance + "0,";
//                    offline = offline + "0,";
//                    monthString = monthString + mon_str + ",";
//                }
//            }
//            running = running.substring(0,running.length()-1);
//            standby = standby.substring(0,standby.length()-1);
//
//
//            failure = failure.substring(0,failure.length()-1);
//            maintenance = maintenance.substring(0,maintenance.length()-1);
//            offline = offline.substring(0,offline.length()-1);
//            monthString = monthString.substring(0,monthString.length()-1);
//            String [] failure_arry = failure.split(",");
//            String [] maintenance_arry = maintenance.split(",");
//            String [] offline_arr = offline.split(",");
//            String [] running_arr = running.split(",");
//            String [] standby_arr = standby.split(",");
//
//            String inuse_run_standby = "";
//
//            if(running_arr!=null && running_arr.length>0 && standby_arr!=null && standby_arr.length>0){
//                for(int i=0;i<running_arr.length;i++){
//                    String run = running_arr[i];
//                    for(int j=0;j<standby_arr.length;j++){
//                        if(i==j){
//                            String stan = standby_arr[j];
//                            if(run!=null && !run.equals("")&&stan!=null && !stan.equals("")){
//                                float inuse = Float.parseFloat(run)+Float.parseFloat(stan);
//                                inuse_run_standby = inuse_run_standby + String.valueOf(inuse)+",";
//                            }
//
//                        }
//                    }
//                }
//            }
//            inuse_run_standby = inuse_run_standby.substring(0,inuse_run_standby.length()-1);
//
//            map.put("inuse",inuse_run_standby);
//            map.put("standby",standby);
//            map.put("failure",failure);
//            map.put("maintenance",maintenance);
//            map.put("offline",offline);
//            map.put("monthString",monthString);
//        }else{
//            return map;
//        }
//        return map;
//    }
}
