package com.gengyun.senscloud.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.ReportMapper;
import com.gengyun.senscloud.mapper.SelectOptionsMapper;
import com.gengyun.senscloud.mapper.StatisticMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.ReportMonthModel;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.StatisticService;
import com.gengyun.senscloud.service.language.impl.MultipleLanguagesServiceImpl;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.*;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StatisticServiceImpl implements StatisticService {
    private Logger logger = LoggerFactory.getLogger(StatisticServiceImpl.class);
    @Value("${senscloud.file_upload}")
    private String file_upload_dir;
    @Resource
    StatisticMapper statisticMapper;
    @Resource
    ReportMapper reportMapper;
    @Resource
    CompanyService companyService;
    @Resource
    SqlSessionFactory sqlSessionFactory;
    @Resource
    LogsService logService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    MultipleLanguagesServiceImpl multipleLanguagesService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 入参
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getReportPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[27]);
    }

    // new 获取报告详情，yzj 2021-02-24
    @Override
    public Map<String, Object> getReportDetailList(MethodParam methodParam, ReportMonthModel reportMonthMdel) {
        try {
            String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
            String beginDate = reportMonthMdel.getBeginDate();
            String endDate = reportMonthMdel.getEndDate();
            if (null == beginDate || "".equals(beginDate) || null == endDate || "".equals(endDate)) {
                return null;
            }
            Map<String, Object> params = new HashMap<>();
            int total = reportMapper.getReportDetailCount(beginDate, endDate);
            params.put("total", total);
            if (total > 1) {
                //查询数据列表
                List<Map<String, Object>> rows = reportMapper.getReportDetailList(pagination, beginDate, endDate);
                params.put("rows", rows);
            }

            return params;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据id获取统计报表数据
     *
     * @param methodParam 入參
     * @param params      入參
     * @return 统计报表数据
     */
    @Override
    public Map<String, Object> getStatisticsReportData(MethodParam methodParam, Map<String, Object> params) {
        Integer id = RegexUtil.optIntegerOrExpParam(params.get("id"), LangConstant.TITLE_AAY_Y);//统计配置不能为空
        Map<String, Object> statisticsInfo = this.getStatisticById(methodParam, id);
        if (!RegexUtil.optNotNull(statisticsInfo).isPresent()) {
            throw new SenscloudException(LangConstant.TITLE_AAY_Y);//统计配置不能为空
        }
        Map<String, Object> result = new HashMap<>();
        if (RegexUtil.optIsPresentStr(statisticsInfo.get("query"))) {
            String query = statisticsInfo.get("query").toString();
            //执行查询语句，对语句进行解密
            if (!RegexUtil.optIsPresentStr(query) || isCanExecuteStatistic(methodParam, query)) {
                return result;
            }
            params.put("schema_name", methodParam.getSchemaName());
            SqlSession session = null;
            List<Map> row;
            try {
                session = sqlSessionFactory.openSession();
                SqlMapper sqlMapper = new SqlMapper(session);
                row = sqlMapper.selectList("<script> " + query + " </script>", params, Map.class);
            } catch (Exception e) {
                throw new SenscloudError(methodParam, ErrorConstant.EC_STATISTIC_002, "id:" + id);
            } finally {
                if (null != session) {
                    session.close();
                }
            }
            result.put("row", row);
        }
        return result;
    }

    /**
     * 根据id获取统计数据
     *
     * @param methodParam 系统参数
     * @param params      页面参数
     * @param isChart     是否为图形
     * @return 统计数据
     */
    @Override
    public Map<String, Object> getStatisticsTableData(MethodParam methodParam, Map<String, Object> params, boolean isChart) {
        Integer id = RegexUtil.optIntegerOrExpParam(params.get("id"), LangConstant.TITLE_AAY_Y);//统计配置不能为空
        Map<String, Object> statisticsInfo = RegexUtil.optMapOrExpNullInfo(this.getStatisticById(methodParam, id), LangConstant.TITLE_AAY_Y);//统计配置信息不存在
        Map<String, Object> result = new HashMap<>();
        RegexUtil.optNotBlankStrOpt(statisticsInfo.get(isChart ? "query" : "table_query")).ifPresent(query -> {
            String pagination = SenscloudUtil.changePagination(methodParam); // 分页参数
            params.put("schema_name", methodParam.getSchemaName());
            params.put("user_id", methodParam.getUserId());
            if (query.contains(SqlConstant.SQL_WORK_AUTH_NAME)) {
                query = query.replace(SqlConstant.SQL_WORK_AUTH_NAME, SqlConstant.WORK_AUTH_SQL);
            }
            if (query.contains(SqlConstant.SQL_ASSET_AUTH_NAME)) {
                query = query.replace(SqlConstant.SQL_ASSET_AUTH_NAME, SqlConstant.ASSET_AUTH_SQL);
            }
            if (query.contains(SqlConstant.SQL_BOM_AUTH_NAME)) {
                query = query.replace(SqlConstant.SQL_BOM_AUTH_NAME, SqlConstant.BOM_AUTH_SQL);
            }
            if (query.contains(SqlConstant.SQL_BOM_WORK_AUTH_NAME)) {
                query = query.replace(SqlConstant.SQL_BOM_WORK_AUTH_NAME, SqlConstant.BOM_WORK_AUTH_SQL);
            }
            if (query.contains(SqlConstant.SQL_CUSTOMER_AUTH_NAME)) {
                query = query.replace(SqlConstant.SQL_CUSTOMER_AUTH_NAME, SqlConstant.CUSTOMER_AUTH_SQL);
            }
            try (SqlSession session = sqlSessionFactory.openSession()) {
                SqlMapper sqlMapper = new SqlMapper(session);
                if (isChart) {
                    result.put("row", sqlMapper.selectList("<script> " + query + " </script>", params, Map.class));
                } else {
                    Integer total1 = RegexUtil.optNotNullListObj(sqlMapper.selectList("<script> SELECT COUNT(1) FROM ( " + query + " ) AS T </script>", params, Object.class)).map(l -> l.get(0)).map(RegexUtil::optIntegerOrNull).orElse(0);
                    if (total1 < 1) {
                        result.put("row", new ArrayList<>());
                    } else {
                        result.put("row", sqlMapper.selectList("<script> " + query + pagination + " </script>", params, Map.class));
                        result.put("total", total1);
                        result.put("columns", statisticsInfo.get("table_columns"));
                    }
                }
            } catch (Exception e) {
                logger.error(ErrorConstant.EC_STATISTIC_001 + id, e);
            }
        });
        return result;
    }

    /**
     * 查询统计分析配置列表
     *
     * @param methodParam 入參
     * @param param       入參
     * @return
     */
    @Override
    public Map<String, Object> getStatisticConfigList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getStatisticConfigWhereString(methodParam, param);

        int total = statisticMapper.findCountStatisticConfigList(schemaName, searchWord); // 获取统计配置的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by s.title desc";
            searchWord += pagination;
            List<Map<String, Object>> list = null;
            list = findStatisticConfigList(schemaName, methodParam.getUserLang(), methodParam.getCompanyId(), searchWord);
            result.put("rows", list);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 添加、编辑统计分析配置
     *
     * @param methodParam 入參
     * @param paramMap    入參
     * @param isAdd       是否新增
     */
    @Override
    public void newStatistic(MethodParam methodParam, Map<String, Object> paramMap, boolean isAdd) {
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optNotNullOrExp(paramMap, LangConstant.MSG_A, new String[]{LangConstant.TITLE_AAY_Y}); // 统计配置不能为空
        String table_query = AESUtils.decryptAES(RegexUtil.optStrOrExpNotNull(paramMap.get("table_query"), LangConstant.TITLE_BA_B));
        String query = AESUtils.decryptAES(RegexUtil.optStrOrExpNotNull(paramMap.get("query"), LangConstant.TITLE_B_L));
        RegexUtil.optStrOrExpNotNull(paramMap.get("title"), LangConstant.TITLE_ABAAA_W);
        int is_show_dashboard = RegexUtil.optIntegerOrVal(paramMap.get("is_show_dashboard"), 0, LangConstant.TITLE_AG_T);
        int group_name = RegexUtil.optSelectOrExpParam(paramMap.get("group_name"), LangConstant.TITLE_ABKL); // 组名称未选择
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "statistic_config_group", String.valueOf(group_name)), LangConstant.TITLE_ABKL); // 组名称不存在
        //解压后先赋值
        paramMap.put("table_query", table_query);
        paramMap.put("query", query);
        paramMap.put("is_show_dashboard", is_show_dashboard);

        //验证值是否有效
        query = (RegexUtil.optIsBlankStr(table_query) ? "" : table_query) + (RegexUtil.optIsBlankStr(query) ? "" : query);
        if (RegexUtil.optIsPresentStr(query) && (query.toLowerCase().indexOf("delete") >= 0 || query.toLowerCase().indexOf("update") >= 0 ||
                query.toLowerCase().indexOf("drop") >= 0 || query.toLowerCase().indexOf("truncate") >= 0)) {
            if (!"yaozhijun".equals(methodParam.getAccount())) {
                throw new SenscloudException(LangConstant.MSG_CA);//权限不足
            }
        }
        if (RegexUtil.optIsPresentStr(paramMap.get("options"))) {
            String options = (String) paramMap.get("options");
            com.alibaba.fastjson.JSONObject optionsJson = com.alibaba.fastjson.JSONObject.parseObject(options);
            paramMap.put("options", optionsJson);
        } else {
            paramMap.put("options", "{}");
        }
        if (RegexUtil.optIsPresentStr(paramMap.get("query_conditions"))) {
            String query_conditions = (String) paramMap.get("query_conditions");
            JSONArray conditionsArray = JSONArray.parseArray(query_conditions);
            paramMap.put("query_conditions", conditionsArray);
        } else {
            paramMap.put("query_conditions", "[]");
        }
        if (RegexUtil.optIsPresentStr(paramMap.get("table_columns"))) {
            String table_columns = (String) paramMap.get("table_columns");
            JSONArray columnsArray = JSONArray.parseArray(table_columns);
//            com.alibaba.fastjson.JSONObject columnsJson = com.alibaba.fastjson.JSONObject.parseObject(table_columns);
            paramMap.put("table_columns", columnsArray);
        } else {
            paramMap.put("table_columns", "[]");
        }
        paramMap.put("create_time", nowTime);
        paramMap.put("schema_name", schemaName);
        paramMap.put("create_user_id", methodParam.getUserId());
        if (isAdd) {
            statisticMapper.insertStatistic(paramMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8005, paramMap.get("id").toString(), LangUtil.doSetLogArray("统计配置新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AAY_Y}));
        } else {
            Map<String, Object> dbInfo = this.getStatisticById(methodParam, Integer.valueOf(paramMap.get("id").toString()));
            statisticMapper.updateStatistic(paramMap);
            net.sf.json.JSONArray log = LangUtil.compareMap(paramMap, dbInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8005, paramMap.get("id").toString(), LangUtil.doSetLogArray("编辑了统计配置", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAY_Y, log.toString()}));
        }

    }

    /**
     * 获取统计配置明细信息，两个sql加密
     *
     * @param methodParam 入參
     * @param id          入參主键
     * @return
     */
    @Override
    public Map<String, Object> getStatisticInfoById(MethodParam methodParam, String id) {
        Map<String, Object> info = this.getStatisticById(methodParam, Integer.valueOf(id));
//        String query = info.get("query").toString();
//        String table_query = info.get("table_query").toString();
//        info.put("query",AESUtils.encryptAES(query));
//        info.put("table_query",AESUtils.encryptAES(table_query));
        return info;
    }

    /**
     * 删除选中统计配置
     *
     * @param methodParam 入參
     */
    @Override
    public void cutStatisticByIds(MethodParam methodParam) {
        String idStr = methodParam.getIds();
        String[] idArr = idStr.split(",");
        int len = idArr.length;
        if (len > 0) {
            statisticMapper.deleteSelectStatistic(methodParam.getSchemaName(), idArr);
            boolean isBatchDel = len == 1 ? false : true;
            String remark = isBatchDel ? LangUtil.doSetLogArrayNoParam("批量删除了统计配置！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了统计配置！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAY_Y});
            for (String id : idArr) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_8005, id, remark);
            }
        }
    }

    /**
     * 显示为看板
     *
     * @param methodParam 入參
     * @param data        入參
     */
    @Override
    public void modifyStatisticShow(MethodParam methodParam, Map<String, Object> data) {
        Map<String, Object> dbStock = this.getStatisticById(methodParam, Integer.valueOf(data.get("id").toString()));
        data.put("schema_name", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(dbStock).isPresent()) {
            statisticMapper.updateStatistic(data);
            net.sf.json.JSONArray logArr = new net.sf.json.JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(dbStock.get("is_show_dashboard"));
            String newIsUse = RegexUtil.optStrOrBlank(data.get("is_show_dashboard"));
            if (!newIsUse.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("统计配置属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_BZ, IsuseEnum.getValue(dbIsUse), IsuseEnum.getValue(newIsUse)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_8005, data.get("id").toString(), logArr.toString());
            }
        }
    }

    /**
     * 查询统计分析列表
     *
     * @param methodParam 入參
     * @param param       入參
     * @return
     */
    @Override
    public List<Map<String, Object>> getStatisticList(MethodParam methodParam, SearchParam param) {
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getStatisticWhereString(methodParam, param);
        List<Map<String, Object>> groupList = selectOptionService.getSelectOptionList(methodParam, "statistic_config_group", null);
        List<Map<String, Object>> rwGroupList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) groupList);
        List<Map<String, Object>> list = findStatisticList(schemaName, methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), searchWord);
        Map<String, List<Map<String, Object>>> mapList = list.stream().collect(Collectors.groupingBy(t -> t.get("group_name").toString()));
        if (RegexUtil.optIsPresentList(rwGroupList)) {
            for (Map<String, Object> group : rwGroupList) {
                group.put("list", RegexUtil.optIsPresentList(mapList.get(group.get("value").toString())) ? mapList.get(group.get("value").toString()) : new ArrayList<>());
            }
            // 用户偏爱报表数据-start
            HashMap<String, Object> map = new HashMap<>();
            map.put("value", "self");
            map.put("text", "默认看板");// title_ain
            map.put("is_lang", "2");
            map.put("reserve1", "");
            map.put("list", RegexUtil.optIsPresentList(mapList.get("self")) ? mapList.get("self") : new ArrayList<>());
            rwGroupList.add(0, map);
            // 用户偏爱报表数据-end
            //报表首页展示数据-start
//            HashMap<String, Object> map = new HashMap<>();
//            map.put("value","dashboard");
//            map.put("text","首页展示");//首页展示
//            map.put("is_lang","2");
//            map.put("reserve1","");
//            map.put("list",RegexUtil.optIsPresentList(mapList.get("dashboard"))?mapList.get("dashboard"):new ArrayList<>());
//            groupList.add(0,map);
            // 报表首页展示数据-end
        }
        return rwGroupList;
//        String schemaName = methodParam.getSchemaName();
//        String searchWord = this.getStatisticWhereString(methodParam, param);
//        List<Map<String, Object>> groupList = selectOptionService.getSelectOptionList(methodParam, "statistic_config_group", null);
//        List<Statistic> list = statisticMapper.findStatisticList(schemaName,methodParam.getUserId(),methodParam.getUserLang(),methodParam.getCompanyId(), searchWord);
//        Map<String, List<Statistic>> mapList = list.stream().collect(Collectors.groupingBy(t -> t.getGroup_name()));
//        if(RegexUtil.optIsPresentList(groupList)){
//            for (Map<String, Object> group : groupList) {
//                group.put("list",RegexUtil.optIsPresentList(mapList.get(group.get("value").toString()))?mapList.get(group.get("value").toString()):new ArrayList<>());
//            }
//            HashMap<String, Object> map = new HashMap<>();
//            map.put("value","self");
//            map.put("text","self");
//            map.put("is_lang","1");
//            map.put("reserve1","");
//            map.put("list",RegexUtil.optIsPresentList(mapList.get("self"))?mapList.get("self"):new ArrayList<>());
//            groupList.add(0,map);
//        }
//        return groupList;
    }

    /**
     * 删除选中看板报表
     *
     * @param methodParam 入參
     * @param id          入參
     */
    @Override
    public void cutUserStatisticDash(MethodParam methodParam, String id) {
        int i = statisticMapper.deleteUserStatisticDash(methodParam.getSchemaName(), methodParam.getUserId(), id);
        if (i > 0) {
            String remark = LangUtil.doSetLogArray("删除了个人统计看板！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAY_Y});
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_5001, id, remark);
        }
    }

    /**
     * 添加用户看板报表
     *
     * @param methodParam 入參
     * @param id          入參
     */
    @Override
    public void newUserStatisticDash(MethodParam methodParam, String id) {
        int i = statisticMapper.insertUserStatisticDash(methodParam.getSchemaName(), methodParam.getUserId(), id);
        if (i > 0) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_5001, id, LangUtil.doSetLogArray("个人看板新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AK_K}));
        }
    }

    @Override
    public Map<String, Map<String, Boolean>> getStatisticConfigListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[20]);
    }

    @Override
    public void modifyStatisticShowForV2(MethodParam methodParam, Map<String, Object> data) {
        Map<String, Object> dbStock = this.getStatisticById(methodParam, Integer.valueOf(data.get("id").toString()));
        data.put("schema_name", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(dbStock).isPresent()) {
            statisticMapper.updateStatistic(data);
            statisticMapper.setDisableDashBoardWithOutChose(data);
            net.sf.json.JSONArray logArr = new net.sf.json.JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(dbStock.get("is_show_dashboard"));
            String newIsUse = RegexUtil.optStrOrBlank(data.get("is_show_dashboard"));
            if (!newIsUse.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("统计配置属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_BZ, IsuseEnum.getValue(dbIsUse), IsuseEnum.getValue(newIsUse)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_8005, data.get("id").toString(), logArr.toString());
            }
        }
    }

    @Override
    public void modifyStatisticShowForV3(MethodParam methodParam, Map<String, Object> data) {
        Integer is_show_dashboard = RegexUtil.optSelectOrExpParam(data.get("is_show_dashboard"), LangConstant.TITLE_BZ);// 显示看板未选择
        Map<String, Object> dbStatistic = this.getStatisticById(methodParam, Integer.valueOf(data.get("id").toString()));
        data.put("schema_name", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(dbStatistic).isPresent()) {
            if (StatusConstant.DASHBOARD_SHOW == is_show_dashboard) {
                RegexUtil.falseExp(statisticMapper.findCountShowDashboard(data) < StatusConstant.DASHBOARD_SHOW_COUNT, LangConstant.TITLE_CFL);//选择最多不能超过5个
            }
            statisticMapper.updateStatistic(data);
//            statisticMapper.setDisableDashBoardWithOutChose(data);
            net.sf.json.JSONArray logArr = new net.sf.json.JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(dbStatistic.get("is_show_dashboard"));
            String newIsUse = RegexUtil.optStrOrBlank(data.get("is_show_dashboard"));
            if (!newIsUse.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("统计配置属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_BZ, IsuseEnum.getValue(dbIsUse), IsuseEnum.getValue(newIsUse)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_8005, data.get("id").toString(), logArr.toString());
            }
        }
    }

    private String getStatisticWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer();
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(s.title) like lower(").append(e).append("))"));
        return whereString.toString();
    }

    /**
     * 根据id查询统计配置详情
     *
     * @param methodParam
     * @param id
     * @return
     */
    private Map<String, Object> getStatisticById(MethodParam methodParam, Integer id) {
        return RegexUtil.optMapOrExpNullInfo(findInfoById(methodParam.getSchemaName(), id, methodParam.getUserLang(), methodParam.getCompanyId()), LangConstant.TITLE_AAY_Y);// 统计配置信息不存在
    }

    /**
     * 查询统计分析配置列表条件
     *
     * @param methodParam 入參
     * @param param       入參
     * @return
     */
    private String getStatisticConfigWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer(" where 1=1 ");
        RegexUtil.optNotBlankStrOpt(param.getGroupName()).ifPresent(e -> whereString.append(" and s.group_name in (").append(DataChangeUtil.joinByStr(e)).append(") "));
//        RegexUtil.optNotBlankStrOpt(param.getIsUseSearch()).ifPresent(e -> whereString.append(" and p.is_use = '").append(e).append("' "));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(s.title) like lower(").append(e).append("))"));
        return whereString.toString();
    }

    // 判断查询语句中是否有非本schema的查询，有的话则不允许查询
    private boolean isCanExecuteStatistic(MethodParam methodParam, String query) {
        boolean result = false;
        //获取所有schema id
        List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew(JSON.parseObject(multipleLanguagesService.requestPublic("findAll", new HashMap<>()), List.class));
        if (RegexUtil.optNotNull(list).isPresent() && list.size() > 0) {
            for (Map<String, Object> company : list) {
                String company_schema = "sc_com_" + company.get("id").toString();
                if (!company_schema.equals(methodParam.getSchemaName()) && query.toLowerCase().indexOf(company_schema) > 0) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * 生成统计分析报表EXCEL,并返回token
     *
     * @param methodParam
     * @param params
     * @return
     */
    @Override
    public String getStatisticsTableDataExport(MethodParam methodParam, Map<String, Object> params) {
        String fileToken = null;
        Map<String, Object> statisticsTableData = getStatisticsTableData(methodParam, params, false);
        List<Map> rows = (List<Map>) statisticsTableData.get("row");
        if (rows != null && rows.size() > 0) {
            String columns = (String) statisticsTableData.get("columns");
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_FILE);
            String sep = System.getProperty("file.separator");
            fileToken = sdf.format(new Date()) + SenscloudUtil.generateUUIDStr();
            String fileName = "statistic_export_" + sdf.format(new Date()) + ".xlsx";
            String filePath = file_upload_dir + sep + methodParam.getSchemaName() + sep + Constants.FILE_EXPORT_DIR + sep + "statistic_export" + sep + fileName;
            File dir = new File(filePath);
            if (!dir.getParentFile().exists()){
                dir.getParentFile().mkdirs();//没有目录则创建目录
            }

            Map<String, Object> stringObjectMap = convertToExcelHeadAndData(rows, columns);
            EasyExcel.write(filePath)
                    // 这里放入动态头
                    .head((List<List<String>>) stringObjectMap.get("head"))
                    .sheet("模板")
                    // 当然这里数据也可以用 List<List<String>> 去传入
                    .doWrite((List<List<Object>>) stringObjectMap.get("data"));
            Map<String, Object> fileTmpInfo = RegexUtil.optMapOrNew(methodParam.getFileTmpInfo());
            Map<String, String> fileInfo = new HashMap<>();
            fileInfo.put("fileName", fileName);
            fileInfo.put("filePath", filePath);
            fileInfo.put("authType", RegexUtil.optNotBlankStrOrExp(SensConstant.MENUS[27] + "_" + SensConstant.PRM_TYPES[4], LangConstant.MSG_CA));// 报表信息导出权限
            fileTmpInfo.put(fileToken, fileInfo);
            MethodParam aaa = RegexUtil.optNotBlankStrOpt(methodParam.getToken())
                    .map(t -> ScdSesRecordUtil.getScdSesDataByKey(t, "loginBaseUserInfo"))
                    .filter(io -> RegexUtil.optNotNull(io).isPresent())
                    .map(io -> (MethodParam) io)
                    .orElseGet(MethodParam::new);
            aaa.setFileTmpInfo(fileTmpInfo);
        }
        return fileToken;
    }

    @Override
    public Map<String, Object> getStatisticInfoNoQueryById(MethodParam methodParam, String id) {
        Map<String, Object> info = this.getStatisticById(methodParam, Integer.valueOf(id));
        info.put("query","");
        info.put("table_query","");
        return info;
    }

    private Map<String, Object> convertToExcelHeadAndData(List<Map> rows, String columns) {
        Map<String, String> columnMap = new HashMap<>();
        if (StringUtils.isNotEmpty(columns)) {
            JSONArray array = JSONArray.parseArray(columns);
            for (Object o : array) {
                JSONObject column = (JSONObject) o;
                columnMap.put((String) column.get("prop"), (String) column.get("label"));
            }
        }
        Map<String, Object> map = new HashMap<>();
        List<List<String>> headList = new ArrayList<>();
        List<List<Object>> dataList = new ArrayList<>();
        if (rows != null) {
            int i = 0;
            for (Map row : rows) {
                if (row != null) {
                    List<Object> data = new ArrayList<>();
                    for (Object key : row.keySet()) {
                        String k = String.valueOf(key);
                        if (!columnMap.containsKey(k)) {
                            continue;
                        }

                        if (i == 0) {
                            List<String> head = new ArrayList<>();
                            head.add(columnMap.get(k));
                            headList.add(head);
                        }
                        data.add(row.get(key));
                    }
                    i++;
                    dataList.add(data);
                }
            }
        }
        map.put("head", headList);
        map.put("data", dataList);
        return map;
    }

    public List<Map<String, Object>> findStatisticConfigList(String schemaName, String userLang, Long company_id, String searchWord) {
        List<Map<String, Object>> list = statisticMapper.findStatisticConfigList(schemaName, searchWord);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                net.sf.json.JSONObject dd = net.sf.json.JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.get("group_method_name"))) {
                        String value = RegexUtil.optStrOrNull(net.sf.json.JSONObject.fromObject(dd.get(sd.get("group_method_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("group_method_name", value);
                        }
                    }
                });
            }
        }
        return list;
    }

    public List<Map<String, Object>> findStatisticList(String schemaName, String user_id, String userLang, Long company_id, String searchWord) {
        List<Map<String, Object>> list = statisticMapper.findStatisticList(schemaName, user_id, searchWord);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                net.sf.json.JSONObject dd = net.sf.json.JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey("title_ain")) {
                        String value = RegexUtil.optStrOrNull(net.sf.json.JSONObject.fromObject(dd.get("title_ain")).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("group_method_name", value);
                        }
                    }

                    if (dd.containsKey(sd.get("group_method_name"))) {
                        String value = RegexUtil.optStrOrNull(net.sf.json.JSONObject.fromObject(sd.get("group_method_name")).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("group_method_name", value);
                        }
                    }
                });
            }
        }
        return list;
    }

    public Map<String, Object> findInfoById(String schemaName,Integer id, String userLang, Long companyId) {
        Map<String, Object> findInfoMap = statisticMapper.findInfoById(schemaName, id);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findInfoById")));
        if (RegexUtil.optIsPresentMap(result)) {
            net.sf.json.JSONObject dd = net.sf.json.JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
            if (RegexUtil.optIsPresentMap(findInfoMap)) {
                if (dd.containsKey(findInfoMap.get("group_method_name"))) {
                    String value = RegexUtil.optStrOrNull(net.sf.json.JSONObject.fromObject(dd.get(findInfoMap.get("group_method_name"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        findInfoMap.put("group_method_name", value);
                    }
                }
            }
        }
        return findInfoMap;
    }

}
