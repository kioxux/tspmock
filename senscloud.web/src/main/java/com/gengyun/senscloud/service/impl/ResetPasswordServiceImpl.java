package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.ResetPasswordService;
import org.springframework.stereotype.Service;


/**
 * ResetPasswordServiceImpl class 用户重置密码接口
 *
 * @author Zys
 * @date 2020/03/19
 */
@Service
public class ResetPasswordServiceImpl implements ResetPasswordService {

//
//    @Autowired
//    UserMapper userMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    MessageService messageService;
//
//    @Autowired
//    VerificationCodeService verificationCodeService;
//
//
//
//    /**
//     * 重置密码
//     * @param schema_name
//     * @param phoneNumber
//     * @return
//     */
//    @Override
//    public ResponseModel resetPassword(String schema_name, String phoneNumber, String newPassWord,String VerificationCode) {
//        if(RegexUtil.isNull(schema_name)||RegexUtil.isNull(phoneNumber)||RegexUtil.isNull(newPassWord)){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//        try{
//            User user=userMapper.getUserByMobile(schema_name,phoneNumber);
//            if(null==user||RegexUtil.isNull(user.getAccount())){
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_USER_ERROR));
//            }
//            String systemVerificationCode=verificationCodeService.getVerificationCode(schema_name,phoneNumber);
//            if(!systemVerificationCode.equals(VerificationCode)){
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.VERITFY_PF));
//            }
//            userMapper.resetUserPassword(schema_name,User.encryptPassword(newPassWord),user.getAccount());
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_VERINONEMIN));
//        }catch (Exception e){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.PASSWORD_FAILURE));
//        }
//
//
//    }
//
//    /**
//     * 发送验证码
//     * @param schema_name
//     * @param phoneNumber
//     * @return
//     */
//    @Override
//    public ResponseModel sendVerificationCode(String schema_name, String phoneNumber) {
//        if(RegexUtil.isNull(schema_name)||RegexUtil.isNull(phoneNumber)){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//        try{
//            User user=userMapper.getUserByMobile(schema_name,phoneNumber);
//            if(null==user||RegexUtil.isNull(user.getAccount())){
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_USER_ERROR));
//            }
//            String VerificationCode=verificationCodeService.getNewestVerificationCode(schema_name,phoneNumber);
//            HttpServletRequest request = HttpRequestUtils.getRequest();
//            request.setAttribute("account",user.getAccount());
//            String[] contentParam = {VerificationCode,"1"};
//            messageService.beginMsg(contentParam, qcloudsmsConfig.SMS_10001002, user.getAccount(), "user",phoneNumber, "system");
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_VERINONEMIN));
//        }catch (Exception e){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//    }
//
//    /**
//     * 验证验证码
//     * @param schema_name
//     * @param phoneNumber
//     * @return
//     */
//    @Override
//    public ResponseModel checkVerificationCode(String schema_name, String phoneNumber,String VerificationCode) {
//        if(RegexUtil.isNull(schema_name)||RegexUtil.isNull(phoneNumber)){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//        try{
//            String systemVerificationCode=verificationCodeService.getVerificationCode(schema_name,phoneNumber);
//            if(!systemVerificationCode.equals(VerificationCode)){
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.VERITFY_PF));
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_VERINONEMIN));
//        }catch (Exception e){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMPANY_INFO_ERR));
//        }
//    }
}
