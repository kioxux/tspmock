package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.AnalysReportService;
import org.springframework.stereotype.Service;

@Service
public class AnalysReportServiceImpl implements AnalysReportService {
//    @Autowired
//    AnalysReportMapper mapper;
//
//    @Autowired
//    AnalysDataMapper analysMapper;
//
//    @Autowired
//    ElasticSearchServiceImpl elasticSearchService;
//
//    @Autowired
//    ResolvingMetaService resolvingMetaService;
//
//    @Autowired
//    FaultServiceImpl faultService;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    IotUrlServiceImpl iotUrlService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @Value("${es.index.monitoring:xinggang.monitoring}")
//    private String index;
//
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数
//    @Override
//    public List<AnalysFacilityRepairMaintainResult> GetFacilityRepairMaintainReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin) {
//        return mapper.GetFacilityRepairMaintainReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//    }
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数的和合计数量
//    @Override
//    public int GetFacilityRepairMaintainReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition) {
//        return mapper.GetFacilityRepairMaintainReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//    }
//
//
//    //按故障类型，进行位置和供应商的设备维护统计
//    @Override
//    public List<AnalysFacilityRepairMaintainResult> GetFacilityRepairTypeReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin) {
//        return mapper.GetFacilityRepairTypeReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//    }
//
//    //按故障类型，进行位置和供应商的设备维护统计，统计总数
//    @Override
//    public int GetFacilityRepairTypeReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition) {
//        return mapper.GetFacilityRepairTypeReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//    }
//
//    //按设备，统计设备维护、保养时效和总数，故障类型的总数和时间
//    @Override
//    public List<AnalysFacilityRepairMaintainResult> GetDeviceRepairMaintainReport(String schema_name, String deviceCondition, String dateCondition, String facilityCondition, String categoryCondition, int pageSize, int begin) {
//        return mapper.GetDeviceRepairMaintainReport(schema_name, deviceCondition, dateCondition, facilityCondition, categoryCondition, pageSize, begin);
//    }
//
//
//    //按设备，统计设备维护、保养时效和总数，故障类型的总数和时间，统计结果的总数
//    @Override
//    public int GetDeviceRepairMaintainReportCount(String schema_name, String deviceCondition, String dateCondition, String facilityCondition, String categoryCondition) {
//        return mapper.GetDeviceRepairMaintainReportCount(schema_name, deviceCondition, dateCondition, facilityCondition, categoryCondition);
//    }
//
//
//    //按个人，统计设备维护、保养时效和总数和时间
//    @Override
//    public List<AnalysFacilityRepairMaintainResult> GetUserRepairMaintainReport(String schema_name, String condition, String groupCondition, String dateCondition, String categoryCondition, int pageSize, int begin) {
//        return mapper.GetUserRepairMaintainReport(schema_name, condition, groupCondition, dateCondition, categoryCondition, pageSize, begin);
//    }
//
//    //按个人，统计设备维护、保养时效和总数和时间的总数facilityCondition, dateCondition, categoryCondition
//    @Override
//    public int GetUserRepairMaintainReportCount(String schema_name, String condition, String groupCondition, String dateCondition, String categoryCondition) {
//        return mapper.GetUserRepairMaintainReportCount(schema_name, condition, groupCondition, dateCondition, categoryCondition);
//    }
//
//    //统计位置的故障率、保养效率、故障间隔时间
//    @Override
//    public List<AnalysFacilityResult> getFacilityReport(String schema_name, String repairCondition, String maintainCondition, String cidCondition, String categoryCondition, Integer pageSize, Integer begin) {
//        return mapper.getFacilitReport(schema_name, repairCondition, maintainCondition, cidCondition, categoryCondition, pageSize, begin);
//    }
//
//    //统计位置的故障率、保养效率、故障间隔时间合计数量
//    @Override
//    public int getFacilityReportCount(String schema_name, String repairCondition, String maintainCondition, String cidCondition, String categoryCondition) {
//        return mapper.getFacilitReportCount(schema_name, repairCondition, maintainCondition, cidCondition, categoryCondition);
//    }
//
//    //供应商的设备故障率，故障明细
//    @Override
//    public List<AnalysSupplierResult> GetSupplierReport(String schema_name, String repairCondition, String sidCondition, String categoryCondition, Integer pageSize, Integer begin) {
//        return mapper.GetSupplierReport(schema_name, repairCondition, sidCondition, categoryCondition, pageSize, begin);
//    }
//
//    //统计供应商的设备故障率，故障明细合计数量
//    @Override
//    public int GetSupplierReportCount(String schema_name, String repairCondition, String sidCondition, String categoryCondition) {
//        return mapper.GetSupplierReportCount(schema_name, repairCondition, sidCondition, categoryCondition);
//    }
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数
//    @Override
//    public List<AnalysFacilityRepairMaintainResult> GetSupplierRepairMaintainReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin) {
//        return mapper.GetSupplierRepairMaintainReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//    }
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数的和合计数量
//    @Override
//    public int GetSupplierRepairMaintainReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition) {
//        return mapper.GetSupplierRepairMaintainReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//    }
//
//
//    //按故障类型，进行位置和供应商的设备维护统计
//    @Override
//    public List<AnalysFacilityRepairMaintainResult> GetSupplierRepairTypeReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin) {
//        return mapper.GetSupplierRepairTypeReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//    }
//
//    //按故障类型，进行位置和供应商的设备维护统计，统计总数
//    @Override
//    public int GetSupplierRepairTypeReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition) {
//        return mapper.GetSupplierRepairTypeReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//    }
//
//    //获取维修的时效数据统计
//    @Override
//    public List<AnalysRepairWorkResult> GetRepairHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin) {
//        return mapper.GetRepairHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//    }
//
//    //获取维修的时效数据统计
//    @Override
//    public int GetRepairHoursReportCount(String schema_name, String condition, String groupColumn) {
//        return mapper.GetRepairHoursReportCount(schema_name, condition, groupColumn);
//    }
//
//    //获取保养的时效数据统计
//    @Override
//    public List<AnalysRepairWorkResult> GetMaintainHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin) {
//        return mapper.GetMaintainHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//    }
//
//    //获取保养的时效数据统计
//    @Override
//    public int GetMaintainHoursCount(String schema_name, String condition, String groupColumn) {
//        return mapper.GetMaintainHoursReportCount(schema_name, condition, groupColumn);
//    }
//
//    //获取巡检的时效数据统计
//    @Override
//    public List<AnalysRepairWorkResult> GetInspectionHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin) {
//        return mapper.GetInspectionHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//    }
//
//    //获取巡检的时效数据统计
//    @Override
//    public int GetInspectionHoursCount(String schema_name, String condition, String groupColumn) {
//        return mapper.GetInspectionHoursReportCount(schema_name, condition, groupColumn);
//    }
//
//    //获取点检的时效数据统计
//    @Override
//    public List<AnalysRepairWorkResult> GetSpotHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin) {
//        return mapper.GetSpotHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//    }
//
//    //获取点检的时效数据统计
//    @Override
//    public int GetSpotHoursCount(String schema_name, String condition, String groupColumn) {
//        return mapper.GetSpotHoursReportCount(schema_name, condition, groupColumn);
//    }
//
//    /********维修分析***********/
//
//    //分析，获取维修的时效数据的总平均值，根据条件
//    @Override
//    public AnalysRepairWorkResult GetRepairHoursByFacilityForChart(String schema_name, String condition) {
//        return mapper.GetRepairHoursByFacilityForChart(schema_name, condition);
//    }
//
//
//    //分析，获取维修的百台设备维修次数，按次数，取前20个，根据条件
//    @Override
//    public List<AnalysRepairWorkResult> GetRepairTimesByPercentForChart(String schema_name, String condition) {
//        return mapper.GetRepairTimesByPercentForChart(schema_name, condition);
//    }
//
//    //分析，单个设备的平均维修时效
//    @Override
//    public AnalysRepairWorkResult GetRepairHourAndTimesByAssetId(String schema_name, String asset_id) {
//        return mapper.GetRepairHourAndTimesByAssetId(schema_name, asset_id);
//    }
//
//    //分析，单个设备的平均故障间隔
//    @Override
//    public AnalysRepairWorkResult GetRepairHourIntervalByAssetId(String schema_name, String asset_id) {
//        return mapper.GetRepairHourIntervalByAssetId(schema_name, asset_id);
//    }
//
//    //获取设备监控数据
//    @Override
//    public List<AssetMonitorData> findAllAssetMonitorList(String schema_name, String condition, int pageSize, int begin) {
//        return mapper.findAllAssetMonitorList(schema_name, condition, pageSize, begin);
//    }
//
//
//    //获取设备监控数据
//    @Override
//    public List<AssetMonitorData> findAllAssetMonitorListByCode(String schema_name, String assetCode) {
//        return mapper.findAllAssetMonitorListByCode(schema_name, assetCode);
//    }
//
//    //获取设备监控数据
//    @Override
//    public List<Map<String, Object>> findAllAssetMonitorListByCodeFromEs(String schema_name, String assetCode, HttpSession session) {
//        Map<String, Object> resultMap = new HashMap<>();
//        List<Map<String, Object>> AssetList = assetMapper.findIotByAssetCode(schema_name, assetCode);
//        if (AssetList.isEmpty()) {
//            return null;
//        }
//        Map<String, Object> Asset = AssetList.get(0);
//        if ((int) Asset.get("iot_status") == 1) {
//            return null;
//        }
//        String category_id = String.valueOf(Asset.get("category_id"));
//        String sub_key = (String) Asset.get("tem_value9");
//        boolean is_sub = false;
//        Map<String, Object> metaMonitor = (Map) resolvingMetaService.monitorMeta(schema_name).get(category_id);
//        if (null == metaMonitor || metaMonitor.isEmpty()) {
//            return null;
//        }
//        Map<String, String> keyNameMap = new HashMap<>();
//        //key namemap
//        Map<String, String> nameValueMap = (Map) metaMonitor.get("nameValueMap");
//
//        //scada字段集合
//        List<String> monitorTableList = (List) metaMonitor.get("monitorTableList");
//
//        SearchRequest searchRequest = new SearchRequest(index);
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//       /* AggregationBuilder termsBuilder = AggregationBuilders.terms("by_searchKeyword").field("asset_code.keyword");
//        AggregationBuilder max = AggregationBuilders.stats("dateMax").field("System_Time_Tag");
//        termsBuilder.subAggregation(max);*/
//        sourceBuilder.query(QueryBuilders.matchQuery("asset_code", assetCode)).sort("System_Time_Tag", SortOrder.DESC).size(1);
//        searchRequest.source(sourceBuilder);
//        resultMap = elasticSearchService.search(searchRequest);
//        List<Map<String, Object>> dataList = (List<Map<String, Object>>) resultMap.get("rows");
//
//        List<Map<String, Object>> resultdataList = new ArrayList<>();
//        for (Map.Entry<String, Object> entry : dataList.get(0).entrySet()) {
//            if (monitorTableList.contains(entry.getKey())) {
//                resultdataList.add(new HashMap<String, Object>() {
//                    {
//                        put("id", entry.getKey());
//                        put("asset_code", assetCode);
//                        put("monitor_name_new", nameValueMap.get(entry.getKey()));
//                        put("monitorValue", entry.getValue());
//                        put("gatherTime", dataList.get(0).get("System_Time"));
//                    }
//                });
//            }
//
//        }
//
//        return resultdataList;
//
//
//    }
//
//    //获取设备监控数据总数
//    @Override
//    public int findAllAssetMonitorListCount(String schema_name, String condition) {
//        return mapper.findAllAssetMonitorListCount(schema_name, condition);
//    }
//
//    @Override
//    public int findAllAssetMonitorListCountByCode(String schema_name, String assetCode) {
//        return mapper.findAllAssetMonitorListCountByCode(schema_name, assetCode);
//    }
//
//
//    @Override
//    public List<BomData> getBom(String schema_name, String condition, int pageSize, int begin) {
//        return mapper.GetBom(schema_name, condition, pageSize, begin);
//    }
//
//    @Override
//    public int getBomCount(String schema_name, String condition) {
//        return mapper.getBomCount(schema_name, condition);
//    }
//
//    //查询备件明细信息
//    @Override
//    public List<AssetMonitorData> analysAssetMonitorListById(String schema_name, int id) {
//        return mapper.analysAssetMonitorListById(schema_name, id);
//    }
//
//    @Override
//    public AssetMonitorData analysAssetMonitorDataById(String schema_name, int id) {
//        return mapper.analysAssetMonitorDataById(schema_name, id);
//    }
//
//    @Override
//    public List<AssetMonitorData> searchHistoryAssetMonitorListByAssetCode(String schema_name, String assetCode, String monitorName, int pageSize, int begin) {
//        return mapper.searchHistoryAssetMonitorListByAssetCode(schema_name, assetCode, monitorName, pageSize, begin);
//    }
//
//    @Override
//    public int searchHistoryAssetMonitorListByAssetCodeCount(String schema_name, String assetCode, String monitorName) {
//        return mapper.searchHistoryAssetMonitorListByAssetCodeCount(schema_name, assetCode, monitorName);
//    }
//
//    @Override
//    public int failurTimes(String schema_name, String facilityCondition, String repairCondition, String maintainCondition) {
//        return mapper.failurTimes(schema_name, facilityCondition, repairCondition, maintainCondition);
//    }
//
//    @Override
//    public int allTime(String schema_name, String facilityCondition, String repairCondition, String maintainCondition) {
//        return mapper.allTime(schema_name, facilityCondition, repairCondition, maintainCondition);
//    }
//
//    @Override
//    public long MTBF(String schema_name, String facilityCondition, String repairCondition, String maintainCondition) {
//        List<RepairData> repairData = mapper.MTBF(schema_name, facilityCondition, repairCondition, maintainCondition);
//        long minMTBF = 0;
//        long skipMTBF = 0;
//        long time = 0;
//        if (repairData.size() > 0) {
//            for (RepairData repair : repairData) {
//                Timestamp occur = repair.getOccurtime();
//                Timestamp finish = repair.getFinishedTime();
//                if (occur != null) {
//                    long timeValue = (occur.getTime() - finish.getTime()) / (1000 * 60);
//                    minMTBF += timeValue;
//                    skipMTBF++;
//                }
//            }
//        }
//        if (skipMTBF != 0) {
//            time = minMTBF / skipMTBF;
//        }
//
//        return time;
//    }
//
//    @Override
//    public long meanTimeToRestore(String schema_name, String facilityCondition, String repairCondition, String maintainCondition) {
//        List<RepairData> repairData = mapper.meanTimeToRestore(schema_name, facilityCondition, repairCondition, maintainCondition);
//        long minMTBF = 0;
//        long skipMTBF = 0;
//        long time = 0;
//
//        if (repairData.size() > 0) {
//            for (RepairData repair : repairData) {
//                long timeValue = 0;
//                Timestamp occur = repair.getOccurtime();
//                Timestamp finish = repair.getFinishedTime();
//                Timestamp create = repair.getCreatetime();
//                Timestamp receive_time = repair.getReveiveTime();
//                Timestamp repair_begin_time = repair.getRepairBeginTime();
//                timeValue = (finish.getTime() - occur.getTime()) / (1000 * 60);
//                minMTBF += timeValue;
//                skipMTBF++;
//            }
//        }
//        if (skipMTBF != 0) {
//            time = minMTBF / skipMTBF;
//        }
//
//        return time;
//    }
//
//    public double MTTR(String schema_name, String facilityCondition, String beginTime, String endTime) {
//        double result = 0.0;
//        Timestamp begin = Timestamp.valueOf(beginTime + " 00:00:00");
//        Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//        double repairTimes = analysMapper.getDashboardRepairTotalFinished(schema_name, facilityCondition, begin, end, null);
//        double repairMinute = analysMapper.getDashboardRepairTotalMinute(schema_name, facilityCondition, begin, end, null);
//        if (repairTimes == 0) {
//            result = 0.0;
//        } else {
//            result = repairMinute / repairTimes;
//        }
//        return result;
//    }
//
//
//    //获取用户的任务完成数量/任务完成率
//    public List<AnalysRepariMaintainTaskCountResult> getUserTaskCount(String schema_name, String condition) {
//        return mapper.getUserTaskCount(schema_name, condition);
//    }
//

}
