package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomAllotService;
import org.springframework.stereotype.Service;

@Service
public class BomAllotServiceImpl implements BomAllotService {
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    BomAllotMapper bomAllotMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    private BomStockListService bomStockListService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    SelectOptionCustomMapper selectOptionCustomMapper;
//
//    @Autowired
//    BomStockListMapper bomStockListMapper;
//
//    @Autowired
//    MessageService messageService;
//
//    @Autowired
//    BomInStockService bomInStockService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    StockMapper stockMapper;
//
//    @Override
//    public String findBomAllotList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 20;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 20;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String[] facilitySearch = {};
//        try {
//            facilitySearch = request.getParameterValues("facilities")[0].split(",");
//        } catch (Exception ex) {
//
//        }
//        String status = request.getParameter("status");
//        String keyWord = request.getParameter("keyWord");
//        User user = AuthService.getLoginUser(request);
//
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        //根据设备组织查询条件，查询用户可见的库房，用于查询备件
//        String stockPermissionCondition = dataPermissionForFacility.getStockPermissionByFacilityAndGroup(schema_name, user, facilitySearch, "stock", null);
//        //根据人员的角色，判断获取值的范围
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (upper(ba.create_user_account) like upper('%" + keyWord + "%') or upper(u.username) like upper('%"
//                    + keyWord + "%'))";
//        }
////        if (null != searchBomType && !"".equals(searchBomType) && !"-1".equals(searchBomType)) {
////            condition += " and t.id in(" + searchBomType + ") ";
////        }
//
//        if (StringUtils.isNotBlank(status)) {
//            condition += " and ba.status = " + status;
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        dataList = bomAllotMapper.getBomAllotList(schema_name, condition, stockPermissionCondition, pageSize, begin);
//        int total = bomAllotMapper.countBomAllotList(schema_name, condition, stockPermissionCondition);
//        handlerFlowData(schema_name, user.getAccount(), dataList);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    @Override
//    public Map<String, Object> queryBomAllotById(String schemaName, String transfer_code) {
//        return bomAllotMapper.queryBomAllotById(schemaName, transfer_code);
//    }
//
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//
//        String result = this.findBomAllotList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("transfer_code");
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        Map<String, Object> bomAllot = bomAllotMapper.queryBomAllotById(schemaName, code);
//        if (!String.valueOf(StatusConstant.COMPLETED).equals(String.valueOf(bomAllot.get("status")))) {//未完成的调拨单可以分配
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_32, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/bom_allot/findSingleBisInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//
//    }
//
//    @Override
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//            Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//            if (null != map_object && map_object.size() > 0) {
//                Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//                Map<String, Object> businessInfo = (Map<String, Object>) dataInfo.get("_sc_bom_allocation");
//                List<Map<String, Object>> allotDetailList = (List<Map<String, Object>>) map_object.get("bomContent3");
//                JSONArray allotDetails = JSONArray.fromObject(map_object.get("bomContent3"));
//                if (allotDetailList == null || allotDetailList.size() == 0) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SELECT_ALLOT_BOM));//验证领用备件;请选择调拨备件。
//                }
//                Map<String, Object> keys = (Map<String, Object>) map_object.get("keys");
//                String key = (String) keys.get("_sc_bom_allocation");
//                boolean isNew = key.startsWith(SqlConstant.IS_NEW_DATA) ? true : false;//判断是否是新增
//                String work_request_type = "";
//                try {
//                    work_request_type = (String) map_object.get("work_request_type");
//                } catch (Exception statusExp) {
//                }
//
//                boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//                String roleIds = (String) map_object.get("roleIds");
//                return addBomAllot(schema_name, doFlowKey, user, processDefinitionId, roleIds, businessInfo, isNew, hasAudit, allotDetailList, allotDetails);
//            }
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    private ResponseModel addBomAllot(String schema_name, String doFlowKey, User user, String processDefinitionId,
//                                      String roleIds, Map<String, Object> businessInfo, boolean isNew, boolean hasAudit,
//                                      List<Map<String, Object>> allotDetailList, JSONArray allotDetails) throws Exception {
//        if (null != doFlowKey && !"".equals(doFlowKey) && Integer.valueOf(doFlowKey) == ButtonConstant.BTN_SUBMIT) {
//            int status;//状态
//            if (hasAudit) {
//                status = StatusConstant.TO_BE_AUDITED;
//            } else {
//                status = StatusConstant.COMPLETED;//如果流程不需要审核，则订单状态为“已完成”
//            }
//            String account = user.getAccount();
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            String code = (String) businessInfo.get("transfer_code");
//            businessInfo.put("status", status);
//            businessInfo.put("create_user_account", account);
//            businessInfo.put("create_time", now);
//            List<Map<String, Object>> detailList = new ArrayList<Map<String, Object>>();
//            if (code != null && !"".equals(code)) {
//                if (allotDetailList != null && allotDetailList.size() > 0) {
//                    for (Map<String, Object> allot : allotDetailList) {
//                        Map<String, Object> detail = new HashMap<String, Object>();
//                        detail.put("transfer_code", code);
//                        detail.put("price",  allot.get("price"));
//                        detail.put("currency_id", allot.get("currency_id"));
//                        detail.put("bom_code", allot.get("bom_code"));
//                        detail.put("material_code", allot.get("material_code"));
//                        detail.put("quantity", allot.get("use_count"));
//                        if (allot.get("use_count") == null || allot.get("use_count").equals("")) {
//                            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SELECT_BOM_ALLOT_COUNT));//请选择调拨备件数量。
//                        }
//                        detailList.add(detail);
//                    }
//                }
//            }
//            if (isNew) {
//                bomAllotMapper.insert(schema_name, businessInfo); //保存备件掉调拨主表信息
//                selectOptionService.doBatchInsertSql(schema_name, "_sc_bom_allocation_detail", SqlConstant._sc_bom_allocation_detail_columns, detailList);
//            } else {
//                bomAllotMapper.update(schema_name, businessInfo);
//                bomAllotMapper.deleAllotDetail(schema_name, code);
//                selectOptionService.doBatchInsertSql(schema_name, "_sc_bom_allocation_detail", SqlConstant._sc_bom_allocation_detail_columns, detailList);
//            }
//
//            String title = selectOptionService.getLanguageInfo(LangConstant.PART_TRANS); // 备件调拨
//            if (!hasAudit) {
//                updateBomStockQuantity(schema_name, account, allotDetails, businessInfo); // 领用人是备件领用发起人；领用数量：
//                bomAllotMapper.updateStatus(schema_name, status, code);//更改状态
//            } else {
//                checkBomStockQuantity(schema_name, allotDetails, businessInfo);//校验库存数量
//                Map map = new HashMap<>();
//                map.put("title_page", title);
//                map.put("sub_work_code", code);
//                map.put("do_flow_key", doFlowKey);
//                map.put("status", status);
//                map.put("create_user_account", account);//发送短信人
//                map.put("create_time", UtilFuns.sysTime());
//                map.put("facility_id", null);
//                // 设置处理人（随机取一个库房管理人）
//                String receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) businessInfo.get("from_stock_code"));
//                map.put("receive_account", receive_account); // 短信接受人
//                if (isNew) {
//                    WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                    if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                        throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_FLOW_START_FAIL));//备件调拨流程启动失败
//                    }
//                    workProcessService.saveWorkProccess(schema_name, code, status, account, Constants.PROCESS_CREATE_TASK); // 进度记录
//                } else {
//                    String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, code);
//                    if (StringUtils.isBlank(taskId))
//                        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_FLOW_ERROR_ALLOT_FAIL));//备件调拨流程信息异常，备件调拨申请失败
//                    String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, code);
//                    boolean isSuccess = workflowService.complete(schema_name, taskId, account, map);
//                    if (!isSuccess) {
//                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_FLOW_START_FAIL));//备件调拨流程启动失败
//                    }
//                    workProcessService.saveWorkProccess(schema_name, code, status, account, taskSid); // 进度记录
//                }
//                messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_32, code,
//                        account, user.getUsername(), null, null); // 发送短信
//            }
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_32), code,
//                    selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_APPLY_SUCCESS), account); // 记录历史;备件调拨申领成功
//            return ResponseModel.ok("ok");
//
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//
//    @Override
//    public ResponseModel bomAllotAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, String node) throws Exception {
//        String title = "";
//        if (node.equals("fromConfirm")) {//出库确定
//            title = selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_OUT_AUDIT);//备件调出审核
//        } else if (node.equals("toConfirm")) {//入库确定
//            title = selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_IN_AUDIT);//备件调入审核
//        } else if (node.equals("fromAudit")) {//出库-备件调出确定
//            title = selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_OUT_CONFIRM);//备件调出确认
//        } else if (node.equals("toAudit")) {//入库-接受备件
//            title = selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_IN_CONFIRM);//备件调入确认
//        }
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_DATA_WRONG));//当前备件领用信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        String approve_remark = (String) map_object.get("approve_remark");//意见
//        Map<String, Object> bom_in_stock_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_allocation");
//
//
//        String transfer_code = (String) bom_in_stock_tmp.get("transfer_code");
//        String from_stock_code = (String) bom_in_stock_tmp.get("from_stock_code");
//        String to_stock_code = (String) bom_in_stock_tmp.get("to_stock_code");
//        StockData stockData =stockMapper.findStockByStockCode(schema_name,to_stock_code);
//        SystemConfigData systemConfigData=systemConfigService.getSystemConfigData(schema_name,"synchronize_price");
//        Boolean synchronizePrice=false;
//        if(null==systemConfigData||"1".equals((systemConfigData).getSettingValue())){
//            synchronizePrice=true;
//        }
//        int stockCurrency=stockData.getCurrency_id();
//        String file_ids = "";//附件ID
//        if (node.equals("fromAudit")) {//出库-备件调出确定
//            file_ids = (String) bom_in_stock_tmp.get("send_file_ids");//附件ID
//        } else if (node.equals("toAudit")) {//入库-接受备件
//            file_ids = (String) bom_in_stock_tmp.get("receive_file_ids");//附件ID
//        }
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String body_property = request.getParameter("body_property");//按钮类型
//        if (StringUtils.isBlank(transfer_code) || StringUtils.isBlank(do_flow_key))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//
//        Map<String, Object> bomAllot = bomAllotMapper.queryBomAllotById(schema_name, transfer_code);
//        if (bomAllot == null)
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_DATE_ERROR_AUDIT_FAIL), title, title));//%s信息异常，%s审核失败
//
//        if (node.equals("fromConfirm")) {//出库确定
//            if (!String.valueOf(bomAllot.get("status")).equals(String.valueOf(StatusConstant.TO_BE_AUDITED)) && !String.valueOf(bomAllot.get("status")).equals(String.valueOf(StatusConstant.PENDING)))
//                return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_STATUS_ERROR_AUDIT_FAIL), title, title));//%s状态异常，%s审核失败
//        }
//        String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, transfer_code);
//        if (StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_FLOW_ERROR_AUDIT_FAIL), title, title));//%s流程信息异常，%s审核失败
//
//        int result = 0;
//        int status = StatusConstant.TO_BE_AUDITED;
//        if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {//如果审核同意，则更新调拨申请附件
//            List<Map<String, Object>> allotDetailList = bomAllotMapper.selectBomAllotDetail(schema_name, transfer_code);//获取备件调拨备件详情信息
//            if (allotDetailList != null && allotDetailList.size() > 0) {
//                for (Map<String, Object> allot : allotDetailList) {
//                    String bom_code = (String) allot.get("bom_code");
//                    String material_code = (String) allot.get("material_code");
//                    int currency_id = (int) allot.get("currency_id");
//
//                    Map<String, Object> bomFromStock = bomAllotMapper.countBomStockQuantity(schema_name, bom_code, from_stock_code, material_code);
//                    float use_count_f = (float) allot.get("quantity");
//                    int use_count = (int) use_count_f;
//                    if (bomFromStock != null) {
//                        int quantity = (int) bomFromStock.get("quantity");
//                        if (use_count > quantity) {
//                            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_STOCKOUT_BOM_ALLOT_FAIL));//库存备件数量不足，无法正常进行备件挑拨
//                        } else {
//                            if (node.equals("fromAudit")) {//出库-备件调出确定
//                                result = bomAllotMapper.updateBomAllotSendStatus(schema_name, file_ids, body_property, transfer_code);
//                            } else if (node.equals("toAudit")) {//入库-接受备件
//                                status = StatusConstant.COMPLETED;
//                                Map<String, Object> businessInfo = new HashMap<String, Object>();
//                                Map<String, Object> businessInfoTo = new HashMap<String, Object>();
//                                Map<String, Object> paramMapValue = new HashMap<String, Object>();
//                                Map<String, Object> paramMapToValue = new HashMap<String, Object>();
//                                paramMapValue.put("stock_code", from_stock_code);//出库
//                                paramMapValue.put("bom_code", bom_code);
//                                paramMapValue.put("material_code", material_code);
//                                paramMapToValue.put("stock_code", to_stock_code);//出库
//                                paramMapToValue.put("bom_code", bom_code);
//                                paramMapToValue.put("material_code", material_code);
//                                Map<String, Object> bom_from = bomStockListMapper.queryBslIdByInfo(schema_name, paramMapValue);
//                                Map<String, Object> bom_to = bomStockListMapper.queryBslIdByInfo(schema_name, paramMapToValue);
//                                businessInfo.put("id", bom_from.get("id"));//更新数量
//                                businessInfo.put("bom_code", bom_code);
//                                businessInfoTo.put("bom_code", bom_code);
//                                businessInfo.put("material_code", material_code);
//                                businessInfoTo.put("material_code", material_code);
//                                businessInfo.put("stock_code", from_stock_code);//出库
//                                businessInfoTo.put("stock_code", to_stock_code);//入库
//                                if (bom_to == null) {//备件库存表是否含有备件，如果没有则只为0 否则保留原最大库存和安全库存数量
//                                    businessInfoTo.put("security_quantity", 0);
//                                    businessInfoTo.put(
//                                            "max_security_quantity", 0);
//                                    businessInfoTo.put("isAddStock", "isAddStock");//入库新增
//                                } else {
//                                    businessInfoTo.put("id", bom_to.get("id"));//入库
//                                }
//                                businessInfoTo.put("currency_id", stockCurrency);//入库新增货币单位
//                                if(synchronizePrice&&stockCurrency==currency_id){
//                                    businessInfoTo.put("show_price", allot.get("price"));//入库新增货币单位
//                                }
//                                //领用时，入库数量为负数
//                                businessInfo.put("quantity", "-" + use_count);
//                                businessInfoTo.put("quantity", use_count + "");
//                                String quantity2 = (String) businessInfo.get("quantity"); // 出入库数量
//                                String quantity3 = (String) businessInfoTo.get("quantity"); // 出入库数量
//                                bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_32, businessInfo, user.getAccount(), selectOptionService.getLanguageInfo(LangConstant.STOCK_OUT_QUANTITY) + quantity2.replace("-", "")); // 调出数量：
//                                bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_32, businessInfoTo, user.getAccount(), selectOptionService.getLanguageInfo(LangConstant.STOCK_IN_QUANTITY) + quantity3);//调入数量：
//                                result = bomAllotMapper.updateBomAllotReceiveStatus(schema_name, file_ids, status, body_property, transfer_code);//更新附件，修改状态为“已完成”
//                            } else {
//                                result = bomAllotMapper.updatebody(schema_name, status, body_property, transfer_code);
//                            }
//                        }
//                    } else {
//                        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_BOM_INFO_IN_STOCK));//所属库房没有对应备件信息
//                    }
//                }
//            }
//        } else if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {//如果审核退回，则调拨申请状态变更为“处理中”
//            status = StatusConstant.PENDING;
//            result = bomAllotMapper.updateStatus(schema_name, status, transfer_code);
//        }
//
//        String title_page = "";
//        String logRemark = "";
//        if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//            title_page = title + selectOptionService.getLanguageInfo(LangConstant.RETURN_A);//退回
//            logRemark = title + selectOptionService.getLanguageInfo(LangConstant.RETURN_A);//退回
//        } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//            title_page = title + selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);//成功
//            logRemark = title + selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);//成功
//        }
//        //完成流程
//        Map<String, String> map = new HashMap<>();
//        map.put("sub_work_code", transfer_code);
//        map.put("title_page", title_page);
//        map.put("do_flow_key", do_flow_key);
//        String roleIds = (String) map_object.get("roleIds");
//        if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//            map.put("receive_account", (String) bom_in_stock_tmp.get("create_user_account"));//短信接受人
//        } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//            String receive_account = null;
//            if (node.equals("fromConfirm")) {//出库确定
//                receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) bom_in_stock_tmp.get("to_stock_code"));
//            } else if (node.equals("toConfirm")) {//入库确定
//                receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) bom_in_stock_tmp.get("from_stock_code"));
//            } else if (node.equals("fromAudit")) {//出库-备件调出确定
//                receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) bom_in_stock_tmp.get("to_stock_code"));
//            }
//            map.put("receive_account", receive_account);//短信接受人
//        }
//        String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, transfer_code);
//        boolean isSuccess = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//        if (approve_remark != null && !approve_remark.equals("")) {
//            logRemark = logRemark + "(" + selectOptionService.getLanguageInfo(LangConstant.WM_OA) + "：" + approve_remark + ")";
//        }
//        if (isSuccess) {
//            workProcessService.saveWorkProccess(schema_name, transfer_code, status, user.getAccount(), taskSid);//工单进度记录
//            messageService.msgPreProcess(ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)?qcloudsmsConfig.SMS_10002006:qcloudsmsConfig.SMS_10002005, map.get("receive_account"), SensConstant.BUSINESS_NO_32, transfer_code,
//                    user.getAccount(), user.getUsername(), null, null); // 发送短信
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_32), transfer_code, logRemark, user.getAccount());
//            return ResponseModel.ok("ok");
//        } else {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_AUDIT_ERROR));//审核出现错误，请重试
//        }
//    }
//
//    @Override
//    public ResponseModel cancel(String schema_name, User user, String in_code) {
//        int doUpdate = bomAllotMapper.updateStatus(schema_name, StatusConstant.CANCEL, in_code);
//        if (doUpdate > 0) {
//            boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, in_code, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            if (deleteflow) {
//                String account = user.getAccount();
//                workProcessService.saveWorkProccess(schema_name, in_code, StatusConstant.CANCEL, account, Constants.PROCESS_CANCEL_TASK);//工单进度记录
//                logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31), in_code, selectOptionService.getLanguageInfo(LangConstant.SPARE_TRANS_APPLY_CANCEL), account);//备件调拨申请作废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SPARE_TRANS_APPLY_SUCC));//备件调拨申请作废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALLOT_CANCEL_FLOW_FAIL));//备件调拨作废流程调用失败
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_TRANS_CANCEL_FAIL));//备件调拨作废失败
//        }
//    }
//
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("transfer_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        //把taskId、权限数据补充到列表数据中去
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("transfer_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//    /**
//     * 备件调出库房数量验证
//     *
//     * @param schema_name
//     * @param bomInStockDetails
//     * @param bomInStock
//     */
//    private void checkBomStockQuantity(String schema_name, JSONArray bomInStockDetails, Map<String, Object> bomInStock) throws Exception {
//        Map<String, Object> bomRow = new HashMap<>();
//        String security_quantity = (String) bomInStock.get("security_quantity");
//        String max_security_quantity = (String) bomInStock.get("max_security_quantity");
//        if (security_quantity == null) {
//            security_quantity = "0";
//        }
//        if (max_security_quantity == null) {
//            max_security_quantity = "0";
//        }
//        bomRow.put("security_quantity", security_quantity);
//        bomRow.put("max_security_quantity", max_security_quantity);
//        for (Object rowData : bomInStockDetails) {
//            net.sf.json.JSONObject bomRowJson = net.sf.json.JSONObject.fromObject(rowData);
//            bomRow.put("isAddStock", "isAddStock"); // 入库
//            bomRow.put("bom_code", bomRowJson.getString("bom_code"));
//            bomRow.put("material_code", bomRowJson.getString("material_code"));
//            bomRow.put("quantity", "-" + bomRowJson.getString("use_count"));
//            bomRow.put("stock_code", (String) bomInStock.get("from_stock_code"));
//            bomStockListService.checkBomStockQuantity(schema_name, bomRow, null);
//        }
//    }
//
//    /**
//     * 备件库房数量更新
//     *
//     * @param schema_name
//     * @param account
//     */
//    private void updateBomStockQuantity(String schema_name, String account, JSONArray bomInStockDetails, Map<String, Object> bomInStock) throws Exception {
//        this.checkBomStockQuantity(schema_name, bomInStockDetails, bomInStock);
//        String security_quantity = (String) bomInStock.get("security_quantity");
//        String max_security_quantity = (String) bomInStock.get("max_security_quantity");
//        if (security_quantity == null) {
//            security_quantity = "0";
//        }
//        if (max_security_quantity == null) {
//            max_security_quantity = "0";
//        }
//        for (Object rowData : bomInStockDetails) {
//            net.sf.json.JSONObject bomRowJson = net.sf.json.JSONObject.fromObject(rowData);
//            updateBomStockQuantity(schema_name, account, "-" + bomRowJson.getString("use_count"), bomRowJson.getString("bom_code"), bomRowJson.getString("material_code"),
//                    (String) bomInStock.get("from_stock_code"), security_quantity, max_security_quantity, "from");//出库
//            updateBomStockQuantity(schema_name, account, bomRowJson.getString("use_count"), bomRowJson.getString("bom_code"), bomRowJson.getString("material_code"),
//                    (String) bomInStock.get("to_stock_code"), security_quantity, max_security_quantity, "to");//入库
//        }
//    }
//
//    /**
//     * 修改库存数量
//     *
//     * @param schema_name
//     * @param account
//     * @param quantity
//     * @param bom_code
//     * @param material_code
//     * @param stock_code
//     * @param security_quantity
//     * @param max_security_quantity
//     * @throws Exception
//     */
//    private void updateBomStockQuantity(String schema_name, String account, String quantity, String bom_code, String material_code, String stock_code, String security_quantity, String max_security_quantity, String source_main) throws Exception {
//        Map<String, Object> bomRow = new HashMap<>();
//        bomRow.put("isAddStock", "isAddStock"); // 入库
//        bomRow.put("bom_code", bom_code);
//        bomRow.put("material_code", material_code);
//        bomRow.put("quantity", quantity);
//        bomRow.put("stock_code", stock_code);
//        bomRow.put("security_quantity", security_quantity);
//        bomRow.put("max_security_quantity", max_security_quantity);
//        String zh_content = "";
//        if (source_main.equals("from")) {
//            zh_content = selectOptionService.getLanguageInfo(LangConstant.STOCK_OUT_QUANTITY);
//            bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_32, bomRow, account, zh_content + quantity.replace("-", "")); // 更新备件库存数量;入库数量：
//        } else if (source_main.equals("to")) {
//            zh_content = selectOptionService.getLanguageInfo(LangConstant.STOCK_IN_QUANTITY);
//            bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_32, bomRow, account, zh_content + quantity); // 更新备件库存数量;入库数量：
//
//        }
//    }
}
