package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.AssetMapper;
import com.gengyun.senscloud.mapper.SncDataImportMapper;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.CloudDataService;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.FacilitiesService;
import com.gengyun.senscloud.service.asset.AssetPositionService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.*;
import io.swagger.annotations.Api;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "导出处理")
@Service
public class ExportServiceImpl implements ExportService {
    @Value("${senscloud.file_upload}")
    private String file_upload_dir;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    SncDataImportMapper sncDataImportMapper;
    @Resource
    AssetMapper assetMapper;
    @Resource
    CloudDataService cloudDataService;
    @Resource
    AssetPositionService assetPositionService;
    @Resource
    FacilitiesService facilitiesService;
    /**
     * 设备导出
     *
     * @param methodParam 系统参数
     * @param assetList   数据信息
     * @return 文件码
     */
    @Override
    public String doDownloadAssetInfo(MethodParam methodParam, List<Map<String, Object>> assetList) {
        RegexUtil.optListOrExp(assetList, LangConstant.TEXT_K); // 信息不存在
        Workbook workbook = null;
        try {
            boolean amFeeShow = PermissionUtil.getPermissionByInfo(methodParam, SensConstant.MENUS[0], SensConstant.PRM_TYPES[7]); // 设备费用显示权限
            Set<String> feeFieldList = null;
            if (amFeeShow) {
                Map<String, Object> feeFieldInfo = selectOptionService.getStaticSelectForInfo(methodParam, "feeFields");
                feeFieldList = RegexUtil.optMapOrNew(feeFieldInfo).keySet();
            }
            Map<String, Object> selectMap = selectOptionService.getStaticSelectForInfo(methodParam, "assetExportSelects"); // 自定义字段下拉框
            Map<String, Object> fieldNameMap = selectOptionService.getStaticSelectForInfo(methodParam, "assetExportFieldNames"); // 字段显示名称
            List<Map<String, Object>> systemCommonFieldList = selectOptionService.getStaticSelectList(methodParam, "assetExportSysFields");
            //List<Map<String, Object>> systemUsualFieldList = selectOptionService.getStaticSelectList(methodParam, "assetExportUsualFields");

            doChangeFieldInfo(systemCommonFieldList);
            //doChangeFieldInfo(systemUsualFieldList);
            Map<String, Object> selectDataMap = new HashMap<>();
            List<Map<String, Object>> customFields = new ArrayList<>();
            workbook = new SXSSFWorkbook(1000);
            int rowCount = 0;
            Sheet sheet = workbook.createSheet(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_I));
            sheet.setDefaultColumnWidth(30);
            Row header = sheet.createRow(rowCount);
            // 系统字段 主，主要
            this.setSheetCell(methodParam, header, systemCommonFieldList, rowCount, null, selectMap, feeFieldList, fieldNameMap, null);
            // 系统字段 副，次要
            //this.setSheetCell(methodParam, header, systemUsualFieldList, rowCount, null, selectMap, feeFieldList, fieldNameMap, null);
            for (Map<String, Object> assetInfo : assetList) {
                // 解析自定义字段
                String fields = RegexUtil.optStrOrBlank(assetInfo.get("fields"));
                if (RegexUtil.optIsPresentStr(fields)) {
                    customFields.addAll(DataChangeUtil.scdStringToList(fields));
                    customFields = customFields.stream().distinct().collect(Collectors.toList());
                }
            }
            // 解析自定义字段
            this.setSheetCell(methodParam, header, customFields, rowCount, null, selectMap, feeFieldList, fieldNameMap, null);
            rowCount = 1;

            List<String> idList = new ArrayList<>();
            for (Map<String, Object> assetInfo : assetList) {
                Row courseRow = sheet.createRow(rowCount++);
                // 解析通用字段，主，重要字段
                this.setSheetCell(methodParam, courseRow, systemCommonFieldList, rowCount, assetInfo, selectMap, feeFieldList, null, selectDataMap);
                Map<String, Object> properties = DataChangeUtil.scdObjToMapOrNew(assetInfo.get("properties"));
                List<Map<String, Object>> pV = RegexUtil.optNotBlankStrOpt(properties.get("value")).map(DataChangeUtil::scdStringToList).orElse(null);
                String fields = RegexUtil.optStrOrNull(assetInfo.get("fields"));
                List<Map<String, Object>> fieldsList = DataChangeUtil.scdStringToList(fields);
                Map<String, Object> propertiesMap = new HashMap<>();
                if (RegexUtil.optIsPresentList(pV)) {
                    pV.forEach(p -> {
                        RegexUtil.optNotNullList(fieldsList).ifPresent(fl -> {
                            fl.forEach(f -> {
                                if (Objects.equals(p.get("field_code"), f.get("field_code"))) {
                                    p.put("field_view_type", f.get("field_view_type"));
                                    p.put("field_source", f.get("field_source"));
                                }
                            });
                        });
                    });
                    for (Map<String, Object> map : pV) {
                        String fileName = RegexUtil.optStrOrBlank(map.get("field_code"));
                        String fileValue = RegexUtil.optStrOrBlank(map.get("field_value"));
                        String selectKey = RegexUtil.optEqualsOpt(map.get("field_view_type"), "select").map(e -> RegexUtil.optStrOrNull(map.get("field_source"))).orElse(null);
                        if (selectKey != null && RegexUtil.optIsPresentStr(fileValue) && "" != fileValue) {
                            fileValue = selectOptionService.getSelectOptionTextByCode(methodParam, selectKey, fileValue);
                        }
                        String finalFileValue = fileValue;
                        RegexUtil.optNotBlankStrOpt(fileValue).ifPresent(s -> propertiesMap.put(fileName, finalFileValue));
                    }
                }
                // 系统字段 副，次要
                //this.setSheetCell(methodParam, courseRow, systemUsualFieldList, rowCount, assetInfo, selectMap, feeFieldList, null, selectDataMap);
                // 解析自定义字段
                this.setSheetCell(methodParam, courseRow, customFields, rowCount, propertiesMap, selectMap, feeFieldList, null, selectDataMap);
                idList.add(RegexUtil.optStrOrBlank(assetInfo.get("id")));
            }
            if (RegexUtil.optIsPresentList(idList)) {
                List<Map<String, Object>> faultList = assetMapper.findFaultRecordListByAssetIds(methodParam.getSchemaName(), idList);
                if (RegexUtil.optIsPresentList(faultList)) {
                    String languageInfoByUserLang = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TAB_FAULT_A);
                    Sheet fault = workbook.createSheet(languageInfoByUserLang);
                    fault.setDefaultColumnWidth(24);
                    Row headerFault = fault.createRow(0);
                    int y = 0;
                    String[] titleFields = {"title_asset_ac_e", "title_asset_a", "title_asset_aj", "title_error_a", "title_comp_a", "title_b"};
                    for (String title : titleFields) {
                        headerFault.createCell(y).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, title));
                        y++;
                    }
                    int rowCount1 = 1;
                    for (Map<String, Object> map : faultList) {
                        Row courseRow = fault.createRow(rowCount1++);
                        courseRow.createCell(0).setCellValue(RegexUtil.optStrOrBlank(map.get("relation_id")));
                        courseRow.createCell(1).setCellValue(RegexUtil.optStrOrBlank(map.get("asset_name")));
                        courseRow.createCell(2).setCellValue(RegexUtil.optStrOrBlank(map.get("asset_code")));
                        courseRow.createCell(3).setCellValue(RegexUtil.optStrOrBlank(map.get("repair_code")));
                        courseRow.createCell(4).setCellValue(RegexUtil.optStrOrBlank(map.get("finished_time")));
                        courseRow.createCell(5).setCellValue(RegexUtil.optStrOrBlank(map.get("work_code")));
                    }
                }
            }
            return doCreateExcel(methodParam, workbook, "asset_export");
        } catch (SenscloudException ex) {
            throw new SenscloudException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
            try {
                if (null != workbook) {
                    workbook.close();
                }
            } catch (Exception wbExp) {
                wbExp.printStackTrace();
            }
        }
    }

    /**
     * 任务模板导出
     *
     * @param methodParam 系统参数
     * @param dataList    数据信息
     * @return 文件码
     */
    @Override
    public String doDownloadTaskTempInfo(MethodParam methodParam, List<Map<String, Object>> dataList) {
        RegexUtil.optListOrExp(dataList, LangConstant.TEXT_K); // 信息不存在
        Workbook workbook = null;
        try {
            Map<String, Object> resultTypeMap = RegexUtil.optMapOrNew(selectOptionService.getStaticSelectForInfo(methodParam, "resultTypes"));
            Map<String, Object> isEnableMap = RegexUtil.optMapOrNew(selectOptionService.getStaticSelectForInfo(methodParam, "is_enable"));
            workbook = new SXSSFWorkbook(1000);
            int rowCount = 0;
            Sheet sheet = workbook.createSheet(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_E));
            sheet.setDefaultColumnWidth(30);
            Row header = sheet.createRow(rowCount);
            int i = Math.max(header.getLastCellNum(), 0);
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_S));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_NAME_AB_J));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_CATEGORY_AE));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_D));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ABKL));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAH_N));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_NAME_AB_C));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BCHN));
            header.createCell(i).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BCIJ));
            rowCount = 1;
            for (Map<String, Object> dataInfo : dataList) {
                Row courseRow = sheet.createRow(rowCount++);
                i = Math.max(courseRow.getLastCellNum(), 0);
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("task_item_code")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("task_item_name")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optNotBlankStrOpt(dataInfo.get("result_type")).map(resultTypeMap::get).map(RegexUtil::optStrOrBlank).orElse(""));
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("requirements")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("group_name")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("task_template_code")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("task_template_name")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optNotBlankStrOpt(dataInfo.get("is_use")).map(isEnableMap::get).map(RegexUtil::optStrOrBlank).orElse(""));
                courseRow.createCell(i).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("remark")));
            }
            return doCreateExcel(methodParam, workbook, "task_temp_export");
        } catch (SenscloudException ex) {
            throw new SenscloudException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
            try {
                if (null != workbook) {
                    workbook.close();
                }
            } catch (Exception wbExp) {
                wbExp.printStackTrace();
            }
        }
    }

    /**
     * 任务项导出
     *
     * @param methodParam 系统参数
     * @param dataList    数据信息
     * @return 文件码
     */
    @Override
    public String doDownloadTaskItem(MethodParam methodParam, List<Map<String, Object>> dataList) {
        RegexUtil.optListOrExp(dataList, LangConstant.TEXT_K); // 信息不存在
        Workbook workbook = null;
        try {
            Map<String, Object> resultTypeMap = RegexUtil.optMapOrNew(selectOptionService.getStaticSelectForInfo(methodParam, "resultTypes"));
            workbook = new SXSSFWorkbook(1000);
            int rowCount = 0;
            Sheet sheet = workbook.createSheet(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_Q));
            sheet.setDefaultColumnWidth(30);
            Row header = sheet.createRow(rowCount);
            int i = Math.max(header.getLastCellNum(), 0);
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_S));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_NAME_AB_J));
            header.createCell(i++).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_CATEGORY_AE));
            header.createCell(i).setCellValue(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_D));
            rowCount = 1;
            for (Map<String, Object> dataInfo : dataList) {
                Row courseRow = sheet.createRow(rowCount++);
                i = Math.max(courseRow.getLastCellNum(), 0);
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("task_item_code")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("task_item_name")));
                courseRow.createCell(i++).setCellValue(RegexUtil.optNotBlankStrOpt(dataInfo.get("result_type")).map(resultTypeMap::get).map(RegexUtil::optStrOrBlank).orElse(""));
                courseRow.createCell(i).setCellValue(RegexUtil.optStrOrBlank(dataInfo.get("requirements")));
            }
            return doCreateExcel(methodParam, workbook, "task_item_export");
        } catch (SenscloudException ex) {
            throw new SenscloudException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
            try {
                if (null != workbook) {
                    workbook.close();
                }
            } catch (Exception wbExp) {
                wbExp.printStackTrace();
            }
        }
    }

    /**
     * 知识库导出
     *
     * @param methodParam 系统参数
     * @param dataList    数据信息
     * @param response    响应信息
     */
    @Override
    public void doDownloadKnowledgeBaseInfo(MethodParam methodParam, List<Map<String, Object>> dataList, HttpServletResponse response) {
        RegexUtil.optListOrExp(dataList, LangConstant.TEXT_K); // 信息不存在
        List<Map<String, String>> ls = new ArrayList<>();
        OutputStream outZ = null;
        String file_separator = System.getProperty("file.separator");
        try {
            for (Map<String, Object> item : dataList) {
                Map<String, String> result = new HashMap<>();
                if (item.containsKey("file_url") && RegexUtil.optIsPresentStr(item.get("file_url"))) {
                    result.put("file_url", RegexUtil.optStrOrNull(item.get("file_url")));
                }
                if (item.containsKey("file_original_name") && RegexUtil.optIsPresentStr(item.get("file_original_name"))) {
                    String file_original_name = RegexUtil.optStrOrBlank(item.get("file_original_name"));
                    String original_name = file_original_name.substring(0, file_original_name.lastIndexOf("."));
                    if ((item.containsKey("asset_name") && RegexUtil.optIsPresentStr(item.get("asset_name"))) || (item.containsKey("asset_code") && RegexUtil.optIsPresentStr(item.get("asset_code")))) {
                        result.put("file_name", RegexUtil.optStrOrBlank(original_name).concat("【").concat(RegexUtil.optStrOrBlank(item.get("asset_name")).concat("-").concat(RegexUtil.optStrOrBlank(item.get("asset_code")))).concat("】"));
                    } else if ((item.containsKey("bom_name") && RegexUtil.optIsPresentStr(item.get("bom_name"))) || (item.containsKey("material_code") && RegexUtil.optIsPresentStr(item.get("material_code")))) {
                        result.put("file_name", RegexUtil.optStrOrBlank(original_name).concat("【").concat(RegexUtil.optStrOrBlank(item.get("bom_name")).concat("-").concat(RegexUtil.optStrOrBlank(item.get("material_code")))).concat("】"));
                    } else if ((item.containsKey("short_title") && RegexUtil.optIsPresentStr(item.get("short_title"))) || (item.containsKey("inner_code") && RegexUtil.optIsPresentStr(item.get("inner_code")))) {
                        result.put("file_name", RegexUtil.optStrOrBlank(original_name).concat("【").concat(RegexUtil.optStrOrBlank(item.get("short_title")).concat("-").concat(RegexUtil.optStrOrBlank(item.get("inner_code")))).concat("】"));
                    } else if (item.containsKey("model_name") && RegexUtil.optIsPresentStr(item.get("model_name"))) {
                        String asset_type_name = RegexUtil.optNotBlankStrOpt(item.get("category_id")).map(w -> RegexUtil.optStrOrNull(selectOptionService.getSelectOptionAttrByCode(methodParam, "asset_type", w, "text"))).orElse(StringUtils.EMPTY);
                        result.put("file_name", RegexUtil.optStrOrBlank(original_name).concat("【").concat(RegexUtil.optStrOrBlank(item.get("model_name"))).concat("-").concat(asset_type_name).concat("】"));
                    } else if (item.containsKey("task_item_code") && RegexUtil.optIsPresentStr(item.get("task_item_code"))) {
                        result.put("file_name", RegexUtil.optStrOrBlank(original_name).concat("【").concat(RegexUtil.optStrOrBlank(item.get("task_item_code"))).concat("】"));
                    } else if (item.containsKey("work_code") && RegexUtil.optIsPresentStr(item.get("work_code"))) {
                        String work_type_name = RegexUtil.optNotBlankStrOpt(item.get("work_type_id")).map(w -> RegexUtil.optStrOrNull(selectOptionService.getSelectOptionAttrByCode(methodParam, "work_type_all", w, "text"))).orElse(StringUtils.EMPTY);
                        result.put("file_name", RegexUtil.optStrOrBlank(original_name).concat("【").concat(RegexUtil.optStrOrBlank(item.get("work_code"))).concat("-").concat(work_type_name).concat("】"));
                    } else {
                        result.put("file_name", file_original_name);
                    }
                }
                RegexUtil.optNotNullMapString(result).ifPresent(e -> {
                    if (e.containsKey("file_url")) {
                        ls.add(result);
                    }
                });
            }
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_FILE);
            String downloadFileName = sdf.format(new Date()) + ".zip";
            String zipName = file_upload_dir.concat(file_separator).concat(methodParam.getSchemaName()).concat(file_separator).concat("tmp").concat(file_separator).concat(downloadFileName);
            response.reset();
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment;filename=" + new String((downloadFileName).getBytes(), "iso8859-1"));
            File file = new File(zipName);
            if (!file.exists()) {
                file.createNewFile();
            }
            outZ = new FileOutputStream(zipName);
            ZipUtils.doCompress(ls, outZ);
            DownloadUtil.download(zipName, downloadFileName,true);
        } catch (SenscloudException ex) {
            throw new SenscloudException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
            try {
                outZ.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                throw new SenscloudException(ex.getMessage());
            }
        }
    }

    /**
     * 单元格处理
     *
     * @param methodParam   系统参数
     * @param courseRow     行信息
     * @param array         字段信息
     * @param rowCount      当前行
     * @param data          行数据
     * @param selectMap     下拉框集合
     * @param feeFieldList  费用字段集合
     * @param fieldNameMap  字段集合
     * @param selectDataMap 下拉框数据集合
     */
    private void setSheetCell(MethodParam methodParam, Row courseRow, List<Map<String, Object>> array, int rowCount, Map<String, Object> data, Map<String, Object> selectMap, Set<String> feeFieldList, Map<String, Object> fieldNameMap, Map<String, Object> selectDataMap) {
        RegexUtil.optNotNullList(array).ifPresent(l -> {
            int i = Math.max(courseRow.getLastCellNum(), 0);
            for (Map<String, Object> field : l) {
                String fieldName = RegexUtil.optStrAndValOrBlank(field.get("name"), field.get("field_code"));
                if (null != feeFieldList && feeFieldList.contains(fieldName)) {
                    continue;
                }
                String cellVal = "";
                if (rowCount == 0) {
                    if (field.containsKey("fieldViewType") && ("select".equals(field.get("fieldViewType"))) && field.containsKey("field_source")) {
                        selectMap.put(fieldName, RegexUtil.optStrOrBlank(field.get("field_source")));
                    }
                    String name = RegexUtil.optStrAndValOrBlank(field.get("alias"), field.get("field_name"));
                    if (RegexUtil.optIsPresentMap(fieldNameMap) && !"".equals(name)) {
                        name = RegexUtil.optStrOrVal(fieldNameMap.get(name), name);
                    }
                    cellVal = commonUtilService.getLanguageInfoByUserLang(methodParam, name);
                    if ((RegexUtil.optIsBlankStr(field.get("name")) && RegexUtil.optIsPresentStr(field.get("field_code")))
                            && (RegexUtil.optIsBlankStr(field.get("alias")) && RegexUtil.optIsPresentStr(field.get("field_name")))) {
                        cellVal = fieldName.concat("【").concat(cellVal).concat("】");
                    } else if (field.containsKey("name") && field.containsKey("field_code") && field.containsKey("alias") && field.containsKey("field_name")) {
                        cellVal = fieldName.concat("【").concat(cellVal).concat("】");
                    }
                } else {
                    if (RegexUtil.optIsPresentMap(data)) {
                        if (RegexUtil.optEquals(field.get("validation"), "date")) {
                            try {
                                Date date = (Date) data.get(fieldName);
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);
                                cellVal = RegexUtil.optStrOrBlank(simpleDateFormat.format(date));
                            } catch (Exception ignored) {
                            }
                        } else if (RegexUtil.optEquals(field.get("validation"), "img")) {
                            String fileId = RegexUtil.optStrOrNull(data.get(fieldName));
                            if (RegexUtil.optIsPresentStr(fileId)) {
                                Map<String, Object> path = sncDataImportMapper.findFileInfoById(methodParam.getSchemaName(), Integer.valueOf(fileId));
                                if (RegexUtil.optIsPresentMap(path)) {
                                    String file_url = RegexUtil.optStrOrNull(path.get("file_url"));
                                    if (RegexUtil.optIsPresentStr(file_url) && new File(file_url).exists()) {
                                        try {
                                            courseRow.setHeightInPoints(70);
                                            Sheet sheet = courseRow.getSheet();
                                            sheet.setColumnWidth(i, 12 * 256);
                                            Drawing patriarch = sheet.createDrawingPatriarch();
                                            CreationHelper helper = sheet.getWorkbook().getCreationHelper();
                                            ClientAnchor anchor = helper.createClientAnchor();
                                            anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);
                                            anchor.setCol1(i);
                                            anchor.setRow1(rowCount - 1);
                                            InputStream is = new FileInputStream(file_url);
                                            byte[] bytes = IOUtils.toByteArray(is);
                                            int pictureIdx = sheet.getWorkbook().addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
                                            Picture pict = patriarch.createPicture(anchor, pictureIdx);
                                            pict.resize(1, 1);
                                            i++;
                                            continue;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                        } else if (RegexUtil.optIsPresentMap(selectMap) && selectMap.containsKey(fieldName)) {
                            cellVal = RegexUtil.optNotBlankStrOpt(data.get(fieldName)).map(e -> {
                                String dataBaseKey = (String) selectMap.get(fieldName);
                                String tmpKey = dataBaseKey + e;
                                if (!selectDataMap.containsKey(tmpKey)) {
                                    selectDataMap.put(tmpKey, selectOptionService.getSelectOptionTextByCode(methodParam, dataBaseKey, e));
                                }
                                return selectDataMap.get(tmpKey);
                            }).map(RegexUtil::optStrOrBlank).orElse("");
                        } else {
                            cellVal = RegexUtil.optStrOrBlank(data.get(fieldName));
                        }
                    }
                }
                courseRow.createCell(i).setCellValue(cellVal);
                i++;
            }
        });
    }

    /**
     * 字段数据处理
     *
     * @param list 数据集合
     */
    private void doChangeFieldInfo(List<Map<String, Object>> list) {
        RegexUtil.optNotNullList(list).ifPresent(l ->
                l.forEach(e -> {
                    if (e.containsKey("text")) {
                        Map<String, Object> map = DataChangeUtil.scdObjToMapOrNew(e.get("text"));
                        map.put("name", e.get("value"));
                        e.clear();
                        e.putAll(map);
                    }
                })
        );
    }

    /**
     * 导入日志文件导出
     *
     * @param methodParam 系统参数
     * @param logList     数据信息
     * @param importType  导入类型
     * @return 文件码
     */
    @Override
    public String doDownloadLogInfo(MethodParam methodParam, List<Map<String, Object>> logList, String importType) {
        RegexUtil.optListOrExp(logList, LangConstant.TEXT_K); // {0}信息不存在
        Workbook workbook = null;
        try {
            String header = "[{'alias':'fieldId'},{'alias':'fieldCode'},{'alias':'level'},{'alias':'message'}]";
            workbook = new SXSSFWorkbook();
            Sheet sheet = workbook.createSheet();
            sheet.setDefaultColumnWidth(30);
            int rowCount = 0;
            Map<String, Object> alias = new HashMap<>();
            String aliasStr = "[{'name':'fieldId'},{'name':'fieldCode'},{'name':'level'},{'name':'message'}]";
            Row headerRow = sheet.createRow(0);
            if (Constants.EXCEL_IMORT_TYPE_USER.equals(importType)) {
                header = "[{'alias':'user_id'},{'alias':'user_account'},{'alias':'level'},{'alias':'message'}]";
//                alias.put("fieldId", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.VMI_USER_ID)); // 用户ID
//                alias.put("fieldCode", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.USER_NAME)); // 用户名
            } else if (Constants.EXCEL_IMORT_TYPE_ASSET.equals(importType)) {
                header = "[{'alias':'asset_id'},{'alias':'asset_code'},{'alias':'level'},{'alias':'message'}]";
//                alias.put("fieldId", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.MSG_ASSET_ID)); // 设备ID
//                alias.put("fieldCode", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.EXCEL_ASSET_CODE)); // 设备代码
            } else if (Constants.EXCEL_IMORT_TYPE_BIS.equals(importType)) {
                header = "[{'alias':'sheet'},{'alias':'row_num'},{'alias':'level'},{'alias':'message'}]";
                alias.put("fieldId", "sheet");
//                alias.put("fieldCode", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.ROW_NM)); // 行号
            }
            this.setSheetCell(methodParam, headerRow, DataChangeUtil.scdStringToList(header), rowCount, null, null, null, null, null);
            rowCount = 1;
//            alias.put("level", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.MSG_LOG_TYPE)); // 日志类型
//            alias.put("message", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.DETAILED_INFO)); // 详细信息
            logList.add(0, alias);
            for (Map<String, Object> importLog : logList) {
                Row courseRow = sheet.createRow(rowCount++);
                // 解析通用字段，主，重要字段
                this.setSheetCell(methodParam, courseRow, DataChangeUtil.scdStringToList(aliasStr), rowCount, importLog, null, null, null, null);
            }
            return doCreateExcel(methodParam, workbook, importType);
        } catch (SenscloudException ex) {
            throw new SenscloudException(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
            try {
                if (null != workbook) {
                    workbook.close();
                }
            } catch (Exception wbExp) {
                wbExp.printStackTrace();
            }
        }
    }

    @Override
    public String doDownloadCustomerInfo(MethodParam methodParam, List<Map<String, Object>> customerList) {
        RegexUtil.optListOrExp(customerList, LangConstant.TEXT_K); // 信息不存在
        Workbook workbook = null;
        try {
            //boolean amFeeShow = PermissionUtil.getPermissionByInfo(methodParam, SensConstant.MENUS[0], SensConstant.PRM_TYPES[7]); // 设备费用显示权限
            String ss ="";
            workbook = new SXSSFWorkbook(1000);
            int rowCount = 0;
            Sheet sheet = workbook.createSheet(commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJ_G));
            sheet.setDefaultColumnWidth(30);
            Row headerRow = sheet.createRow(rowCount);
            String header = "[{'alias':'id'},{'alias':'title'},{'alias':'inner_code'},{'alias':'parent_id'}," +
                    "{'alias':'address'},{'alias':'unloading_group_id'},{'alias':'position_code'}," +
                    "{'alias':'pipeline_code'},{'alias':'sewage_destination'},{'alias':'main_products'}," +
                    "{'alias':'important_level_id'},{'alias':'customer_type_id'},{'alias':'unloading_time'}]";
            header = "[{'alias':'id'},{'alias':'客户名称'},{'alias':'客户编码'},{'alias':'所属企业'}," +
                    "{'alias':'地址'},{'alias':'并管排污分组'},{'alias':'所属片区'}," +
                    "{'alias':'管道编号'},{'alias':'污水去向'},{'alias':'主要产品'}," +
                    "{'alias':'重要等级'},{'alias':'客户类型'},{'alias':'排污时间'}]";
            String aliasStr = "[{'name':'id'},{'name':'title'},{'name':'inner_code'},{'name':'parent_id'}," +
                    "{'name':'address'},{'name':'unloading_group_id'},{'name':'position_code'}," +
                    "{'name':'pipeline_code'},{'name':'sewage_destination'},{'name':'main_products'}," +
                    "{'name':'important_level_id'},{'name':'customer_type_id'},{'name':'unloading_time'}]";
            this.setSheetCell(methodParam, headerRow, DataChangeUtil.scdStringToList(header), rowCount, null, null, null, null, null);
            rowCount = 1;
            Map<String,Object> customerLevels = new HashMap();
            customerLevels.put("data_type","customer_level");
            List<Map<String,Object>> customerLevelMap = cloudDataService.getCloudDataListByDataType(methodParam,customerLevels);
            Map<String,Object> customerTypes = new HashMap();
            customerTypes.put("data_type","customer_type");
            List<Map<String,Object>> customerTypeMap = cloudDataService.getCloudDataListByDataType(methodParam,customerTypes);
            Map<String,Object> unloadingGroups = new HashMap();
            unloadingGroups.put("data_type","unloading_group");
            List<Map<String,Object>> unloadingGroupMap = cloudDataService.getCloudDataListByDataType(methodParam,unloadingGroups);
            for (Map<String, Object> importLog : customerList) {
                Row courseRow = sheet.createRow(rowCount++);
                importLog.put("important_level_id",getCloudDataName(customerLevelMap,RegexUtil.optStrOrBlank(importLog.get("important_level_id"))));
                importLog.put("customer_type_id",getCloudDataName(customerTypeMap,RegexUtil.optStrOrBlank(importLog.get("customer_type_id"))));
                importLog.put("unloading_group_id",getCloudDataName(unloadingGroupMap,RegexUtil.optStrOrBlank(importLog.get("unloading_group_id"))));
                importLog.put("position_code",RegexUtil.optMapOrNew(assetPositionService.getPositionInfoByCode(methodParam,RegexUtil.optStrOrBlank(importLog.get("position_code")))).get("position_name"));
                FacilitiesModel facilitiesModel = new FacilitiesModel();
                if(RegexUtil.optIsPresentStr(importLog.get("parent_id"))) {
                    facilitiesModel.setId(Integer.valueOf(String.valueOf(importLog.get("parent_id"))));
                    importLog.put("parent_id", RegexUtil.optMapOrNew(facilitiesService.findById(methodParam, facilitiesModel)).get("title"));
                }
                // 解析通用字段，主，重要字段
                this.setSheetCell(methodParam, courseRow, DataChangeUtil.scdStringToList(aliasStr), rowCount, importLog, null, null, null, null);
            }
            //权限设置逻辑需要核实一下
            String authInfo = "{'customer_export':'" + SensConstant.MENUS[0] + "_" + SensConstant.PRM_TYPES[4] + "', '" + Constants.EXCEL_IMORT_TYPE_ASSET + "':'" + SensConstant.MENUS[0] + "_" + SensConstant.PRM_TYPES[3] + "'}";
            return doCreateExcel(methodParam,workbook,"customer_export",authInfo);

        }catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        }

    }

    /**
     * 生成文件
     *
     * @param methodParam 系统参数
     * @param workbook    文档
     * @param type        业务类别
     * @return 文件码
     */
    private String doCreateExcel(MethodParam methodParam, Workbook workbook, String type,String authInfo) {
        String fileToken = null;
        if (workbook.getNumberOfSheets() > 0) {
            //authInfo = "{'asset_export':'" + SensConstant.MENUS[0] + "_" + SensConstant.PRM_TYPES[4] + "', '" + Constants.EXCEL_IMORT_TYPE_ASSET + "':'" + SensConstant.MENUS[0] + "_" + SensConstant.PRM_TYPES[3] + "'}";
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_FILE);
            fileToken = sdf.format(new Date()) + SenscloudUtil.generateUUIDStr();
            String sep = System.getProperty("file.separator");
            SimpleDateFormat sdf2 = new SimpleDateFormat(Constants.DATE_FMT_FILE);
            String fileName = type + "_" + sdf2.format(new Date()) + ".xlsx";
            String filePath = file_upload_dir + sep + methodParam.getSchemaName() + sep + Constants.FILE_EXPORT_DIR + sep + type + sep + fileToken + ".xlsx";
            FileUtil.doExportExcel(filePath, workbook);
            Map<String, Object> fileTmpInfo = RegexUtil.optMapOrNew(methodParam.getFileTmpInfo());
            Map<String, String> fileInfo = new HashMap<>();
            fileInfo.put("fileName", fileName);
            fileInfo.put("filePath", filePath);
            fileInfo.put("authType", RegexUtil.optNotBlankStrOrExp(JSONObject.fromObject(authInfo).get(type), LangConstant.MSG_CA));
            fileTmpInfo.put(fileToken, fileInfo);
            MethodParam aaa = RegexUtil.optNotBlankStrOpt(methodParam.getToken()).map(t -> ScdSesRecordUtil.getScdSesDataByKey(t, "loginBaseUserInfo")).filter(io -> RegexUtil.optNotNull(io).isPresent()).map(io -> (MethodParam) io).orElseGet(MethodParam::new);
            aaa.setFileTmpInfo(fileTmpInfo);
        }
        return fileToken;
    }

    /**
     * 查找名称
     * @param map
     * @param code
     * @return
     */
    private String getCloudDataName(List<Map<String,Object>> map,String code){
        return RegexUtil.optStrOrBlank(RegexUtil.optNotNullListOrNew(map.stream()
                .filter(k->{
                    return code.equals(k.get("code"));
                }).collect(Collectors.toList()))
                .stream()
                .findFirst()
                .orElseGet(HashMap<String,Object>::new).get("name"));

    }

    /**
     * 生成文件
     *
     * @param methodParam 系统参数
     * @param workbook    文档
     * @param type        业务类别
     * @return 文件码
     */
    private String doCreateExcel(MethodParam methodParam, Workbook workbook, String type) {
        String fileToken = null;
        if (workbook.getNumberOfSheets() > 0) {
            String authInfo = "{'asset_export':'" + SensConstant.MENUS[0] + "_" + SensConstant.PRM_TYPES[4] + "', '"
                    + Constants.EXCEL_IMORT_TYPE_ASSET + "':'" + SensConstant.MENUS[0] + "_" + SensConstant.PRM_TYPES[3] + "', " +
                    "'task_temp_export':'" + SensConstant.MENUS[7] + "_" + SensConstant.PRM_TYPES[4] + "', " +
                    "'task_item_export':'" + SensConstant.MENUS[8] + "_" + SensConstant.PRM_TYPES[4] + "'}";
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_FILE);
            fileToken = sdf.format(new Date()) + SenscloudUtil.generateUUIDStr();
            String sep = System.getProperty("file.separator");
            SimpleDateFormat sdf2 = new SimpleDateFormat(Constants.DATE_FMT_FILE);
            String fileName = type + "_" + sdf2.format(new Date()) + ".xlsx";
            String filePath = file_upload_dir + sep + methodParam.getSchemaName() + sep + Constants.FILE_EXPORT_DIR + sep + type + sep + fileToken + ".xlsx";
            FileUtil.doExportExcel(filePath, workbook);
            Map<String, Object> fileTmpInfo = RegexUtil.optMapOrNew(methodParam.getFileTmpInfo());
            Map<String, String> fileInfo = new HashMap<>();
            fileInfo.put("fileName", fileName);
            fileInfo.put("filePath", filePath);
            fileInfo.put("authType", RegexUtil.optNotBlankStrOrExp(JSONObject.fromObject(authInfo).get(type), LangConstant.MSG_CA));
            fileTmpInfo.put(fileToken, fileInfo);
            MethodParam aaa = RegexUtil.optNotBlankStrOpt(methodParam.getToken()).map(t -> ScdSesRecordUtil.getScdSesDataByKey(t, "loginBaseUserInfo")).filter(io -> RegexUtil.optNotNull(io).isPresent()).map(io -> (MethodParam) io).orElseGet(MethodParam::new);
            aaa.setFileTmpInfo(fileTmpInfo);
        }
        return fileToken;
    }
}
