package com.gengyun.senscloud.service.cache.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.ehcache.ClusterEhCacheManagerFactoryBean;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.OtherCfgModel;
import com.gengyun.senscloud.model.WorkListModel;
import com.gengyun.senscloud.response.PositionResult;
import com.gengyun.senscloud.service.GroupService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 缓存工具接口
 */
@Service
public class CacheUtilServiceImpl implements CacheUtilService {
    private static final Logger logger = LoggerFactory.getLogger(CacheUtilServiceImpl.class);
    @Resource
    LoginLogMapper loginLogMapper;
    @Resource
    private CompanyMapper companyMapper;
    @Resource
    SerialServiceMapper serialMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    ClusterEhCacheManagerFactoryBean cacheBean;
    @Resource
    GroupService groupService;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    StatisticMapper statisticMapper;
    @Resource
    WorksMapper worksMapper;
    @Resource
    WorksheetDispatchMapper worksheetDispatchMapper;
    @Resource
    AssetMapper assetMapper;
    @Resource
    PagePermissionMapper pagePermissionMapper;
    @Resource
    PlanWorkMapper planWorkMapper;
    @Resource
    CompanyService companyService;

    /**
     * 获取用户常用语言包/系统默认语言包（明细）
     *
     * @param methodParam 系统参数
     * @param lang        国际化类型
     * @return 语言包
     */
    @Cacheable(value = "lang", key = "'sc_com_' + #methodParam.companyId + '_lang_' + #lang", unless = "#result?.size() == 0")
    @Retryable(value = Exception.class, maxAttempts = 3, backoff = @Backoff(delay = 1000L, multiplier = 0))
    @Override
    public Map<String, String> getDefaultLangDtl(MethodParam methodParam, String lang) {
        Map<String, String> result = new HashMap<>();
        RegexUtil.optMapOrExp(selectOptionService.getStaticSelectOption(methodParam, Constants.LANG_TYPE_KEY, lang), "noData");
        String resource = companyService.requestLang(methodParam.getCompanyId(), "companyId", "resourceById");
        //Map<String, JSONObject> rs = DataChangeUtil.scdStringToMap(companyMapper.resourceById(methodParam.getCompanyId()));
        Map<String, JSONObject> rs = DataChangeUtil.scdStringToMap(resource);
        rs.forEach((key, value) -> result.put(key, RegexUtil.optNotNull(value).map(v -> v.get(lang).toString()).orElse(key)));
        return result;
    }

    @Recover
    public Map<String, String> recover(Exception e) {
        Map<String, String> result = new HashMap<>();
        logger.error("语言包重试3次仍然失败，失败原因：{}", e.toString());
        return result;
    }

    /**
     * 新建验证码并存入缓存
     *
     * @param phone 电话号码
     * @return 验证码
     */
    @CachePut(value = "vf_code", key = "'wjmm_' + #phone")
    @Override
    public String newVfCode(String phone) {
        return String.valueOf(new Double((Math.random() * 9 + 1) * 1000).intValue());
    }

    /**
     * 获取验证码
     *
     * @param phone 电话号码
     * @return 验证码
     */
    @Cacheable(value = "vf_code", key = "'wjmm_' + #phone")
    @Override
    public String getVfCode(String phone) {
        return "";
    }

    /**
     * 查询业务编号生成规则配置信息
     *
     * @param schemaName   数据库
     * @param businessType 业务类型
     * @return 编号配置信息
     */
    @Cacheable(value = "serial_number_business", key = "'serial_business' + #schemaName + #businessType")
    @Override
    public Map<String, Object> getSerialByType(String schemaName, String businessType) {
        return serialMapper.findSerialByType(schemaName, businessType);
    }

    /**
     * 获取系统缓存信息（替代session）
     *
     * @param token token
     * @return 系统缓存信息
     */
    @Cacheable(value = "token", key = "'scd_ses_' + #token")
    @Override
    public Map<String, Object> cacheScdSesInfo(String token) {
        return new HashMap<>();
    }

    /**
     * 退出登录
     *
     * @param methodParam 系统参数
     */
    @CacheEvict(value = "token", key = "'scd_ses_' + #methodParam.getToken()")
    @Override
    public void applyLogOut(MethodParam methodParam) {
        loginLogMapper.updateLoginLogForLogout(methodParam.getSchemaName(), methodParam.getToken(), SenscloudUtil.getNowTime());
    }

    /**
     * 获取缓存列表
     *
     * @param ocModel 请求参数
     * @return 缓存列表
     */
    @Override
    public List<Map<String, Object>> getCacheListForPage(OtherCfgModel ocModel) {
        List<Map<String, Object>> list = new ArrayList<>();
        RegexUtil.optNotNull(cacheBean.getObject()).ifPresent(manager ->
                RegexUtil.optNotNullArrayOpt(manager.getCacheNames()).ifPresent(cacheNames -> {
                    DateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String keyWord = RegexUtil.optStrOrBlank(ocModel.getKeywordSearch());
                    for (String cacheName : cacheNames) {
                        Map<String, Object> m = new HashMap<>();
                        m.put("cache_name", cacheName); // 缓存名称
                        RegexUtil.optNotNull(manager.getCache(cacheName)).ifPresent(cache -> {
                            m.put("cache_size", cache.getSize()); // 缓存的对象数量
                            // 缓存总统计数据
                            m.put("memory_size", cache.getMemoryStoreSize()); // 缓存对象占用内存的数量
                            m.put("disk_size", cache.getDiskStoreSize()); // 缓存对象占用磁盘的数量
                            // 获取缓存统计对象
                            try {
                                RegexUtil.optNotNull(cache.getStatistics()).ifPresent(stat -> {
                                    m.put("cache_hits", stat.getCacheHits()); // 缓存读取的命中次数
                                    m.put("in_memory_hits", stat.getInMemoryHits()); // 内存中缓存读取的命中次数
                                    m.put("on_disk_hits", stat.getOnDiskHits()); // 磁盘中缓存读取的命中次数
                                    m.put("cache_misses", stat.getCacheMisses()); // 缓存读取的丢失次数
                                    m.put("eviction_count", stat.getEvictionCount()); // 缓存读取的已经被销毁的对象丢失次数
                                });
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String id = cache.getGuid(); // 缓存主键
                            m.put("id", id); // 缓存主键
                            m.put("pid", "0"); // 缓存所属主键
                            // 缓存元素集合-缓存元素统计数据
                            RegexUtil.optNotNullListObj(cache.getKeys()).ifPresent(keys -> {
                                String tmpKey;
                                for (Object key : keys) {
                                    Map<String, Object> dm = new HashMap<>(m);
                                    dm.put("pid", id); // 缓存所属主键
                                    tmpKey = RegexUtil.optStrOrBlank(key); // 键值
                                    if (!"".equals(keyWord) && !tmpKey.contains(keyWord)) {
                                        continue;
                                    }
                                    dm.put("key", tmpKey); // 缓存所属主键
                                    dm.put("id", tmpKey); // 缓存所属主键
                                    Element ele = cache.get(key);
                                    if (null != ele) {
                                        dm.put("content", ele.getObjectValue()); // 内容
                                        dm.put("count", ele.getHitCount()); // 命中次数
                                        dm.put("create_time", sf.format(ele.getCreationTime())); // 创建时间
                                        dm.put("last_access_time", sf.format(ele.getLastAccessTime())); // 最后访问时间
                                        dm.put("expiration_time", sf.format(ele.getExpirationTime())); // 过期时间
                                        dm.put("last_update_time", sf.format(ele.getLastUpdateTime())); // 最后更新时间
                                        dm.put("time_to_live", ele.getTimeToLive() + "s"); // 存活时间
                                        dm.put("time_to_idle", ele.getTimeToIdle() + "s"); // 空闲时间
                                    }
                                    list.add(dm);
                                    m.put("subCheck", true); // 缓存所属主键
                                }
                            });
                        });
                        if (!"".equals(keyWord) && !cacheName.contains(keyWord)) {
                            if (!RegexUtil.optBool(m.get("subCheck"))) {
                                continue;
                            }
                        }
                        list.add(m);
                    }
                })
        );
        return list;
    }

    /**
     * 根据缓存名称清除缓存
     *
     * @param name 缓存名称
     */
    @Override
    public void evictCacheByName(String name) {
        RegexUtil.optNotNull(cacheBean.getObject()).map(m -> m.getCache(name)).ifPresent(Cache::removeAll);
    }

    /**
     * 根据缓存名称和键值清除缓存
     *
     * @param name 缓存名称
     * @param key  缓存键值
     */
    @Override
    public void evictCacheByKey(String name, String key) {
        RegexUtil.optNotBlankStrOpt(name).ifPresent(n -> RegexUtil.optNotBlankStrOpt(key).ifPresent(k ->
                RegexUtil.optNotNull(cacheBean.getObject()).map(m -> m.getCache(n)).ifPresent(c -> c.remove(k))));
    }

    /**
     * 添加个人信息缓存
     *
     * @param methodParam 入参
     * @param userId      入参
     * @return 个人信息缓存
     */
    @Cacheable(value = "user_info", key = "'user_info_user_id' + #userId + 'schema_name' + #methodParam.schemaName")
    @Override
    public Map<String, Object> getUserInfo(MethodParam methodParam, String userId) {
        Map<String, Object> result = RegexUtil.optMapOrNew(userMapper.findById(methodParam.getSchemaName(), userId));
        List<Integer> positionIds = new ArrayList<>();
        List<PositionResult> positionResultList = groupService.getByUserId(methodParam, userId);
        if (RegexUtil.optIsPresentList(positionResultList)) {
            for (PositionResult positionResult : positionResultList) {
                positionIds.add(positionResult.getId());
            }
        }
        String nameString = userMapper.findGroupPositionNameString(methodParam.getSchemaName(), userId);
        result.put("positionIds", positionIds);
        result.put("groupPositionName", nameString);
        return result;
    }

    @CacheEvict(value = "user_info", key = "'user_info_user_id' + #userId + 'schema_name' + #schemaName")
    @Override
    public void deleteUserInfo(String userId, String schemaName) {
    }


    /**
     * 子工单
     *
     * @param methodParam 入参
     * @param sub_token   入参
     * @return 个人信息缓存
     */
    @Cacheable(value = "user_info", key = "'user_info_user_id' +#sub_token")
    @Override
    public String getSubToken(MethodParam methodParam, String sub_token) {
        return JSONObject.fromObject(methodParam).toString();
    }

    @CacheEvict(value = "user_info", key = "'user_info_user_id' +#sub_token")
    @Override
    public void deleteSubToken(String sub_token) {
    }

    /**
     * 静态语言包
     *
     * @param schemaName 系统参数
     * @param companyId  企业编号
     * @param userLang   国际化类型
     * @return 语言包
     */
    @Cacheable(value = "lang", key = "'public' + #companyId + '_lang_' + #userLang + '_selectKey_' + #selectKey + '_code_' + #code")
    @Override
    public Map<String, Object> findLangStaticOption(String schemaName, Long companyId, String userLang, String selectKey, String code) {
        Map<String, Object> langNeedStaticMap = selectOptionsMapper.findLangNeedStatic(schemaName, selectKey, code);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentMap(result)) {
            JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
            if (RegexUtil.optIsPresentMap(langNeedStaticMap)) {
                if (dd.containsKey(langNeedStaticMap.get("text"))) {
                    String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(langNeedStaticMap.get("text"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        langNeedStaticMap.put("text", value);
                    }
                }
            }
        }
        return langNeedStaticMap;
    }

    @Cacheable(value = "lang", key = "'public' + #companyId + '_lang_' + #userLang + '_selectKey_' + #selectKey")
    @Override
    public List<Map<String, Object>> findLangStaticDataByType(String schemaName, Long companyId, String userLang, String selectKey) {
        List<Map<String, Object>> findLangStaticData = selectOptionsMapper.findLangStaticDataByType(schemaName, selectKey);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(findLangStaticData)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                findLangStaticData.forEach(sd -> {
                    if (dd.containsKey(sd.get("text"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("text"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("text", value);
                        }
                    }
                });
            }
        }
        return findLangStaticData;
    }

    @Cacheable(value = "config", key = "'public' + '_id_' + #id + '_companyId_' + #companyId + '_lang_' + #userLang ")
    @Override
    public Map<String, Object> findInfoById(String schemaName, Integer id, String userLang, Long companyId) {
        Map<String, Object> findInfoMap = statisticMapper.findInfoById(schemaName, id);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findInfoById")));
        if (RegexUtil.optIsPresentMap(result)) {
            net.sf.json.JSONObject dd = net.sf.json.JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
            if (RegexUtil.optIsPresentMap(findInfoMap)) {
                if (dd.containsKey(findInfoMap.get("group_method_name"))) {
                    String value = RegexUtil.optStrOrNull(net.sf.json.JSONObject.fromObject(dd.get(findInfoMap.get("group_method_name"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        findInfoMap.put("group_method_name", value);
                    }
                }
            }
        }
        return findInfoMap;
    }

    @Cacheable(value = "lang", key = "'public' + #companyId + '_lang_' + #userLang + '_dataType_' + #dataType")
    @Override
    public List<Map<String, Object>> handleQueryCloudDataList(String schemaName, Long company_id, String userLang, String dataType) {
        List<Map<String, Object>> list = worksMapper.queryCloudDataList(schemaName, dataType);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.get("type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("type_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("type_name", value);
                        }
                    }
                });
            }
        }
        return list;
    }

    //    @Cacheable(value = "lang", key = "'public' + #companyId + '_lang_' + #userLang + '_userId_' + #userId + '_condition_' + #condition")
    @Override
    public List<WorkListModel> findWorksheetList(String schemaName, String userId, String userLang, Long companyId, String condition, String pagination) {
        List<WorkListModel> workDataList = worksheetDispatchMapper.findWorksheetListByLimit(schemaName, userId, condition, pagination);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(workDataList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                workDataList.forEach(sd -> {
                    if (dd.containsKey(sd.getPriority_level_name())) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.getPriority_level_name())).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.setPriority_level_name(value);
                        }
                    }
                });
            }
        }
        return workDataList;
    }

    @Cacheable(value = "lang", key = "'public' + #company_id + '_lang_' + #user_lang + '_idStr_' + #idStr")
    @Override
    public List<WorkListModel> findWorkListDetailBySwc(String schemaName, String[] idStr, String user_lang, Long company_id) {
        List<WorkListModel> list = worksheetDispatchMapper.findWorkListDetailBySwc(schemaName, idStr);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.getPriority_level_name())) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.getPriority_level_name())).get(user_lang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.setPriority_level_name(value);
                        }
                    }
                });
            }
        }
        return list;
    }

    @Cacheable(value = "lang", key = "'public' + #companyId + '_lang_' + #user_lang + '_id_' + #id")
    @Override
    public Map<String, Object> findAssetInfoForAssetMap(String schema_name, String id, String user_lang, Long companyId) {
        Map<String, Object> assetMap = assetMapper.findAssetInfoForAssetMap(schema_name, id);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentMap(result)) {
            JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
            if (RegexUtil.optIsPresentMap(assetMap)) {
                if (dd.containsKey(assetMap.get("result_type_name"))) {
                    String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(assetMap.get("result_type_name"))).get(user_lang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        assetMap.put("result_type_name", value);
                    }
                }
            }
        }
        return assetMap;
    }

    @Cacheable(value = "lang", key = "'public' + #companyId + '_langKey_' + #langKey")
    @Override
    public List<Map<String, Object>> findAssetOrg(String schemaName, Long companyId, String langKey) {
        List<Map<String, Object>> list = selectOptionsMapper.findAssetOrg(schemaName);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.get("org_type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("org_type_name"))).get(langKey));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("org_type_name", value);
                        }
                    }
                });
            }
        }
        return list;
    }

    @Cacheable(value = "lang", key = "'public' + '_findAllCloudDataType_' + #company_id + '_langKey_' + #langKey")
    @Override
    public List<Map<String, Object>> findAllCloudDataType(String schemaName, String langKey, Long company_id) {
        List<Map<String, Object>> allCloudDataList = selectOptionsMapper.findAllCloudDataType(schemaName);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(allCloudDataList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                allCloudDataList.forEach(sd -> {
                    if (dd.containsKey(sd.get("text"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("text"))).get(langKey));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("text", value);
                        }
                    }
                });
            }
        }
        return allCloudDataList;
    }

    @Cacheable(value = "lang", key = "'public' + #companyId + '_langKey_' + #langKey + '_asset_id_' + #asset_id + '_keyword_' + #keyword + '_org_type_' + #org_type")
    @Override
    public List<Map<String, Object>> findAllFacilityWithType(String schemaName, Long companyId, String langKey, String asset_id, String keyword, String org_type) {
        List<Map<String, Object>> FacilityList = pagePermissionMapper.findAllFacilityWithType(schemaName, asset_id, keyword, org_type);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(FacilityList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                FacilityList.forEach(sd -> {
                    if (dd.containsKey(sd.get("org_type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("org_type_name"))).get(langKey));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("org_type_name", value);
                        }
                    }
                });
            }
        }
        return FacilityList;
    }

    @Cacheable(value = "lang", key = "'public_findInnerFacilityWithType' + #companyId + '_userLang_' + #userLang")
    @Override
    public List<Map<String, Object>> findInnerFacilityWithType(String schemaName, Long companyId, String userLang) {
        List<Map<String, Object>> innerFacilityList = pagePermissionMapper.findInnerFacilityWithType(schemaName);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(innerFacilityList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                innerFacilityList.forEach(sd -> {
                    if (dd.containsKey(sd.get("org_type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("org_type_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("org_type_name", value);
                        }
                    }
                });
            }
        }
        return innerFacilityList;
    }

    @Cacheable(value = "lang", key = "'public' + #company_id + '_langKey_' + #langKey + '_data_type_' + #data_type + '_parent_code_' + #parent_code")
    @Override
    public List<Map<String, Object>> findSpecialPropertyKey(String schemaName, String data_type, String parent_code, String langKey, Long company_id) {
        List<Map<String, Object>> specialPropertyKeyList = selectOptionsMapper.findSpecialPropertyKey(schemaName, data_type, parent_code);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(specialPropertyKeyList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                specialPropertyKeyList.forEach(sd -> {
                    if (dd.containsKey(sd.get("text"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("text"))).get(langKey));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("text", value);
                        }
                    }
                });
            }
        }
        return specialPropertyKeyList;
    }

    @Cacheable(value = "lang", key = "'public' + #company_id + '_user_lang_' + #user_lang + '_plan_code_' + #plan_code")
    @Override
    public List<Map<String, Object>> findAssistByPlanCode(String schemaName, String plan_code, String user_lang, Long company_id) {
        List<Map<String, Object>> assistList = planWorkMapper.findAssistByPlanCode(schemaName, plan_code);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(assistList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                if (RegexUtil.optIsPresentMap(DataChangeUtil.scdObjToMap(result.get("resource")))) {
                    JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                    assistList.forEach(sd -> {
                        if (dd.containsKey(sd.get("duty_type_name"))) {
                            String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("duty_type_name"))).get(user_lang));
                            if (RegexUtil.optIsPresentStr(value)) {
                                sd.put("duty_type_name", value);
                            }
                        }
                    });
                }
            }

            if (RegexUtil.optIsPresentMap(result)) {
                if (RegexUtil.optIsPresentMap(DataChangeUtil.scdObjToMap(result.get("resource")))) {
                    JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                    assistList.forEach(sd -> {
                        if (dd.containsKey(sd.get("self_or_out_name"))) {
                            String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("self_or_out_name"))).get(user_lang));
                            if (RegexUtil.optIsPresentStr(value)) {
                                sd.put("self_or_out_name", value);
                            }
                        }
                    });
                }
            }
        }
        return assistList;
    }
}
