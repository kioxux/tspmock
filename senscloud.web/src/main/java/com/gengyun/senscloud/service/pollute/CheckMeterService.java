package com.gengyun.senscloud.service.pollute;

import com.gengyun.senscloud.model.CheckMeterModel;
import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

public interface CheckMeterService {
    Map<String, Object> findCheckMeterList(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String,Object> findCheckMeterStatistics(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object>  findAssetCheckMeterList(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String,Object> findCheckMeterDetailInfo(MethodParam methodParam, Map<String, Object> paramMap);

    void newCheckMeter(MethodParam methodParam, CheckMeterModel model);

    Map<String,Object> findLastCheckMeter(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> findAfterCheckMeter(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> findCheckMeterCalendar(MethodParam methodParam, Map<String, Object> paramMap);

    void modifyCheckMeter(MethodParam methodParam, CheckMeterModel model);

    /**
     * 根据选中主键下载抄表记录信息
     *
     * @param methodParam 系统参数
     * @param model     请求参数
     */
    String getExportCheckMeter(MethodParam methodParam, CheckMeterModel model);

    Map<String, Object> findCheckMeterMonthList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findCheckMeterInfo(MethodParam methodParam, Map<String, Object> paramMap);

    void autoCheckMeterSeting(MethodParam methodParam, Map<String, Object> paramMap);

    void asyncCronJobToGenerateCheckMeter(String schemaName);

    void asyncCronJobToGenerateSewageRecords(String schemaName);
}