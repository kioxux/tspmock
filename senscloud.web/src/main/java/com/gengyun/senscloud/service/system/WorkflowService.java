package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkflowSearchParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.response.WorkflowStartResult;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * 工作流
 */
public interface WorkflowService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getWorkflowPermission(MethodParam methodParam);

    /**
     * 部署工作流
     *
     * @param methodParam 系统参数
     * @param file        部署文件
     */
    void newWorkflow(MethodParam methodParam, MultipartFile file);

    /**
     * 获取工作流部署对象列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 列表数据
     */
    Map<String, Object> getWorkflowDeploymentList(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除工作流部署对象
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    void cutWorkflowDeployment(MethodParam methodParam, String id);

    /**
     * 获取工作流定义列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 列表数据
     */
    Map<String, Object> getWorkflowDefinitionList(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取工作流定义流程图
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    void getWorkflowProcessDiagram(MethodParam methodParam, String id);

    /**
     * 获取工作流实例列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 列表数据
     */
    Map<String, Object> getWorkflowInstanceList(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除工作流实例
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    void cutWorkflowInstance(MethodParam methodParam, String id);

    /**
     * 删除流程名称之外的工作流实例
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     */
    void cutWorkflowInstanceFilterFlowName(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 根据子工单编号删除工作流实例
     *
     * @param methodParam 系统参数
     * @param subWorkCode 子工单编号
     * @param reason      理由
     */
    void cutWorkflowInstanceBySubWorkCode(MethodParam methodParam, String subWorkCode, String reason);

    /**
     * 获取工作流实例流程图
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    void getWorkflowInstanceDiagram(MethodParam methodParam, String id);

    /**
     * 获取工作流节点流程图
     *
     * @param methodParam 系统参数
     * @param pageParam   页面参数
     * @param id          数据主键
     */
    void getWorkflowNodeDiagram(MethodParam methodParam, WorkflowSearchParam pageParam, String id);

    /**
     * 工作流流程、节点信息列表获取
     *
     * @param methodParam 系统参数
     * @return 列表数据
     */
    List<Map<String, Object>> getWorkflowInstDspList(MethodParam methodParam);

    /**
     * 根据流程号获取节点下拉框
     *
     * @param methodParam 系统参数
     * @param flowCode    流程编号
     * @return 列表数据
     */
    List<Map<String, Object>> getWorkflowNodeListById(MethodParam methodParam, String flowCode);

    /**
     * 启动流程-带表单参数
     *
     * @param methodParam
     * @param processDefinitionId
     * @param starter
     * @param formProperties
     * @return
     */
    WorkflowStartResult startWithForm(MethodParam methodParam, String processDefinitionId, String starter, Map<String, String> formProperties);

    /**
     * 分配任务（根据流程实例主键taskId）
     *
     * @param methodParam
     * @param taskId
     * @param paramMap
     * @return
     */
    ResponseModel assign(MethodParam methodParam, String taskId, Map<String, String> paramMap);

    /**
     * 分配任务（根据工单编号subworkcode）
     *
     * @param methodParam
     * @param subWorkCode
     * @param paramMap
     * @return
     */
    ResponseModel assignWithSWC(MethodParam methodParam, String subWorkCode, String userId, Map<String, String> paramMap);

    /**
     * 分配所有子任务
     *
     * @param methodParam
     * @param subWorkCodes
     * @param paramMap
     * @return
     */
    ResponseModel parentAssign(MethodParam methodParam, List<String> subWorkCodes, String userId, Map<String, String> paramMap);

    /**
     * 完成任务
     *
     * @param methodParam
     * @param taskId
     * @param paramMap
     * @return
     */
    ResponseModel complete(MethodParam methodParam, String taskId, Map<String, String> paramMap);

    /**
     * 完成任务
     *
     * @param methodParam
     * @param taskId
     * @param paramMap
     * @return
     */
    ResponseModel subComplete(MethodParam methodParam, String taskId, Map<String, String> paramMap);

    /**
     * 获取所有流程变量
     *
     * @param methodParam
     * @param taskId
     * @return
     */
    Map<String, Object> getTaskVariables(MethodParam methodParam, String taskId);

    /**
     * 获取所有流程变量
     *
     * @param
     * @param
     * @return
     */
    Map<String, Object> getTaskVariables(String schema, String taskId);

    /**
     * 获取流程变量
     *
     * @param methodParam
     * @param taskId
     * @param variableName
     * @return
     */
    Object getTaskVariableByName(MethodParam methodParam, String taskId, String variableName);

    /**
     * 查询待办列表
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 列表
     */
    Map<String, Object> searchTaskList(MethodParam methodParam, WorkflowSearchParam wfParam);

    /**
     * 根据工单编号获取代码流程
     *
     * @param methodParam
     * @param subWorkCodes
     * @return
     */
    List<Map<String, Object>> getTasksBySubWorkCodes(MethodParam methodParam, List<String> subWorkCodes);


    /**
     * 获取按天分组工作流任务列表
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 列表
     */
    List<Map<String, Object>> getFlowTaskGroupList(MethodParam methodParam, WorkflowSearchParam wfParam);

    /**
     * 获取待办流程列表
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 列表
     */
    List<Map<String, Object>> getTasks(MethodParam methodParam, WorkflowSearchParam wfParam);

    /**
     * 获取待办流程数量
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 数量
     */
    int getTasksCount(MethodParam methodParam, WorkflowSearchParam wfParam);

    /**
     * 根据业务单号获取节点主键（当前节点/下一步节点）
     *
     * @param methodParam 系统参数
     * @param paramMap    页面参数
     * @return 节点主键
     */
    String getNodeIdBySubWorkCode(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取taskKey
     *
     * @param methodParam 系统参数
     * @param subWorkCode 页面参数
     * @return taskKey
     */
    String getTaskKeyBySubWorkCode(MethodParam methodParam, String subWorkCode);

    /**
     * 获取taskId
     *
     * @param methodParam 系统参数
     * @param subWorkCode 页面参数
     * @return taskKey
     */
    String getTaskIdBySubWorkCode(MethodParam methodParam, String subWorkCode);

    /**
     * 判断是否下一步为结束节点  如果是返回结束节点id  不是为null
     *
     * @param taskEntity 页面参数
     * @return nodeId
     */
    String isEndEvent(TaskEntity taskEntity);

    /**
     * 获取待办任务权限列表
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getTaskListPermission(MethodParam methodParam);

    void clearWorkflow(MethodParam methodParam, Map<String, Object> paramMap);

    ModelAndView exportWorkFlow(MethodParam methodParam, Map<String, Object> paramMap);
}
