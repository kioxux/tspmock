package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.AssetAnalysByMapMapper;
import com.gengyun.senscloud.mapper.AssetMapper;
import com.gengyun.senscloud.mapper.AssetPositionMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.model.WorkListModel;
import com.gengyun.senscloud.response.AssetPositionDataResult;
import com.gengyun.senscloud.response.AssetPositionResult;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.asset.AssetMapService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import io.swagger.annotations.Api;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "设备地图")
@Service
public class AssetMapServiceImpl implements AssetMapService {

    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    AssetPositionMapper assetPositionMapper;
    @Resource
    AssetAnalysByMapMapper assetAnalysByMapMapper;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    AssetMapper assetMapper;
    @Resource
    CacheUtilService cacheUtilService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return
     */
    @Override
    public Map<String, Map<String, Boolean>> getAssetMapPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[18]);
    }

    /**
     * 获得所有的位置
     *
     * @param methodParam 入参
     * @param param       入参
     * @return
     */
    @Override
    public List<Map<String, Object>> getAllAssetMapPosition(MethodParam methodParam, SearchParam param) {
        return assetPositionMapper.findTheBiggestAssetPosition(methodParam.getSchemaName(), methodParam.getUserId(), RegexUtil.optStrOrBlank(param.getKeywordSearch()), RegexUtil.optStrOrBlank(param.getShowType()));
    }

    /**
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getAssetAndAssetPositionForMap(MethodParam methodParam, SearchParam param) {
//        String sysValue = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "highest_asset_position_show_type");
        String type = RegexUtil.optStrOrBlank(param.getShowType());//显示类型：0、设备 1、任务
        Integer assetType = RegexUtil.optIntegerOrNull(param.getAssetType(), LangConstant.TAB_ASSET_B);//设备类型：0、全部设备  1、物联设备
        Integer workType = RegexUtil.optIntegerOrNull(param.getWorkType(), LangConstant.TITLE_CATEGORY_AN);//任务类型：0、全部任务 1、维修 2、计划
        String schema_name = methodParam.getSchemaName();
        Map<String, Object> assetPosition = this.getAssetPositionInfoByPositionCode(schema_name, param.getPositionCode());
        RegexUtil.optMapOrExpNullInfo(assetPosition, LangConstant.TITLE_ASSET_G); // 设备位置不存在
        String db_position_code = assetPosition.get("position_code").toString();
        List<Map<String, Object>> list = assetAnalysByMapMapper.findAssetAndPositionByPositionCode(schema_name, db_position_code, type, assetType, workType, RegexUtil.optStrOrBlank(param.getKeywordSearch()));
        if (RegexUtil.optIsPresentMap(assetPosition)) {
            assetPosition.put("list", list);
        }
        return assetPosition;
    }

    /**
     * 获得选择位置或设备的信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getAssetMapDetail(MethodParam methodParam, SearchParam param) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam); // 分页参数
        String id = RegexUtil.optStrOrExpNotNull(param.getId(), LangConstant.TITLE_ACJ);
        String type = RegexUtil.optStrOrExpNotNull(param.getType(), LangConstant.TITLE_CATEGORY_AH);
        Map<String, Object> data = new HashMap<String, Object>();
        StringBuffer whereString = new StringBuffer(" and wd.status > " + StatusConstant.DRAFT + " and wd.status < " + StatusConstant.COMPLETED + " ");
        String condition = "";
        //查询设备或位置详情
        if (Constants.ASSET_MAP_TYPE_ASSET.equals(type)) {
            Map<String, Object> asset = cacheUtilService.findAssetInfoForAssetMap(methodParam.getSchemaName(), id, methodParam.getUserLang(), methodParam.getCompanyId());
            if (!RegexUtil.optIsPresentMap(asset)) {
                data.put("asset", new HashMap<String, Object>());
                data.put("works", new ArrayList<>());
                return data;
            }
            data.put("asset", asset);
            whereString.append(" and w.relation_type = 2").append(" and wd.relation_id = '").append(id).append("' ");
        } else if (Constants.ASSET_MAP_TYPE_POSITION.equals(type)) {
            Map<String, Object> position = assetPositionMapper.findPositionInfoForAssetMap(methodParam.getSchemaName(), id);
            if (!RegexUtil.optIsPresentMap(position)) {
                data.put("position", new HashMap<String, Object>());
                data.put("works", new ArrayList<>());
                data.put("childPositions", new ArrayList<>());
                return data;
            }
            data.put("asset_list", assetMapper.findAssetListByPositionCode(methodParam.getSchemaName(), id));
            data.put("position", position);
            whereString.append(" and w.position_code ='").append(id).append("' ");
            List<Map<String, Object>> list = assetPositionMapper.findChildAssetPositionByPositionCodeLimit(methodParam.getSchemaName(), id, pagination);
            data.put("childPositions", RegexUtil.optIsPresentList(list) ? list : new ArrayList<>());
        }
        condition = whereString.toString();
        List<WorkListModel> workListModels = cacheUtilService.findWorksheetList(methodParam.getSchemaName(), methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), condition, pagination);
        if (RegexUtil.optIsPresentList(workListModels)) {
//            Map<String, List<WorkListModel>> mapList = workListModels.stream().collect(Collectors.groupingBy(t -> t.getBusiness_name()));
//            data.put("works", mapList);
            data.put("works", workListModels);
        } else {
            data.put("works", new ArrayList<>());
//            data.put("works", new HashMap<String, Object>());
        }
        return data;
    }

    @Override
    public List<AssetPositionDataResult> getAssetPositionAndAssetForMap(MethodParam methodParam, SearchParam param) {
        String type = param.getShowType();//显示类型：0、位置 1、设备
        String assetType = param.getAssetType();//设备类型：0、故障 1、计划保养 2、物联 3、物联离线
        String positionCode = param.getPositionCode();
        String schema_name = methodParam.getSchemaName();
        List<Map<String, Object>> userAssetPositionList = selectOptionService.getSelectOptionList(methodParam, "asset_position_permission", null);

        List<AssetPositionDataResult> dataResult = new ArrayList<>();//位置，以及其子位置、设备统计、设备以及设备待办集
        List<AssetPositionResult> lists = null;
        //选择的位置，按位置查询工单
//        if (RegexUtil.optIsPresentStr(positionCode)) {
//            lists = this.getFacilitiesCustomResultById(schema_name, type, assetType, positionCode);
//        } else {
//            //找到根节点的所有位置，并为这些位置赋值：子位置、包含的设备总数、待办总数等
//            lists = this.getFacilitiesCustomResultById(schema_name, type, assetType, null);
//        }
        //获取用户所在的位置，可能多个,看当前位置是否为用户所在的位置
        if (RegexUtil.optIsPresentList(userAssetPositionList) && RegexUtil.optIsPresentList(lists)) {
            for (Map<String, Object> map : userAssetPositionList) {
                for (AssetPositionResult item : lists) {
                    if (map.get("value").toString().equals(item.getPosition_code())) {
                        AssetPositionDataResult singleRsult = new AssetPositionDataResult();
                        //加载位置
                        singleRsult.setAssetPosition(item);
                        dataResult.add(singleRsult);
                        break;
                    }
                }
            }
//            for (String key : userFacilityList.keySet()) {
//                for (FacilitiesResult item : lists) {
//                    if (Integer.valueOf(key).intValue() == item.getId().intValue()) {
//                        FacilitiesDataResult singleRsult = new FacilitiesDataResult();
//                        //加载位置
//                        singleRsult.setFacilityResult(item);
//                        dataResult.add(singleRsult);
//                        break;
//                    }
//                }
//            }
        }
        return dataResult;
    }

    private Map<String, Object> getAssetPositionInfoByPositionCode(String schema_name, String positionCode) {
        return assetAnalysByMapMapper.findAssetPositionInfoByPositionCode(schema_name, positionCode);
    }

    /**
     * @param schema_name
     * @param type         显示类型：0、位置 1、设备
     * @param assetType    设备类型：0、故障 1、计划保养 2、物联 3、物联离线
     * @param positionCode 设备位置
     * @return
     */
    public List<AssetPositionResult> getFacilitiesCustomResultById(String schema_name, String type, String assetType, String workType, String keywordSearch, String positionCode, String mapType) {
        String filed = "";
        String parent_code = "";
        if (mapType.equals(Constants.ASSET_MAP_SHOW_TYPE_PLAN)) {
            //平面图，显示次一级的位置点
            parent_code = " parent_code = (SELECT position_code from " + schema_name + "._sc_asset_position WHERE parent_code = '' ORDER BY create_time asc limit 1)";
        } else {
            //地图，显示最父级位置点
            parent_code = " parent_code = '' ";
        }
        if ("0".equals(type) || ("1".equals(type) && ("0".equals(assetType) || "1".equals(assetType)))) {
            //地图显示位置、设备-保障时，查询待维修设备数量
            //地图显示设备-计划保养时，查询待保养设备数量
            int businessType = Constants.BUSINESS_TYPE_REPAIR_TYPE;
            if ("1".equals(type) && "1".equals(assetType)) {
                businessType = Constants.BUSINESS_TYPE_MAINTAIN_TYPE;
            }

            filed = ",(SELECT COUNT(DISTINCT a.id ) FROM " + schema_name + "._sc_asset a  " +
                    "join (with RECURSIVE re (position_code,parent_code) as " +
                    "(select position_code,parent_code from " + schema_name + "._sc_asset_position  where position_code = ap.position_code " +
                    "union all " +
                    "select a2.position_code,a2.parent_code from " + schema_name + "._sc_asset_position a2,re e where e.position_code=a2.parent_code " +
                    ") select position_code,parent_code from re ) aa ON a.position_code = aa.position_code " +
                    "INNER JOIN " + schema_name + "._sc_works_detail wd ON wd.relation_id = a.id " +
                    "INNER JOIN " + schema_name + "._sc_work_type wt ON wt.id = wd.work_type_id " +
                    "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.COMPLETED +
                    " AND wt.business_type_id = " + businessType + " AND wd.relation_type = 2 ) as repairMaintainAssetCount ";
        } else if ("1".equals(type) && ("-1".equals(assetType) || "2".equals(assetType) || "3".equals(assetType))) {
            //地图显示设备-物联时，查询物联设备数量
            //地图显示设备-离线物联时，查询物联设备在线、离线数量
            String tmp = "SELECT COUNT(DISTINCT a.id) FROM " + schema_name + "._sc_asset a " +
                    "join (with RECURSIVE re (position_code,parent_code) as " +
                    "(select position_code,parent_code from " + schema_name + "._sc_asset_position  where position_code = ap.position_code " +
                    "union all " +
                    "select a2.position_code,a2.parent_code from " + schema_name + "._sc_asset_position a2,re e where e.position_code=a2.parent_code " +
                    ") select position_code,parent_code from re ) aa ON a.position_code = aa.position_code " +
                    "LEFT JOIN " + schema_name + "._sc_asset_iot_setting i ON a.id = i.asset_id ";
            if ("-1".equals(assetType)) {//全部设备数量
                filed = ",(" + tmp + " ) as assetCount ";
            } else if ("2".equals(assetType)) {//物联设备数量
                filed = ",(" + tmp + " WHERE i.iot_status in (" + Constants.ASSET_IOT_SETTING_STATUS_ONLINE + "," + Constants.ASSET_IOT_SETTING_STATUS_OFFLINE + ") ) as iotAssetCount ";
            } else if ("3".equals(assetType)) {//在线、离线数量
                filed = ",(" + tmp + " WHERE i.iot_status = " + Constants.ASSET_IOT_SETTING_STATUS_ONLINE + ") as onlineIotAssetCount " +
                        ",(" + tmp + " WHERE i.iot_status = " + Constants.ASSET_IOT_SETTING_STATUS_OFFLINE + ") as offlineIotAssetCount ";
            }
        } else {
            return null;
        }
        if (RegexUtil.optIsBlankStr(positionCode)) { //查询全部组织
            return assetAnalysByMapMapper.findAllParentAssetPositionCustomResultList(schema_name, filed);
        }
        if (StatusConstant.SHOW_TYPE_POSITION.equals(type)) {//如果是位置，且指定了某个位置，则需要查询选中位置以及它的子位置
            return assetAnalysByMapMapper.findAssetPositionAndChildrenCustomResultByPositionCode(schema_name, filed, positionCode);
        } else {
            return assetAnalysByMapMapper.findAssetPositionCustomResultByPositionCode(schema_name, filed, positionCode);
        }
    }

//    /**
//     * @param schema_name
//     * @param type 显示类型：0、位置 1、设备
//     * @param assetType 设备类型：0、故障 1、计划保养 2、物联 3、物联离线
//     * @param positionCode 设备位置
//     * @return
//     */
//    public List<AssetPositionResult> getFacilitiesCustomResultById(String schema_name, String type, String assetType, String positionCode) {
//        String filed = "";
//        if("0".equals(type) || ("1".equals(type) && ("0".equals(assetType) || "1".equals(assetType) ))) {
//            //地图显示位置、设备-保障时，查询待维修设备数量
//            //地图显示设备-计划保养时，查询待保养设备数量
//            int businessType = Constants.BUSINESS_TYPE_REPAIR_TYPE;
//            if("1".equals(type) && "1".equals(assetType))
//                businessType = Constants.BUSINESS_TYPE_MAINTAIN_TYPE;
//
//            filed = ",(SELECT COUNT(DISTINCT a.id ) FROM "+schema_name+"._sc_asset a  " +
//                    "join (with RECURSIVE re (position_code,parent_code) as " +
//                    "(select position_code,parent_code from " + schema_name + "._sc_asset_position  where position_code = ap.position_code " +
//                    "union all " +
//                    "select a2.position_code,a2.parent_code from " + schema_name + "._sc_asset_position a2,re e where e.position_code=a2.parent_code " +
//                    ") select position_code,parent_code from re ) aa ON a.position_code = aa.position_code " +
//                    "INNER JOIN "+schema_name+"._sc_works_detail wd ON wd.relation_id = a.id " +
////                    "INNER JOIN "+schema_name+"._sc_asset_organization ao ON ao.asset_id = a._id " +
//                    "INNER JOIN "+schema_name+"._sc_work_type wt ON wt.id = wd.work_type_id " +
//                    "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.COMPLETED +
//                    " AND wt.business_type_id = "+ businessType +" AND wd.relation_type = 2 ) as repairMaintainAssetCount ";
//        }else if("1".equals(type) && ("-1".equals(assetType) || "2".equals(assetType) || "3".equals(assetType)) ) {
//            //地图显示设备-物联时，查询物联设备数量
//            //地图显示设备-离线物联时，查询物联设备在线、离线数量
//            String tmp = "SELECT COUNT(DISTINCT a.id) FROM "+schema_name+"._sc_asset a " +
//                    "join (with RECURSIVE re (position_code,parent_code) as " +
//                    "(select position_code,parent_code from " + schema_name + "._sc_asset_position  where position_code = ap.position_code " +
//                    "union all " +
//                    "select a2.position_code,a2.parent_code from " + schema_name + "._sc_asset_position a2,re e where e.position_code=a2.parent_code " +
//                    ") select position_code,parent_code from re ) aa ON a.position_code = aa.position_code " +
//                    "LEFT JOIN "+schema_name+"._sc_asset_iot_setting i ON a.id = i.asset_id ";
//            if("-1".equals(assetType)){//全部设备数量
//                filed = ",(" + tmp + " ) as assetCount ";
//            }else if("2".equals(assetType)) {//物联设备数量
//                filed = ",(" + tmp + " WHERE i.iot_status in ("+Constants.ASSET_IOT_SETTING_STATUS_ONLINE+","+Constants.ASSET_IOT_SETTING_STATUS_OFFLINE+") ) as iotAssetCount ";
//            }else if("3".equals(assetType)){//在线、离线数量
//                filed = ",(" + tmp + " WHERE i.iot_status = "+Constants.ASSET_IOT_SETTING_STATUS_ONLINE+") as onlineIotAssetCount " +
//                        ",(" + tmp + " WHERE i.iot_status = "+Constants.ASSET_IOT_SETTING_STATUS_OFFLINE+") as offlineIotAssetCount ";
//            }
//        }else{
//            return null;
//        }
//        if(RegexUtil.optIsBlankStr(positionCode)){ //查询全部组织
//            return assetAnalysByMapMapper.findAllParentAssetPositionCustomResultList(schema_name, filed);
//        }
//        if(StatusConstant.SHOW_TYPE_POSITION.equals(type)){//如果是位置，且指定了某个位置，则需要查询选中位置以及它的子位置
//            return assetAnalysByMapMapper.findAssetPositionAndChildrenCustomResultByPositionCode(schema_name, filed, positionCode);
//        }else{
//            return assetAnalysByMapMapper.findAssetPositionCustomResultByPositionCode(schema_name, filed, positionCode);
//        }
//    }
}
