package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.DyToPageTypeService;
import org.springframework.stereotype.Service;

/**
 * 字段类型处理接口（动态进页面前）
 * User: sps
 * Date: 2020/06/11
 * Time: 上午09:20
 */
@Service
public class DyToPageTypeServiceImpl implements DyToPageTypeService {
//    private static final Logger logger = LoggerFactory.getLogger(DyToPageTypeServiceImpl.class);
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//    @Autowired
//    private FilesService filesService;
//
//    /**
//     * 字段类型处理接口
//     *
//     * @param result
//     * @param data
//     * @param strInfo
//     * @param allMap
//     * @return
//     */
//    public boolean doSetResultByType(JSONObject result, JSONObject data, Map<String, String> strInfo, Map<String, Object> allMap) {
//        if (TypeUtil.doSetResultByType(result, data, strInfo, allMap)) {
//            return true;
//        }
//        String fieldFormCode = strInfo.get("fieldFormCode");
//        String schemaName = strInfo.get("schemaName");
//        if ("6".equals(strInfo.get("fieldViewType"))) {
//            try {
//                if ("edit".equals(data.getString("fieldRight"))) {
//                    data.put("imgType", RegexUtil.optStrOrVal(data.get("imgType"), "['camera']"));
//                    data.put("fileType", RegexUtil.optStrOrVal(data.get("fileType"), "image"));
//                }
//                String tmpVal = RegexUtil.optStrAndValOrBlank(allMap.get("pageDtlValue"), allMap.get("value"));
//                if (RegexUtil.isNull(tmpVal)) {
//                    data.put("phoneViewType", "32");
//                    if ("readonly".equals(data.getString("fieldRight"))) {
//                        data.put("fieldTextValue", strInfo.get("strNone")); // 数据回显
//                    }
//                } else {
//                    data.put("fieldValue", tmpVal);
//                    List<FilesData> fileList = filesService.FindAllFiles(schemaName, tmpVal);
//                    if (fileList != null && !fileList.isEmpty()) {
//                        List<Map<String, Object>> fileDtlList = new ArrayList<>();
//                        for (FilesData file : fileList) {
//                            String fileId = file.getId().toString();
//                            Map<String, Object> fileInfo = new HashMap<>();
//                            fileInfo.put("fileId", fileId);
//                            fileId = strInfo.get("deviceDomain").concat("service/get_file?id=").concat(fileId).concat("&companyId=").concat(allMap.get("companyId").toString());
//                            fileInfo.put("url", fileId);
//                            fileInfo.put("name", file.getFileName());
//                            fileInfo.put("isImage", 1 == file.getFileType() ? true : false);
//                            fileDtlList.add(fileInfo);
//                        }
//                        data.put("fileDtlList", fileDtlList);
//                    }
//                }
//            } catch (Exception imgExp) {
//                data.put("fieldTextValue", strInfo.get("strNone")); // 数据回显
//            }
//        } else if ("31".equals(strInfo.get("fieldViewType"))) {
//            if ("edit".equals(data.getString("fieldRight"))) {
//                data.put("rgtIcon", "scan");
//            }
//            data.put("phoneViewType", "32");
//        } else if (",2,3,30,".contains("," + strInfo.get("fieldViewType") + ",") || (",2,3,30,".contains("," + strInfo.get("fieldBaseViewType") + ","))) {
//            if ("edit".equals(data.getString("fieldRight")) && !"32".equals(data.getString("phoneShowType"))) {
//                String selectKey = data.get("fieldDataBase").toString();
//                List<String> selectKeyTmp = null;
//                Map<String, List<String>> selectKeyList = (Map<String, List<String>>) allMap.get("selectKeyList");
//                if (selectKeyList.containsKey(selectKey)) {
//                    selectKeyTmp = selectKeyList.get(selectKey);
//                } else {
//                    selectKeyTmp = new ArrayList<String>();
//                }
//                selectKeyTmp.add(fieldFormCode);
//                selectKeyList.put(selectKey, selectKeyTmp);
//                data.put("subContent", "select");
//                data.put("iptCss", "is-hidden");
//            } else if ("readonly".equals(data.getString("fieldRight"))) {
//                data.put("fieldTextValue", RegexUtil.optNotBlankStrOpt(allMap.get("fieldTextValue")).orElseGet(() -> RegexUtil.optNotBlankStrOpt(allMap.get("value")).map(e -> {
//                    return selectOptionService.getOptionNameByCode(schemaName, data.get("fieldDataBase").toString(), allMap.get("value").toString(), (String) data.get("textName"));
//                }).orElseGet(() -> strInfo.get("strNone"))));
//            }
//        } else if ("33".equals(strInfo.get("fieldViewType"))) {
//            List tabInfo = (List) allMap.get("tabInfo");
//            if (null == tabInfo || tabInfo.size() == 0 || (data.containsKey("tabInfoActive") && "1".equals(data.get("tabInfoActive")))) {
//                result.put("tabInfoActive", fieldFormCode);
//            }
//            tabInfo.add(data);
//        } else if ("34".equals(strInfo.get("fieldViewType"))) {
//            try {
//                data.put("fieldDetailRight", "readonly");
//                if (data.containsKey("ratingCheck")) {
//                    if (RegexUtil.isNull((String) allMap.get("value"))) {
//                        Map<String, Object> ratingCheckInfo = (Map<String, Object>) data.get("ratingCheck");
//                        String ratingCheckStatus = (String) ratingCheckInfo.get("status");
//                        if (RegexUtil.isNull(ratingCheckStatus) || Integer.valueOf(ratingCheckStatus) <= (Integer) allMap.get("status")) {
//                            ratingCheckInfo.put("subWorkCode", strInfo.get("subWorkCode"));
//                            boolean isReadOnly = workSheetHandleService.checkRatingByInfo(ratingCheckInfo);
//                            if (!isReadOnly) {
//                                data.put("fieldDetailRight", "edit");
//                            }
//                        }
//                    }
//                } else {
//                    Map<String, Object> oldData = (Map<String, Object>) allMap.get("oldData");
//                    boolean isCreateUserData = (boolean) oldData.get("isCreateUserData");
//                    if (RegexUtil.isNull((String) allMap.get("value")) && isCreateUserData && StatusConstant.COMPLETED == (Integer) allMap.get("status")) {
//                        data.put("fieldDetailRight", "edit");
//                    }
//                }
//            } catch (Exception e) {
//                logger.warn("工单满意度值设置有误：" + allMap.get("value"));
//            }
//        }
//
//        // 纯文本默认值处理
//        if ("edit".equals(data.getString("fieldRight"))) {
//            data.put("isRight", true);
//            data.put("hidA", RegexUtil.optIsPresentStr(data.get("hidA")));
//            if (data.containsKey("placeholder")) {
//                data.put("placeholder", selectOptionService.getLanguageInfo(data.getString("placeholder")));
//            } else {
//                data.put("placeholder", strInfo.get("strPleaseEnter"));
//            }
//        } else {
//            data.put("isRight", false);
//            data.put("hidA", !RegexUtil.optIsPresentStr(data.get("hidA")));
//        }
//        RegexUtil.optNotBlankStrOpt(data.get("hideName")).ifPresent(e -> data.put("phoneViewType", "10"));
//
//        String specLoad = (String) data.get("specLoad");
//        if (RegexUtil.isNotNull(specLoad) && "1".equals(specLoad)) {
//            List<String> specLoadList = (List) allMap.get("specLoadList");
//            specLoadList.add(fieldFormCode);
//        }
//
//        Map<String, Object> pageAllValue = (Map<String, Object>) allMap.get("pageAllValue");
//        pageAllValue.put(fieldFormCode, data);
//
//        TypeUtil.doSetFieldSection(data, strInfo, allMap);
//        return false;
//    }

}
