package com.gengyun.senscloud.service.impl;


import com.gengyun.senscloud.service.ElasticSearchService;
import org.springframework.stereotype.Service;

@Service("ElasticSearchService")
public class ElasticSearchServiceImpl implements ElasticSearchService {
//
//    private static final Logger logger = LoggerFactory.getLogger(ElasticSearchServiceImpl.class);
//
//    private final ESRestClient esRestClient;
//
//    @Value("${es.shard.number:1}")
//    private Integer numberOfShards;
//
//    @Value("${es.replica.number:1}")
//    private Integer numberOfReplicas;
//
//
//    @Autowired
//    public ElasticSearchServiceImpl(ESRestClient esRestClient) {
//        this.esRestClient = esRestClient;
//    }
//
//
//
//    @Override
//    public boolean indexExists(String index) throws Exception {
//        GetIndexRequest request = new GetIndexRequest(index);
//        request.local(false);
//        request.humanReadable(true);
//        request.includeDefaults(false);
//        return esRestClient.getClient().indices().exists(request, RequestOptions.DEFAULT);
//    }
//
//    /**
//     * @param index
//     * @param properties 结构: {name:{type:text}} {age:{type:integer}}
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public boolean indexCreate(String index,
//                               Map<String, Object> properties) throws Exception {
//
//        if (indexExists(index)) {
//            return true;
//        }
//        CreateIndexRequest request = new CreateIndexRequest(index);
//        request.settings(Settings.builder().put("index.number_of_shards", numberOfShards)
//                .put("index.number_of_replicas", numberOfReplicas));
//
//        if (properties != null) {
//            Map<String, Object> mapping = new HashMap<>();
//            mapping.put("properties", properties);
//            request.mapping(mapping);
//        }
//
//        CreateIndexResponse createIndexResponse = esRestClient.getClient().indices().create(request, RequestOptions.DEFAULT);
//        return createIndexResponse.isAcknowledged();
//    }
//
//    /**
//     * 删除索引
//     *
//     * @param index
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public boolean indexDelete(String index) throws Exception {
//        try {
//            DeleteIndexRequest request = new DeleteIndexRequest(index);
//            AcknowledgedResponse deleteIndexResponse = esRestClient.getClient().indices().delete(request, RequestOptions.DEFAULT);
//            return deleteIndexResponse.isAcknowledged();
//        } catch (ElasticsearchException exception) {
//            if (exception.status() == RestStatus.NOT_FOUND) {
//                return true;
//            } else {
//                return false;
//            }
//        }
//    }
//
//    /**
//     * 创建更新文档
//     *
//     * @param index
//     * @param documentId
//     * @param josonStr
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public boolean documentCreate(String index, String documentId, String josonStr) throws Exception {
//        IndexRequest request = new IndexRequest(index);
//        request.id(documentId);
//        request.source(josonStr, XContentType.JSON);
//        IndexResponse indexResponse = esRestClient.getClient().index(request, RequestOptions.DEFAULT);
//        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED
//                || indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
//            return true;
//        }
//        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
//        if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
//            return true;
//        }
//        if (shardInfo.getFailed() > 0) {
//            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo
//                    .getFailures()) {
//                throw new Exception(failure.reason());
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 创建更新索引
//     *
//     * @param index
//     * @param documentId
//     * @param map
//     * @return
//     * @throws Exception
//     */
//    public boolean documentCreate(String index,
//                                  String documentId, Map<String, Object> map) throws Exception {
//        IndexRequest request = new IndexRequest(index);
//        request.id(documentId);
//        request.source(map);
//        IndexResponse indexResponse = esRestClient.getClient().index(request, RequestOptions.DEFAULT);
//        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED
//                || indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
//            return true;
//        }
//        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
//        if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
//            return true;
//        }
//        if (shardInfo.getFailed() > 0) {
//            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo
//                    .getFailures()) {
//                throw new Exception(failure.reason());
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 创建索引
//     *
//     * @param index
//     * @param josonStr
//     * @return
//     * @throws Exception
//     */
//    public String documentCreate(String index, String josonStr)
//            throws Exception {
//        IndexRequest request = new IndexRequest(index);
//        request.source(josonStr, XContentType.JSON);
//        IndexResponse indexResponse = esRestClient.getClient().index(request, RequestOptions.DEFAULT);
//        String id = indexResponse.getId();
//        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED
//                || indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
//            return id;
//        }
//        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
//        if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
//            return id;
//        }
//        if (shardInfo.getFailed() > 0) {
//            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo
//                    .getFailures()) {
//                throw new Exception(failure.reason());
//            }
//        }
//        return null;
//    }
//
//    /**
//     * 创建索引
//     *
//     * @param index
//     * @param map
//     * @return
//     * @throws Exception
//     */
//    public String documentCreate(String index,
//                                 Map<String, Object> map) throws Exception {
//        IndexRequest request = new IndexRequest(index);
//
//        request.source(map);
//        IndexResponse indexResponse = esRestClient.getClient().index(request, RequestOptions.DEFAULT);
//
//        String id = indexResponse.getId();
//        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED
//                || indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
//            return id;
//        }
//        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
//        if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
//            return id;
//        }
//        if (shardInfo.getFailed() > 0) {
//            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo
//                    .getFailures()) {
//                throw new Exception(failure.reason());
//            }
//        }
//        return null;
//    }
//
//    public boolean documentDelete(String index,
//                                  String documentId) throws Exception {
//        DeleteRequest request = new DeleteRequest(index, documentId);
//        DeleteResponse deleteResponse = esRestClient.getClient().delete(request, RequestOptions.DEFAULT);
//        if (deleteResponse.getResult() == DocWriteResponse.Result.NOT_FOUND) {
//            return true;
//        }
//        ReplicationResponse.ShardInfo shardInfo = deleteResponse.getShardInfo();
//        if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
//            return true;
//        }
//        if (shardInfo.getFailed() > 0) {
//            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo
//                    .getFailures()) {
//                throw new Exception(failure.reason());
//            }
//        }
//        return false;
//    }
//
//    @Override
//    public Map search(SearchRequest searchRequest) {
//        try {
//
//            RestHighLevelClient restClient = esRestClient.getClient();
//            SearchResponse searchResponse = restClient.search(searchRequest, RequestOptions.DEFAULT);
//            List<Map<String, Object>> returnRows=new ArrayList<Map<String, Object>>();
//            Map<String, Aggregation> asMap=null;
//            SearchHit[] searchHits = searchResponse.getHits().getHits();
//            long total=searchResponse.getHits().getTotalHits().value;
//            if(null!=searchResponse.getAggregations()){
//                asMap=searchResponse.getAggregations().asMap();
//            }
//            for (SearchHit searchHit : searchHits) {
//                returnRows.add((Map)JSON.parse(searchHit.getSourceAsString()));
//            }
//            if(null!=searchResponse.getScrollId()&&!"".equals(searchResponse.getScrollId())){
//                String scrollId = searchResponse.getScrollId();
//                //遍历搜索命中的数据，直到没有数据
//                while (searchHits != null && searchHits.length > 0) {
//                    SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
//                    scrollRequest.scroll(searchRequest.scroll());
//                    try {
//                        searchResponse = restClient.scroll(scrollRequest, RequestOptions.DEFAULT);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    scrollId = searchResponse.getScrollId();
//                    searchHits = searchResponse.getHits().getHits();
//                    if (searchHits != null && searchHits.length > 0) {
//
//                        for (SearchHit searchHit : searchHits) {
//                            returnRows.add((Map)JSON.parse(searchHit.getSourceAsString()));
//                        }
//                    }
//
//                }
//                //清除滚屏
//                ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
//                clearScrollRequest.addScrollId(scrollId);//也可以选择setScrollIds()将多个scrollId一起使用
//                ClearScrollResponse clearScrollResponse = null;
//                try {
//                    clearScrollResponse = restClient.clearScroll(clearScrollRequest,RequestOptions.DEFAULT);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                boolean succeeded = clearScrollResponse.isSucceeded();
//                System.out.println("succeeded:" + succeeded);
//            }
//            Map<String, Object> resultMap = new HashMap<String, Object>() {
//                {
//                    put("rows", returnRows);
//                    put("total",total);
//                }
//            };
//            resultMap.put("asMap",asMap);
//            return resultMap;
//        } catch (IOException e) {
//            logger.error(e.getLocalizedMessage());
//            throw new RuntimeException("Couldn't get Detail");
//        }
//    }
//
//    /**
//     * 单条件检索
//     *
//     * @param fieldKey
//     * @param fieldValue
//     * @return
//     */
//    @Override
//    public MatchPhraseQueryBuilder uniqueMatchQuery(String fieldKey, String fieldValue) {
//        return QueryBuilders.matchPhraseQuery(fieldKey, fieldValue);
//    }
//
//    /**
//     * 多条件检索并集，适用于搜索比如包含腾讯大王卡，滴滴大王卡的用户
//     *
//     * @param fieldKey
//     * @param queryList
//     * @return
//     */
//    @Override
//    public BoolQueryBuilder orMatchUnionWithList(String fieldKey, List<String> queryList) {
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        for (String fieldValue : queryList) {
//            boolQueryBuilder.should(QueryBuilders.matchPhraseQuery(fieldKey, fieldValue));
//        }
//        return boolQueryBuilder;
//    }
//
//    /**
//     * 范围查询，左右都是闭集
//     *
//     * @param fieldKey
//     * @param start
//     * @param end
//     * @return
//     */
//    @Override
//    public RangeQueryBuilder rangeMathQuery(String fieldKey, String start, String end) {
//        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(fieldKey);
//        rangeQueryBuilder.gte(start);
//        rangeQueryBuilder.lte(end);
//        return rangeQueryBuilder;
//    }
//
//    /**
//     * 根据中文分词进行查询
//     *
//     * @param fieldKey
//     * @param fieldValue
//     * @return
//     */
//    @Override
//    public MatchQueryBuilder matchQueryBuilder(String fieldKey, String fieldValue) {
//        return QueryBuilders.matchQuery(fieldKey, fieldValue).analyzer("ik_smart");
//    }
//    /**
//     *判断条件类似where
//     *
//     * @param fieldKey
//     * @param fieldValue
//     * @return
//     */
//    @Override
//    public BoolQueryBuilder getBoolQueryBuilder(Map conditionMap) {
//        return null;
//    }

}
