package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 消息处理
 */
public interface MessageService {
    /**
     * 消息发送处理
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    void sendMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo, String subject);

    /**
     * 发送邮件消息
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    void doSendEmailMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo, String subject);

    void doSendCCEmailMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo, String subject, List<String> ccUser);

    /**
     * 发送系统消息
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    void doSendSysMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo);

    /**
     * 发送短信
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    void doSendSmsMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo);

    /**
     * 根据短信编码获取短信模板详情信息
     *
     * @param methodParam 入参
     * @param msgCode     入参
     * @return 短信模板详情信息
     */
    Map<String, String> getInfoByCode(MethodParam methodParam, String msgCode);

    /**
     * 查询微信token配置信息
     *
     * @param schemaName    数据库
     * @param usualConfigId 配置主键
     * @return 微信token配置信息
     */
    Map<String, String> getWxTokenConfig(String schemaName, String usualConfigId);

    /**
     * 获取全局数据信息
     *
     * @param id 主键
     * @return 全局数据信息
     */
    Map<String, String> getGlobalInfo(String id);

    /**
     * 更新微信token
     *
     * @param schemaName    数据库
     * @param wxAppToken    wxAppToken
     * @param usualConfigId 配置主键
     * @return 更新数量
     */
    int modifyWxToken(String schemaName, String wxAppToken, String usualConfigId);

    /**
     * 更新全局微信token
     *
     * @param wxAppToken wxAppToken
     * @param id         配置主键
     * @return 更新数量
     */
    int modifyGlobalWxToken(String wxAppToken, String id);

    /**
     * 根据备用字段信息查询全局数据
     *
     * @param reserveName 字段名称
     * @param value       字段值
     * @return 全局数据
     */
    Map<String, Object> getGlobalInfoByReserve(String reserveName, String value);


    /**
     * 更新消息发送日志
     *
     * @param schemaName 入参
     * @param id         入参
     * @param isSuccess  入参
     * @return 更新记录数
     */
    int updateMessageData(String schemaName, int id, String isSuccess);

}
