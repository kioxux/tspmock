package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.BomMapper;
import com.gengyun.senscloud.mapper.BomStockListMapper;
import com.gengyun.senscloud.model.BomSearchParam;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.BussinessConfigService;
import com.gengyun.senscloud.service.FacilitiesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.bom.BomService;
import com.gengyun.senscloud.service.dynamic.DynamicCommonService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.*;
import io.swagger.annotations.Api;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/28.
 */
@Api(tags = "备件管理")
@Service
public class BomServiceImpl implements BomService {

    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    BomMapper bomMapper;
    @Resource
    BussinessConfigService bussinessConfigService;
    @Resource
    LogsService logService;
    @Resource
    FacilitiesService facilitiesService;
    @Resource
    BomStockListMapper bomStockListMapper;
    @Resource
    DynamicCommonService dynamicCommonService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    CompanyService companyService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getBomListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[12]);
    }

    /**
     * 获取备件列表
     *
     * @param methodParam 系统参数
     * @param bParam      请求参数
     * @return
     */
    @Override
    public Map<String, Object> getBomListForPage(MethodParam methodParam, BomSearchParam bParam) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();

        // 获取按条件拼接的sql
        String searchWord = this.getBomWhereString(methodParam, bParam);
        String havingWord = this.getBomHavingString(bParam);
        int total = bomMapper.countBomList(schemaName, searchWord, havingWord); // 获取备件的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            havingWord += " ORDER BY b.data_order,b.create_time DESC " + pagination;
            List<Map<String, Object>> bomList = null;
            String searchDtlType = methodParam.getSearchDtlType();
            if (RegexUtil.optIsPresentStr(searchDtlType)) {
                if (Constants.SEARCH_DTL_TYPE.equals(searchDtlType)) {
//                    bomList = bomMapper.findExportAssetList(schemaName, searchWord, havingWord);
                } else if (Constants.SEARCH_IMPORT_TYPE.equals(searchDtlType)) {
//                    bomList = bomMapper.findExportAssetList(schemaName, searchWord, havingWord);
                }
            } else {
                bomList = bomMapper.findBomList(schemaName, searchWord, havingWord);
            }
            result.put("rows", bomList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 添加备件
     *
     * @param methodParam 系统参数
     * @param data        数据信息
     */
    @Override
    public void newBom(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_NAME_F}); // 备件名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("bom_name"), LangConstant.TITLE_NAME_F); // 备件名称不能为空
        String materialCode = RegexUtil.optStrOrExpNotNull(data.get("material_code"), LangConstant.TITLE_BA); // 物料编码不能为空
        // 业务数据中可重复
        // RegexUtil.trueExp(bomMapper.countBomByMaterialCode(schemaName, materialCode) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_BA}); // 物料编码重复，请修改
        int typeId = RegexUtil.optSelectOrExpParam(data.get("type_id"), LangConstant.TITLE_CATEGORY_C); // 备件类别未选择
//        int currencyId = RegexUtil.optSelectOrExpParam(data.get("currency_id"), LangConstant.TITLE_AG_T); // 货币单位未选择
        int unit_id = RegexUtil.optSelectOrExpParam(data.get("unit_id"), LangConstant.TITLE_KK); // 单位未选择
        RegexUtil.optNotNullOrExpNullInfo(bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_bom_type", typeId), LangConstant.TITLE_CATEGORY_C); // 备件类别不存在
        if (RegexUtil.optIsPresentStr(data.get("service_life"))) {
            RegexUtil.changeToInteger(data.get("service_life").toString(), LangConstant.TITLE_AAU_B);
        }
        if (RegexUtil.optIsPresentStr(data.get("show_price"))) {
            RegexUtil.changeToBd(data.get("show_price").toString(), LangConstant.TITLE_ABAA_Y);
        } else {
            data.put("show_price", 0d);
        }
        Integer currency_id = RegexUtil.optIntegerOrNull(data.get("currency_id"), LangConstant.TITLE_AG_T);
        data.put("currency_id", currency_id);
        String bom_model = RegexUtil.optStrOrVal(data.get("bom_model"), "");
        data.put("bom_model", bom_model);
        Integer dataOrder = this.getMaxBomDataOrder(schemaName);
        data.put("data_order", dataOrder == null ? 1 : dataOrder + 1);
        data.put("create_time", SenscloudUtil.getNowTime());
        data.put("schemaName", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        data.put("is_from_service_supplier", 0);
        String id = SenscloudUtil.generateUUIDStr();
        data.put("id", id);
        int result = bomMapper.insertBom(data);
        if (RegexUtil.optNotNull(data.get("supplier_id")).isPresent()) {
            data.put("bom_id", id);
            bomMapper.insertBomSupplier(data);
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, id, LangUtil.doSetLogArray("备件新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AAAAY}));
    }

    /**
     * 获取备件明细信息
     *
     * @param methodParam 系统参数
     * @param bParam      请求参数
     * @return
     */
    @Override
    public Map<String, Object> getBomDetailById(MethodParam methodParam, BomSearchParam bParam) {
        return RegexUtil.optMapOrExpNullInfo(bomMapper.findBomById(methodParam.getSchemaName(), bParam.getId()), LangConstant.TITLE_ASSET_I); // 设备信息不存在
    }

    /**
     * 获取备件库存信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public Map<String, Object> getBomStockByBomId(MethodParam methodParam, BomSearchParam bParam) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        HashMap<String, Object> result = new HashMap<>();
        int total = bomMapper.findBomStockCountByBomId(methodParam.getSchemaName(), bParam.getId(), methodParam.getUserId());
        List<Map<String, Object>> list = null;
        if (total > 0) {
            list = RegexUtil.optNotBlankStrOpt(bParam.getId()).map(bn -> bomMapper.findBomStockByBomId(methodParam.getSchemaName(), bn, methodParam.getUserId(), pagination)).orElse(null);
        }
        result.put("total", total);
        result.put("rows", list);
        return result;
    }

    /**
     * 获取备件供应商信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public Map<String, Object> getBomSupplierInfoByBomId(MethodParam methodParam, BomSearchParam bParam) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String bomSupplierWhereString = getBomSupplierWhereString(bParam);
        HashMap<String, Object> result = new HashMap<>();
        int total = bomMapper.findBomSupplierCountByBomId(methodParam.getSchemaName(), bParam.getId(), bomSupplierWhereString);
        List<Map<String, Object>> list = null;
        if (total > 0) {
            list = RegexUtil.optNotBlankStrOpt(bParam.getId()).map(bn -> bomMapper.findBomSupplierInfoByBomId(methodParam.getSchemaName(), bn, bomSupplierWhereString, pagination)).orElse(null);
        }
        result.put("total", total);
        result.put("rows", list);
        return result;
    }

    /**
     * 新增备件供应商
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void newBomSupplier(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_SUPPLIER_A}); // 供应商不能为空
        RegexUtil.optStrOrExpNotNull(data.get("is_use"), LangConstant.TITLE_AAL_N); // 启用状态不能为空
//        String material_code = RegexUtil.optStrOrExpNotNull(data.get("material_code"), LangConstant.TITLE_AAL_N);// 启用状态不能为空
        String id = RegexUtil.optStrOrExpNotSelect(data.get("bom_id"), LangConstant.TITLE_AAAAY); // 备件未选择
        Integer supplierId = RegexUtil.optSelectOrExpParam(data.get("supplier_id"), LangConstant.TITLE_SUPPLIER_A); // 供应商未选择
        FacilitiesModel facilitiesModel = new FacilitiesModel();
        facilitiesModel.setId(supplierId);
        Map<String, Object> facility = facilitiesService.findById(methodParam, facilitiesModel);
        RegexUtil.optNotNullOrExpNullInfo(facility, LangConstant.TITLE_SUPPLIER_A); // 供应商不存在

        RegexUtil.trueExp(bomMapper.countBomSupplier(schemaName, id, supplierId) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_SUPPLIER_A}); // 物料编码重复，请修改


        data.put("schemaName", schemaName);
        Map<String, Object> bom = bomMapper.findBomById(methodParam.getSchemaName(), id);
        data.put("material_code", bom.get("material_code"));
        int i = bomMapper.insertBomSupplier(data);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, String.valueOf(id), LangUtil.doSetLogArray("备件供应商新增:" + RegexUtil.optStrAndValOrBlank(facility.get("short_title"), facility.get("title")), LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A,LangConstant.TITLE_SUPPLIER_A,RegexUtil.optStrAndValOrBlank(facility.get("short_title"), facility.get("title"))}));
        RegexUtil.intExp(i, LangConstant.MSG_BK); // 未知错误！

    }

    /**
     * 修改备件供应商
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyBomSupplier(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        Integer id = RegexUtil.optSelectOrExpParam(data.get("id"), LangConstant.TITLE_AAAAY + LangConstant.TITLE_SUPPLIER_A); // 备件未选择
        Integer supplierId = RegexUtil.optSelectOrExpParam(data.get("supplier_id"), LangConstant.TITLE_SUPPLIER_A); // 供应商未选择
        FacilitiesModel facilitiesModel = new FacilitiesModel();
        facilitiesModel.setId(supplierId);
        Map<String, Object> facility = facilitiesService.findById(methodParam, facilitiesModel);
        RegexUtil.optNotNullOrExpNullInfo(facility, LangConstant.TITLE_SUPPLIER_A); // 供应商不存在
        RegexUtil.optStrOrExpNotNull(data.get("is_use"), LangConstant.TITLE_AAL_N); // 启用状态不能为空
        String schemaName = methodParam.getSchemaName();
        data.put("schemaName", schemaName);
        Map<String, Object> dbBomSupplierInfo = this.getBomSupplierDtInfoById(data);
        Map<String, Object> dbBomInfo = this.getBomInfoByBomSupplierId(schemaName, id);
        JSONArray logArr = new JSONArray();

        String dbSupplierId = RegexUtil.optStrOrBlank(dbBomSupplierInfo.get("supplier_id"));
        String newSupplierId = RegexUtil.optStrOrBlank(data.get("supplier_id"));
        String dbFacilityName = RegexUtil.optStrAndValOrBlank(dbBomSupplierInfo.get("short_title"), dbBomSupplierInfo.get("title"));
        String facilityName = RegexUtil.optStrAndValOrBlank(facility.get("short_title"), facility.get("title"));
        if (!newSupplierId.equals(dbSupplierId)) {
            String log = LangUtil.doSetLog("备件供应商属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_SUPPLIER_A, RegexUtil.optStrAndValOrBlank(dbBomSupplierInfo.get("short_title"), dbBomSupplierInfo.get("title")), RegexUtil.optStrAndValOrBlank(facility.get("short_title"), facility.get("title"))});
            logArr.add(log);
        }
        String dbSupplyPeriod = RegexUtil.optStrOrBlank(dbBomSupplierInfo.get("supply_period"));
        String newSupplyPeriod = RegexUtil.optStrOrBlank(data.get("supply_period"));
        if (!newSupplyPeriod.equals(dbSupplyPeriod)) {
            String log = LangUtil.doSetLog("备件供应商属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_PQ, dbFacilityName + ":" + dbSupplyPeriod, facilityName + ":" + newSupplyPeriod});
            logArr.add(log);
        }
        String dbIsuse = RegexUtil.optStrOrBlank(dbBomSupplierInfo.get("is_use"));
        String newIsuse = RegexUtil.optStrOrBlank(data.get("is_use"));
        if (!newIsuse.equals(dbIsuse)) {
            String log = LangUtil.doSetLog("备件供应商属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_AAL_N, dbFacilityName + ":" + IsuseEnum.getValue(dbIsuse), facilityName + ":" + IsuseEnum.getValue(newIsuse)});
            logArr.add(log);
        }
        RegexUtil.intExp(bomMapper.updateBomSupplier(data), LangConstant.MSG_BK); // 未知错误！
        if (RegexUtil.optIsPresentList(logArr)) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, dbBomInfo.get("id").toString(), logArr.toString());
        }
    }

    private Map<String, Object> getBomInfoByBomSupplierId(String schemaName, Integer bomSupplierId) {
        return bomMapper.findBomByBomSupplierId(schemaName, bomSupplierId);
    }

    private Integer getMaxBomDataOrder(String schemaName) {
        return bomMapper.getMaxBomDataOrder(schemaName);
    }

    /**
     * 删除供应商
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void cutBomSupplier(MethodParam methodParam, BomSearchParam data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        Integer id = RegexUtil.optSelectOrExpParam(data.getId(), LangConstant.TITLE_SUPPLIER_A); // 供应商未选择
        Map<String, Object> bom = this.getBomInfoByBomSupplierId(methodParam.getSchemaName(), id);
        Map<String, Object> supplier = bomMapper.findBomSupplierById(methodParam.getSchemaName(), data.getId());
        bomMapper.deleteBomSupplier(methodParam.getSchemaName(), id);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, bom.get("id").toString(), LangUtil.doSetLogArray("备件供应商删除", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_SUPPLIER_A, RegexUtil.optStrAndValOrBlank(supplier.get("short_title"), supplier.get("title"))}));
    }

    /**
     * 启禁用备件供应商状态
     *
     * @param methodParam
     * @param param
     */
    @Override
    public void modifyUseBomSupplier(MethodParam methodParam, Map<String, Object> param) {
        Integer id = RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_SUPPLIER_A);//供应商商不能为空
        Map<String, Object> oldMap = bomMapper.findBomSupplierById(methodParam.getSchemaName(), param.get("id").toString());
        Map<String, Object> dbBomInfo = this.getBomInfoByBomSupplierId(methodParam.getSchemaName(), id);
        param.put("schemaName", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            bomMapper.updateBomSupplier(param);
            JSONArray logArr = new JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(oldMap.get("is_use"));
            String newIsUse = RegexUtil.optStrOrBlank(param.get("is_use"));
            if (!newIsUse.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("备件供应商属性更新【" + oldMap.get("short_title") + "】", LangConstant.LOG_D, new String[]{LangConstant.TITLE_AAL_N, oldMap.get("short_title") + ":" + IsuseEnum.getValue(dbIsUse), oldMap.get("short_title") + ":" + IsuseEnum.getValue(newIsUse)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, dbBomInfo.get("id").toString(), logArr.toString());
            }
        }
    }

    /**
     * 获取备件二维码
     *
     * @param methodParam
     * @param id
     */
    @Override
    public void getBomMaterialQRCode(MethodParam methodParam, String id) {
        Map<String, Object> bom = bomMapper.findBomById(methodParam.getSchemaName(), id);
        RegexUtil.optNotNullOrExp(bom, LangConstant.TEXT_K); // 信息不存在！
        if (Constants.BOM_CATEGORY_ONE_CLASS.equals(bom.get("code_classification").toString())) {
            QRCodeUtil.doShowQRCode(RegexUtil.optStrOrPrmError(bom.get("id"), methodParam, ErrorConstant.EC_BOM_1, id));
        }
    }

    /**
     * 修改备件
     *
     * @param methodParam
     * @param bParam
     * @param data
     */
    @Override
    public void modifyBom(MethodParam methodParam, BomSearchParam bParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        String schemaName = methodParam.getSchemaName();
        String id = bParam.getId();
        JSONArray logArr = new JSONArray();
        String sectionType = RegexUtil.optStrOrError(data.get("sectionType"), methodParam, ErrorConstant.EC_BOM_3, id, LangConstant.MSG_BI);
        Map<String, Object> dbBomInfo = this.getBomDetailById(methodParam, bParam);
        String dataFieldStr = null;
        if ("bomIcon".equals(sectionType)) {
            dataFieldStr = "{'name': 'icon', 'checkType': '6', 'nameKey': 'title_abba_t'}";
        }
        if ("bomTop".equals(sectionType)) {
            RegexUtil.optStrOrExpNotNull(data.get("bom_name"), LangConstant.TITLE_NAME_F); // 备件名称不能为空
//            String materialCode = RegexUtil.optStrOrExpNotNull(data.get("material_code"), LangConstant.TITLE_BA); // 物料编码不能为空
//            RegexUtil.trueExp(bomMapper.countBomByMaterialCodeWithOutId(schemaName, materialCode, id) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_BA}); // 物料编码重复，请修改
            int typeId = RegexUtil.optSelectOrExpParam(data.get("type_id"), LangConstant.TITLE_CATEGORY_C); // 备件类别未选择
            RegexUtil.optNotNullOrExpNullInfo(bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_bom_type", typeId), LangConstant.TITLE_CATEGORY_C); // 备件类别不存在
            int currencyId = RegexUtil.optSelectOrExpParam(data.get("type_id"), LangConstant.TITLE_AG_T); // 货币单位未选择
            if (RegexUtil.optIsPresentStr(data.get("service_life"))) {
                RegexUtil.changeToInteger(data.get("service_life").toString(), LangConstant.TITLE_AAU_B);
            }
            if (RegexUtil.optIsPresentStr(data.get("show_price"))) {
                RegexUtil.changeToBd(data.get("show_price").toString(), LangConstant.TITLE_ABAA_Y);
            }
//            String dbMaterialCode = RegexUtil.optStrOrBlank(dbBomInfo.get("material_code"));
//            RegexUtil.trueExp(!dbMaterialCode.equals(materialCode), LangConstant.TITLE_BA, new String[]{LangConstant.TEXT_I, LangConstant.TITLE_DH});
            dataFieldStr = "{'name': 'bom_name', 'checkType': '6','nameKey': 'title_name_f'}, " +
                    "{'name': 'bom_model', 'checkType': '6','nameKey': 'title_ah_y'}, " +
                    "{'name': 'service_life', 'checkType': '4', 'nameKey': 'title_aau_b'}," +
                    "{'name': 'show_price', 'checkType': '3', 'nameKey': 'title_abaa_y','defaultValue':'0.00'}," +
                    "{'name': 'remark', 'checkType': '6', 'nameKey': 'title_note_a'}";
            String dbTypeId = RegexUtil.optStrOrBlank(dbBomInfo.get("type_id"));
            String newTypeId = RegexUtil.optStrOrBlank(data.get("type_id"));
            Map<String, Object> newBomType = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_bom_type", Integer.valueOf(newTypeId));
            if (!newTypeId.equals(dbTypeId)) {
                String log = LangUtil.doSetLog("备件属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_PM, dbBomInfo.get("type_name").toString(), newBomType.get("type_name").toString()});
                logArr.add(log);
            }
            String dbUnitId = RegexUtil.optStrOrBlank(dbBomInfo.get("unit_id"));
            String newUnitId = RegexUtil.optStrOrBlank(data.get("unit_id"));
            Map<String, Object> newUnit = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_unit", Integer.valueOf(newUnitId));
            if (!newUnitId.equals(dbUnitId)) {
                String log = LangUtil.doSetLog("备件属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_BAAB_A, dbBomInfo.get("unit_name").toString(), newUnit.get("unit_name").toString()});
                logArr.add(log);
            }
            String dbManufacturerId = RegexUtil.optStrOrBlank(dbBomInfo.get("manufacturer_id"));
            String newManufacturerId = RegexUtil.optStrOrBlank(data.get("manufacturer_id"));
            FacilitiesModel facilitiesModel = new FacilitiesModel();
            Map<String, Object> facility = new HashMap<>();
            if (RegexUtil.optIsPresentStr(data.get("manufacturer_id"))) {
                facilitiesModel.setId(Integer.valueOf(newManufacturerId));
                facility = facilitiesService.findById(methodParam, facilitiesModel);
                dbBomInfo.put("manufacturer_name", facility.get("title"));
            }
            if (!newManufacturerId.equals(dbManufacturerId)) {
                if ("0".equals(dbManufacturerId)) {
                    dbBomInfo.put("manufacturer_name", "");
                }
                String log = LangUtil.doSetLog("备件属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_MFR_A, dbBomInfo.get("manufacturer_name").toString(), RegexUtil.optStrAndValOrBlank(facility.get("short_title"), facility.get("title"))});
                logArr.add(log);
            }
        }

        data.put("schemaName", schemaName);
        data.put("id", id);
        this.doSetBomOtherInfo("[" + dataFieldStr + "]", data, dbBomInfo, logArr, true);
        RegexUtil.intExp(bomMapper.updateBom(data), LangConstant.MSG_BK); // 未知错误！
        if (RegexUtil.optIsPresentList(logArr)) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, dbBomInfo.get("id").toString(), logArr.toString());
        }
    }

    /**
     * 修改备件启禁用状态
     *
     * @param methodParam
     * @param param
     */
    @Override
    public void modifyUseBom(MethodParam methodParam, Map<String, Object> param) {
        String id = RegexUtil.optStrOrExpNotNull(param.get("id"), LangConstant.MSG_BI);//缺少必要条件
        Map<String, Object> dbBom = bomMapper.findBomById(methodParam.getSchemaName(), id);
        param.put("schemaName", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(dbBom).isPresent()) {
            param.put("sectionType", "bomUse");
            bomMapper.updateBom(param);
            JSONArray logArr = new JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(dbBom.get("is_use"));
            String newIsUse = RegexUtil.optStrOrBlank(param.get("is_use"));
            if (!newIsUse.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("备件属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_AAL_N, IsuseEnum.getValue(dbIsUse), IsuseEnum.getValue(newIsUse)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, param.get("id").toString(), logArr.toString());
            }
        }
    }

    /**
     * 删除选中备件
     *
     * @param methodParam
     */
    @Override
    public void cutBomByIds(MethodParam methodParam) {
        String idStr = methodParam.getIds();
        String[] idArr = idStr.split(",");
        int len = idArr.length;
        if (len > 0) {
            bomMapper.updateSelectBomStatus(methodParam.getSchemaName(), idArr, StatusConstant.STATUS_DELETEED);
            boolean isBatchDel = RegexUtil.booleanCheck(methodParam.getBatchDeal());
            String remark = isBatchDel ? LangUtil.doSetLogArrayNoParam("批量删除了设备！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了设备！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAAAY});
            for (String id : idArr) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, id, remark);
            }
        }
    }

    /**
     * 获取备件出入库信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getBomInAndOutList(MethodParam methodParam, BomSearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        HashMap<String, Object> result = new HashMap<>();
        int total = bomMapper.findCountBomInAndOut(methodParam.getSchemaName(), param.getId(), methodParam.getUserId());
        List<Map<String, Object>> list = null;
        if (total > 0) {
            list = RegexUtil.optNotBlankStrOpt(param.getId()).map(bn -> bomMapper.findBomInAndOutList(methodParam.getSchemaName(), bn, methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), pagination)).orElse(null);
        }
        result.put("total", total);
        result.put("rows", list);
        return result;
    }

    /**
     * 根据物料编码查询已存在的备件
     *
     * @param schemaName
     * @param materialCodes
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryBomByMaterialCodes(String schemaName, List<String> materialCodes) {
        return bomMapper.queryBomByMaterialCodes(schemaName, materialCodes);
    }

    /**
     * 获取备件明细清单信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getBomDetailedList(MethodParam methodParam, BomSearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        HashMap<String, Object> result = new HashMap<>();
        int total = bomMapper.findCountBomDetailedList(methodParam.getSchemaName(), param.getId(), methodParam.getUserId());
        List<Map<String, Object>> list = null;
        if (total > 0) {
            list = RegexUtil.optNotBlankStrOpt(param.getId()).map(bn -> findBomDetailedList(methodParam.getSchemaName(), bn, methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), pagination)).orElse(null);
        }
        result.put("total", total);
        result.put("rows", list);
        return result;
    }

    private Map<String, Object> getBomSupplierDtInfoById(Map<String, Object> data) {
        return RegexUtil.optMapOrExpNullInfo(bomMapper.findBomSupplierById((String) data.get("schemaName"), (String) data.get("id")), LangConstant.TITLE_SUPPLIER_A); // 供应商信息不存在

    }

    private String getBomHavingString(BomSearchParam bParam) {
        StringBuffer havingString = new StringBuffer();
        StringBuffer havingCondition = new StringBuffer();
        RegexUtil.optNotBlankStrOpt(bParam.getStartCountSearch()).ifPresent(e -> havingCondition.append(" AND sum(bs.quantity)>=").append(e));
        RegexUtil.optNotBlankStrOpt(bParam.getEndCountSearch()).ifPresent(e -> havingCondition.append(" AND sum(bs.quantity)<=").append(e));
        RegexUtil.optNotBlankStrOpt(havingCondition).ifPresent(ps -> havingString.append(" having 1=1").append(havingCondition.toString()));
        return havingString.toString();
    }


    /**
     * 获取查询备件信息的条件
     *
     * @param methodParam 系统参数
     * @param bParam      请求参数
     * @return 拼接sql
     */
    private String getBomWhereString(MethodParam methodParam, BomSearchParam bParam) {
        StringBuffer whereString = new StringBuffer(" where b.status > " + StatusConstant.STATUS_DELETEED);
        RegexUtil.optNotBlankStrOpt(bParam.getId()).ifPresent(e -> whereString.append(" and b.id = '").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(bParam.getIsUseSearch()).ifPresent(e -> whereString.append(" and b.is_use = '").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(bParam.getIds()).ifPresent(e -> whereString.append(" and b.id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrLowerOpt(bParam.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(b.bom_name) like ")
                .append(e).append(" or lower(b.material_code) like ").append(e).append(" or lower(b.bom_model) like ").append(e).append(")"));
        RegexUtil.optNotNullArrayOpt(bParam.getTypeIdsSearch()).ifPresent(e -> {
            StringBuffer typeIdStr = new StringBuffer();
            for (String typeId : e) {
                RegexUtil.optNotBlankStrOpt(typeId).ifPresent(im -> typeIdStr.append(" ,").append(im));
            }
            RegexUtil.optNotBlankStrOpt(typeIdStr).ifPresent(ps -> whereString.append(" AND b.type_id IN ( ").append(ps.substring(2)).append(") "));
        });
        RegexUtil.optNotNullArrayOpt(bParam.getSuppliersSearch()).ifPresent(e -> {
            StringBuffer supplierIdStr = new StringBuffer();
            for (String supplierId : e) {
                RegexUtil.optNotBlankStrOpt(supplierId).ifPresent(im -> supplierIdStr.append(" ,").append(im));
            }
            RegexUtil.optNotBlankStrOpt(supplierIdStr).ifPresent(ps -> whereString.append(" AND s.supplier_id IN ( ").append(ps.substring(2)).append(") "));
        });

        return whereString.toString();
    }

    /**
     * 获取查询备件信息的条件
     *
     * @param bParam 请求参数
     * @return 拼接sql
     */
    private String getBomSupplierWhereString(BomSearchParam bParam) {
        StringBuffer whereString = new StringBuffer(" ");
        RegexUtil.optNotBlankStrOpt(bParam.getIsUseSearch()).ifPresent(e -> whereString.append(" and bs.is_use = '").append(e).append("' "));
        RegexUtil.optNotBlankStrLowerOpt(bParam.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(f.short_title) like ")
                .append(e).append(")"));
        return whereString.toString();
    }


    private void doSetBomOtherInfo(String dataFieldStr, Map<String, Object> data, Map<String, Object> dbData, JSONArray logArr, boolean isEdit) {
        JSONArray dataFields = JSONArray.fromObject(dataFieldStr);
        for (int i = 0; i < dataFields.size(); i++) {
            JSONObject field = dataFields.getJSONObject(i);
            String checkType = field.get("checkType").toString();
            String name = field.get("name").toString();
            String nameKey = RegexUtil.optStrOrVal(field.get("nameKey"), name);
            if ("2".equals(checkType)) {
                data.put(name, RegexUtil.optBigDecimalOrNull(data.get(name), nameKey));
            } else if ("3".equals(checkType)) {
                data.put(name, RegexUtil.optBigDecimalOrVal(data.get(name), field.get("defaultValue").toString(), nameKey));
            } else if ("4".equals(checkType)) {
                data.put(name, RegexUtil.optIntegerOrNull(data.get(name), nameKey));
            } else if ("5".equals(checkType)) {
                data.put(name, RegexUtil.optIntegerOrVal(data.get(name), RegexUtil.optIntegerOrNull(field.get("defaultValue"), nameKey), nameKey));
            } else if ("6".equals(checkType)) {
                data.put(name, RegexUtil.optStrOrBlank(data.get(name)));
            }
            if (isEdit) {
                String newValue = RegexUtil.optStrOrBlank(data.get(name));
                String dbValue = RegexUtil.optStrOrBlank(dbData.get(name));
                if (!newValue.equals(dbValue)) {
                    String log = LangUtil.doSetLog("备件属性更新", LangConstant.LOG_D, new String[]{nameKey, dbValue, newValue});
                    logArr.add(log);
                }
            }
        }
    }

    /**
     * 备件出库
     *
     * @param methodParam 入参
     * @param bomList     入参
     */
    @Override
    public void bomOutPutStock(MethodParam methodParam, List<Map<String, Object>> bomList) {
        if (RegexUtil.optNotNull(bomList).isPresent() && bomList.size() > 0) {
            for (Map<String, Object> bom : bomList) {
                if (RegexUtil.optIsPresentStr(bom.get("bom_id"))) {
                    String bom_id = RegexUtil.optStrOrNull(bom.get("bom_id"));
                    Integer stock_id = Integer.valueOf(RegexUtil.optStrOrNull(bom.get("stock_id")));
                    Float quantity = Float.valueOf(RegexUtil.optStrOrVal(bom.get("use_count"), "0"));
                    //根据备件id获取备件信息
                    Map<String, Object> bomMap = bomMapper.findBomById(methodParam.getSchemaName(), bom_id);
                    int code_classification = Integer.valueOf(RegexUtil.optStrOrNull(bomMap.get("code_classification")));
                    Map<String, Object> bomStock = bomStockListMapper.findBomStockByBomIdStockId(methodParam.getSchemaName(), bom_id, stock_id);
                    if (!RegexUtil.optNotNull(bomStock).isPresent()) {
                        //备件编码：{d}，库存数量不足，当前库存数量：{d}
                        throw new SenscloudException(LangConstant.TEXT_M, new String[]{RegexUtil.optStrOrNull(bomMap.get("material_code")), RegexUtil.optStrOrNull(quantity)});
                    } else {
                        Float now_quantity = Float.valueOf(RegexUtil.optStrOrBlank(bomStock.get("quantity")));
                        if (now_quantity < quantity) {
                            //备件编码：{d}，库存数量不足，当前库存数量：{d}
                            throw new SenscloudException(LangConstant.TEXT_M, new String[]{RegexUtil.optStrOrNull(bomMap.get("material_code")), RegexUtil.optStrOrNull(now_quantity)});
                        }
                    }
                    if (1 == code_classification) {  //一类一码
                        Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                        bomStockMap.put("bom_id", bom_id);
                        bomStockMap.put("quantity", "-" + quantity);
                        bomStockMap.put("stock_id", stock_id);
                        bomStockMap.put("bom_code", bomMap.get("material_code"));
                        bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockQuantityByBomId(methodParam.getSchemaName(), bomStockMap);
                        Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                        bomStockDetailMap.put("bom_id", bom_id);
                        bomStockDetailMap.put("quantity", "-" + quantity);
                        bomStockDetailMap.put("stock_id", stock_id);
                        bomStockDetailMap.put("bom_code", bomMap.get("material_code"));
                        bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockDetailQuantityByBomId(methodParam.getSchemaName(), bomStockDetailMap);
                    } else if (2 == code_classification) {  //一物一码
                        //入库
                        Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                        bomStockMap.put("bom_id", bom_id);
                        bomStockMap.put("quantity", "-" + quantity);
                        bomStockMap.put("stock_id", stock_id);
                        bomStockMap.put("bom_code", bomMap.get("material_code"));
                        bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockQuantityByBomId(methodParam.getSchemaName(), bomStockMap);
                        Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                        bomStockDetailMap.put("bom_id", bom_id);
                        bomStockDetailMap.put("stock_id", stock_id);
                        bomStockDetailMap.put("material_code", bomMap.get("material_code"));
                        bomStockDetailMap.put("quantity", "-" + quantity);
                        bomStockDetailMap.put("bom_code", dynamicCommonService.getBomCode(methodParam));
                        bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockDetailQuantityByBomId(methodParam.getSchemaName(), bomStockDetailMap);
                    }
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, bom_id.toString(),
                            LangUtil.doSetLogArray("减备件库存", LangConstant.TITLE_EIL, new String[]{bom.get("work_type_name").toString(), bomMap.get("quantity").toString(), String.valueOf(Float.valueOf(bomMap.get("quantity").toString()) - quantity), bom.get("work_code").toString()}));
                }
            }
        }
    }

    /**
     * 备件出库-减库存支持负数
     *
     * @param methodParam 入参
     * @param bomList     入参
     */
    public void bomOutPutStockCondition(MethodParam methodParam, List<Map<String, Object>> bomList) {
        if (RegexUtil.optNotNull(bomList).isPresent() && bomList.size() > 0) {
            for (Map<String, Object> bom : bomList) {
                if (RegexUtil.optIsPresentStr(bom.get("bom_id"))) {
                    String bom_id = RegexUtil.optStrOrNull(bom.get("bom_id"));
                    Integer stock_id = Integer.valueOf(RegexUtil.optStrOrNull(bom.get("stock_id")));
                    Float quantity = Float.valueOf(RegexUtil.optStrOrVal(bom.get("use_count"), "0"));
                    //根据备件id获取备件信息
                    Map<String, Object> bomMap = bomMapper.findBomById(methodParam.getSchemaName(), bom_id);
                    int code_classification = Integer.valueOf(RegexUtil.optStrOrNull(bomMap.get("code_classification")));
                    Map<String, Object> bomStock = bomStockListMapper.findBomStockByBomIdStockId(methodParam.getSchemaName(), bom_id, stock_id);
                    if (1 == code_classification) {  //一类一码
                        Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                        bomStockMap.put("bom_id", bom_id);
                        bomStockMap.put("quantity", "-" + quantity);
                        bomStockMap.put("stock_id", stock_id);
                        bomStockMap.put("bom_code", bomMap.get("material_code"));
                        bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockQuantityByBomId(methodParam.getSchemaName(), bomStockMap);
                        Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                        bomStockDetailMap.put("bom_id", bom_id);
                        bomStockDetailMap.put("quantity", "-" + quantity);
                        bomStockDetailMap.put("stock_id", stock_id);
                        bomStockDetailMap.put("bom_code", bomMap.get("material_code"));
                        bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockDetailQuantityByBomId(methodParam.getSchemaName(), bomStockDetailMap);
                    } else if (2 == code_classification) {  //一物一码
                        //入库
                        Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                        bomStockMap.put("bom_id", bom_id);
                        bomStockMap.put("quantity", "-" + quantity);
                        bomStockMap.put("stock_id", stock_id);
                        bomStockMap.put("bom_code", bomMap.get("material_code"));
                        bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockQuantityByBomId(methodParam.getSchemaName(), bomStockMap);
                        Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                        bomStockDetailMap.put("bom_id", bom_id);
                        bomStockDetailMap.put("stock_id", stock_id);
                        bomStockDetailMap.put("material_code", bomMap.get("material_code"));
                        bomStockDetailMap.put("quantity", "-" + quantity);
                        bomStockDetailMap.put("bom_code", dynamicCommonService.getBomCode(methodParam));
                        bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                        bomStockListMapper.updateBomStockDetailQuantityByBomId(methodParam.getSchemaName(), bomStockDetailMap);
                    }
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, bom_id.toString(),
                            LangUtil.doSetLogArray("减备件库存", LangConstant.TITLE_EIL, new String[]{bom.get("work_type_name").toString(), bomMap.get("quantity").toString(), String.valueOf(Float.valueOf(bomMap.get("quantity").toString()) - quantity), bom.get("work_code").toString()}));
                }
            }
        }
    }

    /**
     * 备件入库
     *
     * @param methodParam 入参
     * @param bomList     入参
     */
    @Override
    public void bomInPutStock(MethodParam methodParam, List<Map<String, Object>> bomList) {
        if (RegexUtil.optNotNull(bomList).isPresent() && bomList.size() > 0) {
            for (Map<String, Object> bom : bomList) {
                if (RegexUtil.optIsPresentStr(bom.get("bom_id"))) {
                    String bom_id = RegexUtil.optStrOrNull(bom.get("bom_id"));
                    Integer stock_id = Integer.valueOf(RegexUtil.optStrOrNull(bom.get("stock_id")));
                    Float quantity = Float.valueOf(RegexUtil.optStrOrVal(bom.get("use_count"), "0"));
                    //根据备件id获取备件信息
                    Map<String, Object> bomMap = bomMapper.findBomById(methodParam.getSchemaName(), bom_id);
                    int code_classification = Integer.valueOf(RegexUtil.optStrOrNull(bomMap.get("code_classification")));
                    Map<String, Object> bomStock = bomStockListMapper.findBomStockByBomIdStockId(methodParam.getSchemaName(), bom_id, stock_id);
                    if (1 == code_classification) {  //一类一码
                        if (!RegexUtil.optNotNull(bomStock).isPresent()) {
                            //根据备件id去查询是否已经生成备件编码
                            Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                            bomStockMap.put("bom_id", bom_id);
                            bomStockMap.put("quantity", quantity);
                            bomStockMap.put("stock_id", stock_id);
                            bomStockMap.put("security_quantity", 0);
                            bomStockMap.put("max_security_quantity", 0);
                            bomStockMap.put("bom_code", bomMap.get("material_code"));
                            bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.insertBomStock(methodParam.getSchemaName(), bomStockMap);
                            bomStock = bomStockListMapper.findBomStockByBomIdStockId(methodParam.getSchemaName(), bom_id, stock_id);
                            Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                            bomStockDetailMap.put("bom_id", bom_id);
                            bomStockDetailMap.put("quantity", quantity);
                            bomStockDetailMap.put("stock_id", stock_id);
                            bomStockDetailMap.put("bom_code", bomMap.get("material_code"));
                            bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.insertBomStockDetail(methodParam.getSchemaName(), bomStockDetailMap);
                        } else {
                            bomStock.put("quantity", 0);
                            Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                            bomStockMap.put("bom_id", bom_id);
                            bomStockMap.put("quantity", quantity);
                            bomStockMap.put("stock_id", stock_id);
                            bomStockMap.put("bom_code", bomMap.get("material_code"));
                            bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.updateBomStockQuantityByBomId(methodParam.getSchemaName(), bomStockMap);
                            Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                            bomStockDetailMap.put("bom_id", bom_id);
                            bomStockDetailMap.put("quantity", quantity);
                            bomStockDetailMap.put("stock_id", stock_id);
                            bomStockDetailMap.put("bom_code", bomMap.get("material_code"));
                            bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.updateBomStockDetailQuantityByBomId(methodParam.getSchemaName(), bomStockDetailMap);
                        }
                    } else if (2 == code_classification) {  //一物一码
                        if (!RegexUtil.optNotNull(bomStock).isPresent()) {
                            //根据备件id去查询是否已经生成备件编码
                            Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                            bomStockMap.put("bom_id", bom_id);
                            bomStockMap.put("quantity", quantity);
                            bomStockMap.put("stock_id", stock_id);
                            bomStockMap.put("security_quantity", 0);
                            bomStockMap.put("max_security_quantity", 0);
                            bomStockMap.put("bom_code", bomMap.get("material_code"));
                            bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.insertBomStock(methodParam.getSchemaName(), bomStockMap);
                        } else {
                            //入库
                            Map<String, Object> bomStockMap = new HashMap<>();//备件库存
                            bomStockMap.put("bom_id", bom_id);
                            bomStockMap.put("quantity", quantity);
                            bomStockMap.put("stock_id", stock_id);
                            bomStockMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.updateBomStockQuantityByBomId(methodParam.getSchemaName(), bomStockMap);
                        }
                        for (int i = 0; i < quantity.intValue(); i++) {
                            Map<String, Object> bomStockDetailMap = new HashMap<>();//备件库存明细
                            bomStockDetailMap.put("bom_id", bom_id);
                            bomStockDetailMap.put("quantity", 1);
                            bomStockDetailMap.put("stock_id", stock_id);
                            bomStockDetailMap.put("material_code", bomMap.get("material_code"));
                            bomStockDetailMap.put("bom_code", dynamicCommonService.getBomCode(methodParam));
                            bomStockDetailMap.put("store_position", RegexUtil.optStrOrBlank(bom.get("store_position")));
                            bomStockListMapper.insertBomStockDetail(methodParam.getSchemaName(), bomStockDetailMap);
                        }

                    }
                    //logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, bom_id.toString(),
                    //        LangUtil.doSetLog("加备件库存", LangConstant.TITLE_EIL, new String[]{bom.get("work_type_name").toString(), bomStock.get("quantity").toString(), String.valueOf(Float.valueOf(bomStock.get("quantity").toString()) + quantity), bom.get("work_code").toString()}));
                    bomStock = RegexUtil.optMapOrNew(bomStock);
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_3000, bom_id.toString(),
                            LangUtil.doSetLogArray("加备件库存", LangConstant.TITLE_EIL, new String[]{bom.get("work_type_name").toString(), RegexUtil.optStrOrBlank(bomMap.get("quantity")), String.valueOf(Float.valueOf(RegexUtil.optStrOrVal(bomMap.get("quantity"), "0")) + quantity), bom.get("work_code").toString()}));
                }
            }
        }
    }

    /**
     * 更新备件库存
     *
     * @param methodParam
     * @param bomList
     * @param isAdd       1增加库存 -1 减少库存
     */
    @Override
    public void updateBomStock(MethodParam methodParam, List<Map<String, Object>> bomList, Integer isAdd) {
        synchronized (this) {
            if (1 == isAdd) {
                bomInPutStock(methodParam, bomList);
            } else if (-1 == isAdd) {
                bomOutPutStock(methodParam, bomList);
            } else if (3 == isAdd) {
                bomOutPutStockCondition(methodParam, bomList);
            }
        }
    }

    /**
     * 查询库存低于安全库存的备件库存信息
     *
     * @param schemaName
     * @return
     */
    @Override
    public List<Map<String, Object>> getBomLowerSecurityQuantityList(String schemaName) {
        return bomStockListMapper.findBomLowerSecurityQuantityList(schemaName);
    }

    /**
     * 更新备件库存发送信息时间
     */
    @Override
    public void updateBomStockSendMsgTime(String schemaName, Integer id) {
        bomStockListMapper.updateBomStockSendMsgTime(schemaName, id);
    }

    @Override
    public void updateStockSendMsgTime(String schemaName, Integer id) {
        bomStockListMapper.updateStockSendMsgTime(schemaName, id);
    }

    /**
     * 批量新增、修改备件信息
     *
     * @param schemaName
     * @param bomList
     */
    @Override
    public void batchInsertBom(String schemaName, List<Map<String, Object>> bomList) {
        bomMapper.insertOrUpdateBom(schemaName, bomList);
    }

    /**
     * 批量更新备件库存
     *
     * @param schemaName
     * @param bomStockList
     */
    @Override
    public void batchInsertOrUpdateBomStock(String schemaName, List<Map<String, Object>> bomStockList) {
        bomStockListMapper.insertOrUpdateBomStock(schemaName, bomStockList);
    }

    /**
     * 批量新增，修改备件导入记录
     *
     * @param schemaName
     * @param bomImportList
     */
    @Override
    public void batchInsertBomImport(String schemaName, List<Map<String, Object>> bomImportList) {
        bomStockListMapper.insertBomImport(schemaName, bomImportList);
    }

    /**
     * 批量更新备件库存明细
     *
     * @param schemaName
     * @param bomStockDetailList
     */
    @Override
    public void batchInsertOrUpdateBomStockDetail(String schemaName, List<Map<String, Object>> bomStockDetailList) {
        bomStockListMapper.insertOrUpdateBomStockDetail(schemaName, bomStockDetailList);

    }

    /**
     * 根据主键展示备件二维码（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    @Override
    public void doShowBomQRCode(MethodParam methodParam) {
        String id = methodParam.getDataId();
        Map<String, Object> bom = bomMapper.findBomById(methodParam.getSchemaName(), id);
        RegexUtil.optNotNullOrExp(bom, LangConstant.TEXT_K); // 信息不存在！
        QRCodeUtil.doShowQRCode(RegexUtil.optStrOrPrmError(bom.get("id"), methodParam, ErrorConstant.EC_ASSET_1, id));
    }

    /**
     * 根据选中主键下载备件二维码
     *
     * @param methodParam 系统参数
     * @param bParam     请求参数
     */
    @Override
    public void doExportBomQRCode(MethodParam methodParam, BomSearchParam bParam) {
        Map<String, Object> map = commonUtilService.qrCodeExport(methodParam, 3);
        bParam.setFieldList(DataChangeUtil.scdObjToListStr(map.get("fieldCodes")));
        List<Map<String, Object>> bomList = this.getBomDtlListByParam(methodParam, bParam); // 数据权限判断，获取数据
        map.put("dataList", bomList);
        map.put("companyId", methodParam.getCompanyId());
        map.put("companyName", methodParam.getCompanyName());
        String schemaName = methodParam.getSchemaName();
        map.put("qrCodePrefix", RegexUtil.optStrOrBlank(systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.QR_CODE_PREFIX)));
        String ctVal = systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.CODE_TEMPLATE);
        if (RegexUtil.optEqualsOpt(ctVal, "2").isPresent()) {
            QRCodeUtil.doDownloadBRCodePdf(map);
        } else {
            QRCodeUtil.doDownloadQRCodePdf(map);
        }
    }

    /**
     * 获取备件明细信息列表
     *
     * @param methodParam 系统参数
     * @param bParam     请求参数
     * @return 设备信息列表
     */
    private List<Map<String, Object>> getBomDtlListByParam(MethodParam methodParam, BomSearchParam bParam) {
        Map<String, Object> info = this.getBomListForPage(methodParam, bParam); // 数据权限判断
        return RegexUtil.optObjToListOrExp(RegexUtil.optMapOrExpNullInfo(info, LangConstant.TITLE_AAAAY).get("rows"), LangConstant.TITLE_ASSET_I); // 备件信息不存在
    }

    public List<Map<String, Object>> findBomDetailedList(String schemaName, String bn, String userId, String userLang, Long companyId, String pagination) {
        List<Map<String, Object>> bomDetailedList = bomMapper.findBomDetailedList(schemaName, bn, userId, pagination);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentList(bomDetailedList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                bomDetailedList.forEach(sd -> {
                    if (dd.containsKey(sd.get("in_type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("in_type_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("in_type_name", value);
                        }
                    }
                });
            }
        }
        return bomDetailedList;
    }
}
