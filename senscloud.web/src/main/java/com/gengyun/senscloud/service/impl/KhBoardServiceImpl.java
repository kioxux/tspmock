package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.KhBoardService;
import org.springframework.stereotype.Service;

@Service
public class KhBoardServiceImpl implements KhBoardService {
//
//    @Autowired
//    KhBoardMapper khBoardMapper;
//
//    @Autowired
//    FacilitiesMapper facilitiesMapper;
//
//    @Autowired
//    AssetAnalysByMapMapper mapper;
//
//    /**
//     * 设备类型与数量
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getCategoryAndBillCountList(String schemaName) {
//        return khBoardMapper.getCategoryAndBillCountList(schemaName);
//    }
//
//    /**
//     * 设备状态与数量
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getStatusAndBillCountList(String schemaName) {
//        return khBoardMapper.getStatusAndBillCountList(schemaName);
//    }
//
//    /**
//     * 故障率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getMtbf(String schemaName) {
//        return khBoardMapper.getMtbf(schemaName);
//    }
//
//    /**
//     * 故障间隔
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getMttr(String schemaName) {
//        return khBoardMapper.getMttr(schemaName);
//    }
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getMaintainRate(String schemaName, HttpServletRequest request) {
//        String type = request.getParameter("type");
//        Calendar c = Calendar.getInstance();
//        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
//        if (day_of_week == 0)
//            day_of_week = 7;
//        c.add(Calendar.DATE, -day_of_week + 1);
//
//        Calendar d = Calendar.getInstance();
//        int day_week = d.get(Calendar.DAY_OF_WEEK) - 1;
//        if (day_week == 0)
//            day_week = 7;
//        d.add(Calendar.DATE, -day_week + 7);
//
//        String condition = "";
//        switch (type) {
//            case "day":
//                condition += "";
//                break;
//            case "week":
//                condition += "and w.occur_time>='" + new Timestamp(c.getTimeInMillis()) + "' and w.occur_time<'" + new Timestamp(d.getTimeInMillis()) + "'";
//                break;
//            case "month":
//                condition += "and to_char(w.occur_time, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') = to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')";
//                break;
//            case "year":
//                condition += "and to_char(w.occur_time, '" + SqlConstant.SQL_DATE_FMT_YEAR + "') = to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR + "')";
//                break;
//        }
//        return khBoardMapper.getMaintainRate(schemaName, condition);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public Map<String, Object> getSafetyInProduction(String schemaName) {
//        Map<String, Object> resultMap = new HashMap<String, Object>() {
//            {
//                put("total", 0);
//            }
//        };
//        resultMap.put("total",khBoardMapper.getSafetyInProduction(schemaName).get("run"));
//        resultMap.put("unfinishTotal", khBoardMapper.getCalendarList(schemaName, "and to_char(occur_time,'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') and accident_type=1 ").size());
//        resultMap.put("finishTotal", khBoardMapper.getCalendarList(schemaName, "and to_char(occur_time,'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') and accident_type=2 ").size());
//        return resultMap;
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public Map<String, Object> getLineManNum(String schemaName) {
//        Map<String, Object> resultMap = new HashMap<String, Object>() {
//            {
//                put("total", khBoardMapper.getLineManCount(schemaName));
//                put("list", khBoardMapper.getLineManNum(schemaName));
//            }
//        };
//        return resultMap ;
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public Map<String, Object> getLineOndutyNum(String schemaName) {
//
//        Map<String, Object> resultMap = new HashMap<String, Object>() {
//            {
//                put("total", khBoardMapper.getLineOnDutyCount(schemaName));
//                put("list", khBoardMapper.getLineOndutyNum(schemaName));
//            }
//        };
//        return resultMap ;
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getCalendarList(String schemaName, HttpServletRequest request) {
//        String condition = "and to_char(occur_time,'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')";
//        return khBoardMapper.getCalendarList(schemaName, condition);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public int getTodyWork(String schemaName) {
//        return khBoardMapper.getTodyWork(schemaName);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getArrangement(String schemaName, HttpServletRequest request) {
//        String type = request.getParameter("type");
//        Calendar c = Calendar.getInstance();
//        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
//        if (day_of_week == 0)
//            day_of_week = 7;
//        c.add(Calendar.DATE, -day_of_week + 1);
//
//        Calendar d = Calendar.getInstance();
//        int day_week = d.get(Calendar.DAY_OF_WEEK) - 1;
//        if (day_week == 0)
//            day_week = 7;
//        d.add(Calendar.DATE, -day_week + 7);
//
//        String condition = "";
//
//        switch (type) {
//            case "day":
//                condition += "and to_char(a.end_date, '" + SqlConstant.SQL_DATE_FMT + "') >= to_char(now(), '" + SqlConstant.SQL_DATE_FMT + "') and a.deliverable_type=1";
//                break;
//            case "week":
//                condition += " and a.end_date>='" + new Timestamp(d.getTimeInMillis()) + "' and a.deliverable_type=2";
//                break;
//            case "month":
//                condition += "and to_char(a.end_date, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') >= to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') and a.deliverable_type=3";
//                break;
//            case "year":
//                condition += "and to_char(a.end_date, '" + SqlConstant.SQL_DATE_FMT_YEAR + "') = to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR + "') and a.deliverable_type=4";
//                break;
//        }
//        return khBoardMapper.getArrangement(schemaName, condition);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public Map<String, Object> getNotice(String schemaName) {
//        return khBoardMapper.getNotice(schemaName);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getLineAndBillCountList(String schemaName) {
//        return khBoardMapper.getLineAndBillCountList(schemaName);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getLineList(String schemaName) {
//        DecimalFormat df = new DecimalFormat("#.00");
//        List<Map<String, Object>> lineList = khBoardMapper.getLineList(schemaName);
//        List<String> errorList = khBoardMapper.getLineErrorList(schemaName);
//        for (Map<String, Object> line : lineList) {
//            if (!errorList.isEmpty() && errorList.contains(String.valueOf(line.get("id")))) {
//                line.put("status", "故障");
//            } else {
//                line.put("status", "正常");
//            }
//            line.put("utilizationrate", "10");
//
//        }
//        return lineList;
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public Map<String, Object> getWorkSheetStatusAndBillCountList(String schemaName, HttpServletRequest request) {
//        String type = request.getParameter("type");
//        String work_type = request.getParameter("work_type");
//
//        String condition = getTimeConditionByType("w",type);
//        if ("repair".equals(work_type)) {
//            condition += "and  wt.business_type_id=1 ";
//        } else if ("maintain".equals(work_type)) {
//            condition += "and  wt.business_type_id=2 ";
//        }
//
//        List<Map<String, Object>> list = khBoardMapper.getWorkSheetStatusAndBillCountList(schemaName,condition );
//        Map<String, Object> resultMap = new HashMap<String, Object>() {
//            {
//                put("finishTotal", 0);
//                put("beProcessed", 0);
//            }
//        };
//        int total = 0;
//        int finish = 0;
//        for (Map<String, Object> data : list) {
//            if (60 == ((int) data.get("id"))) {
//                finish = (int) data.get("count");
//                resultMap.put("finishTotal", finish);
//            }
//            if (20 == ((int) data.get("id"))) {
//                resultMap.put("beProcessed", (int) data.get("count"));
//            }
//            total = total + (int) data.get("count");
//        }
//        resultMap.put("total", total);
//        resultMap.put("unfinishTotal", total - finish);
//        return resultMap;
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getLineImportantList(String schemaName) {
//        List<Map<String, Object>> lineList = khBoardMapper.getLineImportantList(schemaName);
//        List<String> errorList = khBoardMapper.getLineErrorList(schemaName);
//        for (Map<String, Object> line : lineList) {
//            if (!errorList.isEmpty() && errorList.contains(String.valueOf(line.get("id")))) {
//                line.put("status", "故障产线");
//            } else {
//                line.put("status", "安全生产");
//            }
//        }
//        return lineList;
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getArrangementList(String schemaName) {
//        return khBoardMapper.getArrangementList(schemaName);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getFailureRate(String schemaName) {
//        return khBoardMapper.getFailureRate(schemaName);
//    }
//
//    /**
//     * 按条件查询故障次数
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getFailureNum(String schemaName) {
//        return khBoardMapper.getFailureNum(schemaName);
//    }
//
//
//    /**
//     * 按条件查询保养完成率
//     *
//     * @param schemaName
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getLineStatusAndBillCountList(String schemaName) {
//        List<String> errorList = khBoardMapper.getLineErrorList(schemaName);
//        List<Facility> list = facilitiesMapper.FacilitiesList(schemaName);
//        Map<String, Object> resultMap = new HashMap<>();
//        Map<String, Object> resultMap1 = new HashMap<>();
//        resultMap.put("name", "故障产线");
//        resultMap.put("value", errorList.size());
//        resultMap1.put("name", "正常运行");
//        resultMap1.put("value", list.size() - errorList.size());
//        List<Map<String, Object>> resultList = new ArrayList<>();
//        resultList.add(resultMap);
//        resultList.add(resultMap1);
//        return resultList;
//    }
//
//
//    /**
//     * 按紧急度查询维修工单数量
//     *
//     * @param schema_name
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getRepairNumByLevelCondition(String schema_name,HttpServletRequest request) {
//        String type = request.getParameter("type");
//        return khBoardMapper.getRepairNumByLevelCondition(schema_name,getTimeConditionByType("wr",type));
//    }
//
//    /**
//     * 按设备类型统计维修工单数
//     *
//     * @param schema_name
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getCategoryAndRepairCountList(String schema_name) {
//        return khBoardMapper.getCategoryAndRepairCountList(schema_name);
//    }
//
//    /**
//     * 按客户统计维修工单数量
//     *
//     * @param schema_name
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getRepairNumByFacilityCondition(String schema_name,HttpServletRequest request) {
//        String type = request.getParameter("type");
//        return khBoardMapper.getRepairNumByFacilityCondition(schema_name,getTimeConditionByType("wr",type));
//    }
//
//    /**
//     * 用户列表
//     *
//     * @param schema_name
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getUserAndRepairCountList(String schema_name) {
//        return khBoardMapper.getUserAndRepairCountList(schema_name);
//    }
//
//    /**
//     * 获取最新上报的工单
//     *
//     * @param schema_name
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getPendingDisposalCondition(String schema_name, HttpServletRequest request) {
//        String type = request.getParameter("type");
//        return khBoardMapper.getPendingDisposalCondition(schema_name,getTimeConditionByType("w",type));
//    }
//
//    /**
//     * 获取用户的坐标
//     *
//     * @param schema_name
//     * @param request
//     * @return List<Map < String ,   Object>>
//     */
//    @Override
//    public List<Map<String, Object>> getPoint(String schema_name,HttpServletRequest request) {
//        String account= request.getParameter("account");
//        String condition="";
//        if(RegexUtil.isNotNull(account)){
//            condition += " and (u.account ='" + account + "' ) ";
//        }
//        return khBoardMapper.getPoint(schema_name,condition);
//    }
//
//    /**
//     * 获取时间条件
//     *
//     * @param tabName
//     * @param type
//     * @return String
//     */
//    private String getTimeConditionByType(String tabName, String type){
//        Calendar c = Calendar.getInstance();
//        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
//        if (day_of_week == 0)
//            day_of_week = 7;
//        c.add(Calendar.DATE, -day_of_week + 1);
//
//        Calendar d = Calendar.getInstance();
//        int day_week = d.get(Calendar.DAY_OF_WEEK) - 1;
//        if (day_week == 0)
//            day_week = 7;
//        d.add(Calendar.DATE, -day_week + 7);
//        String condition = "";
//        switch (type) {
//            case "day":
//                condition += "and to_char("+tabName+".occur_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(now(), '" + SqlConstant.SQL_DATE_FMT + "')";
//                break;
//            case "week":
//                condition += "and "+tabName+".occur_time>='" + new Timestamp(c.getTimeInMillis()) + "' and "+tabName+".occur_time<'" + new Timestamp(d.getTimeInMillis()) + "'";
//                break;
//            case "month":
//                condition += " and to_char("+tabName+".occur_time, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') = to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')";
//                break;
//            case "year":
//                condition += " and to_char("+tabName+".occur_time, '" + SqlConstant.SQL_DATE_FMT_YEAR + "') = to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR + "')";
//                break;
//        }
//        return condition;
//    }
//
//    /**
//     * 获取客户个状态的工单数
//     *
//     * @param schema_name
//     * @return List<FacilitiesResult>
//     */
//    @Override
//    public List<FacilitiesResult> GetFacilitiesCustomResultById(String schema_name) {
//        String filed = "";
//        int businessType = Constants.BUSINESS_TYPE_REPAIR_TYPE;
//        filed = ",(SELECT COUNT(DISTINCT a._id ) FROM " + schema_name + "._sc_asset a  " +
//                "INNER JOIN " + schema_name + "._sc_works_detail wd ON wd.relation_id = a._id " +
//                "INNER JOIN " + schema_name + "._sc_asset_organization ao ON ao.asset_id = a._id " +
//                "INNER JOIN " + schema_name + "._sc_work_type wt ON wt.id = wd.work_type_id " +
//                "INNER JOIN (" +
//                "   SELECT cf.* FROM " + schema_name + "._sc_facilities pf " +
//                "   INNER JOIN " + schema_name + "._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
//                "   WHERE pf.id = fac.ID " +
//                ") f ON ao.org_id = f.id " +
//                "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.COMPLETED +
//                " AND wt.business_type_id = " + businessType + " AND wd.relation_type = 2 ) as repairMaintainAssetCount " +
//                ",(SELECT COUNT(DISTINCT a._id ) FROM " + schema_name + "._sc_asset a  " +
//                "INNER JOIN " + schema_name + "._sc_works_detail wd ON wd.relation_id = a._id " +
//                "INNER JOIN " + schema_name + "._sc_asset_organization ao ON ao.asset_id = a._id " +
//                "INNER JOIN " + schema_name + "._sc_work_type wt ON wt.id = wd.work_type_id " +
//                "INNER JOIN (" +
//                "   SELECT cf.* FROM " + schema_name + "._sc_facilities pf " +
//                "   INNER JOIN " + schema_name + "._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
//                "   WHERE pf.id = fac.ID " +
//                ") f ON ao.org_id = f.id " +
//                "WHERE wd.status > " + StatusConstant.PENDING_ORDER + " AND wd.status < " + StatusConstant.COMPLETED +
//                " AND wt.business_type_id = " + businessType + " AND wd.relation_type = 2 ) as doingRepairMaintainAssetCount " +
//                ",(SELECT COUNT(DISTINCT a._id ) FROM " + schema_name + "._sc_asset a  " +
//                "INNER JOIN " + schema_name + "._sc_works_detail wd ON wd.relation_id = a._id " +
//                "INNER JOIN " + schema_name + "._sc_asset_organization ao ON ao.asset_id = a._id " +
//                "INNER JOIN " + schema_name + "._sc_work_type wt ON wt.id = wd.work_type_id " +
//                "INNER JOIN (" +
//                "   SELECT cf.* FROM " + schema_name + "._sc_facilities pf " +
//                "   INNER JOIN " + schema_name + "._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
//                "   WHERE pf.id = fac.ID " +
//                ") f ON ao.org_id = f.id " +
//                "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.PENDING_ORDER +
//                " AND wt.business_type_id = " + businessType + " AND wd.relation_type = 2 ) as untreatedRepairMaintainAssetCount " ;
//        return mapper.GetAllParentFacilitiesCustomResultList(schema_name, filed);
//    }
}
