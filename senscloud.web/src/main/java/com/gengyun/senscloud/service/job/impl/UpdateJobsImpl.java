package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.service.job.UpdateJobs;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class UpdateJobsImpl implements UpdateJobs {
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AlarmAnomalySendInfoService alarmAnomalySendInfoService;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetPositionService assetPositionService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    AssetInfoService assetInfoService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    BomStockListService bomStockListService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    PlanWorkService planWorkService;
//
//    @Autowired
//    MetadataWorkService metadataWorkService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    AssetGuaranteeService assetGuaranteeService;
//
//    @Autowired
//    MessageService messageService;
//
//    @Autowired
//    MessageCenterService messageCenterService;
//
//    @Autowired
//    private MessageRuleService messageRuleService;
//
//    @Autowired
//    FlowBusinessService flowBusinessService;
//
//    //test
//    @Override
//    @Scheduled(fixedDelay = ONE_Hour)
//    public void fixedDelayJob() {
//        System.out.println("hello");
//    }
//
//    /*  第三版本的维保计划方法开始，主要对维保计划进行强化 2019-07-30 yzj 注  */
//    // 按维保计划，其中配置的设备类型、设备型号、设备，以不同的条件，进行执行
//    // 目前计划corn配置为每小时的第一分钟执行，计划中，非按小时执行的，统一在6点执行，按小时的，则为设置的小时执行
//    public void cronJobToGerenatePlanWorkWithNewModel(String schema_name) {
//        try {
//            int total = 0;
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            //查找有效的计划
//            List<PlanWorkModel> planList = planWorkService.getExecutePlanWork(schema_name, now);
//            if (null != planList && planList.size() > 0) {
//                Date nowDate = new Date();
//                //获取当前天
//                SimpleDateFormat currentDayFormat = new SimpleDateFormat(Constants.DATE_FMT_D);
//                int currentDay = Integer.parseInt(currentDayFormat.format(nowDate));
//                //获取当前小时
//                SimpleDateFormat currentHourFormat = new SimpleDateFormat(Constants.TIME_FMT_H);
//                int currentHour = Integer.parseInt(currentHourFormat.format(nowDate));
//                //获取当前日期
//                SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//                String nowDateWord = simleDateFormat.format(nowDate);
//                //拼接今天起止时间，获取排班中的人员
//                long today = Timestamp.valueOf(nowDateWord + " 23:59:59").getTime();
//                Timestamp beginDateTime = Timestamp.valueOf(nowDateWord + " 23:59:59");
//                Timestamp endDateTime = Timestamp.valueOf(nowDateWord + " 00:00:00");
//
//                // 维修是否类同保养，即维修后，该周期保养自动延后，1：维修和保养；2：只保养；
//                int isRepairEqualMaintain = 2;
//                try {
//                    SystemConfigData repairEqualMaintainData = systemConfigService.getSystemConfigData(schema_name, "is_repair_equal_maintain");
//                    if (null != repairEqualMaintainData && !repairEqualMaintainData.getSettingValue().isEmpty()) {
//                        isRepairEqualMaintain = Integer.parseInt(repairEqualMaintainData.getSettingValue());
//                    }
//                } catch (Exception e) {
//                    isRepairEqualMaintain = 2;
//                }
//                // PM生成时，触发时间是完成时间还是截止时间，1：截止时间；2：完成时间；
//                int isDeadlineTimeOrFinishedTime = 1;
//                try {
//                    SystemConfigData deadlineTimeOrFinishedTimeData = systemConfigService.getSystemConfigData(schema_name, "pm_triggle_time_rule");
//                    if (null != deadlineTimeOrFinishedTimeData && !deadlineTimeOrFinishedTimeData.getSettingValue().isEmpty()) {
//                        isDeadlineTimeOrFinishedTime = Integer.parseInt(deadlineTimeOrFinishedTimeData.getSettingValue());
//                    }
//                } catch (Exception e) {
//                    isDeadlineTimeOrFinishedTime = 2;
//                }
//                // PM生成时，如果有维修(依赖保养计划，维修是否同保养)或保养，是否生成，1：生成；2：不生成；
//                int isGenerateWhenHavingWork = 1;
//                try {
//                    SystemConfigData generateWorkData = systemConfigService.getSystemConfigData(schema_name, "pm_no_triggle_exist_dealing");
//                    if (null != generateWorkData && !generateWorkData.getSettingValue().isEmpty()) {
//                        isGenerateWhenHavingWork = Integer.parseInt(generateWorkData.getSettingValue());
//                    }
//                } catch (Exception e) {
//                    isGenerateWhenHavingWork = 1;
//                }
//                int defaultGenerateTime = -1;
//                try {
//                    SystemConfigData defaultGenerateTimeData = systemConfigService.getSystemConfigData(schema_name, "generate_auto_task_time");
//                    if (null != defaultGenerateTimeData && !defaultGenerateTimeData.getSettingValue().isEmpty()) {
//                        defaultGenerateTime = Integer.parseInt(defaultGenerateTimeData.getSettingValue());
//                    }
//                } catch (Exception e) {
//                    defaultGenerateTime = -1;
//                }
//                // 工单生成默认sleep时间
//                int generateWorkSleepTime = 50;
//                try {
//                    SystemConfigData generateWorkSleepTimeData = systemConfigService.getSystemConfigData(schema_name, "generate_work_sleep_time");
//                    if (null != generateWorkSleepTimeData && !generateWorkSleepTimeData.getSettingValue().isEmpty()) {
//                        generateWorkSleepTime = Integer.parseInt(generateWorkSleepTimeData.getSettingValue());
//                    }
//                } catch (Exception e) {
//                    generateWorkSleepTime = 50;
//                }
//
//                //设备组织类型是设备组织，还是设备位置，默认是设备位置 [{"name": "设备位置","value": "1"},{"name": "设备组织+位置","value": "2"}]
//                int assetOrgPositionType = 1;
//                try {
//                    SystemConfigData orgPositionTypeData = systemConfigService.getSystemConfigData(schema_name, "asset_org_position_type");
//                    if (null != orgPositionTypeData && !orgPositionTypeData.getSettingValue().isEmpty()) {
//                        assetOrgPositionType = Integer.parseInt(orgPositionTypeData.getSettingValue());
//                    }
//                } catch (Exception e) {
//                    assetOrgPositionType = 1;
//                }
//
//                //遍历计划
//                for (PlanWorkModel planWorkModel : planList) {
//                    //获取该计划下所有的配置内容，按配置内容遍历，并进行工单
//                    Map<String, Object> planWorkAllSettingItem = planWorkService.findNeedDoPlanScheduleByPlanCode(schema_name, planWorkModel.getPlan_code());
//                    if (null == planWorkAllSettingItem) {
//                        continue;
//                    }
//                    int doObjectType;   // 是否需要执行
//                    //触发条件：1：所有条件满足执行；2：任一条件满足即执行；
//                    int do_type = planWorkModel.getDo_type();
//                    // 读取配置的执行条件，_sc_plan_work_triggle，看看执行条件是否满足，如果满足，则可以执行
//                    List<Map<String, Object>> planTriggleModelList;
//                    try {
//                        planTriggleModelList = (List<Map<String, Object>>) planWorkAllSettingItem.get("triggleInfo");
//                        doObjectType = findExcuteType(do_type, currentDay, currentHour, defaultGenerateTime, planTriggleModelList);
//                    } catch (Exception e) {
//                        planTriggleModelList = null;
//                        doObjectType = 0;
//                    }
//                    // -1 : 不执行;
//                    // 1 ：执行，还需判断是否为按读数或者条件执行，这时，获取对象需加入这些条件 ；
//                    // result > 10 : 执行，此时为按数量执行，执行的数量为：result - 10 ；
//                    if (doObjectType <= 0) {
//                        continue;
//                    }
//                    // 执行的对象类型：2：设备；3：点巡检区域；...
//                    int relation_type = planWorkModel.getRelation_type();
//                    float plan_hour = planWorkModel.getPlan_hour();
//                    //工单类型
//                    int work_type_id = planWorkModel.getWork_type_id();
//                    //预计完成需多少天
//                    int pre_day = planWorkModel.getPre_day();
//                    //分配人员配置：1：自动指定;2：不分配;
//                    int allocate_receiver_type = planWorkModel.getAllocate_receiver_type();
//                    //生成工单配置：1：生成请求；2：直接生成工单;
////                    int generate_bill_type = planWorkModel.getRelation_type();      维保计划中这个值已经无意义，是从模板中直接读取过来的  yzj 09-11
//                    int generate_bill_type = 2;
//                    // 工单和流程模板
//                    String flowTemplateCode = planWorkModel.getFlow_template_code();
//                    String workTemplateCode = planWorkModel.getWork_template_code();
//                    String assetOrgName = "";
//                    String planName = planWorkModel.getPlan_name();
//                    String businessName = planWorkModel.getBusiness_name();     //业务名称
//                    int businessTypeId = planWorkModel.getBusiness_type_id();   //业务id
//                    String poolId = planWorkModel.getPool_id();                 //工单池，根据工单池找角色与人
//                    //是执行还是分配，1:执行；2：分配；
//                    //模板表单的定义字段
//                    String bodyProperty = "[]";
//                    String formKey = workflowService.getStartFormKey(schema_name, flowTemplateCode);
//                    String doRoleId = "";           // 执行人角色,保外用户
//                    String doRoleIdInService = "";  // 如果设备时保内用户,则找另外一个角色(总部操作),进行确认
//                    Integer source_type = 4;        // 来源于PM（4）计划
//                    int needInService = 0;
//                    //是否有分配节点，如果有分配节点，需给分配人发送待办:1:有分配节点；0：无分配节点
//                    int haveDistributeNode = 0;
//
//                    //找到工单模板,并处理工单模板中的各项值
//                    MetadataWork formTemplateTareget = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//                    if (null != formTemplateTareget) {
//                        bodyProperty = formTemplateTareget.getBodyProperty();
//                        if (null != bodyProperty && !bodyProperty.isEmpty()) {
//                            //body重新给值
//                            JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
//                            for (int i = 0; i < bodyArray.size(); i++) {
//                                JSONObject rowJson = (JSONObject) bodyArray.get(i);
//                                if (rowJson.get("fieldViewType").equals("9")) {
//                                    String taskItemId = rowJson.get("fieldCode").toString();
//                                    if (null != planWorkModel && null != planWorkModel.getTask_list_word() && !planWorkModel.getTask_list_word().isEmpty()) {
//                                        JSONObject taskListJson = JSONObject.fromObject(planWorkModel.getTask_list_word());
//                                        rowJson.put("taskContent", taskListJson.get(taskItemId));
//                                    }
//                                } else if (rowJson.get("fieldCode").equals("roleIds")) {
//                                    JSONObject rolsObject = (JSONObject) rowJson.get("fieldViewRelationDetail");
//                                    // 执行的角色id
//                                    // 先找 role 键的,如果没有,再找 2 键的
//                                    if (null == rolsObject.get("role") || null == rolsObject.get("role").toString()
//                                            || rolsObject.get("role").toString().isEmpty()) {
//                                        if (null != rolsObject.get("2")) {
//                                            doRoleId = rolsObject.get("2").toString();
//                                            rowJson.put("fieldValue", doRoleId);
//                                        }
//                                    } else {
//                                        JSONObject roleDoTaskObject = (JSONObject) rolsObject.get("role");
//                                        if (null != roleDoTaskObject && null != roleDoTaskObject.get("out_service")) {
//                                            // 默认先给保外用户,
//                                            if (null != roleDoTaskObject.get("out_service")) {
//                                                doRoleId = roleDoTaskObject.get("out_service").toString();
//                                                rowJson.put("fieldValue", doRoleId);
//                                            }
//                                        }
//                                        if (null != roleDoTaskObject && null != roleDoTaskObject.get("in_service")) {
//                                            if (null != roleDoTaskObject.get("in_service")) {
//                                                doRoleIdInService = roleDoTaskObject.get("in_service").toString();
//                                            }
//                                        }
//                                    }
//                                } else if (rowJson.get("fieldCode").equals("user_assign_type")) {
////                                    JSONObject rolsObject = (JSONObject) rowJson.get("fieldViewRelationDetail");
////                                    rowJson.put("fieldValue", rolsObject.get("2"));
//                                    String fieldValue = rowJson.get("fieldValue").toString();
//                                    if (null != fieldValue && !fieldValue.isEmpty()) {
//                                        rowJson.put("fieldValue", fieldValue);
//                                    } else {
//                                        rowJson.put("fieldValue", "workHandleAssign");
//                                    }
//                                } else if (rowJson.get("fieldCode").equals("work_request_type")) {
//                                    String fieldValue = rowJson.get("fieldValue").toString();
//                                    //生成工单配置：1：生成请求；2：直接生成工单;
//                                    generate_bill_type = "confirm".equals(fieldValue) ? 1 : 2;
//                                } else if (rowJson.get("fieldCode").equals("distributionOrExecute")) {
//                                    rowJson.put("fieldValue", "1");
//                                } else if (rowJson.get("fieldCode").equals("in_service")) {
//                                    // 需要进行设备是否在保内判断,找保内用户
//                                    needInService = 1;
//                                } else if (rowJson.get("fieldCode").equals("haveDistributeNode")) {
//                                    String fieldValue = rowJson.get("fieldValue").toString();
//                                    if (StringUtil.isNotEmpty(fieldValue) && RegexUtil.isInteger(fieldValue)) {
//                                        haveDistributeNode = Integer.parseInt(fieldValue);
//                                    }
//                                }
//
//                                // TODO 下一节点不是处理中时，状态、角色等配置信息怎么处理
//                                // 关联字段
//                                if (rowJson.containsKey("fieldViewRelation") && !"".equals(rowJson.get("fieldViewRelation")) && !"0".equals(rowJson.get("fieldViewRelation"))) {
//                                    String fvRelation = rowJson.get("fieldViewRelation").toString();
//                                    String fvrDtl = rowJson.get("fieldViewRelationDetail").toString();
//                                    if ("6".equals(fvRelation)) {
//                                        // 联动
//                                    } else if ("8".equals(fvRelation)) {
//                                        JSONObject jo = JSONObject.fromObject(fvrDtl);
////                                        Object pageViewType = jo.get("pageViewType"); // 平台特殊字段处理类型
////                                        if (null != pageViewType) {
////                                            if ("PM".equals(pageViewType)) {
////                                                continue;
////                                            }
////                                        }
//                                        // 取当前登录用户信息
//                                        if (jo.containsKey("nowUser")) {
//                                            rowJson.put("fieldValue", "system"); // 数据回显
//                                        }
////                                        // 联动【与6相同，6需要合并】
////                                        if (jo.containsKey("linkage")) {
////                                        }
//                                        // 设置默认值
//                                        if (jo.containsKey("default")) {
//                                            JSONObject defaultInfo = JSONObject.fromObject(jo.get("default"));
//                                            if (defaultInfo.containsKey("PM")) {
//                                                rowJson.put("fieldValue", defaultInfo.get("PM"));
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//
//                            bodyProperty = bodyArray.toString();
//                        }
//                    }
//
//                    //读取配置批次的内容，批次中可配置 多条facility的维保计划
//                    List<PlanWorkSettingModel> settingList = (List<PlanWorkSettingModel>) planWorkAllSettingItem.get("planWorkSettingList");
//                    if (null == settingList || settingList.size() <= 0) {
//                        continue;
//                    }
//                    // 遍历配置批次，每次中可能包含多个位置和它们的计划
//                    for (PlanWorkSettingModel settingModel : settingList) {
//                        //读取每次配置的位置
//                        List<PlanWorkFacilityPositionModel> needDoFacility = findExcuteFacilityList(settingModel.getId(), planWorkAllSettingItem);
//                        if (null == needDoFacility || needDoFacility.size() <= 0) {
//                            continue;
//                        }
//                        // 遍历位置
//                        for (PlanWorkFacilityPositionModel facilityModel : needDoFacility) {
//                            try {
//                                // 重新处理一下位置，配置中有可能为所有位置
//                                List<PlanWorkFacilityPositionModel> facilityPositionList = new ArrayList<>();
//                                //查看是否所有位置，如果是所有位置，则需获取所有位置，遍历位置进行获取对象
//                                if (assetOrgPositionType == 2) {
//                                    // 设备组织
//                                    if (facilityModel.getFacility_id() <= 0) {
//                                        List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                                        if (null == facilityList || facilityList.size() <= 0) {
//                                            continue;
//                                        }
//                                        for (Facility facilityItem : facilityList) {
//                                            PlanWorkFacilityPositionModel tempFacilityModel = new PlanWorkFacilityPositionModel();
//                                            tempFacilityModel.setPlan_code(facilityModel.getPlan_code());
//                                            tempFacilityModel.setSetting_id(facilityModel.getSetting_id());
//                                            tempFacilityModel.setIs_all_asset(facilityModel.isIs_all_asset());
//                                            tempFacilityModel.setFacility_id(facilityItem.getId());
//                                            tempFacilityModel.setFacility_no(facilityItem.getFacilityNo());
//                                            tempFacilityModel.setFacility_name(facilityItem.getTitle());
//                                            tempFacilityModel.setEnable_begin_date(null);
//                                            tempFacilityModel.setEnable_end_date(null);
//                                            facilityPositionList.add(tempFacilityModel);
//                                        }
//                                    } else {
//                                        facilityPositionList.add(facilityModel);
//                                    }
//                                } else if (assetOrgPositionType == 1) {
//                                    // 设备位置
//                                    if (null == facilityModel.getPosition_code() || facilityModel.getPosition_code().isEmpty()) {
//                                        List<AssetPosition> positionList = assetPositionService.getAssetPositionAll(schema_name);
//                                        if (null == positionList || positionList.size() <= 0) {
//                                            continue;
//                                        }
//                                        for (AssetPosition positionItem : positionList) {
//                                            PlanWorkFacilityPositionModel tempFacilityModel = new PlanWorkFacilityPositionModel();
//                                            tempFacilityModel.setPlan_code(facilityModel.getPlan_code());
//                                            tempFacilityModel.setSetting_id(facilityModel.getSetting_id());
//                                            tempFacilityModel.setIs_all_asset(facilityModel.isIs_all_asset());
//                                            tempFacilityModel.setPosition_code(positionItem.getPosition_code());
//                                            tempFacilityModel.setPosition_name(positionItem.getPosition_name());
//                                            tempFacilityModel.setFacility_id(facilityModel.getFacility_id());
//                                            tempFacilityModel.setEnable_begin_date(null);
//                                            tempFacilityModel.setEnable_end_date(null);
//                                            facilityPositionList.add(tempFacilityModel);
//                                        }
//
//                                    } else {
//                                        facilityPositionList.add(facilityModel);
//                                    }
//                                }
//
//                                // 不管是一个还是多个，将其统一处理后，再进行遍历，按位置生成工单
//                                for (PlanWorkFacilityPositionModel doAutoFacility : facilityPositionList) {
//                                    //获取对象
//                                    String needDoObjectWord = findExcuteObjectList(schema_name, do_type, settingModel.getId(), doAutoFacility, planTriggleModelList, work_type_id, relation_type, planWorkModel.getIs_have_sub(), pre_day, today, now, isRepairEqualMaintain, isDeadlineTimeOrFinishedTime, isGenerateWhenHavingWork, assetOrgPositionType);
//                                    if (null == needDoObjectWord || needDoObjectWord.isEmpty()) {
//                                        continue;
//                                    }
//                                    if (assetOrgPositionType == 2) {
//                                        assetOrgName = doAutoFacility.getFacility_name();
//                                    } else {
//                                        assetOrgName = doAutoFacility.getPosition_name();
//                                    }
//                                    //计划配置的执行人员，给reveive_account
//                                    String reveiveAccount = "";
//                                    List<Map<String, Object>> executorList = (List<Map<String, Object>>) planWorkAllSettingItem.get("executorList");
//                                    if (null != executorList && executorList.size() > 0) {
//                                        for (int j = 0; j < executorList.size(); j++) {
//                                            if (null == executorList.get(j).get("status")) {
//                                                continue;
//                                            }
//                                            int userStatus = (int) executorList.get(j).get("status");
//                                            if (userStatus > 0) {
//                                                reveiveAccount = null == executorList.get(j).get("account") ? "" : executorList.get(j).get("account").toString();
//                                                break;
//                                            }
//                                        }
//                                    }
//
//                                    List<User> doUserList;
//                                    User user = null;    // 工单下一步执行人
//                                    //有子工单，按子工单执行
//                                    if (2 == planWorkModel.getIs_have_sub()) {
//                                        // 随机选择一个人，根据工单类型、设备类型
//                                        doUserList = findDoTaskUser(schema_name, poolId, doAutoFacility.getFacility_no(), doAutoFacility.getPosition_code(), businessTypeId, doRoleId, generate_bill_type, allocate_receiver_type, 0, assetOrgPositionType, beginDateTime, endDateTime);
//                                        if (null != doUserList && doUserList.size() > 0) {
//                                            int max = doUserList.size();
//                                            int getInt = (int) (new Random().nextFloat() * max);
//                                            user = doUserList.get(getInt);
//                                        }
//                                        if (null != bodyProperty && !bodyProperty.isEmpty()) {
//                                            //body重新给值
//                                            JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
//                                            for (int i = 0; i < bodyArray.size(); i++) {
//                                                JSONObject rowJson = (JSONObject) bodyArray.get(i);
//                                                //子对象给relationId
//                                                if (planWorkModel.getIs_have_sub() == 2) {
//                                                    if (rowJson.get("fieldCode").toString().equals("relation_id") && rowJson.get("dbTableType").toString().equals("1")) {
//                                                        rowJson.put("relationContent", needDoObjectWord);
//                                                        break;
//                                                    }
//                                                }
//                                            }
//
//                                            bodyProperty = bodyArray.toString();
//                                        }
//
//                                        //开始生成工单
//                                        total += generateWorkByPlanDefine(schema_name, doAutoFacility.getFacility_id(), doAutoFacility.getPosition_code(),
//                                                poolId, work_type_id, businessTypeId, assetOrgName, relation_type, needDoObjectWord, workTemplateCode,
//                                                flowTemplateCode, now, pre_day, user, true, bodyProperty, formKey,
//                                                generate_bill_type, allocate_receiver_type, planWorkModel.getPlan_code(), do_type,
//                                                doRoleId, doRoleIdInService, needInService, planTriggleModelList, source_type,
//                                                assetOrgPositionType, planName, businessName, reveiveAccount, haveDistributeNode, generateWorkSleepTime);
//                                    } else {
//                                        //没有子工单，遍历对象，进行生成
//                                        String[] needDoObjectList = needDoObjectWord.split(",");
//                                        if (null == needDoObjectList || needDoObjectList.length <= 0) {
//                                            continue;
//                                        }
//                                        // 设备类型有哪些，按设备类型来分配工单
//                                        Map<String, Integer> assetCategoryHash = new HashMap<>();
//                                        for (String assetItemCollection : needDoObjectList) {
//                                            String[] assetDataItem = assetItemCollection.split("\\|");
//                                            if (assetDataItem.length < 2) {
//                                                continue;
//                                            }
//                                            if (!assetCategoryHash.containsKey(assetDataItem[1])) {
//                                                assetCategoryHash.put(assetDataItem[1], 1);
//                                            } else {
//                                                Integer totalAsset = assetCategoryHash.get(assetDataItem[1]) + 1;
//                                                assetCategoryHash.put(assetDataItem[1], totalAsset);
//                                            }
//                                        }
//
//                                        for (String assetCategoryItem : assetCategoryHash.keySet()) {
//                                            int doUserCount = 0, avgNum = 0, isBigYu = 0, assetIndex = 0;
//                                            int categoryId = Integer.parseInt(assetCategoryItem);
//                                            int totalAsset = assetCategoryHash.get(assetCategoryItem);
//                                            // 这时获取对象中，如果设备类型为多个，则需按设备类型，去获取人员
//                                            doUserList = findDoTaskUser(schema_name, poolId, doAutoFacility.getFacility_no(), doAutoFacility.getPosition_code(), businessTypeId, doRoleId, generate_bill_type, allocate_receiver_type, categoryId, assetOrgPositionType, beginDateTime, endDateTime);
//                                            //allocate_receiver_type分配人员配置：1：自动指定;2：不分配; generate_bill_type生成工单配置：1：生成请求；2：直接生成工单;
//                                            if (allocate_receiver_type == 1 && generate_bill_type == 2) {
//                                                if (null == doUserList || doUserList.size() <= 0) {
//                                                    //降级为分配工单
////                                                    allocate_receiver_type = 2;
//                                                    doUserList = findDoTaskUser(schema_name, poolId, doAutoFacility.getFacility_no(), doAutoFacility.getPosition_code(), businessTypeId, doRoleId, generate_bill_type, 2, categoryId, assetOrgPositionType, beginDateTime, endDateTime);
//                                                }
//                                            }
//                                            if (allocate_receiver_type == 1 && generate_bill_type == 2) {
//                                                if (doUserList != null && doUserList.size() > 0) {
//                                                    doUserCount = doUserList.size();
//                                                    avgNum = totalAsset / doUserList.size();
//                                                    isBigYu = totalAsset % doUserList.size();     //是否有余
//                                                }
//                                            } else {
//                                                // 分配工单、或者审核工单，取默认第一个人
//                                                if (doUserList != null && doUserList.size() > 0) {
//                                                    user = doUserList.get(0);
//                                                }
//                                            }
//
//                                            for (String objectCode : needDoObjectList) {
//                                                String[] assetDataItem = objectCode.split("\\|");
//                                                if (assetDataItem.length > 1 && categoryId != Integer.parseInt(assetDataItem[1])) {
//                                                    continue;
//                                                }
//                                                assetIndex++;
//                                                if (allocate_receiver_type == 1 && generate_bill_type == 2) {
//                                                    if (avgNum != 0 && assetIndex / avgNum < doUserCount) {
//                                                        // 能整除的部分平均分配
//                                                        user = doUserList.get(assetIndex / avgNum);
//                                                    } else {
//                                                        // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
//                                                        int max = doUserList.size();
//                                                        int currentIndex = 0;
//                                                        if (isBigYu > 0) {
//                                                            //随机取，无法避免同一次，取值重复问题
//                                                            currentIndex = (int) (new Random().nextFloat() * max);
//                                                        }
//                                                        user = doUserList.get(currentIndex);
//                                                        //把集合中，已经选择的用户排斥掉
//                                                        doUserList.remove(currentIndex);
//                                                    }
//                                                }
//                                                //开始生成工单
//                                                total += generateWorkByPlanDefine(schema_name, doAutoFacility.getFacility_id(), doAutoFacility.getPosition_code(),
//                                                        poolId, work_type_id, businessTypeId, assetOrgName, relation_type, assetDataItem[0], workTemplateCode,
//                                                        flowTemplateCode, now, pre_day, user, false, bodyProperty, formKey,
//                                                        generate_bill_type, allocate_receiver_type, planWorkModel.getPlan_code(), do_type,
//                                                        doRoleId, doRoleIdInService, needInService, planTriggleModelList, source_type,
//                                                        assetOrgPositionType, planName, businessName, reveiveAccount, haveDistributeNode, generateWorkSleepTime);
//                                            }
//                                        }
//                                    }
//                                }
//                            } catch (Exception e) {
//                                continue;
//                            }
//                        }
//                    }
//                }
//            }
//
//            logService.AddLog(schema_name, "system", "work_auto", selectOptionService.getLanguageInfoWithoutRequest(schema_name, LangConstant.MSG_AUTOM_NUM) + total, "system");//工单自动生成总数
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "work_auto", selectOptionService.getLanguageInfoWithoutRequest(schema_name, LangConstant.MSG_AUTOM_SER_ERROR) + e.getMessage(), "system");//工单自动生成服务出错
//        }
//    }
//
//    // 查看计划是否执行，以及执行的类型（不按小时执行的，统一规定 6 点执行）:
//    // -1 : 不执行;
//    // 1 ：执行；
//    // do_type : 触发条件：1：所有条件满足执行；2：任一条件满足即执行；
//    private int findExcuteType(int doType, int currentDay, int currentHour, int defaultGenerateTime, List<Map<String, Object>> planWorkTriggle) {
//        int doObjectType = -1;
//        try {
//            if (null == planWorkTriggle || planWorkTriggle.size() <= 0) {
//                return doObjectType;
//            }
//            if (defaultGenerateTime < 0) {
//                //默认每天7点，生成任务
//                defaultGenerateTime = 7;
//            }
//            boolean isSettingExecuteJudge;
//            int plan_type;
//            int period_type;
//            float period_value;
//            String execute_time;
//            for (int i = 0; i < planWorkTriggle.size(); i++) {
//                isSettingExecuteJudge = false;
//                // 条件类型：1：按时间；2：按读数；3：按条件；
//                switch (Integer.parseInt(planWorkTriggle.get(i).get("triggle_type").toString())) {
//                    //按时间
//                    case 1:
//                        // 计划类型：1：自动计划;2：人工计划;
//                        plan_type = Integer.parseInt(planWorkTriggle.get(i).get("plan_type").toString());
//                        // 周期类型（A、自动计划：只有按数量、天、周、月、年；B、人工计划：按小时（可多选）；天：每多少天做一次，不设那一天;
//                        // 月：可定哪一天做（不可多选）；）： 1：按数量；2：按小时；3：按天；4：按月；5:单次执行
//                        period_type = Integer.parseInt(planWorkTriggle.get(i).get("period_type").toString());
//                        // 周期值（条数、天数、月数等）
//                        period_value = Float.parseFloat(planWorkTriggle.get(i).get("period_value").toString());
//                        // 执行时间（多个以逗号隔开），存放按小时执行时选择的小时（0~23）
//                        execute_time = (String) planWorkTriggle.get(i).get("execute_time");
//                        if (1 == plan_type) {
//                            //自动计划，按system_confign点执行
//                            isSettingExecuteJudge = currentHour == defaultGenerateTime ? true : false;
//                        } else {
//                            //人工计划
//                            if (period_type == 2) {
//                                // 按小时执行
//                                String[] hoursWord = execute_time.split(",");
//                                if (null != hoursWord && hoursWord.length > 0) {
//                                    for (String hours : hoursWord) {
//                                        if (currentHour == Integer.parseInt(hours)) {
//                                            isSettingExecuteJudge = true;
//                                            break;
//                                        }
//                                    }
//                                }
//                            } else if (4 == period_type) {
//                                // 按月执行
//                                isSettingExecuteJudge = currentDay == (int) period_value ? true : false;
//                                isSettingExecuteJudge = (isSettingExecuteJudge && currentHour == defaultGenerateTime) ? true : false;
//                            } else {
//                                // 按天，默认为上午6点执行
//                                isSettingExecuteJudge = currentHour == defaultGenerateTime ? true : false;
//                            }
//                        }
//                        break;
//                    //按读数
//                    case 2:
//                        isSettingExecuteJudge = true;
//                        break;
//                    //按条件
//                    case 3:
//                        isSettingExecuteJudge = true;
//                        break;
//                }
//
//                //根据条件是全部还是任意，判断是否需要执行
//                if (doType == 2) {
//                    //任一条件满足即执行,需要执行，立刻执行
//                    if (isSettingExecuteJudge) {
//                        doObjectType = 1;
//                        break;
//                    }
//                } else {
//                    //所有条件满足执行,需把所有的setting条件循环完，再看执行不执行
//                    if (isSettingExecuteJudge) {
//                        doObjectType = 1;
//                        continue;
//                    } else {
//                        doObjectType = -1;
//                        break;
//                    }
//                }
//            }
//        } catch (Exception e) {
//            doObjectType = -1;
//        }
//        return doObjectType;
//    }
//
//    // 计划执行的位置，需传入配置setting id
//    private List<PlanWorkFacilityPositionModel> findExcuteFacilityList(long settingId, Map<String, Object> planWorkAllSettingItem) {
//        List<PlanWorkFacilityPositionModel> result = new ArrayList<>();
//        try {
//            if (null == planWorkAllSettingItem) {
//                return result;
//            }
//            List<PlanWorkFacilityPositionModel> list = (List<PlanWorkFacilityPositionModel>) planWorkAllSettingItem.get("positionList");
//            if (null == list || list.size() <= 0) {
//                return result;
//            }
//            for (PlanWorkFacilityPositionModel targetItem : list) {
//                if (targetItem.getSetting_id() == settingId) {
//                    result.add(targetItem);
//                }
//            }
//        } catch (Exception e) {
//            result = null;
//        }
//        return result;
//    }
//
//    // 计划执行的对象，需传入位置，settingid（配置批次）
//    // do_type : 触发条件：1：所有条件满足执行；2：任一条件满足即执行；
//    // pre_day ： 工单提前天数
//    // planTriggleModelList : 计划执行的条件
//    // relationType ： 计划执行的类型：2：设备；3：点巡检区域；...
//    // isRepairEqualMaintain ： 维修是否类同保养，即维修后，该周期保养自动延后：1：类同；2：不类同
//    // isDeadlineTimeOrFinishedTime : PM生成时，触发时间是完成时间还是截止时间，1：截止时间；2：完成时间；
//    // isGenerateWhenHavingWork : PM生成时，如果有维修(依赖保养计划，维修是否同保养)或保养工单，是否还生成，1：生成；2：不生成；
//    //assetOrgPositionType : 设备组织类型是设备组织，还是设备位置，默认是设备位置:["设备位置": "1";"设备组织+位置": "2"]
//    private String findExcuteObjectList(String schema, int do_type, long settingId, PlanWorkFacilityPositionModel facilityModel, List<Map<String, Object>> planTriggleModelList, int work_type, int relationType, int isHaveSub, int pre_day, long today, Timestamp now, int isRepairEqualMaintain, int isDeadlineTimeOrFinishedTime, int isGenerateWhenHavingWork, int assetOrgPositionType) {
//        try {
//            // 维修是否类同保养，即维修后，该周期保养自动延后   yzj  2019-09-21
//            String work_type_id = (isRepairEqualMaintain == 1 && work_type < 3) ? "1,2" : String.valueOf(work_type);
//            String deadlineTimeOrFinishedTime = isDeadlineTimeOrFinishedTime == 1 ? "ww.deadline_time" : "wd.finished_time";
//            String generateObjScaleWord = isGenerateWhenHavingWork == 1 ? "" : " and att._id not in " +
//                    "(select relation_id from " + schema + "._sc_works_detail where status<60 or status = 90 and work_type_id in (" + work_type_id + ") ) " +
//                    "and att._id not in (select relation_id from " + schema + "._sc_work_request where status<60 and work_type_id in (" + work_type_id + ") ) ";
//
//            String result = "";
//            if (null == facilityModel || null == planTriggleModelList) {
//                return result;
//            }
//            // 触发条件类型，生成sql
//            boolean is_all_asset = facilityModel.isIs_all_asset();
//            int totalExcuteObjectCount = 0;
//            int needCount = 0;
//
//            // 1、遍历出配置的设备读数、设备条件，用于查询需执行的全部设备
//            String joinTag = do_type == 1 ? "and" : "or";
//            // 设备使用日期的条件,备件，设备监控的个数，如果为全部满足，则需所有的满足条件的监控项目的值 >= monitorConditionCount
//            String enableTimeAndMonitorAndBomCountCondition = do_type == 1 ? " and ( 1=1 " : "and ( 1=0 ";
//            // 备件，设备监控的条件
//            String triggleBomCondition = do_type == 1 ? " 1=1 " : " 1=0 ";
//            String triggleMonitorCondition = do_type == 1 ? " 1=1 " : " 1=0 ";
//            // 设备监控计算总数,判断是所有都满足，还是只需一种满足
//            int calculateMonitorCount = 0;
//            // 备件的计算总数,判断是所有都满足，还是只需一种满足
//            int calculateBomCount = 0;
//            // 是否有设备使用时长的条件
//            boolean isHaveDeviceUseDay = false;
//
//            SimpleDateFormat currentYearFormat = new SimpleDateFormat(Constants.DATE_FMT);
//            for (int i = 0; i < planTriggleModelList.size(); i++) {
//                int triggle_type = Integer.parseInt(planTriggleModelList.get(i).get("triggle_type").toString());
//                if (triggle_type == 1) {
//                    // 1：按计划
//                    continue;
//                }
//                String column_key = null == planTriggleModelList.get(i).get("column_key") ? "" : (String) planTriggleModelList.get(i).get("column_key");
//                String column_value = null == planTriggleModelList.get(i).get("column_value") ? "" : (String) planTriggleModelList.get(i).get("column_value");
//                //column_condition:判断条件：1：<=; 2：==; 3：>=; 4：like;
//                String column_condition = null == planTriggleModelList.get(i).get("column_condition") ? "" : (String) planTriggleModelList.get(i).get("column_condition");
//                if (null == column_key || column_key.isEmpty() || null == column_value || column_value.isEmpty()) {
//                    continue;
//                }
//                if (triggle_type == 2) {
//                    // 2：按读数； 每多少单位，执行一次
//                    if (column_key.indexOf("bom_sens_") == 0) {
//                        // 为备件
//                        //" （备件的最新替换时间非空 且 当前时间-最新替换时间 >= " + column_value + ")
//                        // 且 ( 备件的记录更新时间（记录在triggle_record中）为空 或 当前时间 - 备件的记录更新时间（记录在triggle_record中）>= " + column_value + " ) "
//                        triggleBomCondition += joinTag + " (bom_cur.bom_model_id_key='" + column_key +
//                                "' and bom_cur.current_bom_time is not null and date_part('day',now()-bom_cur.current_bom_time) >= " + column_value +
//                                " and (bom_recd.record_bom_time is null or date_part('day',bom_recd.record_bom_time::TIMESTAMP-bom_cur.current_bom_time) >=" + column_value + " ) ) ";
//                        // 备件参与条件的个数
//                        calculateBomCount = do_type == 1 ? calculateBomCount + 1 : 1;
//                    } else if (column_key.indexOf("enable_time") == 0) {
//                        isHaveDeviceUseDay = true;
//                        // 为设备启用日期
//                        //" （设备的最新维修时间非空 且 当前时间-最新维修时间 >= " + column_value + ")
//                        // 且 ( 设备的记录维修时间（记录在triggle_record中）为空 或 当前时间 - 设备的记录维修时间（记录在triggle_record中）>= " + column_value + " )
//                        enableTimeAndMonitorAndBomCountCondition += joinTag + " (w.deadline_time is not null and date_part('day',now()-w.deadline_time) >= "
//                                + column_value +
//                                " and (ent_ast.current_enable_time is null or date_part('day',now()-ent_ast.current_enable_time::TIMESTAMP) >= "
//                                + column_value + " ) ) ";
//                    } else {
//                        triggleMonitorCondition += joinTag + " (attmt.monitor_key='" + column_key + "' and " +
//                                "( tr.current_value is null or " +
//                                " case " + schema + ".isnumeric(attmt.monitor_value) " +
//                                " when true then  " +
//                                "( attmt.monitor_value::FLOAT - tr.current_value::FLOAT ) >= " + column_value + "" +
//                                " else " +
//                                "( date_part('day',attmt.monitor_value::TIMESTAMP-tr.current_value::TIMESTAMP) ) >= " + column_value + "" +
//                                " end " +
//                                ") " +
//                                ") ";
//                        // 设备监控参与条件的个数
//                        calculateMonitorCount = do_type == 1 ? calculateMonitorCount + 1 : 1;
//                    }
//                } else if (triggle_type == 3) {
//                    // 判断条件
//                    String column_condition_word = " = ";
//                    String column_condition_word_reverse = " != ";
//                    switch (column_condition) {
//                        case "1":
//                            column_condition_word = " >= ";
//                            column_condition_word_reverse = " <= ";
//                            break;
//                        case "3":
//                            column_condition_word = " <= ";
//                            column_condition_word_reverse = " >= ";
//                            break;
//                    }
//
//                    // 3：按条件； 大于/小于/等于多少，执行一遍
//                    if (column_key.indexOf("bom_sens_") == 0) {
//                        //为备件
//                        triggleBomCondition += joinTag + " (bom_cur.bom_model_id_key='" + column_key +
//                                "' and bom_cur.current_bom_time is not null and date_part('day',now()-bom_cur.current_bom_time) "
//                                + column_condition_word + " " + column_value +
//                                " and (bom_recd.record_bom_time is null or date_part('day',bom_recd.record_bom_time::TIMESTAMP-bom_cur.current_bom_time) "
//                                + column_condition_word_reverse + " " + column_value + " ) ) ";
//
//                        // 备件参与条件的个数
//                        calculateBomCount = do_type == 1 ? calculateBomCount + 1 : 1;
//                    } else if ("enable_time".equals(column_key)) {
//                        isHaveDeviceUseDay = true;
//                        //为设备启用日期
//                        enableTimeAndMonitorAndBomCountCondition += joinTag + " (att.enable_time is not null and date_part('day',now()-att.enable_time) "
//                                + column_condition_word + " " + column_value +
//                                " and (ent_ast.current_enable_time is null or date_part('day',ent_ast.current_enable_time::TIMESTAMP-att.enable_time) "
//                                + column_condition_word_reverse + " " + column_value + " ) ) ";
//                    } else {
//                        triggleMonitorCondition += joinTag + " (attmt.monitor_key='" + column_key + "' and " +
//                                "(" +
//                                " case " + schema + ".isnumeric(attmt.monitor_value) " +
//                                "when true then " +
//                                "attmt.monitor_value::FLOAT " + column_condition_word + " " + column_value + " " +
//                                " else " +
//                                "attmt.monitor_value::varchar " + column_condition_word + " '" + column_value + "' " +
//                                " end " +
//                                "and " +
//                                "( tr.current_value is null or " +
//                                " case " + schema + ".isnumeric(tr.current_value) " +
//                                "when true then " +
//                                "tr.current_value::FLOAT " + column_condition_word_reverse + " " + column_value + " " +
//                                " else " +
//                                "tr.current_value::varchar " + column_condition_word_reverse + " '" + column_value + "' " +
//                                " end " +
//                                ")" +
//                                ") " +
//                                ") ";
//                        // 设备监控参与条件的个数
//                        calculateMonitorCount = do_type == 1 ? calculateMonitorCount + 1 : 1;
//                    }
//                }
//            }
//
//            // 看看备件和监控数量是否有,以及设备使用时长是否有
//            if (calculateBomCount > 0) {
//                enableTimeAndMonitorAndBomCountCondition += joinTag + " bom_cunt.bom_count >=" + calculateBomCount + " ";
//            } else {
//                // 没有备件条件
//                triggleBomCondition = " 1=0 ";
//            }
//            if (calculateMonitorCount > 0) {
//                enableTimeAndMonitorAndBomCountCondition += joinTag + " mnt_count.monitor_count >=" + calculateMonitorCount + " ";
//            } else {
//                // 没有监控条件
//                triggleMonitorCondition = " 1=0 ";
//            }
//            if (!isHaveDeviceUseDay && calculateBomCount == 0 && calculateMonitorCount == 0) {
//                enableTimeAndMonitorAndBomCountCondition = " ";
//            } else {
//                enableTimeAndMonitorAndBomCountCondition += ") ";
//            }
//
//
//            // 2、查出所有的需执行的设备，或点巡检区域对象的总数，用于自动任务时，平均分配，得到每次生成多少条
//            // 设备类型、型号、启用日期条件
//            String assetSettingCondition = "";
//            // 如果是设备组织，则按设备组织查询，否则按设备位置查询
//            String facilityOrPositionCondition = "and f.id=" + facilityModel.getFacility_id();
//            if (assetOrgPositionType == 1) {
//                facilityOrPositionCondition = "and att.position_code='" + facilityModel.getPosition_code() + "' ";
//            }
//
//            if (relationType == 3) {
//                // 获取点巡检区域待处理的总数
//                totalExcuteObjectCount = planWorkService.countNeedDoPlanInspectionByFacilityAndSettingId(schema, settingId, facilityOrPositionCondition, work_type);
//            } else {
//                // 获取设备的总数，需按设备类型、设备型号、设备启用日期条件进行查询
//                if (null != facilityModel.getEnable_begin_date()) {
//                    //获取年份
//                    int currentYear = Integer.parseInt(currentYearFormat.format(facilityModel.getEnable_begin_date()));
//                    if (currentYear > 1970) {
//                        assetSettingCondition += " and att.enable_time >='" + facilityModel.getEnable_begin_date() + "' ";
//                    }
//                }
//                if (null != facilityModel.getEnable_end_date()) {
//                    //获取年份
//                    int currentYear = Integer.parseInt(currentYearFormat.format(facilityModel.getEnable_end_date()));
//                    if (currentYear > 1970) {
//                        assetSettingCondition += " and att.enable_time <='" + facilityModel.getEnable_end_date() + "' ";
//                    }
//                }
//                // 配置的设备类型
//                String assetSettingCategory = planWorkService.findPlanWorkAssetTypeByPlanCodeAndSettingId(schema, settingId);
//                // 配置的设备型号
//                String assetSettingModel = planWorkService.findPlanWorkAssetModelByPlanCodeAndSettingId(schema, settingId);
//                if (null != assetSettingCategory && !assetSettingCategory.isEmpty()) {
//                    assetSettingCondition += " and att.category_id in (" + assetSettingCategory + ") ";
//                }
//                if (null != assetSettingModel && !assetSettingModel.isEmpty()) {
//                    assetSettingCondition += " and att.asset_model_id in (" + assetSettingModel + ") ";
//                }
//
//                if (is_all_asset) {
//                    // 配置的是所有设备
//                    totalExcuteObjectCount = planWorkService.countNeedDoPlanAllAssetByFacilityAndSettingId(schema, facilityOrPositionCondition, work_type_id, deadlineTimeOrFinishedTime, assetSettingCondition, triggleMonitorCondition, triggleBomCondition, enableTimeAndMonitorAndBomCountCondition);
//                } else {
//                    // 配置的是部分设备
//                    totalExcuteObjectCount = planWorkService.countNeedDoPlanSettingAssetByFacilityAndSettingId(schema, settingId, facilityOrPositionCondition, work_type_id, deadlineTimeOrFinishedTime, assetSettingCondition, triggleMonitorCondition, triggleBomCondition, enableTimeAndMonitorAndBomCountCondition);
//                }
//            }
//
//            //没有需要执行的设备，返回
//            if (totalExcuteObjectCount <= 0) {
//                return result;
//            }
//
//            // 3、查出当前时刻需要执行的设备的条件，准备生成对象
//            // 获取设备的条件，逻辑较复杂
//            String getDoWorkCondition = do_type == 1 ? " and ( 1=1 " : " and ( 1=0 ";
//            String getDoAssetOrInspectTriggleLimit = "";
//            boolean isHaveWokrCondition = false;
//            List<Map<String, Object>> list;
//            for (int i = 0; i < planTriggleModelList.size(); i++) {
//                int triggle_type = Integer.parseInt(planTriggleModelList.get(i).get("triggle_type").toString());
//                // 条件类型：1：按时间；2：按读数；3：按条件；
//                if (triggle_type == 1) {
//                    // 计划类型：1：自动计划;2：人工计划;
//                    int plan_type = Integer.parseInt(planTriggleModelList.get(i).get("plan_type").toString());
//                    // 周期类型（A、自动计划：只有按数量、天、周、月、年；B、人工计划：按小时（可多选）；天：每多少天做一次，不设那一天;
//                    // 月：可定哪一天做（不可多选）；）： 1：按数量；2：按小时；3：按天；4：按月；5：单次执行；
//                    int period_type = Integer.parseInt(planTriggleModelList.get(i).get("period_type").toString());
//                    // 周期值（条数、天数、月数等）
//                    float period_value = Float.parseFloat(planTriggleModelList.get(i).get("period_value").toString());
//                    if (period_type == 1) {
//                        // 按数量
//                        getDoAssetOrInspectTriggleLimit = " limit " + (int) period_value;
//                    } else if (2 == period_type) {
//                        // 按小时，批量任务，一次性生成
//                        if (pre_day * 24 < period_value) {
//                            getDoWorkCondition += joinTag + " (w.deadline_time is null or date_part('hour',now()-w.deadline_time)>" +
//                                    (period_value - pre_day * 24) + " ) ";
//                            isHaveWokrCondition = true;
//                        }
//                    } else if (3 == period_type) {
//                        // 按天执行
//                        if (pre_day < period_value) {
//                            getDoWorkCondition += joinTag + " (w.deadline_time is null or date_part('day',now()-w.deadline_time)>" +
//                                    (period_value - pre_day) + " ) ";
//                            isHaveWokrCondition = true;
//                        }
//                        if (1 == plan_type && totalExcuteObjectCount > 0 && period_value > 0) {
//                            //自动计划，需选择每天生成的条数
//                            needCount = (totalExcuteObjectCount % ((int) period_value) != 0) ? totalExcuteObjectCount / ((int) period_value) + 1 : totalExcuteObjectCount / ((int) period_value);
//                            getDoAssetOrInspectTriggleLimit = " limit " + needCount;
//                        }
//                    } else if (4 == period_type) {
//                        // 今天前几个月的那一天，距离今天相隔多少天
//                        Calendar calendar = new GregorianCalendar();
//                        calendar.setTime(now);
//                        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - (int) period_value);
//                        Date date = calendar.getTime();
//                        Timestamp monthBefore = new Timestamp(date.getTime());
//                        int days = (int) ((today - monthBefore.getTime()) / (1000 * 3600 * 24));
//                        // 按月执行
//                        if (pre_day < days) {
//                            getDoWorkCondition += joinTag + " (w.deadline_time is null or date_part('day',now()-w.deadline_time)>" +
//                                    (days - pre_day) + " ) ";
//                            isHaveWokrCondition = true;
//                        }
//                        if (1 == plan_type && totalExcuteObjectCount > 0 && days > 0) {
//                            //自动计划，需选择每天生成的条数
//                            needCount = (totalExcuteObjectCount % days != 0) ? totalExcuteObjectCount / days + 1 : totalExcuteObjectCount / days;
//                            getDoAssetOrInspectTriggleLimit = " limit " + needCount;
//                        }
//                    } else if (5 == period_type) {
//                        // 开始日期，针对 period_type==5
//                        String begin_date = null == planTriggleModelList.get(i).get("begin_time") ? "" : (String) planTriggleModelList.get(i).get("begin_time");
//                        Date nowDate = new Date();
//                        //获取当前日期
//                        SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//                        Calendar calendar = Calendar.getInstance();
//                        calendar.setTime(nowDate);
//                        calendar.add(Calendar.DAY_OF_YEAR, pre_day);
//                        Date beforeDate = calendar.getTime();
//                        String beforeDateWord = simleDateFormat.format(beforeDate);
//                        if (!beforeDateWord.equals(begin_date)) {
//                            isHaveWokrCondition = true;
//                            getDoWorkCondition += joinTag + " 1=-1 ";
//                        }
//                    }
//
//                } else {
//                    getDoWorkCondition += joinTag + " 1=1 ";
//                    isHaveWokrCondition = true;
//                }
//            }
//
//            getDoWorkCondition = isHaveWokrCondition ? getDoWorkCondition + " ) " : "";
//
//            // 4、开始生成执行对象
//            if (relationType == 3) {
//                // 获取点巡检区域
//                list = planWorkService.findNeedDoPlanInspectionByFacilityAndSettingId(schema, settingId, facilityOrPositionCondition, work_type, deadlineTimeOrFinishedTime, getDoWorkCondition, getDoAssetOrInspectTriggleLimit);
//            } else {
//                // 获取设备
//                if (is_all_asset) {
//                    //配置的是所有设备
//                    list = planWorkService.findNeedDoPlanAllAssetByFacilityAndSettingId(schema, facilityOrPositionCondition, work_type_id, deadlineTimeOrFinishedTime, assetSettingCondition, triggleMonitorCondition, triggleBomCondition, enableTimeAndMonitorAndBomCountCondition, getDoWorkCondition, generateObjScaleWord, getDoAssetOrInspectTriggleLimit);
//                } else {
//                    //配置的是部分设备
//                    list = planWorkService.findNeedDoPlanAssetByFacilityAndSettingId(schema, settingId, facilityOrPositionCondition, work_type_id, deadlineTimeOrFinishedTime, assetSettingCondition, triggleMonitorCondition, triggleBomCondition, enableTimeAndMonitorAndBomCountCondition, getDoWorkCondition, generateObjScaleWord, getDoAssetOrInspectTriggleLimit);
//                }
//            }
//            if (null == list || list.size() <= 0) {
//                return result;
//            }
//            //拼成一个字符串，返回
//            if (isHaveSub == 2) {
//                //有子工单
//                result = JSON.toJSONString(list);
//            } else {
//                for (int i = 0; i < list.size(); i++) {
//                    if (relationType == 3) {
//                        result += list.get(i).get("inspection_code") + "|0,";
//                    } else {
//                        result += list.get(i).get("asset_id") + "|" + list.get(i).get("category_id") + ",";
//                    }
//                }
//                if (null != result && !result.isEmpty()) {
//                    result = result.substring(0, result.length() - 1);
//                }
//            }
//            return result;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    //根据工单池、位置、工单类型、工单生成条件（请求或工单，分配还是不分配），设备类型，查找人员
//    //generate_bill_type:生成工单配置：1：生成请求；2：直接生成工单;
//    //allocate_receiver_type 分配人员配置：1：自动指定;2：不分配负责人;
//    //roleId : 找工单请求的确认人员，role为固定值，需从流程处读取
//    //assetOrgPositionType : 设备组织类型是设备组织，还是设备位置，默认是设备位置:["设备位置": "1";"设备组织+位置": "2"]
//    private List<User> findDoTaskUser(String schema_name, String pool_id, String doFacilityNO, String positionCode, int businessTypeId, String roleId, int generate_bill_type, int allocate_receiver_type, int asset_category_id, int assetOrgPositionType, Timestamp beginDateTime, Timestamp endDateTime) {
//        List<User> doUserList;
//        try {
//            if (assetOrgPositionType == 2) {
//                if (generate_bill_type == 1) {
//                    doUserList = workScheduleService.getDoTaskUserByWorkTypeRoleIds(schema_name, doFacilityNO, pool_id, businessTypeId, beginDateTime, endDateTime, roleId, asset_category_id);
//                } else {
//                    int distributionOrExecute = 1;  // 1：执行；2：分配；
//                    //找工单的执行人员，或分配人员
//                    if (allocate_receiver_type == 1) {
//                        doUserList = workScheduleService.getDoTaskUserByWorkTypeRoleIds(schema_name, doFacilityNO, pool_id, businessTypeId, beginDateTime, endDateTime, roleId, asset_category_id);
//
//                        // yzj，2020-02-19，替换此方法，改由roleid（工单模板中配置）找到执行人（配置中含执行角色，分配人等）
////                    doUserList = workScheduleService.getDoTaskUserByWorkTypePermission(schema_name, doFacilityNO, pool_id, businessTypeId, beginDateTime, endDateTime, distributionOrExecute, null, asset_category_id);
////                    if (null == doUserList || doUserList.size() <= 0) {
////                        distributionOrExecute = 2;
////                        doUserList = workScheduleService.getDoTaskUserByWorkTypePermission(schema_name, doFacilityNO, pool_id, businessTypeId, beginDateTime, endDateTime, distributionOrExecute, null, asset_category_id);
////                    }
//                    } else {
//                        distributionOrExecute = 2;
//                        // 找工单的分配人员   // yzj，2020-02-19，可能也可以直接通过角色来找分配人员，暂不修改
//                        doUserList = workScheduleService.getDoTaskUserByWorkTypePermission(schema_name, doFacilityNO, pool_id, businessTypeId, beginDateTime, endDateTime, distributionOrExecute, null, asset_category_id);
//                    }
//                }
//            } else {
//                if (generate_bill_type == 2 && allocate_receiver_type == 2) {
//                    // 生成工单，且需分配时，按权限键值，找人
//                    doUserList = workScheduleService.getExecutorByWorkTypeAndPermission(schema_name, positionCode, pool_id, businessTypeId, beginDateTime, endDateTime, allocate_receiver_type, null, asset_category_id);
//                } else {
//                    // 生成请求，或生成工单，需指定负责人时，按角色找人
//                    doUserList = workScheduleService.getExecutorByWorkTypeAndRoleIds(schema_name, positionCode, pool_id, businessTypeId, beginDateTime, endDateTime, roleId, asset_category_id);
//                }
//            }
//        } catch (Exception e) {
//            doUserList = new ArrayList<>();
//        }
//        return doUserList;
//    }
//
//    //按生成工单，按工单类型，生成类型等，生成工单
//    //distributionOrExecute: 1:执行；2：分配；
//    //workTypeId: 工单类型；
//    //businessId: 业务类型：2：保养；3：巡检；4：点检；
//    //generate_bill_type:生成工单配置：1：生成请求；2：直接生成工单;
//    //allocate_receiver_type 分配人员配置：1：自动指定;2：不分配;
//    //do_type : 触发条件：1：所有条件满足执行；2：任一条件满足即执行；
//    //roleIds : 不处于保内时的找人角色；
//    //doRoleIdInService : 保内时,找的角色；
//    //needInService: 是否需要保内,保外判断
//    //assetOrgPositionType: 设备组织类型是设备组织，还是设备位置，默认是设备位置:["设备位置": "1";"设备组织+位置": "2"]
//    //reveiveAccountInPlan: PM 中，如果配置了执行人，
//    //haveDistributeNode: 是否有分配节点，如果有分配节点，需给分配人发送待办:1:有分配节点；0：无分配节点
//    private int generateWorkByPlanDefine(String schema, int facilityId, String positionCode, String poolId, int workTypeId, int businessId,
//                                         String AssetOrgName, int relationType, String relationIds, String workTemplateCode,
//                                         String flowTemplateCode, Timestamp now, int earlyDays, User user, boolean isHaveSub,
//                                         String bodyProperty, String formKey, int generate_bill_type, int allocate_receiver_type,
//                                         String plan_code, int do_type, String roleIds, String doRoleIdInService, int needInService,
//                                         List<Map<String, Object>> planTriggleModelList, Integer source_type, int assetOrgPositionType,
//                                         String planName, String businessName, String reveiveAccountInPlan, int haveDistributeNode,
//                                         int generateWorkSleepTime) {
//        try {
//            //表单无，则没法生成任务，返回
//            if (formKey == null || formKey.isEmpty()) {
//                return 0;
//            }
//            int result = 0;
//            DateFormat timeFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            Long endTime = now.getTime() + earlyDays * (1000 * 3600 * 24);
//            Timestamp deadlineTime = new Timestamp(endTime);
//            String nowDateWord = timeFormat.format(now);
//            String deadlineTimeString = timeFormat.format(deadlineTime);
//            String receiveAccount = null;
//            String businessWorkCode = "";    // 用于日志记录
//            String msgCode = null; // 消息编码
//            if (null == poolId || poolId.isEmpty()) {
//                poolId = "-1";
//            }
//            String title = "";
//            //获取工单执行对象的名称
//            String relationName = "";
//            if (!isHaveSub) {
//                if (2 == relationType) {
//                    Map<String, Object> data = assetInfoService.getAssetSimplyById(schema, relationIds);
//                    if (null != data) {
//                        relationName = data.get("strname").toString() + "(" + data.get("strcode").toString() + ")";
//                    }
//                }
//            }
//            //发送流程
//            Map map = new HashMap();
//            String workRequestCode = dynamicCommonService.getWorkRequestCode(schema);
//            if (generate_bill_type == 1) {
//                //保存工单请求表,并生成流程
//                businessWorkCode = workRequestCode;
//                title = AssetOrgName + relationName + ",执行" + planName + "";
//                if (title.length() > 50) {
//                    title = title.substring(0, 49);
//                }
//                result = workSheetHandleService.insertWorkRequest(schema, workRequestCode, workTypeId, workTemplateCode,
//                        facilityId, positionCode, relationType, relationIds, title, bodyProperty, AssetOrgName + businessName + ",根据计划:" + planName + "生成",
//                        now, deadlineTime, StatusConstant.TO_BE_CONFIRM, "", now, "system", plan_code, source_type);
//                receiveAccount = (null == user || null == user.getAccount() || user.getAccount().isEmpty()) ? null : user.getAccount();
//
//                map.put("status", String.valueOf(StatusConstant.PENDING));
//                map.put("sub_work_code", workRequestCode);
//                map.put("work_request_type", "confirm");
//                if (null != user) {
//                    map.put("receive_account", user.getAccount());
//                    msgCode = qcloudsmsConfig.SMS_10003001;
//                } else {
//                    map.put("receive_account", null);
//                }
//            } else {
//                boolean isDistributed = (allocate_receiver_type == 1 && (null != user));
//                int status = isDistributed ? StatusConstant.PENDING : StatusConstant.UNDISTRIBUTED;
//                map.put("work_request_type", "work");
//                if (allocate_receiver_type == 1 && 2 == relationType && !isHaveSub) {
//                    // distributionOrExecute: 1:执行； 执行时，根据设备的id，查看下是否有为该设备配置的专门执行人员，如果有返回该用户
//                    if (!RegexUtil.isNull(relationIds)) {
//                        List<User> doUser = workScheduleService.getDoExecuteUserByAssetId(schema, relationIds, workTypeId, roleIds, now);
//                        if (null != doUser && doUser.size() > 0) {
//                            int max = doUser.size();
//                            int getInt = (int) (new Random().nextFloat() * max);
//                            user = doUser.get(getInt);
//                        } else {
//                            //如果没有专门人员,则看看设备是否在维保期内,如在,则按保内进行寻找
//                            if (needInService > 0) {
//                                Integer companyId = assetGuaranteeService.findAssetPeriodCompanyId(schema, relationIds, "1");
//                                if (null != companyId && companyId > 0) {
//                                    if (assetOrgPositionType == 2) {
//                                        doUser = userService.findUsersByRoles(schema, doRoleIdInService, String.valueOf(facilityId), "0", false);
//                                    } else {
//                                        doUser = userService.findUsersByRoles(schema, doRoleIdInService, positionCode, "0", true);
//                                    }
//
//                                    map.put("work_request_type", "approve");
//                                    if (null != doUser && doUser.size() > 0) {
//                                        int max = doUser.size();
//                                        int getInt = (int) (new Random().nextFloat() * max);
//                                        user = doUser.get(getInt);
//                                    }
//                                    //同时,执行的角色需要再更换下, body 重新给值
//                                    JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
//                                    for (int i = 0; i < bodyArray.size(); i++) {
//                                        JSONObject rowJson = (JSONObject) bodyArray.get(i);
//                                        if (rowJson.get("fieldCode").equals("roleIds")) {
//                                            JSONObject rolsObject = (JSONObject) rowJson.get("fieldViewRelationDetail");
//                                            // 执行的角色id, 先找 role 键的,如果没有,说明不需要进行保内,保外的判断
//                                            if (null != rolsObject.get("role") && null != rolsObject.get("role").toString()
//                                                    && !rolsObject.get("role").toString().isEmpty()) {
//                                                rowJson.put("fieldValue", doRoleIdInService);
//                                                // 保内,需进行确认的节点,状态为待确认
//                                                status = StatusConstant.TO_BE_CONFIRM;
//                                            }
//                                        } else if (rowJson.get("fieldCode").equals("user_assign_type")) {
//                                            rowJson.put("fieldValue", "flowNextOperatorAssign");
//                                        } else if (rowJson.get("fieldCode").equals("distributionOrExecute")) {
//                                            rowJson.put("fieldValue", "");
//                                        }
//                                    }
//                                    bodyProperty = bodyArray.toString();
//                                }
//                            }
//                        }
//                    }
//                }
//                String work_code = dynamicCommonService.getWorkCode(schema);
//                //生成工单主表
//                title = AssetOrgName + businessName + relationName;
//                if (title.length() > 50) {
//                    title = title.substring(0, 49);
//                }
//                result = workSheetHandleService.insertWorks(schema, work_code, workTypeId, workTemplateCode,
//                        poolId, facilityId, positionCode, relationType, relationIds, now, deadlineTime, status, now,
//                        "system", title, plan_code, workRequestCode);
//                if (result > 0) {
//                    String sub_work_code = dynamicCommonService.getSubWorkCode(schema);
//                    businessWorkCode = sub_work_code;
//                    Timestamp receive_time = isDistributed ? now : null;
//                    Timestamp distribute_time = isDistributed ? now : null;
//
//                    receiveAccount = (isDistributed && null != user) ? user.getAccount() : null;
//                    //如果执行人在PM中直接指定了，则需要把此人直接给work_detail
//                    receiveAccount = StringUtil.isNotEmpty(reveiveAccountInPlan) ? reveiveAccountInPlan : receiveAccount;
//                    String flowExecuteUserAccount = "";
//
//                    if (status == StatusConstant.UNDISTRIBUTED) {
//                        //需要找负责的分配人（主管），提醒他进行任务分配
////                        map.put("receive_account", user.getAccount());
//                        // 目前暂无分配节点，先注释掉给分配人发送待办，因为发送后，会导致分配进入的是处理页面，提交不了工单  2020年6月8日 yzj
//                        if (null != user && 1 == haveDistributeNode) {
//                            flowExecuteUserAccount = user.getAccount();
//                        }
//                        map.put("receive_account", flowExecuteUserAccount);
//                    } else {
//                        flowExecuteUserAccount = receiveAccount;
//                        //没有接收人则传递
//                        map.put("receive_account", flowExecuteUserAccount);
//                    }
//
//                    workSheetHandleService.insertWorkDetail(schema, sub_work_code, work_code, workTypeId,
//                            workTemplateCode, relationType, relationIds, title, status, bodyProperty,
//                            receiveAccount, receive_time, distribute_time, 1, null, 0, null, null);
//                    //记录工单处理过程
//                    workProcessService.saveWorkProccess(schema, sub_work_code, status, "system", Constants.PROCESS_CREATE_TASK);
//                    //插入is_main为1的工单的组织
////                    if (isHaveSub) {
////                        //只插一条
////                        workSheetHandleService.insertWorkBaseMainAssetOrg(schema, sub_work_code, relationIds, facilityId);
////                    } else {
////                        //根据设备或位置等对象，判断设备的组织，并进行插入
////                        workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code, relationIds);
////                    }
//
//                    List<String> subWorkCodeAppendList = new ArrayList<>();
//                    String sub_relations_id = "";
//                    if (isDistributed && isHaveSub) {
//                        List<FlowData> subFlowDataList = new ArrayList<>(); // 子任务流程数据集合
//                        //依次生成子工单
//                        if (2 == relationType) {
//                            //按设备生成工单,子对象存储到工单子对象表
//                            if (relationIds != null && !relationIds.isEmpty()) {
//                                JSONArray relationArray = JSONArray.fromObject(relationIds);
//                                for (Object relationItem : relationArray) {
//                                    JSONObject rowJson = JSONObject.fromObject(relationItem);
//                                    String relationIdAppend = rowJson.getString("asset_id");
//                                    String relationCodeAppend = rowJson.getString("strcode");
//                                    String relationNameAppend = rowJson.getString("strname");
//                                    String titleInit = AssetOrgName + businessName + "设备:" + relationNameAppend + "(" + relationCodeAppend + ")";
//                                    String titleAppend = AssetOrgName + businessName + "设备:" + relationNameAppend + "(" + relationCodeAppend + ")";
//                                    if (titleAppend.length() > 50) {
//                                        titleAppend = titleAppend.substring(0, 49);
//                                    }
//                                    String sub_work_code_append = dynamicCommonService.getSubWorkCode(schema);
//                                    subWorkCodeAppendList.add(sub_work_code_append);
//
//                                    //生成子任务
//                                    workSheetHandleService.insertWorkDetail(schema, sub_work_code_append, work_code, workTypeId,
//                                            workTemplateCode, relationType, relationIdAppend, titleAppend, status, bodyProperty,
//                                            receiveAccount, receive_time, distribute_time, -1, null, 0, null, null);
//
//                                    //记录工单处理过程
//                                    workProcessService.saveWorkProccess(schema, sub_work_code_append, status, "system", Constants.PROCESS_CREATE_TASK);
//
//                                    //保存工单的设备组织关系，此处插入的是is_main=1的子工单号，后期如果is_main=-1的也需插入，需修改此处
////                                    workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code_append, relationIdAppend);
////                                    workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code, relationIdAppend);
//                                    sub_relations_id += relationIdAppend + ",";
//
//                                    FlowData subItem = new FlowData();
//                                    subItem.setName(titleInit); // title
//                                    subItem.setReceive_account(flowExecuteUserAccount); // 用户
//                                    subItem.setSub_work_code(sub_work_code_append); // 主键
//                                    subItem.setOrder(-1); // 并行
//                                    subItem.setFacility_id(String.valueOf(facilityId));
//                                    subItem.setCreate_time(nowDateWord);
//                                    subItem.setDeadline_time(deadlineTimeString);
//                                    subFlowDataList.add(subItem);
//                                }
//                            }
//                        } else if (3 == relationType) {
//                            //按点巡检生成工单,子对象存储到工单子对象表
//                            if (relationIds != null && !relationIds.isEmpty()) {
//                                JSONArray relationArray = JSONArray.fromObject(relationIds);
//                                for (Object relationItem : relationArray) {
//                                    JSONObject rowJson = JSONObject.fromObject(relationItem);
//                                    String relationIdAppend = rowJson.getString("inspection_code");
//                                    String relationNameAppend = rowJson.getString("position_name");
//                                    String titleAppend = AssetOrgName + businessName + "区域:" + relationNameAppend;
//                                    if (titleAppend.length() > 50) {
//                                        titleAppend = titleAppend.substring(0, 49);
//                                    }
//                                    String titleInit = AssetOrgName + businessName + "区域:" + relationNameAppend;
//                                    String sub_work_code_append = dynamicCommonService.getSubWorkCode(schema);
//                                    subWorkCodeAppendList.add(sub_work_code_append);
//
//                                    //生成子任务
//                                    workSheetHandleService.insertWorkDetail(schema, sub_work_code_append, work_code, workTypeId,
//                                            workTemplateCode, relationType, relationIdAppend, titleAppend, status, bodyProperty,
//                                            receiveAccount, receive_time, distribute_time, -1, null, 0, null, null);
//
//                                    //记录工单处理过程
//                                    workProcessService.saveWorkProccess(schema, sub_work_code_append, status, "system", Constants.PROCESS_CREATE_TASK);
//
//                                    //保存工单的设备组织关系，此处插入的是is_main=1的子工单号，后期如果is_main=-1的也需插入，需修改此处
////                                    workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code_append, relationIdAppend);
////                                    workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code, relationIdAppend);
//                                    sub_relations_id += relationIdAppend + ",";
//
//                                    FlowData subItem = new FlowData();
//                                    subItem.setName(titleInit); // title
//                                    subItem.setReceive_account(flowExecuteUserAccount); // 用户
//                                    subItem.setSub_work_code(sub_work_code_append); // 主键
//                                    subItem.setOrder(-1); // 并行
//                                    subItem.setFacility_id(String.valueOf(facilityId));
//                                    subItem.setCreate_time(nowDateWord);
//                                    subItem.setDeadline_time(deadlineTimeString);
//                                    subFlowDataList.add(subItem);
//                                }
//                            }
//                        }
//                        //此对象用于生成流程的子任务
//                        map.put("flow_data", JSON.toJSONString(subFlowDataList));
//                    }
//                    //只插入一条
////                  workSheetHandleService.insertWorkBaseMainAssetOrg(schema, sub_work_code, relationIds, facilityId);
//                    if (isHaveSub) {
//                        if (relationIds != null && !relationIds.isEmpty()) {
//                            if (StringUtil.isNotEmpty(sub_relations_id)) {
//                                sub_relations_id = sub_relations_id.substring(0, sub_relations_id.length() - 1);
//                            }
//                            workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code, sub_relations_id);
//                        } else {
//                            workSheetHandleService.insertWorkBaseMainAssetOrg(schema, sub_work_code, "", facilityId);
//                        }
//                    } else {
//                        sub_relations_id = relationIds;
//                        workSheetHandleService.insertWorkAssetOrg(schema, sub_work_code, sub_relations_id);
//                    }
//
//                    //保存工单请求表,临时存一份，保持一致
//                    title = AssetOrgName + relationName + "执行" + businessName;
//                    if (title.length() > 50) {
//                        title = title.substring(0, 49);
//                    }
//                    result = workSheetHandleService.insertWorkRequest(schema, workRequestCode, workTypeId, workTemplateCode,
//                            facilityId, positionCode, relationType, relationIds, title, bodyProperty, AssetOrgName + businessName + ",根据计划:" + planName + "生成",
//                            now, deadlineTime, StatusConstant.PENDING, "", now, "system", plan_code, source_type);
//
//                    map.put("status", String.valueOf(status));
//                    map.put("sub_work_code", sub_work_code);
//
//                    if (null != receiveAccount && !receiveAccount.isEmpty()) {
//                        msgCode = qcloudsmsConfig.SMS_10003007;
//                    } else {
//                        msgCode = qcloudsmsConfig.SMS_10003005;
//                    }
//                }
//            }
//
//            //如果工单生成成功，则把设备的物联数据生成一份，作为此刻保养时的状态值，用于下次保养时判断； yzj 09-10
//            //do_type : 触发条件：1：所有条件满足执行；2：任一条件满足即执行；
//            planWorkService.updatePlanWorkAssetTriggleValue(schema, relationType, isHaveSub, planTriggleModelList, relationIds, plan_code, nowDateWord);
//
//            //记录日志
//            String businessCode = "maintain";
//            switch (businessId) {
//                case 3:
//                    businessCode = "work_Inspection";
//                    break;
//                case 4:
//                    businessCode = "work_spot";
//                    break;
//            }
//            //记录日志
//            logService.AddLog(schema, businessCode, businessWorkCode, selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_SYS_NEW_TASK), "system");
//
//            map.put("deadline_time", deadlineTimeString);
//            map.put("create_user_account", "system");//发送短信人
//            map.put("title_page", AssetOrgName + businessName + relationName);
//            map.put("businessType", String.valueOf(workTypeId));//发送短信内容
//            map.put("facility_id", String.valueOf(facilityId));
//            map.put("position_code", positionCode);
//            map.put("create_time", nowDateWord);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema, flowTemplateCode, receiveAccount, map);
//            if (null != workflowStartResult && !workflowStartResult.isStarted()) {
//                result = 0;
//            }
//            if (null != workflowStartResult && workflowStartResult.isStarted()) {
//                String msgReceiveAccount = (String) map.get("receive_account");
//                String msgSendAccount = (String) map.get("create_user_account");
//                String msgSubWorkCode = (String) map.get("sub_work_code");
//                String sendName = selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_SYS);
//                messageService.msgPreProcessWithSchema(schema, msgCode, msgReceiveAccount, String.valueOf(businessId), msgSubWorkCode,
//                        msgSendAccount, sendName, AssetOrgName + businessName + relationName, null); // 发送短信
//            }
//            Thread.sleep(generateWorkSleepTime);
//            return result;
//        } catch (Exception e) {
//            return 0;
//        }
//    }
//
//
//    /**** 按维保的时长，进行工单的作废服务，韵达定制 *****/
//    //超时保养，作废流水线服务
//    @Override
//    public void cronJobMaintainWorkDiscard(String schema_name) {
//        String schema = schema_name;//"sc_com_47";
//        try {
//            int totalDueWorkCount = workSheetHandleService.workMaintainWorkDiscard(schema);
//            logService.AddLog(schema, "system", "work", "保养工单时效过长关闭总数：" + totalDueWorkCount, "system");
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema, "system", "work", "保养工单时效过长关闭关闭服务出错：" + e.getMessage(), "system");
//        }
//    }
//
//    //超时保养，作废IT服务
//    @Override
//    public void cronJobMaintainWorkDiscardForIT(String schema_name) {
//        String schema = schema_name;//"sc_com_47";
//        try {
//            int totalDueWorkCount = workSheetHandleService.workMaintainWorkDiscardForIT(schema);
//            logService.AddLog(schema, "system", "work", selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_IT_MA_CO_NUM) + totalDueWorkCount, "system");//IT保养工单时效过长关闭总数
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema, "system", "work", selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_IT_MA_CO_ERROR) + e.getMessage(), "system");//IT保养工单时效过长关闭关闭服务出错
//        }
//    }
//
//    /*  第三版本的维保计划方法开始 2019-07-30 yzj 注  */
//
//
//    //每天更新维修工单的上次故障完修时间，用于统计设备未发生故障的总时间，计算MTBF，2019-11-08 yzj
//    @Override
//    public void cronJobRepairMTBFAutoCalculate(String schema_name) {
//        try {
////            Date now = new Date();
////            SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
////            String today = dateFormat.format(now) + " 00:00:01";
//            planWorkService.autoUpdateRepairLastFinishedTime(schema_name, null);
//            logService.AddLog(schema_name, "system", "work", "repair mtbf data generate success.", "system");
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "work", "repair mtbf data generate failure.", "system");
//        }
//    }
//
//    //自动同步设备的供应商和制造商到设备关联组织中去，韵达定制
//    @Override
//    public void cronUpdateSupplyInfoToDeviceOrganization(String schema_name) {
//        try {
//            assetGuaranteeService.autoUpdateSupplyInfoToDeviceOrganization(schema_name);
//            logService.AddLog(schema_name, "system", "supplier_snyc", "Supplier update success.", "system");
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "supplier_snyc", "Supplier update failure.", "system");
//        }
//    }
//
//
//    /*  第二版本的维保计划方法开始 2019-07-30 yzj 注  */
//    // 自动关闭工单的任务，此方法沿用
//    @Override
//    //@Scheduled(cron = "0 0 0/1 * * ? ")
//    public void cronJobToDuePlanWork(String schema_name) {
//        String schema = schema_name;//"sc_com_47";
//        try {
//            int totalDueWorkCount = workSheetHandleService.closeDueWorkAutomatic(schema);
//            logService.AddLog(schema, "system", "work", selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_WK_CO_NUM) + totalDueWorkCount, "system");//工单过期关闭总数
//
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema, "system", "work", selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_WK_CO_ERROR) + e.getMessage(), "system");//工单任务自动关闭服务出错
//        }
//    }
//
//    //自动工单的计划任务，将删除，2019-08-05  yzj
//    @Override
//    //@Scheduled(cron = "0 0/30 * * * ? ")
//    public synchronized void cronJobToGeneratePlanWork(String schema_name) {
//        //String schema = "${schema_name}";
//        String schema = schema_name;        //"sc_com_47";
//        try {
//            int total = 0;
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//
//            //获取当前日期
//            Date nowDate = new Date();
//            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//            String nowDateWord = simleDateFormat.format(nowDate);
//            long today = Timestamp.valueOf(nowDateWord + " 23:59:59").getTime();
//            Timestamp beginDateTime = Timestamp.valueOf(nowDateWord + " 23:59:59");
//            Timestamp endDateTime = Timestamp.valueOf(nowDateWord + " 00:00:00");
//
//            //查找有效的计划
//            List<PlanWorkModel> planList = planWorkService.getExecutePlanWork(schema, now);
//            if (planList != null && !planList.isEmpty()) {
//                //遍历计划任务
//                for (PlanWorkModel item : planList) {
//                    //List<PlanWorkExecuteTimeModel> executeTimeList = planWorkService.getPlanWorkExecuteTimeModelList(schema, item.getPlan_code());
//
//                    String businessName = item.getBusiness_name();     //业务名称
//                    int businessTypeId = item.getBusiness_type_id();   //业务id
//                    Integer facilityId = item.getFacility_id();            //获取场地
//                    String poolId = item.getPool_id();                 //工单池，根据工单池找角色与人
//                    int relationType = item.getRelation_type();        //对象类型 1：位置；2：设备；3：点巡检点；9：其他；
//                    int workTypeId = item.getWork_type_id();             //工单类型，根据工单类型寻找流程，
//                    int period = item.getPeriod();                     //周期
//                    if (period <= 0) {
//                        period = 1;
//                    }
//                    int earlyDays = item.getPre_day();                 //提前天数
//                    String flowTemplateCode = item.getFlow_template_code();
//                    String workTemplateCode = item.getWork_template_code();
//                    int distributionOrExecute = 1;  //是执行还是分配，1:执行；2：分配；
//                    //模板表单的定义字段
//                    String bodyProperty = "[]";
//                    String formKey = workflowService.getStartFormKey(schema, flowTemplateCode);
//                    //找到工单模板
//                    MetadataWork formTemplateTareget = (MetadataWork) metadataWorkService.queryById(schema, formKey).getContent();
//                    if (null != formTemplateTareget) {
//                        bodyProperty = formTemplateTareget.getBodyProperty();
//                    }
//
//                    //寻找工单池下的员工，按工单类型，以及权限，如果没有人，则进入分配节点，如果有人，则平均分配
//                    List<User> doUserList = workScheduleService.getDoTaskUserByWorkTypePermission(schema, facilityId.toString(), poolId, businessTypeId, beginDateTime, endDateTime, distributionOrExecute, null, 0);
//                    if (null == doUserList || doUserList.isEmpty() || doUserList.size() == 0) {
//                        distributionOrExecute = 2;
//                        doUserList = workScheduleService.getDoTaskUserByWorkTypePermission(schema, facilityId.toString(), poolId, businessTypeId, beginDateTime, endDateTime, distributionOrExecute, null, 0);
//                    }
//                    User user;
//
//                    if (2 == item.getIs_have_sub()) {
//                        //有子工单，这时，不用根据子对象生成多条工单
//                        //查看最近的一条工单，看看是否满足周期要求(？？可能还需要传入对象id)
//                        boolean isNeedGenerate = false;
//                        WorkSheet latestSheet = planWorkService.getLatestWorksByFacility(schema, workTypeId, facilityId);
//                        if (null == latestSheet || 0 == latestSheet.getFacility_id()) {
//                            isNeedGenerate = true;
//                        } else {
//                            int days = (int) ((today - latestSheet.getDeadline_time().getTime()) / (1000 * 3600 * 24));
//                            if (period <= 1 || days > (period - earlyDays)) {
//                                //超过期限，也需要生成
//                                isNeedGenerate = true;
//                            }
//                        }
//                        if (isNeedGenerate) {
//                            if (doUserList != null && doUserList.size() > 0) {
//                                int max = doUserList.size();
//                                int getInt = (int) (new Random().nextFloat() * max);
//                                user = doUserList.get(getInt);
//                                //将获取的对象，拼接为一个json对象，作为自定义字段，存储起来，生成工单
//                                String relationIdContent = "";
//                                if (2 == relationType) {
//                                    List<PlanWorkAssetModel> planWorkAssetModelList = planWorkService.getALLPlanWorkAssetList(schema, item.getPlan_code());
//                                    if (planWorkAssetModelList.size() > 0) {
//                                        relationIdContent = JSONArray.fromObject(planWorkAssetModelList).toString();
//                                    }
//                                } else if (3 == relationType) {
//                                    List<PlanWorkInspectionModel> planWorkInspectionList = planWorkService.getALLPlanWorkInspectionList(schema, item.getPlan_code());
//                                    relationIdContent = JSONArray.fromObject(planWorkInspectionList).toString();
//                                }
//
////                                doGenerateWork(schema, facilityId, poolId, workTypeId, businessTypeId,
////                                        businessName, relationType, relationIdContent, workTemplateCode,
////                                        flowTemplateCode, now, earlyDays, user, true, distributionOrExecute, bodyProperty, formKey, item);
//                                total++;
//                            }
//                        }
//
//                    } else {
//                        //无子工单，这时，根据子对象，每一个都生成一张工单
//                        int doUserCount = 0;
//                        int avgNum = 1;
//                        int isBigYu = 0; //是否有余
//                        if (2 == relationType) {
//                            //获取计划的总设备数，看每天应该执行几个
//                            int totalPlanAsset = planWorkService.getPlanWorkAssetCount(schema, item.getPlan_code());
//                            int needCount = totalPlanAsset / period;       //按周期，每天需要生成的个数
//                            if (totalPlanAsset % period != 0) {
//                                needCount++;
//                            }
//                            //获取设备执行的个数，按最近的任务，以及提前生成的周期
//                            List<PlanWorkAssetModel> planWorkAssetModelList = planWorkService.getCurrentDayPlanWorkAssetList
//                                    (schema, item.getPlan_code(), item.getFacility_id(), item.getWork_type_id(), period - earlyDays, needCount);
//
//                            if (null != planWorkAssetModelList && !planWorkAssetModelList.isEmpty() && planWorkAssetModelList.size() > 0) {
//                                int generateAssetCount = planWorkAssetModelList.size();
//                                //看看是否有人，如果有人，则平均分配，并且把最后不能平均分配的余，随机给人员
//                                if (doUserList != null && doUserList.size() > 0) {
//                                    doUserCount = doUserList.size();
//                                    avgNum = generateAssetCount / doUserList.size();
//                                    isBigYu = generateAssetCount % doUserList.size();     //是否有余
//                                }
//                                //按设备生成工单
//                                for (int i = 0; i < generateAssetCount; i++) {
//                                    if (avgNum != 0 && i / avgNum < doUserCount) {
//                                        // 能整除的部分平均分配
//                                        user = doUserList.get(i / avgNum);
//                                    } else {
//                                        // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
//                                        int last = 0;
//                                        int max = doUserList.size();
//                                        if (isBigYu > 0) {
//                                            //随机取，无法避免同一次，取值重复问题
//                                            last = (int) (new Random().nextFloat() * max);
//                                        }
//                                        user = doUserList.get(last);
//
//                                        //把集合中，已经选择的用户排斥掉
//                                        doUserList.remove(last);
//                                    }
//                                    String assetTitle = businessName + selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.TITLE_ASSET_I)
//                                            + planWorkAssetModelList.get(i).getStrname() + "(" + planWorkAssetModelList.get(i).getStrcode() + ")";
//                                    //创建工单
////                                    doGenerateWork(schema, facilityId, poolId, workTypeId, businessTypeId,
////                                            assetTitle, relationType, planWorkAssetModelList.get(i).getAsset_id(), workTemplateCode,
////                                            flowTemplateCode, now, earlyDays, user, false, distributionOrExecute, bodyProperty, formKey, item);
//                                    total++;
//                                }
//                            }
//                        } else if (3 == relationType) {
//                            //获取点巡检总数，看每天应该执行几个
//                            int totalPlanInspection = planWorkService.getPlanWorkInspectionCount(schema, item.getPlan_code());
//                            int needCount = totalPlanInspection / period;       //按周期，每天需要生成的个数
//                            if (totalPlanInspection % period > 0) {
//                                needCount++;
//                            }
//                            //获取点巡检执行的个数，按最近的任务，以及提前生成的周期
//                            List<PlanWorkInspectionModel> planWorkInspectionList = planWorkService.getCurrentDayPlanWorkInspectionList
//                                    (schema, item.getPlan_code(), item.getFacility_id(), item.getWork_type_id(), period - earlyDays, needCount);
//
//                            if (null != planWorkInspectionList && !planWorkInspectionList.isEmpty() && planWorkInspectionList.size() > 0) {
//                                int generateInspectionCount = planWorkInspectionList.size();
//                                //看看是否有人，如果有人，则平均分配，并且把最后不能平均分配的余，随机给人员
//                                if (doUserList != null && doUserList.size() > 0) {
//                                    doUserCount = doUserList.size();
//                                    avgNum = generateInspectionCount / doUserList.size();
//                                    isBigYu = generateInspectionCount % doUserList.size();     //是否有余
//                                }
//                                //按设备生成工单
//                                for (int i = 0; i < generateInspectionCount; i++) {
//                                    if (avgNum != 0 && i / avgNum < doUserCount) {
//                                        // 能整除的部分平均分配
//                                        user = doUserList.get(i / avgNum);
//                                    } else {
//                                        // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
//                                        int max = doUserList.size();
//                                        int last = 0;
//                                        if (isBigYu > 0) {
//                                            //随机取，无法避免同一次，取值重复问题
//                                            last = (int) (new Random().nextFloat() * max);
//                                        }
//                                        user = doUserList.get(last);
//
//                                        //把集合中，已经选择的用户排斥掉
//                                        doUserList.remove(last);
//                                    }
//                                    String inspectionTitle = businessName + selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.AREA_A) +
//                                            planWorkInspectionList.get(i).getArea_name();
//
//                                    //创建工单
////                                    doGenerateWork(schema, facilityId, poolId, workTypeId, businessTypeId,
////                                            inspectionTitle, relationType, planWorkInspectionList.get(i).getArea_code(), workTemplateCode,
////                                            flowTemplateCode, now, earlyDays, user, false, distributionOrExecute, bodyProperty, formKey, item);
//
//                                    total++;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            logService.AddLog(schema, "system", "work_auto",
//                    selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_AUTOM_NUM) + total, "system");
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema, "system", "work_auto",
//                    selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_AUTOM_SER_ERROR) + e.getMessage(), "system");
//        }
//    }
//
//    //按生成工单，按工单类型，并进行存储
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：2：保养；3：巡检；4：点检；
//    private int doGenerateWork(String schema, int facilityId, String poolId, int workTypeId, int businessId,
//                               String businessName, int relationType, String relationIds,
//                               String workTemplateCode, String flowTemplateCode, Timestamp now, int earlyDays, User user, boolean isHaveSub,
//                               int distributionOrExecute, String bodyProperty, String formKey, PlanWorkModel planModel) {
//        int result = 0;
////        int status = distributionOrExecute == 1 ? StatusConstant.PENDING : StatusConstant.UNDISTRIBUTED;
////        String work_code = dynamicCommonService.getWorkCode(schema);
////        Long endTime = now.getTime() + earlyDays * (1000 * 3600 * 24);
////        Timestamp deadlineTime = new Timestamp(endTime);
////
////        String relationId = " ";
////
////        if (!isHaveSub) {
////            relationId = relationIds;
////        }
////
////        // TODO 要查找客户/设备位置
//////        //先插入主表
//////        result = workSheetHandleService.insertWorks(schema, work_code, workTypeId, workTemplateCode,
//////                poolId, facilityId, relationType, relationId, now, deadlineTime, status, now,
//////                "system", businessName);
////        if (result > 0) {
////            String sub_work_code = dynamicCommonService.getSubWorkCode(schema);
////            Timestamp receive_time = distributionOrExecute == 1 ? now : null;
////            Timestamp distribute_time = distributionOrExecute == 1 ? now : null;
////            String receiveAccount = distributionOrExecute == 1 ? user.getAccount() : null;
////            //表单无，则没法生成任务，返回
////            if (formKey == null || formKey.isEmpty()) {
////                return 0;
////            }
////            if (null != bodyProperty && !bodyProperty.isEmpty()) {
////                //body重新给值
////                JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
////                for (int i = 0; i < bodyArray.size(); i++) {
////                    JSONObject rowJson = (JSONObject) bodyArray.get(i);
////                    //子对象给relationId
////                    if (isHaveSub) {
////                        if (rowJson.get("fieldCode").toString().equals("relation_id") && rowJson.get("dbTableType").toString().equals("1")) {
////                            rowJson.put("relationContent", relationIds);
//////                            ((JSONObject) bodyArray.get(i)).put("relationContent", relationIds);
////                        }
////                    }
////                    if (rowJson.get("fieldViewType").equals("9")) {
////                        String taskItemId = rowJson.get("fieldCode").toString();
////                        if (null != planModel && null != planModel.getTask_list_word() && !planModel.getTask_list_word().isEmpty()) {
////                            JSONObject taskListJson = JSONObject.fromObject(planModel.getTask_list_word());
////                            rowJson.put("taskContent", taskListJson.get(taskItemId));
//////                            ((JSONObject) bodyArray.get(i)).put("taskContent", taskListJson.get(taskItemId));
////                        }
////                    }
////                }
////
////                bodyProperty = bodyArray.toString();
////            }
////
////            workSheetHandleService.insertWorkDetail(schema, sub_work_code, work_code, workTypeId,
////                    workTemplateCode, relationType, relationId, businessName, status, bodyProperty,
////                    receiveAccount, receive_time, distribute_time, 1, null, 0, null, null);
////            //记录工单处理过程
////            workProcessService.saveWorkProccess(schema, sub_work_code, status, "system");
////
////            //发送流程
////            Map map = new HashMap();
////
////            List<String> subWorkCodeAppendList = new ArrayList<>();
////            DateFormat timeFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
////            String deadlineTimeString = timeFormat.format(deadlineTime);
////            String nowDateWord = timeFormat.format(now);
////            if (1 == distributionOrExecute && isHaveSub) {
////                List<FlowData> subFlowDataList = new ArrayList<FlowData>(); // 子任务流程数据集合
////                //依次生成子工单
////                if (2 == relationType) {
////                    //按设备生成工单,子对象存储到工单子对象表
////                    if (relationIds != null && !relationIds.isEmpty()) {
////                        JSONArray relationArray = JSONArray.fromObject(relationIds);
////                        for (Object relationItem : relationArray) {
////                            JSONObject rowJson = JSONObject.fromObject(relationItem);
////                            String relationIdAppend = rowJson.getString("asset_id");
////                            String relationCodeAppend = rowJson.getString("strcode");
////                            String relationNameAppend = rowJson.getString("strname");
////                            String titleAppend = businessName + "设备:" + relationNameAppend + "(" + relationCodeAppend + ")";
////                            String sub_work_code_append = dynamicCommonService.getSubWorkCode(schema);
////                            subWorkCodeAppendList.add(sub_work_code_append);
////
////                            //生成子任务
////                            workSheetHandleService.insertWorkDetail(schema, sub_work_code_append, work_code, workTypeId,
////                                    workTemplateCode, relationType, relationIdAppend, titleAppend, status, bodyProperty,
////                                    receiveAccount, receive_time, distribute_time, -1, null, 0, null, null);
////
////                            //记录工单处理过程
////                            workProcessService.saveWorkProccess(schema, sub_work_code_append, status, "system");
////
////                            FlowData subItem = new FlowData();
////                            subItem.setName(titleAppend); // title
////                            subItem.setReceive_account(receiveAccount); // 用户
////                            subItem.setSub_work_code(sub_work_code_append); // 主键
////                            subItem.setOrder(-1); // 并行
////                            subItem.setFacility_id(String.valueOf(facilityId));
////                            subItem.setCreate_time(nowDateWord);
////                            subItem.setDeadline_time(deadlineTimeString);
////                            subFlowDataList.add(subItem);
////                        }
////                    }
////
////                } else if (3 == relationType) {
////                    //按点巡检生成工单,子对象存储到工单子对象表
////                    if (relationIds != null && !relationIds.isEmpty()) {
////                        JSONArray relationArray = JSONArray.fromObject(relationIds);
////                        for (Object relationItem : relationArray) {
////                            JSONObject rowJson = JSONObject.fromObject(relationItem);
////                            String relationIdAppend = rowJson.getString("area_code");
////                            String relationNameAppend = rowJson.getString("area_name");
////                            String titleAppend = businessName + "区域:" + relationNameAppend;
////                            String sub_work_code_append = dynamicCommonService.getSubWorkCode(schema);
////                            subWorkCodeAppendList.add(sub_work_code_append);
////
////                            //生成子任务
////                            workSheetHandleService.insertWorkDetail(schema, sub_work_code_append, work_code, workTypeId,
////                                    workTemplateCode, relationType, relationIdAppend, titleAppend, status, bodyProperty,
////                                    receiveAccount, receive_time, distribute_time, -1, null, 0, null, null);
////
////                            //记录工单处理过程
////                            workProcessService.saveWorkProccess(schema, sub_work_code_append, status, "system");
////
////                            FlowData subItem = new FlowData();
////                            subItem.setName(titleAppend); // title
////                            subItem.setReceive_account(receiveAccount); // 用户
////                            subItem.setSub_work_code(sub_work_code_append); // 主键
////                            subItem.setOrder(-1); // 并行
////                            subItem.setFacility_id(String.valueOf(facilityId));
////                            subItem.setCreate_time(nowDateWord);
////                            subItem.setDeadline_time(deadlineTimeString);
////                            subFlowDataList.add(subItem);
////                        }
////                    }
////                }
////                //此对象用于生成流程的子任务
////                map.put("flow_data", JSON.toJSONString(subFlowDataList));
////            }
////
////            map.put("status", String.valueOf(status));
////            map.put("title_page", businessName);
////            map.put("sub_work_code", sub_work_code);
////            map.put("deadline_time", deadlineTimeString);
////            map.put("create_user_account", "system");//发送短信人
////            map.put("receive_account", receiveAccount);//没有接收人则传递
////            String content;
////            if (null != receiveAccount && !receiveAccount.isEmpty()) {
////                if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {//韵达短信发送平台
////                    content = MessageFormat.format(SensConstant.UN_COMPLETE_INFO_WORK, "系统", businessName);
////                } else {//腾讯短信发送平台
////                    String pattern = "{0,number,#}::{1}::{2}";
////                    content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_UN_COMPLETE_INFO_WORK, "系统", businessName);
////                }
////            } else {
////                if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {//韵达短信发送平台
////                    content = MessageFormat.format(SensConstant.UN_DISTRIBUTION_INFO_MAINTAIN.replace("{2}", "完成"), "系统", businessName);
////                } else {//腾讯短信发送平台
////                    String pattern = "{0,number,#}::{1}::{2}::{3}";
////                    content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_UN_DISTRIBUTION_INFO_MAINTAIN, "系统", businessName, "完成");
////                }
////            }
//////            map.put("content", "您有" + businessName + "任务，请及时完成。");//发送短信内容
////            map.put("content", content);//发送短信内容
////            map.put("businessType", String.valueOf(workTypeId));//发送短信内容
////            map.put("create_time", nowDateWord);
////            map.put("facility_id", String.valueOf(facilityId));
////
////            //记录日志
////            String businessCode = "maintain";
////            switch (businessId) {
////                case 3:
////                    businessCode = "work_Inspection";
////                    break;
////                case 4:
////                    businessCode = "work_spot";
////                    break;
////            }
////
////            logService.AddLog(schema, businessCode, sub_work_code, "系统新建自动任务", "system");
////
////            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema, flowTemplateCode, user.getAccount(), map);
////            if (null != workflowStartResult && !workflowStartResult.isStarted()) {
////                result = 0;
////            }
////        }
//
//        return result;
//    }
//    /*  第二版本的维保计划方法结束 2019-07-30 yzj 注  */
//
//
//    /* 功能：进行设备监控数据处理，发送短信和生成维修单
//     * status：1：未处理
//     *         2：已经实时到设备实时表
//     *         3：任务已经处理，发送短信或者生成任务单据
//     * */
//    @Override
////    @Scheduled(cron = "0 0 1 * * ? ")
//    public void cronJobToDealMonitorData() {
//        String schema = "${schema_name}";
//        try {
//            // 获取需要处理的监控数据
//            List<AssetMonitorData> needDealList = assetInfoService.findNeedDealAssetMonitorList(schema);
//            if (needDealList != null && needDealList.size() > 0) {
//                //获取当前时间
//                Timestamp now = new Timestamp(System.currentTimeMillis());
//                Calendar calendar = new GregorianCalendar();
//                calendar.setTime(now);
//                calendar.add(calendar.DATE, 1);     //把日期往后增加一天.整数往后推,负数往前移动
//                Date date = calendar.getTime();             //这个时间就是日期往后推一天的结果
//                Timestamp nousedate = new Timestamp(date.getTime());
//
//                for (AssetMonitorData data : needDealList) {//需处理数据集合
//                    int countAlarmInfo = alarmAnomalySendInfoService.isAlarmInfo(schema, data.getAssetCode(), data.getMonitorName());
//                    SystemConfigData systemConfigData = systemConfigService.getSystemConfigData(schema, Constants.EXCEPTION_SMS_INTERVAL);//查询系统配置获取异常事件是时间
//                    long exception_time_value = Long.parseLong(systemConfigData.getSettingValue());//获得系统配置时间单位毫秒
//                    List<AlarmAnomalySendInfoResult> alarmAnomalySendInfoResult = alarmAnomalySendInfoService.getAlarmAnomalySendInfoData(schema, data.getAssetCode(), data.getMonitorName());
//                    if (data.getIs_send_msg() == 1) {
//                        //需要发送上限报警短信
//                        if (data.getAlarmUp() != null && !data.getAlarmUp().isEmpty() && data.getMonitorValue() != null && !data.getMonitorValue().isEmpty()) {
//                            //调用记录异常报警记录单接口
//                            if (Double.parseDouble(data.getAlarmUp()) < Double.parseDouble(data.getMonitorValue())) {//判断为异常上线
//                                alarmSendInfo(alarmAnomalySendInfoResult, data);
//                            }
//                        }
//                        //需要发送下限报警短信
//                        if (data.getAlarmDown() != null && !data.getAlarmDown().isEmpty() && data.getMonitorValue() != null && !data.getMonitorValue().isEmpty()) {
//                            if (Double.parseDouble(data.getAlarmDown()) > Double.parseDouble(data.getMonitorValue())) {//判断为异常上线
//                                alarmSendInfo(alarmAnomalySendInfoResult, data);
//                            }
//                        }
//                    }
//
//                    if (data.getIs_send_msg() == 1) {
//                        if (!(countAlarmInfo > 0)) {//如果不存在报警信息则记录异常信息
//                            if (data.getExceptionUp() != null && !data.getExceptionUp().isEmpty() && data.getMonitorValue() != null && !data.getMonitorValue().isEmpty()) {
//                                if (Double.parseDouble(data.getExceptionUp()) < Double.parseDouble(data.getMonitorValue())) {
//                                    exceptionSendInfo(alarmAnomalySendInfoResult, exception_time_value, data);
//                                }
//                            }
//                            if (data.getExceptionDown() != null && !data.getExceptionDown().isEmpty() && data.getMonitorValue() != null && !data.getMonitorValue().isEmpty()) {
//                                if (Double.parseDouble(data.getExceptionDown()) > Double.parseDouble(data.getMonitorValue())) {
//                                    exceptionSendInfo(alarmAnomalySendInfoResult, exception_time_value, data);
//                                }
//                            }
//                        }
//                    }
//                    int monitorStatus = assetInfoService.updateNeedDealAssetMonitor(schema, data.getId());
//                }
//            }
//        } catch (Exception e) {
//            //系统错误日志
//            logService.AddLog(schema, "system", "asset_monitor", selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_ASSET_MON_ERROR) + e.getMessage(), "system");//设备监控数据处理（发短信和维修任务）服务出错
//        }
//    }
//
//
//    /*
//     * 记录异常报警记录信息
//     * @param data
//     * @return
//     */
//    private String addAlarmAnomaly(AssetMonitorData data) {
//        String schema_name = "${schema_name}";
//        String result = "";
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        try {
//            String ss = serialNumberService.generateMaxBusinessCode(schema_name, "alarm_exception");
//            AlarmAnomalySendInfoResult alarmAnomalySendInfoResult = new AlarmAnomalySendInfoResult();
//            alarmAnomalySendInfoResult.setAlarm_code(serialNumberService.generateMaxBusinessCode(schema_name, "alarm_exception"));//主键
//            alarmAnomalySendInfoResult.setAsset_code(data.getAssetCode());//设备编码
//            alarmAnomalySendInfoResult.setMonitor_name(data.getMonitorName());//监控项名称
//            alarmAnomalySendInfoResult.setMonitor_value(data.getMonitorValue());//监控值
//            alarmAnomalySendInfoResult.setMonitor_name(data.getMonitorName());//收集时间
//            alarmAnomalySendInfoResult.setMonitor_value_type(data.getMonitorValueType());//监控值类型
//            alarmAnomalySendInfoResult.setMonitor_value_unit(data.getMonitorValueUnit());//单位
//            alarmAnomalySendInfoResult.setAlarm_up(data.getAlarmUp());//报警上限
//            alarmAnomalySendInfoResult.setAlarm_down(data.getAlarmDown());//报警下限
//            alarmAnomalySendInfoResult.setException_up(data.getExceptionUp());//异常上限
//            alarmAnomalySendInfoResult.setException_down(data.getExceptionDown());//异常下限
//            alarmAnomalySendInfoResult.setAlarm_msg(data.getAlarm_msg());//是否发送报警消息
//            alarmAnomalySendInfoResult.setException_msg(data.getException_msg());//是否发送异常消息
//            alarmAnomalySendInfoResult.setIs_send_task(data.getIs_send_task());//是否生成维修单
//            alarmAnomalySendInfoResult.setIs_send_msg(data.getIs_send_task());//是否生成维修单
//            alarmAnomalySendInfoResult.setAlarm_up_task_code(data.getAlarmUpTaskCode());//报警上限任务代码
//            alarmAnomalySendInfoResult.setAlarm_down_task_code(data.getAlarmDownTaskCode());//报警下限任务代码
//            alarmAnomalySendInfoResult.setSms_times(0);//短信次数：0；1；2；3；4；5。。。。
//            alarmAnomalySendInfoResult.setStatus(1);//1：未处理；2：处理中；3：已处理；4：取消；
//            alarmAnomalySendInfoResult.setCreate_time(now);//创建时间
//            int doAdd = alarmAnomalySendInfoService.addAlarmAnomalySendInfo(schema_name, alarmAnomalySendInfoResult);
//            if (doAdd > 0) {
//                result = alarmAnomalySendInfoResult.getAlarm_code();
//            }
//            return result;
//        } catch (Exception e) {
//            return result;
//        }
//    }
//
//    /**
//     * 异常逻辑处理方法
//     *
//     * @param alarmAnomalySendInfoResult
//     * @param time_value
//     * @param data
//     */
//    private void exceptionSendInfo(List<AlarmAnomalySendInfoResult> alarmAnomalySendInfoResult, long time_value, AssetMonitorData data) {
//        String schema_name = "${schema_name}";
//        try {
//            Date now_time = new Date();
//            int noException = 0;
//            int total = alarmAnomalySendInfoResult.size();
//            if (alarmAnomalySendInfoResult != null && total > 0) {//已存记录进行判断上次处理的
//                for (AlarmAnomalySendInfoResult data1 : alarmAnomalySendInfoResult) {
//                    int alarm_msg = data1.getIs_send_msg();//判断是否为报警数据
//                    int exception_msg = data1.getIs_send_msg();//判断是否为异常数据
//                    if (exception_msg == 1 && alarm_msg == 0) {
//                        Long timeDiff = (now_time.getTime() - data1.getCreate_time().getTime()) / 1000;//判断上条记录时间的和当前时间之间的毫秒差
//                        if (time_value < timeDiff) {
//                            addAlarmAnomaly(data);
//                            break;//如果第一个也不满足则直接跳出循环
//                        }
//                    } else {
//                        noException += 1;//标记记录表中不是异常数据记录的信息数量
//                    }
//                }
//                if (noException == total) {
//                    addAlarmAnomaly(data);
//                }
//            } else {//没有记录直接记录并发送短信消息
//                addAlarmAnomaly(data);
//            }
//        } catch (Exception e) {
//            logService.AddLog(schema_name, "system", "asset_monitor", selectOptionService.getLanguageInfoWithoutRequest(schema_name, LangConstant.MSG_ASSET_MON_ERROR_LIST) + e.getMessage(), "system");//设备监控异常记录记录失败
//        }
//    }
//
//
//    /**
//     * 记录报警的记录信息
//     *
//     * @param alarmAnomalySendInfoResult
//     * @param data
//     */
//    private void alarmSendInfo(List<AlarmAnomalySendInfoResult> alarmAnomalySendInfoResult, AssetMonitorData data) {
//        //相同的设备编号和监控类型如果处于未处理和处理中，则不进行保存操作
//        String schema_name = "${schema_name}";
//        try {
//            int noAlarm = 0;
//            int total = alarmAnomalySendInfoResult.size();
//            if (alarmAnomalySendInfoResult != null && total > 0) {
//                for (AlarmAnomalySendInfoResult data1 : alarmAnomalySendInfoResult) {
//                    int alarmStatus = data1.getStatus();//报警记录状态
//                    int alarm_msg = data1.getIs_send_msg();//判断是否为报警数据
//                    int exception_msg = data1.getIs_send_msg();//判断是否为异常数据
//                    if (alarm_msg == 1 && exception_msg == 0) {
//                        if (!(alarmStatus == 1 || alarmStatus == 2)) {//同一设备如果存在未处理和处理中则不再新增异常项
//                            addAlarmAnomaly(data);
//                            break;
//                        }
//                    } else {
//                        noAlarm += 1;//标记记录表中不是异常数据记录的信息数量
//                    }
//                }
//                if (noAlarm == total) {//如果历史记录表中没有报警信息怎直接保存该信息
//                    addAlarmAnomaly(data);
//                }
//            } else {//如果不存在同一监控项，则直接保存报警信息
//                addAlarmAnomaly(data);
//            }
//        } catch (Exception e) {
//            logService.AddLog(schema_name, "system", "asset_monitor", selectOptionService.getLanguageInfoWithoutRequest(schema_name, LangConstant.MSG_ASSET_MON_ERROR_LIST) + e.getMessage(), "system");//设备监控异常记录记录失败
//        }
//    }
//
//    // 自动生成任务单据，这个现在不需要了，功能注释掉
//    private void addRepairAlarmData(String schema, AssetMonitorData data, String alarm_code) {
//        try {
//            String repairContent = "";
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            Calendar calendar = new GregorianCalendar();
//            calendar.setTime(now);
//            calendar.add(calendar.DATE, 1);     //把日期往后增加一天.整数往后推,负数往前移动
//            Date date = calendar.getTime();             //这个时间就是日期往后推一天的结果
//            Timestamp nousedate = new Timestamp(date.getTime());
//            if (data.getAlarmUp() != null && !data.getAlarmUp().isEmpty() && data.getMonitorValue() != null && !data.getMonitorValue().isEmpty()) {
//                if (Double.parseDouble(data.getAlarmUp()) < Double.parseDouble(data.getMonitorValue())) {
//                    repairContent = data.getMonitorName() + "上限报警，请维修，任务编码:" + data.getAlarmUpTaskCode();
//                }
//            } else if (data.getAlarmDown() != null && !data.getAlarmDown().isEmpty() && data.getMonitorValue() != null && !data.getMonitorValue().isEmpty()) {
//                if (Double.parseDouble(data.getAlarmDown()) > Double.parseDouble(data.getMonitorValue())) {
//                    repairContent = data.getMonitorName() + "下限报警，请维修，任务编码:" + data.getAlarmUpTaskCode();
//                }
//            }
//            RepairData model = new RepairData();
//            //生成新订单编号
//            String repairCode = serialNumberService.generateMaxBusinessCode(schema, "repair");
//            model.setRepairCode(repairCode);
//            model.setAssetId(data.getAssetId());
//            model.setAssetType(data.getAssetType());
//            model.setAssetStatus(2);
//            model.setDeadlineTime(nousedate);
//            model.setOccurtime(now);
//            model.setFaultNote(repairContent);
//            model.setDistributeAccount("");
//            model.setDistributeTime(null);
//            model.setReceiveAccount("");
//            //判断状态
//            int status = 40;   //待分配
//            model.setReveiveTime(now);
//            model.setDistributeTime(now);
//            model.setRepairBeginTime(null);
//            model.setStatus(status);
//            model.setFacilityId(data.getIntSiteid());
//            model.setCreate_user_account("system");
//            model.setCreatetime(now);
//            model.setPriorityLevel(2);
//            model.setFromCode(alarm_code);
//            //保存上报问题
//            repairService.saveRepairInfo(schema, model);
//        } catch (Exception e) {
//            logService.AddLog(schema, "system", "asset_monitor", "保存报警信息自动生成任务单据失败：" + e.getMessage(), "system");
//        }
//    }
//
//
//    //备件设备实际库存数小于安全库存数发送短信消息推送给相关负责人任任务
//    @Override
//    public void cronJobBomFewSendInfo() {
//        String schema = "${schema_name}";
//        try {
//            //遍历备件实际库存小于安全库存备件信息
//            List<Map<String, Object>> bomList = bomStockListService.queryBomOutOfSecurityQuantity(schema);
//            if (bomList != null && bomList.size() > 0 && !bomList.isEmpty()) {
//                for (Map<String, Object> key : bomList) {
//                    try {
//                        List<User> userData = userService.findUserByFunctionStrKey(schema, "bom_limit_sms", (String) key.get("facility_id"), 0);//根据选线查询是否有权限
//                        if (userData != null && userData.size() > 0 && !userData.isEmpty()) {
//                            for (User userData1 : userData) {
//                                String phone = userData1.getMobile();
//                                if (phone != null && !phone.equals("")) {
//                                    String[] contentParam = {(String) key.get("bom_name"), (String) key.get("bom_code"), (String) key.get("stock_name")};
//                                    messageService.beginMsg(contentParam, qcloudsmsConfig.SMS_10002001, userData1.getAccount(), "stock", "bom", "system");
//                                }
//                            }
//                        }
//                    } catch (Exception e) {
//                        continue;
//                    }
//                }
//            }
//            //发现实际库存数量小于安全库存数量则发送给位置设备间负责人短信只发一次
//        } catch (Exception e) {
//            logService.AddLog(schema, "system", "bom", selectOptionService.getLanguageInfoWithoutRequest(schema, LangConstant.MSG_BOM_ERROR_MSG_FAIL) + e.getMessage(), "system");//备件库存数量小于安全库存数量推送短信消息失败
//
//        }
//    }
//
//
///*    第一版本的维保计划方法 2019-07-30 yzj 注
////    如果保养过期，则将保养单的状态设置为过期
////    @Override
////    public void cronJobToDueMaintainBill() {
////        String schema = "${schema_name}";
////        try {
////            int totalMaintainCount = maintainService.CloseMaintainBillForDue(schema);
////            logService.AddLog(schema, "system", "maintain_task", "保养任务过期关闭总数：" + totalMaintainCount, "system");
////
////        } catch (Exception e) {
////            //系统错误日志
////            logService.AddLog(schema, "system", "maintain_task", "保养任务自动关闭服务出错：" + e.getMessage(), "system");
////        }
////    }
//
////    自动生成保养任务，按设备的保养周期进行生成
////    @Override
////    @Scheduled(cron = "0 0 6 * * ? ")
////    public void cronJob() {
////        String schema = "${schema_name}";
////        try {
////            Company company = null;
////            String[] ss = schema.split("_");
////            String companyId = ss[ss.length - 1];
////            if (StringUtils.isNotEmpty(companyId)) {
////                company = companyService.findById(Long.parseLong(companyId));
////            }
////            //找到位置需要进行保养的设备，以及
////            List<Facility> facilities = facilitiesService.FacilitiesList(schema);
////            int totalMaintainCount = 0;
////            List<MaintainItemData> maintainItemDataList = maintainService.getAllMaintainItemList(schema);
////            //获取当前日期
////            Date nowDate = new Date();
////            SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
////            String nowDateWord = simleDateFormat.format(nowDate);
////            //上班时间
////            Timestamp beginDateTime = Timestamp.valueOf(nowDateWord + " 23:59:59");
////            Timestamp endDateTime = Timestamp.valueOf(nowDateWord + " 00:00:00");
////            long today = Timestamp.valueOf(nowDateWord + " 23:59:59").getTime();
////
////            for (Facility facility : facilities) {
////                List<MaintainData> list = maintainService.findAllAssetAndLastestMaintain(schema, facility.getId());
////                //定义需要保养的设备
////                List<MaintainData> assetNeedMaintain = new ArrayList<>();
////                for (MaintainData asset : list) {
////                    // 据上次一保养时间间隔
////                    int days;
////                    if (asset.getMaintainBeginTime() != null && asset.getAssetStatus() > 0) {
////                        Date beginDate = asset.getMaintainBeginTime();
////                        if (asset != null && asset.getAssetCode() != null && !asset.getAssetCode().isEmpty()) {
////                            if (asset.getDeadlineTime() == null) {
////                                // 没有保养数据，则根据保养开始日期进行计算
////                                days = (int) ((today - beginDate.getTime()) / (1000 * 3600 * 24));
////                            } else {
////                                days = (int) ((today - asset.getDeadlineTime().getTime()) / (1000 * 3600 * 24));
////                            }
////                            // 判断是否需要生成新保养单// 提前一周
////                            if (days >= (asset.getCycleCount() - 7)) {
////                                assetNeedMaintain.add(asset);
////                            }
////                        }
////                    }
////                }
////
////                //开始生成保养单
////                if (assetNeedMaintain != null && !assetNeedMaintain.isEmpty() && assetNeedMaintain.size() > 0) {
////                    //找到该位置所有的，能执行保养提交任务的人员
////                    //修改为白天在上班的能保养的人员
////                    List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility.getId(), "maintain_result_submit", beginDateTime, endDateTime);
////                    if (scheduleList.size() == 0) {
////                        //再找一级所属位置的，看有没有排班的人
////                        scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility.getParentId(), "maintain_result_submit", beginDateTime, endDateTime);
////                        if (scheduleList.size() == 0) {
////                            //还是没有人，则按角色找
////                            scheduleList = userService.findUserByFunctionKey(schema, "maintain_result_submit", facility.getId());
////                        }
////                    }
////                    //找到的人如果没有配置，则不分配保养人员
////                    if (scheduleList == null || scheduleList.isEmpty()) {
////                        scheduleList = new ArrayList<>();
////                    }
////
////                    int assetInSiteNum = assetNeedMaintain.size();      // 该位置需要保养的设备数量
////                    int scheduleInSiteNum = scheduleList.size();        // 该位置当天排班人数\
////                    // 每人平均负责保养的设备数量（向下取整）
////                    int avgNum = 1;
////                    int isBigYu = 0; //是否有余
////                    if (scheduleList.size() != 0) {
////                        avgNum = assetInSiteNum / scheduleInSiteNum;
////                        isBigYu = assetInSiteNum % scheduleInSiteNum;
//////                        if (isBigYu > 0) {
//////                            //避免最后一个人，分配的保养太多，则把任务分配给其他人
//////                            avgNum += 1;
//////                        }
////                    }
////
////                    for (int i = 0; i < assetNeedMaintain.size(); i++) {
////                        MaintainData needToGenerate = assetNeedMaintain.get(i);
////                        MaintainData data = new MaintainData();
////                        String maintainCode = serialNumberService.generateMaxBusinessCode(schema, "maintain");
////                        data.setMaintainCode(maintainCode);
////                        data.setFacilityId(new Long(needToGenerate.getFacilityId()).intValue());
////                        data.setAssetId(needToGenerate.getAssetId());
////                        data.setAssetType(needToGenerate.getAssetType());
////                        data.setStatus(70);
////                        // 保养工单截止时间是创建工单的一周后，也就是本轮保养周期截止时间
////                        Long endTime = new Date().getTime() + 7 * (1000 * 3600 * 24);
////                        data.setDeadlineTime(new Timestamp(endTime));
////
////                        //生成保养结果
////                        net.sf.json.JSONArray array = new net.sf.json.JSONArray();
////                        if (maintainItemDataList != null && !maintainItemDataList.isEmpty()) {
////                            for (MaintainItemData itemData : maintainItemDataList) {
////                                if (itemData.getAsset_type_id().equals(needToGenerate.getAssetType())) {
////                                    net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(itemData);
////                                    array.add(jsonObject);
////                                }
////                            }
////                        }
////                        String resultJson = array.toString();
////                        data.setMaintainResult(resultJson);
////
////                        if (scheduleInSiteNum > 0) {
////                            User user = new User();
////                            //首先看设备有没有专门的负责人员，有的话先给他
////                            List<User> dutyUserList = assetDutyManService.findDutyUserByAssetId(schema, 2, needToGenerate.getAssetId(), new Long(needToGenerate.getFacilityId()).intValue());
////                            if (dutyUserList != null && dutyUserList.size() > 0) {
////                                int max = dutyUserList.size();
////                                int getInt = (int) (new Random().nextFloat() * max);
////                                user = dutyUserList.get(getInt);
////                            }
////                            if (user == null || user.getAccount() == null || user.getAccount().isEmpty()) {
////                                if (avgNum != 0 && i / avgNum < scheduleList.size()) {
////                                    // 能整除的部分平均分配
////                                    user = scheduleList.get(i / avgNum);
////                                } else {
////                                    // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
////                                    int last = 0;
////                                    int max = scheduleList.size();
////                                    if (isBigYu > 0) {
////                                        //随机取，无法避免同一次，取值重复问题
////                                        last = (int) (new Random().nextFloat() * max);
////                                    }
////                                    user = scheduleList.get(last);
////
////                                    //把集合中，已经选择的用户排斥掉
////                                    scheduleList.remove(last);
////                                }
////                                // 如果是按排班计划查出来的用户，只有账号信息，所以要重新查询
////                                //user = userService.getUserByAccount(schema, user.getAccount());
////                            }
////
////                            data.setMaintainAccount(user.getAccount());
////                            data.setReveiveTime(new Timestamp(new Date().getTime()));
////
////                            if (company != null && company.getIs_open_sms()) {
////                                DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
////                                String dateWord = dateFormat.format(new Timestamp(endTime));
////                                String content;
////                                if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
////                                    String pattern = "设备:{0}需进行保养，请于[{1}]前完成";
////                                    content = MessageFormat.format(pattern, needToGenerate.getAssetCode(), dateWord);
////                                } else {
////                                    String pattern = "{0,number,#}::{1}::{2}";
////                                    content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_GEN, needToGenerate.getAssetCode(), dateWord);
////                                }
////                                Message.beginMsg(schema, content, user.getMobile(), user.getAccount(), "maintain", maintainCode, user.getAccount());
////                            }
////
////                        }
////
////                        // 系统生成要如何写创建者
////                        data.setCreatetime(new Timestamp(new Date().getTime()));
////                        data.setCreate_user_account("system");
////                        data.setCreate_user_name("系统");
////
////                        maintainService.saveMaintain(schema, data);
////                        totalMaintainCount++;
////                    }
////                    logService.AddLog(schema, "system", "maintain_task", "保养任务自动生成总数：" + totalMaintainCount, "system");
////                }
////            }
////        } catch (Exception e) {
////            //系统错误日志
////            logService.AddLog(schema, "system", "maintain_task", "保养任务自动生成服务出错：" + e.getMessage(), "system");
////        }
////    }
////    第一版本的维保计划方法结束 */
//
//
//
///*    原始的设备监控服务，后来提到服务中，先注释，后期删除 2019-07-30 yzj 注
////    @Override
////    public void cronJobToCollectionMonitorData() {
////        String schema = "${schema_name}";
////        String toeknUrl = "http://www.shebeiyun.net/oauth2/access_token";//获取token的URL
////        String dataUrl = "";//获取数据的URL
////        String username = "499419483@qq.com";//用户名
////        String password = "19861228";//密码
////        String siteId = "";
////        //获取tonken所需要的参数
////        Map<String, String> toeknmap = new HashMap<String, String>() {
////            {
////                put("username", username);
////                put("password", password);
////                put("client_id", "000017953450251798098136");
////                put("client_secret", "08E9EC6793345759456CB8BAE52615F3");
////                put("grant_type", "password");
////                put("password_type", "1'");
////                put("language", "2'");
////            }
////        };
////        try {
////            List<Asset> assetList = assetDataServiceV2.findAll(schema);
////            List<String> codeList = new ArrayList<String>();
////            for (Asset asset : assetList) {
////                if (!RegexUtil.isNull(asset.getStrcode())) {
////                    codeList.add(asset.getStrcode());
////                }
////            }
////            Timestamp now = new Timestamp(System.currentTimeMillis());
////            String token = HttpRequestUtils.requestByPost(toeknUrl, toeknmap);
////            com.alibaba.fastjson.JSONObject tokenjson = com.alibaba.fastjson.JSONObject.parseObject(token);
////            //------------------------siteId场地id-------------token标识------------codeList设备编号集合
////            dataUrl = "http://www.shebeiyun.net/api/site_rt_data/5c3bf4ce81856300019c520b?access_token=" + tokenjson.getString("access_token") + "&varIds=" + codeList.toString() + "&verbose=200";
////            String resultValue = HttpRequestUtils.requestByGet(dataUrl);//发送请求返回字符串;
////            com.alibaba.fastjson.JSONObject resultData = com.alibaba.fastjson.JSONObject.parseObject(resultValue);//转换成json
////            JSONArray JsonArrays = JSONArray.fromObject(resultData.getString("vars"));//将数据数组转换成jsonArry
////            List<Map<String, Object>> mapListJson = (List) JsonArrays;//转换为List直接插入数据库
////            //将数据插入到监控表中
////
////
////        } catch (Exception e) {
////            logService.AddLog(schema, "system", "bom", "数据采集失败：" + e.getMessage(), "system");
////        }
////    }
//
//
////    对同步数据进行实时处理，即使更新到设备监控实时表中
////    @Override
////    @Scheduled(cron = "5 * * * * *")
////    public void cronJobToDealMonitorDataRealTime(){
////        String schema = "${schema_name}";
////        try {
////            int doSynchronize = assetInfoService.findNeedDealAssetMonitorListRealTime(schema);
////            if(doSynchronize<=0){
////                logService.AddLog(schema, "system", "asset_monitor", "设备监控实时数据同步出现错误" , "system");
////            }
////        }catch (Exception e){
////            //系统错误日志
////            logService.AddLog(schema, "system", "asset_monitor", "设备监控实时数据同步出现错误：" + e.getMessage(), "system");
////
////        }
////
////    }
////    原始的设备监控服务结束 */
//
//    /**
//     * 判定消息规则处理
//     *
//     * @param schema_name
//     */
//    public void messageRuleBusinessBusiness(String schema_name) {
//        try {
//            List<Map<String, Object>> messageRules = messageRuleService.queryMessageRules(schema_name);
//            if (messageRules.isEmpty()) {
//                return;
//            }
//            for (Map<String, Object> messageRule : messageRules) {
//                messageRuleService.messageRuleBusiness(schema_name, messageRule);
//            }
//        } catch (Exception e) {
//
//        }
//    }
//
//    /**
//     * 同步流程业务信息
//     *
//     * @param schema_name
//     */
//    public void syncFlowBusiness(String schema_name) {
//        int pageNumber = 1;
//        int pageSize = 100;
//        Long count = workflowService.getTasksWithoutConditionCount(schema_name);
//        List<Task> tasks;
//        while (count > pageSize * (pageNumber - 1)) {
//            tasks = workflowService.getTasksWithoutCondition(schema_name, pageSize, pageNumber);
//            for (Task task : tasks) {
//                try {
//                    String subWorkCode = (String) workflowService.getTaskVariableByName(schema_name, task.getId(), "sub_work_code");
//                    Object status = workflowService.getTaskVariableByName(schema_name, task.getId(), "status");
//                    flowBusinessService.renovateFlowBusinessData(schema_name, subWorkCode, task.getTaskDefinitionKey(), task.getFormKey(), String.valueOf(status), task.getAssignee());
//                } catch (Exception e) {
//
//                    continue;
//                }
//            }
//            pageNumber++;
//        }
//        try {
//            flowBusinessService.deleteInvalidFlowBusinessData(schema_name);
//        } catch (Exception e) {
//
//        }
//
//    }
//
//    /**
//     * 判定消息规则处理
//     *
//     * @param schema_name
//     */
//    public void monitorMessageBusinessBusiness(String schema_name) {
//        try {
//            List<Map<String, Object>> messages = messageCenterService.queryMessages(schema_name);
//            if (messages.isEmpty()) {
//                return;
//            }
//            for (Map<String, Object> message : messages) {
//                messageCenterService.sendMessage(schema_name, message);
//            }
//        } catch (Exception e) {
//
//        }
//    }
}
