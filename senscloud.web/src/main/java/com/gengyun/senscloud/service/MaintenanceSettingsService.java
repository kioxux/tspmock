package com.gengyun.senscloud.service;

public interface MaintenanceSettingsService {
//    List<RepairType> repairTypeData(String schema_name);
//
//    List<FaultType> faultType(String schema_name);
//
//    List<AssetStatus> assetStatus(String schema_name);
//
//    List<MaintainItemData> maintainItemData(String schema_name, String asset_name);
//
//    List<MaintainCheckItemData> maintainCheckItemData(String schema_name, String asset_name);
//
//    List<InspectionItemData> inspectionItemData(String schema_name);
//
//    List<SpotcheckItemData> spotcheckItemData(String schema_name);
//
//    List<MetaDataAsset> findAllMetaDataAsset(String schema_name);
//
//    List<MaintainCycleData> maintainCycleData(String schema_name, String asset_name);
//
//    List<MaintainCycleData> getMaintainCycleDataList(String schema_name);
//
//    List<FaultCode> faultCodeData(String schema);
//
//    List<PreventiveType> preventiveTypeData(String schema);
//
//    List<ImportmentLevel> importmentLevelData(String schema);
//
//    List<FacilityType> facilityTypeData(String schema);
//
//    List<UnitData> unitData(String schema);
//
//    /**
//     * 查询完修类型表
//     */
//    List<Map<String, Object>> getWorkFinishedTypeList(String schema_name);
//
//    int UpdaterepairTypeData(String schema_name, String type_name, int order, int id);
//
//    int InsertrepairTypeData(RepairType repairType);
//
//    int UpdateFaultType(String schema_name, String type_name, int order, int id);
//
//    int InsertFaultType(FaultType faultType);
//
//    int UpdateAssetStatus(String schema_name, String Status, int order, int id);
//
//    int InsertAssetStatus(AssetStatus assetStatus);
//
//    int UpdateMaintainItem(String schema_name, String item_name, int id, String check_note, int order);
//
//    int InsertMaintainItem(MaintainItemData maintainItemData);
//
//    int UpdateMaintainCheckItem(String schema_name, String item_name, int id, int order);
//
//    int InsertMaintainCheckItem(MaintainCheckItemData maintainCheckItemData);
//
//    int UpdateSpotcheck(String schema_name, String item_name, int id, String check_note, int order, int resultType);
//
//    int InsertInspection(InspectionItemData inspectionItemData);
//
//    int UpdateInspection(String schema_name, String item_name, int id, String check_note, int order, int resultType);
//
//    int InsertSpotcheck(SpotcheckItemData spotcheckItemData);
//
//    int UpdateMaintainCycleData(String schema_name, int cycle_count, int id, int order);
//
//    int InsertMaintainCycleData(MaintainCycleData maintainCycleData);
//
//    int updateFaultCode(String schema_name, String code_name, int order, String code, String fault_code,String assetCategorys, String remark);
//
//    int insertFaultCode(FaultCode faultCode, String assetCategorys);
//
//    int updatePreventiveType(String schema_name, String type_name, int order, int id);
//
//    int insertPreventiveType(PreventiveType preventiveType);
//
//    int updateImportmentLevel(String schema_name, String level_name, int order, int id);
//
//    int insertImportmentLevel(ImportmentLevel importmentLevel);
//
//    int updateFacilityType(String schema_name, String type_name, int id);
//
//    int insertFacilityType(FacilityType facilityType);
//
//    int updateUnitData(String schema_name, String unit_name, int id);
//
//    int insertUnitData(UnitData unitData);
//
//    /**
//     * 更新完修类型
//     */
//    int updateWorkFinishedTypeData(String schema_name, String finished_name, int is_finished, String operate_account, Timestamp operate_time, int id);
//
//    /**
//     * 插入完修类型
//     */
//    int insertWorkFinishedTypeData(String schema_name, String finished_name, int is_finished, String operate_account, Timestamp operate_time);
//
//    int deleteRepairData(String schema_name, int id, String isUse);
//
//    int deleteFaultData(String schema_name, int id, String isUse);
//
//    int deleteAssetStatusData(String schema_name, int id, String isUse);
//
//    int deleteMaintainData(String schema_name, int id);
//
//    int deleteMaintainCheckData(String schema_name, int id);
//
//    int deleteInspectionData(String schema_name, int id);
//
//    int deleteSpotcheckData(String schema_name, int id);
//
//    int deleteMaintainCycleData(String schema_name, int id);
//
//    int deleteFaultCodeData(String schema_name, String id, String isUse);
//
//    int deletePreventiveTypeData(String schema_name, int id, String isUse);
//
//    int deleteImportmentLevelData(String schema_name, int id, String isUse);
//
//    int deleteFacilityTypeData(String schema_name, int id, String isUse);
//
//    int deleteUnitData(String schema_name, int id, String isUse);
//
//    //设备运行状态
//    List<AssetRunningData> getAssetRunningDataList(String schema_name);
//
//    //设备运行状态最大id
//    int getAssetRunningMaxId(String schema_name);
//
//    //设备运行状态新增
//    int addAssetRunningData(String schema_name, AssetRunningData data);
//
//    //设备运行状态修改
//    int updateAssetRunningData(String schema_name, String running_status, int order, int id, int priority_level);
//
//    //设备运行状态删除
//    int disableAssetRunningData(String schema_name, int id, String isUse);
//
//    //工单类型
//    List<WorkTypeData> getWorkTypeDataList(String schema_name);
//
//    List<WorkTypeData> getWorkTypeDataListByBusiessTypeId(String schema_name, int business_type_id);
//
//    //工单类型最大id
//    int getWorkTypeMaxId(String schema_name);
//
//    //工单类型新增
//    int addWorkTypeData(String schema_name, WorkTypeData data);
//
//    //工单类型修改
//    int updateWorkTypeData(String schema_name, String type_name, String flow_template_code, String work_template_code, int business_type_id, int order, int id);
//
//    //启用、禁用工单类型
//    int disableWorkTypeData(String schema_name, int id, String isUse);
//
//    //文档类型状态
//    List<FileTypeData> getFileTypeDataList(String schema_name);
//
//    //文档类型最大id
//    int getFileTypeMaxId(String schema_name);
//
//    //文档类型新增
//    int addFileTypeData(String schema_name, FileTypeData data);
//
//    //文档类型修改
//    int updateFileTypeData(String schema_name, String running_status, int order, int id);
//
//    //启用、禁用文档类型
//    int disablFileTypeData(String schema_name, int id, String isUse);
//
//    //寻找所有有效的工单类型
//    List<WorkTypeData> getWorkTypeListInUse(String schema_name);
//
//    //查找工单状态
//    List<WorkStatus> getWorkStatus(String schema_name);
//
//    //查找工单业务
//    List<Map<String, Object>> findAllBusinessType(String schema_name);

}
