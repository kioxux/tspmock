package com.gengyun.senscloud.service.system.impl;

import com.gengyun.senscloud.service.system.CopyCallbackService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CopyCallbackServiceImpl implements CopyCallbackService {
    private Map<String, Object> result;

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    @Override
    public void callBackResult(List<Map<String, Object>> list) {
        Map<String, Object> result = new HashMap<>();
        if (!list.isEmpty()) {
            result.put("isChooseWorkTemplateCloumList", list);
        }
        setResult(result);
    }
}
