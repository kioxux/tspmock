package com.gengyun.senscloud.service.asset;

import com.gengyun.senscloud.model.AssetPositionModel;
import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 设备位置
 */
public interface AssetPositionService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAssetPositionListPermission(MethodParam methodParam);

    /**
     * 添加设备位置
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    void newAssetPosition(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑设备位置
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    void modifyAssetPosition(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除设备位置
     *
     * @param methodParam        入参
     * @param assetPositionModel 入参
     */
    void cutAssetPosition(MethodParam methodParam, AssetPositionModel assetPositionModel);

    /**
     * 获取设备位置详情
     *
     * @param methodParam        入参
     * @param assetPositionModel 入参
     */
    Map<String, Object> getAssetPositionInfo(MethodParam methodParam, AssetPositionModel assetPositionModel);

    /**
     * 查询设备位置列表
     *
     * @param methodParam 入参
     * @param paramMap    入参
     * @return 设备位置列表
     */
    List<Map<String, Object>> getAssetPositionList(MethodParam methodParam, Map<String, Object> paramMap);


    /**
     * 根据设备位置获取上级的平面图
     *
     * @param methodParam        入参
     * @param assetPositionModel 入参
     */
    Integer getSuperiorPositionPlan(MethodParam methodParam, AssetPositionModel assetPositionModel);

    /**
     * 根据设备位置获取子集和父级所有设备位置名称
     *
     * @param methodParam   入参
     * @param position_code 入参
     * @return 设备位置名称
     */
    StringBuilder getParentByPositionCode(MethodParam methodParam, String position_code);

    /**
     * 根据设备位置编码获取设备位置详情
     *
     * @param methodParam   入参
     * @param position_code 入参
     * @return 设备位置详情
     */
    Map<String, Object> getPositionInfoByCode(MethodParam methodParam, String position_code);

    /**
     * 验证设备位置是否存在
     *
     * @param schemaName       入参
     * @param positionCodeList 入参
     * @return 是否存在
     */
    Map<String, Map<String, Object>> queryPositionByPositionCodes(String schemaName, List<String> positionCodeList);

    /**
     * 根据主键展示设备位置二维码（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    void doShowAssetPositionQRCode(MethodParam methodParam);


    /**
     * 根据选中主键下载设备二维码
     *
     * @param methodParam 系统参数
     * @param asParam     请求参数
     */
    void doExportAssetPositionQRCode(MethodParam methodParam, AssetPositionModel asParam, Map<String, Object> pm);

    /**
     * 获取该设备位置可选择的任务模板列表
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 任务模板列表
     */
    List<Map<String, Object>> getChooseTaskTemplateListByPositionCode(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 获取该设备位置已选择的任务模板列表
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 任务模板列表
     */
    List<Map<String, Object>> getTaskTemplateListByPositionCode(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 新增设备位置和任务模板关联
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     */
    void newPositionCodeTemplate(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除设备位置和任务模板关联
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     */
    void cutPositionCodeTemplate(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 根据位置编码和工单类型获取任务模板列表
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 任务模板列表
     */
    List<Map<String, Object>> getTaskTemplateByPositionCodeAndWorkTypeId(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 根据位置编码获取该位置内的设备的故障状态
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 设备的故障状态
     */
    List<Map<String, Object>> getAssetRepairStatusByPositionCode(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 获取所有位置的设备故障数
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 设备的故障状态
     */
    List<Map<String, Object>> getAllPositionCodeAssetRepairStatusBy(MethodParam methodParam, Map<String, Object> pm);
}
