package com.gengyun.senscloud.service.system.impl;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.LogsMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.OtherCfgModel;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日志信息
 */
@Service
public class LogsServiceImpl implements LogsService {
    @Resource
    LogsMapper mapper;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    SelectOptionService selectOptionService;


    /**
     * 新增日志信息
     *
     * @param methodParam 系统参数
     * @param log_type    模块类别
     * @param business_no 业务数据编号
     * @param remark      日志信息
     */
    @Override
    public void newLog(MethodParam methodParam, String log_type, String business_no, String remark) {
        mapper.insertLog(methodParam.getSchemaName(), log_type, business_no, remark, methodParam.getUserId(), SenscloudUtil.getNowTime());
    }

    /**
     * 查询日志
     *
     * @param methodParam 系统参数
     * @param log_type    模块类别
     * @param business_no 业务数据编号
     * @return 日志列表
     */
    @Override
    public List<Map<String, Object>> getLog(MethodParam methodParam, String log_type, String business_no) {
        return RegexUtil.optNotBlankStrOpt(business_no).map(bn -> {
            String noneData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TEXT_AO); // 未知
            // TODO 时区处理
//        SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
            return mapper.findLog(methodParam.getSchemaName(), log_type, bn, noneData);
        }).orElse(null);
    }

    @Override
    public List<Map<String, Object>> getLogByLogType(MethodParam methodParam, String log_type, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        return RegexUtil.optNotBlankStrOpt(log_type).map(bn -> {
            return mapper.findLogByLogType(methodParam.getSchemaName(), bn, paramMap);
        }).orElse(null);
    }
    /**
     * 新增错误日志信息
     *
     * @param schemaName 数据库
     * @param error_code 错误编号
     * @param userId     当前登录用户ID
     * @param remark     备注
     */
    @Override
    public void newErrorLog(String schemaName, String error_code, String userId, String remark) {
        mapper.insertErrorLog(schemaName, error_code, remark, userId, SenscloudUtil.getNowTime());
    }

    /**
     * 查询错误日志列表
     *
     * @param methodParam 系统参数
     * @param ocModel     请求参数
     * @return 错误日志列表
     */
    @Override
    public Map<String, Object> getErrorLogList(MethodParam methodParam, OtherCfgModel ocModel) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        // 获取按条件拼接的sql
        StringBuffer whereString = new StringBuffer();
        RegexUtil.optNotBlankStrOpt(ocModel.getStartDateSearch()).ifPresent(e -> whereString.append(" and L.create_time >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
        RegexUtil.optNotBlankStrOpt(ocModel.getEndDateSearch()).ifPresent(e -> whereString.append(" and L.create_time <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
        RegexUtil.optNotBlankStrLowerOpt(ocModel.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower(U.user_name) like ").append(e)
                .append(" or lower(U.account) like ").append(e).append(" or lower(U.user_code) like ").append(e)
                .append(" or lower(L.error_code) like ").append(e).append(" or lower(L.remark) like ").append(e).append(")"));
        String searchWord = RegexUtil.optNotBlankStrOpt(whereString).map(s -> s.substring(4)).map(" where "::concat).orElse("");
        int total = mapper.countErrLogList(schemaName, searchWord); // 获取总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " ORDER BY l.ID DESC ";
            searchWord += pagination;
            List<Map<String, Object>> list = mapper.findErrLogList(methodParam.getSchemaName(), searchWord);
            RegexUtil.optNotNullList(list).ifPresent(l -> {
                JSONObject j = JSONObject.fromObject(RegexUtil.optStrOrVal(selectOptionService.getSelectOptionAttrByCode(methodParam, ErrorConstant.EC_DATA_TYPE, ErrorConstant.EC_START, "reserve1"), "{}"));
                list.forEach(d -> d.put("description", j.get(d.get("error_code"))));
            });
            result.put("rows", list);
        }
        result.put("total", total);
        return result;
    }
}
