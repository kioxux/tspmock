package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.TaskItemModel;

import java.util.List;
import java.util.Map;

public interface TaskItemService {
    /**
     * 获取任务项权限
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getTaskItemPermission(MethodParam methodParam);


    /**
     * 添加任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    void newTaskItem(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 编辑任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    void modifyTaskItem(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 删除任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    void cutTaskItem(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 获取任务项详情
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    Map<String, Object> getTaskItemInfo(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 分页查询任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     * @return 任务项列表
     */
    Map<String, Object> getTaskItemList(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 根据选中主键导出任务项信息（文件码）
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @param isAll         是否是全选
     * @return 文件码
     */
    String getExportTaskItem(MethodParam methodParam, TaskItemModel taskItemModel, boolean isAll);

    /**
     * 任务项选择列表
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     * @return 任务项列表
     */
    List<Map<String, Object>> getChoseTaskItemList(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 根据任务模板编码查询任务项列表
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     * @return 任务项列表
     */
    List<Map<String, Object>> getTaskItemListByTempLateCode(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 任务项附件处理
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @param isAdd         是否新增
     */
    void doModifyTaskItemFile(MethodParam methodParam, TaskItemModel taskItemModel, boolean isAdd);

    /**
     * 获取任务项附件文档信息
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @return 列表数据
     */
    List<Map<String, Object>> getTaskItemFileList(MethodParam methodParam, TaskItemModel taskItemModel);

    /**
     * 验证任务项编号是否存在
     *
     * @param schemaName
     * @param itemCodeList
     * @return
     */
    Map<String, Map<String, Object>> findItemsByItemCode(String schemaName, List<String> itemCodeList);

    /**
     * 验证任务项编号是否存在
     *
     * @param schemaName   数据库
     * @param itemCodeList 任务项编号
     * @return 任务项编号集合
     */
    List<String> getItemCodesByCodes(String schemaName, List<String> itemCodeList);

    /**
     * 批量新增或更新任务项
     *
     * @param schemaName 数据库
     * @param itemList   任务项信息
     */
    void batchInsertItem(String schemaName, List<Map<String, Object>> itemList);
}
