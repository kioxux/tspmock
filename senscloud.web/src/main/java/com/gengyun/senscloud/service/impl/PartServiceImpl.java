package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.PartService;
import org.springframework.stereotype.Service;

@Service
public class PartServiceImpl implements PartService {
//
//    @Autowired
//    private PartMapper partMapper;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private CommonUtilService commonUtilService;
//
//    /**
//     * 查询部位列表
//     *
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryPartList(String schema_name) {
//        return partMapper.queryPartList(schema_name);
//    }
//
//    /**
//     * 新增部位
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel addPart(String schemaName, HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String part_name = request.getParameter("part_name");
//        if(StringUtils.isBlank(part_name))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        AssetPartData assetPartData = new AssetPartData();
//        assetPartData.setPart_name(part_name);
//        assetPartData.setCreate_user_account(user.getAccount());
//        assetPartData.setCreatetime(UtilFuns.sysTimestamp());
//        assetPartData.setSchema_name(schemaName);
//        //保存部位
//        int result = partMapper.addPart(assetPartData);
//        if(result > 0)
//            return ResponseModel.ok(assetPartData.getId());//操作成功
//        else
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_BK));//操作失败
//    }
}
