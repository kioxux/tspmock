package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.CloudDataMapper;
import com.gengyun.senscloud.model.CloudDataModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.CloudDataService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 字典管理
 */
@Service
public class CloudDataServiceImpl implements CloudDataService {
    @Resource
    CloudDataMapper cloudDataMapper;
    @Resource
    LogsService logService;
    @Resource
    CompanyService companyService;

    /**
     * 新增数据项类型表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void newCloudDataType(MethodParam methodParam, Map<String, Object> pm) {
        Integer id = RegexUtil.optIntegerOrNull(pm.get("id"), LangConstant.MSG_G);//缺少必要条件
        RegexUtil.optStrOrExpNotNull(pm.get("data_type_name"), LangConstant.MSG_G);//缺少必要条件
        RegexUtil.optStrOrExpNotNull(pm.get("is_lang"), LangConstant.MSG_G);//缺少必要条件
        pm.put("create_user_id", methodParam.getUserId());
        if (!RegexUtil.optNotNull(pm.get("data_order")).isPresent()) {
            pm.put("data_order", 0);
        }
        Map<String, Object> oldMap = cloudDataMapper.findCloudDataTypeInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            throw new SenscloudException(LangConstant.MSG_H, new String[]{LangConstant.TITLE_CATEGORY_AH});//类型重复，请修改
        }
        cloudDataMapper.insertCloudDataType(methodParam.getSchemaName(), pm);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7010, pm.get("data_type").toString(), LangUtil.doSetLogArray("新增了数据项类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TAB_ASSET_A, pm.toString()}));
    }

    /**
     * 获取数据项类型列表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public List<Map<String, Object>> getCloudDataTypeList(MethodParam methodParam, Map<String, Object> pm) {
        if (RegexUtil.optNotNull(pm.get("keywordSearch")).isPresent()) {
            String searchWord = this.getWhereString(methodParam, pm);
            pm.put("searchWord", searchWord);
        }
//        return cloudDataMapper.findCloudDataTypeList(methodParam.getSchemaName(), pm);
        return findCloudDataTypeList(methodParam, pm);
    }

    /**
     * 获取字段列表判断条件
     *
     * @param methodParam
     * @param param
     * @return
     */
    private String getWhereString(MethodParam methodParam, Map<String, Object> param) {
        StringBuffer whereString = new StringBuffer();
//        RegexUtil.optNotBlankStrLowerOpt(param.get("keywordSearch")).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (lower((select (c.resource->>cdt.data_type_name)::jsonb->>'").append(methodParam.getUserLang()).append("' from public.company_resource c where c.company_id = ").append(methodParam.getCompanyId()).append(")) like ").append(e)
//                .append(" or lower(cdt.data_type) like ").append(e)
//                .append(" or lower(cdt.remark) like ").append(e)
//                .append(")"));
        return whereString.toString();
    }

    /**
     * 更新数据项类型表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void modifyCloudDataType(MethodParam methodParam, Map<String, Object> pm) {
        Integer id = RegexUtil.optIntegerOrNull(pm.get("id"), LangConstant.MSG_G);//缺少必要条件
        Map<String, Object> oldMap = cloudDataMapper.findCloudDataTypeInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            if (!RegexUtil.optNotNull(pm.get("data_order")).isPresent()) {
                pm.put("data_order", 0);
            }
            cloudDataMapper.updateCloudDataType(methodParam.getSchemaName(), pm);
            JSONArray loger = LangUtil.compareMap(pm, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7010, id.toString(), LangUtil.doSetLogArray("编辑了数据项类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TAB_ASSET_A, loger.toString()}));
        }
    }

    /**
     * 删除数据项类型表
     *
     * @param methodParam    入参
     * @param cloudDataModel 入参
     */
    @Override
    public void cutCloudDataType(MethodParam methodParam, CloudDataModel cloudDataModel) {
        Integer id = RegexUtil.optIntegerOrNull(cloudDataModel.getId(), LangConstant.MSG_G);//缺少必要条件
        Map<String, Object> oldMap = cloudDataMapper.findCloudDataTypeInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            //先判断该数据项类型是否有数据项
            List<Map<String, Object>> cloudDataList = cloudDataMapper.findCloudDataListByDataType(methodParam.getSchemaName(), oldMap);
            if (RegexUtil.optNotNull(cloudDataList).isPresent() && cloudDataList.size() > 0) {
                //该类型下还有数据 不能删除该数据项类型
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_ACM, LangConstant.TITLE_AAT_P, LangConstant.TITLE_ACM});
            }
            cloudDataMapper.deleteCloudDataType(methodParam.getSchemaName(), id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7010, id.toString(), LangUtil.doSetLogArray("删除了数据项类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_ASSET_A, oldMap.toString()}));
        }
    }

    /**
     * 根据数据项类型获取数据项列表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public List<Map<String, Object>> getCloudDataListByDataType(MethodParam methodParam, Map<String, Object> pm) {
        RegexUtil.optNotBlankStrOrExp(pm.get("data_type"), LangConstant.MSG_BI);//缺少必要条件
        return cloudDataMapper.findCloudDataListByDataType(methodParam.getSchemaName(), pm);
    }

    /**
     * 新增数据项表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void newCloudData(MethodParam methodParam, Map<String, Object> pm) {
        RegexUtil.optNotBlankStrOrExp(pm.get("data_type"), LangConstant.MSG_BI);//缺少必要条件
        RegexUtil.optNotBlankStrOrExp(pm.get("code"), LangConstant.MSG_BI);//缺少必要条件
        if (!RegexUtil.optNotNull(pm.get("data_order")).isPresent()) {
            pm.put("data_order", 0);
        }
        pm.put("create_user_id", methodParam.getUserId());
        cloudDataMapper.insertCloudData(methodParam.getSchemaName(), pm);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7010, pm.get("data_type").toString(), LangUtil.doSetLogArray("新增了数据项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TAB_ASSET_A, pm.toString()}));
    }

    /**
     * 更新数据项表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void modifyCloudData(MethodParam methodParam, Map<String, Object> pm) {
        Integer id = RegexUtil.optIntegerOrNull(pm.get("id"), LangConstant.MSG_BI);//缺少必要条件
        Map<String, Object> oldMap = cloudDataMapper.findCloudDataInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            if (!RegexUtil.optNotNull(pm.get("data_order")).isPresent()) {
                pm.put("data_order", 0);
            }
            cloudDataMapper.updateCloudData(methodParam.getSchemaName(), pm);
            JSONArray loger = LangUtil.compareMap(pm, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7010, oldMap.get("data_type").toString(), LangUtil.doSetLogArray("编辑了数据项类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TAB_ASSET_A, loger.toString()}));

        }

    }

    /**
     * 删除数据项表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    @Override
    public void cutCloudData(MethodParam methodParam, Map<String, Object> pm) {
        Integer id = RegexUtil.optIntegerOrNull(pm.get("id"), LangConstant.MSG_BI);//缺少必要条件
        Map<String, Object> oldMap = cloudDataMapper.findCloudDataInfo(methodParam.getSchemaName(), id);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            cloudDataMapper.deleteCloudData(methodParam.getSchemaName(), id);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7010, oldMap.get("data_type").toString(), LangUtil.doSetLogArray("删除了数据项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_ASSET_A, pm.toString()}));
        }
    }

    public List<Map<String, Object>> findCloudDataTypeList(MethodParam methodParam, Map<String, Object> pm) {
        List<Map<String, Object>> list = cloudDataMapper.findCloudDataTypeList(methodParam.getSchemaName(), pm);
        List<Map<String, Object>> listNew = new ArrayList<>();
        Map<String, Object> result = DataChangeUtil.scdObjToMap(companyService.requestLang(methodParam.getCompanyId(), "companyId", "findLangStaticDataByType"));
        if (RegexUtil.optIsPresentList(list) && RegexUtil.optNotBlankStrLowerOpt(pm.get("keywordSearch")).isPresent()) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                List<Map<String, Object>> finalList = new ArrayList<>();
                list.forEach(sd -> {
                    String value = "";
                    if (RegexUtil.optIsPresentStr(dd.get(sd.get("data_type_name")))) {
                        value = RegexUtil.optStrOrBlank(JSONObject.fromObject(dd.get(sd.get("data_type_name"))).get(methodParam.getUserLang()));
                    }
                    if (value.toLowerCase().indexOf(RegexUtil.optStrOrBlank(pm.get("keywordSearch")).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("data_type")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(pm.get("keywordSearch")).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("remark")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(pm.get("keywordSearch")).toLowerCase()) > -1) {
                        finalList.add(sd);
                    }
                });
                listNew = finalList;
            }
        } else {
            listNew = list;
        }
        return listNew;
    }
}
