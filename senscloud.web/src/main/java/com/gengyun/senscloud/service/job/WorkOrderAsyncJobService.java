package com.gengyun.senscloud.service.job;

public interface WorkOrderAsyncJobService {
    /**
     * 根据行事历生成相应的工单 —— 异步执行
     * @param schemaName
     */
    void asyncCronJobToGenerateWorkOrderByCalendar(String schemaName);
}
