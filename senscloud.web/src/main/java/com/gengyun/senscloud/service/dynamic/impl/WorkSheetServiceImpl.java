package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.WorkSheetService;
import org.springframework.stereotype.Service;

@Service
public class WorkSheetServiceImpl implements WorkSheetService {
//    private static final Logger logger = LoggerFactory.getLogger(WorkSheetServiceImpl.class);
//    @Autowired
//    WorkSheetMapper workSheetMapper;
//
//    @Autowired
//    LogsMapper loggerMapper;
//
//    @Autowired
//
//    UserMapper userMapper;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    FacilitiesMapper facilityMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    /**
//     * 工单列表计数
//     *
//     * @param schema_name
//     * @param condition
//     * @param dySqlMap
//     * @return
//     */
//    @Override
//    public int getWorkSheetListCount(String schema_name, String condition, Map<String, String> dySqlMap) {
//        return workSheetMapper.getWorkSheetListCount(schema_name, condition, dySqlMap);
//    }
//
//    public List<String> getWorkSheetSubWorkcodeList(String schema_name, String condition, int pageSize, int begin) {
//        return workSheetMapper.getWorkSheetSubWorkcodeList(schema_name, condition, pageSize, begin);
//    }
//
//    /**
//     * 查询工单列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param dySqlMap    [feeSelectSql:工单费用信息查询sql]
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Override
//    public List<WorkSheet> getWorkSheetList(String schema_name, String condition, Map<String, String> dySqlMap, int pageSize, int begin) {
//        return workSheetMapper.getWorkSheetList(schema_name, condition, dySqlMap, pageSize, begin);
//    }
//
//    @Override
//    public String findWorkSheetList(HttpServletRequest request) {
//        Boolean repairDistributionFlag = false;//维修分配权限
//        Boolean repairAuditFlag = false;//维修审核权限
//        Boolean maintainDistributionFlag = false;//保养分配权限
//        Boolean maintainAuditFlag = false;//保养审核权限
//        Boolean inspectionDistributionFlag = false;//巡检分配权限
//        Boolean inspectionAuditFlag = false;//巡检审核权限
//        Boolean spotDistributionFlag = false;//点检分配权限
//        Boolean spotAuditFlag = false;//点检审核权限
//        String schema_name = null;
//        try {
//            schema_name = authService.getCompany(request).getSchema_name();
//        } catch (Exception snExp) {
//            schema_name = WxUtil.doGetSchemaName();
//        }
//
//        JSONObject result = new JSONObject();
//        String pageSizeStr = request.getParameter("pageSize");
//        if (RegexUtil.isNull(pageSizeStr)) {
//            pageSizeStr = (String) request.getAttribute("pageSize");
//        }
//        String pageNumberStr = request.getParameter("pageNumber");
//        if (RegexUtil.isNull(pageNumberStr)) {
//            pageNumberStr = (String) request.getAttribute("pageNumber");
//        }
//        String work_code = request.getParameter("work_code");
//        String facilities = request.getParameter("facilities");
//        String positions = request.getParameter("positions");//获取设备位置
//        String org_type = request.getParameter("org_type");//获取组织类型
//        int pageSize = 0;
//        int pageNumber = 0;
//        if (!RegexUtil.isNull(pageSizeStr)) {
//            pageSize = Integer.parseInt(pageSizeStr);
//        }
//        if (!RegexUtil.isNull(pageNumberStr)) {
//            pageNumber = Integer.parseInt(pageNumberStr);
//        }
//        if (RegexUtil.isNull(work_code)) {
//            work_code = (String) request.getAttribute("work_code");
//        }
//        if (null != work_code && !"".equals(work_code)) {
//            pageSize = 1;
//            pageNumber = 1;
//            facilities = null;
//
//        }
//        String searchStatus = request.getParameter("status");
//        if (RegexUtil.isNull(searchStatus)) {
//            searchStatus = (String) request.getAttribute("status");
//        }
//        String keyWord = request.getParameter("keyWord");
//        if (RegexUtil.isNull(keyWord)) {
//            keyWord = (String) request.getAttribute("keyWord");
//        }
//        String beginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("beginTime");
//        String endTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("endTime");
//        String work_type_id = request.getParameter("type");
//        if (RegexUtil.isNull(work_type_id)) {
//            work_type_id = (String) request.getAttribute("type");
//        }
//        String workPool = request.getParameter("workPool");
//        if (RegexUtil.isNull(workPool)) {
//            workPool = (String) request.getAttribute("workPool");
//        }
//        String descStatus = request.getParameter("descStatus");
//        if (RegexUtil.isNull(descStatus)) {
//            descStatus = (String) request.getAttribute("descStatus");
//        }
//        String businessNo = request.getParameter("businessNo");
//        String asset_type = request.getParameter("asset_type");
//        String createBeginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("createBeginTime");
//        String createEndTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("createEndTime");
//        User user = authService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        List<WorkSheet> workDataList = null;
//        List<String> roles = null;
//        roles = new ArrayList<>(user.getRoles().keySet());
//        try {
//            //首页条件
//            String condition = "";
//            //根据人员的角色，判断获取值的范围
//            String account = user.getAccount();
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            Boolean hasFeeFlag = false; // 工单费用显示权限
//
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermissionByMenuKeys(schema_name, user.getId(), Arrays.asList(new String[]{"worksheet","repair", "maintain", "inspection", "spotcheck"}));
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                    if (functionData.getFunctionName().equals("repair_distribute")) {
//                        repairDistributionFlag = true;
//                    } else if (functionData.getFunctionName().equals("repair_audit_pc")) {
//                        repairAuditFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_distribute")) {
//                        maintainDistributionFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_audit_pc")) {
//                        maintainAuditFlag = true;
//                    } else if (functionData.getFunctionName().equals("inspection_distribute")) {
//                        inspectionDistributionFlag = true;
//                    } else if (functionData.getFunctionName().equals("inspection_audit")) {
//                        inspectionAuditFlag = true;
//                    } else if (functionData.getFunctionName().equals("spot_distribute")) {
//                        spotDistributionFlag = true;
//                    } else if (functionData.getFunctionName().equals("spot_audit")) {
//                        spotAuditFlag = true;
//                    } else if (functionData.getFunctionName().equals("work_fee")) {
//                        hasFeeFlag = true;
//                    }
//                }
//            }
//            Map<String, String> dySqlMap = new HashMap<String, String>(); // 按需动态sql
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//            boolean isPositionGuide = true;
//            if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//                isPositionGuide = false;
//            }
//            if (isPositionGuide) {
//                //组织权限
//                condition = SqlConditionUtil.getWorkOrgCondition(isAllFacility, isSelfFacility, "wdaof", condition, facilities, user.getFacilities(), account);
//                //位置权限
//                condition = SqlConditionUtil.getWorkPositionCondition(isAllFacility, isSelfFacility, "w", condition, positions, user.getAssetPositions(), account);
//                //没有权限只查看自己创建，或自己做的工单
//                if ((!isAllFacility && !isSelfFacility) || (!isAllFacility && isSelfFacility && "".equals(condition))) {
//                    condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + "')";
//                }
//            } else {
//                condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//            }
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(w.work_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%" + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or w2.receive_account like '%" + keyWord + "%' )";
//            }
//            if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("-1")) {
//                condition += " and (w.status in(" + searchStatus + ") ) ";
//            }
//            String[] assetTypes = null;
//            if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
////                condition += " and ma.id in(" + asset_type + ") ";
//                assetTypes = asset_type.split(",");
//            }
//            String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//            if (asset_type_word != null && !asset_type_word.isEmpty()) {
//                if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//                    condition += " and ma.id in (" + asset_type_word + ") ";
//                } else {
//                    condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//                }
//            }
//            if (org_type != null && !org_type.isEmpty() && !org_type.equals("-1")) {
//                condition += " and (wdaof.org_type =" + org_type + " ) ";
//            }
//            if (work_type_id != null && !work_type_id.isEmpty() && !work_type_id.equals("-1")) {
//                condition += " and (w.work_type_id = " + work_type_id + " ) ";
//            }
//            if (workPool != null && !workPool.isEmpty() && !workPool.equals("-1")) {
//                condition += " and (w.pool_id = " + workPool + " ) ";
//            }
//            if (RegexUtil.isNotNull(businessNo) && !"-1".equals(businessNo)) {
//                condition += " and (t.business_type_id = " + businessNo + " ) ";
//            }
//            if (StringUtils.isNotBlank(beginTime)) {
//                condition += " and w.occur_time >='" + beginTime + "'";
//            }
//            if (StringUtils.isNotBlank(endTime)) {
//                condition += " and w.occur_time <'" + endTime + "'";
//            }
//            if (StringUtils.isNotBlank(createBeginTime)) {
//                condition += " and w.create_time >='" + createBeginTime + "'";
//            }
//            if (StringUtils.isNotBlank(createEndTime)) {
//                condition += " and w.create_time <='" + createEndTime + "'";
//            }
//            if (null != work_code && !"".equals(work_code)) {
//                condition += " and w.work_code = '" + work_code + "' ";
//            }
//            // 费用sql
//            if (hasFeeFlag) {
//                dySqlMap.put(SqlConstant.SQL_WLF_SELECT_NAME, SqlConstant.SQL_WLF_SELECT);
//                dySqlMap.put(SqlConstant.SQL_WLF_TABLE_NAME, SqlConstant.SQL_WLF_TABLE.replace("${schema_name}", schema_name));
//                dySqlMap.put(SqlConstant.SQL_WLF_GROUP_COLUMN_NAME, SqlConstant.SQL_WLF_GROUP_COLUMN);
//                condition += SqlConstant.SQL_WLF_CONDITION;
//            }
//            int begin = pageSize * (pageNumber - 1);
//            int total = this.getWorkSheetListCount(schema_name, condition, dySqlMap);
//            if (!StringUtils.isEmpty(descStatus) && !StringUtils.isEmpty(descStatus) && !StringUtils.isEmpty(descStatus)) {
//                if (descStatus.equals("desc")) {
//                    condition += " order by w.occur_time desc ";
//                } else if (descStatus.equals("asc")) {
//                    condition += " order by w.occur_time asc ";
//                }
//            } else {
//                condition += " order by w.occur_time desc,w.work_code desc ";
//            }
//            workDataList = this.getWorkSheetList(schema_name, condition, dySqlMap, pageSize, begin);
//            if (null != work_code && !"".equals(work_code) && (null == workDataList || 0 == workDataList.size())) {
//                return "";
//            }
//            Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//            if (workDataList != null) {
//                List<String> subWorkCodes = new ArrayList<>();
//                workDataList.stream().forEach(workSheet -> subWorkCodes.add(workSheet.getSub_work_code()));
//                if (subWorkCodes.size() > 0) {
//                    subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, account, roles, false, "", "", "", pageSize, pageNumber);
//                }
//            }
//            for (WorkSheet worksheet : workDataList) {
//                CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(worksheet.getSub_work_code());
//                if (task != null && StringUtils.isNotBlank(task.getAssignee())) {
//                    worksheet.setIs_handle(task.getAssignee().equals(account));
//                    continue;
//                }
//                int business_type_id = worksheet.getBusiness_type_id();
//                //草稿状态直接看是不是当前创建的用户
//                if (worksheet.getStatus() == StatusConstant.DRAFT) {
//                    worksheet.setIs_handle(account.equals(worksheet.getCreate_user_account()));
//                }
//                //未分配状态
//                else if (worksheet.getStatus() == StatusConstant.UNDISTRIBUTED) {
//                    //如果是维修看他是否有维修分配的权限
//                    if (Constants.BUSINESS_TYPE_REPAIR_TYPE == business_type_id) {
//                        worksheet.setIs_handle(repairDistributionFlag);
//                    }
//                    //如果是保养看他是否有保养分配的权限
//                    else if (Constants.BUSINESS_TYPE_MAINTAIN_TYPE == business_type_id) {
//                        worksheet.setIs_handle(maintainDistributionFlag);
//                    }
//                    //如果是巡检看他是否有巡检分配的权限
//                    else if (Constants.BUSINESS_TYPE_INSPECTION_TYPE == business_type_id) {
//                        worksheet.setIs_handle(inspectionDistributionFlag);
//                    }//如果是点检看他是否有点检分配的权限
//                    else if (Constants.BUSINESS_TYPE_SPOT_TYPE == business_type_id) {
//                        worksheet.setIs_handle(spotDistributionFlag);
//                    }
//                }
//                //处理中状态直接看是不是当前分配的用户
//                else if (worksheet.getStatus() == StatusConstant.PENDING || worksheet.getStatus() == StatusConstant.PENDING_ORDER) {
//                    worksheet.setIs_handle(account.equals(worksheet.getReceive_account()));
//                }
//            }
//            //时区转换处理
//            SCTimeZoneUtil.responseObjectListDataHandler(workDataList);
//            result.put("rows", workDataList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "";
//        }
//    }
//
////    @Override
////    public WorkSheet findByID(String schema_name, String work_code) {
////        return workSheetMapper.findByID(schema_name, work_code);
////    }
//
//    @Override
//    public WorkListDetailModel finddetilByID(String schema_name, String sub_work_code) {
//        return workSheetMapper.finddetilByID(schema_name, sub_work_code);
//    }
//
//    /**
//     * 查询工单详情
//     * @param schema_name
//     * @param sub_work_code
//     * @return
//     */
//    @Override
//    public Map<String, Object> findWorkSheetDetailBySubWorkCode(String schema_name, String sub_work_code) {
//        return workSheetMapper.findWorkSheetDetailBySubWorkCode(schema_name, sub_work_code);
//    }
//
//    @Override
//    public int updateworkList(String schema_name, WorkListModel workListModel) {
//        return workSheetMapper.updateworkList(schema_name, workListModel);
//    }
//
//    @Override
//    public int updateWorkDetailList(String schema_name, WorkListDetailModel workListDetailModel) {
//        return workSheetMapper.updateWorkDetailList(schema_name, workListDetailModel);
//    }
//
//    @Override
//    public int updateworkSheet(String schema_name, WorkSheet workSheet) {
//        return workSheetMapper.updateworkSheet(schema_name, workSheet);
//    }
//
//    @Override
//    public List<WorkSheet> getWorkSheetListforcalendar(String schema_name, String condition) {
//        return workSheetMapper.getWorkSheetListforcalendar(schema_name, condition);
//    }
//
////    @Override
////    public List<WorkListDetailModel> getSubWorkList(String schema_name, String work_code) {
////        return workSheetMapper.getSubWorkList(schema_name, work_code);
////    }
//
//    @Override
//    public List<Map<String, Object>> getSubWorkMapList(String schema_name, String work_code) {
//        return workSheetMapper.getSubWorkMapList(schema_name, work_code);
//    }
//
//
//    //查找工单类型，及其工单总数
//    @Override
//    public List<WorkTypeData> getWorkTypeAndBillCountList(String schema_name, String condition) {
//        return workSheetMapper.getWorkTypeAndBillCountList(schema_name, condition);
//    }
//
//    //查找工单类型，及其工单总数
//    @Override
//    public int getRepairCountByAssetIdRecently(String schema_name, String asset_id, Timestamp current) {
//        return workSheetMapper.getRepairCountByAssetIdRecently(schema_name, asset_id, current);
//    }
//
//    //查找工单类型，及其工单总数
//    @Override
//    public int getRepairServiceCountByAssetIdRecently(String schema_name, String asset_id, Timestamp current) {
//        return workSheetMapper.getRepairServiceCountByAssetIdRecently(schema_name, asset_id, current);
//    }
//
//    /**
//     * 根据任务模板名称查询任务模板、任务项数据
//     *
//     * @param schema_name
//     * @param templateNames
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getTaskTemplateAndTaskItemByTemplateName(String schema_name, String templateNames) {
//        return workSheetMapper.getTaskTemplateAndTaskItemByTemplateName(schema_name, templateNames);
//    }
//
//    /**
//     * 查询工单事前、事后照片
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @return
//     */
//    @Override
//    public Map<String, Object> findWorkSheetImgBySubWorkCode(String schema_name, String sub_work_code) {
//        return workSheetMapper.findWorkSheetImgBySubWorkCode(schema_name, sub_work_code);
//    }
//
//
//    /**
//     * 修改工单信息（修改已完成工单的工单费用）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    public ResponseModel workSheetFix(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            //当前登录人有错误。
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));
//        }
//        //按钮类型
//        String do_flow_key = request.getParameter("do_flow_key");
//        String account = user.getAccount();
//        String subWorkCode = "";
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        //临时处理 通知报修人
//        map_object.put("sendAccount", account);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> works_detaile_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
//        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
//        subWorkCode = (String) works_detaile_tmp.get("sub_work_code");
//        Map<String, Object> dtlInfo = workSheetHandleService.queryBusinessDetailById(schema_name, subWorkCode);
//        String workRequestCode = "";
//        //TODO 只能修改完成工单其他状态的工单不能修改？作废？
//        try {
//            if (StatusConstant.COMPLETED != (Integer) dtlInfo.get("status")) {
//                return ResponseModel.errorMsg(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//            }
//            request.setAttribute("work_code", (String) works_tmp.get("work_code"));
//            if ("".equals(this.findWorkSheetList(request))) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//            }
//            Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schema_name, (String) works_tmp.get("work_code"));
//            workRequestCode = (String) workInfo.get("work_request_code");
//            map_object.put("work_request_code", workRequestCode);
//        } catch (Exception e) {
//            logger.info(subWorkCode + "：" + do_flow_key);
//        }
//        String workTypeId = String.valueOf(dtlInfo.get("work_type_id"));
//        String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString();// 业务类型
//        String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//        String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//        doType = selectOptionService.getLanguageInfo(doType);
//        //TODO 日志记录修改费用 对于修改其他字段的日志 需要补充
//        String logOperation = selectOptionService.getLanguageInfo(LangConstant.LOG_DO) + doType +selectOptionService.getLanguageInfo(LangConstant.WO_FEE)+ selectOptionService.getLanguageInfo(LangConstant.MOD_A);//进行了 保存
//        dataInfo.put("_sc_works", works_tmp);
//        dataInfo.put("_sc_works_detail", works_detaile_tmp);
//        map_object.put("dataInfo", dataInfo);
//        workSheetHandleService.doSaveData(schema_name, map_object);//保存新建工单
//        Map<String, Object> returnType = new HashMap<String, Object>(); // 保存时返回内容
//        returnType.put("type", "editWork");
//        logService.AddLog(schema_name, pageType, subWorkCode, logOperation, account);//记录历史
//        return ResponseModel.ok(returnType);
//    }
}
