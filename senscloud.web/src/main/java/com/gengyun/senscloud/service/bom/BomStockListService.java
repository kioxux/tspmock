package com.gengyun.senscloud.service.bom;

import java.util.List;
import java.util.Map;

/**
 * 备件清单
 */
public interface BomStockListService {
//    /**
//     * 获取备件清单列表
//     *
//     * @param request
//     * @param pageNumber 页码(第一页为0)
//     * @param pageSize   每页显示条数
//     * @return
//     */
//    JSONObject findBslList(String schemaName, HttpServletRequest request, User user, int pageNumber, int pageSize);
//
//    /**
//     * 根据主键查询备件清单数据
//     *
//     * @param schemaName
//     * @param code
//     * @return
//     */
//    Map<String, Object> queryBslById(String schemaName, String code);
//
//    // 查询库存安全数量过大或过小的备件
//    List<Map<String, Object>> queryBomOutOfSecurityQuantity(String schemaName);
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
//
//    /**
//     * 更新备件安全库存
//     *
//     * @param schemaName
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @param pageType
//     * @return
//     */
//    ResponseModel save(String schemaName, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception;
//
    /**
     * 验证库房是否存在
     *
     * @param schemaName
     * @param stockList
     * @return
     */
    Map<String, Map<String, Object>> queryStockByCode(String schemaName, List<String> stockList);

    /**
     * 验证备件类型是否存在
     *
     * @param schemaName
     * @param bomTypeList
     * @return
     */
    Map<String, Map<String, Object>> queryBomTypeByCode(String schemaName, List<String> bomTypeList);

    /**
     * 验证库房是否存在
     *
     * @param schemaName
     * @param bomUnitList
     * @return
     */
    Map<String, Map<String, Object>> queryBomUnitByCode(String schemaName, List<String> bomUnitList);

//    /**
//     * 验证备件是否存在
//     *
//     * @param schemaName
//     * @param bomCodeList
//     * @param materialCodeList
//     * @return
//     */
//    List<String> queryBomByInfo(String schemaName, List<String> bomCodeList, List<String> materialCodeList);
//
//
//    /**
//     * 验证备件型号是否存在
//     *
//     * @param schemaName
//     * @param bomModelList
//     * @return
//     */
//    List<String> queryBomModelByCode(String schemaName, List<String> bomModelList);
//
//    /**
//     * 验证备件库存是否存在
//     *
//     * @param schemaName
//     * @param stockCodeList
//     * @return
//     */
//    List<String> queryBisListByCode(String schemaName, List<String> bomCodeList, List<String> materialCodeList, List<String> stockCodeList);
//    /**
//     * 获取备件用途
//     *
//     * @param schemaName
//     * @return
//     */
//    Map<String, Map<String, Object>> queryBomUseList(String schemaName);
//
//    /**
//     * 导入新备件
//     *
//     * @param schemaName
//     * @param bomData
//     * @param account
//     */
//    void insertBom(String schemaName, LinkedHashMap bomData, String account);
//
//    /**
//     * 导入新备件型号
//     *
//     * @param schemaName
//     * @param bomData
//     * @param account
//     */
//    void insertBomModel(String schemaName, BisExcelMode bomData, String account);
//
//    /**
//     * 更新备件库存数量
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    void updateBomStockQuantity(String schemaName, Map<String, Object> paramMap);
//
//    /**
//     * 仅校验备件库存数量
//     *
//     * @param schemaName
//     * @param paramMap
//     * @param bomConsumeType
//     * @return
//     * @throws Exception
//     */
//    void checkBomStockQuantity(String schemaName, Map<String, Object> paramMap, String bomConsumeType);
}
