package com.gengyun.senscloud.service.dynamic;

import com.gengyun.senscloud.model.MethodParam;

import java.util.Map;

/**
 * 备件工单管理
 */
public interface BomWorksService {
    /**
     * 获取按钮功能权限信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getBwListPermission(MethodParam methodParam);

    /**
     * 查询备件工单列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 备件工单列表
     */
    Map<String, Object> getBomWorksList(MethodParam methodParam, Map<String, Object> pm);
}
