package com.gengyun.senscloud.service.impl;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.CustomersMapper;
import com.gengyun.senscloud.mapper.MeterMapper;
import com.gengyun.senscloud.mapper.PolluteFeeMapper;
import com.gengyun.senscloud.mapper.WorksheetDispatchMapper;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.service.CustomersService;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.asset.AssetMapService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 客户管理
 */
@Service
public class CustomersServiceImpl implements CustomersService {
    private static final Logger logger = LoggerFactory.getLogger(CustomersServiceImpl.class);

    @Resource
    CustomersMapper customersMapper;
    @Resource
    LogsService logService;
    @Resource
    ExportService exportService;
    @Resource
    private AssetMapService assetMapService;
    @Resource
    WorksheetDispatchMapper worksheetDispatchMapper;
    @Resource
    MeterMapper meterMapper;
    @Resource
    PolluteFeeMapper polluteFeeMapper;
    @Resource
    WorksService worksService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    FilesService filesService;
    @Resource
    WorkflowService workflowService;
    @Resource
    SystemConfigService systemConfigService;


    @Override
    public Map<String, Object> findCustomersList(MethodParam methodParam, Map<String, Object> param) {
        param.put("positionCode", treateStr((String) param.get("position_code")));
        param.put("polluteFeeStatus", treateStr((String) param.get("polluteFeeStatus")));
        param.put("importantLevel", treateStr((String) param.get("importantLevel")));
        param.put("customerType", treateStr((String) param.get("customerType")));
        param.put("unloadingGroup", treateStr((String) param.get("unloadingGroup")));
        param.put("sewage_destination", treateStr((String) param.get("sewage_destination")));
        param.put("collection_method", treateStr((String) param.get("collection_method")));
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = customersMapper.findCustomersListCount(methodParam.getSchemaName(), methodParam.getUserId(), param);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        param.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = customersMapper.findCustomersList(methodParam.getSchemaName(), methodParam.getUserId(), param);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    private String treateStr(String importantLevel) {
        if (!RegexUtil.optIsPresentStr(importantLevel))
            return importantLevel;
        String[] arr = importantLevel.split(",");
        String rs = "";
        for (int i = 0; i < arr.length; i++) {
            rs += "'" + arr[i] + "'";
            if (i < arr.length - 1)
                rs += ",";
        }
        return rs;
    }

    @Override
    public void newCustomer(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optStrOrExpNotNull(param.get("org_type_id"), LangConstant.TITLE_CATEGORY_AB_K);//组织类型不能为空
        param.put("is_use", '1');
        param.put("status", 1);
        param.put("create_user_id", methodParam.getUserId());
//        facilitiesMapper.InsertFacilities(methodParam.getSchemaName(), param);
//        Long id = (Long) param.get("id");
//        param.put("facility_id", param.get("id"));
        customersMapper.insertCustomer(methodParam.getSchemaName(), param);
        Long id = (Long) param.get("id");
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, id.toString(), LangUtil.doSetLogArray("添加了客户", LangConstant.TEXT_ADD_A, new String[]{LangConstant.TITLE_AJ_G}));//添加了客户
    }

    @Override
    public Map<String, Object> findById(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        Integer id = RegexUtil.optIntegerOrExpParam(facilitiesModel.getId(), LangConstant.TITLE_AJ_G);//不能为空
        return customersMapper.findByID(methodParam.getSchemaName(), id);
    }

    @Override
    public void modifyCustomers(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
//            facilitiesMapper.updateFacilities(methodParam.getSchemaName(), param);
            customersMapper.updateCustomers(methodParam.getSchemaName(), param);
            Map<String, Map<String, Object>> handleMap = new HashMap<>();
            handleMap.put("keyMap", new HashMap<String, Object>() {{
                put("title", LangConstant.TITLE_NAME_X);//客户名称
                put("important_level_id", LangConstant.TITLE_LEVEL_A);//重要级别
                put("customer_type_id", LangConstant.TITLE_ACEI);//客户类型
                put("unloading_group_id", LangConstant.TITLE_ACFH);//并管排污分组
                put("address", LangConstant.TITLE_PX);//所在地址
                put("position_code", LangConstant.TITLE_ACFI);//所在片区
                put("pipeline_code", LangConstant.TITLE_ACFJ);//管道编号
                put("sewage_destination", LangConstant.TITLE_ACFK);//污水去向
                put("main_products", LangConstant.TITLE_ACFL);//主要产品
                put("remark", LangConstant.TITLE_CK);//详细描述
                put("collection_method", LangConstant.TITLE_AEHN);//收集方式
                put("sewage_value", LangConstant.TITLE_BDFH);//排污阈值
                put("siren_value", LangConstant.TITLE_BDFI);//警号阈值
                put("abnormal_value", LangConstant.TITLE_BDFJ);//异常阈值
                put("sp_code", LangConstant.TITLE_BDEM);//数仓客户id
            }});
            handleMap.put("oldMap", new HashMap<String, Object>() {{
                put("important_level_id", handleData(methodParam, "customer_level", oldMap, "important_level_id"));
                put("customer_type_id", handleData(methodParam, "customer_type", oldMap, "customer_type_id"));
                put("unloading_group_id", handleData(methodParam, "unloading_group", oldMap, "unloading_group_id"));
                put("position_code", handleData(methodParam, "asset_position", oldMap, "position_code"));
                put("sewage_destination", handleData(methodParam, "sewage_destination", oldMap, "sewage_destination"));
                put("collection_method", handleData(methodParam, "collection_method", oldMap, "collection_method"));
            }});

            handleMap.put("newMap", new HashMap<String, Object>() {{
                put("important_level_id", handleData(methodParam, "customer_level", param, "important_level_id"));
                put("customer_type_id", handleData(methodParam, "customer_type", param, "customer_type_id"));
                put("unloading_group_id", handleData(methodParam, "unloading_group", param, "unloading_group_id"));
                put("position_code", handleData(methodParam, "asset_position", param, "position_code"));
                put("sewage_destination", handleData(methodParam, "sewage_destination", param, "sewage_destination"));
                put("collection_method", handleData(methodParam, "collection_method", param, "collection_method"));
            }});

            if (Objects.equals("facilitiesBase", param.get("sectionType"))) {
                //Todo 日志修改一下，定义比较字段
                if (Objects.equals("-1", param.get("unloading_group_id"))) {
                    param.put("unloading_group_id", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("collection_method"))) {
                    oldMap.put("collection_method", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("address"))) {
                    param.put("address", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("pipeline_code"))) {
                    param.put("pipeline_code", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("sewage_destination"))) {
                    param.put("sewage_destination", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("main_products"))) {
                    param.put("main_products", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("remark"))) {
                    param.put("remark", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("sewage_value"))) {
                    param.put("sewage_value", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("siren_value"))) {
                    param.put("siren_value", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("abnormal_value"))) {
                    param.put("abnormal_value", StringUtils.EMPTY);
                }
                if (!RegexUtil.optIsPresentStr(param.get("sp_code"))) {
                    param.put("sp_code", StringUtils.EMPTY);
                }
            }
            JSONArray log = compareMap(param, oldMap, new HashMap<String, Integer>() {{
                put("token", 1);
                put("sectionType", 1);
                put("id", 1);
                put("parent_id", 1);
                put("location", 1);
            }}, handleMap);
            if (log.size() > 0) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, param.get("id").toString(), LangUtil.doSetLogArray("编辑了客户", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AJ_G, log.toString()}));
            }
        }
    }

    @Override
    public List<Map<String, Object>> findRelationCustomersList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        String unloading_group_id = (String) oldMap.get("unloading_group_id");
        if (RegexUtil.optIsPresentStr(unloading_group_id)) {
            Map<String, Object> paramq = new HashMap<>();
            paramq.put("unloadingGroup", unloading_group_id);
            paramq.put("id", param.get("id"));
            paramq.put("keywordSearch", param.get("keywordSearch"));
            List<Map<String, Object>> rows = customersMapper.findRelationCustomersList(methodParam.getSchemaName(), paramq);
            return rows;
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> findRelationAssetList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        List<Map<String, Object>> rows = customersMapper.findRelationCusAssetList(methodParam.getSchemaName(), param);
        return rows;
    }

    @Override
    public List<Map<String, Object>> findPolluteFactorList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        param.put("facility_id", param.get("id"));
        List<Map<String, Object>> rows = customersMapper.findPolluteFactorList(methodParam.getSchemaName(), param);
        return rows;
    }

    @Override
    public void newPolluteFactor(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("facility_id"), LangConstant.TITLE_AJ_G);//不能为空
        if (Integer.valueOf(param.get("facility_id").toString()) != -1) {
            Map<String, Object> oldMap = customersMapper.findByID(methodParam.getSchemaName(), Integer.valueOf(param.get("facility_id").toString()));
            RegexUtil.optMapOrExp(oldMap, LangConstant.TITLE_DC);
        }
        RegexUtil.optStrOrExpNotNull(param.get("factor_id"), LangConstant.TITLE_BU);//因子不能为空
        RegexUtil.optStrOrExpNotNull(param.get("limit_value"), LangConstant.TITLE_BU);//限值不能为空
        List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew(customersMapper.findPolluteFactorList(methodParam.getSchemaName(), param));
        RegexUtil.trueExp(list.size() > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_AJK}); // 因子数据重复
        param.put("status", 1);
        param.put("create_user_id", methodParam.getUserId());
        String facility_id = String.valueOf(param.get("facility_id"));
        customersMapper.InsertPolluteFactor(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, facility_id, LangUtil.doSetLogArray("添加了污染因子", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TAB_AD, RegexUtil.optStrOrBlank(param.get("factor_id"))}));//添加了客户
    }

    @Override
    public void modifyPolluteFactor(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findPolluteFactorByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        RegexUtil.optMapOrExp(oldMap, LangConstant.TITLE_DC); //数据不存在
        RegexUtil.trueExp(!(oldMap.get("factor_id").equals(param.get("factor_id"))), LangConstant.TEXT_G, new String[]{LangConstant.TAB_AD, RegexUtil.optStrOrNull(param.get("factor_id"))}); //因子不可改
        RegexUtil.optStrOrExpNotNull(param.get("factor_id"), LangConstant.TAB_AD);//因子不能为空
        RegexUtil.optStrOrExpNotNull(param.get("limit_value"), LangConstant.TITLE_BU);//限值不能为空
        param.put("facility_id", oldMap.get("facility_id"));
        List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew(customersMapper.findPolluteFactorList(methodParam.getSchemaName(), param));
        RegexUtil.trueExp(list.size() > 1, LangConstant.MSG_H, new String[]{LangConstant.TITLE_AJK}); // 因子数据重复
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            customersMapper.updatePolluteFactor(methodParam.getSchemaName(), param);
            Map<String, Map<String, Object>> handleMap = new HashMap<>();
            handleMap.put("keyMap", new HashMap<String, Object>() {{
                put("limit_value", LangConstant.TITLE_ACHI);//纳管标准限制
                put("limit_unit", LangConstant.TITLE_BAAB_A);//单位
                put("remark", LangConstant.TITLE_AEIL);//依据
            }});
            if (!RegexUtil.optIsPresentStr(param.get("limit_value"))) {
                param.put("limit_value", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(param.get("limit_unit"))) {
                param.put("limit_unit", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(param.get("remark"))) {
                param.put("remark", StringUtils.EMPTY);
            }
            JSONArray log = compareMap(param, oldMap, new HashMap<String, Integer>() {{
                put("token", 1);
                put("id", 1);
                put("parent_id", 1);
            }}, handleMap);
            if (log.size() > 0) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, param.get("facility_id").toString(), LangUtil.doSetLogArray("编辑了污染因子" + param.get("factor_id"), LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TAB_AD, log.toString()}));
            }
        }
    }

    @Override
    public void deletePolluteFactor(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findPolluteFactorByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        RegexUtil.optMapOrExp(oldMap, LangConstant.TITLE_DC);
        param.put("status", StatusConstant.STATUS_DELETEED);
        List<Map<String, Object>> needHandleFacList = RegexUtil.optNotNullListOrNew(meterMapper.findMeterItemByCusAndFac(methodParam.getSchemaName(), oldMap));
        if (!needHandleFacList.isEmpty()) {
            meterMapper.batchUpdateMeterItemFac(methodParam.getSchemaName(), needHandleFacList);
        }
        customersMapper.updatePolluteFactor(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, oldMap.get("facility_id").toString(), LangUtil.doSetLogArray("删除了污染因子", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_AD, RegexUtil.optStrOrBlank(oldMap.get("factor_id"))}));
    }

    @Override
    public List<Map<String, Object>> findUnloadingTimeList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        param.put("facility_id", param.get("id"));
        List<Map<String, Object>> rows = customersMapper.findUnloadingTimeList(methodParam.getSchemaName(), param);
        return rows;
    }

    @Override
    public void newUnloadingTime(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("facility_id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findByID(methodParam.getSchemaName(), Integer.valueOf(param.get("facility_id").toString()));
        RegexUtil.optMapOrExp(oldMap, LangConstant.TITLE_DC);
        RegexUtil.optStrOrExpNotNull(param.get("end_time"), LangConstant.TITLE_AL_K);//时间不能为空
        RegexUtil.optStrOrExpNotNull(param.get("begin_time"), LangConstant.TITLE_AK_H);
        param.put("status", 1);
        param.put("create_user_id", methodParam.getUserId());
        String facility_id = String.valueOf(param.get("facility_id"));
        customersMapper.InsertUnloadingTime(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, facility_id, LangUtil.doSetLogArray("添加了排污时间", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_ACEL, "：".concat(RegexUtil.optStrOrNull(param.get("begin_time"))).concat("-").concat(RegexUtil.optStrOrNull(param.get("end_time")))}));
    }

    @Override
    public void modifyUnloadingTime(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findUnloadingTimeByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        RegexUtil.optMapOrExp(oldMap, LangConstant.TITLE_DC);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            customersMapper.updateUnloadingTime(methodParam.getSchemaName(), param);
            Map<String, Map<String, Object>> handleMap = new HashMap<>();
            handleMap.put("keyMap", new HashMap<String, Object>() {{
                put("begin_time", LangConstant.TITLE_ACHJ);//排污开始时间
                put("end_time", LangConstant.TITLE_ACHK);//排污结束时间
            }});
            JSONArray log = compareMap(param, oldMap, new HashMap<String, Integer>() {{
                put("token", 1);
                put("id", 1);
                put("parent_id", 1);
            }}, handleMap);
            if (log.size() > 0) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, param.get("facility_id").toString(), LangUtil.doSetLogArray("编辑了排污时间", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_ACEL, log.toString()}));
            }
        }
    }

    @Override
    public void deleteUnloadingTime(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//不能为空
        Map<String, Object> oldMap = customersMapper.findUnloadingTimeByID(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        RegexUtil.optMapOrExp(oldMap, LangConstant.TITLE_DC);
        param.put("status", StatusConstant.STATUS_DELETEED);
        customersMapper.updateUnloadingTime(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, oldMap.get("facility_id").toString(), LangUtil.doSetLogArray("删除了排污时间", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ACEL, "：".concat(RegexUtil.optStrOrNull(oldMap.get("begin_time"))).concat("-").concat(RegexUtil.optStrOrNull(oldMap.get("end_time")))}));
    }

    @Override
    public String getExportCustomer(MethodParam methodParam, CustomerModel asParam) {
        String ids = RegexUtil.optStrOrExpNotNull(asParam.getIds(), LangConstant.MSG_A);
        String[] idList = RegexUtil.optNotBlankStrOpt(ids).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
        Map<String, Object> map = new HashMap();
        map.put("ids", idList);
        List<Map<String, Object>> list = customersMapper.findCustomersList(methodParam.getSchemaName(), methodParam.getUserId(), map);
        return exportService.doDownloadCustomerInfo(methodParam, list);
    }

    @Override
    public String getExportAllCustomer(MethodParam methodParam, CustomerModel asParam) {
        Map<String, Object> map = new HashMap();
        // "keywordSearch", "isUseSearch", "orgTypeSearch","importantLevel","customerType","unloadingGroup","isPaid"
        map.put("keywordSearch", asParam.getKeywordSearch());
        map.put("isUseSearch", asParam.getIsUseSearch());
        map.put("orgTypeSearch", asParam.getOrgTypeSearch());
        map.put("importantLevel", asParam.getImportantLevel());
        map.put("customerType", asParam.getCustomerType());
        map.put("unloadingGroup", asParam.getUnloadingGroup());
        map.put("isPaid", asParam.getIsPaid());

        List<Map<String, Object>> list = customersMapper.findCustomersList(methodParam.getSchemaName(), methodParam.getUserId(), map);
        return exportService.doDownloadCustomerInfo(methodParam, list);
    }

    @Override
    public List<Map<String, Object>> findAllCustomerPositionForMap(MethodParam methodParam, Map<String, Object> paramMap) {
        List<Map<String, Object>> customers = RegexUtil.optNotNullListOrNew(customersMapper.findCustomerPositionForMapList(methodParam.getSchemaName(), methodParam.getUserId(), new HashMap<String, Object>()));
        WorkflowSearchParam wfParam = new WorkflowSearchParam();
        wfParam.setMine_only(true);
        Map<String, Object> map = workflowService.searchTaskList(methodParam, wfParam);
        for (Map<String, Object> item : customers) {
            int count = RegexUtil.optNotNullListOrNew(map.get("rows")).stream().filter(e -> Objects.equals(item.get("facility_id"), e.get("relation_id"))).collect(Collectors.toList()).size();
            item.put("works", count);
            try {
                //水质水量数据
                findRealSZInfo(methodParam, item);
            } catch (Exception e) {
                logger.error("获取水质水量数据异常：{}", e.toString());
            }
            //排污状态
            findRealPWInfo(methodParam, item);
        }
        return customers;
    }

    private void findRealPWInfo(MethodParam methodParam, Map<String, Object> item) {
        List<Map<String, Object>> assetList = customersMapper.findAssetForMapByCusList(methodParam.getSchemaName(), item);
        RegexUtil.optNotNullList(assetList).ifPresent(list -> list.forEach(pw -> {
            if (RegexUtil.optIsPresentStr(pw.get("sid_url")) || (RegexUtil.optIsPresentStr(pw.get("grm")) && RegexUtil.optIsPresentStr(pw.get("pass")))) {
                List<String> exDataList = exData(RegexUtil.optStrOrBlank(pw.get("sid_url")), methodParam.getSchemaName(), pw);
                if (exDataList.size() > 0 && exDataList.contains("ERROR")) {
                    if (RegexUtil.optIsPresentStr(pw.get("grm")) && RegexUtil.optIsPresentStr(pw.get("pass"))) {
                        String exDataUrl = this.authYunplc(methodParam.getSchemaName(), pw);
                        if (RegexUtil.optIsPresentStr(exDataUrl)) {
                            pw.put("sid_url", exDataUrl);
                            meterMapper.updateCusAssetSid(methodParam.getSchemaName(), pw);
                            exDataList = this.exData(exDataUrl, methodParam.getSchemaName(), pw);
                        }
                    }
                }
                //排污阈值
                BigDecimal sewage_value = new BigDecimal(RegexUtil.optStrOrVal(pw.get("sewage_value"), "0"));
                //警告阈值
                BigDecimal siren_value = new BigDecimal(RegexUtil.optStrOrVal(pw.get("siren_value"), "0"));
                //异常阈值
                BigDecimal abnormal_value = new BigDecimal(RegexUtil.optStrOrVal(pw.get("abnormal_value"), "0"));
                //瞬间流量
                BigDecimal volume = new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 6), "0"));
                pw.put("real_volume", volume);
                /**
                 * 瞬间流量大于排污阈值小于警告阈值时 为正常排污状态，值为1
                 * 瞬间流量大于警告阈值小于异常阈值时 为警告状态，值为2
                 * 瞬间流量大于异常阈值时 为异常状态，值为3
                 * */
                if (volume.compareTo(sewage_value) > 0 && volume.compareTo(siren_value) < 0) {
                    //正常排污
                    pw.put("realPW", 1);
                } else if (volume.compareTo(siren_value) > 0 && volume.compareTo(abnormal_value) < 0) {
                    //警告状态
                    pw.put("realPW", 2);
                } else if (volume.compareTo(abnormal_value) > 0) {
                    pw.put("realPW", 3);
                } else {
                    pw.put("realPW", 0);
                }
            }
        }));
        item.put("assetSewage", assetList);
    }

    private void findRealSZInfo(MethodParam methodParam, Map<String, Object> item) {
        RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "sc_url")).ifPresent(url -> {
            //数仓请求地址
            List<Map<String, Object>> positons = RegexUtil.optNotNullListOrNew(customersMapper.findAllAssetForMapList(methodParam.getSchemaName(), item));
            for (Map<String, Object> asset : positons) {
                // 实时获取排污口水量及水质数值
                List<Map<String, Object>> cusFactorList = RegexUtil.optNotNullListOrNew(customersMapper.findPolluteFactorList(methodParam.getSchemaName(), new HashMap<String, Object>() {{
                    put("facility_id", asset.get("cus_id"));
                }}));
                if (!cusFactorList.isEmpty()) {
                    List<Map<String, Object>> realSZ = new ArrayList<>();
                    List<Map<String, Object>> cisPKSZDayList = RegexUtil.optNotNullListOrNew(JSON.parseObject(remoteCall("findRealSZInfoList", url, asset), List.class));
                    for (Map<String, Object> fac : cusFactorList) {
                        for (Map<String, Object> sz : cisPKSZDayList) {
                            if (Objects.equals(fac.get("factor_id"), sz.get("code")) &&
                                    RegexUtil.optBigDecimalOrVal(fac.get("limit_value"), "0", null)
                                            .compareTo(RegexUtil.optBigDecimalOrVal(sz.get("monitorAvgValue"), "0", null)) == -1) {
                                sz.put("limit_value", fac.get("limit_value"));
                                sz.put("limit_unit", fac.get("limit_unit"));
                                realSZ.add(sz);
                                break;
                            }
                        }
                    }
                    if (!realSZ.isEmpty()) {
                        asset.put("realSZ", realSZ);
                    }
                }
            }
            item.put("positions", positons);
        });
    }

    private String authYunplc(String schemaName, Map<String, Object> param) {
        return RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(schemaName, "opc_url")).map(opc_url -> {
            //opc请求地址
            String result = HttpUtil.post(opc_url.concat("exlog"), param);
            logger.info("opc授权接口请求响应结果为：{}", result);
            List<String> list = RegexUtil.optNotBlankStrOpt(result).map(e -> Arrays.asList(e.split("\r\n"))).orElse(new ArrayList<>());
            StringBuffer questUrl = new StringBuffer("http://");
            for (String item : list) {
                if (item.contains("ADDR=")) {
                    questUrl.append(item.replace("ADDR=", "")).append("/exdata?");
                } else if (item.contains("SID=") && questUrl.toString().contains("exdata")) {
                    questUrl.append(item).append("&OP=R");
                }
            }
            return questUrl.toString();
        }).orElse(null);
    }

    private List<String> exData(String url, String schemaName, Map<String, Object> handMap) {
        String param = "9\r\nEV1_Remote\r\nEV1_ON\r\nEV1_OFF\r\nPIT01\r\nFIT01_Volume\r\nFIT01_Acc_tod\r\nFIT01_Acc_yes\r\nFIT01_Acc_sum\r\nEV1_G_Fault";
        String result = null;
        try {
            result = HttpRequest.post(url)
                    .header("Content-Type", "text/plain")
                    .body(param)
                    .execute().body();
        } catch (Exception e) {
            logger.error("opc实时接口请求失败，失败原因：{}", e);
            if (RegexUtil.optIsPresentStr(handMap.get("grm")) && RegexUtil.optIsPresentStr(handMap.get("pass"))) {
                String exDataUrl = this.authYunplc(schemaName, handMap);
                if (RegexUtil.optIsPresentStr(exDataUrl)) {
                    handMap.put("sid_url", exDataUrl);
                    meterMapper.updateCusAssetSid(schemaName, handMap);
                }
            }
        }
        logger.info("opc实时接口请求响应结果为：{}", result);
        return RegexUtil.optNotBlankStrOpt(result).map(e -> Arrays.asList(e.split("\r\n"))).orElse(new ArrayList<>());
    }

    private String getValueByIndex(List<String> list, int index) {
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }

    /**
     * 客户地图数据明细
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findCustomerMapDetail(MethodParam methodParam, Map<String, Object> paramMap) {
        String id = String.valueOf(RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_AJ_G));//不能为空
        String[] idList = RegexUtil.optNotBlankStrOpt(id).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
        Map<String, Object> customer = customersMapper.findByID(methodParam.getSchemaName(), Integer.valueOf(String.valueOf(paramMap.get("id"))));
        int polluteCount = customersMapper.findPolluteCustomerCount(methodParam.getSchemaName(), idList);
        customer.put("pollute_fee_status", polluteCount > 0 ? 0 : 1);
        WorkflowSearchParam wfParam = new WorkflowSearchParam();
        wfParam.setMine_only(true);
        methodParam.setNeedPagination(true);
        Map<String, Object> map = workflowService.searchTaskList(methodParam, wfParam);
        List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew(map.get("rows")).stream().filter(e -> Objects.equals(id, e.get("relation_id"))).collect(Collectors.toList());
        paramMap.put("pagination", SenscloudUtil.changePagination(methodParam));
        List<Map<String, Object>> assetList = customersMapper.findRelationCusAssetList(methodParam.getSchemaName(), paramMap);
        customer.put("works", list);
        customer.put("cus_asset", assetList);
        return customer;
    }

    /**
     * 客户历史排污量
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findCustomerCheckMeterHistory(MethodParam methodParam, Map<String, Object> paramMap) {
        String id = String.valueOf(RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_AJ_G));//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_time"), LangConstant.DP_M);//时间不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_time"), LangConstant.TITLE_AK_H);
        return meterMapper.findCustomerMeterHistory(methodParam.getSchemaName(), paramMap);
    }

    @Override
    public List<WorkListModel> findCustomerWorksHistory(MethodParam methodParam, Map<String, Object> paramMap) {
        String id = String.valueOf(RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_AJ_G));//不能为空
        StringBuffer whereString = new StringBuffer("");
        whereString.append(" and w.relation_type = 4").append(" and wd.relation_id = '").append(id).append("' ");
        String condition = whereString.toString();
//        List<WorkListModel> workListModels = worksheetDispatchMapper.findWorksheetList(methodParam.getSchemaName(), methodParam.getUserId(), condition);
        List<WorkListModel> workListModels = cacheUtilService.findWorksheetList(methodParam.getSchemaName(), methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), condition, StringUtils.EMPTY);
        return workListModels;
    }

    /**
     * 客户排污量柱状图
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findCustomerCheckMeterPic(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_time"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_time"), LangConstant.TITLE_DATE_D);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("date_type"), LangConstant.TITLE_ACE);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("id"), LangConstant.TITLE_AJ_G);//客户不能为空
        List<Map<String, Object>> dateMap = getDateMap(String.valueOf(paramMap.get("date_type")));
        List<Map<String, Object>> actualMap = new ArrayList<>();
        if ("year".equals(paramMap.get("date_type")))
            actualMap = meterMapper.findCustomerMeterPicYear(methodParam.getSchemaName(), paramMap);
        else
            actualMap = meterMapper.findCustomerMeterPicMonth(methodParam.getSchemaName(), paramMap);
        Map<String, Object> toMap = new HashMap();
        RegexUtil.optNotNullListOrNew(actualMap).stream().forEach(e -> {
            toMap.put(String.valueOf(e.get("days")), e.get("flow"));
        });
        dateMap.forEach(e -> {
            if (toMap.containsKey(e.get("days"))) {
                e.put("flow", toMap.get(e.get("days")));
            }
        });
        return dateMap;
    }

    /**
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findCustomerCheckMeterList(MethodParam methodParam, Map<String, Object> paramMap) {
        String id = String.valueOf(RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_AJ_G));//不能为空
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam); // 分页参数
        paramMap.put("pagination", pagination);
        return meterMapper.findCustomerMeterList(methodParam.getSchemaName(), paramMap);
    }

    /**
     * 查询客户缴费记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findCustomerPolluteFeeList(MethodParam methodParam, Map<String, Object> paramMap) {
        String id = String.valueOf(RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_AJ_G));//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_time"), LangConstant.DP_M);//时间不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_time"), LangConstant.TITLE_AK_H);
        return polluteFeeMapper.findCustomerPolluteFeeList(methodParam.getSchemaName(), paramMap);
    }

    /**
     * 客户设备地图
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findAllCustomerAssetPositionForMap(MethodParam methodParam, Map<String, Object> paramMap) {
        //数仓请求地址
        String sc_url = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "sc_url");
        List<Map<String, Object>> positons = RegexUtil.optNotNullListOrNew(customersMapper.findAllCustomerPositionForMapList(methodParam.getSchemaName(), methodParam.getUserId(), paramMap));
        for (Map<String, Object> item : positons) {
            // 实时获取排污口水量及水质数值
            List<Map<String, Object>> cusFactorList = RegexUtil.optNotNullListOrNew(customersMapper.findPolluteFactorList(methodParam.getSchemaName(), new HashMap<String, Object>() {{
                put("facility_id", item.get("cus_id"));
            }}));
            if (!cusFactorList.isEmpty()) {
                List<Map<String, Object>> realSZ = new ArrayList<>();
                List<Map<String, Object>> cisPKSZDayList = RegexUtil.optNotNullListOrNew(JSON.parseObject(remoteCall("findRealSZInfoList", sc_url, item), List.class));
                for (Map<String, Object> fac : cusFactorList) {
                    for (Map<String, Object> sz : cisPKSZDayList) {
                        if (Objects.equals(fac.get("factor_id"), sz.get("code")) &&
                                RegexUtil.optBigDecimalOrVal(fac.get("limit_value"), "0", null)
                                        .compareTo(RegexUtil.optBigDecimalOrVal(sz.get("monitorAvgValue"), "0", null)) == -1) {
                            sz.put("limit_value", fac.get("limit_value"));
                            sz.put("limit_unit", fac.get("limit_unit"));
                            realSZ.add(sz);
                            break;
                        }
                    }
                }
                if (!realSZ.isEmpty()) {
                    item.put("realSZ", realSZ);
                }
            }
            // 实时获取排污口状态
        }
        return positons;
    }

    @Override
    public List<Map<String, Object>> findRelationAssetPositionForMap(MethodParam methodParam, Map<String, Object> paramMap) {
        List<Map<String, Object>> assetList = RegexUtil.optNotNullListOrNew(findRelationAssetList(methodParam, paramMap));
        Set<String> positions = assetList.stream().map(e -> (String) e.get("position_code")).collect(Collectors.toSet());
        Map<String, Object> assetIds = new HashMap<String, Object>();
        assetList.stream().forEach(e -> {
            assetIds.put(String.valueOf(e.get("id")), 1);
        });
        List<Map<String, Object>> maps = new ArrayList();
        SearchParam searchParam = new SearchParam();
        if (positions != null && !positions.isEmpty()) {
            positions.stream().forEach(e -> {
                searchParam.setPositionCode(e);
                maps.addAll(((List<Map<String, Object>>) assetMapService.getAssetAndAssetPositionForMap(methodParam, searchParam).get("list"))
                        .stream().filter(s ->
                                assetIds.containsKey(s.get("id"))
                        ).collect(Collectors.toList()));
            });
        }
        return maps;
    }

    /**
     * 初始化当月每天或当年每月的map
     *
     * @param dayType
     * @return
     */
    private List<Map<String, Object>> getDateMap(String dayType) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String months = String.valueOf(cal.get(Calendar.MONTH) + 1);
        cal.set(Calendar.DATE, 1);
        cal.roll(Calendar.DATE, -1);
        int maxDate = cal.get(Calendar.DATE);
        if ("year".equals(dayType)) {
            for (int i = 1; i <= 12; i++) {
                Map<String, Object> map = new HashMap();
                String month = String.valueOf(i < 10 ? "0" + i : i);
                map.put("days", year + "-" + month);
                map.put("flow", 0);
                mapList.add(map);
            }
        } else if ("month".equals(dayType)) {
            for (int i = 1; i <= maxDate; i++) {
                Map<String, Object> map = new HashMap();
                String month = Integer.parseInt(months) < 10 ? "0" + months : months;
                String day = String.valueOf(i < 10 ? "0" + i : i);
                map.put("days", year + "-" + month + "-" + day);
                map.put("flow", 0);
                mapList.add(map);
            }
        }
        return mapList;
    }


    /**
     * 对比新的参数map1和map2的区别
     *
     * @param map1 新值
     * @param map2 旧值
     * @return 对比信息
     */
    public JSONArray compareMap(Map map1, Map map2, Map filters, Map handleMap) {
        JSONArray logArr = new JSONArray();
        Map<String, Object> keyMap = DataChangeUtil.scdObjToMapOrNew(handleMap.get("keyMap"));
        Map<String, Object> oldMap = DataChangeUtil.scdObjToMapOrNew(handleMap.get("oldMap"));
        Map<String, Object> newMap = DataChangeUtil.scdObjToMapOrNew(handleMap.get("newMap"));
        boolean contain;
        for (Object o : map1.keySet()) {
            if (!filters.containsKey(o)) {
                contain = map2.containsKey(o);
                if (RegexUtil.optIsPresentStr(map1.get(o)) || StringUtils.EMPTY.equals(map1.get(o))) {
                    if (contain) {
                        contain = RegexUtil.optStrOrBlank(String.valueOf(map1.get(o)))
                                .equals(RegexUtil.optStrOrBlank(String.valueOf(map2.get(o))));
                    }
                    if (!contain) {
                        logArr.add(LangUtil.doSetLogArray("属性更新", LangConstant.LOG_A, new String[]{RegexUtil.optStrOrVal(keyMap.get(o.toString()), o.toString()), RegexUtil.optStrOrVal(oldMap.get(o.toString()), String.valueOf(map2.get(o))), RegexUtil.optStrOrVal(newMap.get(o.toString()), String.valueOf(map1.get(o)))}));
                    }
                }
            }
        }
        return logArr;
    }


    @Override
    public void removeCustomer(MethodParam methodParam, Map<String, Object> paramMap) {
        String ids = RegexUtil.optStrOrExpNotNull(paramMap.get("ids"), LangConstant.MSG_A);
        String[] idList = RegexUtil.optNotBlankStrOpt(ids).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
        List<String> nameList = customersMapper.findCusNamesByIds(methodParam.getSchemaName(), idList);
        // 销户执行判断条件：1.在用户申请销户前必须缴清所有欠费记录 2.销户审核前，必须拆掉名下所有水表
        int assetCount = customersMapper.findAssetCustomerCount(methodParam.getSchemaName(), idList);
        int polluteCount = customersMapper.findPolluteCustomerCount(methodParam.getSchemaName(), idList);
        if (assetCount > 0) {
            throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AJ_G, LangConstant.TITLE_ASSET_I, LangConstant.TITLE_AJ_G});//客户下还有关联设备，不能删除客户
        }
        if (polluteCount > 0) {
            throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AJ_G, LangConstant.TITLE_ACFG, LangConstant.TITLE_AJ_G});//客户下还有未缴费用，不能删除客户
        }
        if (RegexUtil.optNotNull(nameList).isPresent() && nameList.size() > 0) {
            customersMapper.deleteCusByIds(methodParam.getSchemaName(), idList);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, RegexUtil.optStrOrNull(paramMap.get("ids")), LangUtil.doSetLogArray("删除了客户", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AJ_G, nameList.toString()}));//删除了厂商
        }
    }

    @Override
    public Map<String, Map<String, Boolean>> getCustomerListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[34]);
    }

    @Override
    public void doModifyCustomerFile(MethodParam methodParam, Map<String, Object> paramMap, boolean isAdd) {
        String file_id = RegexUtil.optNotBlankStrOrExp(paramMap.get("file_id"), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAAA_Q}); // 文档未选择
        Integer facility_id = RegexUtil.optIntegerOrNull(paramMap.get("id"));
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> customerMap = customersMapper.findCustomerById(schemaName, facility_id);
        Map<String, Object> customer = new HashMap<>();
        customer.put("id", facility_id);
        customer.put("fileIdsChange", 1);
        customer.put("file_ids", isAdd ? RegexUtil.optNotBlankStrOpt(customerMap.get("file_ids")).map(s -> s.concat(",").concat(file_id)).orElse(file_id) :
                RegexUtil.optNotBlankStrOpt(customerMap.get("file_ids")).map(s -> DataChangeUtil.removeStrForStrings(s, file_id)).orElse(null));
        customersMapper.updateCustomers(schemaName, customer);
        List<String> file_original_names = filesService.getFileNamesByIds(methodParam.getSchemaName(), RegexUtil.optStrOrNull(paramMap.get("file_id")));
        if (isAdd) {
            if (RegexUtil.optNotNull(paramMap.get("file_category_id")).isPresent()
                    && RegexUtil.optNotNull(paramMap.get("file_type_id")).isPresent()) {
                filesService.updateFileCategory(methodParam.getSchemaName(), Integer.valueOf(file_id),
                        RegexUtil.optIntegerOrNull(paramMap.get("file_category_id")),
                        RegexUtil.optStrOrNull(paramMap.get("remark")),
                        RegexUtil.optIntegerOrNull(paramMap.get("file_type_id")));
            }
        }
        if (isAdd) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, RegexUtil.optStrOrNull(facility_id), LangUtil.doSetLogArray("添加了附件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TAB_DOC_A, String.join(",", file_original_names)}));
        } else {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, RegexUtil.optStrOrNull(facility_id), LangUtil.doSetLogArray("删除了附件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_DOC_A, String.join(",", file_original_names)}));
        }
    }

    @Override
    public List<Map<String, Object>> getCustomerFileList(MethodParam methodParam, Map<String, Object> paramMap) {
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> customer = customersMapper.findCustomerById(schemaName, RegexUtil.optIntegerOrNull(paramMap.get("id")));
        return RegexUtil.optNotBlankStrOpt(customer.get("file_ids")).map(ids -> customersMapper.findCustomerFileList(schemaName, ids, RegexUtil.optStrOrNull(paramMap.get("keywordSearch")))).orElse(new ArrayList<>());
    }

    @Override
    public void newCustomerContact(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_AJ_G);//客户不能为空
        param.put("create_user_id", methodParam.getUserId());
        customersMapper.insertCustomerContact(methodParam.getSchemaName(), param);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, param.get("org_id").toString(), LangUtil.doSetLogArray("添加了厂商联系人", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AAJ_P, RegexUtil.optStrOrNull(param.get("contact_name"))}));

    }

    @Override
    public void modifyCustomerContact(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("id"), LangConstant.TITLE_AJ_G);//客户不能为空
        Map<String, Object> oldMap = customersMapper.findOrganizationContactInfo(methodParam.getSchemaName(), Integer.valueOf(param.get("id").toString()));
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            customersMapper.updateOrganizationContact(methodParam.getSchemaName(), param);
            Map<String, Map<String, Object>> handleMap = new HashMap<>();
            handleMap.put("keyMap", new HashMap<String, Object>() {{
                put("contact_name", LangConstant.TITLE_AAJ_Q);//联系人姓名
                put("contact_mobile", LangConstant.TITLE_AAJ_N);//联系电话
                put("contact_email", LangConstant.TITLE_GW);//邮箱
                put("wei_xin", LangConstant.TITLE_AAW_C);//微信
                put("job", LangConstant.TITLE_AEIK);//职务
            }});
            if (!RegexUtil.optIsPresentStr(param.get("contact_name"))) {
                param.put("contact_name", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(param.get("contact_mobile"))) {
                param.put("contact_mobile", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(param.get("contact_email"))) {
                param.put("contact_email", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(param.get("wei_xin"))) {
                param.put("wei_xin", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(param.get("job"))) {
                param.put("job", StringUtils.EMPTY);
            }
            JSONArray log = compareMap(param, oldMap, new HashMap<String, Integer>() {{
                put("token", 1);
                put("id", 1);
                put("org_id", 1);
            }}, handleMap);
            if (log.size() > 0) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, oldMap.get("org_id").toString(), LangUtil.doSetLogArray("编辑了厂商联系人", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAJ_P, log.toString()}));
            }
        }
    }

    @Override
    public void cutCustomerContact(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        RegexUtil.optIntegerOrExpParam(facilitiesModel.getId(), LangConstant.TITLE_AAJ_P);//联系人不能为空
        Map<String, Object> organizationContact = customersMapper.findOrganizationContactInfo(methodParam.getSchemaName(), facilitiesModel.getId());
        if (RegexUtil.optNotNull(organizationContact).isPresent()) {
            customersMapper.deleteOrganizationContact(methodParam.getSchemaName(), facilitiesModel.getId());
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, facilitiesModel.getOrg_id().toString(), LangUtil.doSetLogArray("删除了厂商联系人", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAJ_P, organizationContact.get("contact_name").toString()}));
        }
    }

    @Override
    public List<Map<String, Object>> getCustomerContactList(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optIntegerOrExpParam(param.get("org_id"), LangConstant.TITLE_AJ_G);//厂商不能为空
        return customersMapper.findOrganizationContactList(methodParam.getSchemaName(), param);
    }

    @Override
    public Map<String, Object> searchExecSyncResult(MethodParam methodParam, Map<String, Object> paramMap) {
        // 同步服务请求地址
        return RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "sync_url")).map(u -> {
            JSONObject jsonObject = new JSONObject();
            String jsonString = jsonObject.toString();
            String requestUrl = u.concat("getExecSyncResult");
            return DataChangeUtil.scdObjToMap(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        }).orElse(null);
    }

    @Override
    public Map<String, Object> handExecSync(MethodParam methodParam, Map<String, Object> paramMap) {
        // 同步服务请求地址
        return RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "sync_url")).map(u -> {
            JSONObject jsonObject = new JSONObject();
            String jsonString = jsonObject.toString();
            String requestUrl = u.concat("execSync");
            return DataChangeUtil.scdObjToMap(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        }).orElse(null);
    }

    @Override
    public void doModifyCustomerRecord(MethodParam methodParam, Map<String, Object> paramMap) {
        // 目前只支持文字信息记录
        String type_id = RegexUtil.optNotBlankStrOrExp(paramMap.get("type_id"), LangConstant.MSG_B, new String[]{LangConstant.TITLE_CATEGORY_AH}); // 类型未选择
        Integer facility_id = RegexUtil.optIntegerOrNull(paramMap.get("id"));
        String isDo = RegexUtil.optStrOrNull(paramMap.get("isDo"));
        if ("add".equals(isDo)) {
            Map<String, Object> param = new HashMap<>();
            param.put("type_id", type_id);
            param.put("facility_id", facility_id);
            param.put("remark", paramMap.get("remark"));
            param.put("create_user_id", methodParam.getUserId());
            customersMapper.insertCustomerRecord(methodParam.getSchemaName(), param);
        } else if ("edit".equals(isDo)) {
            Map<String, Object> param = new HashMap<>();
            param.put("id", paramMap.get("record_id"));
            param.put("type_id", type_id);
            param.put("remark", paramMap.get("remark"));
            param.put("update_user_id", methodParam.getUserId());
            customersMapper.updateCustomerRecord(methodParam.getSchemaName(), param);
        } else if ("delete".equals(isDo)) {
            Map<String, Object> param = new HashMap<>();
            param.put("id", paramMap.get("record_id"));
            param.put("status", -1000);
            param.put("update_user_id", methodParam.getUserId());
            customersMapper.updateCustomerRecord(methodParam.getSchemaName(), param);
        }
    }

    @Override
    public List<Map<String, Object>> getCustomerRecordList(MethodParam methodParam, Map<String, Object> paramMap) {
        return customersMapper.findCustomerRecordList(methodParam.getSchemaName(), paramMap);
    }

    @Override
    public void editRelationAssetList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("cus_id"), LangConstant.TITLE_AJ_G);//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("asset_id"), LangConstant.TITLE_ASSET_I);//不能为空
        Map<String, Object> oldMap = customersMapper.findRelationAsset(methodParam.getSchemaName(), paramMap);
        RegexUtil.optNotNullMap(oldMap).ifPresent(old -> {
            customersMapper.updateRelationAsset(methodParam.getSchemaName(), paramMap);
            Map<String, Map<String, Object>> handleMap = new HashMap<>();
            handleMap.put("keyMap", new HashMap<String, Object>() {{
                put("sewage_value", LangConstant.TITLE_BDFH);//排污阈值
                put("siren_value", LangConstant.TITLE_BDFI);//警号阈值
                put("abnormal_value", LangConstant.TITLE_BDFJ);//异常阈值
            }});
            if (!RegexUtil.optIsPresentStr(paramMap.get("sewage_value"))) {
                paramMap.put("sewage_value", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(paramMap.get("siren_value"))) {
                paramMap.put("siren_value", StringUtils.EMPTY);
            }
            if (!RegexUtil.optIsPresentStr(paramMap.get("abnormal_value"))) {
                paramMap.put("abnormal_value", StringUtils.EMPTY);
            }
            JSONArray log = compareMap(paramMap, oldMap, new HashMap<String, Integer>() {{
                put("token", 1);
                put("id", 1);
                put("parent_id", 1);
                put("price", 1);
                put("status", 1);
                put("create_time", 1);
                put("location", 1);
                put("properties", 1);
            }}, handleMap);
            if (log.size() > 0) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_6002, paramMap.get("cus_id").toString(), LangUtil.doSetLogArray("编辑了关联设备" + paramMap.get("asset_name"), LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_ASSET_I, log.toString()}));
            }
        });
    }

    private String handleData(MethodParam methodParam, String selectKey, Map map, String key) {
        String result = RegexUtil.optNotBlankStrOpt(map.get(key)).map(e -> selectOptionService.getSelectOptionAttrByCode(methodParam, selectKey, e, "text")).orElse(StringUtils.EMPTY);
        return result;
    }

    private String remoteCall(String path, String url, Map<String, Object> param) {
        JSONObject jsonObject = new JSONObject();
        Map<String, Object> map = RegexUtil.optMapOrNew(param);
        for (String key : map.keySet()) {
            jsonObject.put(key, map.get(key));
        }
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }
}
