package com.gengyun.senscloud.service.dynamic;

/**
 * 功能：工单列表servcie
 * Created by Administrator on 2018/11/2.
 */
public interface WorkListService {
//
//    /*工单概要表*/
//    int addWorkList(String schema_name,WorkListModel workListModel);
//
//    /*工单详情表*/
//    int addWorkDetailList(String schema_name,WorkListDetailModel workListModel);
//
//    /*查询待分配工单信息*/
//    JSONObject selectWorkListsDetailUndistributed(String schema_name);
//
//    int selectWorkListsDetailUndistributedNum(String schema_name,String condition);
//
//    /*根据工单详情id查询工单详情信息*/
//    List<WorkListDetailModel> selectWorkListsDetailByWorkCode(String schema_name,String sub_work_code);
//
//    /*根据工单号查询工单模板详情*/
//    List<WorkListModel> selectWorkListsByWorkCode(String schema_name,String work_code);
//
//    /*更新工单详情状态*/
//    int updateWorkListStatus(String schema_name,Integer status, String subWorkCode);
//
//    int updateBackWorkListStatus(String schema_name,Integer status, String workCode);
//
//
//    int selectWorkListsDetailStatus(String schema_name,String subWorkCode);
//
//    /*更新工单概要状态*/
//    int updateWorksStatus(String schema_name,Integer status, String subWorkCode);
//
//    /*工单池任务分配*/
//    int linkRepairManToWorkOrder(String  schema_name,String receive_account,String subWorkCode);
//
//    /*更新工单详情状态并修改同步时间*/
//    int updateWorkListStatusAndTime(String schema_name, Integer status, String subWorkCode, WorkListDetailModel workListDetailModel);
//
//    /*更新工单详情状态并修改同步时间*/
//    int updateWorkStatusAndTime(String schema_name, Integer status, String work_code);
//
//    /*作废工单*/
//    Boolean cancelUndistributedWork(String schema_name,String subWorkCode,String workCode,int status);
//
//    /*删除工单*/
//    Boolean delUndistributedWork(String schema_name,String subWorkCode,String workCode);
//
//    /*工单池变更*/
//    int updateWorkPool(String schema_name,String work_code, Integer pool_id);
}
