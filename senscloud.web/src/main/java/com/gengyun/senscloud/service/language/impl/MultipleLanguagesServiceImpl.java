package com.gengyun.senscloud.service.language.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.MultipleLanguagesMapper;
import com.gengyun.senscloud.model.LangSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.language.MultipleLanguagesService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import io.swagger.annotations.Api;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Api(tags = "多语言")
@Service
public class MultipleLanguagesServiceImpl implements MultipleLanguagesService {
    @Resource
    MultipleLanguagesMapper multipleLanguagesMapper;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    SelectOptionService selectOptionService;
    @Value("${senscloud.com.url}")
    private String url;

    @Override
    public Map<String, Map<String, Boolean>> getAssetListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[28]);
    }

    /**
     * 获取多语言列表
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getLanguagesListForPage(MethodParam methodParam, LangSearchParam lgParam) {
        String companyIdSearch = RegexUtil.optStrOrExpNotSelect(lgParam.getCompanyIdSearch(), LangConstant.TITLE_NAME_AB_F);
        String pagination = SenscloudUtil.changePagination(methodParam);
        // TODO 权限处理
        String keywordSearch = lgParam.getKeywordSearch();
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> list = null;
        int count;
        if (RegexUtil.optStrOrBlank(Constants.DEFAULT_COMPANY).equals(companyIdSearch)) {
            count = JSON.parseObject(requestPublic("countLangForBase", new HashMap<String, Object>(){{put("keywordSearch",keywordSearch);}}), int.class);
            if (count > 0) {
                list = JSON.parseObject(requestPublic("findLanguageListForBase", new HashMap<String, Object>(){{put("keywordSearch",keywordSearch);put("companyIdSearch",companyIdSearch);put("pagination",pagination);}}), List.class);
            }
        } else {
            count = JSON.parseObject(requestPublic("countLang", new HashMap<String, Object>(){{put("keywordSearch",keywordSearch);put("companyIdSearch",companyIdSearch);}}), int.class);
            if (count > 0) {
                list = JSON.parseObject(requestPublic("findLanguageList", new HashMap<String, Object>(){{put("keywordSearch",keywordSearch);put("companyIdSearch",companyIdSearch);put("pagination",pagination);}}), List.class);
            }
        }
        DataChangeUtil.scdListSplitByKey(list, "value");
        result.put("rows", list);
        result.put("total", count);
        return result;
    }

    /**
     * 获取多语言列表（企业）
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getLangListForCompanyForPage(MethodParam methodParam, LangSearchParam lgParam) {
        String companyIdSearch = RegexUtil.optStrOrExpNotSelect(methodParam.getCompanyId(), LangConstant.TITLE_NAME_AB_F);
        String pagination = SenscloudUtil.changePagination(methodParam);
        // TODO 权限处理
        String keywordSearch = lgParam.getKeywordSearch();
        Map<String, Object> result = new HashMap<>();
        int count = JSON.parseObject(requestPublic("countLangForCompany", new HashMap<String, Object>(){{put("keywordSearch",keywordSearch);put("companyIdSearch",companyIdSearch);}}), int.class);
        if (count > 0) {
            List<Map<String, Object>> list = JSON.parseObject(requestPublic("findLangListForCompany", new HashMap<String, Object>(){{put("keywordSearch",keywordSearch);put("companyIdSearch",companyIdSearch);put("pagination",pagination);}}), List.class);
            DataChangeUtil.scdListSplitByKey(list, "value");
            result.put("rows", list);
        }
        result.put("total", count);
        return result;
    }

    /**
     * 新增多语言
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @param paramMap    页面参数
     * @param isSelf      是否是单企业编辑
     */
    @Override
    public void newLanguage(MethodParam methodParam, LangSearchParam lgParam, Map<String, Object> paramMap, boolean isSelf) {
        String useType = RegexUtil.optStrOrVal(lgParam.getUse_type(), "text");
        String key = RegexUtil.optNotBlankStrOrExp(RegexUtil.optNotNullListOrNew(JSON.parseObject(requestPublic("getNewKeyName", new HashMap<String, Object>(){{put("use_type",useType);}}),List.class)).get(0), ErrorConstant.EC_LANG_001);
        Map<String, Object> map = new HashMap<>();
        map.put("company_id", Constants.DEFAULT_COMPANY);
        map.put("key", key);
        map.put("remark_cn", RegexUtil.optStrOrBlank(paramMap.get(Constants.LANG_ZH_CN)));
        map.put("remark_en", RegexUtil.optStrOrBlank(paramMap.get(Constants.LANG_EN_US)));
        map.put("use_type", useType);
        map.put("from_id", methodParam.getCompanyId());
        RegexUtil.intExp(JSON.parseObject(requestPublic("insertLanguages", map), int.class), ErrorConstant.EC_LANG_002); // 产品基础语种
        Map<String, Object> langCfg = selectOptionService.getStaticSelectForInfo(methodParam, "langs");
        Set<String> langSet = langCfg.keySet();
        for (String langType : langSet) {
            if (!Constants.LANG_ZH_CN.equals(langType) && !Constants.LANG_EN_US.equals(langType)) {
                RegexUtil.optNotBlankStrOpt(paramMap.get(langType)).ifPresent(v -> {
                    map.put("type", langType);
                    map.put("value", v);
                    RegexUtil.intExp(JSON.parseObject(requestPublic("insertLanguages", map), int.class), ErrorConstant.EC_LANG_003); // 产品其它语种
                });
            }
        }
        String tmpLog = "【" + key + "-" + map.get("remark_cn") + "（" + map.get("remark_en") + "）】";
        if (isSelf) {
            map.put("company_id", methodParam.getCompanyId());
            for (String langType : langSet) {
                RegexUtil.optNotBlankStrOpt(paramMap.get(langType)).ifPresent(v -> {
                    map.put("type", langType);
                    map.put("value", v);
                    RegexUtil.intExp(JSON.parseObject(requestPublic("insertLanguages", map), int.class), ErrorConstant.EC_LANG_004);
                });
            }
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_11008, key, LangUtil.doSetLogArray("企业新增多语言" + key, LangConstant.TAB_DEVICE_B, new String[]{tmpLog}));
        } else {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_11007, key, LangUtil.doSetLogArray("新增多语言" + key, LangConstant.TAB_DEVICE_B, new String[]{tmpLog}));
        }

    }


    /**
     * 编辑多语言
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     * @param paramMap    页面参数
     * @param isSelf      是否是单企业编辑
     */
    @Override
    public void editLanguages(MethodParam methodParam, LangSearchParam lgParam, Map<String, Object> paramMap, boolean isSelf) {
        RegexUtil.optStrToArraySplit(isSelf ? methodParam.getCompanyId() : lgParam.getSelectCompanyIds()).filter(ids -> ids.length > 0).map(DataChangeUtil::scdArrayToListStr).ifPresent(idList ->
                RegexUtil.optNotBlankStrOpt(lgParam.getKey()).ifPresent(k ->
                        RegexUtil.optNotNullMap(DataChangeUtil.scdObjToMap(requestPublic("findBaseLangByKey", new HashMap<String, Object>(){{put("key",k);}}))).ifPresent(b -> {
                            Map<String, Object> map = new HashMap<>();
                            map.put("key", k);
                            map.put("remark_cn", b.get(Constants.LANG_ZH_CN));
                            map.put("remark_en", b.get(Constants.LANG_EN_US));
                            map.put("use_type", b.get("use_type"));
                            map.put("from_id", b.get("from_id"));
                            Map<String, Object> langCfg = selectOptionService.getStaticSelectForInfo(methodParam, "langs");
                            Set<String> langSet = langCfg.keySet();
                            String tmpLog = "【" + k + "-" + b.get(Constants.LANG_ZH_CN) + "（" + b.get(Constants.LANG_EN_US) + "）】";
                            if (!isSelf && idList.contains(String.valueOf(Constants.DEFAULT_COMPANY))) {
                                map.put("remark_cn", RegexUtil.optStrOrBlank(paramMap.get(Constants.LANG_ZH_CN)));
                                map.put("remark_en", RegexUtil.optStrOrBlank(paramMap.get(Constants.LANG_EN_US)));
                                RegexUtil.intExp(JSON.parseObject(requestPublic("updateBaseLang", map), int.class), ErrorConstant.EC_LANG_005); // 产品基础语种
                                map.put("company_id", Constants.DEFAULT_COMPANY);
                                for (String langType : langSet) {
                                    if (!Constants.LANG_ZH_CN.equals(langType) && !Constants.LANG_EN_US.equals(langType)) {
                                        if (paramMap.containsKey(langType)) {
                                            map.put("type", langType);
                                            map.put("value", RegexUtil.optStrOrBlank(paramMap.get(langType)));
                                            int count = JSON.parseObject(requestPublic("existTypeCount", map), int.class);
                                            if (count > 0) {
                                                RegexUtil.intExp(JSON.parseObject(requestPublic("updateCompanyLang", map), int.class), ErrorConstant.EC_LANG_007); // 产品其它语种
                                            } else {
                                                RegexUtil.intExp(JSON.parseObject(requestPublic("insertLanguages", map), int.class), ErrorConstant.EC_LANG_006); // 产品其它语种
                                            }
                                        }
                                    }
                                }
                            }
                            idList.remove(String.valueOf(Constants.DEFAULT_COMPANY));
                            if (idList.contains(String.valueOf(Constants.SUEZ_COMPANY))) {
                                // 查询所有有效企业消息
                                List<String> companyIdList = this.toCompanyIdList();
                                for (int i = 160; i < 179; i++) {
                                    if (!idList.contains(String.valueOf(i)) && companyIdList.contains(String.valueOf(i))) {
                                        // 需要剔除不存在的企业
                                        idList.add(String.valueOf(i));
                                    }
                                }
                            }
                            for (String id : idList) {
                                map.put("company_id", id);
                                for (String langType : langSet) {
                                    if (paramMap.containsKey(langType)) {
                                        map.put("type", langType);
                                        map.put("value", RegexUtil.optStrOrBlank(paramMap.get(langType)));
                                        int count = JSON.parseObject(requestPublic("existTypeCount", map), int.class);
                                        if (count > 0) {
                                            RegexUtil.intExp(JSON.parseObject(requestPublic("updateCompanyLang", map), int.class), ErrorConstant.EC_LANG_008);
                                        } else {
                                            RegexUtil.intExp(JSON.parseObject(requestPublic("insertLanguages", map), int.class), ErrorConstant.EC_LANG_009);
                                        }
                                    }
                                }
                            }
                            logService.newLog(methodParam, isSelf ? SensConstant.BUSINESS_NO_11008 : SensConstant.BUSINESS_NO_11007, k, LangUtil.doSetLogArray("修改多语言" + k + lgParam.getSelectCompanyIds(), LangConstant.TAB_DEVICE_A, new String[]{tmpLog}));
                        })
                )
        );
    }

    /**
     * 发布多语言
     *
     * @param methodParam 系统参数
     * @param lgParam     请求参数
     */
    @Override
    public void releaseLang(MethodParam methodParam, LangSearchParam lgParam) {
        RegexUtil.optStrToArraySplit(lgParam.getSelectCompanyIds()).filter(ids -> ids.length > 0).map(DataChangeUtil::scdArrayToListStr).ifPresent(idList -> {
            if (idList.contains(String.valueOf(Constants.SUEZ_COMPANY))) {
                // 查询所有有效企业消息
                List<String> companyIdList = this.toCompanyIdList();
                for (int i = 160; i < 179; i++) {
                    if (!idList.contains(String.valueOf(i)) && companyIdList.contains(String.valueOf(i))) {
                        // 需要剔除不存在的企业
                        idList.add(String.valueOf(i));
                    }
                }
                idList.remove(String.valueOf(Constants.SUEZ_COMPANY));
            }
            String rst = "";
            for (String id : idList) {
                int count = JSON.parseObject(requestPublic("countCompanyResource", new HashMap<String, Object>(){{put("companyIdSearch",id);}}), int.class);
                if (count == 0) {
                    RegexUtil.intExp(JSON.parseObject(requestPublic("deployLangByNewCompany", new HashMap<String, Object>(){{put("companyIdSearch",id);}}), int.class), LangConstant.MSG_BK);
                } else {
                    RegexUtil.intExp(JSON.parseObject(requestPublic("deployLangByCompany", new HashMap<String, Object>(){{put("companyIdSearch",id);}}), int.class), LangConstant.MSG_BK);
                }
                rst = rst.concat(",").concat(id);
            }
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_11007, rst, LangUtil.doSetLogArrayNoParam("发布多语言", LangConstant.TITLE_BBA_M));
        });
    }

    private List<String> toCompanyIdList() {
        List<String> companyIdList = new ArrayList<>();
        List<Map<String, Object>> companyList = RegexUtil.optNotNullListOrNew(JSON.parseObject(requestPublic("findAll", new HashMap<>()), List.class));
        companyList.forEach(e -> {
            companyIdList.add(RegexUtil.optStrOrNull(e.get("id")));
        });
        return companyIdList;
    }

    /**
     * 发布多语言（企业）
     *
     * @param methodParam 系统参数
     */
    @Override
    public void releaseLangByCompany(MethodParam methodParam) {
        String companyIdSearch = RegexUtil.optStrOrExpNotSelect(methodParam.getCompanyId(), LangConstant.TITLE_NAME_AB_F);
        int count = JSON.parseObject(requestPublic("countCompanyResource", new HashMap<String, Object>(){{put("companyIdSearch",companyIdSearch);}}), int.class);
        if (count == 0) {
            RegexUtil.intExp(JSON.parseObject(requestPublic("deployLangByNewCompany", new HashMap<String, Object>(){{put("companyIdSearch",companyIdSearch);}}), int.class), LangConstant.MSG_BK);
        } else {
            RegexUtil.intExp(JSON.parseObject(requestPublic("deployLangByCompany", new HashMap<String, Object>(){{put("companyIdSearch",companyIdSearch);}}), int.class), LangConstant.MSG_BK);
        }
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_11008, companyIdSearch, LangUtil.doSetLogArrayNoParam("企业发布多语言", LangConstant.TITLE_BBA_M));
    }

    public String requestPublic(String path, Map<String, Object> param) {
        JSONObject jsonObject = new JSONObject();
        Map<String, Object> map = RegexUtil.optMapOrNew(param);
        for (String key : map.keySet()) {
            jsonObject.put(key, map.get(key));
        }
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }
}
