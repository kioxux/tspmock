package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.AssetMapper;
import com.gengyun.senscloud.mapper.FilesMapper;
import com.gengyun.senscloud.model.FileModel;
import com.gengyun.senscloud.model.FilesData;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.login.LoginService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class FilesServiceImpl implements FilesService {
    private static final Logger logger = LoggerFactory.getLogger(FilesService.class);
    @Value("${senscloud.file_upload}")
    private String file_upload_dir;
    @Resource
    LogsService logService;
    @Resource
    private FilesMapper filesMapper;
    @Resource
    AssetMapper assetMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    WorkflowService workflowService;
    @Resource
    LoginService loginService;


    //新增附件
    @Override
    public synchronized int add(String schema_name, FilesData filesData) {
        filesMapper.AddFile(schema_name, filesData.getFile_name(), filesData.getFile_type_id(), filesData.getFile_size(),
                filesData.getFile_width(), filesData.getFile_height(), filesData.getFile_original_name(),
                filesData.getFile_url(), filesData.getRemark(), filesData.getCreate_user_id(), filesData.getFile_category_id(),
                filesData.getBusiness_no(), filesData.getBusiness_no_id());
        return filesMapper.selectCurrval(schema_name);

    }

    //
    //获取附件
    @Override
    public List<FilesData> FindAllFilesByName(String schema_name, String file_name) {
        return filesMapper.FindAllFilesByName(schema_name, file_name);
    }

//    //获取附件
//    @Override
//    public List<FilesData> FindAllFiles(String schema_name, String file_ids) {
//        return filesMapper.FindFileList(schema_name, file_ids);
//    }

    //更新资产的附件id
    @Override
    public void updateAssetfileValue(String schema_name, String field_name, Object value, String _id) {
        assetMapper.updateAssetFieldValue(schema_name, field_name, value, _id);
    }

    /**
     * 根据文件码下载文件
     *
     * @param fileModel 文件码
     */
    @Override
    public void doDownLoad(MethodParam methodParam, FileModel fileModel) {
        try {
            String fileCode = fileModel.getId();
            boolean isTmp = RegexUtil.optNotNullMap(methodParam.getFileTmpInfo()).filter(info -> info.containsKey(fileCode)).map(info -> {
                Map<String, Object> fileInfo = DataChangeUtil.scdObjToMap(info.get(fileCode));
                RegexUtil.optNotBlankStrOpt(fileInfo.get("authType")).filter(at -> pagePermissionService.applyCheckFunPrmByKeyNoExp(methodParam, at)).ifPresent(hasAuth -> {
                    info.remove(fileCode);
                    DownloadUtil.download((String) fileInfo.get("filePath"), (String) fileInfo.get("fileName"), true);
                });
                return true;
            }).orElse(false);
            if (!isTmp) {
                Map<String, Object> fileInfo = filesMapper.findFileById(methodParam.getSchemaName(), Integer.valueOf(fileCode));
                DownloadUtil.download((String) fileInfo.get("file_url"), RegexUtil.optStrAndValOrVal(fileInfo.get("file_original_name"), fileInfo.get("file_name"), SenscloudUtil.generateUUIDStr()), false);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void getImg(MethodParam methodParam, FileModel fileModel) {
        try {
            if (RegexUtil.optNotBlankStrOpt(fileModel.getId()).isPresent()) {
                Map<String, Object> fileInfo = filesMapper.findFileById(methodParam.getSchemaName(), Integer.valueOf(fileModel.getId()));
                String path = (String) fileInfo.get("file_url");
//                File targetFile = new File(path);
//                if (!targetFile.exists()) {
//                    targetFile = new File(path);
//                }
//                BufferedImage image = ImageIO.read(targetFile);
//                ImageUtils.doShowImageByDftWah(image);
                DownloadUtil.download(path, RegexUtil.optStrOrVal(fileInfo.get("fileName"), SenscloudUtil.generateUUIDStr()), false);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * 上传文件处理
     *
     * @param methodParam 入参
     * @param fileModel   文件
     */
    @Override
    public String checkAddFile(MethodParam methodParam, FileModel fileModel) {
        String businessNo = RegexUtil.optStrOrBlank(fileModel.getBusiness_no());
        if (SensConstant.BUSINESS_NO_8003.equals(businessNo)) {
            MultipartFile file = RegexUtil.optNotNull(fileModel.getFiles()).map(fs -> fs.get(0)).filter(f -> RegexUtil.optNotNull(f).isPresent()).orElseThrow(() -> new SenscloudException(LangConstant.TEXT_CHOOSE_A, new String[]{LangConstant.TEXT_W})); // 请选择文件
            workflowService.newWorkflow(methodParam, file);
            return "ok";
        } else {
            return RegexUtil.optStrOrBlank(addFile(methodParam, fileModel));
        }
    }

    /**
     * 上传文件
     *
     * @param methodParam 入参
     * @param fileModel   文件
     */
    private Integer addFile(MethodParam methodParam, FileModel fileModel) {
        Integer newFileId = 0;
        List<MultipartFile> files = fileModel.getFiles();
        Map<String, Object> result = new HashMap<>();
        //上传附件
        if (RegexUtil.optIsPresentStr(files)) {
            MultipartFile file = files.get(0);
            String originalFilename = file.getOriginalFilename();
            Long fileLength = file.getSize() / 1024;//转成k
            String sep = System.getProperty("file.separator");
            SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_DATE);
            String date = format.format(new Date());
            UUID id = UUID.randomUUID();
            String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
            String newFileName = id.toString() + "." + suffix;
            //保存附件实体
            try {
                String schema_name = methodParam.getSchemaName();
                //定义目录，并判断是否存在
                String fileDir = file_upload_dir + sep + schema_name + sep + "attach" + sep + date;
                File dir = new File(fileDir);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                //保存路径
                String filePath = file_upload_dir + sep + schema_name + sep + "attach" + sep + date + sep + newFileName;
                File desc = new File(filePath);
                int imgOrDoc = ImageUtils.getIstance().isImage(desc) ? 1 : 2;
                file.transferTo(desc);
                FilesData attachment = new FilesData();
                attachment.setFile_name(newFileName);
                attachment.setFile_original_name(originalFilename);
                attachment.setFile_url(filePath);
                attachment.setFile_category_id(imgOrDoc);
                attachment.setFile_size((double) fileLength);
                attachment.setFile_width(0);
                attachment.setFile_height(0);
                String extension = suffix.toLowerCase();
                if ("png".equals(extension) || "jpg".equals(extension) || "jpeg".equals(extension) || "bmp".equals(extension)) {
                    attachment.setFile_width(ImageUtils.getImgWidth(desc));
                    attachment.setFile_height(ImageUtils.getImgHeight(desc));
                }
                attachment.setCreate_user_id(methodParam.getUserId());
                attachment.setBusiness_no(fileModel.getBusiness_no());
                attachment.setBusiness_no_id(fileModel.getData_id());
                int count = this.add(schema_name, attachment);
                if (count >= 1) {
                    List<FilesData> list = this.FindAllFilesByName(schema_name, newFileName);
                    if (list != null && !list.isEmpty()) {
                        newFileId = list.get(0).getId();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return newFileId;
    }

    /**
     * 根据附件id获取附件详细信息
     *
     * @param schemaName 入参
     * @param fileId     入参
     * @return 附件详细信息
     */
    @Override
    public Map<String, Object> findById(String schemaName, Integer fileId) {
        return filesMapper.findFileById(schemaName, fileId);
    }

    /**
     * 根据附件id获取名称列表
     *
     * @param schema_name 请求参数
     * @param file_ids    请求参数
     * @return 列表数据
     */
    @Override
    public List<String> getFileNamesByIds(String schema_name, String file_ids) {
        return filesMapper.findFileNamesByIds(schema_name, file_ids);
    }

    /**
     * 更新附近的文件类别
     *
     * @param schema_name  请求参数
     * @param file_id      请求参数
     * @param file_type_id 请求参数
     */
    @Override
    public void updateFileType(String schema_name, Integer file_id, Integer file_type_id) {
        filesMapper.updateFileType(schema_name, file_id, file_type_id);
    }

    /**
     * 根据域名获取相关图片
     *
     * @param methodParam 系统参数
     * @param fileModel   请求参数
     */
    @Override
    public void getLoginImage(MethodParam methodParam, FileModel fileModel) {
        RegexUtil.optNotBlankStrOpt(fileModel.getId()).ifPresent(id -> {
            Map<String, Object> map = loginService.getLoginData(methodParam);
            RegexUtil.optNotBlankStrOpt(map.get(RegexUtil.optEqualsOpt(id, "1").map(t -> "loginBackground").orElse("loginLogo"))).ifPresent(path -> {
                try {
                    DownloadUtil.download(path, SenscloudUtil.generateUUIDStr() + ".png", "x-png", false);
//                    ImageUtils.doShowImageByDftWah(ImageIO.read(RegexUtil.optNotNull(new File(path)).filter(File::exists).orElse(new File(path))));
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    e.printStackTrace();
                }
            });
        });
    }

    /**
     * 更新文件附加信息
     * @param schema_name
     * @param file_id
     * @param file_category_id
     * @param remark
     */
    @Override
    public void updateFileCategory(String schema_name, Integer file_id, Integer file_category_id, String remark,Integer file_type_id) {
        filesMapper.updateFileCategory(schema_name, file_id, file_category_id,remark,file_type_id);
    }
}
