package com.gengyun.senscloud.service.login;

import com.gengyun.senscloud.entity.CompanyEntity;

import java.util.List;
import java.util.Map;

/**
 * 企业信息处理
 */
public interface CompanyService {

    /**
     * 通过企业编号获得企业信息
     *
     * @param id 企业编号
     * @return 企业信息
     */
    CompanyEntity getCompanyById(long id);

    /**
     * 根据登录信息获取企业信息
     *
     * @param user 登录信息
     * @return 企业信息
     */
    List<Map<String, Object>> getCompanyListByUser(String user);

    /**
     * 将用户信息同步到公共表中
     *
     * @param param 参数
     */
    void newUserTenantRelation(Map<String, Object> param);

    /**
     * 根据手机号获取企业信息（后台用）
     *
     * @param phone 手机号
     * @return 企业信息
     */
    List<Map<String, Object>> getCompanyListByPhone(String phone);

    /**
     * 根据手机号获取企业信息（返回客户端）
     *
     * @param phone 手机号
     * @return 企业信息
     */
    List<Map<String, Object>> getCompanyListForPageByPhone(String phone);

    /**
     * 根据登录用户信息查询企业数量
     *
     * @param account   账号
     * @param companyId 企业编号
     * @return 企业数量
     */
    int cntCompanyListByUser(String account, long companyId);

    /**
     * 获取全部有效企业列表
     *
     * @return 企业列表
     */
//    List<Map<String, Object>> findAll();

    /**
     * 根据用户删除公司
     *
     * @param user
     */
    void deleteCompanyListByUser(String user);

    String requestLang(Long companyId, String key, String path);

    String requestParamOne(String condition, String key, String path);
    String requestParamTwo(Long companyId, String condition, String key1, String key2, String path);

    String requestNoParam(String path);

    void deleteCompanyListByUserAndLicense(String user);
}
