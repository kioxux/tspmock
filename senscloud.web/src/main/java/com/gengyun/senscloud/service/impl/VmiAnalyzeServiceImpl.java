package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.VmiAnalyzeService;
import org.springframework.stereotype.Service;

/**
 * VMI分析管理
 */
@Service
public class VmiAnalyzeServiceImpl implements VmiAnalyzeService {
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    VmiAnalyzeMapper vmiAnalyzeMapper;
//
//    @Autowired
//    ElasticSearchServiceImpl elasticSearchService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    IotUrlService iotUrlService;
//
//    @Autowired
//    ResolvingMetaService resolvingMetaService;
//
//    @Autowired
//    TokenService tokenService;
//
//    @Value("${es.index.monitoring:xinggang.monitoring}")
//    private String index;
//
//    /**
//     * 查询设备列表
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Override
//    public List<VmiAssetModel> findVmiAssetList(String schema_name, String condition, int pageSize, int begin) {
//        return vmiAnalyzeMapper.findVmiAssetList(schema_name, condition, pageSize, begin);
//    }
//
//    /**
//     * 查询设备列表总数
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Override
//    public int findVmiAssetListNum(String schema_name, String condition) {
//        return vmiAnalyzeMapper.findVmiAssetListNum(schema_name, condition);
//    }
//
//    /**
//     * 查询设备、客户的详情信息
//     *
//     * @param schema_name
//     * @param facilitiesId
//     * @param assetId
//     * @return
//     */
//    @Override
//    public VmiAssetModel findVmiAssetByFacilitiesIdAndAssetId(String schema_name, Long facilitiesId, String assetId) {
//        return vmiAnalyzeMapper.findVmiAssetByFacilitiesIdAndAssetId(schema_name, facilitiesId, assetId);
//    }
//
//    /**
//     * 查询报表列表
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Override
//    public List<VmiAssetModel> findVmiReportList(String schema_name, String condition, String dateCondition, int pageSize, int begin) {
//        return vmiAnalyzeMapper.findVmiReportList(schema_name, condition, dateCondition, pageSize, begin);
//    }
//
//    /**
//     * 查询报表列表总数
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Override
//    public int findVmiReportListNum(String schema_name, String condition) {
//        return vmiAnalyzeMapper.findVmiReportListNum(schema_name, condition);
//    }
//
//    /**
//     * 根据设备code，查询设备物流信息列表
//     *
//     * @param schema_name
//     * @param assetCode
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getAssetVmiTransferList(String schema_name, String assetCode, int pageSize, int begin) {
//        return vmiAnalyzeMapper.getAssetVmiTransferList(schema_name, assetCode, pageSize, begin);
//    }
//
//    /**
//     * 根据设备code，查询设备物流信息列表总数
//     *
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Override
//    public int countAssetVmiTransferList(String schema_name, String assetCode) {
//        return vmiAnalyzeMapper.countAssetVmiTransferList(schema_name, assetCode);
//    }
//
//    /**
//     * 根据设备code，查询设备入库信息列表
//     *
//     * @param schema_name
//     * @param assetCode
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getAssetStockInList(String schema_name, String assetCode, int pageSize, int begin) {
//        List<Map<String, Object>> results = vmiAnalyzeMapper.getAssetStockInList(schema_name, assetCode, pageSize, begin);
//        if(results != null && results.size() > 0){
//            String total = vmiAnalyzeMapper.totalAssetStockInList(schema_name, assetCode);
//            Map<String, Object> totalMap = new HashMap<>();
//            totalMap.put("receive_start_time", selectOptionService.getLanguageInfo(LangConstant.TOTAL_T)+"：");//合计
//            totalMap.put("receive_actual", total);
//            results.add(totalMap);
//        }
//        return results;
//    }
//
//    /**
//     * 根据设备code，查询设备入库信息列表总数
//     *
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Override
//    public int countAssetStockInList(String schema_name, String assetCode) {
//        return vmiAnalyzeMapper.countAssetStockInList(schema_name, assetCode);
//    }
//
//    /**
//     * 根据设备code，查询设备出库信息列表
//     *
//     * @param schema_name
//     * @param assetCode
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getAssetStockOutList(String schema_name, String assetCode, int pageSize, int begin) {
//        List<Map<String, Object>> results = vmiAnalyzeMapper.getAssetStockOutList(schema_name, assetCode, pageSize, begin);
//        if(results != null && results.size() > 0){
//            String total = vmiAnalyzeMapper.totalAssetStockOutList(schema_name, assetCode);
//            Map<String, Object> totalMap = new HashMap<>();
//            totalMap.put("loading_start_time", selectOptionService.getLanguageInfo(LangConstant.TOTAL_T)+"：");//合计
//            totalMap.put("loading_total", total);
//            results.add(totalMap);
//        }
//        return results;
//    }
//
//    /**
//     * 根据设备code，查询设备出库信息列表总数
//     *
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Override
//    public int countAssetStockOutList(String schema_name, String assetCode) {
//        return vmiAnalyzeMapper.countAssetStockOutList(schema_name, assetCode);
//    }
//
//    /**
//     * 根据设备code，时间区间查询vim总存量
//     * @param request
//     * @param
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getVmiList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String condition = "";
//        String asset_code=request.getParameter("asset_code");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String monitor_name=request.getParameter("monitor_name");
//        try{
//            if(RegexUtil.isNotNull(asset_code)){
//                condition+="and asset_code='"+asset_code+"'";
//            }
//           if(RegexUtil.isNotNull(beginTime)){
//                condition+=" and gather_time >= '" + beginTime + " 00:00:00' ";
//            }
//            if(RegexUtil.isNotNull(endTime)){
//                condition+=" and gather_time < '"+endTime + " 23:59:00' ";
//            }
//            if(RegexUtil.isNotNull(monitor_name)){
//                condition+="and monitor_name='"+monitor_name+"'";
//            }
//            return vmiAnalyzeMapper.getTotalByTime(schema_name, condition);
//        }catch (Exception e){
//            return null;
//        }
//    }
//
//    /**
//     * 根据设备code，时间区间查询vim总存量 es查询
//     * @param request
//     * @param
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getVmiListFromEs(HttpServletRequest request) {
//        List<Map<String, Object>> resultdataList = new ArrayList<>();
//        Map<String, Object> resultMap = new HashMap<>();
//        SearchRequest searchRequest = new SearchRequest(index);
//        SimpleDateFormat sDateFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//
//        String asset_code = request.getParameter("asset_code");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String monitor_name = request.getParameter("monitor_name");
//        try {
//            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//            RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("System_Time_Tag");
//            if(RegexUtil.isNotNull(beginTime)){
//                rangeQueryBuilder.gte(sDateFormat.parse(beginTime).getTime());
//            }
//            if(RegexUtil.isNotNull(endTime)){
//                rangeQueryBuilder.lte(sDateFormat.parse(endTime).getTime());
//            }
//            String[] includes = {Constants.ASSET_MONITOR_AMOUNT_TANK_VOLUME, "System_Time","age"};
//            DateHistogramAggregationBuilder termsBuilder = AggregationBuilders.dateHistogram("hour").field("System_Time_Tag");
//            termsBuilder.calendarInterval(DateHistogramInterval.HOUR);//.offset("+8h");
//            termsBuilder.format(Constants.DATE_FMT_H);
//            termsBuilder.subAggregation((AggregationBuilders.max("maxValue").field(monitor_name)));
//            sourceBuilder.query(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("asset_code",asset_code)).must(rangeQueryBuilder)).sort("System_Time_Tag", SortOrder.DESC).size(100);
//            sourceBuilder.aggregation(termsBuilder);
//            searchRequest.source(sourceBuilder).scroll(TimeValue.timeValueMinutes(1L));
//            resultMap = elasticSearchService.search(searchRequest);
//            Map<String,Object> asMap= ( Map<String,Object>) resultMap.get("asMap");
//            Histogram timeAgg = (Histogram)asMap.get("hour");
//            for(Histogram.Bucket entry:timeAgg.getBuckets()){
//                Max max=entry.getAggregations().get("maxValue");
//                Map<String, Object> map = new HashMap<String, Object>(){{
//                        put("gather_time",entry.getKeyAsString());
//                        put("val",max.getValue());
//                    }};
//                    resultdataList.add(map);
//            }
//            return resultdataList;
//        } catch (Exception e) {
//            return resultdataList;
//        }
//    }
//
//    /**
//     * 录入物流信息
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel doAddVmiLogistics(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String orderNo = request.getParameter("orderNo");
//        String assetId = request.getParameter("assetId");
//        String facilitiesId = request.getParameter("facilitiesId");
//        String user_total = request.getParameter("user_total");
//        String buy_date = request.getParameter("buy_date");
//        int result = vmiAnalyzeMapper.addVmiLogistics(schemaName, orderNo, assetId, facilitiesId, new Double(user_total), buy_date, UtilFuns.sysTimestamp(), loginUser.getAccount());
//        if(result > 0) {
//            //同步物流信息
//            Map<String, Object> vmiAssetIot =  vmiAnalyzeMapper.getVmiIot(schemaName, orderNo);
//            if (vmiAssetIot != null){//同步物流数据
//                SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT_SS_3);
//                Map<String, String> urlSetting = iotUrlService.getUrlSetting(schemaName);//从系统配置获取物联接口地址、账号
//                String iotUrl = urlSetting.get("url");
//                String iotPassword = urlSetting.get("password");
//                String iotUsername = urlSetting.get("username");
//                if(StringUtils.isBlank(iotUrl) || StringUtils.isBlank(iotPassword) || StringUtils.isBlank(iotUsername))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_IOT_INTERFACE_ACCOUNT_WRONG));//物联接口地址、账号配置信息不全，任务执行失败
//
//                handleVarwriteInterface(schemaName, vmiAssetIot, urlSetting, iotUrl, df);
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_SAVE));
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL));
//    }
//
//    /**
//     * 物流发货
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel doSendVmiLogistics(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String order_no = request.getParameter("order_no");
//        String send_date = request.getParameter("send_date");
//        String arrive_date = request.getParameter("arrive_date");
//        int result = vmiAnalyzeMapper.sendVmiLogistics(schemaName, order_no, send_date, arrive_date, loginUser.getAccount());
//        if(result > 0)
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_SAVE));
//
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL));
//    }
//
//    /**
//     * 物流签收
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel doReceiveVmiLogistics(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String order_no = request.getParameter("order_no");
//        String sign_date = request.getParameter("sign_date");
//        String remark = request.getParameter("remark");
//        int result = vmiAnalyzeMapper.receiveVmiLogistics(schemaName, order_no, sign_date, remark, loginUser.getAccount());
//        if(result > 0)
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_SAVE));
//
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL));
//    }
//
//    /**
//     * 获取设备颜色列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> getVmiAssetColor(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String assetId = request.getParameter("assetId");
//        return vmiAnalyzeMapper.getAssetColorListByAssetId(schemaName, assetId);
//    }
//
//    /**
//     * 保存vmi设备颜色配置
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel doSaveVmiAssetColor(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String assetId = request.getParameter("assetId");
//        String colorSettingArray = request.getParameter("colorSettingArray");
//        if(StringUtils.isBlank(assetId) || StringUtils.isBlank(colorSettingArray))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//        JSONArray colorSettingJsonArray = new JSONArray(colorSettingArray);
//        if(colorSettingJsonArray == null || colorSettingJsonArray.length() == 0 )
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//        //清空原来的颜色配置表数据
//        vmiAnalyzeMapper.deleteVmiAssetColorByAssetId(schemaName, assetId);
//        List<Map<String, Object>> vmiAssetColorList = new ArrayList<>();//vmi设备颜色数据
//        for(int i=0;i<colorSettingJsonArray.length();i++){
//            JSONObject colorSetting = colorSettingJsonArray.getJSONObject(i);
//            colorSetting.put("asset_id", assetId);
//            vmiAssetColorList.add(JSON.parseObject(colorSetting.toString(),Map.class));
//        }
//        //批量保存vmi设备颜色数据
//        selectOptionService.batchInsertDatas(schemaName, vmiAssetColorList, "_sc_asset_vmi_alarm_color", SqlConstant._sc_asset_vmi_alarm_color_columns);
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUCC_SAVE));
//    }
//
//    /**
//     *  查询需要同步的vmi物联数据条数
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public int countAssetVmiTransferList(String schemaName){
//        return vmiAnalyzeMapper.countVmiIotList(schemaName);
//    }
//
//    /**
//     * 查询需要同步的vmi物联数据
//     *
//     * @param schemaName
//     */
//    @Override
//    public ResponseModel syncVmiIotData(String schemaName, int pageSize, int begin) {
//        try {
//            List<Map<String, Object>> vmiAssetIotList =  vmiAnalyzeMapper.getVmiIotList(schemaName, pageSize, begin);
//            if (vmiAssetIotList == null || vmiAssetIotList.isEmpty())
//                return ResponseModel.ok("");//没有需要同步的数据直接跳过
//
//            SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT_SS_3);
//            Map<String, String> urlSetting = iotUrlService.getUrlSetting(schemaName);//从系统配置获取物联接口地址、账号
//            String iotUrl = urlSetting.get("url");
//            String iotPassword = urlSetting.get("password");
//            String iotUsername = urlSetting.get("username");
//            if(StringUtils.isBlank(iotUrl) || StringUtils.isBlank(iotPassword) || StringUtils.isBlank(iotUsername))
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_IOT_INTERFACE_ACCOUNT_WRONG));//物联接口地址、账号配置信息不全，任务执行失败
//
//            for(Map<String, Object> vmiAssetIot : vmiAssetIotList){
//                handleVarwriteInterface(schemaName, vmiAssetIot, urlSetting, iotUrl, df);
//            }
//            return ResponseModel.ok("");
//        } catch (Exception e) {
//            return ResponseModel.errorMsg("syncVmiIotData fail");
//        }
//    }
//
//    /**
//     * 调用数据同步接口
//     * @param schemaName
//     * @param vmiAssetIot
//     * @param urlSetting
//     * @param iotUrl
//     * @param df
//     * @return
//     */
//     private void handleVarwriteInterface(String schemaName, Map<String, Object> vmiAssetIot, Map<String, String> urlSetting, String iotUrl, SimpleDateFormat df){
//         String inCode = (String) vmiAssetIot.get("in_code");
//         String assetId = (String)vmiAssetIot.get("asset_id");
//         String orderNo = (String)vmiAssetIot.get("order_no");
//         if(StringUtils.isBlank(inCode) || StringUtils.isBlank(orderNo) || orderNo.length() > 20)
//             return;
//
//        try {
//             double userTotal = (Double)vmiAssetIot.get("user_total");
//             Timestamp buyDate = (Timestamp)vmiAssetIot.get("buy_date");
//             //请求参数
//             JSONArray jsonArray = new JSONArray();
//             String now = df.format(new Date());//获取当前时间
//             handleOrderNo(jsonArray, orderNo, now);
//             handleDate(jsonArray, buyDate, now);
//             handleUserTotal(jsonArray, userTotal, now);
//             String dataUrl = iotUrl + "/api/vars/varwrite/" + inCode + "?access_token=" + tokenService.getToken(urlSetting);
//             dataUrl = dataUrl.replaceAll(" ", "%20");
//             //发送请求，调用接口
//             String response = HttpRequestUtils.requestJsonByPost(dataUrl, jsonArray.toString());
//             com.alibaba.fastjson.JSONObject result = com.alibaba.fastjson.JSONObject.parseObject(response);
//             String errorCode = result.getString("error_code");
//             if (StringUtils.isNotBlank(errorCode)) {
//                 if("21336".equals(errorCode))
//                 tokenService.refreshToken(urlSetting);
//                    return;
//             }
//             com.alibaba.fastjson.JSONObject resultSon = result.getJSONObject("result");
//             String deviceId = resultSon.getString("deviceId");
//             if (!deviceId.equals(inCode))
//                return;
//
//             vmiAnalyzeMapper.updateAssetVmiTransfer(schemaName, assetId, orderNo);
//         } catch (Exception e) {
//         }
//    }
//
//    /**
//     * 订单号字符串转换
//     * @param orderNo
//     * @return
//     */
//    private void handleOrderNo(JSONArray jsonArray, String orderNo, String timestamp){
//        char[] orderNoCharArray = orderNo.toCharArray();
//        for(int i=0;i<10;i++){
//            JSONObject json = new JSONObject();
//            StringBuffer c = new StringBuffer();
//            if(orderNoCharArray.length <= 2*i){
//                c.append("  ");
//            }else if(orderNoCharArray.length == (2*i+1)){
//                c.append(orderNoCharArray[2*i]).append(" ");
//            }else{
//                c.append(orderNoCharArray[2*i]).append(orderNoCharArray[2*i + 1]);
//            }
//            String ascii = String.valueOf(StringUtil.hexToInt(StringUtil.stringToHexAscii(c.toString(), "")));
//            json.put("id", "Order_Number"+(i+1));
//            json.put("address", String.valueOf(40134 + i));
//            json.put("val", ascii);
//            json.put("timestamp", timestamp);
//            jsonArray.put(json);
//        }
//    }
//
//    /**
//     * 购买日期字符串转换
//     * @param date
//     * @return
//     */
//    private void handleDate(JSONArray jsonArray, Timestamp date, String timestamp){
//        char[] orderNoCharArray = date.toString().toCharArray();
//        for(int i=0;i<5;i++){
//            JSONObject json = new JSONObject();
//            StringBuffer c = new StringBuffer();
//            if(orderNoCharArray.length <= 2*i){
//                c.append("  ");
//            }else if(orderNoCharArray.length == (2*i+1)){
//                c.append(orderNoCharArray[2*i]).append(" ");
//            }else{
//                c.append(orderNoCharArray[2*i]).append(orderNoCharArray[2*i + 1]);
//            }
//            String ascii = String.valueOf(StringUtil.hexToInt(StringUtil.stringToHexAscii(c.toString(), "")));
//            json.put("id", "Order_Date"+(i+1));
//            json.put("address", String.valueOf(40129 + i));
//            json.put("val", ascii);
//            json.put("timestamp", timestamp);
//            jsonArray.put(json);
//        }
//    }
//
//    /**
//     * 用量数据处理
//     * @param userTotal
//     * @return
//     */
//    private void handleUserTotal(JSONArray jsonArray, double userTotal, String timestamp){
//        JSONObject json = new JSONObject();
//        json.put("id", "Order_Quantity");
//        json.put("address", "40144");
//        json.put("val", String.valueOf(userTotal));
//        json.put("timestamp", timestamp);
//        jsonArray.put(json);
//    }



}
