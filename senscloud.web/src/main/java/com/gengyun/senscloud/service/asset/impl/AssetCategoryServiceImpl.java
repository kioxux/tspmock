package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.AssetCategoryMapper;
import com.gengyun.senscloud.mapper.AssetMapper;
import com.gengyun.senscloud.mapper.AssetModelMapper;
import com.gengyun.senscloud.model.AssetCategoryModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.asset.AssetCategoryService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AssetCategoryServiceImpl implements AssetCategoryService {

    @Resource
    AssetCategoryMapper assetCategoryMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    LogsService logService;
    @Resource
    AssetModelMapper assetModelMapper;
    @Resource
    AssetMapper assetMapper;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getAssetCategoryListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[1]);
    }

    /**
     * 获取设备类型列表信息
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 设备类型列表
     */
    @Override
    public List<Map<String, Object>> getAssetCategoryList(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        String searchWord = this.getAssetCategoryWhereString(assetCategoryModel);
        return assetCategoryMapper.findAssetCategoryList(methodParam.getSchemaName(), searchWord);
    }

    /**
     * 获取出自己外的设备类型列表
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 设备类型列表
     */
    @Override
    public List<Map<String, Object>> searchAssetCategoryLists(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        return assetCategoryMapper.findAssetCategoryLists(methodParam.getSchemaName());
    }

    /**
     * 根据category_id查找自定义字段信息
     *
     * @param methodParam 系统参数
     * @param categoryId  设备类型Id
     * @return 设备类型列表
     */
    @Override
    public Object findAssetCategoryFields(MethodParam methodParam, Integer categoryId) {
        return assetCategoryMapper.findAssetCategoryFields(methodParam.getSchemaName(), categoryId);
    }

    /**
     * 获取出自己外的设备类型列表
     *
     * @param methodParam 系统参数
     * @return 设备类型列表
     */
    @Override
    public List<Map<String, Object>> searchIotAssetCategoryLists(MethodParam methodParam) {
        return assetCategoryMapper.findIotAssetCategoryLists(methodParam.getSchemaName());
    }

    /**
     * 克隆设备类型自定义字段
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     */
    @Override
    public void cloneAssetCategoryFields(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        RegexUtil.optNotNullOrExp(assetCategoryModel.getAsset_category_id(), LangConstant.TAB_ASSET_B); // 设备类型不能为空
        RegexUtil.optNotNullOrExp(assetCategoryModel.getClone_asset_category_id(), LangConstant.TAB_ASSET_B); // 设备类型不能为空
        //获取被克隆的设备类型信息
        Map<String, Object> clone_asset_category = assetCategoryMapper.findAssetCategoryById(methodParam.getSchemaName(), assetCategoryModel.getClone_asset_category_id());
        //获取本设备类型信息
        Map<String, Object> asset_category = assetCategoryMapper.findAssetCategoryById(methodParam.getSchemaName(), assetCategoryModel.getAsset_category_id());
        asset_category.put("asset_category_id", asset_category.get("id"));
        if (RegexUtil.optNotNull(clone_asset_category).isPresent() && RegexUtil.optNotNull(asset_category).isPresent()) {
            if (!RegexUtil.optIsPresentStr(asset_category.get("fields"))) {
                asset_category.put("fields", clone_asset_category.get("fields"));
            } else if (!RegexUtil.optIsPresentStr(clone_asset_category.get("fields"))) {

            } else if (RegexUtil.optIsPresentStr(clone_asset_category.get("fields")) && RegexUtil.optIsPresentStr(asset_category.get("fields"))) {
                JSONArray clone_fields = JSONArray.fromObject(clone_asset_category.get("fields"));
                JSONArray fields = JSONArray.fromObject(asset_category.get("fields"));
                JSONArray newField = new JSONArray();
                clone_fields.stream().forEach(jsonObject -> newField.add(jsonObject));
                fields.stream().forEach(jsonObject -> newField.add(jsonObject));
                asset_category.put("fields", newField.toString());
            }
            assetCategoryMapper.updateAssetCategory(methodParam.getSchemaName(), asset_category);
        }
    }

    private String getAssetCategoryWhereString(AssetCategoryModel assetCategoryModel) {
        StringBuffer whereString = new StringBuffer();
        whereString.append(" where 1 = 1 ");
        if (RegexUtil.optIsPresentStr(assetCategoryModel.getKeywordSearch())) {
            whereString.append(" and (upper(t1.category_name) like upper('%" + assetCategoryModel.getKeywordSearch()
                    + "%') or upper(t1.category_code) like upper('%" + assetCategoryModel.getKeywordSearch()
                    + "%') or upper(t1.remark) like upper('%" + assetCategoryModel.getKeywordSearch() + "%'))");
        }
        if (RegexUtil.optIsPresentStr(assetCategoryModel.getIsUseSearch())) {
            whereString.append(" and t1.is_use = '" + assetCategoryModel.getIsUseSearch() + "'");
        }
        if (RegexUtil.optIsPresentStr(assetCategoryModel.getAssetCategorySearch())) {
            whereString.append(" and t2.id IN (" + assetCategoryModel.getAssetCategorySearch() + ") ");
        }
        return whereString.toString();
    }

    /**
     * 新增设备类型
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     */
    @Override
    public void newAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        RegexUtil.optNotNullOrExp(assetCategoryModel.getCategory_name(), LangConstant.TITLE_ASSET_C); // 设备类型名称不能为空
        RegexUtil.optNotNullOrExp(assetCategoryModel.getType(), LangConstant.TITLE_CATEGORY_A); // 种类不能为空
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("data_order", assetCategoryModel.getData_order());
        paramMap.put("category_name", assetCategoryModel.getCategory_name());
        paramMap.put("type", assetCategoryModel.getType());
        if (RegexUtil.optNotNull(assetCategoryModel.getParent_id()).isPresent()) {
            paramMap.put("parent_id", assetCategoryModel.getParent_id());
        } else {
            paramMap.put("parent_id", 0);
        }
        paramMap.put("is_use", 1);
        paramMap.put("category_code", assetCategoryModel.getCategory_code());
        paramMap.put("fields", assetCategoryModel.getFields());
        paramMap.put("create_time", new Date());
        paramMap.put("create_user_id", methodParam.getUserId());
        paramMap.put("remark", assetCategoryModel.getRemark());
        Integer id = assetCategoryMapper.insertAssetCategory(schemaName, paramMap);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7001, id.toString(), LangUtil.doSetLogArray("设备类型新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TAB_ASSET_B}));

    }

    /**
     * 编辑设备类型
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 返回数据
     */
    @Override
    public void modifyAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        RegexUtil.optNotNullOrExp(assetCategoryModel.getId(), LangConstant.TITLE_ASSET_C); // 设备类型名称不能为空
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> oldMap = assetCategoryMapper.findAssetCategoryById(methodParam.getSchemaName(), assetCategoryModel.getId());
        Map<String, Object> paramMap = new HashMap<String, Object>();
        String newFields = RegexUtil.optStrAndValOrNull(assetCategoryModel.getFields(), oldMap.get("fields"));//设备类型
        if (!RegexUtil.optIsPresentStr(newFields)) {
            newFields = "[]";
        }
        paramMap.put("parent_id", NumberUtils.toInt(RegexUtil.optStrAndValOrNull(assetCategoryModel.getParent_id(), oldMap.get("parent_id"))));
        paramMap.put("asset_category_id", RegexUtil.optStrAndValOrNull(assetCategoryModel.getId(), oldMap.get("id")));
        paramMap.put("data_order", RegexUtil.optStrAndValOrNull(assetCategoryModel.getData_order(), oldMap.get("data_order")));
        paramMap.put("category_name", RegexUtil.optStrAndValOrNull(assetCategoryModel.getCategory_name(), oldMap.get("category_name")));
        paramMap.put("type", RegexUtil.optStrAndValOrNull(assetCategoryModel.getType(), oldMap.get("type")));
        paramMap.put("is_use", RegexUtil.optStrAndValOrNull(assetCategoryModel.getIs_use(), oldMap.get("is_use")));
        paramMap.put("category_code", RegexUtil.optStrAndValOrNull(assetCategoryModel.getCategory_code(), oldMap.get("category_code")));
        paramMap.put("fields", newFields);
        paramMap.put("create_time", new Date());
        paramMap.put("create_user_id", methodParam.getUserId());
        paramMap.put("remark", RegexUtil.optStrAndValOrNull(assetCategoryModel.getRemark(), oldMap.get("remark")));
        assetCategoryMapper.updateAssetCategory(schemaName, paramMap);

        String oldFields = RegexUtil.optStrOrBlank(oldMap.get("fields"));//设备类型
        if ("".equals(oldFields)) {
            //旧数据无自定义时
        } else {
            JSONArray oldFieldsArray = JSONArray.fromObject(oldFields);
            JSONArray newFieldsArray = JSONArray.fromObject(newFields);
            if (null != oldFieldsArray && oldFieldsArray.size() > 0) {
                for (int i = 0; i < oldFieldsArray.size(); i++) {
                    boolean isDelete = true;
                    JSONObject oldFieldJson = oldFieldsArray.getJSONObject(i);
                    String old_field_code = oldFieldJson.getString("field_code");
                    if (null != newFieldsArray && newFieldsArray.size() > 0) {
                        //遍历新数据，查询出删除的，遍历删除型号和设备自定义
                        for (int x = 0; x < newFieldsArray.size(); x++) {
                            JSONObject newFieldJson = newFieldsArray.getJSONObject(x);
                            String new_field_code = newFieldJson.getString("field_code");
                            if (old_field_code.equals(new_field_code)) {
                                isDelete = false;
                                break;
                            }
                        }
                        if (isDelete) {
                            assetModelMapper.updateCustomFieldsByCategoryIdAndFieldCode(methodParam.getSchemaName(), assetCategoryModel.getId(), old_field_code);
                            assetMapper.updateCustomFieldsByCategoryIdAndFieldCode(methodParam.getSchemaName(), assetCategoryModel.getId(), old_field_code);
                        }
                    } else {
                        //清除型号和设备的自定义字段
                        assetModelMapper.updateModelCustomFieldsResetByCategoryId(methodParam.getSchemaName(), assetCategoryModel.getId());
                        assetMapper.updateCustomFieldsResetByCategoryId(methodParam.getSchemaName(), assetCategoryModel.getId());
                    }
                }
            }
            if (null != newFieldsArray && newFieldsArray.size() > 0) {
                for (int x = 0; x < newFieldsArray.size(); x++) {
                    boolean isAdd = true;
                    JSONObject newFieldJson = newFieldsArray.getJSONObject(x);
                    String new_field_code = newFieldJson.getString("field_code");
                    if (null != oldFieldsArray && oldFieldsArray.size() > 0) {
                        for (int i = 0; i < oldFieldsArray.size(); i++) {
                            JSONObject oldFieldJson = oldFieldsArray.getJSONObject(i);
                            String old_field_code = oldFieldJson.getString("field_code");
                            if (old_field_code.equals(new_field_code)) {
                                isAdd = false;
                                break;
                            }
                        }
                    }
                    if (isAdd) {
                        newFieldJson.put("field_value", "");
                        assetModelMapper.updateCustomFieldsByCategoryIdAndField(methodParam.getSchemaName(), assetCategoryModel.getId(), newFieldJson.toString());
                        assetMapper.updateCustomFieldsByCategoryIdAndField(methodParam.getSchemaName(), assetCategoryModel.getId(), newFieldJson.toString());
                    }
                }
            }

        }

        JSONArray loger = LangUtil.compareMap(paramMap, oldMap);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7001, assetCategoryModel.getId().toString(), LangUtil.doSetLogArray("更新了设备类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TAB_ASSET_B, loger.toString()}));

    }

    /**
     * 删除设备类型
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     */
    @Override
    public void cutAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        Integer assetCategoryId = (Integer) RegexUtil.optNotNullOrExp(assetCategoryModel.getAsset_category_id(), LangConstant.TITLE_ASSET_C); // 设备类型id不能为空
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> oldMap = assetCategoryMapper.findAssetCategoryById(methodParam.getSchemaName(), assetCategoryId);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            //判断该设备类型下是否有子设备类型  如果有 提示不可删除
            Map<String, Object> childMap = assetCategoryMapper.findAssetCategoryChildById(methodParam.getSchemaName(), assetCategoryId);
            if (RegexUtil.optNotNull(childMap).isPresent()) {
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TAB_ASSET_B, LangConstant.TAB_ASSET_B, LangConstant.TAB_ASSET_B});//设备类型下还有设备类型，不能删除设备类型
            }
            //判断该设备类型下是否有设备型号  如果有 提示不可删除
            Map<String, Object> modelMap = assetCategoryMapper.findAssetModelByCategoryId(methodParam.getSchemaName(), assetCategoryId);
            if (RegexUtil.optNotNull(modelMap).isPresent()) {
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TAB_ASSET_B, LangConstant.TAB_ASSET_A, LangConstant.TAB_ASSET_B});//设备类型下还有设备型号，不能删除设备类型
            }
            //判断该设备类型下是否有绑定的设备  如果有 提示不可删除
            Map<String, Object> assetMap = assetCategoryMapper.findAssetByAssetCategoryId(methodParam.getSchemaName(), assetCategoryId);
            if (RegexUtil.optNotNull(assetMap).isPresent()) {
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TAB_ASSET_B, LangConstant.TITLE_ASSET_I, LangConstant.TAB_ASSET_B});//设备类型下还有设备，不能删除设备类型
            }
            assetCategoryMapper.deleteAssetCategoryById(schemaName, assetCategoryId);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7001, assetCategoryId.toString(), LangUtil.doSetLogArray("删除了设备类型", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_ASSET_B, oldMap.get("category_name").toString()}));
        }
    }

    /**
     * 查看设备类型详情
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 设备类型信息
     */
    @Override
    public Map<String, Object> getAssetCategoryInfo(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        int asset_category_id = RegexUtil.optIntegerOrExpParam(assetCategoryModel.getAsset_category_id(), LangConstant.TITLE_ASSET_C); // 设备类型不能为空
        return this.getAssetCategoryInfoById(methodParam, asset_category_id);
    }

    /**
     * 根据设备类型主键获取设备类型信息
     *
     * @param methodParam       系统参数
     * @param asset_category_id 设备类型主键
     * @return 设备类型信息
     */
    @Override
    public Map<String, Object> getAssetCategoryInfoById(MethodParam methodParam, Integer asset_category_id) {
        return RegexUtil.optNotNullMap(assetCategoryMapper.findAssetCategoryById(methodParam.getSchemaName(), asset_category_id)).map(m -> {
            m.put("fields", RegexUtil.optNotBlankStrOpt(m.get("fields")).filter(RegexUtil::optIsPresentStr).map(JSONArray::fromObject).orElseGet(JSONArray::new));
            return m;
        }).orElseGet(HashMap::new);
    }

    /**
     * 根据设备类型获取自定义字段
     *
     * @param methodParam       系统参数
     * @param asset_category_id 设备类型主键
     * @return 自定义参数
     */
    @Override
    public String getfieidsById(MethodParam methodParam, Integer asset_category_id) {
        return assetCategoryMapper.findfieldsById(methodParam.getSchemaName(), asset_category_id);
    }

    /**
     * 验证设备类型是否存在
     *
     * @param schemaName
     * @param assetTypeList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryAssetTypeByCode(String schemaName, List<String> assetTypeList) {
        return assetCategoryMapper.queryAssetTypeByCode(schemaName, assetTypeList);
    }

    /**
     * 根据设备类型id获取类型父级拼接字符串
     *
     * @param schemaName 数据库
     * @return 设备类型id获取类型父级拼接字符串
     */
    @Override
    public Map<Integer, Map<String, Object>> getCategoryNameString(String schemaName) {
        return assetCategoryMapper.findCategoryNameString(schemaName);
    }
}
