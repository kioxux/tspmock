package com.gengyun.senscloud.service;

/**
 * 设备监控报警异常短信发送记录
 * Created by DwD on 2018/8/6.
 */
public interface AlarmAnomalySendInfoService {
//    //提供定时器调用方法
//
//    void dealAlarmAnomalySendInfo();
//
//    //新增报警异常记录信息
//    int addAlarmAnomalySendInfo(String schema_name, AlarmAnomalySendInfoResult alarmAnomalySendInfo);
//
//    //修改短信发送次数
//    int updateAlarmAnomalySendInfoSum(String schema_name,String alarm_code, String monitor_name);
//
//    //修改设备监控报警异常的处理状态
//    int updateStatusByAlarm_code(String schema_name,String alarm_code, int status);
//
//    //查询设备异常监控数据信息
//    List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoDataList(String schema_name,String condition);
//
//    int getAlarmAnomalySendInfoDataListCount(String schema_name,String selectId);
//
//    //实时监控报警记录查询单据根据设备id
//    List<AlarmAnomalySendInfoResult> getAlarmHistoryData(String schema_name,String assetCode,String monitor_name);
//
//    int getAlarmHistoryDataCount(String schema_name,String assetCode,String monitor_name);
//
//    //根据异常编号查询异常报警单据
//    List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoDataListByAlarmCode(String schema_name,String alarm_code);
//
//    //此处暂时不需要编辑生成工单的接口，因为在别的地方存在service
//
//    //判断监控记录是否存在报警记录信息(时间区间还是需要界定的)
//    int isAlarmInfo(String schema_name, String asset_code, String monitor_name);
//
//    List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoData(String schema_name, String asset_code, String monitor_name);

}
