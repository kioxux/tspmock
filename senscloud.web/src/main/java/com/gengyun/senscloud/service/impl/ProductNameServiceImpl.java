package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.ProductNameService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 1:36 2019/10/25 0025
 */
@Service
public class ProductNameServiceImpl implements ProductNameService {
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    ProductNameMapper productNameMapper;
//
//    @Autowired
//    AuthService authService;
//
//    @Override
//    public String findProductNameList( HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 20;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 20;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String[] facilitySearch = {};
//        try {
//            facilitySearch = request.getParameterValues("facilities")[0].split(",");
//        } catch (Exception ex) {
//
//        }
//        String keyWord = request.getParameter("keyWord");
//        User user = AuthService.getLoginUser(request);
//
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        //根据人员的角色，判断获取值的范围
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and product_name like upper('%" + keyWord + "%') ";
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        dataList = productNameMapper.query(schema_name, condition, pageSize, begin);
//        int total = productNameMapper.queryList(schema_name, condition);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    @Override
//    public ResponseModel add(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        String product_name = request.getParameter("product_name");
//        if(product_name.isEmpty()){
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//        }
//        if(!product_name.isEmpty()){
//            if(id ==null || id.isEmpty()){
//                int result = productNameMapper.add(schema_name, product_name);
//                if(result>0){
//                    return ResponseModel.ok("ok");
//                }
//            }else{
//                int id_int = Integer.parseInt(id);
//                int result = productNameMapper.edit(schema_name, id_int, product_name);
//                if(result>0){
//                    return ResponseModel.ok("ok");
//                }
//            }
//
//        }else{
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//        }
//        return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    @Override
//    public ResponseModel dele(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        if(id.isEmpty()){
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//        }
//        if(id.isEmpty()){
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PRODUCT_NAME_NULL));//产品名称不允许为空
//        }
//        int id_int = Integer.parseInt(id);
//        int result = productNameMapper.dele(schema_name, id_int);
//        if( result >0 ){
//            return ResponseModel.ok("ok");
//        }
//        return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
}
