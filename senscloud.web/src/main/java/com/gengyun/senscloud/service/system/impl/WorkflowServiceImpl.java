package com.gengyun.senscloud.service.system.impl;

import com.alibaba.fastjson.JSONObject;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.WorkFlowTemplateMapper;
import com.gengyun.senscloud.mapper.WorkflowManageMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkflowSearchParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.response.WorkflowStartResult;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.*;
import com.gengyun.senscloud.view.WorkFlowDataExportView;
import org.apache.commons.io.FilenameUtils;
import org.flowable.bpmn.model.*;
import org.flowable.bpmn.model.Process;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.engine.*;
import org.flowable.engine.impl.util.ProcessDefinitionUtil;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.runtime.ProcessInstanceQuery;
import org.flowable.image.ProcessDiagramGenerator;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.zip.ZipInputStream;

/**
 * 工作流
 */
@Service
public class WorkflowServiceImpl implements WorkflowService {
    private static final Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);
    @Resource
    WorkflowManageMapper workflowManageMapper;
    @Resource
    WorkFlowTemplateMapper workFlowTemplateMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    RepositoryService repositoryService;
    @Resource
    TenantInfoHolder tenantInfoHolder;
    @Resource
    RuntimeService runtimeService;
    @Resource
    SpringProcessEngineConfiguration springProcessEngineConfiguration;
    @Resource
    IdentityService identityService;
    @Resource
    FormService formService;
    @Resource
    TaskService taskService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    WorksService worksService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getWorkflowPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[10]);
    }

    /**
     * 部署工作流
     *
     * @param methodParam 系统参数
     * @param file        部署文件
     */
    @Override
    public void newWorkflow(MethodParam methodParam, MultipartFile file) {
        RegexUtil.optNotNullOrExp(file, LangConstant.TEXT_CHOOSE_A, new String[]{LangConstant.TEXT_W}); // 请选择文件
        String fileName = RegexUtil.optStrOrBlank(file.getOriginalFilename());
        String fileType = ".xml";
        RegexUtil.falseExp(fileName.contains(fileType), LangConstant.TEXT_AF, new String[]{fileName, LangConstant.TEXT_W, fileType}); // {d}【{0}】格式错误，应该是{1}格式{d}
        String errMsg = null;
        boolean isErr = false;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_11;
            String schemaName = methodParam.getSchemaName();
            tenantInfoHolder.setCurrentTenantId(schemaName);
            InputStream is = null;
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_12;
                is = file.getInputStream();
                if (null != is) {
                    DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
                    String extension = FilenameUtils.getExtension(fileName);
                    String name = fileName.split("\\.")[0];
                    if ("zip".equals(extension) || "bar".equals(extension)) {
                        ZipInputStream zip = new ZipInputStream(is);
                        deploymentBuilder = deploymentBuilder.addZipInputStream(zip);
                    } else {
                        deploymentBuilder = deploymentBuilder.addInputStream(fileName, is);
                    }
                    Deployment deployment = deploymentBuilder.tenantId(schemaName).enableDuplicateFiltering().name(name).deploy();
                    ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionTenantId(schemaName).deploymentId(deployment.getId()).singleResult();
                    repositoryService.setDeploymentKey(deployment.getId(), processDefinition.getKey());
                    repositoryService.setDeploymentCategory(deployment.getId(), processDefinition.getCategory());
                    errMsg = ErrorConstant.EC_WORKFLOW_14;
                    RegexUtil.optNotBlankStrOpt(processDefinition.getId()).ifPresent(pd -> RegexUtil.optNotBlankStrOpt(processDefinition.getKey()).ifPresent(pk -> {
                        Map<String, Object> data = new HashMap<>();
                        data.put("schema_name", schemaName);
                        data.put("pref_id", pk);
                        data.put("flow_code", pd);
                        workFlowTemplateMapper.updateWorkFlowInfo(data);
                    }));
                }
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_13, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                SenscloudUtil.closeInputStream(is);
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_13);
            if (!isErr) {
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg, fileName);
        }
    }

    /**
     * 获取工作流部署对象列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getWorkflowDeploymentList(MethodParam methodParam, Map<String, Object> paramMap) {
        paramMap.put("schemaName", methodParam.getSchemaName());
        int total = workflowManageMapper.selectDeploymentCount(paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        if (total > 0) {
            if (RegexUtil.booleanCheck(methodParam.getNeedPagination())) {
                SenscloudUtil.changePagination(methodParam, paramMap);
                result.put("rows", workflowManageMapper.selectDeploymentPageList(paramMap));
            } else {
                result.put("rows", workflowManageMapper.selectDeploymentList(paramMap));
            }
        } else {
            result.put("rows", null);
        }
        return result;
    }

    /**
     * 删除工作流部署对象
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    @Override
    public void cutWorkflowDeployment(MethodParam methodParam, String id) {
        RegexUtil.optStrOrError(id, methodParam, ErrorConstant.EC_WORKFLOW_8);
        String errMsg = null;
        boolean isErr = false;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_9;
            tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_10;
                repositoryService.deleteDeployment(id, true);
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_10, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_10);
            if (!isErr) {
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg, id);
        }
    }

    /**
     * 获取工作流定义列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getWorkflowDefinitionList(MethodParam methodParam, Map<String, Object> paramMap) {
        paramMap.put("schemaName", methodParam.getSchemaName());
        boolean showLatest = RegexUtil.optBool(paramMap.get("showLatest"));
        int total = showLatest ? workflowManageMapper.selectLatestDefinitionCount(paramMap) : workflowManageMapper.selectDefinitionCount(paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        if (total > 0) {
            if (RegexUtil.booleanCheck(methodParam.getNeedPagination())) {
                SenscloudUtil.changePagination(methodParam, paramMap);
                result.put("rows", showLatest ? workflowManageMapper.selectLatestDefinitionPageList(paramMap) : workflowManageMapper.selectDefinitionPageList(paramMap));
            } else {
                result.put("rows", showLatest ? workflowManageMapper.selectLatestDefinitionList(paramMap) : workflowManageMapper.selectDefinitionList(paramMap));
            }
        } else {
            result.put("rows", null);
        }
        return result;
    }

    /**
     * 获取工作流定义流程图
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    @Override
    public void getWorkflowProcessDiagram(MethodParam methodParam, String id) {
        RegexUtil.optStrOrError(id, methodParam, ErrorConstant.EC_WORKFLOW_106);
        String errMsg = null;
        boolean isErr = false;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_109;
            tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
            InputStream is = null;
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_107;
                is = repositoryService.getProcessDiagram(id);
                if (null != is) {
                    errMsg = ErrorConstant.EC_WORKFLOW_110;
//                    ImageUtils.doShowImageByDftWah(ImageIO.read(is));
                    DownloadUtil.prototypeDownloadByIs(is, "", DownloadUtil.CONTENT_TYPE_OS);
                }
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_108, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                SenscloudUtil.closeInputStream(is);
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_108);
            if (!isErr) {
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg, id);
        }
    }

    /**
     * 获取工作流实例列表
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getWorkflowInstanceList(MethodParam methodParam, Map<String, Object> paramMap) {
        paramMap.put("schemaName", methodParam.getSchemaName());
        int total = workflowManageMapper.selectInstanceCount(paramMap);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        if (total > 0) {
            if (RegexUtil.booleanCheck(methodParam.getNeedPagination())) {
                SenscloudUtil.changePagination(methodParam, paramMap);
                List<String> ids = workflowManageMapper.selectInstanceIdPageList(paramMap);
                if (RegexUtil.optIsPresentList(ids)) {
                    paramMap.put("ids", ids);
                    result.put("rows", workflowManageMapper.selectInstancePageList(paramMap));
                }
            }
        }
        return result;
    }

    /**
     * 删除工作流实例
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    @Override
    public void cutWorkflowInstance(MethodParam methodParam, String id) {
        RegexUtil.optStrOrError(id, methodParam, ErrorConstant.EC_WORKFLOW_206);
        String errMsg = null;
        boolean isErr = false;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_207;
            tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_208;
                runtimeService.deleteProcessInstance(id, "delete");
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_208, se);
                throw new SenscloudException(errMsg);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_208);
            if (!isErr) {
                logger.error(errMsg, e);
            }
            throw new SenscloudError(methodParam, errMsg, id);
        }
    }

    /**
     * 删除流程名称之外的工作流实例
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     */
    @Override
    public void cutWorkflowInstanceFilterFlowName(MethodParam methodParam, Map<String, Object> paramMap) {
        Map<String, Object> idPrm = new HashMap<>();
        idPrm.put("schemaName", methodParam.getSchemaName());
        methodParam.setPageSize(workflowManageMapper.selectInstanceCount(idPrm));
        methodParam.setPageNumber(1);
        SenscloudUtil.changePagination(methodParam, idPrm);
        List<String> ids = workflowManageMapper.selectInstanceIdPageList(idPrm);
        paramMap.put("schemaName", methodParam.getSchemaName());
        RegexUtil.optNotNullList(workflowManageMapper.selectInstancePageList(paramMap)).ifPresent(l -> {
            List<String> insIds = l.stream().filter(m -> !RegexUtil.optStrOrBlank(m.get("pref_name")).contains(RegexUtil.optStrOrBlank(paramMap.get("keywordSearch"))))
                    .map(t -> RegexUtil.optStrOrBlank(t.get("id"))).distinct().collect(Collectors.toList());
            ExecutorService pool = Executors.newFixedThreadPool(50);
            insIds.forEach(s -> RegexUtil.optNotBlankStrOpt(s).filter(ids::contains).ifPresent(d -> {
                pool.execute(() -> {
                    long startTime = System.currentTimeMillis();
                    try {
                        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
                        runtimeService.deleteProcessInstance(d, "delete");
                    } catch (Exception e) {
                        logger.error(ErrorConstant.EC_WORKFLOW_209, e);
                    } finally {
                        tenantInfoHolder.clearCurrentTenantId();
                    }
                    logger.info("time：" + (System.currentTimeMillis() - startTime) + "ms");
                });
            }));
            pool.shutdown();
        });
    }

    /**
     * 根据子工单编号删除工作流实例
     *
     * @param methodParam 系统参数
     * @param subWorkCode 子工单编号
     * @param reason      理由
     */
    @Override
    public void cutWorkflowInstanceBySubWorkCode(MethodParam methodParam, String subWorkCode, String reason) {
        RegexUtil.optStrOrExpNotNull(subWorkCode, LangConstant.MSG_BI);
        List<String> subWorkCodeList = Arrays.asList(subWorkCode);
        List<Map<String, Object>> taskList = getTasksBySubWorkCodes(methodParam, subWorkCodeList);
        if (RegexUtil.optIsPresentList(taskList)) {
            List<String> insIds = taskList.stream().map(t -> RegexUtil.optStrOrBlank(t.get("process_instance_id"))).filter(RegexUtil::optIsPresentStr).collect(Collectors.toList());
            deleteProcessInstanceBatch(methodParam, insIds, reason);
            Set<String> defIds = taskList.stream().map(t -> RegexUtil.optStrOrBlank(t.get("process_definition_id"))).filter(RegexUtil::optIsPresentStr).filter(s -> s.contains("_subflow")).collect(Collectors.toSet());
            if (defIds.size() > 0) {
                ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery().processDefinitionTenantId(methodParam.getSchemaName()).processDefinitionIds(defIds);
                tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
                try {
                    processDefinitionQuery.list().forEach(definition -> {
                        repositoryService.deleteDeployment(definition.getDeploymentId(), true);
                    });
                } catch (Exception e) {
                    logger.error("", e);
                    throw new SenscloudException(LangConstant.MSG_BK);
                } finally {
                    tenantInfoHolder.clearCurrentTenantId();
                }
            }
        } else {
            List<String> insList = getInstanceIdListBySubWorkCodeList(methodParam, subWorkCodeList, null);
            deleteProcessInstanceBatch(methodParam, insList, reason);
        }
    }

    /**
     * 批量删除流程实例
     *
     * @param methodParam 系统参数
     * @param list        实例id
     * @param reason      理由
     */
    private void deleteProcessInstanceBatch(MethodParam methodParam, List<String> list, String reason) {
        if (RegexUtil.optIsPresentList(list)) {
            tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
            try {
                list.forEach(id -> runtimeService.deleteProcessInstance(id, reason));
            } catch (Exception e) {
                logger.error("", e);
                throw new SenscloudException(LangConstant.MSG_BK);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        }
    }

    /**
     * 根据业务单号集合获取流程实例信息集合
     *
     * @param methodParam     系统参数
     * @param subWorkCodeList 子工单编码
     * @param paramMap        请求参数
     * @return
     */
    private List<String> getInstanceIdListBySubWorkCodeList(MethodParam methodParam, List<String> subWorkCodeList, Map<String, String> paramMap) {
        if (!RegexUtil.optIsPresentList(subWorkCodeList)) {
            return null;
        }
        List<String> result = new ArrayList<>();
        subWorkCodeList.forEach(code -> {
            String flowInstanceId = getInstanceIdBySubWorkCode(methodParam, code, paramMap);
            if (RegexUtil.optIsPresentStr(flowInstanceId)) {
                result.add(flowInstanceId);
            }
        });
        return result;
    }

    /**
     * 根据业务单号获取流程实例信息
     *
     * @param methodParam 系统参数
     * @param subWorkCode 子工单编码
     * @param paramMap    请求参数
     * @return
     */
    private String getInstanceIdBySubWorkCode(MethodParam methodParam, String subWorkCode, Map<String, String> paramMap) {
        if (RegexUtil.optIsBlankStr(subWorkCode)) {
            return null;
        }
        ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery().processInstanceTenantId(methodParam.getSchemaName());
        if (RegexUtil.optIsPresentMapStr(paramMap)) {
            String businessKey = paramMap.get("businessKey");
            if (RegexUtil.optIsPresentStr(businessKey)) {
                query = query.processInstanceBusinessKey(businessKey);
            }
            String definitionId = paramMap.get("definitionId");
            if (RegexUtil.optIsPresentStr(definitionId)) {
                query = query.processDefinitionId(definitionId);
            }
        }
        query.variableValueEquals(FlowParmsConstant.SUB_WORK_CODE, subWorkCode);
        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
        try {
            List<ProcessInstance> processInstanceList = query.list();
            if (RegexUtil.optIsPresentList(processInstanceList)) {
                return processInstanceList.get(0).getId();
            }
        } catch (Exception e) {
            logger.error("", e);
            throw new SenscloudException(LangConstant.MSG_BK);
        } finally {
            tenantInfoHolder.clearCurrentTenantId();
        }
        return null;
    }

    /**
     * 获取工作流实例流程图
     *
     * @param methodParam 系统参数
     * @param id          数据主键
     */
    @Override
    public void getWorkflowInstanceDiagram(MethodParam methodParam, String id) {
        RegexUtil.optStrOrError(id, methodParam, ErrorConstant.EC_WORKFLOW_201);
        String errMsg = null;
        boolean isErr = false;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_204;
            String schemaName = methodParam.getSchemaName();
            tenantInfoHolder.setCurrentTenantId(schemaName);
            InputStream is = null;
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_202;
                ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceTenantId(schemaName).processInstanceId(id).singleResult();
                ProcessDefinition pde = repositoryService.getProcessDefinition(processInstance.getProcessDefinitionId());
                if (null != pde && pde.hasGraphicalNotation()) {
                    BpmnModel bpmnModel = repositoryService.getBpmnModel(pde.getId());
                    ProcessDiagramGenerator diagramGenerator = springProcessEngineConfiguration.getProcessDiagramGenerator();
                    is = diagramGenerator.generateDiagram(bpmnModel, "png", runtimeService.getActiveActivityIds(processInstance.getId()), Collections.emptyList(),
                            springProcessEngineConfiguration.getActivityFontName(), springProcessEngineConfiguration.getLabelFontName(),
                            springProcessEngineConfiguration.getAnnotationFontName(), springProcessEngineConfiguration.getClassLoader(), 1.0, springProcessEngineConfiguration.isDrawSequenceFlowNameWithNoLabelDI());
                    if (null != is) {
                        errMsg = ErrorConstant.EC_WORKFLOW_205;
//                        ImageUtils.doShowImageByDftWah(ImageIO.read(is));
                        DownloadUtil.prototypeDownloadByIs(is, "", DownloadUtil.CONTENT_TYPE_OS);
                    }
                }
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_203, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                SenscloudUtil.closeInputStream(is);
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_203);
            if (!isErr) {
                errMsg = ErrorConstant.EC_WORKFLOW_203;
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg, id);
        }
    }

    /**
     * 获取工作流节点流程图
     *
     * @param methodParam 系统参数
     * @param pageParam   页面参数
     * @param id          数据主键
     */
    @Override
    public void getWorkflowNodeDiagram(MethodParam methodParam, WorkflowSearchParam pageParam, String id) {
        String flow_code = RegexUtil.optStrOrError(pageParam.getFlow_code(), methodParam, ErrorConstant.EC_WORKFLOW_401);
        RegexUtil.optStrOrError(id, methodParam, ErrorConstant.EC_WORKFLOW_402);
        String errMsg = null;
        boolean isErr = false;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_404;
            String schemaName = methodParam.getSchemaName();
            tenantInfoHolder.setCurrentTenantId(schemaName);
            InputStream is = null;
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_403;
                ProcessDefinition pde = repositoryService.getProcessDefinition(flow_code);
                if (null != pde && pde.hasGraphicalNotation()) {
                    BpmnModel bpmnModel = repositoryService.getBpmnModel(pde.getId());
                    ProcessDiagramGenerator diagramGenerator = springProcessEngineConfiguration.getProcessDiagramGenerator();
                    is = diagramGenerator.generateDiagram(bpmnModel, "png", Collections.singletonList(id), Collections.emptyList(),
                            springProcessEngineConfiguration.getActivityFontName(), springProcessEngineConfiguration.getLabelFontName(),
                            springProcessEngineConfiguration.getAnnotationFontName(), springProcessEngineConfiguration.getClassLoader(), 1.0, springProcessEngineConfiguration.isDrawSequenceFlowNameWithNoLabelDI());
                    if (null != is) {
                        errMsg = ErrorConstant.EC_WORKFLOW_405;
//                        ImageUtils.doShowImageByDftWah(ImageIO.read(is));
                        DownloadUtil.prototypeDownloadByIs(is, "", DownloadUtil.CONTENT_TYPE_OS);
                    }
                }
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_406, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                SenscloudUtil.closeInputStream(is);
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_406);
            if (!isErr) {
                errMsg = ErrorConstant.EC_WORKFLOW_406;
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg, id);
        }
    }

    /**
     * 工作流流程、节点信息列表获取
     *
     * @param methodParam 系统参数
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getWorkflowInstDspList(MethodParam methodParam) {
        String errMsg = null;
        boolean isErr = false;
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_301;
            String schemaName = methodParam.getSchemaName();
            tenantInfoHolder.setCurrentTenantId(schemaName);
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_302;
                ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery().processDefinitionTenantId(schemaName);
                errMsg = ErrorConstant.EC_WORKFLOW_303;
                query = query.latestVersion();
                errMsg = ErrorConstant.EC_WORKFLOW_304;
                List<ProcessDefinition> pdList = query.list();
                errMsg = ErrorConstant.EC_WORKFLOW_305;
                if (null != pdList && pdList.size() > 0) {
                    for (ProcessDefinition pd : pdList) {
                        errMsg = ErrorConstant.EC_WORKFLOW_306;
                        if (RegexUtil.optStrOrBlank(pd.getKey()).contains("_subflow")) {
                            continue;
                        }
                        ProcessDefinition pde = repositoryService.getProcessDefinition(pd.getId());
                        errMsg = ErrorConstant.EC_WORKFLOW_307;
                        if (null != pde && pde.hasGraphicalNotation()) {
                            errMsg = ErrorConstant.EC_WORKFLOW_308;
                            String pdeId = RegexUtil.optStrOrBlank(pde.getId());
                            BpmnModel bpmnModel = repositoryService.getBpmnModel(pdeId);
                            errMsg = ErrorConstant.EC_WORKFLOW_309;
                            if (null != bpmnModel) {
                                Collection<FlowElement> feList = bpmnModel.getProcessById(pde.getKey()).getFlowElements();
                                if (null != feList && feList.size() > 0) {
                                    errMsg = ErrorConstant.EC_WORKFLOW_310;
                                    Map<String, Object> flowInfo = new HashMap<>();
                                    String flowKey = pde.getKey();
//                                    String id = SenscloudUtil.generateUUIDStr();
                                    flowInfo.put("id", flowKey);
                                    flowInfo.put("name", pde.getName());
                                    flowInfo.put("is_node", false);
                                    flowInfo.put("node_type", "flow");
                                    flowInfo.put("flow_code", pdeId);
                                    flowInfo.put("pageId", pdeId); // id
                                    list.add(flowInfo);
                                    errMsg = ErrorConstant.EC_WORKFLOW_311;
                                    for (FlowElement fe : feList) {
                                        RegexUtil.optNotBlankStrOpt(fe.getName()).filter(n -> !(fe instanceof SequenceFlow || fe instanceof ServiceTask)).ifPresent(n -> {
                                            Map<String, Object> nodeInfo = new HashMap<>();
                                            nodeInfo.put("id", fe.getId());
                                            nodeInfo.put("name", n);
                                            nodeInfo.put("is_node", true);
                                            if (fe instanceof StartEvent) {
                                                nodeInfo.put("node_type", "start");
                                            } else if (fe instanceof EndEvent) {
                                                nodeInfo.put("node_type", "end");
                                            } else if (fe instanceof CallActivity) {
                                                nodeInfo.put("node_type", "subNode");
                                            } else {
                                                nodeInfo.put("node_type", "node");
                                            }
                                            nodeInfo.put("parent", flowKey);
                                            nodeInfo.put("pageId", pdeId + fe.getId()); // SenscloudUtil.generateUUIDStr()
                                            nodeInfo.put("parentPageId", pdeId); // id
                                            list.add(nodeInfo);
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_312, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_312);
            if (!isErr) {
                errMsg = ErrorConstant.EC_WORKFLOW_312;
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg);
        }
        return list;
    }

    /**
     * 根据流程号获取节点下拉框
     *
     * @param methodParam 系统参数
     * @param flowCode    流程编号
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getWorkflowNodeListById(MethodParam methodParam, String flowCode) {
        RegexUtil.optStrOrError(flowCode, methodParam, ErrorConstant.EC_WORKFLOW_601);
        String errMsg = null;
        boolean isErr = false;
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_602;
            String schemaName = methodParam.getSchemaName();
            tenantInfoHolder.setCurrentTenantId(schemaName);
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_603;
                ProcessDefinition pde = repositoryService.getProcessDefinition(flowCode);
                errMsg = ErrorConstant.EC_WORKFLOW_604;
                if (null != pde && pde.hasGraphicalNotation()) {
                    errMsg = ErrorConstant.EC_WORKFLOW_605;
                    BpmnModel bpmnModel = repositoryService.getBpmnModel(pde.getId());
                    errMsg = ErrorConstant.EC_WORKFLOW_606;
                    if (null != bpmnModel) {
                        Collection<FlowElement> feList = bpmnModel.getProcessById(pde.getKey()).getFlowElements();
                        if (null != feList && feList.size() > 0) {
                            errMsg = ErrorConstant.EC_WORKFLOW_607;
                            for (FlowElement fe : feList) {
                                RegexUtil.optNotBlankStrOpt(fe.getName()).filter(n -> !(fe instanceof SequenceFlow || fe instanceof ServiceTask)).ifPresent(n -> {
                                    Map<String, Object> nodeInfo = new HashMap<>();
                                    nodeInfo.put("value", fe.getId());
                                    nodeInfo.put("text", n);
                                    list.add(nodeInfo);
                                });
                            }
                        }
                    }
                }
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_608, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_608);
            if (!isErr) {
                errMsg = ErrorConstant.EC_WORKFLOW_608;
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg);
        }
        return list;
    }

    /**
     * 启动流程-带表单参数
     *
     * @param methodParam
     * @param processDefinitionId
     * @param starter
     * @param formProperties
     * @return
     */
    @Override
    public WorkflowStartResult startWithForm(MethodParam methodParam, String processDefinitionId, String starter, Map<String, String> formProperties) {
        mergeMethodParamAndFormProperties(methodParam, formProperties, null);
        WorkflowStartResult workflowStartResult;
        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
        try {
            identityService.setAuthenticatedUserId(starter);
            ProcessInstance processInstance = formService.submitStartFormData(processDefinitionId, formProperties);
            String processInstanceId = processInstance.getId();
            workflowStartResult = new WorkflowStartResult(true, null);
            //如果开始节点就是结束节点直接调用回调方法
            if (processInstance.isEnded()) {
                List<FlowElement> flowElements = (List<FlowElement>) repositoryService.getBpmnModel(processInstance.getProcessDefinitionId()).getProcessById(processInstance.getProcessDefinitionKey()).getFlowElements();
                if (RegexUtil.optNotNull(flowElements).isPresent() && flowElements.size() == 3) {
                    for (FlowElement flowElement : flowElements
                    ) {
                        if (flowElement instanceof EndEvent) {
                            EndEvent endEvent = (EndEvent) flowElement;
                            formProperties.put("node_id", endEvent.getId());
                        }
                    }
                    //获取结束流程节点
                    worksService.callBackStartWithForm(methodParam, null, formProperties, true, FlowableEngineEventType.TASK_CREATED);
                }
            }
            logger.debug("start process of {pdid={}, pid={}, formProperties={}}", processDefinitionId, processInstanceId, formProperties);
        } catch (Exception e) {
            logger.error("start process fail {pdid={}, error={}}", processDefinitionId, e.getLocalizedMessage());
            throw e;
        } finally {
            identityService.setAuthenticatedUserId(null);
            tenantInfoHolder.clearCurrentTenantId();
        }
        return workflowStartResult;
    }


    /**
     * 分配任务（根据流程实例主键taskId）
     *
     * @param methodParam
     * @param taskId
     * @param paramMap
     * @return
     */
    @Override
    public ResponseModel assign(MethodParam methodParam, String taskId, Map<String, String> paramMap) {
        mergeMethodParamAndFormProperties(methodParam, null, paramMap);
        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
        try {
            getFormProperties(paramMap).forEach((key, value) -> {
                if (!RegexUtil.optIsPresentStr(value)) {
                    taskService.removeVariable(taskId, key);
                } else {
                    taskService.setVariable(taskId, key, value);
                }
            });
            taskService.setAssignee(taskId, methodParam.getAccount());
        } catch (Exception e) {
            logger.error("assign task fail {tid={}, uid = {}, error={}}", taskId, methodParam.getAccount(), e.getLocalizedMessage());
            throw new SenscloudException(LangConstant.MSG_BK);
        } finally {
            tenantInfoHolder.clearCurrentTenantId();
        }
        return ResponseModel.okMsg(LangConstant.MSG_I);
    }

    /**
     * 分配任务（根据工单编号subworkcode）
     *
     * @param methodParam
     * @param subWorkCode
     * @param paramMap
     * @return
     */
    @Override
    public ResponseModel assignWithSWC(MethodParam methodParam, String subWorkCode, String userId, Map<String, String> paramMap) {
        mergeMethodParamAndFormProperties(methodParam, null, paramMap);
        List<Map<String, Object>> taskList = getTasksBySubWorkCodes(methodParam, Arrays.asList(subWorkCode));
        if (taskList != null && taskList.size() > 0) {
            tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
            try {
                taskList.forEach(task -> {
                    String id = RegexUtil.optStrOrBlank(task.get("task_id"));
                    getFormProperties(paramMap).forEach((key, value) -> {
                        if (RegexUtil.optIsPresentStr(value)) {
                            taskService.setVariable(id, key, value);
                        } else {
                            taskService.removeVariable(id, key);
                        }
                    });
                    taskService.setAssignee(id, userId);
                });
            } catch (Exception e) {
                logger.error("assign task fail {swc={}, uid = {}, error={}}", subWorkCode, methodParam.getAccount(), e.getLocalizedMessage());
                throw new SenscloudException(LangConstant.MSG_BK);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        }
        return ResponseModel.okMsg(LangConstant.MSG_I);
    }

    /**
     * 分配所有子任务
     *
     * @param methodParam
     * @param subWorkCodes
     * @param paramMap
     * @return
     */
    @Override
    public ResponseModel parentAssign(MethodParam methodParam, List<String> subWorkCodes, String userId, Map<String, String> paramMap) {
        mergeMethodParamAndFormProperties(methodParam, null, paramMap);
        List<Map<String, Object>> taskList = getTasksBySubWorkCodes(methodParam, subWorkCodes);
        if (taskList != null && taskList.size() > 0) {
            tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
            try {
                taskList.forEach(task -> {
                    String id = RegexUtil.optStrOrBlank(task.get("task_id"));
                    getFormProperties(paramMap).forEach((key, value) -> {
                        if (RegexUtil.optIsPresentStr(value)) {
                            taskService.setVariableLocal(id, key, value);
                        } else {
                            taskService.removeVariableLocal(id, key);
                        }
                    });
                    taskService.setAssignee(id, userId);
                });
            } catch (Exception e) {
                logger.error("assign tasks fail {swc={}, account = {}, error={}}", subWorkCodes, methodParam.getAccount(), e.getLocalizedMessage());
                throw new SenscloudException(LangConstant.MSG_BK);
            }
            tenantInfoHolder.clearCurrentTenantId();
        } else {
            throw new SenscloudException(LangConstant.MSG_BK);
        }
        return ResponseModel.okMsg(LangConstant.MSG_I);
    }

    /**
     * 完成任务
     *
     * @param methodParam
     * @param taskId
     * @param paramMap
     * @return
     */
    @Override
    public ResponseModel complete(MethodParam methodParam, String taskId, Map<String, String> paramMap) {
        mergeMethodParamAndFormProperties(methodParam, null, paramMap);
        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
        identityService.setAuthenticatedUserId(methodParam.getAccount());
        try {
            formService.submitTaskFormData(taskId, paramMap);
        } catch (Exception e) {
            logger.error("complete task fail {tid={}, uid = {}, error={}}", taskId, methodParam.getAccount(), e.getLocalizedMessage());
            throw new SenscloudException(RegexUtil.optNotNull(e.getCause()).filter(c -> c instanceof SenscloudException).map(Throwable::getMessage)
                    .filter(RegexUtil::optIsPresentStr).orElse(LangConstant.MSG_BK));
        } finally {
            identityService.setAuthenticatedUserId(null);
            tenantInfoHolder.clearCurrentTenantId();
        }
        return ResponseModel.okMsg(LangConstant.MSG_I);
    }

    /**
     * 子工单完成任务
     *
     * @param methodParam
     * @param taskId
     * @param paramMap
     * @return
     */
    @Override
    public ResponseModel subComplete(MethodParam methodParam, String taskId, Map<String, String> paramMap) {
        mergeMethodParamAndFormProperties(methodParam, null, paramMap);
        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
        identityService.setAuthenticatedUserId(methodParam.getAccount());
        try {
            String tstr = paramMap.get("main_sub_work_code");
            ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery().processInstanceTenantId(methodParam.getSchemaName());
            query.variableValueEquals("sub_work_code", tstr);
            List<ProcessInstance> processInstanceList = query.list();
            taskService.setVariable(taskId, "do_flow_key", paramMap.get("do_flow_key"));
            taskService.setVariable(taskId, "request_param", paramMap.get("request_param"));
            runtimeService.setVariable(processInstanceList.get(0).getProcessInstanceId(), "do_flow_key", paramMap.get("do_flow_key"));
            runtimeService.setVariableLocal(processInstanceList.get(0).getProcessInstanceId(), "do_flow_key", paramMap.get("do_flow_key"));
            formService.submitTaskFormData(taskId, paramMap);
        } catch (Exception e) {
            logger.error("complete task fail {tid={}, uid = {}, error={}}", taskId, methodParam.getAccount(), e.getLocalizedMessage());
            throw new SenscloudException(LangConstant.MSG_BK);
        } finally {
            identityService.setAuthenticatedUserId(null);
            tenantInfoHolder.clearCurrentTenantId();
        }
        return ResponseModel.okMsg(LangConstant.MSG_I);
    }

    /**
     * 获取所有流程变量
     *
     * @param methodParam
     * @param taskId
     * @return
     */
    @Override
    public Map<String, Object> getTaskVariables(MethodParam methodParam, String taskId) {
        return getTaskVariables(methodParam.getSchemaName(), taskId);
    }

    /**
     * 获取所有流程变量
     *
     * @param schema
     * @param taskId
     * @return
     */
    @Override
    public Map<String, Object> getTaskVariables(String schema, String taskId) {
        Map<String, Object> result = null;
        tenantInfoHolder.setCurrentTenantId(schema);
        try {
            result = taskService.getVariables(taskId);
        } catch (Exception e) {
            logger.error("get task variables fail {tid={}, error={}}", taskId, e.getLocalizedMessage());
            throw new SenscloudException(LangConstant.MSG_BK);
        } finally {
            tenantInfoHolder.clearCurrentTenantId();
        }
        return result;
    }

    /**
     * 获取流程变量
     *
     * @param methodParam
     * @param taskId
     * @param variableName
     * @return
     */
    @Override
    public Object getTaskVariableByName(MethodParam methodParam, String taskId, String variableName) {
        Object result = null;
        tenantInfoHolder.setCurrentTenantId(methodParam.getSchemaName());
        try {
            result = taskService.getVariable(taskId, variableName);
        } catch (Exception e) {
            logger.error("get task variables fail {tid={}, error={}}", taskId, e.getLocalizedMessage());
            throw new SenscloudException(LangConstant.MSG_BK);
        } finally {
            tenantInfoHolder.clearCurrentTenantId();
        }
        return result;
    }

    /**
     * 查询待办列表
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 列表
     */
    @Override
    public Map<String, Object> searchTaskList(MethodParam methodParam, WorkflowSearchParam wfParam) {
        try {
            long total = getTasksCount(methodParam, wfParam);
            Map<String, Object> pageResult = new HashMap<>();
            pageResult.put("rows", new ArrayList<>());
            if (!RegexUtil.booleanCheck(wfParam.getCount_only())) {
                Map<String, Map<String, Boolean>> businessTypeId2PermissionMap = new HashMap<>();
                // 工单管理、设备调拨、备件调拨等权限合并
                Map<String, Map<String, Boolean>> modelPrmByKey = pagePermissionService.getModelPrmByKeyArray(methodParam, SensConstant.MENUS[16], SensConstant.MENUS[14], SensConstant.MENUS[0], SensConstant.MENUS[12]);
                RegexUtil.optNotNullList(selectOptionService.getSelectOptionList(methodParam, "business_permission_relation", null))
                        .ifPresent(l -> l.forEach(wp -> wp.keySet().forEach(k -> businessTypeId2PermissionMap.put(String.valueOf(wp.get("value")), modelPrmByKey.get(String.valueOf(wp.get("text")))))));
                RegexUtil.optNotNullList(getTasks(methodParam, wfParam))
                        .ifPresent(l -> {
                            l.forEach(o -> o.put("permission", handlePermission(methodParam, RegexUtil.optStrOrBlank(o.get("duty_user_id")),
                                    businessTypeId2PermissionMap.getOrDefault(RegexUtil.optStrOrBlank(o.get("business_type_id")), new HashMap<>()), modelPrmByKey.get(SensConstant.MENUS[16]), o)));
                            pageResult.put("rows", l);
                        });
            }
            pageResult.put("total", total);
            return pageResult;
        } catch (Exception ex) {
            logger.error("", ex);
            throw new SenscloudException(LangConstant.MSG_BK);
        }
    }

    /**
     * 根据工单编号获取代码流程
     *
     * @param methodParam
     * @param subWorkCodes
     * @return
     */
    @Override
    public List<Map<String, Object>> getTasksBySubWorkCodes(MethodParam methodParam, List<String> subWorkCodes) {
        WorkflowSearchParam wfParam = new WorkflowSearchParam();
        wfParam.setSubWorkCodesSearch(subWorkCodes);
        return getTasks(methodParam, wfParam);
    }

    /**
     * 获取按天分组工作流任务列表
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 列表
     */
    @Override
    public List<Map<String, Object>> getFlowTaskGroupList(MethodParam methodParam, WorkflowSearchParam wfParam) {
        return RegexUtil.optNotNullListStr(workflowManageMapper.findFlowTaskCodeList(methodParam.getSchemaName(), methodParam.getUserId(), wfParam.getMine_only(), wfParam.getSubWorkCodesSearch())).map(cl ->
                workflowManageMapper.findFlowTaskGroupList(methodParam.getSchemaName(), wfParam, cl, methodParam.getUserId())).orElse(null);
    }

    /**
     * 获取待办流程列表
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 列表
     */
    @Override
    public List<Map<String, Object>> getTasks(MethodParam methodParam, WorkflowSearchParam wfParam) {
        return RegexUtil.optNotNullListStr(workflowManageMapper.findFlowTaskCodeList(methodParam.getSchemaName(), methodParam.getUserId(), wfParam.getMine_only(), wfParam.getSubWorkCodesSearch())).map(cl ->
                workflowManageMapper.findFlowTaskList(methodParam.getSchemaName(), wfParam, cl, methodParam.getUserId(), SenscloudUtil.changePaginationNoErr(methodParam))).orElse(null);
    }

    /**
     * 获取待办流程数量
     *
     * @param methodParam 系统参数
     * @param wfParam     页面参数
     * @return 数量
     */
    @Override
    public int getTasksCount(MethodParam methodParam, WorkflowSearchParam wfParam) {
        return RegexUtil.optNotNullListStr(workflowManageMapper.findFlowTaskCodeList(methodParam.getSchemaName(), methodParam.getUserId(), wfParam.getMine_only(), wfParam.getSubWorkCodesSearch())).map(cl ->
                workflowManageMapper.countFlowTask(methodParam.getSchemaName(), wfParam, cl, methodParam.getUserId())).orElse(0);
    }

    /**
     * 根据业务单号获取节点主键（当前节点/下一步节点）
     *
     * @param methodParam 系统参数
     * @param paramMap    页面参数
     * @return 节点主键
     */
    @Override
    public String getNodeIdBySubWorkCode(MethodParam methodParam, Map<String, Object> paramMap) {
        String errType = "search_node";
        String subWorkCode = RegexUtil.optStrOrPrmError(paramMap.get(FlowParmsConstant.SUB_WORK_CODE), methodParam, ErrorConstant.EC_WORKFLOW_502, errType);
        String errMsg = null;
        boolean isErr = false;
        String nodeId = null;
        try {
            errMsg = ErrorConstant.EC_WORKFLOW_501;
            String schemaName = methodParam.getSchemaName();
            tenantInfoHolder.setCurrentTenantId(schemaName);
            try {
                errMsg = ErrorConstant.EC_WORKFLOW_503;
                ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery().processInstanceTenantId(schemaName);
                query.variableValueEquals(FlowParmsConstant.SUB_WORK_CODE, subWorkCode);
                List<ProcessInstance> processInstanceList = query.list();
                errMsg = ErrorConstant.EC_WORKFLOW_504;
                if (null == processInstanceList || processInstanceList.size() == 0) {
                    nodeId = RegexUtil.optNotNullList(this.getTasksBySubWorkCodes(methodParam, Collections.singletonList(subWorkCode)))
                            .map(l -> l.get(0)).filter(RegexUtil::optIsPresentMap).map(f -> RegexUtil.optStrOrNull(f.get("task_id"))).filter(RegexUtil::optIsPresentStr)
                            .map(i -> RegexUtil.optStrOrNull(getTaskVariableByName(methodParam, i, "node_id"))).orElse(null);
                } else {
                    nodeId = runtimeService.getActiveActivityIds(processInstanceList.get(0).getId()).get(0);
                }
                nodeId = RegexUtil.optStrOrVal(nodeId, "end");
            } catch (Exception se) {
                isErr = true;
                logger.error(ErrorConstant.EC_WORKFLOW_505, se.getMessage());
                throw new SenscloudException(errMsg);
            } finally {
                tenantInfoHolder.clearCurrentTenantId();
            }
        } catch (Exception e) {
            errMsg = RegexUtil.optStrOrVal(errMsg, ErrorConstant.EC_WORKFLOW_505);
            if (!isErr) {
                logger.error(errMsg, e.getMessage());
            }
            throw new SenscloudError(methodParam, errMsg, errType + "_" + subWorkCode);
        }
        return nodeId;
    }

    private Map<String, String> getFormProperties(Map<String, String> paramMap) {
        Map<String, String> formProperties = new HashMap<>();
        Set<Map.Entry<String, String>> entrySet = paramMap.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            String key = entry.getKey();
            if (RegexUtil.optStrOrVal(key, "").startsWith(Constants.FP_PREFIX)) {
                formProperties.put(key.replace(Constants.FP_PREFIX, ""), RegexUtil.optStrOrVal(entry.getValue(), ""));
            }
        }
        return formProperties;
    }

    /**
     * 获取taskKey
     *
     * @param methodParam 系统参数
     * @param subWorkCode 页面参数
     * @return taskKey
     */
    @Override
    public String getTaskKeyBySubWorkCode(MethodParam methodParam, String subWorkCode) {
        return RegexUtil.optNotNullList(this.getTasksBySubWorkCodes(methodParam, Collections.singletonList(subWorkCode)))
                .map(l -> l.get(0)).filter(RegexUtil::optIsPresentMap).map(f -> RegexUtil.optStrOrNull(f.get("task_definition_key"))).orElse(null);
    }

    /**
     * 获取taskId
     *
     * @param methodParam 系统参数
     * @param subWorkCode 页面参数
     * @return taskKey
     */
    @Override
    public String getTaskIdBySubWorkCode(MethodParam methodParam, String subWorkCode) {
        return RegexUtil.optNotNullList(this.getTasksBySubWorkCodes(methodParam, Collections.singletonList(subWorkCode)))
                .map(l -> l.get(0)).filter(RegexUtil::optIsPresentMap).map(f -> RegexUtil.optStrOrNull(f.get("task_id"))).orElse(null);
    }

    /**
     * 判断操作权限
     *
     * @param methodParam
     * @param assignee
     * @param permissionMap          待办数据对应的功能模块上(工单列表、设备工单列表、备件工单列表等)的操作权限
     * @param workSheetPermissionMap 待办列表的操作权限
     * @return
     */
    private Map<String, Boolean> handlePermission(MethodParam methodParam, String assignee, Map<String, Boolean> permissionMap, Map<String, Boolean> workSheetPermissionMap, Map<String, Object> o) {
        if (workSheetPermissionMap != null) {
            Boolean isDistribution = workSheetPermissionMap.get("isDistribution");
            Boolean isDetail = workSheetPermissionMap.get("isDetail");
            Boolean isEdit = workSheetPermissionMap.get("isEdit");
            Boolean isDelete = workSheetPermissionMap.get("isDelete");
            if (isDistribution) {
                // 如果负责人为空，且有分配权限，则显示分配按钮
                isDistribution = !RegexUtil.optIsPresentStr(assignee);
            }
            if (isDetail) {
                // 如果登录账号不是本条待办的责任人，且有详情显示权限，则显示详情按钮
                isDetail = !RegexUtil.optEquals(methodParam.getUserId(), RegexUtil.optStrOrBlank(assignee));
            }
            if (isEdit) {
                // 如果登录账号是本条待办的负责人，且有处理权限，则显示处理按钮
                isEdit = RegexUtil.optEquals(methodParam.getUserId(), RegexUtil.optStrOrBlank(assignee));
            }
            if (permissionMap != null) {
                // 判断待办数据对应各自模块的操作权限，只有待办和各自模块的操作权限都为true时，才给到客户操作权限
                if (!RegexUtil.booleanCheck(permissionMap.get("isDistribution"))) {
                    isDistribution = false;
                }
                if (!RegexUtil.booleanCheck(permissionMap.get("isDetail"))) {
                    isDetail = false;
                }
                if (!RegexUtil.booleanCheck(permissionMap.get("isEdit"))) {
                    isEdit = false;
                }
                if (!RegexUtil.booleanCheck(permissionMap.get("isDelete"))) {
                    isDelete = false;
                }
                // 如果为子工单 则不能进行作废操作
                if (RegexUtil.optIsPresentStr(o.get(FlowParmsConstant.MAIN_SUB_WORK_CODE))) {
                    isDelete = false;
                }
            }
            Map<String, Boolean> pm = new HashMap<>();
            pm.putAll(workSheetPermissionMap);
            pm.put("isDistribution", isDistribution);
            pm.put("isDetail", isDetail);
            pm.put("isEdit", isEdit);
            pm.put("isDelete", isDelete);
            return pm;
        } else {
            return new HashMap<>();
        }
    }

    /**
     * 添加请求参数到工作流自定义参数
     *
     * @param methodParam    入参
     * @param formProperties 入参
     */
    private void mergeMethodParamAndFormProperties(MethodParam methodParam, Map<String, String> formProperties, Map<String, String> variables) {
        if (RegexUtil.optIsPresentMapStr(formProperties)) {
            formProperties.put(FlowParmsConstant.REQUEST_PARAM, JSONObject.toJSONString(methodParam));
        }
        if (RegexUtil.optIsPresentMapStr(variables)) {
            variables.put(FlowParmsConstant.REQUEST_PARAM, JSONObject.toJSONString(methodParam));
        }
    }

    /**
     * 判断是否下一步为结束节点  如果是返回结束节点id  不是为null
     *
     * @param taskEntity 页面参数
     * @return nodeId
     */
    @Override
    public String isEndEvent(TaskEntity taskEntity) {
        String curActId = taskEntity.getTaskDefinitionKey();
        String procDefId = taskEntity.getProcessDefinitionId();
        Process process = ProcessDefinitionUtil.getProcess(procDefId);
        //遍历整个process,找到endEventId是什么，与当前taskId作对比
        List<FlowElement> flowElements = (List<FlowElement>) process.getFlowElements();
        String id = null;
        // && taskEntity.getVariable(FlowParmsConstant.DO_FLOW_KEY).equals("4")
        if (RegexUtil.optNotNull(taskEntity.getVariable(FlowParmsConstant.DO_FLOW_KEY)).isPresent()) {
            //当前节点
            SequenceFlow nowFlow = null;
            //结束节点集合
            List<EndEvent> endFlowList = new ArrayList<>();
            for (FlowElement flowElement : flowElements) {
                //当前节点
                if (flowElement instanceof SequenceFlow) {
                    SequenceFlow flow = (SequenceFlow) flowElement;
                    if (flow.getSourceFlowElement().getId().equals(curActId)) {
                        nowFlow = flow;
                    }
                    FlowElement sourceFlowElement = flow.getSourceFlowElement();
                    FlowElement targetFlowElement = flow.getTargetFlowElement();
                    //如果当前边的下一个节点是endEvent，那么获取当前边
                    if (targetFlowElement instanceof EndEvent && sourceFlowElement.getId().equals(curActId)) {
                        System.out.println("下一个是结束节点！！");
                        id = targetFlowElement.getId();
                        flow.getConditionExpression();
                        return id;
                    }
                }
                if (flowElement instanceof EndEvent) {
                    endFlowList.add((EndEvent) flowElement);
                }
            }
            for (EndEvent endFlow : endFlowList) {
                List<SequenceFlow> sequenceFlowList = endFlow.getIncomingFlows();
                for (SequenceFlow sequenceFlow : sequenceFlowList) {
                    if (RegexUtil.optNotNull(sequenceFlow.getConditionExpression()).isPresent()) {
                        if (sequenceFlow.getConditionExpression().equals("${do_flow_key == '" + taskEntity.getVariable(FlowParmsConstant.DO_FLOW_KEY) + "'}") && sequenceFlow.getSourceRef().equals(nowFlow.getTargetRef())) {
                            return endFlow.getId();
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取待办任务权限列表
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getTaskListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[16]);
    }

    @Override
    public void clearWorkflow(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("pref_id"), LangConstant.TITLE_ACJ);
        workflowManageMapper.deleteWorkFlowNodeColumLinkageCondition(methodParam.getSchemaName(), paramMap);
        workflowManageMapper.deleteWorkFlowNodeColumLinkage(methodParam.getSchemaName(), paramMap);
        workflowManageMapper.deleteWorkFlowNodeColumExt(methodParam.getSchemaName(), paramMap);
        workflowManageMapper.deleteWorkFlowNodeColum(methodParam.getSchemaName(), paramMap);
        workflowManageMapper.deleteWorkFlowNode(methodParam.getSchemaName(), paramMap);
        workflowManageMapper.deleteWorkFlowInfo(methodParam.getSchemaName(), paramMap);
    }

    @Override
    public ModelAndView exportWorkFlow(MethodParam methodParam, Map<String, Object> paramMap) {
        String prefId = RegexUtil.optStrOrExpNotSelect(paramMap.get("pref_id"), LangConstant.TITLE_ACJ);
        Map<String, Object> map = new HashMap<>();
        String schemaName = methodParam.getSchemaName();

        map.put("work_flow_info", workflowManageMapper.findWorkFlowInfo(schemaName, paramMap));
        map.put("work_flow_node", workflowManageMapper.findWorkFlowNode(schemaName, paramMap));
        map.put("work_flow_node_colum", workflowManageMapper.findWorkFlowNodeColum(schemaName, paramMap));
        map.put("work_flow_node_colum_ext", workflowManageMapper.findWorkFlowNodeColumExt(schemaName, paramMap));
        map.put("work_flow_node_colum_linkage", workflowManageMapper.findWorkFlowNodeColumLinkage(schemaName, paramMap));
        map.put("work_flow_node_colum_linkage_codition", workflowManageMapper.findWorkFlowNodeColumLinkageCondition(schemaName, paramMap));

        RegexUtil.optNotNullListStr(workflowManageMapper.findWtCodesForFlow(schemaName, prefId)).ifPresent(wcList -> {
            this.getWtCodesForWt(schemaName, prefId, wcList, wcList);
            RegexUtil.optNotNullListStr(wcList.stream().filter(RegexUtil::optIsPresentStr).distinct().collect(Collectors.toList())).ifPresent(codes -> {
                map.put("work_template", workflowManageMapper.findWorkTemplate(schemaName, codes));
                map.put("work_template_colum_linkage_condition", workflowManageMapper.findWorkTemplateColumLinkageCondition(schemaName, codes));
                map.put("work_template_colum_linkage", workflowManageMapper.findWorkTemplateColumLinkage(schemaName, codes));
                map.put("work_template_colum_ext", workflowManageMapper.findWorkTemplateColumExt(schemaName, codes));
                map.put("work_template_colum", workflowManageMapper.findWorkTemplateColum(schemaName, codes));
            });
        });

//        List<LinkedHashMap<String, Object>> dataSource = workflowManageMapper.findDataSource(schemaName, paramMap);
//        map.put("data_source", dataSource);
        map.put("methodParam", methodParam);
        WorkFlowDataExportView excelView = new WorkFlowDataExportView();
        return new ModelAndView(excelView, map);
    }

    /**
     * 递归查询流程所有模板编号
     *
     * @param schemaName 数据库
     * @param prefId     流程号
     * @param list       模板编号集合
     * @param prtList    父级模板编号集合
     */
    private void getWtCodesForWt(String schemaName, String prefId, List<String> list, List<String> prtList) {
        List<String> tmpList = workflowManageMapper.findWtCodesForWt(schemaName, prefId, prtList);
        if (null != tmpList && tmpList.size() > 0) {
            int i = 0;
            for (String s : tmpList) {
                if (!list.contains(s)) {
                    list.add(s);
                    i++;
                }
            }
            if (i > 0) {
                this.getWtCodesForWt(schemaName, prefId, list, tmpList);
            }
        }
    }
}
