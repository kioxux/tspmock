package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.BomWorksMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.dynamic.BomWorksService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 备件工单管理
 */
@Service
public class BomWorksServiceImpl implements BomWorksService {
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    private BomWorksMapper bomWorksMapper;
    @Resource
    private SelectOptionService selectOptionService;

    /**
     * 获取按钮功能权限信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getBwListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[31]);
    }

    @Override
    public Map<String, Object> getBomWorksList(MethodParam methodParam, Map<String, Object> pm) {
        try {
            methodParam.setNeedPagination(true);
            String pagination = SenscloudUtil.changePagination(methodParam); // 分页参数
            String nowUserId = methodParam.getUserId();
            pm.put("user_id", nowUserId);
            pm.put("pagination", pagination);
            if (RegexUtil.optNotNull(pm.get("workTypeIdSearch")).isPresent()) {
                String[] workTypeIdSearch = RegexUtil.optNotBlankStrOpt(pm.get("workTypeIdSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
                pm.put("workTypeIdSearch", workTypeIdSearch);
            }
            if (RegexUtil.optNotNull(pm.get("bomTypeIdSearch")).isPresent()) {
                String[] bomTypeIdSearch = RegexUtil.optNotBlankStrOpt(pm.get("bomTypeIdSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
                pm.put("bomTypeIdSearch", bomTypeIdSearch);
            }
            if (RegexUtil.optNotNull(pm.get("statusSearch")).isPresent()) {
                String[] statusSearch = RegexUtil.optNotBlankStrOpt(pm.get("statusSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
                pm.put("statusSearch", statusSearch);
            }
            Integer total = bomWorksMapper.findBomWorksCount(methodParam.getSchemaName(), pm, nowUserId);
            if (total < 1) {
                Map<String, Object> result = new HashMap<>();
                result.put("total", total);
                result.put("rows", new ArrayList<>());
                return result;
            }
            List<Map<String, Object>> rows = bomWorksMapper.findBomWorksPage(methodParam.getSchemaName(), pm, nowUserId);
            for (Map<String, Object> result : rows) {
                List<String> menuList = methodParam.getUserPermissionList();
                // 业务类型
                String businessTypeId = RegexUtil.optNotNullMap(selectOptionService.getWorkTypeById(methodParam, RegexUtil.optIntegerOrNull(result.get("work_type_id")))).map(m -> RegexUtil.optStrOrNull(m.get("business_type_id"))).orElse(null);
                result.put("isEdit", false);
                result.put("isDetail", false);
                result.put("isDelete", false);
                result.put("isDistribution", false);
                int status = RegexUtil.optIntegerOrVal(result.get("status"), StatusConstant.NULL_STATUS, methodParam, ErrorConstant.EC_LIST_003, RegexUtil.optStrOrBlank(result.get("status")));
                RegexUtil.optNotBlankStrOpt(result.get("duty_user_id")).ifPresent(d -> {
                    if (menuList.contains("bom_io_edit") && d.equals(nowUserId) && status < StatusConstant.COMPLETED) {
                        result.put("isEdit", true); // 判断是否有处理权限
                    }
                });
                // 备件盘点草稿状态可以继续处理
                if (SensConstant.BUSINESS_NO_3005.equals(businessTypeId)) {
                    RegexUtil.optNotBlankStrOpt(result.get("create_user_id")).ifPresent(d -> {
                        if (menuList.contains("bom_io_edit") && d.equals(nowUserId) && status == StatusConstant.DRAFT) {
                            result.put("isEdit", true); // 判断是否有处理权限
                        }
                    });
                    RegexUtil.optNotBlankStrOpt(result.get("create_user_id")).ifPresent(d -> {
                        RegexUtil.optNotNullIntegerOpt(result.get("is_have_sub"), "0").ifPresent(s -> {
                            if (menuList.contains("bom_io_edit") && (d.equals(nowUserId) && status == StatusConstant.DRAFT || s == 1)) {
                                result.put("isEdit", true); // 判断是否有处理权限
                            }
                        });
                    });
                }
                // 判断是否有作废权限
                if (menuList.contains("bom_io_delete") && status < StatusConstant.COMPLETED) {
                    result.put("isDelete", true);
                } else if (status <= StatusConstant.DRAFT && RegexUtil.optEquals(result.get("create_user_id"), nowUserId)) {
                    result.put("isDelete", true);
                }
                // 判断是否有查看详情权限
                if (menuList.contains("bom_io_detail")) {
                    result.put("isDetail", true);
                }
                // 判断是否有分配权限
                if (menuList.contains("work_sheet_distribution")) {
                    result.put("isDistribution", true);
                }
                // 如果状态为已关闭 已完成 已作废  则只能查看
                if (status == StatusConstant.COMPLETED_CLOSE
                        || status == StatusConstant.COMPLETED
                        || status == StatusConstant.CANCEL) {
                    result.put("isEdit", false);
                    result.put("isDelete", false);
                    result.put("isDistribution", false);
                }
                // 如果是备件调拨单需要单独处理作废权限
                if (SensConstant.BUSINESS_NO_3002.equals(businessTypeId)) {
                    if (status == StatusConstant.TO_BE_CONFIRM) {
                        result.put("isDelete", true);
                    } else {
                        result.put("isDelete", false);
                    }
                }
            }
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", rows);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
