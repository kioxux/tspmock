package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.SecurityManageMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.SecurityManageService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 职安健管理
 */
@Service
public class SecurityManageServiceImpl implements SecurityManageService {
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    LogsService logService;
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    SecurityManageMapper securityManageMapper;

    /**
     * 查询列表
     *
     * @param paramMap 页面条件
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getSecurityManage(MethodParam methodParam, Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        paramMap.put("pagination", pagination);
        Map<String, Object> result = new HashMap<>();
        int total = securityManageMapper.countSecurityManageList(methodParam.getSchemaName(), paramMap);
        if (total > 0) {
            List<Map<String, Object>> list = securityManageMapper.querySecurityManageList(methodParam.getSchemaName(), paramMap);
            result.put("total", total);
            result.put("rows", list);
        } else {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
        }
        return result;
    }

    /**
     * 通过code查询详情
     *
     * @param paramMap 页面条件
     * @return 列表数据
     */
    @Override
    public Map<String, Object> getSecurityDetailByCode(MethodParam methodParam, Map<String, Object> paramMap) {
        String security_item_code = RegexUtil.optStrOrExpNotNull(paramMap.get("security_item_code"), LangConstant.MSG_A);
        return securityManageMapper.getSecurityDetailByCode(methodParam.getSchemaName(), security_item_code);
    }


    /**
     * 更新职安健
     *
     * @param paramMap 页面条件
     */
    @Override
    public void modifySecurityManage(MethodParam methodParam, Map<String, Object> paramMap) {
        String security_item_code = RegexUtil.optStrOrExpNotNull(paramMap.get("security_item_code"), LangConstant.MSG_A);
        Map<String, Object> oldMap = securityManageMapper.getSecurityDetailByCode(methodParam.getSchemaName(), security_item_code);
        securityManageMapper.updateSecurityManage(methodParam.getSchemaName(), paramMap);
        JSONArray loger = LangUtil.compareMap(paramMap, oldMap);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7013, security_item_code, LangUtil.doSetLogArray("编辑了职安健", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_KQ, loger.toString()}));
    }

    /**
     * 删除职安健
     *
     * @param paramMap 页面条件
     */
    @Override
    public void cutSecurityManage(MethodParam methodParam, Map<String, Object> paramMap) {
        String security_item_code = RegexUtil.optStrOrExpNotNull(paramMap.get("security_item_code"), LangConstant.MSG_A);
        Map<String, Object> oldMap = securityManageMapper.getSecurityDetailByCode(methodParam.getSchemaName(), security_item_code);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            securityManageMapper.deleteSecurityManage(methodParam.getSchemaName(), security_item_code);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7013, security_item_code, LangUtil.doSetLogArray("删除了附件文档", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_KQ, security_item_code}));
        }
    }

    /**
     * 新增职安健
     *
     * @param paramMap 页面条件
     */
    @Override
    public void newSecurityManage(MethodParam methodParam, Map<String, Object> paramMap) {
        String security_item_code = serialNumberService.generateMaxBsCodeByType(methodParam, "security_item");
        paramMap.put("security_item_code", security_item_code);
        paramMap.put("create_user_id", methodParam.getUserId());
        securityManageMapper.insertSecurityManage(methodParam.getSchemaName(), paramMap);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7013, security_item_code, LangUtil.doSetLogArray("添加了职安健", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_KQ, security_item_code}));
    }

    /**
     * 有所职安健列表
     */
    @Override
    public List<Map<String, Object>> getSecurityManageList(MethodParam methodParam, Map<String, Object> paramMap) {
        return securityManageMapper.findAllSecurityManageList(methodParam.getSchemaName(), paramMap);
    }

    @Override
    public Map<String, Map<String, Object>> findSafeItemsByItemCode(String schemaName, List<String> itemCodeList) {
        return securityManageMapper.findSafeItemsByItemCode(schemaName, itemCodeList);
    }
}
