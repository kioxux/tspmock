package com.gengyun.senscloud.service.job.impl;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.gengyun.senscloud.mapper.MeterMapper;
import com.gengyun.senscloud.service.job.SewageRecordsJobService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class SewageRecordsJobServiceImpl implements SewageRecordsJobService {
    private static final Logger logger = LoggerFactory.getLogger(SewageRecordsJobServiceImpl.class);
    @Resource
    MeterMapper meterMapper;
    @Resource
    SystemConfigService systemConfigService;

    @Override
    @Transactional
    public void cronJobToGenerateSewageRecords(String schemaName) {
        //记录排污记录
        List<Map<String, Object>> needHandAssetList = meterMapper.findNeedHandAssetList(schemaName);
        RegexUtil.optNotNullList(needHandAssetList).ifPresent(list -> list.forEach(e -> {
            if (RegexUtil.optIsPresentStr(e.get("sid_url")) || (RegexUtil.optIsPresentStr(e.get("grm")) && RegexUtil.optIsPresentStr(e.get("pass")))) {
                List<String> exDataList = exData(RegexUtil.optStrOrBlank(e.get("sid_url")), schemaName, e);
                if (exDataList.size() > 0 && exDataList.contains("ERROR")) {
                    if (RegexUtil.optIsPresentStr(e.get("grm")) && RegexUtil.optIsPresentStr(e.get("pass"))) {
                        String exDataUrl = this.authYunplc(schemaName, e);
                        if (RegexUtil.optIsPresentStr(exDataUrl)) {
                            e.put("sid_url", exDataUrl);
                            meterMapper.updateCusAssetSid(schemaName, e);
                            exDataList = this.exData(exDataUrl, schemaName, e);
                        }
                    }
                }
                //排污阈值
                BigDecimal sewage_value = new BigDecimal(RegexUtil.optStrOrVal(e.get("sewage_value"), "0"));
                //瞬间流量
                BigDecimal volume = new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 6), "0"));
                //设备不在排污
                if (meterMapper.findNoMeterSewageRecords(schemaName, e) == 0) {
                    //瞬间流量大于排污阈值时，此时在排污
                    if (volume.compareTo(sewage_value) > 0) {
                        meterMapper.insertMeterSewageRecords(schemaName, e);
                    }
                    //今天已经排污
                } else if (meterMapper.findHavedMeterSewageRecords(schemaName, e) > 0) {
                    //瞬间流量小于排污阈值时，此时不在排污
                    if (volume.compareTo(sewage_value) < 0) {
                        meterMapper.updateMeterSewageRecords(schemaName, e);
                    }
                }
            }
        }));
    }

    private String authYunplc(String schemaName, Map<String, Object> param) {
        return RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(schemaName, "opc_url")).map(opc_url -> {
            //opc请求地址
            String result = HttpUtil.post(opc_url.concat("exlog"), param);
            logger.info("opc授权接口请求响应结果为：{}", result);
            List<String> list = RegexUtil.optNotBlankStrOpt(result).map(e -> Arrays.asList(e.split("\r\n"))).orElse(new ArrayList<>());
            StringBuffer questUrl = new StringBuffer("http://");
            for (String item : list) {
                if (item.contains("ADDR=")) {
                    questUrl.append(item.replace("ADDR=", "")).append("/exdata?");
                } else if (item.contains("SID=") && questUrl.toString().contains("exdata")) {
                    questUrl.append(item).append("&OP=R");
                }
            }
            return questUrl.toString();
        }).orElse(null);
    }

    private List<String> exData(String url, String schemaName, Map<String, Object> handMap) {
        String param = "9\r\nEV1_Remote\r\nEV1_ON\r\nEV1_OFF\r\nPIT01\r\nFIT01_Volume\r\nFIT01_Acc_tod\r\nFIT01_Acc_yes\r\nFIT01_Acc_sum\r\nEV1_G_Fault";
        String result = null;
        try {
            result = HttpRequest.post(url)
                    .header("Content-Type", "text/plain")
                    .body(param)
                    .execute().body();
        } catch (Exception e) {
            logger.error("opc实时接口请求失败，失败原因：{}", e);
            if (RegexUtil.optIsPresentStr(handMap.get("grm")) && RegexUtil.optIsPresentStr(handMap.get("pass"))) {
                String exDataUrl = this.authYunplc(schemaName, handMap);
                if (RegexUtil.optIsPresentStr(exDataUrl)) {
                    handMap.put("sid_url", exDataUrl);
                    meterMapper.updateCusAssetSid(schemaName, handMap);
                }
            }
        }
        logger.info("opc实时接口请求响应结果为：{}", result);
        return RegexUtil.optNotBlankStrOpt(result).map(e -> Arrays.asList(e.split("\r\n"))).orElse(new ArrayList<>());
    }

    private String getValueByIndex(List<String> list, int index) {
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }
}
