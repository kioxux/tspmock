package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.AlarmAnomalySendInfoService;
import org.springframework.stereotype.Service;

/**
 * 实现接口类设备监控隐藏报警处理
 * Created by DwD on 2018/8/6.
 */

@Service
public class AlarmAnomalySendInfoServiceImpl implements AlarmAnomalySendInfoService {
//
//    @Autowired
//    AlarmAnomalySendInfoMapper alarmAnomalySendInfoMapper;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Value("${senscloud.sms.provider}")
//    String provider;
//
//    @Override
//    @Transactional//事务处理
//    public void dealAlarmAnomalySendInfo() {
//        //从记录表中遍历数据非常重要，一定要选对好什么数据需要查询出来什么数据不需要就不用查询了
//        String schema_name = "${schema_name}";
//        List<AlarmAnomalySendInfoResult> UnDealData= alarmAnomalySendInfoMapper.getAlarmDateUnDeal(schema_name);
//        Timestamp now = new Timestamp(System.currentTimeMillis());
//        List<LeadingOfficialResult> one = new ArrayList<>();//第一责任人
//        List<LeadingOfficialResult> two = new ArrayList<>();//第二责任人
//        List<LeadingOfficialResult> three = new ArrayList<>();//第三责任人
//        List<LeadingOfficialResult> four = new ArrayList<>();//第四责任人
//        long exception_time = 0;//系统配置异常短信发送时间区间
//        long alarm_sms1 = 0;//系统配置报警短信1和2次发送时间区间
//        long alarm_sms2 = 0;//系统配置报警短信2和3次发送时间区间
//        long alarm_sms3 = 0;//系统配置报警短信3和24次发送时间区间
//        long alarm_sms4 = 0;//系统配置报警短信4次以上发送时间区间
//        List<LeadingOfficialResult> scheduleList = workScheduleService.getWorkScheduleLeadingOfficial(schema_name, now, now);//查询当天的几个负责人
//        if(scheduleList.size()>0){
//            for (LeadingOfficialResult schedule : scheduleList){
//                String mobile = schedule.getMobile();
//                String charge_level = schedule.getCharge_level();//遍历等级
//                if(mobile!=null&&!mobile.equals("")){
//                    if(charge_level.equals("1")){//第一负责人
//                        one.add(schedule);
//                    }else if (charge_level.equals("2")){//第二负责人
//                        two.add(schedule);
//                    }else if (charge_level.equals("3")){//第三负责人
//                        three.add(schedule);
//                    }else if (charge_level.equals("4")){//第四负责人
//                        four.add(schedule);
//                    }
//                }
//            }
//        }
//
//        /*时间间隔时间初始化开始*/
//        List<SystemConfigData> systemConfigData = systemConfigService.getSystemConfigDataList(schema_name);//查询系统配置获取异常事件是时间
//        for(SystemConfigData configData:systemConfigData){
//            String configName = configData.getConfigName();//系统配置名称
//            if(configName.equals(Constants.EXCEPTION_SMS_INTERVAL)){//异常
//                exception_time = Long.parseLong(configData.getSettingValue());//获得系统配置时间单位毫秒
//            }else if(configName.equals(Constants.ALARM_SMS1_INTERVAL)){//设备报警时，第1次和第2次发送短信间隔（秒）
//                alarm_sms1 = Long.parseLong(configData.getSettingValue());
//            }else if(configName.equals(Constants.ALARM_SMS2_INTERVAL)){//设备报警时，第2次和第3次发送短信间隔（秒）
//                alarm_sms2 = Long.parseLong(configData.getSettingValue());
//            }else if(configName.equals(Constants.ALARM_SMS3_INTERVAL)){//设备报警时，第3次和第4次发送短信间隔（秒）
//                alarm_sms3 = Long.parseLong(configData.getSettingValue());
//            }else if(configName.equals(Constants.ALARM_SMS4_INTERVAL)){//设备报警时，第4次以后送短信间隔（秒）
//                alarm_sms4 = Long.parseLong(configData.getSettingValue());
//            }
//        }
//        /*时间间隔时间初始化完成*/
//
//        for (AlarmAnomalySendInfoResult data : UnDealData){
//            Date now_time = new Date();
//            Long timeDiff = (now_time.getTime() - data.getSend_sms_time().getTime()) / 1000;
//            if (data.getIs_send_msg() == 1) {//报警发送消息说明字段
//                if(data.getStatus()==1){//本条记录依旧处于未处理状态
//                    if(data.getSms_times()==0){//发送过一次
//                        if(timeDiff>alarm_sms1){
//                            sendMsg(one,schema_name,data,false);//发送短信并修改记录的发送信息次数
//                        }
//                    }else if(data.getSms_times()==1){//发送两次
//                        if(timeDiff>alarm_sms2){
//                            sendMsg(two,schema_name,data,false);//发送短信并修改记录的发送信息次数
//                        }
//                    }else if(data.getSms_times()==2){//发送三次
//                        if(timeDiff>alarm_sms3){
//                            sendMsg(three,schema_name,data,false);//发送短信并修改记录的发送信息次数
//                        }
//                    }else if(data.getSms_times()>=3){//发送四次以上
//                        if(timeDiff>alarm_sms4){
//                            sendMsg(four,schema_name,data,false);//发送短信并修改记录的发送信息次数
//                        }
//                    }
//                }
//            }
//            if(data.getIs_send_msg()==1&&data.getSms_times()==0){//异常发送消息说明字段
//                if(timeDiff>exception_time){
//                    sendMsg(one,schema_name,data,true);//发送短信并修改记录的发送信息次数
//                }
//            }
//        }
//    }
//
//    @Override
//    public int addAlarmAnomalySendInfo(String schema_name, AlarmAnomalySendInfoResult alarmAnomalySendInfo) {
//        return alarmAnomalySendInfoMapper.addAlarmAnomalySendInfo(schema_name, alarmAnomalySendInfo);
//    }
//
//    @Override
//    public int updateAlarmAnomalySendInfoSum(String schema_name, String alarm_code, String monitor_name) {
//        return alarmAnomalySendInfoMapper.updateAlarmAnomalySendInfoSum(schema_name, alarm_code, monitor_name);
//    }
//
//    @Override
//    public int updateStatusByAlarm_code(String schema_name, String alarm_code, int status) {
//        return alarmAnomalySendInfoMapper.updateStatusByAlarm_code(schema_name, alarm_code,  status);
//    }
//
//    @Override
//    public List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoDataList(String schema_name,String condition) {
//        return alarmAnomalySendInfoMapper.getAlarmAnomalySendInfoDataList(schema_name,condition);
//    }
//
//    @Override
//    public int getAlarmAnomalySendInfoDataListCount(String schema_name,String condition) {
//        return alarmAnomalySendInfoMapper.getAlarmAnomalySendInfoDataListCount(schema_name, condition);
//    }
//
//    @Override
//    public List<AlarmAnomalySendInfoResult> getAlarmHistoryData(String schema_name,String assetCode, String monitor_name) {
//        return alarmAnomalySendInfoMapper.getAlarmHistoryData(schema_name,assetCode,monitor_name);
//    }
//
//    @Override
//    public int getAlarmHistoryDataCount(String schema_name,String assetCode,String monitor_name) {
//        return alarmAnomalySendInfoMapper.getAlarmHistoryDataCount(schema_name,assetCode,monitor_name);
//    }
//
//    @Override
//    public List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoDataListByAlarmCode(String schema_name,String alarm_code) {
//        return alarmAnomalySendInfoMapper.getAlarmAnomalySendInfoDataListByAlarmCode(schema_name,alarm_code);
//    }
//
//
//
//    @Override
//    public int isAlarmInfo(String schema_name, String asset_code, String monitor_name) {
//        return alarmAnomalySendInfoMapper.isAlarmInfo(schema_name, asset_code, monitor_name);
//    }
//
//    @Override
//    public List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoData(String schema_name, String asset_code, String monitor_name) {
//        return alarmAnomalySendInfoMapper.getAlarmAnomalySendInfoData(schema_name, asset_code, monitor_name);
//    }
//
//    /**
//     * 发送短信并修改记录的发送信息次数
//     * @param list
//     * @param schema_name
//     * @param data
//     * @param isException
//     * @return
//     */
//    @Deprecated
//    private Boolean sendMsg(List<LeadingOfficialResult> list,String schema_name,AlarmAnomalySendInfoResult data, boolean isException){
//        Boolean result_msg = false;
//        if(list.size()>0){
////            for (int i=0;i<list.size();i++){
////                LeadingOfficialResult leadingOfficialResult = list.get(i);
////                String content;
////                if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
////                    if (isException) {
////                        String pattern = "您有一条设备异常提醒，设备名称：{0}，设备编码：{1}，请及时查看。";
////                        content = MessageFormat.format(pattern, data.getStrname() + ",监控项：" + data.getMonitor_name(), data.getAsset_code());
////                    } else {
////                        String pattern = "您有一条设备报警信息，设备名称：{0}，设备编码：{1}，请及时处理。";
////                        content = MessageFormat.format(pattern, data.getStrname() + ",监控项：" + data.getMonitor_name(), data.getAsset_code());
////                    }
////                } else {
////                    String pattern = "{0,number,#}::{1}::{2}";
////                    if (isException) {
////                        content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_EXCEPTION, data.getStrname() + ",监控项：" + data.getMonitor_name(), data.getAsset_code());
////                    } else {
////                        content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_ALARM, data.getStrname() + ",监控项：" + data.getMonitor_name(), data.getAsset_code());
////                    }
////                }
////                boolean result = Message.beginMsg(schema_name, content, leadingOfficialResult.getMobile(), leadingOfficialResult.getAccount(), "monitor", data.getAlarm_code(), "system");
////                if(result){//短信发送成功
////                    int doUpdate = alarmAnomalySendInfoMapper.updateExceptionSendInfoNum(schema_name,data.getAlarm_code());//更新发送
////                    if(doUpdate>0){
////                        result_msg = true;
////                    }
////                }else{
////                    //记录短信发送失败的异常信息
////                }
////            }
//        }
//        return  result_msg;
//    }

}
