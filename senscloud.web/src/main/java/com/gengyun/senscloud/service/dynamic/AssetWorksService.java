package com.gengyun.senscloud.service.dynamic;

import com.gengyun.senscloud.model.MethodParam;

import java.util.Map;

/**
 * 设备工单管理
 */
public interface AssetWorksService {
    /**
     * 获取按钮功能权限信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAwListPermission(MethodParam methodParam);

    /**
     * 分页获取设备工单列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 设备工单列表
     */
    Map<String, Object> getAssetWorksList(MethodParam methodParam, Map<String, Object> pm);
}
