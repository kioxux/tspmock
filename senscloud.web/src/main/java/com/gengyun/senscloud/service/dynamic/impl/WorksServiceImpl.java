package com.gengyun.senscloud.service.dynamic.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.*;
import com.gengyun.senscloud.service.asset.AssetDataService;
import com.gengyun.senscloud.service.asset.AssetPositionService;
import com.gengyun.senscloud.service.bom.BomService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.dynamic.DynamicCommonService;
import com.gengyun.senscloud.service.dynamic.WorkProcessService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.out.OutService;
import com.gengyun.senscloud.service.system.*;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.engine.FormService;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.gengyun.senscloud.common.ErrorConstant.EC_WORK_006;

/**
 * 工单管理
 */
@Service
public class WorksServiceImpl implements WorksService {
    @Resource
    AssetMapper assetMapper;
    @Resource
    WorksMapper worksMapper;
    @Resource
    AssetWorksMapper assetWorksMapper;
    @Resource
    FormService formService;
    @Resource
    AssetPositionService assetPositionService;
    @Resource
    TenantInfoHolder tenantInfoHolder;
    @Resource
    DynamicCommonService dynamicCommonService;
    @Resource
    WorkflowService workflowService;
    @Resource
    LogsService logService;
    @Resource
    BussinessConfigService bussinessConfigService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    MessageService messageService;
    @Resource
    WorkProcessService workProcessService;
    @Resource
    UserService userService;
    @Resource
    WorkRecordHourService workRecordHourService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    AssetDataService assetDataService;
    @Resource
    WorkFlowTemplateService workFlowTemplateService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    BomWorksMapper bomWorksMapper;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    BomService bomService;
    @Resource
    OutService outService;
    @Resource
    WorkColumnMapper workColumnMapper;
    @Resource
    PagePermissionMapper pagePermissionMapper;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    CustomersService customersService;

    /**
     * 流程回调
     *
     * @param methodParam 入参
     * @param taskEntity  流程节点实例
     * @param isEnd       是否是结束节点
     */
    @Override
    public void callBackStartWithForm(MethodParam methodParam, TaskEntity taskEntity, Map<String, String> endParams, boolean isEnd, FlowableEngineEventType flowableEngineEventType) {
        endParams = RegexUtil.optMapStringOrNew(endParams);
        Integer nextStatus;
        // 获取当前时间
        Timestamp now = new Timestamp(System.currentTimeMillis());
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> nodeInfo;
        Map<String, Object> works_detail_tmp = null;
        int do_flow_key = ButtonConstant.BTN_SUBMIT;
        String subWorkCode = RegexUtil.optStrOrNull(endParams.get(FlowParmsConstant.SUB_WORK_CODE));
        String workCode = RegexUtil.optStrOrNull(endParams.get(FlowParmsConstant.WORK_CODE));
        int businessTypeId;
        if (RegexUtil.optNotNull(taskEntity).isPresent()) {
            businessTypeId = RegexUtil.optIntegerOrExp(taskEntity.getVariable(FlowParmsConstant.BUSINESS_TYPE_ID), methodParam, ErrorConstant.EC_WORK_001, LangConstant.LOG_B);
            String business_type_id = String.valueOf(businessTypeId);
            String work_type_id = RegexUtil.optStrOrErrorExp(taskEntity.getVariable(FlowParmsConstant.WORK_TYPE_ID), methodParam, ErrorConstant.EC_WORK_002, LangConstant.LOG_B);
            String pref_id = RegexUtil.optStrOrErrorExp(taskEntity.getVariable(FlowParmsConstant.PREF_ID), methodParam, ErrorConstant.EC_WORK_003, LangConstant.LOG_B);
            taskEntity.setVariable(FlowParmsConstant.FLOW_NODE_SHOW_NAME, taskEntity.getName());
            String receive_user_id = RegexUtil.optStrOrNull(taskEntity.getVariable(FlowParmsConstant.RECEIVE_USER_ID));
            String next_user_id = RegexUtil.optStrOrNull(taskEntity.getVariable(FlowParmsConstant.NEXT_USER_ID));
            String main_sub_work_code = RegexUtil.optStrOrNull(taskEntity.getVariable(FlowParmsConstant.MAIN_SUB_WORK_CODE));
            String sub_work_code = RegexUtil.optStrAndValOrNull(taskEntity.getVariableLocal(FlowParmsConstant.SUB_WORK_CODE), taskEntity.getVariable(FlowParmsConstant.SUB_WORK_CODE));
            subWorkCode = sub_work_code;
            String nodeId = RegexUtil.optStrOrVal(RegexUtil.optNotBlankStrOpt(main_sub_work_code).map(mc -> {
                Map<String, Object> map = new HashMap<>();
                map.put(FlowParmsConstant.SUB_WORK_CODE, mc);
                map.put(FlowParmsConstant.PDEID, pref_id);
                return workflowService.getNodeIdBySubWorkCode(methodParam, map);//获取流程节点ID
            }).orElse(null), taskEntity.getTaskDefinitionKey());
            taskEntity.setVariableLocal("node_id", nodeId); // yx
            // 工作流推动到下一节点后  获取下一节点的工单流程节点信息
            // 根据当前taskid获取当前节点流程id进而获取当前流程所有信息
            nodeInfo = this.getWorkFlowTemplatesInfoByTaskId(methodParam, nodeId, pref_id);
            nodeInfo.put("sub_work_code", sub_work_code);
            nodeInfo.put("business_type_id", business_type_id);
            // 下一步状态
            nextStatus = RegexUtil.optIntegerOrExp(nodeInfo.get("status"), methodParam, ErrorConstant.EC_WORKFLOW_506, LangConstant.LOG_B);
            Map<String, String> map = null;
            // 流程未结束
            if (!isEnd) {
                map = new HashMap<>();
                map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
            }
            nodeInfo.put("work_type_id", work_type_id);
            do_flow_key = RegexUtil.optIntegerAndValOrNull(taskEntity.getVariableLocal(FlowParmsConstant.DO_FLOW_KEY), taskEntity.getVariable(FlowParmsConstant.DO_FLOW_KEY), methodParam, ErrorConstant.EC_WORK_004, LangConstant.MSG_BI);
            if (Integer.valueOf(SensConstant.BUSINESS_NO_2000) > businessTypeId) {
                works_detail_tmp = RegexUtil.optMapOrNew(worksMapper.findWorksDetailByCode(schemaName, sub_work_code));
                String work_code = RegexUtil.optStrOrNull(works_detail_tmp.get("work_code"));
                Map<String, Object> works_tmp = RegexUtil.optMapOrNew(worksMapper.findWorksByCode(schemaName, work_code));
                RegexUtil.optNotBlankStrOpt(works_detail_tmp.get("position_code")).ifPresent(pc -> nodeInfo.put("position_code", pc));
                // 流程已结束、节点创建
                if (isEnd || FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType)) {
                    // 下一步状态
                    if (StatusConstant.TO_BE_AUDITED == nextStatus) {
                        works_detail_tmp.put("audit_time", now);
                    } else if (StatusConstant.COMPLETED == nextStatus) {
                        reduceWorkBom(methodParam, sub_work_code); // 如果使用备件  进行备件减库存
                    }
                    Integer nowStatus = RegexUtil.optIntegerOrErr(works_detail_tmp.get("status"), methodParam, EC_WORK_006);
                    if (StatusConstant.PENDING == nowStatus && StatusConstant.PENDING != nextStatus) {
                        works_detail_tmp.put("finished_time", now);
                    }
                    if (isEnd) {
                        works_detail_tmp.put("close_time", now); // 关单时间
                    } else if (FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType)) {
                        // 退回操作  返回给操作人 按节点号取操作人
                        if (ButtonConstant.BTN_RETURN == do_flow_key) {
                            next_user_id = workProcessService.getOperateUserIdByNodeId(schemaName, sub_work_code, nodeId);
                        } else {
                            String nid = RegexUtil.optStrOrBlank(nodeInfo.get(Constants.WORK_NODE_USER));
                            // 随机选择下一步操作人
                            if ("".equals(nid)) {
                                nodeInfo.put("relation_id", works_detail_tmp.get("relation_id"));
                                nodeInfo.put("relation_type", works_detail_tmp.get("relation_type"));
                                if (StatusConstant.PENDING == nextStatus && StatusConstant.PENDING != nowStatus && !RegexUtil.optIsPresentStr(next_user_id)) {
                                    next_user_id = receive_user_id;
                                } else if (StatusConstant.PENDING == nextStatus && StatusConstant.PENDING == nowStatus && !RegexUtil.optIsPresentStr(next_user_id)) {
                                    next_user_id = RegexUtil.optStrOrNull(works_detail_tmp.get("receive_user_id"));
                                }
                                next_user_id = RegexUtil.optNotBlankStrOpt(nodeInfo.get(Constants.WORK_AUTO_USER)).map(au -> this.getRandomUserId(methodParam, nodeInfo)).orElse(next_user_id);
                            } else {
                                next_user_id = workProcessService.getOperateUserIdByNodeId(schemaName, sub_work_code, nid);
                            }
                            if (StatusConstant.TO_BE_AUDITED == nextStatus && StatusConstant.PENDING == nowStatus) {
                                RegexUtil.optNotBlankStrOpt(works_detail_tmp.get("receive_user_id")).ifPresent(ru -> {
                                    workRecordHourService.updateWorkRecordHour(schemaName, sub_work_code, ru, now); // 更新工单工时记录
                                    workRecordHourService.updateWorkTotalHour(schemaName, sub_work_code); // 更新工单总工时
                                });
                            }
                        }
                        next_user_id = RegexUtil.optStrOrBlank(next_user_id);
                        workflowService.assignWithSWC(methodParam, sub_work_code, next_user_id, map);
                        if (StatusConstant.PENDING == nextStatus && (StatusConstant.PENDING != nowStatus || !next_user_id.equals(RegexUtil.optStrOrBlank(works_detail_tmp.get("receive_user_id"))))) {
                            works_detail_tmp.put("distribute_user_id", methodParam.getUserId());
                            works_detail_tmp.put("distribute_time", now);
                            works_detail_tmp.put("receive_user_id", next_user_id);
                            works_detail_tmp.put("receive_time", now);
                        } else {
                            works_detail_tmp.remove("receive_user_id");
                        }
                        works_detail_tmp.put("duty_user_id", next_user_id);
                        taskEntity.setAssigneeValue(next_user_id); // 更新流程代办人
                    }
                    works_detail_tmp.put("status", nextStatus);
                    works_tmp.put("status", nextStatus);
                    worksMapper.updateWorks(schemaName, works_tmp);
                    worksMapper.updateWorksDetailByCode(schemaName, works_detail_tmp);
                    outService.outServerAaaAaa(methodParam, subWorkCode, nodeId, works_tmp.get("occur_time"), works_detail_tmp.get("relation_id"), works_detail_tmp.get("receive_user_id"));
                }
                nodeInfo.put("_sc_works", works_tmp);
                nodeInfo.put("_sc_works_detail", works_detail_tmp);
                // 更新设备工单组织信息
                if (nextStatus > StatusConstant.TO_REPORT && nextStatus != StatusConstant.CANCEL) {
                    if (2 == RegexUtil.optIntegerOrVal(works_detail_tmp.get("relation_type"), 0, methodParam, ErrorConstant.EC_WORK_007)) {
                        updateWorksOrgAndGroup(methodParam, subWorkCode, RegexUtil.optStrOrNull(works_detail_tmp.get("relation_id")), methodParam.getUserId());
                    }
                }
                treateAssetAdjust(methodParam, sub_work_code, businessTypeId);
                // 设备流程
            } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3000) > businessTypeId) {
                works_detail_tmp = RegexUtil.optMapOrNew(assetWorksMapper.findAssetWorksDetailInfoByCode(schemaName, sub_work_code));
                Integer isMain = RegexUtil.optIntegerOrErr(works_detail_tmp.get("is_main"), methodParam, ErrorConstant.EC_WORK_008);
                String work_code = RegexUtil.optStrOrNull(works_detail_tmp.get("work_code"));
                Map<String, Object> works_tmp = RegexUtil.optMapOrNew(assetWorksMapper.findAssetWorksInfoByCode(schemaName, work_code));
                works_tmp.put("status", nextStatus);
                works_detail_tmp.put("status", nextStatus);

                if (-1 == isMain && !FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType)) {
                    // 如果最后一个子流程为同意  那么主流程也更新状态为已完成
                    if (assetWorksMapper.findNoEndSubAssetWorksDetailByWorkCode(schemaName, work_code) == 0) {
                        // 如果所有的主工单都已完成则更新主工单状态为已完成
                        Map<String, Object> main_works_detail_tmp = assetWorksMapper.findAssetWorksDetailInfoByCode(schemaName, main_sub_work_code);
                        Integer mainStatus = nextStatus == StatusConstant.PENDING ? StatusConstant.COMPLETED : nextStatus;
                        main_works_detail_tmp.put("status", mainStatus);
                        assetWorksMapper.updateAssetWorksDetailBySubWorkCode(schemaName, main_works_detail_tmp);
                        works_tmp.put("status", mainStatus);
                    } else {
                        works_tmp.remove("status");
                    }
                    nextStatus = StatusConstant.COMPLETED;
                    works_detail_tmp.put("status", nextStatus);
                }

                if (!isEnd && FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType)) {
                    // 审核退回操作  返回给操作人 按节点号取操作人
                    if (do_flow_key == ButtonConstant.BTN_RETURN) {
                        next_user_id = workProcessService.getOperateUserIdByNodeId(schemaName, sub_work_code, nodeId);
                    } else {
                        String nid = RegexUtil.optStrOrBlank(nodeInfo.get(Constants.WORK_NODE_USER));
                        // 随机选择下一步操作人
                        if ("".equals(nid)) {
                            nodeInfo.put("category_id", works_detail_tmp.get("asset_category_id"));
                            next_user_id = RegexUtil.optNotBlankStrOpt(nodeInfo.get(Constants.WORK_AUTO_USER)).map(au -> {
                                nodeInfo.put("position_code", works_tmp.get("position_code"));
                                nodeInfo.put("work_type_id", works_tmp.get("work_type_id"));
                                return this.getRandomUserId(methodParam, nodeInfo);
                            }).orElse(RegexUtil.optStrOrVal(next_user_id, receive_user_id));
                        } else {
                            next_user_id = workProcessService.getOperateUserIdByNodeId(schemaName, sub_work_code, nid);
                        }
                    }
                    next_user_id = RegexUtil.optStrOrBlank(next_user_id);
                    workflowService.assignWithSWC(methodParam, sub_work_code, next_user_id, map);
                    taskEntity.setAssignee(next_user_id);
                    works_detail_tmp.put("duty_user_id", next_user_id);
                }

                // 主子工单更新设备工单详情表
//                    if (2 == selectUserType && StatusConstant.COMPLETED != nowStatus) {
//                        works_detail_tmp.put("status", nextStatus);
//                    }
                assetWorksMapper.updateAssetWorksByWorkCode(schemaName, works_tmp); // 更新设备工单概要表
                assetWorksMapper.updateAssetWorksDetailBySubWorkCode(schemaName, works_detail_tmp); // 更新设备工单详情表
                // 设备验收
                if (Integer.valueOf(SensConstant.BUSINESS_NO_2001) == businessTypeId) {
                    String ot = RegexUtil.optStrOrBlank(taskEntity.getVariable(FlowParmsConstant.OPERATION_TYPE));
                    if ("1".equals(ot)) {
                        addAsset(methodParam, works_detail_tmp);
                    } else if ("2".equals(ot)) {
                        updateAsset(methodParam, works_detail_tmp);
                    } else if ("3".equals(ot) || "4".equals(ot)) {
                        String f = RegexUtil.optStrOrBlank(taskEntity.getVariable(FlowParmsConstant.ATTACHFILE));
                        if (!"".equals(f)) {
                            works_detail_tmp.put(FlowParmsConstant.ATTACHFILE, f);
                            updateAssetFile(methodParam, works_detail_tmp);
                        }
                    }
                    // 设备调拨
                } else if (Integer.valueOf(SensConstant.BUSINESS_NO_2002) == businessTypeId) {
                    if (StatusConstant.COMPLETED == nextStatus) {
                        // 设备出库
                        if (Integer.valueOf(SensConstant.BUSINESS_NO_2005) == businessTypeId || Integer.valueOf(SensConstant.BUSINESS_NO_2006) == businessTypeId) {
                            List<Map<String, Object>> sub_work_detail_item_list = assetWorksMapper.findSubAssetWorksDetailItemListByWorkCode(schemaName, work_code);
                            for (Map<String, Object> sub_asset_work_detail_item : sub_work_detail_item_list) {
                                Map<String, Object> assetMap = new HashMap<>();
                                assetMap.put("id", sub_asset_work_detail_item.get("asset_id"));
                                assetMap.put("status", SensConstant.BUSINESS_NO_2005.equalsIgnoreCase(business_type_id) ? StatusConstant.ASSET_INTSTATUS_OUT : StatusConstant.ASSET_INTSTATUS_USING); // 借出状态、在用状态
                                assetMap.put("schemaName", schemaName);
                                assetMap.put("sectionType", "assetStatusOnly");
                                assetMapper.updateAsset(assetMap);
                            }
                        }
                        works_detail_tmp.put("new_position_code", RegexUtil.optStrAndValOrBlank(taskEntity.getVariableLocal(FlowParmsConstant.NEW_POSITION_CODE), taskEntity.getVariable(FlowParmsConstant.NEW_POSITION_CODE)));
                        works_detail_tmp.put("business_type_Id", business_type_id);
                        updateAssetPosition(methodParam, works_detail_tmp);
                    }
                }
                // 备件流程
            } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3100) > businessTypeId) {
                works_detail_tmp = RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksDetailInfoByCode(schemaName, sub_work_code));
                Integer isMain = RegexUtil.optIntegerOrErr(works_detail_tmp.get("is_main"), methodParam, ErrorConstant.EC_WORK_008);
                String work_code = RegexUtil.optStrOrNull(works_detail_tmp.get("work_code"));
                Map<String, Object> works_tmp = RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksInfoByCode(schemaName, work_code));
                if (Integer.valueOf(SensConstant.BUSINESS_NO_3002) == businessTypeId) { //备件调拨库存位置等相关变化
                    Integer status = NumberUtils.toInt(RegexUtil.optStrOrNull(works_detail_tmp.get("status")));
                    if (status == StatusConstant.TO_BE_CONFIRM && nextStatus == StatusConstant.TO_BE_AUDITED) { // 15->50
                        handleBomStock(methodParam, works_tmp, works_detail_tmp, status, nextStatus); // 备件调拨，对备件库存位置以及数量的处理
                    } else if (status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.COMPLETED) { // 50->60
                        handleBomStock(methodParam, works_tmp, works_detail_tmp, status, nextStatus);
                    } else if (status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.COMPLETED_CLOSE
                            || status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.TO_BE_CONFIRM) { // 50-> 110,50->15
                        handleBomStock(methodParam, works_tmp, works_detail_tmp, status, nextStatus);
                    }

                }
                works_tmp.put("status", nextStatus);
                works_detail_tmp.put("status", nextStatus);
                if (-1 == isMain && !FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType)) {
                    Integer inv_count = bomWorksMapper.findBomWorksDetailItemQuantityBySubWorkCode(schemaName, sub_work_code);
                    List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), main_sub_work_code);
                    for (Map<String, Object> tmp : hadlList) {
                        String field_code = tmp.get("field_code").toString();
                        String field_value = null;
                        if ("inventory_table".equals(field_code)) {
                            Map<String, Object> inv_count_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "inv_count");
                            Map<String, Object> sub_work_code_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "sub_work_code");
                            Map<String, Object> status_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "status");
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(tmp.get("field_value"));
                            list.forEach(e -> {
                                if (e.get(RegexUtil.optStrOrBlank(sub_work_code_map.get("field_form_code"))).equals(sub_work_code)) {
                                    e.put(RegexUtil.optStrOrBlank(inv_count_map.get("field_form_code")), inv_count);
                                    e.put(RegexUtil.optStrOrBlank(status_map.get("field_form_code")), StatusConstant.COMPLETED);
                                }
                            });
                            field_value = list.toString();
                            tmp.put("sub_work_code", main_sub_work_code);
                            tmp.put("field_value", field_value);
                            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                            break;
                        }
                    }
                    // 如果最后一个子流程为同意  那么主流程也更新状态为已完成
                    if (bomWorksMapper.findNoEndSubBomWorksDetailByWorkCode(schemaName, work_code) == 0) {
                        // 如果所有的主工单都已完成则更新主工单状态为已完成
                        Map<String, Object> main_works_detail_tmp = bomWorksMapper.findBomWorksDetailInfoByCode(schemaName, main_sub_work_code);
                        Integer mainStatus = nextStatus == StatusConstant.PENDING ? StatusConstant.COMPLETED : nextStatus;
                        main_works_detail_tmp.put("status", mainStatus);
                        bomWorksMapper.updateBomWorksDetailBySubWorkCode(schemaName, main_works_detail_tmp);
                        works_tmp.put("status", mainStatus);
                    } else {
                        works_tmp.remove("status");
                    }
                    nextStatus = StatusConstant.COMPLETED;
                    works_detail_tmp.put("status", nextStatus);
                }

                if (!isEnd && FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType)) {
                    works_detail_tmp = RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksDetailInfoByCode(schemaName, sub_work_code));
                    // 审核退回操作  返回给操作人 按节点号取操作人
                    if (do_flow_key == ButtonConstant.BTN_RETURN) {
                        next_user_id = workProcessService.getOperateUserIdByNodeId(schemaName, sub_work_code, nodeId);
                    } else {
                        String nid = RegexUtil.optStrOrBlank(nodeInfo.get(Constants.WORK_NODE_USER));
                        // 随机选择下一步操作人
                        if ("".equals(nid)) {
                            work_code = RegexUtil.optStrOrNull(works_detail_tmp.get("work_code"));
                            if (Integer.valueOf(SensConstant.BUSINESS_NO_3005) != businessTypeId && Integer.valueOf(SensConstant.BUSINESS_NO_3002) != businessTypeId) {
                                works_tmp = RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksInfoByCode(schemaName, work_code));
                                nodeInfo.put("stock_id", RegexUtil.optIntegerOrErr(works_tmp.get("stock_id"), methodParam, ErrorConstant.EC_WORK_009));
                            }
                            if (Integer.valueOf(SensConstant.BUSINESS_NO_3002) == businessTypeId) {
                                works_tmp = RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksInfoByCode(schemaName, work_code));
                                nodeInfo.put("stock_id", RegexUtil.optIntegerOrErr(works_tmp.get("to_stock_id"), methodParam, ErrorConstant.EC_WORK_009));
                            }
                            next_user_id = RegexUtil.optNotBlankStrOpt(nodeInfo.get(Constants.WORK_AUTO_USER)).map(au -> this.getRandomUserId(methodParam, nodeInfo)).orElse(RegexUtil.optStrOrVal(next_user_id, receive_user_id));
                        } else {
                            next_user_id = workProcessService.getOperateUserIdByNodeId(schemaName, sub_work_code, nid);
                        }
                    }
                    next_user_id = RegexUtil.optStrOrBlank(next_user_id);
                    workflowService.assignWithSWC(methodParam, sub_work_code, next_user_id, map);
                    taskEntity.setAssignee(next_user_id);
                    works_detail_tmp.put("duty_user_id", next_user_id);
                }
            }
            taskEntity.setVariableLocal(FlowParmsConstant.STATUS, nextStatus);
            taskEntity.setVariable(FlowParmsConstant.STATUS, nextStatus);
        } else {
            businessTypeId = RegexUtil.optIntegerOrExp(endParams.get(FlowParmsConstant.BUSINESS_TYPE_ID), methodParam, ErrorConstant.EC_WORK_001, LangConstant.LOG_B);
            nodeInfo = this.getWorkFlowTemplatesInfoByTaskId(methodParam, endParams.get("node_id"), endParams.get(FlowParmsConstant.PREF_ID));
            // 下一步状态
            nextStatus = RegexUtil.optIntegerOrExp(nodeInfo.get("status"), methodParam, ErrorConstant.EC_WORKFLOW_506, LangConstant.LOG_B);
            nodeInfo.put("sub_work_code", subWorkCode);
            nodeInfo.put("business_type_id", String.valueOf(businessTypeId));

            if (Integer.valueOf(SensConstant.BUSINESS_NO_2000) > businessTypeId) {
                works_detail_tmp = RegexUtil.optMapOrNew(worksMapper.findWorksDetailByCode(schemaName, subWorkCode));
                String work_code = RegexUtil.optStrOrNull(works_detail_tmp.get("work_code"));
                Map<String, Object> works_tmp = RegexUtil.optMapOrNew(worksMapper.findWorksByCode(schemaName, work_code));
                RegexUtil.optNotBlankStrOpt(works_detail_tmp.get("position_code")).ifPresent(c -> nodeInfo.put("position_code", c));
                works_detail_tmp.put("status", nextStatus);
                works_tmp.put("status", nextStatus);
                worksMapper.updateWorks(schemaName, works_tmp);
                worksMapper.updateWorksDetailByCode(schemaName, works_detail_tmp);
                nodeInfo.put("_sc_works", works_tmp);
                nodeInfo.put("_sc_works_detail", works_detail_tmp);
                // 更新设备工单组织信息
                if (nextStatus > StatusConstant.TO_REPORT && nextStatus != StatusConstant.CANCEL) {
                    if (2 == RegexUtil.optIntegerOrVal(works_detail_tmp.get("relation_type"), 0, methodParam, ErrorConstant.EC_WORK_007)) {
                        updateWorksOrgAndGroup(methodParam, subWorkCode, RegexUtil.optStrOrNull(works_detail_tmp.get("relation_id")), methodParam.getUserId());
                    }
                }
            }
        }

        if (Integer.valueOf(SensConstant.BUSINESS_NO_3100) > businessTypeId && Integer.valueOf(SensConstant.BUSINESS_NO_3000) < businessTypeId) {
            works_detail_tmp = RegexUtil.optNotNullMap(works_detail_tmp).orElse(RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksDetailInfoByCode(schemaName, subWorkCode)));
            String work_code = RegexUtil.optStrOrNull(works_detail_tmp.get("work_code"));
            Map<String, Object> works_tmp = RegexUtil.optMapOrNew(bomWorksMapper.findBomWorksInfoByCode(schemaName, work_code));
            if (businessTypeId != Integer.valueOf(SensConstant.BUSINESS_NO_3005)) {
                if (businessTypeId == Integer.valueOf(SensConstant.BUSINESS_NO_3002)) {
                    nodeInfo.put("stock_id", RegexUtil.optIntegerOrErr(works_tmp.get("to_stock_id"), methodParam, ErrorConstant.EC_WORK_009));
                } else {
                    nodeInfo.put("stock_id", RegexUtil.optIntegerOrErr(works_tmp.get("stock_id"), methodParam, ErrorConstant.EC_WORK_009));
                }
                works_tmp.put("status", nextStatus);
                works_detail_tmp.put("status", nextStatus);
                bomWorksMapper.updateBomWorksDetailBySubWorkCode(schemaName, works_detail_tmp);
                bomWorksMapper.updateBomWorksByWorkCode(schemaName, works_tmp);
            } else {
                // 如果最后一个子流程为同意  那么主流程也更新状态为已完成
                if (bomWorksMapper.findNoEndSubBomWorksDetailByWorkCode(schemaName, work_code) == 0) {
                    // 如果所有的主工单都已完成则更新主工单状态为已完成
                    works_tmp.put("status", nextStatus);
                    works_detail_tmp.put("status", nextStatus);
                    bomWorksMapper.updateBomWorksDetailBySubWorkCode(schemaName, works_detail_tmp);
                    bomWorksMapper.updateBomWorksByWorkCode(schemaName, works_tmp);
                } else {
                    works_tmp.remove("status");
                }
            }

            if (StatusConstant.COMPLETED == nextStatus) {
                updateBomStock(methodParam, works_tmp, works_detail_tmp); // 更新备件库存数量
                // 将备件采购需求状态更新为已入库
                RegexUtil.optNotBlankStrOpt(endParams.get(FlowParmsConstant.BOM_NEED_WORK_CODE)).ifPresent(c -> {
                    bomWorksMapper.updateBomWorksStatus(schemaName, c, StatusConstant.WAREHOUSED);
                    bomWorksMapper.updateBomWorksDetailStatus(schemaName, c, StatusConstant.WAREHOUSED);
                });
            }
        }

        if (Integer.valueOf(SensConstant.BUSINESS_NO_2003) == businessTypeId) {
            works_detail_tmp = RegexUtil.optMapOrNew(assetWorksMapper.findAssetWorksDetailInfoByCode(schemaName, subWorkCode));
            works_detail_tmp.put("status", nextStatus);
            if (StatusConstant.COMPLETED == nextStatus) {
                // 将设备报废状态更新为已完成，同时更新设备资产状态为报废
                assetWorksMapper.updateAssetWorksStatus(schemaName, workCode, StatusConstant.COMPLETED);
                assetWorksMapper.updateAssetWorksDetailStatus(schemaName, subWorkCode, StatusConstant.COMPLETED);
                works_detail_tmp.put("new_status", 3);
                works_detail_tmp.put("business_type_Id", businessTypeId);
                updateAssetPosition(methodParam, works_detail_tmp);
            }
        }

        Map<String, Object> oldStatusMap = worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(schemaName, subWorkCode, "status");
        if (RegexUtil.optNotNull(oldStatusMap).isPresent()) {
            oldStatusMap.put("field_value", nextStatus);
            worksMapper.updateWorksDetailColumnByCode(schemaName, oldStatusMap);
        }
        // 发送短信
        if (FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType) && RegexUtil.optIsPresentStr(nodeInfo.get("msg_code"))) {
            if (RegexUtil.optIsPresentStr(nodeInfo.get("cc_msg_code"))) {
                methodParam.setNeedCC(true);
            }
            sendMessage(methodParam, RegexUtil.optMapOrNew(nodeInfo), do_flow_key);
        }
        if (FlowableEngineEventType.TASK_CREATED.equals(flowableEngineEventType) && RegexUtil.optIsPresentStr(nodeInfo.get("cc_msg_code"))) {
            //是否有需要抄送人员，有侧发送短信
            List<String> ccUsersList = RegexUtil.optNotNullListStrOrNew(worksMapper.findWorksCCUsersByCode(methodParam.getSchemaName(),subWorkCode));
            if (!ccUsersList.isEmpty()) {
                RegexUtil.optMapOrNew(nodeInfo).put("ccUserList", ccUsersList);
                sendCCMessage(methodParam, RegexUtil.optMapOrNew(nodeInfo), do_flow_key);
            }
        }
    }

    /**
     * 处理设备调整元素,设备状态调整，表具上线下线时间放在这里显示
     *
     * @param methodParam
     * @param sub_work_code
     */
    private void treateAssetAdjust(MethodParam methodParam, String sub_work_code, int businessTypeId) {
        //目前为测试代码，业务类型需要调整为检定及拆换的业务类型 20211224
        //if (SensConstant.BUSINESS_NO_1100.equals(String.valueOf(businessTypeId))) {
        if(true){
            Map<String,Object> columns =  getWorksDetailColumnKeyValue(methodParam,sub_work_code);
            if(RegexUtil.optIsPresentStr(columns.get("relation_type")) &&
                    columns.get("relation_type").equals("2")){
                if(RegexUtil.optIsPresentStr(columns.get("asset_adjust_status"))){
                    updateAssetStatus(methodParam, String.valueOf(columns.get("relation_id")),String.valueOf(columns.get("asset_adjust_status")));
                }
            }

        }
    }

    /**
     * 调整设备状态
     * @param assetId
     * @param adjustAssetStatus
     */
    private void updateAssetStatus(MethodParam methodParam,String assetId, String adjustAssetStatus) {
        Map<String,Object> asset =  RegexUtil.optMapOrNew(assetDataService.getAssetInfo(methodParam,assetId));
        Map<String,Object> newAssetMap = new HashMap<>();
        newAssetMap.putAll(asset);
        newAssetMap.put("running_status_id",adjustAssetStatus);
        newAssetMap.put("sectionType","assetTop");
        AssetSearchParam assetSearchParam = new AssetSearchParam();
        assetSearchParam.setId(assetId);
        methodParam.setDataId(assetId);
        assetDataService.doEditAsset(methodParam,assetSearchParam,newAssetMap);
    }

    /**
     * 有子工单的主工单处理接口
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public List<Map<String, Object>> haveSubWorksDeal(MethodParam methodParam, Map<String, Object> paramMap) {
        Integer work_type_id = RegexUtil.optIntegerOrExpParam(paramMap.get("work_type_id"), LangConstant.MSG_G);
        String sub_work_code = RegexUtil.optStrOrExpNotNull(paramMap.get("sub_work_code"), LangConstant.MSG_G);
        //根据工单类型id获取业务类型
        Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id);
        String business_type_id = work_type.get("business_type_id").toString();
        Map<String, Object> mainWorkDetail = null;
        String work_code = null;
        List<Map<String, Object>> subWorkDetailList = null;
        if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1002.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1003.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1004.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1005.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1006.equals(business_type_id)) {
            mainWorkDetail = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
        } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)
                || SensConstant.BUSINESS_NO_2004.equals(business_type_id)
                || SensConstant.BUSINESS_NO_2005.equals(business_type_id)) {
            mainWorkDetail = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
            work_code = RegexUtil.optStrOrNull(mainWorkDetail.get("work_code"));
            if (SensConstant.BUSINESS_NO_2004.equals(business_type_id)) {
                subWorkDetailList = assetWorksMapper.findSubAssetWorksDetailListByWorkCodeUserIdNew(methodParam.getSchemaName(), work_code, methodParam.getUserId());
            } else {
                subWorkDetailList = assetWorksMapper.findSubAssetWorksDetailListByWorkCodeUserId(methodParam.getSchemaName(), work_code, methodParam.getUserId());
            }
        } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)) {
            mainWorkDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
        } else if (SensConstant.BUSINESS_NO_3005.equals(business_type_id)) {
            mainWorkDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
            work_code = RegexUtil.optStrOrNull(mainWorkDetail.get("work_code"));
            subWorkDetailList = bomWorksMapper.findSubBomWorksListByWorkCodeUserId(methodParam.getSchemaName(), work_code, methodParam.getUserId());
        } else if (SensConstant.BUSINESS_NO_3007.equals(business_type_id)) {
            mainWorkDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在

        } else if (SensConstant.BUSINESS_NO_3008.equals(business_type_id)) {
            mainWorkDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
        } else {
            mainWorkDetail = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code);
            RegexUtil.optNotNullOrExp(mainWorkDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
        }
        if (subWorkDetailList.size() > 0) {
            for (Map<String, Object> subWorkDetail : subWorkDetailList) {
                //如果该子工单的处理人是自己  显示处理按钮
                if (RegexUtil.optIsPresentStr(subWorkDetail.get("duty_user_id"))
                        && methodParam.getUserId().equals(subWorkDetail.get("duty_user_id").toString())) {
                    subWorkDetail.put("isEdit", true);
                }
            }
        }
        return subWorkDetailList;
    }

    /**
     * 工单回收
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public void worksRecovery(MethodParam methodParam, Map<String, Object> paramMap) {
        String schema_name = methodParam.getSchemaName();
        String sub_work_code = RegexUtil.optStrOrExpNotNull(paramMap.get("sub_work_code"), LangConstant.TITLE_MW);//子工单不能为空
        Map<String, Object> worksDetailInfo = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code); // 明细数据
        String workCode = (String) worksDetailInfo.get("work_code");
        RegexUtil.trueExp(worksMapper.findCountWorkDetailBySwcStatus(schema_name, sub_work_code) == 0, LangConstant.TEXT_I, new String[]{LangConstant.TITLE_AG_Q}); // 不允许回收
        worksMapper.updateWorksDetailReceiveUserAndStatusBySwc(schema_name, "", StatusConstant.UNDISTRIBUTED, sub_work_code);//工单详情表修改分配人员及状态
        int count = worksMapper.findCountUndistributedWorkByCode(schema_name, workCode, sub_work_code);
        if (0 == count) {
            worksMapper.updateWorkStatusByCode(schema_name, StatusConstant.UNDISTRIBUTED, workCode);//工单表修改状态
        }
        Map<String, String> map = new HashMap<>();
        map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
        workflowService.assignWithSWC(methodParam, sub_work_code, "", map);
        Map<String, Object> work_type = RegexUtil.optMapOrNew(bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", RegexUtil.optIntegerOrExpParam(worksDetailInfo.get("work_type_id"), LangConstant.MSG_A)));
        logService.newLog(methodParam, RegexUtil.optStrOrBlank(work_type.get("business_type_id")) + SensConstant.BUSINESS_NO_PROCESS, sub_work_code, LangUtil.doSetLogArray("流转记录", LangConstant.TEXT_AQ, new String[]{LangConstant.TITLE_AC_Z, LangConstant.TITLE_AG_Q})); // 工单调度-回收
    }

    /**
     * 获取按钮功能权限信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getWorksListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[14]);
    }

    /**
     * 查询工单详情
     *
     * @param methodParam 入参
     * @param worksModel  入参
     * @return 工单详情
     */
    @Override
    public Map<String, Object> getWorkDetailByCode(MethodParam methodParam, WorksModel worksModel) {
        return worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), worksModel.getSub_work_code());
    }

    /**
     * 开启工作流
     *
     * @param methodParam 入参
     * @param worksModel  入参
     * @return 开启工作流
     */
    @Override
    public Map<String, Object> start(MethodParam methodParam, WorksModel worksModel) {
        String formKey = formService.getStartFormKey(worksModel.getProcessDefinitionId());
        Map<String, Object> result = new HashMap<>();
        result.put("formKey", formKey);
        tenantInfoHolder.clearCurrentTenantId();
        return result;
    }

    /**
     * 检查验证数据
     *
     * @param methodParam   入参
     * @param sub_work_code 入参
     */
    @Override
    public void doCheckFlowInfo(MethodParam methodParam, String sub_work_code) {
        RegexUtil.optStrOrExpNotSelect(sub_work_code, LangConstant.TITLE_AAT_P);
        WorkflowSearchParam wfParam = new WorkflowSearchParam();
        wfParam.setSubWorkCodesSearch(Collections.singletonList(sub_work_code));
        RegexUtil.intExp(workflowService.getTasksCount(methodParam, wfParam), SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
    }

    /**
     * 分页查询工单列表
     *
     * @param methodParam 入参
     * @param pm          入参
     * @return 工单列表
     */
    @Override
    public Map<String, Object> getWorksList(MethodParam methodParam, Map<String, Object> pm) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam); // 分页参数
        String nowUserId = methodParam.getUserId();
        pm.put("pagination", pagination);
        if (RegexUtil.optIsPresentStr(pm.get("positionCodeSearch"))) {
            String[] positionCodeSearch = RegexUtil.optNotBlankStrOpt(pm.get("positionCodeSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("positionCodeSearch", positionCodeSearch);
        }
        if (RegexUtil.optIsPresentStr(pm.get("assetCategoryIdSearch"))) {
            String[] assetCategoryIdSearch = RegexUtil.optNotBlankStrOpt(pm.get("assetCategoryIdSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("assetCategoryIdSearch", assetCategoryIdSearch);
        }
        if (RegexUtil.optIsPresentStr(pm.get("workTypeIdSearch"))) {
            String[] workTypeIdSearch = RegexUtil.optNotBlankStrOpt(pm.get("workTypeIdSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("workTypeIdSearch", workTypeIdSearch);
        }
        if (RegexUtil.optIsPresentStr(pm.get("statusSearch"))) {
            String[] statusSearch = RegexUtil.optNotBlankStrOpt(pm.get("statusSearch")).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("statusSearch", statusSearch);
        }
        if (RegexUtil.optIsPresentStr(pm.get("overtimeSearch"))) {
            String overtimeSearch = RegexUtil.optNotBlankStrOpt(pm.get("overtimeSearch")).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            pm.put("overtimeSearch", overtimeSearch);
        }
        Integer total = worksMapper.findWorksListCount(methodParam.getSchemaName(), pm, nowUserId);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        List<Map<String, Object>> rows = worksMapper.findWorksList(methodParam.getSchemaName(), pm, nowUserId);
        for (Map<String, Object> result : rows) {
            List<String> menuList = methodParam.getUserPermissionList();
            result.put("isEdit", false);
            result.put("isDelete", false);
            result.put("isDetail", false);
            result.put("isDistribution", false);
            int status = RegexUtil.optIntegerOrVal(result.get("status"), StatusConstant.NULL_STATUS, methodParam, ErrorConstant.EC_LIST_001, RegexUtil.optStrOrBlank(result.get("status")));
            RegexUtil.optNotBlankStrOpt(result.get("duty_user_id")).ifPresent(d -> {
                if (menuList.contains("work_sheet_edit") && d.equals(nowUserId) && status < StatusConstant.COMPLETED) {
                    result.put("isEdit", true); // 判断是否有处理权限
                }
            });
            // 判断是否有作废权限
            if (menuList.contains("work_sheet_delete") && status < StatusConstant.COMPLETED) {
                result.put("isDelete", true);
            } else if (status <= StatusConstant.DRAFT && RegexUtil.optEquals(result.get("create_user_id"), nowUserId)) {
                result.put("isDelete", true);
            }
            //判断是否有查看详情权限
            if (menuList.contains("work_sheet_detail")) {
                result.put("isDetail", true);
            }
            //判断是否有分配权限
            if (menuList.contains("work_sheet_distribution")) {
                result.put("isDistribution", true);
            }
            //如果状态为已关闭 已完成 已作废  则只能查看
            if (status == StatusConstant.COMPLETED_CLOSE
                    || status == StatusConstant.COMPLETED
                    || status == StatusConstant.CANCEL) {
                result.put("isEdit", false);
                result.put("isDelete", false);
                result.put("isDistribution", false);
            } else if (status <= StatusConstant.DRAFT) {
                result.put("isDistribution", false);
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 工单作废
     *
     * @param methodParam 入参
     * @param worksModel  入参
     */
    @Override
    public void cancelWorks(MethodParam methodParam, WorksModel worksModel) {
        String work_code = RegexUtil.optStrOrExpNotSelect(worksModel.getWork_code(), LangConstant.TITLE_AC_I); // 工单编号
        String work_type_id = RegexUtil.optStrOrExpNotSelect(worksModel.getWork_type_id(), LangConstant.TITLE_CATEGORY_X); // 获取当前工单类型id
        // 根据工单类型id获取业务类型
        int businessTypeId = RegexUtil.optSelectOrExpParam(selectOptionService.getSelectOptionAttrByCode(methodParam, "work_type_business_type_id", work_type_id, "value"), LangConstant.TITLE_CATEGORY_AB_F);
        // 更新工单概要表的状态为已作废
        Map<String, Object> worksMap = new HashMap<>();
        worksMap.put("status", StatusConstant.CANCEL);
        worksMap.put("int@status", "status");
        worksMap.put("work_code", work_code);
        String schemaName = methodParam.getSchemaName();
        // 根据概要表主键获取详情表信息
        List<Map<String, Object>> detailList = null;
        // 保存工单明细表，更新分配人、状态、时间等信息，根据工单概要表主键获取详情表信息
        if (Integer.valueOf(SensConstant.BUSINESS_NO_2000) > businessTypeId) {
            worksMapper.update(schemaName, worksMap, "_sc_works", "work_code"); // 工单概要表
            worksMapper.update(schemaName, worksMap, "_sc_works_detail", "work_code"); // 工单详情表
            detailList = worksMapper.findWorksDetailListByWorkCode(schemaName, work_code);
            // 设备流程
        } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3000) > businessTypeId) {
            worksMapper.update(schemaName, worksMap, "_sc_asset_works", "work_code"); // 设备工单概要表
            worksMapper.update(schemaName, worksMap, "_sc_asset_works_detail", "work_code"); // 设备工单详情表
            detailList = assetWorksMapper.findAssetWorksDetailListByWorkCode(schemaName, work_code);
            // 备件流程
        } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3100) > businessTypeId) {
            worksMapper.update(schemaName, worksMap, "_sc_bom_works", "work_code"); // 设备工单概要表
            worksMapper.update(schemaName, worksMap, "_sc_bom_works_detail", "work_code"); // 设备工单详情表
            detailList = bomWorksMapper.findBomWorksDetailListByCode(schemaName, work_code);
        }
        RegexUtil.optNotNullList(detailList).ifPresent(l -> {
            String business_type_id = String.valueOf(businessTypeId) + SensConstant.BUSINESS_NO_PROCESS;
            // 工作流上作废工单流程 删除流程实例或者删除部署
            for (Map<String, Object> workDetail : l) {
                RegexUtil.optNotBlankStrOpt(workDetail.get("sub_work_code")).ifPresent(c -> {
                    workflowService.cutWorkflowInstanceBySubWorkCode(methodParam, c, "作废");
                    workProcessService.saveWorkProccess(schemaName, c, StatusConstant.COMPLETED_CLOSE, methodParam.getUserId(), Constants.PROCESS_CANCEL_TASK); // 工单进度记录
                    logService.newLog(methodParam, business_type_id, c, LangUtil.doSetLogArrayNoParam("流转记录", "作废"));
                    RegexUtil.optNotNullMap(worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(schemaName, c, "status")).ifPresent(m -> {
                        m.put("field_value", StatusConstant.CANCEL);
                        worksMapper.updateWorksDetailColumnByCode(schemaName, m);
                    });
                });
            }
        });
    }

    /**
     * 工单分配
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public void worksAssignment(MethodParam methodParam, Map<String, Object> paramMap) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Timestamp receive_time = RegexUtil.optNotBlankStrOpt(paramMap.get("plan_begin_time")).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT)).map(d -> new Timestamp(d.getTime())).orElse(now);
        String receive_user_id = (String) paramMap.get("receive_user_id");
        String sub_work_code = paramMap.get("sub_work_code").toString();
        List<String> sub_work_codes = new ArrayList<>();
        String work_code = null;
        String business_type_id =
                RegexUtil.optNotBlankStrOpt(paramMap.get("business_type_id")).orElseGet(() -> {
                    //获取当前工单类型id
                    //根据工单类型id获取业务类型
                    Integer work_type_id = RegexUtil.optIntegerOrExpParam(paramMap.get("work_type_id"), LangConstant.MSG_A);
                    Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id);
                    return work_type.get("business_type_id").toString();
                });
        Map<String, Object> worksInfo = null;
        Map<String, Object> worksDetailInfo = null;
        if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1002.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1003.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1004.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1005.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1006.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1100.equals(business_type_id)) {
            //获取工单详情信息
            worksDetailInfo = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code);
            work_code = (String) worksDetailInfo.get("work_code");
            worksInfo = worksMapper.findWorksByCode(methodParam.getSchemaName(), work_code);
            Integer status = (Integer) worksInfo.get("status");
            // 保存工单明细表，更新分配人等信息
//            worksMapper.updateWaitWorkDetailByWorkCode(methodParam.getSchemaName(), receive_user_id, work_code);
            ResponseModel responseModel = null;
            Map<String, String> map = new HashMap<>();
            //更新工作流当前分配人
            map.put(FlowParmsConstant.WORK_CODE, work_code);
            map.put(FlowParmsConstant.RECEIVE_USER_ID, receive_user_id);
            map.put(FlowParmsConstant.BUSINESS_TYPE_ID, business_type_id);
            map.put(FlowParmsConstant.STATUS, status.toString());
            map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
            map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
            if (RegexUtil.optIsPresentStr(paramMap.get("begin_time"))) {
                map.put(FlowParmsConstant.BEGIN_TIME, paramMap.get("begin_time").toString());
            }
            Map<String, Object> dataInfo = new HashMap<String, Object>();
            // 处理中时只更新负责人
            if (StatusConstant.PENDING == status || StatusConstant.UNDISTRIBUTED == status) {
                dataInfo.put("work_code", work_code);
                dataInfo.put("receive_user_id", receive_user_id);
                dataInfo.put("receive_time", receive_time);
                dataInfo.put("distribute_time", now);
                dataInfo.put("distribute_user_id", methodParam.getUserId());
                if (RegexUtil.optIsPresentStr(paramMap.get("begin_time"))) {
                    dataInfo.put("dateTime@begin_time", "begin_time");
                    dataInfo.put("begin_time", Timestamp.valueOf(paramMap.get("begin_time") + " 00:00:00"));
                }
                outService.outServerAaaAaa(methodParam, sub_work_code, Constants.GET_NOW_NODE, worksInfo.get("occur_time"), worksDetailInfo.get("relation_id"), worksDetailInfo.get("receive_user_id"));
            }
            dataInfo.put("work_code", work_code);
            dataInfo.put("duty_user_id", receive_user_id);
            //更新工单状态
            if (StatusConstant.UNDISTRIBUTED == status) {
                map.put(FlowParmsConstant.STATUS, String.valueOf(StatusConstant.PENDING));
                worksMapper.updateWorkStatusByCode(methodParam.getSchemaName(), StatusConstant.PENDING, work_code);
                dataInfo.put("status", StatusConstant.PENDING);
            }
            worksMapper.updateWorksDetailByWorkCode(methodParam.getSchemaName(), dataInfo);
            Map<String, Object> receiveAccount = userService.getUserInfoById(methodParam, receive_user_id);
            map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receiveAccount.get("account").toString());
            if (RegexUtil.optIsPresentStr(paramMap.get("pageType")) && "workDispatch".equals(paramMap.get("pageType"))) {
                map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                responseModel = workflowService.assignWithSWC(methodParam, sub_work_code, receive_user_id, map);
            } else {
                // 明细表所有数据
                List<Map<String, Object>> dataList = worksMapper.findWorksDetailByWorkCode(methodParam.getSchemaName(), work_code);
                if (null != dataList && dataList.size() > 1) {
                    List<String> subWorkCodes = new ArrayList<String>();
                    for (Map<String, Object> codes : dataList) {
                        Integer tmpStatus = (Integer) codes.get("status");
                        if (StatusConstant.COMPLETED != tmpStatus) {
                            subWorkCodes.add(codes.get("sub_work_code").toString());
                        }
                    }
                    // 主明细数据
                    RegexUtil.optNotNullMap(worksMapper.findMainWorksDetailByWorkCode(methodParam.getSchemaName(), work_code)).map(m -> m.get("sub_work_code")).map(RegexUtil::optStrOrNull).ifPresent(c -> {
                        subWorkCodes.remove(c);
                        map.put(FlowParmsConstant.SUB_WORK_CODE, c); // 发短信用
                    });
                    responseModel = workflowService.parentAssign(methodParam, subWorkCodes, receive_user_id, map); // receive_account：被分配人
                } else {
                    map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                    responseModel = workflowService.assignWithSWC(methodParam, sub_work_code, receive_user_id, map);
                }
            }
            if (200 != (responseModel.getCode())) {
                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
            }
        } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)
                || SensConstant.BUSINESS_NO_2004.equals(business_type_id)
                || SensConstant.BUSINESS_NO_2005.equals(business_type_id)
                || SensConstant.BUSINESS_NO_2006.equals(business_type_id)) { //设备验收
            worksDetailInfo = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            work_code = (String) worksDetailInfo.get("work_code");
            worksInfo = assetWorksMapper.findAssetWorksInfoByCode(methodParam.getSchemaName(), work_code);
            Integer status = (Integer) worksInfo.get("status");
            // 保存工单明细表，更新分配人等信息
            assetWorksMapper.updateWaitAssetWorkDetailBySubWorkCode(methodParam.getSchemaName(), receive_user_id, sub_work_code);
            ResponseModel responseModel = null;
            Map<String, String> map = new HashMap<>();
            //更新工作流当前分配人
            map.put(FlowParmsConstant.WORK_CODE, work_code);
            map.put(FlowParmsConstant.RECEIVE_USER_ID, receive_user_id);
            map.put(FlowParmsConstant.BUSINESS_TYPE_ID, business_type_id);
            map.put(FlowParmsConstant.STATUS, status.toString());
            map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
            map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
            // 明细表所有数据
            List<Map<String, Object>> assetWorks = assetWorksMapper.findAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            for (Map<String, Object> assetWorksDetail : assetWorks
            ) {
                sub_work_codes.add(assetWorksDetail.get("sub_work_code").toString());
            }
            Map<String, Object> receiveAccount = userService.getUserInfoById(methodParam, receive_user_id);
            map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receiveAccount.get("account").toString());
            map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code); // 发短信用
            responseModel = workflowService.assignWithSWC(methodParam, sub_work_code, receive_user_id, map);
            if (200 != (responseModel.getCode())) {
                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
            }
        } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)
                || SensConstant.BUSINESS_NO_3003.equals(business_type_id)
                || SensConstant.BUSINESS_NO_3007.equals(business_type_id)
                || SensConstant.BUSINESS_NO_3008.equals(business_type_id)) {
            worksDetailInfo = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            work_code = (String) worksDetailInfo.get("work_code");
            worksInfo = bomWorksMapper.findBomWorksInfoByCode(methodParam.getSchemaName(), work_code);
            Integer status = (Integer) worksInfo.get("status");

            // 保存工单明细表，更新分配人等信息
            bomWorksMapper.updateWaitBomWorkDetailBySubWorkCode(methodParam.getSchemaName(), receive_user_id, sub_work_code);
            Map<String, String> map = new HashMap<>();
            //更新工作流当前分配人
            map.put(FlowParmsConstant.WORK_CODE, work_code);
            map.put(FlowParmsConstant.RECEIVE_USER_ID, receive_user_id);
            map.put(FlowParmsConstant.BUSINESS_TYPE_ID, business_type_id);
            map.put(FlowParmsConstant.STATUS, status.toString());
            map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
            map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
            // 明细表所有数据
            List<Map<String, Object>> bomWorks = bomWorksMapper.findBomWorksDetailListByCode(methodParam.getSchemaName(), work_code);
            for (Map<String, Object> bomWorksDetail : bomWorks) {
                sub_work_codes.add(bomWorksDetail.get("sub_work_code").toString());
            }
            Map<String, Object> receiveAccount = userService.getUserInfoById(methodParam, receive_user_id);
            map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receiveAccount.get("account").toString());
            map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code); // 发短信用
            ResponseModel responseModel = workflowService.parentAssign(methodParam, sub_work_codes, receive_user_id, map); // receive_account：被分配人
            if (200 != (responseModel.getCode())) {
                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
            }
        }
        String s = RegexUtil.optNotNullMap(userService.getUserInfoById(methodParam, receive_user_id)).map(m -> m.get("user_name")).map(n -> RegexUtil.optStrOrVal(n, LangConstant.TITLE_AG_Q)).orElse(LangConstant.TITLE_AG_Q);
        String str = RegexUtil.optEqualsOpt(paramMap.get("pageType"), "workDispatch").map(pts -> LangConstant.TITLE_AC_Z).orElse(LangConstant.TITLE_JD); // 工单调度、再分配
        logService.newLog(methodParam, business_type_id + SensConstant.BUSINESS_NO_PROCESS, sub_work_code, LangUtil.doSetLogArray("流转记录", LangConstant.TEXT_AQ, new String[]{str, s})); // 工单调度-回收
        logService.newLog(methodParam, business_type_id, sub_work_code, LangUtil.doSetLogArray("分配日志记录", LangConstant.TEXT_AQ, new String[]{str, s}));
        paramMap.put("msg_code", SensConstant.SMS_10003007);
        sendMessage(methodParam, paramMap, ButtonConstant.BTN_DISTRIBUTION);
    }

    /**
     * 工单分配列表
     *
     * @param methodParam 入参
     * @param paramMap    入参
     * @return 工单分配列表
     */
    @Override
    public List<Map<String, Object>> worksAssignmentList(MethodParam methodParam, Map<String, Object> paramMap) {
        String sub_work_code = RegexUtil.optStrOrExpNotNull(paramMap.get("sub_work_code"), LangConstant.MSG_A);
        Integer work_type_id = RegexUtil.optIntegerOrExpParam(paramMap.get("work_type_id"), LangConstant.MSG_A);
        //获取当前工单类型id
        //根据工单类型id获取业务类型
        Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id);
        int businessTypeId = RegexUtil.optIntegerOrExp(work_type.get("business_type_id"), methodParam, ErrorConstant.EC_WORK_011, LangConstant.LOG_B);
        Map<String, Object> worksDetailInfo = null;
        if (Integer.valueOf(SensConstant.BUSINESS_NO_2000) > businessTypeId) {
            worksDetailInfo = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code); // 获取工单详情信息
        } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3000) > businessTypeId) {
            worksDetailInfo = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code); // 设备
        } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3100) > businessTypeId) {
            worksDetailInfo = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code); // 备件
        }
        RegexUtil.optMapOrExpNullInfo(worksDetailInfo, "");
        String pref_id = RegexUtil.optStrOrPrmError(worksDetailInfo.get("pref_id"), methodParam, ErrorConstant.EC_WORK_011, LangConstant.LOG_B);
        Map<String, Object> nodeInfo = workFlowTemplateService.getWorkFlowInfoByPrefId(methodParam, pref_id);
        String flow_code = (String) nodeInfo.get("flow_code");
        Map<String, Object> map = new HashMap<>();
        map.put(FlowParmsConstant.PREF_ID, flow_code);
        map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
        String nodeId = workflowService.getNodeIdBySubWorkCode(methodParam, map);
        //查询该工单当前流畅节点id
        nodeInfo = workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, pref_id, nodeId, "base");
        String deal_role_id = nodeInfo.get("deal_role_id").toString();
        worksDetailInfo.put("deal_role_id", deal_role_id);
        worksDetailInfo.put("work_type_id", nodeInfo.get("work_type_id"));
        return RegexUtil.optNotNullListStr(getPickUserList(methodParam, worksDetailInfo)).map(l -> userService.getUserListByIds(methodParam, l)).orElse(new ArrayList<>());
    }

    /**
     * 今日上报数
     *
     * @param methodParam 入参
     * @return 今日上报数
     */
    @Override
    public Integer todayReportWorksCount(MethodParam methodParam) {
        return worksMapper.todayReportWorksCount(methodParam.getSchemaName(), methodParam.getUserId());
    }

    /**
     * 今日完成数
     *
     * @param methodParam 入参
     * @return 今日完成数
     */
    @Override
    public Integer todayFinishedWorksCount(MethodParam methodParam) {
        return worksMapper.todayFinishedWorksCount(methodParam.getSchemaName(), methodParam.getUserId());
    }

    /**
     * 超时任务数
     *
     * @param methodParam 入参
     * @return 超时任务数
     */
    @Override
    public Integer todayOvertimeWorksCount(MethodParam methodParam) {
        Map<String, Object> pm = new HashMap<>();
        pm.put("sectionType", "timeoutTask");
        return worksMapper.findWorksListCount(methodParam.getSchemaName(), pm, methodParam.getUserId());
    }

    /**
     * 工单提交
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public void worksSubmit(MethodParam methodParam, Map<String, Object> paramMap) {

        JSONArray logArr = new JSONArray();
        //提交数据解析  存储工单详情字段信息
        Map<String, Object> map_object = doAnalysisPageDataAndSave(methodParam, paramMap, logArr);
        //参数校验
        checkParams(paramMap);
        this.checkPermission(methodParam, paramMap);
        // 根据数据库中状态判断是否为重复操作
        checkIsRepeat(methodParam, map_object);
        String sub_work_code = map_object.get("sub_work_code").toString();
        String businessTypeId = map_object.get("business_type_id").toString();
        int do_flow_key = Integer.valueOf(paramMap.get("do_flow_key").toString());//按钮类型
        if (do_flow_key == ButtonConstant.BTN_SAVE && SensConstant.BUSINESS_NO_3005.equals(businessTypeId)) {
            bomInventorySave(methodParam, map_object, logArr);
        } else if (do_flow_key == ButtonConstant.BTN_SAVE) {
            saveWorkData(methodParam, map_object, logArr);
        } else {
            if (SensConstant.BUSINESS_NO_1001.equals(businessTypeId)
                    || SensConstant.BUSINESS_NO_1002.equals(businessTypeId)
                    || SensConstant.BUSINESS_NO_1003.equals(businessTypeId)
                    || SensConstant.BUSINESS_NO_1004.equals(businessTypeId)
                    || SensConstant.BUSINESS_NO_1005.equals(businessTypeId)
                    || SensConstant.BUSINESS_NO_1006.equals(businessTypeId)
                    || SensConstant.BUSINESS_NO_1100.equals(businessTypeId)) {
                works(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_2001.equals(businessTypeId)) { //设备验收
                assetAcceptance(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_2002.equals(businessTypeId)) { //设备调拨
                assetChange(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_2003.equals(businessTypeId)) { //设备报废
                assetScrapped(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_2004.equals(businessTypeId)) { //设备盘点
                assetInventory(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_2005.equals(businessTypeId)) { //设备出库
                assetOutPut(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_2006.equals(businessTypeId)) { //设备入库
                assetInput(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3001.equals(businessTypeId)) { //备件入库
                bomInput(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3003.equals(businessTypeId)) { //备件领用
                bomCollect(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3005.equals(businessTypeId)) { //备件盘点
                bomInventory(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3007.equals(businessTypeId)) { //备件销账
                bomRemove(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3008.equals(businessTypeId)) { //备件采购购需求
                bomPurchasingDemand(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3004.equals(businessTypeId)) { //备件报废
                bomScrapped(methodParam, map_object, logArr);
            } else if (SensConstant.BUSINESS_NO_3002.equals(businessTypeId)) { //备件调拨
                bomChange(methodParam, map_object, logArr);
            }
            //生成流转记录
            if (RegexUtil.optIsPresentStr(map_object.get("works_log"))) {
                String works_log = map_object.get("works_log").toString();
                if (RegexUtil.optIsPresentStr(map_object.get("carbon_copy"))) {
                    LinkedHashMap<String, Object> keyMap = new LinkedHashMap<>();
                    String cc_user = userService.findAllCCUser(methodParam, RegexUtil.optStrOrBlank(worksMapper.findCCUserByCode(methodParam.getSchemaName(), sub_work_code)));
                    keyMap.put("key", works_log);
                    keyMap.put("cc_u", cc_user);
                    logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, sub_work_code, LangUtil.doSetLogArrayAndKeyNoParam("流转记录", keyMap));
                } else {
                    logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", works_log));
                }
                if (RegexUtil.optIsPresentStr(map_object.get("main_sub_work_code"))) {
                    String main_sub_work_code = map_object.get("main_sub_work_code").toString();
                    if (SensConstant.BUSINESS_NO_3005.equals(businessTypeId)) {
                        if (bomWorksMapper.findAllSubBomWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) == 1) {
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", works_log));
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", "盘点完成"));
                        } else if (bomWorksMapper.findAllSubBomWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) > 1 && bomWorksMapper.findEndSubBomWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) == 1) {
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", works_log));
                        } else if (bomWorksMapper.findAllSubBomWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) > 1 && bomWorksMapper.findNoEndSubBomWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) == 0) {
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", "盘点完成"));
                        }
                    } else if (SensConstant.BUSINESS_NO_2004.equals(businessTypeId)) {
                        if (assetWorksMapper.findAllSubAssetWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) == 1) {
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", works_log));
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", "盘点完成"));
                        } else if (assetWorksMapper.findAllSubAssetWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) > 1 && assetWorksMapper.findEndSubAssetWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) == 1) {
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", works_log));
                        } else if (assetWorksMapper.findAllSubAssetWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) > 1 && assetWorksMapper.findNoEndSubAssetWorksDetailByWorkCode(methodParam.getSchemaName(), map_object.get("work_code").toString()) == 0) {
                            logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", "盘点完成"));
                        }
                    } else {
                        logService.newLog(methodParam, businessTypeId + SensConstant.BUSINESS_NO_PROCESS, main_sub_work_code, LangUtil.doSetLogArrayNoParam("流转记录", works_log));
                    }
                }
            }
        }
        workProcessService.saveWorkProccess(methodParam.getSchemaName(), sub_work_code, Integer.valueOf(map_object.get("status").toString()), methodParam.getUserId(), map_object.get("node_id").toString()); // 工单进度记录

    }

    /**
     * 校验流程参数是否确实
     *
     * @param map_object 入参
     */
    private void checkParams(Map<String, Object> map_object) {
        //验证do_flow_key对象是否为空
        if (!RegexUtil.optIsPresentStr(map_object.get("do_flow_key"))) {
            throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_AAAAB_I});//按钮不能为空
        }
        //验证工单类型是否为空
        if (!RegexUtil.optIsPresentStr(map_object.get("work_type_id"))) {
            throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_CATEGORY_X});//工单类型不能为空
        }
        //验证工单模板编码是否为空
        if (!RegexUtil.optIsPresentStr(map_object.get("work_template_code"))) {
            throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_AC_W});//工单模板编码不能为空
        }
        //验证状态是否为空
        if (!RegexUtil.optIsPresentStr(map_object.get("status"))) {
            throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_MP});//状态不能为空
        }
        //验证流程id是否为空
        if (!RegexUtil.optIsPresentStr(map_object.get("pref_id"))) {
            throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_AAI_J});//不能为空
        }
    }

    /**
     * 根据数据库中状态判断是否为重复操作
     *
     * @param methodParam 入参
     * @param map_object  入参
     */
    private void checkIsRepeat(MethodParam methodParam, Map<String, Object> map_object) {
        if (RegexUtil.optIsPresentStr(map_object.get("work_code"))) {
            String work_code = map_object.get("work_code").toString();
            String status = map_object.get("status").toString();
            // 根据数据库中状态判断是否为重复操作  数据库查到的状态值和当前节点状态值不一样  则为重复操作
            Map<String, Object> workInfo = worksMapper.findWorksByCode(methodParam.getSchemaName(), work_code);
            if (RegexUtil.optNotNull(workInfo).isPresent() && status.equals(workInfo.get("status"))) {
                throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);//数据已变更，请重新查询获取最新数据！
            }
            RegexUtil.optNotNullMap(workInfo).ifPresent(wi -> RegexUtil.falseExp(RegexUtil.optEquals(RegexUtil.optStrOrBlank(map_object.get("node_id")), RegexUtil.optStrOrBlank(workflowService.getNodeIdBySubWorkCode(methodParam, map_object))), SensConstant.PAGE_ERROR_SUBMIT_REPEAT));
        }
    }

    /**
     * 保存事件
     */
    private void saveWorkData(MethodParam methodParam, Map<String, Object> map_object, JSONArray logArr) {
        String workCode = (String) map_object.get("work_code");
        String subWorkCode = (String) map_object.get("sub_work_code");
        String workRequestCode = (String) map_object.get("work_request_code");
        Map<String, Map<String, Object>> dataInfo = (Map<String, Map<String, Object>>) map_object.get("dataInfo");
        Set<String> tables = dataInfo.keySet();
        String tableKey = null; // 数据主键
        Map<String, String> keys = (Map<String, String>) map_object.get("keys");
        for (String name : tables) {
            tableKey = keys.get(name);
            if (tableKey.startsWith(SqlConstant.IS_NEW_DATA)) {
                worksMapper.insert(methodParam.getSchemaName(), dataInfo.get(name), name); // 新增
                //日志新增记录
                logArr.add(LangUtil.doSetLogArray("新增" + name + "表记录", LangConstant.TAB_DEVICE_B, new String[]{name}));
            } else {
                Map<String, Object> oldMap = worksMapper.query(methodParam.getSchemaName(), name, tableKey, dataInfo.get(name).get(tableKey).toString());
                worksMapper.update(methodParam.getSchemaName(), dataInfo.get(name), name, tableKey); // 修改
                Map<String, Object> nowMap = worksMapper.query(methodParam.getSchemaName(), name, tableKey, dataInfo.get(name).get(tableKey).toString());
                //日志更新记录
                logArr.add(LangUtil.doSetLogArray("更新" + name + "表记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, name, LangUtil.compareMap(oldMap, nowMap).toString()}));
            }
        }
        // 工单步骤，非工单请求步骤
        if ((RegexUtil.optIsPresentStr(workCode)
                && RegexUtil.optIsPresentStr(subWorkCode)
                || RegexUtil.optIsPresentStr(workRequestCode))) {
            String tblCode = subWorkCode;
            if (!RegexUtil.optIsPresentStr(tblCode) && RegexUtil.optIsPresentStr(workRequestCode)) {
                tblCode = workRequestCode;
            }
            if (!RegexUtil.optIsPresentStr(tblCode) && RegexUtil.optIsPresentStr(workCode)) {
                tblCode = workCode;
            }
            Integer subReturnStatus = (Integer) map_object.get("subReturnStatus");
            if (null != subReturnStatus) {
                worksMapper.updateWorkDetailStatusByWorkCode(methodParam.getSchemaName(), subReturnStatus, workCode);
            }
            doSaveMultipleTableData(methodParam.getSchemaName(), "5", "_sc_work_dutyman_hour", map_object); // 工单工时处理，支援人员处理
            if (RegexUtil.optIsPresentStr(tblCode)) {
                doSaveMidTblInfo(methodParam, tblCode, map_object); // 中间表处理
            }
        }
        Map<String, Object> worksInfo = worksMapper.findWorksByCode(methodParam.getSchemaName(), workCode);
        Map<String, Object> worksDetailInfo = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), subWorkCode);
        dataInfo.put("_sc_works", worksInfo);
        dataInfo.put("_sc_works_detail", worksDetailInfo);
        map_object.put("dataInfo", dataInfo);
        String business_type_id = map_object.get("business_type_id").toString();
        if (RegexUtil.optIsPresentList(logArr)) {
            logService.newLog(methodParam, business_type_id, subWorkCode, logArr.toString());
        }
    }

    //2.14 业务接口处理，数据权限验证【工作流操作人/操作人范围是不是当前登录人】
    private void checkPermission(MethodParam methodParam, Map<String, Object> paramMap) {
        String business_type_id = paramMap.get("business_type_id").toString();
        //判断当前的处理人是否是当前用户就可以
        Integer isNew = Integer.valueOf(paramMap.get("isNew").toString());
        if (1 != isNew) {
            String sub_work_code = paramMap.get("sub_work_code").toString();
            if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1002.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1005.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1006.equals(business_type_id)) {
            } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)) {
                Map<String, Object> works_detail_info = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
                if (!works_detail_info.get("duty_user_id").equals(methodParam.getUserId())) {
                    throw new SenscloudException(LangConstant.MSG_CA);//权限不足
                }
            } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)) {
                Map<String, Object> asset_works_detail_info = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
                if (!asset_works_detail_info.get("duty_user_id").equals(methodParam.getUserId())) {
                    throw new SenscloudException(LangConstant.MSG_CA);//权限不足
                }
            }
        }
    }

    /**
     * 工单组织
     *
     * @param schemaName  入参
     * @param subWorkCode 入参
     * @param assetId     入参
     * @return 新增记录数
     */
    public int insertWorkAssetOrg(String schemaName, String subWorkCode, String assetId) {
        worksMapper.deleteBySubWorkCode(schemaName, "_sc_works_detail_asset_org", subWorkCode); // 删除原数据
        if (assetId.contains(",")) {
            String[] ids = assetId.split(",");
            int i = 0;
            int count = 0;
            for (String strId : ids) {
                count = worksMapper.insertWorkAssetOrg(schemaName, subWorkCode, strId); // 工单设备组织处理
                i = i + count;
            }
            return i;
        } else {
            return worksMapper.insertWorkAssetOrg(schemaName, subWorkCode, assetId); // 工单设备组织处理
        }
    }

    /**
     * 保存多选单表数据
     *
     * @param schemaName 入参
     * @param tableIndex 入参
     * @param tableName  入参
     * @param map        入参
     */
    private void doSaveMultipleTableData(String schemaName, String tableIndex, String tableName, Map<String, Object> map) {
        tableIndex = "multipleTableData" + tableIndex;
        if (map.containsKey(tableIndex)) {
            Object pageData = map.get(tableIndex);
            this.doSaveMultipleTableDataCommon(schemaName, tableName, pageData, map);
        }
    }

    /**
     * 保存多选单表数据
     *
     * @param schemaName 入参
     * @param tableName  入参
     * @param pageData   入参
     * @param map        入参
     */
    private void doSaveMultipleTableDataCommon(String schemaName, String tableName, Object pageData, Map<String, Object> map) {
        // 删除原数据
        String subWorkCode = (String) map.get("subWorkCode");
        String workRequestCode = (String) map.get("work_request_code");
        String saveSubType = (String) map.get("saveSubType");
        boolean isInsert = true;
        if (RegexUtil.optIsPresentStr(saveSubType)) {
            worksMapper.deleteBySubWorkCode(schemaName, tableName, subWorkCode);
        } else if ("request".equals(saveSubType)) {
            if (RegexUtil.optIsPresentStr(subWorkCode)) {
                subWorkCode = workRequestCode;
            }
            worksMapper.deleteByWorkRequestCode(schemaName, workRequestCode, tableName);
        } else if ("updateRequest".equals(saveSubType)) {
            worksMapper.updateWorkDutymanHourInfoByCode(schemaName, subWorkCode, workRequestCode);
            isInsert = false;
        }
        if (isInsert && null != pageData && !"".equals(pageData)) {
            JSONArray dataArrays = JSONArray.fromObject(pageData); // 页面数据列表
            int dataSize = dataArrays.size();
            if (dataSize > 0) {
                List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
                Map<String, Object> rowDataInfo = null;
                int i = 0;
                for (Object rowData : dataArrays) {
                    rowDataInfo = new HashMap<String, Object>();
                    rowDataInfo.put("sub_work_code", subWorkCode);
                    rowDataInfo.put("operate_account", rowData);
                    rowDataInfo.put("is_support", true);
                    rowDataInfo.put("work_hour", 0);
                    rowDataInfo.put("work_request_code", workRequestCode);
                    dataList.add(rowDataInfo);
                    i++;
                    if (i == Constants.BATCH_INSERT_NUM) {
                        commonUtilService.doBatchInsertSql(schemaName, tableName, SqlConstant._sc_work_dutyman_hour_columns, dataList);
                        dataList = new ArrayList<Map<String, Object>>();
                        i = 0;
                    }
                }
                if (i > 0) {
                    commonUtilService.doBatchInsertSql(schemaName, tableName, SqlConstant._sc_work_dutyman_hour_columns, dataList);
                }
            }
        }
    }

    /**
     * 保存中间表信息
     *
     * @param methodParam 入参
     * @param subWorkCode 入参
     * @param map         入参
     */
    private void doSaveMidTblInfo(MethodParam methodParam, String subWorkCode, Map<String, Object> map) {
        List<Map<String, Object>> midTblList = selectOptionService.getStaticSelectList(methodParam, Constants.DY_MID_TBL);
        if (RegexUtil.optNotNull(midTblList).isPresent()) {
            for (Map<String, Object> info : midTblList) {
                doSaveMidTblData(methodParam.getSchemaName(), subWorkCode, map, Constants.DY_MID_TBL + info.get("code"), info.get("desc"));
            }
        }
    }

    /**
     * 保存中间表数据
     *
     * @param schemaName  入参
     * @param subWorkCode 入参
     * @param map         入参
     * @param key         入参
     * @param tblMap      入参
     */
    private void doSaveMidTblData(String schemaName, String subWorkCode, Map<String, Object> map, String key, Object tblMap) {
        if (map.containsKey(key) && null != tblMap) {
            JSONObject tblInfo = JSONObject.fromObject(tblMap);
            String tblName = tblInfo.getString("tblName");
            worksMapper.deleteBySubWorkCode(schemaName, tblName, subWorkCode); // 删除原数据
            Object tblData = map.get(key);

            if (null != tblData && !"".equals(tblData)) {
                JSONArray dataArrays = JSONArray.fromObject(tblData); // 页面数据列表
                int dataSize = dataArrays.size();
                String columnStr = "";
                String columnDataStr = "";
                if (dataSize > 0) {
                    if (tblInfo.containsKey("columnStr")) {
                        columnStr = tblInfo.getString("columnStr");
                    } else {
                        columnDataStr = tblInfo.getString("columnDataStr");
                    }
                    List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
                    Map<String, Object> dataRow = null;
                    String[] numStrings = {"price", "use_count"};
                    int i = 0;
                    for (Object rowData : dataArrays) {
                        JSONObject rowJson = JSONObject.fromObject(rowData);
                        dataRow = new HashMap<String, Object>();
                        dataRow.putAll(rowJson);
                        if (!"".equals(columnDataStr)) {
                            JSONObject jsonObject = JSONObject.fromObject(columnDataStr);
                            Iterator iterator = jsonObject.keys();
                            while (iterator.hasNext()) {
                                String key1 = (String) iterator.next();
                                if (!"".equals(columnStr)) {
                                    columnStr = columnStr.concat("," + key1);
                                } else {
                                    columnStr = columnStr.concat(key1);
                                }
                                String value1 = jsonObject.getString(key1);
                                dataRow.put(key1, rowJson.get(value1));
                            }
                        }
                        dataRow.put("sub_work_code", subWorkCode);
                        for (String numStr : numStrings) {
                            try {
                                if (null == dataRow.get(numStr) || "".equals(dataRow.get(numStr))) {
                                    dataRow.put(numStr, 0);
                                }
                            } catch (Exception bst) {
                            }
                        }
                        dataList.add(dataRow);
                        i++;
                        if (i == Constants.BATCH_INSERT_NUM) {
                            commonUtilService.doBatchInsertSql(schemaName, tblName, columnStr, dataList);
                            dataList = new ArrayList<Map<String, Object>>();
                            i = 0;
                        }
                    }
                    if (i > 0) {
                        commonUtilService.doBatchInsertSql(schemaName, tblName, columnStr, dataList);
                    }
                }
            }
        }
    }

    /**
     * 从可选人列表里随机获取一个人
     *
     * @param methodParam 系统参数
     * @param map_object  页面参数
     * @return 人员
     */
    private String getRandomUserId(MethodParam methodParam, Map<String, Object> map_object) {
        return RegexUtil.optNotNullListStr(getPickUserList(methodParam, map_object)).map(l -> l.get(SenscloudUtil.gnrRandNumByNum(l.size()))).orElse(null);
    }

    /**
     * 获取可选人列表
     *
     * @param methodParam 系统参数
     * @param map_object  页面参数
     * @return 可选人列表
     */
    private List<String> getPickUserList(MethodParam methodParam, Map<String, Object> map_object) {
        String roleId = RegexUtil.optStrOrNull(map_object.get("deal_role_id"));
        Integer workTypeId = RegexUtil.optIntegerOrNull(map_object.get("work_type_id"), LangConstant.TITLE_CATEGORY_X);
        if (null == roleId || null == workTypeId) {
            return new ArrayList<>();
        }
        String positionCode = RegexUtil.optStrOrNull(map_object.get("position_code"));
        Integer categoryId = RegexUtil.optIntegerOrNull(map_object.get("category_id"), LangConstant.TITLE_ASSET_C);
//        Integer type = RegexUtil.optIntegerOrVal(map_object.get(Constants.FLOW_USER_TYPE), 0, methodParam, ErrorConstant.EC_WORK_005);
//        if (1 == type) {
//            return worksMapper.findUserIdListByAsset(methodParam.getSchemaName(), roleId, positionCode, categoryId);
//        }
        String relationId = RegexUtil.optStrOrNull(map_object.get("relation_id"));
        String relationType = RegexUtil.optStrOrNull(map_object.get("relation_type"));
        if (null != relationId && null != relationType) {
            // 位置
            if ("1".equals(relationType)) {
                positionCode = relationId;
                // 设备
            } else if ("2".equals(relationType)) {
                // 先查询该设备有无直接维保人员，有的话直接查询人员列表
                String[] relations = relationId.split(",");
                if (relations.length > 0) {
                    Map<String, Object> assetInfo = assetDataService.getAssetInfo(methodParam, relations[0]);
                    if (RegexUtil.optIsPresentMap(assetInfo)) {
                        positionCode = RegexUtil.optStrOrNull(assetInfo.get("position_code"));
                        categoryId = RegexUtil.optIntegerOrNull(assetInfo.get("category_id"), LangConstant.TITLE_ASSET_C);
                    }
                }
            }
        }
        Integer stockId = RegexUtil.optIntegerOrNull(map_object.get("stock_id"), LangConstant.TITLE_AJ_Q);
        return worksMapper.findUserIdListByPermission(methodParam.getSchemaName(), roleId, positionCode, workTypeId, stockId, categoryId);
    }

    /**
     * 更新工单负责的设备组织和工单负责部门
     */
    private void updateWorksOrgAndGroup(MethodParam methodParam, String sub_work_code, String relation_id, String user_id) {
        // 插入新的工单设备负责组织
        if (null != relation_id && relation_id.contains(",")) {
            String[] ids = relation_id.split(",");
            for (String strId : ids) {
                worksMapper.insertWorkAssetOrg(methodParam.getSchemaName(), sub_work_code, strId); // 工单设备组织处理
            }
        } else {
            worksMapper.insertWorkAssetOrg(methodParam.getSchemaName(), sub_work_code, relation_id); // 工单设备组织处理
        }
        //插入新的工单负责部门
        // worksMapper.insertWorkReceiveGroup(methodParam.getSchemaName(), sub_work_code, user_id);
    }

    /**
     * 根据当前taskid获取当前节点流程id进而获取当前流程所有信息
     *
     * @param methodParam 入参
     * @param node_id     入参
     * @param pref_id     入参
     * @return 流程节点信息
     */
    private Map<String, Object> getWorkFlowTemplatesInfoByTaskId(MethodParam methodParam, String node_id, String pref_id) {
        Map<String, Object> workFlowNodeTemplateInfo = workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, pref_id, node_id, "base");
        if (RegexUtil.optNotNull(workFlowNodeTemplateInfo).isPresent()) {
            List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) workFlowNodeTemplateInfo.get("work_flow_node_column_list");
            //将工单节点信息的字段每个都放入一级字段名和字段值对应
            work_flow_node_column_list.forEach(item -> {
                String field_code = item.get("field_code").toString();
                workFlowNodeTemplateInfo.put(field_code, item.get("field_value"));
            });
            return workFlowNodeTemplateInfo;
        } else {
            return new HashMap<>();
        }
    }

    private Map<String, Object> nodeInfoAddColumns(Map<String, Object> nodeInfo) {
        if (RegexUtil.optNotNull(nodeInfo).isPresent()) {
            List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) nodeInfo.get("work_flow_node_column_list");
            //将工单节点信息的字段每个都放入一级字段名和字段值对应
            work_flow_node_column_list.forEach(item -> {
                String field_code = item.get("field_code").toString();
                nodeInfo.put(field_code, item.get("field_value"));
            });
            return nodeInfo;
        } else {
            return new HashMap<>();
        }
    }

    /**
     * 前端传来的数据解析存储到数据库
     *
     * @param methodParam 入参
     * @param paramMap    入参
     * @param logArr      入参
     * @return 前端传来的数据解析存储到数据库
     */
    private Map<String, Object> doAnalysisPageDataAndSave(MethodParam methodParam, Map<String, Object> paramMap, JSONArray logArr) {
        paramMap.remove("create_user_id");
        Integer isNew = 1;
        Integer isDraft = 0;
        String sub_work_code = "";
        String flow_code = (String) paramMap.get("flow_code");
        Map<String, Object> workFlowNodeInfo = new HashMap<>();//流程节点详情和字段信息
        String pref_id = paramMap.get("pref_id").toString();
        int do_flow_key = Integer.valueOf(paramMap.get("do_flow_key").toString());//按钮类型
        List<Map<String, Object>> work_flow_node_column_list = new ArrayList<>();
        Map<String, Object> mainWorkDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), RegexUtil.optStrOrNull(paramMap.get("sub_work_code")));
        if (do_flow_key == ButtonConstant.BTN_SAVE && SensConstant.BUSINESS_NO_3005.equals(RegexUtil.optStrOrNull(paramMap.get("business_type_id")))) {
            //该工单为新建工单 取该工单流程的开始节点 根据流程id获取开始节点详情和字段信息
            workFlowNodeInfo = workFlowTemplateService.getWorkFlowStartNodeTemplateInfo(methodParam, pref_id);
        } else {
            //如果sub_work_code为空 则为新建  否则为更新
            if (RegexUtil.optIsPresentStr(paramMap.get("sub_work_code")) && 10 != RegexUtil.optNotNullMap(mainWorkDetail).map(e -> RegexUtil.optIntegerOrNull(e.get("status"))).orElse(0)) {
                isNew = -1;
                sub_work_code = paramMap.get("sub_work_code").toString();
                Map<String, Object> map = new HashMap<>();
                map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                map.put(FlowParmsConstant.PDEID, flow_code);
                String nodeId = workflowService.getNodeIdBySubWorkCode(methodParam, map);//获取流程节点ID
                String task_id = workflowService.getTaskKeyBySubWorkCode(methodParam, sub_work_code);
                if (!RegexUtil.optIsPresentStr(task_id)) {
                    throw new SenscloudException(LangConstant.MSG_CA);//权限不足
                }
                paramMap.put("task_id", task_id);
                if (!RegexUtil.optIsPresentStr(nodeId) || "end".equals(nodeId)) {
                    throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AK});//节点ID信息不存在
                }
                //工作流推动到下一节点后  获取下一节点的工单流程节点信息
                workFlowNodeInfo = workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, pref_id, nodeId, "base");
            } else {
                if (RegexUtil.optIsPresentStr(paramMap.get("sub_work_code")) && 10 == RegexUtil.optNotNullMap(mainWorkDetail).map(e -> RegexUtil.optIntegerOrNull(e.get("status"))).orElse(0)) {
                    isDraft = 1;
                    isNew = -1;
                }
                //该工单为新建工单 取该工单流程的开始节点 根据流程id获取开始节点详情和字段信息
                workFlowNodeInfo = workFlowTemplateService.getWorkFlowStartNodeTemplateInfo(methodParam, pref_id);
            }
        }
        paramMap.put("isNew", isNew);
        paramMap.put("isDraft", isDraft);
        //将字段信息直接放入map第一层
        workFlowNodeInfo = nodeInfoAddColumns(workFlowNodeInfo);
        flow_code = (String) workFlowNodeInfo.get("flow_code");
        paramMap.put("workFlowNodeInfo", workFlowNodeInfo);
        paramMap.put("flow_code", flow_code);
        paramMap.put("work_flow_node_column_list", workFlowNodeInfo.get("work_flow_node_column_list"));
        work_flow_node_column_list = (List<Map<String, Object>>) workFlowNodeInfo.get("work_flow_node_column_list");
        workFlowNodeInfo = getNodeColumnsByNodeInfo(workFlowNodeInfo);
        //当前工单处理人角色
        if (!RegexUtil.optIsPresentStr(workFlowNodeInfo.get("deal_role_id"))) {
            throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_ABL});//处理角色为空
        }
        String deal_role_id = workFlowNodeInfo.get("deal_role_id").toString();
        paramMap.put("deal_role_id", deal_role_id);
        //根据工单详情编码获取字段详情信息
        Map<String, Object> worksDetailColumn = null;
        if (RegexUtil.optIsPresentStr(sub_work_code) && !"".equals(sub_work_code)) {
            worksDetailColumn = getWorksDetailColumnList(methodParam, sub_work_code);
        } else {
            worksDetailColumn = new HashMap<>();
        }
        Map<String, Map<String, Object>> dataInfo = new HashMap<String, Map<String, Object>>();
        Map<String, Object> tmpMap = null;
        String tableName = null; // 表名
        //String saveType = null; // 保存类型
        String field_code = null; // 数据库字段名
        String change_type = null; // 数据库字段类型
        String field_form_code = null; // 页面字段名
        String field_value = null;
        Integer index = 0;//表类型
        Map<String, String> keys = new HashMap<String, String>();
        //要存入工作流的字段
        List<String> flow_keys = new ArrayList<>();
        //解析后的数据
        List<Map<String, Object>> WorksDetailColumnList = new ArrayList<Map<String, Object>>();
        //获取当前工单类型id
        Integer work_type_id = Integer.valueOf(paramMap.get("work_type_id").toString());
        //根据工单类型id获取业务类型
        Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id);
        Integer business_type_id = Integer.valueOf(work_type.get("business_type_id").toString());
        paramMap.put("business_type_id", business_type_id);
        Map<String, Object> business_type_table_map = new HashMap<>();
        //根据字典表获取该业务类型所需要的目标表
        List<Map<String, Object>> business_type_table_List = selectOptionService.getTargetTableList(methodParam, "business_type_table", business_type_id.toString());
        for (Map<String, Object> business_type_table : business_type_table_List) {
            business_type_table_map.put(business_type_table.get("name").toString(), business_type_table);
        }
        if (RegexUtil.optNotNull(work_flow_node_column_list).isPresent()) {
            for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                field_value = null;
                field_code = work_flow_node_column.get("field_code").toString();
                //判断该字段是自定义字段还是目标表字段
                //获取当前主键
                field_form_code = work_flow_node_column.get("field_form_code").toString();
                //判断前端传来的有没有该字段
                if (RegexUtil.optIsPresentStr(paramMap.get(field_code))) {
                    field_value = paramMap.get(field_code).toString();
                } else if (RegexUtil.optIsPresentStr(paramMap.get(field_form_code))) {
                    field_value = paramMap.get(field_form_code).toString();
                } else {
                    field_value = (String) work_flow_node_column.get("field_value");
                }
                //如果为空的话  查看是否是business_type_table改业务表的主键
                for (Map<String, Object> business_type_table : business_type_table_List) {
                    if (!RegexUtil.optIsPresentStr(field_value) && field_code.equals(business_type_table.get("code"))) {
                        index = Integer.valueOf(business_type_table.get("reserve1").toString()) - 1;
                        field_value = dynamicCommonService.getDataCode(methodParam, index);
                    }
                }
                //查看当前字段存储类型是
                Map<String, Object> field_target_table = selectOptionService.getStaticSelectOption(methodParam, "field_target_table", field_code);
                if (RegexUtil.optNotNull(field_target_table).isPresent()) {
                    String name = field_target_table.get("text").toString();
                    JSONArray tables = JSONArray.fromObject(name);
                    for (Map<String, Object> business_type_table : business_type_table_List) {
                        tableName = business_type_table.get("name").toString();
                        for (Object object : tables) {
                            JSONObject target_table = JSONObject.fromObject(object);
                            change_type = target_table.get("change_type").toString();
                            String table_name = target_table.get("table_name").toString();
                            if (business_type_table.get("name").equals(table_name)) {
                                //是新建目标表主键
                                if (business_type_table.get("code").equals(field_code) && isNew == 1) {
                                    keys.put(tableName, SqlConstant.IS_NEW_DATA + field_code);
                                } else if (business_type_table.get("code").equals(field_code) && isNew == -1) {
                                    keys.put(tableName, field_code);
                                }
                                if (dataInfo.containsKey(tableName)) {
                                    tmpMap = dataInfo.get(tableName);
                                } else {
                                    tmpMap = new HashMap<String, Object>();
                                }
                                // 日期空值转换
                                if (SqlConstant.DB_TYPE_TIMESTAMP.contains("," + change_type + ",") &&
                                        (null == field_value || "".equals(field_value))) {
                                    tmpMap.put("null@" + field_code, field_code);
                                } else {
                                    tmpMap.put(change_type + "@" + field_code, field_code);
                                }
                                if (null == field_code || "".equals(field_code)) {
                                    tmpMap.put(field_code, null);
                                } else {
                                    tmpMap.put(field_code, field_value);
                                }
                                dataInfo.put(tableName, tmpMap);
                            }
                        }
                    }
                }
                paramMap.put(field_code, field_value);
            }
        }
        sub_work_code = paramMap.get("sub_work_code").toString();
        for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
            field_form_code = work_flow_node_column.get("field_form_code").toString();
            field_code = work_flow_node_column.get("field_code").toString();
            if ("works_log".equals(field_code)) {
                continue;
            }
            if (RegexUtil.optIsPresentStr(paramMap.get(field_code))) {
                field_value = paramMap.get(field_code).toString();
            } else {
                field_value = null;
            }
            //根据字段主键和工单详情编号获取以前字段信息 如果没有则新增
            paramMap.put("isMain", "1");
            Map<String, Object> bom_sub_work_detail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
            if (!(do_flow_key == ButtonConstant.BTN_SAVE && RegexUtil.optIsPresentStr(bom_sub_work_detail))) {
                updateWorksDetailColumnByFormCode(methodParam, paramMap, sub_work_code, field_form_code, field_value, workFlowNodeInfo, worksDetailColumn, flow_keys, logArr);
            }
        }
        String position_code = "";
        String title = "";
        if (!RegexUtil.optIsPresentStr(paramMap.get("title"))) {
            if (RegexUtil.optNotNull(dataInfo.get("_sc_works_detail")).isPresent()) {
                tmpMap = dataInfo.get("_sc_works_detail"); // 明细表数据
                //生成工单标题
                Integer relation_type = Integer.valueOf(tmpMap.get("relation_type").toString());
                String relation_id = RegexUtil.optStrOrBlank(tmpMap.get("relation_id"));
                if (relation_type == 1) {//位置
                    Map<String, Object> positionMap = RegexUtil.optMapOrExpNullInfo(assetPositionService.getPositionInfoByCode(methodParam, relation_id), LangConstant.TITLE_ASSET_G);
                    title = RegexUtil.optStrOrBlank(positionMap.get("position_name"));
                } else if (relation_type == 2) {//设备
                    String[] relations = RegexUtil.optStrToArray(relation_id, LangConstant.TITLE_ASSET_I);
                    if (relations.length > 0) {
                        Map<String, Object> assetMap = RegexUtil.optMapOrExpNullInfo(assetDataService.getAssetInfo(methodParam, relations[0]), LangConstant.TITLE_ASSET_I);
                        title = RegexUtil.optStrOrBlank(assetMap.get("asset_name"));
                    }
                } else if(relation_type == 4) { //客户
                    FacilitiesModel facilitiesModel = new FacilitiesModel();
                    facilitiesModel.setId(Integer.valueOf(relation_id));
                    Map<String,Object> customerMap = RegexUtil.optMapOrNew(customersService.findById(methodParam,facilitiesModel));
                    title = String.valueOf(customerMap.get("short_title"));

                }
            }
            title = title + work_type.get("type_name").toString();
            if (RegexUtil.optNotNull(dataInfo.get("_sc_works_detail")).isPresent()) {
                //判断设备位置为 position_code
                if (RegexUtil.optIsPresentStr(paramMap.get("position_code"))) {
                    position_code = paramMap.get("position_code").toString();
                } else {
                    if ("1".equals(tmpMap.get("relation_type"))) {
                        position_code = tmpMap.get("relation_id").toString();
                    } else if ("2".equals(tmpMap.get("relation_type"))) {
                        String[] relationIds = tmpMap.get("relation_id").toString().split(",");
                        Map<String, Object> relationInfo = assetDataService.getAssetInfo(methodParam, relationIds[0]);
                        if (RegexUtil.optNotNull(relationInfo).isPresent()) {
                            position_code = relationInfo.get("position_code").toString();
                        }
                    }
                }
                Map<String, Object> oldPositionCodeMap = worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(methodParam.getSchemaName(), sub_work_code, "position_code");
                if (RegexUtil.optNotNull(oldPositionCodeMap).isPresent()) {
                    Map<String, Object> newPositionCodeMap = oldPositionCodeMap;
                    newPositionCodeMap.put("field_value", position_code);
                    worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), newPositionCodeMap);
                    logArr.add(LangUtil.doSetLogArray("工单详情字段属性更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_A, LangUtil.compareMap(oldPositionCodeMap, newPositionCodeMap).toString()}));
                }
                //判断是否为主工单 在此新增的工单都为子工单
                if (null != tmpMap && tmpMap.size() > 0) {
                    tmpMap.put("work_code", paramMap.get("work_code"));
                    if (keys.get("_sc_works_detail").startsWith(SqlConstant.IS_NEW_DATA)) {
                        Object is_main = tmpMap.get("is_main");
                        if (null == is_main || "".equals(is_main)) {
                            tmpMap.put("int@is_main", "is_main");
                            tmpMap.put("is_main", "1"); // 1：主工单，-1：子工单
                        }
                        //工单位置
                        tmpMap.put("varchar@position_code", "position_code");
                        tmpMap.put("position_code", position_code);
                        //标题
                        tmpMap.put("varchar@title", "title");
                        tmpMap.put("title", title);
                    }
                    paramMap.put("subWorkCode", tmpMap.get("sub_work_code")); // 明细表主键
                    paramMap.put("work_code", paramMap.get("work_code")); // 工单号
                    dataInfo.put("_sc_works_detail", tmpMap);
                }
            }
        } else {
            title = paramMap.get("title").toString();
        }
        //更新工单字段表的title字段的值
        Map<String, Object> oldTitleMap = worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(methodParam.getSchemaName(), sub_work_code, "title");
        if (RegexUtil.optNotNull(oldTitleMap).isPresent()) {
            Map<String, Object> newTitleMap = oldTitleMap;
            newTitleMap.put("field_value", title);
            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), newTitleMap);
            logArr.add(LangUtil.doSetLogArray("工单详情字段属性更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_A, LangUtil.compareMap(oldTitleMap, newTitleMap).toString()}));
        }
        tmpMap = dataInfo.get("_sc_works"); //工单概要表数据
        if (null != tmpMap && tmpMap.size() > 0) {
            if (keys.get("_sc_works").startsWith(SqlConstant.IS_NEW_DATA)) {
                //工单位置
                tmpMap.put("varchar@position_code", "position_code");
                tmpMap.put("position_code", position_code);
                //标题
                tmpMap.put("varchar@title", "title");
                tmpMap.put("title", title);
                tmpMap.put("varchar@create_user_id", "create_user_id");
                tmpMap.put("create_user_id", RegexUtil.optStrOrVal(paramMap.get("create_user_id"), methodParam.getUserId()));
                //如果有上报时间就不进行自动生成，没有的话自动生成  更新到工单详情字段表
                if (RegexUtil.optIsPresentStr(tmpMap.get("create_time"))) {
                    tmpMap.put("dateTime@create_time", "create_time");
                    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
                    String create_time = format.format(new Date());
                    tmpMap.put("create_time", create_time);
                    Map<String, Object> oldCreateTimeMap = worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(methodParam.getSchemaName(), sub_work_code, "create_time");
                    if (RegexUtil.optNotNull(oldCreateTimeMap).isPresent()) {
                        Map<String, Object> newCreateTimeMap = oldCreateTimeMap;
                        newCreateTimeMap.put("field_value", create_time);
                        worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), newCreateTimeMap);
                        logArr.add(LangUtil.doSetLogArray("工单详情字段属性更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_A, LangUtil.compareMap(oldCreateTimeMap, newCreateTimeMap).toString()}));
                    }
                }
                Map<String, Object> oldCreateUserIdMap = worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(methodParam.getSchemaName(), sub_work_code, "create_user_id");
                if (RegexUtil.optNotNull(oldCreateUserIdMap).isPresent()) {
                    Map<String, Object> newCreateUserIdMap = oldCreateUserIdMap;
                    newCreateUserIdMap.put("field_value", methodParam.getUserId());
                    worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), newCreateUserIdMap);
                    logArr.add(LangUtil.doSetLogArray("工单详情字段属性更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_A, LangUtil.compareMap(oldCreateUserIdMap, newCreateUserIdMap).toString()}));
                }
            } else {
                tmpMap.remove("dateTime@create_time");
                tmpMap.remove("varchar@create_user_id");
            }
            dataInfo.put("_sc_works", tmpMap);
        }
        //判断当前节点是否有指定分配人  如果没有则从paramMap中移除receive_user_id字段
        if (!workFlowNodeInfo.containsKey("receive_user_id")) {
            paramMap.remove("receive_user_id");
        }
        //该工单类型下业务类型的目标表集合  name为表明
        paramMap.put("business_type_table_List", business_type_table_List);
        paramMap.put("dataInfo", dataInfo);
        paramMap.put("WorksDetailColumnList", WorksDetailColumnList);
        paramMap.put("keys", keys); // 数据主键
        paramMap.put("flow_keys", flow_keys); // 要传入工作流的字段集合
        paramMap.put("title", title);
        return paramMap;
    }

    //备件销账
    private void bomRemove(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        boolean is_submit = false;
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");
        if ("1".equals(isNew)) { // 新建
            bom_works_tmp.put("dateTime@create_time", "create_time");
            bom_works_tmp.put("create_time", create_time);
            bom_works_tmp.put("varchar@create_user_id", "create_user_id");
            bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        if (is_submit) {
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(FlowParmsConstant.WORK_TYPE_ID)) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    //备件采购需求
    private void bomPurchasingDemand(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        boolean is_submit = false;
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");
        if ("1".equals(isNew)) { // 新建
            bom_works_tmp.put("dateTime@create_time", "create_time");
            bom_works_tmp.put("create_time", create_time);
            bom_works_tmp.put("varchar@create_user_id", "create_user_id");
            bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        if (is_submit) {
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            map_object.put("dataInfo", dataInfo);
            dataInfo.remove("_sc_bom_works_detail_item");
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get("pref_id").toString());

                if (RegexUtil.optIsPresentStr(map_object.get("next_user_id"))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get("next_user_id").toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put("title_page", title);
                if (RegexUtil.optIsPresentStr(map_object.get("duty_user_id"))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get("duty_user_id").toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("next_user_id"))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get("next_user_id").toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get("duty_user_id"))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get("duty_user_id").toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("next_user_id"))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get("next_user_id").toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    //设备验收
    private void assetAcceptance(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> asset_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works_detail");
        Map<String, Object> asset_works_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works");
        if ("1".equals(isNew)) { // 新建
            asset_works_tmp.put("dateTime@create_time", "create_time");
            asset_works_tmp.put("create_time", create_time);
            asset_works_tmp.put("varchar@create_user_id", "create_user_id");
            asset_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        asset_works_detail_tmp.put("int@is_main", "is_main");
        asset_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) asset_works_detail_tmp.get("sub_work_code");
        String work_code = (String) asset_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        dataInfo.put("_sc_asset_works_detail", asset_works_detail_tmp);
        dataInfo.put("_sc_asset_works", asset_works_tmp);
        map_object.put("dataInfo", dataInfo);
        saveWorkData(methodParam, map_object, logger);//保存新建工单
        Map<String, String> flow_map = new HashMap<>();
        if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
            List<String> flow_keys = (List<String>) map_object.get("flow_keys");
            for (String flow_key : flow_keys
            ) {
                if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                    flow_map.put(flow_key, map_object.get(flow_key).toString());
                }
            }
        }
        if ("1".equals(isNew)) {//新建工单  创建工作流
            String date = format.format(new Date());

            flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
            flow_map.put(FlowParmsConstant.CREATE_TIME, date);
            flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
            flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
            flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
            flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
            flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
            flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
            flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
            flow_map.put(FlowParmsConstant.STATUS, status.toString());
            flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
            flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
            flow_map.put(FlowParmsConstant.PDEID, flow_code);
            flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.ATTACHFILE))) {
                flow_map.put(FlowParmsConstant.ATTACHFILE, map_object.get(FlowParmsConstant.ATTACHFILE).toString());
            }
            flow_map.put(FlowParmsConstant.POSITION_CODE, asset_works_tmp.get(FlowParmsConstant.POSITION_CODE).toString());
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
            } else {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.OPERATION_TYPE))) {
                flow_map.put(FlowParmsConstant.OPERATION_TYPE, map_object.get(FlowParmsConstant.OPERATION_TYPE).toString());
            } else {
                flow_map.put(FlowParmsConstant.OPERATION_TYPE, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
            } else {
                flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
            }
            //新建流程实例
            workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
        } else {
            task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
            //将流程推动到下一节点
            flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
            flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
            flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
            flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
            flow_map.put(FlowParmsConstant.STATUS, status.toString());
            flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
            flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
            flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.ATTACHFILE))) {
                flow_map.put(FlowParmsConstant.ATTACHFILE, map_object.get(FlowParmsConstant.ATTACHFILE).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.OPERATION_TYPE))) {
                flow_map.put(FlowParmsConstant.OPERATION_TYPE, map_object.get(FlowParmsConstant.OPERATION_TYPE).toString());
            } else {
                flow_map.put(FlowParmsConstant.OPERATION_TYPE, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
            } else {
                flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
            } else {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
            }
            //推进流程实例
            workflowService.complete(methodParam, task_id, flow_map);
        }
    }

    //设备调拨
    private void assetChange(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> asset_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works_detail");
        Map<String, Object> asset_works_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works");
        if ("1".equals(isNew)) { // 新建
            asset_works_tmp.put("dateTime@create_time", "create_time");
            asset_works_tmp.put("create_time", create_time);
            asset_works_tmp.put("varchar@create_user_id", "create_user_id");
            asset_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        asset_works_detail_tmp.put("int@is_main", "is_main");
        asset_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) asset_works_detail_tmp.get("sub_work_code");
        String work_code = (String) asset_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());

        dataInfo.put("_sc_asset_works_detail", asset_works_detail_tmp);
        dataInfo.put("_sc_asset_works", asset_works_tmp);
        dataInfo.remove("_sc_asset_works_detail_item");
        map_object.put("dataInfo", dataInfo);
        saveWorkData(methodParam, map_object, logger);//保存新建工单
        Map<String, String> flow_map = new HashMap<>();
        if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
            List<String> flow_keys = (List<String>) map_object.get("flow_keys");
            for (String flow_key : flow_keys
            ) {
                if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                    flow_map.put(flow_key, map_object.get(flow_key).toString());
                }
            }
        }
        if ("1".equals(isNew)) {//新建工单  创建工作流
            String date = format.format(new Date());
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEW_POSITION_CODE))) {//设备调往地址
                flow_map.put(FlowParmsConstant.NEW_POSITION_CODE, map_object.get(FlowParmsConstant.NEW_POSITION_CODE).toString());
            }
            flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
            flow_map.put(FlowParmsConstant.CREATE_TIME, date);
            flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
            flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
            flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
            flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
            flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
            flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
            flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
            flow_map.put(FlowParmsConstant.STATUS, status.toString());
            flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
            flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
            flow_map.put(FlowParmsConstant.PDEID, flow_code);
            flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
            flow_map.put(FlowParmsConstant.POSITION_CODE, asset_works_tmp.get(FlowParmsConstant.POSITION_CODE).toString());
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
            } else {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
            } else {
                flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
            }
            //新建流程实例
            workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
        } else {
            task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
            //将流程推动到下一节点
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEW_POSITION_CODE))) {//设备调往地址
                flow_map.put(FlowParmsConstant.NEW_POSITION_CODE, map_object.get(FlowParmsConstant.NEW_POSITION_CODE).toString());
            }
            flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
            flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
            flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
            flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
            flow_map.put(FlowParmsConstant.STATUS, status.toString());
            flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
            flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
            flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
            } else {
                flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
            } else {
                flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
            }
            if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
            }
            //推进流程实例
            workflowService.complete(methodParam, task_id, flow_map);
        }
    }

    //设备报废
    private void assetScrapped(MethodParam methodParam, Map<String, Object> map_object, JSONArray logArr) {

        String now = new SimpleDateFormat(Constants.DATE_FMT_SS).format(new Date());
        String businessTypeId = RegexUtil.optStrOrBlank(map_object.get("business_type_id"));

        Integer doFlowKey = RegexUtil.optIntegerOrNull(map_object.get("do_flow_key"), LangConstant.TITLE_AAAAB_I);
        String flowCode = RegexUtil.optStrOrBlank(map_object.get("flow_code"));
        Map<String, Object> dataInfo = DataChangeUtil.scdToMap(map_object.get("dataInfo"));
        Map<String, Object> detailTmp = DataChangeUtil.scdToMap(dataInfo.get("_sc_asset_works_detail"));
        Map<String, Object> worksTmp = DataChangeUtil.scdToMap(dataInfo.get("_sc_asset_works"));
        dataInfo.remove("_sc_asset_works_detail_item");
        detailTmp.put("int@is_main", "is_main");
        detailTmp.put("is_main", 1);
        String sub_work_code = RegexUtil.optStrOrBlank(detailTmp.get("sub_work_code"));
        String work_code = RegexUtil.optStrOrBlank(detailTmp.get("work_code"));
        Map<String, Object> columnKeyValueData = getWorksDetailColumnKeyValue(methodParam, sub_work_code);
        //对设备列表字段属性进行二次解析
        if (RegexUtil.optIsPresentMap(columnKeyValueData) && RegexUtil.optIsPresentStr(columnKeyValueData.get("asset_list"))) {
            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(map_object.get("asset_list"));
            columnKeyValueData.put("asset_list", list);
        }
        map_object.put("worksDetailColumnKey", columnKeyValueData);
        logService.newLog(methodParam, businessTypeId, sub_work_code, logArr.toString());
        doAssetScrappedWork(methodParam, map_object, worksTmp, flowCode, work_code, sub_work_code, now, doFlowKey, logArr);
    }

    private void doAssetScrappedWork(MethodParam methodParam, Map<String, Object> map_object, Map<String, Object> worksTmp, String flowCode, String work_code, String sub_work_code, String now, Integer doFlowKey, JSONArray logArr) {
        // 新建处理
        String isNew = RegexUtil.optStrOrBlank(map_object.get("isNew"));
        Map<String, Object> worksDetailColumnKey = DataChangeUtil.scdToMap(map_object.get("worksDetailColumnKey"));
        List<Map<String, Object>> assetList = DataChangeUtil.scdObjToList(worksDetailColumnKey.get("asset_list"));
        if ("1".equals(isNew)) {
            worksTmp.put("dateTime@create_time", "create_time");
            worksTmp.put("create_time", now);
            worksTmp.put("varchar@create_user_id", "create_user_id");
            worksTmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
//        // 设备报废流程存储业务时，设备列表有多条，需单独处理
//        ((Map<?, ?>) map_object.get("dataInfo")).remove("_sc_asset_works_detail_item");
//        if (assetList != null) {
//            assetWorksMapper.batchInsertAssetWorksDetailItem(methodParam.getSchemaName(), assetList);
//            //日志新增记录
//            logArr.add(LangUtil.doSetLogArray("新增_sc_asset_works_detail_item表记录", LangConstant.TAB_DEVICE_B, new String[]{assetList.toString()}));
//        }
        saveWorkData(methodParam, map_object, logArr); // 存储业务表
        // 工作流处理
        Map<String, String> flow_map = new HashMap<>();
        List<String> flowKeys = DataChangeUtil.scdObjToListStrOrNew(map_object.get("flow_keys"));
        String[] keys = new String[]{FlowParmsConstant.DO_FLOW_KEY, FlowParmsConstant.BUSINESS_TYPE_ID, FlowParmsConstant.WORK_TYPE_ID, FlowParmsConstant.DO_BRANCH_KEY, FlowParmsConstant.PREF_ID,
                FlowParmsConstant.DEADLINE_TIME, FlowParmsConstant.STATUS, FlowParmsConstant.SUB_WORK_CODE, FlowParmsConstant.WORK_CODE, FlowParmsConstant.POSITION_CODE};
        flowKeys.addAll(DataChangeUtil.scdArrayToListStr(keys));
        for (String flow_key : flowKeys) {
            RegexUtil.optNotBlankStrOpt(map_object.get(flow_key)).ifPresent(fk -> flow_map.put(flow_key, fk));
        }
        flow_map.put(FlowParmsConstant.FLOW_DATA, null);
        flow_map.put(FlowParmsConstant.ASSET_LIST, JSON.toJSONString(assetList));
        flow_map.put(FlowParmsConstant.CREATE_TIME, now);
        flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
        flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
        flow_map.put(FlowParmsConstant.TITLE_PAGE, RegexUtil.optStrOrBlank(map_object.get("title")));
        flow_map.put(FlowParmsConstant.PDEID, flowCode);
        flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, RegexUtil.optStrOrVal(map_object.get(FlowParmsConstant.PRIORITY_LEVEL), "1")); // 默认紧急程度为1  低
        // 新建流程实例
        if ("1".equals(isNew)) {
            workflowService.startWithForm(methodParam, flowCode, methodParam.getUserId(), flow_map);
        } else {
            String task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
            workflowService.complete(methodParam, task_id, flow_map); // 推进流程实例
        }
    }

    //设备盘点
    private void assetInventory(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        String main_sub_work_code = null;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> asset_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works_detail");
        Map<String, Object> asset_works_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works");
        //设备位置为空字符串
        asset_works_tmp.put("position_code", " ");
        asset_works_detail_tmp.put("int@is_main", "is_main");
        asset_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) asset_works_detail_tmp.get("sub_work_code");
        String work_code = (String) asset_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        List<FlowData> flowDataList = new ArrayList<>();
        List<Map<String, Object>> asset_list = (List<Map<String, Object>>) map_object.get("inventory_table");
        List<Map<String, Object>> asset_item_list = RegexUtil.optNotNullListOrNew(map_object.get("asset_list"));
        Map<String, Object> asset_sub_work_detail = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optIsPresentStr(asset_sub_work_detail) && Integer.valueOf(asset_sub_work_detail.get("is_main").toString()) == -1) {
            //该工单为子工单
            asset_works_detail_tmp.put("int@is_main", "is_main");
            asset_works_detail_tmp.put("is_main", "-1"); // 1：主工单，-1：子工单
            //查询出该子工单的主工单
            Map<String, Object> main_sub_work_detail = assetWorksMapper.findMainAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            main_sub_work_code = main_sub_work_detail.get("sub_work_code").toString();
            map_object.put("main_sub_work_code", main_sub_work_code);
            //查询出该工单所有的子工单
            List<Map<String, Object>> sub_work_detail_List = assetWorksMapper.findSubAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            //如果有一个工单为退回  那么其他所有的为完成子工单都走退回流程
            if (do_flow_key == ButtonConstant.BTN_RETURN) {
                for (Map<String, Object> sub_work_detail : sub_work_detail_List) {
                    sub_work_code = sub_work_detail.get("sub_work_code").toString();
                    if (Integer.valueOf(sub_work_detail.get("status").toString()) != StatusConstant.COMPLETED && Integer.valueOf(sub_work_detail.get("status").toString()) != 110) {
                        sub_work_detail.put("status", StatusConstant.COMPLETED_CLOSE);//关闭
                        assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), sub_work_detail);
                        Map<String, String> flow_map = new HashMap<>();
                        task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                        String sub_token = methodParam.getToken() + sub_work_code;
                        cacheUtilService.getSubToken(methodParam, sub_token);
                        //将流程推动到下一节点
                        flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                        flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                        flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                        flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                        flow_map.put(FlowParmsConstant.STATUS, status.toString());
                        flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                        flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                        flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                            flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                            flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                            flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                        }
                        flow_map.put("main_sub_work_code", main_sub_work_code);
                        //推进流程实例
                        workflowService.subComplete(methodParam, task_id, flow_map);
                    }
                }
            } else {
                List<Map<String, Object>> handleAssetList = selectOptionsMapper.findAssetListByPositionWithInventory(methodParam.getSchemaName(), RegexUtil.optStrOrNull(map_object.get("relation_id")), RegexUtil.optStrOrNull(map_object.get("category_id")), null);
                List<Map<String, Object>> assetListNow = new ArrayList<>();
                assetListNow.addAll(handleAssetList);
                for (Map<String, Object> asset_item : asset_item_list) {
                    for (Map<String, Object> handleTmp : handleAssetList) {
                        if (Objects.equals(RegexUtil.optStrOrBlank(handleTmp.get("asset_id")), RegexUtil.optStrOrBlank(asset_item.get("asset_id")))) {
                            assetListNow.remove(handleTmp);
                            break;
                        }
                    }
                }

                if (RegexUtil.optIsPresentList(assetListNow)) {
                    Map<String, Object> asset_id_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "asset_id");
                    Map<String, Object> asset_code_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "asset_code");
                    Map<String, Object> asset_name_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "asset_name");
                    Map<String, Object> category_id_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "category_id");
                    Map<String, Object> relation_id_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "relation_id");
                    Map<String, Object> profit_and_loss_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "profit_and_loss");
                    assetListNow.forEach(e -> {
                        e.remove("asset_icon");
                        e.remove("relation");
                        e.remove("position_code");
                        e.remove("value");
                        e.remove("text");
                        if (e.containsKey("asset_id")) {
                            e.put(RegexUtil.optStrOrBlank(asset_id_map.get("field_form_code")), e.get("asset_id"));
                            e.remove("asset_id");
                        }
                        if (e.containsKey("asset_code")) {
                            e.put(RegexUtil.optStrOrBlank(asset_code_map.get("field_form_code")), e.get("asset_code"));
                            e.remove("asset_code");
                        }
                        if (e.containsKey("asset_name")) {
                            e.put(RegexUtil.optStrOrBlank(asset_name_map.get("field_form_code")), e.get("asset_name"));
                            e.remove("asset_name");
                        }
                        if (e.containsKey("category_id")) {
                            e.put(RegexUtil.optStrOrBlank(category_id_map.get("field_form_code")), e.get("category_id"));
                            e.remove("category_id");
                        }
                        if (e.containsKey("relation_id")) {
                            e.put(RegexUtil.optStrOrBlank(relation_id_map.get("field_form_code")), e.get("relation_id"));
                            e.remove("relation_id");
                        }
                        if (e.containsKey("profit_and_loss")) {
                            e.put(RegexUtil.optStrOrBlank(profit_and_loss_map.get("field_form_code")), "-1");
                            e.remove("profit_and_loss");
                        }
                    });

                    List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
                    for (Map<String, Object> tmp : hadlList) {
                        String field_code = tmp.get("field_code").toString();
                        String field_value = null;
                        if ("asset_list".equals(field_code)) {
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(tmp.get("field_value"));
                            for (int i = 0; i < list.size(); i++) {
                                if (Objects.equals(list.get(i), null)) {
                                    list.remove(i);
                                }
                            }
                            list.addAll(assetListNow);
                            field_value = list.toString();
                            tmp.put("sub_work_code", sub_work_code);
                            tmp.put("field_value", field_value);
                            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                            break;
                        }
                    }
                }

                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                String sub_token = methodParam.getToken() + sub_work_code;
                cacheUtilService.getSubToken(methodParam, sub_token);
                asset_sub_work_detail.put("status", StatusConstant.COMPLETED);
                assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), asset_sub_work_detail);
                Map<String, String> flow_map = new HashMap<>();
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.subComplete(methodParam, task_id, flow_map);
            }
        } else {
            String subWorkCode = null;
            List<Map<String, Object>> subList = new ArrayList<>();
            if (do_flow_key == ButtonConstant.BTN_SUBMIT) {
                int i = 0;
                for (Map<String, Object> asset : asset_list) {
                    if (RegexUtil.optIsPresentStr(asset_works_tmp.get("position_code"))) {
                        asset_works_tmp.put("position_code", asset_works_tmp.get("position_code").toString() + "," + asset.get("position_code"));
                    } else {
                        asset_works_tmp.put("position_code", asset.get("position_code"));
                    }
                    Map<String, Object> tmpMap = new HashMap<>();
                    subWorkCode = dynamicCommonService.getSubAssetInventoryCode(methodParam);
                    tmpMap.put("varchar@pref_id", "pref_id");
                    tmpMap.put("pref_id", map_object.get("pref_id"));
                    tmpMap.put("varchar@sub_work_code", "sub_work_code");
                    tmpMap.put("varchar@work_code", "work_code");
                    tmpMap.put("work_code", work_code);
                    tmpMap.put("int@status", "status");
                    tmpMap.put("status", status);
                    tmpMap.put("sub_work_code", subWorkCode);
                    tmpMap.put("varchar@asset_id", "asset_id");
                    tmpMap.put("asset_id", asset.get("asset_id"));
                    tmpMap.put("varchar@relation_id", "relation_id");
                    tmpMap.put("relation_id", asset.get("relation_id"));
                    tmpMap.put("int@is_main", "is_main");
                    tmpMap.put("is_main", "-1"); // 1：主工单，-1：子工单
                    worksMapper.insert(methodParam.getSchemaName(), tmpMap, "_sc_asset_works_detail"); // 新增
                    // 流程数据
                    FlowData fd = new FlowData();
                    if (RegexUtil.optIsPresentStr(asset.get("inventory_user_id"))) {
                        fd.setReceive_user_id(asset.get("inventory_user_id").toString());
                    } else {
                        throw new SenscloudException("请选择对应的盘点人员");
                    }
                    fd.setName((String) tmpMap.get("title")); // title
                    fd.setSub_work_code(subWorkCode); // 主键
                    fd.setOrder(-1); // 并行
                    fd.setFacility_id("1"); // 所属位置
                    fd.setDeadline_time(RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME))); // 截止时间
                    fd.setCreate_time(now.toString()); // 发生时间
                    fd.setPref_id(map_object.get(FlowParmsConstant.PREF_ID).toString());
                    if (RegexUtil.optIsPresentStr(map_object.get("priority_level"))) {
                        fd.setPriority_level(map_object.get("priority_level").toString());
                    } else {
                        fd.setPriority_level("1");
                    }
                    fd.setTitle_page(title);
                    String sub_token = methodParam.getToken() + fd.getSub_work_code();
                    cacheUtilService.getSubToken(methodParam, sub_token);
                    fd.setMain_sub_work_code(map_object.get("sub_work_code").toString());
                    fd.setSub_token(sub_token);
                    fd.setWork_type_id(work_type_id);
                    fd.setBusiness_type_id(businessTypeId);
                    fd.setWork_code(work_code);
                    fd.setNode_id(map_object.get("node_id").toString());
                    flowDataList.add(fd);
                    List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) map_object.get("work_flow_node_column_list");
                    for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                        String field_form_code = work_flow_node_column.get("field_form_code").toString();
                        String field_code = work_flow_node_column.get("field_code").toString();
                        String field_value = RegexUtil.optStrOrNull(map_object.get(field_code));
                        if ("asset_id".equals(field_code)) {
                            field_value = RegexUtil.optStrOrBlank(asset.get("asset_id"));
                        } else if ("inventory_table".equals(field_code)) {
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(map_object.get(field_form_code));
                            List<Map<String, Object>> array = new ArrayList<>();
                            Map<String, Object> inv_count_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "inv_count");
                            Map<String, Object> sub_work_code_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "sub_work_code");
                            Map<String, Object> status_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "status");
                            Map<String, Object> addMap = list.get(i);
                            addMap.put(RegexUtil.optStrOrBlank(inv_count_map.get("field_form_code")), 0);
                            addMap.put(RegexUtil.optStrOrBlank(sub_work_code_map.get("field_form_code")), subWorkCode);
                            addMap.put(RegexUtil.optStrOrBlank(status_map.get("field_form_code")), status);
                            subList.add(addMap);
                            array.add(list.get(i));
                            field_value = array.toString();
                            i++;
                        }
                        Map<String, Object> worksDetailColumn = new HashMap<>();
                        List<String> flow_keys = new ArrayList<>();
                        Map<String, Object> workFlowNodeInfo = (Map<String, Object>) map_object.get("workFlowNodeInfo");
                        workFlowNodeInfo.put("sub_work_code", subWorkCode);
                        //根据字段主键和工单详情编号获取以前字段信息 如果没有则新增

                        updateWorksDetailColumnByFormCode(methodParam, map_object, subWorkCode, field_form_code, field_value, workFlowNodeInfo, worksDetailColumn, flow_keys, logger);
                    }

                    List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), subWorkCode);
                    for (Map<String, Object> tmp : hadlList) {
                        String field_code = tmp.get("field_code").toString();
                        String field_value = null;
                        if ("category_id".equals(field_code)) {
                            field_value = RegexUtil.optStrOrNull(asset.get("category_id"));
                            tmp.put("sub_work_code", subWorkCode);
                            tmp.put("field_value", field_value);
                            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                            break;
                        }
                    }
                }
            }
            List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
            for (Map<String, Object> tmp : hadlList) {
                String field_code = tmp.get("field_code").toString();
                String field_value = null;
                if ("inventory_table".equals(field_code)) {
                    field_value = subList.toString();
                    tmp.put("sub_work_code", sub_work_code);
                    tmp.put("field_value", field_value);
                    worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                    break;
                }
            }
            if ("1".equals(isNew)) { // 新建
                asset_works_tmp.put("dateTime@create_time", "create_time");
                asset_works_tmp.put("create_time", create_time);
                asset_works_tmp.put("varchar@create_user_id", "create_user_id");
                asset_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
            }
            if (RegexUtil.optIsPresentStr(asset_works_tmp.get("position_code"))) {
                asset_works_tmp.put("varchar@position_code", "position_code");
            }
            dataInfo.put("_sc_asset_works_detail", asset_works_detail_tmp);
            dataInfo.put("_sc_asset_works", asset_works_tmp);
            dataInfo.remove("_sc_asset_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }

        }
    }

    //设备出库(设备借出)
    private void assetOutPut(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        String main_sub_work_code = null;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> asset_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works_detail");
        Map<String, Object> asset_works_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works");
        //设备位置为空字符串
        asset_works_tmp.put("position_code", " ");
        asset_works_detail_tmp.put("int@is_main", "is_main");
        asset_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) asset_works_detail_tmp.get("sub_work_code");
        String work_code = (String) asset_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        List<FlowData> flowDataList = new ArrayList<>();
        List<Map<String, Object>> asset_list = (List<Map<String, Object>>) map_object.get("asset_use");
        Map<String, Object> asset_sub_work_detail = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optIsPresentStr(asset_sub_work_detail) && Integer.valueOf(asset_sub_work_detail.get("is_main").toString()) == -1) {
            //该工单为子工单
            asset_works_detail_tmp.put("int@is_main", "is_main");
            asset_works_detail_tmp.put("is_main", "-1"); // 1：主工单，-1：子工单
            //查询出该子工单的主工单
            Map<String, Object> main_sub_work_detail = assetWorksMapper.findMainAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            main_sub_work_code = main_sub_work_detail.get("sub_work_code").toString();
            map_object.put("main_sub_work_code", main_sub_work_code);
            //查询出该工单所有的子工单
            List<Map<String, Object>> sub_work_detail_List = assetWorksMapper.findSubAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            //如果有一个工单为退回  那么其他所有的为完成子工单都走退回流程
            if (do_flow_key == ButtonConstant.BTN_RETURN) {
                for (Map<String, Object> sub_work_detail : sub_work_detail_List) {
                    sub_work_code = sub_work_detail.get("sub_work_code").toString();
                    if (Integer.valueOf(sub_work_detail.get("status").toString()) != StatusConstant.COMPLETED && Integer.valueOf(sub_work_detail.get("status").toString()) != 110) {
                        sub_work_detail.put("status", StatusConstant.COMPLETED_CLOSE);//关闭
                        assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), sub_work_detail);
                        Map<String, String> flow_map = new HashMap<>();
                        task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                        String sub_token = methodParam.getToken() + sub_work_code;
                        cacheUtilService.getSubToken(methodParam, sub_token);
                        //将流程推动到下一节点
                        flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                        flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                        flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                        flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                        flow_map.put(FlowParmsConstant.STATUS, status.toString());
                        flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                        flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                        flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                            flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                            flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                            flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                        }
                        flow_map.put("main_sub_work_code", main_sub_work_code);
                        //推进流程实例
                        workflowService.subComplete(methodParam, task_id, flow_map);
                    }
                }
            } else {
                String sub_token = methodParam.getToken() + sub_work_code;
                cacheUtilService.getSubToken(methodParam, sub_token);
                asset_sub_work_detail.put("status", StatusConstant.COMPLETED);
                assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), asset_sub_work_detail);
                Map<String, String> flow_map = new HashMap<>();
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.subComplete(methodParam, task_id, flow_map);
            }
        } else {
            String subWorkCode = null;
            if (do_flow_key == ButtonConstant.BTN_SUBMIT) {
                int i = 0;
                for (Map<String, Object> asset : asset_list) {
                    Map<String, Object> tmpMap = new HashMap<>();
                    subWorkCode = dynamicCommonService.getAssetSubWorkCode(methodParam);
                    tmpMap.put("varchar@pref_id", "pref_id");
                    tmpMap.put("pref_id", map_object.get("pref_id"));
                    tmpMap.put("varchar@sub_work_code", "sub_work_code");
                    tmpMap.put("varchar@work_code", "work_code");
                    tmpMap.put("work_code", work_code);
                    tmpMap.put("sub_work_code", subWorkCode);
                    tmpMap.put("varchar@asset_id", "relation_id");
                    tmpMap.put("asset_id", asset.get("relation_id"));
                    tmpMap.put("int@is_main", "is_main");
                    tmpMap.put("is_main", "-1"); // 1：主工单，-1：子工单
                    worksMapper.insert(methodParam.getSchemaName(), tmpMap, "_sc_asset_works_detail"); // 新增
                    // 流程数据
                    FlowData fd = new FlowData();
                    fd.setName((String) tmpMap.get("title")); // title
                    fd.setSub_work_code(subWorkCode); // 主键
                    fd.setOrder(-1); // 并行
                    fd.setFacility_id("1"); // 所属位置
                    fd.setDeadline_time(RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME))); // 截止时间
                    fd.setCreate_time(now.toString()); // 发生时间
                    fd.setPref_id(map_object.get(FlowParmsConstant.PREF_ID).toString());
                    if (RegexUtil.optIsPresentStr(map_object.get("priority_level"))) {
                        fd.setPriority_level(map_object.get("priority_level").toString());
                    } else {
                        fd.setPriority_level("1");
                    }
                    fd.setTitle_page(title);
                    String sub_token = methodParam.getToken() + fd.getSub_work_code();
                    cacheUtilService.getSubToken(methodParam, sub_token);
                    fd.setMain_sub_work_code(map_object.get("sub_work_code").toString());
                    fd.setSub_token(sub_token);
                    fd.setWork_type_id(work_type_id);
                    fd.setBusiness_type_id(businessTypeId);
                    fd.setWork_code(work_code);
                    fd.setNode_id(map_object.get("node_id").toString());
                    flowDataList.add(fd);
                    List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) map_object.get("work_flow_node_column_list");
                    for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                        String field_form_code = work_flow_node_column.get("field_form_code").toString();
                        String field_code = work_flow_node_column.get("field_code").toString();
                        String field_value = null;
                        if (RegexUtil.optIsPresentStr(map_object.get(field_code))) {
                            field_value = map_object.get(field_code).toString();
                        } else {
                            field_value = null;
                        }
                        if ("asset_use".equals(field_code)) {
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(map_object.get(field_form_code));
                            List<Map<String, Object>> array = new ArrayList<>();
                            array.add(list.get(i));
                            field_value = array.toString();
                            i++;
                        }
                        Map<String, Object> worksDetailColumn = new HashMap<>();
                        List<String> flow_keys = new ArrayList<>();
                        Map<String, Object> workFlowNodeInfo = (Map<String, Object>) map_object.get("workFlowNodeInfo");
                        workFlowNodeInfo.put("sub_work_code", subWorkCode);
                        //根据字段主键和工单详情编号获取以前字段信息 如果没有则新增

                        updateWorksDetailColumnByFormCode(methodParam, map_object, subWorkCode, field_form_code, field_value, workFlowNodeInfo, worksDetailColumn, flow_keys, logger);
                    }
                }
            }
            if ("1".equals(isNew)) { // 新建
                asset_works_tmp.put("dateTime@create_time", "create_time");
                asset_works_tmp.put("create_time", create_time);
                asset_works_tmp.put("varchar@create_user_id", "create_user_id");
                asset_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
            }
            dataInfo.put("_sc_asset_works_detail", asset_works_detail_tmp);
            dataInfo.put("_sc_asset_works", asset_works_tmp);
            dataInfo.remove("_sc_asset_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }

        }
    }

    //设备入库(设备归还)
    private void assetInput(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        String main_sub_work_code = null;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> asset_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works_detail");
        Map<String, Object> asset_works_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_works");
        //设备位置为空字符串
        asset_works_tmp.put("position_code", " ");
        asset_works_detail_tmp.put("int@is_main", "is_main");
        asset_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) asset_works_detail_tmp.get("sub_work_code");
        String work_code = (String) asset_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        List<FlowData> flowDataList = new ArrayList<>();
        List<Map<String, Object>> asset_list = (List<Map<String, Object>>) map_object.get("asset_use");
        Map<String, Object> asset_sub_work_detail = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optNotNull(asset_sub_work_detail).isPresent() && Integer.valueOf(asset_sub_work_detail.get("is_main").toString()) == -1) {
            //该工单为子工单
            asset_works_detail_tmp.put("int@is_main", "is_main");
            asset_works_detail_tmp.put("is_main", "-1"); // 1：主工单，-1：子工单
            //查询出该子工单的主工单
            Map<String, Object> main_sub_work_detail = assetWorksMapper.findMainAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            main_sub_work_code = main_sub_work_detail.get("sub_work_code").toString();
            map_object.put("main_sub_work_code", main_sub_work_code);
            //查询出该工单所有的子工单
            List<Map<String, Object>> sub_work_detail_List = assetWorksMapper.findSubAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            //如果有一个工单为退回  那么其他所有的为完成子工单都走退回流程
            if (do_flow_key == 5) {
                for (Map<String, Object> sub_work_detail : sub_work_detail_List) {
                    sub_work_code = sub_work_detail.get("sub_work_code").toString();
                    if (Integer.valueOf(sub_work_detail.get("status").toString()) != StatusConstant.COMPLETED && Integer.valueOf(sub_work_detail.get("status").toString()) != 110) {
                        sub_work_detail.put("status", StatusConstant.COMPLETED_CLOSE);//关闭
                        assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), sub_work_detail);
                        Map<String, String> flow_map = new HashMap<>();
                        task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                        String sub_token = methodParam.getToken() + sub_work_code;
                        cacheUtilService.getSubToken(methodParam, sub_token);
                        //将流程推动到下一节点
                        flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                        flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                        flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                        flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                        flow_map.put(FlowParmsConstant.STATUS, status.toString());
                        flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                        flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                        flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                            flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                            flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                            flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                        }
                        flow_map.put("main_sub_work_code", main_sub_work_code);
                        //推进流程实例
                        workflowService.subComplete(methodParam, task_id, flow_map);
                    }
                }
            } else {
                String sub_token = methodParam.getToken() + sub_work_code;
                cacheUtilService.getSubToken(methodParam, sub_token);
                asset_sub_work_detail.put("status", StatusConstant.COMPLETED);
                assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), asset_sub_work_detail);
                Map<String, String> flow_map = new HashMap<>();
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.subComplete(methodParam, task_id, flow_map);
            }
        } else {
            String subWorkCode = null;
            if (do_flow_key == 2) {
                int i = 0;
                for (Map<String, Object> asset : asset_list) {
                    Map<String, Object> tmpMap = new HashMap<>();
                    subWorkCode = dynamicCommonService.getAssetSubWorkCode(methodParam);
                    tmpMap.put("varchar@pref_id", "pref_id");
                    tmpMap.put("pref_id", map_object.get("pref_id"));
                    tmpMap.put("varchar@sub_work_code", "sub_work_code");
                    tmpMap.put("varchar@work_code", "work_code");
                    tmpMap.put("work_code", work_code);
                    tmpMap.put("sub_work_code", subWorkCode);
                    tmpMap.put("varchar@asset_id", "relation_id");
                    tmpMap.put("asset_id", asset.get("relation_id"));
                    tmpMap.put("int@is_main", "is_main");
                    tmpMap.put("is_main", "-1"); // 1：主工单，-1：子工单
                    worksMapper.insert(methodParam.getSchemaName(), tmpMap, "_sc_asset_works_detail"); // 新增
                    // 流程数据
                    FlowData fd = new FlowData();
                    fd.setName((String) tmpMap.get("title")); // title
                    fd.setSub_work_code(subWorkCode); // 主键
                    fd.setOrder(-1); // 并行
                    fd.setFacility_id("1"); // 所属位置
                    fd.setDeadline_time(RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME))); // 截止时间
                    fd.setCreate_time(now.toString()); // 发生时间
                    fd.setPref_id(map_object.get(FlowParmsConstant.PREF_ID).toString());
                    String sub_token = methodParam.getToken() + fd.getSub_work_code();
                    cacheUtilService.getSubToken(methodParam, sub_token);
                    fd.setMain_sub_work_code(map_object.get("sub_work_code").toString());
                    fd.setSub_token(sub_token);
                    if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                        fd.setPriority_level(map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                    } else {
                        fd.setPriority_level("1");
                    }
                    fd.setTitle_page(title);
                    fd.setWork_type_id(work_type_id);
                    fd.setBusiness_type_id(businessTypeId);
                    fd.setNode_id(map_object.get("node_id").toString());
                    flowDataList.add(fd);
                    List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) map_object.get("work_flow_node_column_list");
                    for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                        String field_form_code = work_flow_node_column.get("field_form_code").toString();
                        String field_code = work_flow_node_column.get("field_code").toString();
                        String field_value = null;
                        if (RegexUtil.optIsPresentStr(map_object.get(field_code))) {
                            field_value = map_object.get(field_code).toString();
                        } else {
                            field_value = null;
                        }
                        if ("asset_use".equals(field_code)) {
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(map_object.get(field_form_code));
                            List<Map<String, Object>> array = new ArrayList<>();
                            array.add(list.get(i));
                            field_value = array.toString();
                            i++;
                        }
                        Map<String, Object> worksDetailColumn = new HashMap<>();
                        List<String> flow_keys = new ArrayList<>();
                        Map<String, Object> workFlowNodeInfo = (Map<String, Object>) map_object.get("workFlowNodeInfo");
                        workFlowNodeInfo.put("sub_work_code", subWorkCode);
                        map_object.put("isMain", "-1");
                        //根据字段主键和工单详情编号获取以前字段信息 如果没有则新增
                        updateWorksDetailColumnByFormCode(methodParam, map_object, subWorkCode, field_form_code, field_value, workFlowNodeInfo, worksDetailColumn, flow_keys, logger);
                    }
                }
            }
            if ("1".equals(isNew)) { // 新建
                asset_works_tmp.put("dateTime@create_time", "create_time");
                asset_works_tmp.put("create_time", create_time);
                asset_works_tmp.put("varchar@create_user_id", "create_user_id");
                asset_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
            }
            dataInfo.put("_sc_asset_works_detail", asset_works_detail_tmp);
            dataInfo.put("_sc_asset_works", asset_works_tmp);
            dataInfo.remove("_sc_asset_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    //备件领用
    private void bomCollect(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        boolean is_submit = false;
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");
        if ("1".equals(isNew)) { // 新建
            bom_works_tmp.put("dateTime@create_time", "create_time");
            bom_works_tmp.put("create_time", create_time);
            bom_works_tmp.put("varchar@create_user_id", "create_user_id");
            bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        bom_works_detail_tmp.put("int@bom_in_out", "bom_in_out");
        bom_works_detail_tmp.put("bom_in_out", 3);

        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        if (is_submit) {
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    //备件报废
    private void bomScrapped(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        boolean is_submit = false;
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");
        if ("1".equals(isNew)) { // 新建
            bom_works_tmp.put("dateTime@create_time", "create_time");
            bom_works_tmp.put("create_time", create_time);
            bom_works_tmp.put("varchar@create_user_id", "create_user_id");
            bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        bom_works_detail_tmp.put("int@bom_in_out", "bom_in_out");
        bom_works_detail_tmp.put("bom_in_out", 2);

        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        if (is_submit) {
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    //备件调拨
    private void bomChange(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        boolean is_submit = false;
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");
        if ("1".equals(isNew)) { // 新建
            bom_works_tmp.put("dateTime@create_time", "create_time");
            bom_works_tmp.put("create_time", create_time);
            bom_works_tmp.put("varchar@create_user_id", "create_user_id");
            bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
            bom_works_tmp.put("int@stock_id", "stock_id");
            bom_works_tmp.put("stock_id", RegexUtil.optStrOrNull(map_object.get("from_stock_code")));
            bom_works_tmp.put("int@to_stock_id", "to_stock_id");
            bom_works_tmp.put("to_stock_id", RegexUtil.optStrOrNull(map_object.get("to_stock_code")));
        }
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        if (is_submit) {
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    //备件入库
    private void bomInput(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        boolean is_submit = false;
        //获取当前时间8
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");
        if ("1".equals(isNew)) { // 新建
            bom_works_tmp.put("dateTime@create_time", "create_time");
            bom_works_tmp.put("create_time", create_time);
            bom_works_tmp.put("varchar@create_user_id", "create_user_id");
            bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        if (is_submit) {
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys
                ) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BOM_NEED_WORK_CODE))) {
                    flow_map.put(FlowParmsConstant.BOM_NEED_WORK_CODE, map_object.get(FlowParmsConstant.BOM_NEED_WORK_CODE).toString());
                } else {
                    flow_map.put(FlowParmsConstant.BOM_NEED_WORK_CODE, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    // 备件盘点
    private void bomInventory(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        String main_sub_work_code = null;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String task_id = (String) map_object.get("task_id");
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        String isDraft = map_object.get("isDraft").toString();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        Integer do_flow_key = Integer.valueOf(map_object.get("do_flow_key").toString());
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");

        //库房id为空字符串
        bom_works_tmp.put("stock_id", " ");
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        List<FlowData> flowDataList = new ArrayList<>();
        List<Map<String, Object>> bom_list = (List<Map<String, Object>>) map_object.get("inventory_table");
        List<Map<String, Object>> bom_item_list = RegexUtil.optNotNullListOrNew(map_object.get("inv_bom_list"));
        Map<String, Object> bom_sub_work_detail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optIsPresentStr(bom_sub_work_detail) && Integer.valueOf(bom_sub_work_detail.get("is_main").toString()) == -1) {
            //该工单为子工单
            bom_works_detail_tmp.put("int@is_main", "is_main");
            bom_works_detail_tmp.put("is_main", "-1"); // 1：主工单，-1：子工单
            //查询出该子工单的主工单
            Map<String, Object> main_sub_work_detail = bomWorksMapper.findMainBomWorksDetailByWorkCode(methodParam.getSchemaName(), work_code);
            main_sub_work_code = main_sub_work_detail.get("sub_work_code").toString();
            map_object.put("main_sub_work_code", main_sub_work_code);
            //查询出该工单所有的子工单
            List<Map<String, Object>> sub_work_detail_List = bomWorksMapper.findSubBomWorksDetailListByWorkCode(methodParam.getSchemaName(), work_code);
            //如果有一个工单为退回  那么其他所有的为完成子工单都走退回流程
            if (do_flow_key == ButtonConstant.BTN_RETURN) {
                for (Map<String, Object> sub_work_detail : sub_work_detail_List) {
                    sub_work_code = sub_work_detail.get("sub_work_code").toString();
                    if (Integer.valueOf(sub_work_detail.get("status").toString()) != StatusConstant.COMPLETED && Integer.valueOf(sub_work_detail.get("status").toString()) != 110) {
                        sub_work_detail.put("status", StatusConstant.COMPLETED_CLOSE);//关闭
                        bomWorksMapper.updateBomWorksDetailBySubWorkCode(methodParam.getSchemaName(), sub_work_detail);
                        Map<String, String> flow_map = new HashMap<>();
                        task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                        String sub_token = methodParam.getToken() + sub_work_code;
                        cacheUtilService.getSubToken(methodParam, sub_token);
                        //将流程推动到下一节点
                        flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                        flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                        flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                        flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                        flow_map.put(FlowParmsConstant.STATUS, status.toString());
                        flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                        flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                        flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                            flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                            flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                        } else {
                            flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                        }
                        if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                            flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                        }
                        flow_map.put("main_sub_work_code", main_sub_work_code);
                        //推进流程实例
                        workflowService.subComplete(methodParam, task_id, flow_map);
                    }
                }
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                String sub_token = methodParam.getToken() + sub_work_code;
                cacheUtilService.getSubToken(methodParam, sub_token);
                bom_sub_work_detail.put("status", StatusConstant.COMPLETED);
                bomWorksMapper.updateBomWorksDetailBySubWorkCode(methodParam.getSchemaName(), bom_sub_work_detail);

                Map<String, Object> bom_works_detail_item_tmp = new HashMap<>();
                Map<String, Object> param = new HashMap<>();
                param = (Map<String, Object>) SerializationUtils.clone((Serializable) map_object);
                Map<String, String> keysNew = new HashMap<>();
                keysNew.put("_sc_bom_works_detail_item", "@newsub_work_code");
                param.remove("dataInfo");
                param.remove("keys");
                param.put("keys", keysNew);

                Integer bomId = RegexUtil.optIntegerOrNull(param.get("relation_id"));
                String bomTypeId = RegexUtil.optStrOrBlank(param.get("bom_type_id"));
                List<Map<String, Object>> handleBomList = pagePermissionMapper.findStockBomListByStockAndType(methodParam.getSchemaName(), null, methodParam.getUserId(), bomId, bomTypeId);
                List<Map<String, Object>> bomListNew = new ArrayList<>();
                bomListNew.addAll(handleBomList);
                for (Map<String, Object> bom_item : bom_item_list) {
                    Map<String, Object> dataInfoNew = new HashMap<>();
                    bom_works_detail_item_tmp.put("float@quantity", "quantity");
                    bom_works_detail_item_tmp.put("quantity", RegexUtil.optIntegerOrNull(bom_item.get("inv_count")));
                    bom_works_detail_item_tmp.put("varchar@material_code", "material_code");
                    bom_works_detail_item_tmp.put("material_code", bom_item.get("material_code"));
                    bom_works_detail_item_tmp.put("varchar@sub_work_code", "sub_work_code");
                    bom_works_detail_item_tmp.put("sub_work_code", sub_work_code);
                    if (RegexUtil.optIsPresentStr(bom_item.get("bom_id"))) {
                        bom_works_detail_item_tmp.put("varchar@bom_id", "bom_id");
                        bom_works_detail_item_tmp.put("bom_id", RegexUtil.optStrOrNull(bom_item.get("bom_id")));
                    }

                    for (Map<String, Object> handleTmp : handleBomList) {
                        if (Objects.equals(RegexUtil.optStrOrBlank(handleTmp.get("bom_id")), RegexUtil.optStrOrBlank(bom_item.get("bom_id")))) {
                            bomListNew.remove(handleTmp);
                            break;
                        }
                    }
                    dataInfoNew.put("_sc_bom_works_detail_item", bom_works_detail_item_tmp);
                    param.put("dataInfo", dataInfoNew);
                    saveWorkData(methodParam, param, logger);
                }

                if (RegexUtil.optIsPresentList(bomListNew)) {
                    Map<String, Object> bom_id_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "bom_id");
                    Map<String, Object> material_code_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "material_code");
                    Map<String, Object> bom_name_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "bom_name");
                    Map<String, Object> bom_type_id_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "bom_type_id");
                    Map<String, Object> quantity_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "quantity");
                    Map<String, Object> inv_count_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "inv_count");
                    Map<String, Object> profit_and_loss_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "profit_and_loss");
                    bomListNew.forEach(e -> {
                        e.remove("type_name");
                        if (e.containsKey("bom_id")) {
                            e.put(RegexUtil.optStrOrBlank(bom_id_map.get("field_form_code")), e.get("bom_id"));
                            e.remove("bom_id");
                        }
                        if (e.containsKey("material_code")) {
                            e.put(RegexUtil.optStrOrBlank(material_code_map.get("field_form_code")), e.get("material_code"));
                            e.remove("material_code");
                        }
                        if (e.containsKey("bom_name")) {
                            e.put(RegexUtil.optStrOrBlank(bom_name_map.get("field_form_code")), e.get("bom_name"));
                            e.remove("bom_name");
                        }
                        if (e.containsKey("bom_type_id")) {
                            e.put(RegexUtil.optStrOrBlank(bom_type_id_map.get("field_form_code")), e.get("bom_type_id"));
                            e.remove("bom_type_id");
                        }
                        if (e.containsKey("quantity")) {
                            e.put(RegexUtil.optStrOrBlank(quantity_map.get("field_form_code")), e.get("quantity"));
                            e.remove("quantity");
                        }
                        if (e.containsKey("inv_count")) {
                            e.put(RegexUtil.optStrOrBlank(inv_count_map.get("field_form_code")), 0);
                            e.remove("inv_count");
                        }
                        if (e.containsKey("profit_and_loss")) {
                            e.put(RegexUtil.optStrOrBlank(profit_and_loss_map.get("field_form_code")), "-1");
                            e.remove("profit_and_loss");
                        }
                    });

                    List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
                    for (Map<String, Object> tmp : hadlList) {
                        String field_code = tmp.get("field_code").toString();
                        String field_value = null;
                        if ("inv_bom_list".equals(field_code)) {
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(tmp.get("field_value"));
                            for (int i = 0; i < list.size(); i++) {
                                if (Objects.equals(list.get(i), null)) {
                                    list.remove(i);
                                }
                            }
                            list.addAll(bomListNew);
                            field_value = list.toString();
                            tmp.put("sub_work_code", sub_work_code);
                            tmp.put("field_value", field_value);
                            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                            break;
                        }
                    }
                }

                if (do_flow_key == ButtonConstant.BTN_SUBMIT) {
                    Map<String, String> flow_map = new HashMap<>();
                    //将流程推动到下一节点
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                    flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                    flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                    flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                    flow_map.put(FlowParmsConstant.STATUS, status.toString());
                    flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                    flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                    flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                    if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                        flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                    }
                    if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                        flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                    }
                    if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                        flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                    } else {
                        flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                    }
                    if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                        flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                    } else {
                        flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                    }
                    if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                        flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                    }
                    flow_map.put("main_sub_work_code", main_sub_work_code);
                    //推进流程实例
                    workflowService.subComplete(methodParam, task_id, flow_map);
                }
            }
        } else {
            String subWorkCode = null;
            List<Map<String, Object>> subList = new ArrayList<>();
            if (do_flow_key == ButtonConstant.BTN_SUBMIT) {
                int i = 0;
                for (Map<String, Object> bom : bom_list) {
                    Map<String, Object> tmpMap = new HashMap<>();
                    subWorkCode = dynamicCommonService.getSubBomInventoryCode(methodParam);
                    tmpMap.put("varchar@pref_id", "pref_id");
                    tmpMap.put("pref_id", map_object.get("pref_id"));
                    tmpMap.put("varchar@sub_work_code", "sub_work_code");
                    tmpMap.put("varchar@work_code", "work_code");
                    tmpMap.put("work_code", work_code);
                    tmpMap.put("int@status", "status");
                    tmpMap.put("status", status);
                    tmpMap.put("sub_work_code", subWorkCode);
                    tmpMap.put("varchar@relation_id", "relation_id");
                    tmpMap.put("relation_id", bom.get("relation_id"));
                    tmpMap.put("int@is_main", "is_main");
                    tmpMap.put("is_main", "-1"); // 1：主工单，-1：子工单
                    if (RegexUtil.optIsPresentStr(bom.get("inventory_user_id"))) {
                        tmpMap.put("varchar@duty_user_id", "duty_user_id");
                        tmpMap.put("duty_user_id", bom.get("inventory_user_id").toString());
                    } else {
                        throw new SenscloudException("请选择对应的盘点人员");
                    }
                    worksMapper.insert(methodParam.getSchemaName(), tmpMap, "_sc_bom_works_detail"); // 新增
                    // 流程数据
                    FlowData fd = new FlowData();
                    if (RegexUtil.optIsPresentStr(bom.get("inventory_user_id"))) {
                        fd.setReceive_user_id(bom.get("inventory_user_id").toString());
                    } else {
                        throw new SenscloudException("请选择对应的盘点人员");
                    }
                    fd.setName((String) tmpMap.get("title")); // title
                    fd.setSub_work_code(subWorkCode); // 主键
                    fd.setOrder(-1); // 并行
                    fd.setFacility_id("1"); // 所属位置
                    fd.setDeadline_time(RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME))); // 截止时间
                    fd.setCreate_time(now.toString()); // 发生时间
                    fd.setPref_id(map_object.get(FlowParmsConstant.PREF_ID).toString());
                    if (RegexUtil.optIsPresentStr(map_object.get("priority_level"))) {
                        fd.setPriority_level(map_object.get("priority_level").toString());
                    } else {
                        fd.setPriority_level("1");
                    }
                    fd.setTitle_page(title);
                    String sub_token = methodParam.getToken() + fd.getSub_work_code();
                    cacheUtilService.getSubToken(methodParam, sub_token);
                    fd.setMain_sub_work_code(map_object.get("sub_work_code").toString());
                    fd.setSub_token(sub_token);
                    fd.setWork_type_id(work_type_id);
                    fd.setBusiness_type_id(businessTypeId);
                    fd.setWork_code(work_code);
                    fd.setNode_id(map_object.get("node_id").toString());
                    flowDataList.add(fd);
                    List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) map_object.get("work_flow_node_column_list");
                    for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                        String field_form_code = work_flow_node_column.get("field_form_code").toString();
                        String field_code = work_flow_node_column.get("field_code").toString();
                        String field_value = null;
                        if (RegexUtil.optIsPresentStr(map_object.get(field_code))) {
                            field_value = map_object.get(field_code).toString();
                        } else {
                            field_value = null;
                        }
                        if ("stock_id".equals(field_code)) {
                            field_value = bom.get("relation_id").toString();
                        }
                        if ("inventory_table".equals(field_code)) {
                            List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(map_object.get(field_form_code));
                            List<Map<String, Object>> array = new ArrayList<>();
                            Map<String, Object> inv_count_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "inv_count");
                            Map<String, Object> sub_work_code_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "sub_work_code");
                            Map<String, Object> status_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "status");
                            Map<String, Object> addMap = list.get(i);
                            addMap.put(RegexUtil.optStrOrBlank(inv_count_map.get("field_form_code")), 0);
                            addMap.put(RegexUtil.optStrOrBlank(sub_work_code_map.get("field_form_code")), subWorkCode);
                            addMap.put(RegexUtil.optStrOrBlank(status_map.get("field_form_code")), status);
                            subList.add(addMap);
                            array.add(addMap);
                            field_value = array.toString();
                            i++;
                        }
                        Map<String, Object> worksDetailColumn = new HashMap<>();
                        List<String> flow_keys = new ArrayList<>();
                        Map<String, Object> workFlowNodeInfo = (Map<String, Object>) map_object.get("workFlowNodeInfo");
                        workFlowNodeInfo.put("sub_work_code", subWorkCode);
                        //根据字段主键和工单详情编号获取以前字段信息 如果没有则新增
                        updateWorksDetailColumnByFormCode(methodParam, map_object, subWorkCode, field_form_code, field_value, workFlowNodeInfo, worksDetailColumn, flow_keys, logger);
                    }
                    List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), subWorkCode);
                    for (Map<String, Object> tmp : hadlList) {
                        String field_code = tmp.get("field_code").toString();
                        String field_value = null;
                        if ("bom_type_id".equals(field_code)) {
                            field_value = RegexUtil.optStrOrNull(bom.get("bom_type_id"));
                            tmp.put("sub_work_code", subWorkCode);
                            tmp.put("field_value", field_value);
                            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                            break;
                        }
                    }
                    for (Map<String, Object> tmp : hadlList) {
                        String field_code = tmp.get("field_code").toString();
                        String field_value = null;
                        if ("in_stock_total".equals(field_code)) {
                            field_value = RegexUtil.optStrOrNull(bom.get("stock_count"));
                            tmp.put("sub_work_code", subWorkCode);
                            tmp.put("field_value", field_value);
                            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                            break;
                        }
                    }
                }
            }
            List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
            for (Map<String, Object> tmp : hadlList) {
                String field_code = tmp.get("field_code").toString();
                String field_value = null;
                if ("inventory_table".equals(field_code)) {
                    field_value = subList.toString();
                    tmp.put("sub_work_code", sub_work_code);
                    tmp.put("field_value", field_value);
                    worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                    break;
                }
            }
            if ("1".equals(isNew)) { // 新建
                bom_works_tmp.put("dateTime@create_time", "create_time");
                bom_works_tmp.put("create_time", create_time);
                bom_works_tmp.put("varchar@create_user_id", "create_user_id");
                bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
            }
            if (RegexUtil.optIsPresentStr(bom_works_tmp.get("stock_id"))) {
                bom_works_tmp.put("int@stock_id", "stock_id");
                bom_works_tmp.put("stock_id", RegexUtil.optStrOrNull(bom_works_tmp.get("stock_id")));
            }
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            if ("1".equals(isNew) || "1".equals(isDraft)) {//新建工单  创建工作流
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                //flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.WORK_TYPE_ID, map_object.get(FlowParmsConstant.WORK_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID))) {
                    flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, map_object.get(FlowParmsConstant.BUSINESS_TYPE_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, now.toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                flow_map.put(FlowParmsConstant.POSITION_CODE, "");
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get("title"))) {
                    flow_map.put(FlowParmsConstant.TITLE_PAGE, map_object.get("title").toString());
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                flow_map.put("main_sub_work_code", main_sub_work_code);
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }

        }
    }

    // 备件盘点保存
    private void bomInventorySave(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        //获取当前时间8
        String main_sub_work_code = null;
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
        String create_time = format.format(new Date());
        String businessTypeId = map_object.get("business_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer status = 10;
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        Map<String, Object> bom_works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works_detail");
        Map<String, Object> bom_works_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_works");

        //库房id为空字符串
        bom_works_tmp.put("stock_id", " ");
        bom_works_detail_tmp.put("int@is_main", "is_main");
        bom_works_detail_tmp.put("is_main", "1"); // 1：主工单，-1：子工单
        bom_works_detail_tmp.put("int@status", "status");
        bom_works_detail_tmp.put("status", 10);
        String sub_work_code = (String) bom_works_detail_tmp.get("sub_work_code");
        String work_code = (String) bom_works_detail_tmp.get("work_code");
        Map<String, Object> bom_sub_work_detail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optIsPresentStr(bom_sub_work_detail) && Integer.valueOf(bom_sub_work_detail.get("is_main").toString()) == -1) {
            //该工单为子工单
            List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) map_object.get("work_flow_node_column_list");
            List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
            Map<String, Object> inv_bom_list_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "inv_bom_list");
            RegexUtil.optNotNullMap(inv_bom_list_map).ifPresent(m -> {
                List<Map<String, Object>> inv_bom_list = DataChangeUtil.scdStringToList(RegexUtil.optStrOrNull(map_object.get(m.get("field_form_code"))));
                Map<String, Object> pm = (Map<String, Object>) SerializationUtils.clone((Serializable) m);
                pm.put("sub_work_code", sub_work_code);
                pm.put("field_value", RegexUtil.optStrOrNull(inv_bom_list));
                worksMapper.deleteWorksDetailColumnBySubWorkCodeAndCode(methodParam.getSchemaName(), RegexUtil.optStrOrNull(pm.get("field_form_code")), sub_work_code);
                worksMapper.insertWorksDetailColumn(methodParam.getSchemaName(), pm);
            });
            for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                String field_form_code = work_flow_node_column.get("field_form_code").toString();
                String field_code = work_flow_node_column.get("field_code").toString();
                String field_value = null;
                if (RegexUtil.optIsPresentStr(map_object.get(field_code))) {
                    field_value = map_object.get(field_code).toString();
                } else {
                    field_value = null;
                }

                for (Map<String, Object> tmp : hadlList) {
                    String field_code_tmp = tmp.get("field_code").toString();
                    if (field_code.equals(field_code_tmp)) {
                        tmp.put("sub_work_code", sub_work_code);
                        tmp.put("field_value", field_value);
                        worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                    }
                }
            }
        } else {
            if (RegexUtil.optIsPresentStr(bom_sub_work_detail)) {
                return;
            }
            String subWorkCode = null;
            List<Map<String, Object>> bom_list = (List<Map<String, Object>>) map_object.get("inventory_table");
            List<Map<String, Object>> subList = new ArrayList<>();
            int i = 0;
            for (Map<String, Object> bom : bom_list) {
                subWorkCode = dynamicCommonService.getSubBomInventoryCode(methodParam);
                List<Map<String, Object>> work_flow_node_column_list = (List<Map<String, Object>>) map_object.get("work_flow_node_column_list");
                for (Map<String, Object> work_flow_node_column : work_flow_node_column_list) {
                    String field_form_code = work_flow_node_column.get("field_form_code").toString();
                    String field_code = work_flow_node_column.get("field_code").toString();
                    String field_value = null;
                    if (RegexUtil.optIsPresentStr(map_object.get(field_code))) {
                        field_value = map_object.get(field_code).toString();
                    } else {
                        field_value = null;
                    }
                    if ("inventory_table".equals(field_code)) {
                        List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(map_object.get(field_form_code));
                        List<Map<String, Object>> array = new ArrayList<>();
                        Map<String, Object> inv_count_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "inv_count");
                        Map<String, Object> sub_work_code_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "sub_work_code");
                        Map<String, Object> status_map = workColumnMapper.findWorkColumnByFieldCode(methodParam.getSchemaName(), "status");
                        Map<String, Object> addMap = list.get(i);
                        addMap.put(RegexUtil.optStrOrBlank(inv_count_map.get("field_form_code")), 0);
                        addMap.put(RegexUtil.optStrOrBlank(sub_work_code_map.get("field_form_code")), subWorkCode);
                        addMap.put(RegexUtil.optStrOrBlank(status_map.get("field_form_code")), status);
                        subList.add(addMap);
                        array.add(addMap);
                        field_value = array.toString();
                        i++;
                    }
                    Map<String, Object> worksDetailColumn = new HashMap<>();
                    List<String> flow_keys = new ArrayList<>();
                    Map<String, Object> workFlowNodeInfo = (Map<String, Object>) map_object.get("workFlowNodeInfo");
                    workFlowNodeInfo.put("sub_work_code", subWorkCode);
                    //根据字段主键和工单详情编号获取以前字段信息 如果没有则新增
                    updateWorksDetailColumnByFormCode(methodParam, map_object, subWorkCode, field_form_code, field_value, workFlowNodeInfo, worksDetailColumn, flow_keys, logger);
                }
                List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), subWorkCode);
                for (Map<String, Object> tmp : hadlList) {
                    String field_code = tmp.get("field_code").toString();
                    String field_value = null;
                    if ("bom_type_id".equals(field_code)) {
                        field_value = RegexUtil.optStrOrNull(bom.get("bom_type_id"));
                        tmp.put("sub_work_code", subWorkCode);
                        tmp.put("field_value", field_value);
                        worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                        break;
                    }
                }
                for (Map<String, Object> tmp : hadlList) {
                    String field_code = tmp.get("field_code").toString();
                    String field_value = null;
                    if ("in_stock_total".equals(field_code)) {
                        field_value = RegexUtil.optStrOrNull(bom.get("stock_count"));
                        tmp.put("sub_work_code", subWorkCode);
                        tmp.put("field_value", field_value);
                        worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                        break;
                    }
                }
            }
            List<Map<String, Object>> hadlList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
            for (Map<String, Object> tmp : hadlList) {
                String field_code = tmp.get("field_code").toString();
                String field_value = null;
                if ("inventory_table".equals(field_code)) {
                    field_value = subList.toString();
                    tmp.put("sub_work_code", sub_work_code);
                    tmp.put("field_value", field_value);
                    worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), tmp);
                    break;
                }
            }
            if ("1".equals(isNew)) { // 新建
                bom_works_tmp.put("dateTime@create_time", "create_time");
                bom_works_tmp.put("create_time", create_time);
                bom_works_tmp.put("varchar@create_user_id", "create_user_id");
                bom_works_tmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
                bom_works_tmp.put("int@status", "status");
                bom_works_tmp.put("status", 10);
            }
            if (RegexUtil.optIsPresentStr(bom_works_tmp.get("stock_id"))) {
                bom_works_tmp.put("int@stock_id", "stock_id");
                bom_works_tmp.put("stock_id", RegexUtil.optStrOrNull(bom_works_tmp.get("stock_id")));
            }
            dataInfo.put("_sc_bom_works_detail", bom_works_detail_tmp);
            dataInfo.put("_sc_bom_works", bom_works_tmp);
            dataInfo.remove("_sc_bom_works_detail_item");
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
        }
    }

    private void doNewWork(MethodParam methodParam, Map<String, Object> map_object, Map<String, Object> worksTmp, String flowCode, String work_code, String sub_work_code, String now, Integer doFlowKey, JSONArray logArr) {
        // 新建处理
        String isNew = RegexUtil.optStrOrBlank(map_object.get("isNew"));
        if ("1".equals(isNew)) {
            worksTmp.put("dateTime@create_time", "create_time");
            worksTmp.put("create_time", now);
            worksTmp.put("varchar@create_user_id", "create_user_id");
            worksTmp.put("create_user_id", RegexUtil.optStrOrVal(map_object.get("create_user_id"), methodParam.getUserId()));
        }
        saveWorkData(methodParam, map_object, logArr); // 存储业务表
        // 工作流处理
        Map<String, String> flow_map = new HashMap<>();
        List<String> flowKeys = DataChangeUtil.scdObjToListStrOrNew(map_object.get("flow_keys"));
        String[] keys = new String[]{FlowParmsConstant.DO_FLOW_KEY, FlowParmsConstant.BUSINESS_TYPE_ID, FlowParmsConstant.WORK_TYPE_ID, FlowParmsConstant.DO_BRANCH_KEY, FlowParmsConstant.PREF_ID,
                FlowParmsConstant.DEADLINE_TIME, FlowParmsConstant.STATUS, FlowParmsConstant.SUB_WORK_CODE, FlowParmsConstant.WORK_CODE, FlowParmsConstant.POSITION_CODE};
        flowKeys.addAll(DataChangeUtil.scdArrayToListStr(keys));
        for (String flow_key : flowKeys) {
            RegexUtil.optNotBlankStrOpt(map_object.get(flow_key)).ifPresent(fk -> flow_map.put(flow_key, fk));
        }
//        flow_map.put(FlowParmsConstant.FLOW_DATA, JSON.toJSONString(flowDataList));
        flow_map.put(FlowParmsConstant.FLOW_DATA, null);
        flow_map.put(FlowParmsConstant.CREATE_TIME, now);
        flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
        flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
        flow_map.put(FlowParmsConstant.TITLE_PAGE, RegexUtil.optStrOrBlank(map_object.get("title")));
        flow_map.put(FlowParmsConstant.PDEID, flowCode);
        flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, RegexUtil.optStrOrVal(map_object.get(FlowParmsConstant.PRIORITY_LEVEL), "1")); // 默认紧急程度为1  低
        // 新建流程实例
        if ("1".equals(isNew)) {
            workflowService.startWithForm(methodParam, flowCode, methodParam.getUserId(), flow_map);
        } else {
            String task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
            workflowService.complete(methodParam, task_id, flow_map); // 推进流程实例
        }
    }

    //普通工单
    private void works(MethodParam methodParam, Map<String, Object> map_object, JSONArray logger) {
        String flow_code = map_object.get("flow_code").toString();
        String title = (String) map_object.get("title");
        String businessTypeId = map_object.get("business_type_id").toString();
        String receive_account = "";
        String work_type_id = map_object.get("work_type_id").toString();
        String isNew = map_object.get("isNew").toString();
        Integer do_flow_key = RegexUtil.optIntegerOrNull(map_object.get("do_flow_key"));
        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
        boolean is_submit = false;
        Map<String, Object> works_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_works_detail");
        Map<String, Object> works_tmp = (Map<String, Object>) dataInfo.get("_sc_works");
        //维保行事历编号
        if (RegexUtil.optIsPresentStr(map_object.get("work_calendar_code"))) {
            works_tmp.put("varchar@work_calendar_code", "work_calendar_code");
            works_tmp.put("work_calendar_code", map_object.get("work_calendar_code"));
        }
        String sub_work_code = (String) works_detail_tmp.get("sub_work_code");
        String work_code = (String) works_tmp.get("work_code");
        String schemaName = methodParam.getSchemaName();
        Integer status = Integer.valueOf(map_object.get("status").toString());
        if (do_flow_key == ButtonConstant.BTN_SUBMIT) {//提交按钮
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_RETURN) { //退回按钮
            is_submit = true;//提交事件
            //进行了二次处理
            works_detail_tmp.put("int@is_redo", "is_redo");
            works_detail_tmp.put("is_redo", 1);
            worksMapper.updateBeginTimeIsNull(schemaName, sub_work_code);
        } else if (do_flow_key == ButtonConstant.BTN_AGREE) { //确认完成
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_DISTRIBUTION) { //分配
            is_submit = true;//提交事件
        } else if (do_flow_key == ButtonConstant.BTN_CLOSE) { //关单
            is_submit = true;//提交事件
        }
        logService.newLog(methodParam, businessTypeId, sub_work_code, logger.toString());
        if (is_submit) {
            if ("1".equals(isNew)) {// 新建工单
                // 添加创建人
                works_tmp.put("varchar@create_user_id", "create_user_id");
                works_tmp.put("create_user_id", methodParam.getUserId());
            }
            SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_SS);
            dataInfo.put("_sc_works_detail", works_detail_tmp);
            dataInfo.put("_sc_works", works_tmp);
            map_object.put("dataInfo", dataInfo);
            saveWorkData(methodParam, map_object, logger);//保存新建工单
            Map<String, String> flow_map = new HashMap<>();
            if (RegexUtil.optIsPresentStr(map_object.get("flow_keys"))) {
                List<String> flow_keys = (List<String>) map_object.get("flow_keys");
                for (String flow_key : flow_keys) {
                    if (RegexUtil.optIsPresentStr(map_object.get(flow_key))) {
                        flow_map.put(flow_key, map_object.get(flow_key).toString());
                    }
                }
            }
            assetSync(methodParam, map_object, businessTypeId, schemaName, work_code);
            if ("1".equals(isNew)) {//新建工单  创建工作流
                RegexUtil.optEqualsOpt(businessTypeId, SensConstant.BUSINESS_NO_1001).ifPresent(bid ->
                        RegexUtil.optNotBlankStrOpt(map_object.get("relation_id")).ifPresent(ri ->
                                RegexUtil.optNotBlankStrOpt(worksMapper.selectWorkByRelationId(schemaName, ri, bid)).ifPresent(wcs -> {
                                    throw new SenscloudException(LangConstant.TEXT_G, new String[]{LangConstant.TITLE_AAW_X, wcs}); // 维修已存在：{d}
                                })
                        )
                );
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                String date = format.format(new Date());
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.CREATE_TIME, date);
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());
                flow_map.put(FlowParmsConstant.CREATE_USER_ID, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.WORK_TYPE_ID, work_type_id);
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, methodParam.getUserId());
                flow_map.put(FlowParmsConstant.DEADLINE_TIME, RegexUtil.optStrOrBlank(map_object.get(FlowParmsConstant.DEADLINE_TIME)));
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.BUSINESS_TYPE_ID, businessTypeId);
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.PDEID, flow_code);
                flow_map.put(FlowParmsConstant.PREF_ID, map_object.get(FlowParmsConstant.PREF_ID).toString());
                flow_map.put(FlowParmsConstant.POSITION_CODE, works_detail_tmp.get(FlowParmsConstant.POSITION_CODE).toString());
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_CALENDAR_CODE))) {
                    flow_map.put(FlowParmsConstant.WORK_CALENDAR_CODE, map_object.get(FlowParmsConstant.WORK_CALENDAR_CODE).toString());
                } else {
                    flow_map.put(FlowParmsConstant.WORK_CALENDAR_CODE, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.PRIORITY_LEVEL))) {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, map_object.get(FlowParmsConstant.PRIORITY_LEVEL).toString());
                } else {
                    flow_map.put(FlowParmsConstant.PRIORITY_LEVEL, "1");//默认紧急程度为1  低
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //新建流程实例
                workflowService.startWithForm(methodParam, flow_code, methodParam.getUserId(), flow_map);
            } else {
                String task_id = workflowService.getTaskIdBySubWorkCode(methodParam, sub_work_code);
                //将流程推动到下一节点
                flow_map.put(FlowParmsConstant.TITLE_PAGE, title);
                flow_map.put(FlowParmsConstant.SUB_WORK_CODE, sub_work_code);
                flow_map.put(FlowParmsConstant.WORK_CODE, work_code);
                flow_map.put(FlowParmsConstant.DO_FLOW_KEY, do_flow_key.toString());
                flow_map.put(FlowParmsConstant.STATUS, status.toString());
                flow_map.put(FlowParmsConstant.CREATE_USER_ACCOUNT, methodParam.getAccount());//发送短信人
                flow_map.put(FlowParmsConstant.CREATE_TIME, new Timestamp(System.currentTimeMillis()).toString());
                flow_map.put(FlowParmsConstant.RECEIVE_ACCOUNT, receive_account);
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.RECEIVE_USER_ID))) {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, map_object.get(FlowParmsConstant.RECEIVE_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.RECEIVE_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.WORK_CALENDAR_CODE))) {
                    flow_map.put(FlowParmsConstant.WORK_CALENDAR_CODE, map_object.get(FlowParmsConstant.WORK_CALENDAR_CODE).toString());
                } else {
                    flow_map.put(FlowParmsConstant.WORK_CALENDAR_CODE, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DUTY_USER_ID))) {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, map_object.get(FlowParmsConstant.DUTY_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.DUTY_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.NEXT_USER_ID))) {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, map_object.get(FlowParmsConstant.NEXT_USER_ID).toString());
                } else {
                    flow_map.put(FlowParmsConstant.NEXT_USER_ID, null);
                }
                if (RegexUtil.optIsPresentStr(map_object.get(FlowParmsConstant.DO_BRANCH_KEY))) {
                    flow_map.put(FlowParmsConstant.DO_BRANCH_KEY, map_object.get(FlowParmsConstant.DO_BRANCH_KEY).toString());
                }
                //推进流程实例
                workflowService.complete(methodParam, task_id, flow_map);
            }
        }
    }

    public void assetSync(MethodParam methodParam, Map<String, Object> map_object, String businessTypeId, String schemaName, String work_code) {
        RegexUtil.optEqualsOpt(businessTypeId, SensConstant.BUSINESS_NO_1001).ifPresent(bid ->
                RegexUtil.optNotBlankStrOpt(map_object.get("relation_id")).ifPresent(ri ->
                        RegexUtil.optNotNullIntegerOpt(map_object.get("running_status_id"), LangConstant.TITLE_ASSET_BA).ifPresent(si -> {
                            if (!RegexUtil.optNotBlankStrOpt(assetMapper.findAssetByCondition(schemaName, ri, si)).isPresent()) {
                                Map<String, Object> assetMap = assetMapper.findAssetByCondition(schemaName, ri, null);
                                String oldValue = RegexUtil.optStrOrNull(selectOptionService.getInfoByValue(methodParam, "running_status_id", RegexUtil.optStrOrNull(assetMap.get("running_status_id"))).get("name"));
                                String newValue = RegexUtil.optStrOrNull(selectOptionService.getInfoByValue(methodParam, "running_status_id", RegexUtil.optStrOrNull(si)).get("name"));
                                logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, ri, LangUtil.doSetLogArray("同步设备运行状态", LangConstant.LOG_D, new String[]{LangConstant.TITLE_ASSET_BA, oldValue, newValue + "【单号：".concat(work_code).concat("】")}));
                                assetMapper.updateAssetByStatusID(schemaName, ri, si);
                            }
                        })
                )
        );
        RegexUtil.optEqualsOpt(businessTypeId, SensConstant.BUSINESS_NO_1001).ifPresent(bid ->
                RegexUtil.optNotBlankStrOpt(map_object.get("relation_id")).ifPresent(ri ->
                        RegexUtil.optNotBlankStrOpt(map_object.get("equipment_situation")).ifPresent(es -> {
                            if (!RegexUtil.optNotBlankStrOpt(assetMapper.findAssetSituation(schemaName, ri, es)).isPresent()) {
                                Map<String, Object> assetPo = assetMapper.findAssetSituation(schemaName, ri, null);
                                int result = assetMapper.updateAssetByProperties(schemaName, ri, es);
                                if (result > 0) {
                                    String oldValue = RegexUtil.optNotNullMap(assetPo).map(m -> RegexUtil.optStrOrNull(m.get("value"))).map(s -> selectOptionService.getSelectOptionTextByCode(methodParam, "equipment_condition", s)).orElse("");
                                    String newValue = selectOptionService.getSelectOptionTextByCode(methodParam, "equipment_condition", es);
                                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, ri, LangUtil.doSetLogArray("同步设备状况", LangConstant.LOG_D, new String[]{LangConstant.TITLE_ASSET_AB_Y, oldValue, newValue + "【单号：".concat(work_code).concat("】")}));
                                }
                            }
                        })
                )
        );
    }

    /**
     * 备件调拨，对备件库存位置以及数量的处理
     */
    private void handleBomStock(MethodParam methodParam, Map<String, Object> bomWorks, Map<String, Object> bomWorksDetail, Integer status, Integer nextStatus) {
        if (Objects.equals(bomWorksDetail.get("stock_id"), bomWorksDetail.get("to_stock_id"))) {
            return;
        }
        int work_type_id = Integer.valueOf(RegexUtil.optStrOrBlank(bomWorks.get("work_type_id")));
        Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id);
        String sub_work_code = bomWorksDetail.get("sub_work_code").toString();
        List<Map<String, Object>> bomWorksDetailItemList = bomWorksMapper.findBomWorksDetailItemListBySubWorkCode(methodParam.getSchemaName(), sub_work_code);
        if (status == StatusConstant.TO_BE_CONFIRM && nextStatus == StatusConstant.TO_BE_AUDITED) { // 15->50
            //调拨出库确认
            for (Map<String, Object> bomOut : bomWorksDetailItemList) {
                bomOut.put("stock_id", bomWorks.get("stock_id"));
                bomOut.put("use_count", bomOut.get("quantity"));
                bomOut.put("work_type_name", work_type.get("type_name"));
                bomOut.put("work_code", bomWorks.get("work_code"));
                extracted(methodParam, bomWorks, nextStatus, work_type_id, sub_work_code, bomOut, "stock_id", "调拨出库");
            }
            bomService.updateBomStock(methodParam, bomWorksDetailItemList, -1);
        } else if (status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.COMPLETED) { // 50->60
            //调拨入库确认
            for (Map<String, Object> bomIn : bomWorksDetailItemList) {
                bomIn.put("stock_id", bomWorks.get("to_stock_id"));
                bomIn.put("use_count", bomIn.get("quantity"));
                bomIn.put("work_type_name", work_type.get("type_name"));
                bomIn.put("work_code", bomWorks.get("work_code"));
                extracted(methodParam, bomWorks, nextStatus, work_type_id, sub_work_code, bomIn, "to_stock_id", "调拨入库");
            }
            bomService.updateBomStock(methodParam, bomWorksDetailItemList, 1);
        } else if (status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.COMPLETED_CLOSE
                || status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.TO_BE_CONFIRM) { // 50-> 110,50->15
            //调拨入库关单,调拨入库退回
            for (Map<String, Object> bomIn : bomWorksDetailItemList) {
                bomIn.put("stock_id", bomWorks.get("stock_id"));
                bomIn.put("use_count", bomIn.get("quantity"));
                bomIn.put("work_type_name", work_type.get("type_name"));
                bomIn.put("work_code", bomWorks.get("work_code"));

                String type_name = status == StatusConstant.TO_BE_AUDITED && nextStatus == StatusConstant.COMPLETED_CLOSE ? "调拨入库关单" : "调拨入库退回";
                extracted(methodParam, bomWorks, nextStatus, work_type_id, sub_work_code, bomIn, "to_stock_id", type_name);
            }
            bomService.updateBomStock(methodParam, bomWorksDetailItemList, 1);
        }
    }

    private void extracted(MethodParam methodParam, Map<String, Object> bomWorks, Integer nextStatus, int work_type_id, String sub_work_code, Map<String, Object> bomData, String stock_code, String type_name) {
        Map<String, Object> param = new HashMap<>();
        param.put("sub_work_code", sub_work_code);
        param.put("work_code", bomWorks.get("work_code"));
        param.put("pref_id", bomWorks.get("pref_id"));
        param.put("bom_id", bomData.get("bom_id"));
        param.put("quantity", bomData.get("quantity"));
        param.put("price", bomData.get("price"));
        param.put("material_code", bomData.get("material_code"));
        param.put("stock_id", bomWorks.get(stock_code));
        param.put("status", nextStatus);
        param.put("type_id", work_type_id);
        param.put("type_name", type_name);
        param.put("create_user_id", methodParam.getUserId());
        bomWorksMapper.insertBomWorksAllotRecord(methodParam.getSchemaName(), param);
    }

    /**
     * 更新备件库存数量
     */
    private void updateBomStock(MethodParam methodParam, Map<String, Object> bomWorks, Map<String, Object> bomWorksDetail) {
        int bom_in_out = 0;
        if (RegexUtil.optIsPresentStr(bomWorksDetail.get("bom_in_out"))) {
            bom_in_out = Integer.valueOf(bomWorksDetail.get("bom_in_out").toString());
        }
        if (bom_in_out == 0) {
            return;
        }
        int work_type_id = Integer.valueOf(RegexUtil.optStrOrBlank(bomWorks.get("work_type_id")));
        Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id);
        String sub_work_code = bomWorksDetail.get("sub_work_code").toString();
        List<Map<String, Object>> bomWorksDetailItemList = bomWorksMapper.findBomWorksDetailItemListBySubWorkCode(methodParam.getSchemaName(), sub_work_code);
        for (Map<String, Object> bom : bomWorksDetailItemList) {
            bom.put("stock_id", bomWorks.get("stock_id"));
            bom.put("use_count", bom.get("quantity"));
            bom.put("work_type_name", work_type.get("type_name"));
            bom.put("work_code", bomWorks.get("work_code"));
        }
        if (1 == bom_in_out) {//入库
            bomService.updateBomStock(methodParam, bomWorksDetailItemList, 1);
        } else if (2 == bom_in_out) {//出库
            bomService.updateBomStock(methodParam, bomWorksDetailItemList, -1);
        } else if (3 == bom_in_out) {//减库存支持负数
            bomService.updateBomStock(methodParam, bomWorksDetailItemList, 3);
        }
    }

    /**
     * 减去工单中使用的备件库存
     *
     * @param methodParam   入参
     * @param sub_work_code 入参
     */
    private void reduceWorkBom(MethodParam methodParam, String sub_work_code) {
        List<Map<String, Object>> workBomList = worksMapper.findWorkBomList(methodParam.getSchemaName(), sub_work_code);
        bomService.updateBomStock(methodParam, workBomList, 3);
    }

    /**
     * 设备调拨 更新设备的设备位置为调往位置（设备调拨工单成功是触发）
     * 设备报废 更新设备的状态为报废（设备报废工单成功是触发）
     *
     * @param methodParam           入参
     * @param assetAssetWorksDetail 入参
     */
    private void updateAssetPosition(MethodParam methodParam, Map<String, Object> assetAssetWorksDetail) {
        String work_code = assetAssetWorksDetail.get("work_code").toString();
        String business_type_Id = RegexUtil.optStrOrNull(assetAssetWorksDetail.get("business_type_Id"));
        List<Map<String, Object>> assetAssetWorksDetailItemList = assetWorksMapper.findAssetWorksDetailItemListByWorkCode(methodParam.getSchemaName(), work_code);
        if (RegexUtil.optNotNull(assetAssetWorksDetailItemList).isPresent() && assetAssetWorksDetailItemList.size() > 0) {
            if (SensConstant.BUSINESS_NO_2002.equals(business_type_Id)) {
                for (Map<String, Object> assetAssetWorksDetailItem : assetAssetWorksDetailItemList) {
                    if (RegexUtil.optIsPresentStr(assetAssetWorksDetailItem.get("asset_id")) &&
                            RegexUtil.optIsPresentStr(assetAssetWorksDetail.get("new_position_code"))) {
                        Map<String, Object> assetMap = new HashMap<>();
                        assetMap.put("id", assetAssetWorksDetailItem.get("asset_id"));
                        assetMap.put("position_code", assetAssetWorksDetail.get("new_position_code"));
                        assetMap.put("schemaName", methodParam.getSchemaName());

                        assetMapper.updateAssetPositionCode(assetMap);
                        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetAssetWorksDetailItem.get("asset_id").toString(), LangUtil.doSetLogArray("设备位置变更", LangConstant.LOG_D,
                                new String[]{LangConstant.TITLE_ASSET_G, selectOptionService.getSelectOptionTextByCode(methodParam, "asset_position_info_by_scan", assetAssetWorksDetail.get("position_code").toString()),
                                        selectOptionService.getSelectOptionTextByCode(methodParam, "asset_position_info_by_scan", assetAssetWorksDetail.get("new_position_code").toString())}));
                    }
                }
            } else if (SensConstant.BUSINESS_NO_2003.equals(business_type_Id)) {
                for (Map<String, Object> assetAssetWorksDetailItem : assetAssetWorksDetailItemList) {
                    if (RegexUtil.optIsPresentStr(assetAssetWorksDetailItem.get("asset_id")) &&
                            RegexUtil.optIsPresentStr(assetAssetWorksDetail.get("new_status"))) {
                        Map<String, Object> assetMap = new HashMap<>();
                        assetMap.put("id", assetAssetWorksDetailItem.get("asset_id"));
                        assetMap.put("status", assetAssetWorksDetail.get("new_status"));
                        assetMap.put("schemaName", methodParam.getSchemaName());

                        assetMapper.updateAssetPositionCode(assetMap);
                        logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, assetAssetWorksDetailItem.get("asset_id").toString(), LangUtil.doSetLogArray("设备状态更新为报废", LangConstant.LOG_D, new String[]{LangConstant.TITLE_ASSET_AB_Z, "", "报废"}));
                    }
                }
            } else {
                return;
            }
        }
    }

    /**
     * 添加设备信息
     *
     * @param methodParam           入参
     * @param assetAssetWorksDetail 入参
     */
    private void addAsset(MethodParam methodParam, Map<String, Object> assetAssetWorksDetail) {
        String sub_work_code = assetAssetWorksDetail.get("sub_work_code").toString();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Map<String, Object> asset = getWorksDetailColumnKeyValue(methodParam, sub_work_code);
        asset.put("create_user_id", methodParam.getUserId()); //添加人
        asset.put("status", 2);//资产状态 闲置
        asset.put("schemaName", methodParam.getSchemaName());
        asset.put("parent_id", " ");
        asset.put("category_id", asset.get("asset_category_id"));
        String asset_id = SenscloudUtil.generateUUIDStr(); //生成设备id
        String asset_code = SenscloudUtil.generateUUIDStr(); //生成设备id
        asset.put("id", asset_id); //设备id
        asset.put("asset_code", asset_code); //设备编码
        asset.put("create_time", now); //创建时间
        if (RegexUtil.optIsPresentStr(asset.get("importment_level"))) {
            asset.put("importment_level_id", asset.get("importment_level")); //创建时间
        }
        asset.put("create_time", now); //创建时间
        assetAssetWorksDetail.put("asset_id", asset_id);
        assetWorksMapper.updateAssetWorksDetailBySubWorkCode(methodParam.getSchemaName(), assetAssetWorksDetail);
        assetMapper.insertAsset(asset);
    }

    /**
     * 批量更新设备状态信息
     *
     * @param methodParam    入参
     * @param assetAssetList 入参
     */
    private void batchUpdateAssetStatus(MethodParam methodParam, List<Map<String, Object>> assetAssetList) {
        Set<String> idSet = new HashSet<>();
        assetAssetList.forEach(item -> {
            idSet.add(RegexUtil.optStrOrNull(item.get("relation_id")));
        });
        if (idSet.size() > 0) {
            assetMapper.setSelectStatus(methodParam.getSchemaName(), idSet.toArray(new String[0]), 3);
            //日志更新记录
            idSet.forEach(id -> {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_2000, id, LangUtil.doSetLogArray("设备状态更新为报废", LangConstant.LOG_D, new String[]{LangConstant.TITLE_ASSET_AB_Z, "", "报废"}));
            });
        }
    }

    /**
     * 更新设备信息
     *
     * @param methodParam           入参
     * @param assetAssetWorksDetail 入参
     */
    private void updateAsset(MethodParam methodParam, Map<String, Object> assetAssetWorksDetail) {
        String sub_work_code = assetAssetWorksDetail.get("sub_work_code").toString();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Map<String, Object> asset = getWorksDetailColumnKeyValue(methodParam, sub_work_code);
        asset.put("create_user_id", methodParam.getUserId()); //添加人
        asset.put("status", 2);//资产状态 闲置
        asset.put("schemaName", methodParam.getSchemaName());
        asset.put("parent_id", " ");
        asset.put("category_id", asset.get("asset_category_id"));
        String asset_id = assetAssetWorksDetail.get("asset_id").toString(); //生成设备id
        asset.put("id", asset_id); //设备id
        asset.put("create_time", now); //创建时间
        if (RegexUtil.optIsPresentStr(asset.get("importment_level"))) {
            asset.put("importment_level_id", asset.get("importment_level")); //创建时间
        }
        asset.put("create_time", now); //创建时间
        assetMapper.updateAsset(asset);
    }

    /**
     * 更新设备附件信息
     *
     * @param methodParam           入参
     * @param assetAssetWorksDetail 入参
     */
    private void updateAssetFile(MethodParam methodParam, Map<String, Object> assetAssetWorksDetail) {
        String sub_work_code = assetAssetWorksDetail.get("sub_work_code").toString();
        Map<String, Object> asset = getWorksDetailColumnKeyValue(methodParam, sub_work_code);
        asset.put("id", assetAssetWorksDetail.get("asset_id")); //设备id
        //设备附件
        //当前前端传出来的设备附件信息是字符串格式“xx,xx...”
        if (RegexUtil.optIsPresentStr(assetAssetWorksDetail.get(FlowParmsConstant.ATTACHFILE))) {
            String file_ids = "";
            file_ids = assetAssetWorksDetail.get(FlowParmsConstant.ATTACHFILE).toString();
            asset.put("file_ids", file_ids); //设备附件
        }
        asset.put("fileIdsChange", "1");
        asset.put("schemaName", methodParam.getSchemaName());
        assetMapper.updateAsset(asset);
    }

    /**
     * 创建维修工单
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    private void createRepairWorks(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optNotNullMap(bussinessConfigService.getWorkTypeByBusiness(methodParam, Integer.valueOf(SensConstant.BUSINESS_NO_1001)))
                .map(wt -> RegexUtil.optStrOrExpNotExist(wt.get("id"), LangConstant.TITLE_AAI_I)).ifPresent(wid -> {
            // 获取流程的开始节点
            RegexUtil.optNotNullMap(workFlowTemplateService.getWorkFlowStartNodeByWorkTypeId(methodParam, wid)).ifPresent(flowStartNode -> {
                String prefId = RegexUtil.optStrOrExpNotExist(flowStartNode.get("pref_id"), LangConstant.TITLE_AAI_I);
                String pageType = RegexUtil.optStrOrErrorExp(flowStartNode.get("page_type"), methodParam, ErrorConstant.EC_WORK_013, LangConstant.MSG_BI);
                String nodeId = RegexUtil.optStrOrErrorExp(flowStartNode.get("node_id"), methodParam, ErrorConstant.EC_WORK_014, LangConstant.MSG_BI);
                RegexUtil.optNotNullMap(workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, prefId, nodeId, pageType)).ifPresent(workFlowNodeTemplateInfo -> {
                    Date now = new Date();
                    paramMap.put("work_type_id", wid);
                    paramMap.put("do_flow_key", ButtonConstant.BTN_SUBMIT);
                    paramMap.put("type_name", workFlowNodeTemplateInfo.get("type_name"));
                    paramMap.put("deal_role_id", flowStartNode.get("deal_role_id"));
                    paramMap.put("create_time", now.getTime());
                    paramMap.put("deal_type_id", flowStartNode.get("deal_type_id"));
                    paramMap.put("page_type", pageType);
                    paramMap.put("node_name", flowStartNode.get("node_name"));
                    paramMap.put("work_template_code", workFlowNodeTemplateInfo.get("work_template_code"));
                    paramMap.put("flow_name", workFlowNodeTemplateInfo.get("flow_name"));
                    paramMap.put("relation_type", workFlowNodeTemplateInfo.get("relation_type"));
                    paramMap.put("relation_type_name", workFlowNodeTemplateInfo.get("relation_type_name"));
                    paramMap.put("flow_code", workFlowNodeTemplateInfo.get("flow_code"));
                    paramMap.put("word_flow_node_id", flowStartNode.get("id"));
                    paramMap.put("node_type", flowStartNode.get("node_type"));
                    paramMap.put("work_template_name", workFlowNodeTemplateInfo.get("work_template_name"));
                    paramMap.put("pref_id", prefId);
                    paramMap.put("node_show_name", flowStartNode.get("show_name"));
                    paramMap.put("flow_show_name", workFlowNodeTemplateInfo.get("flow_show_name"));
                    paramMap.put("id", workFlowNodeTemplateInfo.get("id"));
                    paramMap.put("node_id", flowStartNode.get("node_id"));
                    paramMap.put("create_user_id", methodParam.getUserId());
                    worksSubmit(methodParam, paramMap);
                });
            });
        });
    }

    /**
     * 创建维修工单 创建保养工单
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    private void createMaintainWorks(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optNotNullMap(bussinessConfigService.getWorkTypeByBusiness(methodParam, Integer.valueOf(SensConstant.BUSINESS_NO_1002)))
                .map(wt -> RegexUtil.optStrOrExpNotExist(wt.get("id"), LangConstant.TITLE_AAI_I)).ifPresent(wid -> {
            // 获取流程的开始节点
            RegexUtil.optNotNullMap(workFlowTemplateService.getWorkFlowStartNodeByWorkTypeId(methodParam, wid)).ifPresent(flowStartNode -> {
                String prefId = RegexUtil.optStrOrExpNotExist(flowStartNode.get("pref_id"), LangConstant.TITLE_AAI_I);
                String pageType = RegexUtil.optStrOrErrorExp(flowStartNode.get("page_type"), methodParam, ErrorConstant.EC_WORK_013, LangConstant.MSG_BI);
                String nodeId = RegexUtil.optStrOrErrorExp(flowStartNode.get("node_id"), methodParam, ErrorConstant.EC_WORK_014, LangConstant.MSG_BI);
                RegexUtil.optNotNullMap(workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, prefId, nodeId, pageType)).ifPresent(workFlowNodeTemplateInfo -> {
                    Date now = new Date();
                    paramMap.put("work_type_id", wid);
                    paramMap.put("do_flow_key", ButtonConstant.BTN_SUBMIT);
                    paramMap.put("type_name", workFlowNodeTemplateInfo.get("type_name"));
                    paramMap.put("deal_role_id", flowStartNode.get("deal_role_id"));
                    paramMap.put("create_time", now.getTime());
                    paramMap.put("deal_type_id", flowStartNode.get("deal_type_id"));
                    paramMap.put("page_type", pageType);
                    paramMap.put("node_name", flowStartNode.get("node_name"));
                    paramMap.put("work_template_code", workFlowNodeTemplateInfo.get("work_template_code"));
                    paramMap.put("flow_name", workFlowNodeTemplateInfo.get("flow_name"));
                    paramMap.put("relation_type", workFlowNodeTemplateInfo.get("relation_type"));
                    paramMap.put("relation_type_name", workFlowNodeTemplateInfo.get("relation_type_name"));
                    paramMap.put("flow_code", workFlowNodeTemplateInfo.get("flow_code"));
                    paramMap.put("word_flow_node_id", flowStartNode.get("id"));
                    paramMap.put("node_type", flowStartNode.get("node_type"));
                    paramMap.put("work_template_name", workFlowNodeTemplateInfo.get("work_template_name"));
                    paramMap.put("pref_id", prefId);
                    paramMap.put("node_show_name", flowStartNode.get("show_name"));
                    paramMap.put("flow_show_name", workFlowNodeTemplateInfo.get("flow_show_name"));
                    paramMap.put("id", workFlowNodeTemplateInfo.get("id"));
                    paramMap.put("node_id", flowStartNode.get("node_id"));
                    paramMap.put("create_user_id", methodParam.getUserId());
                    worksSubmit(methodParam, paramMap);
                });
            });
        });
    }

    /**
     * 工单发送短信和记录日志
     */
    private void sendMessage(MethodParam methodParam, Map<String, Object> map_object, Integer do_flow_key) {
        try {
            String business_type_id = RegexUtil.optStrOrBlank(map_object.get("business_type_id"));
            String sub_work_code = RegexUtil.optStrOrBlank(map_object.get("sub_work_code"));
            Map<String, Object> workDetail = null;
            String distribute_user_id = "";
            String receive_user_id = "";
            String duty_user_id = "";
            String distribute_user_name = "";
            String receive_user_name = "";
            String duty_user_name = "";
            String create_user_id = null;
            String create_user_name = null;
            if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1002.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1005.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1006.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1100.equals(business_type_id)) {
                workDetail = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code);
                RegexUtil.optNotNullOrExp(workDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
                create_user_id = RegexUtil.optStrOrBlank(workDetail.get("create_user_id"));
            } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2002.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2005.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2006.equals(business_type_id)) {
                workDetail = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
                RegexUtil.optNotNullOrExp(workDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
            } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3007.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3008.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3005.equals(business_type_id)) {
                workDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
                RegexUtil.optNotNullOrExp(workDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
            }
            Map<String, Object> worksColumnKeyValues = getWorksDetailColumnKeyValue(methodParam, sub_work_code);
            String title = RegexUtil.optStrOrBlank(worksColumnKeyValues.get("title"));


            if (RegexUtil.optIsPresentStr(workDetail.get("distribute_user_id"))) {
                distribute_user_id = RegexUtil.optStrOrBlank(workDetail.get("distribute_user_id"));
                Map<String, Object> distribute_user_info = userService.getUserInfoById(methodParam, distribute_user_id);
                if (RegexUtil.optNotNull(distribute_user_info).isPresent()) {
                    distribute_user_name = (String) distribute_user_info.get("user_name");
                }
            }
            if (RegexUtil.optIsPresentStr(workDetail.get("receive_user_id"))) {
                receive_user_id = RegexUtil.optStrOrBlank(workDetail.get("receive_user_id"));
                Map<String, Object> receive_user_info = userService.getUserInfoById(methodParam, receive_user_id);
                if (RegexUtil.optNotNull(receive_user_info).isPresent()) {
                    receive_user_name = (String) receive_user_info.get("user_name");
                }
            }
            if (RegexUtil.optIsPresentStr(workDetail.get("duty_user_id"))) {
                duty_user_id = RegexUtil.optStrOrBlank(workDetail.get("duty_user_id"));
                Map<String, Object> duty_user_info = userService.getUserInfoById(methodParam, duty_user_id);
                if (RegexUtil.optNotNull(duty_user_info).isPresent()) {
                    duty_user_name = (String) duty_user_info.get("user_name");
                }
            }

            if (!RegexUtil.optIsPresentStr(create_user_id) && RegexUtil.optIsPresentStr(worksColumnKeyValues.get("create_user_id"))) {
                create_user_id = RegexUtil.optStrOrBlank(worksColumnKeyValues.get("create_user_id"));
                Map<String, Object> create_user_info = userService.getUserInfoById(methodParam, create_user_id);
                if (RegexUtil.optNotNull(create_user_info).isPresent()) {
                    create_user_name = (String) create_user_info.get("user_name");
                }
            } else if (RegexUtil.optIsPresentStr(create_user_id)) {
                Map<String, Object> create_user_info = userService.getUserInfoById(methodParam, create_user_id);
                if (RegexUtil.optNotNull(create_user_info).isPresent()) {
                    create_user_name = (String) create_user_info.get("user_name");
                }
            }
            String business_no = business_type_id + SensConstant.BUSINESS_NO_PROCESS;
            if (!RegexUtil.optIsPresentStr(map_object.get("msg_code"))) {
                throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_PH});//短信编码不能为空
            }
            //Integer work_type_id = Integer.valueOf(RegexUtil.optStrOrNull(map_object.get("work_type_id")));
            String business_lang_key = null;
            if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_REPAIR_A;//维修多语言
            } else if (SensConstant.BUSINESS_NO_1002.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_CBM_A;//保养多语言
            } else if (SensConstant.BUSINESS_NO_1003.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_INSPECTION_A;//巡检多语言
            } else if (SensConstant.BUSINESS_NO_1004.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_POINT_A;//点检多语言
            } else if (SensConstant.BUSINESS_NO_1005.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_AAAAB_C;//安装多语言
            } else if (SensConstant.BUSINESS_NO_1006.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_AAAAB_C;//todo 资本性多语言
            } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFK;// 设备验收多语言
            } else if (SensConstant.BUSINESS_NO_2002.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AB_G;//设备调拨多语言
            } else if (SensConstant.BUSINESS_NO_2003.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AD;//设备报废多语言
            } else if (SensConstant.BUSINESS_NO_2004.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AB_A;//设备盘点多语言
            } else if (SensConstant.BUSINESS_NO_2005.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFL;//设备出库多语言
            } else if (SensConstant.BUSINESS_NO_2006.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AB_D;//设备入库多语言
            } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_I;//备件入库多语言
            } else if (SensConstant.BUSINESS_NO_3002.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_O;//备件调拨多语言
            } else if (SensConstant.BUSINESS_NO_3003.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_D;//备件领用多语言
            } else if (SensConstant.BUSINESS_NO_3004.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABAA_U;//备件报废多语言
            } else if (SensConstant.BUSINESS_NO_3005.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_F;//备件盘点多语言
            } else if (SensConstant.BUSINESS_NO_3006.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_H;//备件清单多语言
            } else if (SensConstant.BUSINESS_NO_3007.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFM;//备件销账
            } else if (SensConstant.BUSINESS_NO_3008.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFN;//备件采购需求
            }
            String msg_code = RegexUtil.optStrOrBlank(map_object.get("msg_code"));
            Object[] contentParam = new Object[]{};
            if (do_flow_key != ButtonConstant.BTN_SAVE) {//提交按钮
                if (SensConstant.SMS_10002001.equals(msg_code)) {
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002002.equals(msg_code)) {
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002003.equals(msg_code)) {
                    //申请单处理 {1}请您尽快完成{2}。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002004.equals(msg_code)) {
                    //申请单拒绝 {1}拒绝了{2}，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002005.equals(msg_code)) {
                    //申请单审核通过 {1}的{2}申请单，审核已通过，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002006.equals(msg_code)) {
                    //申请单审核退回 {1}的{2}申请单，审核已退回，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002007.equals(msg_code)) {
                    //申请单-确认人  申请人{1}请您尽快审核提交的{2}申请。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003001.equals(msg_code)) {
                    //服务请求确认   {1}请您尽快确认服务请求：{2}。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003002.equals(msg_code)) {
                    //服务请求-分配人 {1}请您尽快分配人员确认服务请求：{2}。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003003.equals(msg_code)) {
                    //工单完成确认通过 本次服务请求已完成：{1}，请您知悉。
                    contentParam = new Object[]{title};
                } else if (SensConstant.SMS_10003004.equals(msg_code)) {
                    //服务请求关闭 {1}关闭了服务请求：{2}，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003005.equals(msg_code)) {
                    //工单创建-分配人-处理 {1}请您尽快分配人员{2}：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003006.equals(msg_code)) {
                    //工单创建-分配人-确认 {1}请您尽快分配人员确认{2}工单：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003007.equals(msg_code)) {
                    //工单创建-处理人 {1}请您尽快完成{2}工单：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003008.equals(msg_code)) {
                    //工单-确认人  {1}请您尽快确认{2}工单：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003009.equals(msg_code)) {
                    //工单拒绝 {1}拒绝了{2}工单：{3}，请您重新处理。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003010.equals(msg_code)) {
                    //工单完成待确认 {1}完成了{2}工单：{3}，请您尽快确认。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003011.equals(msg_code)) {
                    //工单完成确认通过 {1}确认通过了{2}工单：{3}，请您知悉。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003012.equals(msg_code)) {
                    //工单完成确认退回 {1}退回了{2}工单：{3}，请您重新处理。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003013.equals(msg_code)) {
                    //工单快超时提醒 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
                    //完成时间减现在时间就是快超时时间
                    try {
                        if (RegexUtil.optIsPresentStr(worksColumnKeyValues.get("finish_time"))) {
                            String finish_time = worksColumnKeyValues.get("finish_time").toString();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date findsh_date = df.parse(finish_time);
                            Long between = (findsh_date.getTime() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, title, between};
                        } else {

                            Long between = (System.currentTimeMillis() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, title, between};
                        }
                    } catch (Exception e) {
                    }
                } else if (SensConstant.SMS_10003014.equals(msg_code)) {
                    //工单快超时提醒 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
                    //完成时间减现在时间就是快超时时间
                    try {
                        if (RegexUtil.optIsPresentStr(worksColumnKeyValues.get("finish_time"))) {
                            String finish_time = worksColumnKeyValues.get("finish_time").toString();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date findsh_date = df.parse(finish_time);
                            Long between = (findsh_date.getTime() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, title, between};
                        } else {

                            Long between = (System.currentTimeMillis() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{business_lang_key, title, between};
                        }
                    } catch (Exception e) {
                    }
                } else if (SensConstant.SMS_10003015.equals(msg_code)) {
                    //{1}负责的{2}工单：{3}，已超时{4}，请您知悉。
                    //完成时间减现在时间就是快超时时间
                    try {
                        if (RegexUtil.optIsPresentStr(worksColumnKeyValues.get("finish_time"))) {
                            String finish_time = worksColumnKeyValues.get("finish_time").toString();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date findsh_date = df.parse(finish_time);
                            Long between = (findsh_date.getTime() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{methodParam.getUserName(), title, between};
                        } else {
                            Long between = (System.currentTimeMillis() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, business_lang_key, title, between};
                        }
                    } catch (Exception e) {
                    }
                } else if (SensConstant.SMS_10003016.equals(msg_code)) {
                    //工单快超时提醒 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
                    //完成时间减现在时间就是快超时时间
                    contentParam = new Object[]{duty_user_name, title};
                }
            }
            //发送短信
            if (RegexUtil.optIsPresentStr(duty_user_id)) {
                messageService.sendMsg(methodParam, contentParam, msg_code, duty_user_id, business_type_id, business_no, title);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_SYS_MSG_001, methodParam.getUserId(), "系统消息发送失败");
        }
    }

    /**
     * 工单发送抄送短信和记录抄送日志
     */
    private void sendCCMessage(MethodParam methodParam, Map<String, Object> map_object, Integer do_flow_key) {
        try {
            String business_type_id = RegexUtil.optStrOrBlank(map_object.get("business_type_id"));
            String sub_work_code = RegexUtil.optStrOrBlank(map_object.get("sub_work_code"));
            List<String> ccUserList = RegexUtil.optNotNullListStrOrNew(map_object.get("ccUserList"));
            Map<String, Object> workDetail = null;
            String distribute_user_id = "";
            String receive_user_id = "";
            String duty_user_id = "";
            String distribute_user_name = "";
            String receive_user_name = "";
            String duty_user_name = "";
            String create_user_id = null;
            String create_user_name = null;
            if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1002.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1005.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1006.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_1100.equals(business_type_id)) {
                workDetail = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code);
                RegexUtil.optNotNullOrExp(workDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
                create_user_id = RegexUtil.optStrOrBlank(workDetail.get("create_user_id"));
            } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2002.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2005.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_2006.equals(business_type_id)) {
                workDetail = assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
                RegexUtil.optNotNullOrExp(workDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
            } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3003.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3004.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3007.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3008.equals(business_type_id)
                    || SensConstant.BUSINESS_NO_3005.equals(business_type_id)) {
                workDetail = bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), sub_work_code);
                RegexUtil.optNotNullOrExp(workDetail, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AC_H});//工单信息不存在
            }
            Map<String, Object> worksColumnKeyValues = getWorksDetailColumnKeyValue(methodParam, sub_work_code);
            String title = RegexUtil.optStrOrBlank(worksColumnKeyValues.get("title"));


            if (RegexUtil.optIsPresentStr(workDetail.get("distribute_user_id"))) {
                distribute_user_id = RegexUtil.optStrOrBlank(workDetail.get("distribute_user_id"));
                Map<String, Object> distribute_user_info = userService.getUserInfoById(methodParam, distribute_user_id);
                if (RegexUtil.optNotNull(distribute_user_info).isPresent()) {
                    distribute_user_name = (String) distribute_user_info.get("user_name");
                }
            }
            if (RegexUtil.optIsPresentStr(workDetail.get("receive_user_id"))) {
                receive_user_id = RegexUtil.optStrOrBlank(workDetail.get("receive_user_id"));
                Map<String, Object> receive_user_info = userService.getUserInfoById(methodParam, receive_user_id);
                if (RegexUtil.optNotNull(receive_user_info).isPresent()) {
                    receive_user_name = (String) receive_user_info.get("user_name");
                }
            }
            if (RegexUtil.optIsPresentStr(workDetail.get("duty_user_id"))) {
                duty_user_id = RegexUtil.optStrOrBlank(workDetail.get("duty_user_id"));
                Map<String, Object> duty_user_info = userService.getUserInfoById(methodParam, duty_user_id);
                if (RegexUtil.optNotNull(duty_user_info).isPresent()) {
                    duty_user_name = (String) duty_user_info.get("user_name");
                }
            }

            if (!RegexUtil.optIsPresentStr(create_user_id) && RegexUtil.optIsPresentStr(worksColumnKeyValues.get("create_user_id"))) {
                create_user_id = RegexUtil.optStrOrBlank(worksColumnKeyValues.get("create_user_id"));
                Map<String, Object> create_user_info = userService.getUserInfoById(methodParam, create_user_id);
                if (RegexUtil.optNotNull(create_user_info).isPresent()) {
                    create_user_name = (String) create_user_info.get("user_name");
                }
            } else if (RegexUtil.optIsPresentStr(create_user_id)) {
                Map<String, Object> create_user_info = userService.getUserInfoById(methodParam, create_user_id);
                if (RegexUtil.optNotNull(create_user_info).isPresent()) {
                    create_user_name = (String) create_user_info.get("user_name");
                }
            }
            String business_no = business_type_id + SensConstant.BUSINESS_NO_PROCESS;
            if (!RegexUtil.optIsPresentStr(map_object.get("msg_code"))) {
                throw new SenscloudException(LangConstant.MSG_A, new String[]{LangConstant.TITLE_PH});//短信编码不能为空
            }
            String business_lang_key = null;
            if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_REPAIR_A;//维修多语言
            } else if (SensConstant.BUSINESS_NO_1002.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_CBM_A;//保养多语言
            } else if (SensConstant.BUSINESS_NO_1003.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_INSPECTION_A;//巡检多语言
            } else if (SensConstant.BUSINESS_NO_1004.equals(business_type_id)) {
                business_lang_key = LangConstant.BTN_POINT_A;//点检多语言
            } else if (SensConstant.BUSINESS_NO_1005.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_AAAAB_C;//安装多语言
            } else if (SensConstant.BUSINESS_NO_1006.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_AAAAB_C;//todo 资本性多语言
            } else if (SensConstant.BUSINESS_NO_2001.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFK;// 设备验收多语言
            } else if (SensConstant.BUSINESS_NO_2002.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AB_G;//设备调拨多语言
            } else if (SensConstant.BUSINESS_NO_2003.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AD;//设备报废多语言
            } else if (SensConstant.BUSINESS_NO_2004.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AB_A;//设备盘点多语言
            } else if (SensConstant.BUSINESS_NO_2005.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFL;//设备出库多语言
            } else if (SensConstant.BUSINESS_NO_2006.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ASSET_AB_D;//设备入库多语言
            } else if (SensConstant.BUSINESS_NO_3001.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_I;//备件入库多语言
            } else if (SensConstant.BUSINESS_NO_3002.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_O;//备件调拨多语言
            } else if (SensConstant.BUSINESS_NO_3003.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_D;//备件领用多语言
            } else if (SensConstant.BUSINESS_NO_3004.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABAA_U;//备件报废多语言
            } else if (SensConstant.BUSINESS_NO_3005.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_F;//备件盘点多语言
            } else if (SensConstant.BUSINESS_NO_3006.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_ABBA_H;//备件清单多语言
            } else if (SensConstant.BUSINESS_NO_3007.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFM;//备件销账
            } else if (SensConstant.BUSINESS_NO_3008.equals(business_type_id)) {
                business_lang_key = LangConstant.TITLE_EFN;//备件采购需求
            }
            String cc_msg_code = RegexUtil.optStrOrBlank(map_object.get("cc_msg_code"));
            Object[] contentParam = new Object[]{};
            if (do_flow_key != ButtonConstant.BTN_SAVE) {//提交按钮
                if (SensConstant.SMS_10002001.equals(cc_msg_code)) {
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002002.equals(cc_msg_code)) {
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002003.equals(cc_msg_code)) {
                    //申请单处理 {1}请您尽快完成{2}。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002004.equals(cc_msg_code)) {
                    //申请单拒绝 {1}拒绝了{2}，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002005.equals(cc_msg_code)) {
                    //申请单审核通过 {1}的{2}申请单，审核已通过，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002006.equals(cc_msg_code)) {
                    //申请单审核退回 {1}的{2}申请单，审核已退回，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10002007.equals(cc_msg_code)) {
                    //申请单-确认人  申请人{1}请您尽快审核提交的{2}申请。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003001.equals(cc_msg_code)) {
                    //服务请求确认   {1}请您尽快确认服务请求：{2}。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003002.equals(cc_msg_code)) {
                    //服务请求-分配人 {1}请您尽快分配人员确认服务请求：{2}。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003003.equals(cc_msg_code)) {
                    //工单完成确认通过 本次服务请求已完成：{1}，请您知悉。
                    contentParam = new Object[]{title};
                } else if (SensConstant.SMS_10003004.equals(cc_msg_code)) {
                    //服务请求关闭 {1}关闭了服务请求：{2}，请您知悉。
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10003005.equals(cc_msg_code)) {
                    //工单创建-分配人-处理 {1}请您尽快分配人员{2}：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003006.equals(cc_msg_code)) {
                    //工单创建-分配人-确认 {1}请您尽快分配人员确认{2}工单：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003007.equals(cc_msg_code)) {
                    //工单创建-处理人 {1}请您尽快完成{2}工单：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003008.equals(cc_msg_code)) {
                    //工单-确认人  {1}请您尽快确认{2}工单：{3}。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003009.equals(cc_msg_code)) {
                    //工单拒绝 {1}拒绝了{2}工单：{3}，请您重新处理。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003010.equals(cc_msg_code)) {
                    //工单完成待确认 {1}完成了{2}工单：{3}，请您尽快确认。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003011.equals(cc_msg_code)) {
                    //工单完成确认通过 {1}确认通过了{2}工单：{3}，请您知悉。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003012.equals(cc_msg_code)) {
                    //工单完成确认退回 {1}退回了{2}工单：{3}，请您重新处理。
                    contentParam = new Object[]{duty_user_name, business_lang_key, title};
                } else if (SensConstant.SMS_10003013.equals(cc_msg_code)) {
                    //工单快超时提醒 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
                    //完成时间减现在时间就是快超时时间
                    try {
                        if (RegexUtil.optIsPresentStr(worksColumnKeyValues.get("finish_time"))) {
                            String finish_time = worksColumnKeyValues.get("finish_time").toString();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date findsh_date = df.parse(finish_time);
                            Long between = (findsh_date.getTime() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, title, between};
                        } else {

                            Long between = (System.currentTimeMillis() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, title, between};
                        }
                    } catch (Exception e) {
                    }
                } else if (SensConstant.SMS_10003014.equals(cc_msg_code)) {
                    //工单快超时提醒 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
                    //完成时间减现在时间就是快超时时间
                    try {
                        if (RegexUtil.optIsPresentStr(worksColumnKeyValues.get("finish_time"))) {
                            String finish_time = worksColumnKeyValues.get("finish_time").toString();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date findsh_date = df.parse(finish_time);
                            Long between = (findsh_date.getTime() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, title, between};
                        } else {

                            Long between = (System.currentTimeMillis() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{business_lang_key, title, between};
                        }
                    } catch (Exception e) {
                    }
                } else if (SensConstant.SMS_10003015.equals(cc_msg_code)) {
                    //{1}负责的{2}工单：{3}，已超时{4}，请您知悉。
                    //完成时间减现在时间就是快超时时间
                    try {
                        if (RegexUtil.optIsPresentStr(worksColumnKeyValues.get("finish_time"))) {
                            String finish_time = worksColumnKeyValues.get("finish_time").toString();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date findsh_date = df.parse(finish_time);
                            Long between = (findsh_date.getTime() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{methodParam.getUserName(), title, between};
                        } else {
                            Long between = (System.currentTimeMillis() - System.currentTimeMillis()) / 60000;
                            contentParam = new Object[]{duty_user_name, business_lang_key, title, between};
                        }
                    } catch (Exception e) {
                    }
                } else if (SensConstant.SMS_10003016.equals(cc_msg_code)) {
                    //工单快超时提醒 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
                    //完成时间减现在时间就是快超时时间
                    contentParam = new Object[]{duty_user_name, title};
                } else if (SensConstant.SMS_10004003.equals(cc_msg_code)) {
                    //{1}提交了{2}申请，{3}正在处理，请您知晓。
                    contentParam = new Object[]{distribute_user_name, title, duty_user_name};
                }
            }
            //发送短信
            if (ccUserList.contains(duty_user_id)) {
                ccUserList.remove(duty_user_id);
            }
            if (ccUserList.contains(distribute_user_id)) {
                ccUserList.remove(distribute_user_id);
            }
            if (!ccUserList.isEmpty()) {
                methodParam.setNeedCC(true);
                for (String ccUser : ccUserList) {
                    messageService.sendMsg(methodParam, contentParam, cc_msg_code, ccUser, business_type_id, business_no, title);
                }
                //多语言可以转换
                for (int i = 0; i < contentParam.length; i++) {
                    contentParam[i] = commonUtilService.getLanguageInfoBySysLang(methodParam, contentParam[i].toString());
                }
                // 发送抄送邮件
                if (!ccUserList.contains(distribute_user_id)) {
                    ccUserList.add(distribute_user_id);
                }
                messageService.doSendCCEmailMsg(methodParam, contentParam, cc_msg_code, duty_user_id, business_type_id, business_no, title, ccUserList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_SYS_MSG_001, methodParam.getUserId(), "系统消息发送失败");
        }
    }

    /**
     * 根据工单详情编码获取字段详情信息
     *
     * @param methodParam   入参
     * @param sub_work_code 入参
     * @return 字段详情信息
     */
    private Map<String, Object> getWorksDetailColumnList(MethodParam methodParam, String sub_work_code) {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> worksDetailColumnList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optNotNull(worksDetailColumnList).isPresent() && worksDetailColumnList.size() > 0) {
            for (Map<String, Object> worksDetailColumn : worksDetailColumnList) {
                List<Map<String, Object>> extList = worksMapper.findWorksDetailColumnExtListByFileFormCode(methodParam.getSchemaName(), sub_work_code, worksDetailColumn.get("field_form_code").toString());
                if (RegexUtil.optNotNull(extList).isPresent() && extList.size() > 0) {
                    worksDetailColumn.put("works_detail_column_list", extList);
                }
                List<Map<String, Object>> linkList = worksMapper.findWorksDetailColumnLinkageList(methodParam.getSchemaName(), sub_work_code, worksDetailColumn.get("field_form_code").toString());
                worksDetailColumn.put("works_detail_column_linkage_list", linkList);
                if (RegexUtil.optNotNull(linkList).isPresent() && linkList.size() > 0) {
                    for (Map<String, Object> linkage : linkList) {
                        Map<String, Object> condition = worksMapper.findWorksDetailColumnLinkageConditionByLinkageId(methodParam.getSchemaName(), Integer.valueOf(linkage.get("id").toString()), sub_work_code);
                        linkage.put("works_detail_column_linkage_condition", condition);
                    }
                }
                result.put(worksDetailColumn.get("field_form_code").toString(), worksDetailColumn);
            }
        }
        return result;
    }

    /**
     * 将工单详情的字段表转换为keyvalue的map
     *
     * @param methodParam   入参
     * @param sub_work_code 入参
     * @return map
     */
    private Map<String, Object> getWorksDetailColumnKeyValue(MethodParam methodParam, String sub_work_code) {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> worksDetailColumnList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optNotNull(worksDetailColumnList).isPresent() && worksDetailColumnList.size() > 0) {
            for (Map<String, Object> worksDetailColumn : worksDetailColumnList) {
                result.put(worksDetailColumn.get("field_code").toString(), worksDetailColumn.get("field_value"));
            }
        }
        return result;
    }

    /**
     * 子页面处理
     *
     * @param methodParam               入参
     * @param sub_work_code             入参
     * @param worksFlowNodeColumnInfo   入参
     * @param workFlowNodeColumnLinkage 入参
     * @param field_value               入参
     */
    private void dealSubPage(MethodParam methodParam,
                             Map<String, Object> param,
                             String sub_work_code,
                             Map<String, Object> worksFlowNodeColumnInfo,
                             Map<String, Object> workFlowNodeColumnLinkage,
                             String field_value) {
        //获取子页面工单模板名称
        String sub_page_template_code = workFlowNodeColumnLinkage.get("sub_page_template_code").toString();
        //获取该工单模板的所有字段
        List<Map<String, Object>> subColumnList = workFlowTemplateService.getColumnListByWorkTemplateCode(methodParam, sub_page_template_code);
        Map<String, Object> work_flow_node_column_linkage_condition = (Map<String, Object>) workFlowNodeColumnLinkage.get("work_flow_node_column_linkage_condition");
        //都有扩展字段列表  进行比对
        List<Map<String, Object>> workFlowNodeColumnExtList = (List<Map<String, Object>>) worksFlowNodeColumnInfo.get("work_flow_node_column_ext_list");
        if ("1".equals(param.get("isNew").toString()) && RegexUtil.optNotNull(workFlowNodeColumnExtList).isPresent()) {
            Map<String, Object> workFlowNodeTemplateInfo = workFlowTemplateService.getSubPageWorkFlowNodeTemplateInfo(methodParam, sub_page_template_code);
            List<Map<String, Object>> workFlowColumnExtList = (List<Map<String, Object>>) workFlowNodeTemplateInfo.get("work_flow_node_column_list");
            //该子页面的每个字段的特殊属性值为key  field_form_code为value 为下面维保计划带来的字段取值用
            Map<String, Object> keys = new HashMap<>();
            for (Map<String, Object> workExt : workFlowColumnExtList) {
                if (RegexUtil.optNotNull(workExt.get("work_flow_node_column_ext_list")).isPresent()) {
                    List<Map<String, Object>> extList = (List<Map<String, Object>>) workExt.get("work_flow_node_column_ext_list");
                    for (Map<String, Object> ext : extList) {
                        keys.put(ext.get("field_value").toString(), workExt.get("field_form_code"));
                    }
                }
            }
            for (Map<String, Object> workFlowNodeColumnExt : workFlowNodeColumnExtList) {
                //判断该字段是否是维保计划带过来的任务项 职安健 工具
                if (RegexUtil.optIsPresentStr(workFlowNodeColumnExt.get("field_name")) && "plan_to_works".equals(workFlowNodeColumnExt.get("field_name"))) {
                    JSONArray mewJsonArray = new JSONArray();
                    if (RegexUtil.optIsPresentStr(param.get(workFlowNodeColumnExt.get("field_value")))) {
                        JSONArray jsonArray = JSONArray.fromObject(param.get(workFlowNodeColumnExt.get("field_value")));
                        for (int i = 0; i < jsonArray.size(); i++) {
                            JSONObject jsonObject = JSONObject.fromObject(jsonArray.get(i));
                            Iterator<String> it = jsonObject.keys();
                            JSONObject jsonObject1 = new JSONObject();
                            while (it.hasNext()) {
                                // 获得key
                                String key = it.next();
                                String value = jsonObject.getString(key);
                                //根据key去字段库获取field_form_code
                                if (RegexUtil.optIsPresentStr(keys.get(key)) && RegexUtil.optIsPresentStr(value)) {
                                    jsonObject1.put(keys.get(key), value);
                                }
                            }
                            mewJsonArray.add(jsonObject1);
                        }
                    }
                    field_value = mewJsonArray.toString();
                    worksFlowNodeColumnInfo.put("field_value", field_value);
                    worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), worksFlowNodeColumnInfo);
                }
            }
            //如果有对应的中间表 存入中间表
            if (RegexUtil.optIsPresentStr(field_value)) {
                String field_code = worksFlowNodeColumnInfo.get("field_code").toString();
                Map<String, Object> subColumnMap = new HashMap<>();
                for (Map<String, Object> subColumn : subColumnList) {
                    subColumnMap.put(subColumn.get("field_form_code").toString(), subColumn.get("field_code"));
                }
                List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(field_value);
                List<Map<String, Object>> addList = new ArrayList<>();
                for (Map<String, Object> map : list
                ) {
                    Map<String, Object> addMap = new HashMap<>();
                    for (String sub_field_form_code : map.keySet()) {
                        if (RegexUtil.optIsPresentStr(subColumnMap.get(sub_field_form_code)) && RegexUtil.optIsPresentStr(map.get(sub_field_form_code))) {
                            if (SensConstant.BUSINESS_NO_3001.equals(RegexUtil.optStrOrBlank(param.get("business_type_id"))) || SensConstant.BUSINESS_NO_3003.equals(RegexUtil.optStrOrBlank(param.get("business_type_id")))) {
                                if ("quantity".equals(subColumnMap.get(sub_field_form_code).toString()) && Float.valueOf(RegexUtil.optStrOrNull(map.get(sub_field_form_code))) < 0) {
                                    throw new SenscloudException(LangConstant.TITLE_FT, new String[]{LangConstant.TITLE_AAT_V});//数量不能为负
                                }
                            }
                            addMap.put(subColumnMap.get(sub_field_form_code).toString(), map.get(sub_field_form_code));
                        }
                    }
                    //新建工单
                    addMap.put("sub_work_code", sub_work_code);
                    addList.add(addMap);
                }
                //根据字段名从静态字典获取要存入中间表的字段名和表名
                List<Map<String, Object>> table_columns = selectOptionService.getTargetTableList(methodParam, "work_sub_page", field_code);
                if (RegexUtil.optNotNull(table_columns).isPresent() && table_columns.size() > 0) {
                    for (Map<String, Object> table_column : table_columns) {
                        if ("_sc_work_bom".equals(table_column.get("name").toString()) && !addList.contains("bom_id")) {
                            for (int i = 0; i < addList.size(); i++) {
                                addList.get(i).put("bom_source_type", 2);
                            }
                        }
                        //根据工单模板删除原来数据
                        worksMapper.deleteSubTableBySubWorkCode(methodParam.getSchemaName(), table_column.get("name").toString(), sub_work_code);
                        commonUtilService.batchInsertDataList(methodParam.getSchemaName(), addList, table_column.get("name").toString(), table_column.get("code").toString());
                    }
                }
                param.put(field_code, addList);
            }
        }
        if (RegexUtil.optIsPresentStr(work_flow_node_column_linkage_condition.get("condition"))) {
            String condition = work_flow_node_column_linkage_condition.get("condition").toString();
            JSONObject jsonObject = JSONObject.fromObject(condition);
            JSONArray page = JSONArray.fromObject(jsonObject.get("page"));
            if (page.size() > 0) {
                String field_code = worksFlowNodeColumnInfo.get("field_code").toString();
                Map<String, Object> subColumnMap = new HashMap<>();
                for (Map<String, Object> subColumn : subColumnList) {
                    subColumnMap.put(subColumn.get("field_form_code").toString(), subColumn.get("field_code"));
                }
                worksMapper.updateSubWorksDetailColumnBySubWorkCode(methodParam.getSchemaName(), Integer.valueOf(worksFlowNodeColumnInfo.get("id").toString()), sub_page_template_code, field_value);
                //将子页面对象工单模板的字段 存入works_detail_cloumn存为父子级
                for (Map<String, Object> subColumn : subColumnList) {
                    subColumn.put("parent_id", worksFlowNodeColumnInfo.get("id"));
                    subColumn.put("sub_work_code", sub_work_code);
                    worksMapper.insertWorksDetailColumn(methodParam.getSchemaName(), subColumn);
                }
                //如果有对应的中间表 存入中间表
                if (RegexUtil.optIsPresentStr(field_value)) {
                    List<Map<String, Object>> list = (List<Map<String, Object>>) JSONArray.fromObject(field_value);
                    List<Map<String, Object>> addList = new ArrayList<>();
                    for (Map<String, Object> map : list
                    ) {
                        Map<String, Object> addMap = new HashMap<>();
                        for (String sub_field_form_code : map.keySet()) {
                            if (RegexUtil.optIsPresentStr(subColumnMap.get(sub_field_form_code)) && RegexUtil.optIsPresentStr(map.get(sub_field_form_code))) {
                                if (SensConstant.BUSINESS_NO_3001.equals(RegexUtil.optStrOrBlank(param.get("business_type_id"))) || SensConstant.BUSINESS_NO_3003.equals(RegexUtil.optStrOrBlank(param.get("business_type_id")))) {
                                    if ("quantity".equals(subColumnMap.get(sub_field_form_code).toString()) && Float.valueOf(RegexUtil.optStrOrNull(map.get(sub_field_form_code))) < 0) {
                                        throw new SenscloudException(LangConstant.TITLE_FT, new String[]{LangConstant.TITLE_AAT_V});//数量不能为负
                                    }
                                }
                                addMap.put(subColumnMap.get(sub_field_form_code).toString(), map.get(sub_field_form_code));
                            }
                        }
                        //新建工单
                        if (RegexUtil.optIsPresentStr(param.get("isNew"))
                                && "1".equals(param.get("isNew").toString())
                                && RegexUtil.optIsPresentStr(param.get("isMain"))
                                && "1".equals(param.get("isMain").toString())) {
                            //如果设备为使用状态 创建保养工单
                            if (RegexUtil.optIsPresentStr(addMap.get("enter_state")) && "is_use".equals(addMap.get("enter_state").toString())
                                    && RegexUtil.optIsPresentStr(addMap.get("is_use_sate")) && "1".equals(addMap.get("is_use_sate").toString())) {
                                addMap.put("relation_id", addMap.get("asset_id"));
                                addMap.put("relation_type", 2);
                                if (!RegexUtil.optIsPresentStr(addMap.get("running_status_id"))) {
                                    addMap.remove("running_status_id");
                                }
                                createMaintainWorks(methodParam, addMap);
                            } else if (RegexUtil.optIsPresentStr(addMap.get("enter_state")) && "is_fault".equals(addMap.get("enter_state").toString())) {
                                //如果设备为故障状态 创建维修工单
                                addMap.put("relation_id", addMap.get("asset_id"));
                                addMap.put("relation_type", 2);
                                if (!RegexUtil.optIsPresentStr(addMap.get("running_status_id"))) {
                                    addMap.remove("running_status_id");
                                }
                                createRepairWorks(methodParam, addMap);
                            }
                        }
                        addMap.put("sub_work_code", sub_work_code);
                        addList.add(addMap);
                    }
                    //根据字段名从静态字典获取要存入中间表的字段名和表名
                    List<Map<String, Object>> table_columns = selectOptionService.getTargetTableList(methodParam, "work_sub_page", field_code);
                    if (RegexUtil.optNotNull(table_columns).isPresent() && table_columns.size() > 0) {
                        for (Map<String, Object> table_column : table_columns) {
                            //根据工单模板删除原来数据
                            worksMapper.deleteSubTableBySubWorkCode(methodParam.getSchemaName(), table_column.get("name").toString(), sub_work_code);
                            commonUtilService.batchInsertDataList(methodParam.getSchemaName(), addList, table_column.get("name").toString(), table_column.get("code").toString());
                        }
                    }
                    param.put(field_code, addList);
                }
            }
        }
    }

    /**
     * 将工单流程节点字段信息转换成key为field_form_code value为columnList
     */
    private Map<String, Object> getNodeColumnsByNodeInfo(Map<String, Object> nodeInfo) {
        List<Map<String, Object>> nodeColumnList = (List<Map<String, Object>>) nodeInfo.get("work_flow_node_column_list");
        for (Map<String, Object> column : nodeColumnList) {
            nodeInfo.put(column.get("field_form_code").toString(), column);
        }
        return nodeInfo;
    }

    /**
     * 扫码签到
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public Map<String, Object> codeScanningSign(MethodParam methodParam, Map<String, Object> paramMap) {
        String id = RegexUtil.optNotBlankStrOrExp(paramMap.get("id"), LangConstant.TEXT_K, new String[]{LangConstant.TITLE_QR_A}); // 二维码信息不存在
        String sub_work_code = RegexUtil.optNotBlankStrOrExp(paramMap.get("sub_work_code"), LangConstant.TEXT_K, new String[]{LangConstant.TITLE_QR_A}); // 二维码信息不存在
        //根据工单类型id获取业务类型
        Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", Integer.valueOf(RegexUtil.optNotBlankStrOrExp(paramMap.get("work_type_id"), LangConstant.TEXT_K, new String[]{LangConstant.TITLE_QR_A})));
        String business_type_id = RegexUtil.optStrOrBlank(work_type.get("business_type_id"));
        if (SensConstant.BUSINESS_NO_1001.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1002.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1003.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1004.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1005.equals(business_type_id)
                || SensConstant.BUSINESS_NO_1006.equals(business_type_id)) {
            Map<String, Object> result = null;
            Map<String, Object> works_detail = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), sub_work_code);
            if (RegexUtil.optIsPresentStr(works_detail.get("relation_type"))
                    && RegexUtil.optIsPresentStr(works_detail.get("relation_id"))) {
                Integer relation_type = RegexUtil.optIntegerOrVal(works_detail.get("relation_type"), 0, methodParam, "", null);
                String relation_id = RegexUtil.optStrOrBlank(works_detail.get("relation_id"));
                if (1 == relation_type) {//设备位置
                    RegexUtil.falseExp(id.equals(relation_id), "扫码信息不一致");
                    result = RegexUtil.optMapOrExpNullInfo(assetPositionService.getPositionInfoByCode(methodParam, relation_id), LangConstant.TITLE_QR_A); // 二维码信息不存在
                } else if (2 == relation_type) {//设备id
                    result = RegexUtil.optMapOrExpNullInfo(assetDataService.getAssetByIdOrCode(methodParam, relation_id), LangConstant.TITLE_QR_A); // 二维码信息不存在
                    RegexUtil.falseExp(id.equals(relation_id) || id.equals(RegexUtil.optStrOrBlank(result.get("asset_code"))), "扫码信息不一致");
                }
            }
            RegexUtil.optMapOrExpNullInfo(result, LangConstant.TITLE_QR_A); // 二维码信息不存在
            Date now = new Date();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String begin_time = df.format(now);
            works_detail.put("begin_time", now);
            works_detail.put("sign_in_time", now);
            worksMapper.updateWorksDetailByCode(methodParam.getSchemaName(), works_detail);
            Map<String, Object> begin_time_map = worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(methodParam.getSchemaName(), sub_work_code, "begin_time");
            if (RegexUtil.optNotNull(begin_time_map).isPresent()) {
                begin_time_map.put("field_value", begin_time);
                worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), begin_time_map);
            }
            return result;
        }
        throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_QR_A}); // 二维码信息不存在
    }

    /**
     * 查询紧急度
     *
     * @param schemaName
     * @param company_id
     * @param userLang   * @return
     */
    @Override
    public List<Map<String, Object>> queryCloudDataList(String schemaName, Long company_id, String userLang, String
            dataType) {
//        return worksMapper.queryCloudDataList(schemaName, company_id, userLang, dataType);
        return cacheUtilService.handleQueryCloudDataList(schemaName, company_id, userLang, dataType);
    }

    /**
     * 根据field_form_code获取工单节点字段信息和工单详情字段信息进行比对
     */
    private void updateWorksDetailColumnByFormCode(MethodParam methodParam, Map<String, Object> param,
                                                   String sub_work_code,
                                                   String field_form_code,
                                                   String field_value,
                                                   Map<String, Object> workFlowNodeColumn,
                                                   Map<String, Object> worksDetailColumn,
                                                   List<String> flow_keys,
                                                   JSONArray logArr) {

        //工单流程节点有该字段 工单详情字段信息也有 进行更新操作
        if (workFlowNodeColumn.containsKey(field_form_code) && worksDetailColumn.containsKey(field_form_code)) {
            Map<String, Object> worksFlowNodeColumnInfo = (Map<String, Object>) workFlowNodeColumn.get(field_form_code);
            Map<String, Object> worksDetailColumnInfo = (Map<String, Object>) worksDetailColumn.get(field_form_code);
            String field_code = worksFlowNodeColumnInfo.get("field_code").toString();
            worksFlowNodeColumnInfo.put("sub_work_code", sub_work_code);
            worksFlowNodeColumnInfo.put("field_value", field_value);
            worksMapper.updateWorksDetailColumnByCode(methodParam.getSchemaName(), worksFlowNodeColumnInfo);
            //日志：更新了该字段的工单详情字段信息
            logArr.add(LangUtil.doSetLogArray("工单详情字段属性更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_A, LangUtil.compareMap(worksFlowNodeColumnInfo, worksDetailColumnInfo).toString()}));
            if (worksFlowNodeColumnInfo.containsKey("work_flow_node_column_ext_list") && worksDetailColumnInfo.containsKey("works_detail_column_ext_list")) {
                //都有扩展字段列表  进行比对
                List<Map<String, Object>> workFlowNodeColumnExtList = (List<Map<String, Object>>) worksFlowNodeColumnInfo.get("work_flow_node_column_ext_list");
                List<Map<String, Object>> worksDetailColumnExtList = (List<Map<String, Object>>) worksDetailColumnInfo.get("works_detail_column_ext_list");
                for (Map<String, Object> workFlowNodeColumnExt : workFlowNodeColumnExtList
                ) {
                    //判断该字段是否是存入工作流的字段
                    if (RegexUtil.optIsPresentStr(workFlowNodeColumnExt.get("field_name")) && "to_flow".equals(workFlowNodeColumnExt.get("field_name"))) {
                        flow_keys.add(field_code);
                    }
                    workFlowNodeColumnExt.put("sub_work_code", sub_work_code);
                    //是否进行新增
                    boolean isAdd = true;
                    for (Map<String, Object> worksDetailColumnExt : worksDetailColumnExtList
                    ) {
                        //如果名称一样就更新
                        if (workFlowNodeColumnExt.get("field_name").equals(worksDetailColumnExt.get("field_name"))) {
                            //进行更新扩展字段操作
                            worksMapper.updateWorksDetailColumnExtByCode(methodParam.getSchemaName(), workFlowNodeColumnExt);
                            //日志：工单详情字段扩展属性更新
                            logArr.add(LangUtil.doSetLogArray("工单详情字段扩展属性更新", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_N, LangUtil.compareMap(workFlowNodeColumnExt, worksDetailColumnExt).toString()}));
                            isAdd = false;
                        }
                    }
                    if (isAdd) {
                        //进行新增扩展字段操作
                        worksMapper.insertWorksDetailColumnExt(methodParam.getSchemaName(), workFlowNodeColumnExt);
                        //日志：新增工单详情字段扩展属性
                        logArr.add(LangUtil.doSetLogArray("新增工单详情字段扩展属性", LangConstant.BTN_NEW_A, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_N, workFlowNodeColumnExt.toString()}));
                    }
                }
            } else if (worksFlowNodeColumnInfo.containsKey("work_flow_node_column_ext_list") && !worksDetailColumnInfo.containsKey("works_detail_column_ext_list")) {
                List<Map<String, Object>> workFlowNodeColumnExtList = (List<Map<String, Object>>) worksFlowNodeColumnInfo.get("work_flow_node_column_ext_list");
                for (Map<String, Object> workFlowNodeColumnExt : workFlowNodeColumnExtList
                ) {
                    //判断该字段是否是存入工作流的字段
                    if (RegexUtil.optIsPresentStr(workFlowNodeColumnExt.get("field_name")) && "to_flow".equals(workFlowNodeColumnExt.get("field_name"))) {
                        flow_keys.add(field_code);
                    }
                    workFlowNodeColumnExt.put("sub_work_code", sub_work_code);
                    //进行新增扩展字段操作
                    worksMapper.insertWorksDetailColumnExt(methodParam.getSchemaName(), workFlowNodeColumnExt);
                    //日志：新增工单详情字段扩展属性
                    logArr.add(LangUtil.doSetLogArray("新增工单详情字段扩展属性", LangConstant.BTN_NEW_A, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_N, workFlowNodeColumnExt.toString()}));
                }
            }
            if (worksFlowNodeColumnInfo.containsKey("work_flow_node_column_linkage_list") && worksDetailColumnInfo.containsKey("works_detail_column_linkage_list")) {
                //都有联动  进行比对
                List<Map<String, Object>> workFlowNodeColumnLinkageList = (List<Map<String, Object>>) worksFlowNodeColumnInfo.get("work_flow_node_column_linkage_list");
                List<Map<String, Object>> worksDetailColumnLinkageList = (List<Map<String, Object>>) worksDetailColumnInfo.get("works_detail_column_linkage_list");
                List<Map<String, Object>> newWorkFlowNodeColumnLinkageList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) workFlowNodeColumnLinkageList);
                RegexUtil.optNotNullList(workFlowNodeColumnLinkageList).ifPresent(l -> {
                    l.forEach(e -> {
                        if (Objects.equals(e.get("is_data_from_page"), "1")) {
                            newWorkFlowNodeColumnLinkageList.clear();
                            newWorkFlowNodeColumnLinkageList.add(e);
                            return;
                        }
                    });
                });
                for (Map<String, Object> workFlowNodeColumnLinkage : newWorkFlowNodeColumnLinkageList
                ) {
                    //是子页面
                    if ("4".equals(workFlowNodeColumnLinkage.get("linkage_type"))) {
                        dealSubPage(methodParam, param, sub_work_code, worksFlowNodeColumnInfo, workFlowNodeColumnLinkage, field_value);
                    }
                    workFlowNodeColumnLinkage.put("sub_work_code", sub_work_code);
                    //是否进行新增
                    boolean isAdd = true;
                    for (Map<String, Object> worksDetailColumnLinkage : worksDetailColumnLinkageList
                    ) {//如果一样不需要更新
                        if (workFlowNodeColumnLinkage.get("linkage_type").equals(worksDetailColumnLinkage.get("linkage_type"))) {
                            isAdd = false;
                            if (workFlowNodeColumnLinkage.containsKey("work_flow_node_column_linkage_condition") && worksDetailColumnLinkage.containsKey("works_detail_column_linkage_condition")) {
                                Map<String, Object> workFlowNodeColumnLinkageCondition = (Map<String, Object>) workFlowNodeColumnLinkage.get("work_flow_node_column_linkage_condition");
                                Map<String, Object> worksDetailColumnLinkageCondition = (Map<String, Object>) worksDetailColumnLinkage.get("works_detail_column_linkage_condition");
                                workFlowNodeColumnLinkageCondition.put("sub_work_code", sub_work_code);
                                workFlowNodeColumnLinkageCondition.put("linkage_id", worksDetailColumnLinkageCondition.get("id"));
                                worksMapper.updateWorksDetailColumnLinkageConditionById(methodParam.getSchemaName(), workFlowNodeColumnLinkageCondition);
                                //日志：更新工单详情字段联动条件
                                logArr.add(LangUtil.doSetLogArray("更新工单详情字段联动条件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AC, LangUtil.compareMap(workFlowNodeColumnLinkage, worksDetailColumnLinkage).toString()}));
                            }
                        }
                    }
                    if (isAdd) {
                        //进行新增操作
                        worksMapper.insertWorksDetailColumnLinkage(methodParam.getSchemaName(), workFlowNodeColumnLinkage);
                        //日志：新增工单详情联动
                        logArr.add(LangUtil.doSetLogArray("新增工单详情联动", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_AB, workFlowNodeColumnLinkage.toString()}));
                        if (workFlowNodeColumnLinkage.containsKey("work_flow_node_column_linkage_condition")) {
                            Map<String, Object> workFlowNodeColumnLinkageCondition = (Map<String, Object>) workFlowNodeColumnLinkage.get("work_flow_node_column_linkage_condition");
                            workFlowNodeColumnLinkageCondition.put("sub_work_code", sub_work_code);
                            workFlowNodeColumnLinkageCondition.put("linkage_id", workFlowNodeColumnLinkage.get("id"));
                            if (workFlowNodeColumnLinkageCondition.get("condition") instanceof com.alibaba.fastjson.JSONObject) {
                                com.alibaba.fastjson.JSONObject cond = (com.alibaba.fastjson.JSONObject) workFlowNodeColumnLinkageCondition.get("condition");
                                workFlowNodeColumnLinkageCondition.put("condition", cond.toString());
                            }
                            //新增联动条件表
                            worksMapper.insertWorksDetailColumnLinkageCondition(methodParam.getSchemaName(), workFlowNodeColumnLinkageCondition);
                            //日志：新增工单详情联动条件
                            logArr.add(LangUtil.doSetLogArray("新增工单详情联动条件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_AC, workFlowNodeColumnLinkageCondition.toString()}));
                        }
                    }
                }
            }
            //工单流程节点有该字段 工单详情字段信息没有 进行新增操作
        } else if (workFlowNodeColumn.containsKey(field_form_code) && !worksDetailColumn.containsKey(field_form_code)) {
            Map<String, Object> worksFlowNodeColumnInfo = (Map<String, Object>) workFlowNodeColumn.get(field_form_code);
            String field_code = worksFlowNodeColumnInfo.get("field_code").toString();
            if (!"works_log".equals(field_code)) {
                worksFlowNodeColumnInfo.put("sub_work_code", sub_work_code);
                worksFlowNodeColumnInfo.put("field_value", field_value);
                worksMapper.insertWorksDetailColumn(methodParam.getSchemaName(), worksFlowNodeColumnInfo);
                //日志：新增工单详情字段
                logArr.add(LangUtil.doSetLogArray("新增工单详情字段", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_A, worksFlowNodeColumnInfo.toString()}));
                //扩展表
                if (worksFlowNodeColumnInfo.containsKey("work_flow_node_column_ext_list")) {
                    List<Map<String, Object>> workFlowNodeColumnExtList = (List<Map<String, Object>>) worksFlowNodeColumnInfo.get("work_flow_node_column_ext_list");
                    for (Map<String, Object> workFlowNodeColumnExt : workFlowNodeColumnExtList
                    ) {
                        //判断该字段是否是存入工作流的字段
                        if (RegexUtil.optIsPresentStr(workFlowNodeColumnExt.get("field_name")) && "to_flow".equals(workFlowNodeColumnExt.get("field_name"))) {
                            flow_keys.add(field_code);
                        }

                        workFlowNodeColumnExt.put("sub_work_code", sub_work_code);
                        worksMapper.insertWorksDetailColumnExt(methodParam.getSchemaName(), workFlowNodeColumnExt);
                        //日志：新增工单详情字段扩展属性
                        logArr.add(LangUtil.doSetLogArray("新增工单详情字段扩展属性", LangConstant.BTN_NEW_A, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_N, workFlowNodeColumnExt.toString()}));
                    }
                }
                //联动表
                if (worksFlowNodeColumnInfo.containsKey("work_flow_node_column_linkage_list")) {
                    List<Map<String, Object>> workFlowNodeColumnLinkageList = (List<Map<String, Object>>) worksFlowNodeColumnInfo.get("work_flow_node_column_linkage_list");
                    List<Map<String, Object>> newWorkFlowNodeColumnLinkageList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) workFlowNodeColumnLinkageList);
                    RegexUtil.optNotNullList(workFlowNodeColumnLinkageList).ifPresent(l -> {
                        l.forEach(e -> {
                            if (Objects.equals(e.get("is_data_from_page"), "1")) {
                                newWorkFlowNodeColumnLinkageList.clear();
                                newWorkFlowNodeColumnLinkageList.add(e);
                                return;
                            }
                        });
                    });
                    for (Map<String, Object> workFlowNodeColumnLinkage : newWorkFlowNodeColumnLinkageList
                    ) {
                        //是子页面
                        if ("4".equals(workFlowNodeColumnLinkage.get("linkage_type"))) {
                            dealSubPage(methodParam, param, sub_work_code, worksFlowNodeColumnInfo, workFlowNodeColumnLinkage, field_value);
                        }
                        workFlowNodeColumnLinkage.put("sub_work_code", sub_work_code);
                        worksMapper.insertWorksDetailColumnLinkage(methodParam.getSchemaName(), workFlowNodeColumnLinkage);
                        //日志：新增工单详情联动
                        logArr.add(LangUtil.doSetLogArray("新增工单详情联动", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_AB, workFlowNodeColumnLinkage.toString()}));
                        //联动条件表
                        if (workFlowNodeColumnLinkage.containsKey("work_flow_node_column_linkage_condition")) {
                            Map<String, Object> condition = (Map<String, Object>) workFlowNodeColumnLinkage.get("work_flow_node_column_linkage_condition");
                            condition.put("linkage_id", workFlowNodeColumnLinkage.get("id"));
                            condition.put("sub_work_code", sub_work_code);
                            if (condition.get("condition") instanceof com.alibaba.fastjson.JSONObject) {
                                com.alibaba.fastjson.JSONObject cond = (com.alibaba.fastjson.JSONObject) condition.get("condition");
                                condition.put("condition", cond.toString());
                            }
                            worksMapper.insertWorksDetailColumnLinkageCondition(methodParam.getSchemaName(), condition);
                            //日志：新增工单详情联动条件
                            logArr.add(LangUtil.doSetLogArray("新增工单详情联动条件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_AC, condition.toString()}));
                        }
                    }
                }
            }
        }
    }
}
