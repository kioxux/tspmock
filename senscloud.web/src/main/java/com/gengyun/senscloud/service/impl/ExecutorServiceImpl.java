package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.ExecutorService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.service.dynamic.WorkScheduleService;
import com.gengyun.senscloud.util.RegexUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * ExecutorService class 派案策略接口实现类
 *
 * @author Zys
 * @date 2020/03/19
 */
@Service
public class ExecutorServiceImpl implements ExecutorService {

    @Resource
    WorkScheduleService workScheduleService;

    @Resource
    SelectOptionService selectOptionService;

    @Resource
    SystemConfigService systemConfigService;

    @Resource
    UserMapper userMapper;

    /**
     * 查找人员
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> getConditionalUsers(MethodParam methodParam, Map<String, String> paramMap) {
        try {

            String distributionTypeFromParam = paramMap.get("distributionOrExecute");
            String distributionTypeFromAttr = paramMap.get("distributionOrExecute");
            String distributionTypeFromMap = null;
            if (RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("distributionOrExecute")) {
                distributionTypeFromMap = paramMap.get("distributionOrExecute");
            }
            if (RegexUtil.optNotNull(distributionTypeFromAttr).isPresent() && !RegexUtil.optNotNull(distributionTypeFromParam).isPresent() &&
                    !RegexUtil.optNotNull(distributionTypeFromMap).isPresent()) {
                return this.getExecutorsByRoles(methodParam, paramMap);
            }
            String inServiceFacilityFromParam = paramMap.get("in_service");
            String inServiceFacilityFromAttr = paramMap.get("in_service");
            String inServiceFacilityFromMap = null;
            if (RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("in_service")) {
                inServiceFacilityFromMap = paramMap.get("in_service");
            }
            if (!RegexUtil.optNotNull(inServiceFacilityFromParam).isPresent() && !RegexUtil.optNotNull(inServiceFacilityFromAttr).isPresent()
                    && !RegexUtil.optNotNull(inServiceFacilityFromMap).isPresent()) {
                return this.getExecutorsByPool(methodParam, paramMap);
            }
            List<Map<String, Object>> userList = this.getExecutorsByRoles(methodParam, paramMap);
            List<Map<String, Object>> poolUserList = this.getExecutorsByPool(methodParam, paramMap);
            if (!RegexUtil.optNotNull(userList).isPresent()) {
                return poolUserList;
            }
            if (RegexUtil.optNotNull(poolUserList).isPresent()) {
                userList.addAll(poolUserList);
            }
            return userList;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 按工单池查找操作人-接受人
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> getExecutorsByPool(MethodParam methodParam, Map<String, String> paramMap) {
        Integer distributionOrExecute = null;
        try {
            String distributionTypeFromParam = paramMap.get("distributionOrExecute");
            String distributionTypeFromAttr = paramMap.get("distributionOrExecute");
            String distributionTypeFromMap = null;
            if (RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("distributionOrExecute")) {
                distributionTypeFromMap = paramMap.get("distributionOrExecute");
            }
            if (!RegexUtil.optNotNull(distributionTypeFromAttr).isPresent() && !RegexUtil.optNotNull(distributionTypeFromParam).isPresent() &&
                    !RegexUtil.optNotNull(distributionTypeFromMap).isPresent()) {
                return null;
            }
            // 设备
            String relationId = paramMap.get("relationId");
            if (!RegexUtil.optNotNull(relationId).isPresent()) {
                relationId = paramMap.get("relationId");
            }
            if (!RegexUtil.optNotNull(relationId).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("relation_id")) {
                relationId = paramMap.get("relation_id");
            }
            // 工单类型
            String workTypeId = paramMap.get("workTypeId");
            if (!RegexUtil.optNotNull(workTypeId).isPresent()) {
                workTypeId = paramMap.get("workTypeId");
            }
            if (!RegexUtil.optNotNull(workTypeId).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("work_type_id")) {
                workTypeId = paramMap.get("work_type_id");
            }
            // 特殊角色权限
            String roleIds = paramMap.get("roleIds");
            if (!RegexUtil.optNotNull(roleIds).isPresent()) {
                roleIds = paramMap.get("roleIds");
            }
            if (!RegexUtil.optNotNull(roleIds).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("roleIds")) {
                roleIds = paramMap.get("roleIds");
            }

            Timestamp now = new Timestamp(System.currentTimeMillis());

            //设备关联人员 优先级最高
            if (RegexUtil.optNotNull(roleIds).isPresent() && RegexUtil.optNotNull(relationId).isPresent() && RegexUtil.optNotNull(workTypeId).isPresent()) {
                List<Map<String, Object>> doUser = workScheduleService.getDoExecuteUserByAssetId(methodParam.getSchemaName(), relationId, Integer.parseInt(workTypeId), roleIds, now);
                //设备配了具体的关联人员 优先级最高
                if (RegexUtil.optNotNull(doUser).isPresent()) {
                    return doUser;
                }
            }
            //是否是设备位置主导
            boolean isPositionGuide = true;
            //默认位置
            String guideKey = "positionCode";
            String guideKeyMap = "position_code";
            //去系统配置派案策略 设备组织还是设备位置主导
            Map<String, Object> dataList = systemConfigService.getMapSystemConfigData(methodParam.getSchemaName(), SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
            if (null == dataList || !RegexUtil.optNotNull(dataList.get("setting_value")).isPresent() || dataList.get("setting_value").equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
                isPositionGuide = false;
            }
            // 工单池
            String poolId = paramMap.get("poolId");
            if (!RegexUtil.optNotNull(poolId).isPresent()) {
                poolId = paramMap.get("poolId");
            }
            if (!RegexUtil.optNotNull(poolId).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("pool_id")) {
                poolId = paramMap.get("pool_id");
            }
            //组织或位置
            if (!isPositionGuide) {
                guideKey = "facilityId";
                guideKeyMap = "facility_id";
            }
            String positionCodeStr = paramMap.get(guideKey);
            if (!RegexUtil.optNotNull(positionCodeStr).isPresent()) {
                positionCodeStr = paramMap.get(guideKey);
            }
            if (!RegexUtil.optNotNull(positionCodeStr).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey(guideKeyMap)) {
                positionCodeStr = paramMap.get(guideKeyMap);
            }
            int categoryId = 0;
            String noneStr = LangConstant.TITLE_AAAD_H;
            Integer businessTypeId = null;
            try {
                // 业务类型
                businessTypeId = Integer.valueOf(selectOptionService.getWorkTypeById(methodParam, Integer.valueOf(workTypeId)).get("business_type_id").toString());
            } catch (Exception btiExp) {

            }
            if (RegexUtil.optNotNull(relationId).isPresent()) {
                String categoryIdStr = selectOptionService.getSelectOptionAttrByCode(methodParam, "asset", relationId, "category_id");
                if (RegexUtil.optNotNull(categoryIdStr).isPresent() && !noneStr.equals(categoryIdStr)) {
                    categoryId = Integer.parseInt(categoryIdStr);
                }
            }
            //组织id转换组织No 用于like查询
            if (!isPositionGuide) {
                try {
                    // fid转fNo
                    positionCodeStr = selectOptionService.getSelectOptionAttrByCode(methodParam, "facilities", positionCodeStr, "relation");
                } catch (Exception fidExp) {

                }
            }
            if (RegexUtil.optNotNull(roleIds).isPresent()) {
                return workScheduleService.getExecutorByWorkTypeAndRoleIds(methodParam.getSchemaName(), positionCodeStr, poolId, businessTypeId, now, now, roleIds, categoryId);
            }
            //特殊角色权限
            String rolePermissionKey = paramMap.get("rolePermissionKey");
            if (!RegexUtil.optNotNull(rolePermissionKey).isPresent()) {
                rolePermissionKey = paramMap.get("rolePermissionKey");
            }
            if (!RegexUtil.optNotNull(rolePermissionKey).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey(rolePermissionKey)) {
                rolePermissionKey = paramMap.get("rolePermissionKey");
            }
            if (RegexUtil.optNotNull(distributionTypeFromParam).isPresent()) {
                distributionOrExecute = Integer.parseInt(distributionTypeFromParam);
            }
            if (!RegexUtil.optNotNull(distributionOrExecute).isPresent() && RegexUtil.optNotNull(distributionTypeFromAttr).isPresent()) {
                distributionOrExecute = Integer.valueOf(distributionTypeFromAttr);
            }
            if (!RegexUtil.optNotNull(distributionOrExecute).isPresent() && RegexUtil.optNotNull(distributionTypeFromMap).isPresent()) {
                distributionOrExecute = Integer.parseInt(distributionTypeFromMap);
            }
            if (RegexUtil.optNotNull(rolePermissionKey).isPresent()) {
                return workScheduleService.getExecutorByWorkTypeAndPermission(methodParam.getSchemaName(), positionCodeStr, poolId, businessTypeId, now, now, distributionOrExecute, rolePermissionKey, categoryId);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 按角色查找操作人-接受人
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> getExecutorsByRoles(MethodParam methodParam, Map<String, String> paramMap) {
        try {
            // 设备
            String relationId = paramMap.get("relationId");
            if (RegexUtil.optNotNull(relationId).isPresent()) {
                relationId = paramMap.get("relationId");
            }
            if (RegexUtil.optNotNull(relationId).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("relation_id")) {
                relationId = paramMap.get("relation_id");
            }
            // 工单类型
            String workTypeId = paramMap.get("workTypeId");
            if (!RegexUtil.optNotNull(workTypeId).isPresent()) {
                workTypeId = paramMap.get("workTypeId");
            }
            if (!RegexUtil.optNotNull(workTypeId).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("work_type_id")) {
                workTypeId = paramMap.get("work_type_id");
            }
            // 特殊角色权限
            String roleIds = paramMap.get("roleIds");
            if (!RegexUtil.optNotNull(roleIds).isPresent()) {
                roleIds = paramMap.get("roleIds");
            }
            if (!RegexUtil.optNotNull(roleIds).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("roleIds")) {
                roleIds = paramMap.get("roleIds");
            }
            //设备关联人员 优先级最高
            if (RegexUtil.optNotNull(roleIds).isPresent() && RegexUtil.optNotNull(relationId).isPresent() && RegexUtil.optNotNull(workTypeId).isPresent()) {
                Timestamp now = new Timestamp(System.currentTimeMillis());
                List<Map<String, Object>> doUser = workScheduleService.getDoExecuteUserByAssetId(methodParam.getSchemaName(), relationId, Integer.parseInt(workTypeId), roleIds, now);
                //设备配了具体的关联人员 优先级最高
                if (RegexUtil.optNotNull(doUser).isPresent()) {
                    return doUser;
                }
            }
            //是否是设备位置主导
            boolean isPositionGuide = true;
            //是否是服务商维修
            boolean isCustomerGuide = false;
            //默认位置
            String guideKey = "positionCode";
            String guideKeyMap = "position_code";
            //去系统配置派案策略 设备组织还是设备位置主导
            Map<String, Object> dataList = systemConfigService.getMapSystemConfigData(methodParam.getSchemaName(), SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
            if (null == dataList || !RegexUtil.optNotNull(dataList.get("setting_value")).isPresent() || dataList.get("setting_value").equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
                isPositionGuide = false;
            }
            //组织或位置
            if (!isPositionGuide) {
                guideKey = "facilityId";
                guideKeyMap = "facility_id";
            }
            String positionCodeStr = paramMap.get(guideKey);
            if (!RegexUtil.optNotNull(positionCodeStr).isPresent()) {
                positionCodeStr = paramMap.get(guideKey);
            }
            if (!RegexUtil.optNotNull(positionCodeStr).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey(guideKeyMap)) {
                positionCodeStr = paramMap.get(guideKeyMap);
            }
            // 设备类别
            String categoryId = null;
            if (RegexUtil.optNotNull(relationId).isPresent()) {
                categoryId = selectOptionService.getSelectOptionAttrByCode(methodParam, "asset", relationId, "category_id");
            }
            String noneStr = LangConstant.TITLE_AAAD_H;//无
            if (!RegexUtil.optNotNull(categoryId).isPresent() || noneStr.equals(categoryId)) {
                categoryId = "0";
            }
            // 服务商
            String inServiceFacilityId = paramMap.get("in_service");
            String distributionOrExecuteStr = paramMap.get("distributionOrExecute");
            if (!RegexUtil.optNotNull(inServiceFacilityId).isPresent()) {
                inServiceFacilityId = paramMap.get("in_service");
            }
            if (!RegexUtil.optNotNull(inServiceFacilityId).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("in_service")) {
                inServiceFacilityId = paramMap.get("in_service");
            }
            if (!RegexUtil.optNotNull(distributionOrExecuteStr).isPresent()) {
                distributionOrExecuteStr = paramMap.get("distributionOrExecute");
            }
            if (!RegexUtil.optNotNull(distributionOrExecuteStr).isPresent() && RegexUtil.optNotNull(paramMap).isPresent() && paramMap.containsKey("distributionOrExecute")) {
                distributionOrExecuteStr = paramMap.get("distributionOrExecute");
            }
            if (RegexUtil.optNotNull(inServiceFacilityId).isPresent() && RegexUtil.optNotNull(distributionOrExecuteStr).isPresent()) {
                isCustomerGuide = true;
                positionCodeStr = inServiceFacilityId;
                isPositionGuide = false;
            }
            if (null == roleIds) {
                return null;
            }
            List<String> roleIdList = Arrays.asList(roleIds.split(","));
            //内部设备位置主导派案策略
            if (isPositionGuide) {
                return userMapper.getUsersByRoles(methodParam.getSchemaName(), roleIdList, positionCodeStr, Integer.parseInt(categoryId));
            }
            //组织id转换组织No 用于like查询
            try {
                // fid转fNo
                positionCodeStr = selectOptionService.getSelectOptionAttrByCode(methodParam, "facilities", positionCodeStr, "relation");
            } catch (Exception fidExp) {
            }
            //服务商维修
            if (isCustomerGuide) {
                return userMapper.getUsersByRolesAndCustomer(methodParam.getSchemaName(), roleIdList, positionCodeStr, Integer.parseInt(categoryId));
            }
            //原设备组织主导派案策略
            return userMapper.findUsersByRoles(methodParam.getSchemaName(), roleIdList, positionCodeStr, Integer.parseInt(categoryId));
        } catch (Exception e) {
            return null;
        }
    }
}
