package com.gengyun.senscloud.service.system;

import java.util.List;
import java.util.Map;

/**
 * 微信公众号消息管理
 * User: sps
 * Date: 2020/05/29
 * Time: 上午11:20
 */
public interface WxGzhMsgService {
    /**
     * 微信消息发送
     *
     * @param schemaName  数据库
     * @param senderId    发送人id
     * @param openId      openId
     * @param templateId  templateId
     * @param contentInfo 消息内容
     */
    void sendWxMsg(String schemaName, String senderId, String openId, String templateId, Map<String, Object> contentInfo);

    /**
     * 更新微信Token（定时任务）
     *
     * @param schemaName 数据库
     */
    void checkWxAccessToken(String schemaName);

    /**
     * 获取微信Token
     *
     * @param schemaName 数据库
     * @return 微信Token
     */
    String getAccessTokenByDb(String schemaName);

    /**
     * 新增关注的公众号用户
     *
     * @param schemaName 数据库
     * @param wxCode     微信号
     * @param openId     微信公众号open_id
     * @param unionId    微信union_id
     */
    void newWxGzhUser(String schemaName, String wxCode, String openId, String unionId);

    /**
     * 删除关注的公众号用户
     *
     * @param wxCode 微信号
     * @param openId 微信公众号open_id
     */
    void cutWxGzhUser(String wxCode, String openId);

    /**
     * 更新用户微信公众号openid
     *
     * @param schemaName 数据库
     * @param unionId    微信union_id
     * @param openId     微信公众号open_id
     * @return 成功数量
     */
    int modifyUserWxGzhOpenId(String schemaName, String unionId, String openId);

    /**
     * 清空用户微信公众号openid
     *
     * @param schemaName 数据库
     * @param openId     微信公众号open_id
     * @return 成功数量
     */
    int cutUserWxGzhOpenId(String schemaName, String openId);

    /**
     * 根据条件查询全局数据
     *
     * @param weixinCode
     * @return
     */
    List<Map<String, Object>> queryGlobalInfoListByWeixinCode(String weixinCode);
}
