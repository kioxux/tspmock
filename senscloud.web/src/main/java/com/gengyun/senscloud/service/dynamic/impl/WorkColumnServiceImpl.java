package com.gengyun.senscloud.service.dynamic.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.SelectOptionsMapper;
import com.gengyun.senscloud.mapper.WorkColumnMapper;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.service.WorkColumnService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import io.swagger.annotations.Api;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Administrator on 2018/4/28.
 */
@Api(tags = "字段库")
@Service
public class WorkColumnServiceImpl implements WorkColumnService {

    @Resource
    WorkColumnMapper workColumnMapper;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    LogsService logService;
    @Resource
    CompanyService companyService;
    @Value("${senscloud.com.url}")
    String url;

    /**
     * 获取字段库列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getWorkColumnListForPage(MethodParam methodParam, WorkColumnSearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();

        // 获取按条件拼接的sql
        String searchWord = this.getWorkColumnWhereString(methodParam, param);
//        int total = workColumnMapper.countWorkColumnList(schemaName, searchWord); // 获取字段的总数
        int total = findWorkColumnList(methodParam, searchWord, param).size(); // 获取字段的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " ORDER BY w.field_form_code ";
//            searchWord += pagination;
            List<Map<String, Object>> workColumnList = null;
//            workColumnList = workColumnMapper.findWorkColumnList(schemaName, searchWord);
            workColumnList = findWorkColumnList(methodParam, searchWord, param);
            result.put("rows", workColumnList.stream().skip((methodParam.getPageNumber()-1)*methodParam.getPageSize()).limit(methodParam.getPageSize()).collect(Collectors.toList()));
        }
        result.put("total", total);
        return result;
    }

    /**
     * 获取字段主键
     *
     * @return
     */
    @Override
    public synchronized String getWorkColumnKey() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(Constants.DATE_FMT_FILE);
        String format = sdf1.format(new Date());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return format;
    }

    /**
     * 新增字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public synchronized void newWorkColumn(MethodParam methodParam, Map<String, Object> data) {
        String isAdd = RegexUtil.optStrOrNull(data.get("isAdd"));
        //TODO save_type 默认自定义字段，后续需求明确再改
        data.put("save_type", "0");
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_NG}); // 字段不能为空
//        String field_form_code = RegexUtil.optStrOrExpNotNull(data.get("field_form_code"), LangConstant.TITLE_QD);// 唯一键不能为空
        String field_form_code = "record".equals(isAdd) ? RegexUtil.optStrOrExpNotNull(data.get("field_form_code"), LangConstant.TITLE_QD) : this.getWorkColumnKey();
        data.put("field_form_code",field_form_code);
        RegexUtil.trueExp(workColumnMapper.countInfoByFieldFormCode(schemaName, field_form_code) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_QD}); // 唯一键重复，请修改
        RegexUtil.optStrOrExpNotNull(data.get("field_name"), LangConstant.TITLE_NAME_C);   // 显示名称不能为空
        String fieldCode = RegexUtil.optStrOrExpNotNull(data.get("field_code"), LangConstant.TITLE_NAME_AB_R);// 字段名称不能为空
        RegexUtil.trueExp(workColumnMapper.countFieldCode(schemaName, fieldCode) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_NAME_AB_R}); // 字段名称重复，请修改
        if (!"record".equals(isAdd)) {
            RegexUtil.trueExp(JSON.parseObject(SenscloudUtil.askPublic("countInfoByFieldFormCode",new HashMap<String, Object>(){{put("field_form_code",field_form_code);}},url),int.class) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_QD}); // 验证public里唯一建是否重复
            RegexUtil.trueExp(JSON.parseObject(SenscloudUtil.askPublic("countFieldCode",new HashMap<String, Object>(){{put("field_code",fieldCode);}},url),int.class) > 0, LangConstant.MSG_H, new String[]{LangConstant.TITLE_NAME_AB_R}); // 验证public里字段名称是否重复
        }
        RegexUtil.optStrOrExpNotNull(data.get("field_right"), LangConstant.TITLE_B_C); // 操作权限不能为空
        String field_section_type = RegexUtil.optStrOrExpNotNull(data.get("field_section_type"), LangConstant.TITLE_QE);// 所在区块不能为空
        String field_view_type = RegexUtil.optStrOrExpNotNull(data.get("field_view_type"), LangConstant.TITLE_QF);// 控件类型不能为空
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "block", field_section_type), LangConstant.TITLE_QE); // 所在区块不存在
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "work_column", field_view_type), LangConstant.TITLE_QF); // 控件类型不存在
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            String exList1 = data.get("extList").toString();
            JSONArray extList = JSONArray.fromObject(exList1);
            List<WorkColumnExAdd> list = JSONArray.toList(extList, WorkColumnExAdd.class);
            if (list.size() > 0) {
                for (WorkColumnExAdd workColumnExAdd : list) {
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_name(), LangConstant.TITLE_AAT_P);   // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_write_type(), LangConstant.TITLE_AAT_P); // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_value(), workColumnExAdd.getField_name()); // 特殊属性的名称值不能为空
                    workColumnExAdd.setField_form_code(field_form_code);
                    workColumnExAdd.setCreate_time(nowTime);
                    workColumnExAdd.setCreate_user_id(methodParam.getUserId());
//                    if(Constants.COLUMN_EXT_FIELD_VALUE.equals(workColumnExAdd.getField_name())){
//                        data.put("field_value",workColumnExAdd.getField_value());
//                    }
                }
                data.put("extList", list);
            } else {
                data.remove("extList");
            }
        }
        data.put("create_time", nowTime);
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        int result = workColumnMapper.insertWorkColumn(data);
        if (!"record".equals(isAdd)) {
            int publicResult = JSON.parseObject(SenscloudUtil.askPublic("insertColumnsWorkRelation",data,url),int.class);
            RegexUtil.intExp(publicResult, LangConstant.MSG_BK); // 未知错误！
        }
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            List<WorkColumnExAdd> extList = (List<WorkColumnExAdd>) data.get("extList");
            workColumnMapper.insertWorkColumnExBatch(schemaName, extList);
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, field_form_code, LangUtil.doSetLogArray("字段库字段新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_NG})); //字段
    }

    /**
     * 编辑字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkColumn(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        String schemaName = methodParam.getSchemaName();
        String field_form_code = data.get("field_form_code").toString();
        Map<String, Object> dbWorkColumnInfo = this.getWorkColumnDetailByFieldFormCode(methodParam, field_form_code);
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optStrOrExpNotNull(data.get("field_name"), LangConstant.TITLE_NAME_C);   // 显示名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("field_code"), LangConstant.TITLE_NAME_AB_R); // 字段名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("field_right"), LangConstant.TITLE_B_C); // 操作权限不能为空
        String field_section_type = RegexUtil.optStrOrExpNotNull(data.get("field_section_type"), LangConstant.TITLE_QE);// 所在区块不能为空
        String field_view_type = RegexUtil.optStrOrExpNotNull(data.get("field_view_type"), LangConstant.TITLE_QF);// 控件类型不能为空
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "block", field_section_type), LangConstant.TITLE_QE); // 所在区块不存在
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "work_column", field_view_type), LangConstant.TITLE_QF); // 控件类型不存在
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            String exList1 = data.get("extList").toString();
            JSONArray extList = JSONArray.fromObject(exList1);
            for (int x = 0; x < extList.size(); x++) {
                JSONObject extObject = extList.getJSONObject(x);
                extObject.remove("create_time");
            }
            List<WorkColumnExAdd> list = JSONArray.toList(extList, WorkColumnExAdd.class);
            if (list.size() > 0) {
                for (WorkColumnExAdd workColumnExAdd : list) {
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_name(), LangConstant.TITLE_AAT_P);   // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_write_type(), LangConstant.TITLE_AAT_P); // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_value(), workColumnExAdd.getField_name()); // 特殊属性的名称值不能为空
                    workColumnExAdd.setField_form_code(field_form_code);
                    workColumnExAdd.setCreate_time(nowTime);
                    workColumnExAdd.setCreate_user_id(methodParam.getUserId());
//                    if(Constants.COLUMN_EXT_FIELD_VALUE.equals(workColumnExAdd.getField_name())){
//                        data.put("field_value",workColumnExAdd.getField_value());
//                    }
                }
                data.put("extList", list);
            } else {
                data.remove("extList");
            }
        }
        data.put("create_time", nowTime);
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        data.remove("field_code");
        int result = workColumnMapper.updateWorkColumn(data);
        workColumnMapper.deleteWorkColumnExtByFieldFormCode(schemaName, field_form_code);
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            List<WorkColumnExAdd> extList = (List<WorkColumnExAdd>) data.get("extList");
            workColumnMapper.insertWorkColumnExBatch(schemaName, extList);
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        if (RegexUtil.optNotNull(dbWorkColumnInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbWorkColumnInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, field_form_code, LangUtil.doSetLogArray("编辑了字段库字段", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_NG, log.toString()}));
        }
    }

    /**
     * 删除选中字段
     *
     * @param methodParam
     * @param field_form_code
     */
    @Override
    public void cutWorkColumnByFieldFormCode(MethodParam methodParam, String field_form_code) {
        String schemaName = methodParam.getSchemaName();
        RegexUtil.trueExp(workColumnMapper.findCountUseByFieldFormCode(schemaName, field_form_code) > 0, LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AC_V, LangConstant.TITLE_NG, LangConstant.TITLE_NG}); // 工单模板下还有字段，不能删除该字段
        workColumnMapper.deleteWorkColumnByFieldFormCode(schemaName, field_form_code);
        workColumnMapper.deleteWorkColumnExtByFieldFormCode(schemaName, field_form_code);
        SenscloudUtil.askPublic("deleteColumnsWorkRelation",new HashMap<String, Object>(){{put("field_form_code",field_form_code);}},url);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002,field_form_code  , LangUtil.doSetLogArray("字段库字段删除", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_NG, field_form_code}));
    }

    /**
     * 获取字段明细信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getWorkColumnInfoByFieldFormCode(MethodParam methodParam, WorkColumnSearchParam param) {
        Map<String, Object> info = this.getWorkColumnDetailByFieldFormCode(methodParam, param.getField_form_code());
        List<Map<String, Object>> extList = this.getWorkColumnExtByFieldFormCode(methodParam, param.getField_form_code());
        if (RegexUtil.optIsPresentList(extList)) {
            info.put("extList", extList);
        }
        return info;
    }

    /**
     * 克隆字段
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getWorkColumnCloneByFieldFormCode(MethodParam methodParam, WorkColumnSearchParam param) {
        Map<String, Object> info = this.getWorkColumnDetailByFieldFormCode(methodParam, param.getField_form_code());
        String key = getWorkColumnKey();
        info.put("field_form_code", key);
        List<Map<String, Object>> extList = this.getWorkColumnExtByFieldFormCode(methodParam, param.getField_form_code());
        if (RegexUtil.optIsPresentList(extList)) {
            for (Map<String, Object> map : extList) {
                map.put("field_form_code", key);
            }
            info.put("extList", extList);
        }
        return info;
    }

    /**
     * 查询新增字段查询字段库字段列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkColumnListForTemplate(MethodParam methodParam, WorkColumnSearchParam param) {
        String schemaName = methodParam.getSchemaName();
        // 获取按条件拼接的sql
        String searchWord = this.getWorkColumnWhereString(methodParam, param);
            searchWord += " ORDER BY w.field_form_code ";
            List<Map<String, Object>> workColumnList = null;
//            workColumnList = workColumnMapper.findWorkColumnList(schemaName, searchWord);
            workColumnList = findWorkTemplateColumnList(methodParam, searchWord, param);
        return workColumnList;
    }

    /**
     * 通过field_form_code获取字段特殊属性
     *
     * @param methodParam
     * @param fieldFormCode
     * @return
     */
    private List<Map<String, Object>> getWorkColumnExtByFieldFormCode(MethodParam methodParam, String fieldFormCode) {
        return workColumnMapper.findWorkColumnExtByFieldFormCode(methodParam.getSchemaName(), fieldFormCode);
    }

    /**
     * 通过field_form_code获取字段本身信息
     *
     * @param methodParam
     * @param field_form_code
     * @return
     */
    private Map<String, Object> getWorkColumnDetailByFieldFormCode(MethodParam methodParam, String field_form_code) {
        return RegexUtil.optMapOrExpNullInfo(workColumnMapper.findWorkColumnByFieldFormCode(methodParam.getSchemaName(), field_form_code), LangConstant.TITLE_NG); // 字段信息不存在
    }

    /**
     * 获取字段列表查询条件
     *
     * @param methodParam
     * @param param
     * @return
     */
    private String getWorkColumnWhereString(MethodParam methodParam, WorkColumnSearchParam param) {
        StringBuffer whereString = new StringBuffer(" where 1=1 ");
//        RegexUtil.optNotBlankStrOpt(param.getId()).ifPresent(e -> whereString.append(" and b.id = '").append(e).append("' "));
//        RegexUtil.optNotBlankStrOpt(param.getIsUseSearch()).ifPresent(e -> whereString.append(" and b.is_use = '").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(param.getWork_template_code()).ifPresent(e -> whereString.append(" AND NOT EXISTS (SELECT 1 FROM ").append(methodParam.getSchemaName()).append("._sc_work_template t LEFT JOIN ").append(methodParam.getSchemaName()).append("._sc_work_flow_node_column wfnc ON t.work_template_code=wfnc.node_id WHERE wfnc.field_form_code = w.field_form_code AND t.work_template_code = '").append(e).append("' ) "));
        RegexUtil.optNotBlankStrOpt(param.getField_view_types()).ifPresent(e -> whereString.append(" and w.field_view_type in (").append(DataChangeUtil.joinByStr(e)).append(") "));
//        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower((select (c.resource->>w.field_name)::jsonb->>'").append(methodParam.getUserLang()).append("' from public.company_resource c where c.company_id = ").append(methodParam.getCompanyId()).append(")) like ")
//                .append(e).append(" or lower(w.field_code) like ").append(e).append(")"));
//        RegexUtil.optNotNullArrayOpt(param.getTypeIdsSearch()).ifPresent(e -> {
//            StringBuffer typeIdStr = new StringBuffer();
//            for (String typeId : e) {
//                RegexUtil.optNotBlankStrOpt(typeId).ifPresent(im -> typeIdStr.append(" ,").append(im));
//            }
//            RegexUtil.optNotBlankStrOpt(typeIdStr).ifPresent(ps -> whereString.append(" AND b.type_id IN ( ").append(ps.substring(2)).append(") "));
//        });
//        RegexUtil.optNotNullArrayOpt(param.getSuppliersSearch()).ifPresent(e -> {
//            StringBuffer supplierIdStr = new StringBuffer();
//            for (String supplierId : e) {
//                RegexUtil.optNotBlankStrOpt(supplierId).ifPresent(im -> supplierIdStr.append(" ,").append(im));
//            }
//            RegexUtil.optNotBlankStrOpt(supplierIdStr).ifPresent(ps -> whereString.append(" AND s.supplier_id IN ( ").append(ps.substring(2)).append(") "));
//        });
        return whereString.toString();
    }

    public List<Map<String, Object>> findWorkColumnList(MethodParam methodParam, String searchWord, WorkColumnSearchParam param) {
        List<Map<String, Object>> columnsWorkRelationList = JSON.parseObject(SenscloudUtil.askPublic("findColumnsWorkRelation",new HashMap<>(),url), List.class);
        List<Map<String, Object>> workColumnList = RegexUtil.optNotNullListOrNew(workColumnMapper.findWorkColumnList(methodParam.getSchemaName(), searchWord));
        List<Map<String, Object>> handWorkColumnList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) columnsWorkRelationList);
        columnsWorkRelationList.forEach(c -> {
            for (Map<String, Object> item : workColumnList) {
                if (Objects.equals(c.get("field_form_code"), item.get("field_form_code")) && Objects.equals(c.get("field_code"), item.get("field_code"))) {
                    handWorkColumnList.remove(c);
                    item.put("isEdit", true);
                    handWorkColumnList.add(item);
                    break;
                }
            }
        });
        List<Map<String, Object>> workColumnListNew = new ArrayList<>();
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(methodParam.getCompanyId(), "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(handWorkColumnList) && RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).isPresent()) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                List<Map<String, Object>> finalWorkColumnList = new ArrayList<>();
                handWorkColumnList.forEach(sd -> {
                    String value = "";
                    if (RegexUtil.optIsPresentStr(dd.get(sd.get("field_name")))) {
                        value = RegexUtil.optStrOrBlank(JSONObject.fromObject(dd.get(sd.get("field_name"))).get(methodParam.getUserLang()));
                    }
                    if (value.toLowerCase().indexOf(RegexUtil.optStrOrBlank(param.getKeywordSearch()).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("field_code")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(param.getKeywordSearch()).toLowerCase()) > -1) {
                        finalWorkColumnList.add(sd);
                    }
                });
                workColumnListNew = finalWorkColumnList;
            }
        } else {
            workColumnListNew = handWorkColumnList;
        }
        return workColumnListNew;
    }

    public List<Map<String, Object>> findWorkTemplateColumnList(MethodParam methodParam, String searchWord, WorkColumnSearchParam param) {
        List<Map<String, Object>> workColumnList = RegexUtil.optNotNullListOrNew(workColumnMapper.findWorkColumnList(methodParam.getSchemaName(), searchWord));
        List<Map<String, Object>> workColumnListNew = new ArrayList<>();
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(methodParam.getCompanyId(), "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(workColumnList) && RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).isPresent()) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                List<Map<String, Object>> finalWorkColumnList = new ArrayList<>();
                workColumnList.forEach(sd -> {
                    String value = "";
                    if (RegexUtil.optIsPresentStr(dd.get(sd.get("field_name")))) {
                        value = RegexUtil.optStrOrBlank(JSONObject.fromObject(dd.get(sd.get("field_name"))).get(methodParam.getUserLang()));
                    }
                    if (value.toLowerCase().indexOf(RegexUtil.optStrOrBlank(param.getKeywordSearch()).toLowerCase()) > -1 || RegexUtil.optStrOrBlank(sd.get("field_code")).toLowerCase().indexOf(RegexUtil.optStrOrBlank(param.getKeywordSearch()).toLowerCase()) > -1) {
                        finalWorkColumnList.add(sd);
                    }
                });
                workColumnListNew = finalWorkColumnList;
            }
        } else {
            workColumnListNew = workColumnList;
        }
        return workColumnListNew;
    }

}
