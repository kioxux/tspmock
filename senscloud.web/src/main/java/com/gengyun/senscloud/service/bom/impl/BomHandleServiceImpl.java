package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomHandleService;
import org.springframework.stereotype.Service;

/**
 * 工单处理（备件）
 * User: sps
 * Date: 2019/11/05
 * Time: 下午17:00
 */
@Service
public class BomHandleServiceImpl implements BomHandleService {
//    private static final Logger logger = LoggerFactory.getLogger(BomHandleServiceImpl.class);
//
//    @Autowired
//    MetadataWorkMapper metadataWorkMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    private BomInStockService bomInStockService;
//    @Autowired
//    private BomAllotService bomAllotService;
//    @Autowired
//    private BomRecipientService bomRecipientService;
//    @Autowired
//    private BomDiacardService bomDiacardService;
//    @Autowired
//    private BomInventoryService bomInventoryService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    /**
//     * 整合小程序回显字段及数据
//     *
//     * @param schemaName
//     * @param account
//     * @param workTemplateCode
//     * @param subWorkCode
//     * @param businessNo
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    public JSONObject getWxWorkHandleInfo(String schemaName, String account, String workTemplateCode, String subWorkCode, String businessNo, HttpServletRequest request) throws Exception {
//        MetadataWork template = metadataWorkMapper.queryById(schemaName, workTemplateCode); // 获取新模板
//        Map<String, Object> oldData = null; // 获取原工单数据
//        JSONObject result = new JSONObject();
//        if (null != subWorkCode && !"".equals(subWorkCode) && !"undefined".equals(subWorkCode)) {
//            // 备件入库
//            if (SensConstant.BUSINESS_NO_31.equals(businessNo)) {
//                oldData = bomInStockService.queryBomInStockById(schemaName, subWorkCode); // 获取原数据
//                // 备件调拨
//            } else if (SensConstant.BUSINESS_NO_32.equals(businessNo)) {
//                oldData = bomAllotService.queryBomAllotById(schemaName, subWorkCode); // 获取原数据
//                // 备件领用
//            } else if (SensConstant.BUSINESS_NO_33.equals(businessNo)) {
//                oldData = bomRecipientService.queryBomRecipientById(schemaName, subWorkCode); // 获取原数据
//                // 备件报废
//            } else if (SensConstant.BUSINESS_NO_34.equals(businessNo)) {
//                oldData = bomDiacardService.queryBomDiacardById(schemaName, subWorkCode); // 获取原数据
//                // 备件盘点
//            } else if (SensConstant.BUSINESS_NO_35.equals(businessNo)) {
//                oldData = bomInventoryService.queryBomInventoryStockByCode(schemaName, subWorkCode); // 获取原数据
//            }
//
//            // 获取历史数据
//            try {
//                List<LogsData> historyList = commonUtilService.queryLogByBusinessNo(schemaName, subWorkCode);
//                //多时区处理
//                SCTimeZoneUtil.responseObjectListDataHandler(historyList, new String[]{"create_time"});
//                result.put("historyData", historyList);
//            } catch (Exception exp) {
//                logger.warn(selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_HIS_DATA) + subWorkCode);//历史信息未处理
//            }
//        } else {
//            oldData = new HashMap<String, Object>();
//            // 备件入库
//            if (SensConstant.BUSINESS_NO_31.equals(businessNo)) {
//                User user = AuthService.getLoginUser(request);
//                user = commonUtilService.checkUser(schemaName, user, request);
//                boolean feeRight = pagePermissionService.getPermissionByKey(schemaName, user.getId(), "bom_in_stock", "bis_fee");
//                oldData.put("feeRight", feeRight);
//            }
//        }
////        // 获取货币信息数据
////        try {
////            SystemConfigData scd = systemConfigService.getSystemConfigData(schemaName, SystemConfigConstant.CURRENT_CURRENCY);
////            result.put("currentCurrency", scd.getSettingValue());
////            String currentCurrencyName = selectOptionService.getOptionNameByCode(schemaName, "currency", scd.getSettingValue(), null);
////            result.put("currentCurrencyName", currentCurrencyName);
////        } catch (Exception exp) {
////            logger.warn(selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_CUR_INFO_DATA) + subWorkCode);//小程序货币信息未处理
////        }
////        long startTime = System.currentTimeMillis(); // 获取开始时间
//        Boolean isDistribute = false;
//        String isDistributeStr = (String) request.getParameter("isDistribute");
//        if (RegexUtil.isNotNull(isDistributeStr)) {
//            isDistribute = Boolean.valueOf(isDistributeStr);
//        }
//        dynamicCommonService.getPageInfo(schemaName, request, account, result, template, oldData, false, isDistribute, subWorkCode);
////        long endTime = System.currentTimeMillis(); // 获取结束时间
////        logger.info("小程序回显程序处理时间： " + (endTime - startTime) + "ms");
//        return result;
//    }
}
