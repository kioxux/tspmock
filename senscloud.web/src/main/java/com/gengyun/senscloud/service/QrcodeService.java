package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

public interface QrcodeService {
    String dosageForCode(MethodParam methodParam, String code);
}
