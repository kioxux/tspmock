package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.mapper.PlanWorkMapper;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.job.MaintainJobService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MaintainJobServiceImpl implements MaintainJobService {

    private static final Logger logger = LoggerFactory.getLogger(MaintainJobServiceImpl.class);

    @Resource
    PlanWorkService planWorkService;

    @Resource
    PlanWorkMapper planWorkMapper;

    @Resource
    SystemConfigService systemConfigService;

    @Resource
    CacheUtilService cacheUtilService;

    /**
     * 批量处理条数
     */
    private int BATCH_COUNT = 10;

    /**
     * 批量生成行事历的时间范围-单位：年
     */
    private int BATCH_YEAR = 3;

    /**
     * 行事历批量插入数据库的条数
     */
    private int BATCH_INSERT_COUNT = 1000;

    /**
     * 工作日：周一到周五（每周的第一天是周日）
     */
    private String CALENDAR_WORKDAY = "2,3,4,5,6";

    /**
     * 每周的第一天是周日
     */
    private Map<String, String> TRIGGER_TIME_TYPE_MAP = new HashMap() {{
        put(Constants.TRIGGER_TIME_TYPE_SUNDAY, "1");
        put(Constants.TRIGGER_TIME_TYPE_MONDY, "2");
        put(Constants.TRIGGER_TIME_TYPE_TUESDAY, "3");
        put(Constants.TRIGGER_TIME_TYPE_WEDNESDAY, "4");
        put(Constants.TRIGGER_TIME_TYPE_THURSDAY, "5");
        put(Constants.TRIGGER_TIME_TYPE_FRIDAY, "6");
        put(Constants.TRIGGER_TIME_TYPE_SATURDAY, "7");
    }};

    /**
     * 根据新增、修改的维保计划生成相应的行事历 —— 同步执行
     *
     * @param schemaName
     */
    @Override
    @Transactional
    public void cronJobToGeneratePlanWorkWithNewModel(String schemaName) {
        String sysCfgValueByCfgName = systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.PLAN_WORK_CALENDAR_GENERATE_YEAR);
        int batchYear;//生成行事历年限范围
        try {
            batchYear = RegexUtil.optIsPresentStr(sysCfgValueByCfgName) ? Integer.valueOf(sysCfgValueByCfgName) : BATCH_YEAR;
        } catch (NumberFormatException e) {
            batchYear = BATCH_YEAR;
        }
        //查找有效的未处理、重新编辑的计划，取前100条
        List<Map<String, Object>> planList = planWorkService.getExecutePlanWork(schemaName, BATCH_COUNT);
        if (RegexUtil.optIsPresentList(planList)) {
            StringBuffer planWorkCodes = new StringBuffer();
            planList.forEach(p -> {
                if (RegexUtil.optIsPresentStr(p.get("plan_code"))) {
                    planWorkCodes.append((planWorkCodes.length() > 0 ? "," : "") + "'" + p.get("plan_code") + "'");
                }
            });
            if (RegexUtil.optIsPresentStr(planWorkCodes.toString()))
                //更新计划的任务状态为处理中
            {
                planWorkService.updatePlanWorkJobStatusByPlanWorkCodeList(schemaName, planWorkCodes.toString(), StatusConstant.PLAN_WORK_JOB_STATUS_PROCESSING);
            }

            //遍历处理每一条工单计划，生成相应的计划行事历
            int finalBatchYear = batchYear;
            planList.forEach(p -> generatedPlanWorkCalendar(schemaName, finalBatchYear, p));
        }
    }

    /**
     * 行事历过期数据处理
     *
     * @param schemaName
     */
    @Override
    @Transactional
    public void expirePlanWorkCalendar(String schemaName) {
        planWorkMapper.expirePlanWorkCalendar(schemaName);
    }

    /**
     * 根据一条工单计划生成相应的计划行事历
     *
     * @param schemaName
     * @param batchYear
     * @param planWork
     */
    private void generatedPlanWorkCalendar(String schemaName, int batchYear, Map<String, Object> planWork) {
        String planWorkCode = RegexUtil.optStrOrNull(planWork.get("plan_code"));
        String status = RegexUtil.optStrOrBlank(planWork.get("job_status"));
        // 如果是修改计划后的重新生成，则要先清除已经生成的且还未生成工单的行事历
        RegexUtil.optEqualsOpt(status, String.valueOf(StatusConstant.PLAN_WORK_JOB_STATUS_MODIFIED)).ifPresent(s -> planWorkService.deletePlanWorkCalendarByPlanWorkCode(schemaName, planWorkCode));
        //获取该维保计划的所有维保对象规则列表
        List<Map<String, Object>> planWorkSettingCondition = planWorkService.getPlanWorkSettingCondition(schemaName, planWorkCode);
        if (RegexUtil.optIsPresentList(planWorkSettingCondition)) {
            Map<String, Map<String, Object>> assetMap = new HashMap<>();
            List<Map<String, Object>> timeList = new ArrayList<>();
            int doType = (Integer) planWork.get("do_type");//触发条件 1：所有条件满足执行；2：任一条件满足即执行
            String beginTime = RegexUtil.optStrOrBlank(planWork.get("begin_time"));
            String endTime = RegexUtil.optStrOrBlank(planWork.get("end_time"));
            Date now = new Date();
            //查询维保对象勾选全部设备时，符合条件的设备
            planWorkSettingCondition.forEach(ps -> getAssetByPlanWorkSetting(schemaName, assetMap, ps));
            //查询维保对象直接勾选的设备
            getAssetByPlanWorkCode(schemaName, assetMap, planWorkCode);
            //获取触发条件
            List<Map<String, Object>> triggles = planWorkService.getPlanWorkTriggleByPlanCode(schemaName, planWorkCode);
            //根据触发条件，计算需要生成行事历的日期列表
            getTimeList(batchYear, now, triggles, timeList, doType, beginTime, endTime);
            //获取维保计划的任务列表
            List<String> taskList = planWorkService.getTaskListByPlanWorkCode(schemaName, planWorkCode);
            //循环遍历日期列表、设备列表，生成相应的行事历
            generateCalendarForEachTimeAndAsset(schemaName, planWork, taskList, timeList, assetMap, planWorkCode, status, now);
        }
        //更新计划的任务状态为已完成
        planWorkService.updatePlanWorkJobStatusByPlanWorkCode(schemaName, planWorkCode, StatusConstant.PLAN_WORK_JOB_STATUS_FINISHED);
    }

    /**
     * 循环遍历日期列表、设备列表，生成相应的行事历
     *
     * @param schemaName
     * @param planWork
     * @param taskList
     * @param timeList
     * @param assetMap
     * @param planWorkCode
     * @param status
     * @param now
     */
    private void generateCalendarForEachTimeAndAsset(String schemaName, Map<String, Object> planWork, List<String> taskList, List<Map<String, Object>> timeList, Map<String, Map<String, Object>> assetMap, String planWorkCode, String status, Date now) {
        if (RegexUtil.optIsPresentList(timeList)) {
            String planName = RegexUtil.optStrOrBlank(planWork.get("plan_name"));
            String remark = RegexUtil.optStrOrBlank(planWork.get("remark"));
            String workTemplateCode = RegexUtil.optStrOrBlank(planWork.get("work_template_code"));
            String relationType = RegexUtil.optStrOrBlank(planWork.get("relation_type"));
            Integer priorityLevel = Integer.valueOf(String.valueOf(planWork.get("priority_level")));
            Integer workTypeId = Integer.valueOf(String.valueOf(planWork.get("work_type_id")));
            Integer autoGenerateBill = Integer.valueOf(String.valueOf(planWork.get("auto_generate_bill")));
            Integer generateTimePoint = planWork.get("generate_time_point") == null ? 0 : Integer.valueOf(String.valueOf(planWork.get("generate_time_point")));
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT);
            //查询参与团队
            List<Map<String, Object>> assistList = cacheUtilService.findAssistByPlanCode(schemaName, planWorkCode, "", null);
            int groupId = 0;
            String receiveUserId = "";
            if (RegexUtil.optIsPresentList(assistList)) {
                Map<String, Object> assist = assistList.get(assistList.size() > 1 ? new Random().nextInt(assistList.size() - 1) : 0);//随机取一个参与团队
                receiveUserId = RegexUtil.optStrOrBlank(assist.get("user_id"));
                groupId = Integer.valueOf(String.valueOf(assist.get("group_id")));
            }
            int finalGroupId = groupId;
            String finalReceiveUserId = receiveUserId;
            List<PlanWorkCalendarModel> planWorkCalendars = new ArrayList<>();
            timeList.forEach(t -> {
                boolean disperse = RegexUtil.optBool(t.get("disperse"));
                Date occurTime = (Date) t.get("occur_time");
                Date deadlineTime = (Date) t.get("deadline_time");
                if (disperse) {
                    String disperseDay = String.valueOf(t.get("disperseDay"));
                    List<Map<String, Object>> assetList = new ArrayList<>();
                    assetMap.forEach((k, v) -> assetList.add(v));
                    int batchAssetCount = new BigDecimal(assetList.size()).divide(new BigDecimal(disperseDay), 0, BigDecimal.ROUND_UP).intValue();//每天需要生成行事历的设备数量
                    for (int i = 0; i < assetList.size(); i++) {
                        int offset = i / batchAssetCount;//计算工单开始时间的偏移量
                        Map<String, Object> asset = assetList.get(i);
                        occurTime = offsetDateHandle(occurTime, offset);
                        deadlineTime = offsetDateHandle(deadlineTime, offset);
                        String assetId = String.valueOf(asset.get("id"));
                        //生成行事历并批量新增
                        insertPlanWorkCalendarts(schemaName, planWorkCalendars, status, assetId, sdf.format(occurTime), occurTime, deadlineTime, planWorkCode, now,
                                workTemplateCode, priorityLevel, planName, workTypeId, finalReceiveUserId, finalGroupId, Integer.valueOf(relationType),
                                String.valueOf(asset.get("position_code")), remark, autoGenerateBill, generateTimePoint);
                    }
                } else {
                    Date finalOccurTime = occurTime;
                    Date finalDeadlineTime = deadlineTime;
                    assetMap.forEach((k, v) -> {
                        String assetId = String.valueOf(v.get("id"));
                        //生成行事历并批量新增
                        insertPlanWorkCalendarts(schemaName, planWorkCalendars, status, assetId, sdf.format(finalOccurTime), finalOccurTime, finalDeadlineTime,
                                planWorkCode, now, workTemplateCode, priorityLevel, planName, workTypeId, finalReceiveUserId, finalGroupId, Integer.valueOf(relationType),
                                String.valueOf(v.get("position_code")), remark, autoGenerateBill, generateTimePoint);
                    });
                }
                if (RegexUtil.optIsPresentList(planWorkCalendars) && planWorkCalendars.size() > 0) {
                    planWorkService.insertBatchPlanWorkCalendars(schemaName, planWorkCalendars);
                    planWorkCalendars.clear();
                }
            });
        }
    }

    /**
     * 生成行事历并批量新增
     *
     * @param schemaName
     * @param planWorkCalendars
     * @param status
     * @param assetId
     * @param date
     * @param occurTime
     * @param deadlineTime
     * @param planWorkCode
     * @param now
     * @param workTemplateCode
     * @param priorityLevel
     * @param planName
     * @param workTypeId
     * @param receiveUserId
     * @param groupId
     * @param relationType
     * @param positionCode
     * @param remark
     */
    private void insertPlanWorkCalendarts(String schemaName, List<PlanWorkCalendarModel> planWorkCalendars, String status, String assetId, String date,
                                          Date occurTime, Date deadlineTime, String planWorkCode, Date now, String workTemplateCode, int priorityLevel,
                                          String planName, int workTypeId, String receiveUserId, int groupId, int relationType, String positionCode,
                                          String remark, int autoGenerateBill, int generateTimePoint) {
        // 如果是修改计划重新生成行事历，则要判断同一设备的同一天是否已经有行事历了，如果有则跳过不生成
        if (RegexUtil.optEqualsOpt(status, String.valueOf(StatusConstant.PLAN_WORK_JOB_STATUS_MODIFIED)).map(s -> planWorkService.countPlanWorkCalendarByAssetAndDate(schemaName, planWorkCode, assetId, date)).orElse(0) > 0) {
            return;
        }
        //给设备生成一条维保行事历
        planWorkCalendars.add(addPlanWorkCalendar(planWorkCode, now, workTemplateCode, priorityLevel, planName, workTypeId, receiveUserId, groupId, assetId, relationType,
                positionCode, remark, Constants.PLAN_WORK_CALENDAR_STATUS_NEW, new Timestamp(occurTime.getTime()), new Timestamp(deadlineTime.getTime()), autoGenerateBill, generateTimePoint));
        if (RegexUtil.optIsPresentList(planWorkCalendars) && planWorkCalendars.size() > BATCH_INSERT_COUNT) {
            //当待插入行事历列表条数达到批量处理条数时，插入到数据库，防止设备太多导致列表内存溢出
            planWorkService.insertBatchPlanWorkCalendars(schemaName, planWorkCalendars);
            planWorkCalendars.clear();
        }
    }

    /**
     * 生成维保行事历对象
     *
     * @param planWorkCode
     * @param now
     * @param workTemplateCode
     * @param priorityLevel
     * @param planName
     * @param workTypeId
     * @param receiveUserId
     * @param grouId
     * @param relationId
     * @param relationType
     * @param positionCode
     * @param remark
     * @param status
     * @param occurTime
     * @param deadlineTime
     * @param autoGenerateBill
     * @return
     */
    private PlanWorkCalendarModel addPlanWorkCalendar(String planWorkCode, Date now, String workTemplateCode, int priorityLevel, String planName,
                                                      int workTypeId, String receiveUserId, int grouId, String relationId, int relationType, String positionCode, String remark,
                                                      int status, Timestamp occurTime, Timestamp deadlineTime, int autoGenerateBill, int generateTimePoint) {
        PlanWorkCalendarModel calendar = new PlanWorkCalendarModel();
        calendar.setPlan_code(planWorkCode);
        calendar.setCreate_user_id("system");
        calendar.setCreate_time(new Timestamp(now.getTime()));
        calendar.setWork_template_code(workTemplateCode);
        calendar.setPriority_level(priorityLevel);
        calendar.setTitle(planName);
        calendar.setWork_type_id(workTypeId);
        calendar.setReceive_user_id(receiveUserId);
        calendar.setGroup_id(grouId);
        calendar.setRelation_id(relationId);
        calendar.setRelation_type(relationType);
        calendar.setPosition_code(positionCode);
        calendar.setRemark(remark);
        calendar.setStatus(status);
        calendar.setOccur_time(occurTime);
        calendar.setDeadline_time(deadlineTime);
        calendar.setAuto_generate_bill(autoGenerateBill);
        calendar.setGenerate_time_point(generateTimePoint);
        return calendar;
    }

    /**
     * 查询维保对象勾选全部设备时，符合条件的设备
     *
     * @param schemaName
     * @param assetMap
     * @param planWorkSetting
     */
    private void getAssetByPlanWorkSetting(String schemaName, Map<String, Map<String, Object>> assetMap, Map<String, Object> planWorkSetting) {
        RegexUtil.optNotNullMap(planWorkSetting).ifPresent(m -> {
            boolean isAllAsset = (Boolean) m.get("is_all_asset");
            if (isAllAsset) {
                //如果勾选了全部设备，则按条件查询符合当前筛选条件的所有设备
                StringBuffer condition = new StringBuffer();
                RegexUtil.optNotBlankStrOpt(m.get("position_code")).ifPresent(c -> condition.append(String.format(" AND a.position_code IN (%s)", Arrays.stream(c.split(",")).collect(Collectors.joining("','", "'", "'")))));
                RegexUtil.optNotBlankStrOpt(m.get("asset_category_id")).ifPresent(c -> condition.append(String.format(" AND a.category_id IN (%s)", Arrays.stream(c.split(",")).collect(Collectors.joining(",")))));
                RegexUtil.optNotBlankStrOpt(m.get("asset_model_id")).ifPresent(c -> condition.append(String.format(" AND a.asset_model_id IN (%s)", Arrays.stream(c.split(",")).collect(Collectors.joining(",")))));
                RegexUtil.optNotBlankStrOpt(m.get("enable_begin_date")).ifPresent(c -> condition.append(String.format(" AND a.enable_time >= '%s'", c)));
                RegexUtil.optNotBlankStrOpt(m.get("enable_end_date")).ifPresent(c -> condition.append(String.format(" AND a.enable_time <= '%s'", c)));
                List<Map<String, Object>> assetList = planWorkService.getAssetPlanByCondition(schemaName, condition.toString());
                if (RegexUtil.optIsPresentList(assetList)) {
                    assetList.forEach(a -> {
                        String assetId = String.valueOf(a.get("id"));
                        if (!assetMap.containsKey(assetId))//去除重复设备
                        {
                            assetMap.put(assetId, a);
                        }
                    });
                }
            }
        });
    }

    /**
     * 查询维保对象直接勾选的设备
     *
     * @param schemaName
     * @param assetMap
     * @param planWorkCode
     */
    private void getAssetByPlanWorkCode(String schemaName, Map<String, Map<String, Object>> assetMap, String planWorkCode) {
        List<Map<String, Object>> assetList = planWorkService.getAssetPlanByPlanWorkCode(schemaName, planWorkCode);
        if (RegexUtil.optIsPresentList(assetList)) {
            assetList.forEach(a -> {
                String assetId = String.valueOf(a.get("id"));
                if (!assetMap.containsKey(assetId))//去除重复设备
                {
                    assetMap.put(assetId, a);
                }
            });
        }
    }

    /**
     * 计算需要生成行事历的日期列表
     *
     * @param batchYear
     * @param now
     * @param triggles
     * @param timeList
     * @param doType
     * @param beginTime
     * @param endTime
     */
    private void getTimeList(int batchYear, Date now, List<Map<String, Object>> triggles, List<Map<String, Object>> timeList, int doType, String beginTime, String endTime) {
        if (!RegexUtil.optIsPresentList(triggles)) {
            return;
        }

        triggles.forEach(t -> {
            String periodType = String.valueOf(t.get("period_type"));//周期类型1：单次2：按天3：按周4：按月5：年
            String executeDay = String.valueOf(t.get("execute_day"));//执行日期
            String executeTime = String.valueOf(t.get("execute_time"));//执行时间
            String durationTime = String.valueOf(t.get("duration_time"));//持续时间
            String durationType = String.valueOf(t.get("duration_type"));//持续时间类型：minute：分钟 hour：小时 days：天
            Float periodValue = RegexUtil.optFloatOrNull(t.get("period_value"), "");//周期值（天数、月数等，为年时，表示第几月）
            String typeDaySelect = String.valueOf(t.get("type_day_select"));//天|周|工作日：day:天 week:周 work_day:工作日 mondy: 周一 tuesday：周二 wednesday：周三 thursday：周四 friday：周五 saturday：周六 sunday：周日
            String whichTypeDay = String.valueOf(t.get("which_type_day"));//第几天|周
            String typeWeekSelect = String.valueOf(t.get("type_week_select"));//周几：可多选，用“，”隔开
            switch (periodType) {
                case "1":
                    generatedSingleCalendar(timeList, executeTime, executeDay, durationTime, durationType);
                    break;
                case "2":
                    generatedDayCalendar(batchYear, timeList, now, executeTime, periodValue.intValue(), beginTime, endTime, durationTime, durationType);
                    break;
                case "3":
                    generatedWeekCalendar(timeList, now, executeTime, periodValue.intValue(), typeWeekSelect, beginTime, endTime, durationTime, durationType);
                    break;
                case "4":
                    generatedMonthCalendar(timeList, now, executeTime, periodValue.intValue(), typeDaySelect, whichTypeDay, beginTime, endTime, durationTime, durationType);
                    break;
                case "5":
                    generatedYearCalendar(timeList, now, executeTime, periodValue.intValue(), typeDaySelect, whichTypeDay, beginTime, endTime, durationTime, durationType);
                    break;
            }
        });
    }

    /**
     * 获取”单次“类型的行事历开始时间
     *
     * @param timeList
     * @param executeTime
     * @param executeDay
     * @param durationTime
     * @param durationType
     */
    private void generatedSingleCalendar(List<Map<String, Object>> timeList, String executeTime, String executeDay, String durationTime, String durationType) {
        try {
            Map<String, Object> map = new HashMap<>();
            Date occurTime = new SimpleDateFormat(Constants.DATE_FMT_SS).parse(executeDay + " " + executeTime);
            map.put("occur_time", occurTime);
            map.put("deadline_time", computeDeadlineTime(occurTime, durationTime, durationType));
            timeList.add(map);
        } catch (ParseException e) {
            logger.error("", e);
        }
    }

    /**
     * 获取”按天“类型的行事历开始时间
     *
     * @param timeList
     * @param now
     * @param executeTime
     * @param periodValue
     * @param beginTime
     * @param endTime
     * @param durationTime
     * @param durationType
     */
    private void generatedDayCalendar(int batchYear, List<Map<String, Object>> timeList, Date now, String executeTime, int periodValue, String beginTime, String endTime, String durationTime, String durationType) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            SimpleDateFormat sd = new SimpleDateFormat(Constants.DATE_FMT);
            Calendar cl = Calendar.getInstance();
            cl.setFirstDayOfWeek(Calendar.MONDAY);
            if (cl.getTime().before(sdf.parse(beginTime))) {
                cl.setTime(sdf.parse(beginTime));//如果计划开始时间大于当前时间，则按计划的开始时间计算
            }
            if (cl.getTime().after(sdf.parse(sd.format(now) + " " + executeTime))) {
                cl.add(Calendar.DATE, 1);//如果当天的计划执行时间点已经过了，则跳到明天开始计算
            }
            cl.setTime(sdf.parse(sd.format(cl.getTime()) + " " + executeTime));
            cl.add(Calendar.YEAR, batchYear);
            Date end = cl.getTime().before(sdf.parse(endTime)) ? cl.getTime() : sdf.parse(endTime);//获取截止时间
            cl.add(Calendar.YEAR, -batchYear);
            while (cl.getTime().compareTo(end) <= 0) {
                Map<String, Object> map = new HashMap<>();
                map.put("occur_time", cl.getTime());
                map.put("deadline_time", computeDeadlineTime(cl.getTime(), durationTime, durationType));
                map.put("disperse", true);//是否均匀分布生成行事历
                map.put("disperseDay", periodValue);//维保对象分布在几天内
                timeList.add(map);
                cl.add(Calendar.DATE, periodValue);
            }
        } catch (ParseException e) {
            logger.error("", e);
        }
    }

    /**
     * 获取”按周“类型的行事历开始时间
     *
     * @param timeList
     * @param now
     * @param executeTime
     * @param periodValue
     * @param typeWeekSelect
     * @param beginTime
     * @param endTime
     * @param durationTime
     * @param durationType
     */
    private void generatedWeekCalendar(List<Map<String, Object>> timeList, Date now, String executeTime, int periodValue, String typeWeekSelect, String beginTime, String endTime, String durationTime, String durationType) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            SimpleDateFormat sd = new SimpleDateFormat(Constants.DATE_FMT);
            Calendar cl = Calendar.getInstance();
//            cl.setFirstDayOfWeek(Calendar.MONDAY);
            if (cl.getTime().before(sdf.parse(beginTime))) {
                cl.setTime(sdf.parse(beginTime));//如果计划开始时间大于当前时间，则按计划的开始时间计算
            }
            if (cl.getTime().after(sdf.parse(sd.format(now) + " " + executeTime))) {
                cl.add(Calendar.DATE, 1);//如果当天的计划执行时间点已经过了，则跳到明天开始计算
            }
            cl.setTime(sdf.parse(sd.format(cl.getTime()) + " " + executeTime));
            cl.add(Calendar.YEAR, BATCH_YEAR);
            Date end = cl.getTime().before(sdf.parse(endTime)) ? cl.getTime() : sdf.parse(endTime);//获取截止时间
            cl.add(Calendar.YEAR, -BATCH_YEAR);
            int dayWeek = cl.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
            if (1 == dayWeek) {
                cl.add(Calendar.DAY_OF_MONTH, -1);
            }
            cl.setFirstDayOfWeek(Calendar.MONDAY);
            cl.setMinimalDaysInFirstWeek(7);
            while (cl.getTime().compareTo(end) <= 0) {
                int weekOfYear = cl.get(Calendar.WEEK_OF_YEAR);//不他明白
                while (cl.get(Calendar.WEEK_OF_YEAR) == weekOfYear) {//不他明白
                    int weekDay = cl.get(Calendar.DAY_OF_WEEK);
                    weekDay = weekDay - 1;
                    if (weekDay == 0) {
                        weekDay = 7;
                    }
                    String day = String.valueOf(weekDay);
                    if (typeWeekSelect.contains(day)) {
                        if (cl.getTime().compareTo(end) <= 0) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("occur_time", cl.getTime());
                            map.put("deadline_time", computeDeadlineTime(cl.getTime(), durationTime, durationType));
                            timeList.add(map);
                        }
                    }
                    cl.add(Calendar.DATE, 1);
                }
                cl.add(Calendar.DAY_OF_MONTH, -1);
                cl.add(Calendar.WEEK_OF_YEAR, periodValue);
                cl.add(Calendar.DAY_OF_MONTH, -6);
            }
        } catch (ParseException e) {
            logger.error("", e);
        }
    }

    /**
     * 获取”按月“类型的行事历开始时间
     *
     * @param timeList
     * @param now
     * @param executeTime
     * @param periodValue
     * @param typeDaySelect
     * @param whichTypeDay
     * @param beginTime
     * @param endTime
     * @param durationTime
     * @param durationType
     */
    private void generatedMonthCalendar(List<Map<String, Object>> timeList, Date now, String executeTime, int periodValue, String typeDaySelect, String whichTypeDay, String beginTime, String endTime, String durationTime, String durationType) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            SimpleDateFormat sd = new SimpleDateFormat(Constants.DATE_FMT);
            Calendar cl = Calendar.getInstance();
            cl.setFirstDayOfWeek(Calendar.MONDAY);
            if (cl.getTime().before(sdf.parse(beginTime))) {
                cl.setTime(sdf.parse(beginTime));//如果计划开始时间大于当前时间，则按计划的开始时间计算
            }
            if (cl.getTime().after(sdf.parse(sd.format(now) + " " + executeTime))) {
                cl.add(Calendar.DATE, 1);//如果当天的计划执行时间点已经过了，则跳到明天开始计算
            }
            cl.setTime(sdf.parse(sd.format(cl.getTime()) + " " + executeTime));
            Date begin = cl.getTime();
            cl.add(Calendar.YEAR, BATCH_YEAR);
            Date end = cl.getTime().before(sdf.parse(endTime)) ? cl.getTime() : sdf.parse(endTime);//获取截止时间
            cl.add(Calendar.YEAR, -BATCH_YEAR);
            while (cl.getTime().compareTo(end) <= 0) {
                if (RegexUtil.optEquals(typeDaySelect, Constants.TRIGGER_TIME_TYPE_DAY)) {
                    Calendar day = Calendar.getInstance();
                    day.setTime(cl.getTime());
                    day.set(Calendar.DAY_OF_MONTH, Integer.valueOf(whichTypeDay));
                    if (day.getTime().compareTo(begin) >= 0 && day.getTime().compareTo(end) <= 0) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("occur_time", day.getTime());
                        map.put("deadline_time", computeDeadlineTime(day.getTime(), durationTime, durationType));
                        timeList.add(map);
                    }
                } else if (RegexUtil.optEquals(typeDaySelect, Constants.TRIGGER_TIME_TYPE_WORK_DAY)) {
                    computeTime(timeList, cl.getTime(), begin, end, Integer.valueOf(whichTypeDay), CALENDAR_WORKDAY, durationTime, durationType);
                } else if (TRIGGER_TIME_TYPE_MAP.containsKey(typeDaySelect)) {
                    computeTime(timeList, cl.getTime(), begin, end, Integer.valueOf(whichTypeDay), TRIGGER_TIME_TYPE_MAP.get(typeDaySelect), durationTime, durationType);
                }
                cl.add(Calendar.MONTH, periodValue);
            }
        } catch (ParseException e) {
            logger.error("", e);
        }
    }

    /**
     * 获取”按年“类型的行事历开始时间
     *
     * @param timeList
     * @param now
     * @param executeTime
     * @param periodValue
     * @param typeDaySelect
     * @param whichTypeDay
     * @param beginTime
     * @param endTime
     * @param durationTime
     * @param durationType
     */
    private void generatedYearCalendar(List<Map<String, Object>> timeList, Date now, String executeTime, int periodValue, String typeDaySelect, String whichTypeDay, String beginTime, String endTime, String durationTime, String durationType) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
            SimpleDateFormat sd = new SimpleDateFormat(Constants.DATE_FMT);
            Calendar cl = Calendar.getInstance();
            cl.setFirstDayOfWeek(Calendar.MONDAY);
            if (cl.getTime().before(sdf.parse(beginTime))) {
                cl.setTime(sdf.parse(beginTime));//如果计划开始时间大于当前时间，则按计划的开始时间计算
            }
            if (cl.getTime().after(sdf.parse(sd.format(now) + " " + executeTime))) {
                cl.add(Calendar.DATE, 1);//如果当天的计划执行时间点已经过了，则跳到明天开始计算
            }
            cl.setTime(sdf.parse(sd.format(cl.getTime()) + " " + executeTime));
            Date begin = cl.getTime();
            cl.add(Calendar.YEAR, BATCH_YEAR);
            Date end = cl.getTime().before(sdf.parse(endTime)) ? cl.getTime() : sdf.parse(endTime);//获取截止时间
            cl.add(Calendar.YEAR, -BATCH_YEAR);
            while (cl.getTime().compareTo(end) <= 0) {
                if ((cl.get(Calendar.MONTH) + 1) == periodValue) {
                    if (RegexUtil.optEquals(typeDaySelect, Constants.TRIGGER_TIME_TYPE_DAY)) {
                        Calendar day = Calendar.getInstance();
                        day.setTime(cl.getTime());
                        day.set(Calendar.DAY_OF_MONTH, Integer.valueOf(whichTypeDay));
                        if (day.getTime().compareTo(begin) >= 0 && day.getTime().compareTo(end) <= 0) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("occur_time", day.getTime());
                            map.put("deadline_time", computeDeadlineTime(day.getTime(), durationTime, durationType));
                            timeList.add(map);
                        }
                    } else if (RegexUtil.optEquals(typeDaySelect, Constants.TRIGGER_TIME_TYPE_WORK_DAY)) {
                        computeTime(timeList, cl.getTime(), begin, end, Integer.valueOf(whichTypeDay), CALENDAR_WORKDAY, durationTime, durationType);
                    } else if (TRIGGER_TIME_TYPE_MAP.containsKey(typeDaySelect)) {
                        computeTime(timeList, cl.getTime(), begin, end, Integer.valueOf(whichTypeDay), TRIGGER_TIME_TYPE_MAP.get(typeDaySelect), durationTime, durationType);
                    }
                }
                cl.add(Calendar.MONTH, 1);
            }
        } catch (ParseException e) {
            logger.error("", e);
        }
    }

    /**
     * 计算指定第几个工作日、第几个星期*
     *
     * @param timeList
     * @param now
     * @param begin
     * @param end
     * @param period
     * @param dayOfWeek
     * @param durationTime
     * @param durationType
     */
    private void computeTime(List<Map<String, Object>> timeList, Date now, Date begin, Date end, int period, String dayOfWeek, String durationTime, String durationType) {
        Calendar cl = Calendar.getInstance();
        cl.setTime(now);
        int month = cl.get(Calendar.MONTH);
        int index = 0;
        if (period < 0) {//倒数第几个
            cl.set(Calendar.DAY_OF_MONTH, cl.getActualMaximum(Calendar.DAY_OF_MONTH));//调整到最后1天
            int pd = Math.abs(period);
            while (cl.getTime().compareTo(begin) >= 0 && cl.get(Calendar.MONTH) == month) {
                if (dayOfWeek.contains(String.valueOf(cl.get(Calendar.DAY_OF_WEEK)))) {
                    index++;
                }
                if (index == pd && cl.getTime().compareTo(end) <= 0) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("occur_time", cl.getTime());
                    map.put("deadline_time", computeDeadlineTime(cl.getTime(), durationTime, durationType));
                    timeList.add(map);
                    break;
                }
                cl.add(Calendar.DATE, -1);
            }
        } else {//正数第几个
            cl.set(Calendar.DAY_OF_MONTH, 1);//调整到第1天
            while (cl.getTime().compareTo(end) <= 0 && cl.get(Calendar.MONTH) == month) {
                if (dayOfWeek.contains(String.valueOf(cl.get(Calendar.DAY_OF_WEEK)))) {
                    index++;
                }
                if (index == period) {
                    if (cl.getTime().compareTo(begin) >= 0) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("occur_time", cl.getTime());
                        map.put("deadline_time", computeDeadlineTime(cl.getTime(), durationTime, durationType));
                        timeList.add(map);
                    }
                    break;
                }
                cl.add(Calendar.DATE, 1);
            }
        }
    }

    /**
     * 计算行事历截止时间
     *
     * @param occurTime
     * @param durationTime
     * @param durationType
     * @return
     */
    private Date computeDeadlineTime(Date occurTime, String durationTime, String durationType) {
        if (occurTime == null) {
            return null;
        }

        Calendar cl = Calendar.getInstance();
        cl.setTime(occurTime);
        int field;
        int amount = Float.valueOf(durationTime).intValue();
        if (RegexUtil.optEquals(Constants.TIME_UNIT_HOUR, durationType)) {
            field = Calendar.HOUR;
        } else if (RegexUtil.optEquals(Constants.TIME_UNIT_DAY, durationType)) {
            field = Calendar.DATE;
        } else if (RegexUtil.optEquals(Constants.TIME_UNIT_WEEK, durationType)) {
            field = Calendar.DATE;
            amount = amount * 7;
        } else if (RegexUtil.optEquals(Constants.TIME_UNIT_MONTH, durationType)) {
            field = Calendar.MONTH;
        } else {
            return null;
        }
        cl.add(field, amount);
        return cl.getTime();
    }

    /**
     * 日期偏移几天
     *
     * @param occurTime 原始时间
     * @param offset    偏移天数
     * @return
     */
    private Date offsetDateHandle(Date occurTime, int offset) {
        if (offset == 0) {
            return occurTime;
        }

        Calendar cl = Calendar.getInstance();
        cl.setTime(occurTime);
        cl.add(Calendar.DATE, offset);
        return cl.getTime();
    }
}
