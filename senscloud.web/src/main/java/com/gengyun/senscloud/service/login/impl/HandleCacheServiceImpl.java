package com.gengyun.senscloud.service.login.impl;

import com.gengyun.senscloud.service.login.HandleCacheService;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class HandleCacheServiceImpl implements HandleCacheService {
    @Value("${senscloud.com.url}")
    private String url;

    @Cacheable(value = "ehcCompanyInfo", key = "'request_company_public' + #companyId + '_key_' + #key + '_path_' + #path")
    @Override
    public String requestCompany(Long companyId, String key, String path) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(key, companyId);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }
}
