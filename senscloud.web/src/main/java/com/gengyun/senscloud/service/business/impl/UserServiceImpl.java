package com.gengyun.senscloud.service.business.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.entity.UserEntity;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.UserModel;
import com.gengyun.senscloud.service.GroupService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import com.gengyun.senscloud.view.UserConfigDataExportView;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    UserMapper userMapper;
    @Resource
    GroupService groupsService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    LogsService logService;
    @Resource
    CompanyService companyService;
    @Resource
    CacheUtilService cacheUtilService;
    @Value("${senscloud.com.url}")
    private String url;
    @Value("${senscloud.com.license}")
    private String license;

    /**
     * 获取按钮功能权限信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getUserListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[4]);
    }

    /**
     * 新增用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void newUser(MethodParam methodParam, UserModel userModel) {
        if (!checkCompanyIsAddUser(methodParam)) {
            throw new SenscloudException(LangConstant.MSG_BQ);//超出企业开通收费用户数量上限
        }
        //账号
        String account = RegexUtil.optStrOrExpNotNull(userModel.getAccount(), LangConstant.MSG_A);
        //校验手机号是否重复
        //电话
        //String mobile = RegexUtil.optStrOrExpNotNull(userModel.getMobile(), LangConstant.MSG_A);
        String mobile = RegexUtil.isMobile(userModel.getMobile());
        if (RegexUtil.optIsPresentList(userMapper.findIsUseUserByMobile(methodParam.getSchemaName(), mobile))) {
            throw new SenscloudException(LangConstant.MSG_AC);//该手机号已经被注册，请用别的手机号
        }
        //校验账号是否重复
        if (RegexUtil.optIsPresentList(userMapper.findIsUseUserByAccount(methodParam.getSchemaName(), account))) {
            throw new SenscloudException(LangConstant.LOG_H);//该账号已经被注册
        }
        if (RegexUtil.optIsPresentStr(userModel.getEmail())) {//
            boolean matches = Constants.SCD_PT_MAIL.matcher(userModel.getEmail()).matches();
            RegexUtil.falseExp(matches, LangConstant.TEXT_AF, new String[]{userModel.getEmail(), RegexUtil.optStrOrBlank(LangConstant.TITLE_GW), LangConstant.TITLE_GW});
        }
        //用户名
        String user_name = RegexUtil.optStrOrExpNotNull(userModel.getUser_name(), LangConstant.TITLE_GB);
        //性别
        String gender_tag = RegexUtil.optStrOrExpNotNull(userModel.getGender_tag(), LangConstant.TEXT_BA);
        //是否启用
        String is_use = RegexUtil.optStrOrExpNotNull(userModel.getIs_use(), LangConstant.TITLE_K);
        String password = MD5.getStrByMd5(AESUtils.decryptAES(userModel.getPassword()));
        Map<String, Object> user = new HashMap<>();
        user.put("nfc_code", userModel.getNfc_code());
        user.put("id", (String.valueOf(new IdWorker().nextId())));
        user.put("user_name", user_name);
        user.put("account", account);
        user.put("user_code", userModel.getUser_code());
        user.put("mobile", mobile);
        user.put("email", userModel.getEmail());
        user.put("hour_fee", userModel.getHour_fee());
        user.put("is_use", is_use);
        user.put("is_charge", userModel.getIs_charge());
        user.put("gender_tag", Integer.valueOf(gender_tag));
        if (RegexUtil.optIsPresentStr(userModel.getJoin_date())) {
            Date dateTime = RegexUtil.changeToDate(userModel.getJoin_date(), LangConstant.TITLE_DATE_T, Constants.DATE_FMT);
            user.put("join_date", dateTime);
        }
        user.put("remark", userModel.getRemark());
        user.put("create_time", new Date());
        user.put("password", password);
        userMapper.insertUser(methodParam.getSchemaName(), user);
        //新增企业用户关系表
        user.put("company_id", methodParam.getCompanyId());
        user.put("user_id", user.get("id"));
        companyService.newUserTenantRelation(user);
        //批量插入岗位和用户关联表
        if (RegexUtil.optIsPresentStr(userModel.getPositionIds())) {
            List<Map<String, Object>> list = new ArrayList<>();
            String[] positionIds = RegexUtil.optNotBlankStrOpt(userModel.getPositionIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
            for (String positionId : positionIds
            ) {
                Map<String, Object> map = new HashMap<>();
                map.put("user_id", user.get("id"));
                map.put("position_id", positionId);
                list.add(map);
            }
            commonUtilService.batchInsertDataList(methodParam.getSchemaName(), list, "_sc_user_position", "user_id,position_id");
        }
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, user.get("id").toString(), LangUtil.doSetLogArray("新增用户", LangConstant.TEXT_ADD_A, new String[]{LangConstant.TITLE_FU}));//添加了用户
    }

    /**
     * 重置用户密码
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void changeUserPassword(MethodParam methodParam, UserModel userModel) {
        //密码
        String password = RegexUtil.optNotBlankStrOrExp(MD5.getStrByMd5(RegexUtil.optNotBlankStrOrExp(AESUtils.decryptAES(RegexUtil.optStrOrExpNotNull(userModel.getPassword(), LangConstant.TITLE_AAH_F)), LangConstant.MSG_BC)), LangConstant.MSG_BC); // 密码
        String id = RegexUtil.optStrOrExpNotNull(userModel.getId(), LangConstant.MSG_A);
        Map<String, Object> user = new HashMap<>();
        user.put("id", id);
        user.put("password", password);
        userMapper.updateUserPassword(methodParam.getSchemaName(), user);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, user.get("id").toString(), LangUtil.doSetLogArray("重置用户密码", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_RST_A, LangConstant.TITLE_AAH_F, ""}));//重置了密码

    }

    /**
     * 编辑用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void modifyUser(MethodParam methodParam, UserModel userModel) {
        if ("2".equals(userModel.getSys_user())) {
            RegexUtil.optStrOrExpNotNull(userModel.getId(), LangConstant.TITLE_FU);//用户不能为空
            Map<String, Object> oldMap = userMapper.findById(methodParam.getSchemaName(), userModel.getId());
            if (RegexUtil.optNotNull(userModel.getMobile()).isPresent()) {
                String mobile = RegexUtil.isMobile(userModel.getMobile());
                //校验手机号是否重复
                if (!mobile.equals(oldMap.get("mobile")) && RegexUtil.optIsPresentList(userMapper.findIsUseUserByMobileAndId(methodParam.getSchemaName(), userModel.getMobile(), userModel.getId()))) {
                    throw new SenscloudException(LangConstant.MSG_AC);//该手机号已经被注册，请用别的手机号
                }
            }
            //校验账号是否重复
            if (RegexUtil.optIsPresentList(userMapper.findIsUseUserByAccountAndId(methodParam.getSchemaName(), userModel.getAccount(), userModel.getId()))) {
                throw new SenscloudException(LangConstant.LOG_H);//该账号已经被注册
            }
            if (RegexUtil.optIsPresentStr(userModel.getEmail())) {//
                boolean matches = Constants.SCD_PT_MAIL.matcher(userModel.getEmail()).matches();
                RegexUtil.falseExp(matches, LangConstant.TEXT_AF, new String[]{userModel.getEmail(), RegexUtil.optStrOrBlank(LangConstant.TITLE_GW), LangConstant.TITLE_GW});
            }

            Map<String, Object> user = new HashMap<>();
            user.put("id", userModel.getId());
            user.put("user_name", userModel.getUser_name());
            user.put("account", userModel.getAccount());
            user.put("user_code", userModel.getUser_code());
            user.put("mobile", userModel.getMobile());
            user.put("email", userModel.getEmail());
            user.put("nfc_code", userModel.getNfc_code());
            if (RegexUtil.optNotBlankStrOpt(userModel.getHour_fee()).isPresent()) {
                BigDecimal hour_fee = RegexUtil.changeToBd(userModel.getHour_fee(), LangConstant.TITLE_BAAAB_N);//数据工时单价格式不对
                user.put("hour_fee", hour_fee);
            } else {
                user.put("hour_fee", 0);
            }
            user.put("status", userModel.getStatus());
            user.put("is_charge", userModel.getIs_charge());
            user.put("gender_tag", userModel.getGender_tag());
            user.put("join_date", userModel.getJoin_date());
            user.put("remark", userModel.getRemark());
            user.put("is_use", userModel.getIs_use());
            userMapper.updateUserWithoutPassword(methodParam.getSchemaName(), user);

            //批量插入岗位和用户关联表
            if (RegexUtil.optIsPresentStr(userModel.getPositionIds())) {
                List<Map<String, Object>> list = new ArrayList<>();
                //删除用户岗位关联表 然后重新插入
                userMapper.deleteUserPositionByUserId(methodParam.getSchemaName(), userModel.getId());
                String[] positionIds = RegexUtil.optNotBlankStrOpt(userModel.getPositionIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_BI)); // 失败：缺少必要条件！
                List<String> permissionIdStream = Arrays.asList(positionIds).stream().distinct().collect(Collectors.toList());
                for (String positionId : permissionIdStream
                ) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("user_id", userModel.getId());
                    map.put("position_id", positionId);
                    list.add(map);
                }
                commonUtilService.batchInsertDataList(methodParam.getSchemaName(), list, "_sc_user_position", "user_id,position_id");
            }
            JSONArray log = LangUtil.compareMap(user, oldMap);
            //编辑企业用户关系表
            Map<String, Object> user_tenant_relation = new HashMap<>();
            if (RegexUtil.optNotNull(userModel.getMobile()).isPresent()) {
                user_tenant_relation.put("mobile", userModel.getMobile());
            } else {
                user_tenant_relation.put("mobile", oldMap.get("mobile"));
            }
            if (RegexUtil.optNotNull(userModel.getAccount()).isPresent()) {
                user_tenant_relation.put("newAccount", userModel.getAccount());
            } else {
                user_tenant_relation.put("newAccount", oldMap.get("account"));
            }
            user_tenant_relation.put("account", oldMap.get("account"));
            user_tenant_relation.put("company_id", methodParam.getCompanyId());
            userCompanySyncUpdate(user_tenant_relation);
            companyService.deleteCompanyListByUser(oldMap.get("account").toString());//清除旧账号的公司缓存
            companyService.deleteCompanyListByUser(userModel.getAccount());//新账号的缓存也需要清除一下
            cacheUtilService.deleteUserInfo(userModel.getId(), methodParam.getSchemaName());//清除用户的信息缓存
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, user.get("id").toString(), LangUtil.doSetLogArray("编辑了用户", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_FU, log.toString()}));//编辑了用户
        }
    }

    /**
     * 启用禁用用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void changeIsUse(MethodParam methodParam, UserModel userModel) {
        if ("1".equals(userModel.getIs_use()) && !checkCompanyIsAddUser(methodParam)) {
            throw new SenscloudException(LangConstant.MSG_BQ);//超出企业开通收费用户数量上限
        }
        if ("2".equals(userModel.getSys_user())) {
            RegexUtil.optStrOrExpNotNull(userModel.getId(), LangConstant.TITLE_FU);//用户不能为空
            Map<String, Object> oldMap = userMapper.findById(methodParam.getSchemaName(), userModel.getId());
            if (RegexUtil.optNotNull(oldMap).isPresent()) {
                oldMap.put("company_id", methodParam.getCompanyId());
                oldMap.put("user_id", oldMap.get("id"));
                userMapper.changeIsUse(methodParam.getSchemaName(), userModel.getId(), userModel.getIs_use());
                if ("1".equals(userModel.getIs_use())) {
                    //启用用户 新增企业用户表用户
                    companyService.newUserTenantRelation(oldMap);
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, oldMap.get("id").toString(), LangUtil.doSetLogArray("启用了用户", LangConstant.TEXT_AJ, new String[]{LangConstant.TITLE_AAL_L, LangConstant.TITLE_FU, oldMap.get("user_name").toString()}));//启用用户
                } else if ("0".equals(userModel.getIs_use())) {
                    //禁用用户 删除企业用户表用户
                    userCompanySyncDelete(oldMap);
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, oldMap.get("id").toString(), LangUtil.doSetLogArray("禁用了用户", LangConstant.TEXT_AJ, new String[]{LangConstant.TITLE_AL_X, LangConstant.TITLE_FU, oldMap.get("user_name").toString()}));//禁用用户
                }
            }
        }
    }

    /**
     * 删除用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void cutUser(MethodParam methodParam, UserModel userModel) {
        if ("2".equals(userModel.getSys_user())) {
            Map<String, Object> user = userMapper.findById(methodParam.getSchemaName(), userModel.getId());
            userMapper.deleteUserById(methodParam.getSchemaName(), userModel.getId());
            //注销企业用户关系表
            user.put("company_id", methodParam.getCompanyId());
            userCompanySyncDelete(user);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, user.get("id").toString(), LangUtil.doSetLogArray("删除了用户", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_FU, user.get("user_name").toString()}));//删除了用户
        }
    }

    /**
     * 删除选中用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void cutSelectUser(MethodParam methodParam, UserModel userModel) {
        if ("2".equals(userModel.getSys_user())) {
            String[] idStr = RegexUtil.optNotBlankStrOpt(userModel.getIds()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new))
                    .orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{LangConstant.TITLE_ASSET_I})); // 失败：缺少必要条件！
            RegexUtil.optNotNullArrayOpt(idStr).ifPresent(ids -> {
                userMapper.deleteSelectUser(methodParam.getSchemaName(), ids);
//                userMapper.userCompanySyncDeleteBySelect(methodParam.getSchemaName(), ids);
                userCompanySyncDeleteBySelect(ids);
                String remark = ids.length > 1 ? LangUtil.doSetLogArrayNoParam("批量删除了用户！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了用户！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_FU});
                for (String id : ids) {
                    logService.newLog(methodParam, SensConstant.BUSINESS_NO_7008, id, remark);
                }
            });
        }
    }

    /**
     * 获取用户详情
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public Map<String, Object> getUserInfo(MethodParam methodParam, UserModel userModel) {
        String userId = RegexUtil.optStrOrExpNotNull(userModel.getId(), LangConstant.MSG_A);
        return cacheUtilService.getUserInfo(methodParam, userId);
    }

    /**
     * 分页获取用户信息列表
     *
     * @param methodParam 入参
     * @param userModel   入参
     * @return 分页用户信息列表
     */
    @Override
    public Map<String, Object> getUserListForPage(MethodParam methodParam, UserModel userModel) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        Map<String, Object> params = new HashMap<>();
        params.put("isUseSearch", userModel.getIsUseSearch());
        params.put("positionSearch", userModel.getPositionSearch());
        params.put("keywordSearch", userModel.getKeywordSearch());
        //查询该条件下的总记录数
        int total = userMapper.findUserListTotal(methodParam.getSchemaName(), params);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        params.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = userMapper.findUserListForPage(methodParam.getSchemaName(), params);
        //给每个用户找出所属的岗位名称和部门级联
        for (Map<String, Object> pm : rows
        ) {
            String nameString = userMapper.findGroupPositionNameString(methodParam.getSchemaName(), (String) pm.get("id"));
            pm.put("groupPositionName", nameString);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 通过用户名登录获取账号
     *
     * @param schemaName 数据库
     * @param account    用户名
     * @param password   密码
     * @return account
     */
    @Override
    public String getUserAccountForLogin(String schemaName, String account, String password) {
        return userMapper.findUserAccountForLogin(schemaName, account, password);
    }

    /**
     * 通过员工工号登录获取账号
     *
     * @param schemaName 数据库
     * @param userCode   员工工号
     * @return UserEntity
     */
    @Override
    public UserEntity getUserAccountForUserCode(String schemaName, String userCode) {
        return userMapper.findUserByUserCode(schemaName, userCode);
    }

    /**
     * 通过员工NFC卡号登录获取账号
     *
     * @param schemaName 数据库
     * @param nfcCode    员工NFC卡号
     * @return UserEntity
     */
    @Override
    public UserEntity getUserAccountForNfcCode(String schemaName, String nfcCode) {
        return userMapper.findUserByNfcCode(schemaName, nfcCode);
    }

    /**
     * 获取登录用户信息
     *
     * @param schemaName 数据库
     * @param account    账号
     * @return 用户信息
     */
    @Override
    public UserEntity getUserForSystemLoginCache(String schemaName, String account) {
        return userMapper.findUserForSystemLoginCache(schemaName, account);
    }

    /**
     * 通过手机号获取用户信息
     *
     * @param schemaName 数据库
     * @param phone      手机号
     * @return 用户信息
     */
    @Override
    public UserEntity getUserByPhone(String schemaName, String phone) {
        return userMapper.findUserByPhone(schemaName, phone);
    }

    /**
     * 修改密码
     *
     * @param methodParam 系统参数
     * @param id          用户主键
     * @param psd         密码
     */
    @Override
    public void modifyPassword(MethodParam methodParam, String id, String psd) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("password", psd);
        userMapper.updateUserPassword(methodParam.getSchemaName(), map);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_9000, id, LangUtil.doSetLogArray("修改了密码", LangConstant.TEXT_AJ, new String[]{LangConstant.TITLE_DH, LangConstant.TITLE_AAH_F}));
    }

    /**
     * 根据id查询用户信息
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 用户记录
     */
    @Override
    public Map<String, Object> getUserInfoById(MethodParam methodParam, String id) {
        return userMapper.findById(methodParam.getSchemaName(), id);
    }

    @Override
    public Map<String, Object> findUserInfoById(MethodParam methodParam, String id) {
        return userMapper.findUserInfoById(methodParam.getSchemaName(), id);
    }

    @Override
    public String findAllCCUser(MethodParam methodParam, String ids) {
        return userMapper.findAllCCUser(methodParam.getSchemaName(), ids);
    }

    /**
     * 根据userid列表获取用户信息列表
     *
     * @param methodParam 入参
     * @param userIds     入参
     * @return 用户信息列表
     */
    @Override
    public List<Map<String, Object>> getUserListByIds(MethodParam methodParam, List<String> userIds) {
        return userMapper.findUserListByIds(methodParam.getSchemaName(), userIds);
    }

    /**
     * 根据用户主键获取岗位和部门列表
     *
     * @param methodParam 系统参数
     * @param userId      用户主键
     * @return 用户岗位和部门列表
     */
    @Override
    public List<Map<String, Object>> getUserGpList(MethodParam methodParam, String userId) {
        return userMapper.findUserGpList(methodParam.getSchemaName(), userId);
    }

    /**
     * 根据userId和密码判断是否存在
     *
     * @param methodParam 入参
     * @param userId      入参
     * @return 是否存在
     */
    @Override
    public int getUserInfoByUserIdAndPwd(MethodParam methodParam, String userId, String psd) {
        return userMapper.findUserInfoByUserIdAndPwd(methodParam.getSchemaName(), userId, psd);
    }

    /**
     * 新增用户微信主键
     *
     * @param schemaName
     * @param userId
     * @param wxOpenId
     * @param unionId
     * @param oaOpenId
     * @return
     */
    @Override
    public int insertUserWxOpenId(String schemaName, String userId, String wxOpenId, String unionId, String oaOpenId, String nickName) {
        return userMapper.insertUserWxOpenId(schemaName, userId, wxOpenId, unionId, oaOpenId, nickName);
    }

    /**
     * 更新用户微信主键
     *
     * @param schemaName
     * @param userId
     * @param wxOpenId
     * @return
     */
    @Override
    public int updateUserWxOpenId(String schemaName, String userId, String wxOpenId, String unionId, String oaOpenId, String nickName) {
        if (StringUtils.isBlank(oaOpenId)) {
            return userMapper.updateUserWxOpenId(schemaName, wxOpenId, unionId, nickName, userId);
        } else {
            return userMapper.updateUserWxOpenIdAndOaOpenId(schemaName, wxOpenId, unionId, oaOpenId, nickName, userId);
        }
    }

    /**
     * 查询微信公众号用户信息
     *
     * @param unionId 微信主键
     * @param wxOaId  公众号编码
     * @return 微信公众号用户信息
     */
    @Override
    public Map<String, Object> queryWxoaUserByUnionId(String unionId, String wxOaId) {
//        return userMapper.queryWxoaUserByUnionId(unionId);
        return queryWxoaUserByUnionIdByHttp(unionId, wxOaId);
    }

    /**
     * 查看用户公众号关注状态
     *
     * @param schemaName
     * @param account
     */
    @Override
    public boolean getFollowOAStatus(String schemaName, String account) {
        List<Map<String, Object>> userWxLoginInfo = userMapper.queryUserWxLoginInfoByAccount(schemaName, account);
        if (userWxLoginInfo != null && userWxLoginInfo.size() > 0
                && userWxLoginInfo.get(0).get("official_open_id") != null
                && StringUtils.isNotBlank(String.valueOf(userWxLoginInfo.get(0).get("official_open_id")))) {
            return true;
        }
        return false;
    }

    /**
     * 通过登录账号，查询绑定的微信用户信息
     *
     * @param schemaName 数据库
     * @param id         用户主键
     * @return 微信信息
     */
    @Override
    public Map<String, Object> queryUserWxLoginInfoById(String schemaName, String id) {
        return userMapper.queryUserWxLoginInfo(schemaName, id);
    }

    /**
     * 查看当前公司还能新增或者启用用户
     *
     * @param methodParam 入参
     * @return 是否
     */
    @Override
    public boolean checkCompanyIsAddUser(MethodParam methodParam) {
        //查看当前公司最大可用用户数
//        Integer maxUserTotal = userMapper.findCompanyMaxUserTotal(methodParam.getCompanyId());
        Integer maxUserTotal = findCompanyMaxUserTotal(methodParam.getCompanyId());
        //查看当前启用的用户数
        Integer nowUserTotal = userMapper.findNowCompanyUserTotal(methodParam.getSchemaName());
        if (RegexUtil.optIsPresentStr(maxUserTotal) && RegexUtil.optIsPresentStr(nowUserTotal) && maxUserTotal > nowUserTotal) {
            return true;
        }
        return false;
    }

    /**
     * 更新员工上下班状态
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    @Override
    public void changeUserIsOnDuty(MethodParam methodParam, UserModel userModel) {
        RegexUtil.optStrOrExpNotNull(userModel.getId(), LangConstant.MSG_A);
        RegexUtil.optStrOrExpNotNull(userModel.getIs_on_duty(), LangConstant.MSG_A);
        userMapper.changeUserIsOnDuty(methodParam.getSchemaName(), userModel.getId(), userModel.getIs_on_duty());
        if ("1".equals(userModel.getIs_on_duty())) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_4007, userModel.getId(), LangUtil.doSetLogArrayNoParam("签到", LangConstant.TITLE_AAL_O));
        } else if ("-1".equals(userModel.getIs_on_duty())) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_4007, userModel.getId(), LangUtil.doSetLogArrayNoParam("签退", LangConstant.TITLE_AAL_U));
        }
        cacheUtilService.deleteUserInfo(userModel.getId(), methodParam.getSchemaName());//清除用户的信息缓存
    }

    /**
     * 导出选中用户
     *
     * @param methodParam
     * @param model
     */
    @Override
    public ModelAndView doExportUser(MethodParam methodParam, UserModel model) {
        String ids = model.getIds();
        RegexUtil.falseExp(RegexUtil.optIsPresentStr(ids), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAR_J});
        String idsStr = DataChangeUtil.joinByStr(ids);
        List<Map<String, Object>> rows = userMapper.findUserListForIds(methodParam.getSchemaName(), idsStr);
        if (RegexUtil.optIsPresentList(rows)) {
            //给每个用户找出所属的岗位名称和部门级联
            for (Map<String, Object> pm : rows) {
                String nameString = userMapper.findGroupPositionNameString(methodParam.getSchemaName(), (String) pm.get("id"));
                pm.put("groupPositionName", nameString);
            }
        } else {
            rows = new ArrayList<>();
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userConfigDataList", rows); // 信息不存在
        UserConfigDataExportView excelView = new UserConfigDataExportView();
        return new ModelAndView(excelView, map);
    }

    /**
     * 导出全部用户
     *
     * @param methodParam
     * @param userModel
     */
    @Override
    public ModelAndView doExportAllUser(MethodParam methodParam, UserModel userModel) {
        Map<String, Object> params = new HashMap<>();
        params.put("isUseSearch", userModel.getIsUseSearch());
        params.put("positionSearch", userModel.getPositionSearch());
        params.put("keywordSearch", userModel.getKeywordSearch());
        //查询该条件下的总记录数
        params.put("pagination", "");//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = userMapper.findUserListForPage(methodParam.getSchemaName(), params);
        if (RegexUtil.optIsPresentList(rows)) {
            //给每个用户找出所属的岗位名称和部门级联
            for (Map<String, Object> pm : rows) {
                String nameString = userMapper.findGroupPositionNameString(methodParam.getSchemaName(), (String) pm.get("id"));
                pm.put("groupPositionName", nameString);
            }
        } else {
            rows = new ArrayList<>();
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userConfigDataList", rows); //信息不存在
        UserConfigDataExportView excelView = new UserConfigDataExportView();
        return new ModelAndView(excelView, map);
    }


    /**
     * 验证用户名称是否存在
     *
     * @param schemaName
     * @param userNameList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryUsersByName(String schemaName, List<String> userNameList) {
        return userMapper.queryUsersByName(schemaName, userNameList);
    }

    @Override
    public Map<String, Map<String, Object>> getAccountMobileListMap(String schemaName) {
        return userMapper.findAccountMobileListMap(schemaName);
    }

    /**
     * 查询全部账号
     *
     * @param schemaName
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryAccountListMap(String schemaName) {
        return userMapper.findAccountListMap(schemaName);
    }

    /**
     * 查询全部手机号
     *
     * @param schemaName
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryMobileListMap(String schemaName) {
        return userMapper.findMobileListMap(schemaName);
    }

    @Override
    public Map<String, Map<String, Object>> getUserByMobile(String schemaName, List<String> mobileList) {
        return userMapper.findUserByMobile(schemaName, mobileList);
    }

    @Override
    public Map<String, Map<String, Object>> getUserByAccount(String schemaName, List<String> accountList) {
        return userMapper.findUserByAccount(schemaName, accountList);
    }

    private Map<String, Object> queryWxoaUserByUnionIdByHttp(String unionId, String wxOaId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("unionId", unionId);
        jsonObject.put("wxOaId", wxOaId);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("queryWxoaUserByUnionId");
        return DataChangeUtil.scdToMapUseJson(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
    }

    public int findCompanyMaxUserTotal(long company_id) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("company_id", company_id);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findCompanyMaxUserTotal");
        int result = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), int.class);
        return result;
    }

    public void userCompanySyncUpdate(Map<String, Object> param) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("param", param);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("userCompanySyncUpdate");
        SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

    public void userCompanySyncDelete(Map<String, Object> param) {
        JSONObject jsonObject = new JSONObject();
        param.remove("join_date");
        param.put("license", license);
        jsonObject.put("param", param);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("userCompanySyncDelete");
        SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

    public void userCompanySyncDeleteBySelect(String[] ids) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ids", ids);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("userCompanySyncDeleteBySelect");
        SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }
}

