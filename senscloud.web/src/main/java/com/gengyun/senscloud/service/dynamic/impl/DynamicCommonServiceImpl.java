package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.WorksMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.dynamic.DynamicCommonService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 动态公共方法用接口
 */
@Service
public class DynamicCommonServiceImpl implements DynamicCommonService {
    //    private static final Logger logger = LoggerFactory.getLogger(DynamicCommonServiceImpl.class);
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    WorksMapper worksMapper;
    //    @Autowired
//    CommonUtilService commonUtilService;
    @Resource
    SelectOptionService selectOptionService;
    //    @Autowired
//    WorkSheetHandleService workSheetHandleService;
    @Resource
    WorkflowService workflowService;
//    @Autowired
//    StockService stockService;
//    @Autowired
//    AuthService authService;
//    @Autowired
//    UserMapper userMapper;
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    MessageService messageService;
//    @Autowired
//    DynamicAnalysisService dynamicAnalysisService;
//    @Autowired
//    MetadataWorkMapper metadataWorkMapper;
//    @Autowired
//    DyToPageService dyToPageService;

    /**
     * 取数据编号
     *
     * @param methodParam 入参
     * @param index       入参
     * @return 数据编号
     */
    @Override
    public String getDataCode(MethodParam methodParam, int index) {
        if (0 == index) {
            return this.getWorkCode(methodParam);
        }
        if (1 == index) {
            return this.getSubWorkCode(methodParam);
        }
        if (5 == index) {
            return this.getWorkRequestCode(methodParam);
        }
        if (6 == index) {
            return this.getBomInStockCode(methodParam);
        }
        if (8 == index) {
            return this.getBomAllotCode(methodParam);
        }
        if (9 == index) {
            return this.getBomRecipientCode(methodParam);
        }
        if (10 == index) {
            return this.getBomDiscardCode(methodParam);
        }
        if (12 == index) {
            return this.getBomInventoryCode(methodParam);
        }
        if (13 == index) {
            return this.getSubBomInventoryCode(methodParam);
        }
        if (18 == index) {
            return this.getAssetDiscardCode(methodParam);
        }
        if (20 == index) {
            return this.getAssetTransferCode(methodParam);
        }
        if (22 == index) {
            return this.getAssetInventoryCode(methodParam);
        }
        if (23 == index) {
            return this.getSubAssetInventoryCode(methodParam);
        }
        if (25 == index) {
            return this.getUserRegisterCode(methodParam);
        }
        if (26 == index) {
            return this.getPlanWorkCode(methodParam);
        }
        if (31 == index) {
            return this.getAssetWorkCode(methodParam);
        }
        if (32 == index) {
            return this.getAssetSubWorkCode(methodParam);
        }
        if (33 == index) {
            return this.getBomWorkCode(methodParam);
        }
        if (34 == index) {
            return this.getBomSubWorkCode(methodParam);
        }
        if (35 == index) {
            return this.getAssetOutPutWorkCode(methodParam);//设备出库概要编码
        }
        if (36 == index) {
            return this.getAssetOutPutSubWorkCode(methodParam);//设备出库详情编码
        }
        if (37 == index) {
            return this.getAssetInPutWorkCode(methodParam);//设备入库概要编码
        }
        if (38 == index) {
            return this.getAssetInPutSubWorkCode(methodParam);//设备入库详情编码
        }
        if (39 == index) {
            return this.getAssetChangeWorkCode(methodParam);//设备调拨概要编码
        }
        if (40 == index) {
            return this.getAssetChangeSubWorkCode(methodParam);//设备调拨详情编码
        }
        if (41 == index) {
            return this.getAssetAcceptWorkCode(methodParam);//设备验收概要编码
        }
        if (42 == index) {
            return this.getAssetAcceptSubWorkCode(methodParam);//设备验收详情编码
        }

        return null;
    }

    /**
     * 取工单编号
     *
     * @param methodParam 入参
     * @return 工单编号
     */
    @Override
    public String getWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[0]);
    }

    /**
     * 取工单明细编号
     *
     * @param methodParam 入参
     * @return 工单明细编号
     */
    @Override
    public String getSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[1]);
    }

    /**
     * 取工单请求编号
     *
     * @param methodParam 入参
     * @return 工单请求编号
     */
    @Override
    public String getWorkRequestCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[2]);
    }

    /**
     * 取备件入库编号
     *
     * @param methodParam 入参
     * @return 备件入库编号
     */
    @Override
    public String getBomInStockCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[3]);
    }

    /**
     * 取备件调拨编号
     *
     * @param methodParam 入参
     * @return 备件调拨编号
     */
    public String getBomAllotCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[4]);
    }

    /**
     * 取备件领用编号
     *
     * @param methodParam 入参
     * @return 备件领用编号
     */
    public String getBomRecipientCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[5]);
    }

    /**
     * 取备件报废编号
     *
     * @param methodParam 入参
     * @return 备件报废编号
     */
    @Override
    public String getBomDiscardCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[6]);
    }

    /**
     * 取设备报废编号
     *
     * @param methodParam 入参
     * @return 设备报废编号
     */
    @Override
    public String getAssetDiscardCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[7]);
    }

    /**
     * 取设备调拨编号
     *
     * @param methodParam 入参
     * @return 设备调拨编号
     */
    @Override
    public String getAssetTransferCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[8]);
    }

    /**
     * 取备件盘点编号
     *
     * @param methodParam 入参
     * @return 备件盘点编号
     */
    public String getBomInventoryCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[9]);
    }

    /**
     * 取备件盘点明细编号
     *
     * @param methodParam 入参
     * @return 备件盘点明细编号
     */
    @Override
    public String getSubBomInventoryCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[10]);
    }

    /**
     * 取设备盘点编号
     *
     * @param methodParam 入参
     * @return 设备盘点编号
     */
    public String getAssetInventoryCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[11]);
    }

    /**
     * 取设备盘点明细编号
     *
     * @param methodParam 入参
     * @return 设备盘点明细编号
     */
    @Override
    public String getSubAssetInventoryCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[12]);
    }

    /**
     * 取用户注册编号
     *
     * @param methodParam 入参
     * @return 用户注册编号
     */
    public String getUserRegisterCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[13]);
    }

    /**
     * 取用维保计划编号
     *
     * @param methodParam 入参
     * @return 用户注册编号
     */
    public String getPlanWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[14]);
    }

    /**
     * 取用设备工单编号
     *
     * @param methodParam 入参
     * @return 用户注册编号
     */
    public String getAssetWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[15]);
    }

    /**
     * 取用设备工单详情编号
     *
     * @param methodParam 入参
     * @return 用户注册编号
     */
    @Override
    public String getAssetSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[16]);
    }

    /**
     * 取用备件工单编号
     *
     * @param methodParam 入参
     * @return 备件工单编号
     */
    public String getBomWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[18]);
    }

    /**
     * 取用备件工单详情编号
     *
     * @param methodParam 入参
     * @return 备件工单详情编号
     */
    public String getBomSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[19]);
    }

    /**
     * 取备件编号
     *
     * @param methodParam 入参
     * @return 备件编号
     */
    @Override
    public String getBomCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[17]);
    }

    /**
     * 获取用设备出库工单详情编号
     *
     * @param methodParam 入参
     * @return 设备出库工单详情编号
     */
    @Override
    public String getAssetOutPutWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[20]);
    }

    /**
     * 获取用设备出库工单编号
     *
     * @param methodParam 入参
     * @return 设备出库工单编号
     */
    @Override
    public String getAssetOutPutSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[21]);
    }

    /**
     * 获取用设备入库工单详情编号
     *
     * @param methodParam 入参
     * @return 设备入库工单详情编号
     */
    @Override
    public String getAssetInPutWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[22]);
    }

    /**
     * 获取用设备入库工单编号
     *
     * @param methodParam 入参
     * @return 设备入库工单编号
     */
    @Override
    public String getAssetInPutSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[23]);
    }

    /**
     * 获取用设备调拨工单详情编号
     *
     * @param methodParam 入参
     * @return 设备入库工单详情编号
     */
    @Override
    public String getAssetChangeWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[24]);
    }

    /**
     * 获取用设备调拨工单编号
     *
     * @param methodParam 入参
     * @return 设备入库工单编号
     */
    @Override
    public String getAssetChangeSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[25]);
    }

    /**
     * 获取用设备验收工单详情编号
     *
     * @param methodParam 入参
     * @return 设备入库工单详情编号
     */
    @Override
    public String getAssetAcceptWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[26]);
    }

    /**
     * 获取设备验收工单编号
     *
     * @param methodParam 入参
     * @return 设备入库工单编号
     */
    @Override
    public String getAssetAcceptSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[27]);
    }

    /**
     * 获取用备件入库工单详情编号
     *
     * @param methodParam 入参
     * @return 备件入库工单详情编号
     */
    @Override
    public String getBomInputWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[28]);
    }

    /**
     * 获取备件入库工单编号
     *
     * @param methodParam 入参
     * @return 备件入库工单编号
     */
    @Override
    public String getBomInputSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[29]);
    }

    /**
     * 获取用备件出库工单详情编号
     *
     * @param methodParam 入参
     * @return 备件出库工单详情编号
     */
    @Override
    public String getBomOutPutWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[30]);
    }

    /**
     * 获取备件出库工单编号
     *
     * @param methodParam 入参
     * @return 备件出库工单编号
     */
    @Override
    public String getBomOutPutSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[31]);
    }

    /**
     * 获取用备件采购需求工单详情编号
     *
     * @param methodParam 入参
     * @return 备件采购需求工单详情编号
     */
    @Override
    public String getBomPurchasingWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[32]);
    }

    /**
     * 获取备件采购需求工单编号
     *
     * @param methodParam 入参
     * @return 备件采购需求工单编号
     */
    @Override
    public String getBomPurchasingSubWorkCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[33]);
    }

    /**
     * 获取生产作业单编码
     *
     * @param methodParam 入参
     * @return 生产作业单编码
     */
    @Override
    public String getProductTaskCode(MethodParam methodParam) {
        return serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_DB_KEYS_BASE[34]);
    }

//    /**
//     * 获取工作流编号
//     *
//     * @param schemaName
//     * @param businessTypeId
//     * @return
//     */
//    public String getWorkFlowId(String schemaName, String businessTypeId) {
//        String key = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", businessTypeId, "relation");
//        if (null != key && !"".equals(key) && !key.contains(":")) {
//            List<ProcessDefinition> processDefinitions = workflowService.getDefinitions(schemaName, "", key, true, 10, 1, "key", "desc");
//            if (processDefinitions != null && processDefinitions.size() > 0) {
//                for (ProcessDefinition process : processDefinitions) {
//                    key = process.getId();
//                }
//            }
//        }
//        return key;
//    }
//
//    /**
//     * 处理map数据的字段类型，以便调用动态插入表格方法
//     *
//     * @param objectMap
//     */
//    @Override
//    public void handleTableColumnTypes(Map<String, Object> objectMap) {
//        if (objectMap == null || objectMap.isEmpty())
//            return;
//
//        Map<String, Object> tmp = new HashMap<>();
//        for (String fieldCode : objectMap.keySet()) {
//            if (fieldCode.startsWith("int@") || fieldCode.startsWith("varchar@") || fieldCode.startsWith("dateTime@") || fieldCode.startsWith("jsonb@")
//                    || fieldCode.startsWith("-1@") || fieldCode.startsWith("nowKey"))
//                continue;
//
//            if (objectMap.containsKey("int@" + fieldCode) || objectMap.containsKey("varchar@" + fieldCode) || objectMap.containsKey("dateTime@" + fieldCode)
//                    || objectMap.containsKey("jsonb@" + fieldCode) || objectMap.containsKey("-1@" + fieldCode))
//                continue;
//
//            Object value = objectMap.get(fieldCode);
//            if ("body_property".equals(fieldCode)) {
//                tmp.put("jsonb@" + fieldCode, fieldCode);
//            } else if (value instanceof String || value instanceof Timestamp) {
//                tmp.put("varchar@" + fieldCode, fieldCode);
//            } else if (value instanceof Integer) {
//                tmp.put("int@" + fieldCode, fieldCode);
//            }
//        }
//        objectMap.putAll(tmp);
//    }
//
//    /**
//     * 按用户取服务入口 上报工单|服务请求
//     *
//     * @param schema_name
//     * @param user_id
//     * @return
//     */
//    @Override
//    public Map<String, Object> serviceEntry(String schema_name, String user_id) {
//        if (RegexUtil.isNotNull(schema_name)) {
//            String flowId = null;
//            SystemConfigData scd = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.PHONE_REPIAR_WORK_TYPE_ID);
//            String phoneRepairWorkTypeId = scd.getSettingValue();
//            //一键报修
//            if (RegexUtil.isNull(user_id)) {
//                flowId = selectOptionService.getOptionNameByCode(schema_name, "work_type", phoneRepairWorkTypeId, "relation");
//            } else {
//                boolean repair_add_flag = pagePermissionService.getPermissionByKey(schema_name, user_id, "repair", "repair_add");
//                if (!repair_add_flag) {
//                    return null;
//                }
//                boolean work_sheet_add_flag = pagePermissionService.getPermissionByKey(schema_name, user_id, "worksheet", "worksheet_add");
//                // 直接生成工单
//                if (work_sheet_add_flag) {
////                    flowId = selectOptionService.getOptionNameByCode(schema_name, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_1, "relation");
//                    flowId = selectOptionService.getOptionNameByCode(schema_name, "work_type", phoneRepairWorkTypeId, "relation");
//                    // 生成请求
//                } else {
//                    boolean work_request_add_flag = pagePermissionService.getPermissionByKey(schema_name, user_id, "worksheet_request", "worksheet_request_add");
//                    if (work_request_add_flag) {
//                        flowId = selectOptionService.getOptionNameByCode(schema_name, "work_type_by_business_info", Constants.WORK_REQUEST_BUSINESS_TYPE, "relation");
//                        if (RegexUtil.isNull(flowId) || flowId.equals(selectOptionService.getLanguageInfo(LangConstant.NONE_A))) {
//                            flowId = selectOptionService.getOptionNameByCode(schema_name, "work_type", phoneRepairWorkTypeId, "relation");
//                        }
//                    }
//                }
//            }
//            if (RegexUtil.isNotNull(flowId)) {
//                Map<String, Object> resultMap = new HashMap<>();
//                resultMap.put("work_type_id", Integer.valueOf(phoneRepairWorkTypeId));
//                resultMap.put("flow_id", flowId);
//                resultMap.put("formKey", workflowService.getStartFormKey(schema_name, flowId));
//                return resultMap;
//            }
//        }
//        return null;
//    }
//
//    /**
//     * 新增工单
//     *
//     * @param schemaName
//     * @param pageType
//     * @return
//     */
//    public Map<String, Object> goToAddWork(String schemaName, String pageType) {
//        Map<String, Object> map = new HashMap<>();
//        if (RegexUtil.isNotNull(pageType)) {
//            if ("repair_add".equals(pageType)) {
//                Map<String, Object> dbInfo = commonUtilService.doGetDbBaseInfo();
//                map = this.serviceEntry(schemaName, (String) dbInfo.get("userId"));
//                if (null != map && map.size() > 0 && map.containsKey("formKey")) {
//                    map.put("flowId", map.get("flow_id"));
//                }
//            } else {
//                Map<String, Object> dyMenuAndTypeInfo = metadataWorkMapper.getWdtByCodeInfo(schemaName, "phone_start_type", pageType);
//                if (null != dyMenuAndTypeInfo && dyMenuAndTypeInfo.size() > 0) {
//                    Integer workTypeId = (Integer) dyMenuAndTypeInfo.get("work_type_id");
//                    if (RegexUtil.isNotNull(workTypeId)) {
//                        String workRequestFlow = selectOptionService.getOptionNameByCode(schemaName, "work_type", workTypeId.toString(), "relation");
//                        if (RegexUtil.isNotNull(workRequestFlow)) {
//                            String formKey = workflowService.getStartFormKey(schemaName, workRequestFlow);
//                            map.put("flowId", workRequestFlow);
//                            map.put("formKey", formKey);
//                        }
//                    }
//                }
//            }
//        }
//        return map;
//    }
//

    /**
     * 工单提交公共处理函数（工单及工单请求处理）
     *
     * @param schemaName
     * @param subWorkCode
     * @param worRequestCode
     * @param paramMap
     */
    @Override
    public void doWorkSubmitCommonWithRequest(String schemaName, String subWorkCode, String worRequestCode, Map<String, Object> paramMap) {
        this.doWorkSubmitCommon(schemaName, "sub_work_code", 1, subWorkCode, paramMap);
        this.doWorkSubmitCommon(schemaName, "work_request_code", 5, worRequestCode, paramMap);
    }
//

    /**
     * 工单提交公共处理函数
     *
     * @param schemaName
     * @param keyName
     * @param tableType
     * @param subWorkCode
     * @param paramMap
     */
    public boolean doWorkSubmitCommon(String schemaName, String keyName, int tableType, String subWorkCode, Map<String, Object> paramMap) {
        if (RegexUtil.optNotNull(subWorkCode).isPresent() && paramMap.containsKey("submitCommonInfo")) {
            Object workSubmitCommonInfo = paramMap.get("submitCommonInfo");
            if (RegexUtil.optNotNull(subWorkCode).isPresent() && null != workSubmitCommonInfo) {
                JSONObject jo = JSONObject.fromObject(workSubmitCommonInfo);
                if (jo.containsKey("tableType")) {
                    String tblType = jo.getString("tableType");
                    if (!tblType.equals(String.valueOf(tableType))) {
                        return true;
                    }
                }
                // 均值处理
                if (jo.containsKey("calculationInfo")) {
                    String calculationInfo = jo.getString("calculationInfo");
                    doWorkCalculationInfo(schemaName, keyName, tableType, subWorkCode, calculationInfo);
                    // 短信处理
                } else if (jo.containsKey("multipleSms")) {
                }
            }
        }
        return true;
    }
//
//    /**
//     * 解析前端数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public Map<String, Object> doAnalysisPageData(String schemaName, Map<String, Object> paramMap) throws Exception {
//        return dynamicAnalysisService.doAnalysisPageData(schemaName, paramMap);
//    }
//

    /**
     * 流程操作人校验
     *
     * @param schemaName
     * @param account
     * @param subWorkCode
     * @throws Exception
     */
    @Override
    public void doCheckFlowInfo(String schemaName, String account, String subWorkCode) {
        List<String> subWorkCodes = new ArrayList<>();
        if (RegexUtil.optNotNull(subWorkCode).isPresent()) {
            subWorkCodes.add(subWorkCode);
        }
//        long total = workflowService.getTasksCount(schemaName, null, null, subWorkCodes, account, null, null, null, null);
//        if (total < 1) {
//            throw new SenscloudException(SensConstant.PAGE_ERROR_SUBMIT_REPEAT);
//        }
    }
//
//    /**
//     * 整合小程序回显字段及数据
//     *
//     * @param schemaName
//     * @param request
//     * @param account
//     * @param result
//     * @param template
//     * @param oldData
//     * @param isAssetScan
//     * @param isDistribute
//     * @param subWorkCode
//     */
//    public void getPageInfo(String schemaName, HttpServletRequest request, String account, JSONObject result,
//                            MetadataWork template, Map<String, Object> oldData, Boolean isAssetScan,
//                            Boolean isDistribute, String subWorkCode) {
//        String dyVersion = request.getParameter("dyVersion");
//        if (RegexUtil.isNull(dyVersion)) {
//            getPageInfoV301(schemaName, request, result, template, oldData, isAssetScan, isDistribute, subWorkCode);
//        } else if ("401".equals(dyVersion)) {
//            dyToPageService.getPageInfoV401(schemaName, result, template, oldData, isAssetScan, isDistribute, subWorkCode);
//        }
//    }
//
//    /**
//     * 整合小程序回显字段及数据（v3.0.1）
//     *
//     * @param schemaName
//     * @param request
//     * @param result
//     * @param template
//     * @param oldData
//     * @param isAssetScan
//     * @param isDistribute
//     * @param subWorkCode
//     */
//    private void getPageInfoV301(String schemaName, HttpServletRequest request, JSONObject result,
//                                 MetadataWork template, Map<String, Object> oldData, Boolean isAssetScan,
//                                 Boolean isDistribute, String subWorkCode) {
//        String bodyProperty = null; // 模板数据
//        int relation_type = 0;
//        if (null != template) {
//            bodyProperty = template.getBodyProperty();
//            relation_type = template.getRelationType();
//        }
//        if (null != bodyProperty && !"".equals(bodyProperty)) {
//            // 页面返回参数
//            List<Map<String, Object>> showList = new ArrayList<Map<String, Object>>();
//            Map<String, List<Map<String, Object>>> sectionInfo = new HashMap<String, List<Map<String, Object>>>();
//            Set<String> sectionKeys = new LinkedHashSet<String>();
//            List<Map<String, Object>> hideList = new ArrayList<Map<String, Object>>();
//            List<Map<String, Object>> btnList = new ArrayList<Map<String, Object>>();
//            Map<String, List<String>> selectKeyList = new HashMap<String, List<String>>();
//            Map<String, Object> linkAgeInfo = new HashMap<String, Object>(); // 所有联动根字段
//            Map<String, Object> linkAgeDtlList = new HashMap<String, Object>(); // 所有待联动字段信息
//            Map<String, Object> pageAllValue = new HashMap<String, Object>(); // 所有字段信息
//            Map<String, Object> pageRowDataInfo = new HashMap<String, Object>(); // 行编辑信息
//            Map<String, Object> pageListColumn = new HashMap<String, Object>(); // 列表列信息
//            Map<String, Object> pageImgInfo = new HashMap<String, Object>(); // 图片信息
//            String selectKey = "";
//            List<String> selectKeyTmp = null;
//            Map<String, Object> pageSelectValues = new HashMap<String, Object>();
//            Map<String, Object> pageSelectDefaultOption = new HashMap<String, Object>();
//            Map<String, Object> relationInfo = new HashMap<String, Object>(); // 联动用
//            Integer relationType = null;
//            String businessNo = null;
//            Boolean feeRight = false;
//
//            Map<String, Object> tmp = null; // 临时对象
//            Map<String, JSONObject> dataJsonInfo = null;
//
//            Integer isMain = 0;
//            Integer status = 0;
//            Integer subWorkCnt = 0;
//
//            // 原数据json（键值对）缓存
//            if (null != oldData && oldData.size() > 0) {
//                feeRight = (Boolean) oldData.get("feeRight");
//                businessNo = (String) oldData.get("businessNo");
//                isMain = (Integer) oldData.get("is_main");
//                if (null == isMain || "".equals(isMain)) {
//                    isMain = 1;
//                }
//                subWorkCnt = (Integer) oldData.get("subWorkCnt");
//                if (null == subWorkCnt || "".equals(subWorkCnt)) {
//                    subWorkCnt = 0;
//                }
//                try {
//                    status = (Integer) oldData.get("status");
//                } catch (Exception stExp) {
//                    status = ((Long) oldData.get("status")).intValue();
//                }
//                if (!isAssetScan) {
//                    Object oldDodyProperty = oldData.get("body_property"); // 原模板数据
//                    if (null != oldDodyProperty && !"".equals(oldDodyProperty)) {
//                        dataJsonInfo = new HashMap<String, JSONObject>();
//                        JSONArray arrays = JSONArray.fromObject(JSONObject.fromObject(oldDodyProperty).get("value"));
//                        JSONObject data = null;
//                        for (Object object : arrays) {
//                            data = JSONObject.fromObject(object);
//                            dataJsonInfo.put(data.get("fieldFormCode").toString(), data);
//                        }
//                    }
//                }
//            }
//
//
//            User user = AuthService.getLoginUser(request);
//            user = commonUtilService.checkUser(schemaName, user, request);
//            String clientType = request.getParameter("clientType");
//
//            // 扩展信息
//            Map<String, Object> extInfo = selectOptionService.searchLoginUserInfo();
//            pageAllValue.put("signMapRadius", selectOptionService.getOptionNameByCode(schemaName, "map_radius", "1", null));
//
//            // 新模板数据处理
//            JSONArray arrays = JSONArray.fromObject(bodyProperty);
//            String fieldFormCode = null; // 页面字段名
//            String fieldCode = null; // 数据库字段名
//            String fieldName = null; // 数据库字段名称
//            String fieldRight = null; // 页面字段权限
//            String fieldViewType = null; // 页面控件类型
//            String fieldBaseViewType = null; // 页面控件类型
//            String fieldSectionType = null; // 页签类型
//            String fvRelation = null; // 页面特殊配置类型
//            String fvrDtl = null; // 页面特殊配置内容
//            Object value = null; // 页面字段值
//            Object tmpValue = null; // 临时用
//            JSONObject data = null;
//            JSONObject oldJsonData = null;
//            for (Object object : arrays) {
//                data = JSONObject.fromObject(object);
//                fieldFormCode = data.get("fieldFormCode").toString();
//                fieldCode = data.get("fieldCode").toString();
//                value = data.get("fieldValue");
//                fieldViewType = data.get("fieldViewType").toString();
//                fieldName = data.get("fieldName").toString();
//                fieldName = selectOptionService.getLanguageInfo(fieldName);
//                data.put("fieldName", fieldName);
//                data.put("fieldTextName", fieldName);
//                // 新模板默认值
//                if (null == value || "".equals(value)) {
//                    // 获取原值
//                    if (null != dataJsonInfo && dataJsonInfo.size() > 0) {
//                        oldJsonData = dataJsonInfo.get(fieldFormCode);
//                        if (null != oldJsonData) {
//                            value = oldJsonData.get("fieldValue");
//                            // 对象
//                            if ("8".equals(fieldViewType) && oldJsonData.containsKey("relationContent")) {
//                                try {
//                                    data.put("relationContent", oldJsonData.get("relationContent").toString().replace(":null", ":\"\""));
//                                    result.put("relationNames", oldJsonData.get("relationNames").toString().replace(":null", ":\"\""));
//                                } catch (Exception taskExp) {
//                                    logger.warn("任务数据解析异常！" + oldJsonData.get("taskContent"));
//                                }
//                            }
//                            // 任务
//                            if ("9".equals(fieldViewType) && oldJsonData.containsKey("taskContent")) {
//                                try {
//                                    data.put("taskContent", oldJsonData.get("taskContent").toString().replace(":null", ":\"\""));
//                                } catch (Exception taskExp) {
//                                    logger.warn("任务数据解析异常！" + oldJsonData.get("taskContent"));
//                                }
//                            }
//                            // 列表
//                            if ("12".equals(fieldViewType) && oldJsonData.containsKey("bomContent")) {
//                                try {
//                                    data.put("bomContent", oldJsonData.get("bomContent").toString().replace(":null", ":\"\""));
//                                } catch (Exception taskExp) {
//                                    logger.warn("列表数据解析异常！" + oldJsonData.get("bomContent"));
//                                }
//                            }
//                        }
//                    }
//
//                    // 取原固定字段值
//                    if (null != oldData && oldData.size() > 0) {
//                        // 取原固定字段值
//                        if (oldData.containsKey(fieldCode)) {
//                            value = oldData.get(fieldCode); // 公共字段值
//                        } else if ("9".equals(fieldViewType)) {
//                            if (null != businessNo && ("51".equals(businessNo) || "52".equals(businessNo))) {
//                                try {
//                                    List<Map<String, Object>> taskList = (List<Map<String, Object>>) oldData.get("taskContent");
//                                    for (Map<String, Object> task : taskList) {
//                                        task.put("taskRight", "readonly");
//                                        task.put("nowModalKey", fieldFormCode);
//                                    }
//                                    JSONObject taskListObject = new JSONObject();
//                                    taskListObject.put("key", "WTT000049");
//                                    taskListObject.put("data", taskList);
//                                    data.put("taskContent", taskListObject); // 任务
//                                } catch (Exception taskExp) {
//                                    logger.warn("任务数据解析异常！" + subWorkCode);
//                                }
//                            }
//                        }
//                    }
//                    if (null == value || "".equals(value) || "null".equals(value) || JSONNull.getInstance().equals(value)) {
//                        value = "";
//                    }
//                    data.put("fieldValue", value); // 数据回显
//                }
//
//                // 是否需要生成子工单
//                if ("is_have_sub".equals(fieldCode)) {
//                    result.put("isHaveSub", value); // 数据回显
//                }
//
//                // 状态（页面特殊操作，单独列出）
//                if ("status".equals(fieldCode)) {
//                    result.put("status", value);
//                }
//
//                // 工单池
//                if ("pool_id".equals(fieldCode)) {
//                    if (null == value || "".equals(value)) {
//                        value = -1;
//                        data.put("fieldValue", value); // 数据回显
//                    }
//                    result.put("poolId", value);
//                }
//
//                // 工单类型
//                if ("work_type_id".equals(fieldCode)) {
//                    result.put("workTypeId", value);
//                }
//
//                // 场地、设备
//                if ("relation_id".equals(fieldCode)) {
//                    result.put("facilityId", value);
//                }
//
//                // 对象类型（页面特殊操作，单独列出）
//                if ("relation_type".equals(fieldCode)) {
//                    if (value != "") {
//                        relationType = Integer.valueOf(String.valueOf(value));
//                    } else {
//                        relationType = relation_type;
//                        data.put("fieldValue", relationType); // 数据回显
//                    }
//                }
//                // 对象类型（页面特殊操作，单独列出）
//                if ("relation_type".equals(fieldCode)) {
//                    result.put("relationType", relationType);
//                }
//                fieldBaseViewType = null;
//                // 关联字段
//                if (data.containsKey("fieldViewRelation") && !"".equals(data.get("fieldViewRelation")) && !"0".equals(data.get("fieldViewRelation"))) {
//                    fvRelation = data.get("fieldViewRelation").toString();
//                    fvrDtl = data.get("fieldViewRelationDetail").toString();
//                    if ("3".equals(fvRelation)) {
//                        if ("receive_account".equals(fieldCode)) {
//                            result.put("rolePermissionKey", fvrDtl); // 角色权限，负责人过滤用
//                        }
//                    } else if ("4".equals(fvRelation)) {
//                        try {
//                            fieldBaseViewType = fieldViewType;
//                            fieldViewType = fvrDtl.split(",")[0]; // 小程序问题上报取第一个特殊值
//                        } catch (Exception vtExp) {
//
//                        }
//                    } else if ("6".equals(fvRelation)) {
//                        RelationUtil.getDynamicLinkageInfo(linkAgeInfo, linkAgeDtlList, fvrDtl, fieldFormCode, fieldViewType, (String) data.get("fieldDataBase")); // 联动
//                        JSONObject jo = JSONObject.fromObject(fvrDtl);
//                        // 手机端特殊显示方式（同fvRelation=4，4需要合并删除）
//                        if (jo.containsKey("phoneShowType")) {
//                            fieldBaseViewType = fieldViewType;
//                            fieldViewType = (String) jo.get("phoneShowType"); // 平台特殊字段处理类型
//                            data.put("phoneShowType", fieldViewType);
//                        }
//                    } else if ("8".equals(fvRelation)) {
//                        JSONObject jo = JSONObject.fromObject(fvrDtl);
//                        Object pageViewType = jo.get("pageViewType"); // 平台特殊字段处理类型
//                        if (null != pageViewType) {
//                            if ("PM".equals(pageViewType)) {
//                                continue;
//                            }
//                        }
//                        // 手机端特殊显示方式（同fvRelation=4，4需要合并删除）
//                        if (jo.containsKey("phoneShowType")) {
//                            fieldBaseViewType = fieldViewType;
//                            fieldViewType = (String) jo.get("phoneShowType"); // 平台特殊字段处理类型
//                            data.put("phoneShowType", fieldViewType);
//                        }
//                        // 设备列表过滤条件
//                        if (jo.containsKey("assetSelectStatus")) {
//                            pageAllValue.put("assetSelectStatus", jo.get("assetSelectStatus"));
//                        }
//                        // 行编辑数据
//                        if (jo.containsKey("rowDataInfo")) {
//                            JSONObject rowDataInfo = JSONObject.fromObject(jo.get("rowDataInfo"));
//                            // 自定义字段
//                            if ("0".equals(data.get("saveType"))) {
//                                rowDataInfo.put("jsonInfo", data);
//                            }
//                            pageRowDataInfo.put(fieldFormCode, rowDataInfo);
//                        }
//                        // 图片
//                        if (jo.containsKey("imgInfo")) {
//                            pageImgInfo.put(fieldFormCode, jo.get("imgInfo"));
//                        }
//                        // 设置检索条件【位置】取值字段
//                        String modalFacilityCode = (String) jo.get("bomStockSearchValue");
//                        if (RegexUtil.isNotNull(modalFacilityCode)) {
//                            data.put("modalFacilityCode", modalFacilityCode); // 页面弹出层检索条件控制
//                        }
//                        // 设置检索条件【位置】操作权限
//                        String modalFacilityRight = (String) jo.get("bomStockSearchRight");
//                        data.put("modalFacilityRight", true);
//                        if (RegexUtil.isNotNull(modalFacilityRight) && "readonly".equals(modalFacilityRight)) {
//                            data.put("modalFacilityRight", false); // 页面弹出层检索条件操作权限控制
//                        }
//                        // 列表列控制
//                        if (jo.containsKey("listColumn")) {
//                            pageListColumn.put(fieldFormCode, jo.get("listColumn"));
//                            try {
//                                // 列表
//                                if ("12".equals(fieldViewType)) {
//                                    // 备件
//                                    if ("3".equals(data.get("dbTableType").toString())) {
//                                        data.put("modalUrl", "/bom_searchBomList"); // 页面弹出层数据查接口
//                                        data.put("modalFacilityShow", "modalStockType"); // 页面弹出层检索条件控制
//                                    }
//                                }
//                                JSONObject listColumn = JSONObject.fromObject(jo.get("listColumn"));
//                                if (null != listColumn && listColumn.size() > 0) {
//                                    List<Object> delKey = new ArrayList<Object>();
//                                    JSONObject tmpLcObject = null;
//                                    String searchSelectType = null;
//                                    String rightTypeColumn = null;
//                                    for (Object key : listColumn.keySet()) {
//                                        tmpLcObject = JSONObject.fromObject(listColumn.get(key));
//                                        searchSelectType = (String) tmpLcObject.get("stockType");
//                                        // 查询接口中没有【所属库房】参数
//                                        if (RegexUtil.isNotNull(searchSelectType) && "1".equals(searchSelectType)) {
//                                            data.remove("modalFacilityShow"); // 页面弹出层检索条件控制
//                                            data.remove("modalFacilityRight"); // 页面弹出层检索条件操作权限控制
//                                            data.put("modalUrl", "/bom_search_list"); // 页面弹出层数据查接口
//                                        }
//
//                                        rightTypeColumn = (String) tmpLcObject.get("rightType");
//                                        if (RegexUtil.isNotNull(rightTypeColumn) && "1".equals(rightTypeColumn) && !feeRight) {
//                                            delKey.add(key);
//                                        }
//                                    }
//                                    if (null != delKey && delKey.size() > 0) {
//                                        for (Object key : delKey) {
//                                            listColumn.remove(key);
//                                        }
//                                        pageListColumn.put(fieldFormCode, listColumn);
//                                    }
//                                }
//                            } catch (Exception plcExp) {
//
//                            }
//                        }
//                        // 取当前登录用户信息
//                        if (jo.containsKey("nowUser")) {
//                            try {
//                                value = extInfo.get(jo.get("nowUser"));
//                                data.put("fieldValue", value); // 数据回显
//                            } catch (Exception nowUserExp) {
//
//                            }
//                        }
//                        // 联动【与6相同，6需要合并】
//                        if (jo.containsKey("linkage")) {
//                            RelationUtil.getDynamicLinkageInfo(linkAgeInfo, linkAgeDtlList, jo.get("linkage").toString(), fieldFormCode, fieldViewType, (String) data.get("fieldDataBase")); // 联动
//                            // 手机端特殊显示方式（同fvRelation=4，4需要合并删除）
//                            if (jo.containsKey("phoneShowType")) {
//                                fieldBaseViewType = fieldViewType;
//                                fieldViewType = (String) jo.get("phoneShowType"); // 平台特殊字段处理类型
//                                data.put("phoneShowType", fieldViewType);
//                            }
//                        }
//                        // 设置默认值
//                        if (jo.containsKey("default")) {
//                            JSONObject defaultInfo = JSONObject.fromObject(jo.get("default"));
//                            if (defaultInfo.containsKey("phone")) {
//                                data.put("fieldValue", defaultInfo.get("phone"));
//                            }
//                        }
//                        // 移动端特殊选择图片方式，拍照或选择
//                        if (jo.containsKey("imgType")) {
//                            data.put("imgType", jo.get("imgType"));
//                        }
//                        // 设置醒目
//                        if (jo.containsKey("xingmu")) {
//                            data.put("xingmu", jo.get("xingmu"));
//                        }
//                        //下拉选默认选项
//                        if (jo.containsKey("defaultOption")) {
//                            JSONObject defaultOption = JSONObject.fromObject(jo.get("defaultOption"));
//                            pageSelectDefaultOption.put(fieldFormCode, defaultOption);
//                        }
//                        // 首层属性
//                        if (jo.containsKey("firstAttr")) {
//                            JSONArray firstAttrArray = JSONArray.fromObject(jo.get("firstAttr"));
//                            JSONObject tmpParam = null;
//                            String firstAttrType = null;
//                            for (Object fab : firstAttrArray) {
//                                tmpParam = JSONObject.fromObject(fab);
//                                firstAttrType = (String) tmpParam.get("type");
//                                String tmpKey = (String) tmpParam.get("key");
//                                // 字段属性（6：手机端特殊属性）
//                                if ("1".equals(firstAttrType) || "3".equals(firstAttrType) || "6".equals(firstAttrType)) {
//                                    if (tmpParam.containsKey("info")) {
//                                        data.put(tmpKey, tmpParam.get("info")); // 首层属性
//                                    } else if (tmpParam.containsKey("value")) {
//                                        data.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                                    } else if (tmpParam.containsKey("extInfo")) {
//                                        data.put(tmpKey, extInfo.get(tmpParam.get("extInfo")));
//                                    }
//                                    if (tmpParam.containsKey("rightType")) {
//                                        String rightType = (String) tmpParam.get("rightType");
//                                        if ("1".equals(rightType) && !feeRight) {
//                                            data.remove(tmpKey);
//                                        }
//                                    }
//                                    // 节点属性
//                                } else if ("2".equals(firstAttrType)) {
//                                    if (tmpParam.containsKey("info")) {
//                                        pageAllValue.put(tmpKey, tmpParam.get("info")); // 首层属性
//                                    } else if (tmpParam.containsKey("value")) {
//                                        pageAllValue.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                                    }
//                                    if (tmpParam.containsKey("rightType")) {
//                                        String rightType = (String) tmpParam.get("rightType");
//                                        if ("1".equals(rightType) && !feeRight) {
//                                            pageAllValue.remove(tmpKey);
//                                        }
//                                    }
//                                    // 归类属性
//                                } else if ("4".equals(firstAttrType)) {
//                                    Map<String, Object> tmpInfo = (Map<String, Object>) pageAllValue.get(tmpKey);
//                                    if (null == tmpInfo) {
//                                        tmpInfo = new HashMap<String, Object>();
//                                    }
//                                    if (tmpParam.containsKey("info")) {
//                                        tmpInfo.put(tmpKey, tmpParam.get("info")); // 首层属性
//                                    } else if (tmpParam.containsKey("value")) {
//                                        tmpInfo.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                                    }
//                                    if (tmpParam.containsKey("rightType")) {
//                                        String rightType = (String) tmpParam.get("rightType");
//                                        if ("1".equals(rightType) && !feeRight) {
//                                            tmpInfo.remove(tmpKey);
//                                        }
//                                    }
//                                    pageAllValue.put(tmpKey, tmpInfo);
//                                    // 页面信息
//                                } else if ("5".equals(firstAttrType)) {
//                                    if (tmpParam.containsKey("info")) {
//                                        result.put(tmpKey, tmpParam.get("info")); // 首层属性
//                                    } else if (tmpParam.containsKey("value")) {
//                                        result.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                                    } else if (tmpParam.containsKey("attr")) {
//                                        result.put(tmpKey, data.get(tmpParam.get("attr"))); // 首层属性值
//                                    } else if (tmpParam.containsKey("old")) {
//                                        if (null != oldData) {
//                                            result.put(tmpKey, oldData.get(tmpParam.get("old"))); // 首层属性值
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                if (data.containsKey("rightType")) {
//                    String rightType = (String) data.get("rightType");
//                    if ("1".equals(rightType) && !feeRight) {
//                        fieldViewType = "10";
//                    }
//                }
//                // 日期
//                if (fieldCode.endsWith("_time")) {
//                    if (null != value && !"".equals(value)) {
//                        try {
//                            Timestamp ts = (Timestamp) value;
//                            SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//                            value = dateFormat.format(ts);
//                        } catch (Exception e) {
//                            logger.warn("日期格式未转换：" + fieldCode);
//                        }
//                        data.put("fieldValue", value); // 数据回显
//                    }
//                }
//                // 隐藏值
//                if ("10".equals(fieldViewType)) {
////                    if ("formActionName".equals(fieldCode)) {
////                        result.put("formActionName", value.toString().replace("/", "_")); // 页面标题
////                        continue;
////                    }
//                    pageAllValue.put(fieldFormCode, data);
//                    hideList.add(data);
//                    continue;
//                }
//                // 按钮
//                if ("7".equals(fieldViewType)) {
//                    // 分配页面只在可分配的页面出现，分配判断需要前端给予
//                    if (value.equals(String.valueOf(ButtonConstant.BTN_DISTRIBUTION)) && !isDistribute) {
//                        continue;
//                    } else if (fieldCode.startsWith("btnExportA1") && !SensConstant.APP_CLIENT_NAME.equalsIgnoreCase(clientType)) {
//                        continue;
//                    }
//                    pageAllValue.put(fieldFormCode, data);
//                    btnList.add(data);
//                    continue;
//                }
//                // 子工单数据（主工单编辑状态时，子工单信息隐藏）
//                if ("14".equals(fieldViewType)) {
//                    if (1 == isMain && StatusConstant.DRAFT == status) {
//                        hideList.add(data);
//                        continue;
//                    } else {
//                        try {
//                            data.put("subListContent", oldData.get("subWorkList"));
//                        } catch (Exception taskExp) {
//                            logger.warn("子工单数据解析异常！" + subWorkCode);
//                        }
//                    }
//                }
//                // 子工单数据（主工单提交后，子工单信息显示，任务信息、点巡检对象信息隐藏）
//                if (("9".equals(fieldViewType)) && subWorkCnt > 0) {
//                    hideList.add(data);
//                    continue;
//                }
//                fieldRight = data.get("fieldRight").toString();
//                // 纯文本
//                if (null != fieldRight && "readonly".equals(fieldRight)) {
//                    if (",1,4,5,7,10,".contains("," + fieldViewType + ",")) {
//                        data.put("isText", "text"); // 数据回显文本
//                        if (null != relationInfo && relationInfo.size() > 0 && relationInfo.containsKey(fieldCode)) {
//                            value = relationInfo.get(fieldCode);
//                        }
//                        if (null != value && !"".equals(value)) {
//                            data.put("fieldTextValue", value); // 数据回显
//                        } else {
//                            data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.NONE_A)); // 数据回显
//                        }
//                    }
//                    // 图片显示数量
//                    if ("6".equals(fieldViewType)) {
//                        data.put("isText", "text"); // 数据回显文本
//                        try {
//                            if (null != value && !"".equals(value)) {
//                                data.put("fieldTextValue", value.toString().split(",").length); // 数据回显
//                            } else {
//                                data.put("fieldTextValue", 0); // 数据回显
//                            }
//                        } catch (Exception imgExp) {
//                            data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.NONE_A)); // 数据回显
//                        }
//                    }
//                }
//                // 下拉框、树形、多选、页签
//                if (",2,3,30,33,".contains("," + fieldViewType + ",") || ",2,3,30,33,".contains("," + fieldBaseViewType + ",")) {
//                    selectKey = data.get("fieldDataBase").toString();
//                    if ((null != fieldRight && "readonly".equals(fieldRight)) || "32".equals(fieldViewType) || "3".equals(fieldViewType)) {
//                        if ((null != fieldRight && "readonly".equals(fieldRight))) {
//                            data.put("isText", "text"); // 数据回显文本
//                        } else if ("3".equals(fieldViewType)) {
//                            pageSelectValues.put(fieldFormCode, value);
//                        }
//                        if (null == value || "-1".equals(value) || "".equals(value)) {
//                            // 工单池
//                            if ("pool_id".equals(fieldCode)) {
//                                data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.DWOP_A)); // 数据回显[默认工单池]
//                            } else {
//                                data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.NONE_A)); // 数据回显
//                            }
//                        } else {
//                            // 多选
//                            boolean hasSetValue = false;
//                            if ("30".equals(fieldViewType)) {
//                                try {
//                                    JSONArray tmpArray = (JSONArray) value;
//                                    if (tmpArray.size() == 0) {
//                                        data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.NONE_A)); // 数据回显
//                                        hasSetValue = true;
//                                    } else {
//                                        value = StringUtils.join(tmpArray, ",");
//                                    }
//                                } catch (Exception jaExp) {
//
//                                }
//                                if (5 == Integer.valueOf(data.get("dbTableType").toString()) && RegexUtil.isNotNull(subWorkCode)) {
//                                    tmpValue = workSheetHandleService.queryWorkDutymanHourUserString(schemaName, subWorkCode);
//                                    data.put("fieldTextValue", tmpValue); // 数据回显
//                                    hasSetValue = true;
//                                }
//                                selectKey = selectKey + "_multiple";
//                            }
//                            if (!hasSetValue) {
//                                try {
//                                    JSONObject defaultOption = null;
//                                    if (null != pageSelectDefaultOption.get(fieldFormCode)) {
//                                        defaultOption = (JSONObject) pageSelectDefaultOption.get(fieldFormCode);
//                                    }
//                                    // 对象（页面特殊操作，单独列出）
//                                    if ("relation_id".equals(fieldCode)) {
//                                        tmp = selectOptionService.getOptionByCode(schemaName, selectKey, value.toString());
//                                        tmpValue = tmp.get("relation");
//                                        result.put("relationCode", tmpValue); // 手机端验证编码是否一致使用
//                                        try {
//                                            relationInfo.put("relation_name", tmp.get("desc").toString().split("\\(")[0]); // 小程序设备不用带括号
//                                        } catch (Exception re) {
//                                            relationInfo.put("relation_name", tmp.get("desc")); // 数据回显
//                                        }
//                                    } else if (null != defaultOption && value.equals(defaultOption.get("code"))) {
//                                        tmpValue = defaultOption.get("desc");
//                                        tmp = defaultOption;
//                                    } else {
//                                        tmpValue = selectOptionService.getOptionNameByCode(schemaName, selectKey, value.toString(), null);
//                                        tmp = selectOptionService.getOptionByCode(schemaName, selectKey, value.toString());
//                                    }
//                                    if (null != tmpValue && !"".equals(tmpValue)) {
//                                        data.put("selectOption", tmp); // 数据回显
//                                        data.put("fieldTextValue", tmpValue); // 数据回显
//                                    } else {
//                                        data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.NONE_A)); // 数据回显
//                                    }
//                                    if ("work_type_id".equals(fieldCode)) {
//                                        result.put("pageTitle", tmpValue.toString()); // 页面标题
//                                    }
//                                } catch (Exception e) {
//                                    logger.warn("下拉选值获取为空" + fieldCode);
//                                }
//                            }
//                        }
//                    } else if (",2,30,33,".contains("," + fieldViewType + ",")) {
//                        if (selectKeyList.containsKey(selectKey)) {
//                            selectKeyTmp = selectKeyList.get(selectKey);
//                        } else {
//                            selectKeyTmp = new ArrayList<String>();
//                        }
//                        selectKeyTmp.add(fieldFormCode);
//                        selectKeyList.put(selectKey, selectKeyTmp);
//                        pageSelectValues.put(fieldFormCode, value);
//                    }
//                }
//
//                // 二维码
//                if ("31".equals(fieldViewType)) {
//                    try {
//                        selectKey = data.get("fieldDataBase").toString();
//                        // 对象（页面特殊操作，单独列出）
//                        if ("relation_id".equals(fieldCode)) {
//                            tmp = selectOptionService.getOptionByCode(schemaName, selectKey, value.toString());
//                            tmpValue = tmp.get("relation");
//                            try {
//                                relationInfo.put("relation_name", tmp.get("desc").toString().split("\\(")[0]); // 小程序设备不用带括号
//                            } catch (Exception re) {
//                                relationInfo.put("relation_name", tmp.get("desc")); // 数据回显
//                            }
//                        } else {
//                            tmpValue = selectOptionService.getOptionNameByCode(schemaName, selectKey, value.toString(), null);
//                        }
//                        if (null != tmpValue && !"".equals(tmpValue)) {
//                            data.put("fieldTextValue", tmpValue); // 数据回显
//                        } else {
//                            data.put("fieldTextValue", selectOptionService.getLanguageInfo(LangConstant.NONE_A)); // 数据回显
//                        }
//                    } catch (Exception e) {
//                        logger.warn("下拉选值获取为空" + fieldCode);
//                    }
//                }
//
//                // 行控件
//                if (",6,8,9,11,12,33,34,".contains("," + fieldViewType + ",")) {
//                    data.put("isLine", "isLine"); // 占一整行
//                    // 列表
//                    if ("12".equals(fieldViewType)) {
//                        // 备件
//                        if ("3".equals(data.get("dbTableType").toString())) {
//                            if (null != data.get("bomContent") && !"null".equals(data.get("bomContent")) && !JSONNull.getInstance().equals(data.get("bomContent"))) {
//                                result.put("bomList", data.get("bomContent"));
//                            } else {
//                                result.put("bomList", "[]");
//                                data.put("bomContent", "");
//                            }
////                            if (RegexUtil.isNull(account)) {
////                                result.put("storeList", null); // 有权限的库房信息
////                            } else {
////                                result.put("storeList", stockService.getStockListByUserPermission(schemaName, "", account)); // 有权限的库房信息
////                            }
//                        } else if ("4".equals(data.get("dbTableType").toString())) {
//                            if (null != data.get("bomContent") && !"null".equals(data.get("bomContent")) && !JSONNull.getInstance().equals(data.get("bomContent"))) {
//                                result.put("workFeeList", data.get("bomContent"));
//                            } else {
//                                result.put("workFeeList", "[]");
//                                data.put("bomContent", "");
//                            }
////                        } else {
////                            Object bomContent = data.get("bomContent");
////                            if (null != bomContent && !"null".equals(bomContent) && !JSONNull.getInstance().equals(bomContent)) {
////                                JSONArray tmpDataList = JSONArray.fromObject(bomContent);
////                                if (null != tmpDataList && tmpDataList.size() > 0 && pageListColumn.containsKey(fieldFormCode)) {
////                                    JSONObject listColumn = JSONObject.fromObject(pageListColumn.get(fieldFormCode));
////                                    if (null != listColumn && listColumn.size() > 0) {
////                                        Map<String, String> columnCheckInfo = new HashMap<>();
////                                        for (Object key : listColumn.keySet()) {
////                                            JSONObject tmpLcObject = JSONObject.fromObject(listColumn.get(key));
////                                            String dbColumnCode = (String) tmpLcObject.get("code");
////                                            if (RegexUtil.isNotNull(dbColumnCode)) {
////                                                columnCheckInfo.put(dbColumnCode, "tmp_" + key);
////                                            }
////                                        }
////                                        if (null != columnCheckInfo && columnCheckInfo.size() > 0) {
////                                            for (Object tdo : tmpDataList) {
////                                                JSONObject row = JSONObject.fromObject(tdo);
////                                                for (String column : columnCheckInfo.keySet()) {
////                                                    String code = columnCheckInfo.get(column);
////                                                    row.put(code, row.get(column));
////                                                }
////                                            }
////                                            data.put("bomContent", tmpDataList);
////                                        }
////                                    }
////                                }
////                            }
//                        }
//                    } else if ("11".equals(fieldViewType)) {
//                        try {
//                            result.put("commentList", JSONArray.fromObject(JSONObject.fromObject(value).get("value"))); // 评论
//                        } catch (Exception clExp) {
//                            logger.warn("评论解析未成功！");
//                        }
//                    }
//                }
//
//                // 满意度
//                if ("34".equals(fieldViewType)) {
//                    try {
//                        data.put("fieldDetailRight", "readonly");
//                        if (data.containsKey("ratingCheck")) {
//                            if ((null == value || "".equals(value))) {
//                                Map<String, Object> ratingCheckInfo = (Map<String, Object>) data.get("ratingCheck");
//                                String ratingCheckStatus = (String) ratingCheckInfo.get("status");
//                                if (RegexUtil.isNull(ratingCheckStatus) || Integer.valueOf(ratingCheckStatus) <= status) {
//                                    ratingCheckInfo.put("subWorkCode", subWorkCode);
//                                    boolean isReadOnly = workSheetHandleService.checkRatingByInfo(ratingCheckInfo);
//                                    if (!isReadOnly) {
//                                        data.put("fieldDetailRight", "edit");
//                                    }
//                                }
//                            }
//                        } else {
//                            boolean isCreateUserData = (boolean) oldData.get("isCreateUserData");
//                            if ((null == value || "".equals(value)) && isCreateUserData && StatusConstant.COMPLETED == status) {
//                                data.put("fieldDetailRight", "edit");
//                            }
//                        }
//                    } catch (Exception e) {
//                        logger.warn("工单满意度值设置有误：" + value);
//                    }
//                }
//                fieldSectionType = (String) data.get("fieldSectionType");
//                pageAllValue.put(fieldFormCode, data);
//                // 进行页签分组
//                if (RegexUtil.isNotNull(fieldSectionType) && !"-1".equals(fieldSectionType)) {
//                    sectionKeys.add(fieldSectionType);
//                    List<Map<String, Object>> contentList = null;
//                    if (sectionInfo.containsKey(fieldSectionType)) {
//                        contentList = sectionInfo.get(fieldSectionType);
//                    } else {
//                        contentList = new ArrayList<Map<String, Object>>();
//                    }
//                    contentList.add(data);
//                    sectionInfo.put(fieldSectionType, contentList);
//                }
//            }
//
//            if (null != sectionInfo && sectionInfo.size() > 0) {
//                Map<String, Object> sectionMap = null;
//                for (String key : sectionKeys) {
//                    // 分配页面只在可分配的页面出现，分配判断需要前端给予
//                    if ("Distribute".equals(key) && !isDistribute) {
//                        continue;
//                    }
//                    sectionMap = new HashMap<String, Object>();
//                    sectionMap.put("title", selectOptionService.getOptionNameByCode(schemaName, "dy_section_type", key, null));
//                    sectionMap.put("content", sectionInfo.get(key));
//                    showList.add(sectionMap);
//                }
//            }
//            pageAllValue.put("pageListColumn", pageListColumn);
//            pageAllValue.put("pageImgInfo", pageImgInfo);
//            // 扫码按钮是否显示【处理中】
//            String showScan = (String) request.getAttribute("showScan");
//            if (RegexUtil.isNotNull(showScan)) {
//                pageAllValue.put("showScan", showScan);
//                String showScanType = (String) pageAllValue.get("showScanType");
//                if (RegexUtil.isNotNull(showScanType)) {
//                    if ("1".equals(showScanType)) {
//                        // 开始时间为空时需要更新
//                        if (null != oldData && null != oldData.get("begin_time") && !"".equals(oldData.get("begin_time"))) {
//                            pageAllValue.remove("showScan");
//                        }
//                    } else if ("2".equals(showScanType)) {
//                        pageAllValue.remove("showScan");
//                    }
//                }
//            }
//
//            // 库存消耗类型校验
//            if (pageAllValue.containsKey("checkBomConsume")) {
//                SystemConfigData scd = systemConfigService.getSystemConfigData(schemaName, SystemConfigConstant.BOM_CONSUME_TYPE);
//                String bomConsumeType = scd.getSettingValue();
//                if ("2".equals(bomConsumeType)) {
//                    pageAllValue.put("checkBomConsume", bomConsumeType);
//                } else {
//                    pageAllValue.remove("checkBomConsume");
//                }
//            }
//            result.put("pageRowDataInfo", pageRowDataInfo);
//            result.put("linkAgeInfo", linkAgeInfo);
//            result.put("linkAgeDtlList", linkAgeDtlList);
//            result.put("pageAllValue", pageAllValue);
//            result.put("pageSelectValues", pageSelectValues);
//            result.put("pageSelectDefaultOption", pageSelectDefaultOption);
//            result.put("selectKeyList", selectKeyList);
//            result.put("showList", showList);
//            result.put("hideList", hideList);
//            result.put("btnList", btnList);
//        }
//        String pageTitle = (String) request.getAttribute("pageTitle");
//        if (RegexUtil.isNotNull(pageTitle)) {
//            result.put("pageTitle", pageTitle); // 页面标题
//        }
//    }
//
//

    /**
     * 工单均值处理
     *
     * @param schemaName
     * @param subWorkCode
     * @param calculationInfo
     */
    private void doWorkCalculationInfo(String schemaName, String keyName, int tableType, String subWorkCode, String calculationInfo) {
        JSONArray cciArray = JSONArray.fromObject(calculationInfo);
        JSONObject cciInfo = null;
        String fieldViewType = null;
        String changeType = null;
        String cciCode = null;
        Double score = null;
        for (Object cci : cciArray) {
            cciInfo = JSONObject.fromObject(cci);
            if (cciInfo.containsKey("tableType")) {
                String tblType = cciInfo.getString("tableType");
                if (!tblType.equals(String.valueOf(tableType))) {
                    continue;
                }
            }
            fieldViewType = (String) cciInfo.get("fieldViewType");
            score = selectOptionService.selectPolymerizationJsonColumnData(schemaName, SqlConstant.WORK_DB_TABLES[tableType], keyName,
                    subWorkCode, "body_property", "fieldViewType",
                    fieldViewType, "AVG", "fieldValue");
            changeType = cciInfo.getString("changeType");
            cciCode = cciInfo.getString("fieldCode");
            // 保存工单明细表，更新分配人、状态、时间等信息
            Map<String, Object> dataInfo = new HashMap<String, Object>();
            dataInfo.put(changeType + "@" + cciCode, cciCode);
            dataInfo.put(cciCode, score);
            dataInfo.put("sub_work_code", subWorkCode);
            worksMapper.update(schemaName, dataInfo, "_sc_works_detail", "sub_work_code");
        }
    }
}
