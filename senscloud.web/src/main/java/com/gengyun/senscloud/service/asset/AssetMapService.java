package com.gengyun.senscloud.service.asset;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.response.AssetPositionDataResult;

import java.util.List;
import java.util.Map;

public interface AssetMapService {

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAssetMapPermission(MethodParam methodParam);

    /**
     * 获得所有的位置
     *
     * @param methodParam 入参
     * @param param 入参
     * @return
     */
    List<Map<String, Object>> getAllAssetMapPosition(MethodParam methodParam, SearchParam param);

    /**
     * 获取位置的地图，以及其下设备与设备统计
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<AssetPositionDataResult> getAssetPositionAndAssetForMap(MethodParam methodParam, SearchParam param);

    /**
     * 获取位置的地图，以及其下设备与设备统计
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getAssetAndAssetPositionForMap(MethodParam methodParam, SearchParam param);

    /**
     * 获得选择位置或设备的信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getAssetMapDetail(MethodParam methodParam, SearchParam param);
}
