package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.GroupMapper;
import com.gengyun.senscloud.model.GroupModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.GroupResult;
import com.gengyun.senscloud.response.PositionResult;
import com.gengyun.senscloud.service.*;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GroupServiceImpl implements GroupService {
    @Resource
    GroupMapper groupMapper;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    LogsService logService;
    @Resource
    RoleService roleService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getGroupListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[3]);
    }

    /**
     * 新增部门
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void newGroup(MethodParam methodParam, GroupModel groupModel) {
        Map<String, Object> group = new HashMap<>();
        group.put("group_name", groupModel.getGroup_name());
        group.put("group_code", SenscloudUtil.generateUUIDStr());
        group.put("parent_id", groupModel.getParent_id());
        group.put("remark", groupModel.getRemark());
        group.put("create_time", new Timestamp(System.currentTimeMillis()));
        group.put("create_user_id", methodParam.getUserId());
        group.put("is_out", groupModel.isIs_out());
        //有父部门判断父部门是否是外部 跟随父部门内外部
        if (RegexUtil.optNotNull(groupModel.getParent_id()).isPresent()) {
            Map<String, Object> parentGroup = groupMapper.findGroup(methodParam.getSchemaName(), groupModel.getParent_id());
            group.put("is_out", parentGroup.get("is_out"));
        }
        group.put("org_id", groupModel.getOrg_id());
        groupMapper.insertGroup(methodParam.getSchemaName(), group);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, group.get("id").toString(), LangUtil.doSetLogArray("添加了部门", LangConstant.TEXT_ADD_A, new String[]{LangConstant.TITLE_BA_M}));//添加部门
    }

    /**
     * 更新部门
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void modifyGroup(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getGroup_id(), LangConstant.TITLE_BA_M);
        Map<String, Object> group = new HashMap<>();
        Map<String, Object> oldMap = groupMapper.findGroup(methodParam.getSchemaName(), groupModel.getGroup_id());
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            group.put("id", groupModel.getGroup_id());
            group.put("group_name", groupModel.getGroup_name());
            group.put("group_code", groupModel.getGroup_code());
            group.put("parent_id", groupModel.getParent_id());
            group.put("remark", groupModel.getRemark());
            group.put("is_out", groupModel.isIs_out());
            groupMapper.updateGroup(methodParam.getSchemaName(), group);
            JSONArray log = LangUtil.compareMap(group, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, groupModel.getGroup_id().toString(), LangUtil.doSetLogArray("编辑了部门", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_BA_M, log.toString()}));
        }
    }

    /**
     * 删除部门
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void cutGroup(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getGroup_id(), LangConstant.TITLE_BA_M);
        GroupResult groupResult = groupMapper.findGroupById(methodParam.getSchemaName(), groupModel.getGroup_id());
        if (RegexUtil.optNotNull(groupResult).isPresent()) {
            //删除部门 先判断该部门下是否还有子部门和岗位
            List<GroupResult> groupResultList = groupMapper.findChildrenGroupList(methodParam.getSchemaName(), groupResult.getId());
            if (RegexUtil.optIsPresentList(groupResultList)) {
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_BA_M, LangConstant.TITLE_PG, LangConstant.TITLE_BA_M});//部门下还有子部门，不能删除部门
            }
            //获取该部门的岗位列表ids
            List<PositionResult> positionResultList = groupMapper.findPositionList(methodParam.getSchemaName(), groupModel.getGroup_id());
            if (RegexUtil.optIsPresentList(positionResultList)) {
                throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_BA_M, LangConstant.TITLE_PF, LangConstant.TITLE_BA_M});//部门下还有岗位，不能删除部门
            }
            //删除部门信息
            groupMapper.deleteGroup(methodParam.getSchemaName(), groupModel.getGroup_id());
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, groupModel.getGroup_id().toString(), LangUtil.doSetLogArray("删除了部门", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_BA_M, groupResult.getGroup_name()}));//删除了部门
        }
    }

    /**
     * 获取部门列表
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public List<GroupResult> getGroupList(MethodParam methodParam, GroupModel groupModel) {
        //部门列表
        List<GroupResult> groupList = groupMapper.findGroupList(methodParam.getSchemaName());
        for (GroupResult group : groupList
        ) {
            //根据部门获取岗位列表
            List<PositionResult> positionResultList = groupMapper.findPositionList(methodParam.getSchemaName(), group.getId());
            group.setPositions(positionResultList);
        }
        groupList = TreeUtil.convert(groupList, "id", "parent_id", "childrens");
        return groupList;
    }


    /**
     * 下拉框
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public List<GroupResult> getGroupSelectList(MethodParam methodParam, GroupModel groupModel) {
        List<GroupResult> groupList;
        List<Integer> ids = new ArrayList<>();
        //部门列表
        if (RegexUtil.optNotNull(groupModel.getId()).isPresent()) {
            if (RegexUtil.optNotNull(groupModel.isIs_out()).isPresent()) {
                ids = groupMapper.findGroupIds(methodParam.getSchemaName(), groupModel.getId());
            }
            groupList = groupMapper.findGroupOption(methodParam.getSchemaName(), groupModel.isIs_out(), groupModel.getId(), ids);
        } else {
            groupList = groupMapper.findGroupOption(methodParam.getSchemaName(), null, groupModel.getId(), ids);
        }
        for (GroupResult g : groupList
        ) {
            List<PositionResult> positions = groupMapper.findPositionList(methodParam.getSchemaName(), g.getId());
            g.setPositions(positions);
        }
        groupList = TreeUtil.convert(groupList, "id", "parent_id", "childrens");
        return groupList;
    }

    /**
     * 获取部门详情
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public GroupResult getGroupInfo(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getGroup_id(), LangConstant.TITLE_BA_M);
        return groupMapper.findGroupById(methodParam.getSchemaName(), groupModel.getGroup_id());
    }

    /**
     * 新增岗位
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void newPosition(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getGroup_id(), LangConstant.TITLE_PF);
        Map<String, Object> position = new HashMap<>();
        position.put("position_name", groupModel.getPosition_name());
        position.put("group_id", groupModel.getGroup_id());
        position.put("remark", groupModel.getRemark());
        position.put("create_time", new Timestamp(System.currentTimeMillis()));
        position.put("create_user_id", methodParam.getUserId());
        groupMapper.insertPosition(methodParam.getSchemaName(), position);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, position.get("id").toString(), LangUtil.doSetLogArray("添加了岗位", LangConstant.TEXT_ADD_A, new String[]{LangConstant.TITLE_BA_M}));//todo 多语言缺失 岗位
    }

    /**
     * 更新岗位
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void modifyPosition(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
        Map<String, Object> oldMap = groupMapper.findPosition(methodParam.getSchemaName(), groupModel.getPosition_id());
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            Map<String, Object> position = new HashMap<>();
            position.put("id", groupModel.getPosition_id());
            position.put("position_name", groupModel.getPosition_name());
            position.put("group_id", groupModel.getGroup_id());
            position.put("remark", groupModel.getRemark());
            groupMapper.updatePosition(methodParam.getSchemaName(), position);
            JSONArray log = LangUtil.compareMap(position, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, groupModel.getGroup_id().toString(), LangUtil.doSetLogArray("编辑了岗位", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_BA_M, log.toString()}));//todo 多语言岗位缺失
        }
    }

    /**
     * 删除岗位
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void cutPosition(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
        PositionResult positionResult = groupMapper.findPositionById(methodParam.getSchemaName(), groupModel.getPosition_id());
        if (RegexUtil.optNotNull(positionResult).isPresent()) {
            //删除岗位信息
            groupMapper.deletePosition(methodParam.getSchemaName(), groupModel.getPosition_id());
            //删除岗位角色关联表
            groupMapper.deletePositionRole(methodParam.getSchemaName(), groupModel.getPosition_id());
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, groupModel.getPosition_id().toString(), LangUtil.doSetLogArray("删除了岗位", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_BA_M, positionResult.getPosition_name()}));//todo 多语言岗位缺失
        }
    }

    /**
     * 获取岗位详情
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public PositionResult getPositionInfo(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
        //岗位详情
        return groupMapper.findPositionById(methodParam.getSchemaName(), groupModel.getPosition_id());
    }

    /**
     * 根据部门查询出他的所有父类部门
     *
     * @param groupResult 入参
     * @return 所有父类部门
     */
    private List<GroupResult> getParentByGroup(MethodParam methodParam, GroupResult groupResult) {
        List<GroupResult> groupResults = new ArrayList<>();
        List<GroupResult> childrens =
                groupMapper.findParentGroupList(methodParam.getSchemaName(), groupResult.getParent_id());
        if (RegexUtil.optIsPresentList(childrens)) {
            groupResults.addAll(childrens);
            childrens.forEach(children ->
                    groupResults.addAll(getParentByGroup(methodParam, children)));
        }
        return groupResults;
    }

    /**
     * 新增岗位角色
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public void newPositionRole(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
//        //2新增该岗位角色关联信息
        List<Map<String, Object>> list = new ArrayList<>();
        for (String roleId : groupModel.getRoleIds()
        ) {
            Map<String, Object> map = new HashMap<>();
            map.put("role_id", roleId);
            map.put("position_id", groupModel.getPosition_id());
            list.add(map);
        }
        List<String> roleNames = roleService.getRoleNamesByRoleIds(methodParam.getSchemaName(), groupModel.getRoleIds());
        commonUtilService.batchInsertDataList(methodParam.getSchemaName(), list, "_sc_position_role", "role_id,position_id");
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, groupModel.getPosition_id().toString(), LangUtil.doSetLogArray("添加了角色", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AM_Y, roleNames.toString()}));//添加了角色
    }

    /**
     * 删除岗位角色
     */
    @Override
    public void cutPositionRole(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
        RegexUtil.optStrOrExpNotNull(groupModel.getRoleId(), LangConstant.TITLE_AM_Y);
        Map<String, Object> roleMap = roleService.getByRoleId(methodParam.getSchemaName(), groupModel.getRoleId());
        if (RegexUtil.optNotNull(roleMap).isPresent()) {
            groupMapper.deletePositionRoleById(methodParam.getSchemaName(), groupModel.getPosition_id(), groupModel.getRoleId());
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7007, groupModel.getPosition_id().toString(), LangUtil.doSetLogArray("删除了角色", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AM_Y, roleMap.get("name").toString()}));//删除了角色
        }
    }

    /**
     * 查询岗位角列表
     *
     * @param methodParam 入参
     * @param groupModel  入参
     */
    @Override
    public List<Map<String, Object>> getPositionRoleList(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
        //查询岗位本身的角色
        return groupMapper.findRoleIdsByPositionId(methodParam.getSchemaName(), groupModel.getPosition_id());
    }

    /**
     * 新增岗位角色的可选角色列表
     *
     * @param methodParam 入参
     * @param groupModel  入参
     * @return 可选角色列表
     */
    @Override
    public List<Map<String, Object>> getAddPositionRoleList(MethodParam methodParam, GroupModel groupModel) {
        RegexUtil.optIntegerOrExpParam(groupModel.getPosition_id(), LangConstant.TITLE_PF);
        //查询不在该岗位已拥有的角色
        return groupMapper.findRolesNotInPosition(methodParam.getSchemaName(), groupModel.getPosition_id(), groupModel.getKeywordSearch());
    }

    /**
     * 根据用户id获取岗位列表
     *
     * @param methodParam 入参
     * @param userId      入参
     * @return 岗位列表
     */
    @Override
    public List<PositionResult> getByUserId(MethodParam methodParam, String userId) {
        return groupMapper.findByUserId(methodParam.getSchemaName(), userId);
    }

    /**
     * 根据岗位id获取他的部门级联字符串
     *
     * @param methodParam 入参
     * @param position    入参
     * @return 部门名称排列字符串
     */
    @Override
    public StringBuilder getGrouNameString(MethodParam methodParam, PositionResult position) {
        StringBuilder name = new StringBuilder();
        GroupResult group = groupMapper.findGroupByPositionId(methodParam.getSchemaName(), position.getId());
        name.append(group.getGroup_name());
        name.append("/");
        List<GroupResult> parents = getParentByGroup(methodParam, group);
        for (GroupResult groupResult : parents
        ) {
            name.insert(0, groupResult.getGroup_name() + "/");
        }
        return name;
    }

    @Override
    public Map<String, Map<String, Object>> getGroupListMap(String schemaName) {
        return groupMapper.findGroupListMap( schemaName);
    }

    @Override
    public Map<String, Map<String, Object>> getPositionListMap(String schemaName) {
        return groupMapper.findPositionListMap( schemaName);
    }
}
