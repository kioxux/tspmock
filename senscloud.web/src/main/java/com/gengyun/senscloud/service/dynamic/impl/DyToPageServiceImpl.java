package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.DyToPageService;
import org.springframework.stereotype.Service;

/**
 * 数据处理接口（动态进页面前）
 * User: sps
 * Date: 2020/06/12
 * Time: 上午09:20
 */
@Service
public class DyToPageServiceImpl implements DyToPageService {
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    DyToPageTypeService dyToPageTypeService;
//    @Autowired
//    WorkflowService workflowService;
//
//    /**
//     * 整合小程序回显字段及数据（v4.0.1）
//     *
//     * @param schemaName
//     * @param result
//     * @param template
//     * @param oldData
//     * @param isAssetScan
//     * @param isDistribute
//     * @param subWorkCode
//     */
//    public void getPageInfoV401(String schemaName, JSONObject result,
//                                MetadataWork template, Map<String, Object> oldData, Boolean isAssetScan,
//                                Boolean isDistribute, String subWorkCode) {
//        if (null != template) {
//            String bodyProperty = template.getBodyProperty();
//            if (null != bodyProperty && !"".equals(bodyProperty)) {
//                Map<String, String> strInfo = new HashMap<>();
//                HttpServletRequest request = HttpRequestUtils.getRequest();
//                strInfo.put("deviceDomain", request.getParameter("deviceDomain"));
//                strInfo.put("strNone", selectOptionService.getLanguageInfo(LangConstant.NONE_A));
//                strInfo.put("strNoneList", selectOptionService.getLanguageInfo(LangConstant.NOMORE_DATA));
//                strInfo.put("strPleaseEnter", selectOptionService.getLanguageInfo(LangConstant.PLEASE_ENTER));
//                strInfo.put("strTimeMin", selectOptionService.getLanguageInfo(LangConstant.MIN_A));
//                strInfo.put("schemaName", schemaName);
//                strInfo.put("subWorkCode", subWorkCode);
//                Map<String, Object> dbInfo = commonUtilService.doGetDbBaseInfo();
//                strInfo.put("nowUserAccount", (String) dbInfo.get("account"));
//                strInfo.put("companyId", (String) dbInfo.get("account"));
//
//                Map<String, Object> allMap = new HashMap<>();
//                allMap.put("companyId", dbInfo.get("companyId"));
//                allMap.put("oldData", oldData);
//                allMap.put("isAssetScan", isAssetScan);
//                allMap.put("isDistribute", isDistribute);
//                allMap.put("relation_type", template.getRelationType());
//                allMap.put("work_type_id", template.getWorkTypeId());
//                allMap.put("task_id", request.getParameter("task_id"));
//                allMap.put("pageDtlValue", request.getParameter("pageDtlValue"));
//                long nowSysTime = System.currentTimeMillis();
//                Timestamp now = new Timestamp(nowSysTime);
//                allMap.put("nowSysTime", nowSysTime);
//                allMap.put("nowTime", now);
//                strInfo.put("nowTimeStr", new SimpleDateFormat(Constants.DATE_FMT_SS).format(now));
//                Map<String, Object> extInfo = selectOptionService.searchLoginUserInfo(); // 扩展信息
//                allMap.putAll(extInfo);
//                Map<String, Object> assigneeInfo = null;
//                try {
//                    List<CustomTaskEntityImpl> taskList = workflowService.getTasks(schemaName, null, null, Arrays.asList(subWorkCode), null, null, true, null, null, null, null, null);
//                    String assignee = null;
//                    try {
//                        assignee = taskList.get(0).getAssignee();
//                    } catch (Exception assigneeExp) {
//
//                    }
//                    String receiveAccount = (String) oldData.get("receive_account");
//                    assignee = RegexUtil.optNotBlankStrOrExp(RegexUtil.optStrAndValOrBlank(assignee, receiveAccount), "");
//                    assigneeInfo = selectOptionService.getOptionByCode(schemaName, "user_detail", assignee);
//                } catch (Exception assigneeExp) {
//
//                }
//                allMap.put("assigneeInfo", RegexUtil.optMapOrNew(assigneeInfo));
//                DataUtil.initData(allMap);
//                DataUtil.doCatchOldData(allMap, strInfo);
//                // 新模板数据处理
//                JSONArray arrays = JSONArray.fromObject(bodyProperty);
//                for (Object object : arrays) {
//                    JSONObject data = JSONObject.fromObject(object);
//                    data.put("fieldName", selectOptionService.getLanguageInfo(data.get("fieldName").toString()));
//                    DataUtil.doSetDefaultValue(result, data, strInfo, allMap);
//                    if (RelationUtil.doGetFieldViewRelationInfo(result, data, strInfo, allMap)) {
//                        continue;
//                    }
//                    dyToPageTypeService.doSetResultByType(result, data, strInfo, allMap);
//                }
//                this.doSetSectionInfo(allMap, strInfo);
//                DataUtil.setTailInfo(result, allMap);
//            }
//        }
//    }
//
//    /**
//     * 设置模块信息
//     *
//     * @param allMap
//     * @param strInfo
//     */
//    private void doSetSectionInfo(Map<String, Object> allMap, Map<String, String> strInfo) {
//        Map<String, Map<String, Object>> sectionInfo = (Map<String, Map<String, Object>>) allMap.get("sectionInfo");
//        if (null != sectionInfo && sectionInfo.size() > 0) {
//            List<Map<String, Object>> sectionList = (List<Map<String, Object>>) allMap.get("sectionList");
//            Map<String, List<String>> sectionContentList = (Map<String, List<String>>) allMap.get("sectionContentList");
//            Set<String> sectionKeys = (Set<String>) allMap.get("sectionKeys");
//            String schemaName = strInfo.get("schemaName");
//            for (String key : sectionKeys) {
////                    // 分配页面只在可分配的页面出现，分配判断需要前端给予
////                    if ("Distribute".equals(key) && !isDistribute) {
////                        continue;
////                    }
//                Map<String, Object> tmpMap = sectionInfo.get(key);
//                if (!tmpMap.containsKey("groupType") || RegexUtil.isNull((String) tmpMap.get("groupType"))) {
//                    tmpMap.put("title", selectOptionService.getOptionNameByCode(schemaName, "dy_section_type", key, null));
//                }
//                tmpMap.put("content", sectionContentList.get(key));
//                RelationUtil.setDataUrl(tmpMap, tmpMap, allMap);
//                sectionList.add(tmpMap);
//            }
//            Map<String, Object> pageView = (Map<String, Object>) allMap.get("pageView");
//            pageView.put("sectionInfo", sectionList);
//        }
//    }
}
