package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetIotSettingService;
import org.springframework.stereotype.Service;

/**
 * 设备物联信息
 */
@Service
public class AssetIotSettingServiceImpl implements AssetIotSettingService {
//    @Autowired
//    AssetIotSettingMapper assetIotSettingMapper;
//
//    /**
//     * 获取设备物联信息
//     *
//     * @param schemaName
//     * @param assetId
//     * @return
//     */
//    public Map<String, Object> selectAssetIotSettingByAssetId(String schemaName, String assetId) {
//        return assetIotSettingMapper.selectAisByAssetId(schemaName, assetId);
//    }
//
//    /**
//     * 新增设备物联信息
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    public int addAssetIotSetting(String schemaName, Map<String, Object> paramMap) {
//        return assetIotSettingMapper.insertAssetIotSetting(schemaName, paramMap);
//    }
//
//    /**
//     * 更新设备物联信息
//     *
//     * @param schemaName
//     * @param assetId
//     * @param paramMap
//     * @return
//     */
//    public int updateAssetIotSetting(String schemaName, String assetId, Map<String, Object> paramMap) {
//        Map<String, Object> info = assetIotSettingMapper.selectAisByAssetId(schemaName, assetId);
//        if (null != info && info.size() > 0) {
//            Set<String> keys = paramMap.keySet();
//            for (String key : keys) {
//                if (null != paramMap.get(key) && "".equals(paramMap.get(key))) {
//                    info.put(key, null);
//                } else {
//                    info.put(key, paramMap.get(key));
//                }
//            }
//            return assetIotSettingMapper.updateAisByAssetId(schemaName, info);
//        } else {
//            Set<String> keys = paramMap.keySet();
//            for (String key : keys) {
//                if (null != paramMap.get(key) && "".equals(paramMap.get(key))) {
//                    paramMap.put(key, null);
//                } else {
//                    paramMap.put(key, paramMap.get(key));
//                }
//            }
//            return assetIotSettingMapper.insertAssetIotSetting(schemaName, paramMap);
//        }
//    }
//
//    /**
//     * 根据组织id，查询设备物联数量
//     *
//     * @param schemaName
//     * @param iotStatus
//     * @param facilityId
//     * @return
//     */
//    @Override
//    public int countAssetByFacilityId(String schemaName, Long facilityId, int iotStatus) {
//        return assetIotSettingMapper.countAssetByFacilityId(schemaName, facilityId, iotStatus);
//    }
}
