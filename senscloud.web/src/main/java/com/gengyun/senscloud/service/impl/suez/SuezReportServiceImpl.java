package com.gengyun.senscloud.service.impl.suez;

import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.config.FileConfiguration;
import com.gengyun.senscloud.mapper.FacilitiesMapper;
import com.gengyun.senscloud.mapper.SncDataImportMapper;
import com.gengyun.senscloud.mapper.SuezReportMapper;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.model.FilesData;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.service.CloudDataService;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.suez.SuezReportService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.*;
import com.gengyun.senscloud.util.module.SuezReportUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.RandomUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SuezReportServiceImpl implements SuezReportService {
    private Logger logger = LoggerFactory.getLogger(SuezReportServiceImpl.class);

    @Value("${senscloud.file_upload}")
    private String file_upload_dir;
    @Resource
    SuezReportMapper suezReportMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    FacilitiesMapper facilitiesMapper;
    @Resource
    LogsService logService;
    @Resource
    SncDataImportMapper sncDataImportMapper;
    @Resource
    CloudDataService cloudDataService;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    FileConfiguration fileConfiguration;
    @Resource
    FilesService filesService;
    @Resource
    SelectOptionService selectOptionService;

    @Override
    public Long newSuezReport(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_NAME_E}); // 报告名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("report_name"), LangConstant.TITLE_NAME_E); // 报告名称不能为空
        String begin_time = RegexUtil.optStrOrExpNotNull(data.get("begin_time"), LangConstant.TITLE_AK_H);// 开时时间不能为空
        String end_time = RegexUtil.optStrOrExpNotNull(data.get("end_time"), LangConstant.TITLE_AL_K);// 结束时间不能为空
        JSONObject property = new JSONObject();
        property.put("begin_time", begin_time);
        property.put("end_time", end_time);
        RegexUtil.optNotBlankStrOpt(data.get("work_type_ids")).ifPresent(s -> {
            property.put("work_type_id", s.split(","));
            property.put("work_type_ids", s);
        });
        RegexUtil.optNotBlankStrOpt(data.get("position_code")).ifPresent(s -> {
            property.put("position_code", s.split(","));
            property.put("position_codes", s);
        });
        RegexUtil.optNotBlankStrOpt(data.get("searchAsset")).ifPresent(s -> {
            property.put("select_asset", s.split(","));
            property.put("select_assets", s);
        });
        data.put("create_time", SenscloudUtil.getNowTime());
        data.put("schema_name", methodParam.getSchemaName());
        data.put("create_user_id", methodParam.getUserId());
        data.put("property", property.toString());
        suezReportMapper.insertReport(data);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_5002, String.valueOf(data.get("id")), LangUtil.doSetLogArray("日常报告新增", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.BTN_NEW_A}));
        return (Long) data.get("id");
    }

    @Override
    public Long newSuezReportNT(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_ABAA_I}); // 报告不能为空
        RegexUtil.optStrOrExpNotNull(data.get("report_name"), LangConstant.TITLE_NAME_E); // 报告名称不能为空
        String begin_time = RegexUtil.optStrOrExpNotNull(data.get("begin_time"), LangConstant.TITLE_AK_H);// 开时时间不能为空
        String end_time = RegexUtil.optStrOrExpNotNull(data.get("end_time"), LangConstant.TITLE_AL_K);// 结束时间不能为空
        String position_code = RegexUtil.optStrOrVal(data.get("position_code"), "");//
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String bom_type_ids = RegexUtil.optStrOrVal(data.get("bom_type_ids"), "");
        String stock_ids = RegexUtil.optStrOrVal(data.get("stock_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        JSONObject property = new JSONObject();
        property.put("begin_time", begin_time);
        property.put("end_time", end_time);
        // int[] array = Arrays.asList(position_code.split(",")).stream().mapToInt(Integer::parseInt).toArray();
        if (RegexUtil.optIsPresentStr(work_type_ids)) {
            int[] work_type = Arrays.asList(work_type_ids.split(",")).stream().mapToInt(Integer::parseInt).toArray();
            property.put("work_type_id", work_type);
        } else {
            property.put("work_type_id", new int[]{});
        }
        if (RegexUtil.optIsPresentStr(position_code)) {
            int[] position = Arrays.asList(position_code.split(",")).stream().mapToInt(Integer::parseInt).toArray();
            property.put("facility_id", position);
        } else {
            property.put("facility_id", new int[]{});
        }
        if (RegexUtil.optIsPresentStr(bom_type_ids)) {
            int[] bomType = Arrays.asList(bom_type_ids.split(",")).stream().mapToInt(Integer::parseInt).toArray();
            property.put("bom_type_ids", bomType);
        } else {
            property.put("bom_type_ids", new int[]{});
        }
        if (RegexUtil.optIsPresentStr(stock_ids)) {
            int[] stocks = Arrays.asList(stock_ids.split(",")).stream().mapToInt(Integer::parseInt).toArray();
            property.put("stock_ids", stocks);
        } else {
            property.put("stock_ids", new int[]{});
        }
        if (RegexUtil.optIsPresentStr(asset_type_ids)) {
            int[] assetTypes = Arrays.asList(asset_type_ids.split(",")).stream().mapToInt(Integer::parseInt).toArray();
            property.put("asset_type_ids", assetTypes);
        } else {
            property.put("asset_type_ids", new int[]{});
        }
        if (RegexUtil.optIsPresentStr(asset_position_codes)) {
            String[] positions = asset_position_codes.split(",");
            property.put("positions", positions);
        } else {
            property.put("positions", new String[]{});
        }
        data.put("create_time", SenscloudUtil.getNowTime());
        data.put("schema_name", methodParam.getSchemaName());
        data.put("create_user_id", methodParam.getUserId());
        data.put("property", property.toString());
        suezReportMapper.insertReportNT(data);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_5002, String.valueOf(data.get("id")), LangUtil.doSetLogArray("日常报告新增", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.BTN_NEW_A}));
        return (Long) data.get("id");
    }

    @Override
    public Map<String, Object> getSuezReportListNT(MethodParam methodParam, Map<String, Object> pm) {
        Map<String, Object> result = new HashMap<>();
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String whereString = getWhereString(methodParam, pm);
        //查询该条件下的总记录数
        int total = suezReportMapper.findSuezReportListCountNT(methodParam.getSchemaName(), whereString);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        List<Map<String, Object>> rows = suezReportMapper.findSuezReportListNT(methodParam.getSchemaName(), whereString, pagination);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    @Override
    public void cutSuezReportNT(MethodParam methodParam) {
        String idStr = methodParam.getIds();
        String[] idArr = idStr.split(",");
        int len = idArr.length;
        if (len > 0) {
            suezReportMapper.deleteSuezReportNT(methodParam.getSchemaName(), idArr);
            String remark = len > 1 ? LangUtil.doSetLogArrayNoParam("批量删除了日常报告！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了日常报告！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ABAA_I});
            for (String id : idArr) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_5002, id, remark);
            }
        }
    }


    @Override
    public void cutSuezReport(MethodParam methodParam) {
        String idStr = methodParam.getIds();
        String[] idArr = idStr.split(",");
        int len = idArr.length;
        if (len > 0) {
            suezReportMapper.deleteSuezReport(methodParam.getSchemaName(), idArr);
            String remark = len > 1 ? LangUtil.doSetLogArrayNoParam("批量删除了日常报告！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了日常报告！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ABAA_I});
            for (String id : idArr) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_5002, id, remark);
            }
        }
    }

    /**
     * 生成日常报告文件
     *
     * @param methodParam 系统参数
     * @param param       页面参数
     */
    @Override
    public void getImportSuezReport(MethodParam methodParam, SearchParam param) {
        String schema_name = methodParam.getSchemaName();
        Map<String, Object> data = RegexUtil.optMapOrExpNullInfo(suezReportMapper.findReportById(schema_name, Integer.valueOf(param.getId())), LangConstant.TITLE_ABAA_I); // 报告不存在
        String fileIds = RegexUtil.optStrOrBlank(data.get("file_ids"));
        if (!"-9999".equals(fileIds)) {
            suezReportMapper.updateSuezReportFileIdsById(schema_name, "-9999", param.getId());
            StringBuffer whereString = new StringBuffer(" ap.status > -1000 ");
            RegexUtil.optNotBlankStrOpt(data.get("begin_time")).ifPresent(s -> whereString.append(" and w.create_time >= to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            RegexUtil.optNotBlankStrOpt(data.get("end_time")).ifPresent(s -> whereString.append(" and w.create_time < to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            RegexUtil.optNotBlankStrOpt(data.get("position_codes")).ifPresent(s -> whereString.append(" and ap.position_code in (").append(DataChangeUtil.joinByStr(s)).append(") "));
            RegexUtil.optNotBlankStrOpt(data.get("select_assets")).ifPresent(s -> whereString.append(" and a.id in (").append(DataChangeUtil.joinByStr(s)).append(") "));
            RegexUtil.optNotBlankStrOpt(data.get("work_type_ids")).ifPresent(s -> whereString.append(" and wt.id in (").append(s).append(") "));
            RegexUtil.optNotBlankStrOpt(data.get("begin_time")).ifPresent(s -> whereString.append(" and wp.operate_time >= to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            RegexUtil.optNotBlankStrOpt(data.get("end_time")).ifPresent(s -> whereString.append(" and wp.operate_time < to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            String condition = whereString.toString();
            //本月完成但创建时间不在选择时间范围
            StringBuffer whereStringFinished = new StringBuffer(" ap.status > -1000 ");
            RegexUtil.optNotBlankStrOpt(data.get("begin_time")).ifPresent(s -> whereStringFinished.append(" and w.create_time < to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            RegexUtil.optNotBlankStrOpt(data.get("position_codes")).ifPresent(s -> whereStringFinished.append(" and ap.position_code in (").append(DataChangeUtil.joinByStr(s)).append(") "));
            RegexUtil.optNotBlankStrOpt(data.get("select_assets")).ifPresent(s -> whereStringFinished.append(" and a.id in (").append(DataChangeUtil.joinByStr(s)).append(") "));
            RegexUtil.optNotBlankStrOpt(data.get("work_type_ids")).ifPresent(s -> whereStringFinished.append(" and wt.id in (").append(s).append(") "));
            RegexUtil.optNotBlankStrOpt(data.get("begin_time")).ifPresent(s -> whereStringFinished.append(" and wp.operate_time >= to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            RegexUtil.optNotBlankStrOpt(data.get("end_time")).ifPresent(s -> whereStringFinished.append(" and wp.operate_time < to_date('").append(s).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
            String finishedCondition = whereStringFinished.toString().concat(" and w.status >= ").concat(String.valueOf(StatusConstant.COMPLETED)).concat(" and w.status not in (").concat(String.valueOf(StatusConstant.CANCEL)).concat(")");
            String doCondition = condition.concat(" and wd.status = ").concat(String.valueOf(StatusConstant.COMPLETED));
            RegexUtil.trueExp(suezReportMapper.countReportOnMaintenanceMatters(schema_name, condition) > 20000, LangConstant.MSG_J);
            List<Map<String, Object>> firstList = suezReportMapper.findReportOnMaintenanceMatters(schema_name, doCondition);
            String unDoCondition = condition.concat(" and wd.status < ").concat(String.valueOf(StatusConstant.COMPLETED));
            List<Map<String, Object>> unDoList = suezReportMapper.findReportOnMaintenanceMatters(schema_name, unDoCondition);
            List<Map<String, Object>> finishedList = suezReportMapper.findReportOnMaintenanceMatters(schema_name, finishedCondition);

            List<Map<String, Object>> twoList = new ArrayList<>();
            List<Map<String, Object>> threeList = new ArrayList<>(); // 备件
            List<Map<String, Object>> fourList = new ArrayList<>(); // 材料
            List<Map<String, Object>> fiveList = new ArrayList<>(); // 其他
            List<Map<String, Object>> taskList = new ArrayList<>(); // 任务项
            RegexUtil.optNotNullList(firstList).ifPresent(l -> this.doSetSuezSubInfo(methodParam, l, twoList, threeList, fourList, fiveList, taskList));
            RegexUtil.optNotNullList(finishedList).ifPresent(l -> this.doSetSuezSubInfo(methodParam, l, twoList, threeList, fourList, fiveList, taskList));
            Workbook workbook = new SXSSFWorkbook(1000);
            try {
                SuezReportUtil.createSrExcel(workbook, firstList, twoList, threeList, fourList, fiveList, unDoList, taskList, finishedList);
                if (workbook.getNumberOfSheets() > 0) {
                    String sep = System.getProperty("file.separator");
                    String type = RegexUtil.optStrOrVal(data.get("report_name"), "日常报告下载");
                    String originalFileName = type + "_" + new SimpleDateFormat(Constants.DATE_FMT_FILE).format(new Date()) + ".xlsx";
                    String fileName = SenscloudUtil.generateUUIDStr() + ".xlsx";
                    String filePath = file_upload_dir + sep + schema_name + sep + Constants.FILE_EXPORT_DIR + sep + type + sep + fileName;
                    FileUtil.doExportExcel(filePath, workbook);
                    File file = new File(filePath);
                    FilesData attachment = new FilesData();
                    attachment.setFile_size((double) file.length() / 1024);
                    attachment.setFile_name(fileName);
                    attachment.setFile_original_name(originalFileName);
                    attachment.setFile_url(filePath);
                    attachment.setFile_category_id(2);
                    attachment.setFile_width(0);
                    attachment.setFile_height(0);
                    attachment.setCreate_user_id(methodParam.getUserId());
                    int fileId = filesService.add(schema_name, attachment);
                    suezReportMapper.updateSuezReportFileIdsById(schema_name, String.valueOf(fileId), param.getId());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    workbook.close();
                } catch (Exception wbExp) {
                    wbExp.printStackTrace();
                }
            }
        }

    }

    /**
     * 日常报告子列表信息处理
     *
     * @param methodParam 系统参数
     * @param list        主数据
     * @param twoList     数据
     * @param threeList   数据
     * @param fourList    数据
     * @param fiveList    数据
     * @param taskList    数据
     */
    private void doSetSuezSubInfo(MethodParam methodParam, List<Map<String, Object>> list, List<Map<String, Object>> twoList,
                                  List<Map<String, Object>> threeList, List<Map<String, Object>> fourList, List<Map<String, Object>> fiveList, List<Map<String, Object>> taskList) {
        Map<String, Object> rapMap = RegexUtil.optMapOrNew(DataChangeUtil.scdListToMap(selectOptionService.getSelectOptionListWithCache(methodParam, "root_asset_position_name"), "value", "text"));
        Map<String, Object> sapMap = RegexUtil.optMapOrNew(DataChangeUtil.scdListToMap(selectOptionService.getSelectOptionListWithCache(methodParam, "second_asset_position_name"), "value", "text"));
        Map<String, Map<String, Object>> dicMap = new HashMap<>();
        dicMap.put("fee_attribute", RegexUtil.optMapOrNew(selectOptionService.getStaticSelectForInfo(methodParam, "fee_attribute")));
        dicMap.put("fee_belong", RegexUtil.optMapOrNew(selectOptionService.getStaticSelectForInfo(methodParam, "team_type")));
        String schema_name = methodParam.getSchemaName();
        Map<String, Object> wcMap = DataChangeUtil.scdListToMap(suezReportMapper.findWorkColumns(schema_name), "field_code", "field_form_code");
        Map<String, Map<String, Object>> userMap = userMapper.queryUsers(schema_name);
        Map<String, Object> taskTemplateMap = DataChangeUtil.scdListToMap(suezReportMapper.findTaskTemplate(schema_name), "task_template_code", "task_template_name");
        String companyName = methodParam.getCompanyName();
        for (Map<String, Object> orderInfo : list) {
            RegexUtil.optNotBlankStrOpt(orderInfo.get("position_code")).map(rapMap::get).filter(RegexUtil::optIsPresentStr).ifPresent(s -> orderInfo.put("company_name", s));
            orderInfo.put("root_position", RegexUtil.optNotBlankStrOpt(orderInfo.get("position_code")).map(sapMap::get).filter(RegexUtil::optIsPresentStr).orElseGet(() -> orderInfo.get("company_name")));
            if ("sc_com_169".equals(schema_name)) {
                orderInfo.put("company_name", companyName);
            }
            SuezReportUtil.doCheckWorkFee(orderInfo, wcMap, dicMap, threeList, fourList, fiveList);
            SuezReportUtil.doCheckUserHour(orderInfo, wcMap, dicMap, userMap, twoList);
            RegexUtil.optNotNullJa(orderInfo.get("task_item")).ifPresent(j -> {
                for (int i = 0; i < j.size(); i++) {
                    JSONObject jsonObject = j.getJSONObject(i);
                    Map<String, Object> report = new HashMap<>();
                    report.put("report_day", orderInfo.get("report_day"));
                    report.put("work_code", orderInfo.get("work_code"));
                    String result_type = RegexUtil.optStrOrBlank(jsonObject.get("result_type"));
                    report.put("result_type", result_type);
                    report.put("task_template_name", RegexUtil.optNotBlankStrOpt(jsonObject.get("task_template_code")).map(taskTemplateMap::get).filter(RegexUtil::optIsPresentStr).orElse("默认任务模板"));
                    report.put("group_name", RegexUtil.optStrOrVal(jsonObject.get("group_name"), "默认分组"));
                    report.put("task_item_name", jsonObject.get("task_item_name"));
                    String value = RegexUtil.optStrOrBlank(jsonObject.get("value"));
                    if ("3".equals(result_type)) {
                        try {
                            JSONArray imgArray = JSONArray.fromObject(value);
                            if (null != imgArray && imgArray.size() > 0) {
                                List<String> imgList = new ArrayList<>();
                                for (int x = 0; x < imgArray.size(); x++) {
                                    JSONObject imgJson = imgArray.getJSONObject(x);
                                    RegexUtil.optNotNullIntegerOpt(imgJson.get("id"), LangConstant.TITLE_ACJ).map(d -> sncDataImportMapper.findFileInfoById(methodParam.getSchemaName(), d))
                                            .ifPresent(m -> RegexUtil.optNotBlankStrOpt(m.get("file_url")).ifPresent(imgList::add));
                                }
                                report.put("value", imgList); // 照片
                            }
                        } catch (Exception e) {
                            logger.error(ErrorConstant.CODE_SUEZ_REPORT_102 + RegexUtil.optStrOrBlank(orderInfo.get("work_code")), e);
                        }
                    } else if ("1".equals(result_type)) {
                        report.put("value", "normal".equals(value) ? "正常" : ("abnormal".equals(value) ? "异常" : ""));
                    } else {
                        report.put("value", value);
                    }
                    taskList.add(report);
                }
            });
        }
    }

    /**
     * 导出报告入口
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public String getImportSuezReportNew(MethodParam methodParam, SearchParam param) {
        Map<String, Object> data = this.getSuezReportByIdNT(methodParam, param.getId());
        String report_name = (String) data.get("report_name");
        // 设备维修
        if ("sbwx".equals(report_name)) {
            return getAssetMaintainReport(methodParam, param, data);
            // 大中修
        } else if ("dzx".equals(report_name)) {
            return getBigRepairReport(methodParam, param, data);
            // 技术规格
        } else if ("jsgg".equals(report_name)) {
            return getTechStandersReport(methodParam, param, data);
            // 设备台账
        } else if ("sbtz".equals(report_name)) {
            return getAssetTotalReport(methodParam, param, data);
            // 项修记录
        } else if ("xxjl".equals(report_name)) {
            return getRepairRecordReport(methodParam, param, data);
            // 二级保养记录
        } else if ("ejby".equals(report_name)) {
            return getSecondMaintainReport(methodParam, param, data);
            // 调拨报废记录
        } else if ("dbbf".equals(report_name)) {
            return getAssetWorkReport(methodParam, param, data);
            // 备件月报
        } else if ("bjyb".equals(report_name)) {
            return getBomMonthReport(methodParam, param, data);
            // 备件年报
        } else if ("bjnb".equals(report_name)) {
            return getBomYearReport(methodParam, param, data);
            // 备件记录
        } else if ("bjjl".equals(report_name)) {
            return getBomRecordReport(methodParam, param, data);
        }
        return null;
    }

    @Override
    public ModelAndView downLoadSuezReport(MethodParam methodParam, SearchParam param) {
        RegexUtil.optNotNullMap(this.getSuezReportByIdNT(methodParam, param.getId())).map(m -> m.get("report_name")).filter(RegexUtil::optIsPresentStr).map(String::valueOf).ifPresent(n -> {
            String ext = Constants.XLS_REPORT_TYPES.contains(n) ? ".xls" : ".zip";
            String filePath = getFilePath(methodParam.getSchemaName());
            String fileName = n + "_" + param.getId() + ext;
            String out = filePath + System.getProperty("file.separator") + fileName;
            DownloadUtil.download(out, fileName, false);
        });
        return null;
    }

    /**
     * 判断报告文件是否已存在
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public boolean SuezReportExistNT(MethodParam methodParam, SearchParam param) {
        return RegexUtil.optNotNullMap(this.getSuezReportByIdNT(methodParam, param.getId())).map(m -> m.get("report_name")).filter(RegexUtil::optIsPresentStr).map(String::valueOf).map(n -> {
            String ext = Constants.XLS_REPORT_TYPES.contains(n) ? ".xls" : ".zip";
            String filePath = getFilePath(methodParam.getSchemaName());
            String fileName = n + "_" + param.getId() + ext;
            String out = filePath + System.getProperty("file.separator") + fileName;
            File file = new File(out);
            return file.exists();
        }).orElse(false);
    }


    /**
     * 备品备件年报
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getBomYearReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String bom_type_ids = RegexUtil.optStrOrVal(data.get("bom_type_ids"), "");
        String stock_ids = RegexUtil.optStrOrVal(data.get("stock_ids"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> bomTypeIds = RegexUtil.optIsPresentStr(bom_type_ids) ? Arrays.asList(bom_type_ids.split(",")) : Collections.emptyList();
        List<String> stockIds = RegexUtil.optIsPresentStr(stock_ids) ? Arrays.asList(stock_ids.split(",")) : Collections.emptyList();
        List<Map<String, Object>> workList =
                suezReportMapper.findBomReports(schema_name, beginTime, endTime, facilityIds, bomTypeIds, stockIds);

        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("company_name", methodParam.getCompanyName());
        String[] dates = DateUtil.splitDateStr(beginTime);
        obj.put("year", dates[0]);
        List<Map<String, Object>> items = new ArrayList<>();
        int num = 1;
        for (Map<String, Object> map : workList) {
            map.put("num", num++);
            Map<String, Object> subobj = fillBomMap(map, methodParam);
            items.add(subobj);
        }
        obj.put("items", items);
        String file = Constants.REPORT_TEMPLATENAME_BOMYEAR; //模板文件名需要配置
        String fileName = (String) data.get("report_name") + "_" + param.getId() + ".xls";
        String out = basePath + System.getProperty("file.separator") + fileName;
        buildExcel(obj, file, out);
        //DownloadUtil.download(out,fileName,false);
        return null;
    }

    /**
     * 备件月报
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getBomMonthReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String bom_type_ids = RegexUtil.optStrOrVal(data.get("bom_type_ids"), "");
        String stock_ids = RegexUtil.optStrOrVal(data.get("stock_ids"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> bomTypeIds = RegexUtil.optIsPresentStr(bom_type_ids) ? Arrays.asList(bom_type_ids.split(",")) : Collections.emptyList();
        List<String> stockIds = RegexUtil.optIsPresentStr(stock_ids) ? Arrays.asList(stock_ids.split(",")) : Collections.emptyList();

        List<Map<String, Object>> workList =
                suezReportMapper.findBomReports(schema_name, beginTime, endTime, facilityIds, bomTypeIds, stockIds);

        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("company_name", methodParam.getCompanyName());
        String[] dates = DateUtil.splitDateStr(beginTime);
        obj.put("year", dates[0]);
        obj.put("month", dates[1]);
        int num = 1;
        List<Map<String, Object>> items = new ArrayList<>();
        for (Map<String, Object> map : workList) {
            map.put("num", num++);
            Map<String, Object> subobj = fillBomMap(map, methodParam);
            items.add(subobj);
        }
        obj.put("items", items);
        String file = Constants.REPORT_TEMPLATENAME_BOMMONTH; //模板文件名需要配置
        String fileName = (String) data.get("report_name") + "_" + param.getId() + ".xls";
        String out = basePath + System.getProperty("file.separator") + fileName;
        buildExcel(obj, file, out);
        //DownloadUtil.download(out,fileName,false);
        return null;
    }


    /**
     * 备件记录
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getBomRecordReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getTmpBuildPath(schema_name);
        String filePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String bom_type_ids = RegexUtil.optStrOrVal(data.get("bom_type_ids"), "");
        String stock_ids = RegexUtil.optStrOrVal(data.get("stock_ids"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> bomTypeIds = RegexUtil.optIsPresentStr(bom_type_ids) ? Arrays.asList(bom_type_ids.split(",")) : Collections.emptyList();
        List<String> stockIds = RegexUtil.optIsPresentStr(stock_ids) ? Arrays.asList(stock_ids.split(",")) : Collections.emptyList();

        List<Map<String, Object>> assetList =
                suezReportMapper.findBomReports(schema_name, beginTime, endTime, facilityIds, bomTypeIds, stockIds);

        List<String> files = new ArrayList();
        for (Map<String, Object> map : assetList) {
            Map<String, Object> obj = fillBomRecordMap(map, methodParam);
            String file = Constants.REPORT_TEMPLATENAME_BOMRECORD; //模板文件名需要配置
            String out = basePath + System.getProperty("file.separator") + map.get("bom_name") + "_" + RandomUtils.nextLong(10000, 20000) + ".xls";
            files.add(out);
            buildExcel(obj, file, out);

        }
        String fileName = (String) data.get("report_name") + "_" + param.getId();
        createZipFile(filePath, fileName, files);
        FileUtil.deleteDir(basePath);
        return null;
    }


    /**
     * 设备调拨记录
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getAssetWorkReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        int status = StatusConstant.COMPLETED;
        List<Map<String, Object>> workList =
                suezReportMapper.findAssetWorks(schema_name, status, beginTime, endTime, facilityIds, workTypeIds);

        Map<String, Object> obj = new HashMap<String, Object>();
        List<Map<String, Object>> items = new ArrayList<>();
        int num = 1;
        for (Map<String, Object> map : workList) {
            map.put("num", num++);
            Map<String, Object> subobj = fillAssetWorksMap(map, methodParam);
            items.add(subobj);
        }
        obj.put("items", items);
        String file = Constants.REPORT_TEMPLATENAME_ASSETWORKS; //模板文件名需要配置
        String fileName = (String) data.get("report_name") + "_" + param.getId() + ".xls";
        String out = basePath + System.getProperty("file.separator") + fileName;
        buildExcel(obj, file, out);
        //DownloadUtil.download(out,fileName,false);
        return null;
    }


    /**
     * 二级保养记录
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getSecondMaintainReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        List<String> assetTypeIds = RegexUtil.optIsPresentStr(asset_type_ids) ? Arrays.asList(asset_type_ids.split(",")) : Collections.emptyList();
        List<String> assetPostionCodes = RegexUtil.optIsPresentStr(asset_position_codes) ? Arrays.asList(asset_position_codes.split(",")) : Collections.emptyList();
        int status = StatusConstant.COMPLETED;
        List<Map<String, Object>> workList =
                suezReportMapper.findReportWorksNew(schema_name, status, beginTime, endTime, facilityIds, workTypeIds, assetTypeIds, assetPostionCodes);


        Map<String, Object> repairTypes = new HashMap();
        repairTypes.put("data_type", "repair_type1");
        List<Map<String, Object>> repairTypesMap = cloudDataService.getCloudDataListByDataType(methodParam, repairTypes);
        Map<String, Object> obj = new HashMap<String, Object>();
        List<Map<String, Object>> items = new ArrayList<>();
        int num = 1;
        for (Map<String, Object> map : workList) {
            map.put("num", num++);
            Map<String, Object> subobj = fillSecondMaintainMap(map, methodParam, repairTypesMap);
            items.add(subobj);
        }
        obj.put("items", items);
        String file = Constants.REPORT_TEMPLATENAME_SECONDMAITAIN; //模板文件名需要配置
        String fileName = (String) data.get("report_name") + "_" + param.getId() + ".xls";
        String out = basePath + System.getProperty("file.separator") + fileName;
        buildExcel(obj, file, out);
        //DownloadUtil.download(out,fileName,false);
        return null;

    }


    /**
     * 项修记录
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getRepairRecordReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        List<String> assetTypeIds = RegexUtil.optIsPresentStr(asset_type_ids) ? Arrays.asList(asset_type_ids.split(",")) : Collections.emptyList();
        List<String> assetPostionCodes = RegexUtil.optIsPresentStr(asset_position_codes) ? Arrays.asList(asset_position_codes.split(",")) : Collections.emptyList();
        int status = StatusConstant.COMPLETED;
        List<Map<String, Object>> workList =
                suezReportMapper.findReportWorksNew(schema_name, status, beginTime, endTime, facilityIds, workTypeIds, assetTypeIds, assetPostionCodes);

        Map<String, Object> faultReasons = new HashMap();
        faultReasons.put("data_type", "fault_reason");
        List<Map<String, Object>> faultReasonMap = cloudDataService.getCloudDataListByDataType(methodParam, faultReasons);
        Map<String, Object> repairTypes = new HashMap();
        repairTypes.put("data_type", "repair_type1");
        List<Map<String, Object>> repairTypesMap = cloudDataService.getCloudDataListByDataType(methodParam, repairTypes);
        Map<String, Object> obj = new HashMap<String, Object>();
        List<Map<String, Object>> items = new ArrayList<>();
        int num = 1;
        for (Map<String, Object> map : workList) {
            map.put("num", num++);
            Map<String, Object> subobj = fillRepairRcordMap(map, methodParam, faultReasonMap, repairTypesMap);
            items.add(subobj);
        }
        obj.put("items", items);
        String file = Constants.REPORT_TEMPLATENAME_MAITAINRECORD; //模板文件名需要配置
        String fileName = (String) data.get("report_name") + "_" + param.getId() + ".xls";
        String out = basePath + System.getProperty("file.separator") + fileName;
        buildExcel(obj, file, out);
        //DownloadUtil.download(out,fileName,false);
        return null;

    }


    /**
     * 设备台账报告
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getAssetTotalReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        List<String> assetTypeIds = RegexUtil.optIsPresentStr(asset_type_ids) ? Arrays.asList(asset_type_ids.split(",")) : Collections.emptyList();
        List<String> assetPostionCodes = treateSpecialChar(RegexUtil.optIsPresentStr(asset_position_codes) ? Arrays.asList(asset_position_codes.split(",")) : Collections.emptyList());
        List<Map<String, Object>> assetList =
                suezReportMapper.findAssetReport(schema_name, facilityIds, beginTime, endTime, assetTypeIds, assetPostionCodes);
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("company_name", methodParam.getCompanyName());
        List<Map<String, Object>> items = new ArrayList<>();
        int num = 1;
        for (Map<String, Object> map : assetList) {
            map.put("num", num++);
            Map<String, Object> subobj = fillAssetTotalMapNew(map, methodParam);
            items.add(subobj);
        }
        obj.put("items", items);
        String file = Constants.REPORT_TEMPLATENAME_ASSETTOTAL; //模板文件名需要配置
        String fileName = (String) data.get("report_name") + "_" + param.getId() + ".xls";
        String out = basePath + System.getProperty("file.separator") + fileName;
        buildExcel(obj, file, out);
        //DownloadUtil.download(out,fileName,false);
        return null;
    }


    /**
     * 技术规格报告
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getTechStandersReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getTmpBuildPath(schema_name);
        String filePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        List<String> assetTypeIds = RegexUtil.optIsPresentStr(asset_type_ids) ? Arrays.asList(asset_type_ids.split(",")) : Collections.emptyList();
        List<String> assetPostionCodes = treateSpecialChar(RegexUtil.optIsPresentStr(asset_position_codes) ? Arrays.asList(asset_position_codes.split(",")) : Collections.emptyList());

        List<Map<String, Object>> assetList =
                suezReportMapper.findAssetReport(schema_name, facilityIds, beginTime, endTime, assetTypeIds, assetPostionCodes);

        List<String> files = new ArrayList();
        for (Map<String, Object> map : assetList) {
            Map<String, Object> obj = fillAssetMapNew(map, methodParam);
            String file = Constants.REPORT_TEMPLATENAME_TECHSTANDERD; //模板文件名需要配置
            String out = basePath + System.getProperty("file.separator") + map.get("asset_code") + "_" + RandomUtils.nextLong(10000, 20000) + ".xls";
            files.add(out);
            buildExcel(obj, file, out);

        }
        String fileName = (String) data.get("report_name") + "_" + param.getId();
        createZipFile(filePath, fileName, files);
        FileUtil.deleteDir(basePath);
        return null;
    }

    /**
     * 处理特殊字符
     *
     * @param list
     * @return
     */
    private List<String> treateSpecialChar(List<String> list) {
        List<String> rlist = new ArrayList();
        for (String str : list) {
            rlist.add(str.replaceAll("\"", ""));
        }
        return rlist;
    }


    /**
     * 大中修报告
     *
     * @param methodParam
     * @param param
     * @param data
     * @return
     */
    private String getBigRepairReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getTmpBuildPath(schema_name);
        String filePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        List<String> assetTypeIds = RegexUtil.optIsPresentStr(asset_type_ids) ? Arrays.asList(asset_type_ids.split(",")) : Collections.emptyList();
        List<String> assetPostionCodes = RegexUtil.optIsPresentStr(asset_position_codes) ? Arrays.asList(asset_position_codes.split(",")) : Collections.emptyList();
        List<Map<String, Object>> reportImgs = RegexUtil.optNotNullListOrNew(selectOptionService.getTargetTableList(methodParam, "report_img", (String) data.get("report_name")));
        String beforeImgXyz = RegexUtil.optStrOrBlank(reportImgs.stream().filter(s -> "before_img".equals(s.get("code"))).findAny().orElseGet(HashMap::new).get("reserve1"));
        String resultImgXyz = RegexUtil.optStrOrBlank(reportImgs.stream().filter(s -> "result_img".equals(s.get("code"))).findAny().orElseGet(HashMap::new).get("reserve1"));
        int status = StatusConstant.COMPLETED;
        List<Map<String, Object>> workList =
                suezReportMapper.findReportWorksNew(schema_name, status, beginTime, endTime, facilityIds, workTypeIds, assetTypeIds, assetPostionCodes);

        List<String> files = new ArrayList();
        for (Map<String, Object> map : workList) {
            Map<String, Object> obj = fillBigRepairMapNew(map, methodParam);
            logger.info("fill Image begin : [{}]", System.currentTimeMillis());
            fillImage(schema_name, beforeImgXyz, (String) obj.get("before_img1"), obj);
            fillImage(schema_name, resultImgXyz, (String) obj.get("result_img1"), obj);
            logger.info("fill Image end : [{}]", System.currentTimeMillis());
            String file = Constants.REPORT_TEMPLATENAME_BIGREPAIR; //模板文件名需要配置
            String out = basePath + System.getProperty("file.separator") + map.get("work_code") + "_" + RandomUtils.nextLong(10000, 20000) + ".xls";
            files.add(out);
            logger.info("buildExcel begin : [{}]", System.currentTimeMillis());
            buildExcel(obj, file, out);
            logger.info("buildExcel end : [{}]", System.currentTimeMillis());

        }
        String fileName = (String) data.get("report_name") + "_" + param.getId();
        createZipFile(filePath, fileName, files);
        FileUtil.deleteDir(basePath);
        return null;
    }


    /**
     * 导出设备维修申请表
     *
     * @param methodParam
     * @param param
     * @return
     */
    private String getAssetMaintainReport(MethodParam methodParam, SearchParam param, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        String endTime = (String) data.get("end_time");
        String beginTime = (String) data.get("begin_time");
        String basePath = getTmpBuildPath(schema_name);
        String filePath = getFilePath(schema_name);
        String facility_ids = RegexUtil.optStrOrVal((String) data.get("facility_ids"), "");
        String work_type_ids = RegexUtil.optStrOrVal(data.get("work_type_ids"), "");
        String asset_type_ids = RegexUtil.optStrOrVal(data.get("asset_type_ids"), "");
        String asset_position_codes = RegexUtil.optStrOrVal(data.get("asset_position_codes"), "");
        List<String> facilityIds = RegexUtil.optIsPresentStr(facility_ids) ? Arrays.asList(facility_ids.split(",")) : Collections.emptyList();
        List<String> workTypeIds = RegexUtil.optIsPresentStr(work_type_ids) ? Arrays.asList(work_type_ids.split(",")) : Collections.emptyList();
        List<String> assetTypeIds = RegexUtil.optIsPresentStr(asset_type_ids) ? Arrays.asList(asset_type_ids.split(",")) : Collections.emptyList();
        List<String> assetPostionCodes = RegexUtil.optIsPresentStr(asset_position_codes) ? Arrays.asList(asset_position_codes.split(",")) : Collections.emptyList();
        int status = StatusConstant.COMPLETED;
        List<Map<String, Object>> workList =
                suezReportMapper.findReportWorksNew(schema_name, status, beginTime, endTime, facilityIds, workTypeIds,
                        assetTypeIds, assetPostionCodes);

        Map<String, Object> solutions = new HashMap();
        solutions.put("data_type", "solutions");
        List<Map<String, Object>> solutionsMap = cloudDataService.getCloudDataListByDataType(methodParam, solutions);
        Map<String, Object> repairTypes = new HashMap();
        repairTypes.put("data_type", "repair_type1");
        List<Map<String, Object>> repairTypesMap = cloudDataService.getCloudDataListByDataType(methodParam, repairTypes);
        List<Map<String, Object>> reportImgs = RegexUtil.optNotNullListOrNew(selectOptionService.getTargetTableList(methodParam, "report_img", (String) data.get("report_name")));
        String beforeImgXyz = RegexUtil.optStrOrBlank(reportImgs.stream().filter(s -> "before_img".equals(s.get("code"))).findAny().orElseGet(HashMap::new).get("reserve1"));
        String resultImgXyz = RegexUtil.optStrOrBlank(reportImgs.stream().filter(s -> "result_img".equals(s.get("code"))).findAny().orElseGet(HashMap::new).get("reserve1"));

        List<String> files = new ArrayList();
        for (Map<String, Object> map : workList) {
            Map<String, Object> obj = fillAssetMaintainMapNew(map, methodParam, solutionsMap, repairTypesMap);
            logger.info("fill Image begin : [{}]", System.currentTimeMillis());
            fillImage(schema_name, beforeImgXyz, (String) obj.get("before_img1"), obj);
            fillImage(schema_name, resultImgXyz, (String) obj.get("result_img1"), obj);
            logger.info("fill Image end : [{}]", System.currentTimeMillis());
            String file = Constants.REPORT_TEMPLATENAME_ASSETMAINTAIN; //模板文件名需要配置
            String out = basePath + System.getProperty("file.separator") + map.get("work_code") + RandomUtils.nextLong(10000, 20000) + ".xls";
            files.add(out);
            logger.info("buildExcel begin : [{}]", System.currentTimeMillis());
            buildExcel(obj, file, out);
            logger.info("buildExcel end : [{}]", System.currentTimeMillis());

        }
        String fileName = (String) data.get("report_name") + "_" + param.getId();
        createZipFile(filePath, fileName, files);
        FileUtil.deleteDir(basePath);
        return null;

    }

    /**
     * 生成并下载压缩文件
     *
     * @param name
     * @param files
     */
    private void createZipFile(String basePath, String name, List<String> files) {
        String zipFile = basePath + System.getProperty("file.separator") + name + ".zip";
        String zipFileName = "repot.zip";
        FileUtil.createZip(zipFile, files);
        //DownloadUtil.download(zipFile,zipFileName,false);
    }


    /**
     * 获取图片路径
     *
     * @param id
     * @return
     */
    private String getImgUrl(String schemaName, String id) {
        try {
            Map<String, Object> map = RegexUtil.optMapOrNew(filesService.findById(schemaName, Integer.valueOf(id)));
            return RegexUtil.optStrOrBlank(map.get("file_url"));
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * @param xyz
     * @param imgIds
     */
    private void fillImage(String schemaName, String xyz, String imgIds, Map<String, Object> obj) {
        RegexUtil.optNotBlankStrOpt(imgIds).map(s -> s.split(",")).map(Arrays::asList).filter(RegexUtil::optIsPresentListStr).ifPresent(l -> {
            List<Map<String, Object>> picAllList = RegexUtil.optNotNullListOrNew(obj.get("pic_list"));
            List<String> picList = new ArrayList<>();
            l.stream().forEach(id -> picList.add(getImgUrl(schemaName, id)));
            Map<String, Object> imgInfo = new HashMap<>();
            imgInfo.put("xyz", xyz);
            imgInfo.put("pathList", picList);
            picAllList.add(imgInfo);
            obj.put("pic_list", picAllList);
        });
    }

    /**
     * 查找名称
     *
     * @param map
     * @param code
     * @return
     */
    private String getCloudDataName(List<Map<String, Object>> map, String code) {
        return RegexUtil.optStrOrBlank(RegexUtil.optNotNullListOrNew(map.stream()
                .filter(k -> {
                    return code.equals(k.get("code"));
                }).collect(Collectors.toList()))
                .stream()
                .findFirst()
                .orElseGet(HashMap<String, Object>::new).get("name"));

    }

    /**
     * @param map
     * @param methodParam
     * @return
     */
    private Map<String, Object> fillBomMap(Map<String, Object> map, MethodParam methodParam) {
        Map<String, Object> obj = new HashMap();
        obj.put("bom_name", RegexUtil.optStrOrBlank(map.get("bom_name")));
        obj.put("bom_model", RegexUtil.optStrOrBlank(map.get("bom_model")));
        obj.put("title", RegexUtil.optStrOrBlank(map.get("title")));
        obj.put("in_quantity", RegexUtil.optStrOrBlank(map.get("in_quantity")));
        obj.put("out_quantity", RegexUtil.optStrOrBlank(map.get("out_quantity")));
        obj.put("num", map.get("num"));
        obj.put("last_quantity", "");
        obj.put("site", "");
        obj.put("quantity", RegexUtil.optStrOrBlank(map.get("quantity")));
        return obj;
    }

    /**
     * @param map
     * @param methodParam
     * @return
     */
    private Map<String, Object> fillBomRecordMap(Map<String, Object> map, MethodParam methodParam) {
        Map<String, Object> obj = new HashMap();
        obj.put("bom_name", RegexUtil.optStrOrBlank(map.get("bom_name")));
        obj.put("bom_model", RegexUtil.optStrOrBlank(map.get("bom_model")));
        obj.put("title", RegexUtil.optStrOrBlank(map.get("title")));
        obj.put("material_code", RegexUtil.optStrOrBlank(map.get("material_code")));
        obj.put("quantity", RegexUtil.optStrOrBlank(map.get("quantity")));
        obj.put("stock_name", RegexUtil.optStrOrBlank(map.get("stock_name")));
        obj.put("max_in_time", DateUtil.getDateStr((Date) map.get("max_in_time"), DateUtil.format1));
        obj.put("use_count", RegexUtil.optStrOrBlank(map.get("use_count")));
        return obj;
    }


    /**
     * 填充调拨报废模板
     *
     * @param map
     * @param methodParam
     * @return
     */
    private Map<String, Object> fillAssetWorksMap(Map<String, Object> map, MethodParam methodParam) {
        Map<String, Object> obj = new HashMap();
        obj.put("num", map.get("num"));
        String[] dateStr = DateUtil.splitDateStr(DateUtil.getDateStr((Date) map.get("create_time"), DateUtil.format1));
        obj.put("create_time_0", dateStr[0]);
        obj.put("create_time_1", dateStr[1]);
        obj.put("create_time_2", dateStr[2]);
        obj.put("position_name", RegexUtil.optStrOrBlank(map.get("position_name")));
        obj.put("cost_note", RegexUtil.optStrOrBlank(map.get("cost_note")));
        obj.put("approve_user_id", "");
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("approve_user_id")))) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, RegexUtil.optStrOrBlank(map.get("approve_user_id"))));
            obj.put("approve_user_id", RegexUtil.optStrOrBlank(user.get("user_name")));
        }
        return obj;
    }

    /**
     * 二级保养记录
     *
     * @param map
     * @param methodParam
     * @param repairTypesMap
     * @return
     */
    private Map<String, Object> fillSecondMaintainMap(Map<String, Object> map, MethodParam methodParam, List<Map<String, Object>> repairTypesMap) {
        Map<String, Object> obj = new HashMap();
        obj.put("num", map.get("num"));
        obj.put("plan_name", RegexUtil.optStrOrBlank(map.get("title")));
        obj.put("begin_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("begin_time")), DateUtil.format1, DateUtil.format2));
        obj.put("receive_user_id", "");
        obj.put("task", "");
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("receive_user_id")))) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, RegexUtil.optStrOrBlank(map.get("receive_user_id"))));
            obj.put("receive_user_id", RegexUtil.optStrOrBlank(user.get("user_name")));
        }
        JSONArray modelFields = JSONArray.fromObject(RegexUtil.optStrOrVal(map.get("task"), "[]"));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < modelFields.size(); i++) {
            net.sf.json.JSONObject json = modelFields.getJSONObject(i);
            int nums = i + 1;
            String task_item_name = nums + ". " + RegexUtil.optStrOrBlank(json.get("task_item_name"));
            sb.append(task_item_name);
            if (i < modelFields.size() - 1) {
                sb.append(" &#10;");
            }
        }
        obj.put("task", sb.toString());
        return obj;
    }

    /**
     * 填充项修记录模板
     *
     * @param map
     * @param methodParam
     * @return
     */
    private Map<String, Object> fillRepairRcordMap(Map<String, Object> map, MethodParam methodParam,
                                                   List<Map<String, Object>> faultReasons,
                                                   List<Map<String, Object>> repairTypes) {
        Map<String, Object> obj = new HashMap();
        obj.put("asset_name", RegexUtil.optStrOrBlank(map.get("asset_name")));
        obj.put("asset_code", RegexUtil.optStrOrBlank(map.get("asset_code")));
        obj.put("begin_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("begin_time")), DateUtil.format1, DateUtil.format1));
        obj.put("repair_type_id", getCloudDataName(repairTypes, RegexUtil.optStrOrBlank(map.get("repair_type_id"))));
        obj.put("fault_reason", getCloudDataName(faultReasons, RegexUtil.optStrOrBlank(map.get("fault_reason"))));
        return obj;
    }

    /**
     * 填充技术规格模板数据
     *
     * @param map
     * @return
     */
    private Map<String, Object> fillAssetTotalMapNew(Map<String, Object> map, MethodParam methodParam) {

        Map<String, Object> obj = new HashMap();
        obj.put("num", map.get("num"));
        obj.put("asset_name", RegexUtil.optStrOrBlank(map.get("asset_name")));
        obj.put("asset_code", RegexUtil.optStrOrBlank(map.get("asset_code")));
        obj.put("asset_model", RegexUtil.optStrOrBlank(map.get("model_name")));
        obj.put("price", RegexUtil.optStrOrBlank(map.get("price")));
        //obj.put("instal_date",RegexUtil.optStrOrBlank(map.get("install_date")));
        String[] dateStr = DateUtil.splitDateStr(DateUtil.getDateStr((Date) map.get("install_date"), DateUtil.format1));
        obj.put("instal_date_0", dateStr[0]);
        obj.put("instal_date_1", dateStr[1]);
        //obj.put("enable_time",DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("enable_time")),DateUtil.format1,DateUtil.format2));
        String[] dateStr1 = DateUtil.splitDateStr(DateUtil.getDateStr((Date) map.get("enable_time"), DateUtil.format1));
        obj.put("enable_time_0", dateStr1[0]);
        obj.put("enable_time_1", dateStr1[1]);
        obj.put("position_name", RegexUtil.optStrOrBlank(map.get("position_name")));
        obj.put("basic9", "");
        obj.put("basic13", "");
        obj.put("basic4", "");
        obj.put("basic3", "");
        obj.put("basic2", "");
        obj.put("inner_code", "");
        String properties = RegexUtil.optStrOrVal(map.get("properties"), "[]");
        JSONArray modelFields = JSONArray.fromObject(properties);
        for (int i = 0; i < modelFields.size(); i++) {
            net.sf.json.JSONObject json = modelFields.getJSONObject(i);
            String fieldValue = RegexUtil.optStrOrBlank(json.get("field_value"));
            if ("basic9".equals(json.get("field_code"))) {
                String[] dates = DateUtil.splitDateStr(fieldValue);
                obj.put("basic9_0", dates[0]);
                obj.put("basic9_1", dates[1]);
            } else if ("basic13".equals(json.get("field_code"))) {
                obj.put("basic13", fieldValue);
                if (RegexUtil.optIsPresentStr(fieldValue)) {
                    Map<String, Object> facilities =
                            RegexUtil.optMapOrNew(facilitiesMapper.findByTitle(methodParam.getSchemaName(), fieldValue));
                    obj.put("inner_code", RegexUtil.optStrOrBlank(facilities.get("inner_code")));

                }
            } else if ("basic4".equals(json.get("field_code"))) {
                obj.put("basic4", fieldValue);
            } else if ("basic3".equals(json.get("field_code"))) {
                obj.put("basic3", fieldValue);
            } else if ("basic2".equals(json.get("field_code"))) {
                obj.put("basic2", fieldValue);
            }
        }
        //obj.put("basic2",RegexUtil.optStrOrBlank(map.get("title")));
        return obj;
    }

    /**
     * 填充技术规格模板数据
     *
     * @param map
     * @return
     */
    private Map<String, Object> fillAssetMapNew(Map<String, Object> map, MethodParam methodParam) {

        Map<String, Object> obj = new HashMap();
        obj.put("asset_name", RegexUtil.optStrOrBlank(map.get("asset_name")));
        obj.put("asset_code", RegexUtil.optStrOrBlank(map.get("asset_code")));
        obj.put("asset_model", RegexUtil.optStrOrBlank(map.get("model_name")));
        obj.put("price", RegexUtil.optStrOrBlank(map.get("price")));
        obj.put("basic2", RegexUtil.optStrOrBlank(map.get("title")));
        obj.put("enable_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("enable_time")), DateUtil.format1, DateUtil.format2));
        obj.put("basic12", RegexUtil.optStrOrBlank(map.get("position_name")));
        return obj;
    }

    /**
     * 填充大中修模板数据
     *
     * @param map
     * @return
     */
    private Map<String, Object> fillBigRepairMapNew(Map<String, Object> map, MethodParam methodParam) {

        Map<String, Object> obj = new HashMap();
        obj.put("plan_name", RegexUtil.optStrOrBlank(map.get("title")));
        obj.put("measures_note", RegexUtil.optStrOrBlank(map.get("measures_note")));
        obj.put("begin_time", map.get("begin_time"));
        obj.put("receive_user_id", "");
        obj.put("before_img", "");
        obj.put("result_img", "");
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("receive_user_id")))) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, RegexUtil.optStrOrBlank(map.get("receive_user_id"))));
            obj.put("receive_user_id", RegexUtil.optStrOrBlank(user.get("user_name")));
        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("before_img")))) {
            obj.put("before_img1", RegexUtil.optStrOrBlank(map.get("before_img")));

        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("result_img")))) {
            obj.put("result_img1", RegexUtil.optStrOrBlank(map.get("result_img")));

        }

        return obj;
    }


    /**
     * 填充工单维修模板数据
     *
     * @param map
     * @return
     */
    private Map<String, Object> fillAssetMaintainMapNew(Map<String, Object> map, MethodParam methodParam,
                                                        List<Map<String, Object>> solutions,
                                                        List<Map<String, Object>> repairTypes) {

        Map<String, Object> obj = new HashMap();
        obj.put("work_code", RegexUtil.optStrOrBlank(map.get("work_code")));
        obj.put("asset_name", RegexUtil.optStrOrBlank(map.get("asset_name")));
        obj.put("asset_code", RegexUtil.optStrOrBlank(map.get("asset_code")));
        obj.put("group_name", RegexUtil.optStrOrBlank(map.get("group_name")));
        obj.put("repair_note", RegexUtil.optStrOrBlank(map.get("repair_note")));
        obj.put("occur_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("occur_time")), DateUtil.format1, DateUtil.format1));
        obj.put("measures_note", RegexUtil.optStrOrBlank(map.get("measures_note")));
        obj.put("create_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("create_time")), DateUtil.format1, DateUtil.format1));
        obj.put("audit_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("audit_time")), DateUtil.format1, DateUtil.format1));
        obj.put("confirm_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("finished_time")), DateUtil.format1, DateUtil.format1));
        obj.put("begin_time", DateUtil.transDateFormat(RegexUtil.optStrOrBlank(map.get("begin_time")), DateUtil.format1, DateUtil.format1));
        obj.put("repair_type_id", getCloudDataName(repairTypes, RegexUtil.optStrOrBlank(map.get("repair_type_id"))));
        obj.put("measures", getCloudDataName(solutions, RegexUtil.optStrOrBlank(map.get("measures"))));
        obj.put("receive_user_id", "");
        obj.put("approve_user_id", "");
        obj.put("approve_user_id2", "");
        obj.put("create_user_id", "");
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("receive_user_id")))) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, RegexUtil.optStrOrBlank(map.get("receive_user_id"))));
            obj.put("receive_user_id", RegexUtil.optStrOrBlank(user.get("user_name")));
        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("approve_user_id")))) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, RegexUtil.optStrOrBlank(map.get("approve_user_id"))));
            obj.put("approve_user_id", RegexUtil.optStrOrBlank(user.get("user_name")));
        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("approve_user_id2")))) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, RegexUtil.optStrOrBlank(map.get("approve_user_id2"))));
            obj.put("approve_user_id2", RegexUtil.optStrOrBlank(user.get("user_name")));
        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("before_img")))) {
            //List<Map> list = JsonUtils.jsonToList(RegexUtil.optStrOrBlank(map.get("before_img")), Map.class);
            obj.put("before_img1", RegexUtil.optStrOrBlank(map.get("before_img")));
        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(map.get("result_img")))) {
            //List<Map> list = JsonUtils.jsonToList(RegexUtil.optStrOrBlank(map.get("result_img")), Map.class);
            obj.put("result_img1", RegexUtil.optStrOrBlank(map.get("result_img")));
        }

        String createUser = RegexUtil.optStrOrBlank(map.get("create_user_id"));
        if (RegexUtil.optIsPresentStr(createUser)) {
            Map<String, Object> user = RegexUtil.optMapOrNew(cacheUtilService.getUserInfo(methodParam, createUser));
            obj.put("create_user_id", RegexUtil.optStrOrBlank(user.get("user_name")));
            /*List<Map<String,Object>> orgList = RegexUtil.optNotNullListOrNew(userService.getUserGpList(methodParam,createUser));
            for(Map<String,Object> org : orgList){
                obj.put("group_name",RegexUtil.optStrOrBlank(org.get("group_name")));
            }*/
        }
        return obj;
    }


    /**
     * 获取临时文件生成路径
     *
     * @param schemaName
     * @return
     */
    private String getTmpBuildPath(String schemaName) {
        String basePath = fileConfiguration.file_upload_dir;
        String sep = System.getProperty("file.separator");
        String uuid = UUID.randomUUID().toString();
        basePath = basePath + sep + schemaName + sep + "report" + sep + uuid;
        FileUtil.initDir(basePath);
        return basePath;
    }

    /**
     * 获取生成路径
     *
     * @param schemaName
     * @return
     */
    private String getFilePath(String schemaName) {
        String basePath = fileConfiguration.file_upload_dir;
        String sep = System.getProperty("file.separator");
        basePath = basePath + sep + schemaName + sep + "report";
        FileUtil.initDir(basePath);
        return basePath;
    }


    /**
     * 生成excel
     *
     * @param map
     * @param templateName
     * @param outFile
     */
    private void buildExcel(Map<String, Object> map, String templateName, String outFile) {
        String basePath = fileConfiguration.file_upload_dir;
        String sep = System.getProperty("file.separator");
        basePath = basePath + sep + "template";
        WordExportUtil.createWordByData(map, templateName, "", basePath, outFile, "");

    }

    private Map<String, Object> getSuezReportByIdNT(MethodParam methodParam, String id) {
        return RegexUtil.optMapOrExpNullInfo(suezReportMapper.findReportByIdNT(methodParam.getSchemaName(), Integer.valueOf(id)), LangConstant.TITLE_ABAA_I); // 报告不存在
    }

    @Override
    public Map<String, Object> getSuezReportList(MethodParam methodParam, Map<String, Object> pm) {
        Map<String, Object> result = new HashMap<>();
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String whereString = getWhereString(methodParam, pm);
        //查询该条件下的总记录数
        int total = suezReportMapper.findSuezReportListCount(methodParam.getSchemaName(), whereString);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        List<Map<String, Object>> rows = suezReportMapper.findSuezReportList(methodParam.getSchemaName(), whereString, pagination);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }


    /**
     * 获取查询备件信息的条件
     *
     * @param methodParam 系统参数
     * @param bParam      请求参数
     * @return 拼接sql
     */
    private String getWhereString(MethodParam methodParam, Map<String, Object> bParam) {
        StringBuffer whereString = new StringBuffer(" where 1=1 ");
        RegexUtil.optNotBlankStrOpt(bParam.get("startDateSearch")).ifPresent(e -> whereString.append(" and substring(query_propertity ->> 'begin_time',1,10) >='").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(bParam.get("endDateSearch")).ifPresent(e -> whereString.append(" and substring(query_propertity ->> 'begin_time',1,10) <='").append(e).append("' "));
        RegexUtil.optNotBlankStrLowerOpt(bParam.get("keywordSearch")).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(report_name) like ")
                .append(e).append(")"));
        RegexUtil.optNotBlankStrOpt(bParam.get("workTypeIds")).ifPresent(e -> {
            String workTypeIds = "";
            if (e.contains(",")) {
                StringBuilder sb = new StringBuilder();
                String[] split = e.split(",");
                for (String id : split) {
                    sb.append("query_propertity -> 'work_type_id' @> '").append(id).append("' :: jsonb or ");
                }
                workTypeIds = sb.toString().substring(0, sb.toString().length() - 3);
            } else {
                workTypeIds = "query_propertity -> 'work_type_id' @> '" + e + "' :: jsonb ";
            }
            whereString.append(" and (").append(workTypeIds).append(")");

        });

        return whereString.toString();
    }
}
