package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.UserClientService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 2:06 2019/7/18 0018
 */
@Service
public class UserClientServiceImpl implements UserClientService {
//    private static final Logger logger = LoggerFactory.getLogger(UserClientServiceImpl.class);
//    @Autowired
//    UserClientMapper userClientMapper;
//    @Autowired
//    MetadataWorkMapper metadataWorkMapper;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    SerialNumberService serialNumberService;
//    @Autowired
//    MessageService messageService;
//
//
//    @Override
//    public List<UserClientModal> getUserClientListByAccout(String schema_name, String account, int pageSize, int begin) {
//        return userClientMapper.getUserClientListByAccout(schema_name, account, pageSize, begin);
//    }
//
//    @Override
//    public int getUserClientListByAccoutCount(String schema_name, String account) {
//        return userClientMapper.getUserClientListByAccoutCount(schema_name, account);
//    }
//
//
//    /**
//     * 取数据编号
//     *
//     * @param schemaName
//     * @return
//     */
//    public String getUserClientCode(String schemaName) {
//        String code = serialNumberService.generateMaxBusinessCode(schemaName, "user_client");
//        return code;
//    }
//
//    /**
//     * 添加客户用户
//     *
//     * @param request
//     * @param schema_name
//     * @param userClientModal
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public int addUserClient(HttpServletRequest request, String schema_name, UserClientModal userClientModal) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        int result = 0;
//        userClientModal.setStatus(StatusConstant.TO_BE_AUDITED);
//        //看看用户添加有没有重复的，以及用户表中有没有重复的
//        if (userClientMapper.checkClientUser(schema_name, userClientModal.getUser_account(), userClientModal.getUser_mobile()) > 0) {
//            result = 2;
//        } else if (userService.checkUserByMobile(schema_name, userClientModal.getUser_mobile()) > 0 || userService.checkUserByAccount(schema_name, userClientModal.getUser_account()) > 0 || userService.checkUserByUserCode(schema_name, userClientModal.getUser_account()) > 0) {
//            result = 2;
//        } else {
//            String appCode = this.getUserClientCode(schema_name);
//            userClientModal.setApp_code(appCode);
//            int doAdd = userClientMapper.addUserClient(schema_name, userClientModal);
//            if (doAdd > 0) {
//                String title = selectOptionService.getLanguageInfo(LangConstant.ADD_CLIENT_USER) + userClientModal.getUser_account();//添加用户
//                Map<String, String> map = new HashMap<String, String>();
//                map.put("title_page", title);
//                map.put("sub_work_code", appCode);
//                map.put("facility_id", String.valueOf(userClientModal.getClient_id()));
//                SimpleDateFormat simleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);;
//                Timestamp now = new Timestamp(System.currentTimeMillis());
//                String nowDateWord = simleDateFormat.format(now);
//                map.put("create_time", nowDateWord);
//                Map<String, Object> template = metadataWorkMapper.getWdTemplateByBusinessType(schema_name, Integer.valueOf(SensConstant.BUSINESS_NO_61), 2); // 获取新模板
//                JSONArray arrays = JSONArray.fromObject(JSONObject.fromObject(template.get("body_property")).get("value"));
//                JSONObject data = null;
//                Map<String, Object> templateMap = new HashMap<String, Object>();
//                String fieldCode = null;
//                for (Object object : arrays) {
//                    data = JSONObject.fromObject(object);
//                    fieldCode = data.get("fieldCode").toString();
//                    templateMap.put(fieldCode, data.get("fieldValue"));
//                }
//                String receive_account = commonUtilService.getRandomUserByInfo(schema_name, null, map, templateMap);
//                map.put("receive_account", receive_account);
//                String flowId = dynamicCommonService.getWorkFlowId(schema_name, SensConstant.BUSINESS_NO_61);
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, flowId, userClientModal.getCreate_user_account(), map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new Exception(selectOptionService.getLanguageInfo(LangConstant.ADD_USER_ERROR) + userClientModal.getUser_account());
//                }
//                String account = user.getAccount();
//                messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_61, appCode,
//                        account, user.getUsername(), null, null); // 发送短信
//                workProcessService.saveWorkProccess(schema_name, appCode, StatusConstant.TO_BE_AUDITED, account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//                logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_61), appCode, title, account);//记录历史
//                result = 1;
//            }
//        }
//        return result;
//    }
//
//    /**
//     * 获取用户列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findUserClientList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String keyWord = request.getParameter("keyWord");
//        //首页条件
//        StringBuffer condition = new StringBuffer("");
//
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition.append(" and (");
//            condition.append(" t.user_account like '%" + keyWord + "%'");
//            condition.append(" or t.user_name like '%" + keyWord + "%'");
//            condition.append(" or t.create_user_account like '%" + keyWord + "%'");
//            condition.append(" or u.username like '%" + keyWord + "%'");
//            condition.append(" or t.audit_person like '%" + keyWord + "%'");
//            condition.append(" or au.username like '%" + keyWord + "%'");
//            condition.append(" or s.status like '%" + keyWord + "%'");
//            condition.append(" ) ");
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = userClientMapper.findUserClientList(schemaName, condition.toString(), pageSize, begin);
//        User user = AuthService.getLoginUser(request);
//        String account = user.getAccount();
//        List<String> roles = new ArrayList<>(user.getRoles().keySet());
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("app_code")));
//        if (subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schemaName, "", null, subWorkCodes, account, roles, false, "", "", "", pageSize, pageNumber);
//
//        for (Map<String, Object> dataMap : dataList) {
//            String appCode = (String) dataMap.get("app_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(appCode);
//            if (task != null && null != task.getAssignee()) {
//                dataMap.put("is_handle", task.getAssignee().equals(account));
//                continue;
//            }
//        }
//        int total = userClientMapper.countUserClientList(schemaName, condition.toString());
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 根据主键查询用户数据
//     *
//     * @param schemaName
//     * @param code
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryUserClientById(String schemaName, String code) {
//        return userClientMapper.queryUserClientById(schemaName, code);
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String result = this.findUserClientList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("app_code");
//
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        permissionInfo.put("user_client_audit", "isDistribute");
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "user_client");
//
//        Map<String, Object> data = userClientMapper.queryUserClientById(schemaName, code);
//        Integer status = (Integer) data.get("status"); // 工单类型
//        if (StatusConstant.TO_BE_AUDITED != status) {
//            mav.getModel().put("isDistribute", false);
//        }
//
//        Map<String, Object> template = metadataWorkMapper.getWdTemplateByBusinessType(schemaName, Integer.valueOf(SensConstant.BUSINESS_NO_61), 2); // 获取模板
//        mav.addObject("workFormKey", template.get("work_template_code"));
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/user_client/findSingleUserClientInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 用户添加审核
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel doAuditUserClient(String schema_name, Map<String, Object> paramMap, HttpServletRequest request) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String account = user.getAccount();
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> businessInfo = (Map<String, Object>) dataInfo.get("_sc_client_user");
//        String code = (String) businessInfo.get("app_code");
//        // 根据数据库中状态判断是否为重复操作
//        Map<String, Object> dbDataInfo = userClientMapper.queryUserClientById(schema_name, code);
//        dynamicCommonService.doCheckFlowInfo(schema_name, account, code);
//        String logWord = selectOptionService.getLanguageInfo(LangConstant.MSG_AUDIT_PASS);//审核通过
//
//        String remark = (String) businessInfo.get("audit_content");
//        remark = (null == remark || remark.isEmpty()) ? "" : remark;
//        Map map = new HashMap();
//        int status = StatusConstant.COMPLETED;
//        Boolean is_submit = false; // 是否提交
//        businessInfo.put("audit_time", new Timestamp(System.currentTimeMillis()));
//        businessInfo.put("audit_person", account);
//        String msgCode = null; // 消息编码
//        if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {//同意按钮
//            is_submit = true;//提交事件
//        } else if (do_flow_key != null && !do_flow_key.equals("") && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_RETURN))) {
//            is_submit = true;//提交事件
//            msgCode = qcloudsmsConfig.SMS_10002004;
//            status = StatusConstant.REFUSE;
//            logWord = selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK);
//        }
//        businessInfo.put("status", status);
//        userClientMapper.updateUserClientById(schema_name, businessInfo, code); // 更新信息
//        // 同意按钮
//        if (null != do_flow_key && !"".equals(do_flow_key) && do_flow_key.equals(String.valueOf(ButtonConstant.BTN_AGREE))) {
//            businessInfo.putAll(dbDataInfo);
//            long companyId = authService.getCompany(request).getId();
//            userService.doAddUserByAudit(businessInfo, schema_name, companyId); // 用户信息转移
//        }
//        if (is_submit) {
//            String taskId = request.getParameter("task_id");
//            map.put("status", status);
//            map.put("sub_work_code", code);
//            map.put("do_flow_key", do_flow_key);
//            map.put("create_user_account", account); // 发送短信人
//            String receive_account = (String) dbDataInfo.get("create_user_account");
//            map.put("receive_account", receive_account); // 接受人
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, code);
//            Boolean workFlowResult = workflowService.complete(schema_name, taskId, account, map);
//            if (!workFlowResult) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK));
//            }
//            messageService.msgPreProcess(msgCode, receive_account, SensConstant.BUSINESS_NO_61, code,
//                    account, user.getUsername(), null, null); // 发送短信
//            workProcessService.saveWorkProccess(schema_name, code, status, account, taskSid);//工单进度记录
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_61), code, logWord + "：" + remark, account);//记录历史
//            return ResponseModel.ok("ok");
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_FAULT_CODE) + "：S404"); // 表单数据有误，返回失败提示
//    }
}
