package com.gengyun.senscloud.service;


// 准备删除 yzj  2019-08-04

public interface RepairService {
//
//    //获取维保类型集合
//    RepairTypeCollection findAllRepairTypeCollectionList(String schema_name, long facilityId,String userType);
//
//    //保存维修单信息
//    int saveRepairInfo(String schema_name, RepairData repairData);
//
//    //新增支援人员，初始工时为0，支援人员中包含_sc_repair表中的接受人员
//    int insertRepairWorkHour(String schema_name, RepairWorkHoursData repairWorkHourData);
//
//    //获取最近一小时内的未完成的维修单
//    int getRepairCountByAssetIdRecently(String schema_name,String asset_id, Timestamp current);
//
//    //编辑维保信息,点击保存按钮，或者草稿状态，重新保存或提交
//    int EditRepair(String schema_name, RepairData repairData);
//
//    //更新分配人
//    int EditRepairReceiveMan(String schema_name, RepairData repairData);
//
//    //删除援人员，在重新保存和提交维修单时，先全部删除工时表中的人，再重新新增支援人员进来
//    int deleteRepairWorkHour(String schema_name, String repair_code);
//
//    //更新编辑开始维修时间，为手机上点击设备的维修开始
//    int EditRepairBeginTime(String schema_name, RepairData repairData);
//
//    //设置维修开始计时,缺表，待建 2018-03-29
//
//
//    //保存和提交维修结果
//    int SaveRepairResult(String schema_name, RepairData repairData);
//
//    //新增维修使用的备件
//    int insertRepairBom(String schema_name, RepairBomData repairBomData);
//
//    //删除维修领用的备件,用于维修结果重新保存或提交时
//    int deleteRepairBom(String schema_name, String repair_code);
//
//    //维修结果审核
//    int AuditRepairResult(String schema_name, RepairData repairData);
//
//    //查找维修单列表，待新增分页参数，20180329
//    List<RepairData> getRepairList(String schema_name, String condition, int pageSize, int begin);
//
//    //查找维修单列表总数
//    int getRepairListCount(String schema_name, String condition);
//
//    //查找单条维修单据
//    RepairData getRepariInfo(String schema_name, String repairCode);
//
//    //查找单条维修单据，根据设备id
//    RepairData getRepariInfoByAssetId(String schema_name, String asset_id, String account);
//
//    //保存支援人员
//    void SaveRepairWorkMan(String schema_name, String receive_account, String help_account, String repairCode);
//
//    //查找设备资产名称 根据设备编号
//    Asset getAssetCommonDataByCode(String schema_name, String device_code);
//
//    //查找维修单，查到需维修的设备
//    Asset getAssetCommonDataByRepairCode(String schema_name, String repairCode);
//
//    //通过来源，查询维修单
//    List<RepairData> getRepairListByFromCode(String schema_name, String fromCode);
//    //查找维修记录，通过设备编号
//    List<RepairData> getRepairListByCode(String schema_name,String device_code);
//
//    //备件统计维修使用数量明细查询by bom_model and stock_code
//    List<WorkSheet> getRepairDetailByBomModelAndStockCode(String schema_name, String condition);
//
//    //作废维修
//    int invalidRepair(String schema_name, String repairCode);
//
//    //嘉岩更新添加数据
//    int insertRepairBomJY(String schema_name, RepairBomData repairBomData);
//
//    //嘉研备件申领修改状态
//    int updateRepairApplyStatus(String schema_name, String code);
//
//    //嘉研备件申领修改状态
//    int updateOpenRepairApplyStatus(String schema_name, String code ,int status);
//
//    //维修备件是否存在备件信息
//    List<RepairBomData> selectRepairByMaterialCode(String schema_name, String code,String material_code);
//
//    //修改维修备件已存在的第三方客户来源备件信息
//    int updateRepairBomByMaterCode(String schema_name,String bill_code);
//
//    public List<RepairData> queryAssetFailureList(String schema_name,int pageSize, int begin);
//
//    public int queryAssetFailureListCount(String schema_name);
//
//    FullScreenData getPlannedFailureRepair(String schema_name);
}
