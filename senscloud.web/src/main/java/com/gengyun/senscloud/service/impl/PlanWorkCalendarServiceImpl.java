package com.gengyun.senscloud.service.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.PlanWorkCalendarMapper;
import com.gengyun.senscloud.mapper.PlanWorkMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.PlanWorkCalendarService;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import com.gengyun.senscloud.view.PlanWorkCalendarDataExportView;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PlanWorkCalendarServiceImpl implements PlanWorkCalendarService {
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    PlanWorkCalendarMapper planWorkCalendarMapper;
    @Resource
    PlanWorkService planWorkService;
    @Resource
    LogsService logsService;
    @Resource
    PlanWorkMapper planWorkMapper;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    CompanyService companyService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 入参
     * @return
     */
    @Override
    public Map<String, Map<String, Boolean>> getPlanWorkCalendarPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[19]);
    }

    /**
     * 查询维保行事历列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getPlanWorkCalendarList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getPlanWorkCalendarWhereString(methodParam, param);
        int total = planWorkCalendarMapper.findCountPlanWorkCalendarList(schemaName, searchWord, methodParam.getUserId()); // 获取维保行事历总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by pwc.status,pwc.occur_time ";
            searchWord += pagination;
            List<Map<String, Object>> planScheduleList = null;
            planScheduleList = planWorkCalendarMapper.findPlanWorkCalendarList(schemaName, searchWord, methodParam.getUserId());
            result.put("rows", planScheduleList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 计划行事历生成工单
     *
     * @param methodParam
     * @param work_calendar_code
     */
    @Override
    public void generateWork(MethodParam methodParam, String work_calendar_code) {
        PlanWorkCalendarModel info = planWorkCalendarMapper.findDetailByCode(methodParam.getSchemaName(), work_calendar_code);
        if (RegexUtil.optIsPresentStr(info)) {
            int status = info.getStatus();
            if (status == Constants.PLAN_WORK_CALENDAR_STATUS_NEW) {
                commonUtilService.createWorkOrderByPlanWorkCalendar(methodParam, info);
                planWorkService.updatePlanWorkCalendarStatusByWorkCalendarCodeList(methodParam.getSchemaName(), "'" + work_calendar_code + "'", Constants.PLAN_WORK_CALENDAR_STATUS_GENERATED);
            }
        }
    }

    /**
     * 编辑计划行事历
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWork(MethodParam methodParam, Map<String, Object> data) {
        String work_calendar_code = RegexUtil.optStrOrExpNotNull(data.get("work_calendar_code"), LangConstant.TITLE_ACJ);
        String occur_time = RegexUtil.optStrOrExpNotNull(data.get("occur_time"), LangConstant.TITLE_AK_H);
        String deadline_time = RegexUtil.optStrOrExpNotNull(data.get("deadline_time"), LangConstant.TITLE_AL_O);
        PlanWorkCalendarModel info = planWorkCalendarMapper.findDetailByCode(methodParam.getSchemaName(), work_calendar_code);
        RegexUtil.optNotBlankStrOpt(data.get("planJson")).map(DataChangeUtil::scdStringToMap).ifPresent(m -> {
            String checkDataFieldStr =
                    "{'bom_list':[{'checkKey': 'material_code','lang_key':'" + LangConstant.TITLE_BA + "'}]," +
                            "'tools_list':[{'checkKey': 'type_name','lang_key':'" + LangConstant.TITLE_CATEGORY_Y + "'}], " +
                            "'fee_list':[{'checkKey': 'type_name','lang_key':'" + LangConstant.TITLE_CATEGORY_T + "'}]}";
            Map<String, Object> map = DataChangeUtil.scdObjToMap(checkDataFieldStr);
            map.forEach((k, o) ->
                    RegexUtil.optNotBlankStrOpt(m.get(k)).map(DataChangeUtil::scdStringToList).ifPresent(dr -> dr.forEach((d) ->
                            RegexUtil.optNotNullList(DataChangeUtil.scdObjToList(o)).ifPresent(l -> l.forEach(f ->
                                    RegexUtil.optStrOrExpNotNull(d.get(RegexUtil.optStrOrBlank(f.get("checkKey"))), RegexUtil.optStrOrBlank(f.get("lang_key")))
                            ))
                    ))
            );
        });
        int status = info.getStatus();
        if (status == 2 || status == 4) {
            throw new SenscloudException(LangConstant.TEXT_J, new String[]{LangConstant.TITLE_DH});
        }
        data.put("schema_name", methodParam.getSchemaName());
        data.put("status", status);
        if (status == 3) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date occur_time_date = null;
            Date deadline_time_date = null;
            try {
                occur_time_date = sdf.parse(occur_time);
                deadline_time_date = sdf.parse(deadline_time);
            } catch (Exception e) {
            }
            if (occur_time_date.getTime() > deadline_time_date.getTime()) {
                throw new SenscloudException(LangConstant.MSG_N);//
            }
            Date now = new Date();
            if (occur_time_date.getTime() <= now.getTime() && deadline_time_date.getTime() >= now.getTime()) {
                data.put("status", 1);
            }
        }
        Map<String, Object> map = this.getPlanWorkCalendarInfoByCode(methodParam, work_calendar_code);
        planWorkCalendarMapper.updatePlanWorkCalendar(data);
        Map<String, Object> newMap = this.getPlanWorkCalendarInfoByCode(methodParam, work_calendar_code);
        JSONArray logArr = new JSONArray();
        String dbBodyProperty = RegexUtil.optStrOrNull(map.get("body_property"));
        String newBodyProperty = RegexUtil.optStrOrNull(newMap.get("body_property"));
        newMap.putAll(JSONObject.fromObject(newBodyProperty));
        map.putAll(JSONObject.fromObject(dbBodyProperty));
        String dataFieldStr =
                //1-输入框 ,2-下拉框, 3-集合 4-勾选判断
                "{'name': 'type_name', 'checkType': '1', 'typeKey':'" + LangConstant.TITLE_CATEGORY_X + "'}," +
                        "{'name': 'priority_level', 'checkType': '2','typeKey': '" + LangConstant.TITLE_AL_V + "','selectKey':'priority_level'}, " +
                        "{'name': 'repair_rate', 'checkType': '2','typeKey': '" + LangConstant.TITLE_AAW_U + "','selectKey':'repair_rate'}, " +
                        "{'name': 'work_level', 'checkType': '2','typeKey': '" + LangConstant.TITLE_AI_F + "','selectKey':'work_level'}, " +
                        "{'name': 'auto_generate_bill', 'checkType': '4','typeKey': '" + LangConstant.TITLE_ALM + "','selectKey':'auto_generate_bill'}," +
                        "{'name': 'generate_time_point',  'checkType': '1','typeKey': '" + LangConstant.LOG_AB + "'}, " +
                        "{'name': 'occur_time', 'checkType': '1','typeKey': '" + LangConstant.TITLE_AK_H + "'}," +
                        "{'name': 'deadline_time', 'checkType':'1','typeKey': '" + LangConstant.DP_M + "'}," +
                        "{'name': 'group_name', 'checkType':'1','typeKey': '" + LangConstant.TITLE_BA_Z + "','selectKey':'group_with_type'}, " +
                        "{'name': 'receive_user_name', 'checkType':'1','typeKey': '" + LangConstant.TITLE_AD_K + "','selectKey':'group_role_user'}, " +
                        "{'name': 'task_list', 'checkType': '3', 'typeKey': '" + LangConstant.TITLE_AAQ_Q + "','joinPrm':'group_name,task_item_name'}," +
                        "{'name': 'bom_list', 'checkType': '3','typeKey': '" + LangConstant.TITLE_AAAAY + "','joinPrm':'bom_name,material_code'}," +
                        "{'name': 'fee_list', 'checkType': '3','typeKey': '" + LangConstant.TITLE_AGI + "','joinPrm':'type_name,fee','keyInfo':{'0':'fee_category'}}, " +
                        "{'name': 'tools_list',  'checkType': '3','typeKey': '" + LangConstant.TITLE_BAAAB_I + "','joinPrm':'type_name,tools_name'}," +
                        "{'name': 'safe_list', 'checkType': '3','typeKey': '" + LangConstant.TITLE_FILE_J + "','joinPrm':'security_item_name'}," +
                        "{'name': 'file_list', 'checkType':'3','typeKey': '" + LangConstant.TITLE_BA_Y + "','joinPrm':'file_name'}";
        commonUtilService.doSetLogInfo(methodParam, "[" + dataFieldStr + "]", newMap, map, logArr, work_calendar_code, SensConstant.BUSINESS_NO_10006, "计划行事历属性更新");
    }


    /**
     * 作废计划行事历
     *
     * @param methodParam
     * @param work_calendar_code
     */
    @Override
    public void removeWork(MethodParam methodParam, String work_calendar_code) {
        String schemaName = methodParam.getSchemaName();
        String[] ids = {work_calendar_code};
        RegexUtil.falseExp(RegexUtil.optIsPresentStr(work_calendar_code), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAT_P}); // 数据未选择
        RegexUtil.trueExp(planWorkCalendarMapper.cntPwcWithStatusById(methodParam.getSchemaName(), work_calendar_code) > 0, LangConstant.TEXT_J, new String[]{LangConstant.TITLE_OV}); // 不可以作废
        RegexUtil.intExp(planWorkCalendarMapper.updatePwcStatusById(methodParam.getSchemaName(), work_calendar_code, 4), LangConstant.MSG_BK); // 未知错误！
        Map<String, Map<String, Object>> planWorkCalendarName = planWorkCalendarMapper.findPlanWorkCalendarNameList(schemaName, ids);
        for (String id : ids) {
            logsService.newLog(methodParam, SensConstant.BUSINESS_NO_10006, id, LangUtil.doSetLogArray("作废了维保计划", LangConstant.TEXT_AJ, new String[]{LangConstant.TITLE_OV, LangConstant.BTN_BC, RegexUtil.optNotNullMapObj(planWorkCalendarName).map(m -> m.get(id)).map(a -> a.get("title")).map(RegexUtil::optStrOrBlank).orElse("")}));
        }
    }


    /**
     * 作废选中计划行事历
     *
     * @param methodParam 系统参数
     * @param param       请求参数
     */
    @Override
    public void deletePlanCalendarByIds(MethodParam methodParam, SearchParam param) {
        String schemaName = methodParam.getSchemaName();
        String[] ids = RegexUtil.optNotBlankStrOpt(param.getCalendar_codes()).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAT_P})); // 数据未选择
        RegexUtil.trueExp(planWorkCalendarMapper.cntPwcWithStatusByIds(methodParam.getSchemaName(), ids) > 0, LangConstant.TEXT_J, new String[]{LangConstant.TITLE_OV}); // 不可以作废
        int len = ids.length;
        RegexUtil.falseExp(planWorkCalendarMapper.updatePwcStatusByIds(methodParam.getSchemaName(), ids, 4) == len, LangConstant.MSG_BK); // 未知错误
        Map<String, Map<String, Object>> planWorkCalendarName = planWorkCalendarMapper.findPlanWorkCalendarNameList(schemaName, ids);
        for (String id : ids) {
            logsService.newLog(methodParam, SensConstant.BUSINESS_NO_10006, id, LangUtil.doSetLogArray("作废了维保计划", LangConstant.TEXT_AJ, new String[]{LangConstant.TITLE_OV, LangConstant.BTN_BC, RegexUtil.optNotNullMapObj(planWorkCalendarName).map(m -> m.get(id)).map(a -> a.get("title")).map(RegexUtil::optStrOrBlank).orElse("")}));
        }
    }

    /**
     * 根据id查询计划行事历详情
     *
     * @param methodParam
     * @param work_calendar_code
     * @return
     */
    @Override
    public Map<String, Object> getPlanWorkCalendarInfo(MethodParam methodParam, String work_calendar_code) {
        Map<String, Object> planWorkMap = this.getPlanWorkCalendarInfoByCode(methodParam, work_calendar_code);//查询计划基本信息
        HashMap<String, Object> assist = new HashMap<>();
        assist.put("duty_type", RegexUtil.optStrOrBlank(planWorkMap.get("duty_type")));
        assist.put("duty_type_name", planWorkMap.get("duty_type_name"));
        assist.put("group_id", planWorkMap.get("group_id"));
        assist.put("group_name", planWorkMap.get("group_name"));
        assist.put("self_or_out", planWorkMap.get("self_or_out"));
        assist.put("self_or_out_name", planWorkMap.get("self_or_out_name"));
        assist.put("team_size", planWorkMap.get("team_size"));
        assist.put("total_hour", planWorkMap.get("total_hour"));
        assist.put("receive_user_id", planWorkMap.get("receive_user_id"));
        assist.put("receive_user_name", planWorkMap.get("receive_user_name"));
        planWorkMap.remove("duty_type");
        planWorkMap.remove("duty_type_name");
        planWorkMap.remove("group_id");
        planWorkMap.remove("group_name");
        planWorkMap.remove("self_or_out");
        planWorkMap.remove("self_or_out_name");
        planWorkMap.remove("team_size");
        planWorkMap.remove("total_hour");
        planWorkMap.remove("receive_user_id");
        planWorkMap.remove("receive_user_name");
        planWorkMap.put("assist", assist);
        String body_property = RegexUtil.optStrOrNull(planWorkMap.get("body_property"));
        if (RegexUtil.optIsPresentStr(body_property)) {
            com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(body_property);
            planWorkMap.put("planJson", jsonObject);
        } else {
            planWorkMap.put("planJson", new HashMap<>());
        }
        planWorkMap.remove("body_property");

        List<Map<String, Object>> log = logsService.getLog(methodParam, SensConstant.BUSINESS_NO_10006, work_calendar_code);
        planWorkMap.put("logs", RegexUtil.optIsPresentList(log) ? log : new ArrayList<>());
        return planWorkMap;
    }

    public Map<String, Object> getPlanWorkCalendarInfoByCode(MethodParam methodParam, String work_calendar_code) {
        return RegexUtil.optMapOrExpNullInfo(findPlanWorkCalendarInfoByCode(methodParam.getSchemaName(), work_calendar_code, methodParam.getUserLang(), methodParam.getCompanyId()), LangConstant.BTN_BC); // 维保计划不存在
    }

    public Map<String, Object> findPlanWorkCalendarInfoByCode(MethodParam methodParam, String work_calendar_code) {
        return findPlanWorkCalendarInfoByCode(methodParam.getSchemaName(), work_calendar_code, methodParam.getUserLang(), methodParam.getCompanyId());
    }

    public String findPlanWorkSeq(MethodParam methodParam) {
        return planWorkMapper.findPlanWorkSeq(methodParam.getSchemaName());
    }

    private String getPlanWorkCalendarWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer(" where 1=1 ");
        RegexUtil.optNotBlankStrOpt(param.getWorkTypeIds()).ifPresent(e -> whereString.append(" and wt.id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(param.getStartDateSearch()).ifPresent(e -> whereString.append(" and (to_date(pwc.occur_time::text, '" + SqlConstant.SQL_DATE_FMT + "') >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') or to_date(pwc.deadline_time::text, '" + SqlConstant.SQL_DATE_FMT + "') >=to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') )"));
        RegexUtil.optNotBlankStrOpt(param.getEndDateSearch()).ifPresent(e -> whereString.append(" and (to_date(pwc.occur_time::text, '" + SqlConstant.SQL_DATE_FMT + "') <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') or to_date(pwc.deadline_time::text, '" + SqlConstant.SQL_DATE_FMT + "') <=to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') )"));
        RegexUtil.optNotBlankStrOpt(param.getStatus()).ifPresent(e -> whereString.append(" and pwc.status =").append(Integer.valueOf(e)).append(" "));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(pwc.title) like ").append(e).append("or lower(pwc.plan_code) like ").append(e).append(" or lower(a.asset_name) like").append(e).append(")"));
        RegexUtil.optNotBlankStrOpt(param.getPositionCode()).ifPresent(e -> whereString.append(" and pwc.position_code in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        return whereString.toString();
    }

    /**
     * 导出选中维保计划
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public ModelAndView exportPlanWorkCalendar(MethodParam methodParam, SearchParam param) {
        Map<String, Object> map = new HashMap<>();
        if (RegexUtil.optIsPresentStr(param.getCalendar_codes())) {
            String[] calendar_codes = param.getCalendar_codes().split(",");
            List<Map<String, Object>> planScheduleList = planWorkCalendarMapper.findPlanWorkCalendarListByCodes(methodParam.getSchemaName(), calendar_codes);
            map.put("rows", planScheduleList);
            RegexUtil.trueExp(RegexUtil.optNotNullListOrNew (planScheduleList).size() > 1000, LangConstant.MSG_J);
            List<Map<String, Object>> workPropertyList = planWorkMapper.findPlanWorkPropertyByPlanCodes(methodParam.getSchemaName(), planScheduleList);
            map.put("cows", workPropertyList);
            //获取当前维保行事历的日志
            List<Map<String, Object>> log = new ArrayList<>();
            for (Map<String, Object> planSchedule : planScheduleList) {
                List<Map<String, Object>> list = logsService.getLog(methodParam, SensConstant.BUSINESS_NO_10006, planSchedule.get("work_calendar_code").toString());
                for (Map<String, Object> m : list) {
                    m.put("plan_code", planSchedule.get("plan_code"));
                    m.put("work_calendar_code", planSchedule.get("work_calendar_code"));
                }
                log.addAll(list);
            }
            for (Map<String, Object> log_a : log) {
                JSONArray remark = JSONArray.fromObject(log_a.get("remark"));
                List<String> logArr = new ArrayList();
                for (int n = 0; n < remark.size(); n++) {
                    JSONArray log_arr = JSONArray.fromObject(remark.get(n));
                    for (int i = 0; i < log_arr.size(); i++) {
                        JSONObject log_tr = JSONObject.fromObject(log_arr.get(i));
                        JSONArray params = JSONArray.fromObject(log_tr.get("params"));
                        String[] paramsMap = new String[params.size()];
                        //多语言可以转换
                        for (int l = 0; l < params.size(); l++) {
                            paramsMap[l] = commonUtilService.getLanguageInfoBySysLang(methodParam, params.get(l).toString());
                        }
                        String key = commonUtilService.getLanguageInfoBySysLang(methodParam, log_tr.getString("key"));
                        logArr.add(commonUtilService.stringFormat(paramsMap, key));
                    }
                }
                log_a.put("remark", StringUtils.strip(logArr.toString(), "[]"));
            }
            map.put("dows", log);
            map.put("methodParam", methodParam);
        } else {
            map.put("rows", "");
        }
        PlanWorkCalendarDataExportView excelView = new PlanWorkCalendarDataExportView();
        return new ModelAndView(excelView, map);
    }


    /**
     * 导出全部选中维保计划
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public ModelAndView exportAllPlanWorkCalendar(MethodParam methodParam, SearchParam param) {
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getPlanWorkCalendarWhereString(methodParam, param);
        int total = planWorkCalendarMapper.findCountPlanWorkCalendarList(schemaName, searchWord, methodParam.getUserId()); // 获取维保行事历总数
        Map<String, Object> map = new HashMap<>();
        if (total < 1) {
            map.put("rows", null);
        } else {
            searchWord += " order by pwc.status asc,pwc.create_time asc ";
            List<Map<String, Object>> planScheduleList = null;
            planScheduleList = planWorkCalendarMapper.findPlanWorkCalendarList(schemaName, searchWord, methodParam.getUserId());
            map.put("rows", planScheduleList);
            RegexUtil.trueExp(RegexUtil.optNotNullListOrNew (planScheduleList).size() > 1000, LangConstant.MSG_J);
            List<Map<String, Object>> workPropertyList = planWorkMapper.findPlanWorkPropertyByPlanCodes(schemaName, planScheduleList);
            map.put("cows", workPropertyList);
            //获取当前维保行事历的日志
            List<Map<String, Object>> log = new ArrayList<>();
            for (Map<String, Object> planSchedule : planScheduleList) {
                List<Map<String, Object>> list = logsService.getLog(methodParam, SensConstant.BUSINESS_NO_10006, planSchedule.get("work_calendar_code").toString());
                for (Map<String, Object> m : list) {
                    m.put("plan_code", planSchedule.get("plan_code"));
                    m.put("work_calendar_code", planSchedule.get("work_calendar_code"));
                }
                log.addAll(list);
            }
            for (Map<String, Object> log_a : log) {
                JSONArray remark = JSONArray.fromObject(log_a.get("remark"));
                List<String> logArr = new ArrayList();
                for (int n = 0; n < remark.size(); n++) {
                    JSONArray log_arr = JSONArray.fromObject(remark.get(n));
                    for (int i = 0; i < log_arr.size(); i++) {
                        JSONObject log_tr = JSONObject.fromObject(log_arr.get(i));
                        JSONArray params = JSONArray.fromObject(log_tr.get("params"));
                        String[] paramsMap = new String[params.size()];
                        //多语言可以转换
                        for (int l = 0; l < params.size(); l++) {
                            paramsMap[l] = commonUtilService.getLanguageInfoBySysLang(methodParam, params.get(l).toString());
                        }
                        String key = commonUtilService.getLanguageInfoBySysLang(methodParam, log_tr.getString("key"));
                        logArr.add(commonUtilService.stringFormat(paramsMap, key));
                    }
                }
                log_a.put("remark", StringUtils.strip(logArr.toString(), "[]"));
            }
            map.put("dows", log);
            map.put("methodParam", methodParam);
        }
        PlanWorkCalendarDataExportView excelView = new PlanWorkCalendarDataExportView();
        return new ModelAndView(excelView, map);
    }

    public Map<String, Object> findPlanWorkCalendarInfoByCode(String schemaName, String work_calendar_code, String userLang, Long companyId) {
        Map<String, Object> planWorkCalendarMap = RegexUtil.optMapOrNew(planWorkCalendarMapper.findPlanWorkCalendarInfoByCode(schemaName, work_calendar_code));
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentMap(result)) {
            JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
            if (RegexUtil.optIsPresentMap(planWorkCalendarMap)) {
                if (dd.containsKey(planWorkCalendarMap.get("duty_type_name"))) {
                    String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(planWorkCalendarMap.get("duty_type_name"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        planWorkCalendarMap.put("duty_type_name", value);
                    }
                }

                if (dd.containsKey(planWorkCalendarMap.get("self_or_out_name"))) {
                    String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(planWorkCalendarMap.get("self_or_out_name"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        planWorkCalendarMap.put("self_or_out_name", value);
                    }
                }

                if (dd.containsKey(planWorkCalendarMap.get("priority_level_name"))) {
                    String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(planWorkCalendarMap.get("priority_level_name"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        planWorkCalendarMap.put("priority_level_name", value);
                    }
                }

                if (dd.containsKey(planWorkCalendarMap.get("auto_generate_bill_name"))) {
                    String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(planWorkCalendarMap.get("auto_generate_bill_name"))).get(userLang));
                    if (RegexUtil.optIsPresentStr(value)) {
                        planWorkCalendarMap.put("auto_generate_bill_name", value);
                    }
                }
            }
        }
        return planWorkCalendarMap;
    }
}

