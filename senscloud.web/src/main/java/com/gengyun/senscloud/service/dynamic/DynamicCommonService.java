package com.gengyun.senscloud.service.dynamic;

import com.gengyun.senscloud.model.MethodParam;

import java.util.Map;

/**
 * 动态公共方法用接口
 */
public interface DynamicCommonService {

    /**
     * 取数据编号
     *
     * @param methodParam 入参
     * @param index       入参
     * @return 数据编号
     */
    String getDataCode(MethodParam methodParam, int index);

    /**
     * 取工单编号
     *
     * @param methodParam 入参
     * @return 工单编号
     */
    String getWorkCode(MethodParam methodParam);

    /**
     * 取工单明细编号
     *
     * @param methodParam 入参
     * @return 工单明细编号
     */
    String getSubWorkCode(MethodParam methodParam);

    /**
     * 取工单请求编号
     *
     * @param methodParam 入参
     * @return 工单请求编号
     */
    String getWorkRequestCode(MethodParam methodParam);

    /**
     * 取备件入库编号
     *
     * @param methodParam 入参
     * @return 备件入库编号
     */
    String getBomInStockCode(MethodParam methodParam);

    /**
     * 取备件报废编号
     *
     * @param methodParam 入参
     * @return 备件报废编号
     */
    String getBomDiscardCode(MethodParam methodParam);

    /**
     * 取设备报废编号
     *
     * @param methodParam 入参
     * @return 设备报废编号
     */
    String getAssetDiscardCode(MethodParam methodParam);

    /**
     * 取设备调拨编号
     *
     * @param methodParam 入参
     * @return 设备调拨编号
     */
    String getAssetTransferCode(MethodParam methodParam);

    /**
     * 取备件盘点明细编号
     *
     * @param methodParam 入参
     * @return 备件盘点明细编号
     */
    String getSubBomInventoryCode(MethodParam methodParam);

    /**
     * 取设备盘点明细编号
     *
     * @param methodParam 入参
     * @return 设备盘点明细编号
     */
    String getSubAssetInventoryCode(MethodParam methodParam);

    /**
     * 取用设备工单详情编号
     *
     * @param methodParam 入参
     * @return 用户注册编号
     */
    String getAssetSubWorkCode(MethodParam methodParam);

    /**
     * 取备件编号
     *
     * @param methodParam 入参
     * @return 备件编号
     */
    String getBomCode(MethodParam methodParam);

    /**
     * 获取用设备出库工单详情编号
     *
     * @param methodParam 入参
     * @return 设备出库工单详情编号
     */
    String getAssetOutPutWorkCode(MethodParam methodParam);

    /**
     * 获取用设备出库工单编号
     *
     * @param methodParam 入参
     * @return 设备出库工单编号
     */
    String getAssetOutPutSubWorkCode(MethodParam methodParam);

    /**
     * 获取用设备入库工单详情编号
     *
     * @param methodParam 入参
     * @return 设备入库工单详情编号
     */
    String getAssetInPutWorkCode(MethodParam methodParam);

    /**
     * 获取用设备入库工单编号
     *
     * @param methodParam 入参
     * @return 设备入库工单编号
     */
    String getAssetInPutSubWorkCode(MethodParam methodParam);

    /**
     * 获取用设备调拨工单详情编号
     *
     * @param methodParam 入参
     * @return 设备入库工单详情编号
     */
    String getAssetChangeWorkCode(MethodParam methodParam);

    /**
     * 获取用设备调拨工单编号
     *
     * @param methodParam 入参
     * @return 设备入库工单编号
     */
    String getAssetChangeSubWorkCode(MethodParam methodParam);

    /**
     * 获取用设备验收工单详情编号
     *
     * @param methodParam 入参
     * @return 设备入库工单详情编号
     */
    String getAssetAcceptWorkCode(MethodParam methodParam);

    /**
     * 获取设备验收工单编号
     *
     * @param methodParam 入参
     * @return 设备入库工单编号
     */
    String getAssetAcceptSubWorkCode(MethodParam methodParam);

    /**
     * 获取用备件入库工单详情编号
     *
     * @param methodParam 入参
     * @return 备件入库工单详情编号
     */
    String getBomInputWorkCode(MethodParam methodParam);

    /**
     * 获取备件入库工单编号
     *
     * @param methodParam 入参
     * @return 备件入库工单编号
     */
    String getBomInputSubWorkCode(MethodParam methodParam);

    /**
     * 获取用备件出库工单详情编号
     *
     * @param methodParam 入参
     * @return 备件出库工单详情编号
     */
    String getBomOutPutWorkCode(MethodParam methodParam);

    /**
     * 获取备件出库工单编号
     *
     * @param methodParam 入参
     * @return 备件出库工单编号
     */
    String getBomOutPutSubWorkCode(MethodParam methodParam);

    /**
     * 获取用备件采购需求工单详情编号
     *
     * @param methodParam 入参
     * @return 备件采购需求工单详情编号
     */
    String getBomPurchasingWorkCode(MethodParam methodParam);

    /**
     * 获取备件采购需求工单编号
     *
     * @param methodParam 入参
     * @return 备件采购需求工单编号
     */
    String getBomPurchasingSubWorkCode(MethodParam methodParam);

    /**
     * 获取生产作业单编码
     *
     * @param methodParam 入参
     * @return 生产作业单编码
     */
    String getProductTaskCode(MethodParam methodParam);
//
//    /**
//     * 获取工作流编号
//     *
//     * @param schemaName
//     * @param businessTypeId
//     * @return
//     */
//    String getWorkFlowId(String schemaName, String businessTypeId);
//
//    /**
//     * 处理map数据的字段类型，以便调用动态插入表格方法
//     *
//     * @param objectMap
//     */
//    void handleTableColumnTypes(Map<String, Object> objectMap);
//
//
//    /**
//     * 按用户取服务入口 上报工单|服务请求
//     *
//     * @param schema_name
//     * @param user_id
//     * @return
//     */
//    Map<String, Object> serviceEntry(String schema_name, String user_id);
//
//    /**
//     * 新增工单
//     *
//     * @param schemaName
//     * @param pageType
//     * @return
//     */
//    Map<String, Object> goToAddWork(String schemaName, String pageType);
//

    /**
     * 工单提交公共处理函数（工单及工单请求处理）
     *
     * @param schemaName
     * @param subWorkCode
     * @param worRequestCode
     * @param paramMap
     */
    void doWorkSubmitCommonWithRequest(String schemaName, String subWorkCode, String worRequestCode, Map<String, Object> paramMap);

//    /**
//     * 工单提交公共处理函数
//     *
//     * @param schemaName
//     * @param keyName
//     * @param tableType
//     * @param subWorkCode
//     * @param paramMap
//     */
//    boolean doWorkSubmitCommon(String schemaName, String keyName, int tableType, String subWorkCode, Map<String, Object> paramMap);
//
//    /**
//     * 解析前端数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    Map<String, Object> doAnalysisPageData(String schemaName, Map<String, Object> paramMap) throws Exception;
//

    /**
     * 流程操作人校验
     *
     * @param schemaName
     * @param account
     * @param subWorkCode
     * @throws Exception
     */
    void doCheckFlowInfo(String schemaName, String account, String subWorkCode);

//    /**
//     * 整合小程序回显字段及数据
//     *
//     * @param schemaName
//     * @param request
//     * @param account
//     * @param result
//     * @param template
//     * @param oldData
//     * @param isAssetScan
//     * @param isDistribute
//     * @param subWorkCode
//     */
//    void getPageInfo(String schemaName, HttpServletRequest request, String account, JSONObject result,
//                     MetadataWork template, Map<String, Object> oldData, Boolean isAssetScan,
//                     Boolean isDistribute, String subWorkCode);
}
