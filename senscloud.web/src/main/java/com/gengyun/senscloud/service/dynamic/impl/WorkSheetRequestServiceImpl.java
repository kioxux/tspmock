package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.WorkSheetRequestService;
import org.springframework.stereotype.Service;

/**
 * 工单请求
 * User: sps
 * Date: 2019/05/15
 * Time: 下午15:20
 */
@Service
public class WorkSheetRequestServiceImpl implements WorkSheetRequestService {
//    private static final Logger logger = LoggerFactory.getLogger(WorkSheetRequestServiceImpl.class);
//    @Autowired
//    WorkSheetRequestMapper wsqMapper;
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//    @Autowired
//    AuthService authService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    /**
//     * 获取工单请求列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findWsqList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String searchStatus = request.getParameter("status");
//        String facilities = request.getParameter("facilities");
//        //String positions = request.getParameter("positions");//获取设备位置
//        String keyWord = request.getParameter("keyWord");
//        String beginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("beginTime");
//        String endTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("endTime");
//        String work_type_id = request.getParameter("type");
//        String asset_type = request.getParameter("asset_type");
//        String createBeginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("createBeginTime");
//        String createEndTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("createEndTime");
//        User user = AuthService.getLoginUser(request);
//        String workRequestCodeSearch = request.getParameter("workRequestCodeSearch");
//        if (RegexUtil.isNull(workRequestCodeSearch)) {
//            workRequestCodeSearch = (String) request.getAttribute("work_code");
//        }
//        if (RegexUtil.isNotNull(workRequestCodeSearch)) {
//            pageSize = 1;
//            pageNumber = 1;
//            facilities = null;
//
//        }
//        List<Map<String, Object>> workDataList = null;
//        List<String> roles = null;
//        roles = new ArrayList<>(user.getRoles().keySet());
//        String condition = "";
//        //根据人员的角色，判断获取值的范围
//        String account = user.getAccount();
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "repair"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "maintain"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "inspection"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "spotcheck"));
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                    isAllFacility = true;
//                } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                    isSelfFacility = true;
//                }
//            }
//        }
//        condition += SqlConditionUtil.getWsqFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//        // condition += SqlConditionUtil.getWorkOrgCondition(isAllFacility,isSelfFacility,"aof,apf",condition, facilities, user.getFacilities());组织权限
//        // condition += SqlConditionUtil.getWorkPositionCondition(isAllFacility,isSelfFacility,"w",condition, positions, user.getPositions());位置权限
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (upper(w.work_request_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%"
//                    + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or u.username like '%"
//                    + keyWord + "%' or w.create_user_account like '%" + keyWord + "%' )";
//        }
//        if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("-1")) {
//            condition += " and (w.status ='" + searchStatus + "' ) ";
//        }
//        String[] assetTypes = null;
//        if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
////                condition += " and ma.id in(" + asset_type + ") ";
//            assetTypes = asset_type.split(",");
//        }
//        String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//        if (asset_type_word != null && !asset_type_word.isEmpty()) {
//            if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//                condition += " and ma.id in (" + asset_type_word + ") ";
//            } else {
//                condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//            }
//        }
//        if (work_type_id != null && !work_type_id.isEmpty() && !work_type_id.equals("-1")) {
//            condition += " and (w.work_type_id = " + work_type_id + " ) ";
//        }
////      if (org_type != null && !org_type.isEmpty() && !org_type.equals("-1")) {
////                condition += " and (ft.org_type =" + org_type + " ) ";
////      }
//        if (StringUtils.isNotBlank(beginTime)) {
//            condition += " and w.occur_time >='" + beginTime + "'";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            condition += " and w.occur_time <'" + endTime + "'";
//        }
//        if (StringUtils.isNotBlank(createBeginTime)) {
//            condition += " and w.create_time >='" + createBeginTime + "'";
//        }
//        if (StringUtils.isNotBlank(createEndTime)) {
//            condition += " and w.create_time <='" + createEndTime + "'";
//        }
//        if (null != workRequestCodeSearch && !"".equals(workRequestCodeSearch)) {
//            condition += " and w.work_request_code = '" + workRequestCodeSearch + "' ";
//        }
//        //condition+=" or w.create_user_account='" + account + "' or w2.receive_account='" + account + "' ";
//        int begin = pageSize * (pageNumber - 1);
//        workDataList = wsqMapper.getWsqList(schema_name, condition, pageSize, begin);
//        if (null != workRequestCodeSearch && !"".equals(workRequestCodeSearch) && (null == workDataList || 0 == workDataList.size())) {
//            return "";
//        }
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        if (workDataList != null && workDataList.size() > 0) {
//            List<String> subWorkCodes = new ArrayList<>();
//            workDataList.stream().forEach(data -> subWorkCodes.add((String) data.get("work_request_code")));
//            if (subWorkCodes.size() > 0)
//                subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, account, roles, false, "", "", "", pageSize, pageNumber);
//        }
//        for (Map<String, Object> wsqMap : workDataList) {
//            String work_request_code = (String) wsqMap.get("work_request_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(work_request_code);
//            if (task != null && null != task.getAssignee()) {
//                wsqMap.put("is_handle", task.getAssignee().equals(account));
//                continue;
//            }
//
//            // 草稿状态直接看是不是当前创建的用户
//            if (StatusConstant.DRAFT == (Integer) wsqMap.get("status")) {
//                wsqMap.put("is_handle", account.equals(wsqMap.get("create_user_account")));
//            }
//        }
//        int total = wsqMapper.getWsqListCount(schema_name, condition);
//        //时区转换处理
//        SCTimeZoneUtil.responseMapListDataHandler(workDataList);
//        result.put("rows", workDataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 根据主键查询工单请求数据（不含json串）
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryWsqInfoById(String schemaName, String workRequestCode) {
//        return wsqMapper.queryWsqInfoById(schemaName, workRequestCode);
//    }
//
//    /**
//     * 根据主键查询工单请求数据
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryWorkRequestById(String schemaName, String workRequestCode) {
//        Map<String, Object> workRequestInfo = wsqMapper.queryWorkRequestById(schemaName, workRequestCode);
//        if (null != workRequestInfo && workRequestInfo.size() > 0) {
//            HttpServletRequest request = HttpRequestUtils.getRequest();
//            User user = AuthService.getLoginUser(request);
//            user = commonUtilService.checkUser(schemaName, user, request);
//            boolean feeRight = pagePermissionService.getPermissionByKey(schemaName, user.getId(), "worksheet_request", "work_request_fee");
//            workRequestInfo.put("feeRight", feeRight);
//        }
//        return workRequestInfo;
//    }
//
//    /**
//     * 手动回滚数据
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @param status
//     */
//    @Override
//    public void doRollBackData(String schemaName, String workRequestCode, Object status) {
//        logger.error(selectOptionService.getLanguageInfo(LangConstant.LOG_WORK_REQUEST_ERROR) + workRequestCode);//"工单请求信息保存时异常，状态恢复："
//        this.doUpdateWsqStatus(schemaName, workRequestCode, status);
//    }
//
//    /**
//     * 作废工单
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel cancelWsq(HttpServletRequest request) {
//        String result = this.findWsqList(request); // 权限判断
//        if ("".equals(result)) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String workRequestCode = request.getParameter("workRequestCode");
//        boolean deleteFlow = workflowService.deleteInstancesBySubWorkCode(schemaName, workRequestCode, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));
//        if (!deleteFlow) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.PROCESS_DEL_ERROR));//流程删除出现错误
//        }
//
//        // 保存工单主表，更新状态
//        Map<String, Object> info = new HashMap<String, Object>();
//        info.put("work_request_code", workRequestCode);
//        info.put("int@status", "status");
//        info.put("status", StatusConstant.CANCEL);
//        int i = workSheetHandleMapper.update(schemaName, info, "_sc_work_request", "work_request_code"); // 主表
//        if (i > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_OBSOLETE_SUCCESS));//作废成功
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CANCEL_FAIL));
//    }
//
////    /**
////     * 根据主键删除工单请求数据
////     *
////     * @param schemaName
////     * @param workRequestCode
////     */
////    @Override
////    public void deleteByWsqCode(String schemaName, String workRequestCode) {
////        wsqMapper.deleteByWsqCode(schemaName, workRequestCode); // 删除原数据
////    }
//
//    /**
//     * 更新请求单状态
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @param status
//     */
//    @Override
//    public void doUpdateWsqStatus(String schemaName, String workRequestCode, Object status) {
//        if (null != workRequestCode && !"".equals(workRequestCode)) {
//            Map<String, Object> info = new HashMap<String, Object>();
//            info.put("work_request_code", workRequestCode);
//            info.put("int@status", "status");
//            info.put("status", status);
//            workSheetHandleMapper.update(schemaName, info, "_sc_work_request", "work_request_code"); // 主表
//        }
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String result = this.findWsqList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String workRequestCode = request.getParameter("work_request_code");
//        Map<String, Object> wsqDetail = wsqMapper.queryWsqInfoById(schemaName, workRequestCode);
//        int workTypeId = ((Long) wsqDetail.get("work_type_id")).intValue(); // 工单类型
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        permissionInfo.put("todo_list_distribute", "isDistribute");
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//
//        Integer status = (Integer) wsqDetail.get("status"); // 工单类型
//        if (StatusConstant.TO_BE_CONFIRM != status) {
//            mav.getModel().put("isDistribute", false);
//        }
////        String workRequestFlow = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_info", Constants.WORK_REQUEST_BUSINESS_TYPE, "code");
//        String msgNone = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
////        if (RegexUtil.isNotNull(workRequestFlow) && !msgNone.equals(workRequestFlow)) {
////            workTypeId = Integer.valueOf(workRequestFlow);
////        }
//        String whereString = "and(work_type_id=" + workTypeId + ")";
//        whereString += "and(template_type=4)"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        if (RegexUtil.isNull(workFormKey) || msgNone.equals(workFormKey)) {
//            whereString = "and(template_type=4)"; // 工单请求的详情
//            workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        }
//        mav.addObject("workFormKey", workFormKey);
//        mav.addObject("fpPrefix", Constants.FP_PREFIX);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/worksheet_request/findSingleWsqInfo")
//                .addObject("workCode", workRequestCode)
//                .addObject("workTypeId", workTypeId)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", workRequestCode)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("is_main", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 工单作废关闭服务单
//     *
//     * @param schemaName
//     * @param workCode
//     * @param account
//     */
//    public void cancelWsqByWork(String schemaName, String workCode, String account) {
//        Map<String, Object> workInfo = workSheetHandleService.queryWorkByWorkCode(schemaName, workCode);
//        String workRequestCode = (String) workInfo.get("work_request_code");
//        if (null != workRequestCode && !"".equals(workRequestCode)) {
//            workflowService.deleteInstancesBySubWorkCode(schemaName, workRequestCode, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));
//            this.doUpdateWsqStatus(schemaName, workRequestCode, StatusConstant.COMPLETED_CLOSE);
//            workProcessService.saveWorkProccess(schemaName, workRequestCode, StatusConstant.COMPLETED_CLOSE, account, Constants.PROCESS_CANCEL_TASK); // 工单进度记录
//            String businessTypeId = selectOptionService.queryWorkTypeById(schemaName, String.valueOf((Integer) workInfo.get("work_type_id"))).get("business_type_id").toString(); // 业务类型
//            String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//            logService.AddLog(schemaName, pageType, workRequestCode, selectOptionService.getLanguageInfo(LangConstant.LOG_WORK_CANCEL), account); // 记录历史
//        }
//    }
//
//    /**
//     * 取工单请求业务号
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public String selectWqBusinessInfo(String schemaName) {
//        return wsqMapper.selectWqBusinessInfo(schemaName);
//    }
//
//    /**
//     * 移动端查询工单请求
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> findWsqServiceList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = request.getAttribute("schema_name").toString();//移动端获得企业schema_name
//        User user = (User) request.getAttribute("user");//移动端获得账号信息
//        Map<String, Object> result = new HashMap<String, Object>();
//        String searchStatus = request.getParameter("status");
//        String facilities = request.getParameter("facilities");
//        //String positions = request.getParameter("positions");//获取设备位置
//        //String org_type = request.getParameter("org_type");//获取组织类型
//        String keyWord = request.getParameter("keyWord");
//        String beginTime = SCTimeZoneUtil.requestParamHandler("beginTime", "00:00:00");
//        String endTime = SCTimeZoneUtil.requestParamHandler("endTime", "23:59:59");
//        String work_type_id = request.getParameter("type");
//        String asset_type = request.getParameter("asset_type");
//        String createBeginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("createBeginTime");
//        String createEndTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("createEndTime");
//        String workRequestCodeSearch = request.getParameter("workRequestCodeSearch");
//        if (RegexUtil.isNull(workRequestCodeSearch)) {
//            workRequestCodeSearch = (String) request.getAttribute("work_code");
//        }
//        if (RegexUtil.isNotNull(workRequestCodeSearch)) {
//            pageSize = 1;
//            pageNumber = 1;
//            facilities = null;
//
//        }
//        List<Map<String, Object>> workDataList = null;
//        List<String> roles = null;
//        roles = new ArrayList<>(user.getRoles().keySet());
//        String condition = "";
//        //根据人员的角色，判断获取值的范围
//        String account = user.getAccount();
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "repair"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "maintain"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "inspection"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "spotcheck"));
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                    isAllFacility = true;
//                } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                    isSelfFacility = true;
//                }
//            }
//        }
//        condition += SqlConditionUtil.getWsqFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//        // condition += SqlConditionUtil.getWorkOrgCondition("aof,apf,w",condition, facilities, user.getFacilities());组织权限
//        // condition += SqlConditionUtil.getWorkPositionCondition("aof,apf,w",condition, positions, user.getPositions());位置权限
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (upper(w.work_request_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%"
//                    + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or u.username like '%"
//                    + keyWord + "%' or w.create_user_account like '%" + keyWord + "%' )";
//        }
//        if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("-1")) {
//            condition += " and (w.status ='" + searchStatus + "' ) ";
//        }
//        String[] assetTypes = null;
//        if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//            assetTypes = asset_type.split(",");
//        }
//        String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//        if (asset_type_word != null && !asset_type_word.isEmpty()) {
//            if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//                condition += " and ma.id in (" + asset_type_word + ") ";
//            } else {
//                condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//            }
//        }
//        if (work_type_id != null && !work_type_id.isEmpty() && !work_type_id.equals("-1")) {
//            condition += " and (w.work_type_id = " + work_type_id + " ) ";
//        }
////      if (org_type != null && !org_type.isEmpty() && !org_type.equals("-1")) {
////                condition += " and (ft.org_type =" + org_type + " ) ";
////      }
//        if (StringUtils.isNotBlank(beginTime)) {
//            condition += " and w.occur_time >='" + beginTime + "'";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            condition += " and w.occur_time <'" + endTime + "'";
//        }
//        if (StringUtils.isNotBlank(createBeginTime)) {
//            condition += " and w.create_time >='" + createBeginTime + "'";
//        }
//        if (StringUtils.isNotBlank(createEndTime)) {
//            condition += " and w.create_time <='" + createEndTime + "'";
//        }
//        if (null != workRequestCodeSearch && !"".equals(workRequestCodeSearch)) {
//            condition += " and w.work_request_code = '" + workRequestCodeSearch + "' ";
//        }
//        //没有权限只查看自己创建，或自己做的工单
//        //condition+=" or w.create_user_account='" + account + "' or w2.receive_account='" + account + "' ";
//        int begin = pageSize * (pageNumber - 1);
//        workDataList = wsqMapper.getWsqList(schema_name, condition, pageSize, begin);
//        if (null != workRequestCodeSearch && !"".equals(workRequestCodeSearch) && (null == workDataList || 0 == workDataList.size())) {
//            return workDataList;
//        }
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        if (workDataList != null && workDataList.size() > 0) {
//            List<String> subWorkCodes = new ArrayList<>();
//            workDataList.stream().forEach(data -> subWorkCodes.add((String) data.get("work_request_code")));
//            if (subWorkCodes.size() > 0)
//                subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, account, roles, false, "", "", "", pageSize, pageNumber);
//        }
//        for (Map<String, Object> wsqMap : workDataList) {
//            String work_request_code = (String) wsqMap.get("work_request_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(work_request_code);
//            if (task != null && null != task.getAssignee()) {
//                wsqMap.put("is_handle", task.getAssignee().equals(account));
//                continue;
//            }
//
//            // 草稿状态直接看是不是当前创建的用户
//            if (StatusConstant.DRAFT == (Integer) wsqMap.get("status")) {
//                wsqMap.put("is_handle", account.equals(wsqMap.get("create_user_account")));
//            }
//        }
////        int total = wsqMapper.getWsqListCount(schema_name, condition);
////        result.put("rows", workDataList);
////        result.put("total", total);
//        //时区转换处理
//        SCTimeZoneUtil.responseMapListDataHandler(workDataList, new String[]{"create_time", "occur_time", "deadline_time"});
//        return workDataList;
//    }
//
//    /**
//     * 移动端查询工单请求数量
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public int findWsqServiceListCount(HttpServletRequest request) {
//        String schema_name = request.getAttribute("schema_name").toString();//移动端获得企业schema_name
//        User user = (User) request.getAttribute("user");//移动端获得账号信息
//        String searchStatus = request.getParameter("status");
//        String facilities = request.getParameter("facilities");
//        String keyWord = request.getParameter("keyWord");
//        String beginTime = SCTimeZoneUtil.requestParamHandler("beginTime", "00:00:00");
//        String endTime = SCTimeZoneUtil.requestParamHandler("endTime", "23:59:59");
//        String work_type_id = request.getParameter("type");
//        String asset_type = request.getParameter("asset_type");
//        String createBeginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("createBeginTime");
//        String createEndTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("createEndTime");
//        String workRequestCodeSearch = request.getParameter("workRequestCodeSearch");
//        if (RegexUtil.isNull(workRequestCodeSearch)) {
//            workRequestCodeSearch = (String) request.getAttribute("work_code");
//        }
//        if (RegexUtil.isNotNull(workRequestCodeSearch)) {
//            facilities = null;
//        }
//        String condition = "";
//        //根据人员的角色，判断获取值的范围
//        String account = user.getAccount();
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "repair"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "maintain"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "inspection"));
//        userFunctionList.addAll(userService.getUserFunctionPermission(schema_name, user.getId(), "spotcheck"));
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                    isAllFacility = true;
//                } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                    isSelfFacility = true;
//                }
//            }
//        }
//        condition += SqlConditionUtil.getWsqFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (upper(w.work_request_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%"
//                    + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or u.username like '%"
//                    + keyWord + "%' or w.create_user_account like '%" + keyWord + "%' )";
//        }
//        if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("-1")) {
//            condition += " and (w.status ='" + searchStatus + "' ) ";
//        }
//        String[] assetTypes = null;
//        if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//            assetTypes = asset_type.split(",");
//        }
//        String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//        if (asset_type_word != null && !asset_type_word.isEmpty()) {
//            if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//                condition += " and ma.id in (" + asset_type_word + ") ";
//            } else {
//                condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//            }
//        }
//        if (work_type_id != null && !work_type_id.isEmpty() && !work_type_id.equals("-1")) {
//            condition += " and (w.work_type_id = " + work_type_id + " ) ";
//        }
//        if (StringUtils.isNotBlank(beginTime)) {
//            condition += " and w.occur_time >='" + beginTime + "'";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            condition += " and w.occur_time <'" + endTime + "'";
//        }
//        if (StringUtils.isNotBlank(createBeginTime)) {
//            condition += " and w.create_time >='" + createBeginTime + "'";
//        }
//        if (StringUtils.isNotBlank(createEndTime)) {
//            condition += " and w.create_time <='" + createEndTime + "'";
//        }
//        if (null != workRequestCodeSearch && !"".equals(workRequestCodeSearch)) {
//            condition += " and w.work_request_code = '" + workRequestCodeSearch + "' ";
//        }
//        return wsqMapper.getWsqListCount(schema_name, condition);
//    }
//
//
//    @Override
//    public Map<String, Object> getWorksheetRequestDetail(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String workRequestCode = request.getParameter("work_request_code");
//        Map<String, Object> wsqDetail = wsqMapper.queryWsqInfoById(schemaName, workRequestCode);
//        int workTypeId = ((Long) wsqDetail.get("work_type_id")).intValue(); // 工单类型
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        permissionInfo.put("todo_list_distribute", "isDistribute");
//        Map<String, Object> mav = new HashMap<String, Object>();
//        Integer status = (Integer) wsqDetail.get("status"); // 工单类型
//        if (StatusConstant.TO_BE_CONFIRM != status) {
//            mav.put("isDistribute", false);
//        }
//        String msgNone = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
//        String whereString = "and(work_type_id=" + workTypeId + ")";
//        whereString += "and(template_type=4)"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        if (RegexUtil.isNull(workFormKey) || msgNone.equals(workFormKey)) {
//            whereString = "and(template_type=4)"; // 工单请求的详情
//            workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        }
//
//        mav.put("workFormKey", workFormKey);
//        mav.put("fpPrefix", Constants.FP_PREFIX);
//        mav.put("businessUrl", (Object) "/worksheet_request/findSingleWsqInfo");
//        mav.put("workCode", workRequestCode);
//        mav.put("workTypeId", workTypeId);
//        mav.put("HandleFlag", false);
//        mav.put("DetailFlag", true);
//        mav.put("EditFlag", false);
//        mav.put("finished_time_interval", null);
//        mav.put("arrive_time_interval", null);
//        mav.put("subWorkCode", workRequestCode);
//        mav.put("flow_id", null);
//        mav.put("do_flow_key", null);
//        mav.put("actionUrl", null);
//        mav.put("is_main", null);
//        mav.put("subList", "[]");
//        return mav;
//    }
}
