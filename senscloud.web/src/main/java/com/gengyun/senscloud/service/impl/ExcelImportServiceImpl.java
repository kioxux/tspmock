package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.ExcelImportService;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Excel导入处理
 */
@Service
public class ExcelImportServiceImpl implements ExcelImportService {
    private static final Logger logger = LoggerFactory.getLogger(ExcelImportServiceImpl.class);
    @Resource
    LogsService logService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    ExportService exportService;

//    /**
//     * 导入处理
//     *
//     * @param file 导入文件
//     * @return 返回错误文件码
//     */
//    public String doImport(MethodParam methodParam, MultipartFile file) {
////        RegexUtil.optNotNullOrExp(file, LangConstant.TEXT_CHOOSE_A); // 请选择文件
//        String fileName = file.getOriginalFilename();
//        if (!(fileName.contains(".xls") || fileName.contains(".xlsx"))) {
////            throw new SenscloudException(LangConstant.IMPORT_DATA_TEMPLATE_FORMAT_WRONG); // 导入数据模板格式不正确！
//        }
//        List<Map<String, Object>> logList = new ArrayList<>();
//        FileInputStream is = null;
//        String importType = Constants.EXCEL_IMORT_TYPE_BIS;
//        try {
//            String path = HttpRequestUtils.getSession().getServletContext().getRealPath("/") + "upload";
//            File targetFile = new File(path, fileName);
//            File targetPath = new File(path);
//            if (!targetPath.exists()) {
//                targetPath.mkdirs();
//            }
//            try {
//                file.transferTo(targetFile);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            boolean updateIfExist = RegexUtil.optEqualsOpt(HttpRequestUtils.getRequest().getParameter("update_if_exist"), "true").isPresent();
//            is = new FileInputStream(targetFile);
//            BisExcelListener checkLst = new BisExcelListener(methodParam, "check", logList);
//            EasyExcel.read(targetFile, BisExcelMode.class, checkLst).doReadAll();
//            if (logList.size() == 0) {
//                BisExcelListener saveLst = new BisExcelListener(methodParam, "save", logList);
//                EasyExcel.read(targetFile, BisExcelMode.class, saveLst).doReadAll();
//                if (logList.size() == 0) {
//                    String logSuc = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TEXT_AG) + "：" + saveLst.getCount(); // 导入成功了{d}条
//                    logService.newLog(methodParam, importType, fileName, logSuc); // 记录历史
//                }
//            }
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
//        } finally {
//            try {
//                if (null != is) {
//                    is.close();
//                }
//            } catch (Exception isExp) {
//                isExp.printStackTrace();
//            }
//        }
//        if (RegexUtil.optIsPresentList(logList)) {
//            return exportService.doDownloadLogInfo(methodParam, logList, importType);
//        }
//        return null;
//    }
}
