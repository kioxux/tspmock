package com.gengyun.senscloud.service.dynamic;

/**
 * 功能：工单进度记录表Service
 * Created by Dong wudang on 2018/12/14.
 */
public interface WorkProcessService {
    /**
     * 功能：记录工单进度
     *
     * @param sub_work_code   工单编号
     * @param status          状态
     * @param operate_account 操作人账号
     * @param task_id         流程节点编号
     * @return
     */
    Boolean saveWorkProccess(String schema_name, String sub_work_code, int status, String operate_account, String task_id);

    /**
     * 按节点号取操作人
     *
     * @param schemaName  入参
     * @param subWorkCode 入参
     * @param nodeId      入参
     * @return 操作人
     */
    String getOperateUserIdByNodeId(String schemaName, String subWorkCode, String nodeId);
}
