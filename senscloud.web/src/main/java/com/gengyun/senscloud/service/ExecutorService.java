package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * ExecutorService interface 派案策略接口
 *
 * @author Zys
 * @date 2020/03/19
 */
public interface ExecutorService {


    /**
     * 查找人员
     *
     * @param methodParam 入参
     * @param paramMap 入参
     * @return
     */
    List<Map<String,Object>> getConditionalUsers(MethodParam methodParam, Map<String, String> paramMap);


    /**
     * 按工单池查找操作人-接受人
     *
     * @param methodParam 入参
     * @param paramMap 入参
     * @return
     */
    List<Map<String,Object>> getExecutorsByPool(MethodParam methodParam, Map<String, String> paramMap);

    /**
     * 按角色查找操作人-接受人
     *
     * @param methodParam 入参
     * @param paramMap 入参
     * @return
     */
    List<Map<String,Object>> getExecutorsByRoles(MethodParam methodParam, Map<String, String> paramMap);
}
