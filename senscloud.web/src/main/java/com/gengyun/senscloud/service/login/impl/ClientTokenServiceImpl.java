package com.gengyun.senscloud.service.login.impl;

import com.gengyun.senscloud.entity.LoginLogEntity;
import com.gengyun.senscloud.mapper.LoginLogMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.login.ClientTokenService;
import com.gengyun.senscloud.util.RegexUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 登录token处理
 * User: Wudang Dong
 * Date: 2019/07/15
 * Time: 上午11:42
 */
@Service
public class ClientTokenServiceImpl implements ClientTokenService {
    @Resource
    LoginLogMapper loginLogMapper;

    /**
     * 新增登录记录
     *
     * @param methodParam 系统参数
     * @param loginLog    登录记录
     * @return 成功数量
     */
    @Override
    public int newLoginLog(MethodParam methodParam, LoginLogEntity loginLog) {
        return loginLogMapper.insertLoginLog(methodParam.getSchemaName(), loginLog);
    }

    /**
     * token验证
     *
     * @param schemaName 企业库
     * @param clientName 客户端
     * @param token      token
     * @return 数量
     */
    @Override
    public int getTokenCacheCount(String schemaName, String clientName, String token) {
        return loginLogMapper.countTokenByInfo(schemaName, clientName, token);
    }

    /**
     * 根据token查找用户账号
     *
     * @param methodParam 系统参数
     * @return 账号
     */
    @Override
    public String getLoginAccountByToken(MethodParam methodParam) {
        return RegexUtil.optStrOrBlank(loginLogMapper.findAccountByToken(methodParam.getSchemaName(), methodParam.getClientName(), methodParam.getToken()));
    }
}
