package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomInventoryService;
import org.springframework.stereotype.Service;

/**
 * 备件盘点
 */
@Service
public class BomInventoryServiceImpl implements BomInventoryService {
//
//    private static final Logger logger = LoggerFactory.getLogger(BomInventoryServiceImpl.class);
//
//    @Autowired
//    private BomInventoryMapper bomInventoryMapper;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private CommonUtilService commonUtilService;
//
//    @Autowired
//    private DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    private WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    private LogsService logService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    @Autowired
//    private WorkProcessService workProcessService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private WorkSheetHandleMapper workSheetHandleMapper;
//
//    @Autowired
//    private BomStockListMapper bomStockListMapper;
//
//    @Autowired
//    private PagePermissionService pagePermissionService;
//
//    @Autowired
//    private BomInStockService bomInStockService;
//
//    @Autowired
//    private BomDiacardService bomDiacardService;
//
//    @Autowired
//    private StockMapper stockMapper;
//
//    @Autowired
//    private MetadataWorkService metadataWorkService;
//
//    @Autowired
//    private BomMapper bomMapper;
//
//    @Autowired
//    private MessageService messageService;
//
//    /**
//     * 根据code查询备件盘点数据
//     *
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryBomInventoryByCode(String schemaName, String inventoryCode) {
//        return bomInventoryMapper.queryBomInventoryByCode(schemaName, inventoryCode);
//    }
//
//    /**
//     * 根据code查询盘点位置数据
//     *
//     * @param schemaName
//     * @param subInventoryCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryBomInventoryStockByCode(String schemaName, String subInventoryCode) {
//        Map<String, Object> bomInventoryStock = bomInventoryMapper.queryBomInventoryStockByCode(schemaName, subInventoryCode);
//        Map<String, Object> bomInventory = bomInventoryMapper.queryBomInventoryByCode(schemaName, (String) bomInventoryStock.get("inventory_code"));
//        bomInventoryStock.put("inventory_name", bomInventory.get("inventory_name"));
//        bomInventoryStock.put("begin_time", bomInventory.get("begin_time"));
//        bomInventoryStock.put("deadline_time", bomInventory.get("deadline_time"));
//        return bomInventoryStock;
//    }
//
//    /**
//     * 查询备件盘点列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBomInventoryList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        //首页条件
//        String condition = "";
//        String startTime = request.getParameter("startTime");
//        String endTime = request.getParameter("endTime");
//        if (StringUtils.isNotBlank(startTime)) {
//            condition += " and bi.begin_time >= '" + startTime + " 00:00:00' ";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            try {
//                Calendar cl = Calendar.getInstance();
//                cl.setTime(new SimpleDateFormat(Constants.DATE_FMT);.parse(endTime));
//                cl.add(Calendar.DAY_OF_MONTH, 1);
//                condition += " and bi.begin_time <'" + new SimpleDateFormat(Constants.DATE_FMT);.format(cl.getTime()) + " 00:00:00' ";
//            } catch (ParseException e) {
//            }
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = bomInventoryMapper.getBomInventoryList(schema_name, condition, pageSize, begin);
//        int total = bomInventoryMapper.countBomInventoryList(schema_name, condition);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 备件盘点报废
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel invalidInventory(HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String inventoryCode = request.getParameter("inventoryCode");
//        Map<String, Object> map = bomInventoryMapper.queryBomInventoryByCode(schemaName, inventoryCode);
//        if (map == null || (Integer) map.get("status") == StatusConstant.COMPLETED || (Integer) map.get("status") == StatusConstant.CANCEL) {
//            ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_INVENTORY_INFO_ERROR));//备件盘点信息异常，报废失败
//        }
//        int result = bomInventoryMapper.updateBomInventoryStatus(schemaName, StatusConstant.CANCEL, inventoryCode, null);
//        if (result > 0) {
//            String account = user.getAccount();
//            String condition = " and inventory_code = '" + inventoryCode + "' ";
//            result = bomInventoryMapper.updateBomInventoryStockStatus(schemaName, StatusConstant.CANCEL, null, condition);
//            if (result > 0) {
//                //关闭流程(主流程以及所有子流程)
//                List<Map<String, Object>> stockList = bomInventoryMapper.queryBomInventoryStockListByCode(schemaName, inventoryCode);
//                if (stockList != null) {
//                    for (Map<String, Object> stock : stockList) {
//                        String sub_inventory_code = (String) stock.get("sub_inventory_code");
//                        boolean isSuccess = workflowService.deleteInstancesBySubWorkCode(schemaName, sub_inventory_code, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_CANCEL));//盘点作废
//                        if (!isSuccess) {
//                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_CANCEL_FLOW_ERROR));//盘点作废流程调用失败
//                        }
//                        logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), sub_inventory_code, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_CANCEL), account);//记录历史；盘点作废
//                    }
//                }
//                //关闭流程
//                boolean isSuccess = workflowService.deleteInstancesBySubWorkCode(schemaName, inventoryCode, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_CANCEL));//盘点作废
//                if (!isSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_CANCEL_FLOW_ERROR));//盘点作废流程调用失败
//                }
//                logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), inventoryCode, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_CANCEL), account);//记录历史；盘点作废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_DISCARD_SUCCESS));//报废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            }
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DISCARD_FAIL));//报废失败
//    }
//
//    /**
//     * 查询备件盘点位置列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findInventoryStockList(HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        String inventoryCode = request.getParameter("inventoryCode");
//        String bomStock = request.getParameter("bomStock");
//        String keyword = request.getParameter("keyword");
//        if (StringUtils.isBlank(inventoryCode))
//            return "{}";
//
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        //首页条件
//        String condition = "";
//        if (StringUtils.isNotBlank(bomStock)) {
//            String[] stockCodes = bomStock.split(",");
//            condition += " AND bs.stock_code in (";
//            for (String code : stockCodes) {
//                condition += "'" + code + "',";
//            }
//            condition = condition.substring(0, condition.length() - 1);
//            condition += ") ";
//        }
//        if (StringUtils.isNotBlank(keyword)) {
//            condition += " AND u.username LIKE '%" + keyword + "%' ";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = bomInventoryMapper.getBomInventoryStockList(schema_name, inventoryCode, condition, pageSize, begin);
//        int total = bomInventoryMapper.countBomInventoryStockList(schema_name, inventoryCode, condition);
//        //查询获取流程taskId、按钮权限等数据
//        handlerFlowData(schema_name, user.getAccount(), dataList);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("sub_inventory_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        //把taskId、权限数据补充到列表数据中去
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("sub_inventory_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//    /**
//     * 查询盘点备件列表
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public String findInventoryStockDetailList(HttpServletRequest request, String schema_name) {
//        String subInventoryCode = request.getParameter("subInventoryCode");
//        String keyword = request.getParameter("keyword");
//        String status = request.getParameter("status");//0、未盘点 1、在库 2、盈余 3、亏损
//        String stockCode = request.getParameter("stockCode");//库房编码
//        if (StringUtils.isBlank(subInventoryCode))
//            return "{}";
//
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        JSONObject result = new JSONObject();
//        //首页条件
//        String condition = String.format(" AND bs.stock_code = '%s' ", stockCode);
//        if (StringUtils.isNotBlank(keyword)) {
//            condition += " AND (u.username LIKE '%" + keyword + "%' OR b.bom_name LIKE '%" + keyword + "%') ";
//        }
//        if ("0".equals(status) || "1".equals(status) || "2".equals(status) || "3".equals(status)) {
//            condition += " AND sd.in_status = " + status + " ";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList = bomInventoryMapper.getBomInventoryStockDetailList(schema_name, subInventoryCode, condition, pageSize, begin);
//        int total = bomInventoryMapper.countBomInventoryStockDetailList(schema_name, subInventoryCode, condition);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询库房在库备件总数
//     *
//     * @param schemaName
//     * @param stockCodes
//     * @param bomTypes
//     * @return
//     */
//    @Override
//    public int countStockBomQuantity(String schemaName, String stockCodes, String bomTypes) {
//        StringBuffer stockCodeStr = new StringBuffer();
//
//        String[] stockCodeArray = stockCodes.split(",");
//        for (String code : stockCodeArray) {
//            if (stockCodeStr.length() > 0)
//                stockCodeStr.append(",");
//
//            stockCodeStr.append("'" + code + "'");
//        }
//        String condition = "";
//        if (StringUtils.isNotBlank(bomTypes)) {
//            condition = " AND b.type_id IN (" + bomTypes + ") ";
//        }
//        return bomInventoryMapper.countStockBomQuantity(schemaName, stockCodeStr.toString(), condition);
//    }
//
//    /**
//     * 启动盘点
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel addInventory(String schemaName, HttpServletRequest request, String processDefinitionId, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        int status = StatusConstant.PENDING; //默认待盘点状态
//        Timestamp createTime = new Timestamp(System.currentTimeMillis());
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schemaName, paramMap);
//        //时区转换处理
//        SCTimeZoneUtil.requestMapParamHandler(map_object);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> bom_inventory_stock_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_inventory_stock");
//        Map<String, Object> bom_inventory_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_inventory");
//        String inventoryCode = (String) bom_inventory_tmp.get("inventory_code");
//
//        //解析处理设备类型
//        Object bodyProperty = paramMap.get("body_property"); // 页面所有模板相关信息
//        JSONArray arrays = JSONArray.fromObject(bodyProperty);
//        String fieldCode = null;
//        String bomTypeIds = "";
//        for (Object object : arrays) {
//            net.sf.json.JSONObject data = net.sf.json.JSONObject.fromObject(object);
//            fieldCode = data.get("fieldCode").toString();
//            if ("bom_type_id".equals(fieldCode)) {
//                bomTypeIds = data.getString("fieldValue");
//                break;
//            }
//        }
//        String account = user.getAccount();
//        //解析处理库房
//        net.sf.json.JSONArray stockArrays = (net.sf.json.JSONArray) map_object.get("bomContent14");
//        Map<String, String> stockCodeToManageAccountMap = new HashMap<>();//库房code、管理人code关系映射
//        StringBuffer stockCodeStr = new StringBuffer();
//        if (do_flow_key != null && !do_flow_key.equals("")) {
//            for (int i = 0; i < stockArrays.size(); i++) {
//                net.sf.json.JSONObject stock = stockArrays.getJSONObject(i);
//                if (stockCodeStr.length() > 0)
//                    stockCodeStr.append(",");
//
//                stockCodeStr.append("'" + stock.getString("stock_code") + "'");
//                if (StringUtils.isNotBlank(stock.getString("manager_account")))
//                    stockCodeToManageAccountMap.put(stock.getString("stock_code"), stock.getString("manager_account"));
//            }
//            //查询库房是否已经在盘点中
//            int doingCount = bomInventoryMapper.countDoingBomInventoryStock(schemaName, stockCodeStr.toString());
//            if (doingCount > 0) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_REPEAT_INVENTORY));//库房已经在盘点中，不能重复启用
//            }
//            //提交启动盘点表单数据
//            if (do_flow_key.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//                if (!bom_inventory_tmp.containsKey("begin_time") || StringUtils.isBlank((String) bom_inventory_tmp.get("begin_time"))) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.START_DATE) + SensConstant.REQUIRED_MSG);//开始日期
//                }
//                if (!bom_inventory_tmp.containsKey("deadline_time") || StringUtils.isBlank((String) bom_inventory_tmp.get("deadline_time"))) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.CLOSING_DATE) + SensConstant.REQUIRED_MSG);//截止日期
//                }
//                SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT_SS);
//                bom_inventory_tmp.put("begin_time", new Timestamp(df.parse((String) bom_inventory_tmp.get("begin_time")).getTime()));
//                bom_inventory_tmp.put("deadline_time", new Timestamp(df.parse((String) bom_inventory_tmp.get("deadline_time")).getTime()));
//                bom_inventory_tmp.put("create_time", createTime);
//                bom_inventory_tmp.put("create_user_account", account);
//                bom_inventory_tmp.put("status", status);
//                bom_inventory_tmp.put("inventory_count", 0);
//                bom_inventory_tmp.put("record_count", 0);//更新备件盘点总备件数量
//                bom_inventory_tmp.put("balance_count", 0);
//                bom_inventory_stock_tmp.put("inventory_code", bom_inventory_tmp.get("inventory_code"));
//                bom_inventory_stock_tmp.put("create_time", createTime);
//                bom_inventory_stock_tmp.put("create_user_account", account);
//                bom_inventory_stock_tmp.put("inventory_count", 0);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//            }
//        }
//        //查询解析 盘点位置、盘点备件列表数据
//        boolean result = handlerBomInventoryStockDetail(schemaName, dataInfo, bom_inventory_tmp, bom_inventory_stock_tmp, inventoryCode, account, createTime, stockCodeStr.toString(), bomTypeIds, stockCodeToManageAccountMap);
//        if (!result)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_HAS_NO_BOM));//库房下没有查询到任何备件，无需盘点
//
//        dataInfo.put("_sc_bom_inventory", bom_inventory_tmp);
//        map_object.put("dataInfo", dataInfo);
//        try {
//            String flow_data = doSaveData(schemaName, map_object);//保存新建工单
//            //流程数据
//            Map map = new HashMap();
//            if (StringUtil.isNotEmpty(flow_data)) {
//                map.put("flow_data", flow_data);
//            }
//            map.put("status", status);
//            map.put("sub_work_code", inventoryCode);
//            map.put("workCode", inventoryCode);
//            map.put("do_flow_key", do_flow_key);
//            map.put("create_user_account", account);
//            WorkflowStartResult workflowStartResult = workflowService.startWithForm(schemaName, processDefinitionId, account, map);
//            if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_INVENTORY_FLOW_START_FAIL));//启动盘点流程启动失败
//            }
//            if (StringUtil.isNotEmpty(flow_data)) {
//                org.json.JSONArray flowDatas = new org.json.JSONArray(flow_data);
//                if(flowDatas.length() > 0){
//                    for(int i=0;i<flowDatas.length();i++){
//                        JSONObject flowData = flowDatas.getJSONObject(i);
//                        String receive_account = flowData.optString("receive_account");
//                        messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_35, flowData.optString("sub_work_code"),
//                                account, user.getUsername(), null, null); // 发送短信
//                    }
//                }
//            }
//            List<Map<String, Object>> inventoryStockList = (List<Map<String, Object>>) dataInfo.get("bom_inventory_stock_list_tmp");
//            for (Map<String, Object> inventoryStock : inventoryStockList) {
//                String subInventoryCode = (String) inventoryStock.get("sub_inventory_code");
//                logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), subInventoryCode, selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_INVENTORY_START), account);//记录历史;启动备件盘点
//            }
//            logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), inventoryCode, selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_INVENTORY_START), account);//记录历史；启动备件盘点
//            return ResponseModel.ok("ok");
//        } catch (Exception exp) {
//            throw new Exception(exp);
//        }
//    }
//
//    /**
//     * PC盘点
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel pcInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String inventoryCode = null;
//        String subInventoryCode = null;
//        String stockCode = null;
//        JSONArray bomArray = null;
//        Object bodyProperty = paramMap.get("body_property"); // 页面所有模板相关信息
//        JSONArray arrays = JSONArray.fromObject(bodyProperty);
//        String fieldCode = null;
//        for (Object object : arrays) {
//            net.sf.json.JSONObject data = net.sf.json.JSONObject.fromObject(object);
//            fieldCode = data.get("fieldCode").toString();
//            if ("bom_use".equals(fieldCode)) {
//                bomArray = (JSONArray)data.get("bomContent");
//            } else if ("inventory_code".equals(fieldCode)) {
//                inventoryCode = data.getString("fieldValue");
//            } else if ("sub_inventory_code".equals(fieldCode)) {
//                subInventoryCode = data.getString("fieldValue");
//            } else if ("stock_code".equals(fieldCode)) {
//                stockCode = data.getString("fieldValue");
//            }
//        }
//        if (bomArray == null || bomArray.size() == 0) {
//            return ResponseModel.ok("ok");
//        }
//        if (StringUtils.isBlank(inventoryCode) || StringUtils.isBlank(subInventoryCode) || StringUtils.isBlank(stockCode))
////        if (StringUtils.isBlank(inventoryCode) || StringUtils.isBlank(subInventoryCode) || StringUtils.isBlank(stockCode) || bomArray == null || bomArray.size() == 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PC_INVENTORY_FAIL));//PC盘点失败
//
//        //查询库房信息
//        StockData stockData = stockMapper.findStockByStockCode(schemaName, stockCode);
//        if (stockData == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_DATA_ERROR_INVENTORY_FAIL));//库房数据异常，PC盘点失败
//
//        Map<String, Object> stock = bomInventoryMapper.queryBomInventoryStockByCode(schemaName, subInventoryCode);
//        if (stock == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_DATA_ERROR_INVENTORY_FAIL));//库房数据异常，PC盘点失败
//
//        if (!user.getAccount().equals(stock.get("duty_man")))//只有登录账户是盘点位置表中的盘点人时，才能进行盘点操作
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_PERMISSION_INVENTORY));//当前登录账户没有盘点权限，PC盘点失败
//
//        Timestamp createTime = new Timestamp(System.currentTimeMillis());
//        BigDecimal totalCount = BigDecimal.ZERO;//新增总盘点数量
//        for (int i = 0; i < bomArray.size(); i++) {
//            net.sf.json.JSONObject assetJson = bomArray.getJSONObject(i);
//            if (!assetJson.containsKey("use_count") || StringUtils.isBlank(assetJson.getString("use_count"))) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_QUANTITY_IS_NULL));//备件数量不能为空
//            }
//            if (!UtilFuns.isInteger(assetJson.getString("use_count"))) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_QUANTITY_IS_NOT_NUMBER));//备件数量必须为数字
//            }
//            BigDecimal inventoryCount = new BigDecimal(String.valueOf(assetJson.get("use_count")));//盘点的备件数量
//            String bomCode = assetJson.getString("bom_code");
//            String materialCode = assetJson.getString("material_code");
//            String bom_model = assetJson.getString("bom_model");
//            BigDecimal recordCount;//发起盘点时，该库房下此备件的记录数量
//            BigDecimal alreadyInventoryCount;//已经盘点的备件数量
//            //查看盘点备件清单
//            Map<String, Object> map = bomInventoryMapper.queryBomInventoryStockDetailByCode(schemaName, subInventoryCode, stockCode, bomCode, materialCode);
//            if (map == null){
//                int bomCount = bomMapper.checkBomByCodeAndMaterialCode(schemaName, bomCode, materialCode);
//                if (bomCount == 0) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_NOT_EXIST), bomCode));//备件“%s”不存在，请先录入到系统再进行盘点
//                }
//                //如果盘点备件清单中没有此备件，则在盘点备件明细表中新增一条待盘点备件数据
//                map = new HashMap<>();
//                map.put("stock_code", stockCode);
//                map.put("bom_model", bom_model);
//                map.put("bom_code", bomCode);
//                map.put("material_code", materialCode);
//                map.put("inventory_code", inventoryCode);
//                map.put("sub_inventory_code", subInventoryCode);
//                map.put("record_count", 0);
//                map.put("inventory_count", 0);
//                map.put("in_status", 0);//默认0（未处理）
//                map.put("deal_result", 0);
//                map.put("create_time", createTime);
//                map.put("create_user_account", user.getAccount());
//                map.put("status", StatusConstant.PENDING);//待盘点
//                //新增备件盘点明细表数据
//                selectOptionService.batchInsertDatas(schemaName, Arrays.asList(map), "_sc_bom_inventory_stock_detail", SqlConstant._sc_bom_inventory_stock_detail_columns);
//
//                recordCount = BigDecimal.ZERO;
//                alreadyInventoryCount = BigDecimal.ZERO;
//            }else{
//                if ((Integer) map.get("deal_result") != 0) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_ALREADY_INVENTORY), (String) map.get("bom_code")));//备件(%s)已经盘点并处理完毕，不能再次盘点
//                }
//                recordCount = new BigDecimal(String.valueOf(map.get("record_count")));
//                alreadyInventoryCount = map.get("inventory_count") == null ? BigDecimal.ZERO : (BigDecimal) map.get("inventory_count");
//            }
//            BigDecimal newInventoryCount;
//            //备件可以盘点多次，且可以为负数，累加到数据库
//            if (inventoryCount.compareTo(alreadyInventoryCount) < 1) {
//                newInventoryCount = BigDecimal.ZERO;//如果盘点数量为负数，且数量大于已经盘点的数量，则更新后盘点数量记录为0
//                totalCount = totalCount.add(alreadyInventoryCount.negate());
//            } else {
//                newInventoryCount = inventoryCount.add(alreadyInventoryCount);
//                totalCount = totalCount.add(inventoryCount);
//            }
//            map.put("inventory_count", newInventoryCount);
//            map.put("finished_time", null);
//            int status = 0;
//            if (newInventoryCount.compareTo(recordCount) == 0) {
//                status = 1; //在库
//                map.put("finished_time", new Date());
//            } else if (newInventoryCount.compareTo(recordCount) > 0) {
//                status = 2; //盈余
//            } else if (newInventoryCount.compareTo(recordCount) < 1) {
//                status = 3; //亏损
//            }
//            map.put("in_status", status);
//            int result = bomInventoryMapper.updateBomInventoryStockDetail(schemaName, map);
//            if (result == 0) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PC_INVENTORY_FAIL));//PC盘点失败
//            }
//        }
//        //同步更新备件盘点表、盘点位置表的累计盘点数量
//        bomInventoryMapper.updateBomInventoryCount(schemaName, inventoryCode, totalCount);
//        bomInventoryMapper.updateBomInventoryStockCount(schemaName, subInventoryCode, totalCount);
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 盘点分配
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel inventoryAllot(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//
//        String next_operator = request.getParameter("next_operator");//按钮类型
//        String subInventoryCode = request.getParameter("subWorkCode");
//        if (StringUtils.isBlank(next_operator) || "-1".equals(next_operator))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATOR_IS_NULL));//操作人不能为空，盘点分配失败
//
//        Map<String, Object> stock = bomInventoryMapper.queryBomInventoryStockByCode(schemaName, subInventoryCode);
//        if (stock == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_INVENTORY_ALLOT_FAIL));//数据异常，盘点分配失败
//
//        if ((Integer) stock.get("status") == StatusConstant.COMPLETED)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_FINISHED_ALLOT_FAIL));//库房盘点已完成，盘点分配失败
//
//        int result = bomInventoryMapper.updateBomStockDutyMan(schemaName, next_operator, StatusConstant.PENDING, subInventoryCode);
//        if (result > 0) {
//            //更新流程中的分配人信息
//            boolean isSuccess = workflowService.assignWithSWC(schemaName, subInventoryCode, next_operator, new HashMap());
//            if (!isSuccess) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_ALLOT_FLOW_FAIL));//盘点分配流程调用失败
//            }
//            logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), subInventoryCode, selectOptionService.getLanguageInfo(LangConstant.INVENTORY_ALLOT), user.getAccount());//记录历史;盘点分配
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_ALLOT_FAIL));//盘点分配失败
//        }
//        return ResponseModel.ok("ok");
//    }
//
//    /**
//     * 查询解析 盘点位置、盘点备件列表数据
//     *
//     * @param schemaName
//     * @param bom_inventory_stock_tmp
//     * @param inventoryCode
//     * @param userAccount
//     * @param createTime
//     * @return
//     */
//    private boolean handlerBomInventoryStockDetail(String schemaName, Map<String, Object> dataInfo, Map<String, Object> bom_inventory_tmp, Map<String, Object> bom_inventory_stock_tmp,
//                                                   String inventoryCode, String userAccount, Timestamp createTime, String stockCodeStr, String bomTypeIds, Map<String, String> stockCodeToManageAccountMap) {
//        Map<String, Object> stockCodeToSubInventoryCodeMap = new HashMap<>();//库房code、盘点位置表code关系映射
//        Map<String, String> stockCodeToStockNameMap = new HashMap<>();//库房code、库房名称关系映射
//        List<Map<String, Object>> bom_inventory_stock_list_tmp = new ArrayList<>();//盘点位置表数据
//        String[] stockCodes = stockCodeStr.split(",");
//        for (int i = 0; i < stockCodes.length; i++) {
//            //每个库房生成一条盘点子表数据
//            String stockCode = stockCodes[i];
//            stockCode = stockCode.substring(1, stockCode.length() - 1);
//            HashMap<String, Object> bom_inventory_stock_map = new HashMap<>();
//            bom_inventory_stock_map.putAll(bom_inventory_stock_tmp);
//            bom_inventory_stock_map.put("stock_code", stockCode);
//            if (i > 0)//多个库房时，要给除第一条意外的备件库房表补上主键code
//                bom_inventory_stock_map.put("sub_inventory_code", dynamicCommonService.getSubBomInventoryCode(schemaName));
//
//            stockCodeToSubInventoryCodeMap.put(stockCode, bom_inventory_stock_map.get("sub_inventory_code"));//记录库房对应的盘点位置表主键code
//            bom_inventory_stock_list_tmp.add(bom_inventory_stock_map);
//        }
//        //解析设备类型
//        String condition = "";
//        List<Map<String, Object>> bom_type_list_tmp = new ArrayList<>();//盘点设备类别表数据
//        if (StringUtils.isNotBlank(bomTypeIds)) {
//            condition = " AND b.type_id IN (" + bomTypeIds + ")";
//            String[] bomTypeIdArray = bomTypeIds.split(",");
//            for (String bomTypeId : bomTypeIdArray) {
//                Map<String, Object> bom_type_tmp = new HashMap<>();
//                bom_type_tmp.put("bom_type_id", bomTypeId);
//                bom_type_tmp.put("inventory_code", inventoryCode);
//                bom_type_list_tmp.add(bom_type_tmp);
//            }
//        }
//        //根据库房code（多选），查询库房备件列表
//        List<Map<String, Object>> results = bomStockListMapper.queryBomByStockCodes(schemaName, stockCodeStr, condition);
//        if (results == null || results.size() == 0)
//            return false;
//
//        int totalRecordCount = 0;//盘点备件总数
//        Map<String, Integer> stockRecordCountMap = new HashMap<>();//库房备件总数映射表
//        List<Map<String, Object>> bom_inventory_stock_detail_list_tmp = new ArrayList<>();//盘点备件表数据
//        for (Map<String, Object> result : results) {
//            int record_count = (Integer) result.get("record_count");
//            String stock_code = (String) result.get("stock_code");
//            result.put("inventory_code", inventoryCode);
//            result.put("sub_inventory_code", stockCodeToSubInventoryCodeMap.get(stock_code));
//            result.put("inventory_count", 0);
//            result.put("in_status", 0);//默认0（未处理）
//            result.put("deal_result", 0);
//            result.put("create_time", createTime);
//            result.put("create_user_account", userAccount);
//            result.put("status", StatusConstant.PENDING);//待盘点
//            stockCodeToStockNameMap.put(stock_code, (String) result.get("stock_name"));//记录库房对应的名称
//            bom_inventory_stock_detail_list_tmp.add(result);
//            //累计计算每个库房的备件总数
//            if (stockRecordCountMap.containsKey(stock_code)) {
//                stockRecordCountMap.put(stock_code, stockRecordCountMap.get(stock_code) + record_count);
//            } else {
//                stockRecordCountMap.put(stock_code, record_count);
//            }
//            totalRecordCount += record_count;//累计总备件数
//        }
//
//        Iterator<Map<String, Object>> iter = bom_inventory_stock_list_tmp.iterator();
//        while (iter.hasNext()) {
//            Map<String, Object> bom_inventory_stock = (Map<String, Object>) iter.next();
//            String stockCode = (String) bom_inventory_stock.get("stock_code");
//            if (!stockRecordCountMap.containsKey(stockCode)) {//如果库房中没有任何备件，则该库房无需加到盘点中去，剔除该库房
//                iter.remove();
//                continue;
//            }
//            Object count = stockRecordCountMap.get(stockCode);
//            //更新盘点位置表的库房备件总数
//            bom_inventory_stock.put("record_count", count);
//            bom_inventory_stock.put("balance_count", count);
//            bom_inventory_stock.put("stock_name", stockCodeToStockNameMap.get(stockCode));
//            //补上库房盘点人
//            String dutyMan = stockCodeToManageAccountMap.get(stockCode);
//            if (StringUtils.isBlank(dutyMan)) {
//                bom_inventory_stock.put("status", StatusConstant.PENDING_ORDER);
//                bom_inventory_stock.put("duty_man", "");
//            } else {
//                bom_inventory_stock.put("status", StatusConstant.PENDING);
//                bom_inventory_stock.put("duty_man", dutyMan);
//            }
//        }
//        bom_inventory_tmp.put("record_count", totalRecordCount);//更新备件盘点总备件数量
//        bom_inventory_tmp.put("balance_count", totalRecordCount);
//        dataInfo.put("bom_type_list_tmp", bom_type_list_tmp);
//        dataInfo.put("bom_inventory_stock_list_tmp", bom_inventory_stock_list_tmp);
//        dataInfo.put("bom_inventory_stock_detail_list_tmp", bom_inventory_stock_detail_list_tmp);
//        return true;
//    }
//
//    /**
//     * 保存数据
//     *
//     * @param schemaName
//     * @param map        : keys：数据主键，dataInfo：数据
//     */
//    private String doSaveData(String schemaName, Map<String, Object> map) throws Exception {
//        Map<String, Map<String, Object>> dataInfo = (Map<String, Map<String, Object>>) map.get("dataInfo");
//        Map<String, Object> bomInventory = dataInfo.get("_sc_bom_inventory");
//        dynamicCommonService.handleTableColumnTypes(bomInventory);//补充插入数据字段类型
//        workSheetHandleMapper.insert(schemaName, bomInventory, "_sc_bom_inventory"); //保存备件盘点主表信息
//
//        List<Map<String, Object>> inventoryStockList = (List<Map<String, Object>>) dataInfo.get("bom_inventory_stock_list_tmp");
//        List<Map<String, Object>> inventoryStockDetailList = (List<Map<String, Object>>) dataInfo.get("bom_inventory_stock_detail_list_tmp");
//        List<Map<String, Object>> bomTypeList = (List<Map<String, Object>>) dataInfo.get("bom_type_list_tmp");
//
//        //批量导入备件盘点位置表数据
//        selectOptionService.batchInsertDatas(schemaName, inventoryStockList, "_sc_bom_inventory_stock", SqlConstant._sc_bom_inventory_stock_columns);
//        //批量导入备件盘点明细表数据
//        selectOptionService.batchInsertDatas(schemaName, inventoryStockDetailList, "_sc_bom_inventory_stock_detail", SqlConstant._sc_bom_inventory_stock_detail_columns);
//        //批量导入备件盘点备件类型表数据
//        selectOptionService.batchInsertDatas(schemaName, bomTypeList, "_sc_bom_inventory_bom_type", SqlConstant._sc_bom_inventory_bom_type_columns);
//        if (inventoryStockList != null) {
//            List<FlowData> flowDataList = new ArrayList<FlowData>(); // 流程数据集合
//            for (Map<String, Object> inventoryStock : inventoryStockList) {
//                // 流程数据
//                FlowData fd = new FlowData();
//                fd.setName(inventoryStock.get("stock_name") + selectOptionService.getLanguageInfo(LangConstant.INVEN_ATORY)); //盘点
//                if (StringUtils.isNotBlank((String) inventoryStock.get("duty_man")) && !"-1".equals(inventoryStock.get("duty_man"))) {
//                    fd.setReceive_account((String) inventoryStock.get("duty_man")); // 用户
//                }
//                fd.setSub_work_code((String) inventoryStock.get("sub_inventory_code")); // 主键
//                fd.setOrder(-1); // 并行
//                fd.setFacility_id((String) inventoryStock.get("stock_code")); // 所属位置
//                fd.setDeadline_time(bomInventory.get("deadline_time").toString()); // 截止时间
//                fd.setCreate_time(bomInventory.get("begin_time").toString()); // 发生时间
//                flowDataList.add(fd);
//            }
//            return JSON.toJSONString(flowDataList);
//        }
//        return null;
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String subInventoryCode = request.getParameter("sub_inventory_code");
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        Map<String, Object> bomInventoryStock = bomInventoryMapper.queryBomInventoryStockByCode(schemaName, subInventoryCode);
//        int status = (Integer) bomInventoryStock.get("status");
//        if (StatusConstant.COMPLETED != status && StatusConstant.CANCEL != status) {//未完成的盘点单可以分配
//            permissionInfo.put("inventory_edit", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "bom_inventory");
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_35, "code");
//        String whereString = "and(template_type=3 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/bom_inventory/findSingleBidInfo")
//                .addObject("workCode", subInventoryCode)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", subInventoryCode)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 盘点盈余备件的入库
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel stockInBom(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        String subInventoryCode = request.getParameter("sub_inventory_code");
//        String stockCode = request.getParameter("stock_code");
//        String bomCode = request.getParameter("bom_code");
//        String materialCode = request.getParameter("material_code");
//        BomData bom = bomMapper.findByMaterial_code(schemaName, materialCode);
//        if (bom == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FAIL));//备件入库失败
//
//        Map<String, Object> stockDetail = bomInventoryMapper.queryBomInventoryStockDetailByCode(schemaName, subInventoryCode, stockCode, bomCode, materialCode);
//        if (stockDetail == null || (Integer) stockDetail.get("in_status") != 2 || (Integer) stockDetail.get("deal_result") != 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FAIL));//备件入库失败
//
//        BigDecimal record_count = (BigDecimal) stockDetail.get("record_count");
//        BigDecimal inventory_count = (BigDecimal) stockDetail.get("inventory_count");
//        BigDecimal stock_in_count = inventory_count.subtract(record_count);//计算需要入库的备件数量
//        if (stock_in_count.compareTo(BigDecimal.ZERO) <= 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FAIL));//备件入库失败
//
//        Map<String, Object> bomInStock = new HashMap<>();
//        bomInStock.put("in_code", dynamicCommonService.getBomInStockCode(schemaName));//生成主键
//        bomInStock.put("source_sub_code", subInventoryCode);//记录盘点记录编号
//        bomInStock.put("stock_code", stockCode);
//
//        String work_request_type = "";
//        //获取body_property、flow_id、roleIds数据
//        List<Object> body_property = null;
//        String flow_id = null;
//        Object roleIds = null;
//        try {
//            Map<String, Object> workTypes = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_31);//这里查询的是备件入库的流程模板
//            flow_id = (String) workTypes.get("relation");
//        } catch (Exception e) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_INVENTORY_FLOW_ID_WRONG));//备件盘点业务类型获取失败
//        }
//        String formKey = workflowService.getStartFormKey(schemaName, flow_id);
//        MetadataWork mateWork = (MetadataWork) metadataWorkService.queryById(schemaName, formKey).getContent();
//        body_property = JSON.parseArray(mateWork.getBodyProperty());//自定义字段号集合
//        JSONArray bomInStockDetails = new JSONArray();
//        for (Object bp : body_property) {
//            com.alibaba.fastjson.JSONObject property = (com.alibaba.fastjson.JSONObject) bp;
//            String fieldCode = property.getString("fieldCode");
//            if ("roleIds".equals(fieldCode)) {
//                //TODO 取值待优化
//                roleIds = property.getString("fieldValue");
//            } else if ("bom_use".equals(fieldCode)) {
//                String fieldFormCode = property.getString("fieldFormCode");
//                com.alibaba.fastjson.JSONObject json = new com.alibaba.fastjson.JSONObject();
//                json.put("isuse", true);
//                json.put("quantity", stock_in_count.intValue());
//                json.put("create_time", UtilFuns.sysTime());
//                json.put("type_id", bom.getType_id());
//                json.put("bom_model", bom.getBom_model());
//                json.put("is_from_service_supplier", bom.getIs_from_service_supplier());
//                json.put("brand_name", bom.getBrand_name());
//                json.put("create_user_account", user.getAccount());
//                json.put("maintainCount", bom.getMaintainCount());
//                json.put("supplier", bom.getSupplier());
//                json.put("bom_page_key", bom.getBom_page_key());
//                json.put("id", bom.getId());
//                json.put("bom_code", paramMap.get("bom_code"));
//                json.put("unit_id", bom.getUnit_id());
//                json.put("material_code", materialCode);
//                json.put("bom_name", bom.getBom_name());
//                json.put("nowModalKey", fieldFormCode);
//                json.put("in_type", paramMap.get("in_type"));
//                json.put("buy_time", paramMap.get("date_of_purchase"));
//                json.put("batch_no", paramMap.get("batch_no"));
//                json.put("price", paramMap.get("price"));
//                json.put("currency_id", paramMap.get("currency_id"));
//                json.put("bom_currency", paramMap.get("bom_currency"));
//                json.put("use_count", stock_in_count.intValue());
//                json.put("supplier_name", paramMap.get("supplier_name"));
//                json.put("in_type_name", paramMap.get("in_type_name"));
//                json.put("show_price", 0);
//                json.put("unit_name", paramMap.get("unit_name"));
//                json.put("taskRight", "edit");
//                json.put("type_name", paramMap.get("type_name"));
//                bomInStockDetails.add(json);
//                property.put("bomContent", bomInStockDetails);
//            } else if ("stock_code".equals(fieldCode)) {
//                property.put("fieldValue", stockCode);
//            } else if ("work_request_type".equals(fieldCode)) {
//                work_request_type = property.getString("fieldValue");
//            }
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        bomInStock.put("body_property", body_property.toString());
//        bomInStock.put("status", "");
//        bomInStock.put("create_user_account", "");
//        bomInStock.put("create_time", "");
//        dynamicCommonService.handleTableColumnTypes(bomInStock);
//        ResponseModel result = bomInStockService.addBomInStock(schemaName, String.valueOf(ButtonConstant.BTN_SUBMIT), user, flow_id, String.valueOf(roleIds), bomInStock, bomInStockDetails, subInventoryCode, true, hasAudit);
//        //入库流程调用成功后，需要更新处理状态为“入库中”
//        if (hasAudit && result.getCode() == ResultStatus.SUCCESS.getCode()) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("bom_code", bomCode);
//            map.put("material_code", materialCode);
//            map.put("deal_result", 1);//处理结果为“入库中”
//            map.put("sub_inventory_code", subInventoryCode);
//            bomInventoryMapper.updateBomInventoryStockDetailDealResult(schemaName, map);
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.BOM_IN_STOCK_SUCC));//备件入库申请成功
//        }
//        return result;
//    }
//
//    /**
//     * 盘点盈余备件的报废
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel stockDiscard(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        String subInventoryCode = request.getParameter("sub_inventory_code");
//        String stockCode = request.getParameter("stock_code");
//        String bomCode = request.getParameter("bom_code");
//        String materialCode = request.getParameter("material_code");
//        String leave_cost = request.getParameter("leave_cost");
//        BomData bom = bomMapper.findByMaterial_code(schemaName, materialCode);
//        if (bom == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_FAIL));//备件报废失败
//
//        Map<String, Object> stockDetail = bomInventoryMapper.queryBomInventoryStockDetailByCode(schemaName, subInventoryCode, stockCode, bomCode, materialCode);
//        if (stockDetail == null || (Integer) stockDetail.get("in_status") != 3 || (Integer) stockDetail.get("deal_result") != 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_FAIL));//备件报废失败
//
//        BigDecimal record_count = (BigDecimal) stockDetail.get("record_count");
//        BigDecimal inventory_count = (BigDecimal) stockDetail.get("inventory_count");
//        BigDecimal stock_discard_count = record_count.subtract(inventory_count);//计算需要报废的备件数量
//        if (stock_discard_count.compareTo(BigDecimal.ZERO) <= 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_FAIL));//备件报废失败
//
//        Map<String, Object> bomDiscard = new HashMap<>();
//        bomDiscard.put("discard_code", dynamicCommonService.getBomDiscardCode(schemaName));//生成主键
//        bomDiscard.put("source_sub_code", subInventoryCode);//记录盘点记录编号
//        bomDiscard.put("stock_code", stockCode);
//
//        String work_request_type = "";
//        //获取body_property、flow_id、roleIds数据
//        List<Object> body_property = null;
//        String flow_id = null;
//        Object roleIds = null;
//        try {
//            Map<String, Object> workTypes = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_34);//这里查询的是备件报废的流程模板
//            flow_id = (String) workTypes.get("relation");
//        } catch (Exception e) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_INVENTORY_FLOW_ID_WRONG));//备件盘点业务类型获取失败
//        }
//        String formKey = workflowService.getStartFormKey(schemaName, flow_id);
//        MetadataWork mateWork = (MetadataWork) metadataWorkService.queryById(schemaName, formKey).getContent();
//        body_property = JSON.parseArray(mateWork.getBodyProperty());//自定义字段号集合
//        JSONArray bomInStockDetails = new JSONArray();
//        for (Object bp : body_property) {
//            com.alibaba.fastjson.JSONObject property = (com.alibaba.fastjson.JSONObject) bp;
//            String fieldCode = property.getString("fieldCode");
//            if ("roleIds".equals(fieldCode)) {
//                //TODO 取值待优化
//                roleIds = property.getString("fieldValue");
//            } else if ("bom_use".equals(fieldCode)) {
//                String fieldFormCode = property.getString("fieldFormCode");
//                com.alibaba.fastjson.JSONObject json = new com.alibaba.fastjson.JSONObject();
//                json.put("id", bom.getId());
//                json.put("bom_model", bom.getBom_model());
//                json.put("type_id", bom.getType_id());
//                json.put("brand_name", bom.getBrand_name());
//                json.put("price", bom.getShow_price());
//                json.put("supplier", bom.getSupplier());
//                json.put("bom_page_key", bom.getBom_page_key());
//                json.put("bom_code", paramMap.get("bom_code"));
//                json.put("stock_code", stockCode);
//                json.put("bom_name", bom.getBom_name());
//                json.put("material_code", materialCode);
//                json.put("nowModalKey", fieldFormCode);
//                json.put("use_count", stock_discard_count.intValue());
//                json.put("leave_cost", leave_cost);
//                json.put("currency_id", paramMap.get("currency_id"));
//                json.put("bom_currency", paramMap.get("bom_currency"));
//                json.put("type_name", paramMap.get("type_name"));
//                json.put("supplier_name", paramMap.get("supplier_name"));
//                json.put("taskRight", "edit");
//                json.put("stock_name", paramMap.get("stock_name"));
//                json.put("bom_model_id", paramMap.get("bom_model_id"));
//                json.put("quantity", Integer.valueOf((String)paramMap.get("quantity")));
//                bomInStockDetails.add(json);
//                property.put("bomContent", bomInStockDetails);
//            } else if ("stock_code".equals(fieldCode)) {
//                property.put("fieldValue", stockCode);
//            } else if ("work_request_type".equals(fieldCode)) {
//                work_request_type = property.getString("fieldValue");
//            }
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        bomDiscard.put("body_property", body_property.toString());
//        bomDiscard.put("status", "");
//        bomDiscard.put("create_user_account", "");
//        bomDiscard.put("create_time", "");
//        Map<String, Object> map = new HashMap<>();
//        map.put("bomContent3", bomInStockDetails);
//        dynamicCommonService.handleTableColumnTypes(bomDiscard);
//        return bomDiacardService.addBomDiscard(schemaName, String.valueOf(ButtonConstant.BTN_SUBMIT), user, flow_id, String.valueOf(roleIds), bomDiscard, map, subInventoryCode, true, hasAudit);
//    }
//
//    /**
//     * 盘点完成
//     *
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public ResponseModel finishInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) {
//        User user = AuthService.getLoginUser(request);
//        String inventoryCode = request.getParameter("inventoryCode");
//        String subInventoryCode = request.getParameter("subInventoryCode");
//        String taskId = request.getParameter("taskId");
//        if (StringUtils.isBlank(inventoryCode) || StringUtils.isBlank(subInventoryCode) || StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.TEXT_K_INVENTORY_FINISH_FAIL));//参数异常，盘点完成操作失败
//
//        Map<String, Object> stock = bomInventoryMapper.queryBomInventoryStockByCode(schemaName, subInventoryCode);
//        if (stock == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_FACILITY_ERROR_FINISH_FAIL));//盘点位置信息异常，盘点完成操作失败
//
//        if ((Integer) stock.get("status") == StatusConstant.COMPLETED || (Integer) stock.get("status") == StatusConstant.CANCEL)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_FINISHED_OR_CANCEL));//库房已经完成盘点或者作废，不能重复操作
//
//        List<Map<String, Object>> stockDetails = bomInventoryMapper.queryBomInventoryStockDetailBySubInventoryCode(schemaName, subInventoryCode);
//        if (stockDetails == null || stockDetails.size() == 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_FINISH_FAIL));//盘点完成操作失败
//
//        boolean isFinish = true; //标识整个库房是否已完成盘点
//        for (Map<String, Object> stockDetail : stockDetails) {
//            Timestamp finishedTime = (Timestamp) stockDetail.get("finished_time");
//            int inStatus = (Integer) stockDetail.get("in_status");
//            int dealResult = (Integer) stockDetail.get("deal_result");
//            if (!(inStatus == 1 || dealResult == 2 || dealResult == 3)
//                    || finishedTime == null) {//在库状态为“在库”或者处理结果为“已入库”、“已报废”,且完成时间不能为空时，才能完成盘点
//                isFinish = false; //只要有一个备件没有完成盘点处理，整个库房就不能算盘点完成
//                break;
//            }
//        }
//        if (isFinish) {
//            String account = user.getAccount();
//            Timestamp finishedTime = new Timestamp(System.currentTimeMillis());
//            String condition = " and sub_inventory_code = '" + subInventoryCode + "' ";
//            bomInventoryMapper.updateBomInventoryStockStatus(schemaName, StatusConstant.COMPLETED, finishedTime, condition);
//            //查询是否还有未处理完成的库房
//            int unFinishedCount = bomInventoryMapper.countUnfinishedBomInventoryStock(schemaName, inventoryCode);
//            if (unFinishedCount == 0) {
//                bomInventoryMapper.updateBomInventoryStatus(schemaName, StatusConstant.COMPLETED, inventoryCode, finishedTime);
//                logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), inventoryCode, selectOptionService.getLanguageInfo(LangConstant.INV_FINISHED), account);//记录历史;盘点完成
//            }
//            Map<String, String> map = new HashMap<>();
//            map.put("sub_work_code", subInventoryCode);
//            map.put("title_page", selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_INVENTORY_SUCC));//库房盘点成功
//            //完成流程
//            boolean isSuccess = workflowService.complete(schemaName, taskId, account, map);
//            if (!isSuccess) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_FINISH_FLOW_FAIL));//盘点完成流程调用失败
//            }
//            logService.AddLog(schemaName, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_35), subInventoryCode, selectOptionService.getLanguageInfo(LangConstant.INV_FINISHED), account);//记录历史;盘点完成
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STOCK_IS_UNFINISHED));//库房中备件还没有处理完，库房不能标记为盘点完成
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SPARE_INVENTABLE_FINISH));//备件盘点完成
//    }
}
