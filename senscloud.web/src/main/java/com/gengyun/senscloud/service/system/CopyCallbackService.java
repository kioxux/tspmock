package com.gengyun.senscloud.service.system;

import java.util.List;
import java.util.Map;

public interface CopyCallbackService {
    public void callBackResult(List<Map<String, Object>> list);
}
