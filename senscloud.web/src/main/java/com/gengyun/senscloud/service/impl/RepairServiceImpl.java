package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.RepairService;
import org.springframework.stereotype.Service;

@Service
public class RepairServiceImpl implements RepairService {
//    @Autowired
//    RepairMapper repairMapper;
//
//    @Autowired
//    LogsMapper loggerMapper;
//
//    @Autowired
//    UserMapper userMapper;
//
//    @Autowired
//    FacilitiesMapper facilityMapper;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    LogsService logService;
//
//    @Override
//    public RepairTypeCollection findAllRepairTypeCollectionList(String schema_name, long facilityId, String userType) {
//
//        RepairTypeCollection collection = new RepairTypeCollection();
//        collection.setAssetStatusList(repairMapper.findAllAssetStatusList(schema_name));
//        collection.setFaultTypeList(repairMapper.findAllFaultTypeList(schema_name));
//        collection.setRepairTypeList(repairMapper.findAllRepairTypeList(schema_name));
//        String usercondition = "repair_do";
//        if (null != userType && !"".equals(userType)) {
//            usercondition = userType;
//        }
//
//
//        //先按排班计划找，如果排班计划中没有维修负责人，则按角色信息找
////        if (facilityId > 0) {
////            //获取当前日期
////            Timestamp now = new Timestamp(System.currentTimeMillis());
////            List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema_name, facilityId, usercondition, now, now);
////            if (scheduleList != null && !scheduleList.isEmpty()) {
////                collection.setUserList(scheduleList);
////            } else {
////                //再找一级所属位置的，看有没有排班的人
////                Number facilityIdNumber = facilityId;
////                Integer facilityInt = facilityIdNumber.intValue();
////                FacilitiesResult facilityData = facilityMapper.FacilitiesListById(schema_name, facilityInt);
////                scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema_name, facilityData.getParentId(), usercondition, now, now);
////                if (scheduleList.size() == 0) {
////                    //按角色权限找
////                    List<User> userList = userMapper.findUserByFunctionKey(schema_name, usercondition, facilityId);
////                    collection.setUserList(userList);
////                } else {
////                    collection.setUserList(scheduleList);
////                }
////            }
////        }
////        else {
////            List<User> userList = userMapper.findAll(schema_name);
////            collection.setUserList(userList);
////        }
//        return collection;
//    }
//
//    @Override
//    public int saveRepairInfo(String schema_name, RepairData repairData) {
//        return repairMapper.saveRepair(schema_name, repairData);
//    }
//
//    //新增支援人员，初始工时为0，支援人员中包含_sc_repair表中的接受人员
//    @Override
//    public int insertRepairWorkHour(String schema_name, RepairWorkHoursData repairWorkHourData) {
//        return repairMapper.insertRepairWorkHour(schema_name, repairWorkHourData);
//    }
//
//    //获取最近一小时内的未完成的维修单
//    @Override
//    public int getRepairCountByAssetIdRecently(String schema_name, String asset_id, Timestamp current) {
//        return repairMapper.getRepairCountByAssetIdRecently(schema_name, asset_id, current);
//    }
//
//    //编辑维保信息,点击保存按钮，或者草稿状态，
//    @Override
//    public int EditRepair(String schema_name, RepairData repairData) {
//        return repairMapper.EditRepair(schema_name, repairData);
//    }
//
//    //更新分配人
//    @Override
//    public int EditRepairReceiveMan(String schema_name, RepairData repairData) {
//        return repairMapper.EditRepairReceiveMan(schema_name, repairData);
//    }
//
//    //删除援人员，在重新保存和提交维修单时，先全部删除工时表中的人，再重新新增支援人员进来
//    @Override
//    public int deleteRepairWorkHour(String schema_name, String repair_code) {
//        return repairMapper.deleteRepairWorkHour(schema_name, repair_code);
//    }
//
//    //更新编辑开始维修时间，为手机上点击设备的维修开始
//    @Override
//    public int EditRepairBeginTime(String schema_name, RepairData repairData) {
//        return repairMapper.EditRepairBeginTime(schema_name, repairData);
//    }
//
//    //设置维修开始计时,缺表，待建 2018-03-29
//
//
//    //保存和提交维修结果
//    @Override
//    public int SaveRepairResult(String schema_name, RepairData repairData) {
//        return repairMapper.SaveRepairResult(schema_name, repairData);
//    }
//
//    //新增维修使用的备件
//    @Override
//    public int insertRepairBom(String schema_name, RepairBomData repairBomData) {
//        return repairMapper.insertRepairBom(schema_name, repairBomData);
//    }
//
//    //删除维修领用的备件,用于维修结果重新保存或提交时
//    @Override
//    public int deleteRepairBom(String schema_name, String repair_code) {
//        return repairMapper.deleteRepairBom(schema_name, repair_code);
//    }
//
//    //维修结果审核
//    @Override
//    public int AuditRepairResult(String schema_name, RepairData repairData) {
//        return repairMapper.AuditRepairResult(schema_name, repairData);
//    }
//
//    //查找维修单列表
//    @Override
//    public List<RepairData> getRepairList(String schema_name, String condition, int pageSize, int begin) {
//        return repairMapper.getRepairList(schema_name, condition, pageSize, begin);
//    }
//
//    //查找维修单列表总数
//    @Override
//    public int getRepairListCount(String schema_name, String condition) {
//        return repairMapper.getRepairListCount(schema_name, condition);
//    }
//
//    //查找单条维修单据
//    @Override
//    public RepairData getRepariInfo(String schema_name, String repairCode) {
//        RepairData data = repairMapper.getRepariInfo(schema_name, repairCode);
//        if (data != null) {
//            //找到参与维修的用户及工时
//            List<RepairWorkHoursData> workHoursDataList = repairMapper.getRepariWorkHourList(schema_name, repairCode);
//            data.setWorkHourList(workHoursDataList);
//
//            //找到维修中使用完的备件
//            List<RepairBomData> bomDataList = repairMapper.getRepariBomList(schema_name, repairCode);
//            data.setBomList(bomDataList);
//
//            if (data.getRepairCode() != null && !data.getRepairCode().isEmpty()) {
//                List<LogsData> dataList = logService.findAllLogsList(schema_name, "repair", data.getRepairCode());
//                //多时区处理
//                SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
//                data.setHistoryList(dataList);
//            }
//        }
//        return data;
//    }
//
//
//    //保存支援人员
//    @Override
//    public void SaveRepairWorkMan(String schema_name, String receive_account, String help_account, String repairCode) {
//        RepairWorkHoursData data;
//        //如果接受人非空，将接受人加如到支援人员中，便于后期记录工时
//        String allHelper;
//        if (receive_account != null && !receive_account.isEmpty()) {
//            if (help_account != null && !help_account.isEmpty()) {
//                String tempHelpAccount = "," + help_account + ",";
//                String tempReceiveAccount = "," + receive_account + ",";
//                if (tempHelpAccount.indexOf(tempReceiveAccount) >= 0) {
//                    allHelper = help_account;
//                } else {
//                    allHelper = help_account + "," + receive_account;
//                }
//            } else {
//                allHelper = receive_account;
//            }
//        } else {
//            allHelper = help_account;
//        }
//
//        if (allHelper != null && !allHelper.isEmpty()) {
//            String[] helpList = allHelper.split(",");
//            //如果支援人员中没有接受人，则添加到helpList中
//            if (helpList != null && helpList.length > 0) {
//                for (String helper : helpList) {
//                    data = new RepairWorkHoursData();
//                    data.setRepairCode(repairCode);
//                    data.setOperateAccount(helper);
//                    data.setWorkHour(0);
//                    insertRepairWorkHour(schema_name, data);
//                }
//            }
//        }
//    }
//
//    //查找单条维修单据，根据设备id
//    @Override
//    public RepairData getRepariInfoByAssetId(String schema_name, String asset_id, String account) {
//        RepairData data = repairMapper.getRepariInfoByAssetId(schema_name, asset_id, account);
//        if (data != null) {
//            //找到参与维修的用户
//            List<RepairWorkHoursData> workHoursDataList = repairMapper.getRepariWorkHourList(schema_name, data.getRepairCode());
//            data.setWorkHourList(workHoursDataList);
//
//            //找到维修中使用的备件
//            List<RepairBomData> bomDataList = repairMapper.getRepariBomList(schema_name, data.getRepairCode());
//            data.setBomList(bomDataList);
//
//            if (data.getRepairCode() != null && !data.getRepairCode().isEmpty()) {
//                List<LogsData> dataList = logService.findAllLogsList(schema_name, "repair", data.getRepairCode());
//                //多时区处理
//                SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
//                data.setHistoryList(dataList);
//            }
//        }
//        return data;
//    }
//
//
//    @Override
//    public Asset getAssetCommonDataByCode(String schema_name, String device_code) {
//
//        return repairMapper.getAssetCommonDataByCode(schema_name, device_code);
//    }
//
//    //查找维修单，查到需维修的设备
//    @Override
//    public Asset getAssetCommonDataByRepairCode(String schema_name, String repairCode) {
//        return repairMapper.getAssetCommonDataByRepairCode(schema_name, repairCode);
//    }
//
//    //通过来源，查询维修单
//    @Override
//    public List<RepairData> getRepairListByFromCode(String schema_name, String fromCode) {
//        return repairMapper.getRepairListByFromCode(schema_name, fromCode);
//    }
//
//    @Override
//    public List<RepairData> getRepairListByCode(String schema_name, String device_code) {
//        return repairMapper.getRepairListByCode(schema_name, device_code);
//    }
//
//    @Override
//    public List<WorkSheet> getRepairDetailByBomModelAndStockCode(String schema_name, String condition) {
//        return repairMapper.getRepairDetailByBomModelAndStockCode(schema_name, condition);
//    }
//
//    @Override
//    public int invalidRepair(String schema_name, String repairCode) {
//        return repairMapper.invalidRepair(schema_name, repairCode);
//    }
//
//    //嘉岩更新添加数据
//    @Override
//    public int insertRepairBomJY(String schema_name, RepairBomData repairBomData) {
//        return repairMapper.insertRepairBomJY(schema_name, repairBomData);
//    }
//
//    //嘉研备件申领修改状态
//    @Override
//    public int updateRepairApplyStatus(String schema_name, String code) {
//        return repairMapper.updateRepairApplyStatus(schema_name, code);
//    }
//
//    //嘉研备件申领修改状态
//    @Override
//    public int updateOpenRepairApplyStatus(String schema_name, String code, int status) {
//        return repairMapper.updateOpenRepairApplyStatus(schema_name, code, status);
//    }
//
//    //维修备件是否存在备件信息
//    @Override
//    public List<RepairBomData> selectRepairByMaterialCode(String schema_name, String code, String material_code) {
//        return repairMapper.selectRepairByMaterialCode(schema_name, code, material_code);
//    }
//
//    //修改维修备件已存在的第三方客户来源备件信息
//    @Override
//    public int updateRepairBomByMaterCode(String schema_name, String bill_code) {
//        return repairMapper.updateRepairBomByMaterCode(schema_name, bill_code);
//    }
//
//    @Override
//    public List<RepairData> queryAssetFailureList(String schema_name, int pageSize, int begin) {
//        return repairMapper.queryAssetFailureList(schema_name, pageSize, begin);
//    }
//
//    @Override
//    public int queryAssetFailureListCount(String schema_name) {
//        return repairMapper.queryAssetFailureListCount(schema_name);
//    }
//
//    @Override
//    public FullScreenData getPlannedFailureRepair(String schema_name) {
//        return repairMapper.getPlannedFailureRepair(schema_name);
//    }
}
