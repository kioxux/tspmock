package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MonitorAreaService;
import org.springframework.stereotype.Service;
@Service
public class MonitorAreaServiceImpl implements MonitorAreaService {
//
//    @Autowired
//    MonitorAreaMapper monitorAreaMapper;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Override
//        public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String facilityId, User loginUser) {
//
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String condition = "";
//        if (StringUtils.isNotEmpty(searchKey)) {
//            condition+="and ma.monitor_area_name like '%"+searchKey+"%'";
//        }
//        String[] facilitys={};
//        if(StringUtils.isNotEmpty(facilityId)){
//            facilitys=facilityId.split(",");
//        }
//        //获取组织的根节点数据，用于like进行比较
//        String[] facilityNos = dataPermissionForFacility.findFacilityNOsByBusinessForSingalUser(schemaName, loginUser, facilitys, "asset_monitor");
//        if (facilityNos != null && facilityNos.length > 0) {
//            condition += " and (";
//            for (String facilityNo : facilityNos) {
//                condition += " p.position_code like '" + facilityNo + "%' or"+" f.facility_no like '"+facilityNo+"%' or";
//            }
//            condition = condition.substring(0, condition.length() - 2);
//            condition += ") ";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = monitorAreaMapper.getMonitorAreaList(schemaName,condition, pageSize, begin);
//        int total = monitorAreaMapper.countMonitorAreaList(schemaName, condition);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel addMonitorArea(Map<String, Object> paramMap) {
//        int count = monitorAreaMapper.addMonitorArea( paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel updateMonitorArea(Map<String, Object> paramMap) {
//        int count = monitorAreaMapper.updateMonitorArea(paramMap);
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel deleteById(String schemaName, HttpServletRequest request) {
//        int count = monitorAreaMapper.deleteMonitorAreaById(schemaName,Integer.parseInt(request.getParameter("id")));
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public Map<String, Object> findMonitorAreaById(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String id=request.getParameter("id");
//        return monitorAreaMapper.getmonitorAreaById(schemaName,Integer.parseInt(id));
//    }
//
//    @Override
//    public JSONObject queryForScada(String schemaName, String facilityId,String facilityNo) {
//        String condition="";
//        if(StringUtils.isNotEmpty(facilityId)){
//            condition+=" ma.asset_position_code='"+facilityId+"'";
//            if(!facilityId.startsWith("P")){
//                condition+=" or f.id ='"+facilityId+"'";
//            }
//        }
//        if(StringUtils.isNotEmpty(facilityNo)){
//            condition+="or f.facility_no like '%"+facilityNo+"%' or ma.asset_position_code like '%"+facilityNo+"%'";
//        }
//        List<Map<String, Object>> result = monitorAreaMapper.getmonitorAreaListByPosition(schemaName,condition);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("rows", result);
//        return pageResult;
//    }
}
