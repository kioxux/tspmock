package com.gengyun.senscloud.service.login;


import com.gengyun.senscloud.model.LoginModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SystemPermissionResult;

import java.util.List;
import java.util.Map;

/**
 * 登录用
 */
public interface LoginService {

    /**
     * 根据域名获取登录页信息
     *
     * @param methodParam 系统参数
     * @return 登录用户信息
     */
    Map<String, Object> getLoginData(MethodParam methodParam);

    /**
     * 登录验证
     *
     * @param methodParam 系统参数
     * @param loginModel  页面参数
     * @return token
     */
    String checkLogin(MethodParam methodParam, LoginModel loginModel);


    /**
     * 获取企业列表
     *
     * @param methodParam 页面参数
     * @return 企业列表
     */
    List<Map<String, Object>> getCompanyList(MethodParam methodParam);

    /**
     * 进入登录
     *
     * @param methodParam 页面参数
     * @param c_id        企业编号
     */
    void applyLogin(MethodParam methodParam, Long c_id);

    /**
     * 进入登录(NFC)
     *
     * @param methodParam 页面参数
     * @param c_id        企业编号
     * @param user_code   员工工号
     */
    Map<String, Object> applyNFCLogin(MethodParam methodParam, Long c_id, String user_code);

    /**
     * 获取菜单信息
     *
     * @param methodParam 页面参数
     * @return 菜单信息
     */
    List<SystemPermissionResult> getMenuList(MethodParam methodParam);

    /**
     * 获取登录用户信息
     *
     * @param methodParam 系统参数
     * @return 登录用户信息
     */
    Map<String, Object> getLoginUserInfo(MethodParam methodParam);

    /**
     * 发送验证码
     *
     * @param phone 手机号码
     */
    void sendVfCodeByPhone(String phone);

    /**
     * 验证短信验证码并返回企业列表
     *
     * @param loginModel 页面参数
     * @return 企业列表
     */
    List<Map<String, Object>> getCompanyListByPhone(LoginModel loginModel);

    /**
     * 修改密码（忘记密码用）
     *
     * @param loginModel 页面参数
     */
    void modifyPsdByPhone(LoginModel loginModel);


    /**
     * 发送验证码
     *
     * @param methodParam 系统参数
     */
    void sendVfCode(MethodParam methodParam);

    /**
     * 修改密码
     *
     * @param methodParam 系统参数
     * @param loginModel  页面参数
     */
    void modifyPwd(MethodParam methodParam, LoginModel loginModel);

    /**
     * 获取用户常用语言包/系统默认语言包
     *
     * @param methodParam 系统参数
     * @return 语言包
     */
    Map<String, Object> getDefaultLangInfo(MethodParam methodParam);

    /**
     * 按照旧密码修改新密码
     *
     * @param methodParam 系统参数
     * @param loginModel  页面参数
     */
    void modifyPwdWithoutVfCode(MethodParam methodParam, LoginModel loginModel);

    Map<String, Object> getWeiXinInfo(MethodParam methodParam, LoginModel loginModel);

    List<Map<String, Object>> getAllCache(MethodParam methodParam);

    void clearAllCache(MethodParam methodParam, Map<String, Object> param);

}
