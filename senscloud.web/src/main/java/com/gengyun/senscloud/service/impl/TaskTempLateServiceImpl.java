package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.TaskTemplateMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.TaskTempLateModel;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.TaskTempLateService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TaskTempLateServiceImpl implements TaskTempLateService {
    @Resource
    TaskTemplateMapper taskTemplateMapper;
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    ExportService exportService;

    /**
     * 获取任务模板权限
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getTaskTempLatePermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[7]);
    }

    /**
     * 新增任务模板
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public String newTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String task_template_code = serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.TEMPLATE_TASK);
        String task_template_name = RegexUtil.optStrOrExpNotNull(taskTempLateModel.getTask_template_name(), LangConstant.TITLE_AAQ_E);
        Map<String, Object> pm = new HashMap<>();
        pm.put("task_template_code", task_template_code);
        pm.put("task_template_name", task_template_name);
        pm.put("is_use", taskTempLateModel.getIs_use());
        pm.put("remark", taskTempLateModel.getRemark());
        pm.put("create_user_id", methodParam.getUserId());
        taskTemplateMapper.insertTaskTemplate(methodParam.getSchemaName(), pm);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("任务模板新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AAQ_E}));//任务模板新建
        return task_template_code;
    }

    /**
     * 编辑任务模板
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void modifyTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskTempLateModel.getTask_template_code(), LangConstant.TITLE_AAQ_E);
        Map<String, Object> pmOld = taskTemplateMapper.findTaskTemplateByCode(methodParam.getSchemaName(), task_template_code);
        Map<String, Object> pm = new HashMap<>();
        pm.put("task_template_code", task_template_code);
        pm.put("task_template_name", taskTempLateModel.getTask_template_name());
        pm.put("is_use", taskTempLateModel.getIs_use());
        pm.put("remark", taskTempLateModel.getRemark());
        JSONArray loger = LangUtil.compareMap(pm, pmOld);
        taskTemplateMapper.updateTaskTemplate(methodParam.getSchemaName(), pm);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("编辑了任务模板", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAQ_E, loger.toString()}));//编辑了任务模板
    }

    /**
     * 删除任务模板
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void cutTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String[] task_template_codes = RegexUtil.optStrToArray(taskTempLateModel.getTask_template_codes(), LangConstant.TITLE_AAQ_E); // 任务模板未选择
        String schemaName = methodParam.getSchemaName();
        // 删除任务模板
        int i = taskTemplateMapper.deleteTaskTemplateByCodes(schemaName, task_template_codes);
        // 删除任务模板的所有和任务项的关联
        taskTemplateMapper.deleteTemplateTaskByTempCodes(schemaName, task_template_codes);
        String remark = i > 1 ? LangUtil.doSetLogArrayNoParam("批量删除了任务模板！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了任务模板！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAQ_E});
        for (String id : task_template_codes) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, id, remark);
        }
    }

    /**
     * 获取任务模板详情
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public Map<String, Object> getTaskTempLateInfo(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String getTask_template_code = RegexUtil.optStrOrExpNotNull(taskTempLateModel.getTask_template_code(), LangConstant.TITLE_AAQ_E);
        return taskTemplateMapper.findTaskTemplateByCode(methodParam.getSchemaName(), getTask_template_code);
    }

    /**
     * 获取任务模板详情
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板列表
     */
    @Override
    public Map<String, Object> getTaskTempLatePage(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        Map<String, Object> params = new HashMap<>();
        params.put("keywordSearch", taskTempLateModel.getKeywordSearch());
        params.put("isUseSearch", taskTempLateModel.getIsUseSearch());
        Map<String, Object> result = new HashMap<>();
        //查询该条件下的总记录数
        int total = taskTemplateMapper.findTaskTempLatePageCount(methodParam.getSchemaName(), params);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        params.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = taskTemplateMapper.findTaskTempLatePage(methodParam.getSchemaName(), params);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 根据选中主键导出任务模板信息（文件码）
     *
     * @param methodParam       系统参数
     * @param taskTempLateModel 请求参数
     * @param isAll             是否是全选
     * @return 文件码
     */
    @Override
    public String getExportTaskTempInfo(MethodParam methodParam, TaskTempLateModel taskTempLateModel, boolean isAll) {
        return exportService.doDownloadTaskTempInfo(methodParam, this.getTaskTempGroupItemList(methodParam, taskTempLateModel, isAll));
    }

    /**
     * 查询任务模板、任务项、组信息列表
     *
     * @param methodParam       系统参数
     * @param taskTempLateModel 请求参数
     * @param isAll             是否是全选
     * @return 信息列表
     */
    private List<Map<String, Object>> getTaskTempGroupItemList(MethodParam methodParam, TaskTempLateModel taskTempLateModel, boolean isAll) {
        Map<String, Object> params = new HashMap<>();
        params.put("keywordSearch", taskTempLateModel.getKeywordSearch());
        params.put("isUseSearch", taskTempLateModel.getIsUseSearch());
        if (!isAll) {
            params.put("task_template_codes", RegexUtil.optStrToArray(taskTempLateModel.getTask_template_codes(), LangConstant.TITLE_AAQ_E));
        }
        int total = taskTemplateMapper.findTaskTempLatePageCount(methodParam.getSchemaName(), params);
        RegexUtil.falseExp(total > 0, LangConstant.TEXT_K, new String[]{LangConstant.BTN_EXPORT_C});
        return RegexUtil.optListOrExpNullInfo(taskTemplateMapper.findTaskTempGroupItemList(methodParam.getSchemaName(), params), LangConstant.BTN_EXPORT_C);
    }

    /**
     * 新增任务模板任务项关联
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void newTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskTempLateModel.getTask_template_code(), LangConstant.TITLE_AAQ_E);
        String[] task_item_codes = RegexUtil.optStrToArray(taskTempLateModel.getTask_item_codes(), LangConstant.TITLE_AAQ_Q); // 任务项未选择
        String schemaName = methodParam.getSchemaName();
        String str = taskTemplateMapper.findExistTaskTigInfo(schemaName, task_template_code, task_item_codes, Constants.DEFAULT_TASK_GROUP_NAME);
        RegexUtil.trueExp(RegexUtil.optIsPresentStr(str), LangConstant.TEXT_G, new String[]{LangConstant.TITLE_AAQ_Q, str}); // 任务项已存在：/任务项
        for (String task_item_code : task_item_codes) {
            Map<String, Object> pm = new HashMap<>();
            pm.put("task_item_code", task_item_code);
            pm.put("task_template_code", task_template_code);
            pm.put("group_name", Constants.DEFAULT_TASK_GROUP_NAME);
            taskTemplateMapper.insertTaskTempLateItem(schemaName, pm);
        }
        List<String> task_item_code_names = taskTemplateMapper.findTaskItemNames(schemaName, task_item_codes);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("添加了任务列表", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TITLE_AAQ_C, String.join(",", task_item_code_names)}));
    }

    /**
     * 删除任务模板任务项关联
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void cutTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String[] taskIds = RegexUtil.optStrToArray(taskTempLateModel.getTask_ids(), LangConstant.TITLE_AAQ_Q); // 任务项未选择
        String schemaName = methodParam.getSchemaName();
        List<String> taskNameList = taskTemplateMapper.findTaskItemNamesByIds(schemaName, taskIds);
        String task_template_code = taskTemplateMapper.findTaskTempCodeByIds(schemaName, taskIds);
        taskTemplateMapper.deleteTemplateTaskByIds(schemaName, taskIds);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("删除了任务模板任务项关联", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAQ_C, String.join(",", taskNameList)}));//删除了任务列表
    }

    /**
     * 任务模板任务项关联列表
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板任务项关联列表
     */
    @Override
    public List<Map<String, Object>> getTaskTempLateItemList(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskTempLateModel.getTask_template_code(), LangConstant.TITLE_AAQ_E);
        return taskTemplateMapper.findTemplateItemList(methodParam.getSchemaName(), task_template_code, taskTempLateModel.getKeywordSearch());
    }

    /**
     * 任务模板列表
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板列表
     */
    @Override
    public List<Map<String, Object>> getTaskTempLateList(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return taskTemplateMapper.findTaskTempLateList(methodParam.getSchemaName(), taskTempLateModel.getKeywordSearch());
    }

    /**
     * 验证任务模板编号是否存在
     *
     * @param schemaName       数据库
     * @param templateCodeList 任务项模板编号
     * @return 模板编号集合
     */
    @Override
    public List<String> getTempCodesByCodes(String schemaName, List<String> templateCodeList) {
        return taskTemplateMapper.findTaskTemplateCodesByCodes(schemaName, templateCodeList);
    }

    /**
     * 批量验证任务模板、任务项、组是否存在
     *
     * @param schemaName 数据库
     * @param codeList   编号信息
     * @return 编号集合
     */
    @Override
    public List<String> getTaskTemplateItemByInfo(String schemaName, List<String> codeList) {
        return taskTemplateMapper.findTaskTemplateItemByInfo(schemaName, codeList);
    }


    /**
     * 根据编号获取任务模板信息
     *
     * @param schemaName       数据库
     * @param templateCodeList 任务模板编号
     * @return 模板信息集合
     */
    @Override
    public Map<String, Map<String, Object>> getTaskTemplatesByCodes(String schemaName, List<String> templateCodeList) {
        return taskTemplateMapper.findTaskTemplatesByCodes(schemaName, templateCodeList);
    }

    /**
     * 批量新增或更新任务模板
     *
     * @param schemaName 数据库
     * @param tempList   任务模板信息
     */
    @Override
    public void batchInsertTaskTemp(String schemaName, List<Map<String, Object>> tempList) {
        taskTemplateMapper.insertOrUpdateTaskTemp(schemaName, tempList);
    }

    /**
     * 批量关联任务模板、任务项、组
     *
     * @param schemaName 数据库
     * @param midList    信息
     */
    @Override
    public void batchInsertTaskTempItem(String schemaName, List<Map<String, Object>> midList) {
        taskTemplateMapper.insertOrUpdateTaskTempItem(schemaName, midList);
    }

    /**
     * 设置组名称
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void modifyTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String[] taskIds = RegexUtil.optStrToArray(taskTempLateModel.getTask_ids(), LangConstant.TITLE_AAQ_Q); // 任务项未选择
        String schemaName = methodParam.getSchemaName();
        String groupName = RegexUtil.optStrOrVal(taskTempLateModel.getGroup_name(), Constants.DEFAULT_TASK_GROUP_NAME);
        String str = taskTemplateMapper.findExistTaskTigInfoByIds(schemaName, taskIds, groupName);
        RegexUtil.trueExp(RegexUtil.optIsPresentStr(str), LangConstant.TEXT_G, new String[]{LangConstant.TITLE_AAQ_Q, str}); // 任务项已存在：/任务项
        List<String> taskNameList = taskTemplateMapper.findTaskItemNamesByIds(schemaName, taskIds);
        String task_template_code = taskTemplateMapper.findTaskTempCodeByIds(schemaName, taskIds);
        taskTemplateMapper.updateTaskTempLateItem(schemaName, taskIds, groupName);
        List<String> taskNameNewList = taskTemplateMapper.findTaskItemNamesByIds(schemaName, taskIds);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("编辑了组名称", LangConstant.LOG_D, new String[]{LangConstant.TITLE_ABKL, String.join(",", taskNameList), String.join(",", taskNameNewList)}));//编辑了组名称
    }

    /**
     * 向上移动任务模板任务项
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void moveUpTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String taskId = RegexUtil.optStrOrExpNotSelect(taskTempLateModel.getTask_id(), LangConstant.TITLE_AAQ_Q); // 任务项未选择
        String schemaName = methodParam.getSchemaName();
        // 获取当前任务模板任务项
        Map<String, Object> taskTempLateItemNow = RegexUtil.optMapOrExpNullInfo(taskTemplateMapper.findTemplateItem(schemaName, taskId), LangConstant.TITLE_AAQ_Q);// 任务项信息不存在
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskTempLateItemNow.get("task_template_code"), LangConstant.TITLE_AAQ_E);
        // 获取上一个任务模板任务项
        Map<String, Object> taskTempLateItemUp = RegexUtil.optMapOrExp(taskTemplateMapper.findPreviousTemplateItem(schemaName, taskId), LangConstant.TITLE_AAAD_I);
        String pvTaskId = RegexUtil.optStrOrExpNotNull(taskTempLateItemUp.get("task_id"), LangConstant.TITLE_AAAD_I);
        taskTemplateMapper.updateTemplateItemOrder(schemaName, taskId, RegexUtil.optIntegerOrNull(taskTempLateItemUp.get("data_order"), methodParam, ErrorConstant.EC_TASK_001));
        taskTemplateMapper.updateTemplateItemOrder(schemaName, pvTaskId, RegexUtil.optIntegerOrNull(taskTempLateItemNow.get("data_order"), methodParam, ErrorConstant.EC_TASK_001));
        String tn = taskTemplateMapper.findTaskItemNameById(schemaName, taskId);
        String tnPv = taskTemplateMapper.findTaskItemNameById(schemaName, pvTaskId);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("上移了任务项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_UP_A, LangConstant.TITLE_AAQ_Q, tn}));//上移了任务项
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("下移了任务项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DOWN_B, LangConstant.TITLE_AAQ_Q, tnPv}));//下移了任务项
    }

    /**
     * 向下移动任务模板任务项
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    @Override
    public void moveDownTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        String taskId = RegexUtil.optStrOrExpNotSelect(taskTempLateModel.getTask_id(), LangConstant.TITLE_AAQ_Q); // 任务项未选择
        String schemaName = methodParam.getSchemaName();
        // 获取当前任务模板任务项
        Map<String, Object> taskTempLateItemNow = RegexUtil.optMapOrExpNullInfo(taskTemplateMapper.findTemplateItem(schemaName, taskId), LangConstant.TITLE_AAQ_Q);// 任务项信息不存在
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskTempLateItemNow.get("task_template_code"), LangConstant.TITLE_AAQ_E);
        // 获取下一个任务模板任务项
        Map<String, Object> taskTempLateItemDown = RegexUtil.optMapOrExp(taskTemplateMapper.findNextTemplateItem(schemaName, taskId), LangConstant.TITLE_AAAD_J);
        String pvTaskId = RegexUtil.optStrOrExpNotNull(taskTempLateItemDown.get("task_id"), LangConstant.TITLE_AAAD_J);
        taskTemplateMapper.updateTemplateItemOrder(schemaName, taskId, RegexUtil.optIntegerOrNull(taskTempLateItemDown.get("data_order"), methodParam, ErrorConstant.EC_TASK_001));
        taskTemplateMapper.updateTemplateItemOrder(schemaName, pvTaskId, RegexUtil.optIntegerOrNull(taskTempLateItemNow.get("data_order"), methodParam, ErrorConstant.EC_TASK_001));
        String tn = taskTemplateMapper.findTaskItemNameById(schemaName, taskId);
        String tnPv = taskTemplateMapper.findTaskItemNameById(schemaName, pvTaskId);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("下移了任务项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DOWN_B, LangConstant.TITLE_AAQ_Q, tn}));//下移了任务项
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7003, task_template_code, LangUtil.doSetLogArray("上移了任务项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_UP_A, LangConstant.TITLE_AAQ_Q, tnPv}));//上移了任务项
    }
}