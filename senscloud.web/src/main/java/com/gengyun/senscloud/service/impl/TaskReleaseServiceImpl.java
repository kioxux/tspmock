package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.TaskReleaseService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 5:53 2019/9/17 0017
 */
@Service
public class TaskReleaseServiceImpl implements TaskReleaseService {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    TaskReleaseMapper taskReleaseMapper;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    private LogsService logService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    DataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    MessageService messageService;
//
//
//    @Override
//    public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName, String sortOrder, String groups) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        orderBy = sortName + " " + sortOrder;
//        int begin = pageSize * (pageNumber - 1);
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("taskReleasePublish");
//        List<Map<String, Object>> result = taskReleaseMapper.query(schemaName, orderBy, pageSize, begin, searchKey, groups, idPrefix);
//        int total = taskReleaseMapper.countByCondition(schemaName, searchKey, groups);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    /**
//     * 根据任务编号获取用户组角色信息
//     *
//     * @param schemaName
//     * @param taskId
//     * @return
//     */
//    public List<Map<String, Object>> getGroupRoleListByTask(String schemaName, Integer taskId) {
//        return taskReleaseMapper.getGroupRoleListByTask(schemaName, taskId);
//    }
//
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//            Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//            if (null != map_object && map_object.size() > 0) {
//                Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//                List<Map<String, Object>> content = (List<Map<String, Object>>) map_object.get("bomContent18");
//                Map<String, Object> work_taskInfo = (Map<String, Object>) dataInfo.get("_sc_work_task");
//                boolean is_use = true;//默认状态
//                String account = user.getAccount();
//                Timestamp now = new Timestamp(System.currentTimeMillis());//获取当前时间
//                work_taskInfo.put("is_use", is_use);
//                work_taskInfo.put("create_time", now);
//                work_taskInfo.put("schema_name", schema_name);
//                work_taskInfo.put("create_user_account", account);
//                List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//                dataList.add(work_taskInfo);
//                if (work_taskInfo.get("id") != null && !work_taskInfo.get("id").equals("")) {
//                    taskReleaseMapper.deleteReceive(schema_name, (String) work_taskInfo.get("id"));//删除旧数据
//                    taskReleaseMapper.doUpdate(schema_name, work_taskInfo, (String) work_taskInfo.get("id"));//保存发布任务
//                } else {
//                    taskReleaseMapper.doSubmit(work_taskInfo);//保存发布任务
//                }
//                Map<String, Object> receiver = new HashMap<String, Object>();
//                if (work_taskInfo.get("id") != null) {
//                    if (content != null && content.size() > 0) {
//                        for (Map<String, Object> con : content) {
//                            Object group_id = con.get("group_id");
//                            Object role_id = con.get("role_id");
//                            receiver.put("task_id", work_taskInfo.get("id"));
//                            receiver.put("group_id", group_id);
//                            receiver.put("role_id", role_id);
//                            taskReleaseMapper.doSubmitReceiver(schema_name, receiver);//任务接收人
//                        }
//                    }
//                }
////                workProcessService.saveWorkProccess(schema_name, code, status, account); // 进度记录
//                return ResponseModel.ok("ok");
//            }
//        }
//        return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    @Override
//    public boolean doSubmitTaskRelease(String schema_name, String id, boolean status,String create_user_account,String title) {
//        boolean result = false;
//        int update = taskReleaseMapper.doSubmitTaskRelease(schema_name, id, status);
//        if(update>0){
//            logService.AddLog(schema_name, "task_release", id, selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_BOM_TASK)+id, create_user_account);
//            String[] contentParam = {title};
//            messageService.beginMsg(contentParam, qcloudsmsConfig.SMS_10002009, create_user_account, "task_release",id, "system");
//            result=true;
//        }
//        return result;
//    }
//
//    @Override
//    public Map<String, Object> queryTaskReleaseById(String schemaName, String code) {
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("taskReleasePublish");
//        code = code.substring(idPrefix.length());
//        return taskReleaseMapper.queryTaskReleaseById(schemaName, Integer.valueOf(code));
//    }
//
//    @Override
//    public boolean deleteReceive(String schema_name, String id) {
//        boolean result = false;
//        int delete = taskReleaseMapper.deleteReceive(schema_name, id);
//        if (delete > 0) {
//            result = true;
//        }
//        return result;
//    }
//
//    /**
//     * @param schema_name
//     * @Description:根据定时任务执行方法Job
//     */
//    @Override
//    @Transactional
//    public void productTaskRelease(String schema_name) {
//        try {
//            List<Map<String, Object>> taskRelese = taskReleaseMapper.selectAllRelease(schema_name);//查询所有任务
//            if (taskRelese != null && taskRelese.size() > 0) {
//                for (Map<String, Object> task : taskRelese) {
//                    String taskStr = task.get("id").toString();
//                    int taskInt = Integer.parseInt(taskStr);
//                    List<Map<String, Object>> searchReceiveAccount = taskReleaseMapper.searchReceiveAccountInfo(schema_name, taskInt);
//                    int task_rate = (int) task.get("task_rate");
////                    this.generateTask(schema_name, task, searchReceiveAccount);
//                    if(task_rate ==1){//每日
//                        //每周的具体时间
//                        this.generateTask(schema_name, task, searchReceiveAccount);
//                    }else if(task_rate ==2){//每周
//                        //每周周一具体时间
//                        Calendar cal= Calendar.getInstance();
//                        cal.setTime(new Date());
//                        int week=cal.get(Calendar.DAY_OF_WEEK)-1;
//                        if(week==1){//判断是否是礼拜一
//                            this.generateTask(schema_name, task, searchReceiveAccount);
//                        }
//                    }else if(task_rate == 3){//每月
//                        //每月1号的具体时间
//                        Calendar calendar1=Calendar.getInstance();
//                        calendar1.setTime(new Date());
//                        int month = calendar1.get(Calendar.DAY_OF_MONTH);
//                        if(month==1){
//                            this.generateTask(schema_name, task, searchReceiveAccount);
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//
//        }
//    }
//
//    /**
//     * 根据判断进行任务自动化生成
//     *
//     * @param task
//     * @param searchReceiveAccount
//     */
//    private void generateTask(String schema_name, Map<String, Object> task, List<Map<String, Object>> searchReceiveAccount) {
//        try {
//            String pageType = SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_41);
//            Map<String, Object> finishMap = new HashMap<String, Object>();
//            //组织公共数据
//            String key_id = selectOptionService.getOptionNameByCode(schema_name, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_41, "relation");
//            String audit_man = task.get("audit_man").toString();
//            String task_title = task.get("task_title").toString();
//            int task_rate = (int) task.get("task_rate");
//            int important_level = (int) task.get("important_level");
//            String deliverable_time = task.get("deliverable_time").toString();
//
//            String create_user_account = task.get("create_user_account").toString();
//            finishMap.put("task_id", task.get("id"));
//            finishMap.put("create_user_account", task.get("create_user_account"));
//            finishMap.put("deliverables", task.get("deliverables"));
//            finishMap.put("task_content", task.get("task_content"));
//            finishMap.put("task_rate", task_rate);
//            finishMap.put("important_level", important_level);
//            finishMap.put("task_require", task.get("task_require"));
//            finishMap.put("deliverable_day", task.get("deliverable_day"));  // 暂无用
//            finishMap.put("deliverable_time", task.get("deliverable_time"));
//            finishMap.put("task_title", task_title);
//            Timestamp now = new Timestamp(System.currentTimeMillis());                  //获取当前时间
//            DateFormat dayFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//            Long deadTime = now.getTime() + 1 * (1000 * 3600 * 24);    // 默认延后一天
//            if (task_rate == 2) {
//                deadTime = now.getTime() + 7 * (1000 * 3600 * 24);
//            } else if (task_rate == 3) {
//                deadTime = now.getTime() + 30 * (1000 * 3600 * 24);
//            }
//            Timestamp deadlineTime = new Timestamp(deadTime);
//            String deadLineWord;
//            deadLineWord = dayFormat.format(deadlineTime);
//            if (null == deliverable_time || deliverable_time.isEmpty()) {
//                deadLineWord += " 23:59:59";
//            } else {
//                deadLineWord += " " + deliverable_time;
//            }
//
//            finishMap.put("deadline_time", deadLineWord);
//
//            //组织公共数据结束
//            for (Map<String, Object> receive : searchReceiveAccount) {
//                String sub_work_code = serialNumberService.generateMaxBusinessCode(schema_name, "task_release");//工单详情单号
//                String account = receive.get("account").toString();
//                finishMap.put("create_time", now);
//                finishMap.put("audit_man", audit_man);
//                finishMap.put("duty_man", account);
//                finishMap.put("sub_work_code", sub_work_code);
//                //调用流程----
//                String title = task_title + "自动任务";
//                Map<String, String> map = new HashMap<String, String>();
//                map.put("title_page", title);
//                map.put("sub_work_code", sub_work_code);
//                map.put("receive_account", account);
//                map.put("deadline_time", task.get("end_date").toString()); // 截止时间
//                map.put("occur_time", task.get("begin_date").toString()); // 发送时间
//                taskReleaseMapper.addFinished(schema_name, finishMap);//生成任务
//                WorkflowStartResult workflow = workflowService.startWithForm(schema_name, key_id, account, map);//调用流程
//                boolean flowSuccess = workflow.isStarted();//流程调用状态
//                if (!flowSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return;
//                }
//                String sendName = selectOptionService.getOptionNameByCode(schema_name, "user", create_user_account, null);
//                messageService.msgPreProcess(qcloudsmsConfig.SMS_10002003, account, SensConstant.BUSINESS_NO_41, sub_work_code,
//                        create_user_account, sendName, null, null); // 发送短信
//                workProcessService.saveWorkProccess(schema_name, sub_work_code, 0, task.get("create_user_account").toString(), Constants.PROCESS_CREATE_TASK); // 进度记录
//                logService.AddLog(schema_name, pageType, sub_work_code, selectOptionService.getLanguageInfo(LangConstant.LOG_PISH_TASK_SUCC), create_user_account); // 发布任务生成成功日志记录，解决任务发布人员记录的错误异常
//            }
//            //调用流程-流程调用成功生成
//        } catch (Exception e) {
//        }
//    }
//
//
//    /**
//     * 工作任务分析
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findWorkTaskAnalys(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String groups = request.getParameter("groups");
//        String begin_time = request.getParameter("begin_time");
//        String end_time = request.getParameter("end_time");
//        String keyWord = request.getParameter("keyWord");
//        String taskId = request.getParameter("taskId");
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        User user = AuthService.getLoginUser(request);
//        DataPermissionAllOrSelf allOrSelf = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusiness(schema_name, user, "task_release_analys");
//        if (DataPermissionAllOrSelf.Self_Facility.equals(allOrSelf)) {
//
//        }
//
//        //根据组权限，查询所属组的用户
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and ( su.username like '%" + keyWord + "%' " +
//                    "or upper(su.account) like upper('%" + keyWord + "%') " +
//                    "or upper(su.user_code) like upper('%" + keyWord + "%') " +
//                    ")";
//        }
//        if (groups != null && !groups.isEmpty()) {
//            condition += " and ug.group_id in (" + groups + ") ";
//        }
//        if (begin_time != null && !begin_time.isEmpty()) {
//            condition += " and wtf.create_time >='" + begin_time + "' ";
//        }
//        if (end_time != null && !end_time.isEmpty()) {
//            condition += " and wtf.create_time <='" + end_time + "' ";
//        }
//        if(StringUtils.isNotBlank(taskId)) {
//            condition += String.format(" and wt.id = %d ", Long.valueOf(taskId));
//        }
//
//        dataList = taskReleaseMapper.findWorkTaskAnalys(schema_name, condition, HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//        int total = taskReleaseMapper.countWorkTaskAnalys(schema_name, condition);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 工作任务分析列表导出
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ModelAndView workTaskAnalysExport(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        int pageSize = 50000;
//        String groups = request.getParameter("groups");
//        String begin_time = request.getParameter("begin_time");
//        String end_time = request.getParameter("end_time");
//        String keyWord = request.getParameter("keyWord");
//        String taskId = request.getParameter("taskId");
//        String taskTitle = request.getParameter("taskTitle");
//        //首页条件
//        String condition = "";
//        //根据组权限，查询所属组的用户
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and ( su.username like '%" + keyWord + "%' " +
//                    "or upper(su.account) like upper('%" + keyWord + "%') " +
//                    "or upper(su.user_code) like upper('%" + keyWord + "%') " +
//                    ")";
//        }
//        if (groups != null && !groups.isEmpty()) {
//            condition += " and ug.group_id in (" + groups + ") ";
//        }
//        if (begin_time != null && !begin_time.isEmpty()) {
//            condition += " and wtf.create_time >='" + begin_time + "' ";
//        }
//        if (end_time != null && !end_time.isEmpty()) {
//            condition += " and wtf.create_time <='" + end_time + "' ";
//        }
//        if(StringUtils.isNotBlank(taskId)) {
//            condition += String.format(" and wt.id = %d ", Long.valueOf(taskId));
//        }
//        List<Map<String, Object>> workTaskAnalyList = taskReleaseMapper.findWorkTaskAnalys(schema_name, condition, pageSize, 0);
//
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("workTaskAnalyList", workTaskAnalyList);
//        map.put("selectOptionService",selectOptionService);
//        map.put("taskTitle",StringUtils.isBlank(taskId)?selectOptionService.getLanguageInfo(LangConstant.ALL_WORK_TASK):taskTitle);
//        WorkTaskAnalyListDataExportView excelView = new WorkTaskAnalyListDataExportView();
//        return new ModelAndView(excelView, map);
//    }
//
//    /**
//     * 分析详情列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findWorkTaskAnalysDetail(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String groups = request.getParameter("groups");
//        String begin_time = request.getParameter("begin_time");
//        String end_time = request.getParameter("end_time");
//        String duty_man = request.getParameter("duty_man");
//        String type = request.getParameter("type");
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        User user = AuthService.getLoginUser(request);
//
//        if (groups != null && !groups.isEmpty()) {
//            condition += " and sug.group_id in (" + groups + ") ";
//        }
//        if (begin_time != null && !begin_time.isEmpty()) {
//            condition += " and wtf.create_time >='" + begin_time + "' ";
//        }
//        if (end_time != null && !end_time.isEmpty()) {
//            condition += " and wtf.create_time <='" + end_time + "' ";
//        }
//
//        switch (type) {
//            case "running":
//                condition += "and wtf.finished_time is null ";
//                break;//没完成的是进行中
//            case "finished":
//                condition += "and wtf.finished_time is not null ";
//                break;//完成
//            case "qualified":
//                condition += "and wtf.is_finished=1 ";
//                break;//满意
//            case "unqualified":
//                condition += "and wtf.is_finished=2 ";
//                break;//不满意
//            case "delay":
//                condition += "and wtf.finished_time is not null and substr(TO_CHAR(wtf.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "'), 11 ,17)>TO_CHAR(wtf.deliverable_time,'" + SqlConstant.SQL_TIME_FMT + "') ";
//                break;
//            default:
//                condition = "";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        dataList = taskReleaseMapper.findWorkTaskAnalysDetail(schema_name, duty_man, condition, pageSize, begin);
//        int total = taskReleaseMapper.countWorkTaskAnalysDetail(schema_name, duty_man, condition);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
}
