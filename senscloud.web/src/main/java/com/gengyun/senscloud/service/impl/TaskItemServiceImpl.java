package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.mapper.TaskItemMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.TaskItemModel;
import com.gengyun.senscloud.service.ExportService;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.TaskItemService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 任务项管理
 */
@Service
public class TaskItemServiceImpl implements TaskItemService {
    @Resource
    TaskItemMapper taskItemMapper;
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    FilesService filesService;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    CompanyService companyService;
    @Resource
    ExportService exportService;

    @Override
    public Map<String, Map<String, Boolean>> getTaskItemPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[8]);
    }

    /**
     * 添加任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    @Override
    public void newTaskItem(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_item_code = serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_TASK);
        String task_item_name = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_item_name(), LangConstant.TITLE_NAME_AB_I);
        Map<String, Object> pm = new HashMap<>();
        pm.put("task_item_code", task_item_code);
        pm.put("task_item_name", task_item_name);
        pm.put("result_type", taskItemModel.getResult_type());
        pm.put("requirements", taskItemModel.getRequirements());
        pm.put("result_verification", taskItemModel.getResult_verification());
        pm.put("create_user_id", methodParam.getUserId());
        taskItemMapper.insert(methodParam.getSchemaName(), pm);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7004, task_item_code, LangUtil.doSetLogArray("任务项新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AAQ_Q}));//任务项新建
    }

    /**
     * 编辑任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    @Override
    public void modifyTaskItem(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_item_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_item_code(), LangConstant.TITLE_AAQ_Q);
        Map<String, Object> pmOld = taskItemMapper.findTaskItemInfo(methodParam.getSchemaName(), task_item_code);
        Map<String, Object> pm = new HashMap<>();
        pm.put("task_item_code", task_item_code);
        pm.put("task_item_name", taskItemModel.getTask_item_name());
        pm.put("result_type", taskItemModel.getResult_type());
        pm.put("requirements", taskItemModel.getRequirements());
        pm.put("result_verification", taskItemModel.getResult_verification());
        JSONArray loger = LangUtil.compareMap(pm, pmOld);
        taskItemMapper.updateByCode(methodParam.getSchemaName(), pm);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7004, task_item_code, LangUtil.doSetLogArray("编辑了任务项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AAQ_Q, loger.toString()}));//编辑了任务项
    }

    /**
     * 删除任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    @Override
    public void cutTaskItem(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_item_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_item_code(), LangConstant.TITLE_AAQ_Q);
        //先判断该任务项是否绑定任务模板
        if (RegexUtil.optNotNull(taskItemMapper.findTaskItemBindingTemplate(methodParam.getSchemaName(), task_item_code)).isPresent()) {
            throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AAQ_E, LangConstant.TITLE_AAQ_Q, LangConstant.TITLE_AAQ_Q});//任务模板下有任务项 任务项不能删除
        }
        Map<String, Object> pmOld = taskItemMapper.findTaskItemInfo(methodParam.getSchemaName(), task_item_code);
        taskItemMapper.deleteByCode(methodParam.getSchemaName(), task_item_code);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7004, task_item_code, LangUtil.doSetLogArray("删除了任务项", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AAQ_Q, pmOld.get("task_item_name").toString()}));//删除了任务项
    }

    /**
     * 获取任务项详情
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     */
    @Override
    public Map<String, Object> getTaskItemInfo(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_item_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_item_code(), LangConstant.TITLE_AAQ_Q);
        return taskItemMapper.findTaskItemInfo(methodParam.getSchemaName(), task_item_code);
    }

    /**
     * 分页查询任务项
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     * @return 任务项列表
     */
    @Override
    public Map<String, Object> getTaskItemList(MethodParam methodParam, TaskItemModel taskItemModel) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        Map<String, Object> params = new HashMap<>();
        params.put("keywordSearch", taskItemModel.getKeywordSearch());
        //查询该条件下的总记录数
        Map<String, Object> result = new HashMap<>();
        int total = taskItemMapper.findTaskItemListCount(methodParam.getSchemaName(), params);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
        }
        params.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = taskItemMapper.findTaskItemList(methodParam.getSchemaName(), params);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 根据选中主键导出任务项信息（文件码）
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @param isAll         是否是全选
     * @return 文件码
     */
    @Override
    public String getExportTaskItem(MethodParam methodParam, TaskItemModel taskItemModel, boolean isAll) {
        return exportService.doDownloadTaskItem(methodParam, this.getTaskItemListNoPage(methodParam, taskItemModel, isAll));
    }

    /**
     * 查询任务模板、任务项、组信息列表
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @param isAll         是否是全选
     * @return 信息列表
     */
    private List<Map<String, Object>> getTaskItemListNoPage(MethodParam methodParam, TaskItemModel taskItemModel, boolean isAll) {
        Map<String, Object> params = new HashMap<>();
        params.put("keywordSearch", taskItemModel.getKeywordSearch());
        if (!isAll) {
            params.put("task_item_codes", RegexUtil.optStrToArray(taskItemModel.getTask_item_codes(), LangConstant.TITLE_AAQ_Q));
        }
        int total = taskItemMapper.findTaskItemListCount(methodParam.getSchemaName(), params);
        RegexUtil.falseExp(total > 0, LangConstant.TEXT_K, new String[]{LangConstant.BTN_EXPORT_C});
        return RegexUtil.optListOrExpNullInfo(taskItemMapper.findTaskItemList(methodParam.getSchemaName(), params), LangConstant.BTN_EXPORT_C);
    }

    /**
     * 任务项选择列表
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     * @return 任务项列表
     */
    @Override
    public List<Map<String, Object>> getChoseTaskItemList(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_template_code(), LangConstant.TITLE_AAQ_E);
        Map<String, Object> params = new HashMap<>();
        params.put("task_template_code", task_template_code);
        params.put("keywordSearch", taskItemModel.getKeywordSearch());
        return findChoseTaskItemList(methodParam.getSchemaName(), params, methodParam.getUserLang(), methodParam.getCompanyId());
    }

    /**
     * 根据任务模板编码查询任务项列表
     *
     * @param methodParam   入参
     * @param taskItemModel 入参
     * @return 任务项列表
     */
    @Override
    public List<Map<String, Object>> getTaskItemListByTempLateCode(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_template_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_template_code(), LangConstant.TITLE_AAQ_E);
        Map<String, Object> params = new HashMap<>();
        String[] task_template_codes = task_template_code.split(",");
        params.put("task_template_codes", task_template_codes);
        params.put("keywordSearch", taskItemModel.getKeywordSearch());
        return taskItemMapper.findTaskItemListByTempLateCode(methodParam.getSchemaName(), params, methodParam.getUserLang(), methodParam.getCompanyId());
    }

    /**
     * 任务项附件处理
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @param isAdd         是否新增
     */
    @Override
    public void doModifyTaskItemFile(MethodParam methodParam, TaskItemModel taskItemModel, boolean isAdd) {
        String file_id = RegexUtil.optNotBlankStrOrExp(taskItemModel.getFile_id(), LangConstant.MSG_B, new String[]{LangConstant.TITLE_AAAA_Q}); // 文档未选择
        String task_item_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_item_code(), LangConstant.TITLE_AAQ_Q);
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> taskItemMap = taskItemMapper.findTaskItemInfo(schemaName, task_item_code);
        if (isAdd || RegexUtil.optIsPresentStr(taskItemMap.get("file_ids"))) {
            Map<String, Object> taskItem = new HashMap<>();
            taskItem.put("task_item_code", task_item_code);
            taskItem.put("file_ids", isAdd ? RegexUtil.optNotBlankStrOpt(taskItemMap.get("file_ids")).map(s -> s.concat(",").concat(file_id)).orElse(file_id) :
                    RegexUtil.optNotBlankStrOpt(taskItemMap.get("file_ids")).map(s -> DataChangeUtil.removeStrForStrings(s, file_id)).orElse(null));
            taskItemMapper.updateByCode(schemaName, taskItem);
        }
        List<String> file_original_names = filesService.getFileNamesByIds(methodParam.getSchemaName(), taskItemModel.getFile_id());
        if (isAdd) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7004, task_item_code, LangUtil.doSetLogArray("添加了附件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_ADD_A, LangConstant.TAB_DOC_A, String.join(",", file_original_names)}));
        } else {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7004, task_item_code, LangUtil.doSetLogArray("删除了附件", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_DOC_A, String.join(",", file_original_names)}));
        }
    }

    /**
     * 获取任务项附件文档信息
     *
     * @param methodParam   系统参数
     * @param taskItemModel 请求参数
     * @return 列表数据
     */
    @Override
    public List<Map<String, Object>> getTaskItemFileList(MethodParam methodParam, TaskItemModel taskItemModel) {
        String task_item_code = RegexUtil.optStrOrExpNotNull(taskItemModel.getTask_item_code(), LangConstant.TITLE_AAQ_Q);
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> taskItemMap = taskItemMapper.findTaskItemInfo(schemaName, task_item_code);
        return RegexUtil.optNotBlankStrOpt(taskItemMap.get("file_ids")).map(ids -> taskItemMapper.findTaskItemFileList(schemaName, ids, taskItemModel.getKeywordSearch())).orElse(new ArrayList<>());
    }

    /**
     * 验证任务项编号是否存在
     *
     * @param schemaName
     * @param itemCodeList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> findItemsByItemCode(String schemaName, List<String> itemCodeList) {
        return taskItemMapper.findItemsByItemCode(schemaName, itemCodeList);
    }

    /**
     * 验证任务项编号是否存在
     *
     * @param schemaName   数据库
     * @param itemCodeList 任务项编号
     * @return 任务项编号集合
     */
    @Override
    public List<String> getItemCodesByCodes(String schemaName, List<String> itemCodeList) {
        return taskItemMapper.findItemCodesByCodes(schemaName, itemCodeList);
    }

    /**
     * 批量新增或更新任务项
     *
     * @param schemaName 数据库
     * @param itemList   任务项信息
     */
    @Override
    public void batchInsertItem(String schemaName, List<Map<String, Object>> itemList) {
        taskItemMapper.insertOrUpdateItem(schemaName, itemList);
    }

    public List<Map<String, Object>> findChoseTaskItemList(String schema_name, Map<String, Object> pm, String user_lang, Long company_id) {
        List<Map<String, Object>> list = taskItemMapper.findChoseTaskItemList(schema_name, pm);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(company_id, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.get("result_type_name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("result_type_name"))).get(user_lang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("result_type_name", value);
                        }
                    }
                });
            }
        }
        return list;
    }
}
