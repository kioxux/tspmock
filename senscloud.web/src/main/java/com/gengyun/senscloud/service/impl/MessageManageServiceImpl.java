package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.MessageManageMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.MessageManageService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 消息管理中心
 */
@Service
public class MessageManageServiceImpl implements MessageManageService {
    @Resource
    MessageManageMapper messageManageMapper;
    @Resource
    PagePermissionService pagePermissionService;

    /**
     * 获取菜单信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getMessageManagePermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[17]);
    }

    /**
     * 根据用户分页查询消息列表
     */
    @Override
    public Map<String, Object> findMessagePageByReceiver(MethodParam methodParam, Map<String, Object> pm) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        pm.put("receive_user_id", methodParam.getUserId());
        Integer total = messageManageMapper.findMessagePageByReceiverCount(methodParam.getSchemaName(), pm);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        pm.put("pagination", pagination);
        List<Map<String, Object>> rows = messageManageMapper.findMessagePageByReceiver(methodParam.getSchemaName(), pm);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }


    /**
     * 根据消息id和阅读人更新为已读状态
     */
    @Override
    public void modifyMessageReceiverRead(MethodParam methodParam, Map<String, Object> pm) {
        Integer message_id = RegexUtil.optIntegerOrExpParam(pm.get("message_id"), LangConstant.MSG_G);
        messageManageMapper.updateMessageReceiverRead(methodParam.getSchemaName(), methodParam.getUserId(), message_id);
    }

    /**
     * 查询接收人的未读消息数量
     */
    @Override
    public Integer getNoReadCountByReceiver(MethodParam methodParam, Map<String, Object> pm) {
        return messageManageMapper.findNoReadCountByReceiver(methodParam.getSchemaName(), methodParam.getUserId());
    }

    /**
     * 根据接收人查询最新三个未读消息
     */
    @Override
    public List<Map<String, Object>> getNoReadThreeCountByReceiver(MethodParam methodParam, Map<String, Object> pm) {
        return messageManageMapper.findNoReadThreeCountByReceiver(methodParam.getSchemaName(), methodParam.getUserId());
    }

    /**
     * 根据消息id获取消息详情
     */
    @Override
    public Map<String, Object> getMessageInfoById(MethodParam methodParam, Map<String, Object> pm) {
        Integer message_id = RegexUtil.optIntegerOrExpParam(pm.get("message_id"), LangConstant.MSG_G);
        return messageManageMapper.findMessageInfoById(methodParam.getSchemaName(), message_id, methodParam.getUserId());
    }

    /**
     * 发送消息
     */
    @Override
    public void insertMessageRecord(MethodParam methodParam,
                                    String msg_content,
                                    String business_type,
                                    String business_no,
                                    Integer type,
                                    String receive_mobile,
                                    String receive_content,
                                    String message_key,
                                    String message_param,
                                    List<String> receivers,
                                    List<String> ccUsers) {
        Map<String, Object> pm = new HashMap<>();
        pm.put("msg_content", msg_content);
        pm.put("business_type", business_type);
        pm.put("business_no", business_no);
        pm.put("type", type);
        pm.put("message_key", message_key);
        pm.put("message_param", message_param);
        pm.put("receive_mobile", receive_mobile);
        pm.put("send_user_id", methodParam.getUserId());
        //新增消息主题
        messageManageMapper.insertMessageRecord(methodParam.getSchemaName(), pm);
        Integer message_id = RegexUtil.optIntegerOrExpParam(pm.get("id"), LangConstant.MSG_G);
        //添加主接收人
        if (RegexUtil.optNotNull(receivers).isPresent() && receivers.size() > 0) {
            for (String receiver_id : receivers) {
                messageManageMapper.insertMessageReceiver(methodParam.getSchemaName(), message_id, receiver_id, "1", "-1", "1", receive_content);
            }
            if (RegexUtil.optNotNull(ccUsers).isPresent() && ccUsers.size() > 0) {
                //添加抄送人
                for (String cc_user_id : ccUsers) {
                    messageManageMapper.insertMessageReceiver(methodParam.getSchemaName(), message_id, cc_user_id, "-1", "-1", "1", receive_content);
                }
            }
        }
    }
}
