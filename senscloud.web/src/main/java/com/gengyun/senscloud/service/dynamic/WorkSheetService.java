package com.gengyun.senscloud.service.dynamic;

public interface WorkSheetService {
//    /**
//     * 工单列表计数
//     *
//     * @param schema_name
//     * @param condition
//     * @param dySqlMap
//     * @return
//     */
//    int getWorkSheetListCount(String schema_name, String condition, Map<String, String> dySqlMap);
//
//    List<String> getWorkSheetSubWorkcodeList(String schema_name, String condition, int pageSize, int begin);
//
//    /**
//     * 查询工单列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param dySqlMap    [feeSelectSql:工单费用信息查询sql]
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    List<WorkSheet> getWorkSheetList(String schema_name, String condition, Map<String, String> dySqlMap, int pageSize, int begin);
//
//    //按照id查询
////    WorkSheet findByID(String schema_name, String work_code);
//
//    //列表service
//    String findWorkSheetList(HttpServletRequest request);
//
//
//    //按照id查询
//    WorkListDetailModel finddetilByID(String schema_name, String sub_work_code);
//
//    /**
//     * 查询工单详情
//     * @param schema_name
//     * @param sub_work_code
//     * @return
//     */
//    Map<String, Object> findWorkSheetDetailBySubWorkCode(String schema_name, String sub_work_code);
//
//    //修改工单概要
//    int updateworkList(String schema_name, WorkListModel workListModel);
//
//    //修改工单概要
//    int updateworkSheet(String schema_name, WorkSheet workSheet);
//
//    //修改工单详情
//    int updateWorkDetailList(String schema_name, WorkListDetailModel workListDetailModel);
//
//    //查找维修单列表，日历用20180329
//    List<WorkSheet> getWorkSheetListforcalendar(String schema_name, String condition);
//
////    //查找维修单子列表，日历用20180329
////    List<WorkListDetailModel> getSubWorkList(String schema_name, String work_code);
//
//    //查找维修单子列表
//    List<Map<String, Object>> getSubWorkMapList(String schema_name, String work_code);
//
//    //查找工单类型，及其工单总数
//    List<WorkTypeData> getWorkTypeAndBillCountList(String schema_name, String condition);
//
//    //查找时间内有无工单新增小程序问题上报用
//    int getRepairCountByAssetIdRecently(String schema_name, String asset_id, Timestamp current);
//
//    //查找时间内有无服务请求
//    int getRepairServiceCountByAssetIdRecently(String schema_name, String asset_id, Timestamp current);
//
//    /**
//     * 根据任务模板名称查询任务模板、任务项数据
//     *
//     * @param schema_name
//     * @param templateNames
//     * @return
//     */
//    List<Map<String, Object>> getTaskTemplateAndTaskItemByTemplateName(String schema_name, String templateNames);
//
//    /**
//     * 查询工单事前、事后照片
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @return
//     */
//    Map<String, Object> findWorkSheetImgBySubWorkCode(String schema_name, String sub_work_code);
//
//    /**
//     * 修改工单信息（修改已完成工单的工单费用）
//     *
//     * @param schema_name
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    ResponseModel workSheetFix(String schema_name, Map<String, Object> paramMap, HttpServletRequest request)throws Exception;
}
