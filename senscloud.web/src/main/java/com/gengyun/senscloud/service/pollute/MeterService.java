package com.gengyun.senscloud.service.pollute;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

public interface MeterService {
    Map<String, Object> findMeterList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findMeterAssertList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findMeterItemList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findMeterRecordList(MethodParam methodParam, Map<String, Object> paramMap);

    void modifyMeterItem(MethodParam methodParam, Map<String, Object> paramMap);

    void removeMeterRecord(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> findMeterCalendar(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String,Object> findMeterStatistics(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String,Object> findGuessMeter(MethodParam methodParam, Map<String, Object> paramMap);

    void modifyGuessMeterItem(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String,Object> findMeterSewageRecords(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> meterSewageRecordsChart(MethodParam methodParam, Map<String, Object> paramMap);

    void asyncCronJobToGenerateAbnormalSewageMsg(String schemaName);
}
