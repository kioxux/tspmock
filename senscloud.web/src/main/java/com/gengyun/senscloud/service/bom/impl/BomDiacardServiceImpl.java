package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomDiacardService;
import org.springframework.stereotype.Service;

/**
 * 备件报废
 */
@Service
public class BomDiacardServiceImpl implements BomDiacardService {
//    @Autowired
//    private BomDiacardMapper bomDiacardMapper;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private CommonUtilService commonUtilService;
//
//    @Autowired
//    private DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    private LogsService logService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private WorkProcessService workProcessService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    private PagePermissionService pagePermissionService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    BomInventoryMapper bomInventoryMapper;
//
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//
//    @Autowired
//    BomMapper bomMapper;
//
//    @Autowired
//    BomStockListService bomStockListService;
//
//    @Autowired
//    SelectOptionCustomMapper selectOptionCustomMapper;
//
//    @Autowired
//    BomInStockService bomInStockService;
//
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//
//    @Autowired
//    MessageService messageService;
//
//    /**
//     * 获取备件报废列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBomDiacardList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String[] facilitySearch = {};
//        try {
//            facilitySearch = request.getParameterValues("facilities")[0].split(",");
//        } catch (Exception ex) {
//
//        }
//        String keyWord = request.getParameter("keyWord");
//        String status = request.getParameter("status");
//
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        User user = AuthService.getLoginUser(request);
//        //根据设备组织查询条件，查询用户可见的库房，用于查询备件
//        String stockPermissionCondition = dataPermissionForFacility.getStockPermissionByFacilityAndGroup(schema_name, user, facilitySearch, "stock", null);
//
//        if (StringUtils.isNotBlank(keyWord)) {
//            condition += " and (s.stock_name like '%" + keyWord + "%' or upper(br.discard_code) like upper('%" + keyWord +
//                    "%') or us.username like '%" + keyWord + "%' )";
//        }
//        if (StringUtils.isNotBlank(status)) {
//            condition += " and br.status = " + status;
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        dataList = bomDiacardMapper.getBomDiacardList(schema_name, condition, stockPermissionCondition, pageSize, begin);
//        int total = bomDiacardMapper.countBomDiacardList(schema_name, condition, stockPermissionCondition);
//        handlerFlowData(schema_name, user.getAccount(), dataList);
//        //多时区处理
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("discard_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        //把taskId、权限数据补充到列表数据中去
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("discard_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//
//    /**
//     * 根据主键查询备件报废数据
//     *
//     * @param schemaName
//     * @param discardCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryBomDiacardById(String schemaName, String discardCode) {
//        return bomDiacardMapper.queryBomDiacardById(schemaName, discardCode);
//    }
//
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String source) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        // 提交按钮
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> bomDiscard = (Map<String, Object>) dataInfo.get("_sc_bom_discard");
//        Map<String, Object> keys = (Map<String, Object>) map_object.get("keys");
//        String key = (String) keys.get("_sc_bom_discard");
//        boolean isNew = key.startsWith(SqlConstant.IS_NEW_DATA) ? true : false;//判断是否是新增
//        String work_request_type = "";
//        try {
//            work_request_type = (String) map_object.get("work_request_type");
//        } catch (Exception statusExp) {
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        String roleIds = (String) map_object.get("roleIds");
//        return addBomDiscard(schema_name, doFlowKey, user, processDefinitionId, roleIds, bomDiscard, map_object, null, isNew, hasAudit);
//    }
//
//    /**
//     * 新增备件报废申请
//     *
//     * @param schema_name
//     * @param doFlowKey
//     * @param user
//     * @param processDefinitionId
//     * @param roleIds
//     * @param businessInfo
//     * @param map_object
//     * @param source
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel addBomDiscard(String schema_name, String doFlowKey, User user, String processDefinitionId, String roleIds, Map<String, Object> businessInfo,
//                                       Map<String, Object> map_object, String source, boolean isNew, boolean hasAudit) throws Exception {
//        // 提交按钮
//        if ((null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT)))
//                || StringUtils.isNotBlank(source)) {
//            int status;//状态
//            if (hasAudit) {
//                status = StatusConstant.TO_BE_AUDITED;//如果是带审核节点的流程，则订单状态为“待审核”
//            } else {
//                status = StatusConstant.COMPLETED;//如果流程不需要审核，则订单状态为“已完成”
//            }
//            String account = user.getAccount();
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            String code = (String) businessInfo.get("discard_code");
//            businessInfo.put("status", status);
//            businessInfo.put("create_user_account", account);
//            businessInfo.put("create_time", now);
//            if (isNew) {
//                workSheetHandleMapper.insert(schema_name, businessInfo, "_sc_bom_discard"); //保存备件报废主表信息
//            } else {
//                workSheetHandleMapper.update(schema_name, businessInfo, "_sc_bom_discard", "discard_code"); // 修改
//            }
//            //保存入库详情表信息
//            ResponseModel result = doSaveBomInfo(schema_name, code, map_object);
//            if (result != null) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return result;
//            }
//            String title = selectOptionService.getLanguageInfo(LangConstant.PART_SCRAP); // 备件报废
//            //流程如果没有审核节点，则直接审核通过，并变更备件入库信息
//            if (!hasAudit) {
//                int res = 0;
//                status = StatusConstant.COMPLETED;
//                res = bomDiacardMapper.updateBomDiscardStatus(schema_name, status, code);//更新附件，修改状态为“已完成”
//                if (res > 0) {
//                    updateBomDiscardQuantity(schema_name, account, businessInfo, false);
//                    //如果时从备件盘点发起的入库申请，更新盘点备件出入库处理结果
//                    if (StringUtils.isNotBlank(source) && source.startsWith("BID")) {
//                        Object bomData = map_object.get("bomContent3");
//                        JSONArray bomArrays = JSONArray.fromObject(bomData); // 页面数据列表
//                        updateBomInventoryStatus(schema_name, bomArrays.getJSONObject(0).getString("bom_code"), bomArrays.getJSONObject(0).getString("material_code"), source);
//                    }
//                }
//            } else {
//                updateBomDiscardQuantity(schema_name, account, businessInfo, true);//验证库存数量
//            }
//            //流程数据
//            Map map = new HashMap<>();
//            map.put("title_page", title);//备件报废
//            map.put("sub_work_code", code);
//            map.put("workCode", code);
//            map.put("do_flow_key", doFlowKey);
//            map.put("status", status);
//            map.put("create_user_account", account);//发送短信人
//            map.put("create_time", UtilFuns.sysTime());
//            map.put("facility_id", null);
//            // 设置处理人（随机取一个库房管理人）
//            String receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) businessInfo.get("stock_code"));
//            map.put("receive_account", receive_account); // 短信接受人
//            if (isNew) {
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_FLOW_START_FAIL));//备件报废流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, code, status, account, Constants.PROCESS_CREATE_TASK); // 进度记录
//            } else {
//                String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, code);
//                if (StringUtils.isBlank(taskId))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_FLOW_ERROR_APPLY_FAIL));//备件报废流程信息异常，备件报废申请失败
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, code);
//                boolean isSuccess = workflowService.complete(schema_name, taskId, account, map);
//                if (!isSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_FLOW_START_FAIL));//备件报废流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, code, status, account, taskSid); // 进度记录
//            }
//            messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_34, code,
//                    account, user.getUsername(), null, null); // 发送短信
//            String logInfo = (isNew ? selectOptionService.getLanguageInfo(LangConstant.NEW_K) :
//                    selectOptionService.getLanguageInfo(LangConstant.MOD_A)) +
//                    selectOptionService.getLanguageInfo(LangConstant.BOM_DISCARD_APPLY_SUCC);
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_34), code,
//                    logInfo, account); // 记录历史;创建;修改;备件报废申请成功
//            return ResponseModel.ok("ok");
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    /**
//     * 备件入库审核
//     *
//     * @param schema_name
//     * @param request
//     * @param paramMap
//     * @param user
//     * @return
//     */
//    @Override
//    public ResponseModel bomDiscardAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, boolean isLastAudit) throws Exception {
//        String title = selectOptionService.getLanguageInfo(LangConstant.PART_SCRAP);//备件报废
//
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_INFO_WRONG));//当前备件报废信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> bom_in_stock_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_discard");
//        String body_property = (String) bom_in_stock_tmp.get("body_property");
//        String discard_code = (String) bom_in_stock_tmp.get("discard_code");
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String roleIds = (String) map_object.get("roleIds");
//
//        if (StringUtils.isBlank(discard_code) || StringUtils.isBlank(do_flow_key))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//
//        Map<String, Object> bomInStock = bomDiacardMapper.queryBomDiscardById(schema_name, discard_code);
//        if (bomInStock == null)
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_DATE_ERROR_AUDIT_FAIL), title, title));//%s信息异常，%s审核失败
//
//        if (!String.valueOf(bomInStock.get("status")).equals(String.valueOf(StatusConstant.TO_BE_AUDITED)))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_STATUS_ERROR_AUDIT_FAIL), title, title));//%s状态异常，%s审核失败
//
//        String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, discard_code);
//        if (StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_FLOW_ERROR_AUDIT_FAIL), title, title));//%s流程信息异常，%s审核失败
//
//        int result = 0;
//        int status = 0;
//        if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key) && isLastAudit) {//如果审核同意，则更新调拨申请附件
//            status = StatusConstant.COMPLETED;
//            result = bomDiacardMapper.updateBomDiscardStatus(schema_name, status, discard_code);//更新附件，修改状态为“已完成”
//            if (result > 0) {
//                updateBomDiscardQuantity(schema_name, user.getAccount(), bomInStock, false);
//                //如果时从备件盘点发起的报废申请，更新盘点备件出入库处理结果
//                String source_sub_code = (String) bomInStock.get("source_sub_code");
//                if (StringUtils.isNotBlank(source_sub_code) && source_sub_code.startsWith("BID")) {
//                    List<Map<String, Object>> bomDiscardDetails = bomDiacardMapper.queryBomDiscardkDetailById(schema_name, discard_code);
//                    Map<String, Object> bomDiscardDetail = bomDiscardDetails.get(0);
//                    updateBomInventoryStatus(schema_name, (String) bomDiscardDetail.get("bom_code"), (String) bomDiscardDetail.get("material_code"), source_sub_code);
//                }
//            }
//        } else if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {//如果审核退回，则调拨申请状态变更为“处理中”
//            status = StatusConstant.PENDING;
//            result = bomDiacardMapper.updateBomDiscardStatus(schema_name, status, discard_code);
//        } else {
//            result = bomDiacardMapper.updateBomDiscardBodyProperty(schema_name, discard_code, body_property);
//        }
//        if (result > 0) {
//            String title_page = "";
//            String logRemark = "";
//            Map<String, String> map = new HashMap<>();
//            if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//                title_page = title + selectOptionService.getLanguageInfo(LangConstant.RETURN_A);//退回
//                logRemark = title + selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK);//审核退回
//                map.put("receive_account", (String) bomInStock.get("create_user_account"));//短信接受人
//            } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//                title_page = title + selectOptionService.getLanguageInfo(LangConstant.SUCC_A);//成功
//                logRemark = title + selectOptionService.getLanguageInfo(LangConstant.AUDIT_SUCC);//审核成功
//                // 设置处理人（随机取一个库房管理人）
//                String receive_account = bomInStockService.getBomStockUserRandom(schema_name, roleIds, (String) bomInStock.get("stock_code"));
//                map.put("receive_account", receive_account); // 短信接受人
//            }
//            //完成流程
//            map.put("sub_work_code", discard_code);
//            map.put("title_page", title_page);
//            map.put("do_flow_key", do_flow_key);
//            map.put("facility_id", null);
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, discard_code);
//            boolean isSuccess = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//            if (isSuccess) {
//                workProcessService.saveWorkProccess(schema_name, discard_code, status, user.getAccount(), taskSid);//工单进度记录
//                messageService.msgPreProcess(ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)?qcloudsmsConfig.SMS_10002006:qcloudsmsConfig.SMS_10002005, map.get("receive_account"), SensConstant.BUSINESS_NO_34, discard_code,
//                        user.getAccount(), user.getUsername(), null, null); // 发送短信
//                logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_34), discard_code, logRemark, user.getAccount());
//                return ResponseModel.ok("ok");
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_AUDIT_ERROR));//备件报废审核出现错误，请重试
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作出现错误，请重试
//        }
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String result = this.findBomDiacardList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("discard_code");
//        int status = Integer.parseInt(String.valueOf(bomDiacardMapper.queryBomDiacardById(schemaName, code).get("status")));
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        if (StatusConstant.COMPLETED != status && StatusConstant.CANCEL != status) {
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_34, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 备件报废的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/bom_discard/findSingleBdInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//
//    /**
//     * 保存备件报废详情表数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @param map
//     */
//    private ResponseModel doSaveBomInfo(String schemaName, String subWorkCode, Map<String, Object> map) throws Exception {
//        if (map.containsKey("bomContent3")) {
//            bomDiacardMapper.deleteBySubWorkCode(schemaName, "_sc_bom_discard_detail", subWorkCode);// 删除原数据
//            Object bomData = map.get("bomContent3");
//            if (null != bomData && !"".equals(bomData)) {
//                JSONArray bomArrays = JSONArray.fromObject(bomData); // 页面数据列表
//                int bomSize = bomArrays.size();
//                if (bomSize > 0) {
//                    List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//                    Map<String, Object> bomRow = null;
//                    int i = 0;
//                    for (Object rowData : bomArrays) {
//                        net.sf.json.JSONObject bomRowJson = net.sf.json.JSONObject.fromObject(rowData);
//                        try {
//                            bomRowJson.getInt("use_count");
//                            bomRowJson.getDouble("leave_cost");
//                        } catch (Exception e) {
//                            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COUNT_OR_LEAVE_COST_IS_NULL));//数量或残值不能为空
//                        }
//                        bomRow = new HashMap<String, Object>();
//                        bomRow.putAll(bomRowJson);
//                        bomRow.put("quantity", bomRow.get("use_count"));
//
//                        bomRow.put("discard_code", subWorkCode);//主表主键
//                        dataList.add(bomRow);
//                        i++;
//                        if (i == 10) {//每次插入10条到数据库
//                            selectOptionService.doBatchInsertSql(schemaName, "_sc_bom_discard_detail", SqlConstant._sc_bom_discard_detail_columns, dataList);
//                            dataList = new ArrayList<Map<String, Object>>();
//                            i = 0;
//                        }
//                    }
//                    if (i > 0) {
//                        selectOptionService.doBatchInsertSql(schemaName, "_sc_bom_discard_detail", SqlConstant._sc_bom_discard_detail_columns, dataList);
//                    }
//                }
//            }
//        }
//        return null;
//    }
//
//    /**
//     * 修改库存数量
//     *
//     * @param schema_name
//     * @param account
//     * @param quantity
//     * @param bom_code
//     * @param material_code
//     * @param stock_code
//     * @param security_quantity
//     * @param max_security_quantity
//     * @throws Exception
//     */
//    private void updateBomStatus(String schema_name, String account, String quantity, String bom_code, String material_code, String stock_code, String security_quantity, String max_security_quantity, boolean validQuantity) throws Exception {
//        Map<String, Object> bomRow = new HashMap<>();
//        bomRow.put("isAddStock", "isDisCard"); // 入库
//        bomRow.put("bom_code", bom_code);
//        bomRow.put("material_code", material_code);
//        bomRow.put("quantity", "-" + quantity);
//        bomRow.put("stock_code", stock_code);
//        bomRow.put("security_quantity", security_quantity);
//        bomRow.put("max_security_quantity", max_security_quantity);
//        if (validQuantity) //验证库存数量
//            bomStockListService.checkBomStockQuantity(schema_name, bomRow, null); // 更新备件库存数量;入库数量：
//        else//更新库存数量
//            bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_34, bomRow, account, selectOptionService.getLanguageInfo(LangConstant.DISCARD_QUANTITY) + quantity); // 更新备件库存数量;报废数量：
//    }
//
//    /**
//     * 备件库房数量更新
//     *
//     * @param schema_name
//     * @param account
//     */
//    private void updateBomDiscardQuantity(String schema_name, String account, Map<String, Object> bomInStock, boolean validQuantity) throws Exception {
//        String in_code = (String) bomInStock.get("discard_code");
//        List<Map<String, Object>> bomInStockDetails = bomDiacardMapper.queryBomDiscardkDetailById(schema_name, in_code);
//        if (bomInStockDetails != null && !bomInStockDetails.isEmpty()) {
//            for (Map<String, Object> bomInStockDetail : bomInStockDetails) {
//                updateBomStatus(schema_name, account, String.valueOf(bomInStockDetail.get("quantity")), (String) bomInStockDetail.get("bom_code"), (String) bomInStockDetail.get("material_code"),
//                        (String) bomInStock.get("stock_code"), "0", "0", validQuantity);
//            }
//        }
//    }
//
//    /**
//     * 入库申请作废
//     *
//     * @param schema_name
//     * @param user
//     * @param discard_code
//     * @return
//     */
//    @Override
//    public ResponseModel cancel(String schema_name, User user, String discard_code) {
//        int doUpdate = bomDiacardMapper.updateBomDiscardStatus(schema_name, StatusConstant.CANCEL, discard_code);
//        if (doUpdate > 0) {
//            boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, discard_code, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            if (deleteflow) {
//                String account = user.getAccount();
//                workProcessService.saveWorkProccess(schema_name, discard_code, StatusConstant.CANCEL, account, Constants.PROCESS_CANCEL_TASK);//报废进度记录
//                logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_34), discard_code, selectOptionService.getLanguageInfo(LangConstant.MSG_SPARE_SCRAP_CANCEL), account);//备件报废申请作废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SPARE_SCRAP_CANCEL_SUCC));//备件报废申请作废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_CANCEL_FLOW_FAIL));//备件报废作废流程调用失败
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DISCARD_CANCEL_FAIL));//备件报废作废失败
//        }
//    }
//
//    /**
//     * 更新备件盘点状态
//     *
//     * @param schemaName
//     * @param bomCode
//     * @param materialCode
//     * @param sourceSubCode
//     * @return
//     */
//    private void updateBomInventoryStatus(String schemaName, String bomCode, String materialCode, String sourceSubCode) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("bom_code", bomCode);
//        map.put("material_code", materialCode);
//        map.put("deal_result", 3);//处理结果为“已报废”
//        map.put("sub_inventory_code", sourceSubCode);
//        bomInventoryMapper.updateBomInventoryStockDetailDealResult(schemaName, map);
//    }
}
