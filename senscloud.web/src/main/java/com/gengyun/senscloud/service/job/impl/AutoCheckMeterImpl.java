package com.gengyun.senscloud.service.job.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.mapper.LogsMapper;
import com.gengyun.senscloud.mapper.MeterMapper;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class AutoCheckMeterImpl {
    private static final Logger logger = LoggerFactory.getLogger(AutoCheckMeterImpl.class);
    @Resource
    MeterMapper meterMapper;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    LogsMapper logsMapper;

    @Transactional
    public void autoCheckMeter(String schemaName) {
        //数仓请求地址
        String sc_url = systemConfigService.getSysCfgValueByCfgName(schemaName, "sc_url");
        //自动抄表
        List<Map<String, Object>> meterList = new ArrayList<>();
        List<Map<String, Object>> needMeterCusAssetList = RegexUtil.optNotNullListOrNew(meterMapper.findNeedMeterCusAssetList(schemaName));
        List<Map<String, Object>> needMeterItemList = RegexUtil.optNotNullListOrNew(meterMapper.findNeedMeterItemList(schemaName));
        needMeterCusAssetList.forEach(e -> {
            e.put("check_item", "0");
            meterList.add(e);
            HashMap<String, Object> cloneMap = (HashMap<String, Object>) ObjectUtil.cloneByStream(e);
            cloneMap.put("check_item", "1");
            meterList.add(cloneMap);
        });
        needMeterItemList.forEach(e -> {
            if (Objects.equals(e.get("check_item"), "0")) {
                e.put("status", "1");
                e.put("meter_type", "2");
                if (RegexUtil.optIsPresentStr(e.get("sid_url")) || (RegexUtil.optIsPresentStr(e.get("grm")) && RegexUtil.optIsPresentStr(e.get("pass")))) {
                    List<String> exDataList = this.exData(RegexUtil.optStrOrBlank(e.get("sid_url")), schemaName, e);
                    if (exDataList.size() > 0 && exDataList.contains("ERROR")) {
                        if (RegexUtil.optIsPresentStr(e.get("grm")) && RegexUtil.optIsPresentStr(e.get("pass"))) {
                            String exDataUrl = this.authYunplc(schemaName, e);
                            if (RegexUtil.optIsPresentStr(exDataUrl)) {
                                e.put("sid_url", exDataUrl);
                                meterMapper.updateCusAssetSid(schemaName, e);
                                exDataList = this.exData(exDataUrl, schemaName, e);
                            }
                        }
                    }
                    e.put("current_flow", new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 8), "0")));
                    e.put("indication", new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 9), "0"))
                            .subtract(new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 7), "0"))));
                    e.put("last_indication", new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 9), "0"))
                            .subtract(new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 7), "0")))
                            .subtract(new BigDecimal(RegexUtil.optStrOrVal(this.getValueByIndex(exDataList, 8), "0"))));
                    e.put("meter_status", "1");
                } else {
                    e.put("current_flow", 0);
                    e.put("indication", 0);
                    e.put("last_indication", 0);
                    e.put("meter_status", "0");
                }
            } else {
                e.put("meter_type", "2");
                Map<String, Object> cisSZDayByCondition = RegexUtil.optMapOrNew(JSON.parseObject(remoteCall("findCisSZDayByCondition", sc_url, e), Map.class));
                if (MapUtil.isNotEmpty(cisSZDayByCondition)) {
                    if (new BigDecimal(RegexUtil.optStrOrVal(cisSZDayByCondition.get("monitorAvgValue"), "0")).compareTo(new BigDecimal(RegexUtil.optStrOrVal(e.get("limit_value"), "0"))) > 0) {
                        e.put("status", "2");
                    } else {
                        e.put("status", "1");
                    }
                    e.put("indication", new BigDecimal(RegexUtil.optStrOrVal(cisSZDayByCondition.get("monitorAvgValue"), "0")));
                    e.put("last_indication", new BigDecimal(RegexUtil.optStrOrVal(meterMapper.getLastIndication(schemaName, e), "0")));
                    e.put("meter_status", "1");
                } else {
                    e.put("status", "1");
                    e.put("indication", 0);
                    e.put("last_indication", 0);
                    e.put("meter_status", "0");
                }
            }
        });
        meterMapper.insertOrUpdateMeter(schemaName, meterList);
        meterMapper.insertOrUpdateMeterItem(schemaName, needMeterItemList);
//        logsMapper.insertLog(schemaName, SensConstant.BUSINESS_NO_6003, RegexUtil.optStrOrNull(id), LangUtil.doSetLogArray("新增自动抄表记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_B_U, "记录："
//                .concat(RegexUtil.optStrOrBlank(e.get("spName")))
//                .concat("，流量计编号：").concat(RegexUtil.optStrOrBlank(e.get("monitorSiteCode")))
//                .concat("，当前读数：").concat(RegexUtil.optStrOrBlank(e.get("indication")))}), "system", RegexUtil.optNotBlankStrOpt(e.get("check_time")).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT)).map(d -> new Timestamp(d.getTime())).orElse(SenscloudUtil.getNowTime())); //新增自动抄表记录
    }

    public Map<String, Object> toMapByList(List<Map<String, Object>> list, Map<String, Object> map) {
        Map<String, Object> result = new HashMap<>();
        if (RegexUtil.optIsPresentList(list)) {
            for (Map<String, Object> item : list) {
                if (map.containsKey(item.get("config_name"))) {
                    result.put(RegexUtil.optStrOrNull(item.get("config_name")), item.get("setting_value"));
                }
            }
        }
        return result;
    }

    private String remoteCall(String path, String url, Map<String, Object> param) {
        JSONObject jsonObject = new JSONObject();
        Map<String, Object> map = RegexUtil.optMapOrNew(param);
        for (String key : map.keySet()) {
            jsonObject.put(key, map.get(key));
        }
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString);
    }

    private String authYunplc(String schemaName, Map<String, Object> param) {
        return RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(schemaName, "opc_url")).map(opc_url -> {
            //opc请求地址
            String result = HttpUtil.post(opc_url.concat("exlog"), param);
            logger.info("opc授权接口请求响应结果为：{}", result);
            List<String> list = RegexUtil.optNotBlankStrOpt(result).map(e -> Arrays.asList(e.split("\r\n"))).orElse(new ArrayList<>());
            StringBuffer questUrl = new StringBuffer("http://");
            for (String item : list) {
                if (item.contains("ADDR=")) {
                    questUrl.append(item.replace("ADDR=", "")).append("/exdata?");
                } else if (item.contains("SID=") && questUrl.toString().contains("exdata")) {
                    questUrl.append(item).append("&OP=R");
                }
            }
            return questUrl.toString();
        }).orElse(null);
    }

    private List<String> exData(String url, String schemaName, Map<String, Object> handMap) {
        String param = "9\r\nEV1_Remote\r\nEV1_ON\r\nEV1_OFF\r\nPIT01\r\nFIT01_Volume\r\nFIT01_Acc_tod\r\nFIT01_Acc_yes\r\nFIT01_Acc_sum\r\nEV1_G_Fault";
        String result = null;
        try {
            result = HttpRequest.post(url)
                    .header("Content-Type", "text/plain")
                    .body(param)
                    .execute().body();
        } catch (Exception e) {
            logger.error("opc实时接口请求失败，失败原因：{}", e);
            if (RegexUtil.optIsPresentStr(handMap.get("grm")) && RegexUtil.optIsPresentStr(handMap.get("pass"))) {
                String exDataUrl = this.authYunplc(schemaName, handMap);
                if (RegexUtil.optIsPresentStr(exDataUrl)) {
                    handMap.put("sid_url", exDataUrl);
                    meterMapper.updateCusAssetSid(schemaName, handMap);
                }
            }
        }
        logger.info("opc实时接口请求响应结果为：{}", result);
        List<String> list = RegexUtil.optNotBlankStrOpt(result).map(e -> Arrays.asList(e.split("\r\n"))).orElse(new ArrayList<>());
        return list;
    }

    private String getValueByIndex(List<String> list, int index) {
        if (index < list.size()) {
            return list.get(index);
        }
        return null;
    }
}
