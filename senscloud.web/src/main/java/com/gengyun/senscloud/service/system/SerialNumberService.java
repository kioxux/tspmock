package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;

/**
 * 编号生成工具
 */
public interface SerialNumberService {
    /**
     * 根据前缀等信息生成到一个最大的业务编号
     *
     * @param methodParam  系统参数
     * @param businessType 业务类型
     * @param serialPre    前缀
     * @param number       序号位数
     * @return 编号
     */
    String generateMaxBsCode(MethodParam methodParam, String businessType, String serialPre, int number);

    /**
     * 根据类型生成到一个最大的业务编号
     *
     * @param methodParam  系统参数
     * @param businessType 业务类型
     * @return 编号
     */
    String generateMaxBsCodeByType(MethodParam methodParam, String businessType);


}
