package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.IsuseEnum;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.StockMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.StockService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/25.
 */
@Service
public class StockServiceImpl implements StockService {

    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    StockMapper stockMapper;
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    LogsService logService;

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getStockListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[24]);
    }

    /**
     * 查询库房列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getStockList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String condition = getStockWhereString(methodParam, param);
        int total = stockMapper.findCountStockList(methodParam.getSchemaName(), methodParam.getUserId(), condition);
        Map<String, Object> result = new HashMap<String, Object>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            condition += " ORDER BY s.create_time desc";
            condition += pagination;
            List<Map<String, Object>> workList = null;
            workList = stockMapper.findStockList(methodParam.getSchemaName(), methodParam.getUserId(), condition);
            result.put("rows", workList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 新增库房
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void newStock(MethodParam methodParam, Map<String, Object> data) {
        String schema_name = methodParam.getSchemaName();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_AJ_Q}); // 库房不能为空
        RegexUtil.optStrOrExpNotNull(data.get("stock_name"), LangConstant.TITLE_NAME_F); // 备件名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("manager_user_id"), LangConstant.TITLE_AH_W); // 管理员不能为空
        int currency_id = RegexUtil.optIntegerOrVal(data.get("currency_id"), 0, LangConstant.TITLE_AG_T);
        String stock_code = serialNumberService.generateMaxBsCodeByType(methodParam, "stock_code");
        data.put("create_time", SenscloudUtil.getNowTime());
        data.put("schema_name", schema_name);
        data.put("stock_code", stock_code);
        data.put("status", 1);
        data.put("currency_id", currency_id);
        data.put("is_use", IsuseEnum.ENABLE.getKey());
        data.put("create_user_id", methodParam.getUserId());
        int result = stockMapper.insertStock(data);
        String id = data.get("id").toString();
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_3100, id, LangUtil.doSetLogArray("库房新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AJ_Q}));

    }

    /**
     * 编辑库房
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyStock(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        String schemaName = methodParam.getSchemaName();
        String id = data.get("id").toString();
        JSONArray logArr = new JSONArray();
        Map<String, Object> dbStockInfo = this.getStockDetailById(methodParam, id);
        String dataFieldStr = null;
        RegexUtil.optStrOrExpNotNull(data.get("stock_name"), LangConstant.TITLE_NAME_F); // 备件名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("manager_user_id"), LangConstant.TITLE_AH_W); // 管理员不能为空
        int currency_id = RegexUtil.optIntegerOrVal(data.get("currency_id"), 0, LangConstant.TITLE_AG_T);
        data.put("schema_name", schemaName);
        data.put("currency_id", currency_id);
        RegexUtil.intExp(stockMapper.updateStock(data), LangConstant.MSG_BK); // 未知错误！
        if (RegexUtil.optNotNull(dbStockInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbStockInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_3100, id, LangUtil.doSetLogArray("编辑了库房", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AJ_Q, log.toString()}));
        }
    }

    /**
     * 获取库房明细信息
     *
     * @param methodParam
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getStockDetailById(MethodParam methodParam, String id) {
        return RegexUtil.optMapOrExpNullInfo(stockMapper.findStockById(methodParam.getSchemaName(), id), LangConstant.TITLE_AJ_Q); // 库房信息不存在
    }

    /**
     * 启用禁用库房
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyStockUse(MethodParam methodParam, Map<String, Object> data) {
        Map<String, Object> dbStock = this.getStockDetailById(methodParam, data.get("id").toString());
        data.put("schema_name", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(dbStock).isPresent()) {
            stockMapper.updateStock(data);
            JSONArray logArr = new JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(dbStock.get("is_use"));
            String newIsUse = RegexUtil.optStrOrBlank(data.get("is_use"));
            if (!newIsUse.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("库房属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_AAL_N, IsuseEnum.getValue(dbIsUse), IsuseEnum.getValue(newIsUse)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_3100, data.get("id").toString(), logArr.toString());
            }
        }
    }

    /**
     * 删除选中库房
     *
     * @param methodParam
     */
    @Override
    public void cutStockByIds(MethodParam methodParam) {
        String idStr = methodParam.getIds();
        String[] idArr = idStr.split(",");
        int len = idArr.length;
        if (len > 0) {
            RegexUtil.trueExp(stockMapper.findSumBomStockByStockId(methodParam.getSchemaName(), idArr) > 0, LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AJ_Q, LangConstant.TITLE_AAAAY, LangConstant.TITLE_AJ_Q}); // 库房下还有备件，不能删除该库房
            stockMapper.updateSelectStockStatus(methodParam.getSchemaName(), idArr, StatusConstant.STATUS_DELETEED);
            boolean isBatchDel = len == 1 ? false : true;
            String remark = isBatchDel ? LangUtil.doSetLogArrayNoParam("批量删除了库房！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了库房！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AJ_Q});
            for (String id : idArr) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_3100, id, remark);
            }
        }
    }

    /**
     * 查询库房库存清单列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getStockBomList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String condition = getStockBomWhereString(methodParam, param);
        int total = stockMapper.findCountStockBomList(methodParam.getSchemaName(), param.getId(), condition);
        Map<String, Object> result = new HashMap<String, Object>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            condition += " ORDER BY bs.id desc";
            condition += pagination;
            List<Map<String, Object>> workList = null;
            workList = stockMapper.findStockBomList(methodParam.getSchemaName(), param.getId(), condition);
            result.put("rows", workList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 设置库房安全库存
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifySafeStock(MethodParam methodParam, Map<String, Object> data) {
        data.put("schema_name", methodParam.getSchemaName());
        Float security_quantity = RegexUtil.optFloatOrNull(data.get("security_quantity"), LangConstant.TITLE_AAAAZ);
        Float max_security_quantity = RegexUtil.optFloatOrNull(data.get("max_security_quantity"), LangConstant.TITLE_OO);
        Float show_price = RegexUtil.optFloatOrNull(data.get("show_price"), LangConstant.TITLE_AF_Z);
        data.put("security_quantity", RegexUtil.optIsPresentStr(security_quantity) ? security_quantity : 0f);
        data.put("max_security_quantity", RegexUtil.optIsPresentStr(max_security_quantity) ? max_security_quantity : 0f);
        data.put("show_price", RegexUtil.optIsPresentStr(show_price) ? show_price : 0f);
        stockMapper.updateSafeStockByBomStockId(data);
    }

    /**
     * 查询库房出入库记录列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getStockInAndOutList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String condition = getStockInAndOutWhereString(methodParam, param);
        int total = stockMapper.findCountStockInAndOutList(methodParam.getSchemaName(), param.getId(), condition);
        Map<String, Object> result = new HashMap<String, Object>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            List<Map<String, Object>> list = null;
//            list = stockMapper.findStockStockInAndOutList(methodParam.getSchemaName(), param.getId(), methodParam.getUserLang(),methodParam.getCompanyId(), condition ,pagination);
            list = stockMapper.findStockStockInAndOutList(methodParam.getSchemaName(), param.getId(), condition ,pagination);
            result.put("rows", list);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 查询库房备件导入记录列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getStockBomImportList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String condition = getStockBomImportWhereString(methodParam, param);
        int total = stockMapper.findCountStockBomImportList(methodParam.getSchemaName(), param.getId(), condition);
        Map<String, Object> result = new HashMap<String, Object>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            List<Map<String, Object>> list = null;
            list = stockMapper.findStockBomImportList(methodParam.getSchemaName(), param.getId(), condition ,pagination);
            result.put("rows", list);
        }
        result.put("total", total);
        return result;
    }

    private String getStockInAndOutWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer();
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (upper(b.material_code) like upper(")
                .append(e).append(") or lower(b.bom_name) like lower(").append(e)
//                .append(" or lower(bsd.bom_code) like lower(").append(e)
                .append("))"));
        return whereString.toString();
    }

    private String getStockBomImportWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer();
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (upper(bi.material_code) like upper(")
                .append(e).append(") or lower(bi.bom_name) like lower(").append(e)
                .append("))"));
        return whereString.toString();
    }

    private String getStockBomWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer();
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (upper(b.material_code) like upper(")
                .append(e).append(") or lower(b.bom_name) like lower(").append(e).append("))"));
        return whereString.toString();
    }

    private String getStockWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer(" and s.status > " + StatusConstant.STATUS_DELETEED);
        RegexUtil.optNotBlankStrOpt(param.getIsUseSearch()).ifPresent(e -> whereString.append(" and s.is_use = '").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(param.getCurrencyIds()).ifPresent(e -> whereString.append(" and s.currency_id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (upper(s.stock_name) like upper(")
                .append(e).append(") or lower(s.address) like ").append(e).append(" or lower(u.user_name) like ").append(e).append(")"));
        return whereString.toString();
    }

//    @Autowired
//    StockMapper stockMapper;
//
//    @Autowired
//    UserService userService;
//
//    //查询库房列表，按库房名称、使用组织、负责客户
//    @Override
//    public List<StockData> findStockListByKey(String schema_name, String keySearch, String stockPermissionCondition, int pageSize, int begin) {
//        return stockMapper.findStockListByKey(schema_name, keySearch, stockPermissionCondition, pageSize, begin);
//    }
//
//    //查询库房总数量，按库房名称、使用组织、负责客户
//    @Override
//    public int findStockListCountByKey(String schema_name, String keySearch, String stockPermissionCondition) {
//        return stockMapper.findStockListCountByKey(schema_name, keySearch, stockPermissionCondition);
//    }
//
//    //查询库房信息
//    @Override
//    public StockData findByStockCode(String schema_name, String stock_code) {
//        StockData data = stockMapper.findStockByStockCode(schema_name, stock_code);
//        if (data != null && null != data.getStock_code() && !data.getStock_code().isEmpty()) {
//            //查询库房的所属组织，以及使用位置
//            data.setFacilitiesList(stockMapper.findStockFacilityByStockCode(schema_name, stock_code));
//            data.setUserGroupDataList(stockMapper.findStockUserGroupByStockCode(schema_name, stock_code));
//        }
//        return data;
//    }
//
//    //禁用库房
//    @Override
//    public int deleteStockData(String schema_name, String stock_code, int status) {
//        return stockMapper.deleteStockData(schema_name, stock_code, status);
//    }
//
//    //新增库房
//    @Override
//    @Transactional
//    public int insertStockData(String schema_name, StockData stockData) {
//        int doResult = stockMapper.insertStockData(schema_name, stockData);
//        if (doResult > 0) {
//            //库房使用位置和所属组织新增
//            insertStockFacilityAndUserGroup(schema_name, stockData);
//        }
//        return doResult;
//    }
//
//    //更新库房
//    @Override
//    @Transactional
//    public int updateStockData(String schema_name, StockData stockData) {
//        int doResult = stockMapper.updateStockData(schema_name, stockData);
//        if (doResult > 0) {
//            //先清空库房所属组织，以及库房使用位置
//            stockMapper.clearStockFacility(schema_name, stockData.getStock_code());
//            stockMapper.clearStockUserGroup(schema_name, stockData.getStock_code());
//
//            //库房使用位置和所属组织新增
//            insertStockFacilityAndUserGroup(schema_name, stockData);
//        }
//        return doResult;
//    }
//
//    //库房使用位置和所属组织新增
//    private void insertStockFacilityAndUserGroup(String schema_name, StockData stockData) {
//        //新增库房所属组织
//        if (stockData.getUserGroupDataList() != null && stockData.getUserGroupDataList().size() > 0) {
//            for (UserGroupData item : stockData.getUserGroupDataList()) {
//                stockMapper.insertStockUserGroup(schema_name, stockData.getStock_code(), item.getId());
//            }
//        }
//        //新增库房使用位置
//        if (stockData.getFacilitiesList() != null && stockData.getFacilitiesList().size() > 0) {
//            for (Facility item : stockData.getFacilitiesList()) {
//                stockMapper.insertStockFacility(schema_name, stockData.getStock_code(), item.getId());
//            }
//        }
//    }
//
//    //根据用户权限，查看用户能使用的库房
//    @Override
//    public List<StockData> getStockListByUserPermission(String schema_name, String condition, String account) {
//        //获取当前用户
//        User userInfo = userService.getUserByAccount(schema_name, account);
//        Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, userInfo.getId(), "stock");
//        if (!isAllFacility) {
//            LinkedHashMap<String, IFacility> facilityList = userInfo.getFacilities();
//            String facilityIds = "";
//            if (facilityList != null && !facilityList.isEmpty()) {
//                for (String key : facilityList.keySet()) {
//                    facilityIds += key + ",";
//                }
//            }
//            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                condition += " and s.facility_id in (" + facilityIds + ")  ";
//            }
//        }
//
//        return stockMapper.getStockListByPermission(schema_name, condition);
//    }
}
