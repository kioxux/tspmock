package com.gengyun.senscloud.service.dynamic.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.BussinessConfigService;
import com.gengyun.senscloud.service.dynamic.WorkSheetHandleService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 工单处理
 * User: sps
 * Date: 2018/11/20
 * Time: 上午11:20
 */
@Service
public class WorkSheetHandleServiceImpl implements WorkSheetHandleService {
    private static final Logger logger = LoggerFactory.getLogger(WorkSheetHandleServiceImpl.class);
    @Resource
    WorkSheetHandleMapper workSheetHandleMapper;
    @Resource
    WorksMapper worksMapper;
    @Resource
    BomWorksMapper bomWorksMapper;
    @Resource
    AssetWorksMapper assetWorksMapper;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    LogsService logsService;
    @Resource
    WorkflowService workflowService;
    @Resource
    WorkFlowTemplateService workFlowTemplateService;
    @Resource
    BussinessConfigService bussinessConfigService;
    @Resource
    WorkFlowTemplateMapper workFlowTemplateMapper;

    /**
     * 通过工单编号解析工单新增、处理、详情页面展示信息
     *
     * @param methodParam      系统参数
     * @param subWorkCode      工单编号
     * @param pageType         页面类型
     * @param workTypeId       工单类型ID
     * @param workTemplateCode 工单模板编码
     * @return
     */
    @Override
    public Map<String, Object> getSingleWorkInfo(MethodParam methodParam, Map<String, Object> params, String subWorkCode, String pageType, String workTypeId, String workTemplateCode) {
        if (!((RegexUtil.optIsPresentStr(subWorkCode) && RegexUtil.optIsPresentStr(pageType))
                || RegexUtil.optIsPresentStr(workTypeId)
                || RegexUtil.optIsPresentStr(workTemplateCode))) {
            //1、处理、查看详情时，subWorkCode、pageType不能为空；2、新增时，workTypeId不能为空；3、查询子页面模板数据时，workTemplateCode不能为空
            throw new SenscloudException(LangConstant.MSG_BI);
        }
        Map<String, Object> mainWorkDetail = null;
        boolean is_main = true;
        List<Map<String, Object>> logs = new ArrayList<>();
        String nodeId = "", prefId = "";
        String workCode = "";
        //获取流程节点模板、字段等公共信息
        Map<String, Object> workFlowNodeTemplateInfo;
        Map<String, Object> workDetail = new HashMap<>();
        int businessTypeId = 0;
        if (!RegexUtil.optIsPresentStr(subWorkCode) && RegexUtil.optNotNull(workTypeId).isPresent()) {
            //新增工单
            Map<String, Object> flowStartNode = workFlowTemplateService.getWorkFlowStartNodeByWorkTypeId(methodParam, workTypeId);//获取流程的开始节点
            RegexUtil.optNotNullOrExp(flowStartNode, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ABE});//流程节点信息不存在
            prefId = RegexUtil.optStrOrNull(flowStartNode.get("pref_id"));
            nodeId = RegexUtil.optStrOrNull(flowStartNode.get("node_id"));
            pageType = RegexUtil.optStrOrNull(flowStartNode.get("page_type"));
        } else if (RegexUtil.optIsPresentStr(subWorkCode) && RegexUtil.optIsPresentStr(pageType)) {
            //处理工单、查看工单详情
            //根据工单类型id获取业务类型
            Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", Integer.valueOf(workTypeId));
            businessTypeId = RegexUtil.optIntegerOrExp(work_type.get("business_type_id"), methodParam, ErrorConstant.EC_WORK_010, LangConstant.LOG_B);
            String business_type_id = String.valueOf(businessTypeId);
            if (Integer.valueOf(SensConstant.BUSINESS_NO_2000) > businessTypeId) {
                workDetail = RegexUtil.optMapOrExpNullInfo(worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), subWorkCode), LangConstant.TITLE_AC_H); // 工单信息不存在
            } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3000) > businessTypeId) {
                workDetail = RegexUtil.optMapOrExpNullInfo(assetWorksMapper.findAssetWorksDetailInfoByCode(methodParam.getSchemaName(), subWorkCode), LangConstant.TITLE_AC_H); // 工单信息不存在
                if ("-1".equals(workDetail.get("is_main").toString())) {
                    is_main = false;
                    mainWorkDetail = assetWorksMapper.findMainAssetWorksDetailListByWorkCode(methodParam.getSchemaName(), RegexUtil.optStrOrNull(workDetail.get("work_code")));
                }
            } else if (Integer.valueOf(SensConstant.BUSINESS_NO_3100) > businessTypeId) {
                workDetail = RegexUtil.optMapOrExpNullInfo(bomWorksMapper.findBomWorksDetailInfoByCode(methodParam.getSchemaName(), subWorkCode), LangConstant.TITLE_BAAAB_B); // 工单信息不存在
                if ("-1".equals(workDetail.get("is_main").toString())) {
                    is_main = false;
                    mainWorkDetail = bomWorksMapper.findMainBomWorksDetailListByWorkCode(methodParam.getSchemaName(), RegexUtil.optStrOrNull(workDetail.get("work_code")));
                    if (RegexUtil.optIsPresentStr(params.get("nodeId"))) {
                        workDetail.put("inventory_user_id", RegexUtil.optStrOrBlank(workDetail.get("duty_user_id")));
                    }
                }
            } else {
                workDetail = RegexUtil.optMapOrExpNullInfo(worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), subWorkCode), LangConstant.TITLE_AC_H); // 工单信息不存在
            }
            workCode = RegexUtil.optStrOrNull(workDetail.get("work_code"));
            prefId = RegexUtil.optStrOrNull(workDetail.get("pref_id"));
            Map<String, Object> flowInfo = workFlowTemplateService.getFlowInfo(methodParam.getSchemaName(), prefId);
            String flow_code = flowInfo.get("flow_code").toString();
            Map<String, Object> map = new HashMap<>();
            map.put(FlowParmsConstant.SUB_WORK_CODE, subWorkCode);
            map.put(FlowParmsConstant.PDEID, flow_code);
            if ("detail".equals(pageType)) {
                if (is_main) {
                    nodeId = prefId;
                    pageType = "base";
                } else {
                    nodeId = RegexUtil.optStrOrExpNotExist(params.get("nodeId"), LangConstant.TITLE_AAT_P);
                    pageType = "detail";
                }
            } else {
                if (is_main) {
                    nodeId = workflowService.getNodeIdBySubWorkCode(methodParam, map);//获取流程节点ID
                } else {
                    map.put(FlowParmsConstant.SUB_WORK_CODE, mainWorkDetail.get("sub_work_code"));
                    nodeId = workflowService.getNodeIdBySubWorkCode(methodParam, map);//获取流程节点ID
                }
                if (!RegexUtil.optIsPresentStr(nodeId) || "end".equals(nodeId)) {
                    if (businessTypeId == 3005) {
                        Map<String, Object> pm = new HashMap<>();
                        pm.put("prefId", prefId);
                        pm.put("nodeType", "start");
                        nodeId = workFlowTemplateMapper.findNodeId(methodParam.getSchemaName(), pm);
                    } else {
                        if (StatusConstant.DRAFT == RegexUtil.optIntegerOrVal(workDetail.get("status"), 0)) {
                            Map<String, Object> flowStartNode = workFlowTemplateService.getWorkFlowStartNodeByWorkTypeId(methodParam, workTypeId);//获取流程的开始节点
                            RegexUtil.optNotNullOrExp(flowStartNode, LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ABE});//流程节点信息不存在
                            nodeId = RegexUtil.optStrOrNull(flowStartNode.get("node_id"));
                        } else {
                            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_AK});//节点ID信息不存在
                        }
                    }
                }
            }
            //获取当前工单类型id
            logs = logsService.getLog(methodParam, business_type_id + SensConstant.BUSINESS_NO_PROCESS, subWorkCode);
        }
        if (RegexUtil.optIsPresentStr(workTemplateCode)) {
            workFlowNodeTemplateInfo = workFlowTemplateService.getSubPageWorkFlowNodeTemplateInfo(methodParam, workTemplateCode);
        } else {
            workFlowNodeTemplateInfo = workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, prefId, nodeId, pageType);
        }
        //根据工单类型获取业务类型
        if (RegexUtil.optNotNull(workFlowNodeTemplateInfo.get("work_type_id")).isPresent()) {
            Map<String, Object> work_type = bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", Integer.valueOf(workFlowNodeTemplateInfo.get("work_type_id").toString()));
            Integer business_type_id = Integer.valueOf(work_type.get("business_type_id").toString());
            workFlowNodeTemplateInfo.put("business_type_id", business_type_id);
        }
        //如果是工单处理、查看工单详情，则要填充上字段值
        if (RegexUtil.optIsPresentStr(subWorkCode)) {
            //获取工单已提交数据信息
            //设备报废流程查询，不需要子页面字段信息
            List<Map<String, Object>> worksDetailColumnList = null;
            if (Integer.valueOf(SensConstant.BUSINESS_NO_2003) == businessTypeId) {
                worksDetailColumnList = worksMapper.findWorksDetailColumnListNoParentId(methodParam.getSchemaName(), subWorkCode);
            } else if ((Integer.valueOf(SensConstant.BUSINESS_NO_3005) == businessTypeId
                    || Integer.valueOf(SensConstant.BUSINESS_NO_2004) == businessTypeId) && !is_main && RegexUtil.optIsPresentStr(params.get("nodeId"))) {
                worksDetailColumnList = handleWorksDetailColumnList(methodParam.getSchemaName(), subWorkCode);
            } else {
                worksDetailColumnList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), subWorkCode);
            }
            //填充已提交数据到模板里
            mergeTemplateAndData(methodParam, workDetail, workFlowNodeTemplateInfo, worksDetailColumnList, workCode, subWorkCode);
        }
        //工单字段分组处理、数据结构处理
        fieldSectionHandle(methodParam, workFlowNodeTemplateInfo, params);
        workFlowNodeTemplateInfo.put("logs", logs);
        workFlowNodeTemplateInfo.put("node_show_name", RegexUtil.optStrOrVal(workFlowNodeTemplateInfo.get("node_show_name"), commonUtilService.getLanguageInfoBySysLang(methodParam, LangConstant.TITLE_AAH_K)));
        return workFlowNodeTemplateInfo;
    }

    /**
     * 整合小程序回显字段及数据（设备扫码用）
     *
     * @param methodParam  系统参数
     * @param deviceCode   设备编码
     * @param workTypeId   工单类型id
     * @param needWorkInfo 是否返回工单信息
     * @return
     */
    @Override
    public Map<String, Object> getWxWorkHandleInfoByAssetCode(MethodParam methodParam, String deviceCode, String workTypeId, boolean needWorkInfo) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> deviceData = this.getWxAssetCommonDataByCode(methodParam, deviceCode);
        if (!RegexUtil.optIsPresentMap(deviceData)) {
            throw new SenscloudException(LangConstant.TEXT_K, new String[]{LangConstant.TITLE_ASSET_I});// 设备不存在
        }
        if (needWorkInfo) {
            //返回工单字段并填充设备相关信息
            Map<String, Object> singleWorkInfo = this.getSingleWorkInfo(methodParam, null, null, null, String.valueOf(workTypeId), null);
            handleDeviceData(singleWorkInfo, deviceData);
            result.putAll(singleWorkInfo);
        }
        result.put("scanData", handleDeviceData(deviceData));
        return result;
    }

    /**
     * 获取设备信息（手机端使用）
     *
     * @param methodParam 系统参数
     * @param deviceCode  设备编码
     * @return
     */
    @Override
    public Map<String, Object> getWxAssetCommonDataByCode(MethodParam methodParam, String deviceCode) {
        return workSheetHandleMapper.getAssetCommonDataByCode(methodParam.getSchemaName(), methodParam.getUserId(), deviceCode);
    }

    /**
     * 封装设备数据
     *
     * @param deviceData
     */
    private List<Map<String, Object>> handleDeviceData(Map<String, Object> deviceData) {
        List<Map<String, Object>> deviceDataList = new ArrayList<>();
        RegexUtil.optNotNullMap(deviceData).ifPresent(m ->
                deviceData.forEach((k, v) -> deviceDataList.add(new HashMap<String, Object>() {{
                    put("field_code", k);
                    put("field_value", v);
                }}))
        );
        return deviceDataList;
    }

    /**
     * 填充工单数据到工单字段数据里
     *
     * @param
     */
    private void handleDeviceData(Map<String, Object> singleWorkInfo, Map<String, Object> workData) {
        if (RegexUtil.optIsPresentMap(singleWorkInfo) && RegexUtil.optIsPresentMap(workData)) {
            List<Map<String, List<Map<String, Object>>>> workFlowNodeColumnList = (ArrayList<Map<String, List<Map<String, Object>>>>) singleWorkInfo.get("field_section_list");
            if (RegexUtil.optIsPresentList(workFlowNodeColumnList)) {
                workFlowNodeColumnList.forEach(nc -> {
                    List<Map<String, Object>> columnList = nc.get("work_flow_node_column_list");
                    if (RegexUtil.optIsPresentList(columnList)) {
                        columnList.forEach(c -> {
                            String fieldCode = (String) c.get("field_code");
                            if (workData.containsKey(fieldCode)) {
                                c.put("field_value", workData.get(fieldCode));
                            }
                        });
                    }
                });
            }
        }
    }

    /**
     * 填充工单数据到工单节点模板
     *
     * @param workFlowNodeTemplateInfo 工单流程节点模板数据
     * @param worksDetailColumnList    工单已提交节点数据
     */
    private void mergeTemplateAndData(MethodParam methodParam, Map<String, Object> data, Map<String, Object> workFlowNodeTemplateInfo, List<Map<String, Object>> worksDetailColumnList, String workCode, String subWorkCode) {
        if (RegexUtil.optIsPresentMap(workFlowNodeTemplateInfo)) {
            workFlowNodeTemplateInfo.put("sub_work_code", subWorkCode);
            workFlowNodeTemplateInfo.put("work_code", workCode);
            if (RegexUtil.optIsPresentList(worksDetailColumnList)) {
                Map<String, Map<String, Object>> worksDetailColumnMap = new HashMap<>();//字段编码、字段对象映射表
                worksDetailColumnList.forEach(wd -> worksDetailColumnMap.put(String.valueOf(wd.get("field_form_code")), wd));
                List<Map<String, Object>> workFlowNodeColumnList = (ArrayList<Map<String, Object>>) workFlowNodeTemplateInfo.get("work_flow_node_column_list");
                if (RegexUtil.optIsPresentList(workFlowNodeColumnList)) {
                    workFlowNodeColumnList.forEach(nc -> {

                        if (RegexUtil.optNotNull(nc.get("work_flow_node_column_ext_list")).isPresent()) {
                            List<Map<String, Object>> work_flow_node_column_ext_list = (List<Map<String, Object>>) nc.get("work_flow_node_column_ext_list");
                            for (Map<String, Object> work_flow_node_column_ext : work_flow_node_column_ext_list) {
                                if (RegexUtil.optNotNull(work_flow_node_column_ext.get("field_name")).isPresent() && "is_sub_work_order".equals(work_flow_node_column_ext.get("field_name").toString())) {
                                    //是子工单子页面
                                    if (RegexUtil.optNotNull(nc.get("work_flow_node_column_linkage_list")).isPresent()) {
                                        List<Map<String, Object>> work_flow_node_column_linkage_list = (List<Map<String, Object>>) nc.get("work_flow_node_column_linkage_list");
                                        for (Map<String, Object> work_flow_node_column_linkage : work_flow_node_column_linkage_list) {
                                            //获取该工单模板的所有字段
                                            List<Map<String, Object>> subColumnList = workFlowTemplateService.getColumnListByWorkTemplateCode(methodParam, work_flow_node_column_linkage.get("sub_page_template_code").toString());
                                            List<Map<String, Object>> asset_work_detail_item_list = assetWorksMapper.findSubAssetWorksDetailItemListByWorkCode(methodParam.getSchemaName(), workCode);
                                            asset_work_detail_item_list = new ArrayList<>();
                                            List<Map<String, Object>> subColumnMapList = new ArrayList<>();
                                            for (Map<String, Object> asset_work_detail_item : asset_work_detail_item_list) {
                                                Map<String, Object> subColumnMap = new HashMap<>();
                                                for (Map<String, Object> subColumn : subColumnList) {
                                                    subColumnMap.put(subColumn.get("field_form_code").toString(), asset_work_detail_item.get(subColumn.get("field_code")));
                                                }
                                                subColumnMapList.add(subColumnMap);
                                            }
                                            nc.put("field_value", JSON.toJSONString(subColumnMapList));
                                        }
                                    }
                                }
                            }
                        }
                        Map<String, Object> column = worksDetailColumnMap.get(nc.get("field_form_code"));
                        RegexUtil.optNotBlankStrOpt(nc.get("field_code")).filter(e -> "state".equals(e)).ifPresent(c -> {
                            nc.put("field_value", RegexUtil.optStrOrNull(!"".equals("status") && data.containsKey("status") ? data.get("status") : column.get("field_value")));
                        });
                        boolean ncValue = RegexUtil.optNotBlankStrOpt(nc.get("field_value")).filter(e -> !"[]".equals(e)).isPresent();
                        if (RegexUtil.optIsPresentMap(column) && !ncValue) {
                            String code = RegexUtil.optStrOrBlank(nc.get("field_code"));
                            nc.put("field_value", RegexUtil.optStrOrNull(!"".equals(code) && data.containsKey(code) ? data.get(code) : column.get("field_value")));
                        }
//                        if ("sub_work_order".equals(nc.get("field_code").toString())) {
//                            //查询是否有子工单
//                            List<Map<String, Object>> sub_work_detail_list = assetWorksMapper.findSubAssetWorksDetailListBySubWorkCode(methodParam.getSchemaName(), workCode);
//                            if (sub_work_detail_list.size() > 0) {
//                                nc.put("field_value", JSON.toJSONString(sub_work_detail_list));
//                            }
//                        }
                    });
                }
            }
        }
    }

    /**
     * 工单字段分组处理、数据结构处理
     *
     * @param workFlowNodeTemplateInfo 工单数据
     */
    private void fieldSectionHandle(MethodParam methodParam, Map<String, Object> workFlowNodeTemplateInfo, Map<String, Object> params) {
        if (RegexUtil.optNotNull(workFlowNodeTemplateInfo.get("business_type_id")).isPresent()) {
            String business_type_id = workFlowNodeTemplateInfo.get("business_type_id").toString();
            //如果当前工单业务类型是备件入库
            if (business_type_id.equals(SensConstant.BUSINESS_NO_3001)) {
                //获取备件编码
                if (RegexUtil.optNotNull(params.get("dataId")).isPresent()) {
                    Integer bom_id = Integer.valueOf(params.get("dataId").toString());
                    params.put("bom_id", bom_id.toString());
                }
            }
            List<Map<String, Object>> workFlowNodeColumnList = RegexUtil.optNotNullListOrNew(workFlowNodeTemplateInfo.get("work_flow_node_column_list"));
            Map<String, List<Map<String, Object>>> sectionCode2ColumnListMap = new LinkedHashMap<>();
            workFlowNodeColumnList.forEach(column -> {
                //如果入参这个字段有值 则直接代替原来字段的值
                if (RegexUtil.optNotNull(params.get(column.get("field_code").toString())).isPresent()) {
                    column.put("field_value", params.get(column.get("field_code").toString()));
                }
                if ("work_type_id".equals(column.get("field_code"))) {
                    column.put("field_value", workFlowNodeTemplateInfo.get("work_type_id"));
                }

                String sectionType = RegexUtil.optStrOrVal(column.get("field_section_type"), "");
                if (RegexUtil.optIsPresentStr(sectionType)) {
                    List<Map<String, Object>> columnList = RegexUtil.optNotNullListOrNew(sectionCode2ColumnListMap.get(sectionType));
                    columnList.add(column);
                    sectionCode2ColumnListMap.put(sectionType, columnList);
                }
                List<Map<String, Object>> workFlowNodeColumnExtList = RegexUtil.optNotNullListOrNew(column.get("work_flow_node_column_ext_list"));
                if (RegexUtil.optIsPresentList(workFlowNodeColumnExtList)) {
                    workFlowNodeColumnExtList.forEach(c -> {
                        String fieldValue = RegexUtil.optStrOrNull(c.get("field_value"));
                        if (RegexUtil.optIsPresentStr(fieldValue)) {
                            column.put(String.valueOf(c.get("field_name")), fieldValue);
                        }
                    });
                }
                column.remove("work_flow_node_column_ext_list");
                List<Map<String, Object>> workFlowNodeColumnLinkageList = RegexUtil.optNotNullListOrNew(column.get("work_flow_node_column_linkage_list"));
                if (RegexUtil.optIsPresentList(workFlowNodeColumnLinkageList)) {
                    workFlowNodeColumnLinkageList.forEach(l -> {
                        Map<String, Object> workFlowNodeColumnLinkageCondition = RegexUtil.optMapOrNew((Map<String, Object>) l.get("work_flow_node_column_linkage_condition"));
                        String condition = RegexUtil.optStrOrNull(workFlowNodeColumnLinkageCondition.get("condition"));
                        if (RegexUtil.optIsPresentStr(condition)) {
                            workFlowNodeColumnLinkageCondition.put("condition", JSONObject.parseObject(condition));
                        }
                        workFlowNodeColumnLinkageCondition.forEach((k, v) -> l.put(k, v));
                        l.remove("work_flow_node_column_linkage_condition");
                    });
                }
            });
            List<Map<String, Object>> fieldSectionList = new ArrayList<>();
            sectionCode2ColumnListMap.keySet().forEach(k -> fieldSectionList.add(new HashMap<String, Object>() {{
                put("field_section_type", k);
                put("work_flow_node_column_list", sectionCode2ColumnListMap.get(k));
            }}));
            workFlowNodeTemplateInfo.put("field_section_list", fieldSectionList);
            workFlowNodeTemplateInfo.remove("work_flow_node_column_list");
        } else {//通用
            List<Map<String, Object>> workFlowNodeColumnList = RegexUtil.optNotNullListOrNew(workFlowNodeTemplateInfo.get("work_flow_node_column_list"));
            Map<String, List<Map<String, Object>>> sectionCode2ColumnListMap = new LinkedHashMap<>();
            workFlowNodeColumnList.forEach(column -> {
                if ("work_type_id".equals(column.get("field_code"))) {
                    column.put("field_value", workFlowNodeTemplateInfo.get("work_type_id"));
                }
                String sectionType = RegexUtil.optStrOrVal(column.get("field_section_type"), "");
                if (RegexUtil.optIsPresentStr(sectionType)) {
                    List<Map<String, Object>> columnList = RegexUtil.optNotNullListOrNew(sectionCode2ColumnListMap.get(sectionType));
                    columnList.add(column);
                    sectionCode2ColumnListMap.put(sectionType, columnList);
                }
                List<Map<String, Object>> workFlowNodeColumnExtList = RegexUtil.optNotNullListOrNew(column.get("work_flow_node_column_ext_list"));
                if (RegexUtil.optIsPresentList(workFlowNodeColumnExtList)) {
                    workFlowNodeColumnExtList.forEach(c -> {
                        String fieldValue = RegexUtil.optStrOrNull(c.get("field_value"));
                        if (RegexUtil.optIsPresentStr(fieldValue)) {
                            column.put(String.valueOf(c.get("field_name")), fieldValue);
                        }
                    });
                }
                column.remove("work_flow_node_column_ext_list");
                List<Map<String, Object>> workFlowNodeColumnLinkageList = RegexUtil.optNotNullListOrNew(column.get("work_flow_node_column_linkage_list"));
                if (RegexUtil.optIsPresentList(workFlowNodeColumnLinkageList)) {
                    workFlowNodeColumnLinkageList.forEach(l -> {
                        Map<String, Object> workFlowNodeColumnLinkageCondition = RegexUtil.optMapOrNew((Map<String, Object>) l.get("work_flow_node_column_linkage_condition"));
                        String condition = RegexUtil.optStrOrNull(workFlowNodeColumnLinkageCondition.get("condition"));
                        if (RegexUtil.optIsPresentStr(condition)) {
                            workFlowNodeColumnLinkageCondition.put("condition", JSONObject.parseObject(condition));
                        }
                        workFlowNodeColumnLinkageCondition.forEach((k, v) -> l.put(k, v));
                        l.remove("work_flow_node_column_linkage_condition");
                    });
                }
            });
            List<Map<String, Object>> fieldSectionList = new ArrayList<>();
            sectionCode2ColumnListMap.keySet().forEach(k -> fieldSectionList.add(new HashMap<String, Object>() {{
                put("field_section_type", k);
                put("work_flow_node_column_list", sectionCode2ColumnListMap.get(k));
            }}));
            workFlowNodeTemplateInfo.put("field_section_list", fieldSectionList);
            workFlowNodeTemplateInfo.remove("work_flow_node_column_list");
        }

    }

    private List<Map<String, Object>> handleWorksDetailColumnList(String schemaName, String subWorkCode) {
        List<Map<String, Object>> worksDetailColumnList = worksMapper.findWorksDetailColumnList(schemaName, subWorkCode);
        if (RegexUtil.optIsPresentList(worksDetailColumnList)) {
            List<Map<String, Object>> worksDetailColumnListGroup = worksMapper.findWorksDetailColumnListGroup(schemaName, subWorkCode);
            worksDetailColumnListGroup.forEach(tmp -> {
                worksDetailColumnList.forEach(e -> {
                    if (Objects.equals(e.get("field_form_code"), tmp.get("field_form_code"))) {
                        e.put("field_value", tmp.get("field_value"));
                    }
                });
            });
        }
        return worksDetailColumnList;
    }

    /**
     * @param map
     * @param subWorkCode
     * @param map_source
     */
    private void putRequestToWork(Map<String, Object> map, String subWorkCode, Map<String, Object> map_source) {
        List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("field_section_list");
        //List<Map<String, Object>> rList = (List<Map<String, Object>>) map_source.get("field_section_list");
        Map<String, Integer> columns = new HashMap();
        columns.put("facility_id", 1);
        columns.put("facility_name", 1);
        columns.put("contact_person", 1);
        columns.put("contact_name", 1);
        columns.put("mobile", 1);
        columns.put("customer_remark", 1);
        Map<String, Object> maps = new HashMap();
        /*rList.forEach(e -> {
            if (e.containsKey("work_flow_node_column_list")) {
                List<Map<String, Object>> slist = (List<Map<String, Object>>) e.get("work_flow_node_column_list");
                slist.forEach(k -> {
                    if (columns.containsKey(k.get("field_code")) &&
                            RegexUtil.optIsPresentStr(k.get("field_value"))) {
                        maps.put(k.get("field_code").toString(), k.get("field_value"));
                    }

                });
            }
        });*/
        list.forEach(e -> {
            //if (e.get("field_section_type").equals("base")) {
            if (e.containsKey("work_flow_node_column_list")) {
                List<Map<String, Object>> rlist = (List<Map<String, Object>>) e.get("work_flow_node_column_list");
                rlist.forEach(k -> {
                    if (k.get("field_code").equals("work_request_code")) {
                        k.put("field_value", subWorkCode);
                    } else if (columns.containsKey(k.get("field_code"))) {
                        k.put("field_value", map_source.get(k.get("field_code")));
                    }

                });
            }
        });
    }

    /**
     * 从服务请求发起工单
     *
     * @param methodParam
     * @param params
     * @param subWorkCode
     * @param pageType
     * @param workTypeId
     * @param workTemplateCode
     * @return
     */
    @Override
    public Map<String, Object> getSingleWorkInfoFromCustomerRequest(MethodParam methodParam, Map<String, Object> params, String subWorkCode, String pageType, String workTypeId, String workTemplateCode) {
        Map<String, Object> map = getSingleWorkInfo(methodParam, params, null, pageType, workTypeId, workTemplateCode);
        Map<String, Object> worksDetailInfo = worksMapper.findWorksDetailByCode(methodParam.getSchemaName(), subWorkCode);
        //Map<String, Object> map_source = getSingleWorkInfo(methodParam, new HashMap<>(), subWorkCode, "base", String.valueOf(worksDetailInfo.get("work_type_id")), null);
        Map<String, Object> map_source = getWorksDetailColumnKeyValue(methodParam, subWorkCode);
        putRequestToWork(map, subWorkCode, map_source);
        return map;
    }

    /**
     * 将工单详情的字段表转换为keyvalue的map
     *
     * @param methodParam   入参
     * @param sub_work_code 入参
     * @return map
     */
    private Map<String, Object> getWorksDetailColumnKeyValue(MethodParam methodParam, String sub_work_code) {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> worksDetailColumnList = worksMapper.findWorksDetailColumnList(methodParam.getSchemaName(), sub_work_code);
        if (RegexUtil.optNotNull(worksDetailColumnList).isPresent() && worksDetailColumnList.size() > 0) {
            for (Map<String, Object> worksDetailColumn : worksDetailColumnList) {
                result.put(worksDetailColumn.get("field_code").toString(), worksDetailColumn.get("field_value"));
            }
        }
        return result;
    }
}
