package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.mapper.WorkRecordHourMapper;
import com.gengyun.senscloud.service.WorkRecordHourService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;

/**
 * 工单工时记录表
 * User: sps
 * Date: 2020/04/27
 * Time: 下午15:00
 */
@Service
public class WorkRecordHourServiceImpl implements WorkRecordHourService {
    //    private static final Logger logger = LoggerFactory.getLogger(WorkRecordHourServiceImpl.class);
    @Resource
    WorkRecordHourMapper workRecordHourMapper;
//
//    /**
//     * 新增工单工时记录
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @param operate_account
//     * @param begin_time
//     * @param finished_time
//     * @return
//     */
//    public int insertWorkRecordHour(String schema_name, String sub_work_code, String operate_account,
//                                    Timestamp begin_time, Timestamp finished_time) {
//        return workRecordHourMapper.insertWorkRecordHour(schema_name, sub_work_code, operate_account, begin_time,
//                finished_time);
//    }
//

    /**
     * 更新工单工时记录
     *
     * @param schema_name
     * @param sub_work_code
     * @param operate_user_id
     * @param finished_time
     * @return
     */
    @Override
    public int updateWorkRecordHour(String schema_name, String sub_work_code, String operate_user_id,
                                    Timestamp finished_time) {
        return workRecordHourMapper.updateWorkRecordHour(schema_name, sub_work_code, operate_user_id, finished_time);
    }
//
    /**
     * 更新工单总工时
     *
     * @param schema_name
     * @param sub_work_code
     * @return
     */
    @Override
    public int updateWorkTotalHour(String schema_name, String sub_work_code) {
        return workRecordHourMapper.updateWorkTotalHour(schema_name, sub_work_code);
    }
//
//    /**
//     * 根据信息统计未完成工单工时记录
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @param operate_account
//     * @param isCheckNull
//     * @return
//     */
//    public int countWrhByInfo(String schema_name, String sub_work_code, String operate_account, String isCheckNull) {
//        return workRecordHourMapper.countWrhByInfo(schema_name, sub_work_code, operate_account, isCheckNull);
//    }
}
