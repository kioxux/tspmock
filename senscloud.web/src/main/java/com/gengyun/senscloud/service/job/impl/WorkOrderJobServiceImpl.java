package com.gengyun.senscloud.service.job.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.entity.CompanyEntity;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.job.WorkOrderJobService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WorkOrderJobServiceImpl implements WorkOrderJobService {
    private static final Logger logger = LoggerFactory.getLogger(WorkOrderJobServiceImpl.class);
    @Resource
    private PlanWorkService planWorkService;
    @Resource
    private CompanyService companyService;
    @Resource
    private CommonUtilService commonUtilService;
    @Resource
    SystemConfigService systemConfigService;

    /**
     * 行事历批量生成工单的条数
     */
    private int BATCH_COUNT = 1000;

    /**
     * 根据行事历生成相应的工单
     *
     * @param schemaName
     */
    @Override
    @Transactional
    public void cronJobToGenerateWorkOrderByCalendar(String schemaName) {
        /**
         * 查询符合条件需要生成工单的行事历
         * 1、未生成过工单的
         * 2、截止时间大于当前时间
         * 3、类型为自动生成工单的行事历
         * 4、当前时间大于自动生成时间点（取generate_time_point字段，例如：generate_time_point为2，则在当天凌晨2时后，开始生成工单）
         */
        List<PlanWorkCalendarModel> calendarModels = planWorkService.getPlanWorkCalendarListByOccurTime(schemaName, Constants.AUTO_GENERATE_BILL_YES, BATCH_COUNT);
        if (!RegexUtil.optIsPresentList(calendarModels)) {
            return;
        }

        StringBuffer workCalendarCodes = new StringBuffer();
        MethodParam methodParam = new MethodParam();
        methodParam.setSchemaName(schemaName);
        methodParam.setCompanyId(Long.valueOf(schemaName.replace(SensConstant.SCHEMA_PREFIX, "")));
        methodParam.setAccount("system");
        methodParam.setUserId("system");
        methodParam.setUserName("系统");
        String Lang = RegexUtil.optStrOrVal(systemConfigService.getSysLang(methodParam), Constants.LANG_ZH_CN);
        methodParam.setUserLang(Lang);
        methodParam.setSystemLang(Lang);
        CompanyEntity companyEntity = companyService.getCompanyById(methodParam.getCompanyId());
        methodParam.setOpenSms(companyEntity.getIs_open_sms());
        calendarModels.forEach(c -> {
            RegexUtil.optNotBlankStrOpt(c.getWork_calendar_code()).ifPresent(s -> {
                try {
                    //根据维保计划生成一条工单
                    commonUtilService.createWorkOrderByPlanWorkCalendar(methodParam, c);
                    workCalendarCodes.append((workCalendarCodes.length() > 0 ? "," : "") + "'" + c.getWork_calendar_code() + "'");
                } catch (Exception we) {
                    logger.error("pTw-ws", we);
                }
            });
        });
        if (RegexUtil.optIsPresentStr(workCalendarCodes.toString()))
        //已生成工单的行事历，修改下状态
        {
            planWorkService.updatePlanWorkCalendarStatusByWorkCalendarCodeList(schemaName, workCalendarCodes.toString(), Constants.PLAN_WORK_CALENDAR_STATUS_GENERATED);
        }
    }
}
