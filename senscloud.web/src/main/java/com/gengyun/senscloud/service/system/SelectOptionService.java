package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 字典处理
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
public interface SelectOptionService {
    /**
     * 根据类型查询字典
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param extParam    扩展参数
     * @return 字典
     */
    List<Map<String, Object>> getSelectOptionList(MethodParam methodParam, String selectKey, Map<String, Object> extParam);

    /**
     * 根据code取字典选中项信息（默认取名称）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param selectValue     选中项
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    String getSelectOptionAttrByCode(MethodParam methodParam, String selectKey, String selectValue, String querySelectName);

    /**
     * 根据类型查询字典（默认取名称【缓存模式】）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @return 选中项信息
     */
    List<Map<String, Object>> getSelectOptionListWithCache(MethodParam methodParam, String selectKey);

    /**
     * 根据code取字典选中项信息（默认取名称【缓存模式】）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param selectValue     选中项
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    String getSelectOptionAttrByCodeWithCache(MethodParam methodParam, String selectKey, String selectValue, String querySelectName);

    /**
     * 根据Text取字典选中项信息（默认取名称【缓存模式】）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param selectValue     选中项
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    String getSelectOptionAttrByTextWithCache(MethodParam methodParam, String selectKey, String selectValue, String querySelectName);


    /**
     * 根据code取字典选中项名称（默认取名称【缓存模式】）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @return 选中项信息
     */
    String getSelectOptionTextByCodeWithCache(MethodParam methodParam, String selectKey, String selectValue);

    /**
     * 根据Text取字典选中项名称（默认取名称【缓存模式】）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @return 选中项信息
     */
    String getSelectOptionCodeByTextWithCache(MethodParam methodParam, String selectKey, String selectValue);

    /**
     * 取字典第一个选项信息（默认取名称【缓存模式】）
     *
     * @param methodParam     系统参数
     * @param selectKey       字典类型
     * @param querySelectName 选中项属性
     * @return 选中项信息
     */
    String getSelectFirstOptionAttrByCodeWithCache(MethodParam methodParam, String selectKey, String querySelectName);

    /**
     * 根据code取字典选中项名称
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @param paramMap 扩展字段
     * @return 选中项信息
     */
    String getSelectOptionTextByCode(MethodParam methodParam, String selectKey, String selectValue);

    /**
     * 根据code取字典选中项信息
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param selectValue 选中项
     * @return 选中项信息
     */
    Map<String, Object> getSelectOptionByCode(MethodParam methodParam, String selectKey, String selectValue, Map<String, Object> paramMap);

    /**
     * 静态字典选中项查询
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @param code        选中项值
     * @return 选中项信息
     */
    Map<String, Object> getStaticSelectOption(MethodParam methodParam, String selectKey, String code);

    /**
     * 静态字典查询
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @return 字典信息
     */
    List<Map<String, Object>> getStaticSelectList(MethodParam methodParam, String selectKey);

    /**
     * 静态字典查询（对象形式）
     *
     * @param methodParam 系统参数
     * @param selectKey   字典类型
     * @return 字典信息
     */
    Map<String, Object> getStaticSelectForInfo(MethodParam methodParam, String selectKey);

    /**
     * 根据业务id获取目标表列表
     */
    List<Map<String, Object>> getTargetTableList(MethodParam methodParam, String data_type, String parent_code);

    /**
     * 聚合json字段数据
     *
     * @param schemaName
     * @param tableName
     * @param keyName           数据主键名称
     * @param keyValue          数据主键值
     * @param columnName        数据json字段名称
     * @param jsonColumnName    json待处理字段名称
     * @param jsonColumnValue   json待处理字段值
     * @param polymerizationStr 聚合类型
     * @param fieldColumnName   聚合字段名
     */
    Double selectPolymerizationJsonColumnData(String schemaName, String tableName, String keyName, String keyValue, String columnName, String jsonColumnName, String jsonColumnValue, String polymerizationStr, String fieldColumnName);


    /**
     * 根据key和code获取静态字典参数数据
     *
     * @param methodParam 系统参数
     * @param selectValue 选中项
     * @return
     */
    List<Map<String, Object>> getSelectOptionInfoByKeyAndCode(MethodParam methodParam, String selectValue);

    /**
     * 根据工单类型id查询工单类型信息
     *
     * @param methodParam 入参
     * @param id          入参
     * @return 工单类型信息
     */
    Map<String, Object> getWorkTypeById(MethodParam methodParam, Integer id);


    /**
     * 根据key和extKey获取下拉框数据
     *
     * @param methodParam  入参
     * @param selectKey    入参
     * @param selectExtKey 入参
     * @return 下拉框数据
     */
    List<Map<String, Object>> getSelectOptionByKeyAndExtKey(MethodParam methodParam, String selectKey, String selectExtKey);

    /**
     * 根据字段名和value值获取名称
     *
     * @param methodParam 入参
     * @param name        入参
     * @param value       入参
     * @return 获取名称
     */
    Map<String, Object> getInfoByValue(MethodParam methodParam, String name, String value);

    /**
     * 查询设备基本字段信息（【缓存模式】）
     *
     * @param methodParam 系统参数
     * @return 设备基本字段信息
     */
    List<String> getAssetCommonFieldList(MethodParam methodParam);
}
