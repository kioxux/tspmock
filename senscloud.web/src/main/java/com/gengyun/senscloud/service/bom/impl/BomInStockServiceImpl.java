package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.service.bom.BomInStockService;
import org.springframework.stereotype.Service;

/**
 * 备件入库
 * * User: sps
 * * Date: 2019/07/08
 * * Time: 下午15:20
 */
@Service
public class BomInStockServiceImpl implements BomInStockService {
//    private static final Logger logger = LoggerFactory.getLogger(BomInStockServiceImpl.class);
//    @Autowired
//    BomInStockMapper bisMapper;
//    @Autowired
//    AuthService authService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    BomStockListService bomStockListService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//    @Autowired
//    BomInventoryMapper bomInventoryMapper;
//    @Autowired
//    WorkSheetHandleMapper workSheetHandleMapper;
//    @Autowired
//    WorkflowService workflowService;
//    @Autowired
//    SelectOptionCustomMapper selectOptionCustomMapper;
//    @Autowired
//    LogsService logsService;
//    @Autowired
//    MessageService messageService;
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    StockMapper stockMapper;
//    /**
//     * 获取备件入库列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findBomInStockList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String[] facilitySearch = {};
//        try {
//            facilitySearch = request.getParameterValues("facilities")[0].split(",");
//        } catch (Exception ex) {
//
//        }
//        String keyWord = request.getParameter("keyWord");
//        String status = request.getParameter("status");
//        User user = AuthService.getLoginUser(request);
//        List<Map<String, Object>> dataList = null;
//
//        //首页条件
//        String condition = "";
//        //根据设备组织查询条件，查询用户可见的库房，用于查询备件
//        String stockPermissionCondition = dataPermissionForFacility.getStockPermissionByFacilityAndGroup(schema_name, user, facilitySearch, "stock", null);
//
//        if (keyWord != null && !keyWord.isEmpty()) {
//            stockPermissionCondition += " and (u.username like '%" + keyWord + "%' or st.stock_name like '%" + keyWord + "%' ) ";
//        }
//        if (StringUtils.isNotBlank(status)) {
//            condition += " and bis.status = " + status;
//        }
//
//        int begin = pageSize * (pageNumber - 1);
//        dataList = bisMapper.getBomInStockList(schema_name, condition, stockPermissionCondition, pageSize, begin);
//        int total = bisMapper.countBomInStockList(schema_name, condition, stockPermissionCondition);
//        //查询获取流程taskId、按钮权限等数据
//        handlerFlowData(schema_name, user.getAccount(), dataList);
//        //多时区转换
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("in_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        //把taskId、权限数据补充到列表数据中去
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("in_code");
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", "");
//                data.put("isBtnShow", false);
//                data.put("formKey", "");
//            }
//        }
//    }
//
//    /**
//     * 根据主键查询备件入库数据
//     *
//     * @param schemaName
//     * @param inCode
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryBomInStockById(String schemaName, String inCode) {
//        Map<String, Object> bisMap = bisMapper.queryBomInStockByInCode(schemaName, inCode);
//        if (null != bisMap && bisMap.size() > 0) {
//            HttpServletRequest request = HttpRequestUtils.getRequest();
//            User user = AuthService.getLoginUser(request);
//            user = commonUtilService.checkUser(schemaName, user, request);
//            boolean feeRight = pagePermissionService.getPermissionByKey(schemaName, user.getId(), "bom_in_stock", "bis_fee");
//            bisMap.put("feeRight", feeRight);
//        }
//        return bisMap;
//    }
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String result = this.findBomInStockList(request); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String inCode = request.getParameter("in_code");
//
//        Map<String, Object> bomInStock = bisMapper.queryBomInStockByInCode(schemaName, inCode);
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        int status = (Integer) bomInStock.get("status");
//        if (StatusConstant.COMPLETED != status && StatusConstant.CANCEL != status) {//未完成的调拨单可以分配
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_31, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/bom_in_stock/findSingleBisInfo")
//                .addObject("workCode", inCode)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", inCode)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @param pageType
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);//备件入库管理，发起的备件入库
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> bomInStock = (Map<String, Object>) dataInfo.get("_sc_bom_in_stock");
//        JSONArray bomInStockDetails = JSONArray.fromObject(map_object.get("bomContent3")); // 备件入库详情数据列表
//        Map<String, Object> keys = (Map<String, Object>) map_object.get("keys");
//        String key = (String) keys.get("_sc_bom_in_stock");
//        boolean isNew = key.startsWith(SqlConstant.IS_NEW_DATA) ? true : false;//判断是否是新增
//        String work_request_type = "";
//        try {
//            work_request_type = (String) map_object.get("work_request_type");
//        } catch (Exception statusExp) {
//        }
//        boolean hasAudit = WorkRequestType.CONFIRM.getType().equals(work_request_type) ? true : false;//流程是否包含审核节点
//        String roleIds = (String) map_object.get("roleIds");
//        return addBomInStock(schema_name, doFlowKey, user, processDefinitionId, roleIds, bomInStock, bomInStockDetails, null, isNew, hasAudit);
//    }
//
//    /**
//     * 生成备件入库数据、流程
//     *
//     * @param schema_name
//     * @param doFlowKey
//     * @param user
//     * @param processDefinitionId
//     * @param roleIds
//     * @param bomInStock
//     * @param bomInStockDetails
//     * @param source
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel addBomInStock(String schema_name, String doFlowKey, User user, String processDefinitionId, String roleIds, Map<String, Object> bomInStock,
//                                       JSONArray bomInStockDetails, String source, boolean isNew, boolean hasAudit) throws Exception {
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && Integer.valueOf(doFlowKey) == ButtonConstant.BTN_SUBMIT) {
//            int status;//状态
//            if (hasAudit)
//                status = StatusConstant.TO_BE_AUDITED;//如果是带审核节点的流程，则订单状态为“待审核”
//            else
//                status = StatusConstant.COMPLETED;//如果流程不需要审核，则订单状态为“已完成”
//
//            String account = user.getAccount();
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            String code = (String) bomInStock.get("in_code");
//            bomInStock.put("status", status);
//            bomInStock.put("create_user_account", account);
//            bomInStock.put("create_time", now);
//            if (isNew) {
//                workSheetHandleMapper.insert(schema_name, bomInStock, "_sc_bom_in_stock"); //保存备件入库主表信息
//            } else {
//                workSheetHandleMapper.update(schema_name, bomInStock, "_sc_bom_in_stock", "in_code"); // 修改
//            }
//            //保存入库详情表信息
//            ResponseModel result = doSaveBomInfo(schema_name, code, bomInStockDetails);
//            if (result != null) {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return result;
//            }
//            //流程如果没有审核节点，则直接审核通过，并变更备件入库信息
//            if (!hasAudit) {
//                updateBomStockQuantity(schema_name, account, bomInStockDetails, bomInStock);
//                //如果时从备件盘点发起的入库申请，更新盘点备件出入库处理结果
//                if (StringUtils.isNotBlank(source) && source.startsWith("BID"))
//                    updateBomInventoryStatus(schema_name, bomInStockDetails.getJSONObject(0).getString("bom_code"), bomInStockDetails.getJSONObject(0).getString("material_code"), source);
//            }
//            String title = selectOptionService.getLanguageInfo(LangConstant.PART_STORE); // 备件入库
//            //流程数据
//            Map map = new HashMap<>();
//            map.put("title_page", title);//备件入库
//            map.put("sub_work_code", code);
//            map.put("workCode", code);
//            map.put("do_flow_key", doFlowKey);
//            map.put("status", status);
//            map.put("create_user_account", account);//发送短信人
//            map.put("create_time", UtilFuns.sysTime());
//            map.put("facility_id", null);
//            // 设置处理人（随机取一个库房管理人）
//            String receive_account = this.getBomStockUserRandom(schema_name, roleIds, (String) bomInStock.get("stock_code"));
//            map.put("receive_account", receive_account); // 短信接受人
//            if (isNew) {
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FLOW_START_FAIL));//备件入库流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, code, status, account, Constants.PROCESS_CREATE_TASK); // 进度记录
//            } else {
//                String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, code);
//                if (StringUtils.isBlank(taskId))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FLOW_ERROR_APPLY_FAIL));//备件入库流程信息异常，备件入库申请失败
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, code);
//                boolean isSuccess = workflowService.complete(schema_name, taskId, account, map);
//                if (!isSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_FLOW_START_FAIL));//备件入库流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, code, status, account, taskSid); // 进度记录
//            }
//            messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, receive_account, SensConstant.BUSINESS_NO_31, code,
//                    account, user.getUsername(), null, null); // 发送短信
//            logService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31), code, (isNew ? selectOptionService.getLanguageInfo(LangConstant.NEW_K) : selectOptionService.getLanguageInfo(LangConstant.MOD_A)) + selectOptionService.getLanguageInfo(LangConstant.BOM_IN_STOCK_SUCC), account); // 记录历史;创建;修改;备件入库申请成功
//            return ResponseModel.ok("ok");
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    /**
//     * 保存备件入库详情表数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @param bomInStockDetails
//     */
//    private ResponseModel doSaveBomInfo(String schemaName, String subWorkCode, JSONArray bomInStockDetails) throws Exception {
//        bisMapper.deleteBySubWorkCode(schemaName, "_sc_bom_in_stock_detail", subWorkCode);// 删除原数据
//        int bomSize = bomInStockDetails.size();
//        if (bomSize > 0) {
//            List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//            Map<String, Object> bomRow = null;
//            for (Object rowData : bomInStockDetails) {
//                net.sf.json.JSONObject bomRowJson = net.sf.json.JSONObject.fromObject(rowData);
//                if (!bomRowJson.containsKey("in_type") || StringUtils.isBlank(bomRowJson.getString("in_type")) || "-1".equals(bomRowJson.getString("in_type")))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_TYPE_IS_NULL));//备件入库类型不能为空
//
//                if (!bomRowJson.containsKey("price") || StringUtils.isBlank(bomRowJson.getString("price")))
//                    bomRowJson.put("price", 0);
////                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_PRICE_IS_NULL));//备件价格不能为空
//
//                if (!UtilFuns.isNumeric(bomRowJson.getString("price")))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_PRICE_WRONG));//备件价格格式不合法
//
//                if (!bomRowJson.containsKey("use_count"))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_QUANTITY_IS_NULL));//备件数量不能为空
//
//                //备件入库货币单位不能为空
//                if (!bomRowJson.containsKey("currency_id"))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.CURRENCY)+selectOptionService.getLanguageInfo(LangConstant.NOT_NULL));
//
//                if (!UtilFuns.isInteger(bomRowJson.getString("use_count")))
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_QUANTITY_IS_NOT_NUMBER));//备件数量必须为数字
//
//                bomRow = new HashMap<String, Object>();
//                bomRow.putAll(bomRowJson);
//                bomRow.put("in_code", subWorkCode);//主表主键
//                bomRow.put("quantity", bomRowJson.getInt("use_count"));
//                bomRow.put("currency_id", bomRowJson.getInt("currency_id"));
//                bomRow.put("bom_code", bomRowJson.getString("bom_code"));
//                bomRow.put("material_code", bomRowJson.getString("material_code"));
//                bomRow.put("price", bomRowJson.getString("price"));
//                bomRow.put("batch_no", bomRowJson.get("batch_no"));
//                if (bomRowJson.containsKey("file_ids"))
//                    bomRow.put("file_ids", bomRowJson.getString("file_ids"));
//                else
//                    bomRow.put("file_ids", null);
//
//                bomRow.put("in_type", bomRowJson.getString("in_type"));
//                bomRow.put("buy_time", bomRowJson.get("buy_time"));
//                dataList.add(bomRow);
//            }
//            //每次插入10条到数据库
//            selectOptionService.batchInsertDatas(schemaName, dataList, "_sc_bom_in_stock_detail", SqlConstant._sc_bom_in_stock_detail_columns);
//        }
//        return null;
//    }
//
//    /**
//     * 备件库房数量更新
//     *
//     * @param schema_name
//     * @param account
//     */
//    private void updateBomStockQuantity(String schema_name, String account, Map<String, Object> bomInStock) throws Exception {
//        String in_code = (String) bomInStock.get("in_code");
//        String stock_code=(String) bomInStock.get("stock_code");
//        StockData stockData =stockMapper.findStockByStockCode(schema_name,stock_code);
//        List<Map<String, Object>> bomInStockDetails = bisMapper.queryBomInStockDetailById(schema_name, in_code);
//        int stockCurrency=stockData.getCurrency_id();
//        //是否根据入库修改备件的对外价格
//        SystemConfigData systemConfigData=systemConfigService.getSystemConfigData(schema_name,"synchronize_price");
//        Boolean systemSynchronizePrice=false;
//        Boolean synchronizePrice=false;
//        if(null!=systemConfigData&&"1".equals((systemConfigData).getSettingValue())){
//            systemSynchronizePrice=true;
//        }
//        if (bomInStockDetails != null && !bomInStockDetails.isEmpty()) {
//            for (Map<String, Object> bomInStockDetail : bomInStockDetails) {
//                if(systemSynchronizePrice&&stockCurrency==Integer.parseInt(String.valueOf(bomInStockDetail.get("currency_id")))){
//                    synchronizePrice=true;
//                }
//                updateBomStockQuantity(schema_name, account, String.valueOf(bomInStockDetail.get("quantity")), (String) bomInStockDetail.get("bom_code"), (String) bomInStockDetail.get("material_code"),
//                        stock_code, "0", "0",String.valueOf(bomInStockDetail.get("price")),String.valueOf(bomInStockDetail.get("currency_id")),synchronizePrice);
//            }
//        }
//    }
//
//    /**
//     * 备件库房数量更新
//     *
//     * @param schema_name
//     * @param account
//     */
//    private void updateBomStockQuantity(String schema_name, String account, JSONArray bomInStockDetails, Map<String, Object> bomInStock) throws Exception {
//        String stock_code=(String) bomInStock.get("stock_code");
//        StockData stockData =stockMapper.findStockByStockCode(schema_name,stock_code);
//        int stockCurrency=stockData.getCurrency_id();
//        //是否根据入库修改备件的对外价格
//        SystemConfigData systemConfigData=systemConfigService.getSystemConfigData(schema_name,"synchronize_price");
//        Boolean systemSynchronizePrice=false;
//        Boolean synchronizePrice=false;
//        if(null!=systemConfigData&&"1".equals((systemConfigData).getSettingValue())){
//            systemSynchronizePrice=true;
//        }
//        for (Object rowData : bomInStockDetails) {
//            net.sf.json.JSONObject bomRowJson = net.sf.json.JSONObject.fromObject(rowData);
//            if(systemSynchronizePrice&&stockCurrency==Integer.parseInt(String.valueOf(bomRowJson.get("currency_id")))){
//                synchronizePrice=true;
//            }
//            updateBomStockQuantity(schema_name, account, bomRowJson.getString("use_count"), bomRowJson.getString("bom_code"), bomRowJson.getString("material_code"),
//                    stock_code, "0", "0",String.valueOf(bomRowJson.get("price")),String.valueOf(bomRowJson.get("currency_id")),synchronizePrice);
//        }
//    }
//
//    /**
//     * 修改库存数量
//     *
//     * @param schema_name
//     * @param account
//     * @param quantity
//     * @param bom_code
//     * @param material_code
//     * @param stock_code
//     * @param security_quantity
//     * @param max_security_quantity
//     * @throws Exception
//     */
//    private void updateBomStockQuantity(String schema_name, String account, String quantity, String bom_code, String material_code, String stock_code, String security_quantity, String max_security_quantity,String show_price,String currency_id,boolean synchronizePrice) throws Exception {
//        Map<String, Object> bomRow = new HashMap<>();
//        bomRow.put("isAddStock", "isAddStock"); // 入库
//        bomRow.put("bom_code", bom_code);
//        bomRow.put("material_code", material_code);
//        bomRow.put("quantity", quantity);
//        bomRow.put("stock_code", stock_code);
//        bomRow.put("security_quantity", security_quantity);
//        bomRow.put("max_security_quantity", max_security_quantity);
//        bomRow.put("currency_id", currency_id);
//        if(synchronizePrice){
//            bomRow.put("show_price", show_price);
//        }
//        bomStockListService.updateBomStockQuantity(schema_name, SensConstant.BUSINESS_NO_31, bomRow, account, selectOptionService.getLanguageInfo(LangConstant.IN_STOCK_QUANTITY) + quantity); // 更新备件库存数量;入库数量：
//    }
//
//    /**
//     * 入库申请作废
//     *
//     * @param schema_name
//     * @param user
//     * @param in_code
//     * @return
//     */
//    @Override
//    public ResponseModel cancel(String schema_name, User user, String in_code) {
//        int doUpdate = bisMapper.updateStatus(schema_name, StatusConstant.CANCEL, in_code);
//        if (doUpdate > 0) {
//            boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, in_code, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            if (deleteflow) {
//                String account = user.getAccount();
//                workProcessService.saveWorkProccess(schema_name, in_code, StatusConstant.CANCEL, account, Constants.PROCESS_CANCEL_TASK);//工单进度记录
//                logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31), in_code, selectOptionService.getLanguageInfo(LangConstant.SPARE_STORE_CANCEL), account);//备件入库申请作废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SPARE_STORE_CANCEL_SUCC));//备件入库申请作废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_CANCEL_FLOW_ERROR));//备件入库作废流程调用失败
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_CANCEL_FAIL));//备件入库作废失败
//        }
//    }
//
//    /**
//     * 备件入库审核
//     *
//     * @param schema_name
//     * @param request
//     * @param paramMap
//     * @param user
//     * @param isFinish    审核通过后是否完成入库
//     * @return
//     */
//    @Override
//    public ResponseModel inStockAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, boolean isFinish) throws Exception {
//        String title = isFinish ? selectOptionService.getLanguageInfo(LangConstant.BOM_IN_STOCK_AUDIT) : selectOptionService.getLanguageInfo(LangConstant.BOM_IN_STOCK_CHECK);//备件审核入库;备件验收入库
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_IN_STOCK_INFO_ERROR));//当前备件入库信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> bom_in_stock_tmp = (Map<String, Object>) dataInfo.get("_sc_bom_in_stock");
//        String in_code = (String) bom_in_stock_tmp.get("in_code");
//        String file_ids = (String) bom_in_stock_tmp.get("file_ids");//附件ID
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        String body_property = (String) bom_in_stock_tmp.get("body_property");
//        String roleIds = (String) map_object.get("roleIds");
//        String approve_remark = (String) map_object.get("approve_remark");//意见
//        if (StringUtils.isBlank(in_code) || StringUtils.isBlank(do_flow_key))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//
//        Map<String, Object> bomInStock = bisMapper.queryBomInStockByInCode(schema_name, in_code);
//        if (bomInStock == null)
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_DATE_ERROR_AUDIT_FAIL), title, title));//%s信息异常，%s审核失败
//
//        if (!String.valueOf(bomInStock.get("status")).equals(String.valueOf(StatusConstant.TO_BE_AUDITED)))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_STATUS_ERROR_AUDIT_FAIL), title, title));//%s状态异常，%s审核失败
//
//        String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, in_code);
//        if (StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_FLOW_ERROR_AUDIT_FAIL), title, title));//%s流程信息异常，%s审核失败
//
//        if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key) && StringUtils.isBlank(approve_remark))
//            return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_REMARK_NULL_AUDIT_FAIL), title));//意见不能为空，%s审核失败
//
//        int result = 0;
//        int status = 0;
//        if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//            if (isFinish) {//如果审核同意，且为最后一个审核节点，则更新调拨申请附件
//                status = StatusConstant.COMPLETED;
//                result = bisMapper.updateBomInStockStatus(schema_name, file_ids, status, body_property, in_code);//更新附件，修改状态为“已完成”
//                if (result > 0) {
//                    //备件库房数量更新
//                    updateBomStockQuantity(schema_name, user.getAccount(), bomInStock);
//                    //如果时从备件盘点发起的入库申请，更新盘点备件出入库处理结果
//                    String source_sub_code = (String) bomInStock.get("source_sub_code");
//                    if (StringUtils.isNotBlank(source_sub_code) && source_sub_code.startsWith("BID")) {
//                        List<Map<String, Object>> bomInStockDetails = bisMapper.queryBomInStockDetailById(schema_name, in_code);
//                        Map<String, Object> bomInStockDetail = bomInStockDetails.get(0);
//                        updateBomInventoryStatus(schema_name, (String) bomInStockDetail.get("bom_code"), (String) bomInStockDetail.get("material_code"), source_sub_code);
//                    }
//                }
//            } else {
//                status = StatusConstant.TO_BE_AUDITED;//进入下一个审核节点
//                result = bisMapper.updateBomInStockStatus(schema_name, file_ids, status, body_property, in_code);//修改状态为“待审核”
//            }
//        } else if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {//如果审核退回，则调拨申请状态变更为“处理中”
//            status = StatusConstant.PENDING;
//            result = bisMapper.updateStatusAndBodyProperty(schema_name, status, body_property, in_code);
//        }
//        if (result > 0) {
//            String title_page = "";
//            String logRemark = "";
//            if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//                title_page = title + selectOptionService.getLanguageInfo(LangConstant.RETURN_A);//退回
//                logRemark = title + selectOptionService.getLanguageInfo(LangConstant.AUDIT_BACK);//审核退回
//            } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//                title_page = title + selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);//成功
//                logRemark = title + selectOptionService.getLanguageInfo(LangConstant.AUDIT_SUCC);//审核成功
//            }
//            if (StringUtils.isNotBlank(approve_remark))
//                logRemark += "（" + selectOptionService.getLanguageInfo(LangConstant.WM_OA) + "：" + approve_remark + "）";//意见
//
//            //完成流程
//            Map<String, String> map = new HashMap<>();
//            map.put("sub_work_code", in_code);
//            map.put("title_page", title_page);
//            map.put("do_flow_key", do_flow_key);
//            map.put("facility_id", null);
//            if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//                //如果是审核退回，则退回给申请人
//                map.put("receive_account", (String) bomInStock.get("create_user_account"));
//            } else {
//                // 设置处理人（随机取一个库房管理人）
//                String receive_account = this.getBomStockUserRandom(schema_name, roleIds, (String) bomInStock.get("stock_code"));
//                map.put("receive_account", receive_account); // 短信接受人
//            }
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, in_code);
//            boolean isSuccess = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//            if (isSuccess) {
//                workProcessService.saveWorkProccess(schema_name, in_code, status, user.getAccount(), taskSid);//工单进度记录
//                messageService.msgPreProcess(ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)?qcloudsmsConfig.SMS_10002006:qcloudsmsConfig.SMS_10002005, map.get("receive_account"), SensConstant.BUSINESS_NO_31, in_code,
//                        user.getAccount(), user.getUsername(), null, null); // 发送短信
//                logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31), in_code, logRemark, user.getAccount());
//                return ResponseModel.ok("ok");
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_AUDIT_ERROR));//审核出现错误，请重试
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作出现错误，请重试
//        }
//    }
//
//    /**
//     * 更新备件盘点状态
//     *
//     * @param schemaName
//     * @param bomCode
//     * @param materialCode
//     * @param sourceSubCode
//     * @return
//     */
//    private void updateBomInventoryStatus(String schemaName, String bomCode, String materialCode, String sourceSubCode) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("bom_code", bomCode);
//        map.put("material_code", materialCode);
//        map.put("deal_result", 2);//处理结果为“已入库”
//        map.put("sub_inventory_code", sourceSubCode);
//        bomInventoryMapper.updateBomInventoryStockDetailDealResult(schemaName, map);
//    }
//
//    /**
//     * 随机取一个库房管理人
//     *
//     * @param schema_name
//     * @param roleIds
//     * @param stock_code
//     * @return
//     */
//    @Override
//    public String getBomStockUserRandom(String schema_name, String roleIds, String stock_code) {
//        List<Map<String, Object>> stockUserList = new ArrayList<>();
//        if (StringUtils.isNotBlank(roleIds)) {
//            String[] roleIdArray = roleIds.split(",");
//            for (int i = 0; i < roleIdArray.length; i++) {
//                stockUserList.addAll(selectOptionCustomMapper.findAllStockUserListByRoleIdAndStockCode(schema_name, roleIdArray[i], stock_code));
//            }
//            if (null == stockUserList || stockUserList.size() == 0) {
//                return null;
//            }
//            Random random = new Random();
//            int n = random.nextInt(stockUserList.size());
//            Map<String, Object> stockUser = stockUserList.get(n);//随机取一个库房管理人
//            return (String) stockUser.get("code");
//        } else {
//            return null;
//        }
//    }
//
//    /**
//     * 修改备件对外价格及货币单位
//     *
//     * @param schema_name
//     * @param bomStockData
//     * @return
//     */
//    @Override
//    public synchronized int synchronizePrice(String schema_name, Map<String, Object> bomStockData) {
//        return bisMapper.synchronizePrice(schema_name,bomStockData);
//    }
}
