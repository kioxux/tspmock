package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.ScadaService;
import org.springframework.stereotype.Service;

@Service
public class ScadaServiceImpl implements ScadaService {
//    @Autowired
//    TokenService tokenService;
//
//    @Autowired
//    ResolvingMetaService resolvingMetaService;
//
//    @Autowired
//    FaultServiceImpl faultService;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    IotUrlServiceImpl iotUrlService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Override
//    public ResponseModel getIotData(String schema_name, String asset_code, HttpSession session) {
//        String dataUrl;
//        String outCode;
//        String category_id;
//
//        SimpleDateFormat sDateFormat=new SimpleDateFormat(Constants.DATE_FMT_SS);
//
//        Map<String,String> urlSetting=iotUrlService.getUrlSetting(schema_name);
//
//        dataUrl=urlSetting.get("url");
//
//        List<Map<String,Object>> AssetList=assetMapper.findIotByAssetCode(schema_name,asset_code);
//        if(AssetList.isEmpty()){
//            return ResponseModel.errorMsg( selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//        }
//        Map<String,Object> Asset=AssetList.get(0);
//
//
//        if((int)Asset.get("iot_status")==1){
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NO_IOT));//设备不支持物联
//        }
//        outCode=(String)Asset.get("out_code");
//        category_id=String.valueOf(Asset.get("category_id"));
//        String sub_key=(String) Asset.get("tem_value9");
//        boolean is_sub=false;
//        String varIds="";
//        Map<String, Object> metaMonitor = (Map) resolvingMetaService.monitorMeta(schema_name).get(category_id);
//        if (null == metaMonitor || metaMonitor.isEmpty()) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_MATE_MON_ERROR));//没有监控配置
//        }
//        Map<String, String> keyNameMap = new HashMap<>();
//        Map<String, Object> faultMap=(Map)faultService.getFault(schema_name).get(category_id);
//        //记录间隔
//        Map<String, String> recordIntervalMap = (Map) metaMonitor.get("recordIntervalMap");
//        //key namemap
//        Map<String, String> nameValueMap = (Map) metaMonitor.get("nameValueMap");
//        //报警判定值
//        Map<String, String> judgeAlarmMap = (Map) metaMonitor.get("judgeAlarmMap");
//        //批报，用量判定值
//        Map<String, String> judgeMap = (Map) metaMonitor.get("judgeMap");
//        //状态字段集合
//        List<String> statusList = (List) metaMonitor.get("statusList");
//        //scada字段集合
//        List<String> scadaList = (List) metaMonitor.get("scadaList");
//        //用量字段集合
//        List<String> dosageList = (List) metaMonitor.get("dosageList");
//        //报警字段集合
//        List<String> alarmList = (List) metaMonitor.get("alarmList");
//        //所有需要取得配置
//        //同一个网关对应多个设备
//        if(null!=sub_key&&!"".equals(sub_key)){
//            List<String> keyList=(List) metaMonitor.get("keyList");
//            for(String key:keyList){
//                varIds += (key +sub_key+ ",");
//                keyNameMap.put((key +sub_key),key);
//            }
//            varIds=varIds.substring(0, varIds.length() - 1);
//            is_sub=true;
//        }else{
//            varIds= (String) metaMonitor.get("varIds");
//        }
//
//        try {
//            dataUrl = dataUrl+"/api/site_rt_data/"+ outCode + "?access_token=" + tokenService.getToken(urlSetting) + "&varIds=" + varIds;
//            dataUrl=dataUrl.replaceAll(" ", "%20");
//            String value = HttpRequestUtils.requestByGet(dataUrl);
//            JSONObject result = JSONObject.parseObject(value);
//            String errorCode=result.getString("error_code");
//            if(null!=errorCode||"21336".equals(errorCode)){
//                tokenService.refreshToken(urlSetting);
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_MON_DATA_ERROR));//无物联数据
//            }
//            JSONObject resultSon = JSONObject.parseObject(result.getString("result"));
//            String siteId = resultSon.getString("siteId");
//            if (!siteId.equals(outCode)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_URL_ERROR));//无效链接
//            }
//            List<Map<String, Object>> dataList = (List) JSONArray.parseArray(resultSon.getString("vars"));
//            List<Map<String, Object>> reportDataList=new ArrayList<>();
//            List<Map<String, Object>> alamDataList=new ArrayList<>();
//            List<Map<String, Object>> statusDataList=new ArrayList<>();
//            List<Map<String, Object>> scadaDataList=new ArrayList<>();
//            Map<String,Object> resultMap=new HashMap<>();
//            String alarmKey="";
//            String monitor_id="";
//            boolean isneedAlarm=false;
//            for(Map<String, Object> data:dataList){
//                monitor_id=(String )data.get("id");
//                if(is_sub){
//                    monitor_id=keyNameMap.get((String )data.get("id"));
//                }
//                alarmKey=judgeAlarmMap.get(monitor_id);
//                Map<String, Object> reData=new HashMap<>();
//                reData.put("asset_code",asset_code);
//                reData.put("name",nameValueMap.get(monitor_id));
//                reData.put("key",monitor_id);
//                reData.put("timestamp", sDateFormat.format(new Date((Long) data.get("timestamp"))));
//                reData.put("value",String.valueOf(data.get("value")));
//                if(null!=alarmKey&&alarmKey.startsWith(String.valueOf(data.get("value")))){
//                    isneedAlarm=true;
//                }
//                reportDataList.add(reData);
//
//                if(scadaList.contains(monitor_id)){
//                    scadaDataList.add(reData);
//                }
//                if(alarmList.contains(monitor_id)&&(!String.valueOf(data.get("value")).equals("0"))&&!faultMap.isEmpty()){
//                    reData.put("value",faultMap.get(String.valueOf(data.get("value"))));
//                    alamDataList.add(reData);
//                }
//            }
//            if(isneedAlarm&&!alamDataList.isEmpty()){
//                resultMap.put("alamDataList",alamDataList);
//            }
//            if(!reportDataList.isEmpty()){
//                resultMap.put("reportDataList",reportDataList);
//            }
//            if(!scadaDataList.isEmpty()){
//                resultMap.put("scadaDataList",scadaDataList);
//            }
//            if(!statusList.isEmpty()){
//                resultMap.put("statusList",statusList);
//            }
//            resultMap.put("assetData",Asset);
//           return ResponseModel.ok(resultMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.FAIL_GETDATA));
//        }
//
//    }
}
