package com.gengyun.senscloud.service;

import java.sql.Timestamp;

/**
 * 工单工时记录表
 * User: sps
 * Date: 2020/04/27
 * Time: 下午15:00
 */
public interface WorkRecordHourService {
//    /**
//     * 新增工单工时记录
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @param operate_account
//     * @param begin_time
//     * @param finished_time
//     * @return
//     */
//    int insertWorkRecordHour(String schema_name, String sub_work_code, String operate_account,
//                             Timestamp begin_time, Timestamp finished_time);

    /**
     * 更新工单工时记录
     *
     * @param schema_name 入参
     * @param sub_work_code 入参
     * @param operate_user_id 入参
     * @param finished_time 入参
     */
    int updateWorkRecordHour(String schema_name, String sub_work_code, String operate_user_id,
                             Timestamp finished_time);
//
    /**
     * 更新工单总工时
     *
     * @param schema_name 入参
     * @param sub_work_code 入参
     */
    int updateWorkTotalHour(String schema_name, String sub_work_code);
//
//    /**
//     * 根据信息统计未完成工单工时记录
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @param operate_account
//     * @param isCheckNull
//     * @return
//     */
//    int countWrhByInfo(String schema_name, String sub_work_code, String operate_account, String isCheckNull);
}
