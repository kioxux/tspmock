package com.gengyun.senscloud.service;

/**
 * VMI分析管理
 */
public interface VmiAnalyzeService {
//
//    /**
//     * 查询设备列表
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    List<VmiAssetModel> findVmiAssetList(String schema_name , String condition, int pageSize, int begin);
//
//    /**
//     * 查询设备列表总数
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    int findVmiAssetListNum(String schema_name ,String condition);
//
//    /**
//     * 查询设备、客户的详情信息
//     * @param schema_name
//     * @param facilitiesId
//     * @param assetId
//     * @return
//     */
//    VmiAssetModel findVmiAssetByFacilitiesIdAndAssetId(String schema_name, Long facilitiesId, String assetId);
//
//    /**
//     * 查询报表列表
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    List<VmiAssetModel> findVmiReportList(String schema_name , String condition, String dateCondition, int pageSize, int begin);
//
//    /**
//     * 查询报表列表总数
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    int findVmiReportListNum(String schema_name ,String condition);
//
//    /**
//     * 根据设备code，查询设备物流信息列表
//     * @param schema_name
//     * @param assetCode
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    List<Map<String, Object>> getAssetVmiTransferList(String schema_name,String assetCode, int pageSize, int begin);
//
//    /**
//     * 根据设备code，查询设备物流信息列表总数
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    int countAssetVmiTransferList(String schema_name,String assetCode);
//
//    /**
//     * 根据设备code，查询设备入库信息列表
//     * @param schema_name
//     * @param assetCode
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    List<Map<String, Object>> getAssetStockInList(String schema_name,String assetCode, int pageSize, int begin);
//
//    /**
//     * 根据设备code，查询设备入库信息列表总数
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    int countAssetStockInList(String schema_name,String assetCode);
//
//    /**
//     * 根据设备code，查询设备出库信息列表
//     * @param schema_name
//     * @param assetCode
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    List<Map<String, Object>> getAssetStockOutList(String schema_name,String assetCode, int pageSize, int begin);
//
//    /**
//     * 根据设备code，查询设备出库信息列表总数
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    int countAssetStockOutList(String schema_name,String assetCode);
//
//    /**
//     * 根据设备code，时间区间查询vim总存量
//     * @param request
//     * @param
//     * @return
//     */
//    List<Map<String, Object>> getVmiList(HttpServletRequest request);
//    /**
//     * 根据设备code，时间区间查询vim总存量 es
//     * @param request
//     * @param
//     * @return
//     */
//    List<Map<String, Object>> getVmiListFromEs(HttpServletRequest request);
//
//    /**
//     * 录入物流信息
//     * @param request
//     * @return
//     */
//    ResponseModel doAddVmiLogistics(HttpServletRequest request);
//
//    /**
//     * 物流发货
//     * @param request
//     * @return
//     */
//    ResponseModel doSendVmiLogistics(HttpServletRequest request);
//
//    /**
//     * 物流签收
//     * @param request
//     * @return
//     */
//    ResponseModel doReceiveVmiLogistics(HttpServletRequest request);
//
//    /**
//     * 获取设备颜色列表
//     * @param request
//     * @return
//     */
//    List<Map<String, Object>> getVmiAssetColor(HttpServletRequest request);
//
//    /**
//     * 保存vmi设备颜色配置
//     * @param request
//     * @return
//     */
//    ResponseModel doSaveVmiAssetColor(HttpServletRequest request);
//
//    /**
//     *  查询需要同步的vmi物联数据条数
//     * @param schemaName
//     * @return
//     */
//    public int countAssetVmiTransferList(String schemaName);
//
//    /**
//     * 同步vmi物联数据
//     * @param schemaName
//     */
//    ResponseModel syncVmiIotData(String schemaName, int pageSize, int begin);
}
