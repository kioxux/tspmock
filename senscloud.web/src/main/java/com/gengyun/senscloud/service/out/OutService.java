package com.gengyun.senscloud.service.out;

import com.gengyun.senscloud.model.MethodParam;

/**
 * 对外接口
 */
public interface OutService {
    /**
     * 传递班组、执行人、设备等信息（中法长寿-智慧上锁-外部接口10）
     *
     * @param methodParam   系统参数
     * @param subWorkCode   工单主键
     * @param nextNodeId    下一步节点号
     * @param occurTime     发生时间
     * @param relationId    工单对象
     * @param receiveUserId 工单执行人主键
     */
    void outServerAaaAaa(MethodParam methodParam, String subWorkCode, String nextNodeId, Object occurTime, Object relationId, Object receiveUserId);
}
