package com.gengyun.senscloud.service;

/**
 * 工单派工配置
 * Created by Administrator on 2018/11/20.
 */
public interface WorkSheetPoolConfigService {
//
//
//    /*新增工单派工配置*/
//    int addWorkSheetPoolConfig(String schema_name,WorkSheetPoolConfigModel workSheetPoolConfigModel);
//
//    /*更新工单派工配置*/
//    int updateWorkSheetPoolConfig(String schema_name, WorkSheetPoolConfigModel workSheetPoolConfigModel);
//
//    /*删除工单派工配置列表数据*/
//    int delWorkSheetPoolConfig(String schema_name,String id, Boolean isuse);
//
//    /*查询工单派工配置列表*/
//    List<WorkSheetPoolConfigModel> selectWorkSheetPoolConfig(String schema_name,String condition, int pageSize,int begin);
//
//    /*查询工单派默认工单池有效*/
//    List<WorkSheetPoolConfigModel> selectWorkSheetPoolConfigValid(String schema_name,String condition);
//
//    /*查询工单派工配置详情根据id*/
//    WorkSheetPoolConfigModel selectWorkSheetPoolConfigById(String schema_name,String id);
//
//    /*查询工单派工配置列表数量*/
//    int selectWorkSheetPoolConfigCount(String schema_name,String condition);
//
//    /*查询工单池关联位置*/
//    List<WorkSheetPoolConfigFacilityModel> selectWorkSheetPoolConfigFacilityById(String schema_name, String pool_id);
//
//    /*查询工单池关联人员*/
//    List<WorkSheetPoolConfigRoleModel> selectWorkSheetPoolConfigMemberById(String schema_name, String pool_id);
//
//
//    /*保存工单池设置成员角色*/
//    int addWorkSheetPoolRole(String schema_name,String pool_id,String role_id);
//
//    /*保存工单池设置位置*/
//    int addWorkSheetPoolFacility(String schema_name,String pool_id,Integer facility);
//
//    /*更新工单池设置成员角色*/
//    int updateWorkSheetPoolRole(String schema_name,String pool_id,String role_id);
//
//    /*更新工单池设置位置*/
//    int updateWorkSheetPoolFacility(String schema_name,String pool_id,Integer facility);
//
//    /*查询设置角色是否存在*/
//    int selectPoolmemberByRoleId(String schema_name, String pool_id,String role_id);
//
//    /*查询设置位置是否存在记录*/
//    int selectPoolmemberByFacilityId(String schema_name, String pool_id,Integer facility_id);
//
//   /*查询工单派工配置列表*/
//    List<WorkSheetPoolConfigModel> selectWorkSheetPool(String schema_name);
//
//    //查询工单派工配置列表数量
//    List<WorkSheetPoolConfigRoleModel> selectPoolmember(String schema_name, String pool_id);
//
//    /*删除负责角色根据工单池id*/
//    int delWorkSheetPoolRoleByPoolId(String schema_name, String pool_id);
//
//    /*删除设置位置根据工单池id*/
//    int delWorkSheetPoolFacilityByPoolId(String schema_name, String pool_id);
//
//    //查询工单池关联用户
//    List<User> getPoolUser(String schema_name, String condition);
//
//    /*查询工单派默认工单池有效*/
//    String selectWorkSheetPoolConfigValidPlanWork(String schema_name,String condition);


}
