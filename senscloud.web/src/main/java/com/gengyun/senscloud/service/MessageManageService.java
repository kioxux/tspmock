package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 消息管理中心
 */
public interface MessageManageService {
    /**
     * 获取菜单信息
     */
    Map<String, Map<String, Boolean>> getMessageManagePermission(MethodParam methodParam);

    /**
     * 根据用户分页查询消息列表
     */
    Map<String, Object> findMessagePageByReceiver(MethodParam methodParam, Map<String, Object> pm);


    /**
     * 根据消息id和阅读人更新为已读状态
     */
    void modifyMessageReceiverRead(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 查询接收人的未读消息数量
     */
    Integer getNoReadCountByReceiver(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 根据接收人查询最新三个未读消息
     */
    List<Map<String, Object>> getNoReadThreeCountByReceiver(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 根据消息id获取消息详情
     */
    Map<String, Object> getMessageInfoById(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 发送消息
     */
    void insertMessageRecord(MethodParam methodParam,
                             String msg_content,
                             String business_type,
                             String business_no,
                             Integer type,
                             String receive_mobile,
                             String receive_content,
                             String message_key,
                             String message_param,
                             List<String> receivers,
                             List<String> ccUsers);

}
