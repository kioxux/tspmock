package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.*;

import java.util.List;
import java.util.Map;

public interface CustomersService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getCustomerListPermission(MethodParam methodParam);

    Map<String, Object> findCustomersList(MethodParam methodParam, Map<String, Object> paramMap);

    void newCustomer(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> findById(MethodParam methodParam, FacilitiesModel facilitiesModel);

    void modifyCustomers(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findRelationCustomersList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findRelationAssetList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findPolluteFactorList(MethodParam methodParam, Map<String, Object> paramMap);

    void newPolluteFactor(MethodParam methodParam, Map<String, Object> paramMap);

    void modifyPolluteFactor(MethodParam methodParam, Map<String, Object> paramMap);

    void deletePolluteFactor(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findUnloadingTimeList(MethodParam methodParam, Map<String, Object> paramMap);

    void newUnloadingTime(MethodParam methodParam, Map<String, Object> paramMap);

    void modifyUnloadingTime(MethodParam methodParam, Map<String, Object> paramMap);

    void deleteUnloadingTime(MethodParam methodParam, Map<String, Object> paramMap);

    String getExportCustomer(MethodParam methodParam, CustomerModel asParam);

    String getExportAllCustomer(MethodParam methodParam, CustomerModel asParam);

    List<Map<String, Object>> findRelationAssetPositionForMap(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findAllCustomerPositionForMap(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> findCustomerMapDetail(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> findCustomerCheckMeterHistory(MethodParam methodParam, Map<String, Object> paramMap);

    List<WorkListModel> findCustomerWorksHistory(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> findCustomerCheckMeterPic(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> findCustomerCheckMeterList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String,Object>> findCustomerPolluteFeeList(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findAllCustomerAssetPositionForMap(MethodParam methodParam, Map<String, Object> paramMap);

    void removeCustomer(MethodParam methodParam, Map<String, Object> paramMap);

    void doModifyCustomerFile(MethodParam methodParam, Map<String, Object> paramMap, boolean isAdd);

    List<Map<String, Object>> getCustomerFileList(MethodParam methodParam, Map<String, Object> paramMap);

    void newCustomerContact(MethodParam methodParam, Map<String, Object> pm);

    void modifyCustomerContact(MethodParam methodParam, Map<String, Object> pm);

    void cutCustomerContact(MethodParam methodParam, FacilitiesModel facilitiesModel);

    List<Map<String, Object>> getCustomerContactList(MethodParam methodParam, Map<String, Object> pm);

    Map<String, Object> searchExecSyncResult(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> handExecSync(MethodParam methodParam, Map<String, Object> paramMap);

    void doModifyCustomerRecord(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> getCustomerRecordList(MethodParam methodParam, Map<String, Object> paramMap);

    void editRelationAssetList(MethodParam methodParam, Map<String, Object> paramMap);
}
