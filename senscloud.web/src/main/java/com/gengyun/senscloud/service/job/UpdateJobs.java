package com.gengyun.senscloud.service.job;

/**
 * 执行更新任务接口
 */

public interface UpdateJobs {
//    final static long ONE_Minute = 60 * 1000;
//    final static long ONE_Hour = 60 * 60 * 1000;
//
//    //测试job
//    void fixedDelayJob();
//
//    /*  第三版本的维保计划方法开始，主要对维保计划进行强化 2019-07-30 yzj 注  */
//    // 按维保计划，其中配置的设备类型、设备型号、设备，以不同的条件，进行执行
//    void cronJobToGerenatePlanWorkWithNewModel(String schema_name);
//
//    /**
//     * 新增，按维保的时长，进行工单的作废服务，韵达定制
//     */
//    //超时保养，流水线设备作废服务
//    void cronJobMaintainWorkDiscard(String schema_name);
//
//    //超时保养，IT设备作废服务
//    void cronJobMaintainWorkDiscardForIT(String schema_name);
//
//    /*  第三版本的维保计划方法开始 2019-07-30 yzj 注  */
//
//
//    /*  第二版本的维保计划方法开始 2019-07-30 yzj 注  */
//    // 自动工单的计划任务
//    void cronJobToGeneratePlanWork(String schema_name);
//
//    // 自动关闭工单的任务
//    void cronJobToDuePlanWork(String schema_name);
//    /*  第二版本的维保计划方法结束 2019-07-30 yzj 注  */
//
//
//    // 进行设备监控数据处理，发送短信和生成维修单
//    void cronJobToDealMonitorData();
//
//    //备件设备实际库存数小于安全库存数发送短信消息推送给相关负责人任任务
//    void cronJobBomFewSendInfo();
//
//    //每天更新维修工单的上次故障完修时间，用于统计设备未发生故障的总时间，计算MTBF
//    void cronJobRepairMTBFAutoCalculate(String schema_name);
//
//    //自动同步设备的供应商和制造商到设备关联组织中去，韵达定制
//    void cronUpdateSupplyInfoToDeviceOrganization(String schema_name);
//
//
//    /*  第一版本的维保计划方法开始 2019-07-30 yzj 注
//    // 如果保养过期，则将保养单的状态设置为过期
//    //void cronJobToDueMaintainBill();
//    // 自动生成保养任务，按设备的保养周期进行生成
//    //void cronJob();
//    第一版本的维保计划方法结束 */
//
//
//
//
//
//
//    /* 原始的设备监控服务，后来提到服务中，先注释，后期删除 2019-07-30 yzj 注
//    // 进行设备监控监控数据采集
//    //void cronJobToCollectionMonitorData();
//    // 对同步数据进行实时处理，即使更新到设备监控实时表中
//    //void cronJobToDealMonitorDataRealTime();
//    // 原始的设备监控服务结束 */
}
