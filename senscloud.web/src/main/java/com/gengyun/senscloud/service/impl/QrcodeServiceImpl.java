package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.QrcodeService;
import org.springframework.stereotype.Service;

/**
 * 扫码信息特殊解析处理
 */
@Service
public class QrcodeServiceImpl implements QrcodeService {

//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    AlarmAnomalySendInfoMapper alarmAnomalySendInfoMapper;
//
//    @Autowired
//    MonitorCountService monitorCountService;

    @Override
    public String dosageForCode(MethodParam methodParam, String code) {
//        try{
//            SystemConfigData systemConfigData=systemConfigService.getSystemConfigData(schema_name,"qr_code_prefix");
//            if(null!=systemConfigData&&!"0".equals((systemConfigData).getSettingValue())){
//                String qr_code_prefix=systemConfigData.getSettingValue();
//                if(code.indexOf(qr_code_prefix)!=-1&&code.indexOf("/")!=-1){
//                    String[] keyList=code.split("/");
//                    if(keyList.length>=2){
//                        code=keyList[keyList.length-1];
//                    }
//                }
//            }
//            systemConfigData=systemConfigService.getSystemConfigData(schema_name,"iot_code");
//            if(null==systemConfigData||"0".equals((systemConfigData).getSettingValue())){
//                return code;
//            }
//            Map<String, Object> dataMap= Resolving.resolvingStr(code);
//            String serNumber=((String)dataMap.get("serNumber")).trim();
//            String gatherTime=(String)dataMap.get("gatherTime");
//            SimpleDateFormat sDateFormat=new SimpleDateFormat(Constants.DATE_FMT_SS);
//            SimpleDateFormat sDateFormatDay=new SimpleDateFormat(Constants.DATE_FMT);;
//            SimpleDateFormat sDateFormatYear=new SimpleDateFormat(Constants.DATE_FMT);
//            String gatherTimeDay=sDateFormatDay.format(sDateFormat.parse(gatherTime));
//            String nowDay=sDateFormatDay.format(System.currentTimeMillis());
//            //判断二维码是否是当天的二维码如果不是直接返回编码不做处理
//            if(null==gatherTimeDay||!nowDay.equals(gatherTimeDay)){
//                return serNumber;
//            }
//            Map<String, String> AlarmMap=(Map)dataMap.get("AlarmMap");
//            AlarmAnomalySendInfoResult alarmAnomalySendInfo=new AlarmAnomalySendInfoResult();
//            //如果有报警插入报警异常表
//            AlarmMap.forEach((key, value) -> {
//              if(!"0".equals(value)){
//                  alarmAnomalySendInfo.setAsset_code(serNumber);
//                  alarmAnomalySendInfo.setAlarm_code(value);
//                  alarmAnomalySendInfo.setGather_time(sDateFormat.format(System.currentTimeMillis()));
//                  alarmAnomalySendInfo.setMonitor_name(key);
//                  alarmAnomalySendInfo.setMonitor_value(value);
//                  alarmAnomalySendInfoMapper.addAlarmAnomalySendInfo(schema_name,alarmAnomalySendInfo);
//              }
//            });
//            String[] Consumptions=(String[])dataMap.get("Consumptions");
//            String month="";
//            Map<String, Object> data=new HashMap<>();
//            for(int i=0;i<Consumptions.length-1;i++){
//                if(i==12){
//                    break;
//                }
//                if(i<9){
//                    month="0"+(i+1);
//                }else{
//                    month=String.valueOf((i+1));
//                }
//                String newTime =(sDateFormatYear.format(System.currentTimeMillis())+"-"+month);
//                monitorCountService.deleteMonitorCountData(schema_name, serNumber, newTime);
//                data.put("asset_code", serNumber);
//                data.put("count_time", newTime);//count_value
//                data.put("schema_name", schema_name);
//                data.put("monitor_name", "Month_Dosing_Accumulate_Total");
//                data.put("count_value",Consumptions[i]);
//                data.put("monitor_value_type", 20);
//                data.put("source_type", SensConstant.DOSAGE_QR_CODE);
//                data.put("create_time", new Timestamp(System.currentTimeMillis()));
//                monitorCountService.InsertMonitorCountData(data);
//        }
//            return serNumber;
//        }catch (Exception e){
//            return code;
//        }

        return null;

    }
}