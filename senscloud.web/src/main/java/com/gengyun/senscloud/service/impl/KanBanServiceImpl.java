package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.mapper.KanBanMapper;
import com.gengyun.senscloud.mapper.UserMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.KanBanService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.RegexUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 大屏看板
 */
@Service
public class KanBanServiceImpl implements KanBanService {
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    KanBanMapper kanBanMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    SystemConfigService systemConfigService;


    /**
     * 获取待办故障任务列表
     *
     * @return 待办故障任务列表
     */
    @Override
    public List<Map<String, Object>> getRepairTaskList(MethodParam methodParam) {
        return kanBanMapper.findRepairWorksList(methodParam.getSchemaName());
    }

    /**
     * 获取待办维护任务列表
     *
     * @return 待办维护任务列表
     */
    @Override
    public List<Map<String, Object>> getMaintainTaskList(MethodParam methodParam) {
        return kanBanMapper.findMaintainWorksList(methodParam.getSchemaName());
    }

    /**
     * 今日完成  本周完成  本月完成
     *
     * @param methodParam 入参
     * @return 今日完成  本周完成  本月完成
     */
    @Override
    public Map<String, Object> getFinishedTaskCount(MethodParam methodParam) {
        return kanBanMapper.findFinishedTaskCount(methodParam.getSchemaName());
    }

    /**
     * 当前上班人员列表
     *
     * @param methodParam 入参
     * @return 当前上班人员列表
     */
    @Override
    public List<Map<String, Object>> getOnDutyUserList(MethodParam methodParam) {
        List<Map<String, Object>> user_list = kanBanMapper.findOnDutyUserList(methodParam.getSchemaName());
        //给每个用户找出所属的岗位名称和部门级联
        for (Map<String, Object> pm : user_list) {
            String nameString = userMapper.findGroupPositionNameString(methodParam.getSchemaName(), (String) pm.get("id"));
            pm.put("groupPositionName", nameString);
        }
        return user_list;
    }

    /**
     * 故障任务总数
     * 当前的故障任务数   今日新增故障任务数   总数
     *
     * @param methodParam 入参
     * @return 故障任务总数
     */
    @Override
    public Map<String, Object> getRepairFinishedCount(MethodParam methodParam) {
        return kanBanMapper.findRepairFinishedCount(methodParam.getSchemaName());
    }

    /**
     * 维护任务总数
     * 当前的维护任务数   今日新增维护任务数   总数
     *
     * @param methodParam 入参
     * @return 维护任务总数
     */
    @Override
    public Map<String, Object> getMaintainFinishedCount(MethodParam methodParam) {
        return kanBanMapper.findMaintainFinishedCount(methodParam.getSchemaName());
    }

    /**
     * 获取本年设备利用率列表
     *
     * @param methodParam
     * @return
     */
    @Override
    public Map<String, Object> getNowYearRateList(MethodParam methodParam) {
        Map<String, Object> assetRate = new HashMap<>();
        try {
            Date date = new Date();
            Calendar c = Calendar.getInstance();
            // 获取当前的年份
            int year = c.get(Calendar.YEAR);
            // 定义时间格式
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            // 开始日期为当前年拼接1月份
            Date startDate = sdf.parse(year + "-01");
            // 结束日期为当前年拼接12月份
            Date endDate = sdf.parse(year + "-12");
            // 设置calendar的开始日期
            c.setTime(startDate);
            // 当前时间小于等于设定的结束时间
            Calendar cal = Calendar.getInstance();//定义日期实例
            //设置年份
            cal.set(Calendar.YEAR, year);
            while (c.getTime().compareTo(endDate) <= 0) {
                String year_month = sdf.format(c.getTime());
                int month = Integer.valueOf(year_month.substring(5));
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month - 1);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd ");
                String lastDayOfMonth = format.format(calendar.getTime());
                String start_date = year_month + "-01 00:00:00";
                String end_date = null;
                if (year_month.equals(sdf.format(date.getTime()))) {
                    SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd");//定义起始日期
                    end_date = sd1.format(date) + " 23:59:59";
                } else {
                    end_date = lastDayOfMonth + " 23:59:59";
                }
                System.out.println(year_month + "  " + start_date + " " + end_date);
                Map<String, Object> asset_rate = kanBanMapper.findMonthAssetRate(methodParam.getSchemaName(), start_date, end_date);
                asset_rate.put("month", month);
                assetRate.put(month + "", asset_rate);
                // 当前月份加1
                c.add(Calendar.MONTH, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, Object> sys = systemConfigService.getMapSystemConfigData(methodParam.getSchemaName(), "equipment_utilization_trend_target");
        if (RegexUtil.optNotNull(sys).isPresent() && RegexUtil.optIsPresentStr(sys.get("setting_value"))) {
            assetRate.put("standLine", RegexUtil.optStrOrBlank(sys.get("setting_value")));
        }
        return assetRate;
    }

    /**
     * 本月运行时间 本月故障时间 本月维护时间
     */
    @Override
    public Map<String, Object> getAssetRunningTime(MethodParam methodParam) {
        return kanBanMapper.findAssetRunningTime(methodParam.getSchemaName());
    }

    /**
     * 查询组织的利用率
     */
    @Override
    public List<Map<String, Object>> getAssetByOrgUtilizationRate(MethodParam methodParam) {
        List<Map<String, Object>> list = kanBanMapper.findAssetByOrgUtilizationRate(methodParam.getSchemaName());
        return list;
    }
    /**-----------------------故障情况-------------------------------------*/
    /**
     * 今日故障数本周故障数本月故障数
     */
    @Override
    public Map<String, Object> getRepairCount(MethodParam methodParam) {
        return kanBanMapper.findRepairCount(methodParam.getSchemaName());
    }

    /**
     * 当天修复情况
     */
    @Override
    public List<Map<String, Object>> getTodayRepairSituation(MethodParam methodParam) {
        return kanBanMapper.findTodayRepairSituation(methodParam.getSchemaName());
    }

    /**
     * 当天完好率
     */
    @Override
    public Map<String, Object> getTodayIntactRate(MethodParam methodParam) {
        return kanBanMapper.findTodayIntactRate(methodParam.getSchemaName());
    }

    /**
     * 当天故障分布
     */
    @Override
    public List<Map<String, Object>> getFaultDistribution(MethodParam methodParam) {
        return kanBanMapper.findFaultDistribution(methodParam.getSchemaName());
    }

    /**
     * 故障趋势
     */
    @Override
    public Map<String, Object> getFaultTrend(MethodParam methodParam) {
        List<Map<String, Object>> list = kanBanMapper.findFaultTrend(methodParam.getSchemaName());
        Map<String, Object> result = new HashMap<>();
        result.put("list", list);
        Map<String, Object> sys = systemConfigService.getMapSystemConfigData(methodParam.getSchemaName(), "fault_trend_target");
        if (RegexUtil.optNotNull(sys).isPresent() && RegexUtil.optIsPresentStr(sys.get("setting_value"))) {
            result.put("standLine", RegexUtil.optStrOrBlank(sys.get("setting_value")));
        }
        return result;
    }
    /**-----------------------维护情况-------------------------------------*/
    /**
     * 今日维护数本周维护数本月维护数
     */
    @Override
    public Map<String, Object> getMainCount(MethodParam methodParam) {
        return kanBanMapper.findMainCount(methodParam.getSchemaName());
    }

    /**
     * 当天设备点检
     */
    @Override
    public List<Map<String, Object>> getTodayEquipmentSpotCheck(MethodParam methodParam) {
        return kanBanMapper.findTodayEquipmentSpotCheck(methodParam.getSchemaName());
    }

    /**
     * 当月常规维护
     */
    @Override
    public Map<String, Object> getMonthRoutineMaintenance(MethodParam methodParam) {
        return kanBanMapper.findMonthRoutineMaintenance(methodParam.getSchemaName());
    }

    /**
     * 当天常规维护
     */
    @Override
    public Map<String, Object> getTodayRoutineMaintenance(MethodParam methodParam) {
        return kanBanMapper.findTodayRoutineMaintenance(methodParam.getSchemaName());
    }

    /**
     * 维护趋势
     */
    @Override
    public Map<String, Object> getMainTrend(MethodParam methodParam) {
        List<Map<String, Object>> list = kanBanMapper.findMainTrend(methodParam.getSchemaName());
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> sys = systemConfigService.getMapSystemConfigData(methodParam.getSchemaName(), "maintain_trend_target");
        if (RegexUtil.optNotNull(sys).isPresent() && RegexUtil.optIsPresentStr(sys.get("setting_value"))) {
            result.put("standLine", RegexUtil.optStrOrBlank(sys.get("setting_value")));
        }
        result.put("list", list);
        return result;
    }

    /**
     * 获取设备的故障状态（大屏用）
     *
     * @param methodParam 系统参数
     * @return 设备列表
     */
    @Override
    public List<Map<String, Object>> getAssetRepairStatusForScreen(MethodParam methodParam) {
        return kanBanMapper.findAssetRepairStatusForScreen(methodParam.getSchemaName());
    }
}
