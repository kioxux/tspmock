package com.gengyun.senscloud.service.bom;

/**
 * 备件报废
 */
public interface BomDiacardService {
//    /**
//     * 获取备件报废列表
//     *
//     * @param request
//     * @return
//     */
//    String findBomDiacardList(HttpServletRequest request);
//
//    /**
//     * 根据主键查询备件报废数据
//     *
//     * @param schemaName
//     * @param discardCode
//     * @return
//     */
//    Map<String, Object> queryBomDiacardById(String schemaName, String discardCode);
//
//    /**
//     * 保存事件
//     *
//     * @param schema_name
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @param source
//     * @return
//     */
//    ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String source) throws Exception;
//
//    /**
//     * 新增备件报废申请
//     *
//     * @param schema_name
//     * @param doFlowKey
//     * @param user
//     * @param processDefinitionId
//     * @param roleIds
//     * @param businessInfo
//     * @param map_object
//     * @param source
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    public ResponseModel addBomDiscard(String schema_name, String doFlowKey, User user, String processDefinitionId, String roleIds, Map<String, Object> businessInfo,
//                                       Map<String, Object> map_object, String source, boolean isNew, boolean hasAudit) throws Exception;
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
//
//    /**
//     * 备件入库审核
//     *
//     * @param schema_name
//     * @param request
//     * @param paramMap
//     * @param user
//     * @return
//     */
//    ResponseModel bomDiscardAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, boolean isLastAudit) throws Exception;
//
//    /**
//     * 入库申请作废
//     *
//     * @param schema_name
//     * @param user
//     * @param discard_code
//     * @return
//     */
//    ResponseModel cancel(String schema_name, User user, String discard_code);
}
