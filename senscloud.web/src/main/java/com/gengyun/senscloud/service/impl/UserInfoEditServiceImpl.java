package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.mapper.UserInfoEditMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.UserInfoEditService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.ScdSesRecordUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户个人配置
 */
@Service
public class UserInfoEditServiceImpl implements UserInfoEditService {
    @Resource
    UserInfoEditMapper userInfoEditMapper;
    @Resource
    SelectOptionService selectOptionService;

    /**
     * 更新用户常用语言类型
     *
     * @param methodParam 系统参数
     * @param lang        语言类型
     */
    @Override
    public void modifyUserConfig(MethodParam methodParam, String lang) {
        String empErr = LangConstant.MSG_BI; // 失败：缺少必要条件！
        RegexUtil.optNotBlankStrOrExp(lang, empErr);
        RegexUtil.optMapOrExp(selectOptionService.getStaticSelectOption(methodParam, Constants.LANG_TYPE_KEY, lang), empErr);
        RegexUtil.intExp(userInfoEditMapper.updateUserConfig(methodParam.getSchemaName(), methodParam.getUserId(), lang), LangConstant.MSG_BK); // 未知错误！
        methodParam.setUserLang(lang);
        MethodParam aaa = RegexUtil.optNotBlankStrOpt(methodParam.getToken()).map(t -> ScdSesRecordUtil.getScdSesDataByKey(t, "loginBaseUserInfo")).filter(io -> RegexUtil.optNotNull(io).isPresent()).map(io -> (MethodParam) io).orElseGet(MethodParam::new);
        aaa.setUserLang(lang);
    }
}
