package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.WorkSheetPoolConfigService;
import org.springframework.stereotype.Service;

/**
 * 工单派工配置
 * Created by Administrator on 2018/11/20.
 */
@Service
public class WorkSheetPoolConfigServiceImpl implements WorkSheetPoolConfigService {

//    @Autowired
//    WorkSheetPoolConfigMapper workSheetPoolConfigMapper;
//    /**
//     * 功能：新增工单派工配置
//     * @param schema_name
//     * @param workSheetPoolConfigModel
//     * @return
//     */
//    @Override
//    public int addWorkSheetPoolConfig(String schema_name, WorkSheetPoolConfigModel workSheetPoolConfigModel) {
//        return workSheetPoolConfigMapper.addWorkSheetPoolConfig(schema_name,workSheetPoolConfigModel);
//    }
//
//    /*更新工单派工配置*/
//    @Override
//    public int updateWorkSheetPoolConfig(String schema_name, WorkSheetPoolConfigModel workSheetPoolConfigModel) {
//        return workSheetPoolConfigMapper.updateWorkSheetPoolConfig(schema_name,workSheetPoolConfigModel);
//    }
//
//    /**
//     * 功能：删除工单派工配置列表数据
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Override
//    public int delWorkSheetPoolConfig(String schema_name, String id ,Boolean isuse) {
//        return workSheetPoolConfigMapper.delWorkSheetPoolConfig(schema_name,id,isuse);
//    }
//
//    /**
//     * 功能：查询工单派工配置列表
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<WorkSheetPoolConfigModel> selectWorkSheetPoolConfig(String schema_name,String condition, int pageSize,int begin) {
//        List<WorkSheetPoolConfigModel> workSheetPoolConfigModels = workSheetPoolConfigMapper.selectWorkSheetPoolConfig(schema_name ,condition, pageSize, begin);
//        if(workSheetPoolConfigModels!=null && workSheetPoolConfigModels.size()>0){
//            for(int i=0;i<workSheetPoolConfigModels.size();i++){
//                List<WorkSheetPoolConfigFacilityModel> workSheetPoolConfigFacilityModels = workSheetPoolConfigMapper.selectWorkSheetPoolConfigFacilityById(schema_name , workSheetPoolConfigModels.get(i).getId());
//                List<WorkSheetPoolConfigRoleModel> workSheetPoolConfigRoleModels = workSheetPoolConfigMapper.selectWorkSheetPoolConfigRoleById(schema_name , workSheetPoolConfigModels.get(i).getId());
//                workSheetPoolConfigModels.get(i).setRoleModels(workSheetPoolConfigRoleModels);
//                workSheetPoolConfigModels.get(i).setFacilityModels(workSheetPoolConfigFacilityModels);
//            }
//        }
//        return  workSheetPoolConfigModels;
//    }
//    /**
//     * 功能：查询工单派默认工单池有效
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<WorkSheetPoolConfigModel> selectWorkSheetPoolConfigValid(String schema_name , String condition) {
//        return  workSheetPoolConfigMapper.selectWorkSheetPoolConfigValid(schema_name , condition);
//    }
//
//    /**
//     * 功能：查看详情根据id
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Override
//    public WorkSheetPoolConfigModel selectWorkSheetPoolConfigById(String schema_name,String id){
//        WorkSheetPoolConfigModel workSheetPoolConfigModel = workSheetPoolConfigMapper.selectWorkSheetPoolConfigById(schema_name , id);
//        if(workSheetPoolConfigModel!=null){
//            String pool_id = workSheetPoolConfigModel.getId();
//            List<WorkSheetPoolConfigFacilityModel> workSheetPoolConfigFacilityModels = workSheetPoolConfigMapper.selectWorkSheetPoolConfigFacilityById(schema_name , pool_id);
//            List<WorkSheetPoolConfigRoleModel> workSheetPoolConfigRoleModels = workSheetPoolConfigMapper.selectWorkSheetPoolConfigRoleById(schema_name , pool_id);
//            workSheetPoolConfigModel.setFacilityModels(workSheetPoolConfigFacilityModels);
//            workSheetPoolConfigModel.setRoleModels(workSheetPoolConfigRoleModels);
//            return  workSheetPoolConfigModel;
//        }else{
//            WorkSheetPoolConfigModel workSheetPoolConfigModel1 = new WorkSheetPoolConfigModel();
//            return  workSheetPoolConfigModel1;
//        }
//    }
//
//    /*查询工单池关联位置*/
//    @Override
//    public List<WorkSheetPoolConfigFacilityModel> selectWorkSheetPoolConfigFacilityById(String schema_name,String pool_id){
//        return workSheetPoolConfigMapper.selectWorkSheetPoolConfigFacilityById(schema_name , pool_id);
//    }
//
//    /*查询工单池关联人员*/
//    @Override
//    public List<WorkSheetPoolConfigRoleModel> selectWorkSheetPoolConfigMemberById(String schema_name,String pool_id){
//        return workSheetPoolConfigMapper.selectWorkSheetPoolConfigRoleById(schema_name , pool_id);
//    }
//
//    /**
//     * 功能：查询列表数据总条数
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public int selectWorkSheetPoolConfigCount(String schema_name,String condition) {
//        return workSheetPoolConfigMapper.selectWorkSheetPoolConfigCount(schema_name,condition);
//    }
//
//    @Override
//    public int addWorkSheetPoolRole(String schema_name,String pool_id,String role_id){
//        return workSheetPoolConfigMapper.addWorkSheetPoolRole(schema_name , pool_id , role_id);
//    }
//
//    @Override
//    public int addWorkSheetPoolFacility(String schema_name,String pool_id,Integer member){
//        return workSheetPoolConfigMapper.addWorkSheetPoolFacility(schema_name , pool_id , member);
//    }
//    /**
//     * 功能：查询工单派工
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<WorkSheetPoolConfigModel> selectWorkSheetPool(String schema_name) {
//        return workSheetPoolConfigMapper.selectWorkSheetPool(schema_name);
//    }
//   /**
//     * 功能：查询列表数据总条数
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<WorkSheetPoolConfigRoleModel> selectPoolmember(String schema_name, String pool_id){
//        return workSheetPoolConfigMapper.selectPoolmember(schema_name,pool_id);
//
//    }
//
//    /*删除负责角色根据工单池id*/
//    @Override
//    public int delWorkSheetPoolRoleByPoolId(String schema_name, String pool_id){
//        return workSheetPoolConfigMapper.delWorkSheetPoolRoleByPoolId(schema_name,pool_id);
//    }
//
//    /*删除设置位置根据工单池id*/
//    @Override
//    public int delWorkSheetPoolFacilityByPoolId(String schema_name, String pool_id){
//        return workSheetPoolConfigMapper.delWorkSheetPoolFacilityByPoolId(schema_name,pool_id);
//    }
//
//    /*更新工单池设置成员角色*/
//    @Override
//    public int updateWorkSheetPoolRole(String schema_name,String pool_id,String role_id){
//        return workSheetPoolConfigMapper.updateWorkSheetPoolRole(schema_name,pool_id,role_id);
//    }
//
//    /*更新工单池设置位置*/
//    @Override
//    public int updateWorkSheetPoolFacility(String schema_name,String pool_id,Integer facility){
//        return workSheetPoolConfigMapper.updateWorkSheetPoolFacility(schema_name,pool_id,facility);
//    }
//
//    /*查询设置角色是否存在*/
//    @Override
//    public int selectPoolmemberByRoleId(String schema_name, String pool_id,String role_id){
//        return workSheetPoolConfigMapper.selectPoolmemberByRoleId(schema_name,pool_id,role_id);
//    }
//
//    /*查询设置位置是否存在记录*/
//    @Override
//    public int selectPoolmemberByFacilityId(String schema_name, String pool_id,Integer facility_id){
//        return workSheetPoolConfigMapper.selectPoolmemberByFacilityId(schema_name,pool_id,facility_id);
//    }
//    /*查询当前工单池下全部人员*/
//    @Override
//    public List<User> getPoolUser(String schema_name, String condition){
//        return workSheetPoolConfigMapper.getPoolUser(schema_name,condition);
//    }
//
//    /**
//     * 功能：查询工单派默认工单池有效
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public String selectWorkSheetPoolConfigValidPlanWork(String schema_name,String condition) {
//        StringBuffer stringBuffer = new StringBuffer();
//        List<WorkSheetPoolConfigModel> workTypeData = workSheetPoolConfigMapper.selectWorkSheetPoolConfigValid(schema_name , condition);
//        if(workTypeData.size()>0){
//            for(int i=0;i<workTypeData.size();i++){
//                stringBuffer.append("<option value=\"" + workTypeData.get(i).getId() + "\">" + workTypeData.get(i).getPool_name() + "</option>");
//            }
//        }
//        return stringBuffer.toString();
//    }
}
