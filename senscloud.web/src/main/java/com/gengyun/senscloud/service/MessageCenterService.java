package com.gengyun.senscloud.service;


/**
 * MessageCenterService interface 消息中心业务接口
 *
 * @author Zys
 * @date 2020/04/10
 */
public interface MessageCenterService {
//
//    /**
//     * 插入消息历史表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    void insertHistoryMessageColumn(String schema_name, Map<String, Object> paramMap);
//
//    /**
//     * 插入消息中心表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    void insertMessageCenterColumn(String schema_name, Map<String, Object> paramMap);
//
//
//    /**
//     * 插入消息接受表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    void insertMessageReceiverColumn(String schema_name, Map<String, Object> paramMap);
//
//    /**
//     * 查询未发送的消息
//     *
//     * @param schema_name
//     * @return
//     */
//    List<Map<String, Object>> queryMessages(String schema_name);
//
//    /**
//     * 修改消息表状态
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    int updateSendStatus(String schema_name, Long id);
//
//    /**
//     * 发送消息
//     * @param schema_name
//     * @param message
//     * @return
//     */
//    void sendMessage(String schema_name, Map<String,Object> message);
//
//    /**
//     * 按条件查询消息
//     *
//     * @param account
//     * @return
//     */
//    JSONObject getMessageList(String schema_name, HttpServletRequest request, String account);
//
//    /**
//     * 按条件查询消息数量
//     *
//     * @param account
//     * @return
//     */
//    int getMessageListCount(String schema_name, String account);
//
//    /**
//     * 修改消息状态
//     *
//     * @param request
//     * @return
//     */
//    int UpdateReadStatus(String schema_name, HttpServletRequest request);
}
