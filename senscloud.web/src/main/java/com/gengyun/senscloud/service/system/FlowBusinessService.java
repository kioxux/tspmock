package com.gengyun.senscloud.service.system;

/**
 * FlowBusinessService interface 业务数据和流程同步接口
 *
 * @author Zys
 * @date 2020/04/10
 */
public interface FlowBusinessService {
//
//    /**
//     * 存在数据则更新 流程业务同步表,不存在则新增一条记录
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @param taskId
//     * @param formKey
//     * @param status
//     * @param dutyAccount
//     * @return
//     */
//     void renovateFlowBusinessData(String schemaName,String subWorkCode,String taskId,String formKey,String status,String dutyAccount);
//
//    /**
//     * 删除已经完成,或者作废的工单的相关数据(不会再有后续操作的数据)
//     *
//     * @param schemaName
//     * @return
//     */
//    int deleteInvalidFlowBusinessData(String schemaName);
//
//    /**
//     * 按subWorkCode删除指定某条数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    int deleteFlowBusinessDataByCode(String schemaName,String subWorkCode);
//
//    /**
//     * 按subWorkCodes查询数据
//     *
//     * @param schemaName
//     * @param subWorkCodes
//     * @return
//     */
//    List<Map<String,Object>> queryFlowBusinessDataByCodes(String schemaName, String subWorkCodes);
//
//    /**
//     * 按subWorkCodes查询key:subWorkCode value:form_key数据
//     *
//     * @param schemaName
//     * @param subWorkCodes
//     * @param keySet
//     * @param valueSet
//     * @return
//     */
//    Map<String,Object> queryFlowBusinessMapByCodes(String schemaName, String subWorkCodes,String keySet,String valueSet);
//
//    /**
//     * 根据formKey查询对应业务流程信息
//     *
//     * @param schemaName
//     * @param formKey
//     * @return
//     */
//    List<Map<String, Object>> queryFlowBusinessDataByFromKey(String schemaName, String formKey);

}
