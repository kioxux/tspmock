package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MetadataWorkService;
import org.springframework.stereotype.Service;

/**
 * 工单模板
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
@Service
public class MetadataWorkServiceImpl implements MetadataWorkService {
//    @Autowired
//    MetadataWorkMapper metadataWorkMapper;
//    @Autowired
//    SerialNumberService serialNumberService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param workTemplateName
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param type
//     * @return
//     */
//    @Override
//    public JSONObject query(String schemaName, String workTemplateName, Integer pageSize, Integer pageNumber,
//                            String sortName, String sortOrder, String type) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String orderBy = null;
//        if (StringUtils.isNotEmpty(sortName)) {
//            if (StringUtils.isEmpty(sortOrder)) {
//                sortOrder = "asc";
//            }
//            orderBy = sortName + " " + sortOrder;
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<MetadataWork> result = metadataWorkMapper.query(schemaName, orderBy, pageSize, begin, workTemplateName, type);
//        int total = metadataWorkMapper.countByCondition(schemaName, workTemplateName, type);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    // 按条件查询工单列表（不分页）
//    @Override
//    public List<MetadataWork> queryAllRootList(String schemaName, String condition) {
//        return metadataWorkMapper.queryByCondition(schemaName, condition);
//    }
//
//    // 查询工单列表
//    @Override
//    public List<MetadataWork> queryRootList(String schemaName, String type) {
//        return metadataWorkMapper.queryAll(schemaName, type);
//    }
//
//    /**
//     * 保存数据
//     *
//     * @param schemaName
//     * @param loginUser
//     * @param metadataWork
//     * @param type
//     */
//    @Override
//    public void saveMetadataWork(String schemaName, User loginUser, MetadataWork metadataWork, String type) {
//        String user = null;
//        if (null != loginUser) {
//            user = loginUser.getAccount();
//        }
//        metadataWork.setCreateUserAccount(user);
//        metadataWork.setIsuse("1"); // 启用
//        // var contentViewTypes = {"common": "公共模板", "field": "自定义模板", "system": "系统模板"};
//        if ("common".equals(type)) {
//            metadataWork.setParentCode("0");
//            metadataWork.setIsCommonTemplate("1");
//        }
//        if ("field".equals(type)) {
//            if (null == metadataWork.getParentCode() || "".equals(metadataWork.getParentCode()) || "-1".equals(metadataWork.getParentCode())) {
//                metadataWork.setParentCode("0");
//            }
//            metadataWork.setIsCommonTemplate("0");
//        }
//        if ("system".equals(type)) {
//            metadataWork.setParentCode("-1");
//        }
//        // 新增
//        if (null == metadataWork.getWorkTemplateCode() || "".equals(metadataWork.getWorkTemplateCode())) {
//            // 设定主键
//            String uuid = serialNumberService.generateMaxBusinessCode(schemaName, "work_template");
//            metadataWork.setWorkTemplateCode(uuid);
//            metadataWorkMapper.insert(schemaName, metadataWork);
//        } else {
//            // 修改
//            metadataWorkMapper.update(schemaName, metadataWork);
//        }
//    }
//
//    /**
//     * 根据主键查询数据
//     *
//     * @param schemaName
//     * @param workTemplateCode
//     * @return
//     */
//    @Override
//    public ResponseModel queryById(String schemaName, String workTemplateCode) {
//        ResponseModel result = new ResponseModel();
//        MetadataWork metadataWork = metadataWorkMapper.queryById(schemaName, workTemplateCode);
//        result.setContent(metadataWork);
//        result.setCode(1);
//        result.setMsg("");
//        return result;
//    }
//
//    /**
//     * 根据主键查询数据（PC业务取模版用）
//     *
//     * @param schemaName
//     * @param workTemplateCode
//     * @return
//     */
//    @Override
//    public ResponseModel queryMwInfoById(String schemaName, HttpServletRequest request, String workTemplateCode) {
//        MetadataWork metadataWork = metadataWorkMapper.queryById(schemaName, workTemplateCode);
//        Map<String, Object> extInfo = selectOptionService.searchLoginUserInfo();
//        metadataWork.setExtInfo(extInfo);
//        //多时区处理
//        SCTimeZoneUtil.responseObjectDataHandler(metadataWork);
//        return ResponseModel.ok(metadataWork);
//    }
//
//    /**
//     * 更新使用状态
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel updateStatus(String schemaName, HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        String workTemplateCode = request.getParameter("workTemplateCode");
//        String isuse = request.getParameter("isuse");
//        int count = metadataWorkMapper.updateStatus(schemaName, isuse, workTemplateCode);
//        if (count > 0) {
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//        result.setCode(1);
//        result.setMsg("");
//        return result;
//    }
//
//    /**
//     * 查询所有可用的模板名称和编号
//     *
//     * @param schemaName
//     * @param type
//     * @param isCommonTemplate
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryCodeAndDesc(String schemaName, String type, String isCommonTemplate) {
//        if (null == type || "".equals(type)) {
//            type = "=";
//        } else {
//            type = "<>";
//        }
//        return metadataWorkMapper.queryCodeAndDesc(schemaName, type, isCommonTemplate);
//    }
//
//    /**
//     * 查询所有可用的公共模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> commonTemplateList(String schemaName) {
//        return metadataWorkMapper.commonTemplateList(schemaName);
//    }
//
//    /**
//     * 查询可用的系统模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    @Override
//    public Map<String, Object> getSystemTemplate(String schemaName) {
//        return metadataWorkMapper.getSystemTemplate(schemaName);
//    }
}
