package com.gengyun.senscloud.service.bom;

/**
 * 备件盘点
 */
public interface BomInventoryService {
//
//    /**
//     * 根据code查询备件盘点数据
//     *
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    Map<String, Object> queryBomInventoryByCode(String schemaName, String inventoryCode);
//
//    /**
//     * 根据code查询盘点位置数据
//     *
//     * @param schemaName
//     * @param subInventoryCode
//     * @return
//     */
//    Map<String, Object> queryBomInventoryStockByCode(String schemaName, String subInventoryCode);
//
//    /**
//     * 查询备件盘点列表
//     *
//     * @param request
//     * @return
//     */
//    String findBomInventoryList(HttpServletRequest request);
//
//    /**
//     * 备件盘点报废
//     * @param request
//     * @return
//     */
//    ResponseModel invalidInventory(HttpServletRequest request);
//
//    /**
//     * 查询备件盘点位置列表
//     * @param request
//     * @return
//     */
//    String findInventoryStockList(HttpServletRequest request);
//
//    /**
//     * 查询盘点备件列表
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    String findInventoryStockDetailList(HttpServletRequest request, String schema_name);
//
//    /**
//     * 查询库房在库备件总数
//     * @param schemaName
//     * @param stockCode
//     * @param bomTypes
//     * @return
//     */
//    int countStockBomQuantity(String schemaName, String stockCode, String bomTypes);
//
//    /**
//     * 启动盘点
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    ResponseModel addInventory(String schemaName, HttpServletRequest request, String processDefinitionId, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * PC盘点
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    ResponseModel pcInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 盘点分配
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    ResponseModel inventoryAllot(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
//
//    /**
//     * 盘点盈余备件的入库
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    public ResponseModel stockInBom(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 盘点盈余备件的报废
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    public ResponseModel stockDiscard(String schemaName, HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 盘点完成
//     * @param schemaName
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    public ResponseModel finishInventory(String schemaName, HttpServletRequest request, Map<String, Object> paramMap);

}
