package com.gengyun.senscloud.service.impl.pollute;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.LogsMapper;
import com.gengyun.senscloud.mapper.MeterMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.job.AbnormalSewageMsgService;
import com.gengyun.senscloud.service.pollute.MeterService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

@Service
public class MeterServiceImpl implements MeterService {
    private static final Logger logger = LoggerFactory.getLogger(MeterServiceImpl.class);
    private static boolean isRunning = false;
    @Resource
    MeterMapper meterMapper;
    @Resource
    LogsMapper logsMapper;
    @Resource
    AbnormalSewageMsgService abnormalSewageMsgService;

    /**
     * 查询抄表记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findMeterList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        Map<String, Object> result = new HashMap<>();
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = meterMapper.findMetersListCount(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        paramMap.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = meterMapper.findMetersList(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 根据客户查询排污口
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findMeterAssertList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("facility_id"), LangConstant.TITLE_AJ_G);//客户编号不能为空
        List<Map<String, Object>> result = meterMapper.findMeterAssertList(methodParam.getSchemaName(), paramMap);
        return result;
    }

    /**
     * 根据客户和排污口查询水质水量信息
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findMeterItemList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        List<Map<String, Object>> result = meterMapper.findMeterItemList(methodParam.getSchemaName(), paramMap);
        return result;
    }

    /**
     * 根据客户和排污口查询检测记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findMeterRecordList(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        List<Map<String, Object>> result = meterMapper.findMeterRecordList(methodParam.getSchemaName(), paramMap);
        return result;
    }

    /**
     * 编辑水质水量记录
     *
     * @param methodParam
     * @param paramMap
     */
    @Override
    public void modifyMeterItem(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        BigDecimal modify_indecation = new BigDecimal(RegexUtil.optStrOrVal(paramMap.get("indication"), "0"));
        if (Objects.equals(paramMap.get("check_item"), "0")) {
            Map<String, Object> rec = RegexUtil.optMapOrNew(findRecentAssetMeterItem(methodParam, paramMap));
            Map<String, Object> after = RegexUtil.optMapOrNew(findAfterAssetMeterItem(methodParam, paramMap));
            BigDecimal rec_indecation = new BigDecimal(RegexUtil.optStrOrVal(rec.get("indication"), "0"));
            BigDecimal after_indecation = new BigDecimal(RegexUtil.optStrOrVal(after.get("indication"), "0"));
            //校验数据是否合理
            if (modify_indecation.compareTo(rec_indecation) < 0 || (after_indecation.compareTo(BigDecimal.ZERO) == 0 ? false : modify_indecation.compareTo(after_indecation) > 0)) {
                RegexUtil.trueExp(true, LangConstant.MSG_BU);
            }
        }
        Map<String, Object> oldMap = meterMapper.findGuessMeterItem(methodParam.getSchemaName(), paramMap);
        meterMapper.modifyMeterItemIndecation(methodParam.getSchemaName(), paramMap);
        //同步更新往后的当前排水量
        List<Map<String, Object>> afterMeterItem = RegexUtil.optNotNullListOrNew(meterMapper.findAfterMeterItemList(methodParam.getSchemaName(), paramMap));
        if (!afterMeterItem.isEmpty()) {
            meterMapper.updateMeterItem(methodParam.getSchemaName(), afterMeterItem);
        }
        //判断是否是复测
        if (paramMap.containsKey("isReSet")) {
            meterMapper.insertMeterRecord(methodParam.getSchemaName(), paramMap);
        } else {
            if (RegexUtil.optBigDecimalOrVal(oldMap.get("indication"), "0", null).compareTo(modify_indecation) != 0) {
                StringBuffer log = new StringBuffer("，：").append(RegexUtil.optStrOrBlank(oldMap.get("check_item_name")))
                        .append("，【当前读数】").append(RegexUtil.optStrOrBlank(oldMap.get("indication"))).append("修改为").append(RegexUtil.optStrOrBlank(modify_indecation));
                logsMapper.insertLog(methodParam.getSchemaName(), SensConstant.BUSINESS_NO_6003, RegexUtil.optStrOrBlank(oldMap.get("facility_id")).concat("-").concat(RegexUtil.optStrOrBlank(oldMap.get("asset_id"))), LangUtil.doSetLogArray("编辑了水质水量记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_BCJN, log.toString()}), methodParam.getUserId(),
                        RegexUtil.optNotBlankStrOpt(paramMap.get("begin_date")).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT_SS)).map(d -> new Timestamp(d.getTime())).orElse(SenscloudUtil.getNowTime()));
            }
        }
    }

    /**
     * 删除检测记录
     *
     * @param methodParam
     * @param paramMap
     */
    @Override
    public void removeMeterRecord(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        Map<String, Object> oldMap = meterMapper.findMeterRecord(methodParam.getSchemaName(), paramMap);
        meterMapper.deleteMeterRecord(methodParam.getSchemaName(), paramMap);
        logsMapper.insertLog(methodParam.getSchemaName(), SensConstant.BUSINESS_NO_6003, RegexUtil.optStrOrBlank(oldMap.get("facility_id")).concat("-").concat(RegexUtil.optStrOrBlank(oldMap.get("asset_id"))), LangUtil.doSetLogArray("删除了检测记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_AK, RegexUtil.optStrOrBlank(oldMap.get("check_item_name"))}), methodParam.getUserId(),
                RegexUtil.optNotBlankStrOpt(paramMap.get("begin_date")).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT_SS)).map(d -> new Timestamp(d.getTime())).orElse(SenscloudUtil.getNowTime()));
    }

    /**
     * 抄表日历
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> findMeterCalendar(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        return meterMapper.findMeterCalendar(methodParam.getSchemaName(), methodParam.getUserId(), paramMap);
    }

    /**
     * 抄表计量统计数据
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findMeterStatistics(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        Map<String, Object> result = RegexUtil.optMapOrNew(meterMapper.findMeterStatistics(methodParam.getSchemaName(), methodParam.getUserId(), paramMap));
        List<Map<String, Object>> list = meterMapper.findIsGeneratePollute(methodParam.getSchemaName(), paramMap);
        result.put("fac", list);
        return result;
    }

    @Override
    public Map<String, Object> findGuessMeter(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        Map<String, Object> guess = RegexUtil.optMapOrNew(meterMapper.findGuessMeterItem(methodParam.getSchemaName(), paramMap));
        //校验数据是否合理
        if (Objects.equals(guess.get("meter_status"), "1") || !Objects.equals(guess.get("check_item"), "0")) {
            RegexUtil.trueExp(true, LangConstant.MSG_CB);
        }
        //获取上个缴费周期的月均值
        guess.put("begin_date", paramMap.get("begin_date"));
        BigDecimal indication = meterMapper.findGuessData(methodParam.getSchemaName(), guess);
        guess.put("indication", indication);
        return guess;
    }

    @Override
    public void modifyGuessMeterItem(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optIntegerOrExpParam(paramMap.get("id"), LangConstant.TITLE_ACJ);//不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        //校验数据是否合理
        if (Objects.equals(paramMap.get("meter_status"), "1") && !Objects.equals(paramMap.get("check_item"), "0")) {
            RegexUtil.trueExp(true, LangConstant.MSG_CB);
        }
        BigDecimal modify_indecation = new BigDecimal(RegexUtil.optStrOrVal(paramMap.get("indication"), "0"));
        Map<String, Object> oldMap = meterMapper.findGuessMeterItem(methodParam.getSchemaName(), paramMap);
        meterMapper.modifyGuessMeterItemIndecation(methodParam.getSchemaName(), paramMap);
        if (RegexUtil.optBigDecimalOrVal(oldMap.get("indication"), "0", null).compareTo(modify_indecation) != 0) {
            StringBuffer log = new StringBuffer("，：").append(RegexUtil.optStrOrBlank(oldMap.get("check_item_name")))
                    .append("估抄为").append(RegexUtil.optStrOrBlank(modify_indecation));
            logsMapper.insertLog(methodParam.getSchemaName(), SensConstant.BUSINESS_NO_6003, RegexUtil.optStrOrBlank(oldMap.get("facility_id")).concat("-").concat(RegexUtil.optStrOrBlank(oldMap.get("asset_id"))), LangUtil.doSetLogArray("估抄了水质水量记录", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_CF, LangConstant.TITLE_BCJN, log.toString()}), methodParam.getUserId(),
                    RegexUtil.optNotBlankStrOpt(paramMap.get("begin_date")).map(t -> RegexUtil.changeToDate(t, "", Constants.DATE_FMT_SS)).map(d -> new Timestamp(d.getTime())).orElse(SenscloudUtil.getNowTime()));
        }
    }

    /**
     * 排污记录查询
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> findMeterSewageRecords(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        Map<String, Object> result = new HashMap<>();
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = meterMapper.findMeterSewageRecordsCount(methodParam.getSchemaName(), paramMap);
        if (total < 1) {
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        paramMap.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = meterMapper.findMeterSewageRecordsList(methodParam.getSchemaName(), paramMap);
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 排污记录图表
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    @Override
    public List<Map<String, Object>> meterSewageRecordsChart(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("begin_date"), LangConstant.TITLE_DATE_C);//日期不能为空
        RegexUtil.optStrOrExpNotNull(paramMap.get("end_date"), LangConstant.TITLE_DATE_D);//日期不能为空
        List<String> timeDay = meterMapper.findTimeDay(methodParam.getSchemaName(), paramMap);
        List<Map<String, Object>> result = new ArrayList<>();
        RegexUtil.optNotNullListStr(timeDay).ifPresent(tm -> {
            List<Map<String, Object>> meterSewageRecordsTime = RegexUtil.optNotNullListOrNew(meterMapper.findMeterSewageRecordsTime(methodParam.getSchemaName(), paramMap));
            tm.forEach(t -> {
                Map<String, Object> item = new LinkedHashMap<>();
                item.put("time", t);
                meterSewageRecordsTime.forEach(s -> {
                    if (t.compareTo(RegexUtil.optStrOrBlank(s.get("begin_date"))) >= 0) {
                        item.put("plan", 1);
                    }
                    if (t.compareTo(RegexUtil.optStrOrBlank(s.get("end_date"))) >= 0) {
                        item.remove("plan");
                    }
                    if (t.compareTo(RegexUtil.optStrOrBlank(s.get("begin_time"))) >= 0) {
                        item.put("actual", 1);
                    }
                    if (t.compareTo(RegexUtil.optStrOrBlank(s.get("end_time"))) >= 0) {
                        item.remove("actual");
                    }
                });
                result.add(item);
            });
        });
        return result;
    }

    @Override
    @Async
    public void asyncCronJobToGenerateAbnormalSewageMsg(String schemaName) {
        logger.info("MeterServiceImpl ~ start-" + RegexUtil.optStrOrBlank(schemaName));
        if (!isRunning) {
            try {
                //加锁，防止定时器重复执行，
                isRunning = true;
                logger.info("MeterServiceImpl ~ start-sub-" + RegexUtil.optStrOrBlank(schemaName));
                abnormalSewageMsgService.cronJobToGenerateAbnormalSewageMsg(schemaName);
                logger.info("MeterServiceImpl ~ end-sub-" + RegexUtil.optStrOrBlank(schemaName));
            } catch (Exception e) {
                logger.error("MeterServiceImpl.cronJobToGenerateAbnormalSewageMsg fail ~ ", e);
            } finally {
                //运行完后释放锁
                isRunning = false;
            }
        }
        logger.info("MeterServiceImpl ~ end-" + RegexUtil.optStrOrBlank(schemaName));
    }

    /**
     * 排污口最近一笔水质水量记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    public Map<String, Object> findRecentAssetMeterItem(MethodParam methodParam, Map<String, Object> paramMap) {
        Map<String, Object> defaultMap = new HashMap();
        defaultMap.put("indication", 0);
        Map<String, Object> recentMap = meterMapper.findRecentAssetMeterItem(methodParam.getSchemaName(), paramMap);
        if (RegexUtil.optIsPresentMap(recentMap))
            return recentMap;
        return defaultMap;
    }

    /**
     * 排污口最近后一笔水质水量记录
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    public Map<String, Object> findAfterAssetMeterItem(MethodParam methodParam, Map<String, Object> paramMap) {
        Map<String, Object> defaultMap = new HashMap();
        defaultMap.put("indication", 0);
        Map<String, Object> afterMap = meterMapper.findAfterAssetMeterItem(methodParam.getSchemaName(), paramMap);
        if (RegexUtil.optIsPresentMap(afterMap))
            return afterMap;
        return defaultMap;
    }
}
