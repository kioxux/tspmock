package com.gengyun.senscloud.service.asset;

import com.gengyun.senscloud.model.AssetCategoryModel;
import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

public interface AssetCategoryService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getAssetCategoryListPermission(MethodParam methodParam);

    /**
     * 获取设备类型列表信息
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 设备类型列表
     */
    List<Map<String, Object>> getAssetCategoryList(MethodParam methodParam, AssetCategoryModel assetCategoryModel);

    /**
     * 获取出自己外的设备类型列表
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 设备类型列表
     */
    List<Map<String, Object>> searchAssetCategoryLists(MethodParam methodParam, AssetCategoryModel assetCategoryModel);

    /**
     * 根据category_id查询自定义字段信息
     *
     * @param methodParam 系统参数
     * @param categoryId  设备类型id
     * @return 自定义字段信息
     */
    Object findAssetCategoryFields(MethodParam methodParam, Integer categoryId);

    /**
     * 获取出自己外的设备类型列表
     *
     * @param methodParam 系统参数
     * @return 设备类型列表
     */
    List<Map<String, Object>> searchIotAssetCategoryLists(MethodParam methodParam);

    /**
     * 克隆设备类型自定义字段
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     */
    void cloneAssetCategoryFields(MethodParam methodParam, AssetCategoryModel assetCategoryModel);


    /**
     * 新增设备类型
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     */
    void newAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel);

    /**
     * 编辑设备类型
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 返回数据
     */
    void modifyAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel);

    /**
     * 删除设备类型
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     */
    void cutAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel);

    /**
     * 查看设备类型详情
     *
     * @param methodParam        系统参数
     * @param assetCategoryModel 请求参数
     * @return 设备类型信息
     */
    Map<String, Object> getAssetCategoryInfo(MethodParam methodParam, AssetCategoryModel assetCategoryModel);

    /**
     * 根据设备类型主键获取设备类型信息
     *
     * @param methodParam       系统参数
     * @param asset_category_id 设备类型主键
     * @return 设备类型信息
     */
    Map<String, Object> getAssetCategoryInfoById(MethodParam methodParam, Integer asset_category_id);

    /**
     * 根据设备类型获取自定义字段
     *
     * @param methodParam       系统参数
     * @param asset_category_id 设备类型主键
     * @return 自定义参数
     */
    String getfieidsById(MethodParam methodParam, Integer asset_category_id);

    /**
     * 验证设备类型是否存在
     *
     * @param schemaName
     * @param assetTypeList
     * @return
     */
    Map<String, Map<String, Object>> queryAssetTypeByCode(String schemaName, List<String> assetTypeList);

    /**
     * 根据设备类型id获取类型父级拼接字符串
     *
     * @param schemaName 数据库
     * @return 设备类型id获取类型父级拼接字符串
     */
    Map<Integer, Map<String, Object>> getCategoryNameString(String schemaName);
}
