package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.TaskTempLateModel;

import java.util.List;
import java.util.Map;

/**
 * 任务模板
 */
public interface TaskTempLateService {
    /**
     * 获取任务模板权限
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getTaskTempLatePermission(MethodParam methodParam);

    /**
     * 新增任务模板
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    String newTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 编辑任务模板
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void modifyTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 删除任务模板
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void cutTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 获取任务模板详情
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板详情
     */
    Map<String, Object> getTaskTempLateInfo(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 获取任务模板详情
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板列表
     */
    Map<String, Object> getTaskTempLatePage(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 根据选中主键导出任务模板信息（文件码）
     *
     * @param methodParam       系统参数
     * @param taskTempLateModel 请求参数
     * @param isAll             是否是全选
     * @return 文件码
     */
    String getExportTaskTempInfo(MethodParam methodParam, TaskTempLateModel taskTempLateModel, boolean isAll);


    /**
     * 新增任务模板任务项关联
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void newTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 删除任务模板任务项关联
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void cutTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 设置组名称
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void modifyTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 任务模板任务项关联列表
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板任务项关联列表
     */
    List<Map<String, Object>> getTaskTempLateItemList(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 任务模板列表
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     * @return 任务模板任务项关联列表
     */
    List<Map<String, Object>> getTaskTempLateList(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 向上移动任务模板任务项
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void moveUpTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 向下移动任务模板任务项
     *
     * @param methodParam       入参
     * @param taskTempLateModel 入参
     */
    void moveDownTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel);

    /**
     * 验证任务模板编号是否存在
     *
     * @param schemaName       数据库
     * @param templateCodeList 任务项模板编号
     * @return 模板编号集合
     */
    List<String> getTempCodesByCodes(String schemaName, List<String> templateCodeList);

    /**
     * 根据编号获取任务模板信息
     *
     * @param schemaName       数据库
     * @param templateCodeList 任务模板编号
     * @return 模板信息集合
     */
    Map<String, Map<String, Object>> getTaskTemplatesByCodes(String schemaName, List<String> templateCodeList);

    /**
     * 批量新增或更新任务模板
     *
     * @param schemaName 数据库
     * @param tempList   任务模板信息
     */
    void batchInsertTaskTemp(String schemaName, List<Map<String, Object>> tempList);

    /**
     * 批量验证任务模板、任务项、组是否存在
     *
     * @param schemaName 数据库
     * @param codeList   编号信息
     * @return 编号集合
     */
    List<String> getTaskTemplateItemByInfo(String schemaName, List<String> codeList);

    /**
     * 批量关联任务模板、任务项、组
     *
     * @param schemaName 数据库
     * @param midList    信息
     */
    void batchInsertTaskTempItem(String schemaName, List<Map<String, Object>> midList);
}
