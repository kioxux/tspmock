package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.DosageService;
import org.springframework.stereotype.Service;

@Service
public class DosageServiceImpl implements DosageService {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    ResolvingMetaService resolvingMetaService;
//
//    @Autowired
//    FaultServiceImpl faultService;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    BatchReportMapper batchReportMapper;
//
//    @Autowired
//    ElasticSearchServiceImpl elasticSearchService;
//
//    @Value("${es.index.monitoring:xinggang.monitoring}")
//    private String index;
//
//    @Value("${es.index.monitoring:xinggang.monitoring_report}")
//    private String report_index;
//
//    @Value("${es.index.monitoring:xinggang.monitoring_receive}")
//    private String receive_index;
//
//    @Value("${es.index.monitoring:xinggang.monitoring_loading}")
//    private String loading_index;
//
//    @Value("${es.index.monitoring:xinggang.monitoring_allocation}")
//    private String allocation_index;
//
//    @Override
//    public String getReportData(HttpServletRequest request, HttpSession session) {
//        int pageNumber = 1;
//        int pageSize = 10;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.parseInt(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter(
//                    "pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.parseInt(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 10;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String asset_code=request.getParameter("asset_code");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String category_id;
//        List<Map<String,Object>> AssetList=assetMapper.findIotByAssetCode(schema_name,asset_code);
//        if(AssetList.isEmpty()){
//            return "{}";//设备不存在
//        }
//        Map<String,Object> Asset=AssetList.get(0);
//        category_id=String.valueOf(Asset.get("category_id"));
//        Map<String, Object> metaMonitor = (Map) resolvingMetaService.monitorMeta(schema_name).get(category_id);
//        if (null == metaMonitor || metaMonitor.isEmpty()) {
//            return "";//没有监控配置
//        }
//        //用量字段集合
//        List<String> dosageList = (List) metaMonitor.get("dosageList");
//        List<String> countSumList = (List) metaMonitor.get("countSumList");
//        List<String> countAvgList = (List) metaMonitor.get("countAvgList");
//        List<String> countMaxList = (List) metaMonitor.get("countMaxList");
//        List<String> countMinList = (List) metaMonitor.get("countMinList");
//        List<String>  newcountSumList =new ArrayList<>();
//        List<String>  newcountAvgList =new ArrayList<>();
//        List<String>  newcountMaxList =new ArrayList<>();
//        List<String>  newcountMinList =new ArrayList<>();
//        for(String countItem:countSumList){
//            if(dosageList.contains(countItem)){
//                newcountSumList.add(countItem);
//            }
//        }
//        for(String countItem:countAvgList){
//            if(dosageList.contains(countItem)){
//                newcountAvgList.add(countItem);
//            }
//        }
//        for(String countItem:countMaxList){
//            if(dosageList.contains(countItem)){
//                newcountMaxList.add(countItem);
//            }
//        }
//        for(String countItem:countMinList){
//            if(dosageList.contains(countItem)){
//                newcountMinList.add(countItem);
//            }
//        }
//        return queryBatchReportList(schema_name,asset_code,pageNumber,pageSize,beginTime,endTime,dosageList,newcountSumList,newcountAvgList,newcountMaxList,newcountMinList,null);
//        //
//    }
//
//    @Override
//    public String getReportTimeData(HttpServletRequest request,HttpSession session) {
//        int pageNumber = 1;
//        int pageSize = 10;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.parseInt(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.parseInt(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 10;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String asset_code=request.getParameter("asset_code");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String category_id;
//        List<Map<String,Object>> AssetList=assetMapper.findIotByAssetCode(schema_name,asset_code);
//        if(AssetList.isEmpty()){
//            return "{}";//设备不存在
//        }
//        Map<String,Object> Asset=AssetList.get(0);
//        category_id=String.valueOf(Asset.get("category_id"));
//        Map<String, Object> metaMonitor = (Map) resolvingMetaService.monitorMeta(schema_name).get(category_id);
//        if (null == metaMonitor || metaMonitor.isEmpty()) {
//            return "";//没有监控配置
//        }
//        //用量字段集合
//        List<String> dosageList = (List) metaMonitor.get("dosageList");
//        List<String> countSumList = (List) metaMonitor.get("countSumList");
//        List<String> countAvgList = (List) metaMonitor.get("countAvgList");
//        List<String> countMaxList = (List) metaMonitor.get("countMaxList");
//        List<String> countMinList = (List) metaMonitor.get("countMinList");
//        List<String>  newcountSumList =new ArrayList<>();
//        List<String>  newcountAvgList =new ArrayList<>();
//        List<String>  newcountMaxList =new ArrayList<>();
//        List<String>  newcountMinList =new ArrayList<>();
//        for(String countItem:countSumList){
//            if(dosageList.contains(countItem)){
//                newcountSumList.add(countItem);
//            }
//        }
//        for(String countItem:countAvgList){
//            if(dosageList.contains(countItem)){
//                newcountAvgList.add(countItem);
//            }
//        }
//        for(String countItem:countMaxList){
//            if(dosageList.contains(countItem)){
//                newcountMaxList.add(countItem);
//            }
//        }
//        for(String countItem:countMinList){
//            if(dosageList.contains(countItem)){
//                newcountMinList.add(countItem);
//            }
//        }
//        return queryBatchReportList(schema_name,asset_code,pageNumber,pageSize,beginTime,endTime,dosageList,newcountSumList,newcountAvgList,newcountMaxList,newcountMinList,"substring (s.gather_time from 1 for "+beginTime.length()+" )");
//    }
//
//    /**
//     * 查询批报数据
//     */
//    private String queryBatchReportList(String schema_name,String searchKey,int pageNumber,int pageSize,
//                                        String beginTime,String endTime,
//                                        List<String> columns,List<String> sumcolumns,
//                                        List<String> avgcolumns,List<String> maxcolumns,
//                                        List<String> mincolumns, String groupString ) {
//
//        JSONObject result = new JSONObject();
//
//        //首页条件
//        String condition = "";
//        String condition2 = "";
//        if (StringUtils.isNotBlank(searchKey)) {
//            condition2 = " and ( asset.strname like '%" + searchKey + "%' or upper(asset.strcode) like upper('%" + searchKey + "%') ) ";
//        }
//
//        if (StringUtils.isNotBlank(beginTime)) {
//            condition += " and am.gather_time >= '" + beginTime + "' ";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            condition += " and am.gather_time <'" + endTime + "' ";
//        }
//        String queryColumns = "";
//        if(null !=  columns && columns.size() > 0){
//            String subCondition = "";
//            for(String column : columns){
//                subCondition += (subCondition==""?"":",") + "'"+column+"'";
//                queryColumns += " MAX(CASE am.monitor_name WHEN '"+ column+"' THEN am.monitor_value ELSE '' END) AS "+column+", ";
//            }
//            condition += " AND am.monitor_name IN (" + subCondition + ") ";
//        }
//        String querySunColumns = "";
//        if( null!= sumcolumns && sumcolumns.size() > 0){
//            for(String column : sumcolumns){
//                querySunColumns += " SUM(CAST ((CASE s."+ column+" WHEN '' THEN '0.00' ELSE s."+column+" END )AS DECIMAL ( 10, 2 ) )) AS "+column+", ";
//            }
//        }
//        if( null!= avgcolumns && avgcolumns.size() > 0){
//            for(String column : avgcolumns){
//                querySunColumns +=" AVG(CAST ((CASE s."+ column+" WHEN '' THEN '0.00' ELSE s."+column+" END )AS DECIMAL ( 10, 2 ) )) AS "+column+", ";
//            }
//        }
//        if( null!= maxcolumns && maxcolumns.size() > 0){
//            for(String column : maxcolumns){
//                querySunColumns += " MAX(CAST ((CASE s."+ column+" WHEN '' THEN '0.00' ELSE s."+column+" END )AS DECIMAL ( 10, 2 ) )) AS "+column+", ";
//            }
//        }
//        if( null!= mincolumns && mincolumns.size() > 0){
//            for(String column : mincolumns){
//                querySunColumns += " MIN(CAST ((CASE s."+ column+" WHEN '' THEN '0.00' ELSE s."+column+" END )AS DECIMAL ( 10, 2 ) )) AS "+column+", ";
//            }
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> dataList;
//        int total;
//        if(null==groupString){
//            dataList=batchReportMapper.findReportList(schema_name, queryColumns, condition, condition2, pageSize, begin);
//            Map map=batchReportMapper.countReportList(schema_name, queryColumns,querySunColumns,condition, condition2);
//            map.put("gather_time",selectOptionService.getLanguageInfo( LangConstant.TOTAL_T));
//            total= Integer.parseInt(String.valueOf(map.get("total")));
//            dataList.add(map);
//        }else{
//            dataList=batchReportMapper.findReportTimeList(schema_name, queryColumns,querySunColumns,groupString ,condition2 + condition,pageSize, begin);
//            total=batchReportMapper.countReportTimeList(schema_name, queryColumns,querySunColumns,groupString , condition2 + condition);
//        }
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//
//    /**
//     * 查询月报 年报
//     *
//     * @param request
//     * @param session
//     * @return
//     */
//    @Override
//    public String getReportMonthAndYearData(HttpServletRequest request, HttpSession session) {
//        JSONObject result = new JSONObject();
//        int pageNumber = 1;
//        int pageSize = 10;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.parseInt(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.parseInt(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 10;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String asset_code=request.getParameter("asset_code");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        if(RegexUtil.isNull(schema_name)||RegexUtil.isNull(asset_code)){
//            return "{}";
//        }
//        String condition="and asset_code='"+asset_code+"'";
//        if (StringUtils.isNotBlank(beginTime)) {
//            condition += " and gather_time >= '" + beginTime + "' ";
//        }
//        if (StringUtils.isNotBlank(endTime)) {
//            condition += " and gather_time <'" + endTime + "' ";
//        }
//        String groupString="substring (gather_time from 1 for "+beginTime.length()+" )";
//        List<Map<String, Object>> dataList;
//        int begin = pageSize * (pageNumber - 1);
//        int total;
//        try{
//            dataList=batchReportMapper.findReportMonthAndYearList(schema_name,groupString,condition,pageSize,begin);
//            Map map=batchReportMapper.countReportMonthAndYearList(schema_name,groupString,condition);
//            map.put("gather_time",selectOptionService.getLanguageInfo( LangConstant.TOTAL_T));
//            dataList.add(map);
//            total=Integer.parseInt(String.valueOf(map.get("total")));
//            result.put("rows", dataList);
//            result.put("total", total);
//            return result.toString();
//        }catch (Exception e){
//            return "{}";
//        }
//
//    }
//
//    /**
//     * 从es查询批报数据
//     */
//    private String queryBatchReportListFromEs(String schema_name,String searchKey,int pageNumber,int pageSize,
//                                        String beginTime,String endTime,
//                                        List<String> columns,List<String> sumcolumns,
//                                        List<String> avgcolumns,List<String> maxcolumns,
//                                        List<String> mincolumns, String groupString )  {
//        JSONObject result = new JSONObject();
//        List<Map<String, Object>> resultdataList;
//        Map<String, Object> resultMap;
//        SimpleDateFormat sDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//        SearchRequest searchRequest = new SearchRequest(report_index);
//        try {
//            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//            RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("System_Time_Tag");
//            if (RegexUtil.isNotNull(beginTime)) {
//                rangeQueryBuilder.gte(sDateFormat.parse(beginTime).getTime());
//            }
//            if (RegexUtil.isNotNull(endTime)) {
//                rangeQueryBuilder.lte(sDateFormat.parse(endTime).getTime());
//            }
//            AggregationBuilder termsBuilder = AggregationBuilders.terms("asset").field("asset_code");
//
//            if (null != sumcolumns && sumcolumns.size() > 0) {
//                for (String column : sumcolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.sum(column).field(column));
//                }
//            }
//            if (null != avgcolumns && avgcolumns.size() > 0) {
//                for (String column : avgcolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.avg(column).field(column));
//                }
//            }
//            if (null != maxcolumns && maxcolumns.size() > 0) {
//                for (String column : maxcolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.max(column).field(column));
//                }
//            }
//            if (null != mincolumns && mincolumns.size() > 0) {
//                for (String column : mincolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.min(column).field(column));
//                }
//            }
//            int begin = pageSize * (pageNumber - 1);
//            sourceBuilder.query(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("asset_code",searchKey)).must(rangeQueryBuilder)).sort("System_Time_Tag", SortOrder.DESC).size(pageSize);
//            sourceBuilder.aggregation(termsBuilder);
//            sourceBuilder.from(begin);
//            searchRequest.source(sourceBuilder).scroll(TimeValue.timeValueMinutes(1L));
//            resultMap = elasticSearchService.search(searchRequest);
//            resultdataList=( List<Map<String,Object>>)resultMap.get("rows");
//            Map<String,Object> asMap= ( Map<String,Object>) resultMap.get("asMap");
//            StringTerms teamAgg= (StringTerms) asMap.get("asset");
//            Iterator<StringTerms.Bucket> teamBucketIt = teamAgg.getBuckets().iterator();
//            Map lastMap=new HashMap();
//            lastMap.put("gather_time",selectOptionService.getLanguageInfo( LangConstant.TOTAL_T));
//            while (teamBucketIt .hasNext()) {
//                StringTerms.Bucket buck = teamBucketIt .next();
//                String team = buck.getKeyAsString();
//                long count = buck.getDocCount();
//                Map subaggmap = buck.getAggregations().asMap();
//                if (null != sumcolumns && sumcolumns.size() > 0) {
//                    for (String column : sumcolumns) {
//                        lastMap.put(column,((InternalSum) subaggmap.get(column)).getValue());
//                    }
//                }
//                if (null != avgcolumns && avgcolumns.size() > 0) {
//                    for (String column : avgcolumns) {
//                        lastMap.put(column,((InternalAvg) subaggmap.get(column)).getValue());
//                    }
//                }
//                if (null != maxcolumns && maxcolumns.size() > 0) {
//                    for (String column : maxcolumns) {
//                        lastMap.put(column,((InternalMax) subaggmap.get(column)).getValue());
//                    }
//                }
//                if (null != mincolumns && mincolumns.size() > 0) {
//                    for (String column : mincolumns) {
//                        lastMap.put(column,((InternalMin) subaggmap.get(column)).getValue());
//                    }
//                }
//
//            }
//            resultdataList.add(lastMap);
//            result.put("rows", resultdataList);
//            result.put("total", resultMap.get("total"));
//            return result.toString();
//        }catch (Exception e){
//            return "{}";
//        }
//    }
//
//    /**
//     * 从es查询批报数据
//     */
//    private String queryTimeBatchReportListFromEs(String schema_name,String searchKey,int pageNumber,int pageSize,
//                                              String beginTime,String endTime,
//                                              List<String> columns,List<String> sumcolumns,
//                                              List<String> avgcolumns,List<String> maxcolumns,
//                                              List<String> mincolumns, String groupString )  {
//        JSONObject result = new JSONObject();
//        List<Map<String, Object>> resultdataList;
//        Map<String, Object> resultMap;
//        SimpleDateFormat sDayFormat = new SimpleDateFormat(Constants.DATE_FMT);;
//        SimpleDateFormat sMonthFormat = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON_2);
//        SimpleDateFormat sYearFormat = new SimpleDateFormat(Constants.DATE_FMT);
//        boolean isDate=false;
//        boolean isMonth=false;
//        boolean isYear=false;
//        if(beginTime.length()==4){
//            isYear=true;
//        }else if(beginTime.length()==7){
//            isMonth=true;
//        }else {
//            isDate=true;
//        }
//        SearchRequest searchRequest = new SearchRequest(report_index);
//        try {
//            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//            RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("System_Time_Tag");
//            if (RegexUtil.isNotNull(beginTime)) {
//                if(isYear){
//                    rangeQueryBuilder.gte(sYearFormat.parse(beginTime).getTime());
//                }else if(isMonth){
//                    rangeQueryBuilder.gte(sMonthFormat.parse(beginTime).getTime());
//                }else if(isDate){
//                    rangeQueryBuilder.gte(sDayFormat.parse(beginTime).getTime());
//                }
//            }
//            if (RegexUtil.isNotNull(endTime)) {
//                if(isYear){
//                    rangeQueryBuilder.lte(sYearFormat.parse(beginTime).getTime());
//                }else if(isMonth){
//                    rangeQueryBuilder.lte(sMonthFormat.parse(beginTime).getTime());
//                }else if(isDate){
//                    rangeQueryBuilder.lte(sDayFormat.parse(beginTime).getTime());
//                }
//            }
//            DateHistogramAggregationBuilder termsBuilder=AggregationBuilders.dateHistogram("time").field("System_Time_Tag");
//            if(isYear){
//                //.offset("+8h"); 若有跨时区需求
//                termsBuilder.calendarInterval(DateHistogramInterval.YEAR);
//                termsBuilder.format(DATE_FMT_YEAR);
//            }else if(isMonth){
//                //.offset("+8h");若有跨时区需求
//                termsBuilder.calendarInterval(DateHistogramInterval.MONTH);
//                termsBuilder.format(Constants.DATE_FMT_YEAR_MON_2);
//            }else if(isDate){
//                //.offset("+8h");若有跨时区需求
//                termsBuilder.calendarInterval(DateHistogramInterval.DAY);
//                termsBuilder.format(Constants.DATE_FMT);
//            }
//            if (null != sumcolumns && sumcolumns.size() > 0) {
//                for (String column : sumcolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.sum(column).field(column));
//                }
//            }
//            if (null != avgcolumns && avgcolumns.size() > 0) {
//                for (String column : avgcolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.avg(column).field(column));
//                }
//            }
//            if (null != maxcolumns && maxcolumns.size() > 0) {
//                for (String column : maxcolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.max(column).field(column));
//                }
//            }
//            if (null != mincolumns && mincolumns.size() > 0) {
//                for (String column : mincolumns) {
//                    termsBuilder.subAggregation(AggregationBuilders.min(column).field(column));
//                }
//            }
//            int begin = pageSize * (pageNumber - 1);
//            sourceBuilder.query(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("asset_code",searchKey)).must(rangeQueryBuilder)).sort("System_Time_Tag", SortOrder.DESC).size(pageSize);
//            sourceBuilder.aggregation(termsBuilder);
//            sourceBuilder.from(begin);
//            searchRequest.source(sourceBuilder).scroll(TimeValue.timeValueMinutes(1L));
//            resultMap = elasticSearchService.search(searchRequest);
//            resultdataList=( List<Map<String,Object>>)resultMap.get("rows");
//            Map<String,Object> asMap= ( Map<String,Object>) resultMap.get("asMap");
//            Histogram timeAgg = (Histogram)asMap.get("time");
//            for(Histogram.Bucket entry:timeAgg.getBuckets()){
//                Map map=new HashMap();
//                map.put("gather_time",entry.getKeyAsString());
//                if (null != sumcolumns && sumcolumns.size() > 0) {
//                    for (String column : sumcolumns) {
//                        map.put(column,((InternalSum) entry.getAggregations().get(column)).getValue());
//                    }
//                }
//                if (null != avgcolumns && avgcolumns.size() > 0) {
//                    for (String column : avgcolumns) {
//                        map.put(column,((InternalAvg)  entry.getAggregations().get(column)).getValue());
//                    }
//                }
//                if (null != maxcolumns && maxcolumns.size() > 0) {
//                    for (String column : maxcolumns) {
//                        map.put(column,((InternalMax) entry.getAggregations().get(column)).getValue());
//                    }
//                }
//                if (null != mincolumns && mincolumns.size() > 0) {
//                    for (String column : mincolumns) {
//                        map.put(column,((InternalMin) entry.getAggregations().get(column)).getValue());
//                    }
//                }
//                resultdataList.add(map);
//            }
//            result.put("rows", resultdataList);
//            result.put("total", timeAgg.getBuckets().size());
//            return result.toString();
//        }catch (Exception e){
//            return "{}";
//        }
//    }
}
