package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.ReportMonthModel;
import com.gengyun.senscloud.model.SearchParam;

import java.util.List;
import java.util.Map;

public interface StatisticService {

    /**
     * 获取菜单信息
     *
     * @param methodParam 入参
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getReportPermission(MethodParam methodParam);

    Map<String, Object> getReportDetailList(MethodParam methodParam, ReportMonthModel roleModel);

    /**
     * 根据id获取统计报表数据
     *
     * @param methodParam 入參
     * @param params      入參
     * @return 统计报表数据
     */
    Map<String, Object> getStatisticsReportData(MethodParam methodParam, Map<String, Object> params);

    /**
     * 根据id获取统计数据
     *
     * @param methodParam 系统参数
     * @param params      页面参数
     * @param isChart     是否为图形
     * @return 统计数据
     */
    Map<String, Object> getStatisticsTableData(MethodParam methodParam, Map<String, Object> params, boolean isChart);

    /**
     * 查询统计分析配置列表
     *
     * @param methodParam 入參
     * @param param       入參
     * @return
     */
    Map<String, Object> getStatisticConfigList(MethodParam methodParam, SearchParam param);

    /**
     * 添加、编辑统计分析配置
     *
     * @param methodParam 入參
     * @param paramMap    入參
     */
    void newStatistic(MethodParam methodParam, Map<String, Object> paramMap, boolean isAdd);

    /**
     * 获取统计配置明细信息
     *
     * @param methodParam 入參
     * @param id          入參主键
     * @return
     */
    Map<String, Object> getStatisticInfoById(MethodParam methodParam, String id);

    /**
     * 删除选中统计配置
     *
     * @param methodParam 入參
     */
    void cutStatisticByIds(MethodParam methodParam);

    /**
     * 显示为看板
     *
     * @param methodParam 入參
     * @param paramMap    入參
     */
    void modifyStatisticShow(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 查询统计分析列表
     *
     * @param methodParam 入參
     * @param param       入參
     * @return
     */
    List<Map<String, Object>> getStatisticList(MethodParam methodParam, SearchParam param);

    /**
     * 删除选中看板报表
     *
     * @param methodParam 入參
     * @param id          入參
     */
    void cutUserStatisticDash(MethodParam methodParam, String id);

    /**
     * 添加用户看板报表
     *
     * @param methodParam 入參
     * @param id          入參
     */
    void newUserStatisticDash(MethodParam methodParam, String id);

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getStatisticConfigListPermission(MethodParam methodParam);

    /**
     * 显示为看板,仅有一个能显示
     *
     * @param methodParam 入參
     * @param paramMap    入參
     */
    void modifyStatisticShowForV2(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 显示为看板,仅有一个能显示
     *
     * @param methodParam 入參
     * @param paramMap    入參
     */
    void modifyStatisticShowForV3(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 生成统计分析报表EXCEL,并返回token
     *
     * @param methodParam
     * @param params
     * @return
     */
    String getStatisticsTableDataExport(MethodParam methodParam, Map<String, Object> params);

    /**
     * 获取统计配置明细信息-不报告数据库操作语句
     *
     * @param methodParam 入參
     * @param id          入參主键
     * @return
     */
    Map<String, Object> getStatisticInfoNoQueryById(MethodParam methodParam, String id);
}

