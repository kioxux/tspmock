package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetUseService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 上午 9:29 2019/11/30 0030
 */
@Service
public class AssetUseServiceImpl implements AssetUseService {
//
//    @Autowired
//    AssetUseMapper assetUseMapper;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Override
//    public String findUseList(HttpServletRequest request) {
//
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String facilities = request.getParameter("facilities");
//        String keyWord = request.getParameter("searchKey");
//        String categoryId = request.getParameter("categoryId");
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        //根据设备组织查询条件，查询用户可见的库房，用于查询备件
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (asset.strname like '%" + keyWord + "%' or upper(asset.strcode) like upper('%" + keyWord +
//                    "%') )";
//        }
//        if (RegexUtil.isNotNull(categoryId) && !"-1".equals(categoryId)) {
//            condition += "and asset.category_id='" + categoryId + "'";
//        }
//        User user=AuthService.getLoginUser(request);
//        //全部位置 所在位置权限判断来源于 设备的权限
//        String rootPermission="asset_data_management";
//        boolean isAll = userService.isAllUserFunctionPermission(schema_name, user.getId(), rootPermission);
//        //若没有所有位置的权限且没有筛选的组织根据用户的权限 筛选所能看见得设备
//        if(!isAll&& RegexUtil.isNull(facilities)){
//            condition+="AND f.id in(select f.id from "+schema_name+"._sc_facilities f " +
//                    "LEFT JOIN "+schema_name+"._sc_group_org gr on gr.org_id=f.id " +
//                    "LEFT JOIN "+schema_name+"._sc_user_group up on up.group_id=gr.group_id " +
//                    "WHERE up.user_id='"+user.getId()+"')";
//        }
//        condition += splitJointCondition(facilities);
//        int begin = pageSize * (pageNumber - 1);
//        dataList = assetUseMapper.findUseList(schema_name, condition, pageSize, begin);
//        int total = assetUseMapper.countUseList(schema_name, condition);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//
//    private String splitJointCondition(String facilities) {
//        String condition = "";
//        if (null != facilities && !facilities.isEmpty()) {
//            String[] fs = facilities.split(",");
//            StringBuffer fsb = new StringBuffer();
//            for (String value : fs) {
//                fsb.append(" or f.facility_no like '" + value + "%' ");
//            }
//            String nfs = fsb.toString();
//            if (!nfs.isEmpty() && !"".equals(nfs)) {
//                nfs = nfs.substring(3);
//                condition += " and ( " + nfs + ") ";
//            }
//        }
//        return condition;
//    }

}
