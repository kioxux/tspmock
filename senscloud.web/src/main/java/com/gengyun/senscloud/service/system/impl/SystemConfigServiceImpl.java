package com.gengyun.senscloud.service.system.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.mapper.SystemConfigMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SystemConfigData;
import com.gengyun.senscloud.model.SystemConfigModel;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 系统配置信息处理
 */
@CacheConfig(cacheNames = "config")
@Service
public class SystemConfigServiceImpl implements SystemConfigService {
    @Resource
    SystemConfigMapper systemConfigMapper;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;


    /**
     * 获取系统默认语言
     *
     * @param methodParam 系统参数
     * @return 多语言转换值
     */
    @Override
    public String getSysLang(MethodParam methodParam) {
        return this.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.DEFAULT_LANGUAGE);
    }

    /**
     * 根据系统配置名获取配置值
     *
     * @param schema_name 数据库
     * @param config_name 系统配置名
     * @return 配置值
     */
    @Override
    public String getSysCfgValueByCfgName(String schema_name, String config_name) {
        return systemConfigMapper.findSysCfgValueByCfgName(schema_name, config_name);
    }

    /**
     * 查询系统配置基础配置列表
     *
     * @param methodParam       入参
     * @param systemConfigModel 入参
     * @return 系统配置基础配置列表
     */
    @Override
    public List<Map<String, Object>> getSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        String group_title = RegexUtil.optStrOrExpNotNull(systemConfigModel.getGroup_title(), LangConstant.TITLE_ABAAA_W);//标题不能为空
        return systemConfigMapper.findSysConfigList(methodParam.getSchemaName(), group_title);
    }

    /**
     * 查询地图类型系统配置基础配置
     *
     * @param methodParam       入参
     * @param systemConfigModel 入参
     * @return 地图类型系统配置基础配
     */
    @Override
    public Map<String, Object> getMapTypeSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        return systemConfigMapper.findMapTypeSysConfig(methodParam.getSchemaName());
    }

    /**
     * 查询顶层地图类型系统配置基础配置
     *
     * @param methodParam       入参
     * @param systemConfigModel 入参
     * @return 地图类型系统配置基础配
     */
    @Override
    public Map<String, Object> getTopMapTypeSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        return systemConfigMapper.findTopMapTypeSysConfig(methodParam.getSchemaName());
    }

    /**
     * 更新系统配置
     *
     * @param methodParam 入参
     * @param params      入参
     * @return 系统配置基础配置列表
     */
    @Override
    public void modifySysConfigList(MethodParam methodParam, Map<String, Object> params) {
        JSONArray jsonArray = JSONArray.fromObject(params.get("sysConfigs"));
        for (Object object : jsonArray
        ) {
            Map<String, Object> map = (Map<String, Object>) JSONObject.fromObject(object);
            Map<String, Object> oldMap = systemConfigMapper.findMapSysConfigInfo(methodParam.getSchemaName(), map.get("config_name").toString());
            if (RegexUtil.optNotNull(oldMap).isPresent()) {
                if (RegexUtil.optNotNull(map.get("source_data")).isPresent()) {
                    String stringData = map.get("source_data").toString();
                    map.put("source_data", JSON.toJSONString(stringData));
                }
                systemConfigMapper.updateSysConfig(methodParam.getSchemaName(), map);
                JSONArray log = LangUtil.compareMap(map, oldMap);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_8001, map.get("config_name").toString(), LangUtil.doSetLogArray("编辑了厂商", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_BL, log.toString()}));
            }
        }
    }

    @Override
    public SystemConfigData getSystemConfigData(String schema_name, String config_name) {
        return systemConfigMapper.findSysConfigInfo(schema_name, config_name);
    }

    @Override
    public Map<String, Object> getMapSystemConfigData(String schema_name, String config_name) {
        return systemConfigMapper.findMapSysConfigInfo(schema_name, config_name);
    }

    /**
     * 查询系统配置组名称列表
     *
     * @param schema_name 数据库
     * @return 组名称列表
     */
    @Override
    public String[] getGroupTitleList(String schema_name) {
        return systemConfigMapper.findGroupTitleList(schema_name);
    }
}
