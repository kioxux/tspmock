package com.gengyun.senscloud.service.asset;

/**
 * @Author: Wudang Dong
 * @Description:设备调拨接口
 * @Date: Create in 上午 11:13 2019/4/30 0030
 */
public interface AssetTransferService {
//    //    AssetTransferModel selectDetailById(String schema_name, int id);
//    List<Map<String, Object>> selectAllDiscard(String schena_name, String condition, User loginUser);
//
//    int selectAllDiscardNum(String schena_name, String condition);
////    boolean deal(String schema_name , String remark ,int id ,String mark,String account,String asset_id,String taskId,String intsiteid, String asset_name);
//
//    /**
//     * 设备调拨-调出审核
//     *
//     * @param schema_name
//     * @param request
//     * @param paramMap
//     * @param user
//     * @param auditNode   审核节点
//     * @return
//     */
//    ResponseModel transferAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, AssetTransferAuditNode auditNode) throws Exception;
//
//    ResponseModel cancel(String schema_name, User user, String transfer_code);
//
//    /**
//     * 申请设备调拨
//     *
//     * @param schema_name
//     * @param request
//     * @param user
//     * @param processDefinitionId
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    ResponseModel add(String schema_name, HttpServletRequest request, User user, String processDefinitionId, Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 生成设备调拨以及调拨流程
//     *
//     * @param schema_name
//     * @param do_flow_key
//     * @param user
//     * @param processDefinitionId
//     * @param map_object
//     * @param asset_transfer_tmp
//     * @param asset_transfer_detail_tmp
//     * @param body_property
//     * @param source                    来源：盘点子表编码（盘点调整发起的调拨申请）
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    ResponseModel addTransfer(String schema_name, String do_flow_key, User user, String processDefinitionId, Map<String, Object> map_object, Map<String, Object> asset_transfer_tmp, Map<String, Object> asset_transfer_detail_tmp, String body_property, String inv_position_id, String source, boolean isNew, boolean hasAudit) throws Exception;
//
//    /**
//     * 根据code查询设备调拨数据
//     *
//     * @param schemaName
//     * @param transferCode
//     * @return
//     */
//    Map<String, Object> queryAssetTransferByCode(String schemaName, String transferCode);
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
}
