package com.gengyun.senscloud.service;

public interface AnalysReportService {
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数
//    List<AnalysFacilityRepairMaintainResult> GetFacilityRepairMaintainReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin);
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数的和合计数量
//    int GetFacilityRepairMaintainReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition);
//
//    //按故障类型，进行位置和供应商的设备维护统计
//    List<AnalysFacilityRepairMaintainResult> GetFacilityRepairTypeReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin);
//
//    //按故障类型，进行位置和供应商的设备维护统计，统计总数
//    int GetFacilityRepairTypeReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition);
//
//
//    //按设备，统计设备维护、保养时效和总数，故障类型的总数和时间
//    List<AnalysFacilityRepairMaintainResult> GetDeviceRepairMaintainReport(String schema_name, String deviceCondition, String dateCondition, String facilityCondition, String categoryCondition, int pageSize, int begin);
//
//    //按设备，统计设备维护、保养时效和总数，故障类型的总数和时间，统计结果的总数
//    int GetDeviceRepairMaintainReportCount(String schema_name, String deviceCondition, String dateCondition, String facilityCondition, String categoryCondition);
//
//    //按个人，统计设备维护、保养时效和总数和时间
//    List<AnalysFacilityRepairMaintainResult> GetUserRepairMaintainReport(String schema_name, String condition, String groupCondition, String dateCondition, String categoryCondition, int pageSize, int begin);
//
//    //按个人，统计设备维护、保养时效和总数和时间的总数
//    int GetUserRepairMaintainReportCount(String schema_name, String condition, String groupCondition, String dateCondition, String categoryCondition);
//
//    //统计位置的故障率、保养效率、故障间隔时间和故障处理能力
//    List<AnalysFacilityResult> getFacilityReport(String schema_name, String repairCondition, String maintainCondition, String cidCondition, String categoryCondition, Integer pageSize, Integer begin);
//
//    //统计位置的故障率、保养效率、故障间隔时间合计数量
//    int getFacilityReportCount(String schema_name, String repairCondition, String maintainCondition, String cidCondition, String categoryCondition);
//
//    //统计供应商的设备故障率，故障明细
//    List<AnalysSupplierResult> GetSupplierReport(String schema_name, String repairCondition, String sidCondition, String categoryCondition, Integer pageSize, Integer begin);
//
//    //统计供应商的设备故障率，故障明细合计数量
//    int GetSupplierReportCount(String schema_name, String repairCondition, String sidCondition, String categoryCondition);
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数
//    List<AnalysFacilityRepairMaintainResult> GetSupplierRepairMaintainReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin);
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数的和合计数量
//    int GetSupplierRepairMaintainReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition);
//
//    //按故障类型，进行位置和供应商的设备维护统计
//    List<AnalysFacilityRepairMaintainResult> GetSupplierRepairTypeReport(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition, int pageSize, int begin);
//
//    //按故障类型，进行位置和供应商的设备维护统计，统计总数
//    int GetSupplierRepairTypeReportCount(String schema_name, String dateCondition, String facilityTypeCondition, String facilityCondition, String categoryCondition);
//
//    //获取维修的时效数据统计
//    List<AnalysRepairWorkResult> GetRepairHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin);
//
//    //获取维修的时效数据统计
//    int GetRepairHoursReportCount(String schema_name, String condition, String groupColumn);
//
//    //获取保养的时效数据统计
//    List<AnalysRepairWorkResult> GetMaintainHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin);
//
//    //获取保养的时效数据统计
//    int GetMaintainHoursCount(String schema_name, String condition, String groupColumn);
//
//    //获取巡检的时效数据统计
//    List<AnalysRepairWorkResult> GetInspectionHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin);
//
//    //获取巡检的时效数据统计
//    int GetInspectionHoursCount(String schema_name, String condition, String groupColumn);
//
//    //获取点检的时效数据统计
//    List<AnalysRepairWorkResult> GetSpotHoursReport(String schema_name, String condition, String analysColumn, String groupColumn, int pageSize, int begin);
//
//    //获取点检的时效数据统计
//    int GetSpotHoursCount(String schema_name, String condition, String groupColumn);
//
//    //分析，获取维修的时效数据的总平均值，根据条件
//    AnalysRepairWorkResult GetRepairHoursByFacilityForChart(String schema_name, String condition);
//
//    //分析，获取维修的百台设备维修次数，按次数，取前20个，根据条件
//    List<AnalysRepairWorkResult> GetRepairTimesByPercentForChart(String schema_name, String condition);
//
//    //分析，单个设备的平均维修时效
//    AnalysRepairWorkResult GetRepairHourAndTimesByAssetId(String schema_name, String asset_id);
//
//    //分析，单个设备的平均故障间隔
//    AnalysRepairWorkResult GetRepairHourIntervalByAssetId(String schema_name, String asset_id);
//
//    //获取设备监控数据
//    List<AssetMonitorData> findAllAssetMonitorList(String schema_name, String condition, int pageSize, int begin);
//
//    //设备编号查询监控项内容信息
//    List<AssetMonitorData> findAllAssetMonitorListByCode(String schema_name, String assetCode);
//
//    //获取设备监控数据总数
//    int findAllAssetMonitorListCount(String schema_name, String condition);
//
//    //设备编号查询监控项内容总数量
//    int findAllAssetMonitorListCountByCode(String schema_name, String assetCode);
//
//    //获取备件统计
//    List<BomData> getBom(String schema_name, String condition, int pageSize, int begin);
//
//    int getBomCount(String schema_name, String condition);
//
//    //查询备件明细信息
//    List<AssetMonitorData> analysAssetMonitorListById(String schema_name, int id);
//
//    AssetMonitorData analysAssetMonitorDataById(String schema_name, int id);
//
//    List<AssetMonitorData> searchHistoryAssetMonitorListByAssetCode(String schema_name, String assetCode, String monitorName, int pageSize, int begin);
//
//    int searchHistoryAssetMonitorListByAssetCodeCount(String schema_name, String assetCode, String monitorName);
//
//    int failurTimes(String schema_name, String facilityCondition, String repairCondition, String maintainCondition);
//
//    int allTime(String schema_name, String facilityCondition, String repairCondition, String maintainCondition);
//
//    long MTBF(String schema_name, String facilityCondition, String repairCondition, String maintainCondition);
//
//    long meanTimeToRestore(String schema_name, String facilityCondition, String repairCondition, String maintainCondition);
//
//    double MTTR(String schema_name, String facilityCondition, String repairCondition, String maintainCondition);
//
//    //获取用户的任务完成数量/任务完成率
//    List<AnalysRepariMaintainTaskCountResult> getUserTaskCount(String schema_name, String condition);
//
//    List<Map<String, Object>> findAllAssetMonitorListByCodeFromEs(String schema_name, String assetCode, HttpSession session);

}
