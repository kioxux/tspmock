package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.model.WorkListModel;
import com.gengyun.senscloud.model.WorkSheetDistribution;

import java.util.List;
import java.util.Map;

public interface WorksheetDispatchService {

    /**
     * 查询已分配工单列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String, Object>> getWorksheetDistributionList(MethodParam methodParam, SearchParam param);

    /**
     * 查询待分配工单信息列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getWorksheetUndistributedList(MethodParam methodParam, SearchParam param);

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getWorksheetDispatchPermission(MethodParam methodParam);

    /**
     * 根据子工单编号查询工单信息列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<WorkListModel> getWorkListDetailBySwc(MethodParam methodParam, SearchParam param);
}
