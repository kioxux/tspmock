package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.FileModel;
import com.gengyun.senscloud.model.FilesData;
import com.gengyun.senscloud.model.MethodParam;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface FilesService {
    //添加附件
    int add(String schema_name, FilesData filesData);

//    //获取附件
//    List<FilesData> FindAllFiles(String schema_name, String file_ids);

    //获取附件,通过名称
    List<FilesData> FindAllFilesByName(String schema_name, String file_name);

    void updateAssetfileValue(String schema_name, String field_name, Object value, String _id);


    /**
     * 根据文件码下载文件
     *
     * @param fileModel 文件码
     */
    void doDownLoad(MethodParam methodParam, FileModel fileModel);


    /**
     * 获取图片
     *
     * @param methodParam 入参
     * @param fileModel   入参那
     */
    void getImg(MethodParam methodParam, FileModel fileModel);

    /**
     * 上传文件处理
     *
     * @param methodParam 入参
     * @param fileModel   文件
     */
    String checkAddFile(MethodParam methodParam, FileModel fileModel);

    /**
     * 根据附件id获取附件详细信息
     *
     * @param schemaName 入参
     * @param fileId     入参
     * @return 附件详细信息
     */
    Map<String, Object> findById(String schemaName, Integer fileId);

    /**
     * 根据附件id获取名称列表
     *
     * @param schema_name 请求参数
     * @param file_ids    请求参数
     * @return 列表数据
     */
    List<String> getFileNamesByIds(String schema_name, String file_ids);

    /**
     * 更新附近的文件类别
     *
     * @param schema_name  请求参数
     * @param file_id      请求参数
     * @param file_type_id 请求参数
     */
    void updateFileType(String schema_name, Integer file_id, Integer file_type_id);
//    /**
//     * 更新资产附件
//     *
//     * @param methodParam 入参
//     * @param fileModel   入参
//     * @return 是否成功
//     */
//    Map<String, Object> assetFileUpdate(MethodParam methodParam, FileModel fileModel);

    /**
     * 根据域名获取相关图片
     *
     * @param methodParam 系统参数
     * @param fileModel   请求参数
     */
    void getLoginImage(MethodParam methodParam, FileModel fileModel);

    /**
     *
     * @param schema_name
     * @param file_id
     * @param file_category_id
     * @param remark
     */
    void updateFileCategory(String schema_name, Integer file_id, Integer file_category_id, String remark, Integer file_type_id);
}
