package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MaintainService;
import org.springframework.stereotype.Service;

@Service
public class MaintainServiceImpl implements MaintainService {
//    @Autowired
//    MaintainMapper maintainMapper;
//
//    @Autowired
//    LogsMapper loggerMapper;
//
//    @Autowired
//    LogsService logService;
//
//    //查询设备最近一条保养信息，如果没有，则读取最近的一条空的，先注释掉，直接按编号读取
//    //增加传入保养编号，如果编号有，则读取该编号的保养
//    @Override
//    public MaintainData getMaintainInfoAndItemByAssetTypeId(String schema_name, String account, String assetId, String maintainCode) {
//        MaintainData result = null;
//        if (maintainCode != null && !maintainCode.equals("null") && !maintainCode.isEmpty()) {
//            result = maintainMapper.getMaintainInfo(schema_name, maintainCode);
//        }
//
//        //读取该设备最近的一条保养记录，做为自主保存，不过先注释掉，因为韵达不需要这种情况
////        if (result == null || result.getMaintainCode().isEmpty()) {
////            //证明没有需要当前保养的，则读取最近保养的一条，获取最近保养的时间,用于提醒
////            List<MaintainData> latestData = maintainMapper.getLatestMaintainList(schema_name, assetId);
////            result = new MaintainData();
////            if (latestData != null && !latestData.isEmpty()) {
////                result.setFinishedTime(latestData.get(0).getFinishedTime());
////                result.setCycleCount(latestData.get(0).getCycleCount());
////            } else {
////                result.setCycleCount(60);
////            }
////            result.setMaintainItemList(getMaintainItemByAssetTypeId(schema_name, deviceType));
////        }
//
//        if (result != null && result.getMaintainCode() != null && !result.getMaintainCode().isEmpty()) {
//            if (result.getMaintainResult() == null || result.getMaintainResult().isEmpty()) {
//                result.setMaintainResult("");
//            }
//            List<LogsData> dataList = logService.findAllLogsList(schema_name, "maintain", result.getMaintainCode());
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
//            //加载历史
//            result.setHistoryList(dataList);
//            //加载备件列表
//            result.setBomList(maintainMapper.getMaintainBomList(schema_name, result.getMaintainCode()));
//            result.setMaintainItemList(getMaintainItemByAssetTypeId(schema_name, result.getAssetType()));
//        }
//        return result;
//    }
//
//
//    //保存保养信息
//    @Override
//    public int saveMaintain(String schema_name, MaintainData maintainData) {
//        return maintainMapper.saveMaintain(schema_name, maintainData);
//    }
//
//    //新增支援人员，初始工时为0，支援人员中包含_sc_maintain表中的接受人员
//    @Override
//    public int insertMaintainWorkHour(String schema_name, MaintainWorkHourData maintainWorkHourData) {
//        return maintainMapper.insertMaintainWorkHour(schema_name, maintainWorkHourData);
//    }
//
//    //编辑保养信息,点击保存按钮，或者草稿状态，重新保存或提交
//    @Override
//    public int EditMaintain(String schema_name, MaintainData maintainData) {
//        return maintainMapper.EditMaintain(schema_name, maintainData);
//    }
//
//    //更新分配人
//    @Override
//    public int EditMaintainMan(String schema_name, MaintainData maintainData) {
//        return maintainMapper.EditMaintainMan(schema_name, maintainData);
//    }
//
//    //删除援人员，在重新保存和提交保养单时，先全部删除工时表中的人，再重新新增支援人员进来
//    @Override
//    public int deleteMaintainWorkHour(String schema_name, String maintain_code) {
//        return maintainMapper.deleteMaintainWorkHour(schema_name, maintain_code);
//    }
//
//    //更新编辑开始保养时间，为手机上点击设备的保养开始
//    @Override
//    public int EditMaintainBeginTime(String schema_name, MaintainData maintainData) {
//        return maintainMapper.EditMaintainBeginTime(schema_name, maintainData);
//    }
//
//    //设置维修开始计时,缺表，待建 2018-03-29
//
//
//    //保存和提交保养结果
//    @Override
//    public int SaveMaintainResult(String schema_name, MaintainData maintainData) {
//        return maintainMapper.SaveMaintainResult(schema_name, maintainData);
//    }
//
//    //新增保养领用的备件
//    @Override
//    public int insertMaintainBom(String schema_name, MaintainBomData maintainBomData) {
//        return maintainMapper.insertMaintainBom(schema_name, maintainBomData);
//    }
//
//    //删除保养领用的备件,用于保养结果重新保存或提交时
//    @Override
//    public int deleteMaintainBom(String schema_name, String maintain_code) {
//        return maintainMapper.deleteMaintainBom(schema_name, maintain_code);
//    }
//
//    //保养结果确认
//    @Override
//    public int AuditMaintainResult(String schema_name, MaintainData maintainData) {
//        return maintainMapper.AuditMaintainResult(schema_name, maintainData);
//    }
//
//    //查找保养单列表(按，条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    @Override
//    public List<MaintainData> getMaintainList(String schema_name, String condition, int pageSize, int begin) {
//        return maintainMapper.getMaintainList(schema_name, condition, pageSize, begin);
//    }
//
//    //查找保养单列表总数(按，条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    @Override
//    public int getMaintainListCount(String schema_name, String condition) {
//        return maintainMapper.getMaintainListCount(schema_name, condition);
//    }
//
//    //查找单条保养单据
//    @Override
//    public MaintainData getMaintainInfo(String schema_name, String maintainCode) {
//        MaintainData data = maintainMapper.getMaintainInfo(schema_name, maintainCode);
//        if (data != null && data.getMaintainCode() != null && !data.getMaintainCode().isEmpty()) {
//            List<LogsData> dataList = logService.findAllLogsList(schema_name, "maintain", data.getMaintainCode());
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
//            data.setHistoryList(dataList);
//            //加载备件列表
//            data.setBomList(maintainMapper.getMaintainBomList(schema_name, maintainCode));
//        }
//        return data;
//    }
//
//    @Override
//    public MaintainData getLastMaintainInfoByAssetId(String schema_name, String assetId) {
//        return maintainMapper.getLastOneMaintain(schema_name, assetId);
//    }
//
//    //按位置，查找所有的设备，以及其最新的一条保养数据（不管是否执行完成，只要生成了）, 供系统自动生成保养单用
//    @Override
//    public List<MaintainData> findAllAssetAndLastestMaintain(String schema_name, int facility_id) {
//        return maintainMapper.findAllAssetAndLastestMaintain(schema_name, facility_id);
//    }
//
//    //过期的保养单，进行关闭操作
//    @Override
//    public int CloseMaintainBillForDue(String schema_name) {
//        return maintainMapper.CloseMaintainBillForDue(schema_name);
//    }
//
//    //获取备件列表，用于保养的备件
//    @Override
//    public List<MaintainBomData> getMaintainBomList(String schema_name, String maintainCode) {
//        return maintainMapper.getMaintainBomList(schema_name, maintainCode);
//    }
//
//    //获取保养工时列表
//    @Override
//    public List<MaintainWorkHourData> getMaintainWorkHourList(String schema_name, String maintainCode) {
//        return maintainMapper.getMaintainWorkHourList(schema_name, maintainCode);
//    }
//
//    //获取设备的保养项，按设备的类型
//    @Override
//    public List<MaintainItemData> getMaintainItemByAssetTypeId(String schema_name, String asset_type_id) {
//        return maintainMapper.getMaintainItemByAssetId(schema_name, asset_type_id);
//    }
//
//    //获取设备的保养确认项，按设备的类型
//    @Override
//    public List<MaintainCheckItemData> getMaintainCheckItem(String schema_name, String asset_type_id) {
//        return maintainMapper.getMaintainCheckItem(schema_name, asset_type_id);
//    }
//
//    //获取所有的设备保养项
//    @Override
//    public List<MaintainItemData> getAllMaintainItemList(String schema_name) {
//        return maintainMapper.getAllMaintainItemList(schema_name);
//    }
//
//
//    //查找保养单，查到需保养的设备
//    @Override
//    public Asset getAssetCommonDataByMaintainCode(String schema_name, String maintainCode) {
//        return maintainMapper.getAssetCommonDataByMaintainCode(schema_name, maintainCode);
//    }
//
//    @Override
//    public List<MaintainData> getMaintainListByCode(String schema_name, String device_code) {
//        return maintainMapper.getMaintainListByCode(schema_name, device_code);
//    }
//
//    //备件统计保养使用数量明细查询by bom_model and stock_code
//    @Override
//    public List<MaintainBomData> getMaintainDetailByBomModelAndStockCode(String schema_name, String bom_model, String stock_code) {
//        return maintainMapper.getMaintainDetailByBomModelAndStockCode(schema_name, bom_model, stock_code);
//    }
//
//    @Override
//    public int invalidMaintain(String schema_name, String maintainCode) {
//        return maintainMapper.invalidRepair(schema_name, maintainCode);
//    }
//
//    //更新保养的截止日期，以重新定义下一次生成保养的日期
//    @Override
//    public int updateMaintainDeadlineTime(String schema_name, String maintainCode, Timestamp deadlineTime) {
//        return maintainMapper.updateMaintainDeadlineTime(schema_name, maintainCode, deadlineTime);
//    }
//
//    //根据物料编号查询记录
//    @Override
//    public int deleteMaintianByMaterial_code(String schema_name, String bill_code) {
//        return maintainMapper.deleteMaintianByMaterial_code(schema_name, bill_code);
//    }
//
//    @Override
//    public int saveMaintainBom(String schema_name, MaintainBomData maintainBomData) {
//        return maintainMapper.saveMaintainBom(schema_name, maintainBomData);
//    }
//
//
//    //嘉研备件申领修改状态
//    @Override
//    public int updateMaintainApplyStaus(String schema_name, String code) {
//        return maintainMapper.updateMaintainApplyStaus(schema_name, code);
//    }
//
//    @Override
//    public int updateOpenMaintainApplyStaus(String schema_name, String code, int status) {
//        return maintainMapper.updateOpenMaintainApplyStaus(schema_name, code, status);
//    }
//
//    //查询物料编号是否存在保养备件信息
//    @Override
//    public List<MaintainBomData> selectMaintainByMaterialCode(String schema_name, String code) {
//        return maintainMapper.selectMaintainByMaterialCode(schema_name, code);
//    }
//
//    //更新物料编号保养备件数量信息
//    @Override
//    public int udpateMaintainBomBymaterial(String schema_name, String bill_code) {
//        return maintainMapper.udpateMaintainBomBymaterial(schema_name, bill_code);
//    }
//
//    @Override
//    public FullScreenData getPlannedAndCompleteMaintain(String schema_name) {
//        return maintainMapper.getPlannedAndCompleteMaintain(schema_name);
//    }

}
