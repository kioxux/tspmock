package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.DashboardConfigService;
import org.springframework.stereotype.Service;

@Service
public class DashboardConfigServiceImpl implements DashboardConfigService {
//    @Autowired
//    DashboardConfigMapper mapper;
//
//    @Autowired
//    MetaDataAssetMapper metaDataAssetMapper;
//
//    @Autowired
//    AnalysDataMapper analysMapper;
//
//    @Autowired
//    FacilitiesMapper facilitiesMapper;
//
//    /*
//    * 查询所有的看板配置
//    */
//    @Override
//    public List<DashboardConfigData> findAllDashboardConfigList(String schema_name) {
//        return mapper.findAllDashboardConfigList(schema_name);
//    }
//
//    /*
//     * 新增看板配置信息
//     */
//    @Override
//    public int AddDashboardConfig(String schema_name, String title, String snippet, String showtype, String date_type, String description, String datasource, String boardtype) {
//        return mapper.AddDashboardConfig(schema_name, title, snippet, description, showtype, date_type, datasource, boardtype);
//    }
//
//    /*
//     * 修改看板配置信息
//     */
//    @Override
//    public int EditDashboardConfig(String schema_name, Integer id, String title, String snippet, String showtype, String date_type, String description, String boardtype) {
//        return mapper.EditDashboardConfig(schema_name, id, title, snippet, showtype, date_type, description, boardtype);
//    }
//
//    /*
//     * 修改看板配置条件
//     */
//    @Override
//    public int EditDashboardCondition(String schema_name, Integer id, String datasource) {
//        return mapper.EditDashboardDatasource(schema_name, id, datasource);
//    }
//
//    /*
//     * 删除看板配置
//     */
//    @Override
//    public int DeleteDashboardConfigList(String schema_name, Integer id) {
//        return mapper.DeleteDashboardConfig(schema_name, id);
//    }
//
//    /*
//    * 查询看板配置
//    */
//    @Override
//    public DashboardConfigData findDashboardConfigData(String schema_name, Integer id) {
//        return mapper.findDashboardConfigData(schema_name, id);
//    }
//
//    /*
//     * 更新看板显示状态：是否显示
//     */
//    @Override
//    public int UpdateDashboardIsShow(String schema_name, Integer id, boolean isshow) {
//        return mapper.UpdateDashboardIsShow(schema_name, id, isshow);
//    }
//
//    /*
//    * 查询看板指标，通过看板配置的条件，条件可以为空，这时统计所有的
//    * device_tota：设备总数
//    * repair_total:维修总数
//    * need_repair_total:待维修总数
//    * maintain_total:保养总数
//    * need_maintain_total:待保养总数
//    * inspection_total: 巡检总数
//    * spot_total:点检总数
//    * mttr:MTTR（平均维修时间）
//    * repair_hour:平均维修到场时效
//    * mtbf:MTBF（平均故障间隔时间）
//    * maintain_mttr:平均保养时效
//    * system:系统可用时间
//    * pmc:PMC（预防性维护合规性）
//    * smcp:SMCP（计划维护关键百分比）
//    * pmp:PMP（计划性维护百分比）
//    */
//    @Override
//    public Double getDashboardConfigKPI(String schema_name, String showType, String datasource, String dateType, String facilityId, String groupId, String account) {
//        StringBuffer sb = new StringBuffer();
//        Pattern pattern = Pattern.compile(SensConstant.DASHBOARD_REGEX);
//        Matcher matcher = pattern.matcher(showType);
//        while (matcher.find()) {
//            matcher.appendReplacement(sb, String.valueOf(getDashboardConfigKPI(schema_name, matcher.group(1), datasource, dateType, facilityId, groupId, account)));
//        }
//        matcher.appendTail(sb);
//        Evaluator evaluator = new Evaluator();
//        try {
//            return Double.parseDouble(evaluator.evaluate(sb.toString()));
//        } catch (EvaluationException e) {
//        }
//        Double result = 0.0;
//        String condition = " ";   //统计条件
//        String facilityIds;      //位置的id合并，“，”隔开
//        String accounts = " ";
//        String assetType = " ";   //统计条件
//        String leftCondition = " ";
//        Timestamp beginTime = Timestamp.valueOf("2000-01-01 00:00:00");
//        Timestamp endTime = Timestamp.valueOf("2050-01-01 00:00:00");
//
//        //看板统计时间
//        if (dateType != null && !dateType.equals("null") && !dateType.isEmpty()) {
//            Calendar now = Calendar.getInstance();
//            int year = now.get(Calendar.YEAR);
//            int month = now.get(Calendar.MONTH) + 1;
//            switch (dateType) {
//                case "year":
//                    beginTime = Timestamp.valueOf(year + "-01-01 00:00:00");
//                    endTime = Timestamp.valueOf((year + 1) + "-01-01 00:00:00");
//                    break;
//                case "season":
//                    Date today = new Date();
//                    int season = getSeason(today);
//                    beginTime = Timestamp.valueOf(year + "-" + ((season - 1) * 3 + 1) + "-01 00:00:00");
//                    String tempEndTime = season == 4 ? (year + 1) + "-01-01 00:00:00" : (year) + "-" + ((season - 1) * 3 + 4) + "-01 00:00:00";
//                    endTime = Timestamp.valueOf(tempEndTime);
//                    break;
//                case "month":
//                    beginTime = Timestamp.valueOf(year + "-" + month + "-01 00:00:00");
//                    if (month != 12) {
//                        endTime = Timestamp.valueOf((year) + "-" + (month + 1) + "-01 00:00:00");
//                    } else {
//                        endTime = Timestamp.valueOf((year + 1) + "-01-01 00:00:00");
//                    }
//                    break;
//            }
//        }
//
//        //查询其所有的子位置
//        if (facilityId != null && !facilityId.isEmpty() && !facilityId.equals("") && !facilityId.equals("null") && !facilityId.equals("0")) {
//            List<Facility> allFacility = facilitiesMapper.FacilitiesList(schema_name);
//            String fids = GetAllfacilityIds(allFacility, Integer.parseInt(facilityId));
//            facilityIds = " and f.id in (" + fids.substring(1, fids.length()) + ") ";
//        } else {
//            facilityIds = " ";
//        }
//
//        if (account != null && !account.isEmpty() && !account.equals("") && !account.equals("null") && !account.equals("0")) {
//            accounts = " and wd.receive_account ='" + account + "'";
//        } else {
//            accounts = " ";
//        }
//
//        if (groupId != null && !groupId.isEmpty() && !groupId.equals("") && !groupId.equals("null") && !groupId.equals("0")) {
//            groupId = " and ug.group_id in (" + groupId + ") ";
//            leftCondition = "left join ${schema_name}._sc_user as u on u.account = wd.receive_account " +
//                    "left join ${schema_name}._sc_user_group as ug on ug.user_id = u.id ";
//        } else {
//            groupId = " ";
//        }
//
//
//        // 看板条件：是和类型的设备
//        if (datasource != null && !datasource.isEmpty() && !datasource.equals("{}") && !datasource.equals("[]") && !datasource.equals("[{}]")) {
//            JSONArray fieldArray = new JSONArray(datasource);
//            if (fieldArray != null && fieldArray.length() > 0) {
//                assetType += " and ( 1!=1 ";
//                for (int i = 0; i < fieldArray.length(); i++) {
//                    JSONObject field = fieldArray.getJSONObject(i);
//                    assetType += " or dv.asset_type='" + field.optString("show_asset", "") + "'";
//
//                }
//                assetType += " ) ";
//            }
//        }
//
//        try {
//            condition += facilityIds + assetType;
//            switch (showType) {
//                case "device_total":
//                    //获取资产总数，可按设备位置、设备类型进行统计
//                    result += analysMapper.getDashboardAssetTotal(schema_name, condition);
//                    break;
//                case "repair_total":
//                    //获取维修单总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardRepairTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    break;
//                case "need_repair_total":
//                    //获取待维修单总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardRepairTotalByStatus(schema_name, condition, beginTime, endTime, "20,30,40,50", leftCondition);
//                    break;
//                case "maintain_total":
//                    //获取保养单总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardMaintainTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    break;
//                case "need_maintain_total":
//                    //获取待保养单总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardMaintainTotalByStatus(schema_name, condition, beginTime, endTime, "20,30,40,50", leftCondition);
//                    break;
//                case "finished_maintain":
//                    //获取保养完成总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardFinishedTotal(schema_name, condition, beginTime, endTime, leftCondition, 2);
//                    break;
//                case "inspection_total":
//                    //获取巡检单总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardInspectionTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    break;
//                case "finished_inspection":
//                    //获取巡检完成总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardFinishedTotal(schema_name, condition, beginTime, endTime, leftCondition, 3);
//                    break;
//                case "spot_total":
//                    //获取点检单总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardSpotTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    break;
//                case "finished_spot":
//                    //获取点检完成总数
//                    condition += accounts + groupId;
//                    result += analysMapper.getDashboardFinishedTotal(schema_name, condition, beginTime, endTime, leftCondition, 4);
//                    break;
//                case "mttr":
//                    //MTTR（平均维修时效）MTTR = 总维修时间 / 总维修次数
//                    condition += accounts + groupId;
//                    double repairTimes = analysMapper.getDashboardRepairTotalFinished(schema_name, condition, beginTime, endTime, leftCondition);
//                    double repairMinute = analysMapper.getDashboardRepairTotalMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    if (repairTimes == 0) {
//                        result = 0.0;
//                    } else {
//                        result = repairMinute / repairTimes;
//                    }
//                    break;
//                case "repair_hour":
//                    //平均维修到场时效
//                    condition += accounts + groupId;
//                    double repairArriveTimes = analysMapper.getDashboardRepairArriveTotalMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    double repairFinishedTimes = analysMapper.getDashboardRepairTotalFinished(schema_name, condition, beginTime, endTime, leftCondition);
//                    if (repairFinishedTimes == 0) {
//                        result = 0.0;
//                    } else {
//                        result = repairArriveTimes / repairFinishedTimes;
//                    }
//                    break;
//                case "repair_time":
//                    //维修时间
//                    condition += accounts + groupId;
//                    result = analysMapper.getDashboardRepairTotalMinute(schema_name, condition, beginTime, endTime, leftCondition) / 60.0;
//                    break;
//                case "maintain_time":
//                    //维修时间
//                    condition += accounts + groupId;
//                    result = analysMapper.getDashboardMaintainTotalMinute(schema_name, condition, beginTime, endTime, leftCondition) / 60.0;
//                    break;
//                case "mtbf":
//                    //MTBF（平均故障间隔时间）MTBF = 故障间隔时间总和（每次宕机开始时间 — 每次正常运行开始时间）/ 故障次数
//                    condition += accounts + groupId;
//                    double stopMinute = analysMapper.getDashboardRepairStopTotalMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    double stopTimes = analysMapper.getDashboardRepairStopTotalMinuteCount(schema_name, condition, beginTime, endTime, leftCondition);
//                    if (stopTimes == 0) {
//                        result = 0.0;
//                    } else {
//                        result = stopMinute / stopTimes;
//                    }
//                    break;
//                case "fault_total":
//                    condition += accounts + groupId;
//                    result = analysMapper.getDashboardRepairStopTotalTimes(schema_name, condition, beginTime, endTime, leftCondition);
//                    break;
//                case "maintain_mttr":
//                    //>平均保养时效
//                    condition += accounts + groupId;
//                    double maintainTimes = analysMapper.getDashboardMaintainTotalFinished(schema_name, condition, beginTime, endTime, leftCondition);
//                    double maintainMinute = analysMapper.getDashboardMaintainTotalMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    if (maintainTimes == 0) {
//                        result = 0.0;
//                    } else {
//                        result = maintainMinute / maintainTimes;
//                    }
//                    break;
//                case "system":
//                    //系统可用时间，系统可用时间 = MTBF/（MTBF+MTTR）
//                    //MTBF
//                    condition += accounts + groupId;
//                    double mtbfMinute = analysMapper.getDashboardRepairStopTotalMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    double mtbfTimes = analysMapper.getDashboardRepairStopTotalTimes(schema_name, condition, beginTime, endTime, leftCondition);
//                    double mtbf = 0;
//                    if (mtbfTimes > 0) {
//                        mtbf = mtbfMinute / mtbfTimes;
//                    }
//                    //MTTR
//                    double MTTRMinute = analysMapper.getDashboardRepairTotalFinished(schema_name, condition, beginTime, endTime, leftCondition);
//                    double MTTRTimes = analysMapper.getDashboardRepairTotalMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    double MTTR = 0;
//                    if (MTTRTimes > 0) {
//                        MTTR = MTTRMinute / MTTRTimes;
//                    }
//                    //系统可用时间，系统可用时间 = MTBF/（MTBF+MTTR）
//                    if (mtbf == 0 && MTTR == 0) {
//                        result = 0.0;
//                    } else {
//                        result = mtbf / (mtbf + MTTR);
//                    }
//                    break;
//                case "pmc":
//                    //PMC（预防性维护合规性）PMC = 预防性维护工单完成数量之和/预防性维护工单数量之和
//                    condition += accounts + groupId;
//                    double planMaintainFinishedTotal = analysMapper.getDashboardMaintainTotalFinishedByCreateTime(schema_name, condition, beginTime, endTime, leftCondition);
//                    double planMaintainTotal = analysMapper.getDashboardMaintainTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    if (planMaintainTotal == 0) {
//                        result = 0.0;
//                    } else {
//                        result = planMaintainFinishedTotal / planMaintainTotal;
//                    }
//                    break;
//                case "smcp":
//                    //SMCP（计划维护关键百分比）SMCP = （预防性维护延迟天数+预防性维护周期）/ 预防性维护周期 X 100%
//                    condition += accounts + groupId;
//                    double delayMinute = analysMapper.getDashboardMaintainTotalDelayMinute(schema_name, condition, beginTime, endTime, leftCondition);
//                    result = (delayMinute + 60) / 60;
//                    break;
//                case "pmp":
//                    //PMP = 计划性维护工单数量 / 总的维护工单数量
//                    condition += accounts + groupId;
//                    double planTotal = analysMapper.getDashboardMaintainTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    double repairTotal = analysMapper.getDashboardRepairTotal(schema_name, condition, beginTime, endTime, leftCondition);
//                    if ((planTotal + repairTotal) == 0) {
//                        result = 0.0;
//                    } else {
//                        result = planTotal / (planTotal + repairTotal);
//                    }
//                    break;
//                case "chang":
//                    result = analysMapper.getDashboardChangTotal(schema_name, condition);
//                    break;
//                case "important_total":
//                    result = analysMapper.getDashboardAssetTotalByImportantLevelId(schema_name, 1, condition);
//                    break;
//                case "common_total":
//                    result = analysMapper.getDashboardAssetTotalByImportantLevelId(schema_name, 2, condition);
//                    break;
//            }
//        } catch (Exception e) {
//            result = 0.0;
//        }
//        return result;
//    }
//
//    //查找自己，以及所有的子位置
//    private String GetAllfacilityIds(List<Facility> allFacility, Integer currentFacility) {
//        String result = "," + currentFacility;
//        if (allFacility == null || allFacility.isEmpty()) {
//            return result;
//        }
//        for (Facility item : allFacility) {
//            if (item.getParentId() == currentFacility) {
//                result += "," + item.getParentId();
//                result += GetAllfacilityIds(allFacility, item.getId());
//            }
//        }
//        return result;
//    }
//
//    //获取季度
//    public static int getSeason(Date date) {
//        int season = 0;
//        Calendar c = Calendar.getInstance();
//        c.setTime(date);
//        int month = c.get(Calendar.MONTH);
//        switch (month) {
//            case Calendar.JANUARY:
//            case Calendar.FEBRUARY:
//            case Calendar.MARCH:
//                season = 1;
//                break;
//            case Calendar.APRIL:
//            case Calendar.MAY:
//            case Calendar.JUNE:
//                season = 2;
//                break;
//            case Calendar.JULY:
//            case Calendar.AUGUST:
//            case Calendar.SEPTEMBER:
//                season = 3;
//                break;
//            case Calendar.OCTOBER:
//            case Calendar.NOVEMBER:
//            case Calendar.DECEMBER:
//                season = 4;
//                break;
//            default:
//                break;
//        }
//        return season;
//    }
}
