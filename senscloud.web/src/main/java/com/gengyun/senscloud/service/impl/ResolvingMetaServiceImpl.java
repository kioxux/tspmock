package com.gengyun.senscloud.service.impl;


import com.gengyun.senscloud.service.ResolvingMetaService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by Zys on 2019/04/23.
 */
//@CacheConfig(cacheNames = "meta")
@Service
@Component
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ResolvingMetaServiceImpl implements ResolvingMetaService {
//    private static final String ALARM_TYPE = "2";//数据类型-报警类型
//    private static final String DOSAGE_TYPE = "1";//数据类型-用量类型
//    private static final String RECEIVE_TYPE = "4";
//    private static final String STATUS_TYPE = "3";//数据类型-状态类型
//
//    private static final String SCADA_TYPE = "4";//显示类型-scada
//    private static final String SCADA_CHART_TYPE = "2";//显示类型-scada和监控类型
//    private static final String MONITOR_CHART_TYPE = "1";//显示类型-监控列表
//
//    private static final String COUNT_TYPE_SUM = "1";//统计类型-累计
//    private static final String COUNT_TYPE_AVG= "2";//统计类型-平均
//    private static final String COUNT_TYPE_MAX = "3";//统计类型-最大
//    private static final String COUNT_TYPE_MIN= "4";//统计类型-最小
//
//    @Autowired
//    SetMonitorMapper setMonitorMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @Cacheable
//    @Override
//    public Map<String, Object> monitorMeta(String schema_name) {
//        return this.monitorMetaall(schema_name);
//    }
//
//    @CachePut
//    @Override
//    public Map<String, Object> refreshMonitorMeta(String schema_name) {
//        return this.monitorMetaall(schema_name);
//    }
//
//
//    public Map<String, Object> monitorMetaall(String schema_name) {
//        try {
//            Map<String, Object> resultMap = new HashMap<String, Object>();
//            List<Map<String, Object>> assetCategoryList = selectOptionService.SelectOptionList(schema_name,null,"asset_type_app",null,null);
//            String asset_category_id = "";
//            for (Map<String, Object> assetCategory : assetCategoryList) {
//                asset_category_id = String.valueOf(assetCategory.get("code"));
//                resultMap.put(asset_category_id, this.monitorMetaById(asset_category_id,schema_name));
//            }
//            return resultMap;
//        } catch (Exception e) {
//            return new HashMap<>();
//        }
//
//    }
//
//    private Map<String, Object> monitorMetaById(String asset_category_id,String schema_name) {
//        try {
//            Map<String, Object> resultMap = new HashMap<String, Object>();
//            Map<String, String> recordIntervalMap = new HashMap<String, String>();
//            Map<String, String> judgeAlarmMap = new HashMap<String, String>();
//            Map<String, String> judgeMap = new HashMap<String, String>();
//            Map<String, String> nameValueMap = new HashMap<String, String>();
//            List<String> statusList = new ArrayList<String>();
//            List<String> scadaList = new ArrayList<String>();
//            List<String> monitorTableList = new ArrayList<String>();
//            List<String> dosageList = new ArrayList<String>();
//            List<String> countList = new ArrayList<String>();
//            List<String> countSumList = new ArrayList<String>();
//            List<String> countAvgList = new ArrayList<String>();
//            List<String> countMaxList = new ArrayList<String>();
//            List<String> countMinList = new ArrayList<String>();
//            List<String> alarmList = new ArrayList<String>();
//            List<String> keyList=new ArrayList<String>();
//            List<Map<String, Object>> monitorList = setMonitorMapper.findSetMonitorListByCategoryId(schema_name,asset_category_id);
//            String record_interval = "";
//            String monitor_key = "";
//            String monitor_name = "";
//            String gather_value_type = "";
//            Boolean judge_parameter = false;
//            String judge_value = "";
//            String monitor_category = "";
//            String varIds = "";
//            String showType="";
//            if (null == monitorList || monitorList.isEmpty()) {
//                return null;
//            }
//            for (Map<String, Object> monitorData : monitorList) {
//                record_interval = String.valueOf(monitorData.get("record_interval"));
//                monitor_key = (String) monitorData.get("monitor_key");
//                monitor_name = (String) monitorData.get("monitor_name");
//                nameValueMap.put(monitor_key,monitor_name);
//                gather_value_type = String.valueOf(monitorData.get("gather_value_type")) ;
//                judge_parameter = (Boolean) monitorData.get("is_judge_parameter");
//                judge_value = String.valueOf(monitorData.get("judge_value"));
//                monitor_category = String.valueOf(monitorData.get("monitor_category"));
//                showType=String.valueOf(monitorData.get("show_type"));
//                if (null != record_interval&&!"null".equals(record_interval)&&!"0".equals(record_interval)) {
//                    recordIntervalMap.put(monitor_key, record_interval);
//                }
//                if (null != judge_parameter && judge_parameter) {
//                    if (ALARM_TYPE.equals(monitor_category)) {
//                        judgeAlarmMap.put(monitor_key, judge_value);
//                    } else {
//                        judgeMap.put(monitor_key, judge_value);
//                    }
//                }
//                if (ALARM_TYPE.equals(monitor_category) && !judge_parameter) {
//                    alarmList.add(monitor_key);
//                }
//                if (COUNT_TYPE_SUM.equals(gather_value_type)) {
//                    countSumList.add(monitor_key);
//                    countList.add(monitor_key);
//                }
//                if (COUNT_TYPE_AVG.equals(gather_value_type)) {
//                    countAvgList.add(monitor_key);
//                    countList.add(monitor_key);
//                }
//                if (COUNT_TYPE_MAX.equals(gather_value_type)) {
//                    countMaxList.add(monitor_key);
//                    countList.add(monitor_key);
//                }
//                if (COUNT_TYPE_MIN.equals(gather_value_type)) {
//                    countMinList.add(monitor_key);
//                    countList.add(monitor_key);
//                }
//                if (DOSAGE_TYPE.equals(monitor_category)||RECEIVE_TYPE.equals(monitor_category)) {
//                    dosageList.add(monitor_key);
//                }
//                if (SCADA_TYPE.equals(showType)||SCADA_CHART_TYPE.equals(showType)) {
//                    scadaList.add(monitor_key);
//                }
//                if (MONITOR_CHART_TYPE.equals(showType)||SCADA_CHART_TYPE.equals(showType)) {
//                    monitorTableList.add(monitor_key);
//                }
//                if (STATUS_TYPE.equals(monitor_category)) {
//                    statusList.add(monitor_key);
//                }
//                varIds += (monitor_key + ",");
//                keyList.add(monitor_key);
//            }
//            if (varIds.isEmpty()) {
//                return null;
//            }
//            varIds = varIds.substring(0, varIds.length() - 1);
//            resultMap.put("recordIntervalMap", recordIntervalMap);
//            resultMap.put("judgeAlarmMap", judgeAlarmMap);
//            resultMap.put("judgeMap", judgeMap);
//            resultMap.put("statusList", statusList);
//            resultMap.put("dosageList", dosageList);
//            resultMap.put("countList", countList);
//            resultMap.put("countSumList", countSumList);
//            resultMap.put("countAvgList", countAvgList);
//            resultMap.put("countMinList", countMinList);
//            resultMap.put("countMaxList", countMaxList);
//            resultMap.put("scadaList", scadaList);
//            resultMap.put("monitorTableList", monitorTableList);
//            resultMap.put("nameValueMap", nameValueMap);
//            resultMap.put("alarmList", alarmList);
//            resultMap.put("keyList", keyList);
//            resultMap.put("varIds", varIds);
//            return resultMap;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
}
