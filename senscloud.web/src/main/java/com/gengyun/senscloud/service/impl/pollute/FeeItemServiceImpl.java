package com.gengyun.senscloud.service.impl.pollute;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.CustomerPolluteFeeItemMapper;
import com.gengyun.senscloud.model.FeeItemModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.pollute.FeeItemService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FeeItemServiceImpl implements FeeItemService {

    @Resource
    CustomerPolluteFeeItemMapper customerPolluteFeeItemMapper;

    /**
     * 查询费用项列表
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> findPolluteFeeItemList(MethodParam methodParam, Map<String, Object> param) {
        methodParam.setNeedPagination(true);
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        //查询该条件下的总记录数
        int total = customerPolluteFeeItemMapper.findPolluteFeeItemListCount(methodParam.getSchemaName(), param);
        if (total < 1) {
            Map<String, Object> result = new HashMap<>();
            result.put("total", total);
            result.put("rows", new ArrayList<>());
            return result;
        }
        param.put("pagination", pagination);//分页参数
        //查询数据列表
        List<Map<String, Object>> rows = customerPolluteFeeItemMapper.findPolluteFeeItemList(methodParam.getSchemaName(), param);
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("rows", rows);
        return result;
    }

    /**
     * 新增费用项
     * @param methodParam
     * @param param
     */
    @Override
    public void newPolluteFeeItem(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optStrOrExpNotNull(param.get("fee_code"), LangConstant.TITLE_ABAAA_T);//编码不能为空
        param.put("is_use", '1');
        param.put("status", 1);
        param.put("create_user_id", methodParam.getUserId());
        customerPolluteFeeItemMapper.insertPolluteFeeItem(methodParam.getSchemaName(), param);
    }

    /**
     * 修改费用项
     * @param methodParam
     * @param param
     */
    @Override
    public void modifyPolluteFeeItem(MethodParam methodParam, Map<String, Object> param) {
        RegexUtil.optStrOrExpNotNull(param.get("fee_code"), LangConstant.TITLE_ABAAA_T);//编码不能为空
        customerPolluteFeeItemMapper.updatePolluteFeeItem(methodParam.getSchemaName(), param);
    }

    /**
     * 费用项详情
     * @param methodParam
     * @param feeItemModel
     * @return
     */
    @Override
    public Map<String, Object> findById(MethodParam methodParam, FeeItemModel feeItemModel) {
        Integer id = RegexUtil.optIntegerOrExpParam(feeItemModel.getId(), LangConstant.TITLE_B_O);//不能为空
        return customerPolluteFeeItemMapper.findById(methodParam.getSchemaName(),id);
    }

    /**
     * 费用项删除
     * @param methodParam
     * @param feeItemModel
     */
    @Override
    public void deletePolluteFeeItem(MethodParam methodParam, FeeItemModel feeItemModel) {
        Integer id = RegexUtil.optIntegerOrExpParam(feeItemModel.getId(), LangConstant.TITLE_B_O);//不能为空
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        param.put("status", StatusConstant.STATUS_DELETEED);
        customerPolluteFeeItemMapper.updatePolluteFeeItem(methodParam.getSchemaName(),param);
    }

    /**
     * 启用禁用
     * @param methodParam
     * @param feeItemModel
     */
    @Override
    public void modifyUsePolluteFeeItem(MethodParam methodParam, FeeItemModel feeItemModel) {
        Integer id = RegexUtil.optIntegerOrExpParam(feeItemModel.getId(), LangConstant.TITLE_B_O);//不能为空
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        param.put("is_use", feeItemModel.getIs_use());
        customerPolluteFeeItemMapper.updatePolluteFeeItem(methodParam.getSchemaName(),param);
    }


}
