package com.gengyun.senscloud.service.impl.snc;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.mapper.SncDataImportMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.snc.SncDataImportService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

@Service
public class SncDataImportServiceImpl implements SncDataImportService {
    private static final Logger logger = LoggerFactory.getLogger(SncDataImportServiceImpl.class);
    @Value("${senscloud.file_upload}")
    private String file_upload_dir;

    @Resource
    SncDataImportMapper sncDataImportMapper;
    @Resource
    CompanyService companyService;
    /**
     * 维修工单打印pdf
     *
     * @param methodParam
     * @param paramMap
     */
    @Override
    public void exportWorksPdf(MethodParam methodParam, Map<String, Object> paramMap) {
//        paramMap.put("subWorkCode","WD20210519000004");
        String subWorkCode = RegexUtil.optStrOrNull(paramMap.get("sub_work_code"));
        List<Map<String, Object>> list = findRepairWorks(methodParam.getSchemaName(), subWorkCode, methodParam.getUserLang(), methodParam.getCompanyId());
        Map<String, Object> ext = sncDataImportMapper.findRepairWorksExt(methodParam.getSchemaName(), subWorkCode);
        Map<String, Map<String, String>> centers = sncDataImportMapper.findCostCenter(methodParam.getSchemaName());
        Map<String, Map<String, String>> types = findCostType(methodParam.getSchemaName(), methodParam.getUserLang(), methodParam.getCompanyId(), "cost_type");
        List<Map<String, String>> subs = sncDataImportMapper.findWorksCostControl(methodParam.getSchemaName(), subWorkCode);
        Map<String, Map<String, String>> bomTypes = sncDataImportMapper.findBomType(methodParam.getSchemaName());
        Map<String, Map<String, String>> boms = sncDataImportMapper.findBoms(methodParam.getSchemaName());
        Map<String, Map<String, String>> stocks = sncDataImportMapper.findStocks(methodParam.getSchemaName());

        Map<String, Object> data = new HashMap<>();
        if (RegexUtil.optIsPresentMap(ext)) {
            data.putAll(ext);
        } else {
            data.put("finished_time", "");
            data.put("audit_time", "");
            data.put("audit_user", "");
            data.put("create_time", "");
        }
        Map<String, String> bom_title = new HashMap<>();
        Map<String, String> fee_title = new HashMap<>();
        List<Map<String, String>> bomList = new ArrayList<>();
        List<Map<String, String>> feeList = new ArrayList<>();
        List<String> bom_form_code_list = new ArrayList<>();
        List<String> fee_form_code_list = new ArrayList<>();
        for (Map<String, String> sub : subs) {
            String fieldCode = RegexUtil.optStrOrNull(sub.get("field_code"));
            String condition = RegexUtil.optStrOrNull(sub.get("condition"));
            JSONObject jsonObject = JSONObject.fromObject(condition);
            JSONArray array = jsonObject.optJSONArray("page");
            for (int i = 0; i < array.size(); i++) {
                JSONObject object = array.getJSONObject(i);
                String field_form_code = object.optString("field_form_code");
                String source = object.optString("source");
                String fieldCode1 = object.optString("field_code");
                if (RegexUtil.optIsPresentStr(source)) {
                    field_form_code = field_form_code + "-" + source;
                }
                if ("bom_write_ues".equals(fieldCode)) {
                    bom_form_code_list.add(field_form_code);
                    bom_title.put(fieldCode1, field_form_code);

                } else {
                    fee_form_code_list.add(field_form_code);
                    fee_title.put(fieldCode1, field_form_code);
                }
            }
        }

        //文字类
        Map<String, String> dataMap = new HashMap<String, String>();
        for (Map<String, Object> map : list) {
            String fieldCode = RegexUtil.optStrOrNull(map.get("field_code"));
            String field_value = RegexUtil.optStrOrVal(map.get("field_value"), "");
            String textFields = "sub_work_code,relation_id,position_code,relation_name,category_id,running_status_id,guarantee_status_id,problem_note,create_user_id,fault_type_id,deal_result,fault_reason,solutions_note,fault_reason_note,solutions,receive_user_id,audit_opinion,approve_remark";
            String imgFields = "problem_img,result_img";
            String tableFields = "bom_write_ues,cost_control";
            if (textFields.contains(fieldCode)) {
                data.put(fieldCode, field_value);
                dataMap.put(fieldCode, field_value);
            }
            if (imgFields.contains(fieldCode)) {
                //  if (RegexUtil.optIsPresentStr(field_value) && field_value.startsWith("[")) {
                if (RegexUtil.optIsPresentStr(field_value) ) {
                    //JSONArray array = JSONArray.fromObject(field_value);
                    List<Map<String, String>> picList = new ArrayList<>();
                    String[] array = field_value.split(",");
                    for (int i = 0; i < array.length; i++) {
                        Map<String, String> problem = new HashMap<>();
                        //JSONObject jsonObject = array.getJSONObject(i);
                        // int id = jsonObject.optInt("id", 0);
                        try {
                            int id = Integer.valueOf(array[i]); //防止id错误异常
                            Map<String, Object> path = sncDataImportMapper.findFileInfoById(methodParam.getSchemaName(), id);
                            if (RegexUtil.optIsPresentMap(path)) {
                                String file_original_name = RegexUtil.optStrOrNull(path.get("file_original_name"));
                                String file_url = RegexUtil.optStrOrNull(path.get("file_url"));
                                //String base64 = WordExportUtil.imageToBase64Str(file_url);
                                String base64 = imageToBase64ApacheStr(file_url);
                                logger.info("imgName=====:" + file_original_name);
                                logger.info("imgId=====:" + id);
                                logger.info("imgUrl=====:" + file_url);
                                if (RegexUtil.optIsPresentStr(base64)) {
                                    problem.put("id", String.valueOf(id));
                                    problem.put("img", file_original_name);
                                    problem.put("base64", base64);
                                    picList.add(problem);
                                }
                            }
                        }catch (Exception e){
                            logger.error(e.getMessage());
                        }
                    }
                    if (RegexUtil.optIsPresentList(picList)) {
                        data.put(fieldCode, picList);
                    }
                }
            }
            if (tableFields.contains(fieldCode)) {
                if (RegexUtil.optIsPresentStr(field_value) && field_value.startsWith("[")) {
                    if ("bom_write_ues".equals(fieldCode)) {
                        JSONArray array = JSONArray.fromObject(field_value);
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            Map<String, String> bom = new HashMap<>();
                            Set<String> bom_set = bom_title.keySet();
                            for (String b_title_code : bom_set) {
                                String bom_form_code = RegexUtil.optStrOrNull(bom_title.get(b_title_code));
                                String form_code = bom_form_code;
                                String source = "";
                                if (bom_form_code.contains("-")) {
                                    String[] split = bom_form_code.split("-");
                                    form_code = split[0];
                                    source = split[1];
                                }
                                if (jsonObject.containsKey(form_code)) {
                                    String value = jsonObject.optString(form_code, "");
                                    if ("bom_list".equals(source)) {
                                        if (boms.containsKey(value)) {
                                            Map<String, String> map1 = boms.get(value);
                                            String bom_name = RegexUtil.optStrOrVal(map1.get("bom_name"), "");
                                            bom.put(b_title_code, bom_name);
                                        } else {
                                            bom.put(b_title_code, "");
                                        }
                                    } else if ("bom_type".equals(source)) {
                                        if (bomTypes.containsKey(value)) {
                                            Map<String, String> map1 = bomTypes.get(value);
                                            String type_name = RegexUtil.optStrOrVal(map1.get("type_name"), "");
                                            bom.put(b_title_code, type_name);
                                        } else {
                                            bom.put(b_title_code, "");
                                        }
                                    } else if ("stock_list".equals(source)) {
                                        if (stocks.containsKey(value)) {
                                            Map<String, String> map1 = stocks.get(value);
                                            String stock_name = RegexUtil.optStrOrVal(map1.get("stock_name"), "");
                                            bom.put(b_title_code, stock_name);
                                        } else {
                                            bom.put(b_title_code, "");
                                        }
                                    } else {
                                        bom.put(b_title_code, value);
                                    }
                                } else {
                                    bom.put(b_title_code, "");
                                }
                            }
                            bomList.add(bom);
                        }
                    } else {
                        JSONArray array = JSONArray.fromObject(field_value);
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            Map<String, String> fee = new HashMap<>();
                            Set<String> fee_set = fee_title.keySet();
                            for (String f_title_code : fee_set) {
                                String fee_form_code = RegexUtil.optStrOrNull(fee_title.get(f_title_code));
                                String form_code = fee_form_code;
                                String source = "";
                                if (fee_form_code.contains("-")) {
                                    String[] split = fee_form_code.split("-");
                                    form_code = split[0];
                                    source = split[1];
                                }
                                if (jsonObject.containsKey(form_code)) {
                                    String value = jsonObject.optString(form_code, "");
                                    if (RegexUtil.optIsPresentStr(source)) {
                                        if (source.contains("type")) {
                                            if (types.containsKey(value)) {
                                                Map<String, String> map1 = types.get(value);
                                                String stock_name = RegexUtil.optStrOrVal(map1.get("stock_name"), "");
                                                fee.put(f_title_code, stock_name);
                                            } else {
                                                fee.put(f_title_code, "");
                                            }
                                        } else {
                                            if (centers.containsKey(value)) {
                                                Map<String, String> map1 = centers.get(value);
                                                String cost_name = RegexUtil.optStrOrVal(map1.get("cost_name"), "");
                                                fee.put(f_title_code, cost_name);
                                            } else {
                                                fee.put(f_title_code, "");
                                            }
                                        }
                                    } else {
                                        fee.put(f_title_code, value);
                                    }
                                } else {
                                    fee.put(f_title_code, "");
                                }
                            }
                            feeList.add(fee);
                        }
                    }
                }
            }
        }
        BigDecimal total = BigDecimal.ZERO;
        if (RegexUtil.optIsPresentList(feeList)) {
            for (Map<String, String> map : feeList) {
                String cost_money = RegexUtil.optStrOrVal(map.get("cost_money"), "1");
                BigDecimal price_b = BigDecimal.ZERO;
                try {
                    price_b = new BigDecimal(cost_money);
                } catch (Exception e) {
                }
                total = total.add(price_b);
                map.put("money", String.valueOf(price_b));
            }
            int size = feeList.size();
            if (size < 3) {
                for (int i = size; i < 3; i++) {
                    Map<String, String> map = new HashMap<>();
                    map.put("cost_property", "");
                    map.put("cost_class", "");
                    map.put("money", "");
                    map.put("cost_note", "");
                    feeList.add(map);
                }
            }
            data.put("cost_control", feeList);
        } else {
            for (int i = 1; i < 4; i++) {
                Map<String, String> map = new HashMap<>();
                map.put("cost_property", "");
                map.put("cost_class", "");
                map.put("money", "");
                map.put("cost_note", "");
                feeList.add(map);
            }
            data.put("cost_control", feeList);
        }
        data.put("total", String.valueOf(total));
        if (RegexUtil.optIsPresentList(bomList)) {
            int size = bomList.size();
            if (size < 5) {
                for (int i = size; i < 5; i++) {
                    Map<String, String> map = new HashMap<>();
                    map.put("bom_id", "");
                    map.put("bom_code", "");
                    map.put("bom_model", "");
                    map.put("bom_type_id", "");
                    map.put("stock_id", "");
                    map.put("use_count", "");
                    bomList.add(map);
                }
            }
            data.put("bom_write_ues", bomList);
        } else {
            for (int i = 1; i < 6; i++) {
                Map<String, String> map = new HashMap<>();
                map.put("bom_id", "");
                map.put("bom_code", "");
                map.put("bom_model", "");
                map.put("bom_type_id", "");
                map.put("stock_id", "");
                map.put("use_count", "");
                bomList.add(map);
            }
            data.put("bom_write_ues", bomList);
        }

        String sep = System.getProperty("file.separator");
        String wordFileName = subWorkCode + ".doc";
        String pdfFileName = subWorkCode + ".pdf";
        String importFilePath = file_upload_dir + sep + "importTemplate" + sep;
        String exportFilePath = file_upload_dir + sep + "exportTemp" + sep;
        String wordFilePath = exportFilePath + wordFileName;
        String pdfFilePath = exportFilePath + pdfFileName;
        File file = new File(exportFilePath);
        if(!file.exists()){
            file.mkdirs();
        }
        WordExportUtil.createWord(data, importFilePath, "repair726.ftl", wordFilePath);
        AsposeUtil.wordToPdf(wordFilePath, pdfFilePath);
        DownloadUtil.download(pdfFilePath, pdfFileName, DownloadUtil.CONTENT_TYPE_PDF, false);
    }



    //    public void exportWorksPdf(MethodParam methodParam, Map<String, Object> paramMap) {
////        Map<String, Object> singleWorkInfo = workSheetHandleService.getSingleWorkInfo(methodParam, paramMap, "WD20210519000004", "detail", "60", null);
//        String subWorkCode = RegexUtil.optStrOrNull(paramMap.get("subWorkCode"));
//        List<Map<String, Object>> list = worksMapper.findRepairWorks(methodParam.getSchemaName(), subWorkCode, methodParam.getUserLang(), methodParam.getCompanyId());
//        Map<String, Map<String, String>> centers = worksMapper.findCostCenter(methodParam.getSchemaName());
//        Map<String, Map<String, String>> types = worksMapper.findCostType(methodParam.getSchemaName(), methodParam.getUserLang(), methodParam.getCompanyId(), "cost_type");
//        List<Map<String, String>> subs = worksMapper.findWorksCostControl(methodParam.getSchemaName(), subWorkCode);
//        Map<String, Map<String, String>> bomTypes = worksMapper.findBomType(methodParam.getSchemaName());
//        Map<String, Map<String, String>> boms = worksMapper.findBoms(methodParam.getSchemaName());
//        Map<String, Map<String, String>> stocks = worksMapper.findStocks(methodParam.getSchemaName());
//
//        ArrayList<List<String>> bom_list = new ArrayList<>();
//        ArrayList<List<String>> fee_list = new ArrayList<>();
//
//
//        List<String> bom_name_list = new ArrayList<>();
//        List<String> fee_name_list = new ArrayList<>();
//        List<String> bom_form_code_list = new ArrayList<>();
//        List<String> fee_form_code_list = new ArrayList<>();
//        for (Map<String, String> sub : subs) {
//            String fieldCode = RegexUtil.optStrOrNull(sub.get("field_code"));
//            String condition = RegexUtil.optStrOrNull(sub.get("condition"));
//            JSONObject jsonObject = JSONObject.fromObject(condition);
//            JSONArray array = jsonObject.optJSONArray("page");
//            for (int i = 0; i < array.size(); i++) {
//                JSONObject object = array.getJSONObject(i);
//                String field_name = object.optString("field_name");
//                String field_form_code = object.optString("field_form_code");
//                String source = object.optString("source");
//                if (RegexUtil.optIsPresentStr(source)) {
//                    field_form_code = field_form_code + "-" + source;
//                }
//                if ("bom_write_ues".equals(fieldCode)) {
//                    bom_name_list.add(field_name);
//                    bom_form_code_list.add(field_form_code);
//                } else {
//                    fee_name_list.add(field_name);
//                    fee_form_code_list.add(field_form_code);
//                }
//            }
//        }
//        bom_list.add(bom_name_list);
//        fee_list.add(fee_name_list);
//
//        //文字类
//        Map<String, String> dataMap = new HashMap<String, String>();
//        Map<String, String> imgMap = new HashMap<String, String>();
//        for (Map<String, Object> map : list) {
//            String fieldCode = RegexUtil.optStrOrNull(map.get("field_code"));
//            String field_value = RegexUtil.optStrOrVal(map.get("field_value"), "");
//            String textFields = "relation_id,position_code,relation_name,category_id,running_status_id,guarantee_status_id,problem_note,create_user_id,fault_type_id,deal_result,fault_reason,solutions_note,fault_reason_note,solutions,receive_user_id,audit_opinion,approve_remark";
//            String imgFields = "problem_img,result_img";
//            String tableFields = "bom_write_ues,cost_control";
//
//            if (textFields.contains(fieldCode)) {
//                dataMap.put(fieldCode, field_value);
//            }
//            if (imgFields.contains(fieldCode)) {
//                if (RegexUtil.optIsPresentStr(field_value) && field_value.startsWith("[")) {
//                    JSONArray array = JSONArray.fromObject(field_value);
//                    for (int i = 0; i < array.size(); i++) {
//                        JSONObject jsonObject = array.getJSONObject(i);
//                        int id = jsonObject.optInt("id", 0);
//                        String path = worksMapper.findFileById(methodParam.getSchemaName(), id);
//                        imgMap.put(fieldCode, path);
//                    }
//                }
////                else {
////                    imgMap.put(fieldCode, "");
////                }
//            }
//            if (tableFields.contains(fieldCode)) {
//                if (RegexUtil.optIsPresentStr(field_value) && field_value.startsWith("[")) {
//                    if ("bom_write_ues".equals(fieldCode)) {
//                        JSONArray array = JSONArray.fromObject(field_value);
//                        for (int i = 0; i < array.size(); i++) {
//                            JSONObject jsonObject = array.getJSONObject(i);
//                            List<String> dataList = new ArrayList<>();
//                            for (String bom_form_code : bom_form_code_list) {
//                                String form_code = bom_form_code;
//                                String source = "";
//                                if (bom_form_code.contains("-")) {
//                                    String[] split = bom_form_code.split("-");
//                                    form_code = split[0];
//                                    source = split[1];
//                                }
//                                if (jsonObject.containsKey(form_code)) {
//                                    String value = jsonObject.optString(form_code, "");
//                                    if ("bom_list".equals(source)) {
//                                        if (boms.containsKey(value)) {
//                                            Map<String, String> map1 = boms.get(value);
//                                            String bom_name = RegexUtil.optStrOrVal(map1.get("bom_name"), "");
//                                            dataList.add(bom_name);
//                                        } else {
//                                            dataList.add("");
//                                        }
//                                    } else if ("bom_type".equals(source)) {
//                                        if (bomTypes.containsKey(value)) {
//                                            Map<String, String> map1 = bomTypes.get(value);
//                                            String type_name = RegexUtil.optStrOrVal(map1.get("type_name"), "");
//                                            dataList.add(type_name);
//                                        } else {
//                                            dataList.add("");
//                                        }
//                                    } else if ("stock_list".equals(source)) {
//                                        if (stocks.containsKey(value)) {
//                                            Map<String, String> map1 = stocks.get(value);
//                                            String stock_name = RegexUtil.optStrOrVal(map1.get("stock_name"), "");
//                                            dataList.add(stock_name);
//                                        } else {
//                                            dataList.add("");
//                                        }
//                                    } else {
//                                        dataList.add(value);
//                                    }
//                                } else {
//                                    dataList.add("");
//                                }
//                            }
//                            bom_list.add(dataList);
//                        }
//                    } else {
//                        JSONArray array = JSONArray.fromObject(field_value);
//                        for (int i = 0; i < array.size(); i++) {
//                            JSONObject jsonObject = array.getJSONObject(i);
//                            List<String> dataList = new ArrayList<>();
//                            for (String fee_form_code : fee_form_code_list) {
//                                String form_code = fee_form_code;
//                                String source = "";
//                                if (fee_form_code.contains("-")) {
//                                    String[] split = fee_form_code.split("-");
//                                    form_code = split[0];
//                                    source = split[1];
//                                }
//                                if (jsonObject.containsKey(form_code)) {
//                                    String value = jsonObject.optString(form_code, "");
//                                    if (RegexUtil.optIsPresentStr(source)) {
//                                        if (source.contains("type")) {
//                                            if (types.containsKey(value)) {
//                                                Map<String, String> map1 = types.get(value);
//                                                String stock_name = RegexUtil.optStrOrVal(map1.get("stock_name"), "");
//                                                dataList.add(stock_name);
//                                            } else {
//                                                dataList.add("");
//                                            }
//                                        } else {
//                                            if (centers.containsKey(value)) {
//                                                Map<String, String> map1 = centers.get(value);
//                                                String stock_name = RegexUtil.optStrOrVal(map1.get("cost_name"), "");
//                                                dataList.add(stock_name);
//                                            } else {
//                                                dataList.add("");
//                                            }
//                                        }
//                                    } else {
//                                        dataList.add(value);
//                                    }
//                                } else {
//                                    dataList.add("");
//                                }
//                            }
//                            fee_list.add(dataList);
//                        }
//                    }
//                }
//            }
//        }
//        Map<String, List<List<String>>> listMap = new HashMap<String, List<List<String>>>();
//        listMap.put("cost_control", fee_list);
//        listMap.put("bom_write_ues", bom_list);
//        Map<String, Object> o = new HashMap<String, Object>();
//        o.put("datemap", dataMap);
//        o.put("imgmap", imgMap);
//        o.put("list", listMap);
//
//        String filePath = "C:/data/createSamplePDF.pdf";
//        creatPdf(o, filePath);
//    }


    /**
     * 图片转base64的字符串
     *
     * @param imageAbsolutePath 图片路径
     * @return String
     */
    private static String imageToBase64ApacheStr(String imageAbsolutePath) {
        InputStream is = null;
        byte[] data = null;
        try {
            is = new FileInputStream(imageAbsolutePath);
            data = new byte[is.available()];
            is.read(data);
            is.close();
        } catch (Exception e) {
            logger.error("imgPath2base64===:" + e);
            e.printStackTrace();
            return null;
        }
        //BASE64Encoder encoder = new BASE64Encoder();
        org.apache.commons.codec.binary.Base64 base64 = new Base64();
        //return encoder.encode(data);
        return base64.encodeToString(data);
    }

    public List<Map<String, Object>> findRepairWorks(String schemaName, String subWorkCode, String userLang, Long companyId) {
        List<Map<String, Object>> list = sncDataImportMapper.findRepairWorks(schemaName, subWorkCode);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(list)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                list.forEach(sd -> {
                    if (dd.containsKey(sd.get("name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.get("name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("name", value);
                        }
                    }
                });
            }
        }
        return list;
    }

    public Map<String, Map<String, String>> findCostType(String schemaName, String userLang, Long companyId, String data_type) {
        Map<String, Map<String, String>> types = sncDataImportMapper.findCostType(schemaName, data_type);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentMap(result)) {
            JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
            if (Optional.ofNullable(types).filter(e -> types.size() > 0).isPresent()) {
                List<Map<String, String>> res = JSON.parseObject(JSON.toJSONString(types.values()), List.class);
                res.forEach(re -> {
                    if (dd.containsKey(re.get("name"))) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(re.get("name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            re.put("name", value);
                            if (types.containsKey(re.get("code"))) {
                                types.put(re.get("code"), re);
                            }
                        }
                    }
                });
            }
        }
        return types;
    }

}
