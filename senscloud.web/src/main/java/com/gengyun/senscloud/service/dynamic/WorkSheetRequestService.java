package com.gengyun.senscloud.service.dynamic;

/**
 * 工单请求
 * User: sps
 * Date: 2019/05/15
 * Time: 下午15:20
 */
public interface WorkSheetRequestService {
//
//    /**
//     * 获取工单请求列表
//     *
//     * @param request
//     * @return
//     */
//    String findWsqList(HttpServletRequest request);
//
//    /**
//     * 根据主键查询工单请求数据（不含json串）
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    Map<String, Object> queryWsqInfoById(String schemaName, String workRequestCode);
//
//    /**
//     * 根据主键查询工单请求数据
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    Map<String, Object> queryWorkRequestById(String schemaName, String workRequestCode);
//
//    /**
//     * 手动回滚数据
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @param status
//     */
//    void doRollBackData(String schemaName, String workRequestCode, Object status);
//
//    /**
//     * 作废工单
//     *
//     * @param request
//     * @return
//     */
//    ResponseModel cancelWsq(HttpServletRequest request);
//
////    /**
////     * 根据主键删除工单请求数据
////     *
////     * @param schemaName
////     * @param workRequestCode
////     */
////    void deleteByWsqCode(String schemaName, String workRequestCode);
//
//    /**
//     * 更新请求单状态
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @param status
//     */
//    void doUpdateWsqStatus(String schemaName, String workRequestCode, Object status);
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception;
//
//    /**
//     * 工单作废关闭服务单
//     *
//     * @param schemaName
//     * @param workCode
//     * @param account
//     */
//    void cancelWsqByWork(String schemaName, String workCode, String account);
//
//    /**
//     * 取工单请求业务号
//     *
//     * @param schemaName
//     * @return
//     */
//    String selectWqBusinessInfo(String schemaName);
//
//    List<Map<String, Object>> findWsqServiceList(HttpServletRequest request);
//    /**
//     * 移动端查询工单请求数量
//     *
//     * @param request
//     * @return
//     */
//    int findWsqServiceListCount(HttpServletRequest request);
//
//    Map<String, Object> getWorksheetRequestDetail(HttpServletRequest request);
}
