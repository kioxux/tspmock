package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 大屏看板
 */
public interface KanBanService {

    /**
     * 获取待办故障任务列表
     *
     * @return 待办故障任务列表
     */
    List<Map<String, Object>> getRepairTaskList(MethodParam methodParam);

    /**
     * 获取待办维护任务列表
     *
     * @return 待办维护任务列表
     */
    List<Map<String, Object>> getMaintainTaskList(MethodParam methodParam);

    /**
     * 今日完成  本周完成  本月完成
     *
     * @param methodParam
     * @return
     */
    Map<String, Object> getFinishedTaskCount(MethodParam methodParam);

    /**
     * 当前上班人员列表
     *
     * @param methodParam
     * @return
     */
    List<Map<String, Object>> getOnDutyUserList(MethodParam methodParam);

    /**
     * 故障任务总数
     * 当前的故障任务数   今日新增故障任务数   总数
     *
     * @param methodParam
     * @return
     */
    Map<String, Object> getRepairFinishedCount(MethodParam methodParam);

    /**
     * 维护任务总数
     * 当前的维护任务数   今日新增维护任务数   总数
     *
     * @param methodParam
     * @return
     */
    Map<String, Object> getMaintainFinishedCount(MethodParam methodParam);

    /**
     * 获取本年设备利用率列表
     *
     * @param methodParam
     * @return
     */
    Map<String, Object> getNowYearRateList(MethodParam methodParam);

    /**
     * 本月运行时间 本月故障时间 本月维护时间
     */
    Map<String, Object> getAssetRunningTime(MethodParam methodParam);

    /**
     * 查询组织的利用率
     */
    List<Map<String, Object>> getAssetByOrgUtilizationRate(MethodParam methodParam);


    /**-----------------------故障情况-------------------------------------*/
    /**
     * 今日故障数本周故障数本月故障数
     */
    Map<String, Object> getRepairCount(MethodParam methodParam);

    /**
     * 当天修复情况
     */
    List<Map<String, Object>> getTodayRepairSituation(MethodParam methodParam);

    /**
     * 当天完好率
     */
    Map<String, Object> getTodayIntactRate(MethodParam methodParam);

    /**
     * 当天故障分布
     */
    List<Map<String, Object>> getFaultDistribution(MethodParam methodParam);

    /**
     * 故障趋势
     */
    Map<String, Object> getFaultTrend(MethodParam methodParam);

    /**-----------------------维护情况-------------------------------------*/
    /**
     * 今日维护数本周维护数本月维护数
     */
    Map<String, Object> getMainCount(MethodParam methodParam);

    /**
     * 当天设备点检
     */
    List<Map<String, Object>> getTodayEquipmentSpotCheck(MethodParam methodParam);

    /**
     * 当天常规维护
     */
    Map<String, Object> getTodayRoutineMaintenance(MethodParam methodParam);

    /**
     * 当月常规维护
     */
    Map<String, Object> getMonthRoutineMaintenance(MethodParam methodParam);

    /**
     * 维护趋势
     */
    Map<String, Object> getMainTrend(MethodParam methodParam);

    /**
     * 获取设备的故障状态（大屏用）
     *
     * @param methodParam 系统参数
     * @return 设备列表
     */
    List<Map<String, Object>> getAssetRepairStatusForScreen(MethodParam methodParam);
}
