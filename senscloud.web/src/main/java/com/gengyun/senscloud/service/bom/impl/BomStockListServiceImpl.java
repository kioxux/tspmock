package com.gengyun.senscloud.service.bom.impl;

import com.gengyun.senscloud.mapper.BomInStockMapper;
import com.gengyun.senscloud.mapper.BomStockListMapper;
import com.gengyun.senscloud.mapper.StockMapper;
import com.gengyun.senscloud.service.bom.BomStockListService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 备件清单
 * * User: sps
 * * Date: 2019/07/08
 * * Time: 下午15:20
 */
@Service
public class BomStockListServiceImpl implements BomStockListService {
    //    private static final Logger logger = LoggerFactory.getLogger(BomStockListServiceImpl.class);
    @Resource
    BomStockListMapper bslMapper;
    @Resource
    CommonUtilService commonUtilService;
    //    @Autowired
//    AuthService authService;
    @Resource
    LogsService logService;
    //    @Autowired
//    UserService userService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    WorkProcessService workProcessService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//    @Autowired
//    SystemConfigService systemConfigService;
    @Resource
    StockMapper stockMapper;
    @Resource
    BomInStockMapper bomInStockMapper;

//    /**
//     * 获取备件清单列表
//     *
//     * @param request
//     * @param pageNumber 页码(第一页为0)
//     * @param pageSize   每页显示条数
//     * @return
//     */
//    @Override
//    public JSONObject findBslList(String schemaName, HttpServletRequest request, User user, int pageNumber, int pageSize) {
//        JSONObject result = new JSONObject();
//        String[] facilitySearch = {};
//        try {
//            String fs = request.getParameterValues("facilities")[0];
//            if (StringUtils.isNotBlank(fs))
//                facilitySearch = request.getParameterValues("facilities")[0].split(",");
//        } catch (Exception ex) {
//
//        }
//        String keyWord = request.getParameter("keyWord");
//        String searchBomType = request.getParameter("searchBomType");
//        String stockCode = request.getParameter("stockCode");
//        String bomModel = request.getParameter("bomModel");
//        List<Map<String, Object>> dataList = null;
//        //首页条件
//        String condition = "";
//        //根据设备组织查询条件，查询用户可见的库房，用于查询备件
//        String stockPermissionCondition = dataPermissionForFacility.getStockPermissionByFacilityAndGroup(schemaName, user, facilitySearch, "stock", null);
//
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " and (upper(b.bom_code) like upper('%"
//                    + keyWord + "%') or upper(b.bom_name) like upper('%" + keyWord + "%') or b.bom_model like '%"
//                    + keyWord + "%' or b.material_code like '%" + keyWord + "%' )";
//        }
//        if (null != searchBomType && !"".equals(searchBomType) && !"-1".equals(searchBomType)) {
//            condition += " and t.id in(" + searchBomType + ") ";
//        }
//        if (null != bomModel && !"".equals(bomModel) && !"-1".equals(bomModel)) {
//            bomModel=bomModel.replace(",","','");
//            condition += " and b.bom_model in('" + bomModel +"')";
//        }
//        if(StringUtils.isNotBlank(stockCode)){
////            if(stockCode.indexOf("g")>-1){
////                stockCode=stockCode.replace("g","");
////                stockPermissionCondition += String.format(" and sg.group_id = '%s' ", stockCode);
////            }else{
////                stockCode=stockCode.replace("s","");
//                condition += String.format(" and w.stock_code = '%s' ", stockCode);
////            }
//        }
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("bomStockList");
//        //如果分页参数为空
//        if(RegexUtil.isNull(pageSize)){
//            dataList = bslMapper.getBslList(schemaName, condition, stockPermissionCondition, 0, 0, idPrefix);
//            result.put("rows", dataList);
//            return result;
//        }
//        int begin = pageSize * pageNumber;
//        try {
//            dataList = bslMapper.getBslList(schemaName, condition, stockPermissionCondition, pageSize, begin, idPrefix);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        int total = bslMapper.countBslList(schemaName, condition, stockPermissionCondition);
//        //多时区转换
//        SCTimeZoneUtil.responseMapListDataHandler(dataList);
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result;
//    }
//
//    /**
//     * 根据主键查询备件清单数据
//     *
//     * @param schemaName
//     * @param code
//     * @return
//     */
//    @Override
//    public Map<String, Object> queryBslById(String schemaName, String code) {
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("bomStockList");
//        code = code.substring(idPrefix.length());
//        return bslMapper.queryBomStockListById(schemaName, Integer.valueOf(code));
//    }
//
//
//    // 查询库存安全数量过大或过小的备件
//    @Override
//    public List<Map<String, Object>> queryBomOutOfSecurityQuantity(String schemaName) {
//        return bslMapper.queryBomOutOfSecurityQuantity(schemaName);
//    }
//
//
//    /**
//     * 详情信息获取
//     *
//     * @param request
//     * @param url
//     * @return
//     */
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) throws Exception {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        String result = this.findBslList(schemaName, request, user, 0, 1).toString(); // 权限判断
//        if ("".equals(result)) {
//            throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));//权限不足！
//        }
//        String code = request.getParameter("id");
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "bom_stock_list");
//
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_36, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
////        mav.addObject("workRequestCode", workRequestCode);
////        mav.addObject("WorkListDetailModel", wsqDetail);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/bom_stock_list/findSingleBslInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//    /**
//     * 更新备件安全库存
//     *
//     * @param schemaName
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @param pageType
//     * @return
//     */
//    public ResponseModel save(String schemaName, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        // 提交按钮
//        if (null != doFlowKey && !"".equals(doFlowKey) && doFlowKey.equals(String.valueOf(ButtonConstant.BTN_SUBMIT))) {
//            Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schemaName, paramMap);
//            if (null != map_object && map_object.size() > 0) {
//                Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//                Map<String, Object> businessInfo = (Map<String, Object>) dataInfo.get("_sc_bom_stock");
//                String code = (String) businessInfo.get("id");
//                int count = bslMapper.updateBslSqInfoById(schemaName, businessInfo);
//                if (count > 0) {
//                    String idPrefix = Constants.SPECIAL_ID_PREFIX.get("bomStockList");
//                    logService.AddLog(schemaName, pageType, idPrefix + code, selectOptionService.getLanguageInfo(LangConstant.UPDATE_SAFE_STOCK_SUCC), user.getAccount()); // 记录历史;更新安全库存成功
//                    return ResponseModel.ok("ok");
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.UPDATE_SAFE_STOCK_FAIL));//更新安全库存失败
//                }
//            }
//        }
//        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
    /**
     * 验证库房是否存在
     *
     * @param schemaName
     * @param stockList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryStockByCode(String schemaName, List<String> stockList) {
        return bslMapper.queryStockByCode(schemaName, stockList);
    }

    /**
     * 验证备件类型是否存在
     *
     * @param schemaName
     * @param bomTypeList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryBomTypeByCode(String schemaName, List<String> bomTypeList) {
        return bslMapper.queryBomTypeByCode(schemaName, bomTypeList);
    }

    /**
     * 验证库房是否存在
     *
     * @param schemaName
     * @param bomUnitList
     * @return
     */
    @Override
    public Map<String, Map<String, Object>> queryBomUnitByCode(String schemaName, List<String> bomUnitList) {
        return bslMapper.queryBomUnitByCode(schemaName, bomUnitList);
    }

//    /**
//     * 验证备件是否存在
//     *
//     * @param schemaName
//     * @param bomCodeList
//     * @param materialCodeList
//     * @return
//     */
//    public List<String> queryBomByInfo(String schemaName, List<String> bomCodeList, List<String> materialCodeList) {
//        return bslMapper.queryBomByInfo(schemaName, bomCodeList, materialCodeList);
//    }
//
//
//    /**
//     * 验证备件型号是否存在
//     *
//     * @param schemaName
//     * @param bomModelList
//     * @return
//     */
//    public List<String> queryBomModelByCode(String schemaName, List<String> bomModelList) {
//        return bslMapper.queryBomModelByCode(schemaName, bomModelList);
//    }
//
//    /**
//     * 验证备件库存是否存在
//     *
//     * @param schemaName
//     * @param stockCodeList
//     * @return
//     */
//    public List<String> queryBisListByCode(String schemaName, List<String> bomCodeList, List<String> materialCodeList, List<String> stockCodeList) {
//        return bslMapper.queryBisListByCode(schemaName, bomCodeList, materialCodeList, stockCodeList);
//    }
//
//    /**
//     * 获取外部组织
//     *
//     * @param schemaName
//     * @return
//     */
//    public Map<String, Map<String, Object>> querySuppliesList(String schemaName) {
//        return bslMapper.querySuppliesList(schemaName);
//    }
//
//    /**
//     * 获取备件用途
//     *
//     * @param schemaName
//     * @return
//     */
//    public Map<String, Map<String, Object>> queryBomUseList(String schemaName) {
//        return bslMapper.queryBomUseList(schemaName);
//    }
//
//    /**
//     * 导入新备件
//     *
//     * @param schemaName
//     * @param bomData
//     * @param account
//     */
//    public void insertBom(String schemaName, LinkedHashMap bomData, String account) {
//        bslMapper.insertBom(schemaName, bomData, new Timestamp(System.currentTimeMillis()), account);
//    }
//
//    /**
//     * 导入新备件型号
//     *
//     * @param schemaName
//     * @param bomData
//     * @param account
//     */
//    public void insertBomModel(String schemaName, BisExcelMode bomData, String account) {
//        bslMapper.insertBomModel(schemaName, bomData, new Timestamp(System.currentTimeMillis()), account);
//    }
//
//
//
//    /**
//     * 仅校验备件库存数量
//     *
//     * @param schemaName
//     * @param paramMap
//     * @param bomConsumeType
//     * @return
//     * @throws Exception
//     */
//    public synchronized void checkBomStockQuantity(String schemaName, Map<String, Object> paramMap, String bomConsumeType) {
//        try {
//            Map<String, Object> data = null;
//            if (null == bomConsumeType || "1".equals(bomConsumeType)) {
//                data = bslMapper.queryBslIdByInfo(schemaName, paramMap);
//            } else {
//                data = bslMapper.queryReceiverBslByInfo(schemaName, paramMap);
//            }
//            if (null == data || data.size() == 0 || null == data.get("quantity")) {
//                // throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.STOCK_BOM_QUANTITY_INFO), String.valueOf(paramMap.get("bom_code")), "0"));//备件编码：%s，库存数量不足，当前库存数量：%s
//            } else {
//                BigDecimal quantity = new BigDecimal(String.valueOf(data.get("quantity")));
//                BigDecimal quantityPage = new BigDecimal(String.valueOf(paramMap.get("quantity")));
//                quantity = quantity.add(quantityPage);
//                if (quantity.compareTo(new BigDecimal(0)) < 0) {
//                    //  throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.STOCK_BOM_QUANTITY_INFO), String.valueOf(paramMap.get("bom_code")), String.valueOf(data.get("quantity"))));//备件编码：%s，库存数量不足，当前库存数量：%s
//                }
//            }
//        } catch (SenscloudException e) {
//            throw new SenscloudException(e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//            // throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.BOM_STOCK_QUANTITY_WRONG), String.valueOf(paramMap.get("bom_code"))));//备件编码：%s，库存数量有误！
//        }
//    }
//
//
//    /**
//     * 更新备件库存数量
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    public synchronized void updateBomStockQuantity(String schemaName, Map<String, Object> paramMap) {
//        Map<String, Object> data = bslMapper.queryBslIdByInfo(schemaName, paramMap);
//        Integer id = null;
//        String isAddStock = (String) paramMap.get("isAddStock");
//        // 入库、调拨入库新增
//        if ((null == data || data.size() == 0 || null == data.get("id")) && null != isAddStock && "isAddStock".equals(isAddStock)) {
//            List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//            dataList.add(paramMap);
//            commonUtilService.doBatchInsertSql(schemaName, "_sc_bom_stock", SqlConstant._sc_bom_stock_columns, dataList);
//        } else {
//            try {
//                id = (Integer) data.get("id");
//                BigDecimal quantity = new BigDecimal(String.valueOf(data.get("quantity")));
//                BigDecimal quantityPage = new BigDecimal(String.valueOf(paramMap.get("quantity")));
//                quantity = quantity.add(quantityPage);
//                if (quantity.compareTo(new BigDecimal(0)) < 0) {
//                    //   throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.STOCK_BOM_QUANTITY_INFO), String.valueOf(paramMap.get("bom_code")), String.valueOf(data.get("quantity"))));//备件编码：%s，库存数量不足，当前库存数量：%s
//                }
//                paramMap.put("quantity", quantity);
//            } catch (SenscloudException e) {
//                throw new SenscloudException(e.getMessage());
//            } catch (Exception e) {
//                e.printStackTrace();
//                //throw new SenscloudException(String.format(selectOptionService.getLanguageInfo(LangConstant.BOM_STOCK_QUANTITY_WRONG), String.valueOf(paramMap.get("bom_code"))));//备件编码：%s，库存数量有误！
//            }
//            bslMapper.updateBslQuantityByInfo(schemaName, paramMap, id);
//            //根据参数判断是否需要更新库房备件的价格
//            if (paramMap.containsKey("show_price") && RegexUtil.optNotNull(String.valueOf(paramMap.get("show_price"))).isPresent()) {
//                bomInStockMapper.synchronizePrice(schemaName, paramMap);
//            }
//        }
//    }
}
