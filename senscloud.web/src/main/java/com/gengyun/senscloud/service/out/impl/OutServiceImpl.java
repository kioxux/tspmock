package com.gengyun.senscloud.service.out.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.FlowParmsConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.mapper.WorksMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.out.OutService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 对外接口
 */
@Service
public class OutServiceImpl implements OutService {
    private Logger logger = LoggerFactory.getLogger(OutServiceImpl.class);
    @Resource
    WorksMapper worksMapper;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    WorkflowService workflowService;

    /**
     * 传递班组、执行人、设备等信息（中法长寿-智慧上锁-外部接口10）
     *
     * @param methodParam   系统参数
     * @param subWorkCode   工单主键
     * @param nodeId        节点号
     * @param occurTime     发生时间
     * @param relationId    工单对象
     * @param receiveUserId 工单执行人主键
     */
    @Override
    public void outServerAaaAaa(MethodParam methodParam, String subWorkCode, String nodeId, Object occurTime, Object relationId, Object receiveUserId) {
        try {
            RegexUtil.optNotBlankStrOpt(nodeId).ifPresent(nd -> {
                if (Constants.GET_NOW_NODE.equals(nd)) {
                    nd = RegexUtil.optNotBlankStrOpt(subWorkCode).map(mc -> {
                        Map<String, Object> map = new HashMap<>();
                        map.put(FlowParmsConstant.SUB_WORK_CODE, mc);
                        return workflowService.getNodeIdBySubWorkCode(methodParam, map);//获取流程节点ID
                    }).orElse(null);
                }
                String tnd = RegexUtil.optStrOrVal(nd, "tmpNodeId@Xxx");
                String schemaName = methodParam.getSchemaName();
                RegexUtil.optNotBlankStrOpt(systemConfigService.getSysCfgValueByCfgName(schemaName, SystemConfigConstant.OUT_SERVER_AAA_AAA))
                        .filter(srv -> srv.contains(tnd)).ifPresent(srv -> {
                    String assetId = RegexUtil.optNotBlankStrOrExp(relationId, ErrorConstant.CODE_OUT_SERVER_1002);
                    JSONObject jo = new JSONObject();
                    // 设备编号
                    jo.put("equipmentNo", RegexUtil.optStrOrNull(selectOptionService.getSelectOptionAttrByCode(methodParam, "asset", assetId, "relation")));
                    // 预计开始时间-format:date-time
                    String time = RegexUtil.optNotNullMap(worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(schemaName, subWorkCode, "adv_start_tiame"))
                            .map(m -> m.get("field_value")).map(RegexUtil::optStrOrNull).orElseGet(() -> RegexUtil.optStrOrNull(occurTime));
                    SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
                    try {
                        jo.put("expectedStartTime", String.valueOf(sdf.format(sdf.parse(time))));
                    } catch (Exception te) {
                        jo.put("expectedStartTime", time);
                    }
                    // 维修工
                    String receiveUserName = RegexUtil.optNotBlankStrOpt(receiveUserId).map(ru -> selectOptionService.getSelectOptionTextByCode(methodParam, "user", ru)).orElse("");
                    // 班组名称;维修工team_info
                    RegexUtil.optNotNullMap(worksMapper.findWorksDetailColumnBySubWorkCodeAndFieldCode(schemaName, subWorkCode, "team_info"))
                            .map(m -> RegexUtil.optStrOrNull(m.get("field_value"))).map(v -> selectOptionService.getSelectOptionTextByCode(methodParam, "team_info", v)).ifPresent(tn -> {
                        jo.put("groupName", tn + ";" + receiveUserName); // 班组名称;维修工
                        String[] pms = srv.split(Constants.SCD_SPLIT_TYPE);
                        new Thread(() -> logger.info("outServer1001：" + HttpRequestUtils.requestJsonByPost("http://" + pms[0] + ":" + pms[1] + "/zf/coswinWorkOrder/insert", jo.toString()))).start();
                    });
                });
            });
        } catch (SenscloudException se) {
            throw se;
        } catch (Exception e) {
            logger.error(ErrorConstant.CODE_OUT_SERVER_1001, e);
        }
    }
}
