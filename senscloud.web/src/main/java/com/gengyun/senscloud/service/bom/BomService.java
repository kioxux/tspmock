package com.gengyun.senscloud.service.bom;

import com.gengyun.senscloud.model.BomSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/28.
 */
public interface BomService {

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getBomListPermission(MethodParam methodParam);

    /**
     * 获取备件列表
     *
     * @param methodParam 系统参数
     * @param bParam      请求参数
     * @return 列表数据
     */
    Map<String, Object> getBomListForPage(MethodParam methodParam, BomSearchParam bParam);

    /**
     * 添加备件
     *
     * @param methodParam 系统参数
     * @param data        数据信息
     */
    void newBom(MethodParam methodParam, Map<String, Object> data);

    /**
     * 获取备件明细信息
     *
     * @param methodParam 系统参数
     * @param bParam      请求参数
     * @return 备件信息
     */
    Map<String, Object> getBomDetailById(MethodParam methodParam, BomSearchParam bParam);

    /**
     * 获取备件库存信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getBomStockByBomId(MethodParam methodParam, BomSearchParam bParam);

    /**
     * 获取备件供应商信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getBomSupplierInfoByBomId(MethodParam methodParam, BomSearchParam bParam);

    /**
     * 新增备件供应商
     *
     * @param methodParam
     * @param paramMap
     */
    void newBomSupplier(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 修改备件供应商
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyBomSupplier(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除供应商
     *
     * @param methodParam
     * @param bParam
     */
    void cutBomSupplier(MethodParam methodParam, BomSearchParam bParam);

    /**
     * 启禁用备件供应商状态
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyUseBomSupplier(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取备件二维码
     *
     * @param methodParam
     * @param id
     */
    void getBomMaterialQRCode(MethodParam methodParam, String id);

    /**
     * 修改备件
     *
     * @param methodParam
     * @param bParam
     * @param paramMap
     */
    void modifyBom(MethodParam methodParam, BomSearchParam bParam, Map<String, Object> paramMap);

    /**
     * 修改备件启禁用状态
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyUseBom(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除选中备件
     *
     * @param methodParam
     */
    void cutBomByIds(MethodParam methodParam);

    /**
     * 获取备件出入库信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getBomInAndOutList(MethodParam methodParam, BomSearchParam param);

    /**
     * 根据物料编码查询已存在的备件
     *
     * @param schemaName
     * @param materialCodes
     * @return
     */
    Map<String, Map<String, Object>> queryBomByMaterialCodes(String schemaName, List<String> materialCodes);

    /**
     * 获取备件明细清单信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getBomDetailedList(MethodParam methodParam, BomSearchParam bParam);

    /**
     * 备件入库
     *
     * @param methodParam
     * @param bomList
     * @return
     */
    void bomInPutStock(MethodParam methodParam, List<Map<String, Object>> bomList);

    /**
     * 备件出库
     *
     * @param methodParam
     * @param bomList
     * @return
     */
    void bomOutPutStock(MethodParam methodParam, List<Map<String, Object>> bomList);

    /**
     * 更新备件库存
     *
     * @param methodParam
     * @param bomList
     * @param isAdd       1增加库存 -1 减少库存 3 减库存支持负数
     */
    void updateBomStock(MethodParam methodParam, List<Map<String, Object>> bomList, Integer isAdd);

    /**
     * 查询库存低于安全库存的备件库存信息
     *
     * @param schemaName
     * @return
     */
    List<Map<String, Object>> getBomLowerSecurityQuantityList(String schemaName);

    /**
     * 更新备件库存发送信息时间
     */
    void updateBomStockSendMsgTime(@Param("schemaName") String schemaName, @Param("id") Integer id);

    /**
     * 更新库房发送信息时间
     */
    void updateStockSendMsgTime(@Param("schemaName") String schemaName, @Param("id") Integer id);

    /**
     * 批量新增、修改备件信息
     * @param schemaName
     * @param bomList
     */
    void batchInsertBom(String schemaName, List<Map<String, Object>> bomList);

    /**
     *  批量新增，修改备件库存
     * @param schemaName
     * @param bomStockList
     */
    void batchInsertOrUpdateBomStock(String schemaName, List<Map<String, Object>> bomStockList);

    /**
     *  批量新增，修改备件导入记录
     * @param schemaName
     * @param bomImportList
     */
    void batchInsertBomImport(String schemaName, List<Map<String, Object>> bomImportList);


    /**
     * 批量新增，更新备件库存明细
     * @param schemaName
     * @param bomStockDetailList
     */
    void batchInsertOrUpdateBomStockDetail(String schemaName, List<Map<String, Object>> bomStockDetailList);

    /**
     * 根据主键展示备件二维码（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    void doShowBomQRCode(MethodParam methodParam);

    /**
     * 根据选中主键下载备件二维码
     *
     * @param methodParam 系统参数
     * @param bParam     请求参数
     */
    void doExportBomQRCode(MethodParam methodParam, BomSearchParam bParam);


//    //新增备件
//    int insertBom(String schema_name, BomData bomData);
//
//    //禁用启用备件
//    int disableBom(String schema_name, int id, boolean isuse);
//
//    //删除备件
//    int deleteBom(String schema_name, int id);
//
//    //查询备件列表
//    List<BomData> findAllForPage(String schema_name, int pageSize, int page);
//
//    //PageResult findAll(String schema_name, int page, int pageSize);
//
//    //更新备件信息
//    int updateBom(String schema_name, BomData bomData);
//
//    //更新备件的数量
//    int updateBomCount(String schema_name, String bomModel, String bomCode, String stockCode, int facilityId, float useCount);
//
//    //根据ID查找备件信息
//    BomData findById(String schema_name, int id);
//
//    //根据位置id查询库房信息
//    List<StockData> getBomName(String schema_name, Integer facility_id);
//
////    //获取备件列表
////    List<BomData> getBomList(String schema_name, String condition, int pageSize, int begin);
//
//    //按库房，获取备件列表
//    List<BomData> getBomListByStock(String schema_name, String stockCode);
//
//    /**
//     * 按库房，获取有权限的备件列表
//     *
//     * @param schemaName
//     * @param request
//     * @param pageNumber
//     * @param pageSize
//     * @return
//     */
//    net.sf.json.JSONObject getBomListByStockWithPage(String schemaName, HttpServletRequest request, int pageNumber, int pageSize);
//
//    List<BomData> findAll(String schema_name);
//
//    List<BomData> findByAny(String schema_name, String bomname);
//
//    //按位置，查询所有的备件信息，不重复
//    List<BomData> getBomDataDistinctByFacility(String schema_name, String facilityIds);
//
//    //查询所有的备件信息，不重复
//    List<BomData> getBomDataDistinctDistinct(String schema_name);
//
//    //获取单位
//    List<UnitData> getUnit(String schema_name);
//
////    /**
////     * 核查在备件添加的时候监测同库下是否存在相同设备型号和设备编号
////     *
////     * @param schema_name
////     * @param bom_model
////     * @param stock_code
////     * @return
////     */
////    int checkAddBom2(String schema_name, String bom_model, String bom_code, String stock_code);
//
//    /**
//     * 校验备件编码唯一性
//     *
//     * @param schema_name
//     * @param bom_code
//     * @return
//     */
//    int checkBomCode(String schema_name, String bom_code);
//
//    /**
//     * 校验物料编码唯一性
//     *
//     * @param schema_name
//     * @param material_code
//     * @return
//     */
//    int checkMaterialCode(String schema_name, String material_code);
//
//    int getBomListCount(String schema_name, String condition);
//
//    //根据物料编号查询备件信息
//    BomData findByMaterial_code(String schema_name, String material_code);
//
//    //获取备件类型
//    List<BomType> getBomType(String schema_name);
//
//    //新增备件类型
//    int insertBomType(String schema_name, BomType bomType);
//
//    //修改备件类型
//    int updateBomType(String schema_name, BomType bomType);
//
//    //启用、禁用备件类型
//    int disableBomType(String schema_name, Integer id, String isUse);
//
//    //根据id，找到备件类型
//    BomType findBomTypeById(String schema_name, Integer id);
//
//    /**
//     * 按父位置id，查询其最大的子位置编号
//     */
//    String getMaxBomTypeCodeByParentId(String schema_name, Integer parentId);
//
//    /**
//     * 按条件查询备件列表
//     *
//     * @param request
//     * @param schema_name
//     * @param isSelf      //是否只查询自己权限下的库房备件
//     * @return
//     */
//    String searchList(HttpServletRequest request, String schema_name, boolean isSelf);
//
//    /**
//     * 按条件查询备件（支持按库房查询）
//     *
//     * @param request
//     * @param schema_name
//     * @return
//     */
//    String searchBomList(HttpServletRequest request, String schema_name);
//
//    /**
//     * 按条件查询备件（支持按人员查询个人库存）
//     *
//     * @param request
//     * @return
//     */
//    String getReceiverBomList(HttpServletRequest request);
//
//    /**
//     * 按条件查询设备备件
//     *
//     * @param paramMap
//     * @return
//     * @throws Exception
//     */
//    List<Map<String, Object>> getAssetBomList(Map<String, Object> paramMap) throws Exception;
//
//    /**
//     * 通过备件模板id，获取模板信息
//     *
//     * @param schema_name
//     * @param bomModel
//     * @return
//     */
//    BomData getBomTemplateById(String schema_name, String bomModel);
//
//    /**
//     * 新增备件附件
//     *
//     * @param schema_name
//     * @param bomDoc
//     * @return
//     */
//    int insertBomDoc(String schema_name, BomDoc bomDoc);
//
//    /**
//     * 删除备件附件
//     *
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    int deleteDoc(String schema_name, int id);
//
//    /**
//     * 修改备件附件
//     *
//     * @param schema_name
//     * @param bom_code
//     * @param material_code
//     */
//    void updateDocBomCode(String schema_name, String ids, String bom_code, String material_code);
//
//    /**
//     * 获取备件附件
//     *
//     * @param schema_name
//     * @param bom_code
//     * @param material_code
//     * @return
//     */
//    List<BomDoc> docListByBomCodeAndMaterialCode(String schema_name, String bom_code, String material_code);
//
//    /**
//     * 通过备件编码、物料编码查询备件维修记录
//     *
//     * @param schema_name
//     * @param bom_code
//     * @return
//     */
//    List<Map<String, Object>> getBomRepairList(String schema_name, String bom_code, String material_code, Integer businessTyp);
//
//    /**
//     * 通过备件编码、查询费用合计等聚合
//     *
//     * @param schemaName
//     * @param bomCode
//     * @param materialCode
//     * @param businessTyp
//     * @return
//     */
//    Map<String, Object> getBomWorkListPolymeric(String schemaName, String bomCode, String materialCode, Integer businessTyp);
//
//    /**
//     * 通过备件编码、物料编码查询备件保养记录
//     *
//     * @param schema_name
//     * @param bom_code
//     * @return
//     */
//    List<Map<String, Object>> getBomMaintainList(String schema_name, String bom_code, String material_code);
//
//    List<Map<String, Object>> queryBomAssetModelList(String schema_name, String bom_code, String material_code);
//
//    /**
//     * 通过备件编码、物料编码查询相关人员
//     *
//     * @param schema_name
//     * @param bom_code
//     * @param material_code
//     * @return
//     */
//    List<Map<String, Object>> queryBomDutyManList(String schema_name, String bom_code, String material_code);
//
//    /**
//     * 删除关联人员
//     *
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    int deleteBomDutyMan(String schema_name, int id);
//
//    /**
//     * 添加关联人员
//     *
//     * @param schema_name
//     * @param bomDutyMan
//     * @return
//     */
//    int insertDutyMan(String schema_name, BomDutyMan bomDutyMan);
//
//    /**
//     * 备件导出
//     *
//     * @param request
//     * @param schemaName
//     * @return
//     */
//    ModelAndView exportBom(HttpServletRequest request, String schemaName);
//
//
//    /**
//     * 备件导出二维码
//     *
//     * @param request
//     * @param schemaName
//     * @return
//     */
//    void exportBomQRCode(HttpServletRequest request, String schemaName);
}
