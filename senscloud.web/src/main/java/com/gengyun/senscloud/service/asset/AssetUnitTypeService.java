package com.gengyun.senscloud.service.asset;

/**
 * 地址管理
 * User: sps
 * Date: 2018/11/20
 * Time: 上午11:20
 */
public interface AssetUnitTypeService {
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @return
//     */
//    JSONObject query_modal(String schemaName, String searchKey, Integer pageSize, Integer pageNumber, String sortName,
//                           String sortOrder);
//
//    /**
//     * 新增数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    ResponseModel insert_modal(String schemaName, Map<String, Object> paramMap);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    ResponseModel update_model(String schemaName, Map<String, Object> paramMap);
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    ResponseModel delete_model(String schemaName, HttpServletRequest request);
//
//    /**
//     * 查询bom列表
//     *
//     * @param schemaName
//     * @param model_id
//     * @return
//     */
//    JSONObject query_modal_bom(String schemaName, String model_id);
//
//    /**
//     * 新增bom数据
//     *
//     * @param schemaName
//     * @param paramMaps
//     * @param user
//     * @return
//     */
//    ResponseModel insert_modal_bom(String schemaName, List<Map<String, Object>> paramMaps, User user, String model_id);
//
//
//    /**
//     * 删除bom数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    ResponseModel delete_model_bom(String schemaName, Integer id);
//
//    /**
//     * 更新设备型号的备件周期
//     *
//     * @param schemaName
//     * @return
//     */
//    ResponseModel updateAssetModelBomCycle(String schemaName, Integer id, float useDays, float maintenanceDays, int unitType);
//
//    /**
//     * 查询bom列表
//     *
//     * @param schemaName
//     * @param model_id
//     * @return
//     */
//    JSONObject query_modal_doc(String schemaName, String model_id);
//
//    /**
//     * 新增doc数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    ResponseModel insert_modal_doc(String schemaName, Map<String, Object> paramMap);
//
//
//    /**
//     * 删除doc数据
//     *
//     * @param schemaName
//     * @param
//     * @param file_id
//     * @return
//     */
//    ResponseModel delete_model_doc(String schemaName, Integer file_id);
//
//    /**
//     * 查询bom列表
//     *
//     * @param schemaName
//     * @param model_id
//     * @return
//     */
//    JSONObject query_modal_customer(String schemaName, String model_id);
//
//    /**
//     * 新增customer数据
//     *
//     * @param schemaName
//     * @param paramMaps
//     * @param user
//     * @return
//     */
//    ResponseModel insert_modal_customer(String schemaName, List<Map<String, Object>> paramMaps, User user, String model_id);
//
////    /**
////     * 新增bom part
////     *
////     * @param schemaName
////     * @param bom_id
////     * @param part_id
////     * @return
////     */
////    ResponseModel update_model_bom_part(String schemaName, Long bom_id,Long part_id);
//
//    /**
//     * 删除customer数据
//     *
//     * @param schemaName
//     * @param id
//     * @param
//     * @return
//     */
//    ResponseModel delete_model_customer(String schemaName, Integer id);
//
//    /**
//     * 查找所有的bomlist
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    JSONObject findBomList(String schemaName, HttpServletRequest request);
//
//    /**
//     * 查找所有的facilities
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    JSONObject findFacilitiesList(String schemaName, HttpServletRequest request);
//
//
//    Map<String, Object> find_model_by_id(String schemaName, Integer id);
//
//    List<Map<String, Object>> find_model_list(String schemaName);
//
//    //设备型号，新增工单类型的任务模板
//    int addAssetModelTaskTemplate(String schema_name, AssetUnitTypeTaskTemplateData data);
//
//    //查询设备型号的工单类型的任务模板
//    List<AssetUnitTypeTaskTemplateData> getAssetModelTaskTemplate(String schema_name, Integer model_id);
//
//    //删除设备型号的工单类型的任务模板
//    int deletaAssetModelTaskTemplate(String schema_name, Integer model_id, int workTypeId, String taskTemplateCode);
}
