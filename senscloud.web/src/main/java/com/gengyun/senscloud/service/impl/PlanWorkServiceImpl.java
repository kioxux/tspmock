package com.gengyun.senscloud.service.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.mapper.*;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.model.PlanWorkSettingModel;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.service.BussinessConfigService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.util.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能：计划工单serviceImpl
 * Created by Dong wudang on 2018/11/24.
 */
@Service
public class PlanWorkServiceImpl implements PlanWorkService {

    @Resource
    PagePermissionMapper pagePermissionMapper;
    @Resource
    LogsService logService;
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    BussinessConfigService bussinessConfigService;
    @Resource
    PlanWorkMapper planWorkMapper;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    WorkFlowTemplateService workFlowTemplateService;
    @Resource
    AssetMapper assetMapper;
    @Resource
    TaskTemplateMapper taskTemplateMapper;
    @Resource
    TaskItemMapper taskItemMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    TaskItemServiceImpl taskItemService;
    @Resource
    CacheUtilService cacheUtilService;


    /**
     * 获取设备位置树列表-带用户权限查询
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getAssetPositionTree(MethodParam methodParam, SearchParam param) {
//        String condition = this.getTreeWhereString(methodParam, param);
        List<Map<String, Object>> assetPositionTree = pagePermissionMapper.findAssetPositionTree(methodParam.getSchemaName(), methodParam.getUserId(), param);
        if(RegexUtil.optIsPresentList(assetPositionTree)){
            for (Map<String, Object> map : assetPositionTree) {
                map.put("schema_name", methodParam.getSchemaName());
            }
        }
        return assetPositionTree;
    }

    /**
     * 新建、编辑维保计划功能
     *
     * @param methodParam
     * @param data
     * @param isEditor
     * @return
     */
    @Override
    public void newPlanSchedule(MethodParam methodParam, Map<String, Object> data, boolean isEditor) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_AG_Y}); // 计划不能为空
        Timestamp nowTime = SenscloudUtil.getNowTime();
        String plan_code = "";
        String schema_name = methodParam.getSchemaName();
        if (isEditor) {
            plan_code = data.get("plan_code").toString();
        } else {
            plan_code = serialNumberService.generateMaxBsCodeByType(methodParam, "plan_work");
            data.put("plan_code", plan_code);
        }
        String plan_name = RegexUtil.optStrOrExpNotNull(data.get("plan_name"), LangConstant.TITLE_NAME_A);// 计划名称不能为空
        String jsonArrObject = RegexUtil.optStrOrExpNotNull(data.get("maintenanceObject"), LangConstant.TITLE_AAW_I);// 维保计划不能为空
        String triggleInfo = RegexUtil.optStrOrVal(data.get("triggleInfo"), "[]");
        String conditionTriggle = RegexUtil.optStrOrVal(data.get("conditionTriggle"), "[]");
        if("[]".equals(triggleInfo)&&"[]".equals(conditionTriggle)){
            throw new SenscloudException(LangConstant.MSG_BV);//请设置计划触发条件
        }
//        String triggleInfo = RegexUtil.optStrOrExpNotNull(data.get("triggleInfo"), LangConstant.TITLE_ABBBB_W);// 触发条件不能为空
        String assistList = RegexUtil.optStrOrExpNotNull(data.get("assistList"), LangConstant.TITLE_BA_Z);// 参与团队不能为空
        String is_use = RegexUtil.optStrOrExpNotNull(data.get("is_use"), LangConstant.TITLE_AAL_L);// 启用不能为空
        String priority_level = RegexUtil.optStrOrExpNotNull(data.get("priority_level"), LangConstant.TITLE_AL_V);// 紧急程度不能为空
        String begin_time = RegexUtil.optStrOrExpNotNull(data.get("begin_time"), LangConstant.TITLE_AAL_L);// 启用不能为空
        String end_time = RegexUtil.optStrOrExpNotNull(data.get("end_time"), LangConstant.TITLE_AAL_L);// 启用不能为空
        int work_type_id = RegexUtil.optSelectOrExpParam(data.get("work_type_id"), LangConstant.TITLE_CATEGORY_C);// 工单类型不能为空
        int auto_generate_bill = RegexUtil.optSelectOrExpParam(data.get("auto_generate_bill"), LangConstant.TITLE_ALM);// 工单类型不能为空
        int generate_time_point = Integer.valueOf(RegexUtil.optStrOrVal(data.get("generate_time_point"), "0"));// 自动生成工单触发时间点（小时）
        int do_type = RegexUtil.optSelectOrExpParam(data.get("do_type"), LangConstant.MSG_BV);// 请设置计划触发条件不能为空
        Integer isCreateSchedule = RegexUtil.optSelectOrExpParam(data.get("isCreateSchedule"),"是否生成计划必填");
//        Integer isCreateSchedule = 1;
//        int allocate_receiver_type = RegexUtil.optSelectOrExpParam(data.get("allocate_receiver_type"), LangConstant.MSG_BV);// 请设置计划触发条件不能为空
        RegexUtil.optNotNullOrExpNullInfo(bussinessConfigService.getSystemConfigInfoById(methodParam, "_sc_work_type", work_type_id), LangConstant.TITLE_CATEGORY_C); // 工单类型不存在
//        data.put("begin_time", begin_time + " 00:00:00");
//        data.put("end_time", end_time + " 00:00:00");
        data.put("auto_generate_bill",auto_generate_bill);
        data.put("generate_time_point", generate_time_point);
//        int allocate_receiver_type = Integer.parseInt(request.getParameter("allocate_receiver_type"));
//        int generate_bill_type = Integer.parseInt(request.getParameter("generate_bill_type"));
//        int relation_type = RegexUtil.optSelectOrExpParam(data.get("relation_type"), LangConstant.TITLE_AAT_A);// 适用对象不能为空
//        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schema_name, "relationTypes", relation_type+""), LangConstant.TITLE_AAT_A); // 适用对象不存在
//        int pre_day = Integer.parseInt(request.getParameter("pre_day"));
//        String facilityFilter = request.getParameter("facilityFilter");
//        String jsonArrTaskList = data.get("taskList").toString();//任务list
//
//
//        String jsonArrObject = data.get("maintenanceObject").toString();//任务list
//        String assistList = data.get("assistList").toString();//协助团队list
//        String triggleInfo = data.get("triggleInfo").toString();//触发条件

//        Map<String, Object> jsonbTaskList = (Map<String, Object>) JSON.parse(jsonArrTaskList);
        List<Map<String, Object>> jsonAssistList = (List<Map<String, Object>>) JSON.parse(assistList);
        List<Map<String, Object>> jsonArrObjectList = (List<Map<String, Object>>) JSON.parse(jsonArrObject);
        JSONArray jsonObjectArray = new JSONArray(jsonArrObject);
        List<Map<String, Object>> triggleInfoList = (List<Map<String, Object>>) JSON.parse(triggleInfo);
        List<Map<String, Object>> conditionTriggleList = (List<Map<String, Object>>) JSON.parse(conditionTriggle);
        RegexUtil.optListOrExpNullInfo(jsonArrObjectList, LangConstant.TITLE_AAW_I);//维保对象
//        RegexUtil.optListOrExpNullInfo(triggleInfoList, LangConstant.TITLE_ABBBB_W); //触发条件
        for (int i = 0; i < triggleInfoList.size(); i++) {
            Map<String, Object> map = triggleInfoList.get(i);
            int period_type = RegexUtil.optSelectOrExpParam(map.get("period_type"), LangConstant.TITLE_ACE);// 周期类型不能为空
            map.put("period_type", period_type);
            RegexUtil.optStrOrExpNotNull(map.get("duration_time"), LangConstant.TITLE_ACF);// 持续时间不能为空
            float duration_time = (map.get("duration_time") == null || !RegexUtil.optIsPresentStr(map.get("duration_time"))) ? 0 : Float.parseFloat(map.get("duration_time").toString());
            map.put("duration_time", duration_time);
            String duration_type = RegexUtil.optStrOrExpNotNull(map.get("duration_type"), LangConstant.TITLE_ACF);// 持续时间类型不能为空
            map.put("duration_type", duration_type);
            float period_value = (map.get("period_value") == null || !RegexUtil.optIsPresentStr(map.get("period_value"))) ? 0 : Float.parseFloat( map.get("period_value").toString());
            map.put("period_value", period_value);
            if (Constants.TRIGGER_PERIOD_TYPE_SINGLE == period_type) {
                RegexUtil.optStrOrExpNotNull(map.get("execute_day"), LangConstant.TITLE_DATE_T);// 日期不能为空
                Date execute_day = RegexUtil.changeToDate(map.get("execute_day").toString(), LangConstant.TITLE_DATE_T, Constants.DATE_FMT);
                map.put("execute_day", execute_day);
            } else {
                RegexUtil.optStrOrExpNotNull(map.get("period_value"), LangConstant.TITLE_LN);// 周期不能为空
                if (Constants.TRIGGER_PERIOD_TYPE_WEEK == period_type) {
                    RegexUtil.optStrOrExpNotNull(map.get("type_week_select"), LangConstant.TITLE_LP);// 周期频率不能为空
                } else if (Constants.TRIGGER_PERIOD_TYPE_MONTH == period_type || Constants.TRIGGER_PERIOD_TYPE_YEAR == period_type) {
                    RegexUtil.optStrOrExpNotNull(map.get("type_day_select"), LangConstant.TITLE_LO);// 周期模式不能为空
                    RegexUtil.optStrOrExpNotNull(map.get("which_type_day"), LangConstant.TITLE_LN);// 周期不能为空
                }
            }
            map.put("which_type_day", RegexUtil.optIntegerOrVal(map.get("which_type_day"), 0, LangConstant.TITLE_LN));
            map.put("type_day_select", RegexUtil.optStrOrBlank(map.get("type_day_select")));
            map.put("type_week_select", RegexUtil.optStrOrBlank(map.get("type_week_select")));

            String execute_hour = RegexUtil.optStrOrExpNotNull(map.get("execute_hour"), LangConstant.MSG_AT);// 时，执行不能为空
            String execute_minute = RegexUtil.optStrOrExpNotNull(map.get("execute_minute"), LangConstant.TITLE_AE_D);// 分不能为空
            map.put("execute_time", execute_hour + ":" + execute_minute + ":00");
        }
        for (Map<String, Object> conTriggle : conditionTriggleList) {
            String column_value = RegexUtil.optStrOrExpNotNull(conTriggle.get("column_value"), LangConstant.TITLE_ADM);// 条件不能为空
            if(column_value.endsWith("%")){
                column_value = "0." + column_value.substring(0, column_value.length() - 1);
                    RegexUtil.falseExp(RegexUtil.isNumeric(column_value), LangConstant.TEXT_AF, new String[]{LangConstant.TITLE_ADM, LangConstant.TITLE_ADM, "百分数"}); // {d}【{0}】格式错误，应该是{1}格式{d}
            }else{
                    RegexUtil.falseExp(RegexUtil.isNumeric(column_value), LangConstant.TEXT_AF, new String[]{LangConstant.TITLE_ADM, LangConstant.TITLE_ADM, LangConstant.TITLE_AAT_X}); // {d}【{0}】格式错误，应该是{1}格式{d}
            }
        }
        int executor = 0;
        for (Map<String, Object> assist : jsonAssistList) {
            if(RegexUtil.optEquals(assist.get("duty_type"),Constants.PLAN_WORK_DUTY_EXECUTOR)){
                executor ++;
            }
        }
        RegexUtil.trueExp(executor > 1, LangConstant.MSG_H, new String[]{LangConstant.TITLE_AF}); // 执行人重复，请修改
        Map<String, Object> workFlowInfo = workFlowTemplateService.getWorkFlowNodeTemplateInfoByWorkTypeId(methodParam, work_type_id);//流程信息
        Integer relation_type = Integer.valueOf(workFlowInfo.get("relation_type").toString());
        String work_template_code = workFlowInfo.get("work_template_code").toString();
        data.put("relation_type", relation_type);
        data.put("work_template_code", work_template_code);
        if (jsonAssistList != null && jsonAssistList.size() > 0) {
            Float total_hour = 0f;
            for (Map<String, Object> map : jsonAssistList) {
                if (RegexUtil.optIsPresentStr(map.get("total_hour"))) {
                    total_hour += Float.parseFloat(map.get("total_hour").toString());
                }
            }
            data.put("plan_hour", total_hour);
        }
        net.sf.json.JSONArray log = new net.sf.json.JSONArray();
        //保存维保计划
        int doWrite = 0;
        data.put("schema_name", schema_name);
        if (isEditor) {
            Map<String, Object> planScheduleInfo = this.getPlanScheduleInfo(methodParam, plan_code);
            if (RegexUtil.optIsPresentStr(planScheduleInfo.get("job_status")) && planScheduleInfo.get("job_status").equals(StatusConstant.PENDING)) {
                throw new SenscloudException(LangConstant.MSG_D);//维保计划正在生成行事历，请稍后修改
            }
            //清空计划配置、设备对象、位置、设备类型、设备型号、触发条件等数据
            cutOldPlanScheduleData(schema_name, plan_code);
            //更新计划
            Integer job_status= (Integer)planScheduleInfo.get("job_status");
            if(1==isCreateSchedule){
                //需要生成计划
                if(StatusConstant.PLAN_WORK_JOB_STATUS_DRAFT == job_status){
                    data.put("job_status", StatusConstant.PLAN_WORK_JOB_STATUS_NEW);
                }else {
                    data.put("job_status", StatusConstant.PLAN_WORK_JOB_STATUS_MODIFIED);
                }
            }else{
                //不需要
                if(StatusConstant.PLAN_WORK_JOB_STATUS_DRAFT == job_status){
                    data.put("job_status", StatusConstant.PLAN_WORK_JOB_STATUS_DRAFT);
                }else{
                    data.put("job_status", StatusConstant.PLAN_WORK_JOB_STATUS_DEPOSIT);
                }
            }
            doWrite = modifyPlanSchedule(data);
            if (RegexUtil.optNotNull(planScheduleInfo).isPresent()) {
                log = LangUtil.compareMap(data, planScheduleInfo);
            }
        } else {
            //新增计划
            if(1==isCreateSchedule){
                data.put("job_status", StatusConstant.PLAN_WORK_JOB_STATUS_NEW);
            }else{
                data.put("job_status", StatusConstant.PLAN_WORK_JOB_STATUS_DRAFT);
            }
            data.put("create_time", nowTime);
            data.put("create_user_id", methodParam.getUserId());
            data.put("status", StatusConstant.STATUS_ENABLE);
            doWrite = addPlanSchedule(data);
        }
        final String planCode = plan_code;
        RegexUtil.optNotBlankStrOpt(data.get("planJson")).ifPresent(pj -> {
            planWorkMapper.insertPlanJson(schema_name, planCode, pj);
        });
        if (doWrite > 0) {
            //计划触发条件
            if (triggleInfoList != null && triggleInfoList.size() > 0) {
                boolean status = addPlanWorkTriggleList(schema_name, plan_code, triggleInfoList);
            }
            if (conditionTriggleList != null && conditionTriggleList.size() > 0) {
                boolean status = addPlanWorkConditionTriggleList(schema_name, plan_code, conditionTriggleList);
            }
            if (jsonAssistList != null && jsonAssistList.size() > 0) {
                addPlanWorkAssistList(schema_name, plan_code, jsonAssistList);
            }
            //保存维保设备
            for (int k = 0; k < jsonObjectArray.length(); k++) {
                //保存维保计划配置信息
                PlanWorkSettingModel planWorkSetting = new PlanWorkSettingModel();
                planWorkSetting.setPlan_code(plan_code);
                planWorkSetting.setSchema_name(schema_name);
                long result = addPlanWorkSetting(planWorkSetting);
                if (result > 0 && planWorkSetting.getId() > 0) {
                    JSONObject jsonObject = jsonObjectArray.getJSONObject(k);
                    String treeIds = jsonObject.getString("position_codes");
                    if (Constants.RELATION_TYPE_POSITION.equals(relation_type)) {
                        //保存位置（如果没有选择任何组织，则记录为0）
                        boolean positionStatus = addPlanWorkFacilityPosition(schema_name, plan_code, planWorkSetting.getId(), treeIds, true, null, null, false);
                    } else if (Constants.RELATION_TYPE_ASSET.equals(relation_type)) {
                        String assetTypeCategories = jsonObject.optString("asset_category_id","");
                        String assetModels = jsonObject.optString("asset_model_id","");
                        boolean selectAsset = jsonObject.optBoolean("is_all_asset");
                        String beginDate = jsonObject.optString("enable_begin_date");
                        String endDate = jsonObject.optString("enable_end_date");
                        String assetInfo = jsonObject.optString("assets");

                        //保存位置（如果没有选择任何组织，则记录为0）
                        boolean positionStatus = addPlanWorkFacilityPosition(schema_name, plan_code, planWorkSetting.getId(), treeIds, selectAsset, beginDate, endDate, false);
                        //保存设备类型
                        boolean assetTypeStatus = addPlanWorkAssetType(schema_name, plan_code, planWorkSetting.getId(), assetTypeCategories);
                        //保存设备型号
                        boolean modelStatus = addPlanWorkAssetModel(schema_name, plan_code, planWorkSetting.getId(), assetModels);
                        boolean assetStatus = false;
                        if (!selectAsset) {
                            //保存设备
                            String[] split = assetInfo.split(",");
                            for (String asset_id : split) {
                                int query = addPlanWorkAsset(schema_name, plan_code, planWorkSetting.getId(), asset_id);
                                if (query > 0) {
                                    assetStatus = true;
                                }
                            }
                        } else {
                            assetStatus = true;
                        }
                    } else if (Constants.RELATION_TYPE_AREA.equals(relation_type)) {
                        JSONArray assetPositionArray = jsonObject.getJSONArray("assets");
                        //保存位置（如果没有选择任何组织，则记录为0）
                        boolean positionStatus = addPlanWorkFacilityPosition(schema_name, plan_code, planWorkSetting.getId(), treeIds, false, null, null, false);
                        //保存点巡检点位置
                        boolean assetPositionStatus = false;
                        if (assetPositionArray != null && assetPositionArray.length() > 0) {
                            for (int i = 0; i < assetPositionArray.length(); i++) {
                                JSONObject assetPositionObject = assetPositionArray.getJSONObject(i);
                                int query = addPlanWorkInspection(schema_name, plan_code, planWorkSetting.getId(), assetPositionObject.getString("position_code"));
                                if (query > 0) {
                                    assetPositionStatus = true;
                                }
                            }
                        }
                    }
                } else {
                }
            }
            if (isEditor) {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_10002, data.get("plan_code").toString(), LangUtil.doSetLogArray("维保计划编辑", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TAB_PLAN_A, log.toString()}));
            } else {
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_10002, data.get("plan_code").toString(), LangUtil.doSetLogArray("维保计划新建", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TAB_PLAN_A, LangConstant.TITLE_AG_Y}));
            }
        } else {
        }
    }

    private boolean addPlanWorkConditionTriggleList(String schema_name, String plan_code, List<Map<String, Object>> conditionTriggles) {
        Boolean status = false;
        for (int i = 0; i < conditionTriggles.size(); i++) {
            Map<String, Object> map = conditionTriggles.get(i);
            map.put("schema_name", schema_name);
            map.put("plan_code", plan_code);
            map.put("execute_time","00:00:00");
            map.put("period_type",0);
            int query = addPlanWorkTriggle(map);
        }
        return status;


    }

    @Override
    public Map<String, Object> getPlanScheduleInfo(MethodParam methodParam, String plan_code) {
//        Map<String, Object> planWorkMap = new HashMap<>();
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> planWorkMap = this.getPlanScheduleInfoByCode(methodParam.getSchemaName(), plan_code);//查询计划基本信息
//        List<Map<String, Object>> planWorkSettingList = this.getPlanWorkSettingByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划配置信息
//        List<Map<String, Object>> assetList = this.getPlanWorkAssetByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划设备对象信息
//        List<Map<String, Object>> positionList = this.getPlanWorkFacilityPositionByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划位置信息
//        List<Map<String, Object>> assetTypeList = this.getPlanWorkAssetTypeByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划设备类型信息
//        List<Map<String, Object>> assetModelList = this.getPlanWorkAssetModelByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划设备型号信息
        List<Map<String, Object>> triggleInfo = this.getPlanWorkTriggleByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划触发条件
        List<Map<String, Object>> conditionTriggleInfo = this.getPlanWorkConditionTriggleByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划触发条件
//        List<Map<String, Object>> inspectionList = this.getPlanWorkInspectionByPlanCode(methodParam.getSchemaName(), plan_code);//查询点巡检点信息
//        List<Map<String, Object>> executorList = this.getPlanWorkExecutorByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划执行人列表
//        List<Map<String, Object>> assistOrgList = this.getPlanWorkAssistOrgByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划协助团队列表
        List<Map<String, Object>> maintenanceObjectList = planWorkMapper.findMaintenanceObjectByPlanCode(schemaName, plan_code);//查询计划维保对象信息列表
        List<Map<String, Object>> assistList = cacheUtilService.findAssistByPlanCode(schemaName, plan_code,methodParam.getUserLang(),methodParam.getCompanyId());//查询计划参与团队
        Map<String, Object> property = this.getPlanWorkPropertyByPlanCode(methodParam.getSchemaName(), plan_code);//查询计划自定义配置
        if (RegexUtil.optIsPresentMap(property)) {
            String body_property = property.get("body_property").toString();
            com.alibaba.fastjson.JSONObject jsonObject = JSON.parseObject(body_property);
            planWorkMap.put("planJson", jsonObject);
        } else {
            planWorkMap.put("planJson", new HashMap<>());
        }

//        com.alibaba.fastjson.JSONArray task_list = jsonObject.getJSONArray("task_list");

//        planWorkMap.put("planWorkSettingList", planWorkSettingList);
//        planWorkMap.put("inspectionList", inspectionList);
//        planWorkMap.put("executorList", executorList);
//        planWorkMap.put("assistOrgList", assistOrgList);
//        planWorkMap.put("assetList", assetList);
//        planWorkMap.put("positionList", positionList);
//        planWorkMap.put("assetTypeList", assetTypeList);
//        planWorkMap.put("assetModelList", assetModelList);
//        planWorkMap.put("triggleInfo", triggleInfo);
        planWorkMap.put("maintenanceObject", maintenanceObjectList);
        planWorkMap.put("assistList", assistList);
        planWorkMap.put("triggleInfo", triggleInfo);
        planWorkMap.put("conditionTriggle", conditionTriggleInfo);


        return planWorkMap;
    }

    /**
     * 查询计划条件的触发条件
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkConditionTriggleByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkConditionTriggleByPlanCode(schemaName, plan_code);
    }

    @Override
    public Map<String, Object> getPlanScheduleListForPage(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        String searchWord = this.getPlanScheduleWhereString(methodParam, param);

        int total = planWorkMapper.findCountPlanScheduleList(schemaName, searchWord); // 获取设备的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            searchWord += " order by p.create_time desc";
            searchWord += pagination;
            List<Map<String, Object>> planScheduleList = null;
            planScheduleList = planWorkMapper.findPlanScheduleList(schemaName, searchWord);
            result.put("rows", planScheduleList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 启用禁用维修计划
     *
     * @param methodParam
     * @param param
     */
    @Override
    public void modifyPlanScheduleUse(MethodParam methodParam, Map<String, Object> param) {
        String plan_code = RegexUtil.optStrOrExpNotNull(param.get("plan_code"), LangConstant.TAB_PLAN_A);//维保计划不能为空
        String is_use = RegexUtil.optStrOrExpNotNull(param.get("is_use"), LangConstant.TITLE_AAL_N);//启用状态不能为空
        Map<String, Object> dbPlanInfo = this.getPlanScheduleInfoByCode(methodParam.getSchemaName(), plan_code);
        param.put("schema_name", methodParam.getSchemaName());
        if (RegexUtil.optNotNull(dbPlanInfo).isPresent()) {
            planWorkMapper.updatePlanSchedule(param);
            net.sf.json.JSONArray logArr = new net.sf.json.JSONArray();
            String dbIsUse = RegexUtil.optStrOrBlank(dbPlanInfo.get("is_use"));
            if (!is_use.equals(dbIsUse)) {
                String log = LangUtil.doSetLog("维保计划属性更新", LangConstant.LOG_D, new String[]{LangConstant.TITLE_AAL_N, IsuseEnum.getValue(dbIsUse), IsuseEnum.getValue(is_use)});
                logArr.add(log);
                logService.newLog(methodParam, SensConstant.BUSINESS_NO_10002, plan_code, logArr.toString());
            }
        }
    }

    /**
     * 删除选中维保计划（支持多选，逗号拼接）
     *
     * @param methodParam
     * @param param
     */
    @Override
    public void cutSelectPlanSchedule(MethodParam methodParam, Map<String, Object> param) {
        String schemaName = methodParam.getSchemaName();
        String planCodes = RegexUtil.optStrOrExpNotNull(param.get("planCodes"), LangConstant.TAB_PLAN_A);//维保计划不能为空
        String[] idStr = planCodes.split(",");
        int len = idStr.length;
        RegexUtil.falseExp(len > 0, LangConstant.MSG_CD, new String[]{LangConstant.TAB_PLAN_A}); // 维保计划未选中

        RegexUtil.falseExp(planWorkMapper.setSelectStatus(methodParam.getSchemaName(), idStr, StatusConstant.STATUS_DELETEED) == len, LangConstant.MSG_CD); // 失败：请获取最新数据后重试！

//        //删除计划配置、设备对象、位置、设备类型、设备型号、触发条件等数据
//        cutOldPlanScheduleDataBatch(schemaName, idStr);
//        planWorkMapper.deletePlanScheduleBatch(schemaName, idStr);
        String remark = len > 2 ? LangUtil.doSetLogArrayNoParam("批量删除了维保计划！", LangConstant.TEXT_AP) : LangUtil.doSetLogArray("删除了维保计划！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TAB_PLAN_A});
        for (String id : idStr) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_10002, id, remark);
        }


    }

    /**
     * 维保计划查询设备对象根据设备位置、时间、类型、型号
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getPlanWorkAssetByPositionCodeAndAssetType(MethodParam methodParam, SearchParam param) {
        String schemaName = methodParam.getSchemaName();
        String planAssetWhereString = getPlanAssetWhereString(methodParam, param);
        int size = assetMapper.findAssetPlanCount(schemaName, planAssetWhereString);
        int pageSize;
        if ("true".equals(param.getIsAllAsset())) {
            pageSize = 10;//不选择设备时，默认显示符合筛选条件的10条
        } else {
            pageSize = size;//选择设备时，查询所有设备
        }
        methodParam.setPageNumber(1);
        String pagination = SenscloudUtil.changePagination(methodParam);
        Map<String, Object> result = new HashMap<>();
        if (size < 1) {
            result.put("rows", null);
        } else {
//            planAssetWhereString += " order by t.create_time desc ";
            planAssetWhereString += pagination;
            List<Map<String, Object>> planScheduleList = null;
            planScheduleList = assetMapper.findAssetPlanList(schemaName, planAssetWhereString, pageSize, 0);
            result.put("rows", planScheduleList);
        }
        result.put("total", size);
        return result;
    }

    @Override
    public List<Map<String, Object>> getPlanWorkTaskTemplateList(MethodParam methodParam, SearchParam param) {
//        String searchWord = this.getPlanTaskTemplateWhereString(methodParam, param);
        Map<String, Object> params = new HashMap<>();
        params.put("keywordSearch", param.getKeywordSearch());
        params.put("isUseSearch", "1");
        List<Map<String, Object>> taskTempLatePage = taskTemplateMapper.findTaskTempLatePage(methodParam.getSchemaName(), params);
        return taskTempLatePage;
    }

    @Override
    public Map<String, Object> getPlanWorkTaskTemplateItemList(MethodParam methodParam, SearchParam param) {
        String searchWord = this.getPlanTaskTemplateItemWhereString(methodParam, param);
        String schemaName = methodParam.getSchemaName();

        int total = taskTemplateMapper.findCountPlanWorkTaskTemplateItemList(schemaName, searchWord); // 获取任务项的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            List<Map<String, Object>> itemList = null;
            itemList = taskTemplateMapper.findPlanWorkTaskTemplateItemList(schemaName, searchWord);
            result.put("rows", itemList);
        }
        result.put("total", total);
        return result;
    }

    @Override
    public List<Map<String, Object>> getPlanWorkTaskItemList(MethodParam methodParam, SearchParam param) {
        Map<String, Object> params = new HashMap<>();
        params.put("task_template_code", "");
        params.put("keywordSearch", param.getKeywordSearch());
        return taskItemService.findChoseTaskItemList(methodParam.getSchemaName(), params,methodParam.getUserLang(),methodParam.getCompanyId());
    }

    /**
     * 查询计划日期的触发条件
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Override
    public List<Map<String, Object>> getPlanWorkTriggleByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkTriggleByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询维保计划的任务列表
     *
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    @Override
    public List<String> getTaskListByPlanWorkCode(String schemaName, String planWorkCode) {
        return planWorkMapper.findTaskListByPlanWorkCode(schemaName, planWorkCode);
    }

    private String getPlanTaskTemplateItemWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer(" where 1=1 ");
        RegexUtil.optNotBlankStrOpt(param.getTaskTemplateCodes()).ifPresent(e -> whereString.append(" and tti.task_template_code in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(tti.group_name) like ")
                .append(e).append(" or lower(ti.task_item_name) like ").append(e).append(" or lower(ti.requirements) like ").append(e).append(")"));
        return whereString.toString();
    }

    /**
     * 清空计划配置、设备对象、位置、设备类型、设备型号、触发条件等数据
     *
     * @param schema_name
     * @param planCodes
     */
    private void cutOldPlanScheduleDataBatch(String schema_name, String[] planCodes) {
        planWorkMapper.deletePlanWorkAssetBatch(schema_name, planCodes);//删除计划设备对象
        planWorkMapper.deletePlanWorkAssetTypeBatch(schema_name, planCodes);//删除设备类型信息
        planWorkMapper.deletePlanWorkAssetModelBatch(schema_name, planCodes);//删除设备型号信息
        planWorkMapper.deletePlanWorkFacilityPositionBatch(schema_name, planCodes);//删除计划位置信息
        planWorkMapper.deletePlanWorkTriggleBatch(schema_name, planCodes);//删除触发条件信息
        planWorkMapper.deletePlanWorkInspectionBatch(schema_name, planCodes);//删除点巡检点信息
        planWorkMapper.deletePlanWorkSettingBatch(schema_name, planCodes);//删除计划配置信息
        planWorkMapper.deletePlanWorkAssistOrgBatch(schema_name, planCodes);//删除计划协助团队
//        planWorkMapper.deletePlanWorkExecutorBatch(schema_name, planCodes);//删除计划执行人
        planWorkMapper.deletePlanJsonBatch(schema_name, planCodes); // 删除计划自定义配置信息
    }

    /**
     * 获取查询维保计划列表的条件
     *
     * @param methodParam 系统参数
     * @param param       请求参数
     * @return 拼接sql
     */
    private String getPlanScheduleWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer(" where p.status > " + StatusConstant.STATUS_DELETEED);
        RegexUtil.optNotBlankStrOpt(param.getWorkTypeIds()).ifPresent(e -> whereString.append(" and t.id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(param.getIsUseSearch()).ifPresent(e -> whereString.append(" and p.is_use = '").append(e).append("' "));
        RegexUtil.optNotBlankStrOpt(param.getStartDateSearch()).ifPresent(e -> whereString.append(" and (to_date(p.begin_time::text, '" + SqlConstant.SQL_DATE_FMT + "') >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') or to_date(p.end_time::text, '" + SqlConstant.SQL_DATE_FMT + "') >=to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') )"));
        RegexUtil.optNotBlankStrOpt(param.getEndDateSearch()).ifPresent(e -> whereString.append(" and (to_date(p.begin_time::text, '" + SqlConstant.SQL_DATE_FMT + "') <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') or to_date(p.end_time::text, '" + SqlConstant.SQL_DATE_FMT + "') <=to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') )"));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(p.plan_name) like ")
                .append(e).append(" or lower(p.plan_code) like ").append(e).append(")"));

        RegexUtil.optNotBlankStrOpt(param.getPositionCode()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and ( pfp.position_code LIKE ").append(e).append(" OR pfp.position_code = '' )  "));
//        RegexUtil.optNotBlankStrOpt(param.getPositionCode()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and ( pfp.position_code LIKE ").append(e).append(" OR ( pfp.facility_id = 0 AND pfp.position_code = '' ) )  "));
        return whereString.toString();
    }

    /**
     * 获取查询维保计划设备列表的条件
     *
     * @param methodParam 系统参数
     * @param param       请求参数
     * @return 拼接sql
     */
    private String getPlanAssetWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer();
        RegexUtil.optNotBlankStrOpt(param.getPositionCode()).ifPresent(e -> whereString.append(" and ap.position_code in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(param.getAsset_model_id()).ifPresent(e -> whereString.append(" and t1.asset_model_id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(param.getAsset_category_id()).ifPresent(e -> whereString.append(" and t1.category_id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(param.getEnable_begin_date()).ifPresent(e -> whereString.append(" and to_date(t1.enable_time::text, '" + SqlConstant.SQL_DATE_FMT + "') >= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "') "));
        RegexUtil.optNotBlankStrOpt(param.getEnable_end_date()).ifPresent(e -> whereString.append(" and (to_date(t1.enable_time::text, '" + SqlConstant.SQL_DATE_FMT + "') <= to_date('").append(e).append("', '" + SqlConstant.SQL_DATE_FMT + "')) "));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(t1.asset_name) like ")
                .append(e).append(" or lower(t1.asset_code) like ").append(e).append(")"));
        return whereString.toString();
    }

    /**
     * 查询计划自定义配置
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private Map<String, Object> getPlanWorkPropertyByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkPropertyByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询计划协助团队列表
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkAssistOrgByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkAssistOrgByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询计划执行人列表
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkExecutorByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkExecutor(schemaName, plan_code);

    }

    /**
     * 查询点巡检点信息
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkInspectionByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkInspectionByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询计划设备型号信息
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkAssetModelByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkAssetModelByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询计划设备类型信息
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkAssetTypeByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkAssetTypeByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询计划位置信息
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkFacilityPositionByPlanCode(String schema_name, String plan_code) {
        return planWorkMapper.findPlanWorkFacilityPositionByPlanCode(schema_name, plan_code);
    }

    private List<Map<String, Object>> getPlanWorkAssetByPlanCode(String schema_name, String plan_code) {
        return planWorkMapper.findPlanWorkAsset(schema_name, plan_code);
    }

    /**
     * 查询计划配置信息
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private List<Map<String, Object>> getPlanWorkSettingByPlanCode(String schemaName, String plan_code) {
        return planWorkMapper.findPlanWorkSettingByPlanCode(schemaName, plan_code);
    }

    /**
     * 查询计划基本信息
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    private Map<String, Object> getPlanScheduleInfoByCode(String schemaName, String plan_code) {
        return RegexUtil.optMapOrExpNullInfo(planWorkMapper.findPlanScheduleInfoByCode(schemaName, plan_code), LangConstant.TAB_PLAN_A); // 维保计划不存在
    }

    /**
     * 功能：新建维保计划
     *
     * @param data
     * @return
     */
//    @Override
    public int addPlanSchedule(Map<String, Object> data) {
        return planWorkMapper.insertPlanSchedule(data);
    }

    /**
     * 功能：更新维修计划
     *
     * @param data
     * @return
     */
//    @Override
    public int modifyPlanSchedule(Map<String, Object> data) {
        return planWorkMapper.updatePlanSchedule(data);
    }

    /**
     * 清空计划配置、设备对象、位置、设备类型、设备型号、触发条件等数据
     *
     * @param schema_name
     * @param plan_code
     */
    private void cutOldPlanScheduleData(String schema_name, String plan_code) {
        planWorkMapper.deletePlanWorkAsset(schema_name, plan_code);//删除计划设备对象
        planWorkMapper.deletePlanWorkAssetType(schema_name, plan_code);//删除设备类型信息
        planWorkMapper.deletePlanWorkAssetModel(schema_name, plan_code);//删除设备型号信息
        planWorkMapper.deletePlanWorkFacilityPosition(schema_name, plan_code);//删除计划位置信息
        planWorkMapper.deletePlanWorkTriggle(schema_name, plan_code);//删除触发条件信息
        planWorkMapper.deletePlanWorkInspection(schema_name, plan_code);//删除点巡检点信息
        planWorkMapper.deletePlanWorkSetting(schema_name, plan_code);//删除计划配置信息
        planWorkMapper.deletePlanWorkAssistOrg(schema_name, plan_code);//删除计划协助团队
//        planWorkMapper.deletePlanWorkExecutor(schema_name, plan_code);//删除计划执行人
        planWorkMapper.deletePlanJson(schema_name, plan_code); // 删除计划自定义配置信息
    }

    /**
     * 功能：计划触发条件保存
     *
     * @param schema_name
     * @param plan_code
     * @param planWorkTriggles
     * @return
     */
    private Boolean addPlanWorkTriggleList(String schema_name, String plan_code, List<Map<String, Object>> planWorkTriggles) {
        Boolean status = false;
        for (int i = 0; i < planWorkTriggles.size(); i++) {
            Map<String, Object> map = planWorkTriggles.get(i);
            map.put("schema_name", schema_name);
            map.put("plan_code", plan_code);
            map.put("execute_day",RegexUtil.optIsPresentStr(map.get("execute_day"))?map.get("execute_day"):null);
            int query = addPlanWorkTriggle(map);
        }
        return status;
    }

    /**
     * 新增计划触发条件
     *
     * @param data
     * @return
     */
//    @Override
    public int addPlanWorkTriggle(Map<String, Object> data) {
        return planWorkMapper.insertPlanWorkTriggle(data);
    }

    /**
     * 新增维保计划配置
     */
//    @Override
    public long addPlanWorkSetting(PlanWorkSettingModel planWorkSetting) {
        return planWorkMapper.insertPlanWorkSetting(planWorkSetting);
    }


    /**
     * 新增计划位置
     *
     * @param schema_name
     * @param plan_code
     * @param setting_id
     * @param facility_id
     * @param is_all_asset
     * @param enable_begin_date
     * @param enable_end_date
     * @return
     */
//    @Override
    public int addPlanWorkFacilityPosition(String schema_name, String plan_code, long setting_id, int facility_id, String position_code, boolean is_all_asset, String enable_begin_date, String enable_end_date) {
        return planWorkMapper.insertPlanWorkFacilityPosition(schema_name, plan_code, setting_id, facility_id, position_code, is_all_asset, enable_begin_date, enable_end_date);
    }

    /**
     * 功能：计划位置保存
     *
     * @param schema_name
     * @param plan_code
     * @param planWorkFacilityPositions
     * @param isFacility
     * @return
     */
    private Boolean addPlanWorkFacilityPosition(String schema_name, String plan_code, long setting_id, String planWorkFacilityPositions, boolean is_all_asset, String enable_begin_date, String enable_end_date, boolean isFacility) {
        Boolean status = false;
        if ("".equals(enable_begin_date)) {
            enable_begin_date = null;
        }
        if ("".equals(enable_end_date)) {
            enable_end_date = null;
        }
        if (!RegexUtil.optIsPresentStr(planWorkFacilityPositions)) {
            //如果没有选择组织，则记录一条组织ID为0的数据
            int query = addPlanWorkFacilityPosition(schema_name, plan_code, setting_id, 0, "", is_all_asset, enable_begin_date, enable_end_date);
            if (query > 0) {
                status = true;
            }
        } else {
            String[] planWorkFacilityPositionArray = planWorkFacilityPositions.split(",");
            for (int i = 0; i < planWorkFacilityPositionArray.length; i++) {
                String position = planWorkFacilityPositionArray[i];
                if (RegexUtil.optIsPresentStr(position)) {
                    int query;
                    query = addPlanWorkFacilityPosition(schema_name, plan_code, setting_id, 0, position, is_all_asset, enable_begin_date, enable_end_date);
                    if (query > 0) {
                        status = true;
                    }
                }
            }
        }
        return status;
    }

    /**
     * 功能：计划设备类型保存
     *
     * @param schema_name
     * @param plan_code
     * @param planWorkAssetTypes
     * @return
     */
    private Boolean addPlanWorkAssetType(String schema_name, String plan_code, long setting_id, String planWorkAssetTypes) {
        Boolean status = false;
        if (RegexUtil.optIsPresentStr(planWorkAssetTypes)) {
            String[] planWorkAssetTypeArray = planWorkAssetTypes.split(",");
            for (int i = 0; i < planWorkAssetTypeArray.length; i++) {
                int asset_category_id = Integer.parseInt(planWorkAssetTypeArray[i]);
                int query = addPlanWorkAssetType(schema_name, plan_code, setting_id, asset_category_id);
                if (query > 0) {
                    status = true;
                }
            }
        } else {
            status = true;
        }
        return status;
    }

    /**
     * 新增计划设备类型
     *
     * @param schema_name
     * @param plan_code
     * @param setting_id
     * @param asset_category_id
     * @return
     */
//    @Override
    public int addPlanWorkAssetType(String schema_name, String plan_code, long setting_id, int asset_category_id) {
        return planWorkMapper.insertPlanWorkAssetType(schema_name, plan_code, setting_id, asset_category_id);
    }

    /**
     * 功能：计划设备型号保存
     *
     * @param schema_name
     * @param plan_code
     * @param assetModels
     * @return
     */
    private Boolean addPlanWorkAssetModel(String schema_name, String plan_code, long setting_id, String assetModels) {
        Boolean status = false;
        if (RegexUtil.optIsPresentStr(assetModels)) {
            String[] assetModelArray = assetModels.split(",");
            for (int i = 0; i < assetModelArray.length; i++) {
                long asset_model_id = Long.parseLong(assetModelArray[i]);
                int query = addPlanWorkAssetModel(schema_name, plan_code, setting_id, asset_model_id);
                if (query > 0) {
                    status = true;
                }
            }
        } else {
            status = true;
        }
        return status;
    }

    /**
     * 新增计划设备型号
     *
     * @param schema_name
     * @param plan_code
     * @param setting_id
     * @param asset_model_id
     * @return
     */
//    @Override
    public int addPlanWorkAssetModel(String schema_name, String plan_code, long setting_id, long asset_model_id) {
        return planWorkMapper.insertPlanWorkAssetModel(schema_name, plan_code, setting_id, asset_model_id);
    }

    /**
     * 计划排程设备对象
     */
//    @Override
    public int addPlanWorkAsset(String schema_name, String plan_code, long setting_id, String asset_id) {
        return planWorkMapper.insertPlanWorkAsset(schema_name, plan_code, setting_id, asset_id);
    }

    /**
     * 计划排程定义巡检对象
     */
//    @Override
    public int addPlanWorkInspection(String schema_name, String plan_code, long setting_id, String inspection_code) {
        return planWorkMapper.insertPlanWorkInspection(schema_name, plan_code, setting_id, inspection_code);
    }

    /**
     * 功能：计划协助团队保存
     *
     * @param schema_name
     * @param plan_code
     * @param jsonAssistList
     * @return
     */
    private boolean addPlanWorkAssistList(String schema_name, String plan_code, List<Map<String, Object>> jsonAssistList) {
        Boolean status = false;
        for (int i = 0; i < jsonAssistList.size(); i++) {
            Map<String, Object> assist = jsonAssistList.get(i);
            assist.put("schema_name", schema_name);
            assist.put("plan_code", plan_code);
            assist.put("total_hour",RegexUtil.optStrOrNull(assist.get("total_hour")));
            assist.put("team_size", RegexUtil.optStrOrNull(assist.get("team_size")));

//            if (RegexUtil.optIsPresentStr(assist.get("total_hour")) && "".equals(assist.get("total_hour").toString())) {
//                assist.put("total_hour", null);
//            }
//            if (RegexUtil.optIsPresentStr(assist.get("team_size")) && "".equals(assist.get("team_size").toString())) {
//                assist.put("team_size", null);
//            }
            int query = addPlanWorkAssistOrg(assist);
            if (query > 0) {
                status = true;
            }
        }
        return status;
    }

    /**
     * 新增计划执行人
     *
     * @param executor
     * @return
     */
//    @Override
    public int addPlanWorkExecutor(Map<String, Object> executor) {
        return planWorkMapper.insertPlanWorkExecutor(executor);
    }

    /**
     * 新增计划协助团队
     *
     * @param assist
     * @return
     */
//    @Override
    public int addPlanWorkAssistOrg(Map<String, Object> assist) {
        return planWorkMapper.insertPlanWorkAssistTeam(assist);
    }

    /**
     * 查询计划任务，开始根据配置的任务，进行工单生成
     *
     * @param schemaName
     * @return
     */
    @Override
    public List<Map<String, Object>> getExecutePlanWork(String schemaName, int limit) {
        return planWorkMapper.getExecutePlanWork(schemaName, limit);
    }

    /**
     * 修改维保计划的任务状态
     *
     * @param schemaName
     * @param planWorkCodes
     * @param jobStatus
     */
    @Override
    public void updatePlanWorkJobStatusByPlanWorkCodeList(String schemaName, String planWorkCodes, int jobStatus) {
        planWorkMapper.updatePlanWorkJobStatusByPlanWorkCodeList(schemaName, planWorkCodes, jobStatus);
    }

    /**
     * 修改维保计划的任务状态
     *
     * @param schemaName
     * @param planWorkCode
     * @param jobStatus
     */
    @Override
    public void updatePlanWorkJobStatusByPlanWorkCode(String schemaName, String planWorkCode, int jobStatus) {
        planWorkMapper.updatePlanWorkJobStatusByPlanWorkCode(schemaName, planWorkCode, jobStatus);
    }

    /**
     * 查询维保计划的所有维保对象规则列表
     *
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    @Override
    public List<Map<String, Object>> getPlanWorkSettingCondition(String schemaName, String planWorkCode) {
        return planWorkMapper.getPlanWorkSettingCondition(schemaName, planWorkCode);
    }

    /**
     * 获取符合筛选条件的设备列表（维保计划）
     *
     * @param schemaName
     * @param condition
     * @return
     */
    @Override
    public List<Map<String, Object>> getAssetPlanByCondition(String schemaName, String condition) {
        return assetMapper.findAssetPlanByCondition(schemaName, condition);
    }

    /**
     * 获取菜单信息
     *
     * @param methodParam 入参
     * @return
     */
    @Override
    public Map<String, Map<String, Boolean>> getPlanWorkPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[17]);
    }


    /**
     * 根据维保计划编号获取设备列表（维保计划）
     *
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    @Override
    public List<Map<String, Object>> getAssetPlanByPlanWorkCode(String schemaName, String planWorkCode) {
        return assetMapper.getAssetPlanByPlanWorkCode(schemaName, planWorkCode);
    }

    /**
     * 批量插入维保行事历
     *
     * @param schemaName
     * @param planWorkCalendars
     */
    @Override
    public int insertBatchPlanWorkCalendars(String schemaName, List<PlanWorkCalendarModel> planWorkCalendars) {
        return planWorkMapper.insertBatchPlanWorkCalendars(schemaName, planWorkCalendars);
    }

    /**
     * 新增维保行事历
     *
     * @param schemaName
     * @param planWorkCalendar
     */
    @Override
    public int insertPlanWorkCalendar(String schemaName, PlanWorkCalendarModel planWorkCalendar) {
        return planWorkMapper.insertPlanWorkCalendar(schemaName, planWorkCalendar);
    }

    /**
     * 修改维保行事历的任务项
     * @param schemaName
     * @param taskItems
     * @return
     */
    @Override
    public int updatePlanWorkCalendarTaskItems(String schemaName, List<Map<String, Object>> taskItems) {
        return planWorkMapper.updatePlanWorkCalendarTaskItems(schemaName, taskItems);
    }

    /**
     * 查询计划行事历列表
     * @param schemaName
     * @param limit
     * @return
     */
    @Override
    public List<PlanWorkCalendarModel> getPlanWorkCalendarListByOccurTime(String schemaName, int autoGenerateBill, int limit) {
        return planWorkMapper.getPlanWorkCalendarListByOccurTime(schemaName, autoGenerateBill, limit);
    }

    /**
     * 批量修改行事历状态
     *
     * @param schemaName
     * @param workCalendarCodes
     * @param status
     */
    @Override
    public void updatePlanWorkCalendarStatusByWorkCalendarCodeList(String schemaName, String workCalendarCodes, int status) {
        planWorkMapper.updatePlanWorkCalendarStatusByWorkCalendarCodeList(schemaName, workCalendarCodes, status);
    }

    /**
     * 删除计划生成的还未产生工单的计划行事历
     *
     * @param schemaName
     * @param planWorkCode
     */
    @Override
    public void deletePlanWorkCalendarByPlanWorkCode(String schemaName, String planWorkCode) {
        planWorkMapper.deletePlanWorkCalendarByPlanWorkCode(schemaName, planWorkCode);
    }

    /**
     * 查询指定设备某一天的行事历数量
     *
     * @param schemaName
     * @param planWorkCode
     * @param assetId
     * @param date
     */
    @Override
    public int countPlanWorkCalendarByAssetAndDate(String schemaName, String planWorkCode, String assetId, String date) {
        return planWorkMapper.countPlanWorkCalendarByAssetAndDate(schemaName, planWorkCode, assetId, date);
    }

    @Override
    public Map<String, Object> findPlanWorkPropertyInfo(String schemaName, String work_calendar_code) {
        return planWorkMapper.findPlanWorkPropertyInfo(schemaName, work_calendar_code);
    }
}
