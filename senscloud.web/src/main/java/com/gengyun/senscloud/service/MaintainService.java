package com.gengyun.senscloud.service;

public interface MaintainService {
//
//    //查询设备最近一条保养信息，先获取未处理的当前用户保养，如果没有，再获取最近一条保养，在界面显示该设备当前保养上次保养时间和截止时间
//    MaintainData getMaintainInfoAndItemByAssetTypeId(String schema_name, String account, String assetId, String maintainCode);
//
//    //保存保养信息
//    int saveMaintain(String schema_name, MaintainData maintainData);
//
//    //新增支援人员，初始工时为0，支援人员中包含_sc_maintain表中的接受人员
//    int insertMaintainWorkHour(String schema_name, MaintainWorkHourData maintainWorkHourData);
//
//    //编辑保养信息,点击保存按钮，或者草稿状态，重新保存或提交
//    int EditMaintain(String schema_name, MaintainData maintainData);
//
//    //更新分配人
//    int EditMaintainMan(String schema_name, MaintainData maintainData);
//
//    //删除援人员，在重新保存和提交保养单时，先全部删除工时表中的人，再重新新增支援人员进来
//    int deleteMaintainWorkHour(String schema_name, String maintain_code);
//
//    //更新编辑开始保养时间，为手机上点击设备的保养开始
//    int EditMaintainBeginTime(String schema_name, MaintainData maintainData);
//
//    //设置维修开始计时,缺表，待建 2018-03-29
//
//
//    //保存和提交保养结果
//    int SaveMaintainResult(String schema_name, MaintainData maintainData);
//
//    //新增保养领用的备件
//    int insertMaintainBom(String schema_name, MaintainBomData maintainBomData);
//
//    //删除保养领用的备件,用于保养结果重新保存或提交时
//    int deleteMaintainBom(String schema_name, String maintain_code);
//
//    //保养结果确认
//    int AuditMaintainResult(String schema_name, MaintainData maintainData);
//
//    //查找保养单列表(按，条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    List<MaintainData> getMaintainList(String schema_name, String condition, int pageSize, int begin);
//
//    //查找保养单列表总数(按，条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    int getMaintainListCount(String schema_name, String condition);
//
//    //查找单条保养单据
//    MaintainData getMaintainInfo(String schema_name, String maintainCode);
//
//    //按设备查找最近保养单据
//    MaintainData getLastMaintainInfoByAssetId(String schema_name, String assetId);
//
//    //按位置，查找所有的设备，以及其最新的一条保养数据（不管是否执行完成，只要生成了）, 供系统自动生成保养单用
//    List<MaintainData> findAllAssetAndLastestMaintain(String schema_name, int facility_id);
//
//    //过期的保养单，进行关闭操作
//    int CloseMaintainBillForDue(String schema_name);
//
//    //获取备件列表，用于保养的备件
//    List<MaintainBomData> getMaintainBomList(String schema_name, String maintainCode);
//
//    //获取保养工时列表
//    List<MaintainWorkHourData> getMaintainWorkHourList(String schema_name, String maintainCode);
//
//    //获取设备的保养项，按设备的类型
//    List<MaintainItemData> getMaintainItemByAssetTypeId(String schema_name, String asset_type_id);
//
//    //获取所有的设备保养项
//    List<MaintainItemData> getAllMaintainItemList(String schema_name);
//
//    //获取设备的保养确认项，按设备的类型
//    List<MaintainCheckItemData> getMaintainCheckItem(String schema_name, String asset_type_id);
//
//    //查找保养单，查到需保养的设备
//    Asset getAssetCommonDataByMaintainCode(String schema_name, String maintainCode);
//
//    //查找保养记录，通过设备编号
//    List<MaintainData> getMaintainListByCode(String schema_name, String device_code);
//
//    //备件统计保养使用数量明细查询by bom_model and stock_code
//    List<MaintainBomData> getMaintainDetailByBomModelAndStockCode(String schema_name, String bom_model, String stock_code);
//
//    //作废保养
//    int invalidMaintain(String schema_name, String maintainCode);
//
//
//    //更新保养的截止日期，以重新定义下一次生成保养的日期
//    int updateMaintainDeadlineTime(String schema_name, String maintainCode, Timestamp deadlineTime);
//
//    //根据物料编号查询记录
//    int deleteMaintianByMaterial_code(String schema_name, String bill_code);
//
//    //更新嘉岩备件信息maintain_Bom
//    int saveMaintainBom(String schema_name, MaintainBomData maintainBomData);
//
//
//    //嘉研备件申领修改状态禁止
//    int updateMaintainApplyStaus(String schema_name, String code);
//
//    //嘉研备件申领修改状态释放
//    int updateOpenMaintainApplyStaus(String schema_name, String code, int status);
//
//    //查询物料编号是否存在保养备件信息
//    List<MaintainBomData> selectMaintainByMaterialCode(String schema_name, String code);
//
//    //更新物料编号保养备件数量信息
//    int udpateMaintainBomBymaterial(String schema_name,String bill_code);
//
//    /*全名屏保养任务tip面板数据*/
//    FullScreenData getPlannedAndCompleteMaintain(String schema_name);
}
