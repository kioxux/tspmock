package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.PerformanceManagementService;
import org.springframework.stereotype.Service;

@Service
public class PerformanceManagementServiceImpl implements PerformanceManagementService {
//
//    @Autowired
//    PerformanceManagementMapper performanceManagementMapper;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    DashboardConfigService dashboardConfigService;
//
//    @Autowired
//    DataPermissionForFacility dataPermissionForFacility;
//
//    private static String PERSON = "person";
//    private static String TEAM = "team";
//    private static String MONTH = "month";
//    private static String YEAR = "year";
//    //分数精确位数
//    private final int SCORE_SCALE = 2;
//
//    /**
//     * 查询个人绩效列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPerformanceList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        JSONObject result = new JSONObject();
//        String keyWord = request.getParameter("keyWord");
//        String performanceTemplate = request.getParameter("performanceTemplate");
//        String groups = request.getParameter("groups");
//        String period = request.getParameter("period");
//        if (StringUtils.isBlank(performanceTemplate) || StringUtils.isBlank(period))
//            return result.toString();
//
//        List<Map<String, Object>> dataList = null;
//        int total = 0;
//        //首页条件
//        String condition = String.format(" pm.template_id = %d AND ( pm.period = '%s' OR pm.period = '%s') ", Long.valueOf(performanceTemplate), period, period.split("-")[0]);
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " AND u.username LIKE '%" + keyWord + "%' ";
//        }
//        if (StringUtils.isNotBlank(groups)) {
//            condition += " AND (ug.group_id IN (" + groups + ") OR pm.group_id IN (" + groups + ")) ";
//        }
//        condition += dataPermissionForFacility.getDataPermissionQueryCondition(schema_name, user, "performance_management_index",
//                " and (ug.group_id in (" + user.getGroupIdsString() + ") or pm.group_id in (" + user.getGroupIdsString() + "))",
//                " and pm.user_id = '" + user.getId() + "'");
//
//        //查询绩效的考核项
//        List<Map<String, Object>> performanceOptions = performanceManagementMapper.queryPerformanceOptions(schema_name, condition);
//        if (performanceOptions != null && performanceOptions.size() > 0) {
//            StringBuffer queryFileds = new StringBuffer();
//            //动态凭借表格需要查询的考核项列
//            for (Map<String, Object> performanceOption : performanceOptions) {
//                String inspection_items = (String) performanceOption.get("inspection_items");
//                queryFileds.append(String.format("SUM(CASE t.inspection_items WHEN '%s' THEN t.score ELSE 0 END) AS \"dashboard_%s\", ", inspection_items, inspection_items));
//            }
//            dataList = performanceManagementMapper.getPerformanceList(schema_name, condition, queryFileds.toString(), HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            total = performanceManagementMapper.countPerformanceList(schema_name, condition);
//        }
//        result.put("rows", dataList);
//        result.put("total", total);
//        result.put("performanceOptions", performanceOptions);
//        return result.toString();
//    }
//
//    /**
//     * 查询个人绩效列表-获取考核项字段（bootstraptable动态列columns）
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel findPerformanceOptionList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String keyWord = request.getParameter("keyWord");
//        String performanceTemplate = request.getParameter("performanceTemplate");
//        String groups = request.getParameter("groups");
//        String period = request.getParameter("period");
//        if (StringUtils.isBlank(performanceTemplate))
//            return ResponseModel.ok(null);
//
//        //首页条件
//        String condition = String.format(" pm.template_id = %d AND (pm.period = '%s' OR pm.period = '%s') ", Long.valueOf(performanceTemplate), period, period.split("-")[0]);
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition += " AND u.username LIKE '%" + keyWord + "%' ";
//        }
//        if (StringUtils.isNotBlank(groups)) {
//            condition += " AND (ug.group_id IN (" + groups + ") OR pm.group_id IN (" + groups + ")) ";
//        }
//        //查询绩效的考核项
//        List<Map<String, Object>> performanceOptions = performanceManagementMapper.queryPerformanceOptions(schema_name, condition);
//        return ResponseModel.ok(performanceOptions);
//    }
//
//    /**
//     * 评分页面-通过ID，查询个人绩效列表-获取考核项字段（bootstraptable动态列columns）注意：这里的分数是没有经过公式折算的原始评分分数
//     *
//     * @param request
//     * @return
//     */
//    public ResponseModel findPerformanceOptionListById(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String ids = request.getParameter("ids");
//        if (StringUtils.isBlank(ids))
//            return ResponseModel.ok(null);
//
//        //查询绩效的考核项columns(动态查询)
//        List<Map<String, Object>> performanceOptions = performanceManagementMapper.queryPerformanceOptionsById(schema_name, Long.parseLong(ids.split(",")[0]));
//        if (performanceOptions != null && performanceOptions.size() > 0) {
//            StringBuffer queryFileds = new StringBuffer();
//            //动态凭借表格需要查询的考核项列
//            for (Map<String, Object> performanceOption : performanceOptions) {
//                String inspection_items = (String) performanceOption.get("inspection_items");
//                queryFileds.append(String.format("MAX(CASE t.inspection_items WHEN '%s' THEN t.pmd_id || '-' || t.score ELSE '' END) AS \"dashboard_%s\", ", inspection_items, inspection_items));
//            }
//            //通过ID，查询人员绩效详情列表
//            List<Map<String, Object>> performanceDetails = performanceManagementMapper.queryPerformanceDetailsByIds(schema_name, queryFileds.toString(), ids);
//            Map<String, Object> map = new HashMap<>();
//            map.put("performanceOptions", performanceOptions);
//            map.put("performanceDetails", performanceDetails);
//            return ResponseModel.ok(map);
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_DATA_ERR));
//        }
//    }
//
//    /**
//     * 查询绩效详情
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public Map<String, Object> findPerformanceDetail(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        return performanceManagementMapper.queryPerformanceById(schema_name, Long.parseLong(id));
//    }
//
//    /**
//     * 查询个人评分项列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPerformanceItemsById(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        long id = Long.parseLong(request.getParameter("id"));
//        List<Map<String, Object>> dataList = performanceManagementMapper.findPerformanceItemsById(schema_name, id);
//        result.put("rows", dataList);
//        return result.toString();
//    }
//
//    /**
//     * 查询绩效模板列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPerformanceTemplateList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String keyWord = request.getParameter("keyWord");
//        String assessmentType = request.getParameter("assessmentType");
//
//        //首页条件
//        StringBuffer condition = new StringBuffer();
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition.append(" AND t.template_name LIKE '%" + keyWord + "%' ");
//        }
//        if (StringUtils.isNotBlank(assessmentType)) {
//            condition.append(" AND t.assessment_type = " + assessmentType + " ");
//        }
//        List<Map<String, Object>> dataList = performanceManagementMapper.getPerformanceTemplateList(schema_name, condition.toString(), HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//        int total = performanceManagementMapper.countPerformanceTemplateList(schema_name, condition.toString());
//
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询绩效报告列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPerformanceReportList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        JSONObject result = new JSONObject();
//        String groups = request.getParameter("groups");
//        String period = request.getParameter("period");
//        String keyWord = request.getParameter("keyWord");
//        String assessmentType = request.getParameter("assessmentType");
//        if (StringUtils.isBlank(assessmentType) || StringUtils.isBlank(period))
//            return result.toString();
//
//        //查询条件
//        StringBuffer condition2 = new StringBuffer();
//        StringBuffer condition3 = new StringBuffer();
//        String condition = String.format(" AND p.assessment_type = %d AND p.period = '%s' ", Integer.valueOf(assessmentType), period);
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition2.append(" AND u.username LIKE '%" + keyWord + "%' ");
//        }
//        if (StringUtils.isNotBlank(groups) && !"0".equals(groups)) {
//            condition3.append(" AND s.group_id IN (" + groups + ") ");
//        }
//
//        String con = dataPermissionForFacility.getDataPermissionQueryCondition(schema_name, user, "performance_report", "self", "oneself");
//        if ("self".equals(con)) {
//            condition3.append(" and s.group_id in (" + user.getGroupIdsString() + ") ");
//        } else if ("oneself".equals(con)) {
//            condition += " and p.user_id = '" + user.getId() + "' ";
//        }
//
//        List<Map<String, Object>> dataList = performanceManagementMapper.findPerformanceReportList(schema_name, condition, condition2.toString(), condition3.toString(), HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//        int total = performanceManagementMapper.countPerformanceReportList(schema_name, condition, condition2.toString(), condition3.toString());
//
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 查询绩效报告用户列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPerformanceReportUserList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        JSONObject result = new JSONObject();
//        String groups = request.getParameter("groups");
//        String period = request.getParameter("period");
//        String keyWord = request.getParameter("keyWord");
//        String assessmentType = request.getParameter("assessmentType");
//        if (StringUtils.isBlank(groups) || StringUtils.isBlank(period) || StringUtils.isBlank(assessmentType))
//            return result.toString();
//
//        String con = dataPermissionForFacility.getDataPermissionQueryCondition(schema_name, user, "performance_report", "self", "oneself");
//        List<Map<String, Object>> dataList = null;
//        int total = 0;
//        if ("2".equals(assessmentType)) {
//            //查询团队绩效列表
//            StringBuffer condition = new StringBuffer(String.format(" AND p.assessment_type = %d AND p.period = '%s' AND p.group_id =%d ", Integer.valueOf(assessmentType), period, Long.valueOf(groups)));
//            if ("self".equals(con)) {
//                condition.append(" and p.group_id in (" + user.getGroupIdsString() + ") ");
//            } else if ("oneself".equals(con)) {
//                condition.append(" and P.user_id = '" + user.getId() + "' ");
//            }
//            dataList = performanceManagementMapper.findPerformanceReportGroupList(schema_name, condition.toString(), HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            total = performanceManagementMapper.countPerformanceReportGroupList(schema_name, condition.toString());
//        } else {
//            //查询个人绩效列表
//            StringBuffer condition = new StringBuffer(String.format(" AND p.assessment_type = %d AND p.period = '%s' AND ug.group_id =%d ", Integer.valueOf(assessmentType), period, Long.valueOf(groups)));
//            if ("self".equals(con)) {
//                condition.append(" and (ug.group_id in (" + user.getGroupIdsString() + ") or p.group_id in (" + user.getGroupIdsString() + ")) ");
//            } else if ("oneself".equals(con)) {
//                condition.append(" and P.user_id = '" + user.getId() + "' ");
//            }
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition.append(" AND u.username LIKE '%" + keyWord + "%' ");
//            }
//            dataList = performanceManagementMapper.findPerformanceReportUserList(schema_name, condition.toString(), HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            total = performanceManagementMapper.countPerformanceReportUserList(schema_name, condition.toString());
//        }
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//    /**
//     * 绩效报告导出
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public ModelAndView performanceReportExport(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        int pageSize = 50000;//Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = 1;//Integer.parseInt(request.getParameter("pageNumber"));
//
//        User user = AuthService.getLoginUser(request);
//        String groups = request.getParameter("groups");
//        String period = request.getParameter("period");
//        String keyWord = request.getParameter("keyWord");
//        String assessmentType = request.getParameter("assessmentType");
//        List<Map<String, Object>> dataList = null;
//        if (StringUtils.isNotBlank(assessmentType) && StringUtils.isNotBlank(period)) {
//            //查询条件
//            StringBuffer condition2 = new StringBuffer();
//            StringBuffer condition3 = new StringBuffer();
//            String condition = String.format(" AND p.assessment_type = %d AND p.period = '%s' ", Integer.valueOf(assessmentType), period);
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition2.append(" AND u.username LIKE '%" + keyWord + "%' ");
//            }
//            if (StringUtils.isNotBlank(groups) && !"0".equals(groups)) {
//                condition3.append(" AND s.group_id IN (" + groups + ") ");
//            }
//            String con = dataPermissionForFacility.getDataPermissionQueryCondition(schema_name, user, "performance_report", "self", "oneself");
//            if ("self".equals(con)) {
//                condition3.append(" and s.group_id in (" + user.getGroupIdsString() + ") ");
//            } else if ("oneself".equals(con)) {
//                condition += " and p.user_id = '" + user.getId() + "' ";
//            }
//            dataList = performanceManagementMapper.findPerformanceReportList(schema_name, condition, condition2.toString(), condition3.toString(), HttpRequestUtils.getPageSize(pageSize), HttpRequestUtils.getPageBegin(pageNumber, pageSize));
//        }
//        Map<String, Object> map = new HashMap<>();
//        map.put("dataList", dataList);
//        map.put("selectOptionService", selectOptionService);
//        PerformanceReportExportView excelView = new PerformanceReportExportView();
//        return new ModelAndView(excelView, map);
//    }
//
//    /**
//     * 绩效模板启用、禁用
//     *
//     * @param schemaName
//     * @param id
//     * @param status
//     * @return
//     */
//    @Override
//    public ResponseModel enableOrDisablePerformanceTemplateById(String schemaName, long id, String status) {
//        int dpUpdate = performanceManagementMapper.enableOrDisablePerformanceTemplateById(schemaName, id, "true".equals(status) ? false : true);
//        if (dpUpdate > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作失败
//        }
//    }
//
//    /**
//     * 新增、编辑绩效考核模板
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel doAddPerformanceTemplate(String schemaName, HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        PerformanceTemplate performanceTemplate = null;
//        try {
//            performanceTemplate = HttpRequestUtils.request2Bean(PerformanceTemplate.class);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//        }
//        String performanceTemplateOptionsArray = request.getParameter("performanceTemplateOptionsArray");
//        if (StringUtils.isBlank(performanceTemplateOptionsArray))
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        performanceTemplate.setSchema_name(schemaName);
//        int result = 0;
//        if (performanceTemplate.getId() == 0) {
//            performanceTemplate.setIsuse(true);//默认启用
//            performanceTemplate.setCreate_user_account(user.getAccount());
//            performanceTemplate.setCreate_time(UtilFuns.sysTimestamp());
//            //保存考核模板主表数据
//            result = performanceManagementMapper.addPerformanceTemplate(performanceTemplate);
//        } else {
//            result = performanceManagementMapper.updatePerformanceTemplate(performanceTemplate);
//            if (result > 0) {
//                performanceManagementMapper.deletePerformanceTemplateOptions(schemaName, performanceTemplate.getId());
//                performanceManagementMapper.deletePerformanceTemplateOptionsEquation(schemaName, performanceTemplate.getId());
//            }
//        }
//        if (result > 0) {
//            List<Map<String, Object>> equations = new ArrayList<>();//考核标准表数据
//            List<PerformanceTemplateOptions> performanceTemplateOptions = JSON.parseArray(performanceTemplateOptionsArray, PerformanceTemplateOptions.class);
//            for (PerformanceTemplateOptions performanceTemplateOption : performanceTemplateOptions) {
//                performanceTemplateOption.setTemplate_id(performanceTemplate.getId());
//                performanceTemplateOption.setSchema_name(schemaName);
//                //保存考核模板-考核项表数据
//                result = performanceManagementMapper.addPerformanceTemplateOption(performanceTemplateOption);
//                if (result > 0) {
//                    List<PerformanceTemplateOptionsEquation> performanceTemplateOptionsEquations = performanceTemplateOption.getPerformanceTemplateOptionsEquations();
//                    if (performanceTemplateOptionsEquations != null && performanceTemplateOptionsEquations.size() > 0) {
//                        for (PerformanceTemplateOptionsEquation performanceTemplateOptionsEquation : performanceTemplateOptionsEquations) {
//                            performanceTemplateOptionsEquation.setTemplate_id(performanceTemplate.getId());
//                            performanceTemplateOptionsEquation.setTemplate_option_id(performanceTemplateOption.getId());
//                            equations.add(JSON.parseObject(new JSONObject(performanceTemplateOptionsEquation).toString(), Map.class));
//                        }
//                    }
//                } else {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_FAIL));//添加失败
//                }
//            }
//            //批量保存绩效模板考核项考核标准表数据
//            selectOptionService.batchInsertDatas(schemaName, equations, "_sc_performance_management_template_options_equation", SqlConstant._sc_performance_management_template_options_equation_columns);
//            if (result > 0) {
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_SUCC));//添加成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_FAIL));//添加失败
//            }
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_FAIL));//添加失败
//        }
//    }
//
//    /**
//     * 通过ID查询绩效模板详情
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Override
//    public ResponseModel getTemplateDetailById(String schemaName, long id) {
//        PerformanceTemplate performanceTemplate = performanceManagementMapper.queryPerformanceTemplateById(schemaName, id);
//        if (performanceTemplate == null)
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.LOAD_DATA_FAILURE));//加载数据失败
//
//        List<PerformanceTemplateOptions> performanceTemplateOptions = performanceManagementMapper.queryPerformanceTemplateOptionsByTemplateId(schemaName, id);
//        if (performanceTemplateOptions == null)
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.LOAD_DATA_FAILURE));//加载数据失败
//
//        List<PerformanceTemplateOptionsEquation> performanceTemplateOptionsEquations = performanceManagementMapper.queryPerformanceTemplateOptionsEquationByTemplateId(schemaName, id);
//        if (performanceTemplateOptionsEquations == null)
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.LOAD_DATA_FAILURE));//加载数据失败
//
//        Map<String, Object> result = new HashMap<>();
//        result.put("performanceTemplate", performanceTemplate);
//        result.put("performanceTemplateOptions", performanceTemplateOptions);
//        result.put("performanceTemplateOptionsEquations", performanceTemplateOptionsEquations);
//        return ResponseModel.ok(result);
//    }
//
//    /**
//     * 绩效评分
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel doAddPerformanceScore(String schemaName, HttpServletRequest request) {
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schemaName, user, request);
//        if (null == user) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        boolean isHold = Boolean.parseBoolean(request.getParameter("isHold"));//是否暂存分数
//        String pmIds = request.getParameter("pmIds");
//        String performanceScoreArray = request.getParameter("performanceScoreArray");
//        if (StringUtils.isBlank(pmIds) || StringUtils.isBlank(performanceScoreArray))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        JSONArray performanceScoreArrays = JSONArray.fromObject(performanceScoreArray);
//        if (performanceScoreArrays == null || performanceScoreArrays.size() == 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        Map<String, Object> performanceManagement = performanceManagementMapper.queryPerformanceManagementById(schemaName, Long.parseLong(pmIds.split(",")[0]));
//        if (performanceManagement == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        if (performanceManagement.get("has_score") == null || (Boolean) performanceManagement.get("has_score") == true)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PERFORMANCE_SCORE_REPEAT));//参数有误，已经评分过的人员绩效不能再次评分！
//
//        //查询人员绩效的客观（不可编辑的）考核项得分
//        List<Map<String, Object>> psList = performanceManagementMapper.queryPerformanceScore(schemaName, pmIds);
//        Map<Long, BigDecimal> performanceScoreMap = new HashMap<>();//人员绩效-客观评分映射表
//        if (psList != null && psList.size() > 0) {
//            for (Map<String, Object> ps : psList) {
//                performanceScoreMap.put((Long) ps.get("id"), (BigDecimal) ps.get("weight_score"));
//            }
//        }
//        //考核项-考核标准关系表
//        Map<Long, List<PerformanceTemplateOptionsEquation>> equationMap = getPerEquationMap(schemaName, (Long) performanceManagement.get("template_id"));
//        //更新评分数据
//        for (int i = 0; i < performanceScoreArrays.size(); i++) {
//            net.sf.json.JSONObject performanceScore = performanceScoreArrays.getJSONObject(i);
//            long pmId = performanceScore.getLong("pmId");
//            long pmdId = performanceScore.getLong("performanceDetailId");
//            long templateOptionsId = performanceScore.getLong("templateOptionsId");
//            BigDecimal initialScore = new BigDecimal(performanceScore.getString("score"));//初始打分
//            //按绩效标准（公式）折算实际单项得分
//            BigDecimal score;
//            if (equationMap == null || !equationMap.containsKey(templateOptionsId))
//                score = initialScore;//没有配置考核标准时（公式），即为初始打分
//            else
//                score = computeScore(initialScore, equationMap.get(templateOptionsId), SCORE_SCALE, RoundingMode.HALF_UP); //精确到小数点后两位
//
//            //计算权重得分
//            BigDecimal weightScore = score.multiply(new BigDecimal(performanceScore.getString("weightiness"))).divide(new BigDecimal(100)).setScale(SCORE_SCALE, RoundingMode.HALF_UP);//精确到小数点后两位
//            performanceManagementMapper.updatePerformanceScore(schemaName, pmdId, initialScore, score, weightScore);
//            if (performanceScoreMap.get(pmId) == null)
//                performanceScoreMap.put(pmId, BigDecimal.ZERO);
//
//            performanceScoreMap.put(pmId, performanceScoreMap.get(pmId).add(weightScore));
//        }
//        Set<Long> keys = performanceScoreMap.keySet();
//        for (Long key : keys) {
//            //如果是“提交”，则要修改人员绩效为“已评分”；如果是“保存”，则只修改评分，可再次修改评分
//            //修改人员绩效评分状态、总得分
//            performanceManagementMapper.updatePerformanceScoreStatus(schemaName, performanceScoreMap.get(key), (isHold ? false : true), key);
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//    }
//
//    /**
//     * 获取考核项-考核标准关系表
//     *
//     * @param schemaName
//     * @param templateId
//     * @return
//     */
//    private Map<Long, List<PerformanceTemplateOptionsEquation>> getPerEquationMap(String schemaName, Long templateId) {
//        if (templateId == null)
//            return null;
//
//        //查询模板考核项的考核标准（公式）列表数据
//        List<PerformanceTemplateOptionsEquation> equationList = performanceManagementMapper.queryPerformanceTemplateOptionsEquationByTemplateId(schemaName, templateId);
//        Map<Long, List<PerformanceTemplateOptionsEquation>> equationMap = new HashMap<>();//考核项-考核标准关系表
//        if (equationList != null && equationList.size() > 0) {
//            for (PerformanceTemplateOptionsEquation equation : equationList) {
//                long key = equation.getTemplate_option_id();
//                if (equationMap.containsKey(key)) {
//                    equationMap.get(key).add(equation);
//                } else {
//                    equationMap.put(key, new ArrayList(Arrays.asList(equation)));
//                }
//            }
//        }
//        return equationMap;
//    }
//
//    /**
//     * 计算考核单项得分
//     *
//     * @param initialScore
//     * @param equations
//     */
//    private BigDecimal computeScore(BigDecimal initialScore, List<PerformanceTemplateOptionsEquation> equations, int scale, RoundingMode roundingMode) {
//        if (equations == null)
//            return initialScore;
//
//        for (PerformanceTemplateOptionsEquation equation : equations) {
//            String expression = spliceExpression(equation.getGreater(), equation.getLess(), equation.getGreater_value(), equation.getLess_value());
//            if (JexlEngineUtil.compareExpression(expression, initialScore)) {//判断是否符合条件
//                return JexlEngineUtil.executeExpression(equation.getEquation(), initialScore, scale, roundingMode);//按考核标准填写的公式计算实际得分
//            }
//        }
//        return initialScore;
//    }
//
//    /**
//     * 拼接条件判断公式
//     *
//     * @param greater
//     * @param less
//     * @param greater_value
//     * @param less_value
//     * @return
//     */
//    private String spliceExpression(int greater, int less, BigDecimal greater_value, BigDecimal less_value) {
//        //S>=70&&S<80
//        StringBuffer expression = new StringBuffer("S");
//        if (greater == 1)
//            expression.append(">");
//        else if (greater == 2)
//            expression.append(">=");
//        else
//            return null;
//
//        expression.append(greater_value.toString() + "&&S");
//        if (less == 1)
//            expression.append("<");
//        else if (less == 2)
//            expression.append("<=");
//        else
//            return null;
//
//        expression.append(less_value.toString());
//        return expression.toString();
//    }
//
//    /**
//     * 查询绩效模板列表
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public String findPerformancePlanList(HttpServletRequest request) {
//        int pageNumber = 1;
//        int pageSize = 15;
//        try {
//            String strPage = request.getParameter("pageNumber");
//            if (RegexUtil.isNumeric(strPage)) {
//                pageNumber = Integer.valueOf(strPage);
//            }
//        } catch (Exception ex) {
//            pageNumber = 1;
//        }
//        try {
//            String strPageSize = request.getParameter("pageSize");
//            if (RegexUtil.isNumeric(strPageSize)) {
//                pageSize = Integer.valueOf(strPageSize);
//            }
//        } catch (Exception ex) {
//            pageSize = 15;
//        }
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String keyWord = request.getParameter("keyWord");
//
//        List<Map<String, Object>> dataList = null;
//        int total = 0;
//        //首页条件
//        StringBuffer condition = new StringBuffer();
//        if (keyWord != null && !keyWord.isEmpty()) {
//            condition.append(" AND p.plan_name LIKE '%" + keyWord + "%' ");
//        }
//        int begin = pageSize * (pageNumber - 1);
//        String rolesTitle = selectOptionService.getLanguageInfo(LangConstant.ALL_ROLES);
//        String groupTitle = selectOptionService.getLanguageInfo(LangConstant.USER_GROUP);
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("performancePlan");
//        dataList = performanceManagementMapper.getPerformancePlanList(schema_name, condition.toString(), pageSize, begin, rolesTitle, groupTitle, idPrefix);
//        total = performanceManagementMapper.countPerformancePlanList(schema_name, condition.toString());
//
//        result.put("rows", dataList);
//        result.put("total", total);
//        return result.toString();
//    }
//
//
//    /**
//     * 绩效模板启用、禁用
//     *
//     * @param schemaName
//     * @param id
//     * @param status
//     * @return
//     */
//    @Override
//    public ResponseModel enableOrDisablePerformancePlanById(String schemaName, String id, String status) {
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("performancePlan");
//        id = id.substring(idPrefix.length());
//        int dpUpdate = performanceManagementMapper.enableOrDisablePerformancePlanById(schemaName, Long.parseLong(id), "true".equals(status) ? false : true);
//        if (dpUpdate > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作失败
//        }
//    }
//
//    @Override
//    public Map<String, Object> queryPlanById(String schemaName, String subWorkCode) {
//        String idPrefix = Constants.SPECIAL_ID_PREFIX.get("performancePlan");
//        subWorkCode = subWorkCode.substring(idPrefix.length());
//        return performanceManagementMapper.queryPlanById(schemaName, subWorkCode);
//    }
//
//    @Override
//    public ResponseModel save(String schema_name, String processDefinitionId, Map<String, Object> paramMap, HttpServletRequest request, String pageType) throws Exception {
//        User user = AuthService.getLoginUser(request);
//        String role = "role";
//        String groups = "group";
//        user = commonUtilService.checkUser(schema_name, user, request);
//        if (null == user) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//        }
//        String doFlowKey = request.getParameter("do_flow_key");//按钮类型
//        // 提交按钮
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (null != map_object && map_object.size() > 0) {
//            Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//            List<Map<String, Object>> bomContentrole = (List<Map<String, Object>>) map_object.get("bomContentrole");
//            List<Map<String, Object>> bomContentuserGroup = (List<Map<String, Object>>) map_object.get("bomContentuserGroup");
//            Map<String, Object> work_taskInfo = (Map<String, Object>) dataInfo.get(SqlConstant.OTHER_TABLE);
//            boolean is_use = true;//默认状态
//            String account = user.getAccount();
//            Timestamp now = new Timestamp(System.currentTimeMillis());//获取当前时间
//            work_taskInfo.put("isuse", is_use);
//            work_taskInfo.put("create_time", now);
//            work_taskInfo.put("schema_name", schema_name);
//            work_taskInfo.put("create_user_account", account);
//            List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
//            dataList.add(work_taskInfo);
//            if (work_taskInfo.get("id") != null && !work_taskInfo.get("id").equals("")) {
//                performanceManagementMapper.deleteRoles(schema_name, (String) work_taskInfo.get("id"));//删除旧数据
//                performanceManagementMapper.deleteGroups(schema_name, (String) work_taskInfo.get("id"));//删除旧数据
//                performanceManagementMapper.doUpdate(schema_name, work_taskInfo, (String) work_taskInfo.get("id"));//保存发布任务
//            } else {
//                performanceManagementMapper.doSubmit(work_taskInfo);//保存发布任务
//            }
//            addPerformanceDetail(schema_name, work_taskInfo.get("id"), bomContentrole, role);
//            addPerformanceDetail(schema_name, work_taskInfo.get("id"), bomContentuserGroup, groups);
//            return ResponseModel.ok("ok");
//        }
//        return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PAGE_DATA_WRONG));//页面数据有问题，问题代码：S401
//    }
//
//    /**
//     * 保存考核计划 用户组和角色信息
//     *
//     * @param schema_name
//     * @param id
//     * @param content
//     * @param type
//     */
//    private void addPerformanceDetail(String schema_name, Object id, List<Map<String, Object>> content, String type) {
//        Map<String, Object> receiver = new HashMap<String, Object>();
//        if (id != null) {
//            if (content != null && content.size() > 0) {
//                for (Map<String, Object> con : content) {
//                    Object code = con.get("code");
//                    if (type.equals("role")) {
//                        receiver.put("plan_id", id);
//                        receiver.put("role_id", code);
//                        performanceManagementMapper.doSubmitRoles(schema_name, receiver);//任务接收人
//                    } else if (type.equals("group")) {
//                        receiver.put("plan_id", id);
//                        receiver.put("group_id", code);
//                        performanceManagementMapper.doSubmitGroups(schema_name, receiver);//任务接收人
//                    }
//                }
//            }
//        }
//    }
//
//
//    /**
//     * 绩效计划执行处理方法
//     *
//     * @param schema_name
//     */
//    @Override
//    public void performancePlanDeal(String schema_name) {
//        List<Map<String, Object>> mainPerformance = dealPerformanceManagement(schema_name);//处理考核计划在设定时间区间内的计划信息
//        if (!mainPerformance.isEmpty() && mainPerformance.size() > 0) {
//            int result = performanceManagementMapper.addPerformanceManagement(schema_name, mainPerformance);//批量插入到考核表信息中
//            if (result > 0) {
//                List<Map<String, Object>> needDetailList = dealPerformanceManagementDetail(schema_name, mainPerformance);//处理考核模板详情option信息添加到评分详情里面
//                if (!needDetailList.isEmpty() && needDetailList.size() > 0) {
//                    performanceManagementMapper.addPerformanceManageDetail(schema_name, needDetailList);//批量插入到课表信息详情中
//                }
//            }
//        }
//    }
//
//    /**
//     * 处理考核计划在设定时间区间内的计划信息
//     *
//     * @param schema_name
//     * @return
//     */
//    private List<Map<String, Object>> dealPerformanceManagement(String schema_name) {
//        List<Map<String, Object>> mainPerformance = new ArrayList<Map<String, Object>>();
//        Calendar ca = Calendar.getInstance();
//        ca.add(Calendar.MONTH, -1);//取上个月绩效
//        int m = ca.get(Calendar.MONTH) + 1;
//        String month = ca.get(Calendar.YEAR) + "-" + (m > 9 ? m : ("0" + m));
//        ca.add(Calendar.MONTH, 1);
//        ca.add(Calendar.YEAR, -1);//取去年绩效
//        String year = String.valueOf(ca.get(Calendar.YEAR));
//        List<Map<String, Object>> userPersonPlanDetail = performanceManagementMapper.getPerformancePlanPersonDetail(schema_name, month, year);//查询考核计划 待评价人信息 并关联了计划信息 个人
//        List<Map<String, Object>> userTeamPlanDetail = performanceManagementMapper.getPerformancePlanTeamDetail(schema_name, month, year);
//        if (!userPersonPlanDetail.isEmpty() && userPersonPlanDetail.size() > 0) {
//            for (Map<String, Object> person : userPersonPlanDetail) {
//                Map<String, Object> mainPerformanceMap = dealMainDetail(person, PERSON, month, year);//逻辑处理团队类型和个人类型数据
//                mainPerformance.add(mainPerformanceMap);
//            }
//        }
//        if (!userTeamPlanDetail.isEmpty() && userTeamPlanDetail.size() > 0) {
//            for (Map<String, Object> team : userTeamPlanDetail) {
//                Map<String, Object> mainPerformanceMap = dealMainDetail(team, TEAM, month, year);//逻辑处理团队类型和个人类型数据
//                mainPerformance.add(mainPerformanceMap);
//            }
//        }
//        return mainPerformance;
//    }
//
//    /**
//     * 逻辑处理团队类型和个人类型数据
//     *
//     * @param planDetail
//     * @return
//     */
//    private Map<String, Object> dealMainDetail(Map<String, Object> planDetail, String type, String month, String year) {
//        Timestamp now = new Timestamp(System.currentTimeMillis());//获取当前时间
//        Map<String, Object> mainPerformanceMap = new HashMap<String, Object>();
//        mainPerformanceMap.put("id", "");
//        mainPerformanceMap.put("total_points", 0);
//        mainPerformanceMap.put("has_score", false);
//        mainPerformanceMap.put("create_time", now);
//        if (type != null && type.equals(PERSON)) {//个人
//            mainPerformanceMap.put("user_id", planDetail.get("id"));
//            mainPerformanceMap.put("user_code", planDetail.get("user_code"));
//            mainPerformanceMap.put("user_account", planDetail.get("account"));
//        }
//        mainPerformanceMap.put("template_id", planDetail.get("template_id"));
//        mainPerformanceMap.put("create_user_account", planDetail.get("create_user_account"));
//        mainPerformanceMap.put("assessment_type", planDetail.get("assessment_type"));
//        if (type != null && type.equals(TEAM)) {//团队
//            mainPerformanceMap.put("group_id", planDetail.get("group_id"));
//        }
//        int period = (Integer) planDetail.get("period");
//        if (period == 1)
//            mainPerformanceMap.put("period", month);//月度绩效，月份
//        else
//            mainPerformanceMap.put("period", year);//年度绩效，年份
//
//        mainPerformanceMap.put("period_type", period);
//        return mainPerformanceMap;
//    }
//
//    /**
//     * 处理考核模板详情option信息添加到评分详情里面
//     *
//     * @param schema_name
//     * @param mainPerformance
//     * @return
//     */
//    private List<Map<String, Object>> dealPerformanceManagementDetail(String schema_name, List<Map<String, Object>> mainPerformance) {
//        List<Map<String, Object>> needDetailList = new ArrayList<>();
//        List<Map<String, Object>> templateOptions = performanceManagementMapper.getManagementDetail(schema_name);//查询考核模板详情option
//        if (templateOptions == null || templateOptions.isEmpty())
//            return needDetailList;
//
//        //查询模板考核项的考核标准（公式）列表数据
//        List<PerformanceTemplateOptionsEquation> equationList = performanceManagementMapper.queryPerformanceTemplateOptionsEquation(schema_name);
//        if (equationList == null || equationList.isEmpty())
//            return needDetailList;
//
//        Map<Long, List<PerformanceTemplateOptionsEquation>> equationMap = new HashMap<>();//考核项-考核标准关系表
//        for (PerformanceTemplateOptionsEquation equation : equationList) {
//            long key = equation.getTemplate_option_id();
//            if (equationMap.containsKey(key)) {
//                equationMap.get(key).add(equation);
//            } else {
//                equationMap.put(key, new ArrayList(Arrays.asList(equation)));
//            }
//        }
//        Map<Long, List<Map<String, Object>>> templateOptionMap = new HashMap<>();
//        for (Map<String, Object> templateOption : templateOptions) {
//            Long key = (Long) templateOption.get("template_id");
//            if (templateOptionMap.containsKey(key))
//                templateOptionMap.get(key).add(templateOption);
//            else
//                templateOptionMap.put(key, new ArrayList(Arrays.asList(templateOption)));
//        }
//        if (!mainPerformance.isEmpty() && mainPerformance.size() > 0) {
//            for (Map<String, Object> mainDetail : mainPerformance) {
//                Long pmId = Long.valueOf((String) mainDetail.get("id"));
//                List<Map<String, Object>> templateOptionList = templateOptionMap.get(mainDetail.get("template_id"));
//                if (templateOptionList != null) {
//                    boolean hasScore = true;//是否
//                    BigDecimal totalPoint = BigDecimal.ZERO;
//                    for (Map<String, Object> tp : templateOptionList) {
//                        Map<String, Object> needMap = new HashMap<>();
//                        needMap.put("pm_id", pmId);
//                        needMap.put("type", tp.get("type"));
//                        needMap.put("weightiness", tp.get("weightiness"));
//                        needMap.put("inspection_items", tp.get("inspection_items"));
//                        needMap.put("can_fill_in", tp.get("can_fill_in"));
//                        needMap.put("template_options_id", tp.get("id"));//新增template_options id
//                        Map<String, Object> result = resultNum(schema_name, tp, needMap, mainDetail, equationMap);
//                        if (!(Boolean) result.get("has_score"))
//                            hasScore = false;
//
//                        BigDecimal weightScore = (BigDecimal) result.get("weight_score");
//                        totalPoint.add(weightScore);
//                        needDetailList.add(result);
//                    }
//                    performanceManagementMapper.updatePerformanceScoreStatus(schema_name, totalPoint, hasScore, pmId);
//                }
//            }
//        }
//        return needDetailList;
//    }
//
//    /**
//     * 处理score、weight_score initial_score
//     *
//     * @param schema_name
//     * @param needMap
//     * @return
//     */
//    private Map<String, Object> resultNum(String schema_name, Map<String, Object> tp, Map<String, Object> needMap, Map<String, Object> mainPerformance, Map<Long, List<PerformanceTemplateOptionsEquation>> equationMap) {
//        Boolean bool_result = Boolean.parseBoolean(String.valueOf(tp.get("can_fill_in")));
//        Long templateOptionsId = (Long) needMap.get("template_options_id");
//        int types = Integer.parseInt(String.valueOf(tp.get("type")));
//        if (bool_result) {//如果是手填分数的考核项，则给上默认值，不用处理
//            needMap.put("score", BigDecimal.ZERO);
//            needMap.put("weight_score", BigDecimal.ZERO);
//            needMap.put("initial_score", BigDecimal.ZERO);
//            needMap.put("has_score", false);//是否已评分
//        } else {
//            int dashboard_id = Integer.parseInt(String.valueOf(tp.get("dashboard_id")));
//            DashboardConfigData dashboardConfigData = dashboardConfigService.findDashboardConfigData(schema_name, dashboard_id);
//            String grouoId = null;
//            String account = null;
//            String dataType = "";
//            String facilityId = null;
//            String showType = dashboardConfigData.getShowType();
//            String datasource = dashboardConfigData.getDatasource();
//            int periodType = Integer.parseInt(String.valueOf(mainPerformance.get("period_type")));
//            if (periodType == 1) {
//                dataType = MONTH;
//            } else if (periodType == 2) {
//                dataType = YEAR;
//            }
//            if (types == 1) {//个人
//                account = String.valueOf(mainPerformance.get("user_account"));
//            } else if (types == 2) {//团队
//                grouoId = String.valueOf(mainPerformance.get("group_id"));
//            }
//            Double dashboardScore = dashboardConfigService.getDashboardConfigKPI(schema_name, showType, datasource, dataType, facilityId, grouoId, account);
//            if (null == dashboardScore || dashboardScore.isNaN()) {
//                dashboardScore = 0.0;
//            }
//            BigDecimal initialScore = new BigDecimal(String.valueOf(dashboardScore)).setScale(SCORE_SCALE, RoundingMode.HALF_UP); //精确到小数点后两位
//            needMap.put("initial_score", initialScore);//初始得分（接口查询值）
//            //按绩效标准（公式）折算实际单项得分
//            BigDecimal score;
//            if (templateOptionsId == null || !equationMap.containsKey(templateOptionsId))
//                score = initialScore;//没有配置考核标准时（公式），即为初始打分
//            else
//                score = computeScore(initialScore, equationMap.get(templateOptionsId), SCORE_SCALE, RoundingMode.HALF_UP); //精确到小数点后两位
//
//            needMap.put("score", score);//实际得分（初始得分乘以 公式）
//            //计算权重得分
//            BigDecimal weightScore = score.multiply((BigDecimal) needMap.get("weightiness")).divide(new BigDecimal(100)).setScale(SCORE_SCALE, RoundingMode.HALF_UP); //精确到小数点后两位
//            needMap.put("weight_score", weightScore);//权重得分
//            needMap.put("has_score", true);//是否已评分
//        }
//        return needMap;
//    }
//
//    /**
//     * 使用公式计算总分
//     *
//     * @param schema_name
//     * @param request
//     * @return
//     */
//    @Override
//    public ResponseModel equationExecute(String schema_name, HttpServletRequest request) {
//        String optionScoreArray = request.getParameter("optionScoreArray");//考核项分数
//        if (StringUtils.isBlank(optionScoreArray))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        JSONArray array = JSONArray.fromObject(optionScoreArray);
//        if (array.size() == 0)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//
//        BigDecimal totalScore = BigDecimal.ZERO;
//        for (int i = 0; i < array.size(); i++) {
//            net.sf.json.JSONObject optionScore = array.getJSONObject(i);
//            BigDecimal score = new BigDecimal(optionScore.getString("score"));
//            BigDecimal weightiness = new BigDecimal(optionScore.getString("weightiness"));
//            long templateOptionsId = Long.parseLong(optionScore.getString("template_options_id"));
//            List<PerformanceTemplateOptionsEquation> equations = performanceManagementMapper.queryPerformanceTemplateOptionsEquationByTemplateOptionId(schema_name, templateOptionsId);
//            if (equations != null && equations.size() > 0)
//                score = computeScore(score, equations, SCORE_SCALE, RoundingMode.HALF_UP); //精确到小数点后两位
//
//            totalScore = totalScore.add(score.multiply(weightiness).divide(new BigDecimal(100)).setScale(SCORE_SCALE, RoundingMode.HALF_UP)); //精确到小数点后两位
//        }
//        return ResponseModel.ok(totalScore.toString());
//    }
}
