package com.gengyun.senscloud.service.asset;

import java.util.List;
import java.util.Map;

public interface AssetInfoService {

    /**
     * 验证设备编码是否存在
     *
     * @param schemaName
     * @param assetCodeList
     * @return
     */
    Map<String, Map<String, Object>> queryAssetCodeByCode(String schemaName, List<String> assetCodeList);

    /**
     * 验证设备名称是否存在
     *
     * @param schemaName
     * @param assetNameList
     * @return
     */
    Map<String, Map<String, Object>> queryAssetNameByName(String schemaName, List<String> assetNameList);

}
