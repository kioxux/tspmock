package com.gengyun.senscloud.service.login;

public interface HandleCacheService {
    String requestCompany(Long companyId, String key, String path);
}
