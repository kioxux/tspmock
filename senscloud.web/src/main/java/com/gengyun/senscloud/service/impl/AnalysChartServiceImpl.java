package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.AnalysChartService;
import org.springframework.stereotype.Service;

@Service
public class AnalysChartServiceImpl implements AnalysChartService {
//    @Autowired
//    AnalysChartMapper mapper;
//
//    //位置维修时效、故障率、保养完成率、保养效率的图表分析
//    @Override
//    public List<RepairAndMaintainChartAnalysIndicator> GetFacilityAnalysChart(String schema_name, String facilityCondition, String repairCondition, String maintainCondition, String orderBy) {
//        return mapper.GetFacilityAnalysChart(schema_name, facilityCondition, repairCondition, maintainCondition, orderBy);
//    }
//
//    //获取供应商设备故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、平均维修时长统计数据
//    @Override
//    public List<SupplierChartResult> GetSupplierAnalysChart(String schema_name, String facilityCondition, String repairCondition, String orderBy, int pageNumber) {
//        return mapper.GetSupplierAnalysChart(schema_name, facilityCondition, repairCondition, orderBy, pageNumber);
//    }
//
//
//    //设备维修时效、保养时效、故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、平均维修时长、维修次数、保养次数数据
//    @Override
//    public List<SupplierChartResult> GetDeviceAnalysChart(String schema_name, String facilityCondition, String repairCondition, String maintainCondition, String orderBy, int pageNumber) {
//        return mapper.GetDeviceAnalysChart(schema_name, facilityCondition, repairCondition, maintainCondition, orderBy, pageNumber);
//    }
//
//
//    //获取员工维修时效、保养时效、工作饱和度、保养时效占比、保养完成率、维修次数、保养次数数据
//    @Override
//    public List<UserRepairChartResult> GetUserAnalysChart(String schema_name, String repairCondition, String maintainCondition, String orderBy, int pageNumber) {
//        return mapper.GetUserAnalysChart(schema_name, repairCondition, maintainCondition, orderBy, pageNumber);
//    }
//
//    //按工单类型，查询工单的完成数和未完成数量
//    @Override
//    public Map<String, Object> getFinishedAndProcessingWorkTotal(String schema_name, String condition) {
//        return mapper.getFinishedAndProcessingWorkTotal(schema_name, condition);
//    }
//
//    //按工单类型，查询个各工单的完成数和未完成数量
//    @Override
//    public List<Map<String, Object>> getFacilityFinishedAndProcessingWorkTotal(String schema_name, String condition, int pageSize, int begin) {
//        return mapper.getFacilityFinishedAndProcessingWorkTotal(schema_name, condition, pageSize, begin);
//    }
//
//    //按从场地和设备位置，查询个工单的完成数和未完成数量
//    @Override
//    public List<Map<String, Object>> getFacilityFinishedAndProcessingWorkListForAsset(String schema_name, String condition, int pageSize, int begin) {
//        return mapper.getFacilityFinishedAndProcessingWorkListForAsset(schema_name, condition, pageSize, begin);
//    }
//
//    //按日期，统计所有费用
//    @Override
//    public Map<String, Object> getWorkFeeTotalByWorkType(String schema_name, String condition) {
//        return mapper.getWorkFeeTotalByWorkType(schema_name, condition);
//    }
//
//
//    //按日期，统计个各位置的费用
//    @Override
//    public List<Map<String, Object>> getWorkFeeTotalListByFacility(String schema_name, String condition, int pageSize, int begin) {
//        return mapper.getWorkFeeTotalListByFacility(schema_name, condition, pageSize, begin);
//    }
//
//    //按位置，统计各工单费用
//    @Override
//    public List<Map<String, Object>> getWorkFeeListForAsset(String schema_name, String condition, int pageSize, int begin) {
//        return mapper.getWorkFeeListForAsset(schema_name, condition, pageSize, begin);
//    }
//
//
//    //按工单类型，查询个各天的完成数和未完成数量
//    @Override
//    public List<Map<String, Object>> getFinishedAndProcessingWorkTotalByDay(String schema_name, String condition, String select_day) {
//        return mapper.getFinishedAndProcessingWorkTotalByDay(schema_name, condition, select_day);
//    }
//
//    //按位置，统计各天费用
//    @Override
//    public List<Map<String, Object>> getWorkFeeTotalListByDay(String schema_name, String condition) {
//        return mapper.getWorkFeeTotalListByDay(schema_name, condition);
//    }
}
