package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MonitorAreaComponentService;
import org.springframework.stereotype.Service;
@Service
public class MonitorAreaComponentServiceImpl implements MonitorAreaComponentService {
//
//    @Autowired
//    MonitorAreaComponentMapper monitorAreaComponentMapper;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Override
//    public JSONObject query(String schemaName, String searchKey, Integer pageSize, Integer pageNumber,String area, String facilityId, User loginUser) {
//        if (pageNumber == null) {
//            pageNumber = 1;
//        }
//        if (pageSize == null) {
//            pageSize = 20;
//        }
//        String condition = "";
//        if (StringUtils.isNotEmpty(searchKey)) {
//            condition+="and cm.component_name like '%"+searchKey+"%'";
//        }
//        if (StringUtils.isNotEmpty(area)) {
//            condition+="and ma.monitor_area_code = '"+area+"'";
//        }
//        String[] facilitys={};
//        if(StringUtils.isNotEmpty(facilityId)){
//            facilitys=facilityId.split(",");
//        }
//        //获取组织的根节点数据，用于like进行比较
//        String[] facilityNos = dataPermissionForFacility.findFacilityNOsByBusinessForSingalUser(schemaName, loginUser, facilitys, "asset_monitor");
//        if (facilityNos != null && facilityNos.length > 0) {
//            condition += " and (";
//            for (String facilityNo : facilityNos) {
//                condition += " p.position_code like '" + facilityNo + "%' or"+" f.facility_no like '"+facilityNo+"%' or";
//            }
//            condition = condition.substring(0, condition.length() - 2);
//            condition += ") ";
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> result = monitorAreaComponentMapper.getMonitorAreaComponentList(schemaName,condition, pageSize, begin);
//        int total = monitorAreaComponentMapper.countMonitorAreaComponentList(schemaName, condition);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", result);
//        return pageResult;
//    }
//
//    @Override
//    public ResponseModel addMonitorAreaComponent(Map<String, Object> paramMap,List<String> asset_ids) {
//        String schemaName=(String)paramMap.get("schema_name");
//        if("on".equals(paramMap.get("is_iot_asset"))){
//            paramMap.put("is_iot_asset",1);
//        }else{
//            paramMap.put("is_iot_asset",0);
//        }
//        int count = monitorAreaComponentMapper.addMonitorAreaComponent(paramMap);
//        if (count > 0) {
//                int component_id=Integer.parseInt((String)paramMap.get("id"));
//                for(String asset_id:asset_ids){
//                    monitorAreaComponentMapper.addMonitorAreaComponentAsset(schemaName,component_id,asset_id);
//                }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel updateMonitorAreaComponent(Map<String, Object> paramMap,List<String> asset_ids) {
//        String schemaName=(String)paramMap.get("schema_name");
//        int component_id=Integer.parseInt((String)paramMap.get("id"));
//        monitorAreaComponentMapper.deleteMonitorAreaComponentAssetById(schemaName,component_id);
//
//        int count = monitorAreaComponentMapper.updateMonitorAreaComponent(paramMap);
//        if (count > 0) {
//            monitorAreaComponentMapper.deleteMonitorAreaComponentAssetById(schemaName,component_id);
//            for(String asset_id:asset_ids){
//                monitorAreaComponentMapper.addMonitorAreaComponentAsset(schemaName,component_id,asset_id);
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel deleteById(String schemaName, HttpServletRequest request) {
//        int count = monitorAreaComponentMapper.deleteMonitorAreaComponentById(schemaName,Integer.parseInt(request.getParameter("id")));
//        count+=monitorAreaComponentMapper.deleteMonitorAreaComponentAssetById(schemaName,Integer.parseInt(request.getParameter("id")));
//        if (count > 0) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_I));//操作成功
//        }else{
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//    }
//
//    @Override
//    public ResponseModel findIotAsset(String schemaName, HttpServletRequest request) {
//        User loginUser = AuthService.getLoginUser(request);
//        String[] asset_types = {};
//        String[] facilities = {};
//        try {
//            String facilityInfo = (String) request.getParameter("facilities");
//            if (null != facilityInfo && !"".equals(facilityInfo)) {
//                facilities = facilityInfo.split(",");
//            }
//            String keyword = request.getParameter("keyword");
//            String whereString=assetDataServiceV2.getAssertWhereString(schemaName, loginUser, asset_types, null, null, facilities, null, null, keyword, false, false,false, null,0);
//            whereString += " ORDER BY t1.strcode,t1._id ";
//            return ResponseModel.ok(assetMapper.searchAssetList(schemaName, whereString));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));//操作失败！
//        }
//
//    }
//    @Override
//    public Map<String, Object> findMonitorAreaComponentById(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String id=request.getParameter("id");
//        return monitorAreaComponentMapper.getmonitorAreaComponentById(schemaName,Integer.parseInt(id));
//    }
//
//    @Override
//    public ResponseModel findIotAssetByComponentId(String schemaName, HttpServletRequest request) {
//        String id=request.getParameter("id");
//        List<Map<String, Object>> result=monitorAreaComponentMapper.getmonitorAreaComponentAssetById(schemaName,Integer.parseInt(id));
//        if(result.isEmpty()){
//            return ResponseModel.ok("{}");
//        }
//        return ResponseModel.ok(result);
//    }
}
