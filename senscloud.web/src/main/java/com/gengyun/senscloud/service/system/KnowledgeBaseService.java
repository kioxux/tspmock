package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.MethodParam;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface KnowledgeBaseService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getKnowledgeBaseListPermission(MethodParam methodParam);

    /**
     * 获取知识库列表
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    Map<String, Object> getKnowledgeBaseListForPage(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 获取知识库明细信息
     *
     * @param methodParam
     * @param paramMap
     * @return
     */
    Map<String, Object> getKnowledgeBaseDetailById(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 根据选中主键下载知识库信息
     *
     * @param methodParam
     * @param paramMap
     */
    void getExportKnowledgeBaseCode(MethodParam methodParam, Map<String, Object> paramMap, HttpServletResponse response);
}