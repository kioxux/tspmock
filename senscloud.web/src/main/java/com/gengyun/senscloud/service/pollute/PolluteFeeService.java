package com.gengyun.senscloud.service.pollute;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PolluteFeeModel;

import java.util.List;
import java.util.Map;

public interface PolluteFeeService {

    Map<String, Object> findPolluteFeeList(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> findPolluteFeeStatistics(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findsearchPolluteFeeCalendar(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Object> findPolluteFeeDetailInfo(MethodParam methodParam, java.util.Map<String, Object> paramMap);

    void modifyPolluteFee(MethodParam methodParam, Map<String, Object> paramMap);

    void auditPolluteFee(MethodParam methodParam, PolluteFeeModel model);

    void autoPolluteFeeSeting(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findPolluteFeeSetingInfo(MethodParam methodParam, Map<String, Object> paramMap);

    void asyncCronJobToGeneratePolluteFree(String schemaName);

    Map<String, Object> recalculatePolluteFeeDetailInfo(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Map<String, Object>> queryCusNameByName(String schemaName, List<String> cusNameList);

    int findPolluteFeeByCondition(String schemaName, Map<String, Object> paramMap);

    void insertPolluteFee(String schemaName, Map<String, Object> paramMap);

    void insertPolluteFeeNormalDetail(String schemaName, Map<String, Object> paramMap);

    void insertPolluteFeeExcDetail(String schemaName, Map<String, Object> paramMap);

    int findPolluteFeeByUpdate(String schemaName, Map<String, Object> paramMap);

    int updatePolluteFee(String schemaName, Map<String, Object> paramMap);

    int updatePolluteFeeNormalDetail(String schemaName, Map<String, Object> paramMap);

    int updatePolluteFeeExcDetail(String schemaName, Map<String, Object> paramMap);

    Map<String, Map<String, Boolean>> getPolluteFeePermission(MethodParam methodParam);
}
