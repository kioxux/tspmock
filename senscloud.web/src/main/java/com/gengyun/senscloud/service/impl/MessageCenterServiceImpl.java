package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MessageCenterService;
import org.springframework.stereotype.Service;

/**
 * MessageCenterServiceImpl class 消息中心业务接口实现
 *
 * @author Zys
 * @date 2020/03/19
 */
@Service
public class MessageCenterServiceImpl implements MessageCenterService {
//
//    private static final Logger logger = LoggerFactory.getLogger(MessageCenterServiceImpl.class);
//
//    @Autowired
//    MessageCenterMapper messageCenterMapper;
//
//    @Autowired
//    MessageService messageService;
//
//    @Autowired
//    EmailUtil emailUtil;
//
//    @Autowired
//    private CompanyService companyService;
//
//    /**
//     * 插入消息历史表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public void insertHistoryMessageColumn(String schema_name, Map<String, Object> paramMap) {
//        try {
//            messageCenterMapper.insertHistoryMessageColumn(schema_name, paramMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 插入消息中心表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public void insertMessageCenterColumn(String schema_name, Map<String, Object> paramMap) {
//        try {
//            paramMap.put("schema_name", schema_name);
//            messageCenterMapper.insertMessageCenterColumn(paramMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 插入消息接受表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    @Override
//    public void insertMessageReceiverColumn(String schema_name, Map<String, Object> paramMap) {
//        try {
//            messageCenterMapper.insertMessageReceiverColumn(schema_name, paramMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 查询未发送的消息
//     *
//     * @param schema_name
//     * @return
//     */
//    @Override
//    public List<Map<String, Object>> queryMessages(String schema_name) {
//        return messageCenterMapper.queryMessages(schema_name);
//    }
//
//    /**
//     * 修改消息表状态
//     *
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Override
//    public synchronized int updateSendStatus(String schema_name, Long id) {
//        return messageCenterMapper.UpdateSendStatus(schema_name, id);
//    }
//
//    /**
//     * 发送消息
//     *
//     * @param schema_name
//     * @param message
//     * @return
//     */
//    @Override
//    @Transactional
//    public void sendMessage(String schema_name, Map<String, Object> message) {
//        try {
//            boolean isSms = (boolean) message.get("is_sms");
//            boolean isEmail = (boolean) message.get("is_email");
//            boolean isWeChat = (boolean) message.get("is_weixin");
//            boolean isAPP = (boolean) message.get("is_app");
//            Long id = (Long) message.get("id");
//            String messageContent = String.valueOf(message.get("msg_content")),
//                    messageSmsContent = String.valueOf(message.get("real_msg_content")),
//                    messageWxoaMsgContent = String.valueOf(message.get("real_wx_oa_msg_content")),
//                    businessType = String.valueOf(message.get("business_type")),
//                    fileIds = String.valueOf(message.get("file_id")),
//                    businessNo = String.valueOf(message.get("business_no"));
//            //无效的消息
//            List<User> receivers = messageCenterMapper.queryReceiver(schema_name, id);
//            //不许要发送
//            if (!isEmail && !isWeChat && !isSms && !isAPP) {
//                return;
//            }
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            String companyId = schema_name.replace(SensConstant.SCHEMA_PREFIX, "");
//            Company company = companyService.findById(Long.parseLong(companyId));
//            if (null == company) {
//                return;
//            }
//            Boolean isOpenSms = company.getIs_open_sms();
//            if (null == isOpenSms || !isOpenSms) {
//                message.put("is_sms",false);
//                isSms=false;
//            }
//            Boolean isOpenEmail= company.getIs_open_email();
//            if (null == isOpenEmail || !isOpenEmail) {
//                message.put("is_email",false);
//                isEmail=false;
//            }
//            String receiveAccount;
//            for (User receiver : receivers) {
//                receiveAccount = receiver.getAccount();
//                if (RegexUtil.isNull(receiveAccount)) {
//                    continue;
//                }
//                message.put("send_time", now);
//                message.put("receive_account", receiveAccount);
//                //发送短信
//                if (isSms && RegexUtil.isNotNull(messageSmsContent) && RegexUtil.isNotNull(receiver.getMobile())) {
//                    messageService.sendMsg(schema_name, null, null, receiver.getAccount(),
//                            businessType, businessNo, receiver.getMobile(), messageSmsContent);
//                }
//                //发送短信
//                if (isEmail && RegexUtil.isNotNull(messageContent) && RegexUtil.isNotNull(receiver.getEmail())) {
//                    Map<String, Object>[] fileMap = null;
//                    if (RegexUtil.isNotNull(fileIds)) {
//                        String[] files = fileIds.split(",");
//                        fileMap = new HashMap[files.length];
//                    }
//                    //TODO 主题定什么： 服务邮件提醒--谢晋
//                    emailUtil.sendEmail(receiver.getEmail(), null, "服务邮件提醒", messageContent, fileMap);
//                }
//                //TODO 微信消息推送
//                if (isWeChat) {
//                    //发送微信公众号模板消息
//                    messageService.sendWxOfficialAccountMsg(schema_name, null, null, receiveAccount, businessType, businessNo, receiver.getMobile(), messageWxoaMsgContent);
//                }
//                //TODO app消息推送
//                if (isAPP) {
//                }
//                messageCenterMapper.insertHistoryMessageColumn(schema_name, message);
//            }
//            this.updateSendStatus(schema_name, id);
//        } catch (Exception e) {
//            logger.error("sendMessageError:" + e.getMessage());
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//        }
//    }
//
//    /**
//     * 按条件查询消息
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public JSONObject getMessageList(String schema_name, HttpServletRequest request, String account) {
//        int pageSize = 10;
//        int pageNumber = 1;
//        String unRead="un_read";
//        String pageSizeStr = request.getParameter("pageSize");
//        String pageNumberSre = request.getParameter("pageNumber");
//        String queryType = request.getParameter("queryType");
//        if (RegexUtil.isNotNull(pageSizeStr) && RegexUtil.isNotNull(pageNumberSre)) {
//            pageSize = Integer.parseInt(pageSizeStr);
//            pageNumber = Integer.parseInt(pageNumberSre);
//        }
//        String condition = "AND mc.is_pc='t' AND mr.receive_account='" + account + "'";
//        //查询未读
//        if(RegexUtil.isNotNull(queryType)){
//            if(unRead.equals(queryType)){
//                condition += "AND  mr.is_read='f'";
//            }
//        }
//        int begin = pageSize * (pageNumber - 1);
//        List<Map<String, Object>> messageList = messageCenterMapper.getMessageList(schema_name, condition, pageSize, begin);
//        int total = messageCenterMapper.getMessageListCount(schema_name, condition);
//        JSONObject pageResult = new JSONObject();
//        pageResult.put("total", total);
//        pageResult.put("rows", messageList);
//        return pageResult;
//    }
//
//    /**
//     * 按条件查询消息数量
//     *
//     * @param account
//     * @return
//     */
//    @Override
//    public int getMessageListCount(String schema_name, String account) {
//        String condition = "AND mc.is_pc='t'AND mr.is_read='f' AND mr.receive_account='" + account + "'";
//        return messageCenterMapper.getMessageListCount(schema_name, condition);
//    }
//
//    /**
//     * 修改消息状态
//     *
//     * @param request
//     * @return
//     */
//    @Override
//    public int UpdateReadStatus(String schema_name, HttpServletRequest request) {
//        int count = 0;
//        Boolean readAll=false;
//        String id = request.getParameter("id");
//        try{
//            readAll=Boolean.parseBoolean(request.getParameter("read_all"));
//        }catch (Exception e){
//        }
//        if (RegexUtil.isNull(id)&&!readAll) {
//            return count;
//        }
//        //全部已读 单条已读
//        if(readAll){
//            count = messageCenterMapper.UpdateReadStatusByAccount(schema_name, AuthService.getLoginUser(request).getAccount());
//        }else if(RegexUtil.isNotNull(id)){
//            count = messageCenterMapper.UpdateReadStatus(schema_name, Integer.parseInt(id));
//        }
//        return count;
//    }


}
