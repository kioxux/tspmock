package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

public interface PlanWorkCalendarService {

    /**
     * 获取维保行事历模块权限
     *
     * @param methodParam
     * @return
     */
    Map<String, Map<String, Boolean>> getPlanWorkCalendarPermission(MethodParam methodParam);


    /**
     * 查询维保行事历列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    Map<String, Object> getPlanWorkCalendarList(MethodParam methodParam, SearchParam param);


    /**
     * 计划行事历生成工单
     *
     * @param methodParam
     * @param work_calendar_code
     */
    void generateWork(MethodParam methodParam, String work_calendar_code);

    /**
     * 编辑计划行事历
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWork(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 作废计划行事历
     *
     * @param methodParam
     * @param work_calendar_code
     */
    void removeWork(MethodParam methodParam, String work_calendar_code);

    /**
     * 作废选中计划行事历
     *
     * @param methodParam 系统参数
     * @param param       请求参数
     */
    void deletePlanCalendarByIds(MethodParam methodParam, SearchParam param);

    /**
     * 根据id查询计划行事历详情
     *
     * @param methodParam
     * @param work_calendar_code
     * @return
     */
    Map<String, Object> getPlanWorkCalendarInfo(MethodParam methodParam, String work_calendar_code);

    /**
     * 导出选中维保计划
     *
     * @param methodParam
     * @param pm
     * @return
     */
    ModelAndView exportPlanWorkCalendar(MethodParam methodParam, SearchParam pm);

    /**
     * 导出全部选中维保计划
     *
     * @param methodParam
     * @param pm
     * @return
     */
    ModelAndView exportAllPlanWorkCalendar(MethodParam methodParam, SearchParam pm);

}
