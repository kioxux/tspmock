package com.gengyun.senscloud.service;

/**
 * 工单模板
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
public interface MetadataWorkService {
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param workTemplateName
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param type
//     * @return
//     */
//    JSONObject query(String schemaName, String workTemplateName, Integer pageSize, Integer pageNumber,
//                     String sortName, String sortOrder, String type);
//
//
//    //查询工单列表
//    List<MetadataWork> queryRootList(String schemaName, String type);
//
//    /**
//     * 保存数据
//     *
//     * @param schemaName
//     * @param loginUser
//     * @param metadataWork
//     * @param type
//     */
//    void saveMetadataWork(String schemaName, User loginUser, MetadataWork metadataWork, String type);
//
//    /**
//     * 根据主键查询数据
//     *
//     * @param schemaName
//     * @param workTemplateCode
//     * @return
//     */
//    ResponseModel queryById(String schemaName, String workTemplateCode);
//
//    /**
//     * 根据主键查询数据（PC业务取模版用）
//     *
//     * @param schemaName
//     * @param workTemplateCode
//     * @return
//     */
//    ResponseModel queryMwInfoById(String schemaName, HttpServletRequest request, String workTemplateCode);
//
//    /**
//     * 更新使用状态
//     *
//     * @param schemaName
//     * @param request
//     * @return
//     */
//    ResponseModel updateStatus(String schemaName, HttpServletRequest request);
//
//    /**
//     * 查询所有可用的模板名称和编号
//     *
//     * @param schemaName
//     * @param type
//     * @param isCommonTemplate
//     * @return
//     */
//    List<Map<String, Object>> queryCodeAndDesc(String schemaName, String type, String isCommonTemplate);
//
//    /**
//     * 查询所有可用的公共模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    List<Map<String, Object>> commonTemplateList(String schemaName);
//
//    /**
//     * 按条件查询工单列表（不分页）
//     *
//     * @param schemaName
//     * @param condition
//     * @return MetadataWork
//     */
//    List<MetadataWork> queryAllRootList(String schemaName, String condition);
//
//    /**
//     * 查询可用的系统模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    Map<String, Object> getSystemTemplate(String schemaName);
}
