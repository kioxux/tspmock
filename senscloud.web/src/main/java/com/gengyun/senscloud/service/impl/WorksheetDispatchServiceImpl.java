package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.mapper.WorksheetDispatchMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.model.WorkListModel;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.WorksheetDispatchService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WorksheetDispatchServiceImpl implements WorksheetDispatchService {

    @Resource
    WorksheetDispatchMapper worksheetDispatchMapper;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    CompanyService companyService;

    /**
     * 查询已分配工单列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorksheetDistributionList(MethodParam methodParam, SearchParam param) {
        String schema_name = methodParam.getSchemaName();
        String uid = RegexUtil.optStrOrNull(methodParam.getUserId());
        String ks = RegexUtil.optStrOrNull(param.getKeywordSearch());
        List<Map<String, Object>> userList = this.getUser(schema_name, uid, ks);
        List<Map<String, Object>> ucList = worksheetDispatchMapper.findWorkListForCount(schema_name, uid, ks);
        Map<String, Long> ucInfo = null;
        Map<String, Long> ucOtInfo = null;
        if (null != ucList && ucList.size() > 0) {
            ucInfo = ucList.stream().collect(Collectors.groupingBy(um -> RegexUtil.optStrOrBlank(um.get("duty_user_id")), Collectors.counting()));
            ucOtInfo = ucList.stream().filter(um -> RegexUtil.optEquals(um.get("is_overtime"), "true")).collect(Collectors.groupingBy(um -> RegexUtil.optStrOrBlank(um.get("duty_user_id")) + RegexUtil.optStrOrBlank(um.get("is_overtime")), Collectors.counting()));
        }
        ucInfo = RegexUtil.optMapLongOrNew(ucInfo);
        ucOtInfo = RegexUtil.optMapLongOrNew(ucOtInfo);
        String distributionWhereString = getDistributionWhereString(methodParam, param);
        List<WorkListModel> workDataList = findWorksheetList(schema_name, methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), distributionWhereString);
        Map<String, List<WorkListModel>> wdInfo = null;
        if (null != workDataList && workDataList.size() > 0) {
            wdInfo = workDataList.stream().collect(Collectors.groupingBy(WorkListModel::getDuty_user_id));
        }
        if (null == wdInfo || wdInfo.size() == 0) {
            wdInfo = new HashMap<>();
        }
        Map<String, Map<String, String>> business = worksheetDispatchMapper.findBusinessTypeMap(schema_name);
        List<Map<String, Object>> listWdb = new ArrayList<>();
        for (Map<String, Object> t : userList) {
            if (!RegexUtil.optIsPresentMap(t) || !RegexUtil.optIsPresentStr(t.get("id"))) {
                continue;
            }
            Map<String, Object> wdb = new HashMap<>();
            String user_id = t.get("id").toString();
            wdb.put("receive_name", t.get("user_name").toString());
            wdb.put("receive_user_id", user_id);
            wdb.put("workload", RegexUtil.optIntegerOrVal(ucInfo.get(user_id), 0, methodParam, "", null));
            wdb.put("workListModel", wdInfo.get(user_id));
            wdb.put("overtimeCount", RegexUtil.optIntegerOrVal(ucOtInfo.get(user_id + "true"), 0, methodParam, "", null));
            listWdb.add(wdb);
        }
//        listWdb.sort((o1, o2) -> o2.get("workload").toString().compareTo(o1.get("workload").toString()));

//        List<WorkSheetDistribution> collect = listWdb.stream().sorted(Comparator.comparing(c->{}).reversed()).collect(Collectors.toList());
//        listWdb.sort((((o1, o2) -> o2.getWorkload() - o1.getWorkload())));
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT);
        for (Map<String, Object> map : listWdb) {
            Object o = map.get("workListModel");
            if (null != o) {
                List<WorkListModel> list = (List<WorkListModel>) o;
                List<String> collect = list.stream().map(WorkListModel::getReceive_time).filter(RegexUtil::optIsPresentStr).map(sdf::format).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
                for (String date : collect) {
                    ArrayList<Map<String, Object>> dataList = new ArrayList<>();
                    int count1001 = 0;
                    int count1002 = 0;
                    int count1003 = 0;
                    int count1004 = 0;
                    String code1001 = "";
                    String code1002 = "";
                    String code1003 = "";
                    String code1004 = "";
                    for (WorkListModel model : list) {
                        String begin_time = RegexUtil.optNotBlankStrOpt(model.getReceive_time()).map(rt -> sdf.format(model.getReceive_time())).orElse("");
                        if (date.equals(begin_time)) {
                            Integer business_type_id = model.getBusiness_type_id();
                            String sub_work_code = model.getSub_work_code();
                            switch (business_type_id) {
                                case 1001:
                                    count1001++;
                                    code1001 += sub_work_code + ",";
                                    break;
                                case 1002:
                                    count1002++;
                                    code1002 += sub_work_code + ",";
                                    break;
                                case 1003:
                                    count1003++;
                                    code1003 += sub_work_code + ",";
                                    break;
                                case 1004:
                                    count1004++;
                                    code1004 += sub_work_code + ",";
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if (count1001 > 0) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("business_type_id", SensConstant.BUSINESS_NO_1001);
                        data.put("order_number", count1001);
                        data.put("order_name", RegexUtil.optStrOrVal(business.get("1001").get("business_name"), "维修"));
                        data.put("sub_work_codes", code1001.substring(0, code1001.length() - 1));
                        dataList.add(data);
                    }


                    if (count1002 > 0) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("business_type_id", SensConstant.BUSINESS_NO_1002);
                        data.put("order_number", count1002);
                        data.put("order_name", RegexUtil.optStrOrVal(business.get("1002").get("business_name"), "保养"));
                        data.put("sub_work_codes", code1002.substring(0, code1002.length() - 1));
                        dataList.add(data);
                    }


                    if (count1003 > 0) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("business_type_id", SensConstant.BUSINESS_NO_1003);
                        data.put("order_number", count1003);
                        data.put("order_name", RegexUtil.optStrOrVal(business.get("1003").get("business_name"), "巡检"));
                        data.put("sub_work_codes", code1003.substring(0, code1003.length() - 1));
                        dataList.add(data);
                    }

                    if (count1004 > 0) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("business_type_id", SensConstant.BUSINESS_NO_1004);
                        data.put("order_number", count1004);
                        data.put("order_name", RegexUtil.optStrOrVal(business.get("1004").get("business_name"), "点检"));
                        data.put("sub_work_codes", code1004.substring(0, code1004.length() - 1));
                        dataList.add(data);
                    }

                    map.put(date, dataList);
                    map.put("workListModel", null);
                }
            }
        }
        return listWdb;
    }

    /**
     * 查询待分配工单信息列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getWorksheetUndistributedList(MethodParam methodParam, SearchParam param) {
        String pagination = SenscloudUtil.changePagination(methodParam);//分页参数
        String condition = getUndistributedWhereString(param);
        int total = worksheetDispatchMapper.findCountWorksheetList(methodParam.getSchemaName(), methodParam.getUserId(), condition);
        Map<String, Object> result = new HashMap<String, Object>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            String sort = RegexUtil.optIsPresentStr(param.getDescStatusSearch()) ? param.getDescStatusSearch() : "desc";
            condition += " ORDER BY to_char(wd.begin_time, 'yyyy-MM-dd') " + sort + ",work_code desc";
            condition += pagination;
            result.put("rows", findWorksheetList(methodParam.getSchemaName(), methodParam.getUserId(), methodParam.getUserLang(), methodParam.getCompanyId(), condition));
        }
        result.put("total", total);
        return result;
    }

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    @Override
    public Map<String, Map<String, Boolean>> getWorksheetDispatchPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[15]);
    }

    /**
     * 根据子工单编号查询工单信息列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<WorkListModel> getWorkListDetailBySwc(MethodParam methodParam, SearchParam param) {
        String subWorkCodes = param.getSubWorkCodes();
        List<WorkListModel> workListDetailModels = new ArrayList<>();
        if (RegexUtil.optIsPresentStr(subWorkCodes)) {
            String[] idStr = subWorkCodes.split(",");
            workListDetailModels = cacheUtilService.findWorkListDetailBySwc(methodParam.getSchemaName(), idStr, methodParam.getUserLang(), methodParam.getCompanyId());
        }
        return workListDetailModels;
    }

    /**
     * 获取未分配的工单列表
     *
     * @param param
     * @return
     */
    private String getUndistributedWhereString(SearchParam param) {
        StringBuffer whereString = new StringBuffer(" and wd.status = " + StatusConstant.UNDISTRIBUTED);
        RegexUtil.optNotBlankStrOpt(param.getWorkTypeIds()).ifPresent(e -> whereString.append(" and wd.work_type_id in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        RegexUtil.optNotBlankStrOpt(param.getBeginDateSearch()).ifPresent(e -> whereString.append(" and (to_date(wd.begin_time::varchar, 'yyyy-MM-dd') >= to_date('").append(e).append("', 'yyyy-MM-dd') ").append(") "));
        RegexUtil.optNotBlankStrOpt(param.getEndDateSearch()).ifPresent(e -> whereString.append(" and (to_date(wd.begin_time::varchar, 'yyyy-MM-dd') <= to_date('").append(e).append("', 'yyyy-MM-dd') ").append(") "));
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (upper(w.work_code) like upper(")
                .append(e).append(") or lower(ap.position_name) like ").append(e).append(" or lower(w.position_code) like ").append(e).append(")"));
        return whereString.toString();
    }

    /**
     * 获取人员
     *
     * @param schema_name
     * @param user_id
     * @param condition
     * @return
     */
    private List<Map<String, Object>> getUser(String schema_name, String user_id, String condition) {
        return worksheetDispatchMapper.getUser(schema_name, user_id, condition);
    }


    /**
     * 获取已分配工单列表的条件
     *
     * @param methodParam 系统参数
     * @param param       请求参数
     * @return 拼接sql
     */
    private String getDistributionWhereString(MethodParam methodParam, SearchParam param) {
        StringBuffer whereString = new StringBuffer(" and wd.duty_user_id is not null and wd.duty_user_id <> '' and wd.status = " + StatusConstant.PENDING);
//        RegexUtil.optNotBlankStrOpt(param.getBeginDateSearch()).ifPresent(e -> whereString.append(" and (to_date(wd.plan_begin_time::text, 'yyyy-MM-dd') >= to_date('").append(e).append("', 'yyyy-MM-dd') ").append(") "));
//        RegexUtil.optNotBlankStrOpt(param.getEndDateSearch()).ifPresent(e -> whereString.append(" and (to_date(wd.plan_begin_time::text, 'yyyy-MM-dd') <= to_date('").append(e).append("', 'yyyy-MM-dd') ").append(") "));
        RegexUtil.optNotBlankStrOpt(param.getBeginDateSearch()).ifPresent(e -> whereString.append(" and (to_char(wd.receive_time, 'yyyy-MM-dd') >= '").append(e).append("' ").append(") "));
        RegexUtil.optNotBlankStrOpt(param.getEndDateSearch()).ifPresent(e -> whereString.append(" and (to_char(wd.receive_time, 'yyyy-MM-dd') <= '").append(e).append("' ").append(") "));
//        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e + "%'").ifPresent(e -> whereString.append(" and (upper(w.work_code) like upper(")
//                .append(e).append(") or lower(a.asset_name) like ").append(e).append(" or lower(a.asset_code) like ").append(e).append(")"));
        return whereString.toString();
    }

    public List<WorkListModel> findWorksheetList(String schemaName, String userId, String userLang, Long companyId, String condition) {
        List<WorkListModel> workDataList = worksheetDispatchMapper.findWorksheetList(schemaName, userId, condition);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticDataByType")));
        if (RegexUtil.optIsPresentList(workDataList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                JSONObject dd = JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                workDataList.forEach(sd -> {
                    if (dd.containsKey(sd.getPriority_level_name())) {
                        String value = RegexUtil.optStrOrNull(JSONObject.fromObject(dd.get(sd.getPriority_level_name())).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.setPriority_level_name(value);
                        }
                    }
                });
            }
        }
        return workDataList;
    }

}
