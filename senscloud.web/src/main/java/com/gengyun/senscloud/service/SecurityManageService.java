package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 职安健管理
 */
public interface SecurityManageService {
    /**
     * 查询列表
     *
     * @param paramMap 页面条件
     * @return 列表数据
     */
    Map<String, Object> getSecurityManage(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 通过code查询详情
     *
     * @param paramMap 页面条件
     * @return 列表数据
     */
    Map<String, Object> getSecurityDetailByCode(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 更新职安健
     *
     * @param paramMap 页面条件
     */
    void modifySecurityManage(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除职安健
     *
     * @param paramMap 页面条件
     */
    void cutSecurityManage(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 新增职安健
     *
     * @param paramMap 页面条件
     */
    void newSecurityManage(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 有所职安健列表
     */
    List<Map<String, Object>> getSecurityManageList(MethodParam methodParam, Map<String, Object> paramMap);

    Map<String, Map<String, Object>> findSafeItemsByItemCode(String schemaName, List<String> itemCodeList);
}
