package com.gengyun.senscloud.service.dynamic;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/9.
 */
public interface WorkScheduleService {
    //    //新增备件
//    int insertWorkSchedule(String schema_name, WorkScheduleData workScheduleData);
//
//    //删除备件
//    int deleteWorkSchedule(String schema_name, int id);
//
//    //更新备件信息
//    int updateWorlSchedule(String schema_name, WorkScheduleData workScheduleData);
//
//    //根据ID查找备件信息
//    WorkScheduleData findById(String schema_name, int id);
//
//    //获取备件列表
//    List<WorkScheduleData> getWorkScheduleList(String schema_name);
//
//    //按用户组织和日期，获取排班数据
//    List<WorkScheduleData> getWorkScheduleListByGroup(String schema_name, String condition);
//
//    WorkScheduleData getWorkScheduleListByFacilityById(String schema_name, int id);
//
//    // 改为新方法，去掉 单独获取人的方法
////    //根据工单池、位置和人员权限查询对应排班人员,在固定时间上班的人员
////    List<User> getWorkScheduleByPoolAndSiteAndTimeOnDuty(String schema_name, long facility_id, String pool_id, String functionKey, Timestamp beginDateTime, Timestamp endDateTime);
////
////    //根据位置和人员权限查询对应排班人员,在固定时间上班的人员
////    List<User> getWorkScheduleBySiteAndTimeOnDuty(String schema_name, long facility_id, String functionKey, Timestamp beginDateTime, Timestamp endDateTime);
//
//    //拖动更新时间
//    int updateDate(String schema_name, int id, Timestamp begin_time, Timestamp end_time);
//
////    List<User> getWorkScheduleBySiteAndTime(String schema_name, long facilityId, String functionKey);
//
////    List<User> getWorkScheduleBySiteAndTimeInterval(String schema_name, long facilityId, String functionKey);
//
//    List<WorkScheduleData> findByFacilities(String schema_name, String facilities);
//
//    //获取当天排班联系负责人
//    List<LeadingOfficialResult> getWorkScheduleLeadingOfficial(String schema_name, Timestamp moningTime, Timestamp afternoonTime);
//
//    //按工单类型，获取有操作权限的用户
//    //facilityNO（场地NO）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //roleIds: 角色id,多个时，逗号隔开
//    List<User> getDoTaskUserByWorkTypeRoleIds(String schema, String facilityNO, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, String roleIds, int category_id);
//
//    // LSP工单处理变更-20190529-sps start
//    //facilityNO（场地NO）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //rolePermissionKey: 固定角色权限key
//    List<User> getDoTaskUserByWorkTypePermission(String schema, String facilityNO, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, int distributionOrExecute, String rolePermissionKey, int category_id);
//
//
//    //按工单类型，获取有操作权限的用户（单个设备）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //roleIds: 角色id,多个时，逗号隔开
//    List<User> getDoTaskUserByAssetIdRoles(String schema, String assetId, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, String roleIds);
//
//    //按工单类型，获取有操作权限的用户（单个设备）
//    //distributionOrExecute: 1:执行；2：分配；
//    //businessTypeId: 业务类型：1:维修；2：保养；3：巡检；4：点检；
//    //rolePermissionKey: 固定角色权限key
//    List<User> getDoTaskUserByAssetIdPermission(String schema, String assetId, String
//            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, int distributionOrExecute, String rolePermissionKey);
//
    //找设备专门配置的维保执行人员
    List<Map<String, Object>> getDoExecuteUserByAssetId(String schema, String assetId, Integer workTypeId, String roleIds, Timestamp now);

//    // LSP工单处理变更-20190529-sps end
//
//
//    //派案策略变更 -zys 2020/03/25

    /**
     * 按工单类型，获取有操作权限的用户 派案策略变更新方法 加入设备位置为参数作为派案中间连接条件 -zys 2020/03/25
     *
     * @param schema
     * @param positionCode
     * @param poolId
     * @param businessTypeId
     * @param beginDateTime
     * @param endDateTime
     * @param roleIds
     * @param category_id
     * @return
     */
    List<Map<String, Object>> getExecutorByWorkTypeAndRoleIds(String schema, String positionCode, String
            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, String roleIds, int category_id);
//

    /**
     * 根据rolePermissionKey 固定角色权限key，获取有操作权限的用户 派案策略变更新方法 加入设备位置为参数作为派案中间连接条件 -zys 2020/03/25
     *
     * @param schema
     * @param positionCode
     * @param poolId
     * @param businessTypeId
     * @param beginDateTime
     * @param endDateTime
     * @param distributionOrExecute
     * @param rolePermissionKey
     * @param category_id
     * @return
     */
    List<Map<String, Object>> getExecutorByWorkTypeAndPermission(String schema, String positionCode, String
            poolId, Integer businessTypeId, Timestamp beginDateTime, Timestamp endDateTime, int distributionOrExecute, String rolePermissionKey, int category_id);

//    //派案策略变更 -zys 2020/03/25

}
