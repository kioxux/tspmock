package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface DataImportService {

    /**
     * 导入处理
     * @param methodParam
     * @param importType    数据导入类型
     * @param file
     * @return
     */
    ResponseModel doImport(MethodParam methodParam, int importType, MultipartFile file);

    /**
     * 获取数据导入模板
     * @param methodParam
     * @param importType
     * @return
     */
    void getDataTemplate(MethodParam methodParam, int importType);

    /**
     * 获取数据导入日志列表
     * @param methodParam
     * @param importType
     * @return
     */
    Map<String, Object> getImportDataLogs(MethodParam methodParam, Integer importType);

    /**
     * 获取数据导入类型列表
     * @param methodParam
     * @return
     */
    List<Map<String, Object>> searchImportTypes(MethodParam methodParam);

}
