package com.gengyun.senscloud.service.guacamole;

import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

public interface GuacamoleWebService {

    Map<String, String> getFacilityGuacamole(MethodParam methodParam, String facility_id);

    List<Map<String, String>> getAssetPositionGuacamole(MethodParam methodParam, String position_code);
}
