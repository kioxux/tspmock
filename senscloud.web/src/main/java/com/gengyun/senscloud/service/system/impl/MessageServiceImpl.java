package com.gengyun.senscloud.service.system.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.entity.CompanyEntity;
import com.gengyun.senscloud.mapper.MessageMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.MessageManageService;
import com.gengyun.senscloud.service.MessageService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WxGzhMsgService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 消息处理
 */
@Service
public class MessageServiceImpl implements MessageService {
    private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);
    @Resource
    MessageMapper messageMapper;
    @Resource
    LogsService logService;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    WxGzhMsgService wxGzhMsgService;
    @Resource
    MessageManageService messageManageService;
    @Resource
    UserService userService;
    @Resource
    MailUntil mailUntil;
    @Resource
    CompanyService companyService;
    @Value("${senscloud.com.url}")
    private String url;

    /**
     * 消息发送处理
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人账号
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    @Override
    public void sendMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo, String subject) {
        CompanyEntity companyEntity = companyService.getCompanyById(methodParam.getCompanyId());

        // 发送系统消息
        this.doSendSysMsg(methodParam, contentParam, smsCode, receiveUserId, businessType, businessNo);
        //多语言可以转换
        for (int i = 0; i < contentParam.length; i++) {
            contentParam[i] = commonUtilService.getLanguageInfoBySysLang(methodParam, contentParam[i].toString());
        }
        // 校验是否需要发送短信
        this.doSendSmsMsg(methodParam, contentParam, smsCode, receiveUserId, businessType, businessNo);
        // 发送微信公众号模板消息
        this.sendWxGzhMsg(methodParam, contentParam, smsCode, receiveUserId, businessType, businessNo);
        if (companyEntity.getIs_open_email() && null == methodParam.getNeedCC()) {
            // 发送邮件
            this.doSendEmailMsg(methodParam, contentParam, smsCode, receiveUserId, businessType, businessNo, subject);
        }
    }

    /**
     * 发送系统消息
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    @Override
    public void doSendEmailMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo, String subject) {
        try {

            String[] contentStrings = new String[contentParam.length];
            for (int i = 0; i < contentParam.length; i++) {
                contentStrings[i] = contentParam[i].toString();
            }
            Map<String, Object> userInfo = userService.getUserInfoById(methodParam, receiveUserId);
            Map<String, String> msgContent = RegexUtil.optMapStrOrExp(messageMapper.findMsgContentTemp(methodParam.getSchemaName(), smsCode), ErrorConstant.EC_PRIMARY_SMS_8);
            //新增系统消息
            List<Map<String, Object>> receivers = new ArrayList<>();
            receivers.add(userInfo);
            //多语言可以转换
            for (int i = 0; i < contentParam.length; i++) {
                contentParam[i] = commonUtilService.getLanguageInfoBySysLang(methodParam, contentParam[i].toString());
            }
            String receiveContent = DataChangeUtil.wxGzhMsgTemplateValueHandler(contentParam, msgContent.get("pattern_description"));
            //发送邮件
            mailUntil.sendEmailMessage(methodParam, subject, receiveContent, receivers, null);

        } catch (Exception e) {
            logService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_EMAIL_MSG_001, methodParam.getUserId(), "系统消息发送失败");
            logger.error("emailMsgErr-1001:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void doSendCCEmailMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo, String subject, List<String> ccUser) {
        try {

            String[] contentStrings = new String[contentParam.length];
            for (int i = 0; i < contentParam.length; i++) {
                contentStrings[i] = contentParam[i].toString();
            }
            Map<String, Object> userInfo = userService.getUserInfoById(methodParam, receiveUserId);
            Map<String, String> msgContent = RegexUtil.optMapStrOrExp(messageMapper.findMsgContentTemp(methodParam.getSchemaName(), smsCode), ErrorConstant.EC_PRIMARY_SMS_8);
            //新增系统消息
            List<Map<String, Object>> receivers = new ArrayList<>();
            List<Map<String, Object>> ccUsers = new ArrayList<>();
            for (String cc : ccUser) {
                ccUsers.add(userService.findUserInfoById(methodParam, cc));
            }
            receivers.add(userInfo);
            //多语言可以转换
            for (int i = 0; i < contentParam.length; i++) {
                contentParam[i] = commonUtilService.getLanguageInfoBySysLang(methodParam, contentParam[i].toString());
            }
            String receiveContent = DataChangeUtil.wxGzhMsgTemplateValueHandler(contentParam, msgContent.get("pattern_description"));
            //发送邮件
            mailUntil.sendCCEmailMessage(methodParam, subject, receiveContent, receivers, ccUsers);

        } catch (Exception e) {
            logService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_EMAIL_MSG_001, methodParam.getUserId(), "系统消息发送失败");
            logger.error("emailMsgErr-1001:" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 发送系统消息
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    @Override
    public void doSendSysMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo) {
        try {
            String[] contentStrings = new String[contentParam.length];
            for (int i = 0; i < contentParam.length; i++) {
                contentStrings[i] = contentParam[i].toString();
            }
            String senderId = null;
            Map<String, Object> userInfo = userService.getUserInfoById(methodParam, receiveUserId);
            senderId = methodParam.getUserId();
            Map<String, String> msgContent = RegexUtil.optMapStrOrExp(messageMapper.findMsgContentTemp(methodParam.getSchemaName(), smsCode), ErrorConstant.EC_PRIMARY_SMS_8);
            String log_pattern_key = msgContent.get("log_pattern_key");
            String content = "";
            content = LangUtil.doSetLogArray("", log_pattern_key, contentStrings);
            //新增系统消息
            List<String> receivers = new ArrayList<>();
            receivers.add(receiveUserId);
            //多语言可以转换
            for (int i = 0; i < contentParam.length; i++) {
                contentParam[i] = commonUtilService.getLanguageInfoBySysLang(methodParam, contentParam[i].toString());
            }
            String receiveContent = DataChangeUtil.wxGzhMsgTemplateValueHandler(contentParam, msgContent.get("pattern_description"));
            messageManageService.insertMessageRecord(methodParam, content, businessType, businessNo, 3, null, receiveContent, log_pattern_key, contentParam.toString(), receivers, null);
        } catch (Exception e) {
            logService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_SYS_MSG_001, methodParam.getUserId(), "系统消息发送失败");
            logger.error("sysMsgErr-1001:" + e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * 发送短信
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人ID
     * @param businessType  业务类型
     * @param businessNo    业务编号
     */
    @Override
    public void doSendSmsMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo) {
        try {
            if (methodParam.getOpenSms()) {
                boolean isSuccess = false;
                String errCode = null;
                String errMsg = "";
                String senderId = null;
                String schemaName = methodParam.getSchemaName();
                String phone = null;
                try {
                    senderId = methodParam.getUserId();
                    phone = RegexUtil.optNotBlankStrOpt(receiveUserId).map(ra -> selectOptionService.getSelectOptionAttrByCode(methodParam, "user_detail", ra, "mobile"))
                            .map(mb -> RegexUtil.isMobile(mb, ErrorConstant.EC_PRIMARY_SMS_7)).orElseThrow(() -> new SenscloudException(ErrorConstant.EC_PRIMARY_SMS_7));
                    Map<String, String> configInfo = RegexUtil.optMapStrOrExp(messageMapper.findMsgConfig(schemaName, Constants.USUAL_CONFIG_ID_SMS), ErrorConstant.EC_PRIMARY_SMS_8); // 获取短信配置信息
                    String provider = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve1"), ErrorConstant.EC_PRIMARY_SMS_6);
                    // 短信内容解析
                    // 获取短信模板解析参数
                    Map<String, String> contentConfigInfo = RegexUtil.optMapStrOrExp(messageMapper.findMsgContentTemp(schemaName, smsCode), ErrorConstant.EC_PRIMARY_SMS_9);
                    String pattern = RegexUtil.optNotBlankStrOrExp(contentConfigInfo.get("sms_pattern"), ErrorConstant.EC_PRIMARY_SMS_10);
                    String content = "";
                    // 腾讯短信发送平台（特殊参数处理）
                    if (Constants.SENSCLOUD_SMS_DEFAULT.equals(provider)) {
                        int msgTempCode = RegexUtil.optIntegerOrExp(contentConfigInfo.get("sms_reserve1"), methodParam, ErrorConstant.EC_PRIMARY_SMS_11, contentConfigInfo.get("sms_reserve1"), null);
                        content = RegexUtil.optNotBlankStrOrExp(MessageFormat.format(pattern, DataChangeUtil.firstAddArray(msgTempCode, contentParam)), ErrorConstant.EC_PRIMARY_SMS_2);
                        String splitStr = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve5"), ErrorConstant.EC_PRIMARY_SMS_14);
                        String[] contents = RegexUtil.optArrayOrExp(content.split(splitStr), ErrorConstant.EC_PRIMARY_SMS_2);
                        Map<String, Object> resultMap = RegexUtil.optMapOrExp(SmsByDbUtil.sendSms(methodParam, configInfo, provider, phone, contents), ErrorConstant.EC_PRIMARY_SMS_13); // 发短信
                        isSuccess = (boolean) resultMap.get("result");
                        errMsg = RegexUtil.optStrOrBlank(resultMap.get("errMsg"));
                    }
                    // 西门子短信发送
                    else if (Constants.SENSCLOUD_SMS_SIEMENS.equals(provider)) {
                        Map<String, Object> resultMap = RegexUtil.optMapOrExp(SmsByDbUtil.sendSms(methodParam, configInfo, provider, phone, null), ErrorConstant.EC_PRIMARY_SMS_13); // 发短信
                        isSuccess = (boolean) resultMap.get("result");
                        errMsg = RegexUtil.optStrOrBlank(resultMap.get("errMsg"));
                    } else {
                        errCode = ErrorConstant.EC_PRIMARY_SMS_15;
                    }
                } catch (SenscloudError e) {
                    errMsg = RegexUtil.optNotNull(e.getMsgContent()).map(ThreadLocal::get).filter(info -> info.size() > 0).map(m -> m.get(ErrorConstant.EC_REMARK_KEY)).map(RegexUtil::optStrOrNull).orElse("");
                    errCode = e.getMessage();
                } catch (SenscloudException e) {
                    errCode = e.getMessage();
                } catch (Exception e) {
                    errCode = ErrorConstant.EC_PRIMARY_SMS_16;
                    String tmpMsg = e.getMessage();
                    errMsg += RegexUtil.optNotBlankStrOpt(tmpMsg).filter(em -> em.length() > 50).map(em -> {
                        logger.error(ErrorConstant.EC_PRIMARY_SMS_16 + em);
                        return em.substring(0, 50);
                    }).orElse(tmpMsg);
                }
                try {
                    // 记录短信发送结果
                    Map<String, Object> messageData = new HashMap<>();
                    messageData.put("receive_user_id", RegexUtil.optStrOrVal(receiveUserId, phone));
                    messageData.put("msg_content", RegexUtil.optStrOrBlank(smsCode));
                    messageData.put("receive_mobile", phone);
                    messageData.put("is_success", isSuccess);
                    messageData.put("business_type", businessType);
                    messageData.put("business_no", businessNo);
                    messageData.put("send_user_id", senderId);
                    RegexUtil.intExp(messageMapper.insertMessage(schemaName, messageData), ErrorConstant.EC_PRIMARY_SMS_18);
                } catch (SenscloudException e) {
                    errCode += e.getMessage();
                } catch (Exception e) {
                    errCode += ErrorConstant.EC_PRIMARY_SMS_17;
                    String tmpMsg = e.getMessage();
                    errMsg += RegexUtil.optNotBlankStrOpt(tmpMsg).filter(em -> em.length() > 50).map(em -> {
                        logger.error(ErrorConstant.EC_PRIMARY_SMS_17 + em);
                        return em.substring(0, 50);
                    }).orElse(tmpMsg);
                }
                if (!isSuccess) {
                    String splitStr = SensConstant.LOG_SPLIT;
                    errMsg = RegexUtil.optStrOrVal(errMsg, "error").concat(splitStr)
                            .concat(RegexUtil.optStrOrVal(smsCode, "msgCode")).concat(splitStr)
                            .concat(RegexUtil.optStrOrVal(receiveUserId, "receiver")).concat(splitStr)
                            .concat(RegexUtil.optStrOrVal(phone, "phone")).concat(splitStr)
                            .concat(RegexUtil.optStrOrVal(businessType, "type")).concat(splitStr)
                            .concat(RegexUtil.optStrOrVal(businessNo, "dataNo")).concat(splitStr)
                            .concat(RegexUtil.optStrOrVal(senderId, "sender"));
                    errMsg = RegexUtil.optNotBlankStrOpt(errMsg).filter(em -> em.length() > 200).map(em -> em.substring(0, 200)).orElse(errMsg);
                    logService.newErrorLog(schemaName, errCode, senderId, errMsg);
                }
            }
        } catch (Exception e) {
            logger.error("smsMsgErr-1003:" + e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * 微信公众号模板信息发送
     *
     * @param methodParam   系统参数
     * @param contentParam  短信内容参数
     * @param smsCode       短信编号
     * @param receiveUserId 接收人工号
     * @param businessType  业务名称
     * @param businessNo    业务类型
     */
    private void sendWxGzhMsg(MethodParam methodParam, Object[] contentParam, String smsCode, String receiveUserId, String businessType, String businessNo) {
        try {
            boolean isSuccess = false;
            String errCode = null;
            String errMsg = null;
            String senderId = null;
            String schemaName = methodParam.getSchemaName();
            String openId = null;
            try {
                senderId = methodParam.getUserId();
                RegexUtil.optMapStrOrExp(messageMapper.findMsgConfig(schemaName, Constants.USUAL_CONFIG_ID_WX_GZH_TOKEN), ErrorConstant.EC_WX_GZH_021); // 获取微信配置信息
                openId = RegexUtil.optNotBlankStrOpt(receiveUserId).map(ra -> selectOptionService.getSelectOptionAttrByCode(methodParam, "user_detail", ra, "official_open_id"))
                        .orElseThrow(() -> new SenscloudException(ErrorConstant.EC_WX_GZH_007));
                logger.debug("sendWxGzhMsg-receiveUserId:" + receiveUserId);
                Map<String, String> contentConfigInfo = RegexUtil.optMapStrOrExp(messageMapper.findMsgContentTemp(schemaName, smsCode), ErrorConstant.EC_WX_GZH_022); // 模板配置内容
                Map<String, Object> contentInfo = RegexUtil.optMapOrExp(getWxGzhContent(methodParam, contentConfigInfo, contentParam), ErrorConstant.EC_WX_GZH_023); // 解析微信公众号内容
                wxGzhMsgService.sendWxMsg(schemaName, senderId, openId, contentConfigInfo.get("wx_oa_msg_template_code"), contentInfo); // 发送消息
                isSuccess = true;

            } catch (SenscloudException e) {
                errCode = RegexUtil.optStrOrVal(e.getMessage(), ErrorConstant.EC_WX_GZH_010);
                errMsg = RegexUtil.optNotNull(e.getMsgContent()).map(ThreadLocal::get).filter(info -> info.size() > 0).map(m -> m.get(ResponseConstant.ERROR_MSG_REMARK)).map(RegexUtil::optStrOrNull).orElse("");
            } catch (Exception e) {
                errCode = ErrorConstant.EC_WX_GZH_010;
                errMsg = e.getMessage();
            }
            try {
                // 记录短信发送结果
                Map<String, Object> messageData = new HashMap<>();
                messageData.put("receive_user_id", RegexUtil.optStrOrBlank(receiveUserId));
                messageData.put("msg_content", RegexUtil.optStrOrBlank(smsCode));
                messageData.put("receive_mobile", openId);
                messageData.put("is_success", isSuccess);
                messageData.put("business_type", businessType);
                messageData.put("business_no", businessNo);
                messageData.put("send_user_id", senderId);
                RegexUtil.intExp(messageMapper.insertMessage(schemaName, messageData), ErrorConstant.EC_WX_GZH_024);
            } catch (SenscloudException e) {
                errCode += e.getMessage();
            } catch (Exception e) {
                errCode = ErrorConstant.EC_WX_GZH_025;
                errMsg += e.getMessage();
            }
            if (!isSuccess) {
                logger.error("wxGzhMsgErr-4002:", errMsg);
                String splitStr = SensConstant.LOG_SPLIT;
                errMsg = RegexUtil.optStrOrVal(errMsg, "error").concat(splitStr)
                        .concat(RegexUtil.optStrOrVal(smsCode, "msgCode")).concat(splitStr)
                        .concat(RegexUtil.optStrOrVal(receiveUserId, "receiver")).concat(splitStr)
                        .concat(RegexUtil.optStrOrVal(openId, "openId")).concat(splitStr)
                        .concat(RegexUtil.optStrOrVal(businessType, "type")).concat(splitStr)
                        .concat(RegexUtil.optStrOrVal(businessNo, "dataNo")).concat(splitStr)
                        .concat(RegexUtil.optStrOrVal(senderId, "sender"));
                logService.newErrorLog(schemaName, errCode, senderId, errMsg);
            }
        } catch (Exception e) {
            logger.error("wxGzhMsgErr-4001:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, String> getInfoByCode(MethodParam methodParam, String msgCode) {
        return messageMapper.findMsgContentTemp(methodParam.getSchemaName(), msgCode);
    }

    /**
     * 查询微信token配置信息
     *
     * @param schemaName    数据库
     * @param usualConfigId 配置主键
     * @return 微信token配置信息
     */
    @Override
    public Map<String, String> getWxTokenConfig(String schemaName, String usualConfigId) {
        return messageMapper.findWxTokenConfig(schemaName, usualConfigId);
    }

    /**
     * 获取全局数据信息
     *
     * @param id 主键
     * @return 全局数据信息
     */
    @Override
    public Map<String, String> getGlobalInfo(String id) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findGlobalInfo");
        Map<String, String> result = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), Map.class);
        return result;
//        return messageMapper.findGlobalInfo(id);
    }


    /**
     * 更新微信token
     *
     * @param schemaName    数据库
     * @param wxAppToken    wxAppToken
     * @param usualConfigId 配置主键
     * @return 更新数量
     */
    @Override
    public int modifyWxToken(String schemaName, String wxAppToken, String usualConfigId) {
        return messageMapper.updateWxToken(schemaName, wxAppToken, usualConfigId);
    }

    /**
     * 更新全局微信token
     *
     * @param wxAppToken wxAppToken
     * @param id         配置主键
     * @return 更新数量
     */
    @Override
    public int modifyGlobalWxToken(String wxAppToken, String id) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("wxAppToken", wxAppToken);
        jsonObject.put("id", id);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("updateGlobalWxToken");
        int result = JSON.parseObject(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString), int.class);
        return result;
//        return messageMapper.updateGlobalWxToken(wxAppToken, id);
    }

    /**
     * 根据备用字段信息查询全局数据
     *
     * @param reserveName 字段名称
     * @param value       字段值
     * @return 全局数据
     */
    @Override
    public Map<String, Object> getGlobalInfoByReserve(String reserveName, String value) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("reserveName", reserveName);
        jsonObject.put("value", value);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findGlobalInfoByReserve");
        Map<String, Object> result = DataChangeUtil.scdObjToMap(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
//        return messageMapper.findGlobalInfoByReserve(reserveName, value);
    }

    /**
     * 微信公众号模板消息解析
     *
     * @param methodParam       系统参数
     * @param contentConfigInfo 模板配置信息
     * @param contentParam      内容参数
     * @return 解析后消息内容
     */
    private Map<String, Object> getWxGzhContent(MethodParam methodParam, Map<String, String> contentConfigInfo, Object[] contentParam) {
        // 短信内容解析
        String pattern = contentConfigInfo.get("wx_oa_msg_pattern");
        if (RegexUtil.optIsPresentStr(pattern)) {
            Map<String, Object> contentInfo = new HashMap<>();
            String[] patterns = pattern.split(",");
            String dataKey;
            String dataValue = "";
            String dataValueStr;
            String[] tmpKeys;
            String tmpValue;
            String[] tmpIndexes;
            int contentParamLen = 0;
            if (null != contentParam) {
                contentParamLen = contentParam.length;
            }
            for (int i = 0; i < patterns.length; i++) {
                dataKey = patterns[i];
                if (RegexUtil.optIsBlankStr(dataKey)) {
                    continue;
                }
                if (contentParamLen > 0) {
                    if (dataKey.contains("@t&lang")) {//包含@t&lang的字段，需国际化处理
                        tmpKeys = dataKey.split("@t&lang");
                        dataKey = tmpKeys[0];
                        dataValue = commonUtilService.getLanguageInfoBySysLang(methodParam, tmpKeys[1]);
                    } else if (dataKey.contains("@t")) {//包含@t的字段，直接用模板里的固定字符串
                        tmpKeys = dataKey.split("@t");
                        dataKey = tmpKeys[0];
                        dataValue = tmpKeys[1];
                    } else if (dataKey.contains("@gp")) {//包含@gp的字段，需拼接字符串数组参数中的多个字符串
                        tmpKeys = dataKey.split("@gp");
                        dataKey = tmpKeys[0];
                        tmpValue = tmpKeys[1];
                        tmpIndexes = tmpValue.split("&");
                        for (String ti : tmpIndexes) {
                            dataValue = dataValue + contentParam[Integer.valueOf(ti)];
                        }
                    } else if (dataKey.contains("@now")) {
                        tmpKeys = dataKey.split("@now");
                        dataKey = tmpKeys[0];
                        dataValue = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                    } else if (dataKey.contains("@f")) {////包含@f的字段，按配置的格式替换
                        tmpKeys = dataKey.split("@f");
                        dataKey = tmpKeys[0];
                        dataValue = tmpKeys[1];
                        dataValue = DataChangeUtil.wxGzhMsgTemplateValueHandler(contentParam, dataValue);
                    } else {
                        dataValue = (contentParam.length > i) ? String.valueOf(contentParam[i]) : "";
                    }
                    if (RegexUtil.optIsBlankStr(dataKey)) {
                        continue;
                    }
                    dataValueStr = dataValue;
                    if (dataValueStr.length() > 200) {//模板内容长度不能超过200个字符
                        dataValue = dataValueStr.substring(0, 197) + "...";
                    }
                }
                Map<String, Object> dataValueInfo = new HashMap<>();
                dataValueInfo.put("value", dataValue);
                dataValueInfo.put("color", "#828282");
                contentInfo.put(dataKey, dataValueInfo);
            }
            return contentInfo;
        }
        return null;
    }

    /**
     * 更新消息发送日志
     *
     * @param schemaName 入参
     * @param id         入参
     * @param isSuccess  入参
     */
    @Override
    public int updateMessageData(String schemaName, int id, String isSuccess) {
        Timestamp sendTime = new Timestamp((new Date()).getTime());
        return messageMapper.updateMessageData(schemaName, id, isSuccess, sendTime);
    }

    /**
     * 替换模板中的占位符，输出填完值后的字符串
     *
     * @param params
     * @param patternStr
     * @return
     */
    private String wxoaMsgTemplateValueHandler(Object[] params, String patternStr) {
        try {
            Pattern p = Pattern.compile("\\{\\d+\\}");
            Matcher m = p.matcher(patternStr);
            List<String> strList = new ArrayList<>();
            String format = patternStr;
            while (m.find()) {
                String tag = m.group();
                format = format.replace(tag, "%s");
                int index = Integer.valueOf(tag.replace("{", "").replace("}", ""));
                strList.add(params[index - 1] != null ? String.valueOf(params[index - 1]) : "");
            }
            return String.format(format, strList.toArray());
        } catch (NumberFormatException e) {
            return "";
        }
    }
}
