package com.gengyun.senscloud.service.system.impl;

import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.mapper.SelectOptionsMapper;
import com.gengyun.senscloud.mapper.WorkFlowTemplateMapper;
import com.gengyun.senscloud.mapper.WorkTemplateMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkColumnExAdd;
import com.gengyun.senscloud.model.WorkColumnSearchParam;
import com.gengyun.senscloud.model.WorkTemplateSearchParam;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.system.*;
import com.gengyun.senscloud.util.*;
import io.swagger.annotations.Api;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "工单模板")
@Service
public class WorkTemplateServiceImpl implements WorkTemplateService {

    @Resource
    WorkTemplateMapper workTemplateMapper;
    @Resource
    SerialNumberService serialNumberService;
    @Resource
    SelectOptionsMapper selectOptionsMapper;
    @Resource
    SelectOptionService selectOptionService;
    @Resource
    LogsService logService;
    @Resource
    WorkFlowTemplateService workFlowTemplateService;
    @Resource
    WorkFlowTemplateMapper workFlowTemplateMapper;
    @Resource
    CacheUtilService cacheUtilService;
    @Value("${senscloud.com.url}")
    private String url;

    /**
     * 获取工单模板列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkTemplateListForPage(MethodParam methodParam, WorkTemplateSearchParam param) {
        String schemaName = methodParam.getSchemaName();

        // 获取按条件拼接的sql
        String searchWord = this.getWorkTemplateWhereString(methodParam, param);
        searchWord += " ORDER BY t.create_time desc ";
        List<Map<String, Object>> workColumnList = null;
        workColumnList = workTemplateMapper.findWorkTemplateList(schemaName, searchWord);
        return workColumnList;
    }

    /**
     * 新增模板基础信息
     *
     * @param methodParam
     * @param data
     * @return
     */
    @Override
    public String newWorkTemplate(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_AC_V}); // 工单模板不能为空
        RegexUtil.optStrOrExpNotNull(data.get("work_template_name"), LangConstant.TITLE_NAME_AB_C);// 模板名称不能为空
        String uuid = serialNumberService.generateMaxBsCodeByType(methodParam, "work_template");
        data.put("work_template_code", uuid);
        data.put("create_time", nowTime);
        data.put("is_use", "1");
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        int result = workTemplateMapper.insertWorkTemplate(data);
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, uuid, LangUtil.doSetLogArray("工单模板新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AC_V}));
        return uuid;
    }

    /**
     * 获取模板基本信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getWorkTemplateInfoByWorkTemplateCode(MethodParam methodParam, WorkTemplateSearchParam param) {
        Map<String, Object> info = this.getWorkTemplateDetailByCode(methodParam, param.getWork_template_code());
        return info;
    }

    /**
     * 获取模板字段信息列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public List<Map<String, Object>> getWorkTemplateColumnListByWorkTemplateCode(MethodParam methodParam, WorkTemplateSearchParam param) {
        String schemaName = methodParam.getSchemaName();

        // 获取按条件拼接的sql
        String searchWord = this.getWorkTemplateColumnWhereString(methodParam, param);
        searchWord += " ORDER BY wfnc.data_order asc ,wfnc.id desc ";
        List<Map<String, Object>> workColumnList = null;
        Long companyId = methodParam.getCompanyId();
        String userLang = methodParam.getUserLang();
        workColumnList = workFlowTemplateService.findWorkTemplateColumnList(schemaName, searchWord, param.getWork_template_code(), companyId, userLang, param.getKeywordSearch());
        return workColumnList;
    }

    /**
     * 删除工单模板
     *
     * @param methodParam
     * @param work_template_code
     */
    @Override
    public void cutWorkTemplateByWorkTemplateCode(MethodParam methodParam, String work_template_code) {
        String schemaName = methodParam.getSchemaName();
        // todo 子页面关联的work_template_code也需要校验一下，暂查询的_sc_work_flow_node_column_ext表的field_value字段
        RegexUtil.trueExp(workTemplateMapper.findCountUseByWorkTemplateCode(schemaName, work_template_code) > 0, LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AAI_I, LangConstant.TITLE_AC_V, LangConstant.TITLE_AC_V}); // 流程下还有工单模板，不能删除该工单模板
        workTemplateMapper.deleteTemplateByWorkTemplateCode(schemaName, work_template_code);
        workTemplateMapper.deleteTemplateColumnByWorkTemplateCode(schemaName, work_template_code);
        workTemplateMapper.deleteTemplateColumnExtByWorkTemplateCode(schemaName, work_template_code);
        workTemplateMapper.deleteTemplateColumnLinkageConditionByWorkTemplateCode(schemaName, work_template_code);
        workTemplateMapper.deleteTemplateColumnLinkageByWorkTemplateCode(schemaName, work_template_code);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, work_template_code, LangUtil.doSetLogArray("工单模板删除", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AC_V, work_template_code}));
    }

    /**
     * 删除工单模板字段
     *
     * @param methodParam
     * @param param
     */
    @Override
    public void cutWorkTemplateColumnByWorkTemplateCode(MethodParam methodParam, WorkTemplateSearchParam param) {
        String schemaName = methodParam.getSchemaName();
        String work_template_code = param.getWork_template_code();
        String field_form_code = param.getField_form_code();
//        Map<String, Object> columnInfo = workTemplateMapper.findWorkTemplateColumnInfoByCode(schemaName, work_template_code, field_form_code);
        // todo 子页面关联的work_template_code也需要校验一下，暂查询的_sc_work_flow_node_column_ext表的field_value字段
        RegexUtil.trueExp(workTemplateMapper.findColumnCountUseByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code) > 0, LangConstant.MSG_CE, new String[]{LangConstant.TITLE_AAI_I, LangConstant.TITLE_NG, LangConstant.TITLE_NG}); // 流程下还有字段，不能删除该字段
        workTemplateMapper.deleteTemplateColumnByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        workTemplateMapper.deleteTemplateColumnExtByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        workTemplateMapper.deleteTemplateColumnLinkageConditionByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        workTemplateMapper.deleteTemplateColumnLinkageByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, work_template_code, LangUtil.doSetLogArray("工单模板字段删除", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_AC_V, LangConstant.TITLE_NG}));
    }

    /**
     * 新增工单模板字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void newWorkTemplateColumn(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_NG}); // 字段不能为空
        String work_template_code = RegexUtil.optStrOrExpNotNull(data.get("work_template_code"), LangConstant.TITLE_AC_V);// 工单模板不能为空
        String field_form_codes = RegexUtil.optStrOrExpNotNull(data.get("field_form_codes"), LangConstant.TITLE_NG);// 字段不能为空
        String[] fieldFormCodes = field_form_codes.split(",");
        data.put("create_time", nowTime);
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        Integer dataOrder = workTemplateMapper.findMaxDataOrderByWorkTemplateCode(schemaName, work_template_code); // 获取工单模板字段最大排序
        if (dataOrder == null) {
            dataOrder = 0;
        }


        int result = 0;
        for (String fieldFormCode : fieldFormCodes) {
            dataOrder = dataOrder + 1;
            data.put("field_form_code", fieldFormCode);
            data.put("sort", dataOrder);
            result += workTemplateMapper.insertWorkTemplateColumn(data);
            workTemplateMapper.insertWorkTemplateColumnExt(data);
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        for (String id : fieldFormCodes) {
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, work_template_code, LangUtil.doSetLogArray("新增工单模板字段！", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_NEW_A, LangConstant.TITLE_NG, id}));
        }

    }

    /**
     * 是否处理工单模板字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void isHandleWorkTemplateColumn(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_NG}); // 字段不能为空
        int result = 0;
        List<Map<String, Object>> list = RegexUtil.optNotNullListOrNew(JSON.parseObject(RegexUtil.optStrOrBlank(data.get("isChooseWorkTemplateCloumList")), List.class));
        for (Map<String, Object> item : list) {
            String work_template_code = RegexUtil.optStrOrExpNotNull(item.get("hd_work_template_code"), LangConstant.TITLE_AC_V);// 工单模板不能为空
            String field_form_code = RegexUtil.optStrOrExpNotNull(item.get("field_form_code"), LangConstant.TITLE_NG);// 字段不能为空
            String field_code = RegexUtil.optStrOrExpNotNull(item.get("field_code"), LangConstant.TITLE_NG);// 字段不能为空
            String field_name = RegexUtil.optStrOrExpNotNull(RegexUtil.optStrOrVal(item.get("field_name_key"), RegexUtil.optStrOrNull(item.get("field_name"))), LangConstant.TITLE_NG);// 字段不能为空
            item.put("schema_name", schemaName);
            item.put("field_form_code", field_form_code);
            item.put("field_code", field_code);
            item.put("field_name", field_name);
            item.put("work_template_code", work_template_code);
            result += workTemplateMapper.isHandleWorkTemplateColumn(item);
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
    }

    /**
     * 编辑工单模板字段
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkTemplateColumn(MethodParam methodParam, Map<String, Object> data) {
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_BI); // 失败：缺少必要条件！
        String schemaName = methodParam.getSchemaName();
        String field_form_code = data.get("field_form_code").toString();
        String work_template_code = data.get("work_template_code").toString();
//        Map<String, Object> dbWorkColumnInfo = this.getWorkTemplateColumnDetail(methodParam, field_form_code, work_template_code);
        WorkTemplateSearchParam param = new WorkTemplateSearchParam();
        param.setWork_template_code(work_template_code);
        param.setField_form_code(field_form_code);
        Map<String, Object> dbWorkColumnInfo = this.getWorkTemplateColumnInfo(methodParam, param);
        Timestamp nowTime = SenscloudUtil.getNowTime();
        RegexUtil.optStrOrExpNotNull(data.get("field_name"), LangConstant.TITLE_NAME_C);   // 显示名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("field_code"), LangConstant.TITLE_NAME_AB_R); // 字段名称不能为空
        RegexUtil.optStrOrExpNotNull(data.get("field_right"), LangConstant.TITLE_B_C); // 操作权限不能为空
        String field_section_type = RegexUtil.optStrOrExpNotNull(data.get("field_section_type"), LangConstant.TITLE_QE);// 所在区块不能为空
        String field_view_type = RegexUtil.optStrOrExpNotNull(data.get("field_view_type"), LangConstant.TITLE_QF);// 控件类型不能为空
        Map<String, Object> block = selectOptionsMapper.findUnLangStaticOption(schemaName, "block", field_section_type);
        RegexUtil.optNotNullOrExpNullInfo(block, LangConstant.TITLE_QE); // 所在区块不存在
        RegexUtil.optNotNullOrExpNullInfo(selectOptionsMapper.findUnLangStaticOption(schemaName, "work_column", field_view_type), LangConstant.TITLE_QF); // 控件类型不存在
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            String extList1 = data.get("extList").toString();
            JSONArray extList = JSONArray.fromObject(extList1);
            for (int x = 0; x < extList.size(); x++) {
                JSONObject extObject = extList.getJSONObject(x);
                extObject.remove("create_time");
            }
            List<WorkColumnExAdd> list = JSONArray.toList(extList, WorkColumnExAdd.class);
            if (list.size() > 0) {
                for (WorkColumnExAdd workColumnExAdd : list) {
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_name(), LangConstant.TITLE_AAT_P);   // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_write_type(), LangConstant.TITLE_AAT_P); // 数据不能为空
//                    RegexUtil.optStrOrExpNotNull(workColumnExAdd.getField_value(), workColumnExAdd.getField_name()); // 特殊属性的名称值不能为空
                    workColumnExAdd.setField_form_code(field_form_code);
                    workColumnExAdd.setCreate_time(nowTime);
                    workColumnExAdd.setCreate_user_id(methodParam.getUserId());
                    workColumnExAdd.setNode_id(work_template_code);
                    workColumnExAdd.setColumn_id(Integer.valueOf(data.get("id").toString()));
//                    if (Constants.COLUMN_EXT_FIELD_VALUE.equals(workColumnExAdd.getField_name())) {
//                        data.put("field_value", workColumnExAdd.getField_value());
//                    }
                }
                data.put("extList", list);
            } else {
                data.remove("extList");
            }
        }
        if (RegexUtil.optNotNull(data.get("linkList")).isPresent()) {
            String linkList = data.get("linkList").toString();
            com.alibaba.fastjson.JSONArray linkLists = com.alibaba.fastjson.JSONArray.parseArray(linkList);
//            JSONArray linkLists = JSONArray.fromObject(linkList);
            if (linkLists.size() > 0) {
                for (int x = 0; x < linkLists.size(); x++) {
                    com.alibaba.fastjson.JSONObject linkObject = linkLists.getJSONObject(x);
                    RegexUtil.optStrOrExpNotNull(linkObject.get("linkage_type"), LangConstant.TITLE_AG);   // 来源方式为空
                    linkObject.put("create_time", nowTime.toString());
                    linkObject.put("create_user_id", methodParam.getUserId());
                    linkObject.put("field_form_code", field_form_code);
                    linkObject.put("node_id", work_template_code);
                    linkObject.put("schema_name", schemaName);
                    linkObject.put("column_id", Integer.valueOf(data.get("id").toString()));
                    if (linkObject.containsKey("condition")) {
                    } else {
                        linkObject.put("conditions", "{}");
                    }
                }
                data.put("linkList", linkLists);
            }
        }
        data.put("schema_name", schemaName);
//        data.put("field_code",dbWorkColumnInfo.get("field_code"));
        int result = workTemplateMapper.updateWorkTemplateColumn(data);
        workTemplateMapper.deleteTemplateColumnExtByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        if (RegexUtil.optNotNull(data.get("extList")).isPresent()) {
            List<WorkColumnExAdd> extList = (List<WorkColumnExAdd>) data.get("extList");
            workTemplateMapper.insertWorkTemplateColumnExBatch(schemaName, extList);
        }

        workTemplateMapper.deleteTemplateColumnLinkageConditionByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        workTemplateMapper.deleteTemplateColumnLinkageByWorkTemplateCodeAndFieldFormCode(schemaName, work_template_code, field_form_code);
        if (RegexUtil.optNotNull(data.get("linkList")).isPresent()) {
            com.alibaba.fastjson.JSONArray linkList = (com.alibaba.fastjson.JSONArray) data.get("linkList");
            for (int i = 0; i < linkList.size(); i++) {
                com.alibaba.fastjson.JSONObject linkObject = linkList.getJSONObject(i);
                workTemplateMapper.insertWorkTemplateColumnLinkage(linkObject);
                // todo 条件字段未作处理
                workTemplateMapper.insertWorkTemplateColumnLinkageCondition(linkObject);
            }
        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        if (RegexUtil.optNotNull(dbWorkColumnInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbWorkColumnInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, work_template_code, LangUtil.doSetLogArray("编辑了工单模板字段", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_NG, log.toString()}));
        }
    }

    /**
     * 获取工单模板字段明细信息
     *
     * @param methodParam
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getWorkTemplateColumnInfo(MethodParam methodParam, WorkTemplateSearchParam param) {
        Map<String, Object> info = this.getWorkTemplateColumnDetail(methodParam, param.getField_form_code(), param.getWork_template_code());
        RegexUtil.optMapOrExpNullInfo(info, LangConstant.TITLE_NG); // 字段不存在
        List<Map<String, Object>> extList = this.getWorkTemplateColumnExt(methodParam, param.getField_form_code(), param.getWork_template_code());
        if (RegexUtil.optIsPresentList(extList)) {
            info.put("extList", extList);
        } else {
            info.put("extList", null);
        }
        List<Map<String, Object>> linkList = this.getWorkTemplateColumnLinkage(methodParam, param.getField_form_code(), param.getWork_template_code());
        if (RegexUtil.optIsPresentList(linkList)) {
            for (Map<String, Object> condition : linkList) {
                com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(condition.get("condition").toString());
                condition.put("condition", jsonObject);
            }
            info.put("linkList", linkList);
        } else {
            info.put("linkList", null);
        }

        return info;
    }

    /**
     * 获取工单模板字段关联条件明细信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public Map<String, Object> getWorkTemplateColumnLinkageInfo(MethodParam methodParam, WorkTemplateSearchParam bParam) {
        return workTemplateMapper.findWorkTemplateColumnLinkageInfo(methodParam.getSchemaName(), bParam.getId());
    }

    /**
     * 克隆字段
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public String newWorkTemplateClone(MethodParam methodParam, WorkColumnSearchParam bParam) {
        String schemaName = methodParam.getSchemaName();
        Timestamp nowTime = SenscloudUtil.getNowTime();
        String uuid = serialNumberService.generateMaxBsCodeByType(methodParam, "work_template");
        HashMap<String, Object> data = new HashMap<>();
        data.put("work_template_code", uuid);
        data.put("old_work_template_code", bParam.getWork_template_code());
        data.put("create_time", nowTime);
        data.put("is_use", "1");
        data.put("schema_name", schemaName);
        data.put("create_user_id", methodParam.getUserId());
        int result = workTemplateMapper.insertWorkTemplateClone(data);

        //copy数据
        this.cloneWorkTemplateColumn(methodParam, schemaName, nowTime, bParam.getWork_template_code(),uuid, data);


//        workTemplateMapper.insertWorkTemplateColumnClone(data);
//        workTemplateMapper.insertWorkTemplateColumnExtClone(data);
////        workTemplateMapper.insertWorkTemplateColumnLinkageClone(data);
//        List<Map<String, Object>> linkList = workTemplateMapper.findWorkTemplateColumnLinkageAndConditionListByWorkTemplateCode(methodParam.getSchemaName(), bParam.getWork_template_code());
//        if (RegexUtil.optNotNull(linkList).isPresent()) {
//            for (Map<String, Object> linkage : linkList) {
//                linkage.put("create_time", nowTime);
//                linkage.put("create_user_id", methodParam.getUserId());
//                linkage.put("schema_name", schemaName);
//                linkage.put("node_id", uuid);
//                String condition = (String) linkage.get("condition");
//                com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(condition);
////                JSONObject jsonObject = JSONObject.fromObject(condition);
//                linkage.put("condition", jsonObject);
//                workTemplateMapper.insertWorkTemplateColumnLinkage(linkage);
//                workTemplateMapper.insertWorkTemplateColumnLinkageCondition(linkage);
//            }
//        }
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, uuid, LangUtil.doSetLogArray("克隆工单模板", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_AC_V}));
        return uuid;
    }

    /**
     * 克隆模板字段
     *
     * @param methodParam
     * @param schemaName
     * @param nowTime
     * @param old_work_template_code
     * @param new_work_template_code
     * @param data
     */
    private void cloneWorkTemplateColumn(MethodParam methodParam, String schemaName, Timestamp nowTime, String old_work_template_code,String new_work_template_code, HashMap<String, Object> data) {
        List<Map<String, Object>> columnList = workTemplateMapper.findColumnListByNodeId(schemaName, old_work_template_code);
        for (Map<String, Object> dbColumn : columnList) {
            dbColumn.put("node_id", new_work_template_code);
            dbColumn.put("create_time", nowTime);
            dbColumn.put("schema_name", schemaName);
            dbColumn.put("create_user_id", methodParam.getUserId());
            workFlowTemplateMapper.insertWorkFlowColumnAdd(dbColumn);
            workFlowTemplateMapper.insertWorkFlowColumnExtAdd(dbColumn);
            //根据被copy数据的字段id查询联动条件
            List<Map<String, Object>> linkList = workFlowTemplateMapper.findWorkTemplateColumnLinkageAndConditionListByColumnId(methodParam.getSchemaName(), dbColumn.get("id").toString());
            if (RegexUtil.optNotNull(linkList).isPresent()) {
                for (Map<String, Object> linkage : linkList) {
                    linkage.put("create_time", nowTime);
                    linkage.put("create_user_id", methodParam.getUserId());
                    linkage.put("schema_name", schemaName);
                    String condition = (String) linkage.get("condition");
                    com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(condition);
//                    JSONObject jsonObject = JSONObject.fromObject(condition);
                    linkage.put("condition", jsonObject);
                    linkage.put("node_id", new_work_template_code);
                    linkage.put("column_id", dbColumn.get("column_id"));
                    workTemplateMapper.insertWorkTemplateColumnLinkage(linkage);
                    workTemplateMapper.insertWorkTemplateColumnLinkageCondition(linkage);
                }
            }
        }
    }

    /**
     * 编辑工单模板基本信息
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkTemplate(MethodParam methodParam, Map<String, Object> data) {
        String schemaName = methodParam.getSchemaName();
        RegexUtil.optNotNullOrExp(data, LangConstant.MSG_A, new String[]{LangConstant.TITLE_AC_V}); // 工单模板不能为空
        RegexUtil.optStrOrExpNotNull(data.get("work_template_name"), LangConstant.TITLE_NAME_AB_C);// 模板名称不能为空
        String work_template_code = RegexUtil.optStrOrExpNotNull(data.get("work_template_code"), LangConstant.TITLE_AC_V);// 工单模板不能为空
        data.put("is_use", "1");
        data.put("schema_name", schemaName);
        int result = workTemplateMapper.updateWorkTemplate(data);
        RegexUtil.intExp(result, LangConstant.MSG_BK); // 未知错误！
        Map<String, Object> dbInfo = this.getWorkTemplateDetailByCode(methodParam, work_template_code);
        if (RegexUtil.optNotNull(dbInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, work_template_code, LangUtil.doSetLogArray("编辑了工单模板", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_AC_V, log.toString()}));
        }
    }

    /**
     * 根据控件类型获取特殊属性key
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public List<Map<String, Object>> getSpecialPropertyByWidgetType(MethodParam methodParam, WorkTemplateSearchParam bParam) {
        String type = bParam.getSelectKey();
        List<Map<String, Object>> keyList = cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), "work_column_key", type, methodParam.getUserLang(), methodParam.getCompanyId());
        for (Map<String, Object> keyMap : keyList) {
            String valueKey = keyMap.get("value").toString();
            if ("source".equals(valueKey)) {
                List<Map<String, Object>> staticSelectList = cacheUtilService.findAllCloudDataType(methodParam.getSchemaName(), methodParam.getUserLang(), methodParam.getCompanyId());
                keyMap.put("keyValue", staticSelectList);
            } else if ("works_log".equals(valueKey)) {
                String condition = " where field like 'log%'";
                List<Map<String, Object>> resource = findResourceForLog(methodParam.getCompanyId(), methodParam.getUserLang(), condition);
                keyMap.put("keyValue", resource);
            } else {
                List<Map<String, Object>> valueList = cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), "work_column_key_value", valueKey, methodParam.getUserLang(), methodParam.getCompanyId());
                keyMap.put("keyValue", valueList);
            }
        }
        return keyList;
    }

    /**
     * 根据字段id获取基本及特殊属性
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    @Override
    public List<Map<String, Object>> getColumnProperty(MethodParam methodParam, WorkTemplateSearchParam bParam) {
        Map<String, Object> columnInfo = workFlowTemplateService.getWorkFlowColumnDetailById(methodParam.getSchemaName(), bParam.getId());
//        Map<String, Object> columnInfo = getWorkTemplateColumnDetailById(methodParam.getSchemaName(), bParam.getId());
        List<Map<String, Object>> htmlBasePropListRow = selectOptionService.getStaticSelectList(methodParam, "html_default_property");
        List<Map<String, Object>> htmlBasePropList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) htmlBasePropListRow);
        String field_view_type = columnInfo.get("field_view_type").toString();//控件类型
        List<Map<String, Object>> keyListRow = cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), "work_column_key", field_view_type, methodParam.getUserLang(), methodParam.getCompanyId());
        List<Map<String, Object>> keyList = (List<Map<String, Object>>) SerializationUtils.clone((Serializable) keyListRow);
        List<Map<String, Object>> extList = this.getWorkTemplateColumnExtByColumnId(methodParam, bParam.getId());
        for (int i = 0; i < keyList.size(); i++) {
            if (keyList.get(i).containsKey("value") && "field_value".equals(keyList.get(i).get("value"))) {
                keyList.remove(i--);
            }
        }
        htmlBasePropList.addAll(keyList);
//        map.put("field_view_type", field_view_type);
        if (RegexUtil.optIsPresentStr(columnInfo.get("data_key"))) {
            this.getDataKeyPropertyFields(methodParam, columnInfo.get("data_key").toString(), htmlBasePropList);
        }
//        this.getSpecialPropertyByFieldViewType(methodParam, htmlBasePropList, field_view_type, extList, keyList);
        return htmlBasePropList;
    }

    /**
     * 编辑模板字段是否显示
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkTemplateColumnUse(MethodParam methodParam, Map<String, Object> data) {
        Map<String, Object> dbColumnInfo = this.getWorkTemplateColumnDetailById(methodParam.getSchemaName(), Integer.valueOf(data.get("id").toString()));
        data.put("schema_name", methodParam.getSchemaName());
        if (!RegexUtil.optIsPresentStr(data.get("field_is_show"))) {
            throw new SenscloudException(LangConstant.MSG_BI);
        }
        if (RegexUtil.optIsPresentStr(data.get("field_is_show")) && "hide".equals(data.get("field_is_show").toString())) {
            data.put("is_required", "2");
        }
        workTemplateMapper.updateWorkTemplateColumnUse(data);
        if (RegexUtil.optNotNull(dbColumnInfo).isPresent()) {
            JSONArray log = LangUtil.compareMap(data, dbColumnInfo);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_8002, dbColumnInfo.get("node_id").toString(), LangUtil.doSetLogArray("编辑工单模板字段使用", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_NG, log.toString()}));
        }
    }

    /**
     * 编辑字段 上移、下移信息
     *
     * @param methodParam
     * @param data
     */
    @Override
    public void modifyWorkTemplateOrder(MethodParam methodParam, Map<String, Object> data) {
        String type = data.get("type").toString();
        data.put("schema_name", methodParam.getSchemaName());
        Map<String, Object> info = this.getWorkTemplateColumnDetailById(methodParam.getSchemaName(), Integer.valueOf(data.get("id").toString()));
        if ("4".equals(type)) {// 上移
            List<Map<String, Object>> upList = workFlowTemplateMapper.findColumnListForDescOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(),null);
            if (RegexUtil.optIsPresentStr(info.get("data_order"))) {
                if (upList.size() > 0) {
                    if (RegexUtil.optEquals(info.get("data_order").toString(), upList.get(0).get("data_order").toString())) {
                        info.put("schema_name", methodParam.getSchemaName());
                        workFlowTemplateMapper.reduceOrderByData(info);
                        upList = workFlowTemplateMapper.findColumnListForDescOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(),null);
                    }
//                    else {
                    upList.get(0).put("schema_name", methodParam.getSchemaName());
                    upList.get(0).put("down_order", info.get("data_order"));
                    data.put("top_order", upList.get(0).get("data_order"));
                    workFlowTemplateMapper.upOrder(data);
                    workFlowTemplateMapper.downOrder(upList.get(0));
//                    }
                }
            } else {

            }
        } else {// 下移
            List<Map<String, Object>> downList = workFlowTemplateMapper.findColumnListForAscOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(),null);
            if (RegexUtil.optIsPresentStr(info.get("data_order"))) {
                if (downList.size() > 0) {
                    if (RegexUtil.optEquals(info.get("data_order").toString(), downList.get(0).get("data_order").toString())) {
                        info.put("schema_name", methodParam.getSchemaName());
                        workFlowTemplateMapper.plusOrderByData(info);
                        downList = workFlowTemplateMapper.findColumnListForAscOrder(methodParam.getSchemaName(), info.get("node_id").toString(), (Integer) info.get("data_order"), info.get("id").toString(),null);
                    }
//                    else {
                    downList.get(0).put("schema_name", methodParam.getSchemaName());
                    downList.get(0).put("top_order", info.get("data_order"));
                    data.put("down_order", downList.get(0).get("data_order"));
                    workFlowTemplateMapper.downOrder(data);
                    workFlowTemplateMapper.upOrder(downList.get(0));
//                    }
                }
            } else {

            }
        }
    }

    /**
     * 通过字段id获取字段特殊属性
     *
     * @param methodParam
     * @param id
     * @return
     */
    private List<Map<String, Object>> getWorkTemplateColumnExtByColumnId(MethodParam methodParam, Integer id) {
        return workTemplateMapper.findWorkTemplateColumnExtByColumnId(methodParam.getSchemaName(), id);
    }


    /**
     * 通过控件类型获取控件的特殊属性列表
     *
     * @param methodParam
     * @param list
     * @param field_view_type
     * @param extList
     * @param keyList
     */
    private void getSpecialPropertyByFieldViewType(MethodParam methodParam, List<Map<String, Object>> list, String field_view_type, List<Map<String, Object>> extList, List<Map<String, Object>> keyList) {
        if ("select".equals(field_view_type)) {
            for (Map<String, Object> extMap : extList) {
                String field_value = extMap.get("field_value").toString();
                if ("source".equals(extMap.get("field_name"))) {
                    List<Map<String, Object>> valueList = cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), "interface_address_value", field_value, methodParam.getUserLang(), methodParam.getCompanyId());
                    list.addAll(valueList);
                }
            }
        }

    }

    /**
     * 通过根据数据源获取字段属性
     *
     * @param methodParam
     * @param data_key
     * @param list
     */
    private void getDataKeyPropertyFields(MethodParam methodParam, String data_key, List<Map<String, Object>> list) {
        List<Map<String, Object>> valueList = cacheUtilService.findSpecialPropertyKey(methodParam.getSchemaName(), "interface_address_value", data_key, methodParam.getUserLang(), methodParam.getCompanyId());
        list.addAll(valueList);
    }

    /**
     * 通过字段id获取字段本身信息
     *
     * @param schemaName
     * @param id
     * @return
     */
    private Map<String, Object> getWorkTemplateColumnDetailById(String schemaName, Integer id) {
        return RegexUtil.optMapOrExpNullInfo(workTemplateMapper.findWorkTemplateColumnInfoById(schemaName, id), LangConstant.TITLE_NG); // 字段不存在
    }

    /**
     * 通过work_template_code和field_form_code获取字段联动信息
     *
     * @param methodParam
     * @param field_form_code
     * @param work_template_code
     * @return
     */
    private List<Map<String, Object>> getWorkTemplateColumnLinkage(MethodParam methodParam, String field_form_code, String work_template_code) {
        List<Map<String, Object>> conditions = workTemplateMapper.findWorkTemplateColumnLinkageByCode(methodParam.getSchemaName(), field_form_code, work_template_code);
        return conditions;
    }

    /**
     * 通过work_template_code和field_form_code获取字段特殊属性
     *
     * @param methodParam
     * @param field_form_code
     * @param work_template_code
     * @return
     */
    private List<Map<String, Object>> getWorkTemplateColumnExt(MethodParam methodParam, String field_form_code, String work_template_code) {
        return workTemplateMapper.findWorkTemplateColumnExtByCode(methodParam.getSchemaName(), field_form_code, work_template_code);
    }

    /**
     * 通过work_template_code和field_form_code获取字段本身信息
     *
     * @param methodParam
     * @param field_form_code
     * @param work_template_code
     * @return
     */
    private Map<String, Object> getWorkTemplateColumnDetail(MethodParam methodParam, String field_form_code, String work_template_code) {
        return RegexUtil.optMapOrExpNullInfo(workTemplateMapper.findWorkTemplateColumnInfoByCode(methodParam.getSchemaName(), work_template_code, field_form_code), LangConstant.TITLE_NG); // 字段不存在
    }

    /**
     * 获取字段列表判断条件
     *
     * @param methodParam
     * @param param
     * @return
     */
    private String getWorkTemplateColumnWhereString(MethodParam methodParam, WorkTemplateSearchParam param) {
        StringBuffer whereString = new StringBuffer();
//        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower((select (c.resource->>wfnc.field_name)::jsonb->>'").append(methodParam.getUserLang()).append("' from public.company_resource c where c.company_id = ").append(methodParam.getCompanyId()).append(")) like ").append(e)
//                .append(" or lower(wfnc.field_code) like ").append(e).append(")"));
        RegexUtil.optNotBlankStrOpt(param.getField_view_types()).ifPresent(e -> whereString.append(" and wfnc.field_view_type in (").append(DataChangeUtil.joinByStr(e)).append(") "));
        return whereString.toString();
    }

    /**
     * 通过work_template_code获取模板基本属性
     *
     * @param methodParam
     * @param work_template_code
     * @return
     */
    @Override
    public Map<String, Object> getWorkTemplateDetailByCode(MethodParam methodParam, String work_template_code) {
        return RegexUtil.optMapOrExpNullInfo(workTemplateMapper.findWorkTemplateInfoByCode(methodParam.getSchemaName(), work_template_code), LangConstant.TITLE_AC_V); // 工单模板不存在
    }

    /**
     * 获取模板列表判断条件
     *
     * @param methodParam
     * @param param
     * @return
     */
    private String getWorkTemplateWhereString(MethodParam methodParam, WorkTemplateSearchParam param) {
        StringBuffer whereString = new StringBuffer(" where 1=1 ");
        RegexUtil.optNotBlankStrLowerOpt(param.getKeywordSearch()).map(e -> "'%" + e.toLowerCase() + "%'").ifPresent(e -> whereString.append(" and (lower(t.work_template_name) like ")
                .append(e).append(")"));
        return whereString.toString();
    }

    public List<Map<String, Object>> findResourceForLog(Long company_id, String user_lang, String condition) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("company_id", company_id);
        jsonObject.put("user_lang", user_lang);
        jsonObject.put("condition", condition);
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat("findLangStaticOption");
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(SenscloudUtil.requestPublicJsonByPost(requestUrl, jsonString));
        return result;
    }

    //    @Override
//    public Map<String, Object> getWorkTemplateColumnSpecialProperty(MethodParam methodParam, WorkTemplateSearchParam bParam) {
//        Map<String, Object> columnInfo = getWorkTemplateColumnDetailById(methodParam.getSchemaName(), bParam.getId());
//        List<Map<String, Object>> extList = this.getWorkTemplateColumnExt(methodParam, columnInfo.get("field_form_code").toString(), columnInfo.get("node_id").toString());
//
//        String field_view_type = columnInfo.get("field_view_type").toString();
//        Map<String, Object> map = new HashMap<>();
//        map.put("field_view_type", field_view_type);
////        getSpecialPropertyByFieldViewType(methodParam, map, field_view_type, extList);
//        return map;
//    }
}
