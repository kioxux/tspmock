package com.gengyun.senscloud.service.impl;

import com.alibaba.excel.EasyExcel;
import com.gengyun.senscloud.common.ImportTypeEnum;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.mapper.AssetCategoryMapper;
import com.gengyun.senscloud.mapper.LogsMapper;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.DataImportService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.util.*;
import net.sf.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据导入功能
 */
@Service
public class DataImportServiceImpl implements DataImportService {

    private static final Logger logger = LoggerFactory.getLogger(DataImportServiceImpl.class);

    @Value("${senscloud.file_upload}")
    private String file_upload_dir;

    @Resource
    private LogsService logsService;

    @Resource
    private LogsMapper logsMapper;

    @Resource
    CommonUtilService commonUtilService;

    @Resource
    CompanyService companyService;

    @Resource
    AssetCategoryMapper assetCategoryMapper;

    /**
     * 导入处理
     *
     * @param methodParam
     * @param importType  数据导入类型
     * @param file
     * @return
     */
    @Override
    @Transactional
    public ResponseModel doImport(MethodParam methodParam, int importType, MultipartFile file) {
        try {
            return doImport(methodParam, file, ImportTypeEnum.getValue(importType), ImportTypeEnum.getBusinessNo(importType));
        } catch (Exception e) {
            throw new SenscloudException(LangConstant.BTN_IMPORT_A, new String[]{LangConstant.TITLE_EIK});
        }
    }

    /**
     * 获取数据导入模板
     *
     * @param methodParam
     * @param importType
     * @return
     */
    @Override
    public void getDataTemplate(MethodParam methodParam, int importType) {
        String sep = System.getProperty("file.separator");
        String fileName = ImportTypeEnum.getTemplateFileName(importType);
        String filePath = file_upload_dir + sep + "importTemplate" + sep + fileName;
        if (1 == importType) {
            DownloadUtil.downloadExtr(filePath, fileName, DownloadUtil.CONTENT_TYPE_EXCEL, false, assetCategoryMapper.findAssetFields(methodParam.getSchemaName()));
        } else {
            DownloadUtil.download(filePath, fileName, DownloadUtil.CONTENT_TYPE_EXCEL, false);
        }
    }

    /**
     * 获取数据导入日志列表
     *
     * @param methodParam
     * @param importType
     * @return
     */
    @Override
    public Map<String, Object> getImportDataLogs(MethodParam methodParam, Integer importType) {
        String pagination = SenscloudUtil.changePagination(methodParam);
        String schemaName = methodParam.getSchemaName();
        Map<String, Object> params = new HashMap<>();
        params.put("log_type", importType);
        if (importType != null) {
            String businessNo = ImportTypeEnum.getBusinessNo(importType.intValue());
            params.put("whereSql", String.format(" L.log_type = '%s' ", businessNo));
        } else {
            StringBuffer typeList = new StringBuffer();
            for (ImportTypeEnum value : ImportTypeEnum.values()) {
                if (typeList.length() > 0) {
                    typeList.append(",");
                }
                typeList.append("'" + value.getBusinessNo() + "'");
            }
            params.put("whereSql", String.format(" L.log_type IN (%s)", typeList.toString()));
        }
        // 获取按条件拼接的sql
        int total = logsMapper.countLogList(schemaName, params); // 获取备件的总数
        Map<String, Object> result = new HashMap<>();
        if (total < 1) {
            result.put("rows", null);
        } else {
            params.put("pagination", pagination);//分页参数
            List<Map<String, Object>> rowList = findLogList(schemaName, methodParam.getUserLang(), methodParam.getCompanyId(), params);
            result.put("rows", rowList);
        }
        result.put("total", total);
        return result;
    }

    /**
     * 获取数据导入类型列表
     *
     * @param methodParam
     * @return
     */
    @Override
    public List<Map<String, Object>> searchImportTypes(MethodParam methodParam) {
        List<Map<String, Object>> mapsList = new ArrayList<>();
        for (ImportTypeEnum value : ImportTypeEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            if (RegexUtil.optEquals(value.getKey(), RegexUtil.optStrOrNull(ImportTypeEnum.WORK_FLOW.getKey()))) {
                List<String> prmList = RegexUtil.optNotNullListStrOrNew(methodParam.getUserPermissionList());
                if (prmList.contains(SensConstant.MENUS[13]) && prmList.contains(SensConstant.MENUS[33])) {
                    map.put("importTypeId", value.getKey());
                    map.put("importTypeName", value.getName());
                    map.put("importBusinessNo", value.getBusinessNo());
                }
            } else if (RegexUtil.optEquals(value.getKey(), RegexUtil.optStrOrNull(ImportTypeEnum.POLLUTE_FEE.getKey()))) {
                List<String> prmList = RegexUtil.optNotNullListStrOrNew(methodParam.getUserPermissionList());
                if (prmList.contains(SensConstant.MENUS[35])) {
                    map.put("importTypeId", value.getKey());
                    map.put("importTypeName", value.getName());
                    map.put("importBusinessNo", value.getBusinessNo());
                }
            } else {
                map.put("importTypeId", value.getKey());
                map.put("importTypeName", value.getName());
                map.put("importBusinessNo", value.getBusinessNo());
            }
            RegexUtil.optNotNullMap(map).ifPresent(mapsList::add);
        }
        return mapsList;
    }

    /**
     * 导入处理
     *
     * @param methodParam
     * @param file
     * @param listener
     * @return
     */
    private ResponseModel doImport(MethodParam methodParam, MultipartFile file, Class<? extends BaseExcelListener> listener, String businessNo) {
        List<ImportLog> logList = new ArrayList<>();
        try {
            String fileName = file.getOriginalFilename();
            if (!fileName.endsWith(".xls") && !fileName.endsWith(".xlsx")) {
                logger.error("文件格式错误，目前只支持xls、xlsx文件！");
                throw new SenscloudException(LangConstant.TITLE_EIM);
            }
            readExcel(methodParam, file, logList, listener, BaseExcelListener.HANDLE_TYPE_CHECK);//读取excel并校验单元格数据
            JSONObject result = new JSONObject();
            if (logList.size() == 0) {//校验没有任务异常信息时，才导入数据
                int count = readExcel(methodParam, file, logList, listener, BaseExcelListener.HANDLE_TYPE_SAVE);//读取excel并导入数据
                //记录成功日志
                String msg = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BDF) + count + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.COM_STRIP_A);//导入成功*条
                result.put("result", LangConstant.TITLE_EIJ);//成功
                result.put("msg", msg);
                logsService.newLog(methodParam, businessNo, "", result.toString());
                return ResponseModel.okMsg(LangConstant.TITLE_BDF);
            } else {
                JSONArray logArr = new JSONArray();
                for (ImportLog importLog : logList) {
                    logArr.add(LangUtil.doSetLogArray("错误提示", LangConstant.TITLE_EIN, new String[]{importLog.getNum(), importLog.getMessage()}));//第{d}行，{d}
                }
                result.put("content", logArr);
                result.put("result", LangConstant.TITLE_EIK);//失败
                //记录错误日志
                logsService.newLog(methodParam, businessNo, "", result.toString());
                return ResponseModel.errorQuickExp(new SenscloudException(LangConstant.BTN_IMPORT_A, new String[]{LangConstant.TITLE_EIK}));
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
            logger.error(e.getMessage());
            throw new SenscloudException(LangConstant.MSG_BK);//未知错误
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                logger.error("", e);
            }
        }
    }

    /**
     * easyExcel读取、处理excel文件数据
     *
     * @param methodParam
     * @param file
     * @param logList
     * @param listener
     * @param type
     * @throws Exception
     */
    private int readExcel(MethodParam methodParam, MultipartFile file, List<ImportLog> logList, Class<? extends BaseExcelListener> listener, String type) throws Exception {
        Class[] paramTypes = {MethodParam.class, String.class, String.class, String.class, List.class};
        Object[] checkListParams = {methodParam, type, methodParam.getSchemaName(), methodParam.getAccount(), logList};
        Constructor con = listener.getConstructor(paramTypes);
        BaseExcelListener checkList = (BaseExcelListener) con.newInstance(checkListParams);
        //这里需要注意 AnalysisEventListener的doAfterAllAnalysed 会在每个sheet读取完毕后调用一次。然后所有sheet都会往同一个AnalysisEventListener里面写
        EasyExcel.read(file.getInputStream(), null, checkList).headRowNumber(checkList.getHeadRowNumber()).doReadAll();
        return checkList.getCount();
    }

    public List<Map<String, Object>> findLogList(String schema_name, String userLang, Long companyId, Map<String, Object> pm) {
        List<Map<String, Object>> logList = logsMapper.findLogList(schema_name, pm);
        Map<String, Object> result = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(companyService.requestLang(companyId, "companyId", "findLangStaticOption")));
        if (RegexUtil.optIsPresentList(logList)) {
            if (RegexUtil.optIsPresentMap(result)) {
                net.sf.json.JSONObject dd = net.sf.json.JSONObject.fromObject(DataChangeUtil.scdObjToMap(result.get("resource")).get("value"));
                logList.forEach(sd -> {
                    if (dd.containsKey(sd.get("log_type_name"))) {
                        String value = RegexUtil.optStrOrNull(net.sf.json.JSONObject.fromObject(dd.get(sd.get("log_type_name"))).get(userLang));
                        if (RegexUtil.optIsPresentStr(value)) {
                            sd.put("log_type_name", value);
                        }
                    }
                });
            }
        }
        return logList;
    }

}
