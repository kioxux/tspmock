package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.CloudDataModel;
import com.gengyun.senscloud.model.MethodParam;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 */
public interface CloudDataService {
    /**
     * 新增数据项类型表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void newCloudDataType(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 获取数据项类型列表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    List<Map<String, Object>> getCloudDataTypeList(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 更新数据项类型表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void modifyCloudDataType(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除数据项类型表
     *
     * @param methodParam    入参
     * @param cloudDataModel 入参
     */
    void cutCloudDataType(MethodParam methodParam, CloudDataModel cloudDataModel);

    /**
     * 根据数据项类型获取数据项列表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    List<Map<String, Object>> getCloudDataListByDataType(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 新增数据项表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void newCloudData(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 更新数据项表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void modifyCloudData(MethodParam methodParam, Map<String, Object> pm);

    /**
     * 删除数据项表
     *
     * @param methodParam 入参
     * @param pm          入参
     */
    void cutCloudData(MethodParam methodParam, Map<String, Object> pm);

}
