package com.gengyun.senscloud.service.impl.guacamole;

import com.gengyun.senscloud.mapper.GuacamoleWebMapper;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.guacamole.GuacamoleWebService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.RegexUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class GuacamoleWebServiceImpl implements GuacamoleWebService {
    @Resource
    GuacamoleWebMapper guacamoleWebMapper;
    @Resource
    SelectOptionService selectOptionService;

    @Override
    public Map<String, String> getFacilityGuacamole(MethodParam methodParam, String facility_id) {
        return guacamoleWebMapper.findFacilityGuacamole(methodParam.getSchemaName(),facility_id);
    }

    @Override
    public List<Map<String, String>> getAssetPositionGuacamole(MethodParam methodParam, String position_code) {
        return guacamoleWebMapper.findAssetPositionGuacamoleList(methodParam.getSchemaName(), "scada_config", position_code);
    }
}
