package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.MaintenanceSettingsService;
import org.springframework.stereotype.Service;

@Service
public class MaintenanceSettingsServiceImpl implements MaintenanceSettingsService {
//
//    @Autowired
//    MaintenanceSettingsMapper maintenanceSettingsMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Override
//    public List<RepairType> repairTypeData(String schema_name) {
//        return maintenanceSettingsMapper.repairTypeData(schema_name);
//    }
//
//    @Override
//    public List<FaultType> faultType(String schema_name) {
//        return maintenanceSettingsMapper.faultType(schema_name);
//    }
//
//    /**
//     * 查询设备状态
//     */
//    @Override
//    public List<AssetStatus> assetStatus(String schema_name) {
//        return maintenanceSettingsMapper.assetStatus(schema_name);
//    }
//
//    /**
//     * 查询保养项
//     */
//    @Override
//    public List<MaintainItemData> maintainItemData(String schema_name, String asset_name) {
//        return maintenanceSettingsMapper.maintainItemData(schema_name, asset_name);
//    }
//
//    /**
//     * 查询保养确认
//     */
//    @Override
//    public List<MaintainCheckItemData> maintainCheckItemData(String schema_name, String asset_name) {
//        return maintenanceSettingsMapper.maintainCheckItemData(schema_name, asset_name);
//    }
//
//    /**
//     * 查询点检
//     */
//    @Override
//    public List<InspectionItemData> inspectionItemData(String schema_name) {
//        return maintenanceSettingsMapper.inspectionItemData(schema_name);
//    }
//
//    /**
//     * 查询巡检
//     */
//    @Override
//    public List<SpotcheckItemData> spotcheckItemData(String schema_name) {
//        return maintenanceSettingsMapper.spotcheckItemData(schema_name);
//    }
//
//    /**
//     * 查询周期
//     */
//    @Override
//    public List<MaintainCycleData> maintainCycleData(String schema_name, String asset_name) {
//        return maintenanceSettingsMapper.maintainCycleData(schema_name, asset_name);
//    }
//
//    //查找所有的保养周期
//    @Override
//    public List<MaintainCycleData> getMaintainCycleDataList(String schema_name) {
//        return maintenanceSettingsMapper.getMaintainCycleDataList(schema_name);
//    }
//
//    @Override
//    public List<FaultCode> faultCodeData(String schema) {
//        return maintenanceSettingsMapper.faultCodeData(schema);
//    }
//
//    @Override
//    public List<PreventiveType> preventiveTypeData(String schema) {
//        return maintenanceSettingsMapper.preventiveTypeData(schema);
//    }
//
//    @Override
//    public List<ImportmentLevel> importmentLevelData(String schema) {
//        return maintenanceSettingsMapper.importmentLevelData(schema);
//    }
//
//    @Override
//    public List<FacilityType> facilityTypeData(String schema) {
//        return maintenanceSettingsMapper.facilityTypeData(schema);
//    }
//
//    @Override
//    public List<UnitData> unitData(String schema) {
//        return maintenanceSettingsMapper.unitData(schema);
//    }
//
//    /**
//     * 查询完修类型表
//     */
//    @Override
//    public List<Map<String, Object>> getWorkFinishedTypeList(String schema_name) {
//        return maintenanceSettingsMapper.getWorkFinishedTypeList(schema_name);
//    }
//
//    /**
//     * 查询设备汇总
//     */
//    @Override
//    public List<MetaDataAsset> findAllMetaDataAsset(String schema_name) {
//        return maintenanceSettingsMapper.findAllMetaDataAsset(schema_name);
//    }
//
//    /**
//     * 更新维修类型
//     */
//    @Override
//    public int UpdaterepairTypeData(String schema_name, String type_name, int order, int id) {
//        return maintenanceSettingsMapper.UpdaterepairTypeData(schema_name, type_name, order, id);
//    }
//
//    /**
//     * 插入维修类型
//     */
//    @Override
//    public int InsertrepairTypeData(RepairType repairType) {
//        return maintenanceSettingsMapper.InsertrepairTypeData(repairType);
//    }
//
//
//    /**
//     * 更新故障类型
//     */
//    @Override
//    public int UpdateFaultType(String schema_name, String type_name, int order, int id) {
//        return maintenanceSettingsMapper.UpdateFaultType(schema_name, type_name, order, id);
//    }
//
//    /**
//     * 插入故障类型
//     */
//    @Override
//    public int InsertFaultType(FaultType faultType) {
//        return maintenanceSettingsMapper.InsertFaultType(faultType);
//    }
//
//
//    /**
//     * 更新设备状态类型
//     */
//    @Override
//    public int UpdateAssetStatus(String schema_name, String Status, int order, int id) {
//        return maintenanceSettingsMapper.UpdateAssetStatus(schema_name, Status, order, id);
//    }
//
//    /**
//     * 插入设备状态类型
//     */
//    @Override
//    public int InsertAssetStatus(AssetStatus assetStatus) {
//        return maintenanceSettingsMapper.InsertAssetStatus(assetStatus);
//    }
//
//    /**
//     * 更新保养类型
//     */
//    @Override
//    public int UpdateMaintainItem(String schema_name, String item_name, int id, String check_note, int order) {
//        return maintenanceSettingsMapper.UpdateMaintainItem(schema_name, item_name, id, check_note, order);
//    }
//
//    /**
//     * 插入保养类型
//     */
//    @Override
//    public int InsertMaintainItem(MaintainItemData maintainItemData) {
//        return maintenanceSettingsMapper.InsertMaintainItem(maintainItemData);
//    }
//
//
//    /**
//     * 更新保养确认项类型
//     */
//    @Override
//    public int UpdateMaintainCheckItem(String schema_name, String item_name, int id, int order) {
//        return maintenanceSettingsMapper.UpdateMaintainCheckItem(schema_name, item_name, id, order);
//    }
//
//    /**
//     * 插入保养确认项类型
//     */
//    @Override
//    public int InsertMaintainCheckItem(MaintainCheckItemData maintainCheckItemData) {
//        return maintenanceSettingsMapper.InsertMaintainCheckItem(maintainCheckItemData);
//    }
//
//
//    /**
//     * 更新点检类型
//     */
//    @Override
//    public int UpdateSpotcheck(String schema_name, String item_name, int id, String check_note, int order, int resultType) {
//        return maintenanceSettingsMapper.UpdateSpotcheck(schema_name, item_name, id, check_note, order, resultType);
//    }
//
//    /**
//     * 插入点检类型
//     */
//    @Override
//    public int InsertSpotcheck(SpotcheckItemData spotcheckItemData) {
//        return maintenanceSettingsMapper.InsertSpotcheck(spotcheckItemData);
//    }
//
//    /**
//     * 更新巡检类型
//     */
//    @Override
//    public int UpdateInspection(String schema_name, String item_name, int id, String check_note, int order, int resultType) {
//        return maintenanceSettingsMapper.UpdateInspection(schema_name, item_name, id, check_note, order, resultType);
//    }
//
//    /**
//     * 插入巡检类型
//     */
//    @Override
//    public int InsertInspection(InspectionItemData inspectionItemData) {
//        return maintenanceSettingsMapper.InsertInspection(inspectionItemData);
//    }
//
//    /**
//     * 更新周期
//     */
//    @Override
//    public int UpdateMaintainCycleData(String schema_name, int cycle_count, int id, int order) {
//        return maintenanceSettingsMapper.UpdateMaintainCycleData(schema_name, cycle_count, id, order);
//    }
//
//    /**
//     * 插入周期
//     */
//    @Override
//    public int InsertMaintainCycleData(MaintainCycleData maintainCycleData) {
//        return maintenanceSettingsMapper.InsertMaintainCycleData(maintainCycleData);
//    }
//
//    /**
//     * 更新故障代码
//     */
//    @Override
//    public int updateFaultCode(String schema_name, String code_name, int order,String code,String fault_code, String assetCategorys, String remark) {
//        maintenanceSettingsMapper.deleteFaultCodeWithCategory(schema_name, code);
//        if (RegexUtil.isNotNull(assetCategorys)) {
//            String[] asset_categorys = assetCategorys.split(",");
//            for (String category_id : asset_categorys) {
//                maintenanceSettingsMapper.addFaultCodeWithCategory(schema_name, code, Integer.parseInt(category_id));
//            }
//        }
//        return maintenanceSettingsMapper.updateFaultCode(schema_name, code_name, order, code,fault_code, remark);
//    }
//
//    /**
//     * 插入故障代码
//     */
//    @Override
//    public int insertFaultCode(FaultCode faultCode, String assetCategorys) {
//        int i=maintenanceSettingsMapper.insertFaultCode(faultCode);
//        if (RegexUtil.isNotNull(assetCategorys)&&i>0) {
//            String[] asset_categorys = assetCategorys.split(",");
//            for (String category_id : asset_categorys) {
//                maintenanceSettingsMapper.addFaultCodeWithCategory(faultCode.getSchema_name(), faultCode.getCode(), Integer.parseInt(category_id));
//            }
//        }
//        return i;
//    }
//
//    @Override
//    public int updatePreventiveType(String schema_name, String type_name, int order, int id) {
//        return maintenanceSettingsMapper.updatePreventiveType(schema_name, type_name, order, id);
//    }
//
//    @Override
//    public int insertPreventiveType(PreventiveType preventiveType) {
//        return maintenanceSettingsMapper.insertPreventiveType(preventiveType);
//    }
//
//    @Override
//    public int updateImportmentLevel(String schema_name, String level_name, int order, int id) {
//        return maintenanceSettingsMapper.updateImportmentLevel(schema_name, level_name, order, id);
//    }
//
//    @Override
//    public int insertImportmentLevel(ImportmentLevel importmentLevel) {
//        return maintenanceSettingsMapper.insertImportmentLevel(importmentLevel);
//    }
//
//    @Override
//    public int updateFacilityType(String schema_name, String type_name, int id) {
//        return maintenanceSettingsMapper.updateFacilityType(schema_name, type_name, id);
//    }
//
//    @Override
//    public int insertFacilityType(FacilityType facilityType) {
//        return maintenanceSettingsMapper.insertFacilityType(facilityType);
//    }
//
//    @Override
//    public int updateUnitData(String schema_name, String unit_name, int id) {
//        return maintenanceSettingsMapper.updateUnitData(schema_name, unit_name, id);
//    }
//
//    @Override
//    public int insertUnitData(UnitData unitData) {
//        return maintenanceSettingsMapper.insertUnitData(unitData);
//    }
//
//    /**
//     * 更新完修类型
//     */
//    @Override
//    public int updateWorkFinishedTypeData(String schema_name, String finished_name, int is_finished, String operate_account, Timestamp operate_time, int id) {
//        return maintenanceSettingsMapper.updateWorkFinishedTypeData(schema_name, finished_name, is_finished, operate_account, operate_time, id);
//    }
//
//    /**
//     * 插入完修类型
//     */
//    @Override
//    public int insertWorkFinishedTypeData(String schema_name, String finished_name, int is_finished, String operate_account, Timestamp operate_time) {
//        return maintenanceSettingsMapper.insertWorkFinishedTypeData(schema_name, finished_name, is_finished, operate_account, operate_time);
//    }
//
//    /**
//     * 删除维修类型
//     */
//    @Override
//    public int deleteRepairData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deleteRepairData(schema_name, id, isUse);
//    }
//
//    /**
//     * 删除故障类型
//     */
//    @Override
//    public int deleteFaultData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deleteFaultData(schema_name, id, isUse);
//    }
//
//    /**
//     * 删除状态类型
//     */
//    @Override
//    public int deleteAssetStatusData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deleteAssetStatusData(schema_name, id, isUse);
//    }
//
//
//    /**
//     * 删除保养项类型
//     */
//    @Override
//    public int deleteMaintainData(String schema_name, int id) {
//        return maintenanceSettingsMapper.deleteMaintainData(schema_name, id);
//    }
//
//
//    /**
//     * 删除保养确认类型
//     */
//    @Override
//    public int deleteMaintainCheckData(String schema_name, int id) {
//        return maintenanceSettingsMapper.deleteMaintainCheckData(schema_name, id);
//    }
//
//
//    /**
//     * 删除巡检类型
//     */
//    @Override
//    public int deleteInspectionData(String schema_name, int id) {
//        return maintenanceSettingsMapper.deleteInspectionData(schema_name, id);
//    }
//
//
//    /**
//     * 删除点检类型
//     */
//    @Override
//    public int deleteSpotcheckData(String schema_name, int id) {
//        return maintenanceSettingsMapper.deleteSpotcheckData(schema_name, id);
//    }
//
//    /**
//     * 删除周期
//     */
//    @Override
//    public int deleteMaintainCycleData(String schema_name, int id) {
//        return maintenanceSettingsMapper.deleteMaintainCycleData(schema_name, id);
//    }
//
//    @Override
//    public int deleteFaultCodeData(String schema_name, String id, String isUse) {
//        return maintenanceSettingsMapper.deleteFaultCodeData(schema_name, id, isUse);
//    }
//
//    @Override
//    public int deletePreventiveTypeData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deletePreventiveTypeData(schema_name, id, isUse);
//    }
//
//    @Override
//    public int deleteImportmentLevelData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deleteImportmentLevelData(schema_name, id, isUse);
//    }
//
//    @Override
//    public int deleteFacilityTypeData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deleteFacilityTypeData(schema_name, id, isUse);
//    }
//
//    @Override
//    public int deleteUnitData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.deleteUnitData(schema_name, id, isUse);
//    }
//
//
//    //设备运行状态
//    @Override
//    public List<AssetRunningData> getAssetRunningDataList(String schema_name) {
//        return maintenanceSettingsMapper.getAssetRunningDataList(schema_name);
//    }
//
//    //设备运行状态最大id
//    @Override
//    public int getAssetRunningMaxId(String schema_name) {
//        return maintenanceSettingsMapper.getAssetRunningMaxId(schema_name);
//    }
//
//    //设备运行状态新增
//    @Override
//    public int addAssetRunningData(String schema_name, AssetRunningData data) {
//        return maintenanceSettingsMapper.addAssetRunningData(schema_name, data);
//    }
//
//    //设备运行状态修改
//    @Override
//    public int updateAssetRunningData(String schema_name, String running_status, int order, int id, int priority_level) {
//        return maintenanceSettingsMapper.updateAssetRunningData(schema_name, running_status, order, id, priority_level);
//    }
//
//    //启用禁用设备运行状态
//    @Override
//    public int disableAssetRunningData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.disableAssetRunningData(schema_name, id, isUse);
//    }
//
//    //工单类型状态
//    @Override
//    public List<WorkTypeData> getWorkTypeDataList(String schema_name) {
//        return maintenanceSettingsMapper.getWorkTypeDataList(schema_name);
//    }
//
//    @Override
//    public List<WorkTypeData> getWorkTypeDataListByBusiessTypeId(String schema_name, int business_type_id) {
//        return maintenanceSettingsMapper.getWorkTypeDataListByBusiessTypeId(schema_name, business_type_id);
//    }
//
//
//    //工单类型最大id
//    @Override
//    public int getWorkTypeMaxId(String schema_name) {
//        return maintenanceSettingsMapper.getWorkTypeMaxId(schema_name);
//    }
//
//    //工单类型状态新增
//    @Override
//    public int addWorkTypeData(String schema_name, WorkTypeData data) {
//        return maintenanceSettingsMapper.addWorkTypeData(schema_name, data);
//    }
//
//    //工单类型状态修改
//    @Override
//    public int updateWorkTypeData(String schema_name, String type_name, String flow_template_code, String work_template_code, int business_type_id, int order, int id) {
//        return maintenanceSettingsMapper.updateWorkTypeData(schema_name, type_name, flow_template_code, work_template_code, business_type_id, order, id);
//    }
//
//    //启用、禁用工单类型状态
//    @Override
//    public int disableWorkTypeData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.disableWorkTypeData(schema_name, id, isUse);
//    }
//
//    //文档类型状态
//    @Override
//    public List<FileTypeData> getFileTypeDataList(String schema_name) {
//        return maintenanceSettingsMapper.getFileTypeDataList(schema_name);
//    }
//
//    //文档类型最大id
//    @Override
//    public int getFileTypeMaxId(String schema_name) {
//        return maintenanceSettingsMapper.getFileTypeMaxId(schema_name);
//    }
//
//    //文档类型新增
//    @Override
//    public int addFileTypeData(String schema_name, FileTypeData data) {
//        return maintenanceSettingsMapper.addFileTypeData(schema_name, data);
//    }
//
//    //文档类型修改
//    @Override
//    public int updateFileTypeData(String schema_name, String running_status, int order, int id) {
//        return maintenanceSettingsMapper.updateFileTypeData(schema_name, running_status, order, id);
//    }
//
//    //启用、禁用文档类型
//    @Override
//    public int disablFileTypeData(String schema_name, int id, String isUse) {
//        return maintenanceSettingsMapper.disablFileTypeData(schema_name, id, isUse);
//    }
//
//    //寻找所有有效的工单类型
//    @Override
//    public List<WorkTypeData> getWorkTypeListInUse(String schema_name) {
//        return maintenanceSettingsMapper.getWorkTypeListInUse(schema_name);
//    }
//
//    //查找工单状态
//    @Override
//    public List<WorkStatus> getWorkStatus(String schema_name) {
//        return maintenanceSettingsMapper.getWorkStatus(schema_name);
//    }
//
//    //查找工单业务
//    @Override
//    public List<Map<String, Object>> findAllBusinessType(String schema_name) {
//        return maintenanceSettingsMapper.findAllBusinessType(schema_name);
//    }
}
