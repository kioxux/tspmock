package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.service.asset.AssetDiscardService;
import org.springframework.stereotype.Service;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 上午 11:14 2019/4/30 0030
 */
@Service
public class AssetDiscardServiceImpl implements AssetDiscardService {
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    AssetDiscardMapper assetDiscardMapper;
//
//    @Autowired
//    AssetTransferService assetTransferService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    LogsService logsService;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    AssetInventoryMapper assetInventoryMapper;
//
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    MessageService messageService;
//
//    /**
//     * @param schema_name
//     * @param id
//     * @return
//     * @Description:查看详情
//     */
//    @Override
//    public AssetDiscardModel selectDetailById(String schema_name, int id) {
//        AssetDiscardModel model = assetDiscardMapper.selectDetailById(schema_name, id);
//        return model;
//    }
//
//    /**
//     * @param schena_name
//     * @param condition
//     * @return
//     * @Description：查看列表
//     */
//    @Override
//    public List<Map<String, Object>> selectAllDiscard(String schena_name, String condition, User loginUser) {
//        List<Map<String, Object>> all = assetDiscardMapper.selectAllDiscard(schena_name, condition);
//        handlerFlowData(schena_name, loginUser.getAccount(), all);
//        return all;
//
//    }
//
//    @Override
//    public int selectAllDiscardNum(String schena_name, String condition) {
//        int num = assetDiscardMapper.selectAllDiscardNum(schena_name, condition);
//        return num;
//    }
//
//    /**
//     * @param schema_name
//     * @param remark
//     * @param discardCode
//     * @return
//     * @Description:设备报废处理
//     */
//    @Override
//    @Transactional
//    public boolean deal(String schema_name, String remark, String discardCode, String mark, String account, String asset_id, String taskId, String asset_name) {
//        boolean result = false;
//        int status = 0;
//        String key_id = "";
//        String key = Constants.ASSET_RETIREMENT;
//        Map<String, Object> map = new HashMap<String, Object>();
//        List<ProcessDefinition> processDefinitions = workflowService.getDefinitions(schema_name, "", key, true, 10, 1, "key", "desc");
//        if (processDefinitions != null && processDefinitions.size() > 0) {
//            for (ProcessDefinition process : processDefinitions) {
//                key_id = process.getId();
//            }
//        }
//        if (mark != null && mark.equals("submit")) {//提交
//            status = 1;
//            map.put("sub_work_code", discardCode);
//            map.put("title_page", asset_name + selectOptionService.getLanguageInfo(LangConstant.DISCARD_APPLY));//报废申请
//            map.put("do_flow_key", ButtonConstant.BTN_SUBMIT);
//            WorkflowStartResult workflow = workflowService.start(schema_name, key_id, account, map);//调用流程
//            boolean result_b = workflow.isStarted();
//            if (!result_b) {
//                return false;
//            }
//        } else if (mark != null && mark.equals("ok")) {//确定
//            status = 2;
//            Map<String, String> map_ok = new HashMap<String, String>();
//            map_ok.put("sub_work_code", discardCode);
//            map_ok.put("title_page", asset_name + selectOptionService.getLanguageInfo(LangConstant.TITLE_DISCARD_SUCCESS));//报废成功
//            map_ok.put("do_flow_key", ButtonConstant.BTN_AGREE + "");
//            boolean workflow = workflowService.complete(schema_name, taskId, account, map_ok);//同意
//            if (!workflow) {
//                return false;
//            }
//        } else if (mark != null && mark.equals("cancel")) {//作废同样也得关闭
//            status = 3;
//            Map<String, String> map_ok = new HashMap<String, String>();
//            map_ok.put("sub_work_code", discardCode);
//            map_ok.put("title_page", asset_name + selectOptionService.getLanguageInfo(LangConstant.DISCARD_CANCEL));//报废作废
//            map_ok.put("do_flow_key", ButtonConstant.BTN_AGREE + "");
//            if (taskId != null && !taskId.equals("")) {
//                boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, taskId, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            }
//        } else if (mark != null && mark.equals("go_back")) {//退回
//            status = 1;
//            Map<String, String> map_ok = new HashMap<String, String>();
//            map.put("sub_work_code", discardCode);
//            map.put("title_page", asset_name + selectOptionService.getLanguageInfo(LangConstant.DISCARD_BACK));//报废退回
//            map.put("do_flow_key", ButtonConstant.BTN_RETURN);//关闭流程
//            boolean workflow = workflowService.complete(schema_name, taskId, account, map_ok);//调用流程
//            if (!workflow) {
//                return false;
//            }
//        }
//        int doUpdate = assetDiscardMapper.deal(schema_name, status, remark, discardCode);
//        if (doUpdate > 0) {
//            result = true;
//            if (mark != null && mark.equals("ok")) {
//                //修改设备状态为作废
//                int doDiscard = assetDiscardMapper.discardAsset(schema_name, asset_id);
//                if (doDiscard <= 0) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//                }
//
//            }
//        }
//        return result;
//    }
//
//    /**
//     * @param schema_name
//     * @return
//     * @Description:查看报废历史记录
//     */
//    @Override
//    public List<AssetDiscardModel> history(String schema_name) {
//        return null;
//    }
//
//    @Override
//    public ResponseModel cancel(String schema_name, User loginUser, String discard_code) {
//        int doUpdate = assetDiscardMapper.updateStatus(schema_name, StatusConstant.CANCEL, discard_code,null);
//        if (doUpdate > 0) {
//            boolean deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, discard_code, selectOptionService.getLanguageInfo(LangConstant.OBSOLETE_A));//作废
//            if (deleteflow) {
//                workProcessService.saveWorkProccess(schema_name, discard_code, StatusConstant.CANCEL, loginUser.getAccount(), Constants.PROCESS_CANCEL_TASK);//工单进度记录
//                logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_23), loginUser.getAccount(), selectOptionService.getLanguageInfo(LangConstant.ASSET_TRANS_CANCEL), loginUser.getAccount());//设备调度报废
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_OBSOLETE_SUCCESS));//作废成功
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_CANCEL_FLOW_ERROR));//设备报废作废流程调用失败
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_CANCEL_FAIL));//设备报废作废失败
//        }
//    }
//
//    @Override
//    public ResponseModel add(String schema_name, HttpServletRequest request, User loginUser, String processDefinitionId, Map<String, Object> paramMap, boolean hasAudit) throws Exception {
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DISCARD_DATA_WRONG));//当前报废单信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> asset_discard_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_discard");
//        Map<String, Object> asset_discard_detail_tmp = (Map<String, Object>) dataInfo.get("_sc_asset_discard_detail");
//        Map<String, Object> keys = (Map<String, Object>) map_object.get("keys");
//        Object body_property = paramMap.get("body_property");
//        String key = (String) keys.get("_sc_asset_discard");
//        List<Map<String, Object>> assetList = (List<Map<String, Object>>) map_object.get("relationIdContent");
//        boolean isNew = true;
//        if (hasAudit) {
//            //判断是否是新增
//            if (!key.startsWith(SqlConstant.IS_NEW_DATA))
//                isNew = false;
//        }
//        return addDiscard(schema_name, do_flow_key, loginUser, processDefinitionId, paramMap, map_object, asset_discard_tmp, asset_discard_detail_tmp, assetList, body_property.toString(), null, isNew, hasAudit);
//    }
//
//    /**
//     * 生成设备报废以及报废流程
//     *
//     * @param schema_name
//     * @param do_flow_key
//     * @param user
//     * @param processDefinitionId
//     * @param paramMap
//     * @param map_object
//     * @param asset_discard_tmp
//     * @param asset_discard_detail_tmp
//     * @param assetList
//     * @param body_property
//     * @param source
//     * @param isNew
//     * @param hasAudit
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public ResponseModel addDiscard(String schema_name, String do_flow_key, User user, String processDefinitionId, Map<String, Object> paramMap, Map<String, Object> map_object,
//                                    Map<String, Object> asset_discard_tmp, Map<String, Object> asset_discard_detail_tmp, List<Map<String, Object>> assetList,
//                                    String body_property, String source, boolean isNew, boolean hasAudit) throws Exception {
//        String title = hasAudit ? (isNew ? selectOptionService.getLanguageInfo(LangConstant.ADD_K) : selectOptionService.getLanguageInfo(LangConstant.MOD_A)) : "";//新增;修改
//
//        String positionId = (String) asset_discard_tmp.get("position_id");
//        String remark = (String) asset_discard_tmp.get("remark");
//        String facility_id = (String) asset_discard_tmp.get("facility_id");
//        String subWorkCode = (String) asset_discard_tmp.get("discard_code");
//        String asset_ids = (String) asset_discard_detail_tmp.get("relation_id");
//        String[] assetIds = asset_ids.split(",");
//        String account = user.getAccount();
//        List<Map<String, Object>> assets = assetMapper.getAssetDetailByIds(schema_name, assetIds);
//        if (assets.isEmpty()) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_IS_NULL));//设备不能为空
//        } else {
//            for (Map<String, Object> asset : assets) {
//                if (asset == null) {
//                    return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_DATA_WRONG));//报废单出现错误
//                } else if ((Integer) asset.get("intstatus") == StatusConstant.ASSET_INTSTATUS_DISCARD) {
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_ALREADY_DISCARD), asset.get("strname") + "（" + asset.get("strcode") + "）"));//设备“%s”已经报废，无法再次报废
//                }
//            }
//        }
//
//        AssetDiscardModel assetDiscardModel = new AssetDiscardModel();
//        assetDiscardModel.setDiscard_code(subWorkCode);
//        assetDiscardModel.setSchema_name(schema_name);
//        assetDiscardModel.setRemark(remark);
//        assetDiscardModel.setPosition_id(positionId);
//        assetDiscardModel.setSource_sub_code(source);
//        if (hasAudit)
//            assetDiscardModel.setStatus(StatusConstant.TO_BE_AUDITED);//待审核状态
//        else
//            assetDiscardModel.setStatus(StatusConstant.COMPLETED);//待审核状态
//        assetDiscardModel.setBody_property(body_property);
//        if (isNew) {
//            assetDiscardModel.setCreate_user_account(account);
//            assetDiscardModel.setCreate_time(new Timestamp(System.currentTimeMillis()));
//        }
//
//        int doAdd = 0;
//        if (ButtonConstant.BTN_SUBMIT == Integer.valueOf(do_flow_key)) {//提交报废申请
//            if (isNew)
//                doAdd = assetDiscardMapper.add(assetDiscardModel);
//            else
//                doAdd = assetDiscardMapper.updateAssetDiscard(assetDiscardModel);
//
//            if (doAdd > 0) {
//                if (!isNew)//如果是修改，则要把旧的设备报废详情表记录清空
//                    assetDiscardMapper.deleteAssetDiscardDetail(schema_name, subWorkCode);
//                if (doAdd == 0) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_DATA_WRONG));//报废单出现错误
//                } else {
//                    if (!hasAudit) {
//                        String assetId = null;
//                        for (Map<String, Object> assetMap : assetList) {
//                            assetId = (String) assetMap.get("_id");
//                            assetMap.put("schema_name", schema_name);
//                            assetMap.put("discard_code", subWorkCode);
//                            assetDiscardMapper.add_discard_detail(assetMap);
//                            assetDiscardMapper.updateAssetStatus(schema_name, StatusConstant.ASSET_INTSTATUS_DISCARD, assetId);//改变设备状态
//
//                            if (RegexUtil.isNull(source) || !source.startsWith("AID")) {
//                                String logContent = selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD);
//                                logsService.AddLog(schema_name, "asset", assetId, logContent, account);
//                            }
//                        }
//                        //如果是设备盘点发起的报废申请，则报废完后同步更新盘点数据
//                        if (StringUtils.isNotBlank(source) && source.startsWith("AID"))
//                            updateAssetInventoryStatus(schema_name, assetId, source, account);
//                    } else
//                        for (Map<String, Object> assetMap : assetList) {
//                            assetMap.put("schema_name", schema_name);
//                            assetMap.put("discard_code", subWorkCode);
//                            assetDiscardMapper.add_discard_detail(assetMap);
//
//                        }
//                }
//            }
//        }
//        if (doAdd > 0) {
//            //流程数据，跳转到审核流程
//            Map map = new HashMap<>();
//            map.put("title_page", selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD));//设备报废
//            map.put("sub_work_code", subWorkCode);
//            map.put("workCode", subWorkCode);
//            map.put("do_flow_key", do_flow_key);
//            map.put("status", String.valueOf(assetDiscardModel.getStatus()));
//            map.put("create_user_account", account);
//            map.put("create_time", UtilFuns.sysTime());
//            map.put("facility_id", positionId); // 所属位置
//            map.put("receive_account", commonUtilService.getRandomUserByInfo(schema_name, null, map, map_object));
//            if (isNew) {
//                WorkflowStartResult workflowStartResult = workflowService.startWithForm(schema_name, processDefinitionId, account, map);
//                if (null == workflowStartResult || !workflowStartResult.isStarted()) {
//                    throw new Exception();
//                }
//                workProcessService.saveWorkProccess(schema_name, subWorkCode, assetDiscardModel.getStatus(), account, Constants.PROCESS_CREATE_TASK);//工单进度记录
//            } else {
//                String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, subWorkCode);
//                if (StringUtils.isBlank(taskId))
//                    return ResponseModel.errorMsg(String.format(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_DATA_ERROR), title));//设备报废流程信息异常，%s设备报废申请失败
//                String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, subWorkCode);
//                boolean isSuccess = workflowService.complete(schema_name, taskId, account, map);
//                if (!isSuccess) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_FLOW_ERROR));//设备报废流程启动失败
//                }
//                workProcessService.saveWorkProccess(schema_name, subWorkCode, assetDiscardModel.getStatus(), account, taskSid);//工单进度记录
//            }
//            if (hasAudit) {
//                messageService.msgPreProcess(qcloudsmsConfig.SMS_10002002, (String) map.get("receive_account"), SensConstant.BUSINESS_NO_23, subWorkCode, account, user.getUsername(), null, null); // 发送短信
//            }
//            title = title + selectOptionService.getLanguageInfo(SensConstant.LOG_TYPE_NAME.get(SensConstant.BUSINESS_NO_23)) + selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//            logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_23), subWorkCode, title, account);//记录历史;成功
//            if (StringUtils.isNotBlank(source))
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_APPLY_SUCC));//设备报废申请成功
//            else
//                return ResponseModel.ok("ok");
//        } else {
//            return ResponseModel.errorMsg(title + selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_DATA_WRONG));//报废单出现错误
//        }
//    }
//
//    /**
//     * 更新设备盘点状态
//     *
//     * @param schemaName
//     * @param assetId
//     * @param sourceSubCode
//     * @param account
//     * @return
//     */
//    private void updateAssetInventoryStatus(String schemaName, String assetId, String sourceSubCode, String account) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("asset_id", assetId);
//        map.put("deal_result", 3);
//        map.put("sub_inventory_code", sourceSubCode);
//        assetInventoryMapper.updateAssetInventoryOrgDetailDealResult(schemaName, map);
//        String logContent = selectOptionService.getLanguageInfo(LangConstant.LOG_DISCARD_A);
//        logsService.AddLog(schemaName, "asset", assetId, logContent, account);
//    }
//
//    @Override
//    public Map<String, Object> queryAssetDiscardByCode(String schemaName, String subWorkCode) {
//        return assetDiscardMapper.selectDetailByDiscardCode(schemaName, subWorkCode);
//    }
//
//    @Override
//    public ModelAndView getDetailInfo(HttpServletRequest request, String url) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("discard_code");
//        int status = Integer.parseInt(String.valueOf(assetDiscardMapper.selectDetailByDiscardCode(schemaName, code).get("status")));
//        Map<String, String> permissionInfo = new HashMap<String, String>();
//        if (status != StatusConstant.COMPLETED) {
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//        }
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, url, "todo_list");
//        String workTypeId = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_23, "code");
//        String whereString = "and(template_type=2 and work_type_id=" + workTypeId + ")"; // 工单请求的详情
//        String workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//        mav.addObject("workFormKey", workFormKey);
//        return mav.addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("businessUrl", "/assetDiscard/findSingleAdInfo")
//                .addObject("workCode", code)
//                .addObject("HandleFlag", false)
//                .addObject("DetailFlag", true)
//                .addObject("EditFlag", false)
//                .addObject("finished_time_interval", null)
//                .addObject("arrive_time_interval", null)
//                .addObject("subWorkCode", code)
//                .addObject("flow_id", null)
//                .addObject("do_flow_key", null)
//                .addObject("actionUrl", null)
//                .addObject("subList", "[]");
//    }
//
//
//    @Override
//    public ResponseModel disCardAudit(String schema_name, HttpServletRequest request, Map<String, Object> paramMap, User user, boolean isLastAudit) throws Exception {
//
//        Map<String, Object> map_object = dynamicCommonService.doAnalysisPageData(schema_name, paramMap);
//        if (map_object == null || map_object.size() < 1)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_INFO_WRONG));//当前设备报废信息有错误！
//
//        Map<String, Object> dataInfo = (Map<String, Object>) map_object.get("dataInfo");
//        Map<String, Object> _sc_asset_discard = (Map<String, Object>) dataInfo.get("_sc_asset_discard");
//        String discard_code = (String) _sc_asset_discard.get("discard_code");
//        List<Map<String, Object>> assetList = JSONArray.fromObject(map_object.get("relationIdContent"));
//        String body_property = (String) _sc_asset_discard.get("body_property");
//        String do_flow_key = request.getParameter("do_flow_key");//按钮类型
//        if (StringUtils.isBlank(discard_code) || StringUtils.isBlank(do_flow_key))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));//获取系统参数异常
//        Map<String, Object> assetDiscard = assetDiscardMapper.selectDetailByDiscardCode(schema_name, discard_code);
//        if (assetDiscard == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_DATA_ERROR_AUDIT_FAIL));//设备报废信息异常，审核失败
//
//        if (!String.valueOf(assetDiscard.get("status")).equals(String.valueOf(StatusConstant.TO_BE_AUDITED)))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_STATUS_ERROR_AUDIT_FAIL));//设备报废状态异常审核失败
//
//        String taskId = workflowService.getTaskIdBySubWorkCode(schema_name, discard_code);
//        if (StringUtils.isBlank(taskId))
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_FLOW_ERROR_AUDIT_FAIL));//设备报废流程信息异常审核失败
//
//        Map<String, String> map = new HashMap<>();
//        int result = 0;
//        int status = StatusConstant.TO_BE_AUDITED;
//        if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key) && isLastAudit) {//如果审核同意，则更新报废申请附件
//            //如果是设备盘点发起的报废申请，则报废完后同步更新盘点数据
//            String source_sub_code = (String) assetDiscard.get("source_sub_code");
//            String assetId = null;
//            status = StatusConstant.COMPLETED;
//            for (Map<String, Object> assetMap : assetList) {
//                assetId = (String) assetMap.get("_id");
//                result += assetDiscardMapper.updateAssetStatus(schema_name, StatusConstant.ASSET_INTSTATUS_DISCARD, assetId);//改变设备状态
//
//                if (RegexUtil.isNull(source_sub_code) || !source_sub_code.startsWith("AID")) {
//                    String logContent = selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD);
//                    logsService.AddLog(schema_name, "asset", assetId, logContent, user.getAccount());
//                }
//            }
//            assetDiscardMapper.updateStatus(schema_name, status, discard_code,body_property);
//            if (StringUtils.isNotBlank(source_sub_code) && source_sub_code.startsWith("AID"))
//                map.put("receive_account", null);
//            updateAssetInventoryStatus(schema_name, assetId, source_sub_code, user.getAccount());
//        } else if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {//如果审核退回，则报废申请状态变更为“处理中”
//            status = StatusConstant.PENDING;
//            result = assetDiscardMapper.updateStatus(schema_name, status, discard_code,body_property);
//            map.put("receive_account", (String) assetDiscard.get("create_user_account"));
//        } else {
//            result = assetDiscardMapper.updateStatus(schema_name, StatusConstant.TO_BE_AUDITED, discard_code,body_property);
//            String positionId = (String) _sc_asset_discard.get("position_id");
//            map.put("facility_id", positionId); // 所属位置
//            map.put("receive_account", commonUtilService.getRandomUserByInfo(schema_name, null, map, map_object));
//        }
//        if (result > 0) {
//            String title_page = "";
//            String logRemark = "";
//            if (ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)) {
//                title_page = selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD_BACK);//设备报废退回
//                logRemark = selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD_BACK);//设备报废退回
//            } else if (ButtonConstant.BTN_AGREE == Integer.valueOf(do_flow_key)) {
//                title_page = selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD_SUCC);//设备报废成功
//                logRemark = selectOptionService.getLanguageInfo(LangConstant.ASSET_DISCARD_AUDIT_SUCC);//报废审核成功
//            }
//            //完成流程
//            map.put("sub_work_code", discard_code);
//            map.put("title_page", title_page);
//            map.put("do_flow_key", do_flow_key);
//            map.put("status", String.valueOf(status));
//            String taskSid = workflowService.getTaskKeyBySubWorkCode(schema_name, discard_code);
//            boolean isSuccess = workflowService.complete(schema_name, taskId, user.getAccount(), map);
//            if (isSuccess) {
//                workProcessService.saveWorkProccess(schema_name, discard_code, status, user.getAccount(), taskSid);//工单进度记录
//                messageService.msgPreProcess(ButtonConstant.BTN_RETURN == Integer.valueOf(do_flow_key)?qcloudsmsConfig.SMS_10002006:qcloudsmsConfig.SMS_10002005,
//                        map.get("receive_account"), SensConstant.BUSINESS_NO_23, discard_code, user.getAccount(), user.getUsername(), null, null); // 发送短信
//                logsService.AddLog(schema_name, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_23), discard_code, logRemark, user.getAccount());
//                return ResponseModel.ok("ok");
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_DISCARD_AUDIT_FAIL));//设备报废审核出现错误，请重试
//            }
//        } else {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作出现错误，请重试
//        }
//
//    }
//
//    /**
//     * 查询获取流程taskId、按钮权限等数据
//     *
//     * @param schema_name
//     * @param dataList
//     */
//    private void handlerFlowData(String schema_name, String account, List<Map<String, Object>> dataList) {
//        if (dataList == null || dataList.size() == 0)
//            return;
//
//        Map<String, CustomTaskEntityImpl> subWorkCode2AssigneeMap = new HashMap<>();
//        List<String> subWorkCodes = new ArrayList<>();
//        dataList.stream().forEach(data -> subWorkCodes.add((String) data.get("discard_code")));
//        if(subWorkCodes.size() > 0)
//            subWorkCode2AssigneeMap = workflowService.getSubWorkCode2TasksMap(schema_name, "", null, subWorkCodes, null, null, false, null, null, null, null, null);
//
//        for (Map<String, Object> data : dataList) {
//            String subWorkCode = (String) data.get("discard_code");
//            //获取流程信息
//            CustomTaskEntityImpl task = subWorkCode2AssigneeMap.get(subWorkCode);
//            if (task != null) {
//                data.put("taskId", task.getId());
//                data.put("isBtnShow", account.equals(task.getAssignee()));
//                data.put("formKey", task.getFormKey());
//            } else {
//                data.put("taskId", null);
//                data.put("isBtnShow", false);
//                data.put("formKey", null);
//            }
//        }
//    }
}
