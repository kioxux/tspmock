package com.gengyun.senscloud.service.dynamic;

import com.gengyun.senscloud.model.MethodParam;

import java.util.Map;

/**
 * 工单处理
 * User: sps
 * Date: 2018/11/20
 * Time: 上午11:20
 */
public interface WorkSheetHandleService {

    /**
     * 通过工单编号解析工单新增、处理、详情页面展示信息
     *
     * @param methodParam      系统参数
     * @param subWorkCode      工单编号
     * @param pageType         页面类型
     * @param workTypeId       工单类型ID
     * @param workTemplateCode 工单模板编码
     * @return
     */
    Map<String, Object> getSingleWorkInfo(MethodParam methodParam, Map<String, Object> params, String subWorkCode, String pageType, String workTypeId, String workTemplateCode);

    /**
     * 整合小程序回显字段及数据（设备扫码用）
     *
     * @param methodParam  系统参数
     * @param deviceCode   设备编码
     * @param workTypeId   工单类型id
     * @param needWorkInfo 是否返回工单信息
     * @return
     */
    Map<String, Object> getWxWorkHandleInfoByAssetCode(MethodParam methodParam, String deviceCode, String workTypeId, boolean needWorkInfo);

    /**
     * 获取设备信息（手机端使用）
     *
     * @param methodParam 系统参数
     * @param deviceCode  设备编码
     * @return
     */
    Map<String, Object> getWxAssetCommonDataByCode(MethodParam methodParam, String deviceCode);

    /**
     * 从客户请求发起工单
     * @param methodParam
     * @param params
     * @param subWorkCode
     * @param pageType
     * @param workTypeId
     * @param workTemplateCode
     * @return
     */
    Map<String, Object> getSingleWorkInfoFromCustomerRequest(MethodParam methodParam, Map<String, Object> params, String subWorkCode, String pageType, String workTypeId, String workTemplateCode);
}
