package com.gengyun.senscloud.service;

import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.RoleModel;
import com.gengyun.senscloud.response.*;

import java.util.List;
import java.util.Map;

public interface RoleService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getRoleListPermission(MethodParam methodParam);

    /**
     * 新增角色
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    void newRole(MethodParam methodParam, RoleModel roleModel);


    /**
     * 更新角色信息
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    void modifyRoleById(MethodParam methodParam, RoleModel roleModel);

    /**
     * 更新角色功能权限信息
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    void modifyPermissionRoleById(MethodParam methodParam, RoleModel roleModel);

    /**
     * 更新角色数据权限信息
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    void modifyDataPermissionRoleById(MethodParam methodParam, RoleModel roleModel);


    /**
     * 根据id删除角色
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    void cutRoleById(MethodParam methodParam, RoleModel roleModel);

    /**
     * 获取角色详情
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色详情
     */
    Map<String, Object> getRoleInfo(MethodParam methodParam, RoleModel roleModel);

    /**
     * 分页查询角色列表
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色列表信息
     */
    Map<String, Object> getRoleList(MethodParam methodParam, RoleModel roleModel);

    /**
     * 查询角色功能权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色功能权限
     */
    List<PermissionRoleResult> getRolePermission(MethodParam methodParam, RoleModel roleModel);

    /**
     * 查询角色设备位置数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色设备位置数据权限
     */
    List<AssetPositionRoleResult> getRoleAssetPositionPermission(MethodParam methodParam, RoleModel roleModel);

    /**
     * 查询角色设备类型数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色设备类型数据权限
     */
    List<AssetCategoryRoleResult> getRoleAssetTypePermission(MethodParam methodParam, RoleModel roleModel);

    /**
     * 查询角色创建工单类型数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色工单类型数据权限
     */
    List<WorkTypeRoleResult> getRoleWorkTypePermission(MethodParam methodParam, RoleModel roleModel);

    /**
     * 获取创建工单类型角色列表
     *
     * @param methodParam 系统参数
     * @param roleModel   角色参数
     * @return 创建工单类型角色列表
     */
    List<Map<String, Object>> getNewWorkTypePermission(MethodParam methodParam, RoleModel roleModel);

    /**
     * 查询角色库房数据权限
     *
     * @param methodParam 入参
     * @param roleModel   入参
     * @return 角色库房数据权限
     */
    List<StockRoleResult> getRoleStockPermission(MethodParam methodParam, RoleModel roleModel);

    /**
     * 克隆角色
     *
     * @param methodParam 入参
     * @param roleModel   入参
     */
    void cloneRole(MethodParam methodParam, RoleModel roleModel);

    /**
     * 根据角色id列表获取角色名称列表
     *
     * @param schema_name 入参
     * @param roleIds     入参
     * @return 角色名称列表
     */
    List<String> getRoleNamesByRoleIds(String schema_name, List<String> roleIds);

    /**
     * 根据角色id获取角色信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 角色信息
     */
    Map<String, Object> getByRoleId(String schema_name, String roleId);
}
