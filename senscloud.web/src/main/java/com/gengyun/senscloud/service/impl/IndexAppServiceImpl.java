package com.gengyun.senscloud.service.impl;

import com.gengyun.senscloud.service.IndexAppService;
import org.springframework.stereotype.Service;

@Service
public class IndexAppServiceImpl implements IndexAppService {
//
//    @Autowired
//    IndexMapper indexMapper;
//
//    @Autowired
//    UserMapper userMapper;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    //查询所有待办
//    @Override
//    public List<ToDoDataListResult> toDoDataList(String schema_name, String account, int pageSize, int begin) {
//        String repairCondition = getRepairDistributeCondition(schema_name, account);
//        String maintainCondition = getMaintainAuditCondition(schema_name, account);
//        String repairTitle = selectOptionService.getLanguageInfo(LangConstant.REPAIR_A);
//        String maintainTitle = selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_A);
//        return indexMapper.toDoDataList(schema_name, account, repairCondition, maintainCondition, pageSize, begin, repairTitle, maintainTitle);
//    }
//
//    //总数
//    @Override
//    public int toDoDataListCount(String schema_name, String account) {
//        String repairCondition = getRepairDistributeCondition(schema_name, account);
//        String maintainCondition = getMaintainAuditCondition(schema_name, account);
//        String repairTitle = selectOptionService.getLanguageInfo(LangConstant.REPAIR_A);
//        String maintainTitle = selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_A);
//        return indexMapper.toDoDataListCount(schema_name, account, repairCondition, maintainCondition, repairTitle, maintainTitle);
//    }
//
//
//    @Override
//    public List<ToDoDataListResult> toDoDataListForNumb(String schema_name, String account) {
//        String repairCondition = getRepairDistributeCondition(schema_name, account);
//        String maintainCondition = getMaintainAuditCondition(schema_name, account);
//        String repairTitle = selectOptionService.getLanguageInfo(LangConstant.REPAIR_A);
//        String maintainTitle = selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_A);
//        return indexMapper.toDoDataListForNumb(schema_name, account, repairCondition, maintainCondition, repairTitle, maintainTitle);
//    }
//
//    @Override
//    public List<ToDoDataListResult> toDoDataListForNumblitter(String schema_name, String account) {
//        String repairCondition = getRepairDistributeCondition(schema_name, account);
//        String maintainCondition = getMaintainAuditCondition(schema_name, account);
//        String repairTitle = selectOptionService.getLanguageInfo(LangConstant.REPAIR_A);
//        String maintainTitle = selectOptionService.getLanguageInfo(LangConstant.MAINTENANCE_A);
//        return indexMapper.toDoDataListForNumblitter(schema_name, account, repairCondition, maintainCondition, repairTitle, maintainTitle);
//    }
//
//    /*@Override
//    public List<ToDoDataListResult> toDoDataListInPC(String schema_name, String account, int pageSize, int begin) {
//        String repairCondition = getRepairDistributeCondition(schema_name, account);
//        String maintainCondition = getMaintainAuditCondition(schema_name, account);
//
//        return indexMapper.toDoDataListInPC(schema_name, account, repairCondition, maintainCondition, pageSize, begin);
//    }
//
//    @Override
//    public int toDoDataListCount(String schema_name, String account) {
//        String repairCondition = getRepairDistributeCondition(schema_name, account);
//        String maintainCondition = getMaintainAuditCondition(schema_name, account);
//
//        return indexMapper.toDoDataListCount(schema_name, account, repairCondition, maintainCondition);
//    }
//
//    //按权限，查询所有的待办
//    @Override
//    public List<ToDoDataListResult> toDoDataListAllInPC(String schema_name, User user) {
//        String repairCondition = returnFacility( schema_name, user, user.getAccount(), "repair");
//        String maintainCondition = returnFacility( schema_name, user, user.getAccount(), "maintain");
//
//        return indexMapper.toDoDataListInPC(schema_name, user.getAccount(), repairCondition, maintainCondition, 1000, 0);
//    }
//
//    //按权限，查询所有的待办的总数
//    @Override
//    public int toDoDataListAllCount(String schema_name, User user) {
//        String repairCondition = returnFacility( schema_name, user, user.getAccount(), "repair");
//        String maintainCondition = returnFacility( schema_name, user, user.getAccount(), "maintain");
//
//        return indexMapper.toDoDataListCount(schema_name, user.getAccount(), repairCondition, maintainCondition);
//    }*/
//
//
//    /*
//    * 今日上报
//     */
//    @Override
//    public int toDoDataQuestionToday(String schema_name, String account, User user, Timestamp beginDate, Timestamp endDate) {
//        String repair = "repair";
//        String repairCondition = returnFacility(schema_name, user, account, repair);
//        return indexMapper.toDoDataQuestionToday(schema_name, repairCondition, beginDate, endDate);
//    }
//
//    /*
//    * 今日完成
//     */
//    @Override
//    public int toDoDataFinishedToday(String schema_name, String account, User user, Timestamp beginDate, Timestamp endDate) {
//        String repair = "repair";
//        String maintain = "maintain";
//        String spotcheck = "spotcheck";
//        String inspection = "inspection";
//
//        String repairCondition = returnFacility(schema_name, user, account, repair);
//        String maintainCondition = returnFacility(schema_name, user, account, maintain);
//        String inspectionCondition = returnFacility(schema_name, user, account, inspection);
//        String spotCondition = returnFacility(schema_name, user, account, spotcheck);
//
//        return indexMapper.toDoDataFinishedToday(schema_name, repairCondition, maintainCondition, inspectionCondition, spotCondition, beginDate, endDate);
//    }
//
//    /*
//    * 今日逾期
//     */
//    @Override
//    public int outOfTimeMaintainTotal(String schema_name, String account, User user, Timestamp beginDate, Timestamp endDate) {
////        String maintainByname = "m";
//        String maintain = "maintain";
//        String maintainCondition = returnFacility(schema_name, user, account, maintain);
//        return indexMapper.outOfTimeMaintainTotal(schema_name, maintainCondition, beginDate, endDate);
//    }
//
//
//    //统一注解：需增加，如果此人为维修的分配角色、以及保养的确认角色，则需查看其所在位置的维修待分配与保养待确认数据
//    private String getRepairDistributeCondition(String schema_name, String account) {
//        String repairCondition = " (r.facility_id in (-1) and r.status=20) ";
//        String facilityIds = getUserFacilityByFunctionName(schema_name, account, "repair", "repair_distribute");
//        if (facilityIds != null && !facilityIds.isEmpty()) {
//            repairCondition = " (r.facility_id in (" + facilityIds + ") and r.status=20) ";
//        }
//        return repairCondition;
//    }
//
//    //统一注解：需增加，如果此人为维修的分配角色、以及保养的确认角色，则需查看其所在位置的维修待分配与保养待确认数据
//    private String getMaintainAuditCondition(String schema_name, String account) {
//        String maintainCondition = " (m.facility_id in (-1) and m.status=80) ";
//        String facilityIds = getUserFacilityByFunctionName(schema_name, account, "maintain", "maintain_audit");
//        if (facilityIds != null && !facilityIds.isEmpty()) {
//            maintainCondition = " (m.facility_id in (" + facilityIds + ") and m.status=80) ";
//        }
//        return maintainCondition;
//    }
//
//    //统一注解：需增加，如果此人为维修的分配角色、以及保养的确认角色，则需查看其所在位置的维修待分配与保养待确认数据
//    private String getUserFacilityByFunctionName(String schema_name, String account, String menuKey, String functionName) {
//        String facilityIds = "";
//        //查看当前登录人是否有分配权限，如果有，则找到其负责的位置
//        User user = userMapper.findByAccount(schema_name, account);
//        boolean isHaveDistribute = false;
//        if (user != null) {
//            //menuKey= "repair";
//            List<UserFunctionData> functionDataList = userMapper.getUserFunctionPermission(schema_name, user.getId(), menuKey);
//            if (functionDataList != null && !functionDataList.isEmpty()) {
//                for (UserFunctionData data : functionDataList) {
//                    //functionName= "repair_distribute"；
//                    if (data.getFunctionName().equals(functionName)) {
//                        isHaveDistribute = true;
//                        break;
//                    }
//                }
//            }
//        }
//        if (isHaveDistribute) {
//            //找到给用户所属的位置
//            List<FacilityBase> facilityList = userMapper.getUserFacilites(schema_name, user.getId());
//            if (facilityList != null && !facilityList.isEmpty()) {
//                for (IFacility data : facilityList) {
//                    facilityIds += data.getId() + ",";
//                }
//            }
//            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//            }
//        }
//        return facilityIds;
//    }
//
//    /**
//     * 功能：完成对首页待办任务、今日逾期、今日完成、今日上班等数据查询追加查询权限限制功能
//     */
//    private String returnFacility(String schema_name, User user, String account, String menuKey) {
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        String condition = "";
//
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), menuKey);
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals(menuKey + IDataPermissionForFacility.ALL_FACILITY)) {
//                    isAllFacility = true;
//                    break;
//                } else if (functionData.getFunctionName().equals(menuKey + IDataPermissionForFacility.SELF_FACILITY)) {
//                    isSelfFacility = true;
//                }
//            }
//        }
//        if (isAllFacility) {
//            condition = " 1=1 ";
//        } else if (isSelfFacility) {
//            LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//            String facilityIds = "";
//            if (facilityList != null && !facilityList.isEmpty()) {
//                for (String key : facilityList.keySet()) {
//                    facilityIds += facilityList.get(key).getId() + ",";
//                }
//            }
//            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                condition = " w.facility_id in (" + facilityIds + ") ";
//            } else {
//                String facilityIds_distribute_account = getUserFacilityByFunctionName(schema_name, account, "repair", "repair_distribute");
//                if (facilityIds_distribute_account != null && !facilityIds_distribute_account.isEmpty()) {
//                    facilityIds_distribute_account = " ( w.facility_id in (" + facilityIds_distribute_account + ")) ";
//                    condition = " (w.create_user_account='" + account + "' or wd.receive_account='" + account + "' or " + facilityIds_distribute_account + " )";
//                } else {
//                    condition = " (w.create_user_account='" + account + "' or wd.receive_account='" + account + "' )";
//                }
//            }
//        } else {
//            condition = " w.facility_id in (-1)";
//        }
//        return condition;
//    }
//
//    //查找所有的域名
//    @Override
//    public List<DomainPortUrlData> findAllDomain(String domain) {
//        return indexMapper.findAllDomain(domain);
//    }
//
//
//    /**
//     * 首页按条件获取,工单信息（例今日上报）
//     *
//     * @param request
//     * @param resultType total 条数,table 列表数据
//     * @param viewType   repair_today 今日上报,finish_today 今日完成,be_overdue 逾期任务
//     * @return JSONObject
//     */
//    @Override
//    public JSONObject getWorkSheetInfo(HttpServletRequest request, String resultType, String viewType) {
//        JSONObject result = new JSONObject();
//        String schema_name = null;
//        int pageSize = 0;
//        int pageNumber = 0;
//        try {
//            schema_name = AuthService.getCompany(request).getSchema_name();
//        } catch (Exception snExp) {
//            schema_name = WxUtil.doGetSchemaName();
//        }
//        //若只查询total不需要获取pageSize,pageNumber
//        if (RegexUtil.isNotNull(resultType) && "table".equals(resultType)) {
//            String pageSizeStr = request.getParameter("pageSize");
//            String pageNumberStr = request.getParameter("pageNumber");
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//        }
//        String facilities = request.getParameter("facilities");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String businessNo = request.getParameter("businessNo");
//        User user = AuthService.getLoginUser(request);
//        user = commonUtilService.checkUser(schema_name, user, request);
//        String account = user.getAccount();
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        String condition = "";
//        List<WorkSheet> workDataList = null;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                    isAllFacility = true;
//                    break;
//                } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                    isSelfFacility = true;
//                }
//            }
//        }
//        condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//        String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, null);
//        if (asset_type_word != null && !asset_type_word.isEmpty()) {
//            condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//        }
//        Timestamp beginDate = null;
//        Timestamp end = null;
//        if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//            beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//        }
//        if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//            end = Timestamp.valueOf(endTime + " 23:59:59");
//        }
//        //今日上报
//        if (RegexUtil.isNotNull(viewType) && "repair_today".equals(viewType)) {
//            if (null != beginDate) {
//                condition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (null != end) {
//                condition += " and w.occur_time <'" + end + "'";
//            }
//            if (RegexUtil.isNotNull(businessNo) && !"-1".equals(businessNo)) {
//                condition += " and (t.business_type_id = " + businessNo + " ) ";
//            }
//        }
//        //今日完成
//        if (RegexUtil.isNotNull(viewType) && "finish_today".equals(viewType)) {
//            if (null != beginDate) {
//                condition += " and w2.finished_time >='" + beginDate + "'";
//            }
//            if (null != end) {
//                condition += " and w2.finished_time <'" + end + "'";
//            }
//            condition += "and w.status='" + StatusConstant.COMPLETED + "'";
//        }
//        //逾期任务
//        if (RegexUtil.isNotNull(viewType) && "be_overdue".equals(viewType)) {
//            if (null != beginDate) {
//                condition += " and w.deadline_time >='" + beginDate + "' ";
//            }
//            if (null != end) {
//                condition += " and w.deadline_time <'" + end + "' ";
//            }
//            condition += " and (w.status='" + StatusConstant.MAINTENANCE_OVERDUE + "'  " +
//                    " or ( w.status = '" + StatusConstant.COMPLETED + "' and w2.finished_time > w.deadline_time ) " +
//                    " or ( w.status < " + StatusConstant.COMPLETED + " and w.deadline_time< current_timestamp ) " +
//                    ") ";
//        }
//        if (RegexUtil.isNotNull(resultType) && "table".equals(resultType)) {
//            int begin = pageSize * (pageNumber - 1);
//            workDataList = indexMapper.getWorkSheetList(schema_name, condition, pageSize, begin);
//        }
//        int total = indexMapper.getWorkSheetListCount(schema_name, condition);
//        result.put("rows", workDataList);
//        result.put("total", total);
//        return result;
//    }
}
