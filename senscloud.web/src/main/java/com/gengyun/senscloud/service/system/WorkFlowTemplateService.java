package com.gengyun.senscloud.service.system;

import com.gengyun.senscloud.model.*;

import java.util.List;
import java.util.Map;

public interface WorkFlowTemplateService {

    /**
     * 获取流程模板基本信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkFlowTemplateInfo(MethodParam methodParam, WorkFlowTemplateSearchParam bParam);

    /**
     * 新增/编辑流程模板基础信息
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkFlowTemplate(MethodParam methodParam, Map<String, Object> paramMap, CopyCallbackService copyCallbackService);

    /**
     * 查询流程、节点字段信息列表
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    List<Map<String, Object>> getWorkFlowTemplateColumnListByNodeIdAndNodeType(MethodParam methodParam, WorkFlowTemplateSearchParam bParam);

    /**
     * 新增流程、节点模板字段
     *
     * @param methodParam
     * @param paramMap
     */
    void newWorkFlowTemplateColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑流程字段区块、是否显示、操作权限 信息
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkFlowColumnUse(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑流程字段 上移、下移信息
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkFlowOrder(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑流程配置字段
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkFlowTemplateColumn(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 编辑流程模板操作人信息
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkFlowTemplateHandle(MethodParam methodParam, Map<String, Object> paramMap);

    /**
     * 删除流程模板字段
     *
     * @param methodParam
     * @param param
     */
    void cutWorkFlowColumnById(MethodParam methodParam, WorkFlowTemplateColumnAdd param);

    /**
     * 查询新增字段查询工单模板字段列表
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String, Object>> getWorkFlowColumnListForAdd(MethodParam methodParam, WorkFlowTemplateSearchParam param);

    /**
     * 根据流程、节点信息获取全部详细信息
     *
     * @param methodParam
     * @param pref_id
     * @param node_id
     * @param page_type
     * @return
     */
    Map<String, Object> getWorkFlowNodeTemplateInfo(MethodParam methodParam, String pref_id, String node_id, String page_type);

    /**
     * 根据流程、节点信息获取全部详细信息
     *
     * @param methodParam      请求参数
     * @param workTemplateCode 工单模板编号
     * @return
     */
    Map<String, Object> getSubPageWorkFlowNodeTemplateInfo(MethodParam methodParam, String workTemplateCode);

    /**
     * 根据流程、节点类型获取克隆对应模板
     *
     * @param methodParam
     * @param param
     * @return
     */
    List<Map<String, Object>> getWorkFlowTemplateListForClone(MethodParam methodParam, WorkFlowTemplateSearchParam param);

    /**
     * 克隆
     *
     * @param methodParam
     * @param bParam
     */
    void newWorkFlowNodeClone(MethodParam methodParam, WorkFlowTemplateAdd bParam, CopyCallbackService copyCallbackService);

    /**
     * 获取流程配置字段详细信息
     *
     * @param methodParam
     * @param bParam
     * @return
     */
    Map<String, Object> getWorkFlowColumnInfoById(MethodParam methodParam, WorkTemplateSearchParam bParam);

//    /**
//     * 根据流程表主键id获取节点表start节点的主键id
//     *
//     * @param methodParam   系统参数
//     * @param id            流程表主键id
//     * @return
//     */
//    Map<String, Object> getWorkFlowStartNode(MethodParam methodParam, String id);

    /**
     * 根据工单类型id获取节点表start节点的主键id
     *
     * @param methodParam 系统参数
     * @param workTypeId  工单类型id
     * @return
     */
    Map<String, Object> getWorkFlowStartNodeByWorkTypeId(MethodParam methodParam, String workTypeId);

    /**
     * 通过pref_id获取流程start节点的全部信息
     *
     * @param methodParam
     * @param pref_id
     * @return
     */
    Map<String, Object> getWorkFlowStartNodeTemplateInfo(MethodParam methodParam, String pref_id);

    /**
     * 通过work_type_id查询流程节点信息
     *
     * @param methodParam
     * @param work_type_id
     * @return
     */
    Map<String, Object> getWorkFlowNodeTemplateInfoByWorkTypeId(MethodParam methodParam, Integer work_type_id);

    /**
     * 获取流程信息列表
     * @param methodParam
     * @return
     */
    List<Map<String, Object>> getWorkFlowInfoList(MethodParam methodParam);

    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getWorkFlowTemplateListPermission(MethodParam methodParam);

    /**
     * 通过pref_id获取流程end节点的全部信息
     *
     * @param methodParam
     * @param pref_id
     * @return
     */
    Map<String, Object> getWorkFlowEndNodeTemplateInfo(MethodParam methodParam, String pref_id);

    /**
     * 通过pref_id获取流程全部信息
     *
     * @param schemaName
     * @param pref_id
     * @return
     */
    Map<String, Object> getFlowInfo(String schemaName, String pref_id);

    /**
     * 单独获取字段表数据
     *
     * @param schemaName
     * @param id
     * @return
     */
    Map<String, Object> getWorkFlowColumnDetailById(String schemaName, Integer id);

    /**
     * 通过pref_id获取流程end节点的信息
     *
     * @param methodParam
     * @param pref_id
     * @return
     */
    Map<String, Object> getWorkFlowInfoByPrefId(MethodParam methodParam, String pref_id);

    /**
     * 通过工单模板编码查询所有的字段列表
     * @param methodParam
     * @param workTemplateCode
     * @return
     */
    List<Map<String, Object>> getColumnListByWorkTemplateCode(MethodParam methodParam, String workTemplateCode);

    /**
     * 批量删除流程模板字段
     *
     * @param methodParam
     * @param param
     */
    void cutWorkFlowColumnByIds(MethodParam methodParam, WorkFlowTemplateColumnAdd param);

    /**
     * 一键同步流程、节点
     *
     * @param methodParam
     * @param paramMap
     */
    void modifyWorkFlowNodeSync(MethodParam methodParam, Map<String, Object> paramMap);

    List<Map<String, Object>> findWorkTemplateColumnList(String schemaName, String searchWord, String work_template_code, Long company_id, String userLang, String keywordSearch);
}
