package com.gengyun.senscloud.service.asset.impl;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SystemConfigConstant;
import com.gengyun.senscloud.mapper.AssetPositionMapper;
import com.gengyun.senscloud.model.AssetPositionModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.asset.AssetPositionService;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.QRCodeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 设备位置
 */
@Service
public class AssetPositionServiceImpl implements AssetPositionService {
    @Resource
    AssetPositionMapper assetPositionMapper;
    @Resource
    LogsService logService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    CommonUtilService commonUtilService;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    SelectOptionService selectOptionService;

    /**
     * 获取设备位置模块权限
     *
     * @param methodParam 系统参数
     * @return 设备位置模块权限
     */
    @Override
    public Map<String, Map<String, Boolean>> getAssetPositionListPermission(MethodParam methodParam) {
        return pagePermissionService.getModelPrmByKey(methodParam, SensConstant.MENUS[9]);
    }

    /**
     * 添加设备位置
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public void newAssetPosition(MethodParam methodParam, Map<String, Object> paramMap) {
        RegexUtil.optStrOrExpNotNull(paramMap.get("position_name"), LangConstant.TITLE_NAME_AB_L);//位置名称不能为空
        RegexUtil.optIntegerOrExpParam(paramMap.get("position_type_id"), LangConstant.TITLE_CATEGORY_AZ);//位置类型不能为空
        //生成设备位置编码
        String position_code = assetPositionMapper.findMaxPositionCode(methodParam.getSchemaName());
        if (RegexUtil.optNotNull(position_code).isPresent()) {
            int code = Integer.valueOf(position_code.replace("P", "")) + 1;
            position_code = "P00" + code;
        } else {
            position_code = "P001";
        }
        paramMap.put("create_user_id", methodParam.getUserId());
        if (!RegexUtil.optNotNull(paramMap.get("location")).isPresent()) {
            paramMap.put("location", "(0,0)");
        }
        paramMap.put("position_code", position_code);
        assetPositionMapper.insertAssetPosition(methodParam.getSchemaName(), paramMap);
        logService.newLog(methodParam, SensConstant.BUSINESS_NO_7005, position_code, LangUtil.doSetLogArray("设备位置新建", LangConstant.TAB_DEVICE_B, new String[]{LangConstant.TITLE_ASSET_G}));//设备位置新建
    }

    /**
     * 编辑设备位置
     *
     * @param methodParam 入参
     * @param paramMap    入参
     */
    @Override
    public void modifyAssetPosition(MethodParam methodParam, Map<String, Object> paramMap) {
        String position_code = RegexUtil.optStrOrExpNotNull(paramMap.get("position_code"), LangConstant.TITLE_AAAA_L);//位置编码不能为空
        Map<String, Object> oldMap = assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), position_code);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            String scada_config = RegexUtil.optStrOrNull(paramMap.get("scada_config"));
            paramMap.put("scada_config", scada_config);
            assetPositionMapper.updateAssetPosition(methodParam.getSchemaName(), paramMap);
            JSONArray loger = LangUtil.compareMap(paramMap, oldMap);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7005, position_code, LangUtil.doSetLogArray("编辑了设备位置", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_EDIT_A, LangConstant.TITLE_ASSET_G, loger.toString()}));//编辑了设备位置
        }
    }

    /**
     * 删除设备位置
     *
     * @param methodParam        入参
     * @param assetPositionModel 入参
     */
    @Override
    public void cutAssetPosition(MethodParam methodParam, AssetPositionModel assetPositionModel) {
        String position_code = RegexUtil.optStrOrExpNotNull(assetPositionModel.getPosition_code(), LangConstant.TITLE_AAAA_L);//位置编码不能为空
        List<Map<String, Object>> childrens = assetPositionMapper.findChildAssetPositionByPositionCode(methodParam.getSchemaName(), position_code);
        if (!RegexUtil.optNotNull(childrens).isPresent() || childrens.size() > 0) {
            throw new SenscloudException(LangConstant.MSG_CE, new String[]{LangConstant.TITLE_ASSET_G, LangConstant.TITLE_ASSET_G, LangConstant.TITLE_ASSET_G});//设备位置下还有设备位置，不能删除设备位置
        }
        Map<String, Object> oldMap = assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), position_code);
        if (RegexUtil.optNotNull(oldMap).isPresent()) {
            Map<String, Object> pm = new HashMap<>();
            pm.put("position_code", position_code);
            assetPositionMapper.deletePositionTemplate(methodParam.getSchemaName(), pm);
            assetPositionMapper.deleteAssetPosition(methodParam.getSchemaName(), position_code);
            logService.newLog(methodParam, SensConstant.BUSINESS_NO_7005, position_code, LangUtil.doSetLogArray("删除了设备位置", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_DELETE_A, LangConstant.TITLE_ASSET_G, oldMap.get("position_name").toString()}));//删除了设备位置
        }
    }

    /**
     * 获取设备位置详情
     *
     * @param methodParam        入参
     * @param assetPositionModel 入参
     */
    @Override
    public Map<String, Object> getAssetPositionInfo(MethodParam methodParam, AssetPositionModel assetPositionModel) {
        String position_code = RegexUtil.optStrOrExpNotNull(assetPositionModel.getPosition_code(), LangConstant.TITLE_AAAA_L);//位置编码不能为空
        Map<String, Object> assetPosition = assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), position_code);
        //查询他的父类信息
        if (RegexUtil.optNotNull(assetPosition.get("parent_id")).isPresent()) {
            Map<String, Object> parentPosition = assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), assetPosition.get("parent_id").toString());
            assetPosition.put("parentPosition", parentPosition);
        }
        return assetPosition;
    }

    /**
     * 查询设备位置
     *
     * @param methodParam 入参
     * @param paramMap    入参
     * @return 设备位置列表
     */
    @Override
    public List<Map<String, Object>> getAssetPositionList(MethodParam methodParam, Map<String, Object> paramMap) {
        if (RegexUtil.optNotNull(paramMap.get("positionTypeIdSearch")).isPresent()) {
            String[] positionTypeIdSearch = paramMap.get("positionTypeIdSearch").toString().split(",");
            paramMap.put("positionTypeIdSearch", positionTypeIdSearch);
        }
        return assetPositionMapper.findAssetPositionList(methodParam.getSchemaName(), paramMap);
    }

    /**
     * 根据设备位置获取上级的平面图
     *
     * @param methodParam        入参
     * @param assetPositionModel 入参
     */
    @Override
    public Integer getSuperiorPositionPlan(MethodParam methodParam, AssetPositionModel assetPositionModel) {
        Integer fileId = getPositionPlan(methodParam, assetPositionModel.getPosition_code());
        return null == fileId ? RegexUtil.optIntegerOrNull(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.TOP_PIC_ID), methodParam, ErrorConstant.EC_ASSET_POSITION_001) : fileId;
    }

    /**
     * 递归获取上级平面图
     *
     * @param methodParam  入参
     * @param positionCode 入参
     * @return 获取上级平面图
     */
    private Integer getPositionPlan(MethodParam methodParam, String positionCode) {
        return RegexUtil.optNotBlankStrOpt(positionCode).map(s ->
                RegexUtil.optNotNullMap(assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), s)).map(p -> {
                    Integer fi = RegexUtil.optIntegerOrNull(p.get("file_id"), LangConstant.TITLE_AAM_Q);
                    return null == fi ? RegexUtil.optNotBlankStrOpt(p.get("parent_id")).map(ps -> getPositionPlan(methodParam, ps)).orElse(null) : fi;
                }).orElse(null)
        ).orElse(null);
    }

    /**
     * 根据设备位置递归获取子集和父级所有设备位置名称
     *
     * @param methodParam   入参
     * @param position_code 入参
     * @return 设备位置名称
     */
    @Override
    public StringBuilder getParentByPositionCode(MethodParam methodParam, String position_code) {
        StringBuilder name = new StringBuilder();
        Map<String, Object> assetPosition = assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), position_code);
        name.append(assetPosition.get("position_name"));
        if (RegexUtil.optNotNull(assetPosition.get("parent_code")).isPresent()) {
            name.append("/");
            getParentByPositionCode(methodParam, assetPosition.get("parent_code").toString());
        }
        return name;
    }

    /**
     * 根据设备位置编码获取设备位置详情
     *
     * @param methodParam   入参
     * @param position_code 入参
     * @return 设备位置详情
     */
    @Override
    public Map<String, Object> getPositionInfoByCode(MethodParam methodParam, String position_code) {
        return assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), position_code);
    }

    /**
     * 验证设备位置是否存在
     *
     * @param schemaName       入参
     * @param positionCodeList 入参
     * @return 验证设备位置是否存在
     */
    @Override
    public Map<String, Map<String, Object>> queryPositionByPositionCodes(String schemaName, List<String> positionCodeList) {
        return assetPositionMapper.queryPositionByPositionCodes(schemaName, positionCodeList);
    }

    /**
     * 根据主键展示设备位置二维码（不做数据权限验证）
     *
     * @param methodParam 系统参数
     */
    @Override
    public void doShowAssetPositionQRCode(MethodParam methodParam) {
        String id = methodParam.getDataId();
        Map<String, Object> asset_position = assetPositionMapper.findAssetPositionInfo(methodParam.getSchemaName(), id);
        RegexUtil.optNotNullOrExp(asset_position, LangConstant.TEXT_K); // 信息不存在！
        QRCodeUtil.doShowQRCode(RegexUtil.optStrOrPrmError(asset_position.get("id"), methodParam, ErrorConstant.EC_ASSET_3, id));
    }

    /**
     * 根据选中主键下载设备二维码
     *
     * @param methodParam        系统参数
     * @param assetPositionModel 请求参数
     */
    @Override
    public void doExportAssetPositionQRCode(MethodParam methodParam, AssetPositionModel assetPositionModel, Map<String, Object> pm) {
        String[] asset_positions = null;
        if (RegexUtil.optIsPresentStr(pm.get("ids"))) {
            asset_positions = pm.get("ids").toString().split(",");
        }
        List<Map<String, Object>> asset_position_list = assetPositionMapper.findAssetPositionListByPositions(methodParam.getSchemaName(), asset_positions, pm);
        Map<String, Object> map = commonUtilService.qrCodeExport(methodParam, 2);
        map.put("companyId", methodParam.getCompanyId());
        map.put("companyName", methodParam.getCompanyName());
        map.put("dataList", RegexUtil.optNotNullOrExp(asset_position_list, LangConstant.TEXT_K)); // 信息不存在
        map.put("qrCodePrefix", RegexUtil.optStrOrBlank(systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.QR_CODE_PREFIX)));
        String ctVal = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), SystemConfigConstant.CODE_TEMPLATE);
        if (RegexUtil.optEqualsOpt(ctVal, "2").isPresent()) {
            QRCodeUtil.doDownloadBRCodePdf(map);
        } else {
            QRCodeUtil.doDownloadQRCodePdf(map);
        }
    }

    /**
     * 获取该设备位置可选择的任务模板列表
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 任务模板列表
     */
    @Override
    public List<Map<String, Object>> getChooseTaskTemplateListByPositionCode(MethodParam methodParam, Map<String, Object> pm) {
        return assetPositionMapper.findChooseTaskTemplateListByPositionCode(methodParam.getSchemaName(), pm);
    }

    /**
     * 获取该设备位置可选择的任务模板列表
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 任务模板列表
     */
    @Override
    public List<Map<String, Object>> getTaskTemplateListByPositionCode(MethodParam methodParam, Map<String, Object> pm) {
        return assetPositionMapper.findTaskTemplateListByPositionCode(methodParam.getSchemaName(), pm);
    }

    /**
     * 新增设备位置和任务模板关联
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     */
    @Override
    public void newPositionCodeTemplate(MethodParam methodParam, Map<String, Object> pm) {
        RegexUtil.optStrOrExpNotNull(pm.get("position_code"), LangConstant.TITLE_ASSET_G);
        RegexUtil.optStrOrExpNotNull(pm.get("work_type_id"), LangConstant.TITLE_CATEGORY_X);
        pm.put("template_codes", RegexUtil.optStrToArray(pm.get("template_codes"), LangConstant.TITLE_NAME_AB_C));
        String schemaName = methodParam.getSchemaName();
        String str = assetPositionMapper.findTaskTemplateNamesForAddTask(schemaName, pm);
        RegexUtil.trueExp(RegexUtil.optIsPresentStr(str), LangConstant.TEXT_G, new String[]{LangConstant.TITLE_NAME_AB_C, str}); // 模板名称已存在：模板名称
        pm.put("create_user_id", methodParam.getUserId());
        assetPositionMapper.insertPositionTemplate(schemaName, pm);
    }

    /**
     * 删除设备位置和任务模板关联
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     */
    @Override
    public void cutPositionCodeTemplate(MethodParam methodParam, Map<String, Object> pm) {
        RegexUtil.optStrOrExpNotNull(pm.get("position_code"), LangConstant.TITLE_ASSET_G);
        RegexUtil.optStrOrExpNotNull(pm.get("work_type_id"), LangConstant.TITLE_CATEGORY_X);
        pm.put("template_codes", RegexUtil.optStrToArray(pm.get("template_codes"), LangConstant.TITLE_NAME_AB_C));
        assetPositionMapper.deletePositionTemplate(methodParam.getSchemaName(), pm);
    }

    /**
     * 根据位置编码和工单类型获取任务模板列表
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 任务模板列表
     */
    @Override
    public List<Map<String, Object>> getTaskTemplateByPositionCodeAndWorkTypeId(MethodParam methodParam, Map<String, Object> pm) {
        RegexUtil.optStrOrExpNotNull(pm.get("position_code"), LangConstant.TITLE_AAAA_L);//位置编码不能为空
        RegexUtil.optIntegerOrExpParam(pm.get("work_type_id"), LangConstant.TITLE_CATEGORY_X);//工单类型不能为空
        return assetPositionMapper.findTaskTemplateListByPositionCodeAndWorkTypeId(methodParam.getSchemaName(), pm);
    }

    /**
     * 根据位置编码获取该位置内的设备的故障状态
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 设备的故障状态
     */
    @Override
    public List<Map<String, Object>> getAssetRepairStatusByPositionCode(MethodParam methodParam, Map<String, Object> pm) {
        return assetPositionMapper.findAssetRepairStatusByPositionCode(methodParam.getSchemaName(), pm.get("position_code").toString());
    }

    /**
     * 获取所有位置的设备故障数
     *
     * @param methodParam 系统参数
     * @param pm          系统参数
     * @return 设备的故障状态
     */
    @Override
    public List<Map<String, Object>> getAllPositionCodeAssetRepairStatusBy(MethodParam methodParam, Map<String, Object> pm) {
        return assetPositionMapper.findAllPositionCodeAssetRepairStatusBy(methodParam.getSchemaName());
    }
}

