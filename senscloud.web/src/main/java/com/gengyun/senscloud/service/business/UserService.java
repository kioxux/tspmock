package com.gengyun.senscloud.service.business;

import com.gengyun.senscloud.entity.UserEntity;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.UserModel;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * 用户处理
 */
public interface UserService {
    /**
     * 获取菜单信息
     *
     * @param methodParam 系统参数
     * @return 权限信息
     */
    Map<String, Map<String, Boolean>> getUserListPermission(MethodParam methodParam);

    /**
     * 新增用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void newUser(MethodParam methodParam, UserModel userModel);

    /**
     * 编辑用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void modifyUser(MethodParam methodParam, UserModel userModel);

    /**
     * 启用禁用用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void changeIsUse(MethodParam methodParam, UserModel userModel);

    /**
     * 删除用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void cutUser(MethodParam methodParam, UserModel userModel);

    /**
     * 删除选中用户
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void cutSelectUser(MethodParam methodParam, UserModel userModel);

    /**
     * 获取用户详情
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    Map<String, Object> getUserInfo(MethodParam methodParam, UserModel userModel);

    /**
     * 分页获取用户信息列表
     *
     * @param methodParam 入参
     * @param userModel   入参
     * @return 分页用户信息列表
     */
    Map<String, Object> getUserListForPage(MethodParam methodParam, UserModel userModel);

    /**
     * 重置用户密码
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void changeUserPassword(MethodParam methodParam, UserModel userModel);

    /**
     * 通过用户名登录获取账号
     *
     * @param schemaName 数据库
     * @param account    用户名
     * @param password   密码
     * @return account
     */
    String getUserAccountForLogin(String schemaName, String account, String password);

    /**
     * 通过员工工号登录获取账号
     *
     * @param schemaName 数据库
     * @param userCode   员工工号
     * @return UserEntity
     */
    UserEntity getUserAccountForUserCode(String schemaName, String userCode);

    /**
     * 通过员工NFC卡号登录获取账号
     *
     * @param schemaName 数据库
     * @param nfcCode    员工NFC卡号
     * @return UserEntity
     */
    UserEntity getUserAccountForNfcCode(String schemaName, String nfcCode);

    /**
     * 获取登录用户信息
     *
     * @param schemaName 数据库
     * @param account    账号
     * @return 用户信息
     */
    UserEntity getUserForSystemLoginCache(String schemaName, String account);

    /**
     * 通过手机号获取用户信息
     *
     * @param schemaName 数据库
     * @param phone      手机号
     * @return 用户信息
     */
    UserEntity getUserByPhone(String schemaName, String phone);

    /**
     * 修改密码
     *
     * @param methodParam 系统参数
     * @param id          用户主键
     * @param psd         密码
     */
    void modifyPassword(MethodParam methodParam, String id, String psd);

    /**
     * 根据id查询用户信息
     *
     * @param methodParam 入参
     * @param user_id     入参
     * @return 用户记录
     */
    Map<String, Object> getUserInfoById(MethodParam methodParam, String user_id);

    /**
     * 根据id查询用户和联系人信息
     *
     * @param methodParam 入参
     * @param user_id     入参
     * @return 用户和联系人记录
     */
    Map<String, Object> findUserInfoById(MethodParam methodParam, String user_id);

    String findAllCCUser(MethodParam methodParam, String ids);

    /**
     * 根据userid列表获取用户信息列表
     *
     * @param methodParam 入参
     * @param userIds     入参
     * @return 用户信息列表
     */
    List<Map<String, Object>> getUserListByIds(MethodParam methodParam, List<String> userIds);

    /**
     * 根据用户主键获取岗位和部门列表
     *
     * @param methodParam 系统参数
     * @param userId      用户主键
     * @return 用户岗位和部门列表
     */
    List<Map<String, Object>> getUserGpList(MethodParam methodParam, String userId);

    /**
     * 根据userId和密码判断是否存在
     *
     * @param methodParam 入参
     * @param userId      入参
     * @param oldPsd      入参
     * @return 是否存在
     */
    int getUserInfoByUserIdAndPwd(MethodParam methodParam, String userId, String oldPsd);

    /**
     * 新增用户微信主键
     *
     * @param schemaName
     * @param userId
     * @param wxOpenId
     * @param unionId
     * @param oaOpenId
     * @return
     */
    int insertUserWxOpenId(String schemaName, String userId, String wxOpenId, String unionId, String oaOpenId, String nickName);

    /**
     * 更新用户微信主键
     *
     * @param schemaName
     * @param userId
     * @param wxOpenId
     * @return
     */
    int updateUserWxOpenId(String schemaName, String userId, String wxOpenId, String unionId, String oaOpenId, String nickName);

    /**
     * 查询微信公众号用户信息
     *
     * @param unionId 微信主键
     * @param wxOaId  公众号编码
     * @return 微信公众号用户信息
     */
    Map<String, Object> queryWxoaUserByUnionId(String unionId, String wxOaId);

    /**
     * 查看用户公众号关注状态
     *
     * @param schemaName
     * @param account
     */
    boolean getFollowOAStatus(String schemaName, String account);

    /**
     * 通过登录账号，查询绑定的微信用户信息
     *
     * @param schemaName 数据库
     * @param id         用户主键
     * @return 微信信息
     */
    Map<String, Object> queryUserWxLoginInfoById(String schemaName, String id);

    /**
     * 查看当前公司还能新增或者启用用户
     *
     * @param methodParam 入参
     * @return 是否
     */
    boolean checkCompanyIsAddUser(MethodParam methodParam);

    /**
     * 更新员工上下班状态
     *
     * @param methodParam 入参
     * @param userModel   入参
     */
    void changeUserIsOnDuty(MethodParam methodParam, UserModel userModel);

    /**
     * 导出选中用户
     *
     * @param methodParam
     * @param model
     */
    ModelAndView doExportUser(MethodParam methodParam, UserModel model);

    /**
     * 导出全部用户
     *
     * @param methodParam
     * @param model
     */
    ModelAndView doExportAllUser(MethodParam methodParam, UserModel model);

    /**
     * 查询全部账号
     *
     * @param schemaName
     * @return
     */
    Map<String, Map<String, Object>> queryAccountListMap(String schemaName);

    /**
     * 查询全部手机号
     *
     * @param schemaName
     * @return
     */
    Map<String, Map<String, Object>> queryMobileListMap(String schemaName);

    Map<String, Map<String, Object>> getUserByMobile(String schemaName, List<String> strings);

    Map<String, Map<String, Object>> getUserByAccount(String schemaName, List<String> strings);

    /**
     * 验证用户名称是否存在
     *
     * @param schemaName
     * @param userNameList
     * @return
     */
    Map<String, Map<String, Object>> queryUsersByName(String schemaName, List<String> userNameList);

    Map<String, Map<String, Object>> getAccountMobileListMap(String schemaName);
}
