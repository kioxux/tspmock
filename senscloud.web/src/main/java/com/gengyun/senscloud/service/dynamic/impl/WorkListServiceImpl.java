package com.gengyun.senscloud.service.dynamic.impl;

import com.gengyun.senscloud.service.dynamic.WorkListService;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2018/11/2.
 */
@Service
public class WorkListServiceImpl implements WorkListService {
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkListMapper workListMapper;
//
//    @Override
//    public int addWorkList(String schema_name, WorkListModel workListModel) {
//        return workListMapper.addWorkList(schema_name, workListModel);
//    }
//
//    @Override
//    public int addWorkDetailList(String schema_name, WorkListDetailModel workListDetailModel) {
//        return workListMapper.addWorkDetailList(schema_name, workListDetailModel);
//    }
//
//    /*查询待分配工单信息*/
//    @Override
//    public JSONObject selectWorkListsDetailUndistributed(String schema_name) {
//        HttpServletRequest request = HttpRequestUtils.getRequest();
//        request.setAttribute("pageSize", "4");
//        request.setAttribute("pageNumber", Constants.PAGE_NUMBER);
//        request.setAttribute("status", Constants.WORK_STATUS_UNDISTRIBUTE);
//        request.setAttribute("keyWord", request.getParameter("content"));
//        request.setAttribute("type", request.getParameter("orderType"));
//        request.setAttribute("workPool", request.getParameter("workPool"));
//        request.setAttribute("descStatus", request.getParameter("descStatus"));
//        String searchResult = workSheetService.findWorkSheetList(request);
//        net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(searchResult);
//        JSONObject result = new JSONObject();
//        JSONArray workDataList = (JSONArray) json.get("rows");
//        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
//        if (null != workDataList && workDataList.size() > 0) {
//            Map<String, Object> map = null;
//            for (int i = 0; i < workDataList.size(); i++) {
//                net.sf.json.JSONObject ws = net.sf.json.JSONObject.fromObject(workDataList.get(i));
//                String workCode = (String) ws.get("work_code");
//                List<WorkListDetailModel> workListDetailModelList = workListMapper.selectWorkListsDetailUndistributed(schema_name, workCode);
//                if (null != workListDetailModelList && workListDetailModelList.size() > 0) {
//                    map = new HashMap<String, Object>();
//                    map.put("workListModel", ws);
//                    map.put("workListDetailModel", workListDetailModelList);
//                    list.add(map);
//                }
//            }
//        }
//        result.put("content", list);
//        result.put("total", json.get("total"));
//        result.put("code", StatusConstant.SUCCES_RETURN);
//        return result;
//    }
//
//    @Override
//    public int selectWorkListsDetailUndistributedNum(String schema_name, String condition) {//, String descStatus
//        return workListMapper.selectWorkListsDetailUndistributedNum(schema_name, condition);//,descStatus
//    }
//
//    /*查询待分配工单信息*/
//    @Override
//    public List<WorkListDetailModel> selectWorkListsDetailByWorkCode(String schema_name, String sub_work_code) {//, String descStatus
//        return workListMapper.selectWorkListsDetailByWorkCode(schema_name, sub_work_code);//,descStatus
//    }
//
//    /*根据工单号查询工单模板详情*/
//    @Override
//    public List<WorkListModel> selectWorkListsByWorkCode(String schema_name, String sub_work_code) {//, String descStatus
//        List<WorkListModel> workListModelsResult = new ArrayList<WorkListModel>();
//        List<WorkListModel> workListModels = workListMapper.selectWorkListsByWorkCode(schema_name, sub_work_code);//,descStatus
//        if (workListModels != null && workListModels.size() > 0) {
//            for (WorkListModel workListModel : workListModels) {
//                //根据work_code查询工单详情获取sub_work_code
//                String str_sub_work_code = "";
//                List<WorkListDetailModel> workListDetailModelList = workListMapper.selectDetailByWorkCode(schema_name, workListModel.getWork_code());
//                for (WorkListDetailModel workListDetailModel : workListDetailModelList) {
//                    String subWorkDetail = workListDetailModel.getSub_work_code() + ",";
//                    str_sub_work_code += subWorkDetail;
//                }
//                str_sub_work_code = str_sub_work_code.substring(0, str_sub_work_code.length() - 1);
//                workListModel.setStr_sub_work_code(str_sub_work_code);
//                workListModelsResult.add(workListModel);
//            }
//        }
//
//        return workListModelsResult;//,descStatus
//    }
//
//    /*更新工单详情状态*/
//    @Override
//    public int updateWorkListStatus(String schema_name, Integer status, String updateWorkListStatus) {
//        return workListMapper.updateWorkListStatus(schema_name, status, updateWorkListStatus);
//    }
//
//    @Override
//    public int updateBackWorkListStatus(String schema_name, Integer status, String updateWorkListStatus) {
//        return workListMapper.updateBackWorkListStatus(schema_name, status, updateWorkListStatus);
//    }
//
//    /*更新工单概要状态*/
//    @Override
//    public int updateWorksStatus(String schema_name, Integer status, String updateWorkListStatus) {
//        return workListMapper.updateWorksStatus(schema_name, status, updateWorkListStatus);
//    }
//
//    /*更新工单详情状态并修改同步时间*/
//    @Override
//    public int updateWorkListStatusAndTime(String schema_name, Integer status, String updateWorkListStatus, WorkListDetailModel workListDetailModel) {
//        return workListMapper.updateWorkListStatusAndTime(schema_name, status, updateWorkListStatus, workListDetailModel);
//    }
//
//    /*更新工单状态并修改同步时间*/
//    @Override
//    public int updateWorkStatusAndTime(String schema_name, Integer status, String work_code) {
//        return workListMapper.updateWorkStatusAndTime(schema_name, status, work_code);
//    }
//
//
//    /*工单池任务分配*/
//    @Override
//    public int linkRepairManToWorkOrder(String schema_name, String receive_account, String subWorkCode) {
//        return workListMapper.linkRepairManToWorkOrder(schema_name, receive_account, subWorkCode);
//    }
//
//    /*工单池任务分配*/
//    @Override
//    public int selectWorkListsDetailStatus(String schema_name, String subWorkCode) {
//        return workListMapper.selectWorkListsDetailStatus(schema_name, subWorkCode);
//    }
//
//    /*作废工单*/
//    @Override
//    public Boolean cancelUndistributedWork(String schema_name, String subWorkCode, String workCode, int status) {
//        int doUpdate = workListMapper.updateWorksStatus(schema_name, status, workCode);
//        Boolean cancelStatus = false;
//        if (doUpdate > 0) {
//            int doUpdateDetail = workListMapper.updateWorkListStatus(schema_name, status, workCode);
//            if (doUpdateDetail > 0) {
//                cancelStatus = true;
//            }
//        }
//        return cancelStatus;
//    }
//
//    /*删除工单*/
//    @Override
//    public Boolean delUndistributedWork(String schema_name, String subWorkCode, String workCode) {
//        int doDeleteList = workListMapper.delWorkListStatus(schema_name, subWorkCode);//删除工单详情
//        Boolean cancelStatus = false;
//        if (doDeleteList > 0) {
//            int doDelete = workListMapper.delWorkStatus(schema_name, workCode);//删除工单概要表
//            if (doDelete > 0) {
//                cancelStatus = true;
//            }
//        }
//        return cancelStatus;
//    }
//
//    @Override
//    public int updateWorkPool(String schema_name, String work_code, Integer pool_id) {
//        return workListMapper.updateWorkPool(schema_name, work_code, pool_id);
//    }
}
