package com.gengyun.senscloud.rabbitmq.listener;


import org.springframework.stereotype.Component;

/**
 * QuickRepairServiceListener class 监听一键报修
 *
 * @author Zys
 * @date 2020/03/19
 */
@Component
public class QuickRepairServiceListener {
//    private static final Logger logger = LoggerFactory.getLogger(QuickRepairServiceListener.class);
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    QuickRepairService quickRepairService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    /**
//     * 监听方法
//     *
//     * @param message
//     * @return
//     */
//    @RabbitListener(queues = "${repair.system.queue.name}",containerFactory = "singleListenerContainer")
//    public void reportRepairWork(Message message) {
//        Map<String, Object> dataqMap;
//        String schema_name;
//        Map<String, Object> infoMap=new HashMap<>();
//        try {
//            long tag = message.getMessageProperties().getDeliveryTag();
//            byte[] jsonMessage = message.getBody();
//            String dataStr = new String(jsonMessage, "UTF-8");
//            JSONObject dataJson = JSONObject.parseObject(dataStr);
//            dataqMap = (Map<String, Object>) dataJson;
//        } catch (Exception e) {
//            return;
//        }
//        try {
//
//            if(dataqMap.containsKey("schema_name")&&null==dataqMap.get("schema_name")){
//                return;
//            }
//            schema_name=dataqMap.get("schema_name").toString();
//            if(dataqMap.containsKey("_id")&&null!=dataqMap.get("_id")){
//                infoMap= assetMapper.getAssetFacilitiesById(schema_name,(String)dataqMap.get("_id"));
//                infoMap.put("schema_name",schema_name);
//
//            }
//            if(dataqMap.containsKey("title")&&null!=dataqMap.get("title")){
//                infoMap.put("title",(String)dataqMap.get("title"));
//            }
//            if(dataqMap.containsKey("account")&&null!=dataqMap.get("account")){
//                infoMap.put("account",(String)dataqMap.get("account"));
//            }
//            quickRepairService.reportRepairWork(infoMap);
//        }catch (Exception e){
//            logger.info(selectOptionService.getLanguageInfo(LangConstant.LOG_INS_REP_UNTREATED));
//        }
//    }
}
