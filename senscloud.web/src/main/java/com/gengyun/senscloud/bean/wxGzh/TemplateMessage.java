package com.gengyun.senscloud.bean.wxGzh;

import net.sf.json.JSONObject;

/**
 * 微信公众号-模板消息处理类
 */
public class TemplateMessage {
    private String touser;
    private String template_id;
    private String url = "";
    private String topcolor = "#173177";
    private Object data;

    public TemplateMessage(String touser, String template_id, Object data) {
        this.touser = touser;
        this.template_id = template_id;
        this.data = data;
    }

    public TemplateMessage(String touser, String template_id, String url, String topcolor, Object data) {
        this.touser = touser;
        this.template_id = template_id;
        this.url = url;
        this.data = data;
        if (topcolor != null) {
            this.topcolor = topcolor;
        }
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return JSONObject.fromObject(this).toString();
    }
}
