package com.gengyun.senscloud.job.impl;

import com.gengyun.senscloud.job.BaseJobBean;
import com.gengyun.senscloud.service.system.WxGzhMsgService;
import com.gengyun.senscloud.util.SenscloudException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 微信appToken刷新
 */
public class WxAppTokenJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(WxAppTokenJobBean.class);
    @Resource
    private WxGzhMsgService wxGzhMsgService;

    /**
     * 抽象方法-业务数据处理
     *
     * @param schemaName 数据库
     */
    @Override
    public void handleData(String schemaName) {
        logger.info("wxAppTokenCheckJob-start");
        try {
            wxGzhMsgService.checkWxAccessToken(schemaName);
        } catch (SenscloudException e) {
            logger.warn("wxAppTokenCheckJob-warn", e.getMessage());
        } catch (Exception e) {
            logger.error("wxAppTokenCheckJob-error", e.getMessage());
        }
        logger.info("wxAppTokenCheckJob-end");
    }

}
