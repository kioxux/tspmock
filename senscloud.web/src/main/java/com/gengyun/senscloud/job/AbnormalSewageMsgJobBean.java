package com.gengyun.senscloud.job;

import com.gengyun.senscloud.service.pollute.MeterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class AbnormalSewageMsgJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(AbnormalSewageMsgJobBean.class);

    @Resource
    MeterService meterService;

    @Override
    public void handleData(String schemaName) {
        logger.debug("AbnormalSewageMsgJobBean *********************** start");
        try {
            meterService.asyncCronJobToGenerateAbnormalSewageMsg(schemaName);
        } catch (Exception e) {
            logger.error("AbnormalSewageMsgJobBean.handleData fail ~ ", e);
        }
        logger.debug("AbnormalSewageMsgJobBean *********************** end");
    }
}
