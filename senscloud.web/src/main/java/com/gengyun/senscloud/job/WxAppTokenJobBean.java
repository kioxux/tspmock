package com.gengyun.senscloud.job;

import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WxGzhMsgService;
import com.gengyun.senscloud.service.system.WxMsgService;
import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;

/**
 * 微信appToken更新定时任务
 * User: sps
 * Date: 2020/05/29
 * Time: 上午11:20
 */
public class WxAppTokenJobBean extends QuartzJobBean {
    private static final Logger logger = LoggerFactory
            .getLogger(WxAppTokenJobBean.class);
    @Resource
    private SelectOptionService selectOptionService;
    @Resource
    private CommonUtilService commonUtilService;

    @Resource
    private WxMsgService wxMsgService;

    @Resource
    private WxGzhMsgService wxGzhMsgService;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        logger.info("wxAppTokenCheckJob-start");
        String schema_name = (String) context.getJobDetail().getJobDataMap().get("schema_name");
        if (StringUtils.isEmpty(schema_name)) {
         // todo  logger.error(commonUtilService.getLanguageInfoByUserLang(me LangConstant.MSG_M));
            return;
        }
        try {
            wxMsgService.checkWxAccessToken(schema_name);
        } catch (Exception e) {
            logger.error("wxAppTokenCheckJob deal with job error! {}", e.getLocalizedMessage());
        }
        try {
            wxGzhMsgService.checkWxAccessToken(schema_name);
        } catch (Exception e) {
            logger.error("wxAppTokenCheckJob deal with wxGzh job error! {}", e);
        }
        logger.info("wxAppTokenCheckJob-end");
    }
}
