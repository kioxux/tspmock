package com.gengyun.senscloud.job;

import com.gengyun.senscloud.service.pollute.CheckMeterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class CheckMeterJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(CheckMeterJobBean.class);

    @Resource
    CheckMeterService checkMeterService;

    @Override
    public void handleData(String schemaName) {
        logger.debug("CheckMeterJobBean *********************** start");
        try {
            checkMeterService.asyncCronJobToGenerateCheckMeter(schemaName);
        } catch (Exception e) {
            logger.error("CheckMeterJobBean.handleData fail ~ ", e);
        }
        logger.debug("CheckMeterJobBean *********************** end");
    }
}
