package com.gengyun.senscloud.job.impl;

import com.gengyun.senscloud.job.BaseJobBean;
import com.gengyun.senscloud.service.job.MaintainJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 行事历过期数据处理job
 */
public class PlanWorkCalendarExpireHandleJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(PlanWorkCalendarExpireHandleJobBean.class);

    @Resource
    private MaintainJobService maintainJobService;

    /**
     * 抽象方法-业务数据处理
     *
     * @param schemaName
     * @return
     */
    @Override
    public void handleData(String schemaName) {
        logger.debug("MaintainJobBean *********************** start");
        try {
            maintainJobService.expirePlanWorkCalendar(schemaName);
        } catch (Exception e) {
            logger.error("MaintainJobBean.handleData fail ~ ", e);
        }
        logger.debug("MaintainJobBean *********************** end");
    }
}
