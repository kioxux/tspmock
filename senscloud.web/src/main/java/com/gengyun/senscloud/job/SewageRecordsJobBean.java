package com.gengyun.senscloud.job;

import com.gengyun.senscloud.service.pollute.CheckMeterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class SewageRecordsJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(SewageRecordsJobBean.class);

    @Resource
    CheckMeterService checkMeterService;

    @Override
    public void handleData(String schemaName) {
        logger.debug("SewageRecordsJobBean *********************** start");
        try {
            checkMeterService.asyncCronJobToGenerateSewageRecords(schemaName);
        } catch (Exception e) {
            logger.error("SewageRecordsJobBean.handleData fail ~ ", e);
        }
        logger.debug("SewageRecordsJobBean *********************** end");
    }
}
