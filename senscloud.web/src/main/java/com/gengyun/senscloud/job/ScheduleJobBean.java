package com.gengyun.senscloud.job;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.model.ScheduleData;
import com.gengyun.senscloud.service.ScheduleService;
import com.gengyun.senscloud.util.RegexUtil;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import javax.annotation.Resource;
import java.util.List;

public class ScheduleJobBean extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(ScheduleJobBean.class);

    @Resource
    private ScheduleService scheduleService;

    @Resource
    private Scheduler scheduler;

    @Value("${schedule.initial.enable}")
    private String enable;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        // 控制服务,在调试环境下,不需要持续监控,正式版本后,再开启监控;
        if (!RegexUtil.optEquals(enable, "true")) {
            return;
        }
        String schema_name = (String) context.getJobDetail().getJobDataMap().get("schema_name");
        if (RegexUtil.optIsBlankStr(schema_name)) {
            logger.error(ErrorConstant.EC_JOB_002);
            return;
        }
        try {
            List<ScheduleData> scheduleList = scheduleService.find(schema_name, null, null, "t", null, null,1);
            if (null != scheduleList && scheduleList.size() > 0) {
                for (ScheduleData schedule : scheduleList) {
                    JobDetailFactoryBean jobDetailBean = new JobDetailFactoryBean();
                    jobDetailBean.setName("job_schedule_" + schema_name + "_" + schedule.getId());
                    jobDetailBean.setJobClass((Class<? extends Job>) Class.forName(schedule.getTask_class()));
                    JobDataMap map = new JobDataMap();
                    map.put("schedule_id", schedule.getId());
                    map.put("schema_name", schema_name);
                    jobDetailBean.setJobDataMap(map);
                    jobDetailBean.afterPropertiesSet();
                    TriggerKey triggerKey = null;
                    Trigger trigger = null;
                    if (schedule.getTask_type() == 0) {
                        if (RegexUtil.optIsPresentStr(schedule.getExpression())) {
                            long interval = Long.parseLong(schedule.getExpression());
                            if (interval > 0) {
                                SimpleTriggerFactoryBean simpleTriggerBean = new SimpleTriggerFactoryBean();
                                simpleTriggerBean.setName("simple_schedule_" + schema_name + "_"
                                        + schedule.getId());
                                simpleTriggerBean.setGroup(Scheduler.DEFAULT_GROUP);
                                simpleTriggerBean.setRepeatInterval(interval);
                                simpleTriggerBean.afterPropertiesSet();
                                triggerKey = simpleTriggerBean.getObject().getKey();
                                trigger = simpleTriggerBean.getObject();
                            }
                        }
                    } else if (schedule.getTask_type() == 1) {
                        CronTriggerFactoryBean cronTriggerBean = new CronTriggerFactoryBean();
                        cronTriggerBean.setName("cron_schedule_" + schema_name + "_"
                                + schedule.getId().toString());
                        cronTriggerBean.setGroup(Scheduler.DEFAULT_GROUP);
                        cronTriggerBean.setCronExpression(schedule.getExpression());
                        cronTriggerBean.afterPropertiesSet();
                        triggerKey = cronTriggerBean.getObject().getKey();
                        trigger = cronTriggerBean.getObject();
                    }
                    JobKey jobKey = jobDetailBean.getObject().getKey();
                    if (scheduler.checkExists(triggerKey)) {
                        if (RegexUtil.optEquals(schedule.getIs_use(), "t")) {
                            if (schedule.getReschedule()) {
                                scheduler.rescheduleJob(triggerKey, trigger);
                            }
                        } else {
                            scheduler.deleteJob(jobKey);
                        }
                    } else {
                        if (scheduler.checkExists(jobKey)) {
                            scheduler.deleteJob(jobKey);
                        }
                        if (RegexUtil.optEquals(schedule.getIs_use(), "t")) {
                            scheduler.scheduleJob(jobDetailBean.getObject(), trigger);
                        }
                    }
                    if (schedule.getReschedule()) {
                        schedule.setReschedule(false);
                        scheduleService.update(schema_name, schedule);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("ScheduleJobBean deal with job error! {}", e.getLocalizedMessage());
        }
    }
}
