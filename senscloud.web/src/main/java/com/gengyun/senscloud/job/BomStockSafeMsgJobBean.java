package com.gengyun.senscloud.job;

import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.entity.CompanyEntity;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.MessageService;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.bom.BomService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.WxMsgService;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 备件低于安全库存消息提醒
 * User: sps
 * Date: 2020/05/29
 * Time: 上午11:20
 */
public class BomStockSafeMsgJobBean extends QuartzJobBean {
    private static final Logger logger = LoggerFactory
            .getLogger(BomStockSafeMsgJobBean.class);
    @Resource
    private WxMsgService wxMsgService;
    @Resource
    private MessageService messageService;
    @Resource
    private PagePermissionService pagePermissionService;

    @Resource
    private CompanyService companyService;
    @Resource
    private BomService bomService;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        logger.info("BomStockSafeMsgJob-start");
        String schema_name = (String) context.getJobDetail().getJobDataMap().get("schema_name");
        if (StringUtils.isEmpty(schema_name)) {
            return;
        }
        try {
            List<Map<String, Object>> bom_list = bomService.getBomLowerSecurityQuantityList(schema_name);
            MethodParam methodParam = new MethodParam();
            String company = schema_name.substring(7);
            Long companyId = Long.valueOf(company);
            CompanyEntity companyEntity = companyService.getCompanyById(companyId);
            methodParam.setOpenSms(companyEntity.getIs_open_sms());
            methodParam.setSchemaName(schema_name);
            Map<String, String> msg_config = messageService.getInfoByCode(methodParam, SensConstant.SMS_10002001);
            if (RegexUtil.optNotNull(bom_list).isPresent() && bom_list.size() > 0) {
                for (Map<String, Object> bom : bom_list) {
                    Integer security_quantity_msg_interval = null;
                    if (RegexUtil.optIsPresentStr(bom.get("security_quantity_msg_interval"))) {
                        security_quantity_msg_interval = Integer.valueOf(bom.get("security_quantity_msg_interval").toString());
                    } else {
                        security_quantity_msg_interval = Integer.valueOf(msg_config.get("security_quantity_msg_interval"));
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = null;
                    // 注意格式需要与上面一致，不然会出现异常
                    if (RegexUtil.optIsPresentStr(bom.get("security_quantity_msg_time"))) {
                        date = sdf.parse(bom.get("security_quantity_msg_time").toString());
                        Long diff = System.currentTimeMillis() - date.getTime();
                        Long hour = (diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
                        if (security_quantity_msg_interval <= hour) {
                            //查询有备件库房查看权限的用户
                            List<Map<String, Object>> user_list = pagePermissionService.getBomSmsPermissionUserList(schema_name, RegexUtil.optIntegerOrNull(bom.get("stock_id")));
                            for (Map<String, Object> user : user_list) {
                                methodParam.setCompanyName("备件安全库存提醒");
                                methodParam.setUserId(user.get("id").toString());
                                methodParam.setSchemaName(schema_name);
                                Long company_id = Long.valueOf(schema_name.substring(7));
                                methodParam.setCompanyId(company_id);
                                //备件名称：{1}，备件编码：{2}，库房：{3}，当前库存小于安全库存，请及时安排补充。
                                Object[] contents = new Object[]{bom.get("bom_name"), bom.get("material_code"), bom.get("stock_name")};
                                messageService.sendMsg(methodParam, contents, SensConstant.SMS_10002001, user.get("id").toString(), null, bom.get("id").toString(), "备件库存安全提醒");
                                //更新该仓库当前备件短信发送提醒时间
                                //bomService.updateBomStockSendMsgTime(methodParam.getSchemaName(), Integer.valueOf(bom.get("bom_id").toString()));
                                bomService.updateStockSendMsgTime(methodParam.getSchemaName(), Integer.valueOf(bom.get("stock_id").toString()));
                            }
                        }
                    } else {
                        //查询有备件库房查看权限的用户
                        List<Map<String, Object>> user_list = pagePermissionService.getBomSmsPermissionUserList(schema_name, RegexUtil.optIntegerOrNull(bom.get("stock_id")));
                        for (Map<String, Object> user : user_list) {
                            methodParam.setCompanyName("备件安全库存提醒");
                            methodParam.setUserId(user.get("id").toString());
                            methodParam.setSchemaName(schema_name);
                            Long company_id = Long.valueOf(schema_name.substring(7));
                            methodParam.setCompanyId(company_id);
                            //备件名称：{1}，备件编码：{2}，库房：{3}，当前库存小于安全库存，请及时安排补充。
                            Object[] contents = new Object[]{bom.get("bom_name"), bom.get("material_code"), bom.get("stock_name")};
                            messageService.sendMsg(methodParam, contents, SensConstant.SMS_10002001, user.get("id").toString(), null, bom.get("id").toString(), "备件库存安全提醒");
                            //更新该仓库当前备件短信发送提醒时间
                            //bomService.updateBomStockSendMsgTime(methodParam.getSchemaName(), Integer.valueOf(bom.get("bom_id").toString()));
                            bomService.updateStockSendMsgTime(methodParam.getSchemaName(), Integer.valueOf(bom.get("stock_id").toString()));
                        }
                    }
                }
            }
        } catch (
                Exception e) {
            logger.error("BomStockSafeMsgJob deal with job error! {}", e.getLocalizedMessage());
        }
        logger.info("BomStockSafeMsgJob-end");
    }
}
