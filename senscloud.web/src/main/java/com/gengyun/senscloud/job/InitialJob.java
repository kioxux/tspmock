package com.gengyun.senscloud.job;

import com.gengyun.senscloud.model.ScheduleData;
import com.gengyun.senscloud.service.ScheduleService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
public class InitialJob {
    Logger logger = LoggerFactory.getLogger(InitialJob.class);

    @Resource
    Scheduler scheduler;

    @Value("${schedule.initial.interval}")
    private Long interval;

    @Resource
    ScheduleService scheduleService;

    @Resource
    private CompanyService companyService;

    @Value("${senscloud.com.license}")
    private String license;

    @PostConstruct
    public void init() {
        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamOne(license,"license","findAllCompanyByLicense"));
        List<Map<String, Object>> companyList = result;
        // List<Map<String, Object>> companyList = companyService.findAll();
        for (Map<String, Object> company : companyList) {
            try {
                if (RegexUtil.optEquals(company.get("is_use"), "1")) {
                    scheduleCompany(company);
                }
            } catch (Exception e) {
                logger.error("InitialJob init {} error!", company.get("company_name"));
            }
        }
    }

    public void scheduleCompany(Map<String, Object> company) throws Exception {
        String schema_name = SenscloudUtil.getSchemaByCompId(company.get("id"));
        if (!scheduler.checkExists(new TriggerKey("simple_schedule_" + schema_name))) {
            ScheduleData scheduleData = new ScheduleData();
            scheduleData.setIs_running(false);
            try {
                scheduleService.update(schema_name, scheduleData);
            } catch (Exception e) {
                logger.error("InitialJob Schedule Company {} error!", company.get("company_name"));
                return;
            }
            JobDetailFactoryBean jobDetailBean = new JobDetailFactoryBean();
            jobDetailBean.setName("job_schedule_" + schema_name);
            jobDetailBean.setJobClass(ScheduleJobBean.class);
            JobDataMap map = new JobDataMap();
            map.put("schema_name", schema_name);
            jobDetailBean.setJobDataMap(map);
            jobDetailBean.afterPropertiesSet();
            SimpleTriggerFactoryBean simpleTriggerBean = new SimpleTriggerFactoryBean();
            simpleTriggerBean.setName("simple_schedule_" + schema_name);
            simpleTriggerBean.setGroup(Scheduler.DEFAULT_GROUP);
            simpleTriggerBean.setRepeatInterval(interval);
            simpleTriggerBean.afterPropertiesSet();
            scheduler.scheduleJob(jobDetailBean.getObject(), simpleTriggerBean.getObject());
        }
    }

    @PreDestroy
    public void destroy() throws Exception {
        scheduler.clear();
        scheduler.shutdown();
    }
}
