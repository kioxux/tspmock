package com.gengyun.senscloud.job;

import com.gengyun.senscloud.service.pollute.PolluteFeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class PolluteFreeJobBean extends BaseJobBean{
    private static final Logger logger = LoggerFactory.getLogger(PolluteFreeJobBean.class);

    @Resource
    PolluteFeeService polluteFeeService;

    @Override
    public void handleData(String schemaName) {
        logger.debug("PolluteFreeJobBean *********************** start");
        try {
            polluteFeeService.asyncCronJobToGeneratePolluteFree(schemaName);
        } catch (Exception e) {
            logger.error("PolluteFreeJobBean.handleData fail ~ ", e);
        }
        logger.debug("PolluteFreeJobBean *********************** end");
    }
}
