package com.gengyun.senscloud.job;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.model.ScheduleData;
import com.gengyun.senscloud.service.ScheduleService;
import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class BaseJobBean extends QuartzJobBean {

    private static final Logger logger = LoggerFactory.getLogger(BaseJobBean.class);

    @Resource
    private ScheduleService scheduleService;

    /**
     * 抽象方法-业务数据处理
     * @param schemaName
     * @return
     */
    public abstract void handleData(String schemaName);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        logger.debug("BaseJobBean *********************** start");
        Object oId = jobExecutionContext.getJobDetail().getJobDataMap().get("schedule_id");
        String schemaName = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("schema_name");
        if (oId == null) {
            logger.error(ErrorConstant.EC_JOB_001);
            return;
        }
        if (StringUtils.isEmpty(schemaName)) {
            logger.error(ErrorConstant.EC_JOB_002);
            return;
        }
        int id = (int) oId;
        if (scheduleService.checkRunning(schemaName, id)) {
            logger.debug("【BaseJobBean】" + "任务开始" + new SimpleDateFormat(Constants.DATE_FMT_SSS).format(Calendar.getInstance().getTime()));
            try {
                handleData(schemaName);
            } catch (Exception e) {
                logger.error(ErrorConstant.EC_JOB_003, e);
            }
            ScheduleData scheduleData = new ScheduleData();
            scheduleData.setId(id);
            scheduleData.setIs_running(false);
            scheduleService.update(schemaName, scheduleData);
            logger.debug("【BaseJobBean】" + "任务结束" + new SimpleDateFormat(Constants.DATE_FMT_SSS).format(Calendar.getInstance().getTime()));
        }
    }
}
