package com.gengyun.senscloud.job.impl;

import com.gengyun.senscloud.job.BaseJobBean;
import com.gengyun.senscloud.service.job.MaintainAsyncJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 维保计划生成行事历job
 */
public class MaintainJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(MaintainJobBean.class);

    @Autowired
    MaintainAsyncJobService maintainAsyncJobService;

    /**
     * 抽象方法-业务数据处理
     *
     * @param schemaName
     * @return
     */
    @Override
    public void handleData(String schemaName) {
        logger.debug("MaintainJobBean *********************** start");
        try {
            maintainAsyncJobService.asyncCronJobToGeneratePlanWorkWithNewModel(schemaName);
        } catch (Exception e) {
            logger.error("MaintainJobBean.handleData fail ~ ", e);
        }
        logger.debug("MaintainJobBean *********************** end");
    }
}
