package com.gengyun.senscloud.job.impl;

import com.gengyun.senscloud.job.BaseJobBean;
import com.gengyun.senscloud.service.job.WorkOrderAsyncJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 行事历生成工单job
 */
public class PlanWorkCalendar2WorkOrderJobBean extends BaseJobBean {
    private static final Logger logger = LoggerFactory.getLogger(PlanWorkCalendar2WorkOrderJobBean.class);

    @Autowired
    WorkOrderAsyncJobService workOrderAsyncJobService;

    /**
     * 抽象方法-业务数据处理
     *
     * @param schemaName
     * @return
     */
    @Override
    public void handleData(String schemaName) {
        logger.debug("MaintainJobBean *********************** start");
        try {
            workOrderAsyncJobService.asyncCronJobToGenerateWorkOrderByCalendar(schemaName);
        } catch (Exception e) {
            logger.error("PlanWorkCalendar2WorkOrderJobBean.handleData fail ~ ", e);
        }
        logger.debug("MaintainJobBean *********************** end");
    }
}
