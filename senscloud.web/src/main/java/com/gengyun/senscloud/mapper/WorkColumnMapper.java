package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.model.WorkColumnExAdd;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface WorkColumnMapper {

    @Select("SELECT count(1) " +
            "FROM ${schema_name}._sc_work_columns w " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON w.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON w.field_section_type = c2.code AND c2.data_type = 'block' " +
            "${searchWord}")
    int countWorkColumnList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord);

    @Select("SELECT w.field_form_code,w.field_code,w.field_name,w.field_view_type,w.field_section_type," +
            "w.create_time,w.create_user_id,c1.name as field_view_type_name,c2.name as field_section_type_name," +
            "w.save_type,w.field_right,w.is_required,w.field_remark " +
//            ",w.change_type,w.field_value,w.is_table_key,w.db_table_type,w.field_data_base,w.field_validate_method " +
            "FROM ${schema_name}._sc_work_columns w " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON w.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON w.field_section_type = c2.code AND c2.data_type = 'block' " +
            "${searchWord}")
    List<Map<String, Object>> findWorkColumnList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord);

    @Insert("INSERT INTO ${schema_name}._sc_work_columns(field_form_code, field_code, field_name, save_type, change_type, field_right," +
            " field_value, is_required, is_table_key, db_table_type, field_remark, field_data_base, field_view_type, field_section_type, " +
            "field_validate_method, create_time, create_user_id,data_key) " +
            "VALUES (#{field_form_code}, #{field_code}, #{field_name}, #{save_type}, #{change_type}, #{field_right}, #{field_value}, " +
            "#{is_required}, #{is_table_key}, #{db_table_type}, #{field_remark}, #{field_data_base}, #{field_view_type}, #{field_section_type}," +
            " #{field_validate_method}, #{create_time}, #{create_user_id},#{data_key})")
    int insertWorkColumn(Map<String, Object> data);

    @Insert("<script> INSERT INTO ${schema_name}._sc_work_column_ext(field_form_code, field_name, field_write_type, " +
            "field_value, create_time, create_user_id) " +
            "VALUES " +
            "<foreach collection ='list' item='ex' separator =','>" +
            "(#{ex.field_form_code}, #{ex.field_name}, #{ex.field_write_type}, #{ex.field_value}, #{ex.create_time}, #{ex.create_user_id}) " +
            "</foreach > </script>")
    int insertWorkColumnExBatch(@Param("schema_name") String schemaName, @Param("list") List<WorkColumnExAdd> extList);

    @Select("SELECT w.field_form_code,w.field_code,w.field_name,w.field_view_type,w.field_section_type," +
            "w.create_time,w.create_user_id,c1.name as field_view_type_name,c2.name as field_section_type_name," +
            "w.save_type,w.change_type,w.field_right,w.field_value,w.is_required,w.is_table_key,w.db_table_type," +
            "w.field_remark,w.field_data_base,w.field_validate_method,w.data_key " +
            "FROM ${schema_name}._sc_work_columns w " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON w.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON w.field_section_type = c2.code AND c2.data_type = 'block' " +
            "where w.field_form_code= #{field_form_code}")
    Map<String, Object> findWorkColumnByFieldFormCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code);

    @Select("SELECT * FROM ${schema_name}._sc_work_columns w " +
            "where w.field_code= #{field_code}")
    Map<String, Object> findWorkColumnByFieldCode(@Param("schema_name") String schemaName, @Param("field_code") String field_code);

    @Select("SELECT count(1) " +
            "FROM ${schema_name}._sc_work_columns w " +
            "where w.field_form_code= #{field_form_code}")
    int countInfoByFieldFormCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_column_ext WHERE field_form_code = #{field_form_code}")
    int deleteWorkColumnExtByFieldFormCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code);

    @Update("<script> UPDATE ${schema_name}._sc_work_columns  " +
            "<set> " +
            "<if test=\"field_code!=null and field_code !=''\"> " +
            "field_code = #{field_code}, " +
            "</if> " +
            "<if test=\"field_name!=null and field_name !=''\"> " +
            "field_name = #{field_name}, " +
            "</if> " +
            "<if test=\"save_type!=null and save_type !=''\"> " +
            "save_type = #{save_type}, " +
            "</if> " +
            "<if test=\"change_type!=null and change_type !=''\"> " +
            "change_type = #{change_type}, " +
            "</if> " +
            "<if test=\"field_right!=null and field_right !=''\"> " +
            "field_right = #{field_right}, " +
            "</if> " +
            "<if test=\"is_required!=null and is_required !=''\"> " +
            "is_required = #{is_required}, " +
            "</if> " +
            "<if test=\"is_table_key!=null and is_table_key !=''\"> " +
            "is_table_key = #{is_table_key}, " +
            "</if> " +
            "<if test=\"db_table_type!=null and db_table_type !=''\"> " +
            "db_table_type = #{db_table_type}, " +
            "</if> " +
            "<if test=\"field_remark!=null and field_remark !=''\"> " +
            "field_remark = #{field_remark}, " +
            "</if> " +
            "<if test=\"field_data_base!=null and field_data_base !=''\"> " +
            "field_data_base = #{field_data_base}, " +
            "</if> " +
            "<if test=\"field_view_type!=null and field_view_type !=''\"> " +
            "field_view_type = #{field_view_type}, " +
            "</if> " +
            "<if test=\"field_section_type!=null and field_section_type !=''\"> " +
            "field_section_type = #{field_section_type}, " +
            "</if> " +
            "<if test=\"field_validate_method!=null and field_validate_method !=''\"> " +
            "field_validate_method = #{field_validate_method}, " +
            "</if> " +
            "<if test=\"create_time!=null \"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"create_user_id!=null and create_user_id !=''\"> " +
            "create_user_id = #{create_user_id} , " +
            "</if> " +
//            "<if test=\"field_value!=null and field_value !=''\"> " +
            "field_value = #{field_value}, " +
//            "</if> " +
//            "<if test=\"data_key!=null\"> " +
            "data_key = #{data_key}  " +
//            "</if> " +
            "</set> " +
            "WHERE " +
            "field_form_code = #{field_form_code} </script>")
    int updateWorkColumn(Map<String, Object> data);

    @Select("SELECT count(1) FROM ${schema_name}._sc_work_flow_node_column WHERE field_form_code = #{field_form_code}")
    int findCountUseByFieldFormCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_columns WHERE field_form_code = #{field_form_code}")
    int deleteWorkColumnByFieldFormCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code);

    @Select("SELECT id,field_form_code,field_name,field_write_type,field_value,create_time,create_user_id " +
            "FROM ${schema_name}._sc_work_column_ext WHERE field_form_code = #{field_form_code} order by id")
    List<Map<String, Object>> findWorkColumnExtByFieldFormCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code);

    @Select("SELECT count(1) FROM ${schema_name}._sc_work_columns WHERE field_code = #{field_code}")
    int countFieldCode(@Param("schema_name") String schemaName, @Param("field_code") String fieldCode);
}
