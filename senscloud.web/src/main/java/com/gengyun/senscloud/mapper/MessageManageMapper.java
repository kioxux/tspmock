package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 消息管理中心
 */
public interface MessageManageMapper {
    /**
     * 根据用户分页查询消息列表
     */
    @Select(" <script> " +
            " SELECT  mr.message_id,mr.id,mr.content, m.msg_content, M.send_time, M.send_user_id,M.type,mr.receive_user_id,mr.is_read," +
            " u1.user_name AS send_user_name , u2.user_name AS receive_user_name " +
            " FROM ${schema_name}._sc_message_receiver AS mr " +
            " LEFT JOIN ${schema_name}._sc_message_record AS m ON M.ID=mr.message_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u1 ON u1.id=M.send_user_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id=mr.receive_user_id " +
            " WHERE m.type = 3 AND   mr.is_success = '1' AND mr.receive_user_id = #{pm.receive_user_id} " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND m.msg_content LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " </when> " +
            " <when test='pm.startTime!=null and pm.startTime!=\"\" and pm.endTime!=\"\" and pm.endTime!=null'>" +
            " AND m.send_time &gt;=#{pm.startTime}::TIMESTAMP AND m.send_time &lt;=#{pm.endTime}::TIMESTAMP  " +
            " </when>" +
            " ORDER BY m.send_time DESC ${pm.pagination}" +
            " </script> ")
    List<Map<String, Object>> findMessagePageByReceiver(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据用户分页查询消息列表数量
     */
    @Select(" <script> " +
            " SELECT COUNT(1)" +
            " FROM ${schema_name}._sc_message_receiver AS mr " +
            " LEFT JOIN ${schema_name}._sc_message_record AS m ON M.ID=mr.message_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u1 ON u1.id=M.send_user_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id=mr.receive_user_id " +
            " WHERE m.type = 3 AND mr.receive_user_id = #{pm.receive_user_id} " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND m.msg_content LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " </when> " +
            " <when test='pm.startTime!=null and pm.startTime!=\"\" and pm.endTime!=\"\" and pm.endTime!=null'>" +
            " AND m.send_time &gt;=#{pm.startTime}::TIMESTAMP AND m.send_time &lt;=#{pm.endTime}::TIMESTAMP  " +
            " </when>" +
            " </script> ")
    Integer findMessagePageByReceiverCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增消息主体
     */
    @Insert(" INSERT INTO ${schema_name}._sc_message_record( msg_content, send_time, business_type, " +
            " business_no, send_user_id, type, receive_mobile,message_key,message_param) " +
            " VALUES ( #{pm.msg_content},now(), #{pm.business_type}, #{pm.business_no}, " +
            " #{pm.send_user_id}, #{pm.type}, #{pm.receive_mobile},#{pm.message_key},#{pm.message_param})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertMessageRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增消息接收人
     */
    @Insert(" INSERT INTO ${schema_name}._sc_message_receiver( message_id, receive_user_id, is_main_receiver, is_read,is_success,content) " +
            " VALUES (#{message_id}, #{receive_user_id}, #{is_main_receiver}, #{is_read} , #{is_success}, #{content})")
    void insertMessageReceiver(@Param("schema_name") String schema_name,
                               @Param("message_id") Integer message_id,
                               @Param("receive_user_id") String receive_user_id,
                               @Param("is_main_receiver") String is_main_receiver,
                               @Param("is_read") String is_read,
                               @Param("is_success") String is_success,
                               @Param("content") String content);

    /**
     * 根据消息id和阅读人更新为已读状态
     */
    @Update(" UPDATE  ${schema_name}._sc_message_receiver " +
            " SET is_read = '1' ," +
            " read_time = now() " +
            " WHERE is_read = '-1' " +
            " AND receive_user_id = #{receive_user_id} " +
            " AND message_id = #{message_id} ")
    void updateMessageReceiverRead(@Param("schema_name") String schema_name, @Param("receive_user_id") String receive_user_id, @Param("message_id") Integer message_id);

    /**
     * 查询接收人的未读消息数量
     */
    @Select(" SELECT COUNT(id) FROM ${schema_name}._sc_message_receiver " +
            " WHERE is_read='-1' AND receive_user_id = #{receive_user_id} AND is_success = '1' ")
    Integer findNoReadCountByReceiver(@Param("schema_name") String schema_name, @Param("receive_user_id") String receive_user_id);

    /**
     * 根据接收人查询最新三个未读消息
     */
    @Select(" SELECT  M.send_time," +
            " mr.id,mr.message_id,m.msg_content, mr.receive_user_id,mr.is_main_receiver,mr.is_read,mr.read_time,mr.content,mr.is_success," +
            " u1.user_name AS send_user_name,  u2.user_name AS receive_user_name" +
            " FROM ${schema_name}._sc_message_receiver AS mr " +
            " LEFT JOIN ${schema_name}._sc_message_record AS m ON M.ID=mr.message_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u1 ON u1.id=M.send_user_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id=mr.receive_user_id " +
            " WHERE m.type = 3 AND mr.receive_user_id = #{receive_user_id} AND mr.is_success = '1'  AND is_read = '-1' " +
            " ORDER BY m.send_time DESC LIMIT 3 ")
    List<Map<String, Object>> findNoReadThreeCountByReceiver(@Param("schema_name") String schema_name, @Param("receive_user_id") String receive_user_id);

    /**
     * 根据消息id获取消息详情
     */
    @Select(" SELECT to_char(M.send_time, '" + Constants.DATE_FMT + "') AS send_time," +
            " mr.id,m.send_user_id ,mr.message_id,mr.receive_user_id,mr.is_main_receiver," +
            " mr.is_read,mr.read_time,mr.content,mr.is_success," +
            " u1.user_name AS send_user_name ,m.msg_content " +
            " FROM ${schema_name}._sc_message_receiver mr " +
            " LEFT JOIN ${schema_name}._sc_message_record AS m ON M.ID=mr.message_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u1 ON u1.id=M.send_user_id " +
            " WHERE  mr.message_id = #{message_id} AND mr.receive_user_id = #{receive_user_id} LIMIT 1 ")
    Map<String, Object> findMessageInfoById(@Param("schema_name") String schema_name, @Param("message_id") Integer message_id, @Param("receive_user_id") String receive_user_id);
}
