package com.gengyun.senscloud.mapper;

/**
 * 批报查询
 */
public interface BatchReportMapper {
//
//    /**
//     * 查询设备批报列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT ${queryColumns} ARRAY_TO_STRING(ARRAY_AGG(DISTINCT f.short_title), ',') as facility_name,asset.strname,ac.category_name " +
//            "FROM ${schema_name}._sc_asset_monitor am " +
//            "INNER JOIN ${schema_name}._sc_metadata_monitors mm ON mm.monitor_key = am.monitor_name " +
//            "INNER JOIN ${schema_name}._sc_asset as asset ON asset.strcode = am.asset_code " +
//            "INNER JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id AND f.org_type in ("+ SqlConstant.FACILITY_INSIDE +") " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY am.asset_code,am.gather_time,asset.strname,ac.category_name " +
//            "ORDER BY am.gather_time DESC,am.asset_code limit ${page} offset ${begin}")
//    List<Map<String, Object>> findBTSReceiveBatchReportList(@Param("schema_name") String schema_name, @Param("queryColumns") String queryColumns, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询设备批报总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM " +
//            "(SELECT ${queryColumns} ARRAY_TO_STRING(ARRAY_AGG(DISTINCT f.short_title), ',') as facilityName,asset.strname,ac.category_name " +
//            "FROM ${schema_name}._sc_asset_monitor am " +
//            "INNER JOIN ${schema_name}._sc_metadata_monitors mm ON mm.monitor_key = am.monitor_name " +
//            "INNER JOIN ${schema_name}._sc_asset as asset ON asset.strcode = am.asset_code " +
//            "INNER JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id AND f.org_type in ("+ SqlConstant.FACILITY_INSIDE +") " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY am.asset_code,am.gather_time,asset.strname,ac.category_name ) t")
//    int countBTSReceiveBatchReportList(@Param("schema_name") String schema_name, @Param("queryColumns") String queryColumns, @Param("condition") String condition);
//    /**
//     * 查询设备批报列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
////    @Select("SELECT ${queryColumns} am.gather_time,asset.strname,am.asset_code " +
////            "FROM ${schema_name}._sc_asset_monitor am " +
////            "INNER JOIN ${schema_name}._sc_asset as asset ON asset.strcode = am.asset_code " +
////            "WHERE 1=1 ${condition} " +
////            "GROUP BY am.asset_code,am.gather_time,asset.strname " +
////            "ORDER BY am.gather_time DESC,am.asset_code limit ${page} offset ${begin}")
//    @Select("SELECT ${queryColumns} am.gather_time,am.asset_code,max(am.strname) as strname " +
//            "FROM (" +
//            "   SELECT m.gather_time,m.asset_code,m.monitor_name,m.monitor_value,asset.strname " +
//            "   FROM ${schema_name}._sc_asset_monitor m " +
//            "   LEFT JOIN ${schema_name}._sc_asset AS asset ON asset.strcode = m.asset_code " +
//            "   WHERE 1 = 1 ${condition2} " +
//            ") am " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY am.gather_time,am.asset_code " +
//            "ORDER BY am.gather_time DESC,am.asset_code limit ${page} offset ${begin}")
//    List<Map<String, Object>> findReportList(@Param("schema_name") String schema_name, @Param("queryColumns") String queryColumns, @Param("condition") String condition, @Param("condition2") String condition2, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询设备批报总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
////    @Select("SELECT ${querySunColumns}count(1) as total FROM " +
////            "(SELECT ${queryColumns} am.asset_code,asset.strname,gather_time " +
////            "FROM ${schema_name}._sc_asset_monitor am " +
////            "INNER JOIN ${schema_name}._sc_asset as asset ON asset.strcode = am.asset_code " +
////            "WHERE 1=1 ${condition} " +
////            "GROUP BY am.asset_code,am.gather_time,asset.strname) s")
//    @Select("SELECT ${querySunColumns} count(1) as total FROM " +
//            "(" +
//            "   SELECT ${queryColumns} am.gather_time,am.asset_code " +
//            "   FROM (" +
//            "       SELECT m.gather_time,m.asset_code,m.monitor_name,m.monitor_value,asset.strname " +
//            "       FROM ${schema_name}._sc_asset_monitor m " +
//            "       LEFT JOIN ${schema_name}._sc_asset AS asset ON asset.strcode = m.asset_code " +
//            "       WHERE 1 = 1 ${condition2} " +
//            "   ) am " +
//            "   WHERE 1=1 ${condition} " +
//            "   GROUP BY am.gather_time,am.asset_code " +
//            ") s")
//    Map<String,Object> countReportList(@Param("schema_name") String schema_name, @Param("queryColumns") String queryColumns, @Param("querySunColumns") String querySunColumns, @Param("condition") String condition, @Param("condition2") String condition2);
//
//    /**
//     * 查询设备批报列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT ${sunColumns}${groupString}as gather_time,count(s.gather_time) as report_times, s.asset_code,s.strname FROM " +
//            "(SELECT ${queryColumns} am.gather_time,asset.strname,am.asset_code " +
//            "FROM ${schema_name}._sc_asset_monitor am " +
//            "INNER JOIN ${schema_name}._sc_asset as asset ON asset.strcode = am.asset_code " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY am.asset_code,am.gather_time,asset.strname)s " +
//            "GROUP BY s.asset_code,s.strname,${groupString} "+
//            "ORDER BY ${groupString} DESC,s.asset_code limit ${page} offset ${begin}")
//    List<Map<String, Object>> findReportTimeList(@Param("schema_name") String schema_name, @Param("queryColumns") String queryColumns,@Param("sunColumns") String sunColumns,@Param("groupString") String groupString, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询设备批报总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM (" +
//            "SELECT ${sunColumns}${groupString},s.asset_code,s.strname FROM " +
//            "(SELECT ${queryColumns} am.gather_time,asset.strname,am.asset_code " +
//            "FROM ${schema_name}._sc_asset_monitor am " +
//            "INNER JOIN ${schema_name}._sc_asset as asset ON asset.strcode = am.asset_code " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY am.asset_code,am.gather_time,asset.strname)s " +
//            "GROUP BY s.asset_code,s.strname,${groupString} "+
//            ") t")
//    int countReportTimeList(@Param("schema_name") String schema_name, @Param("queryColumns") String queryColumns,@Param("sunColumns") String sunColumns,@Param("groupString") String groupString, @Param("condition") String condition);
//
//    /**
//     * 查询设备、客户的详情信息
//     * @param schema_name
//     * @param
//     * @param strcode
//     * @return
//     */
//    @Select("SELECT array_to_string(array_agg(f.short_title),',')as short_title,asset.strname,asset.strcode,asset.enable_time,am.model_name,asset._id as assetId,asset.category_id,ac.category_name " +
//            " FROM ${schema_name}._sc_asset as asset " +
//            " LEFT JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id AND f.org_type in ("+ SqlConstant.FACILITY_INSIDE +") " +
//            " LEFT JOIN ${schema_name}._sc_asset_iot_setting i ON i.asset_id = asset._id " +
//            " LEFT JOIN ${schema_name}._sc_asset_model am ON am.id = asset.asset_model_id " +
//            " WHERE asset.strcode = '${strcode}' and intstatus>0 " +
//            "GROUP BY asset.strname,asset.strcode,am.model_name,asset._id,asset.enable_time ,asset.category_id,ac.category_name ")
//    public Map<String,Object> findVmiAssetBydAssetCode(@Param("schema_name") String schema_name, @Param("strcode") String strcode);
//
//    /**
//     * 查询设备月报年报列表
//     *
//     * @param schemaName
//     * @param groupString
//     * @param assetCondition
//     * @return
//     */
//    @Select("SELECT ${groupString} AS gather_time,SUM(CAST ((CASE monitor_value  WHEN '' THEN '0.00' ELSE monitor_value END )AS DECIMAL ( 10, 2 ) )) AS monitor_value FROM " +
//            "${schemaName}._sc_asset_monitor_current  " +
//            "WHERE monitor_name='Month_Dosing_Accumulate_Total' AND monitor_value_type='20' ${assetCondition} " +
//            "GROUP BY ${groupString} " +
//            "limit ${page} offset ${begin} ")
//    List<Map<String, Object>> findReportMonthAndYearList(@Param("schemaName") String schemaName, @Param("groupString") String groupString,@Param("assetCondition") String assetCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询设备月报年报总数
//     *
//     * @param schemaName
//     * @param groupString
//     * @param assetCondition
//     * @return
//     */
//    @Select("SELECT count(1) as total,SUM(monitor_value) as monitor_value  FROM (" +
//            "SELECT ${groupString} AS gather_time,SUM(CAST ((CASE monitor_value  WHEN '' THEN '0.00' ELSE monitor_value END )AS DECIMAL ( 10, 2 ) )) AS monitor_value FROM " +
//            "${schemaName}._sc_asset_monitor_current  " +
//            "WHERE monitor_name='Month_Dosing_Accumulate_Total' AND monitor_value_type='20' ${assetCondition} " +
//            "GROUP BY ${groupString} " +
//            ") t")
//    Map<String,Object> countReportMonthAndYearList(@Param("schemaName") String schemaName, @Param("groupString") String groupString,@Param("assetCondition") String assetCondition);
}
