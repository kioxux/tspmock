package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetNoticeMapper {
//    @Select("select n.*,u.username,u.mobile from ${schema_name}._sc_asset_notice n left join ${schema_name}._sc_user u on n.user_account=u.account " +
//            "where n.asset_id=#{asset_id} order by n.create_time desc")
//    List<AssetNotice> findAllByAssetId(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    @Select("select * from ${schema_name}._sc_asset_notice where asset_id=#{asset_id} and user_account=#{user_account}")
//    AssetNotice findOneByAssetIdAndUserAccount(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id,
//                                               @Param("user_account") String user_account);
//
//    @Select("select * from ${schema_name}._sc_asset_notice where id=#{id}")
//    AssetNotice findOneById(@Param("schema_name") String schema_name, @Param("id") Integer id);
//
//    @Insert("insert into ${schema_name}._sc_asset_notice (asset_id, user_account, work_begin_notice, work_status_change_notice," +
//            "work_finished_notice, open_close_notice, create_time, create_user_account) values (#{notice.asset_id}, #{notice.user_account}," +
//            "#{notice.work_begin_notice}, #{notice.work_status_change_notice}, #{notice.work_finished_notice}, #{notice.open_close_notice}," +
//            "#{notice.create_time}, #{notice.create_user_account})")
//    int insert(@Param("schema_name") String schema_name, @Param("notice") AssetNotice assetNotice);
//
//    @Update("update ${schema_name}._sc_asset_notice set asset_id=#{notice.asset_id}, user_account=#{notice.user_account}, " +
//            "work_begin_notice=#{notice.work_begin_notice}, work_status_change_notice=#{notice.work_status_change_notice}, " +
//            "work_finished_notice=#{notice.work_finished_notice}, open_close_notice=#{notice.open_close_notice} where id=#{notice.id}")
//    int update(@Param("schema_name") String schema_name, @Param("notice") AssetNotice assetNotice);
//
//    @Delete("delete from ${schema_name}._sc_asset_notice where id=#{id}")
//    int delete(@Param("schema_name") String schema_name, @Param("id") Integer id);
}
