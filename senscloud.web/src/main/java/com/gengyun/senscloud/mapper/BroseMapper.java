package com.gengyun.senscloud.mapper;

public interface BroseMapper {
//
//    //查询所有设备
//    @Select("select asset._id as _id , asset.strcode as strcode, asset.strname as strname , asset.x as x,asset.y as y , cur.alarm_up as setting_hum_value ,cur.exception_up as setting_tem_value from ${schema_name}._sc_asset as asset " +
//            " left join ${schema_name}._sc_asset_status as st on st.id = asset.intstatus " +
//            " left join ${schema_name}._sc_asset_monitor_current as cur on asset.strcode=cur.asset_code and cur.monitor_value_unit='℃' " +
//            " where asset.intstatus<> "+StatusConstant.STATUS_DELETEED+" order by asset.strcode asc")
//    public List<BroseAssetMonitorModel>  getAssetMonitorTable(@Param("schema_name") String schema_name);
//
//    @Select("select asset._id as _id , asset.strcode as strcode, asset.strname as strname ,st.status as monitor_status, asset.x as x,asset.y as y from ${schema_name}._sc_asset as asset " +
//            "left join ${schema_name}._sc_asset_monitor_current  as mc on mc.asset_code =  asset.strcode and mc.monitor_value_unit='S' " +
//            "left join ${schema_name}._sc_asset_status as st on st.id = asset.intstatus " +
//            "where asset.intstatus<> "+StatusConstant.STATUS_DELETEED+" and asset._id =#{_id} order by asset.createtime asc")
//    public BroseAssetMonitorModel  getAssetMonitorTableById(@Param("schema_name") String schema_name,@Param("_id") String id);
//
//    @Select("select mc.asset_code as asset_code, mc.monitor_value as value,mc.monitor_value_unit as unit,mc.status as brose_status ,mc.alarm_up_task_code as error_info ,mc.alarm_up as setting_hum_value, mc.exception_up as setting_tem_value from ${schema_name}._sc_asset_monitor_current as mc ")
//    public List<BroseMonitorModel> getMonitorData(@Param("schema_name") String schema_name );
//
//
//    @Select("SELECT\n" +
//            "\thvalue AS tem_value,\n" +
//            "\tHH.asset_code AS asset_code,\n" +
//            "\tHH.gtime AS gather_time,\n" +
//            "\tHH.exception_up AS setting_tem_value,\n" +
//            "\tDD.monitor_value AS hum_value,\n" +
//            "\tDD.alarm_up AS setting_hum_value\n" +
//            "FROM\n" +
//            "\t(\n" +
//            "\t\tSELECT\n" +
//            "\t\t\tmonitor_name AS hname,\n" +
//            "\t\t\tmonitor_value AS hvalue,\n" +
//            "\t\t\tasset_code,\n" +
//            "\t\t\tgather_time AS gtime,\n" +
//            "\t\t\texception_up,\n" +
//            "\t\t\tcreate_time\n" +
//            "\t\tFROM\n" +
//            "\t\t\t${schema_name}._sc_asset_monitor\n" +
//            "\t\tWHERE\n" +
//            "\t\t\tmonitor_value_unit = '℃'\n" +
//            "\t) HH\n" +
//            "LEFT JOIN (\n" +
//            "\tSELECT\n" +
//            "\t\tmonitor_name,\n" +
//            "\t\tmonitor_value,\n" +
//            "\t\tasset_code,\n" +
//            "\t\tgather_time,\n" +
//            "\t\talarm_up,\n" +
//            "\t\tcreate_time\n" +
//            "\tFROM\n" +
//            "\t\t${schema_name}._sc_asset_monitor\n" +
//            "\tWHERE\n" +
//            "\t\tmonitor_value_unit = '%'\n" +
//            ") DD ON HH.asset_code = DD.asset_code\n" +
//            "AND HH.gtime = DD.gather_time\n" +
//            "WHERE\n" +
//            "\tHH.asset_code =  #{strcode}\n" +
//            "AND HH.create_time > (current_timestamp -  interval '${i} hour') \n" +
//            "ORDER BY\n" +
//            "\tHH.create_time ASC")
//    public List<BroseMonitorModel> getTemAndHumByAssetCode(@Param("schema_name") String schema_name ,@Param("strcode") String strcode,@Param("i") int i);
//
//    @Update("update ${schema_name}._sc_asset set intstatus=${status} where _id ='${ids}'")
//    int setSelectStatus(@Param("schema_name") String schema_name, @Param("ids") String _id, @Param("status") int status);
//
//
//    @Select("SELECT   bb.monitor_value   " +
//            "FROM " +
//            " ${schema_name}._sc_asset_monitor AS bb" +
//            " WHERE bb.monitor_value_unit = 'S' and bb.monitor_value!='4' and bb.asset_code = #{strcode} " +
//            " order by id desc  " +
//            " limit 1 ")
//    String selectBeforeStatus(@Param("schema_name") String schema_name, @Param("strcode") String strcode);
//
//    @Select("select * from ${schema_name}._sc_asset where _id=#{ids}")
//    Asset selectAssetInfoById(@Param("schema_name") String schema_name, @Param("ids") String _id);
//
//    @Insert("insert into ${schema_name}._sc_asset_monitor (asset_code , monitor_name , monitor_value_type, monitor_value_unit , monitor_value , gather_time,create_time, status) values (" +
//            "#{strcode},'Status',1,'S','${status}',#{gather_time},now(),1)")
//    int addMaintain(@Param("schema_name") String schema_name, @Param("strcode")String strcode ,@Param("gather_time")String gather_time,@Param("status") int status);
//
//    @Select("select ${schema_name}.real_time_synchronize_asset_monitor_current() ")
//    int findNeedDealAssetMonitorListRealTime(@Param("schema_name") String schema_name);
//
//    //查询13种设备的状态效率（建议使用缓存机制）
//    @Select("select egin.asset_code, sum(case when egin.nape='1' then result_data end) as result_data1,  \n" +
//            "sum(case when egin.nape='2' then result_data end) as result_data2,  \n" +
//            "sum(case when egin.nape='3' then result_data end) as result_data3,\n" +
//            "sum(case when egin.nape='4' then result_data end) as result_data4,\n" +
//            "sum(case when egin.nape='5' then result_data end) as result_data5,\n" +
//            "to_char(now() - INTERVAL '1 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' ) as month \n" +
//            "from ${schema_name}._sc_statistics_engine as egin  \n" +
//            "where substring(egin.month from 1 for 7) = to_char(now() - INTERVAL '1 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' )\n" +
//            "group by egin.asset_code \n" +
//            "UNION\n" +
//            "select egin.asset_code, sum(case when egin.nape='1' then result_data end) as result_data1,  \n" +
//            "sum(case when egin.nape='2' then result_data end) as result_data2,  \n" +
//            "sum(case when egin.nape='3' then result_data end) as result_data3,\n" +
//            "sum(case when egin.nape='4' then result_data end) as result_data4,\n" +
//            "sum(case when egin.nape='5' then result_data end) as result_data5,\n" +
//            "to_char(now() - INTERVAL '2 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' ) as month \n" +
//            "from ${schema_name}._sc_statistics_engine as egin  \n" +
//            "where substring(egin.month from 1 for 7) = to_char(now() - INTERVAL '2 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' )\n" +
//            "group by egin.asset_code \n" +
//            "UNION\n" +
//            "select egin.asset_code, sum(case when egin.nape='1' then result_data end) as result_data1,  \n" +
//            "sum(case when egin.nape='2' then result_data end) as result_data2,  \n" +
//            "sum(case when egin.nape='3' then result_data end) as result_data3,\n" +
//            "sum(case when egin.nape='4' then result_data end) as result_data4,\n" +
//            "sum(case when egin.nape='5' then result_data end) as result_data5,\n" +
//            "to_char(now() - INTERVAL '0 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' ) as month \n" +
//            "from ${schema_name}._sc_statistics_engine as egin  \n" +
//            "where substring(egin.month from 1 for 7) = to_char(now() - INTERVAL '0 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' )\n" +
//            "group by egin.asset_code ")
//    public List<BroseThreeDataModel> getAllAssetThreeMonth(@Param("schema_name") String schema_name);
//
//    //设备监控地图展示6月占比和统计结果数据查询(建议使用缓存机制)
//    @Select("select foo.nape as nape , sum(foo.result_data) as result_data ,to_char(now() - INTERVAL '${i} MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') as month from (SELECT " +
//            "egin.asset_code, " +
//            "egin.nape, " +
//            "SUM (result_data) AS result_data, " +
//            "to_char( now() - INTERVAL '0 MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' ) AS MONTH " +
//            "FROM " +
//            "${schema_name}._sc_statistics_engine AS egin " +
//            "WHERE egin.asset_code<>'P-5-501' and " +                    //统计分析屏蔽掉P-5-501统计结果
//            "SUBSTRING (egin. MONTH FROM 1 FOR 7) = to_char(now() - INTERVAL '${i} MONTH', '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "' ) " +
//            "GROUP BY " +
//            "egin.asset_code, " +
//            "egin.nape) as foo GROUP BY foo.nape order by foo.nape asc")
//    public List<BroseStatusRateMonthModel> getAllAssetSixMonth(@Param("schema_name") String schema_name,@Param("i") int month);
}
