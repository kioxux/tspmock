package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 任务模板
 */
@Mapper
public interface TaskTemplateMapper {
    /**
     * 批量新增或更新任务模板
     *
     * @param schemaName 数据库
     * @param tempList   任务模板信息
     */
    @Insert({"<script>",
            "<foreach collection='tempList' item='pm' index='index' separator=';'>",
            "INSERT INTO ${schema_name}._sc_task_template(task_template_code,task_template_name,remark,is_use,create_time,create_user_id) ",
            "VALUES (#{pm.task_template_code},#{pm.task_template_name},#{pm.remark},#{pm.is_use},current_timestamp,#{pm.create_user_id}) ",
            "ON CONFLICT (task_template_code) ",
            "DO UPDATE SET task_template_name=#{pm.task_template_name},remark=#{pm.remark},is_use=#{pm.is_use} ",
            "</foreach>",
            "</script>"})
    void insertOrUpdateTaskTemp(@Param("schema_name") String schemaName, @Param("tempList") List<Map<String, Object>> tempList);

    /**
     * 新增任务模板
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_task_template" +
            "(task_template_code,task_template_name,remark,is_use,create_time,create_user_id) values " +
            "(#{pm.task_template_code},#{pm.task_template_name},#{pm.remark},#{pm.is_use},current_timestamp,#{pm.create_user_id})")
    void insertTaskTemplate(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 编辑任务模板
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script> " +
            " UPDATE ${schema_name}._sc_task_template " +
            " SET task_template_code = #{pm.task_template_code} " +
            "  <when test='pm.task_template_name!=null'>" +
            " ,task_template_name=#{pm.task_template_name}" +
            "  </when>" +
            "  <when test='pm.remark!=null'>" +
            " ,remark=#{pm.remark}" +
            "  </when>" +
            "  <when test='pm.is_use!=null'>" +
            " ,is_use=#{pm.is_use}" +
            "  </when>" +
            " WHERE task_template_code=#{pm.task_template_code}" +
            " </script>")
    void updateTaskTemplate(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除任务模板
     *
     * @param schema_name         数据库
     * @param task_template_codes 任务项模板编号
     */
    @Delete("<script>DELETE FROM ${schema_name}._sc_task_template WHERE task_template_code in " +
            "  <foreach collection='task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach></script>")
    int deleteTaskTemplateByCodes(@Param("schema_name") String schema_name, @Param("task_template_codes") String[] task_template_codes);

    /**
     * 获取任务模板详情
     *
     * @param schema_name        入参
     * @param task_template_code 入参
     */
    @Select(" SELECT task_template_code,task_template_name,remark,is_use,create_time,create_user_id" +
            " FROM ${schema_name}._sc_task_template WHERE task_template_code=#{task_template_code}")
    Map<String, Object> findTaskTemplateByCode(@Param("schema_name") String schema_name, @Param("task_template_code") String task_template_code);

    /**
     * 分页查询任务模板数量
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Select(" <script>" +
            " SELECT COUNT(1) FROM ${schema_name}._sc_task_template " +
            " <where> " +
            " <when test='pm.keywordSearch!=null'> " +
            " and (task_template_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR task_template_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " <when test='pm.isUseSearch!=null'> " +
            " AND is_use  =#{pm.isUseSearch}" +
            " </when> " +
            " <when test='pm.task_template_codes!=null'> " +
            "   AND task_template_code in " +
            "  <foreach collection='pm.task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach>" +
            " </when> " +
            " </where>  " +
            " </script>")
    int findTaskTempLatePageCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 分页查询任务模板列表
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Select(" <script>" +
            " SELECT t.task_template_code,t.task_template_name,t.remark,t.is_use,t.create_time,t.create_user_id," +
            " (SELECT COUNT(1) FROM ${schema_name}._sc_task_template_item WHERE task_template_code = t.task_template_code ) AS itemNum" +
            " FROM ${schema_name}._sc_task_template t " +
            " <where> " +
            " <when test='pm.keywordSearch!=null'> " +
            " and (lower(t.task_template_code) LIKE  CONCAT('%', lower(#{pm.keywordSearch}), '%') " +
            " OR lower(t.task_template_name) LIKE  CONCAT('%', lower(#{pm.keywordSearch}), '%')) " +
            " </when> " +
            " <when test='pm.isUseSearch!=null'> " +
            " AND t.is_use  = #{pm.isUseSearch}" +
            " </when> " +
            " </where> ORDER BY  t.task_template_code ASC ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findTaskTempLatePage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增任务模板任务项
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_task_template_item" +
            "(task_template_code,task_item_code,group_name,data_order) values " +
            "(#{pm.task_template_code},#{pm.task_item_code},#{pm.group_name},( " +
            "   select CASE WHEN MAX(data_order) IS NULL THEN 0 ELSE MAX(data_order)  END + 1 " +
            "   from ${schema_name}._sc_task_template_item " +
            "   where task_template_code = #{pm.task_template_code} " +
            "))")
    void insertTaskTempLateItem(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除关联任务项
     *
     * @param schema_name         数据库
     * @param task_template_codes 任务项模板
     */
    @Delete(" <script>" +
            " DELETE FROM ${schema_name}._sc_task_template_item WHERE task_template_code in " +
            "  <foreach collection='task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach>" +
            " </script>")
    void deleteTemplateTaskByTempCodes(@Param("schema_name") String schema_name, @Param("task_template_codes") String[] task_template_codes);

    /**
     * 删除关联任务项
     *
     * @param schema_name 数据库
     * @param task_ids    模板任务项关联主键
     */
    @Delete(" <script>" +
            " DELETE FROM ${schema_name}._sc_task_template_item WHERE task_id in " +
            "  <foreach collection='task_ids' item='task_id' open='(' close=')' separator=','> #{task_id} </foreach>" +
            " </script>")
    void deleteTemplateTaskByIds(@Param("schema_name") String schema_name, @Param("task_ids") String[] task_ids);

    /**
     * 关联任务项设置组名称
     *
     * @param schema_name 数据库
     * @param task_ids    模板任务项关联主键
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_task_template_item SET group_name = #{group_name}" +
            " WHERE task_id IN " +
            "  <foreach collection='task_ids' item='task_id' open='(' close=')' separator=','> #{task_id} </foreach>" +
            " </script>")
    void updateTaskTempLateItem(@Param("schema_name") String schema_name, @Param("task_ids") String[] task_ids, @Param("group_name") String group_name);

    /**
     * 获取关联任务项（用于换顺序）
     *
     * @param schema_name 数据库
     * @param task_id     模板任务项关联主键
     */
    @Select(" SELECT task_template_code, data_order FROM ${schema_name}._sc_task_template_item WHERE task_id = #{task_id} ")
    Map<String, Object> findTemplateItem(@Param("schema_name") String schema_name, @Param("task_id") String task_id);

    /**
     * 获取关联任务项上一条记录
     *
     * @param schema_name 数据库
     * @param task_id     模板任务项关联主键
     */
    @Select(" SELECT T.task_id, T.data_order FROM ${schema_name}._sc_task_template_item T " +
            " INNER JOIN ${schema_name}._sc_task_template_item T2 on T.task_template_code = T2.task_template_code AND T2.task_id = #{task_id}" +
            " WHERE T.data_order < T2.data_order ORDER BY T.data_order DESC LIMIT 1 ")
    Map<String, Object> findPreviousTemplateItem(@Param("schema_name") String schema_name, @Param("task_id") String task_id);

    /**
     * 获取关联任务项下一条记录
     *
     * @param schema_name 数据库
     * @param task_id     模板任务项关联主键
     */
    @Select(" SELECT T.task_id, T.data_order FROM ${schema_name}._sc_task_template_item T " +
            " INNER JOIN ${schema_name}._sc_task_template_item T2 on T.task_template_code = T2.task_template_code AND T2.task_id = #{task_id}" +
            " WHERE T.data_order > T2.data_order ORDER BY T.data_order ASC LIMIT 1 ")
    Map<String, Object> findNextTemplateItem(@Param("schema_name") String schema_name, @Param("task_id") String task_id);

    /**
     * 更新模板任务项排序
     *
     * @param schema_name 入参
     * @param task_id     模板任务项关联主键
     * @param data_order  顺序
     */
    @Update(" UPDATE ${schema_name}._sc_task_template_item SET data_order = #{data_order} WHERE task_id =#{task_id} ")
    void updateTemplateItemOrder(@Param("schema_name") String schema_name, @Param("task_id") String task_id, @Param("data_order") Integer data_order);

    /**
     * 获取模板的任务项列表
     *
     * @param schema_name        入参
     * @param task_template_code 入参
     * @param keywordSearch      入参
     * @return 模板的任务项列表
     */
    @Select(" <script>" +
            " SELECT ti.requirements,tti.task_id,tti.group_name,tti.task_template_code,tti.task_item_code,tti.data_order,ti.result_type,ti.result_verification,ti.task_item_name" +
            " FROM ${schema_name}._sc_task_template_item AS tti" +
            " LEFT JOIN ${schema_name}._sc_task_item AS ti ON tti.task_item_code = ti.task_item_code" +
            " WHERE tti.task_template_code = #{task_template_code}" +
            " <when test='keywordSearch!=null'> " +
            " AND ti.task_item_name LIKE CONCAT('%', #{keywordSearch}, '%') " +
            " </when> ORDER BY tti.data_order ASC, tti.group_name ASC ,tti.task_item_code ASC " +
            " </script>")
    List<Map<String, Object>> findTemplateItemList(@Param("schema_name") String schema_name, @Param("task_template_code") String task_template_code, @Param("keywordSearch") String keywordSearch);

    /**
     * 任务模板列表
     *
     * @param schema_name   入参
     * @param keywordSearch 入参
     * @return 任务模板列表
     */
    @Select(" <script>" +
            " SELECT t.task_template_code,t.task_template_name,t.remark,t.is_use,t.create_time,t.create_user_id" +
            " FROM ${schema_name}._sc_task_template t " +
            " <where> " +
            " <when test='keywordSearch!=null'> " +
            " and (t.task_template_code LIKE  CONCAT('%', #{keywordSearch}, '%') " +
            " OR t.task_template_name LIKE  CONCAT('%', #{keywordSearch}, '%')) " +
            " </when> " +
            " </where> ORDER BY  t.task_template_code ASC " +
            " </script>")
    List<Map<String, Object>> findTaskTempLateList(@Param("schema_name") String schema_name, @Param("keywordSearch") String keywordSearch);

    /**
     * 根据任务项编码列表获取任务项名称列表
     *
     * @param schema_name     入参
     * @param task_item_codes 入参
     * @return 任务项名称列表
     */
    @Select(" <script>" +
            " SELECT task_item_name FROM  ${schema_name}._sc_task_item WHERE task_item_code IN " +
            "  <foreach collection='task_item_codes' item='task_item_code' open='(' close=')' separator=','> #{task_item_code} </foreach>" +
            " </script>")
    List<String> findTaskItemNames(@Param("schema_name") String schema_name, @Param("task_item_codes") String[] task_item_codes);

    /**
     * 根据任务项关联主键获取任务项名称列表
     *
     * @param schema_name 数据库
     * @param task_ids    任务模板任务项关联主键
     * @return 任务项名称列表
     */
    @Select(" <script>" +
            " SELECT t.group_name || '-' || TI.task_item_name as name " +
            "  FROM ${schema_name}._sc_task_template_item T " +
            "  LEFT JOIN ${schema_name}._sc_task_item TI ON T.task_item_code = TI.task_item_code " +
            " WHERE T.TASK_ID IN " +
            "  <foreach collection='task_ids' item='task_id' open='(' close=')' separator=','> #{task_id} </foreach>" +
            " order by t.group_name, TI.task_item_name </script>")
    List<String> findTaskItemNamesByIds(@Param("schema_name") String schema_name, @Param("task_ids") String[] task_ids);

    /**
     * 根据任务项关联主键获取任务项名称
     *
     * @param schema_name 数据库
     * @param task_id     任务模板任务项关联主键
     * @return 任务项名称
     */
    @Select(" SELECT t.group_name || '-' || TI.task_item_name as name " +
            "  FROM ${schema_name}._sc_task_template_item T " +
            "  LEFT JOIN ${schema_name}._sc_task_item TI ON T.task_item_code = TI.task_item_code " +
            " WHERE T.TASK_ID = #{task_id}")
    String findTaskItemNameById(@Param("schema_name") String schema_name, @Param("task_id") String task_id);

    /**
     * 根据任务项关联主键获取任务模板编号列表
     *
     * @param schema_name 数据库
     * @param task_ids    任务模板任务项关联主键
     * @return 任务模板编号列表
     */
    @Select(" <script>" +
            " SELECT task_template_code FROM ${schema_name}._sc_task_template_item T " +
            " WHERE T.TASK_ID IN " +
            "  <foreach collection='task_ids' item='task_id' open='(' close=')' separator=','> #{task_id} </foreach> limit 1 " +
            " </script>")
    String findTaskTempCodeByIds(@Param("schema_name") String schema_name, @Param("task_ids") String[] task_ids);

    @Select("SELECT count(1) " +
            "FROM ${schema_name}._sc_task_template_item tti " +
            "LEFT JOIN ${schema_name}._sc_task_item ti ON tti.task_item_code=ti.task_item_code " +
            "${searchWord} ")
    int findCountPlanWorkTaskTemplateItemList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord);

    @Select("SELECT tti.task_id,tti.task_template_code,tti.task_item_code,tti.group_name,ti.task_item_name,ti.requirements,ti.result_type " +
            "FROM ${schema_name}._sc_task_template_item tti " +
            "LEFT JOIN ${schema_name}._sc_task_item ti ON tti.task_item_code=ti.task_item_code " +
            "${searchWord} ORDER BY tti.group_name,tti.data_order")
    List<Map<String, Object>> findPlanWorkTaskTemplateItemList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord);

    /**
     * 查询已存在的任务模板、任务项、组信息
     *
     * @param schemaName         数据库
     * @param task_template_code 任务模板
     * @param task_item_codes    任务项
     * @param group_name         组
     * @return 已存在信息
     */
    @Select("<script>SELECT string_agg(t.group_name || '-' || i.task_item_name , ',') as name FROM ${schema_name}._sc_task_template_item t " +
            "LEFT JOIN ${schema_name}._sc_task_item i on t.task_item_code = i.task_item_code " +
            "where t.task_template_code = #{task_template_code} and t.group_name = #{group_name} and t.task_item_code in " +
            "<foreach collection='task_item_codes' item='task_item_code' open='(' close=')' separator=','>#{task_item_code}</foreach> " +
            "</script> ")
    String findExistTaskTigInfo(@Param("schema_name") String schemaName, @Param("task_template_code") String task_template_code,
                                @Param("task_item_codes") String[] task_item_codes, @Param("group_name") String group_name);

    /**
     * 查询已存在的任务模板、任务项、组信息
     *
     * @param schemaName 数据库
     * @param task_ids   模板任务项关联主键
     * @param group_name 组
     * @return 已存在信息
     */
    @Select("<script>SELECT string_agg(t.group_name || '-' || i.task_item_name , ',') as name FROM ${schema_name}._sc_task_template_item t " +
            "inner join ${schema_name}._sc_task_template_item t1 on t.task_template_code = t1.task_template_code and t.task_item_code = t1.task_item_code and t1.task_id in " +
            "<foreach collection='task_ids' item='task_id' open='(' close=')' separator=','>#{task_id}</foreach> " +
            "LEFT JOIN ${schema_name}._sc_task_item i on t.task_item_code = i.task_item_code " +
            "where t.group_name = #{group_name} " +
            "</script> ")
    String findExistTaskTigInfoByIds(@Param("schema_name") String schemaName, @Param("task_ids") String[] task_ids, @Param("group_name") String group_name);

    /**
     * 验证任务模板编号是否存在
     *
     * @param schema_name      数据库
     * @param templateCodeList 任务项模板编号
     * @return 模板编号集合
     */
    @Select(" <script>SELECT distinct task_template_code FROM ${schema_name}._sc_task_template WHERE task_template_code in " +
            "  <foreach collection='task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach></script>")
    List<String> findTaskTemplateCodesByCodes(@Param("schema_name") String schema_name, @Param("task_template_codes") List<String> templateCodeList);

    /**
     * 根据编号获取任务模板信息
     *
     * @param schema_name      数据库
     * @param templateCodeList 任务模板编号
     * @return 模板信息集合
     */
    @Select(" <script>SELECT distinct task_template_code,task_template_name,remark,is_use FROM ${schema_name}._sc_task_template WHERE task_template_code in " +
            " <foreach collection='task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach></script>")
    @MapKey("task_template_code")
    Map<String, Map<String, Object>> findTaskTemplatesByCodes(@Param("schema_name") String schema_name, @Param("task_template_codes") List<String> templateCodeList);

    /**
     * 批量验证任务模板、任务项、组是否存在
     *
     * @param schema_name 数据库
     * @param codeList    编号信息
     * @return 编号集合
     */
    @Select(" <script>SELECT distinct task_template_code || '-' || group_name || '-' || task_item_code " +
            "   FROM ${schema_name}._sc_task_template_item WHERE task_template_code || '-' || group_name || '-' || task_item_code in " +
            "  <foreach collection='codes' item='code' open='(' close=')' separator=','> #{code} </foreach></script>")
    List<String> findTaskTemplateItemByInfo(@Param("schema_name") String schema_name, @Param("codes") List<String> codeList);

    /**
     * 批量关联任务模板、任务项、组
     *
     * @param schemaName 数据库
     * @param midList    信息
     */
    @Insert({"<script>" +
            "<foreach collection='midList' item='pm' index='index' separator=';'>" +
            "INSERT INTO ${schema_name}._sc_task_template_item" +
            "(task_template_code,task_item_code,group_name,data_order) values " +
            "(#{pm.task_template_code},#{pm.task_item_code},#{pm.group_name},( " +
            "   select CASE WHEN MAX(data_order) IS NULL THEN 0 ELSE MAX(data_order)  END + 1 " +
            "   from ${schema_name}._sc_task_template_item where task_template_code = #{pm.task_template_code} " +
            "))" +
            "</foreach>" +
            "</script>"})
    void insertOrUpdateTaskTempItem(@Param("schema_name") String schemaName, @Param("midList") List<Map<String, Object>> midList);

    /**
     * 查询任务模板、任务项、组信息列表
     *
     * @param schemaName 数据库
     * @param pm         页面参数
     * @return 信息列表
     */
    @Select("<script>SELECT data_order,m.task_item_code,ti.task_item_name,ti.result_type,ti.requirements,m.group_name,t.task_template_code,t.task_template_name,t.is_use,t.remark " +
            "FROM ${schema_name}._sc_task_template t LEFT JOIN ${schema_name}._sc_task_template_item m ON t.task_template_code = m.task_template_code " +
            "LEFT JOIN ${schema_name}._sc_task_item ti ON m.task_item_code=ti.task_item_code " +
            " <where> " +
            " <when test='pm.keywordSearch!=null'> " +
            " and (t.task_template_code LIKE CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR t.task_template_name LIKE CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " <when test='pm.isUseSearch!=null'> " +
            " AND t.is_use = #{pm.isUseSearch}" +
            " </when> " +
            " <when test='pm.task_template_codes!=null'> " +
            "   AND t.task_template_code in " +
            "  <foreach collection='pm.task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach>" +
            " </when> " +
            " </where>  " +
            " ORDER BY task_template_code,group_name,data_order,task_item_code</script>")
    List<Map<String, Object>> findTaskTempGroupItemList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);
}
