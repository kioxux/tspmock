package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 备件型号
 * User: sps
 * Date: 2019/04/02
 * Time: 下午15:00
 */
@Mapper
public interface BomTampLateMapper {
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = BomTampLateMapper.BomTampLateMapperProvider.class, method = "queryBomTampLateList")
//    List<Map<String, Object>> queryBomTampLateList(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                                   @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                                   @Param("searchKey") String searchKey);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = BomTampLateMapper.BomTampLateMapperProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey);
//
//    /**
//     * 查重
//     *
//     * @param schemaName
//     * @param bomModel
//     * @return
//     */
//    @Select("select id from ${schema_name}._sc_bom_template where bom_model=#{bom_model}")
//    Integer selectByBomModel(@Param("schema_name") String schemaName, @Param("bom_model") String bomModel);
//
//    /**
//     * 按id查询查重
//     *
//     * @param schemaName
//     * @param bom_model
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_template where bom_model=#{bom_model}")
//    Map<String,Object> selectByBomID(@Param("schema_name") String schemaName, @Param("bom_model") String bom_model);
//
//
//    /**
//     * 新增类型文档
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @InsertProvider(type = BomTampLateMapper.BomTampLateMapperProvider.class, method = "insert_model_doc")
//    int insert_model_doc(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 查询关联文档列表
//     *
//     * @param schemaName
//     * @param model_id
//     * @return
//     */
//    @SelectProvider(type = BomTampLateMapper.BomTampLateMapperProvider.class, method = "query_doc")
//    List<Map<String, Object>> query_doc(@Param("schema_name") String schemaName,
//                                        @Param("model_id") String model_id);
//
//
//    /**
//     * 新增数据
//     *
//     * @param
//     * @param paramMap
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_bom_template ( " +
//            "bom_name,bom_model,type_id,unit_id,icon,alternate,specification,create_time,create_user_account) values  " +
//            " (#{bom_name},#{bom_model},#{type_id}::int,#{unit_id}::int,#{icon},#{alternate},#{specification},current_timestamp,#{create_user_account}) RETURNING id")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int insert(Map<String, Object> paramMap);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom_template set " +
//            "bom_name=#{s.bom_name}, " +
//            "type_id=#{s.type_id}::int, " +
//            "unit_id=#{s.unit_id}::int, " +
//            "alternate=#{s.alternate}, " +
//            "specification=#{s.specification}, " +
//            "icon=#{s.icon}  " +
//            "where id=#{s.id}::int ")
//    int update(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Update("DELETE FROM ${schema_name}._sc_bom_template where id=#{id} ")
//    int delete(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//    /**
//     * 更新doc数据
//     *
//     * @param schemaName
//     * @param file_ids
//     * @param model_id
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom_template_doc set " +
//            "model_id=#{model_id}  " +
//            "where file_id in ${file_ids} ")
//    int update_doc(@Param("schema_name") String schemaName, @Param("file_ids") String file_ids, @Param("model_id") int model_id);
//
//    /**
//     * 删除文档数据
//     *
//     * @param schemaName
//     * @param
//     * @param file_id
//     * @return
//     */
//    @Update("DELETE FROM ${schema_name}._sc_bom_template_doc where file_id=#{file_id}  ")
//    int delete_model_doc(@Param("schema_name") String schemaName, @Param("file_id") Integer file_id);
//
//
//    /**
//     * 更新备件名称
//     *
//     * @param schema_name
//     * @param oldId
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom b set bom_name = " +
//            "(select bom_name from ${schema_name}._sc_bom_template t " +
//            "where t.id=#{id}::int and b.bom_model = t.bom_model) " +
//            "where exists (select 1 from ${schema_name}._sc_bom_template t where t.id=#{id}::int and b.bom_model = t.bom_model)")
//    int updateBomNameByModel(@Param("schema_name") String schema_name, @Param("id") String oldId);
//
//
//    /**
//     * 实现类
//     */
//    class BomTampLateMapperProvider {
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param searchKey
//         * @return
//         */
//        public String queryBomTampLateList(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                           @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                           @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT(" a.*,b.type_name,c.unit_name ");
//                FROM(schemaName + "._sc_bom_template a  " +
//                        "LEFT JOIN " + schemaName + "._sc_bom_type b ON b.id=a.type_id " +
//                        "LEFT JOIN " + schemaName + "._sc_unit c ON c.id=a.unit_id ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("upper(a.bom_name) like upper('%" + searchKey
//                            + "%') or upper(a.bom_model) like upper('%" + searchKey
//                            + "%') or upper(b.type_name) like upper('%" + searchKey + "%') ");
//                }
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_bom_template a  " +
//                        "LEFT JOIN " + schemaName + "._sc_bom_type b ON b.id=a.type_id " +
//                        "LEFT JOIN " + schemaName + "._sc_unit c ON c.id=a.unit_id ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("upper(a.bom_name) like upper('%" + searchKey
//                            + "%') or upper(a.bom_model) like upper('%" + searchKey
//                            + "%') or upper(b.type_name) like upper('%" + searchKey + "%') ");
//                }
//            }}.toString();
//        }
//
//        /**
//         * 新增doc数据
//         *
//         * @param schemaName
//         * @param paramMap
//         * @return
//         */
//        public String insert_model_doc(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_bom_template_doc");
//                VALUES("model_id ", "#{s.model_id}::int");
//                VALUES("file_id ", "#{s.file_id}::int");
//                VALUES("remark ", "#{s.remark}");
//                VALUES("create_user_account ", "#{s.create_user_account}");
//                VALUES("create_time ", "current_timestamp");
//                VALUES("file_name ", "0");
//                VALUES("file_type ", "0 ");
//            }}.toString();
//        }
//
//        /**
//         * 查询doc列表
//         *
//         * @param schemaName
//         * @param model_id
//         * @return
//         */
//        public String query_doc(@Param("schema_name") String schemaName,
//                                @Param("model_id") String model_id) {
//            return new SQL() {{
//                SELECT(" a.create_time,a.file_id as id,a.file_type,a.remark,b.file_original_name as file_name,c.username as create_user_account ");
//                FROM(schemaName + "._sc_bom_template_doc a " +
//                        "LEFT JOIN " + schemaName + "._sc_files b ON b.id=a.file_id  " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                if (null != model_id && !"".equals(model_id)) {
//                    WHERE("a.model_id=" + Integer.parseInt(model_id));
//                }
//            }}.toString();
//        }
//
//    }

}
