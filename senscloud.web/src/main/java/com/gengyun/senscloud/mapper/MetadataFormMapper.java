package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 *   表单模板
 *   User: sps
 *   Date: 2018/11/13
 *   Time: 下午14:33
 */
@Mapper
public interface MetadataFormMapper {
//    @InsertProvider(type = MetadataFormMapper.MetadataFormMapperProvider.class, method = "insert")
//    void insert(@Param("schema_name") String schema_name, @Param("s") MetadataForm metadataForm);
//
//    class MetadataFormMapperProvider {
//        public String insert(@Param("schema_name") String schema_name, @Param("s") MetadataForm metadataForm) {
//            return new SQL() {{
//                INSERT_INTO(schema_name + "._sc_work_template");
//                VALUES("id", "#{s.id}");
//            }}.toString();
//        }
//    }
}
