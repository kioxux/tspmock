package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkListDetailMapper {
//
//
//    //查找分配表单
//    @Select("select w.*,c.username as receive_name,s.status as status_name," +
//            " t.type_name as type_name,t.business_type_id as business_type_id,dv.strname as asset_name" +
//            " from ${schema_name}._sc_works_detail w " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
//            " left join ${schema_name}._sc_user c on w.receive_account=c.account " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            "where w.status=40 ${condition} " +
//            "group by w.work_code,c.username,s.status,t.type_name,dv.strname,w.sub_work_code,t.business_type_id "
//    )
//    List<WorkListDetailModel> getWorkListDetailList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    @Select("Select DISTINCT s.work_code as work_code_bak ,t.business_type_id as business_type_id, " +
//            "w.receive_account,w.receive_time," +
//            "s.* " +
//            "from ${schema_name}._sc_works s  " +
//            "left join ${schema_name}._sc_work_type t on s.work_type_id=t.id  " +
//            "left join ${schema_name}._sc_works_detail w on w.work_code = s.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org ao on ao.sub_work_code = w.sub_work_code " +
//            " left join ${schema_name}._sc_user c on w.receive_account=c.account " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            "where w.status=40 ${condition}")
//    List<WorkListModel> getWorkListByReviceTime(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
////    @Select("Select d.* ,t.flow_template_code as flow_code,w.facility_id as facility_id, w.occur_time as occur_time, w.deadline_time as deadline_time,w.title as works_title, u.mobile as phone,t.business_type_id as business_type_id ,t.type_name as work_type_name,w.create_user_account as account_name from ${schema_name}._sc_works_detail as d " +
////            "left join ${schema_name}._sc_works as w on d.work_code = w.work_code " +
////            "left join ${schema_name}._sc_user as u on u.account = w.create_user_account " +
////            "left join ${schema_name}._sc_work_type t on t.id = d.work_type_id " +
////            "where d.status=40 and d.work_code = #{work_code}")
//
//    @Select("select w.sub_work_code,w.work_code,w.work_type_id,w.work_template_code,w.relation_type,w.relation_id," +
//            "w.title,w.problem_note,w.problem_img,w.asset_running_status,w.fault_type,w.repair_type,w.priority_level," +
//            "w.before_img,w.result_note,w.after_img,w.file_ids,w.status,w.remark," +
//            "w.from_code,w.waiting,w.bom_app_result,w.receive_account,w.receive_time,w.distribute_account," +
//            "w.distribute_time,w.plan_arrive_time,w.begin_time,w.finished_time,w.audit_time,w.fault_number,w.is_main," +
//            "w.service_score,w.repair_code,w.is_finished,w.finished_id, " +
//            " c.username as receive_name,s.status as status_name," +
//            " t.type_name as type_name,t.business_type_id as business_type_id,dv.strname as asset_name " +
//            " from ${schema_name}._sc_works_detail w " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
//            " left join ${schema_name}._sc_user c on w.receive_account=c.account " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            "where w.status=40 and w.work_code = #{work_code} ")
//    public List<WorkListDetailModel> selectWorkListsDetail(@Param("schema_name") String schema_name, @Param("work_code") String work_code);//, @Param("descStatus") String descStatus


}