package com.gengyun.senscloud.mapper;

/**
 * 备件领用
 */
public interface BomRecipientMapper {
//
//    //备件领用
//    @Insert("insert into ${schema_name}._sc_bom_recipient (recipient_code,stock_code,file_ids,status,remark,create_time,create_user_account,client_org_id,body_property,use_to)" +
//            " values (#{bom.recipient_code},#{bom.stock_code},#{bom.file_ids},#{bom.status}::int,#{bom.remark},#{bom.create_time}::timestamp,#{bom.create_user_account},#{bom.client_org_id}::int,#{bom.body_property}::jsonb,#{bom.use_to})")
//    int insert(@Param("schema_name") String schema_name, @Param("bom") Map<String, Object> businessInfo);
//
//    //退回订单重新编辑
//    @Update("update ${schema_name}._sc_bom_recipient set " +
//            "stock_code=#{bom.stock_code}," +
//            "file_ids=#{bom.file_ids}," +
//            "status=#{bom.status}::int," +
//            "remark=#{bom.remark}," +
//            "client_org_id=#{bom.client_org_id}::int ," +
//            "body_property=#{bom.body_property}::jsonb ," +
//            "use_to=#{bom.use_to} " +
//            "where recipient_code=#{bom.recipient_code}")
//    int update(@Param("schema_name") String schema_name, @Param("bom") Map<String, Object> businessInfo);
//
//    /**
//     * 功能：删除备件领用编号关联备件详情信息，重新修改
//     *
//     * @param schema_name
//     * @param recipient_code
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_bom_recipient_detail where recipient_code=#{recipient_code}")
//    int deleRecipientDetail(@Param("schema_name") String schema_name, @Param("recipient_code") String recipient_code);
//
//    /**
//     * 查找备件领用列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("select DISTINCT re.recipient_code, re.use_to as use_to_name,s.stock_name,sta.status as status_name , u.username, re.status,brt.quantity_total,  " +
//            "re.stock_code, re.client_org_id, re.create_time " +
//            "from ${schema_name}._sc_bom_recipient as re " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON re.stock_code = bs.stock_code  " +
//            "left JOIN (" +
//            "select distinct st.stock_code,st.stock_name from ${schema_name}._sc_stock st " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org on org.stock_code=st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg on sg.stock_code=st.stock_code " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            ") s on bs.stock_code = s.stock_code " +
//            "LEFT JOIN ( SELECT recipient_code,sum(quantity) as quantity_total " +
//            "FROM ${schema_name}._sc_bom_recipient_detail group by recipient_code" +
//            ") brt ON brt.recipient_code = re.recipient_code " +
//            "left join ${schema_name}._sc_status as sta on sta.id=re.status " +
//            "left join ${schema_name}._sc_user as u on u.account=re.create_user_account " +
//            "where 1=1 ${condition} and s.stock_code is not null " +
//            "order by re.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getBomRecipientList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 备件领用列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(DISTINCT re.recipient_code) from ${schema_name}._sc_bom_recipient as re " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON re.stock_code = bs.stock_code  " +
//            "left JOIN (" +
//            "select distinct st.stock_code,st.stock_name from ${schema_name}._sc_stock st " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org on org.stock_code=st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg on sg.stock_code=st.stock_code " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            ") s on bs.stock_code = s.stock_code " +
//            "left join ${schema_name}._sc_status as sta on sta.id=re.status " +
//            "left join ${schema_name}._sc_user as u on u.account=re.create_user_account " +
//            "where 1=1 ${condition} and s.stock_code is not null ")
//    int countBomRecipientList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition);
//
//    /**
//     * 根据主键查询备件领用数据
//     *
//     * @param schemaName
//     * @param recipientCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_recipient w " +
//            "where w.recipient_code = #{recipientCode} ")
//    Map<String, Object> queryBomRecipientById(@Param("schema_name") String schemaName, @Param("recipientCode") String recipientCode);
//
//    @Select("select * from ${schema_name}._sc_bom_recipient_detail d " +
//            "where d.recipient_code = #{recipientCode} ")
//    List<Map<String, Object>> getAllBomRecipientDetail(@Param("schema_name") String schemaName, @Param("recipientCode") String recipientCode);
//
//    @Update("update ${schema_name}._sc_bom_recipient set file_ids=#{file_ids},status = #{status},body_property = #{body_property}::jsonb where recipient_code = #{recipient_code}")
//    int updateBomRecipientStatus(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids, @Param("status") int status, @Param("body_property") String body_property, @Param("recipient_code") String recipient_code);
//
//    @Update("update ${schema_name}._sc_bom_recipient set file_ids=#{file_ids}, body_property = #{body_property}::jsonb where recipient_code = #{recipient_code}")
//    int updateBomRecipientProperty(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids, @Param("body_property") String body_property, @Param("recipient_code") String recipient_code);
//
//    /**
//     * @param schema_name
//     * @param quantity
//     * @param recipient_code
//     * @return
//     * @Description:更新备件领用数量
//     */
//    @Update("update ${schema_name}._sc_bom_recipient set quantity=#{quantity}::int where recipient_code = #{recipient_code}")
//    int updateQuantity(@Param("schema_name") String schema_name, @Param("quantity") int quantity, @Param("recipient_code") String recipient_code);
//
//    @Update("update ${schema_name}._sc_bom_recipient set status = #{status} where recipient_code = #{recipient_code}")
//    int updateStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("recipient_code") String recipient_code);
}
