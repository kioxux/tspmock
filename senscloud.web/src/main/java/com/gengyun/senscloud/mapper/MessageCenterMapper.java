package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface MessageCenterMapper {

    /**
     * 插入消息历史表
     *
     * @param schema_name
     * @param paramMap
     * @return
     */
    @Update("INSERT INTO ${schema_name}._sc_message_center(receive_account,msg_content,receive_mobile,send_time,business_type,business_no," +
            "send_account,is_send,is_sms,is_email,is_pc,is_app,is_weixin,process_id,rule_id) " +
            "VALUES (#{s.receive_account},#{s.msg_content},#{s.receive_mobile},current_timestamp,#{s.business_type},#{s.business_no}" +
            ",#{s.send_account},#{s.is_send},#{s.is_sms},#{s.is_email},#{s.is_pc},#{s.is_app},#{s.is_weixin},#{s.process_id},#{s.rule_id}) ")
    void insertHistoryMessageColumn(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);

    /**
     * 插入消息接受表
     *
     * @param schema_name
     * @param paramMap
     * @return
     */
    @Update("INSERT INTO ${schema_name}._sc_message_receiver(message_id,receive_account,is_main_receiver,is_read,read_time ) " +
            "VALUES (#{s.message_id},#{s.receive_account},#{s.is_main_receiver},#{s.is_read},#{s.read_time}) ")
    void insertMessageReceiverColumn(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);

    /**
     * 插入消息中心表
     *
     * @param paramMap
     * @return
     */
    @SelectKey(statement = "SELECT nextval('${schema_name}._sc_message_center_init_id_seq')  AS id ", keyProperty = "id", before = true, resultType = Long.class)
    @Update("INSERT INTO ${schema_name}._sc_message_center_init(id,msg_content,real_msg_content,createtime,business_type,business_no," +
            "is_send,is_sms,is_email,is_pc,is_app,is_weixin,process_id,rule_id) " +
            "VALUES (#{id},#{msg_content},#{real_msg_content},#{createtime},#{business_type},#{business_no}" +
            ",#{is_send},#{is_sms},#{is_email},#{is_pc},#{is_app},#{is_weixin},#{process_id},#{rule_id})  RETURNING id")
    void insertMessageCenterColumn(Map<String, Object> paramMap);

    /**
     * 查询未发送的消息
     *
     * @param schema_name
     * @return
     */
    @Select("SELECT * FROM ${schema_name}._sc_message_center_init WHERE  is_send='f' ")
    List<Map<String, Object>> queryMessages(@Param("schema_name") String schema_name);

    /**
     * 查询消息对应的接受人
     *
     * @param schema_name
     * @return
     */
    @Select("SELECT u.* FROM ${schema_name}._sc_message_receiver mr " +
            "LEFT JOIN ${schema_name}._sc_user u ON u.account=mr.receive_account " +
            "WHERE mr.message_id=#{id}")
    List<Map<String, Object>> queryReceiver(@Param("schema_name") String schema_name, @Param("id") Long id);

    /**
     * 修改消息表状态
     *
     * @param schema_name
     * @param id
     * @return
     */
    @Update("UPDATE ${schema_name}._sc_message_center_init " +
            "SET is_send='t' " +
            "WHERE id=#{id} ")
    int UpdateSendStatus(@Param("schema_name") String schema_name, @Param("id") Long id);

    /**
     * 按条件查询消息
     *
     * @param schema_name
     * @param condition
     * @param page
     * @param pageSize
     * @return
     */
    @Select("SELECT mc.*,mr.is_read,mr.id as receive_id FROM ${schema_name}._sc_message_receiver mr " +
            "LEFT JOIN ${schema_name}._sc_message_center_init mc ON mc.id=mr.message_id " +
            "WHERE 1=1 ${condition} " +
            "ORDER BY mc.createtime DESC  LIMIT ${page} offset ${pageSize}")
    List<Map<String, Object>> getMessageList(@Param("schema_name") String schema_name, @Param("condition") String condition
            , @Param("page") int page, @Param("pageSize") int pageSize);


    /**
     * 按条件查询消息数量
     *
     * @param schema_name
     * @param condition
     * @return
     */
    @Select("SELECT COUNT(mr.id) FROM ${schema_name}._sc_message_receiver mr " +
            "LEFT JOIN ${schema_name}._sc_message_center_init mc ON mc.id=mr.message_id " +
            "WHERE 1=1 ${condition} ")
    int getMessageListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);


    /**
     * 修改消息状态
     *
     * @param schema_name
     * @param id
     * @return
     */
    @Update("UPDATE ${schema_name}._sc_message_receiver " +
            "SET is_read='t', " +
            "read_time=current_timestamp " +
            "WHERE id=#{id} ")
    int UpdateReadStatus(@Param("schema_name") String schema_name, @Param("id") int id);

    /**
     * 修改该用户所有消息消息状态
     *
     * @param schema_name
     * @param account
     * @return
     */
    @Update("UPDATE ${schema_name}._sc_message_receiver " +
            "SET is_read='t', " +
            "read_time=current_timestamp " +
            "WHERE receive_account=#{account} ")
    int UpdateReadStatusByAccount(@Param("schema_name") String schema_name, @Param("account") String account);

}