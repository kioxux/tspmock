package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface SncDataImportMapper {

//    @Select("SELECT wdc.id,wdc.sub_work_code,wdc.field_form_code,wdc.field_code,wdc.field_name, " +
//            "case wdc.field_code when 'audit_opinion' then cd1.name when 'receive_user_id' then u1.user_name " +
//            "when 'solutions' then cd2.name when 'fault_reason' then cd3.name when 'deal_result' then wft.finished_name " +
//            "when 'fault_type_id' then ft.type_name when 'create_user_id' then u2.user_name when 'guarantee_status_id' then " +
//            "coalesce((select (c.resource->>cd4.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd4.name) " +
//            "when 'running_status_id' then ars.running_status when 'category_id' then ac.category_name " +
//            "when 'position_code' then ap.position_name when 'relation_id' then a.asset_code else wdc.field_value end as field_value, " +
//            "wdc.save_type,wdc.change_type,wdc.field_right,wdc.is_required,wdc.is_table_key,wdc.db_table_type,wdc.field_remark, " +
//            "wdc.field_data_base,wdc.field_view_type,wdc.field_section_type,wdc.field_validate_method,wdc.create_time, " +
//            "wdc.create_user_id,wdc.parent_id " +
//            "FROM ${schema_name}._sc_works w " +
//            "LEFT JOIN ${schema_name}._sc_works_detail wd on w.work_code = wd.work_code " +
//            "LEFT JOIN ${schema_name}._sc_works_detail_column wdc ON wd.sub_work_code = wdc.sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 on wdc.field_value = cd1.code and wdc.field_code = 'audit_opinion' and cd1.data_type = 'audit_opinion' " +
//            "LEFT JOIN ${schema_name}._sc_user u1 on u1.id = wdc.field_value and wdc.field_code = 'receive_user_id' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 on wdc.field_value = cd2.code and wdc.field_code = 'solutions' and cd2.data_type = 'solutions' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd3 on wdc.field_value = cd3.code and wdc.field_code = 'fault_reason' and cd3.data_type = 'fault_reason' " +
//            "LEFT JOIN ${schema_name}._sc_work_finished_type wft on wdc.field_value = wft.id::varchar and wdc.field_code = 'deal_result' " +
//            "LEFT JOIN ${schema_name}._sc_fault_type ft on wdc.field_value = ft.id::varchar and wdc.field_code = 'fault_type_id' " +
//            "LEFT JOIN ${schema_name}._sc_user u2 on u2.id = wdc.field_value and wdc.field_code = 'create_user_id' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd4 on wdc.field_value = cd4.code and wdc.field_code = 'guarantee_status_id' and cd4.data_type = 'guarantee_status' " +
//            "LEFT JOIN ${schema_name}._sc_asset_running_status ars on wdc.field_value = ars.id::varchar and wdc.field_code = 'running_status_id' " +
//            "LEFT JOIN ${schema_name}._sc_asset_category ac on wdc.field_value = ac.id::varchar and wdc.field_code = 'category_id' " +
//            "LEFT JOIN ${schema_name}._sc_asset_position ap on wdc.field_value = ap.position_code and wdc.field_code = 'position_code' " +
//            "LEFT JOIN ${schema_name}._sc_asset a on wdc.field_value = a.id and wdc.field_code = 'relation_id' " +
//            "WHERE wd.sub_work_code = #{sub_work_code} " +
//            "ORDER BY wdc.field_section_type ")
//    List<Map<String, Object>> findRepairWorks(@Param("schema_name") String schemaName, @Param("sub_work_code") String subWorkCode, @Param("userLang") String userLang, @Param("companyId") Long companyId);

    @Select("SELECT wdc.id,wdc.sub_work_code,wdc.field_form_code,wdc.field_code,wdc.field_name, " +
            "case wdc.field_code when 'audit_opinion' then cd1.name when 'receive_user_id' then u1.user_name " +
            "when 'solutions' then cd2.name when 'fault_reason' then cd3.name when 'deal_result' then wft.finished_name " +
            "when 'fault_type_id' then ft.type_name when 'create_user_id' then u2.user_name when 'guarantee_status_id' then " +
            "cd4.name " +
            "when 'running_status_id' then ars.running_status when 'category_id' then ac.category_name " +
            "when 'position_code' then ap.position_name when 'relation_id' then a.asset_code else wdc.field_value end as field_value, " +
            "wdc.save_type,wdc.change_type,wdc.field_right,wdc.is_required,wdc.is_table_key,wdc.db_table_type,wdc.field_remark, " +
            "wdc.field_data_base,wdc.field_view_type,wdc.field_section_type,wdc.field_validate_method,wdc.create_time, " +
            "wdc.create_user_id,wdc.parent_id " +
            "FROM ${schema_name}._sc_works w " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd on w.work_code = wd.work_code " +
            "LEFT JOIN ${schema_name}._sc_works_detail_column wdc ON wd.sub_work_code = wdc.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 on wdc.field_value = cd1.code and wdc.field_code = 'audit_opinion' and cd1.data_type = 'audit_opinion' " +
            "LEFT JOIN ${schema_name}._sc_user u1 on u1.id = wdc.field_value and wdc.field_code = 'receive_user_id' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 on wdc.field_value = cd2.code and wdc.field_code = 'solutions' and cd2.data_type = 'solutions' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd3 on wdc.field_value = cd3.code and wdc.field_code = 'fault_reason' and cd3.data_type = 'fault_reason' " +
            "LEFT JOIN ${schema_name}._sc_work_finished_type wft on wdc.field_value = wft.id::varchar and wdc.field_code = 'deal_result' " +
            "LEFT JOIN ${schema_name}._sc_fault_type ft on wdc.field_value = ft.id::varchar and wdc.field_code = 'fault_type_id' " +
            "LEFT JOIN ${schema_name}._sc_user u2 on u2.id = wdc.field_value and wdc.field_code = 'create_user_id' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd4 on wdc.field_value = cd4.code and wdc.field_code = 'guarantee_status_id' and cd4.data_type = 'guarantee_status' " +
            "LEFT JOIN ${schema_name}._sc_asset_running_status ars on wdc.field_value = ars.id::varchar and wdc.field_code = 'running_status_id' " +
            "LEFT JOIN ${schema_name}._sc_asset_category ac on wdc.field_value = ac.id::varchar and wdc.field_code = 'category_id' " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap on wdc.field_value = ap.position_code and wdc.field_code = 'position_code' " +
            "LEFT JOIN ${schema_name}._sc_asset a on wdc.field_value = a.id and wdc.field_code = 'relation_id' " +
            "WHERE wd.sub_work_code = #{sub_work_code} " +
            "ORDER BY wdc.field_section_type ")
    List<Map<String, Object>> findRepairWorks(@Param("schema_name") String schemaName, @Param("sub_work_code") String subWorkCode);

    @Select("select id::varchar,cost_name from ${schema_name}._sc_cost_center order by id")
    @MapKey("id")
    Map<String, Map<String, String>>  findCostCenter(@Param("schema_name") String schemaName);

//    @Select("select code ,coalesce((select (c.resource->>cd.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd.name) as name from ${schema_name}._sc_cloud_data cd where data_type = #{data_type}  order by code")
//    @MapKey("code")
//    Map<String, Map<String, String>>  findCostType(@Param("schema_name") String schemaName, @Param("userLang") String userLang, @Param("companyId") Long companyId, @Param("data_type") String data_type);

    @Select("select code ,cd.name as name from ${schema_name}._sc_cloud_data cd where data_type = #{data_type}  order by code")
    @MapKey("code")
    Map<String, Map<String, String>> findCostType(@Param("schema_name") String schemaName, @Param("data_type") String data_type);

    @Select("SELECT c.condition::text,nc.field_code,nc.field_form_code,nc.id::varchar " +
            "from ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column_linkage l on l.id = c.linkage_id " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column nc on nc.id = l.column_id " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node n on nc.word_flow_node_id = n.id AND n.node_id = n.pref_id " +
            "LEFT JOIN ${schema_name}._sc_work_flow_info i on n.pref_id = i.pref_id " +
            "LEFT JOIN ${schema_name}._sc_works_detail d on i.work_type_id = d.work_type_id " +
            "WHERE d.sub_work_code = #{sub_work_code} " +
            "AND nc.field_code in ('bom_write_ues','cost_control') ")
    List<Map<String, String>> findWorksCostControl(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code);

    @Select("SELECT file_url FROM ${schema_name}._sc_files WHERE id = #{id}")
    String findFileById(@Param("schema_name") String schemaName, @Param("id") int id);

    @Select("SELECT * FROM ${schema_name}._sc_files WHERE id = #{id}")
    Map<String,Object> findFileInfoById(@Param("schema_name") String schemaName, @Param("id") int id);

    @Select("SELECT id::varchar,type_name,type_code from ${schema_name}._sc_bom_type ORDER BY id")
    @MapKey("id")
    Map<String, Map<String,String>> findBomType(@Param("schema_name")String schemaName);

    @Select("SELECT id::varchar,bom_name from ${schema_name}._sc_bom ORDER BY id")
    @MapKey("id")
    Map<String, Map<String, String>> findBoms(@Param("schema_name")String schemaName);

    @Select("SELECT id::varchar,stock_name from ${schema_name}._sc_stock ORDER BY id")
    @MapKey("id")
    Map<String, Map<String, String>> findStocks(@Param("schema_name")String schemaName);

    //    @Select("SELECT to_char(wd.finished_time,'yyyy-mm-dd HH24:mi') as finished_time, " +
//            "COALESCE(to_char(wp.operate_time,'yyyy-mm-dd HH24:mi'),'') as audit_time, " +
//            "wp.operate_user_id::varchar,COALESCE(u.user_name,'') as audit_user " +
//            "FROM ${schema_name}._sc_works_detail wd " +
//            "LEFT JOIN ${schema_name}._sc_work_process wp ON wd.sub_work_code = wp.sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_user u ON wp.operate_user_id = u.id " +
//            "WHERE wd.sub_work_code = #{sub_work_code} AND wp.status = 50 " +
//            "ORDER BY wp.operate_time desc limit 1 ")
    @Select("SELECT COALESCE(to_char(wd.finished_time, 'yyyy-mm-dd HH24:mi'),'') AS finished_time, " +
            "to_char(w.create_time, 'yyyy-mm-dd HH24:mi') AS create_time, " +
            "COALESCE((SELECT to_char(wp.operate_time,'yyyy-mm-dd HH24:mi') from ${schema_name}._sc_work_process wp WHERE wp.sub_work_code = wd.sub_work_code and wp.status = 50 ORDER BY wp.operate_time DESC LIMIT 1),'') AS audit_time, " +
            "COALESCE((SELECT u.user_name from ${schema_name}._sc_user u LEFT JOIN ${schema_name}._sc_work_process wp ON wp.operate_user_id = u.id WHERE wp.sub_work_code = wd.sub_work_code and wp.status = 50 ORDER BY wp.operate_time DESC LIMIT 1),'') as audit_user " +
            "FROM ${schema_name}._sc_works_detail wd " +
            "LEFT JOIN ${schema_name}._sc_works w on wd.work_code = w.work_code " +
            "WHERE wd.sub_work_code = #{sub_work_code} ")
    Map<String, Object> findRepairWorksExt(@Param("schema_name") String schemaName, @Param("sub_work_code") String subWorkCode);

}
