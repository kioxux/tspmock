package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/9.
 */
@Mapper
public interface WorkScheduleMapper {
    //    //新增排班
//    @Insert("insert into ${schema_name}._sc_work_schedule (user_account,group_id,begin_time,end_time,remark,create_user_account,create_time)" +
//            "values(#{r.user_account},#{r.group_id},#{r.begin_time},#{r.end_time},#{r.remark},#{r.create_user_account},#{r.create_time})")
//    int insertWorkSchedule(@Param("schema_name") String schema_name, @Param("r") WorkScheduleData workScheduleData);
//
//    //删除排班
//    @Delete("delete from ${schema_name}._sc_work_schedule where id='${id}'")
//    int deleteWorkSchedule(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    //更新排班
//    @Update("update ${schema_name}._sc_work_schedule " +
//            "set user_account = #{s.user_account}, " +
//            "group_id = #{s.group_id}, " +
//            "begin_time = #{s.begin_time}, " +
//            "end_time = #{s.end_time}, " +
//            "remark = #{s.remark}  " +
//            "where id = #{s.id}")
//    int updateWorkSchedule(@Param("schema_name") String schema_name, @Param("s") WorkScheduleData workScheduleData);
//
//    //根据ID查询排班信息
//    @Select("select b.*,g.group_name as title,u.username from ${schema_name}._sc_work_schedule b " +
//            "left join ${schema_name}._sc_group g on b.group_id=g.id " +
//            "left join ${schema_name}._sc_user u on b.user_account=u.account where u.status >= " + User.STATUS_LOCKED + " and b.id= #{id}")
//    WorkScheduleData findById(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    //获取排班列表
//    @Select("select b.*,g.group_name as title,u.username from ${schema_name}._sc_work_schedule b " +
//            "left join ${schema_name}._sc_group g on b.group_id=g.id " +
//            "left join ${schema_name}._sc_user u on b.user_account=u.account where 1=1")
//    List<WorkScheduleData> getWorkScheduleList(@Param("schema_name") String schema_name);
//
//    @Select("select b.*,g.group_name as title,u.username from ${schema_name}._sc_work_schedule b " +
//            "left join ${schema_name}._sc_user u on b.user_account=u.account " +
//            "left join ${schema_name}._sc_group g on b.group_id=g.id " +
//            "where u.status >= " + User.STATUS_LOCKED + " ${condition}")
//    List<WorkScheduleData> getWorkScheduleListByGroup(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//
//    @Select("select b.*,g.group_name as title,u.username from ${schema_name}._sc_work_schedule b " +
//            "left join ${schema_name}._sc_group g on b.group_id=g.id " +
//            "left join ${schema_name}._sc_user u on b.user_account=u.account where b.id=${id}")
//    WorkScheduleData getWorkScheduleListByFacilityById(@Param("schema_name") String schema_name, @Param("id") int id);
//
//
//    //根据位置和人员权限查询对应排班人员
////    @Select("select w.user_account as account, u.mobile as mobile, u.username as username " +
////            "from ${schema_name}._sc_work_schedule w " +
////            "INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
////            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
////            "left join ${schema_name}._sc_user_role ur on u.id=ur.user_id " +
////            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
////            "left join public.system_permission sp on sp.id=rp.permission_id " +
////            "where gorg.org_id in (#{facility_ids}) and u.status=1 and sp.key = #{key} " +
////            "and to_char(begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
////            "group by w.id, u.mobile, u.username,w.user_account;")
////    List<User> getWorkScheduleBySiteAndTime(@Param("schema_name") String schema_name,
////                                            @Param("facility_ids") String facility_ids,
////                                            @Param("key") String functionKey);
//
//    //根据工单池，位置和人员权限查询对应排班人员,在固定时间上班的人员
//    @Select(" <script>" +
//            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
//            " from ${schema_name}._sc_work_schedule w " +
//            " INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
//            " left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
//            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            " group by urn.user_id ) nr on u.id = nr.user_id " +
//            " left join " +
//            " (" +
//            " select role_id,user_id from ${schema_name}._sc_user_role " +
//            " <when test=\"category_id>0\"> " +
//            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
//            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            " </when>" +
//            " ) " +
//            " ur on u.id=ur.user_id " +
//            " left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
//            " left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            " left join public.system_permission sp on sp.id=rp.permission_id " +
//            " where f.facility_no = #{facility_no} and pr.pool_id = #{pool_id} and u.status=1 and sp.key = #{key} " +
//            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]>) " +
//            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
//            " </script>")
//    List<Map<String, Object>> getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("facility_no") String facility_no, @Param("pool_id") String pool_id, @Param("key") String functionKey, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names, rp.permission_id " +
            " from ${schema_name}._sc_work_schedule w " +
            " INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
            " left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id ) nr on u.id = nr.user_id " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            " ur on u.id=ur.user_id " +
            " left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
            " left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
            " where f.facility_no = #{facility_no} and pr.pool_id = #{pool_id} and u.status=1 " +
            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]>) " +
            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
            " </script>")
    List<Map<String, Object>> getWorkScheduleUserByPermissionKeyAndPoolAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("facility_no") String facility_no, @Param("pool_id") String pool_id , @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

//    //根据位置和人员权限查询对应排班人员,在固定时间上班的人员
//    @Select(" <script>" +
//            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
//            " from ${schema_name}._sc_work_schedule w " +
//            " INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
//            " left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
//            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            " group by urn.user_id ) nr on u.id = nr.user_id " +
//            " left join " +
//            " (" +
//            " select role_id,user_id from ${schema_name}._sc_user_role " +
//            " <when test=\"category_id>0\"> " +
//            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
//            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            " </when>" +
//            " ) " +
//            " ur on u.id=ur.user_id " +
//            " left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            " left join public.system_permission sp on sp.id=rp.permission_id " +
//            " where f.facility_no = #{facility_no} and u.status=1 and sp.key = #{key} " +
//            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]> ) " +
//            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
//            " </script>")
//    List<Map<String, Object>> getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("facility_no") String facility_no, @Param("key") String functionKey, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names, rp.permission_id " +
            " from ${schema_name}._sc_work_schedule w " +
            " INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
            " left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id ) nr on u.id = nr.user_id " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            " ur on u.id=ur.user_id " +
            " left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
            " where f.facility_no = #{facility_no} and u.status=1 " +
            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]> ) " +
            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
            " </script>")
    List<Map<String, Object>> getWorkScheduleUserByPermissionKeyAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("facility_no") String facility_no , @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

    //
////    //根据位置、人员权限、排班时间区间查询当前时间下对应排班人员
////    @Select("select w.user_account as account, u.mobile as mobile, u.username as username " +
////            "from ${schema_name}._sc_work_schedule w " +
////            "INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
////            "left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
////            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
////            "left join ${schema_name}._sc_user_role ur on u.id=ur.user_id " +
////            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
////            "left join public.system_permission sp on sp.id=rp.permission_id " +
////            "where gorg.org_id in (#{facility_ids}) and u.status=1 and sp.key = #{key} " +
////            "and begin_time <= current_timestamp and current_timestamp <= end_time " +
////            "group by w.id, u.mobile, u.username,w.user_account;")
////    List<User> getWorkScheduleBySiteAndTimeInterval(@Param("schema_name") String schema_name,
////                                                    @Param("facility_ids") String facility_ids,
////                                                    @Param("key") String functionKey);
//
//
    //根据工单池，位置和人员角色，查询对应排班人员,在固定时间上班的人员
    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
            " from ${schema_name}._sc_work_schedule w " +
            " INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
            " left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id ) nr on u.id = nr.user_id " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            "  ur on u.id=ur.user_id " +
            " left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
            " where f.facility_no = #{facility_no} and pr.pool_id = #{pool_id} and u.status=1 " +
            " <when test='roleIds!=null'> " +
            "  and ur.role_id in " +
            " <foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
            " #{roleId}" +
            " </foreach>" +
            " </when>" +
            "  and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]>) " +
            " group by w.id, u.mobile, u.use_rname, nr.roles,w.user_account " +
            " </script>")
    List<Map<String, Object>> getWorkScheduleUserByRoleAndPoolAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name,
                                                                                   @Param("facility_no") String facility_no,
                                                                                   @Param("pool_id") String pool_id,
                                                                                   @Param("roleIds") List<String> roleIds,
                                                                                   @Param("beginDateTime") Timestamp beginDateTime,
                                                                                   @Param("endDateTime") Timestamp endDateTime,
                                                                                   @Param("category_id") int category_id);

    //根据位置和人员角色，查询对应排班人员,在固定时间上班的人员
    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
            " from ${schema_name}._sc_work_schedule w " +
            " INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
            " left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id ) nr on u.id = nr.user_id " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            " ur on u.id=ur.user_id " +
            " where f.facility_no = #{facility_no} and u.status=1 " +
            " <when test='roleIds!=null'> " +
            "  and ur.role_id in " +
            " <foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
            " #{roleId}" +
            " </foreach>" +
            " </when>" +
            "  and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]> ) " +
            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
            " </script>")
    List<Map<String, Object>> getWorkScheduleUserByRoleAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name,
                                                                            @Param("facility_no") String facility_no,
                                                                            @Param("roleIds") List<String> roleIds,
                                                                            @Param("beginDateTime") Timestamp beginDateTime,
                                                                            @Param("endDateTime") Timestamp endDateTime,
                                                                            @Param("category_id") int category_id);
//
//    @Update("update ${schema_name}._sc_work_schedule " +
//            "set begin_time = #{begin_time}, " +
//            "end_time = #{end_time}  " +
//            "where id = #{id}")
//    int updateDate(@Param("schema_name") String schema_name, @Param("id") int id, @Param("begin_time") Timestamp begin_time, @Param("end_time") Timestamp end_time);
//
//    @Select("select b.*,f.title,u.username from ${schema_name}._sc_work_schedule b " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on b.group_id=gorg.group_id " +
//            "left join ${schema_name}._sc_facilities f on gorg.org_id=f.id " +
//            "left join ${schema_name}._sc_user u on b.user_account=u.account " +
//            "where gorg.org_id in (${findByFacilities}) and u.status=1 ")
//    List<WorkScheduleData> findByFacilities(@Param("schema_name") String schema_name, @Param("findByFacilities") String findByFacilities);
//
//    @Select("select distinct w.user_account as account, u.mobile as mobile, u.username as username,c.charge_level " +
//            "from ${schema_name}._sc_work_schedule w " +
//            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            " left join ${schema_name}._sc_facility_asset_charge c on w.user_account=c.charge_account " +
//            " where to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') and u.status=1 " +
//            " and ( (w.begin_time <= #{moningTime} and w.end_time>=#{moningTime} ) or (w.begin_time <= #{afternoonTime} and w.end_time>=#{afternoonTime}) ) " +
//            " group by w.id, u.mobile, u.username,c.charge_level,w.user_account")
//    List<LeadingOfficialResult> getWorkScheduleLeadingOfficial(@Param("schema_name") String schema_name, @Param("moningTime") Timestamp moningTime, @Param("afternoonTime") Timestamp afternoonTime);
//
//
//    //根据工单池，位置和人员角色，查询对应排班人员,在固定时间上班的人员
////    @Select("select distinct w.user_account as account, u.mobile as mobile, u.username || '(' || nr.roles || ')' as username " +,
//    @Select("select distinct w.user_account as account, u.mobile as mobile, u.username,nr.roles as role_names " +
//            "from ${schema_name}._sc_work_schedule w " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
//            "INNER JOIN ( " +
//            "select org_id from ${schema_name}._sc_asset_organization where asset_id=#{asset_id} " +
//            ") af on gorg.org_id=af.org_id " +
//            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
//            "where pr.pool_id = #{pool_id} and u.status=1 and ur.role_id in (#{role_ids}) " +
//            "and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            "and ( w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}) " +
//            "group by w.id, u.mobile, u.username, nr.roles,w.user_account")
//    List<User> getWorkScheduleUserByRoleAndPoolAndAssetAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("pool_id") String pool_id, @Param("role_ids") String role_ids, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime);
//
//    //根据位置和人员角色，查询对应排班人员,在固定时间上班的人员
//    @Select("select distinct w.user_account as account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "from ${schema_name}._sc_work_schedule w " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
//            "INNER JOIN ( " +
//            "select org_id from ${schema_name}._sc_asset_organization where asset_id=#{asset_id} " +
//            ") af on gorg.org_id=af.org_id " +
//            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "where u.status=1 and ur.role_id in (#{role_ids}) " +
//            "and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            "and ( w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime} ) " +
//            "group by w.id, u.mobile, u.username, nr.roles,w.user_account;")
//    List<User> getWorkScheduleUserByRoleAndAssetAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("role_ids") String role_ids, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime);
//
//
//    //根据工单池，设备id，人员权限查询对应排班人员,在固定时间上班的人员
//    @Select("select distinct w.user_account as account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "from ${schema_name}._sc_work_schedule w " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
//            "INNER JOIN ( " +
//            "select org_id from ${schema_name}._sc_asset_organization where asset_id=#{asset_id} " +
//            ") af on gorg.org_id=af.org_id " +
//            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
//            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            "left join public.system_permission sp on sp.id=rp.permission_id " +
//            "where pr.pool_id = #{pool_id} and u.status=1 and sp.key = #{key} " +
//            "and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            "and ( w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}) " +
//            "group by w.id, u.mobile, u.username, nr.roles,w.user_account;")
//    List<User> getWorkScheduleUserByPermissionKeyAndPoolAndAssetAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("pool_id") String pool_id, @Param("key") String functionKey, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime);
//
//    //根据位置和人员权限查询对应排班人员,在固定时间上班的人员
//    @Select("select distinct w.user_account as account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "from ${schema_name}._sc_work_schedule w " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on w.group_id=gorg.group_id " +
//            "INNER JOIN ( " +
//            "select org_id from ${schema_name}._sc_asset_organization where asset_id=#{asset_id} " +
//            ") af on gorg.org_id=af.org_id " +
//            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            "left join public.system_permission sp on sp.id=rp.permission_id " +
//            "where u.status=1 and sp.key = #{key} " +
//            "and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            "and ( w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime} ) " +
//            "group by w.id, u.mobile, u.username, nr.roles,w.user_account;")
//    List<User> getWorkScheduleUserByPermissionKeyAndAssetAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("key") String functionKey, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime);
//
//
//    //根据设备id，查找设备所在组织里的具有角色的用户
//    @Select("select distinct u.account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "from ${schema_name}._sc_user u " +
//            "left join ${schema_name}._sc_user_group ug on u.id=ug.user_id " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on ug.group_id=gorg.group_id " +
//            "INNER JOIN ( " +
//            "select org_id from ${schema_name}._sc_asset_organization where asset_id=#{asset_id} " +
//            ") af on gorg.org_id=af.org_id " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "where u.status=1 and ur.role_id in (#{roleIds}) " +
//            "group by u.id, u.mobile, u.username, nr.roles,u.account;")
//    List<User> findUsersByAssetIdAndRoles(@Param("schema_name") String schema_name, @Param("roleIds") String roleIds, @Param("asset_id") String asset_id);
//
//
//    //根据设备id，查找设备所在组织里的具有权限的用户
//    @Select("select distinct u.account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "from ${schema_name}._sc_user u " +
//            "left join ${schema_name}._sc_user_group ug on u.id=ug.user_id " +
//            "INNER JOIN ${schema_name}._sc_group_org gorg on ug.group_id=gorg.group_id " +
//            "INNER JOIN ( " +
//            "select org_id from ${schema_name}._sc_asset_organization where asset_id=#{asset_id} " +
//            ") af on gorg.org_id=af.org_id " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            "left join public.system_permission sp on sp.id=rp.permission_id " +
//            "where u.status=1 and sp.key = #{key} " +
//            "group by u.id, u.mobile, u.username, nr.roles,u.account;")
//    List<User> findUserByAssetIdAndFunctionStrKey(@Param("schema_name") String schema_name, @Param("key") String functionKey, @Param("asset_id") String asset_id);

    //按排班，获取设备关联人员
    @Select(" <script>" +
            " SELECT distinct u.account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
            " FROM ${schema_name}._sc_asset_duty_man dm " +
            " LEFT JOIN ${schema_name}._sc_work_schedule w on w.user_account=dm.user_account " +
            " LEFT JOIN ${schema_name}._sc_user u ON u.account = dm.user_account " +
            " LEFT JOIN ( " +
            " SELECT urn.user_id,string_agg (NAME || '', ',') roles " +
            " FROM ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn ON urn.role_id = rn. ID " +
            " GROUP BY urn.user_id " +
            " ) nr ON u.ID = nr.user_id " +
            " LEFT JOIN ${schema_name}._sc_work_type wt ON dm.duty_id = wt.id " +
            " LEFT JOIN ${schema_name}._sc_user_role tur ON u.ID = tur.user_id " +
            " where dm.asset_id=#{asset_id} " +
            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            " and ( <![CDATA[w.begin_time <= #{now_time} and w.end_time>=#{now_time}]]> ) " +
            " and wt.id=#{work_type_id} and u.status>0 and dm.isuse='t' " +
            " <when test='roleIds!=null'> " +
            "  and tur.role_id in " +
            " <foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
            " #{roleId}" +
            " </foreach>" +
            " </when>" +
            " group by u.id, u.mobile, u.user_name, nr.roles,u.account" +
            " </script>")
    List<Map<String, Object>> getDoExecuteScheduleUserByAssetId(@Param("schema_name") String schema, @Param("asset_id") String assetId, @Param("work_type_id") Integer workTypeId, @Param("roleIds") List<String> roleIds, @Param("now_time") Timestamp nowTime);

    //
    //根据设备id，找设备专门配置的维保执行人员  旧的zlz
//    @Select("<script>" +
//            "SELECT distinct u.account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "FROM ${schema_name}._sc_asset_duty_man dm " +
//            "LEFT JOIN ${schema_name}._sc_user u ON u.account = dm.user_account " +
//            "LEFT JOIN ( " +
//            "SELECT urn.user_id,string_agg (NAME || '', ',') roles " +
//            "FROM ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn ON urn.role_id = rn. ID " +
//            "GROUP BY urn.user_id " +
//            ") nr ON u.ID = nr.user_id " +
//            "LEFT JOIN ${schema_name}._sc_work_type wt ON dm.duty_id = wt.id " +
//            "LEFT JOIN ${schema_name}._sc_user_role tur ON u.ID = tur.user_id " +
//            "where dm.asset_id=#{asset_id} and wt.id=#{work_type_id} and u.status>0 and dm.isuse='t' " +
//            "<when test='roleIds!=null'> " +
//            " and tur.role_id in " +
//            "<foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
//            "#{roleId}" +
//            "</foreach>" +
//            "</when>" +
//            "group by u.id, u.mobile, u.username, nr.roles,u.account" +
//            "</script>")
    @Select(" <script>" +
            " SELECT DISTINCT u.id, u.mobile, u.use_rname, nr.roles,u.account" +
            " FROM ${schema_name}._sc_asset_duty_man AS adm" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON u.id = adm.user_id" +
            " LEFT JOIN ${schema_name}._sc_user_position AS up ON u.id = up.user_id" +
            " LEFT JOIN ${schema_name}._sc_position_role AS pr ON pr.position_id = up.position_id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON adm.duty_id = wt.id" +
            " WHERE adm.asset_id=#{asset_id} and wt.id=#{work_type_id} and u.status > 0 and adm.is_use='1' " +
            " <when test='roleIds!=null'> " +
            " AND pr.role_id IN " +
            " <foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
            " #{roleId}" +
            " </foreach>" +
            " </when>" +
            " GROUP BY u.id, u.mobile, u.user_name, nr.roles,u.account" +
            " </script>")
    List<Map<String, Object>> getDoExecuteUserByAssetId(@Param("schema_name") String schema, @Param("asset_id") String assetId, @Param("work_type_id") Integer workTypeId, @Param("roleIds") List<String> roleIds);


    //派案策略变更 --zys

    /**
     * 根据工单池，位置和人员角色，查询对应排班人员,在固定时间上班的人员 派案策略变更新方法
     * 加入设备位置为参数作为派案中间连接条件
     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
     *
     * @param schema_name
     * @param positionCode
     * @param pool_id
     * @param roleIds
     * @param beginDateTime
     * @param endDateTime
     * @param category_id
     * @return
     */
    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
            "from ${schema_name}._sc_work_schedule w " +
            "INNER JOIN ${schema_name}._sc_group_position sgp on w.group_id=sgp.group_id " +
            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            "group by urn.user_id ) nr on u.id = nr.user_id " +
            "left join " +
            "(" +
            "select role_id,user_id from ${schema_name}._sc_user_role " +
            "<when test=\"category_id>0\"> " +
            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            "</when>" +
            ") " +
            " ur on u.id=ur.user_id " +
            "left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
            "where sgp.position_code = #{positionCode} and pr.pool_id = #{pool_id} and u.status=1 " +
            "<when test='roleIds!=null'> " +
            " and ur.role_id in " +
            "<foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
            "#{roleId}" +
            "</foreach>" +
            "</when>" +
            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            "and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]>) " +
            "group by w.id, u.mobile, u.user_name, nr.roles,w.user_account " +
            " </script>")
    List<Map<String, Object>> getWorkScheduleExecutorByRoleAndPoolAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name,
                                                             @Param("positionCode") String positionCode,
                                                             @Param("pool_id") String pool_id,
                                                             @Param("roleIds") List<String> roleIds,
                                                             @Param("beginDateTime") Timestamp beginDateTime,
                                                             @Param("endDateTime") Timestamp endDateTime,
                                                             @Param("category_id") int category_id);

    /**
     * 根据角色，位置和人员角色，查询对应排班人员,在固定时间上班的人员
     * 派案策略变更新方法 加入设备位置为参数作为派案中间连接条件
     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
     *
     * @param schema_name
     * @param positionCode
     * @param roleIds
     * @param beginDateTime
     * @param endDateTime
     * @param category_id
     * @return
     */
    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
            "from ${schema_name}._sc_work_schedule w " +
            "INNER JOIN ${schema_name}._sc_group_position sgp on w.group_id=sgp.group_id " +
            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            "group by urn.user_id ) nr on u.id = nr.user_id " +
            "left join " +
            "(" +
            "select role_id,user_id from ${schema_name}._sc_user_role " +
            "<when test=\"category_id>0\"> " +
            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            "</when>" +
            ") " +
            "ur on u.id=ur.user_id " +
            "where sgp.position_code = #{positionCode} and u.status=1 " +
            "<when test='roleIds!=null'> " +
            " and ur.role_id in " +
            "<foreach collection='roleIds' item='roleId' index='index' open='(' close=')' separator=','>" +
            "#{roleId}" +
            "</foreach>" +
            "</when>" +
            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            "and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]> ) " +
            "group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
            " </script>")
    List<Map<String, Object>> getWorkScheduleExecutorByRoleAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name,
                                                                                @Param("positionCode") String positionCode,
                                                                                @Param("roleIds") List<String> roleIds,
                                                                                @Param("beginDateTime") Timestamp beginDateTime,
                                                                                @Param("endDateTime") Timestamp endDateTime,
                                                                                @Param("category_id") int category_id);
//
//    /**
//     * 根据设备id，查找设备所在位置里的具有权限的用户
//     * 派案策略变更新方法 加入设备位置为参数作为派案中间连接条件
//     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
//     *
//     * @param schema_name
//     * @param functionKey
//     * @param asset_id
//     * @return
//     */
//    @Select("select distinct u.account, u.mobile as mobile, u.username, nr.roles as role_names " +
//            "from ${schema_name}._sc_user u " +
//            "left join ${schema_name}._sc_user_group ug on u.id=ug.user_id " +
//            "INNER JOIN ${schema_name}._sc_group_position sgp on ug.group_id=sgp.group_id " +
//            "INNER JOIN ( " +
//            "select position from ${schema_name}._sc_asset where _id=#{asset_id} " +
//            ") af on sgp.position_code=af.position_code " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=( " +
//            "SELECT category_id FROM ${schema_name}._sc_asset where _id=#{asset_id}" +
//            " ) ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            "left join public.system_permission sp on sp.id=rp.permission_id " +
//            "where u.status=1 and sp.key = #{key} " +
//            "group by u.id, u.mobile, u.username, nr.roles,u.account;")
//    List<User> getUserByAssetIdAndFunctionStrKey(@Param("schema_name") String schema_name, @Param("key") String functionKey, @Param("asset_id") String asset_id);
//
//

//    /**
//     * 根据工单池,设备所在位置和人员权限查询对应排班人员
//     * ,在固定时间上班的人员 派案策略变更新方法
//     * 加入设备位置为参数作为派案中间连接条件
//     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
//     *
//     * @param schema_name
//     * @param positionCode
//     * @param pool_id
//     * @param functionKey
//     * @param beginDateTime
//     * @param endDateTime
//     * @param category_id
//     * @return
//     */
//    @Select(" <script>" +
//            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
//            " from ${schema_name}._sc_work_schedule w " +
//            " INNER JOIN ${schema_name}._sc_group_org sgp on w.group_id=sgp.group_id " +
//            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            " group by urn.user_id ) nr on u.id = nr.user_id " +
//            " left join " +
//            " (" +
//            " select role_id,user_id from ${schema_name}._sc_user_role " +
//            " <when test=\"category_id>0\"> " +
//            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
//            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            " </when>" +
//            " ) " +
//            " ur on u.id=ur.user_id " +
//            " left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
//            " left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            " left join public.system_permission sp on sp.id=rp.permission_id " +
//            " where sgp.position_code = #{positionCode} and pr.pool_id = #{pool_id} and u.status=1 and sp.key = #{key} " +
//            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]>) " +
//            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
//            " </script>")
//    List<Map<String, Object>> getWorkScheduleExecutorByPermissionKeyAndPoolAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("positionCode") String positionCode, @Param("pool_id") String pool_id, @Param("key") String functionKey, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names, rp.permission_id " +
            " from ${schema_name}._sc_work_schedule w " +
            " INNER JOIN ${schema_name}._sc_group_org sgp on w.group_id=sgp.group_id " +
            " left join ${schema_name}._sc_user u on w.user_account=u.account " +
            " left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id ) nr on u.id = nr.user_id " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            " ur on u.id=ur.user_id " +
            " left join ${schema_name}._sc_work_pool_role pr on ur.role_id=pr.role_id " +
            " left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
            " where sgp.position_code = #{positionCode} and pr.pool_id = #{pool_id} and u.status=1 " +
            " and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            " and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]>) " +
            " group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
            " </script>")
    List<Map<String, Object>> getWorkScheduleExecutorByPermissionKeyAndPoolAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("positionCode") String positionCode, @Param("pool_id") String pool_id , @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

//    /**
//     * 根据设备所在位置和人员权限查询对应排班人员
//     * ,在固定时间上班的人员 派案策略变更新方法
//     * 加入设备位置为参数作为派案中间连接条件
//     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
//     *
//     * @param schema_name
//     * @param positionCode
//     * @param functionKey
//     * @param beginDateTime
//     * @param endDateTime
//     * @param category_id
//     * @return
//     */
//    @Select(" <script>" +
//            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names " +
//            "from ${schema_name}._sc_work_schedule w " +
//            "INNER JOIN ${schema_name}._sc_group_org sgp on w.group_id=sgp.group_id " +
//            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
//            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            "group by urn.user_id ) nr on u.id = nr.user_id " +
//            "left join " +
//            "(" +
//            "select role_id,user_id from ${schema_name}._sc_user_role " +
//            "<when test=\"category_id>0\"> " +
//            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
//            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            "</when>" +
//            ") " +
//            "ur on u.id=ur.user_id " +
//            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
//            "left join public.system_permission sp on sp.id=rp.permission_id " +
//            "where sgp.position_code = #{positionCode} and u.status=1 and sp.key = #{key} " +
//            "and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            "and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]> ) " +
//            "group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
//            " </script>")
//    List<Map<String, Object>> getWorkScheduleExecutorByPermissionKeyAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("positionCode") String positionCode, @Param("key") String functionKey, @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

    @Select(" <script>" +
            " select distinct w.user_account as account, u.mobile as mobile, u.user_name, nr.roles as role_names, rp.permission_id " +
            "from ${schema_name}._sc_work_schedule w " +
            "INNER JOIN ${schema_name}._sc_group_org sgp on w.group_id=sgp.group_id " +
            "left join ${schema_name}._sc_user u on w.user_account=u.account " +
            "left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            "INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            "group by urn.user_id ) nr on u.id = nr.user_id " +
            "left join " +
            "(" +
            "select role_id,user_id from ${schema_name}._sc_user_role " +
            "<when test=\"category_id>0\"> " +
            "where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            "or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            "</when>" +
            ") " +
            "ur on u.id=ur.user_id " +
            "left join ${schema_name}._sc_role_permission rp on ur.role_id=rp.role_id " +
            "where sgp.position_code = #{positionCode} and u.status=1 " +
            "and to_char(w.begin_time, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
            "and ( <![CDATA[w.begin_time <= #{beginDateTime} and w.end_time>=#{endDateTime}]]> ) " +
            "group by w.id, u.mobile, u.user_name, nr.roles,w.user_account" +
            " </script>")
    List<Map<String, Object>> getWorkScheduleExecutorByPermissionKeyAndSiteAndTimeOnDuty(@Param("schema_name") String schema_name, @Param("positionCode") String positionCode , @Param("beginDateTime") Timestamp beginDateTime, @Param("endDateTime") Timestamp endDateTime, @Param("category_id") int category_id);

//    //派案策略变更 --zys
}
