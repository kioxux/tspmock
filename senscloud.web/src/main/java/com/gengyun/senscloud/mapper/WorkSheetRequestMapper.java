package com.gengyun.senscloud.mapper;

public interface WorkSheetRequestMapper {
//
//    //查找维修单列表
//    @Select("select count(1) from (select distinct w.work_request_code,w.source_type,w.close_type,w.priority_level,u.username as create_user_name,s.status as status_name," +
//            "t.type_name as type_name,dv.strname as asset_name, " +
//            "dv.strcode as strcode,ct.short_title as customer_name,t.flow_template_code as flow_code," +
//            "t.business_type_id as business_type_id,ma.category_name as asset_type," +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN ft.short_title ELSE ap.position_name END AS facility_name, " +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN pf.short_title ELSE apt.position_name END AS parent_name " +
//            " from ${schema_name}._sc_work_request w " +
//            " INNER join ${schema_name}._sc_work_type t on w.work_type_id=t.id and t.business_type_id in ('1', '2') " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
////            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
//            " left join ${schema_name}._sc_facilities ft on w.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_facilities ct on ct.isuse=true and ct.status > 0 and ct.id=dv.supplier " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            "left join ${schema_name}._sc_asset_organization ao on dv._id = ao.asset_id " +
//            "left join ${schema_name}._sc_facilities aof on aof.id = ao.org_id and aof.isuse = true and aof.status > 0 " +
//            "left join ${schema_name}._sc_asset_position_organization apo on dv.position_code = apo.position_code " +
//            "left join ${schema_name}._sc_facilities apf on apf.id = ao.org_id and apf.isuse = true and apf.status > 0 " +
//            "where 1=1 ${condition}) t ")
//    int getWsqListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查找维修单列表
//    @Select("select distinct w.work_request_code,w.source_type,w.close_type,w.priority_level,w.work_type_id,w.work_template_code,w.parent_code,w.position_code," +
//            "w.relation_type,w.relation_id,w.title,w.problem_note,w.problem_img,w.occur_time,w.deadline_time," +
//            "w.status,w.customer_id,w.plan_code,w.remark,w.create_user_account,w.create_time,w.facility_id," +
//            "u.username as create_user_name,s.status as status_name, " +
//            "t.type_name as type_name,dv.strname as asset_name, " +
//            "dv.strcode as strcode,ct.short_title as customer_name,t.flow_template_code as flow_code," +
//            "t.business_type_id as business_type_id,ma.category_name as asset_type," +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN ft.short_title ELSE ap.position_name END AS facility_name, " +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN pf.short_title ELSE apt.position_name END AS parent_name " +
//            " from ${schema_name}._sc_work_request w " +
//            " INNER join ${schema_name}._sc_work_type t on w.work_type_id=t.id and t.business_type_id in ('1', '2') " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
////            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
//            " left join ${schema_name}._sc_facilities ft on w.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_facilities ct on ct.isuse=true and ct.status > 0 and ct.id=dv.supplier " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            "left join ${schema_name}._sc_asset_organization ao on dv._id = ao.asset_id " +
//            "left join ${schema_name}._sc_facilities aof on aof.id = ao.org_id and aof.isuse = true and aof.status > 0 " +
//            "left join ${schema_name}._sc_asset_position_organization apo on dv.position_code = apo.position_code " +
//            "left join ${schema_name}._sc_facilities apf on apf.id = ao.org_id and apf.isuse = true and apf.status > 0 " +
//            "where 1=1 ${condition} " +
//            "order by w.occur_time desc,w.work_request_code desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getWsqList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据主键查询工单请求数据（不含json串）
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Select("select w.work_request_code,w.source_type,w.close_type,w.priority_level,w.work_type_id,w.work_template_code,w.parent_code,w.position_code," +
//            " w.relation_type,w.relation_id,w.title,w.problem_note,w.problem_img,w.occur_time,w.deadline_time," +
//            " w.status,w.customer_id,w.plan_code,w.remark,w.create_user_account,w.create_time, " +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN ltrim(to_char(w.facility_id, '" + SqlConstant.FACILITY_ID_LEN + "')) ELSE w.position_code END AS facility_id " +
//            "from ${schema_name}._sc_work_request w where work_request_code = #{workRequestCode} ")
//    Map<String, Object> queryWsqInfoById(@Param("schema_name") String schemaName, @Param("workRequestCode") String workRequestCode);
//
//    /**
//     * 根据主键查询工单请求数据
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Select("select w.work_request_code,w.source_type,w.close_type,w.priority_level,w.work_type_id,w.work_template_code,w.parent_code,w.position_code," +
//            " w.relation_type,w.relation_id,w.title,w.problem_note,w.problem_img,w.occur_time,w.deadline_time," +
//            " w.status,w.customer_id,w.plan_code,w.remark,w.create_user_account,w.create_time,w.body_property, " +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN ltrim(to_char(w.facility_id, '" + SqlConstant.FACILITY_ID_LEN + "')) ELSE w.position_code END AS facility_id " +
//            "from ${schema_name}._sc_work_request w where work_request_code = #{workRequestCode} ")
//    Map<String, Object> queryWorkRequestById(@Param("schema_name") String schemaName, @Param("workRequestCode") String workRequestCode);
//
////    /**
////     * 取工单请求业务号
////     *
////     * @param schemaName
////     * @return
////     */
////    @Select("SELECT ',' || string_agg(business_type_id||'' , ',') || ',' as business_type_id " +
////            "from (" +
////            "   select jsonb_array_elements(t.body_property) j, wt.business_type_id " +
////            "   from ${schema_name}._sc_work_template t " +
////            "   INNER JOIN ${schema_name}._sc_work_type wt on t.work_type_id = wt.id " +
////            "   where t.is_common_template = '0' and wt.business_type_id < 21 and t.isuse = '1' " +
////            ") t " +
////            "where (j->>'fieldCode') = 'work_request_type' and (j->>'fieldValue') = 'confirm'")
////    String selectWqBusinessInfo(@Param("schema_name") String schemaName);
//
//    /**
//     * 取工单请求类型
//     *
//     * @param schemaName
//     * @return
//     */
//    @Select("SELECT ',' || string_agg(id||'' , ',') || ',' as work_type_id " +
//            "from (" +
//            "   select jsonb_array_elements(t.body_property) j, wt.id " +
//            "   from ${schema_name}._sc_work_template t " +
//            "   INNER JOIN ${schema_name}._sc_work_type wt on t.work_type_id = wt.id " +
//            "   where t.is_common_template = '0' and wt.business_type_id < 21 and t.isuse = '1' " +
//            ") t " +
//            "where (j->>'fieldCode') = 'work_request_type' and (j->>'fieldValue') = 'confirm'")
//    String selectWqBusinessInfo(@Param("schema_name") String schemaName);
//
//    /**
//     * 根据主键查询工单请求数据（不含json串）
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Select("select w.work_request_code,w.create_time,u.username,u.mobile,f.short_title,w.remark " +
//            "from ${schema_name}._sc_work_request w " +
//            " LEFT JOIN ${schema_name}._sc_facilities f ON f.id = w.facility_id " +
//            " LEFT JOIN ${schema_name}._sc_user u ON u.account = w.create_user_account " +
//            "where work_request_code = #{workRequestCode} ")
//    Map<String, Object> queryWqInfoById(@Param("schema_name") String schemaName, @Param("workRequestCode") String workRequestCode);
}
