package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.model.FilesData;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface FilesMapper {
    /**
     * 新增附件文件
     *
     * @param schema_name
     * @param file_name
     * @param file_type_id
     * @param file_size
     * @param file_original_name
     * @param file_url
     * @param remark
     * @param create_user_id
     * @param business_no
     * @param business_no_id
     * @return
     */
    @Insert("insert into ${schema_name}._sc_files (file_name,file_type_id,file_size,file_width,file_height," +
            "file_original_name,file_url,remark,is_use,create_time,create_user_id,file_category_id,business_no,business_no_id) values(#{file_name},#{file_type_id}," +
            "#{file_size},#{file_width},#{file_height},#{file_original_name},#{file_url},#{remark},'1'," +
            "current_timestamp,#{create_user_id},#{file_category_id},#{business_no},#{business_no_id})")
    int AddFile(@Param("schema_name") String schema_name, @Param("file_name") String file_name,
                @Param("file_type_id") Integer file_type_id,
                @Param("file_size") Double file_size, @Param("file_width") Integer file_width,
                @Param("file_height") Integer file_height, @Param("file_original_name") String file_original_name,
                @Param("file_url") String file_url, @Param("remark") String remark,
                @Param("create_user_id") String create_user_id, @Param("file_category_id") Integer file_category_id,
                @Param("business_no") String business_no, @Param("business_no_id") String business_no_id);

    //查找附件
    @Select("select t.*, to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time_str from ${schema_name}._sc_files t where id in (${file_ids}) order by id desc ")
    List<FilesData> FindFileList(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids);

    /**
     * 根据主键获取文件信息
     *
     * @param schema_name 数据库
     * @param id          主键
     * @return 文件信息
     */
    @Select("select * from ${schema_name}._sc_files t where id = #{id} ")
    Map<String, Object> findFileById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    //查找附件，通过名称
    @Select("select * from ${schema_name}._sc_files where file_name = '${file_name}'")
    List<FilesData> FindAllFilesByName(@Param("schema_name") String schema_name, @Param("file_name") String file_name);

    // 获得当前插入数据id（在调用addFile之后手动调用）
    @Select("SELECT currval('${schema_name}._sc_files_id_seq')")
    int selectCurrval(@Param("schema_name") String schema_name);


    /**
     * 根据附件id获取名称列表
     *
     * @param schema_name 请求参数
     * @param file_ids    请求参数
     * @return 列表数据
     */
    @Select("SELECT file_original_name FROM ${schema_name}._sc_files t WHERE t.id IN (${file_ids})")
    List<String> findFileNamesByIds(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids);

    @Update(" UPDATE ${schema_name}._sc_files SET file_type_id = #{file_type_id} WHERE ID = #{file_id} ")
    void updateFileType(@Param("schema_name") String schema_name, @Param("file_id") Integer file_id, @Param("file_type_id") Integer file_type_id);

    @Update(" UPDATE ${schema_name}._sc_files SET file_category_id = #{file_category_id},remark = #{remark},file_type_id = #{file_type_id} WHERE ID = #{file_id} ")
    void updateFileCategory(@Param("schema_name") String schema_name, @Param("file_id") Integer file_id, @Param("file_category_id") Integer file_category_id,
                            @Param("remark") String remark, @Param("file_type_id") Integer file_type_id);
}
