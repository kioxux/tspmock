package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 备件工单
 */
@Mapper
public interface BomWorksMapper {
    /**
     * 根据备件工单详情编码获取备件工单详情信息
     *
     * @param schema_name 入参
     * @param work_code   入参
     * @return 备件工单详情信息
     */
    @Select(" SELECT * FROM ${schema_name}._sc_bom_works WHERE work_code = #{work_code}")
    Map<String, Object> findBomWorksInfoByCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据备件工单详情编码获取备件工单详情列表
     *
     * @param schema_name 入参
     * @param work_code   入参
     * @return 备件工单详情信息
     */
    @Select(" SELECT * FROM ${schema_name}._sc_bom_works_detail WHERE work_code = #{work_code}")
    List<Map<String, Object>> findBomWorksDetailListByCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据设备工单编码获取设备工单自己为处理人的子工单
     */
    @Select(" SELECT DISTINCT tb.sub_work_code,tb.work_code,tb.work_type_id,tb.relation_id,tb.duty_user_name,detc.field_value bom_type_id FROM " +
            "(SELECT awd.sub_work_code,awd.work_code,aw.work_type_id,awd.relation_id,u.user_name AS duty_user_name FROM " +
            "${schema_name}._sc_bom_works_detail awd " +
            "LEFT JOIN ${schema_name}._sc_bom_works aw ON aw.work_code = awd.work_code " +
            "LEFT JOIN ${schema_name}._sc_user u ON u.ID = awd.duty_user_id WHERE awd.work_code = #{work_code} " +
            "AND awd.is_main = - 1 AND awd.status <> 110 AND awd.status <> 60 AND awd.status <> 10 AND awd.status <> 900 AND awd.duty_user_id = #{user_id} ) tb " +
            "LEFT JOIN ${schema_name}._sc_works_detail_column detc ON detc.sub_work_code = tb.sub_work_code " +
            "AND detc.field_code = 'bom_type_id' INNER JOIN ${schema_name}.ACT_RU_VARIABLE arv ON arv.NAME_ = 'sub_work_code' " +
            "AND arv.TYPE_ = 'string' AND arv.TEXT_ = tb.sub_work_code " +
            "INNER JOIN ${schema_name}.ACT_RU_TASK ART ON ART.ID_ = arv.TASK_ID_ " +
            "OR ART.PROC_INST_ID_ = arv.EXECUTION_ID_")
    List<Map<String, Object>> findSubBomWorksListByWorkCodeUserId(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("user_id") String user_id);

    /**
     * 根据备件工单编码获取备件工单概要信息
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     * @return 备件工单概要信息
     */
    @Select(" SELECT bwd.*,bw.* FROM ${schema_name}._sc_bom_works_detail bwd" +
            " LEFT JOIN ${schema_name}._sc_bom_works bw on bwd.work_code = bw.work_code" +
            " WHERE bwd.sub_work_code = #{sub_work_code}")
    Map<String, Object> findBomWorksDetailInfoByCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据工单编号获取备件工单详情主工单数据
     *
     * @param schema_name 数据库
     * @param work_code   工单编号
     * @return 主工单数据
     */
    @Select(" SELECT * FROM ${schema_name}._sc_bom_works_detail WHERE work_code = #{work_code} AND is_main = 1 ")
    Map<String, Object> findMainBomWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);


    /**
     * 根据工单编号获取备件子工单详情列表数据
     *
     * @param schema_name
     * @param work_code
     * @return
     */
    @Select(" SELECT * FROM ${schema_name}._sc_bom_works_detail awd WHERE awd.work_code = #{work_code} AND awd.is_main = -1 ")
    List<Map<String, Object>> findSubBomWorksDetailListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据设备工单编码获取设备工单详情列表
     */
    @Select(" SELECT aw.*,awd.* FROM ${schema_name}._sc_bom_works_detail awd" +
            " LEFT JOIN ${schema_name}._sc_bom_works aw on awd.work_code = aw.work_code" +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = 1 ")
    Map<String, Object> findMainBomWorksDetailListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 更新子工单（未完成数据）
     *
     * @param schemaName      入参
     * @param receive_user_id 入参
     * @param sub_work_code   入参
     */
    @Update("UPDATE ${schema_name}._sc_bom_works_detail SET duty_user_id =#{receive_user_id} WHERE sub_work_code = #{sub_work_code} and status <= 50 ")
    void updateWaitBomWorkDetailBySubWorkCode(@Param("schema_name") String schemaName, @Param("receive_user_id") String receive_user_id, @Param("sub_work_code") String sub_work_code);

    /**
     * 查询所有子工单是否还有未完成的
     */
    @Select(" SELECT count(1) " +
            " FROM ${schema_name}._sc_bom_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 AND awd.status <> 110 AND awd.status <> 60  LIMIT 1 ")
    int findNoEndSubBomWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);


    /**
     * 查询一共有多少子工单
     */
    @Select(" SELECT count(1) " +
            " FROM ${schema_name}._sc_bom_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 LIMIT 1 ")
    int findAllSubBomWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 查询一共有多少已完成的子工单
     */
    @Select(" SELECT count(1) " +
            " FROM ${schema_name}._sc_bom_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 AND awd.status = 60 LIMIT 1 ")
    int findEndSubBomWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 查询备件流程单分页总数
     *
     * @param schema_name 数据库信息
     * @param pm          页面参数
     * @param user_id     当前登陆人
     * @return 分页总数
     */
    @Select(" <script>" +
            " SELECT count(1) " +
            " FROM ${schema_name}._sc_bom_works AS bw" +
            " LEFT JOIN  ${schema_name}._sc_bom_works_detail AS bwd ON bw.work_code = bwd.work_code " +
            " LEFT JOIN  ${schema_name}._sc_bom AS b ON b.id = bwd.bom_id " +
            " LEFT JOIN  ${schema_name}._sc_user AS u ON bw.create_user_id = u.id" +
            " LEFT JOIN  ${schema_name}._sc_work_type AS wt ON wt.id = bw.work_type_id" +
            " LEFT JOIN  ${schema_name}._sc_stock  AS s ON s.id = bw.stock_id" + SqlConstant.BOM_WORK_AUTH_SQL +
            " WHERE 1=1 " +
            //当前处理人是自己
            //" OR aw.create_user_id = #{pm.user_id} " +
            " <when test='pm.statusSearch!=null'>" +
            " AND bw.status IN " +
            "  <foreach collection='pm.statusSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.workTypeIdSearch!=null'>" +
            " AND bw.work_type_id IN " +
            "  <foreach collection='pm.workTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.bomTypeIdSearch!=null'>" +
            " AND b.type_id IN " +
            "  <foreach collection='pm.bomTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.inTypeSearch!=null'>" +
            " AND bwd.in_type IN " +
            "  <foreach collection='pm.inTypeSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.beginTime!=null and pm.beginTime!=\"\" and pm.finishedTime!=\"\" and pm.finishedTime!=null'>" +
            " AND bw.create_time &gt;=#{pm.beginTime}::TIMESTAMP AND bw.create_time &lt;=#{pm.finishedTime}::TIMESTAMP  " +
            " </when>" +
            " <when test='pm.start_quantity!=null and pm.start_quantity!=\"\" and pm.end_quantity!=\"\" and pm.end_quantity!=null'>" +
            " AND (SELECT SUM(quantity) FROM  ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = bwd.sub_work_code)  &gt;=#{pm.start_quantity}::float " +
            " AND (SELECT SUM(quantity) FROM  ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = bwd.sub_work_code)  &lt;=#{pm.end_quantity}::float  " +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " bw.work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR s.stock_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when>" +
            " </script>")
    int findBomWorksCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm, @Param("user_id") String user_id);

    /**
     * 查询备件流程单分页列表
     *
     * @param schema_name 数据库信息
     * @param pm          页面参数
     * @param user_id     当前登陆人
     * @return 分页列表
     */
    @Select(" <script>" +
            " SELECT  bw.work_type_id,wt.type_name AS work_type_name, u2.user_name AS duty_user_name,bwd.duty_user_id," +
            " bw.status,bw.work_code,bwd.sub_work_code,s.stock_name,s1.status AS status_name ," +
            " to_char(bw.create_time, 'YYYY-MM-DD hh24:mi:ss') AS create_time,bw.create_user_id," +
            " u.user_name AS create_user_name,(CASE WHEN (SELECT count(1) FROM ${schema_name}._sc_bom_works_detail  WHERE work_code = bw.work_code AND status &lt;&gt; 60 AND status &lt;&gt; 110 AND status &lt;&gt; 10 AND status &lt;&gt; 900 AND is_main = -1  AND duty_user_id = #{pm.user_id}) > 0 THEN 1 ELSE -1 END ) AS is_have_sub," +
            " (SELECT SUM(quantity) FROM  ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = bwd.sub_work_code) AS quantity  " +
            " FROM ${schema_name}._sc_bom_works AS bw" +
            " LEFT JOIN  ${schema_name}._sc_bom_works_detail AS bwd ON bw.work_code = bwd.work_code " +
            " LEFT JOIN  ${schema_name}._sc_user AS u ON bw.create_user_id = u.id" +
            " LEFT JOIN  ${schema_name}._sc_user AS u2 ON bwd.duty_user_id = u2.id" +
            " LEFT JOIN  ${schema_name}._sc_work_type AS wt ON wt.id = bw.work_type_id" +
            " LEFT JOIN  ${schema_name}._sc_stock AS s ON s.id = bw.stock_id" +
            " LEFT JOIN  ${schema_name}._sc_status AS s1 ON s1.id = bw.status" + SqlConstant.BOM_WORK_AUTH_SQL +
            " WHERE 1=1 " +
            //当前处理人是自己
            //" OR aw.create_user_id = #{pm.user_id} " +
            " <when test='pm.statusSearch!=null'>" +
            " AND bw.status IN " +
            "  <foreach collection='pm.statusSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.bomTypeIdSearch!=null'>" +
            " AND b.type_id IN " +
            "  <foreach collection='pm.bomTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.workTypeIdSearch!=null'>" +
            " AND bw.work_type_id IN " +
            "  <foreach collection='pm.workTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.beginTime!=null and pm.beginTime!=\"\" and pm.finishedTime!=\"\" and pm.finishedTime!=null'>" +
            " AND bw.create_time &gt;=#{pm.beginTime}::TIMESTAMP AND bw.create_time &lt;=#{pm.finishedTime}::TIMESTAMP  " +
            " </when>" +
            " <when test='pm.start_quantity!=null and pm.start_quantity!=\"\" and pm.end_quantity!=\"\" and pm.end_quantity!=null'>" +
            " AND (SELECT SUM(quantity) FROM  ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = bwd.sub_work_code) &gt;=#{pm.start_quantity}::float  " +
            " AND (SELECT SUM(quantity) FROM  ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = bwd.sub_work_code) &lt;=#{pm.end_quantity}::float   " +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            "  bw.work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR s.stock_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when> order by bw.create_time DESC ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findBomWorksPage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm, @Param("user_id") String user_id);

    /**
     * 更新设备工单概要表
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_bom_works " +
            " SET work_code = #{pm.work_code}" +
            " <when test='pm.stock_id!=null'>" +
            " ,stock_id =#{pm.stock_id}" +
            " </when> " +
            " <when test='pm.work_type_id!=null'>" +
            " ,work_type_id = #{pm.work_type_id}::int" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " ,status =#{pm.status}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " ,remark =#{pm.remark}" +
            " </when> " +
            " <when test='pm.file_ids!=null'>" +
            " ,file_ids = #{pm.file_ids}" +
            " </when> " +
            " <when test='pm.client_org_id!=null'>" +
            " ,client_org_id = #{pm.client_org_id}" +
            " </when> " +
            " <when test='pm.use_to!=null'>" +
            " ,use_to = #{pm.use_to}" +
            " </when> " +
            " <when test='pm.to_stock_id!=null'>" +
            " ,to_stock_id = #{pm.to_stock_id}" +
            " </when> " +
            " <when test='pm.from_bill_code!=null'>" +
            " ,from_bill_code = #{pm.from_bill_code}" +
            " </when> " +
            " <when test='pm.source_sub_code!=null'>" +
            " ,source_sub_code = #{pm.source_sub_code}" +
            " </when> " +
            " <when test='pm.source_sub_code!=null'>" +
            " ,source_sub_code = #{pm.source_sub_code}" +
            " </when> " +
            " <when test='pm.create_user_id!=null'>" +
            " ,create_user_id = #{pm.create_user_id}" +
            " </when> " +
            " WHERE work_code = #{pm.work_code}" +
            " </script>")
    void updateBomWorksByWorkCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新设备工单详情
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_bom_works_detail " +
            " SET sub_work_code = #{pm.sub_work_code}" +
            " <when test='pm.work_code!=null'>" +
            " ,work_code =#{pm.work_code}" +
            " </when> " +
            " <when test='pm.bom_id!=null'>" +
            " ,bom_id = #{pm.bom_id}" +
            " </when> " +
            " <when test='pm.quantity!=null'>" +
            " ,quantity =#{pm.quantity}::float" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " ,status =#{pm.status}::int" +
            " </when> " +
            " <when test='pm.real_quantity!=null'>" +
            " ,real_quantity =#{pm.real_quantity}::float" +
            " </when> " +
            " <when test='pm.area_code!=null'>" +
            " ,area_code = #{pm.area_code}" +
            " </when> " +
            " <when test='pm.area_code_out!=null'>" +
            " ,area_code_out = #{pm.area_code_out}" +
            " </when> " +
            " <when test='pm.price!=null'>" +
            " ,price = #{pm.price}" +
            " </when> " +
            " <when test='pm.file_ids!=null'>" +
            " ,file_ids = #{pm.file_ids}" +
            " </when> " +
            " <when test='pm.leave_cost!=null'>" +
            " ,leave_cost =#{pm.leave_cost}" +
            " </when> " +
            " <when test='pm.currency_id!=null'>" +
            " ,currency_id = #{pm.currency_id}::int" +
            " </when> " +
            " <when test='pm.buy_time!=null'>" +
            " ,buy_time = #{pm.buy_time}" +
            " </when> " +
            " <when test='pm.batch_no!=null'>" +
            " ,batch_no = #{pm.batch_no}" +
            " </when> " +
            " <when test='pm.in_type!=null'>" +
            " ,in_type = #{pm.in_type}::int" +
            " </when> " +
            " <when test='pm.supplier_id!=null'>" +
            " ,supplier_id = #{pm.supplier_id}::int" +
            " </when> " +
            " <when test='pm.duty_user_id!=null'>" +
            " ,duty_user_id = #{pm.duty_user_id}" +
            " </when> " +
            " WHERE sub_work_code = #{pm.sub_work_code}" +
            " </script>")
    void updateBomWorksDetailBySubWorkCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据编码获取备件详情清单列表
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     * @return 备件详情清单列表
     */
    @Select("SELECT * FROM ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = #{sub_work_code} ")
    List<Map<String, Object>> findBomWorksDetailItemListBySubWorkCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    @Select("SELECT SUM(quantity) AS quantity FROM ${schema_name}._sc_bom_works_detail_item WHERE sub_work_code = #{sub_work_code}")
    Integer findBomWorksDetailItemQuantityBySubWorkCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    @Update(" UPDATE ${schema_name}._sc_bom_works " +
            " SET status = #{status}" +
            " WHERE work_code = #{work_code}")
    void updateBomWorksStatus(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("status") Integer status);

    @Update(" UPDATE ${schema_name}._sc_bom_works_detail " +
            " SET status = #{status}" +
            " WHERE work_code = #{work_code}")
    void updateBomWorksDetailStatus(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("status") Integer status);

    @Insert("insert into ${schema_name}._sc_bom_works_allot_record (sub_work_code,work_code,pref_id,bom_id,quantity,price,material_code,stock_id,status,type_id,type_name,create_user_id) " +
            "values(#{pm.sub_work_code},#{pm.work_code},#{pm.pref_id},#{pm.bom_id},#{pm.quantity}::float,#{pm.price}::float,#{pm.material_code},#{pm.stock_id}::int,#{pm.status}::int,#{pm.type_id}::int,#{pm.type_name},#{pm.create_user_id})")
    int insertBomWorksAllotRecord(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> data);
}
