package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.Map;

/**
 * 监控实时
 */
@Mapper
public interface AssetMonitorCurrentMapper {

    @Update("UPDATE ${schema_name}._sc_asset_monitor_current " +
            "SET monitor_value = #{monitor_value}, " +
            "gather_time = #{gather_time} " +
            "WHERE asset_code = #{asset_code} AND monitor_name = #{monitor_name} ")
    int updateAssetMonitorCurrentByAssetIdAndMonitorName(Map<String, Object> pm);

    @Select("select * from ${schema_name}._sc_asset_monitor_current where asset_code = #{asset_code} AND monitor_name = #{monitor_name}")
    Map<String, Object> queryByAssetIdAndMonitorName(Map<String, Object> current);

    @Insert("INSERT INTO ${schema_name}._sc_asset_monitor_current (asset_code,monitor_name,monitor_value_type,monitor_value_unit,monitor_value,  " +
            "gather_time,alarm_up,alarm_down,exception_up,exception_down,alarm_msg,exception_msg,send_task,alarm_up_task_code,  " +
            "alarm_down_task_code,remark,source_serial_id,status,alarm_repair_code,create_time)  " +
            "VALUES  " +
            "(#{asset_code},#{monitor_name},#{monitor_value_type},#{monitor_value_unit},#{monitor_value},#{gather_time},  " +
            "#{alarm_up},#{alarm_down},#{exception_up},#{exception_down},#{alarm_msg},#{exception_msg},#{send_task},#{alarm_up_task_code},  " +
            "#{alarm_down_task_code},#{remark},#{source_serial_id},#{status},#{alarm_repair_code},#{create_time})")
    int insertAssetMonitorCurrent(Map<String, Object> current);

    @Delete("delete from ${schema_name}._sc_asset_monitor_current where asset_code = #{asset_code} AND monitor_name = #{monitor_name}")
    void deleteAssetMonitorCurrentByAssetIdAndMonitorName(Map<String, Object> current);
}
