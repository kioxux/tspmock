package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 字典处理
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
@Mapper
public interface SelectOptionsMapper {
    /**
     * 动态查询sql（列表）
     *
     * @param sql sql
     * @return 返回集合
     */
    @Select("${sql}")
    List<Map<String, Object>> findSelectOptionList(@Param("sql") String sql);

    /**
     * 动态查询sql（选中项）
     *
     * @param sql sql
     * @return 返回集合
     */
    @Select("${sql} limit 1 ")
    Map<String, Object> findSelectOption(@Param("sql") String sql);

//    /**
//     * 静态字典数据查询（已国际化）
//     *
//     * @param schemaName 数据库
//     * @param companyId  企业
//     * @param langKey    国际化类型
//     * @param dataType   字典类型
//     * @return 字典的对象形式
//     */
//    @Select("select t.reserve1,t.code as value, " +
//            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
//            "   coalesce((select (c.resource->>t.name)::jsonb->>'${langKey}' from public.company_resource c where c.company_id = #{companyId}), t.name) " +
//            " else t.name end as text, t.is_lang, t.parent_code AS parent  " +
//            "from ${schema_name}._sc_cloud_data t where t.data_type = #{dataType} " +
//            "order by t.data_type, t.data_order ")
//    List<Map<String, Object>> findLangStaticDataByType(@Param("schema_name") String schemaName, @Param("companyId") Long companyId, @Param("langKey") String langKey, @Param("dataType") String dataType);

    @Select("select t.reserve1,t.code as value, " +
            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
            " t.name else t.name end as text, t.is_lang, t.parent_code AS parent  " +
            "from ${schema_name}._sc_cloud_data t where t.data_type = #{dataType} " +
            "order by t.data_type, t.data_order ")
    List<Map<String, Object>> findLangStaticDataByType(@Param("schema_name") String schemaName, @Param("dataType") String dataType);

    /**
     * 静态字典数据查询（未国际化）
     *
     * @param schemaName 数据库
     * @param dataType   字典类型
     * @return 字典的对象形式
     */
    @Select("select t.code as value, t.name as text, t.is_lang ,t.reserve1,t.parent_code AS parent " +
            "from ${schema_name}._sc_cloud_data t where t.data_type = #{dataType} " +
            "order by t.data_type, t.data_order ")
    List<Map<String, Object>> findUnLangStaticDataByType(@Param("schema_name") String schemaName, @Param("dataType") String dataType);

//    /**
//     * 静态字典选中项查询（已国际化）
//     *
//     * @param schemaName 数据库
//     * @param companyId  企业
//     * @param langKey    国际化类型
//     * @param dataType   字典类型
//     * @param code       选中项值
//     * @return 选中项信息
//     */
//    @Select("select t.code as value, " +
//            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
//            "   coalesce((select (c.resource->>t.name)::jsonb->>'${langKey}' from public.company_resource c where c.company_id = #{companyId}), t.name) " +
//            " else t.name end as text, t.reserve1 " +
//            "from ${schema_name}._sc_cloud_data t where t.code = #{code} and t.data_type = #{dataType} ")
//    Map<String, Object> findLangStaticOption(@Param("schema_name") String schemaName, @Param("companyId") Long companyId, @Param("langKey") String langKey, @Param("dataType") String dataType, @Param("code") String code);

    @Select("select t.code as value, " +
            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
            " t.name else t.name end as text, t.reserve1 " +
            "from ${schema_name}._sc_cloud_data t where t.code = #{code} and t.data_type = #{dataType} ")
    Map<String, Object> findLangNeedStatic(@Param("schema_name") String schemaName, @Param("dataType") String dataType, @Param("code") String code);

    /**
     * 静态字典选中项查询（未国际化）
     *
     * @param schemaName 数据库
     * @param dataType   字典类型
     * @param code       选中项值
     * @return 选中项信息
     */
    @Select("select t.code as value, t.name as text, t.is_lang ,t.reserve1 " +
            "from ${schema_name}._sc_cloud_data t where t.code = #{code} and t.data_type = #{dataType} ")
    Map<String, Object> findUnLangStaticOption(@Param("schema_name") String schemaName, @Param("dataType") String dataType, @Param("code") String code);

//    @Select("SELECT field as text,field as value,2 as is_lang FROM (select jsonb_object_keys(resource) as field from public.company_resource WHERE company_id = #{company_id} ) t ${condition}")
//    List<Map<String, Object>> findResource(@Param("company_id") Long company_id, @Param("condition") String condition);

    /**
     * 根据业务id获取目标表列表
     *
     * @param schemaName 数据库
     * @param dataType   字典类型
     */
    @Select(" SELECT cd.code,cd.name,cd.is_lang,cd.create_time,cd.remark,cd.type_remark,cd.parent_code,cd.reserve1,cd.data_order  " +
            " from ${schema_name}._sc_cloud_data cd where cd.parent_code = #{parent_code} and cd.data_type = #{dataType} ")
    List<Map<String, Object>> findTargetTableList(@Param("schema_name") String schemaName, @Param("dataType") String dataType, @Param("parent_code") String parent_code);

    /**
     * 根据工单类型id查询工单类型信息
     *
     * @param schemaName 入参
     * @param id         入参
     * @return 工单类型信息
     */
    @Select(" SELECT wt.id,wt.type_name,wt.data_order,wt.is_use,wt.create_time,wt.create_user_id,wt.business_type_id" +
            " FROM ${schema_name}._sc_work_type AS wt WHERE  wt.id=#{id} ")
    Map<String, Object> findWorkTypeById(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 根据key和code获取静态字典参数数据
     *
     * @param schemaName 数据库
     * @param code       选中项
     * @return 根据key和code获取静态字典参数数据
     */
    @Select("select t.code as value, t.name as text, t.is_lang, t.reserve1 " +
            "from ${schema_name}._sc_cloud_data t where t.parent_code = #{code} and t.data_type = 'interface_address_param' ")
    List<Map<String, Object>> findSelectOptionInfoByKeyAndCode(@Param("schema_name") String schemaName, @Param("code") String code);

    /**
     * 聚合json字段数据
     *
     * @param schemaName        入参
     * @param tableName         入参
     * @param keyName           数据主键名称
     * @param keyValue          数据主键值
     * @param columnName        数据json字段名称
     * @param jsonColumnName    json待处理字段名称
     * @param jsonColumnValue   json待处理字段值
     * @param polymerizationStr 聚合类型
     * @param fieldColumnName   聚合字段名
     */
    @Select("   SELECT CAST(${polymerizationStr}(to_number(j->>'${fieldColumnName}','9999')) AS DECIMAL(10,2)) from ( " +
            "       SELECT jsonb_array_elements(${columnName}) j " +
            "       from ${schema_name}.${tableName} a " +
            "       where a.${keyName} = #{keyValue} " +
            "   ) t where (j->>#{jsonColumnName})=#{jsonColumnValue} and (j->>#{fieldColumnName}) IS NOT NULL and (j->>#{fieldColumnName})<>'' ")
    Double selectPolymerizationJsonColumnData(@Param("schema_name") String schemaName, @Param("tableName") String tableName,
                                              @Param("keyName") String keyName, @Param("keyValue") String keyValue, @Param("columnName") String columnName,
                                              @Param("jsonColumnName") String jsonColumnName, @Param("jsonColumnValue") String jsonColumnValue,
                                              @Param("polymerizationStr") String polymerizationStr, @Param("fieldColumnName") String fieldColumnName);

//    /**
//     * 静态字典数据查询
//     *
//     * @param schemaName  数据库
//     * @param data_type   字典类型
//     * @param parent_code 父节点类型
//     * @return 字典的对象形式
//     */
//    @Select("select t.code as value,t.reserve1 as relation, " +
//            "case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
//            "coalesce((select (c.resource->>t.name)::jsonb->>'${langKey}' from public.company_resource c where c.company_id = #{companyId}), t.name) " +
//            "else t.name end as text, t.is_lang, t.reserve1 " +
//            "from ${schema_name}._sc_cloud_data t where t.data_type = #{data_type} AND parent_code = #{parent_code} " +
//            "order by t.data_type, t.data_order")
//    List<Map<String, Object>> findSpecialPropertyKey(@Param("schema_name") String schemaName, @Param("data_type") String data_type, @Param("parent_code") String parent_code, @Param("langKey") String langKey, @Param("companyId") Long company_id);

    @Select("select t.code as value,t.reserve1 as relation, " +
            "case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
            "t.name " +
            "else t.name end as text, t.is_lang, t.reserve1 " +
            "from ${schema_name}._sc_cloud_data t where t.data_type = #{data_type} AND parent_code = #{parent_code} " +
            "order by t.data_type, t.data_order")
    List<Map<String, Object>> findSpecialPropertyKey(@Param("schema_name") String schemaName, @Param("data_type") String data_type, @Param("parent_code") String parent_code);


//    @Select("SELECT   data_type as value, " +
//            "case when is_lang = '" + Constants.STATIC_LANG + "' then " +
//            "coalesce((select (c.resource->>data_type_name)::jsonb->>'${langKey}' from public.company_resource c where c.company_id = #{companyId}), data_type_name) " +
//            "else data_type_name end as text , is_lang FROM ${schema_name}._sc_cloud_data_type ORDER BY create_time desc ")
//    List<Map<String, Object>> findAllCloudDataType(@Param("schema_name") String schemaName, @Param("langKey") String langKey, @Param("companyId") Long company_id);

    @Select("SELECT   data_type as value, " +
            "case when is_lang = '" + Constants.STATIC_LANG + "' then " +
            "data_type_name " +
            "else data_type_name end as text , is_lang FROM ${schema_name}._sc_cloud_data_type ORDER BY create_time desc ")
    List<Map<String, Object>> findAllCloudDataType(@Param("schema_name") String schemaName);


//    @Select(" SELECT" +
//            " coalesce((select (c.resource->>t.field)::jsonb->>2 from public.company_resource c where c.company_id = #{companyId}), t.field) as text,field as value, 2 is_lang " +
//            " FROM (SELECT field ,2 as is_lang FROM (select jsonb_object_keys(resource) as field from public.company_resource WHERE company_id = #{companyId} ) t where t.field like CONCAT('%', 'log', '%')) t")
//    List<Map<String, Object>> findWorksLog(@Param("schema_name") String schemaName, @Param("companyId") Long company_id);


    /**
     * 根据设备类型 工单类型  设备位置查询维修工
     *
     * @param schemaName    入参
     * @param position_code 入参
     * @param category_id   入参
     * @param work_type_id  入参
     * @return 维修工
     */
    @Select(" <script>" +
            " SELECT DISTINCT u.ID AS value ,u.user_name AS text" +
            " ,(SELECT count(1) FROM ${schema_name}._sc_works_detail d where d.status &gt; " + StatusConstant.DRAFT + " AND d.status &lt; " + StatusConstant.COMPLETED + " AND d.receive_user_id = u.id) as work " +
            " FROM ${schema_name}._sc_user AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON pr.role_id = rap.role_id " +
            " WHERE u.status = 1  AND u.is_use = '1'  AND rap.asset_position_code =#{position_code} " +
            " INTERSECT " +
            " SELECT DISTINCT u.ID  AS value,u.user_name  AS text " +
            " ,(SELECT count(1) FROM ${schema_name}._sc_works_detail d where d.status &gt; " + StatusConstant.DRAFT + " AND d.status &lt; " + StatusConstant.COMPLETED + " AND d.receive_user_id = u.id) as work " +
            " FROM ${schema_name}._sc_user AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON pr.role_id = rwt.role_id " +
            " WHERE u.status = 1 AND u.is_use = '1'  AND rwt.work_type_id =#{work_type_id} " +
            " <when test='category_id!=null'>" +
            " INTERSECT" +
            " SELECT DISTINCT u.ID  AS value, u.user_name  AS text " +
            " ,(SELECT count(1) FROM ${schema_name}._sc_works_detail d where d.status &gt; " + StatusConstant.DRAFT + " AND d.status &lt; " + StatusConstant.COMPLETED + " AND d.receive_user_id = u.id) as work " +
            " FROM ${schema_name}._sc_user  AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON pr.role_id = rat.role_id " +
            " WHERE u.status = 1 AND u.is_use = '1' AND rat.category_id =#{category_id}" +
            " </when> " +
            " </script>")
    List<Map<String, Object>> findReceiveUserWithPermission(@Param("schema_name") String schemaName, @Param("position_code") String position_code, @Param("category_id") Integer category_id, @Param("work_type_id") Integer work_type_id);

    /**
     * 根据值获取状态表详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 状态表详情
     */
    @Select("SELECT id,status AS name FROM ${schema_name}._sc_status WHERE id = #{value}::int ")
    Map<String, Object> findStatusInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 根据厂商表详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 状态表详情
     */
    @Select("SELECT id,short_title AS name FROM ${schema_name}._sc_facilities WHERE id = #{value}::int ")
    Map<String, Object> findFacilitiesInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 单位详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 单位详情
     */
    @Select("SELECT id,unit_name AS name FROM ${schema_name}._sc_unit WHERE id = #{value}::int ")
    Map<String, Object> findUnitInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 设备类型详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 状态表详情
     */
    @Select("SELECT id,category_name AS name FROM ${schema_name}._sc_asset_category WHERE id = #{value}::int ")
    Map<String, Object> findCategoryInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 重要级别详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 状态表详情
     */
    @Select("SELECT id,level_name AS name FROM ${schema_name}._sc_importment_level WHERE id = #{value}::int ")
    Map<String, Object> findImportmentInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 货币单位详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 货币单位详情
     */
    @Select("SELECT id,currency_name AS name FROM ${schema_name}._sc_currency WHERE id = #{value}::int ")
    Map<String, Object> findCurrencyInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 设备位置详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 设备位置详情
     */
    @Select("SELECT position_code AS id,position_name AS name FROM ${schema_name}._sc_asset_position WHERE position_code = #{value} ")
    Map<String, Object> findAssetPositionInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 设备型号详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 设备位置详情
     */
    @Select("SELECT id,model_name AS name FROM ${schema_name}._sc_asset_model WHERE id = #{value}::int ")
    Map<String, Object> findAssetModelInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 设备型号详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 设备位置详情
     */
    @Select("SELECT id,running_status AS name FROM ${schema_name}._sc_asset_running_status WHERE id = #{value}::int ")
    Map<String, Object> findRunningStatusInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 设备状态详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 设备位置详情
     */
    @Select("SELECT id,status AS name FROM ${schema_name}._sc_asset_status WHERE id = #{value}::int ")
    Map<String, Object> findAssetStatusInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);

    /**
     * 数据字典详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 数据字典详情
     */
    @Select("SELECT code AS id,name, reserve1 FROM ${schema_name}._sc_cloud_data WHERE data_type = #{data_type} AND code = #{value} ")
    Map<String, Object> findCloudDataInfoByValue(@Param("schema_name") String schemaName, @Param("data_type") String data_type, @Param("value") String value);

    /**
     * 数据字典详情
     *
     * @param schemaName 入参
     * @param value      入参
     * @return 数据字典详情
     */
    @Select("SELECT id,file_original_name AS name FROM ${schema_name}._sc_files WHERE id = #{value}::int ")
    Map<String, Object> findFilesInfoByValue(@Param("schema_name") String schemaName, @Param("value") String value);


//    @Select("SELECT " +
//            "coalesce((select (c.resource->>t.field)::jsonb->>#{user_lang} from public.company_resource c where c.company_id = #{company_id}), t.field) as text,field as value, 2 is_lang " +
//            "FROM (SELECT field ,2 as is_lang FROM (select jsonb_object_keys(resource) as field from public.company_resource WHERE company_id = #{company_id} ) t ${condition}) t")
//    List<Map<String, Object>> findResourceForLog(@Param("company_id") Long company_id, @Param("user_lang") String user_lang, @Param("condition") String condition);

    /**
     * 设备列表查询
     *
     * @param schemaName   入参
     * @param selectExtKey 入参
     * @param category_id  入参
     * @return 设备列表
     */
    @Select(" <script>" +
            " SELECT distinct running_status_id ,id AS id,asset_name AS text , id as asset_id, asset_name ,category_id::varchar AS asset_category_id FROM ${schema_name}._sc_asset" +
            " WHERE  status > " + StatusConstant.STATUS_DELETEED +
            " <when test='selectExtKey!=null'>" +
            " AND ( asset_code LIKE  CONCAT('%', #{selectExtKey}, '%') " +
            "      OR asset_name LIKE  CONCAT('%', #{selectExtKey}, '%') ) " +
            " </when> " +
            "<when test='category_id!=null'>" +
            " AND category_id = #{category_id} " +
            " </when> " +
            "</script>")
    List<Map<String, Object>> findAssetForSearch(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey, @Param("category_id") Integer category_id);

    /**
     * 可借出的设备列表
     *
     * @param schemaName   入参
     * @param selectExtKey 入参
     * @param category_id  入参
     * @return 可归还的设备列表
     */
    @Select(" <script>" +
            " SELECT distinct running_status_id ,id AS id,asset_name AS text , id as asset_id, asset_name ,category_id::varchar AS asset_category_id FROM ${schema_name}._sc_asset" +
            " WHERE status BETWEEN 1 AND  2  " +
            " <when test='selectExtKey!=null'>" +
            " AND ( asset_code LIKE  CONCAT('%', #{selectExtKey}, '%') " +
            "      OR asset_name LIKE  CONCAT('%', #{selectExtKey}, '%') ) " +
            " </when> " +
            "<when test='category_id!=null'>" +
            " AND category_id = #{category_id} " +
            " </when> " +
            "</script>")
    List<Map<String, Object>> findAssetOutForSearch(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey, @Param("category_id") Integer category_id);

    /**
     * 可归还的设备列表
     *
     * @param schemaName   入参
     * @param selectExtKey 入参
     * @param category_id  入参
     * @param bak_value1   入参
     * @return 可归还的设备列表
     */
    @Select(" <script>" +
            " SELECT A.running_status_id,A.ID AS ID,A.asset_name AS TEXT,A.ID AS asset_id,A.asset_name,A.category_id :: VARCHAR AS asset_category_id," +
            " awd.sub_work_code AS out_code,aw.bak_value1 " +
            " FROM ${schema_name}._sc_asset_works_detail_item awdi" +
            " LEFT JOIN ${schema_name}._sc_asset_works_detail AS awd ON awd.sub_work_code = awdi.sub_work_code" +
            " LEFT JOIN ${schema_name}._sc_asset_works aw ON aw.work_code = awd.work_code" +
            " LEFT JOIN ${schema_name}._sc_asset A ON A.ID = awdi.asset_id" +
            " INNER JOIN (" +
            " SELECT MAX( awdi.sub_work_code ) AS sub_work_code,A.ID " +
            " FROM ${schema_name}._sc_asset_works_detail_item awdi" +
            " LEFT JOIN ${schema_name}._sc_asset A ON A.ID = awdi.asset_id" +
            " LEFT JOIN ${schema_name}._sc_asset_works_detail AS awd ON awd.sub_work_code = awdi.sub_work_code " +
            " WHERE" +
            " A.status = 3 " +
            " AND awd.status = 60 " +
            " AND awd.is_main = 1 " +
            " GROUP BY" +
            " A.ID " +
            " ) AS t2 ON t2.ID = A.ID " +
            " AND t2.sub_work_code = awdi.sub_work_code " +
            " WHERE a.status =  3 AND awd.status = 60 AND awd.is_main = 1 " +
            " <when test='selectExtKey!=null'>" +
            " AND ( a.asset_code LIKE  CONCAT('%', #{selectExtKey}, '%') " +
            "       OR a.asset_name LIKE  CONCAT('%', #{selectExtKey}, '%') ) " +
            " </when> " +
            " <when test='category_id!=null'>" +
            "  AND a.category_id = #{category_id} " +
            " </when> " +
            " <when test='bak_value1!=null'>" +
            "  AND aw.bak_value1 = #{bak_value1} " +
            " </when> " +
            " <when test='out_code!=null'>" +
            "  AND aw.work_code = #{out_code} " +
            " </when>" +
            " </script>")
    List<Map<String, Object>> findAssetInputForSearch(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey, @Param("category_id") Integer category_id, @Param("bak_value1") String bak_value1, @Param("out_code") String out_code);

    /**
     * 设备调拨设备列表
     *
     * @param schemaName    入参
     * @param selectExtKey  入参
     * @param category_id   入参
     * @param position_code 入参
     * @return 设备列表
     */
    @Select(" <script>" +
            " SELECT distinct running_status_id ,id AS id,asset_name AS text , id as asset_id, asset_name," +
            " category_id::varchar AS asset_category_id FROM ${schema_name}._sc_asset" +
            " WHERE status > " + StatusConstant.STATUS_DELETEED +
            " <when test='selectExtKey!=null'>" +
            " AND ( asset_code LIKE  CONCAT('%', #{selectExtKey}, '%') " +
            "      OR asset_name LIKE  CONCAT('%', #{selectExtKey}, '%') ) " +
            " </when> " +
            "<when test='category_id!=null'>" +
            " AND category_id = #{category_id} " +
            " </when> " +
            "<when test='position_code!=null'>" +
            " AND position_code = #{position_code} " +
            " </when> " +
            "</script>")
    List<Map<String, Object>> findAssetAllocationForSearch(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey, @Param("category_id") Integer category_id, @Param("position_code") String position_code);


//    @Select("SELECT DISTINCT parent_id as parent, f.id as value, short_title as text,f.org_type_id,f.inner_code, " +
//            " case when cd.is_lang = '" + Constants.STATIC_LANG + "' then coalesce((select (c.resource->>cd.name)::jsonb->>#{langKey} from public.company_resource c where c.company_id = #{companyId}), cd.name) else cd.name end as org_type_name " +
//            "FROM ${schema_name}._sc_facilities f " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
////            "JOIN ( SELECT DISTINCT org_id FROM ${schema_name}._sc_asset_organization) ao ON f.id = ao.org_id " +
//            "JOIN ( SELECT DISTINCT facility_id FROM ${schema_name}._sc_works_detail_asset_org) ao ON f.id = ao.facility_id " +
//            "where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED +
//            " ORDER BY f.id ")
//    List<Map<String, Object>> findAssetOrg(@Param("schema_name") String schemaName, @Param("companyId") Long companyId, @Param("langKey") String langKey);

    @Select("SELECT DISTINCT parent_id as parent, f.id as value, short_title as text,f.org_type_id,f.inner_code, " +
            " case when cd.is_lang = '" + Constants.STATIC_LANG + "' then cd.name else cd.name end as org_type_name " +
            "FROM ${schema_name}._sc_facilities f " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
//            "JOIN ( SELECT DISTINCT org_id FROM ${schema_name}._sc_asset_organization) ao ON f.id = ao.org_id " +
            "JOIN ( SELECT DISTINCT facility_id FROM ${schema_name}._sc_works_detail_asset_org) ao ON f.id = ao.facility_id " +
            "where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED +
            " ORDER BY f.id ")
    List<Map<String, Object>> findAssetOrg(@Param("schema_name") String schemaName);

    /**
     * //设备扫码
     *
     * @param schemaName
     * @param selectExtKey
     * @return
     */
    @Select(" <script>" +
            " SELECT  t1.asset_code as relation,t1.asset_code,t1.category_id,t1.position_code, " +
            "  t1.currency_id as currency_id, cur.currency_code as currency_name, t1.id as value, t1.asset_name || '(' || t1.asset_code || ')' as text, t1.asset_name " +
            " FROM  ${schema_name}._sc_asset t1 " +
            " LEFT JOIN ${schema_name}._sc_currency cur ON t1.currency_id = cur.id " +
            " WHERE t1.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='selectExtKey!=null'>" +
            " and( t1.id = #{selectExtKey} or t1.asset_code = #{selectExtKey} )" +
            " </when> " +
            " ORDER BY t1.status  ASC " +
            " </script> ")
    List<Map<String, Object>> findAssetInfoForByScan(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey);

    @Select(" <script>" +
            " SELECT  t1.asset_code as relation,t1.asset_code,t1.category_id,t1.position_code, " +
            "  t1.currency_id as currency_id, cur.currency_code as currency_name, t1.id as value, t1.asset_name || '(' || t1.asset_code || ')' as text, t1.asset_name " +
            " FROM  ${schema_name}._sc_asset t1 " +
            " LEFT JOIN ${schema_name}._sc_currency cur ON t1.currency_id = cur.id " +
            " WHERE t1.status > " + StatusConstant.STATUS_DELETEED + "and t1.position_code = #{position_code}" +
            " <when test='selectExtKey!=null'>" +
            " and( t1.id = #{selectExtKey} or t1.asset_code = #{selectExtKey} )" +
            " </when> " +
            " <when test='category_id != null and category_id != \"\"'> " +
            " and t1.category_id in" +
            "   <foreach collection='category_id.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </when> " +
            " ORDER BY t1.status  ASC " +
            " </script> ")
    List<Map<String, Object>> findAssetInfoForByScanCondition(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey, @Param("category_id") String categoryId, @Param("position_code") String positionCode);

    /**
     * 设备位置扫码
     *
     * @param schemaName
     * @param selectExtKey
     * @return
     */
    @Select(" <script>" +
            " SELECT DISTINCT position_code AS value,status,file_id,create_time,remark,create_user_id," +
            " layer_path,position_type_id,parent_code,position_name AS text " +
            " FROM  ${schema_name}._sc_asset_position " +
            " WHERE status > " + StatusConstant.STATUS_DELETEED +
            " <when test='selectExtKey!=null'>" +
            " AND position_code = #{selectExtKey} " +
            " </when> " +
            " </script> ")
    List<Map<String, Object>> findAssetPositionInfoForByScan(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey);

    /**
     * 备件扫码
     *
     * @param schemaName
     * @param bomId
     * @param stockId
     * @return
     */
    @Select(" <script>" +
            " SELECT b.ID AS VALUE,b.bom_name AS TEXT,COALESCE(bs.show_price, b.show_price) price,b.material_code,b.type_id,b.bom_model,b.manufacturer_id,b.unit_id as unit," +
            " b.currency_id,bs.stock_id," +
            " COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) AS supplier_id,COALESCE (CASE WHEN POSITION ( ',' IN COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) ) > 0 THEN SUM ( bs.quantity ) / 2 ELSE SUM ( bs.quantity ) END,0) now_quantity," +
            " COALESCE(bs.security_quantity, 0) security_quantity FROM ${schema_name}._sc_bom b " +
            " LEFT JOIN ${schema_name}._sc_bom_stock bs on bs.bom_id = b.id AND bs.stock_id = #{stock_id} " +
            " LEFT JOIN ${schema_name}._sc_bom_supplies sup ON b.ID = sup.bom_id" +
            " WHERE b.is_use = '1' AND b.status = 1 AND b.ID = #{bom_id}" +
            " GROUP BY b.ID,bs.show_price,bs.stock_id,bs.security_quantity" +
            " ORDER BY b.ID"+
            " </script> ")
    Map<String, Object> findBomInfoForByScan(@Param("schema_name") String schemaName, @Param("bom_id") String bomId, @Param("stock_id") Integer stockId);

    /**
     * 备件扫码根据备件id,库房id
     *
     * @param schemaName
     * @param bomId
     * @param stockId
     * @return
     */
    @Select(" <script>" +
            " SELECT DISTINCT b.ID AS VALUE,b.bom_name AS TEXT,b.show_price AS price,b.material_code,b.type_id,b.bom_model,b.manufacturer_id,b.unit_id,b.currency_id,bs.stock_id,bs.quantity now_quantity," +
            " COALESCE(bs.security_quantity, 0) security_quantity,COALESCE(bs.show_price, b.show_price) price FROM ${schema_name}._sc_bom b " +
            " LEFT JOIN ${schema_name}._sc_bom_stock bs on bs.bom_id = b.id " + SqlConstant.BOM_AUTH_SQL +
            " WHERE b.is_use = '1' AND b.status = 1 AND b.ID = #{bom_id} AND bs.stock_id = #{stock_id}" +
            " ORDER BY b.ID"+
            " </script>")
    Map<String, Object> findBomInfoForByScanAndStockId(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("bom_id") String bomId, @Param("stock_id") Integer stockId);

    @Select("SELECT DISTINCT b.id as value, " +
            " b.bom_name || '(' || b.material_code || ')' as text, " +
            " COALESCE( bs.show_price,b.show_price) AS price,b.material_code,b.type_id,b.bom_model,b.unit_id as unit,b.currency_id,b.manufacturer_id," +
            " COALESCE (bs.quantity, 0) AS now_quantity," +
            " bs.stock_id, COALESCE(bs.security_quantity, 0) security_quantity " +
            " FROM ${schema_name}._sc_bom b " +
            " LEFT JOIN ${schema_name}._sc_bom_stock bs on bs.bom_id=b.id and bs.stock_id = #{stock_id}" +
            " WHERE b.is_use = '1' AND b.status = 1 " +
            " ORDER BY b.ID ")
    List<Map<String, Object>> findBomListByBomIdAndStockId(@Param("schema_name") String schemaName, @Param("stock_id") Integer stockId);


    @Select("<script> " +
            "select tab.* from (SELECT b.id as value, " +
            " b.bom_name || '(' || b.material_code || ')' as text, " +
            " COALESCE( bs.show_price,b.show_price) AS price,b.material_code,b.type_id,b.bom_model,b.unit_id as unit,b.currency_id," +
            " COALESCE (CASE WHEN POSITION ( ',' IN COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) ) > 0 THEN SUM ( bs.quantity ) / 2 ELSE SUM ( bs.quantity ) END,0) AS now_quantity," +
            " COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT b.manufacturer_id ), ',' ), '' ) AS manufacturer_id, " +
            " COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) AS supplier_id, " +
            " bs.stock_id, COALESCE(bs.security_quantity, 0) security_quantity " +
            " FROM ${schema_name}._sc_bom b " +
            " LEFT JOIN ${schema_name}._sc_bom_stock bs on bs.bom_id=b.id and bs.stock_id = #{stock_id}" +
            " LEFT JOIN ${schema_name}._sc_bom_supplies sup ON b.ID = sup.bom_id" +
            " WHERE b.is_use = '1' AND b.status = 1 " +
            " GROUP BY b.ID,bs.show_price,bs.stock_id,bs.security_quantity) tab " +
            " <where> " +
            " <if test=\"supplierId != null and supplierId != ''\"> " +
            "   <foreach collection='supplierId.split(\",\")' item='item' open='(to_tsvector(tab.supplier_id) @@ to_tsquery' close=')' separator='or to_tsvector(tab.supplier_id) @@ to_tsquery'> " +
            "     (#{item})" +
            "   </foreach>" +
            " </if>" +
            " <if test=\"manufacturerId != null and manufacturerId != ''\"> " +
            " and " +
            "   <foreach collection='manufacturerId.split(\",\")' item='item' open='(to_tsvector(tab.manufacturer_id) @@ to_tsquery' close=')' separator='or to_tsvector(tab.manufacturer_id) @@ to_tsquery'> " +
            "   (#{item}) " +
            "   </foreach>" +
            " </if> " +
            " </where>" +
            "</script>")
    List<Map<String, Object>> findBomListByBomIdAndSupId(@Param("schema_name") String schemaName, @Param("stock_id") Integer stockId, @Param("supplierId") String supplier_id, @Param("manufacturerId") String manufacturer_id);

    @Select("<script> " +
            "select tab.* from (SELECT b.id as value, " +
            " b.bom_name as text,b.id as bom_id, b.bom_name, " +
            " COALESCE( bs.show_price,b.show_price) AS price,b.material_code,b.type_id as bom_type_id,b.bom_model,b.unit_id as unit,b.currency_id," +
            " COALESCE (CASE WHEN POSITION ( ',' IN COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) ) > 0 THEN SUM ( bs.quantity ) / 2 ELSE SUM ( bs.quantity ) END,0) AS now_quantity," +
            " COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT b.manufacturer_id ), ',' ), '' ) AS manufacturer_id, " +
            " COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) AS supplier_id, " +
            " bs.stock_id, COALESCE(bs.security_quantity, 0) security_quantity " +
            " FROM ${schema_name}._sc_bom b " +
            " JOIN ${schema_name}._sc_bom_stock bs on bs.bom_id=b.id" +
            " LEFT JOIN ${schema_name}._sc_bom_supplies sup ON b.ID = sup.bom_id" +
            " WHERE b.is_use = '1' AND b.status = 1 AND bs.stock_id = #{stock_id} " +
            " GROUP BY b.ID,bs.show_price,bs.stock_id,bs.security_quantity) tab " +
            " <where> " +
            " <if test=\"bomTypeId != null and bomTypeId != ''\"> " +
            "   and " +
            "   <foreach collection='bomTypeId.split(\",\")' item='item' open='(to_tsvector(tab.bom_type_id ::text) @@ to_tsquery' close=')' separator='or to_tsvector(tab.bom_type_id ::text) @@ to_tsquery'> " +
            "   (#{item}) " +
            "   </foreach>" +
            " </if>" +
            " <if test=\"supplierId != null and supplierId != ''\"> " +
            "   and " +
            "   <foreach collection='supplierId.split(\",\")' item='item' open='(to_tsvector(tab.supplier_id) @@ to_tsquery' close=')' separator='or to_tsvector(tab.supplier_id) @@ to_tsquery'> " +
            "     (#{item})" +
            "   </foreach>" +
            " </if>" +
            " <if test=\"manufacturerId != null and manufacturerId != ''\"> " +
            " and " +
            "   <foreach collection='manufacturerId.split(\",\")' item='item' open='(to_tsvector(tab.manufacturer_id) @@ to_tsquery' close=')' separator='or to_tsvector(tab.manufacturer_id) @@ to_tsquery'> " +
            "   (#{item}) " +
            "   </foreach>" +
            " </if> " +
            " <if test=\"keywordSearch != null and keywordSearch != ''\"> " +
            "   and lower(tab.bom_name) like concat('%', lower(#{keywordSearch}),'%') " +
            " </if>" +
            " </where>" +
            "</script>")
    List<Map<String, Object>> findStockBomList(@Param("schema_name") String schemaName, @Param("keywordSearch") String keywordSearch, @Param("stock_id") Integer stockId, @Param("bomTypeId") String bom_type_id, @Param("supplierId") String supplier_id, @Param("manufacturerId") String manufacturer_id);

    /**
     * 根据备件类型选择建卡备件列表
     *
     * @param schemaName
     * @param selectExtKey
     * @return
     */
    @Select("<script> " +
            "SELECT b.id,b.bom_name AS text,b.material_code,b.type_id,b.bom_model,b.code_classification " +
            "FROM ${schema_name}._sc_bom b " +
            "WHERE b.is_use = '1' AND b.status = 1 " +
            " <when test=\"selectExtKey!=null and selectExtKey!= ''\"> " +
            " AND b.type_id = #{selectExtKey}::int " +
            " </when> " +
            "ORDER BY b.ID " +
            " </script> ")
    List<Map<String, Object>> findBomListWithType(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey);

    @Select("SELECT code as value ,name as text ,is_lang  FROM ${schema_name}._sc_cloud_data WHERE data_type = 'scada_config' and parent_code = 'scada_config' ORDER BY data_order")
    List<Map<String, Object>> findScadaConfig(@Param("schema_name") String schemaName);

    /**
     * 设备盘点位置列表
     *
     * @param schemaName   入参
     * @param selectExtKey 入参
     * @return 设备盘点位置列表
     */
    @Select(" <script>" +
            " select tab.VALUE,tab.TEXT,tab.asset_id,tab.position_code,tab.position_name,tab.remark,tab.position_code as relation_id," +
            " tab.inventory_user_id,COALESCE ( COUNT ( tab.ID ), 0 ) as stock_count,ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT tab.category_id ), ',' ) as category_id " +
            " from (select tb.*,inv.user_id as inventory_user_id" +
            " from (select asp.*,asst.id,asst.category_id from ( " +
            " SELECT position_code  AS value, position_name AS text,position_code AS asset_id,position_code,position_name,remark " +
            " FROM ${schema_name}._sc_asset_position " +
            " WHERE status > " + StatusConstant.STATUS_DELETEED +") asp " +
            " LEFT JOIN ( SELECT t1.* FROM ${schema_name}._sc_asset t1 " +
            " INNER JOIN ( SELECT DISTINCT rat.category_id,rap.asset_position_code FROM ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur ON ur.ID = pr.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_type rat ON pr.role_id = rat.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_position rap ON pr.role_id = rap.role_id " +
            " WHERE up.user_id = #{userId} ) prm ON t1.category_id = prm.category_id " +
            " AND t1.position_code = prm.asset_position_code " +
            " <where> " +
            " t1.status > - 1000 " +
            " <if test=\"categoryId != null and categoryId != ''\"> " +
            "   and t1.category_id IN " +
            "   <foreach collection='categoryId.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </if>" +
            "</where> " +
            " ) asst ON asp.asset_id = asst.position_code " +
            " INNER JOIN ( " +
            " SELECT DISTINCT s.asset_position_code FROM " +
            " ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur ON ur.ID = pr.role_id " +
            " LEFT JOIN ${schema_name}._sc_role_work_type wt ON ur.ID = wt.role_id " +
            " LEFT JOIN ${schema_name}._sc_role_asset_position s ON ur.ID = s.role_id " +
            " WHERE up.user_id = #{userId} AND s.asset_position_code IS NOT NULL ) P " +
            " ON asp.asset_id = P.asset_position_code) tb"+
            " LEFT JOIN (SELECT DISTINCT " +
            " ( ARRAY_AGG ( up.user_id ) ) [ 1 ] user_id,s.asset_position_code" +
            " FROM ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur ON ur.ID = pr.role_id " +
            " LEFT JOIN ${schema_name}._sc_role_work_type wt ON ur.ID = wt.role_id " +
            " LEFT JOIN ${schema_name}._sc_role_asset_position s ON ur.ID = s.role_id " +
            " WHERE s.asset_position_code IS NOT NULL " +
            " AND ur.ID = #{dealRoleId} " +
            " GROUP BY s.asset_position_code) inv ON inv.asset_position_code = tb.asset_id ) tab " +
            " <where> " +
            " <when test=\"selectExtKey!=null and selectExtKey!= ''\"> " +
            " AND ( upper(tab.position_code) LIKE  upper(CONCAT('%', #{selectExtKey}, '%')) " +
            " OR    upper(tab.position_name) LIKE  upper(CONCAT('%', #{selectExtKey}, '%')) ) " +
            " </when>" +
            " </where>"+
            " GROUP BY tab.asset_id, tab.VALUE,tab.TEXT,tab.position_code,tab.position_name,tab.remark,tab.inventory_user_id" +
            " </script>")
    List<Map<String, Object>> findAssetPositionListWithInventory(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey, @Param("userId") String userId, @Param("categoryId") String categoryId, @Param("dealRoleId") String dealRoleId);

    /**
     * 设备盘点位置设备列表
     *
     * @param schemaName
     * @param selectExtKey
     * @return
     */
    @Select("<script>" +
            " SELECT '0' AS profit_and_loss, t1.id AS asset_id, t1.asset_icon,t1.asset_code,t1.asset_code as relation, t1.category_id,t1.position_code,t1.position_code as relation_id, " +
            "  t1.currency_id as currency_id,t1.id as value, t1.asset_name || '(' || t1.asset_code || ')' as text, t1.asset_name " +
            " FROM ${schema_name}._sc_asset AS t1" +
            " WHERE  t1.status > " + StatusConstant.STATUS_DELETEED +
            " AND t1.position_code = #{position_code}" +
            " <if test=\"category_id != null and category_id != ''\"> " +
            "   and t1.category_id IN " +
            "   <foreach collection='category_id.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </if> " +
            " <if test=\"selectExtKey != null and selectExtKey != ''\"> " +
            "   and upper(t1.asset_name) like upper('%' || #{selectExtKey} || '%') " +
            " </if> " +
            " </script>")
    List<Map<String, Object>> findAssetListByPositionWithInventory(@Param("schema_name") String schemaName, @Param("position_code") String relation_id, @Param("category_id") String category_id, @Param("selectExtKey") String selectExtKey);

    /**
     * @param schemaName
     * @param selectExtKey
     * @return
     */
    @Select(" SELECT field_value FROM ${schema_name}._sc_works_detail_column wdc " +
            " LEFT JOIN ${schema_name}._sc_bom_works_detail wd ON wd.sub_work_code = wdc.sub_work_code " +
            "  where  field_code = 'bom_write_buy' AND wd.work_code =#{selectExtKey} ")
    String findBomNeedByList(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey);

    /**
     * 同意的备件采购需求编码列表
     *
     * @param schemaName
     * @return
     */
    @Select(" SELECT bw.work_code AS value,bw.work_code AS text" +
            " FROM ${schema_name}._sc_bom_works AS bw" +
            " LEFT JOIN ${schema_name}._sc_work_type wt ON bw.work_type_id = wt.ID " +
            " WHERE wt.business_type_id = 3008 " +
            "       AND bw.status = 60 ")
    List<Map<String, Object>> findBomNeedWorkCodes(@Param("schema_name") String schemaName);

    /**
     * 查询设备基本字段信息
     *
     * @param schemaName 数据库
     * @return 字段信息
     */
    @Select("select t.reserve1 from ${schema_name}._sc_cloud_data t " +
            "where (t.data_type = 'assetExportFieldNames' or t.data_type = 'assetFieldNames') and t.reserve1 is not null order by data_order")
    List<String> findAssetCommonFieldList(@Param("schema_name") String schemaName);


    /**
     * 获取根节点设备位置名称列表
     *
     * @param schemaName 数据库
     * @return 根节点设备位置名称列表
     */
    @Select("<script>SELECT ( SELECT root_name FROM ( WITH RECURSIVE re AS ( SELECT t.position_code, t.position_name, t.parent_code " +
            "   FROM ${schema_name}._sc_asset_position t WHERE position_code = d.position_code " +
            "   UNION ALL " +
            "   SELECT t2.position_code, t2.position_name, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e " +
            "   WHERE e.parent_code = t2.position_code ) select position_name as root_name from re WHERE parent_code is null " +
            ") t ) as text, d.position_code as value " +
            "FROM ${schema_name}._sc_asset_position d </script> ")
    List<Map<String, Object>> getRootAssetPositionName(@Param("schema_name") String schemaName);

    /**
     * 获取根节点设备位置名称列表
     *
     * @param schemaName 数据库
     * @return 根节点设备位置名称列表
     */
    @Select("<script>SELECT ( SELECT root_name FROM ( WITH RECURSIVE re AS ( SELECT t.position_code, t.position_name, t.parent_code, t.position_type_id " +
            "   FROM ${schema_name}._sc_asset_position t WHERE position_code = d.position_code " +
            "   UNION ALL " +
            "   SELECT t2.position_code, t2.position_name, t2.parent_code, t2.position_type_id FROM ${schema_name}._sc_asset_position t2, re e " +
            "   WHERE e.parent_code = t2.position_code ) select position_name as root_name from re, ${schema_name}._sc_facility_type f WHERE parent_code is not null and position_type_id = f.id and f.type_name = '组织' " +
            ") t ) as text, d.position_code as value " +
            "FROM ${schema_name}._sc_asset_position d </script> ")
    List<Map<String, Object>> getSecondAssetPositionName(@Param("schema_name") String schemaName);


    /**
     * 根据设备位置查询设备列表（多选）
     *
     * @param schemaName 数据库
     * @param codes      设备位置
     * @return 数据列表
     */
    @Select(" <script> SELECT id AS value, asset_name AS text FROM ${schema_name}._sc_asset" +
            " WHERE status > " + StatusConstant.STATUS_DELETEED +
            " and position_code in " +
            " <foreach collection='codes' item='arr' open='(' separator=',' close=')'> " +
            " #{arr} " +
            " </foreach> </script>")
    List<Map<String, Object>> findAssetByPositions(@Param("schema_name") String schemaName, @Param("codes") String[] codes);

    /**
     * 查询关联子工单
     * @param schemaName
     * @param selectExtKey
     * @return
     */
    @Select(" select  b.sub_work_code,su.user_name  as next_user_id,to_char(a.create_time, 'YYYY-MM-DD hh24:mi:ss') create_time,s.status,\n" +
            " b.work_type_id,sw.type_name as work_type, a.title from " +
            " ${schema_name}._sc_works a " +
            " join ${schema_name}._sc_works_detail b on a.work_code= b.work_code " +
            " left join ${schema_name}._sc_status s on a.status = s.id " +
            " left join ${schema_name}._sc_work_type sw on b.work_type_id = sw.id " +
            " left join ${schema_name}._sc_user su on b.duty_user_id = su.id " +
            " where  a.status >" + StatusConstant.STATUS_DELETEED+
            " and  a.work_request_code =#{selectExtKey} ")
    List<Map<String, Object>> findSubWorkList(@Param("schema_name") String schemaName,@Param("selectExtKey") String selectExtKey);
}
