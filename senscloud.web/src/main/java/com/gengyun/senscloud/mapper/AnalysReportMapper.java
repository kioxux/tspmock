package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnalysReportMapper {
//
//    //    @Select("select f.title as analysName,f.facilitycode as analysCode,md.asset_alias,dv.strname,COUNT(dv._id) as asset_count," +
//    //综合统计，位置和供应商的设备维护、保养时效和总数
//    @Select("select analysCode,analysName,strname,asset_count,finishedMaintainTimes,totalMaintainMinute," +
//            "(case when finishedMaintainTimes=0 then 0 else totalMaintainMinute/finishedMaintainTimes end) as maintainMinutePreTime," +
//            "noFinishedMaintainTimes,runTimeRepairTimes,runTimeTotalRepairMinute,repairTimes,totalRepairMinute," +
//            "(case when runTimeRepairTimes=0 then 0 else runTimeTotalRepairMinute/runTimeRepairTimes end) as runTimeRepairMinutePreTime," +
//            "(case when repairTimes=0 then 0 else totalRepairMinute/repairTimes end) as totalRepairMinutePreTime," +
//            "(case when ((case when totalMaintainMinute is null then 0 else totalMaintainMinute end )+(case when totalRepairMinute is null then 0 else totalRepairMinute end ))=0 then 0 else (case when totalMaintainMinute is null then 0 else totalMaintainMinute end )/((case when totalMaintainMinute is null then 0 else totalMaintainMinute end )+(case when totalRepairMinute is null then 0 else totalRepairMinute end )) end) as totalMaintainOfRepairRate " +
//            "from (" +
//            "select facilitycode as analysCode,title as analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(finishedMaintainTimes) as finishedMaintainTimes," +
//            "SUM(totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(runTimeRepairTimes) as runTimeRepairTimes," +
//            "SUM(runTimeTotalRepairMinute) as runTimeTotalRepairMinute," +
//            "SUM(repairTimes) as repairTimes," +
//            "SUM(totalRepairMinute) as totalRepairMinute " +
//            "from (" +
//            "select f.id as facilitycode,f.short_title as title,mt_rp.strname,mt_rp.relation_id," +
//            "mt_rp.finishedMaintainTimes,mt_rp.totalMaintainMinute,mt_rp.noFinishedMaintainTimes,mt_rp.runTimeRepairTimes," +
//            "mt_rp.runTimeTotalRepairMinute,mt_rp.repairTimes,mt_rp.totalRepairMinute " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ( " +
//            "select wd_org.facility_id,wd.relation_id,dv.strname," +
//            "SUM(case when wt.business_type_id =2 and wd.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =2 and w.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wt.business_type_id =2 and wd.status=100 then 1 else 0 end) as noFinishedMaintainTimes," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute " +
//            "from ${schema_name}._sc_works_detail_asset_org wd_org " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where (wt.business_type_id=1 or wt.business_type_id=2) and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd_org.facility_id,wd.relation_id,dv.strname" +
//            ") mt_rp on f.id=mt_rp.facility_id  " +
//            "where 1=1 ${facilityTypeCondition} ${facilityCondition} " +
//            "and (mt_rp.finishedMaintainTimes>0 or mt_rp.noFinishedMaintainTimes>0 or mt_rp.repairTimes>0) " +
//            " ) TempAnalys " +
//            "group by facilitycode,title,strname " +
//            "order by facilitycode,title " +
//            ") AS AnalysTotal " +
//            "limit ${page} offset ${begin}")
//    List<AnalysFacilityRepairMaintainResult> GetFacilityRepairMaintainReport(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数的和合计数量
//    @Select("select count(analysCode) from (" +
//            "select facilitycode as analysCode,title as analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(finishedMaintainTimes) as finishedMaintainTimes," +
//            "SUM(totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(runTimeRepairTimes) as runTimeRepairTimes," +
//            "SUM(runTimeTotalRepairMinute) as runTimeTotalRepairMinute," +
//            "SUM(repairTimes) as repairTimes," +
//            "SUM(totalRepairMinute) as totalRepairMinute " +
//            "from (" +
//            "select f.id as facilitycode,f.title,mt_rp.strname,mt_rp.relation_id," +
//            "mt_rp.finishedMaintainTimes,mt_rp.totalMaintainMinute,mt_rp.noFinishedMaintainTimes,mt_rp.runTimeRepairTimes," +
//            "mt_rp.runTimeTotalRepairMinute,mt_rp.repairTimes,mt_rp.totalRepairMinute " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ( " +
//            "select wd_org.facility_id,wd.relation_id,dv.strname," +
//            "SUM(case when wt.business_type_id =2 and wd.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =2 and w.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wt.business_type_id =2 and wd.status=100 then 1 else 0 end) as noFinishedMaintainTimes," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute " +
//            "from ${schema_name}._sc_works_detail_asset_org wd_org " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where (wt.business_type_id=1 or wt.business_type_id=2) and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd_org.facility_id,wd.relation_id,dv.strname" +
//            ") mt_rp on f.id=mt_rp.facility_id  " +
//            "where 1=1 ${facilityTypeCondition} ${facilityCondition} " +
//            "and (mt_rp.finishedMaintainTimes>0 or mt_rp.noFinishedMaintainTimes>0 or mt_rp.repairTimes>0) " +
//            " ) TempAnalys " +
//            "group by facilitycode,title,strname " +
//            "order by facilitycode,title " +
//            ") AS AnalysTotal ")
//    int GetFacilityRepairMaintainReportCount(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition);
//
//
//    //按故障类型，进行位置和供应商的设备维护统计
//    @Select("select analysCode,analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(device_fault_times) as device_fault_times,SUM(device_fault_minutes) as device_fault_minutes," +
//            "SUM(engine_fault_times) as engine_fault_times,SUM(engine_fault_minutes) as engine_fault_minutes," +
//            "SUM(belt_fault_times) as belt_fault_times,SUM(belt_fault_minutes) as belt_fault_minutes," +
//            "SUM(roll_fault_times) as roll_fault_times,SUM(roll_fault_minutes) as roll_fault_minutes," +
//            "SUM(bearing_fault_times) as bearing_fault_times,SUM(bearing_fault_minutes) as bearing_fault_minutes," +
//            "SUM(other_fault_times) as other_fault_times,SUM(other_fault_minutes) as other_fault_minutes " +
//            "from ( " +
//            "select f.facilitycode as analysCode,f.title as analysName,wd.relation_id,dv.strname," +
//            "SUM(case when wd.fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as device_fault_minutes," +
//            "SUM(case when wd.fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=2 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as engine_fault_minutes," +
//            "SUM(case when wd.fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=3 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as belt_fault_minutes," +
//            "SUM(case when wd.fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=4 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as roll_fault_minutes," +
//            "SUM(case when wd.fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=5 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as bearing_fault_minutes," +
//            "SUM(case when wd.fault_type=6 then 1 else 0 end) as other_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=6 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as other_fault_minutes " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_works_detail_asset_org wd_org on f.id=wd_org.facility_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${facilityTypeCondition} ${facilityCondition} ${dateCondition} ${categoryCondition} " +
//            "group by f.facilitycode,f.title,wd.relation_id,dv.strname " +
//            "order by f.facilitycode,f.title " +
//            ") AS AnalysTotal " +
//            "where device_fault_times>0 or engine_fault_times>0 or belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 " +
//            "group by analysCode,analysName,strname " +
//            "limit ${page} offset ${begin}")
//    List<AnalysFacilityRepairMaintainResult> GetFacilityRepairTypeReport(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //按故障类型，进行位置和供应商的设备维护统计，统计总数
//    @Select("select count(analysCode) from (  " +
//            "select analysCode,analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(device_fault_times) as device_fault_times,SUM(device_fault_minutes) as device_fault_minutes," +
//            "SUM(engine_fault_times) as engine_fault_times,SUM(engine_fault_minutes) as engine_fault_minutes," +
//            "SUM(belt_fault_times) as belt_fault_times,SUM(belt_fault_minutes) as belt_fault_minutes," +
//            "SUM(roll_fault_times) as roll_fault_times,SUM(roll_fault_minutes) as roll_fault_minutes," +
//            "SUM(bearing_fault_times) as bearing_fault_times,SUM(bearing_fault_minutes) as bearing_fault_minutes," +
//            "SUM(other_fault_times) as other_fault_times,SUM(other_fault_minutes) as other_fault_minutes " +
//            "from ( " +
//            "select f.facilitycode as analysCode,f.title as analysName,wd.relation_id,dv.strname," +
//            "SUM(case when wd.fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as device_fault_minutes," +
//            "SUM(case when wd.fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=2 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as engine_fault_minutes," +
//            "SUM(case when wd.fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=3 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as belt_fault_minutes," +
//            "SUM(case when wd.fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=4 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as roll_fault_minutes," +
//            "SUM(case when wd.fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=5 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as bearing_fault_minutes," +
//            "SUM(case when wd.fault_type=6 then 1 else 0 end) as other_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=6 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as other_fault_minutes " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_works_detail_asset_org wd_org on f.id=wd_org.facility_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${facilityTypeCondition} ${facilityCondition} ${dateCondition} ${categoryCondition} " +
//            "group by f.facilitycode,f.title,wd.relation_id,dv.strname " +
//            "order by f.facilitycode,f.title " +
//            ") AS AnalysTotal " +
//            "where device_fault_times>0 or engine_fault_times>0 or belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 " +
//            "group by analysCode,analysName,strname " +
//            ") TTotal ")
//    int GetFacilityRepairTypeReportCount(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition);
//
//    //按设备，统计设备维护、保养时效和总数，故障类型的总数和时间
//    @Select("select * from ( " +
//            "select distinct act.category_name as analysName,dv.strname,count(dv._id) as asset_count," +
//            "SUM(wd.finishedMaintainTimes) as finishedMaintainTimes," +
//            "SUM(wd.totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(wd.noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(wd.runTimeRepairTimes) as runTimeRepairTimes,SUM(wd.runTimeTotalRepairMinute) as runTimeTotalRepairMinute," +
//            "SUM(wd.repairTimes) as repairTimes,SUM(wd.totalRepairMinute) as totalRepairMinute," +
//            "(case when SUM(wd.finishedMaintainTimes)=0 then 0 else SUM(wd.totalMaintainMinute)/SUM(wd.finishedMaintainTimes) end) as maintainMinutePreTime," +
//            "(case when SUM(wd.runTimeRepairTimes)=0 then 0 else SUM(wd.runTimeTotalRepairMinute)/SUM(wd.runTimeRepairTimes) end) as runTimeRepairMinutePreTime," +
//            "(case when SUM(wd.repairTimes)=0 then 0 else SUM(wd.totalRepairMinute)/SUM(wd.repairTimes) end) as totalRepairMinutePreTime," +
//            "(case when (SUM(case when wd.totalMaintainMinute is null then 0 else wd.totalMaintainMinute end )+SUM(case when wd.totalRepairMinute is null then 0 else wd.totalRepairMinute end ))=0 then 0 else SUM(case when wd.totalMaintainMinute is null then 0 else wd.totalMaintainMinute end )/(SUM(case when wd.totalMaintainMinute is null then 0 else wd.totalMaintainMinute end )+SUM(case when wd.totalRepairMinute is null then 0 else wd.totalRepairMinute end )) end) as totalMaintainOfRepairRate," +
//            "SUM(wd.device_fault_times) as device_fault_times,SUM(wd.device_fault_minutes) as device_fault_minutes," +
//            "SUM(wd.engine_fault_times) as engine_fault_times,SUM(wd.engine_fault_minutes) as engine_fault_minutes," +
//            "SUM(wd.belt_fault_times) as belt_fault_times,SUM(wd.belt_fault_minutes) as belt_fault_minutes," +
//            "SUM(wd.roll_fault_times) as roll_fault_times,SUM(wd.roll_fault_minutes) as roll_fault_minutes," +
//            "SUM(wd.bearing_fault_times) as bearing_fault_times,SUM(wd.bearing_fault_minutes) as bearing_fault_minutes," +
//            "SUM(wd.other_fault_times) as other_fault_times,SUM(wd.other_fault_minutes) as other_fault_minutes " +
//            "from (" +
//            "select d.relation_id," +
//            "SUM(case when wt.business_type_id=2 and d.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when wt.business_type_id=2 and d.status=60 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wt.business_type_id=2 and d.status=100 then 1 else 0 end) as noFinishedMaintainTimes, " +
//            "SUM(case when wt.business_type_id=1 and d.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id=1 and d.status=60 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when d.status=60 and repair_type=1 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=1 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as device_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=2 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as engine_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=3 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as belt_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=4 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as roll_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=5 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as bearing_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=6 then 1 else 0 end) as other_fault_times," +
//            "COALESCE(SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=6 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as other_fault_minutes " +
//            "from ${schema_name}._sc_works_detail d " +
//            "left join ${schema_name}._sc_works w on w.work_code=d.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= d.work_type_id " +
//            "left join ${schema_name}._sc_works_detail_asset_org wd_org on d.sub_work_code=wd_org.sub_work_code " +
//            "left join ${schema_name}._sc_facilities f on wd_org.facility_id=f.id " +
//            "where d.relation_type=2 ${dateCondition} ${facilityCondition} " +
//            "group by d.relation_id " +
//            ") wd " +
//            "left join ${schema_name}._sc_asset dv on dv._id = wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "left join ${schema_name}._sc_asset_organization a_org on dv._id=a_org.asset_id " +
//            "left join ${schema_name}._sc_facilities f on a_org.org_id=f.id " +
//            "where dv.intstatus>0 and f.org_type<3 ${deviceCondition} ${categoryCondition} ${facilityCondition} " +
//            "group by act.category_name,dv.strname" +
//            ") as Foo " +
//            "where repairTimes>0 or noFinishedMaintainTimes>0 or finishedMaintainTimes>0 or device_fault_times>0 or engine_fault_times>0 " +
//            "or belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 " +
//            "order by analysName,strname " +
//            "limit ${page} offset ${begin}")
//    List<AnalysFacilityRepairMaintainResult> GetDeviceRepairMaintainReport(@Param("schema_name") String schema_name, @Param("deviceCondition") String deviceCondition, @Param("dateCondition") String dateCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按设备，统计设备维护、保养时效和总数，故障类型的总数和时间，统计结果的总数
//    @Select("select count(analysName) from (" +
//            "select distinct act.category_name as analysName,dv.strname,count(dv._id) as asset_count," +
//            "SUM(wd.finishedMaintainTimes) as finishedMaintainTimes," +
//            "SUM(wd.totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(wd.noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(wd.runTimeRepairTimes) as runTimeRepairTimes,SUM(wd.runTimeTotalRepairMinute) as runTimeTotalRepairMinute," +
//            "SUM(wd.repairTimes) as repairTimes,SUM(wd.totalRepairMinute) as totalRepairMinute," +
//            "(case when SUM(wd.finishedMaintainTimes)=0 then 0 else SUM(wd.totalMaintainMinute)/SUM(wd.finishedMaintainTimes) end) as maintainMinutePreTime," +
//            "(case when SUM(wd.runTimeRepairTimes)=0 then 0 else SUM(wd.runTimeTotalRepairMinute)/SUM(wd.runTimeRepairTimes) end) as runTimeRepairMinutePreTime," +
//            "(case when SUM(wd.repairTimes)=0 then 0 else SUM(wd.totalRepairMinute)/SUM(wd.repairTimes) end) as totalRepairMinutePreTime," +
//            "(case when (SUM(case when wd.totalMaintainMinute is null then 0 else wd.totalMaintainMinute end )+SUM(case when wd.totalRepairMinute is null then 0 else wd.totalRepairMinute end ))=0 then 0 else SUM(case when wd.totalMaintainMinute is null then 0 else wd.totalMaintainMinute end )/(SUM(case when wd.totalMaintainMinute is null then 0 else wd.totalMaintainMinute end )+SUM(case when wd.totalRepairMinute is null then 0 else wd.totalRepairMinute end )) end) as totalMaintainOfRepairRate," +
//            "SUM(wd.device_fault_times) as device_fault_times,SUM(wd.device_fault_minutes) as device_fault_minutes," +
//            "SUM(wd.engine_fault_times) as engine_fault_times,SUM(wd.engine_fault_minutes) as engine_fault_minutes," +
//            "SUM(wd.belt_fault_times) as belt_fault_times,SUM(wd.belt_fault_minutes) as belt_fault_minutes," +
//            "SUM(wd.roll_fault_times) as roll_fault_times,SUM(wd.roll_fault_minutes) as roll_fault_minutes," +
//            "SUM(wd.bearing_fault_times) as bearing_fault_times,SUM(wd.bearing_fault_minutes) as bearing_fault_minutes," +
//            "SUM(wd.other_fault_times) as other_fault_times,SUM(wd.other_fault_minutes) as other_fault_minutes " +
//            "from (" +
//            "select d.relation_id," +
//            "SUM(case when wt.business_type_id=2 and d.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when wt.business_type_id=2 and d.status=60 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wt.business_type_id=2 and d.status=100 then 1 else 0 end) as noFinishedMaintainTimes, " +
//            "SUM(case when wt.business_type_id=1 and d.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id=1 and d.status=60 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when d.status=60 and repair_type=1 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=1 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as device_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=2 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as engine_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=3 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as belt_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=4 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as roll_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "COALESCE(SUM(case when d.status=60 and fault_type=5 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as bearing_fault_minutes," +
//            "SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=6 then 1 else 0 end) as other_fault_times," +
//            "COALESCE(SUM(case when wt.business_type_id=1 and d.status=60 and fault_type=6 then (EXTRACT(EPOCH from finished_time)-EXTRACT(EPOCH from begin_time))/60 else 0 end),0) as other_fault_minutes " +
//            "from ${schema_name}._sc_works_detail d " +
//            "left join ${schema_name}._sc_works w on w.work_code=d.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= d.work_type_id " +
//            "left join ${schema_name}._sc_works_detail_asset_org wd_org on d.sub_work_code=wd_org.sub_work_code " +
//            "left join ${schema_name}._sc_facilities f on wd_org.facility_id=f.id " +
//            "where d.relation_type=2 ${dateCondition} ${facilityCondition} " +
//            "group by d.relation_id " +
//            ") wd " +
//            "left join ${schema_name}._sc_asset dv on dv._id = wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "left join ${schema_name}._sc_asset_organization a_org on dv._id=a_org.asset_id " +
//            "left join ${schema_name}._sc_facilities f on a_org.org_id=f.id " +
//            "where dv.intstatus>0 and f.org_type<3 ${deviceCondition} ${categoryCondition} ${facilityCondition} " +
//            "group by act.category_name,dv.strname" +
//            ") as Foo " +
//            "where repairTimes>0 or noFinishedMaintainTimes>0 or finishedMaintainTimes>0 or device_fault_times>0 or engine_fault_times>0 " +
//            "or belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 ")
//    int GetDeviceRepairMaintainReportCount(@Param("schema_name") String schema_name, @Param("deviceCondition") String deviceCondition, @Param("dateCondition") String dateCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition);
//
//    //按个人，统计设备维护、保养时效和总数和时间
//    @Select("select distinct u.account,u.username,u.user_code as analysCode,g.group_name as title," +
//            "mt.finishedMaintainTimes,mt.totalMaintainMinute,mt.noFinishedMaintainTimes,mt.planMaintainTimes,rp.runTimeRepairTimes,rp.runTimeTotalRepairMinute," +
//            "rp.repairTimes,rp.totalRepairMinute,rp.totalArriveMinute,rp2.auditRepairTimes,rp2.totalAuditRepairMinute," +
//            "(case when mt.planMaintainTimes=0 then 0 else mt.finishedMaintainTimes/mt.planMaintainTimes::numeric end) as maintainFinishedRate," +
//            "(case when mt.finishedMaintainTimes=0 then 0 else mt.totalMaintainMinute/mt.finishedMaintainTimes end) as maintainMinutePreTime," +
//            "(case when rp.runTimeRepairTimes=0 then 0 else rp.runTimeTotalRepairMinute/rp.runTimeRepairTimes end) as runTimeRepairMinutePreTime," +
//            "(case when rp.repairTimes=0 then 0 else rp.totalRepairMinute/rp.repairTimes end) as totalRepairMinutePreTime," +
//            "(case when rp.repairTimes=0 then 0 else rp.totalArriveMinute/rp.repairTimes end) as arriveMinutePreTime," +
//            "(case when rp2.auditRepairTimes=0 then 0 else rp2.totalAuditRepairMinute/rp2.auditRepairTimes end) as auditMinutePreTime," +
//            "(case when ((case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end) + " +
//            "(case when rp.totalRepairMinute is null or rp.totalRepairMinute=0 then 0 else rp.totalRepairMinute end) )=0 then 0 " +
//            "else (case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end)/((case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end) + " +
//            "(case when rp.totalRepairMinute is null or rp.totalRepairMinute=0 then 0 else rp.totalRepairMinute end) ) end) as totalMaintainOfRepairRate," +
//            "((case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end) +" +
//            "(case when rp.totalRepairMinute is null or rp.totalRepairMinute=0 then 0 else rp.totalRepairMinute end) ) as totalMinute " +
//            "from ${schema_name}._sc_user u " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//            "left join ( " +
//            "select wd.receive_account,SUM(case when wd.status=60 then 1 else 0 end) as finishedMaintainTimes, " +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wd.status=100 then 1 else 0 end) as noFinishedMaintainTimes," +
//            "SUM(case when wd.status<900 then 1 else 0 end) as planMaintainTimes " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code= wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= wd.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id  " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=2 and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd.receive_account " +
//            ") mt on u.account=mt.receive_account " +
//            "left join (" +
//            "select wd.receive_account,SUM(case when wd.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.begin_time)-EXTRACT(EPOCH from wd.distribute_time))/60 else 0 end),0) as totalArriveMinute," +
//            "SUM(case when wd.status=60 and wd.repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when wd.status=60 and wd.repair_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code= wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= wd.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd.receive_account " +
//            ") rp on u.account=rp.receive_account " +
//            "left join (" +
//            "select receive_account,SUM(case when wd.status=60 then 1 else 0 end) as auditRepairTimes," +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.audit_time)-EXTRACT(EPOCH from wd.finished_time))/60 else 0 end),0) as totalAuditRepairMinute " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code= wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= wd.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd.receive_account " +
//            ") rp2 on u.account=rp2.receive_account " +
//            "where u.status>= "+StatusConstant.STATUS_DELETEED+" ${condition} ${groupCondition} and " +
//            "(mt.finishedMaintainTimes>0 or mt.noFinishedMaintainTimes>0 or mt.planMaintainTimes>0 or rp.runTimeRepairTimes>0 or rp.repairTimes>0) " +
//            "order by g.group_name,u.account " +
//            "limit ${page} offset ${begin}")
//    List<AnalysFacilityRepairMaintainResult> GetUserRepairMaintainReport(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("groupCondition") String groupCondition, @Param("dateCondition") String dateCondition, @Param("categoryCondition") String categoryCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按个人，统计设备维护、保养时效和总数和时间的总数
//    @Select("select count(account) from (" +
//            "select distinct u.account,u.username,u.user_code as analysCode,g.group_name as title," +
//            "mt.finishedMaintainTimes,mt.totalMaintainMinute,mt.noFinishedMaintainTimes,mt.planMaintainTimes,rp.runTimeRepairTimes,rp.runTimeTotalRepairMinute," +
//            "rp.repairTimes,rp.totalRepairMinute,rp.totalArriveMinute,rp2.auditRepairTimes,rp2.totalAuditRepairMinute," +
//            "(case when mt.planMaintainTimes=0 then 0 else mt.finishedMaintainTimes/mt.planMaintainTimes::numeric end) as maintainFinishedRate," +
//            "(case when mt.finishedMaintainTimes=0 then 0 else mt.totalMaintainMinute/mt.finishedMaintainTimes end) as maintainMinutePreTime," +
//            "(case when rp.runTimeRepairTimes=0 then 0 else rp.runTimeTotalRepairMinute/rp.runTimeRepairTimes end) as runTimeRepairMinutePreTime," +
//            "(case when rp.repairTimes=0 then 0 else rp.totalRepairMinute/rp.repairTimes end) as totalRepairMinutePreTime," +
//            "(case when rp.repairTimes=0 then 0 else rp.totalArriveMinute/rp.repairTimes end) as arriveMinutePreTime," +
//            "(case when rp2.auditRepairTimes=0 then 0 else rp2.totalAuditRepairMinute/rp2.auditRepairTimes end) as auditMinutePreTime," +
//            "(case when ((case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end) + " +
//            "(case when rp.totalRepairMinute is null or rp.totalRepairMinute=0 then 0 else rp.totalRepairMinute end) )=0 then 0 " +
//            "else (case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end)/((case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end) + " +
//            "(case when rp.totalRepairMinute is null or rp.totalRepairMinute=0 then 0 else rp.totalRepairMinute end) ) end) as totalMaintainOfRepairRate," +
//            "((case when mt.totalMaintainMinute is null or mt.totalMaintainMinute=0 then 0 else mt.totalMaintainMinute end) +" +
//            "(case when rp.totalRepairMinute is null or rp.totalRepairMinute=0 then 0 else rp.totalRepairMinute end) ) as totalMinute " +
//            "from ${schema_name}._sc_user u " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//            "left join ( " +
//            "select wd.receive_account,SUM(case when wd.status=60 then 1 else 0 end) as finishedMaintainTimes, " +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wd.status=100 then 1 else 0 end) as noFinishedMaintainTimes," +
//            "SUM(case when wd.status<900 then 1 else 0 end) as planMaintainTimes " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code= wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= wd.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id  " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=2 and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd.receive_account " +
//            ") mt on u.account=mt.receive_account " +
//            "left join (" +
//            "select wd.receive_account,SUM(case when wd.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.begin_time)-EXTRACT(EPOCH from wd.distribute_time))/60 else 0 end),0) as totalArriveMinute," +
//            "SUM(case when wd.status=60 and wd.repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when wd.status=60 and wd.repair_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code= wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= wd.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd.receive_account " +
//            ") rp on u.account=rp.receive_account " +
//            "left join (" +
//            "select receive_account,SUM(case when wd.status=60 then 1 else 0 end) as auditRepairTimes," +
//            "COALESCE(SUM(case when wd.status=60 then (EXTRACT(EPOCH from wd.audit_time)-EXTRACT(EPOCH from wd.finished_time))/60 else 0 end),0) as totalAuditRepairMinute " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code= wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wt.id= wd.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by wd.receive_account " +
//            ") rp2 on u.account=rp2.receive_account " +
//            "where u.status>= "+StatusConstant.STATUS_DELETEED+" ${condition} ${groupCondition} and " +
//            "(mt.finishedMaintainTimes>0 or mt.noFinishedMaintainTimes>0 or mt.planMaintainTimes>0 or rp.runTimeRepairTimes>0 or rp.repairTimes>0) " +
//            ") total")
//    int GetUserRepairMaintainReportCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("groupCondition") String groupCondition, @Param("dateCondition") String dateCondition, @Param("categoryCondition") String categoryCondition);
//
//    //位置统计：故障率，保养效率、故障间隔时间和故障处理能力
//    @Select("select TT.short_title as facilityName,TT.facilitycode as facilityCode," +
//            "(case when TT.total_asset=0 then 0 else TT.repair_count/TT.total_asset::numeric end ) as failureRate, " +
//            "TT.plan_maintain_count as planMaintainTotal,TT.finished_maintain_count as finishedMaintainTotal," +
//            "(case when TT.plan_maintain_count=0 then 0 else TT.finished_maintain_count/TT.plan_maintain_count::numeric end ) maintainFinishedRate," +
//            "(case when (TT.repair_time=0 and TT.maintain_time=0) or TT.plan_maintain_count=0 then 0 else " +
//            " (TT.maintain_time/(TT.maintain_time+TT.repair_time)::numeric)*(TT.finished_maintain_count/TT.plan_maintain_count::numeric) " +
//            " end ) as maintainRate, " +
//            "(case when TT.mtbf_count=0 then 0 else TT.mtbf_time/TT.mtbf_count::numeric end ) as timeBetweenFailures, " +
//            "(case when TT.repair_count=0 then 0 else TT.repair_time/TT.repair_count::numeric end ) as aveRepairTime " +
//            "from ( " +
//            "select f.title,f.short_title,f.facilitycode,f.id,f.org_type," +
//            "COALESCE(ast_total.total_asset,0) as total_asset," +
//            "COALESCE(repair_wd.repair_time,0) as repair_time," +
//            "COALESCE(repair_wd.repair_count,0) as repair_count," +
//            "COALESCE(repair_wd.mtbf_time,0) as mtbf_time," +
//            "COALESCE(repair_wd.mtbf_count,0) as mtbf_count," +
//            "COALESCE(maintain_wd.plan_maintain_count,0) as plan_maintain_count," +
//            "COALESCE(maintain_wd.finished_maintain_count,0) as finished_maintain_count," +
//            "COALESCE(maintain_wd.maintain_time,0) as maintain_time " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join " +
//            "(" +
//            "select f.id,count(distinct ast._id) as total_asset from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset_organization a_org on f.id=a_org.org_id " +
//            "left join ${schema_name}._sc_asset ast on a_org.asset_id=ast._id " +
//            "left join ${schema_name}._sc_asset_category act on ast.category_id=act.id " +
//            "where f.org_type<3 and ast.intstatus>0 ${categoryCondition} " +
//            "group by f.id " +
//            ") ast_total on f.id=ast_total.id " +
//            "left join " +
//            "(" +
//            "select wd_org.facility_id, " +
//            "COALESCE((SUM(EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time)))/60,0) as repair_time,count(wd.sub_work_code) as repair_count," +
//            "COALESCE((SUM(" +
//            "case when w.last_finished_time is null then 0 else EXTRACT(EPOCH from w.occur_time)-EXTRACT(EPOCH from w.last_finished_time) end" +
//            ")" +
//            ")/60,0) as mtbf_time," +
//            "sum(" +
//            "case when w.last_finished_time is null then 0 else 1 end " +
//            ") as mtbf_count " +
//            "from ${schema_name}._sc_works_detail_asset_org wd_org " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id=ast._id and wd.relation_type=2 " +
//            "left join ${schema_name}._sc_asset_category act on ast.category_id=act.id " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where wt.business_type_id=1 and wd.status=60 ${repairCondition} ${categoryCondition} " +
//            "group by wd_org.facility_id " +
//            ") repair_wd on f.id=repair_wd.facility_id " +
//            "left join " +
//            "(" +
//            "select wd_org.facility_id,count(wd.sub_work_code) as plan_maintain_count," +
//            "sum(" +
//            "case when wd.status=60 then 1 else 0 end " +
//            ") as finished_maintain_count," +
//            "COALESCE((SUM(" +
//            "case when wd.status=60 then 0 else EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time) end " +
//            ")" +
//            ")/60,0) as maintain_time " +
//            "from ${schema_name}._sc_works_detail_asset_org wd_org " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id=ast._id and wd.relation_type=2 " +
//            "left join ${schema_name}._sc_asset_category act on ast.category_id=act.id " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where wt.business_type_id=2 and wd.status<900 ${maintainCondition} ${categoryCondition} " +
//            "group by wd_org.facility_id " +
//            ") maintain_wd on f.id=maintain_wd.facility_id " +
//            "where f.org_type<3 ${cidCondition} " +
//            "order by f.short_title " +
//            "limit ${page} offset ${begin}" +
//            ") TT ")
//    List<AnalysFacilityResult> getFacilitReport(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition,
//                                                @Param("maintainCondition") String maintainCondition, @Param("cidCondition") String cidCondition,
//                                                @Param("categoryCondition") String categoryCondition,
//                                                @Param("page") Integer pageSize, @Param("begin") Integer begin);
//
//    //供应位置统计：故障率，保养效率、故障间隔时间和故障处理能力 总数
//    @Select("select count(f.id) " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join " +
//            "(" +
//            "select f.id,count(distinct ast._id) as total_asset from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset_organization a_org on f.id=a_org.org_id " +
//            "left join ${schema_name}._sc_asset ast on a_org.asset_id=ast._id " +
//            "left join ${schema_name}._sc_asset_category act on ast.category_id=act.id " +
//            "where f.org_type<3 and ast.intstatus>0  ${categoryCondition} " +
//            "group by f.id " +
//            ") ast_total on f.id=ast_total.id " +
//            "left join " +
//            "(" +
//            "select wd_org.facility_id, " +
//            "COALESCE((SUM(EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time)))/60,0) as repair_time,count(wd.sub_work_code) as repair_count," +
//            "COALESCE((SUM(" +
//            "case when w.last_finished_time is null then 0 else EXTRACT(EPOCH from w.occur_time)-EXTRACT(EPOCH from w.last_finished_time) end" +
//            ")" +
//            ")/60,0) as mtbf_time," +
//            "sum(" +
//            "case when w.last_finished_time is null then 0 else 1 end " +
//            ") as mtbf_count " +
//            "from ${schema_name}._sc_works_detail_asset_org wd_org " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id=ast._id and wd.relation_type=2 " +
//            "left join ${schema_name}._sc_asset_category act on ast.category_id=act.id " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where wt.business_type_id=1 and wd.status=60 ${repairCondition}  ${categoryCondition} " +
//            "group by wd_org.facility_id " +
//            ") repair_wd on f.id=repair_wd.facility_id " +
//            "left join " +
//            "(" +
//            "select wd_org.facility_id,count(wd.sub_work_code) as plan_maintain_count," +
//            "sum(" +
//            "case when wd.status=60 then 1 else 0 end " +
//            ") as finished_maintain_count," +
//            "COALESCE((SUM(" +
//            "case when wd.status=60 then 0 else EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time) end " +
//            ")" +
//            ")/60,0) as maintain_time " +
//            "from ${schema_name}._sc_works_detail_asset_org wd_org " +
//            "left join ${schema_name}._sc_works_detail wd on wd_org.sub_work_code=wd.sub_work_code " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id=ast._id and wd.relation_type=2 " +
//            "left join ${schema_name}._sc_asset_category act on ast.category_id=act.id " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where wt.business_type_id=2 and wd.status<900 ${maintainCondition} ${categoryCondition} " +
//            "group by wd_org.facility_id " +
//            ") maintain_wd on f.id=maintain_wd.facility_id " +
//            "where f.org_type<3 ${cidCondition} ")
//    int getFacilitReportCount(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition,
//                              @Param("maintainCondition") String maintainCondition, @Param("cidCondition") String cidCondition,
//                              @Param("categoryCondition") String categoryCondition);
//
//    //供应商统计：设备故障率，故障明细
//    @Select("<script>" +
//            "select supplier,customer_name as supplier_name,fault_rate,device_fault_rate,engine_fault_rate,belt_fault_rate,roll_fault_rate,bearing_fault_rate, " +
//            "pmx_fault_rate,ssj_fault_rate,zwj_fault_rate,jcd_fault_rate " +
//            "from ( " +
//            "select cust_device.id,cust_device.facilitycode as supplier,cust_device.short_title as customer_name," +
//            "(case when deviceCount=0 then 0 else repairTimes/(deviceCount::numeric) end) as fault_rate, " +
//            "(case when deviceCount=0 then 0 else device_fault_times/(deviceCount::numeric) end) as device_fault_rate, " +
//            "(case when deviceCount=0 then 0 else engine_fault_times/(deviceCount::numeric) end) as engine_fault_rate, " +
//            "(case when deviceCount=0 then 0 else belt_fault_times/(deviceCount::numeric) end) as belt_fault_rate, " +
//            "(case when deviceCount=0 then 0 else roll_fault_times/(deviceCount::numeric) end) as roll_fault_rate, " +
//            "(case when deviceCount=0 then 0 else bearing_fault_times/(deviceCount::numeric) end) as bearing_fault_rate, " +
//            "(case when deviceCount=0 then 0 else pmx_fault_times/(deviceCount::numeric) end) as pmx_fault_rate, " +
//            "(case when deviceCount=0 then 0 else ssj_fault_times/(deviceCount::numeric) end) as ssj_fault_rate, " +
//            "(case when deviceCount=0 then 0 else zwj_fault_times/(deviceCount::numeric) end) as zwj_fault_rate, " +
//            "(case when deviceCount=0 then 0 else jcd_fault_times/(deviceCount::numeric) end) as jcd_fault_rate, " +
//            "device_fault_times,engine_fault_times,belt_fault_times,roll_fault_times,bearing_fault_times," +
//            "pmx_fault_times,ssj_fault_times,zwj_fault_times,jcd_fault_times " +
//            "from (" +
//            "select f.id,f.facilitycode,f.short_title,count(dv._id) as deviceCount " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset dv on f.id=dv.supplier " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where f.org_type=4 and dv.supplier is not null ${sidCondition} ${categoryCondition} " +
//            "group by f.id,f.facilitycode,f.short_title " +
//            ") cust_device " +
//            "left join (" +
//            "select f.id,count(wd.sub_work_code) repairTimes, " +
//            "COALESCE(SUM(case when wd.fault_type=1 then 1 else 0 end),0) as device_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=2 then 1 else 0 end),0) as engine_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=3 then 1 else 0 end),0) as belt_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=4 then 1 else 0 end),0) as roll_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=5 then 1 else 0 end),0) as bearing_fault_times, " +
//            "COALESCE(SUM(case when position('平面线' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as pmx_fault_times, " +
//            "COALESCE(SUM(case when position('伸缩机' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as ssj_fault_times, " +
//            "COALESCE(SUM(case when position('转弯机' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as zwj_fault_times, " +
//            "COALESCE(SUM(case when position('交叉带' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as jcd_fault_times " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_fault_type ft on wd.fault_type=ft.id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "left join ${schema_name}._sc_facilities f on dv.supplier=f.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${sidCondition} ${categoryCondition} ${repairCondition} " +
//            "group by f.id " +
//            ") repair on cust_device.id=repair.id " +
//            ") AnalysTotal  " +
//            "where id>0 and (fault_rate>0 or device_fault_rate>0 or engine_fault_rate>0 or belt_fault_rate>0 or roll_fault_rate>0 or bearing_fault_rate>0 " +
//            "or pmx_fault_rate>0 or ssj_fault_rate>0 or zwj_fault_rate>0 or jcd_fault_rate>0) " +
//            "order by customer_name " +
//            "<when test='page!=null and begin!=null'>" +
//            "limit ${page} offset ${begin}" +
//            "</when>" +
//            "</script>")
//    List<AnalysSupplierResult> GetSupplierReport(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition,
//                                                 @Param("sidCondition") String sidCondition, @Param("categoryCondition") String categoryCondition,
//                                                 @Param("page") Integer pageSize, @Param("begin") Integer begin);
//
//    //供应商统计：设备故障率，故障明细总数
//    @Select("select count(distinct supplier) from (" +
//            "select cust_device.id,cust_device.facilitycode as supplier,cust_device.short_title as customer_name," +
//            "(case when deviceCount=0 then 0 else repairTimes/(deviceCount::numeric) end) as fault_rate, " +
//            "(case when deviceCount=0 then 0 else device_fault_times/(deviceCount::numeric) end) as device_fault_rate, " +
//            "(case when deviceCount=0 then 0 else engine_fault_times/(deviceCount::numeric) end) as engine_fault_rate, " +
//            "(case when deviceCount=0 then 0 else belt_fault_times/(deviceCount::numeric) end) as belt_fault_rate, " +
//            "(case when deviceCount=0 then 0 else roll_fault_times/(deviceCount::numeric) end) as roll_fault_rate, " +
//            "(case when deviceCount=0 then 0 else bearing_fault_times/(deviceCount::numeric) end) as bearing_fault_rate, " +
//            "(case when deviceCount=0 then 0 else pmx_fault_times/(deviceCount::numeric) end) as pmx_fault_rate, " +
//            "(case when deviceCount=0 then 0 else ssj_fault_times/(deviceCount::numeric) end) as ssj_fault_rate, " +
//            "(case when deviceCount=0 then 0 else zwj_fault_times/(deviceCount::numeric) end) as zwj_fault_rate, " +
//            "(case when deviceCount=0 then 0 else jcd_fault_times/(deviceCount::numeric) end) as jcd_fault_rate, " +
//            "device_fault_times,engine_fault_times,belt_fault_times,roll_fault_times,bearing_fault_times," +
//            "pmx_fault_times,ssj_fault_times,zwj_fault_times,jcd_fault_times " +
//            "from (" +
//            "select f.id,f.facilitycode,f.short_title,count(dv._id) as deviceCount " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset dv on f.id=dv.supplier " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where f.org_type=4 and dv.supplier is not null ${sidCondition} ${categoryCondition} " +
//            "group by f.id,f.facilitycode,f.short_title " +
//            ") cust_device " +
//            "left join (" +
//            "select f.id,count(wd.sub_work_code) repairTimes, " +
//            "COALESCE(SUM(case when wd.fault_type=1 then 1 else 0 end),0) as device_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=2 then 1 else 0 end),0) as engine_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=3 then 1 else 0 end),0) as belt_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=4 then 1 else 0 end),0) as roll_fault_times, " +
//            "COALESCE(SUM(case when wd.fault_type=5 then 1 else 0 end),0) as bearing_fault_times, " +
//            "COALESCE(SUM(case when position('平面线' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as pmx_fault_times, " +
//            "COALESCE(SUM(case when position('伸缩机' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as ssj_fault_times, " +
//            "COALESCE(SUM(case when position('转弯机' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as zwj_fault_times, " +
//            "COALESCE(SUM(case when position('交叉带' in dv.strname)>0 and dv._id=wd.relation_id then 1 else 0 end),0) as jcd_fault_times " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_fault_type ft on wd.fault_type=ft.id " +
//            "left join ${schema_name}._sc_asset dv on wd.relation_id=dv._id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "left join ${schema_name}._sc_facilities f on dv.supplier=f.id " +
//            "where wt.business_type_id=1 and wd.relation_type=2 ${sidCondition} ${categoryCondition} ${repairCondition} " +
//            "group by f.id " +
//            ") repair on cust_device.id=repair.id " +
//            ") AnalysTotal  " +
//            "where id>0 and (fault_rate>0 or device_fault_rate>0 or engine_fault_rate>0 or belt_fault_rate>0 or roll_fault_rate>0 or bearing_fault_rate>0 " +
//            "or pmx_fault_rate>0 or ssj_fault_rate>0 or zwj_fault_rate>0 or jcd_fault_rate>0) ")
//    int GetSupplierReportCount(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition,
//                               @Param("sidCondition") String sidCondition, @Param("categoryCondition") String categoryCondition);
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数
//    @Select("select analysCode,analysName,strname,asset_count,finishedMaintainTimes,totalMaintainMinute," +
//            "(case when finishedMaintainTimes=0 then 0 else totalMaintainMinute/finishedMaintainTimes end) as maintainMinutePreTime," +
//            "noFinishedMaintainTimes,runTimeRepairTimes,runTimeTotalRepairMinute,repairTimes,totalRepairMinute," +
//            "(case when runTimeRepairTimes=0 then 0 else runTimeTotalRepairMinute/runTimeRepairTimes end) as runTimeRepairMinutePreTime," +
//            "(case when repairTimes=0 then 0 else totalRepairMinute/repairTimes end) as totalRepairMinutePreTime," +
//            "(case when ((case when totalMaintainMinute is null then 0 else totalMaintainMinute end )+(case when totalRepairMinute is null then 0 else totalRepairMinute end ))=0 then 0 else (case when totalMaintainMinute is null then 0 else totalMaintainMinute end )/((case when totalMaintainMinute is null then 0 else totalMaintainMinute end )+(case when totalRepairMinute is null then 0 else totalRepairMinute end )) end) as totalMaintainOfRepairRate " +
//            "from (" +
//            "select facilitycode as analysCode,title as analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(finishedMaintainTimes) as finishedMaintainTimes," +
//            "SUM(totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(runTimeRepairTimes) as runTimeRepairTimes," +
//            "SUM(runTimeTotalRepairMinute) as runTimeTotalRepairMinute," +
//            "SUM(repairTimes) as repairTimes," +
//            "SUM(totalRepairMinute) as totalRepairMinute " +
//            "from (" +
//            "select f.id as facilitycode,f.short_title as title,mt_rp.strname,mt_rp.relation_id," +
//            "mt_rp.finishedMaintainTimes,mt_rp.totalMaintainMinute,mt_rp.noFinishedMaintainTimes,mt_rp.runTimeRepairTimes," +
//            "mt_rp.runTimeTotalRepairMinute,mt_rp.repairTimes,mt_rp.totalRepairMinute " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ( " +
//            "select dv.supplier,wd.relation_id,dv.strname," +
//            "SUM(case when wt.business_type_id =2 and wd.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =2 and w.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wt.business_type_id =2 and wd.status=100 then 1 else 0 end) as noFinishedMaintainTimes," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where dv.supplier is not null and (wt.business_type_id=1 or wt.business_type_id=2) and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by dv.supplier,wd.relation_id,dv.strname" +
//            ") mt_rp on f.id=mt_rp.supplier  " +
//            "where 1=1 ${facilityTypeCondition} ${facilityCondition} " +
//            "and (mt_rp.finishedMaintainTimes>0 or mt_rp.noFinishedMaintainTimes>0 or mt_rp.repairTimes>0) " +
//            " ) TempAnalys " +
//            "group by facilitycode,title,strname " +
//            "order by facilitycode,title " +
//            ") AS AnalysTotal " +
//            "limit ${page} offset ${begin}")
//    List<AnalysFacilityRepairMaintainResult> GetSupplierRepairMaintainReport(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //综合统计，位置和供应商的设备维护、保养时效和总数的和合计数量
//    @Select("select count(analysCode) from (" +
//            "select facilitycode as analysCode,title as analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(finishedMaintainTimes) as finishedMaintainTimes," +
//            "SUM(totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(runTimeRepairTimes) as runTimeRepairTimes," +
//            "SUM(runTimeTotalRepairMinute) as runTimeTotalRepairMinute," +
//            "SUM(repairTimes) as repairTimes," +
//            "SUM(totalRepairMinute) as totalRepairMinute " +
//            "from (" +
//            "select f.id as facilitycode,f.title,mt_rp.strname,mt_rp.relation_id," +
//            "mt_rp.finishedMaintainTimes,mt_rp.totalMaintainMinute,mt_rp.noFinishedMaintainTimes,mt_rp.runTimeRepairTimes," +
//            "mt_rp.runTimeTotalRepairMinute,mt_rp.repairTimes,mt_rp.totalRepairMinute " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ( " +
//            "select dv.supplier,wd.relation_id,dv.strname," +
//            "SUM(case when wt.business_type_id =2 and wd.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =2 and w.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when wt.business_type_id =2 and wd.status=100 then 1 else 0 end) as noFinishedMaintainTimes," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then 1 else 0 end) as runTimeRepairTimes," +
//            "COALESCE(SUM(case when wt.business_type_id =1 and wd.status=60 and wd.repair_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as runTimeTotalRepairMinute " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where dv.supplier is not null and (wt.business_type_id=1 or wt.business_type_id=2) and wd.relation_type=2 ${dateCondition} ${categoryCondition} " +
//            "group by dv.supplier,wd.relation_id,dv.strname" +
//            ") mt_rp on f.id=mt_rp.supplier " +
//            "where 1=1 ${facilityTypeCondition} ${facilityCondition} " +
//            "and (mt_rp.finishedMaintainTimes>0 or mt_rp.noFinishedMaintainTimes>0 or mt_rp.repairTimes>0) " +
//            " ) TempAnalys " +
//            "group by facilitycode,title,strname " +
//            "order by facilitycode,title " +
//            ") AS AnalysTotal ")
//    int GetSupplierRepairMaintainReportCount(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition);
//
//
//    //按故障类型，进行位置和供应商的设备维护统计
//    @Select("select analysCode,analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(device_fault_times) as device_fault_times,SUM(device_fault_minutes) as device_fault_minutes," +
//            "SUM(engine_fault_times) as engine_fault_times,SUM(engine_fault_minutes) as engine_fault_minutes," +
//            "SUM(belt_fault_times) as belt_fault_times,SUM(belt_fault_minutes) as belt_fault_minutes," +
//            "SUM(roll_fault_times) as roll_fault_times,SUM(roll_fault_minutes) as roll_fault_minutes," +
//            "SUM(bearing_fault_times) as bearing_fault_times,SUM(bearing_fault_minutes) as bearing_fault_minutes," +
//            "SUM(other_fault_times) as other_fault_times,SUM(other_fault_minutes) as other_fault_minutes " +
//            "from ( " +
//            "select f.facilitycode as analysCode,f.title as analysName,wd.relation_id,dv.strname," +
//            "SUM(case when wd.fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as device_fault_minutes," +
//            "SUM(case when wd.fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=2 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as engine_fault_minutes," +
//            "SUM(case when wd.fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=3 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as belt_fault_minutes," +
//            "SUM(case when wd.fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=4 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as roll_fault_minutes," +
//            "SUM(case when wd.fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=5 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as bearing_fault_minutes," +
//            "SUM(case when wd.fault_type=6 then 1 else 0 end) as other_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=6 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as other_fault_minutes " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset dv on dv.supplier=f.id " +
//            "left join ${schema_name}._sc_works_detail wd on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where dv.supplier is not null and wt.business_type_id=1 and wd.relation_type=2 " +
//            "${facilityTypeCondition} ${facilityCondition} ${dateCondition} ${categoryCondition} " +
//            "group by f.facilitycode,f.title,wd.relation_id,dv.strname " +
//            "order by f.facilitycode,f.title " +
//            ") AS AnalysTotal " +
//            "where device_fault_times>0 or engine_fault_times>0 or belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 " +
//            "group by analysCode,analysName,strname " +
//            "limit ${page} offset ${begin}")
//    List<AnalysFacilityRepairMaintainResult> GetSupplierRepairTypeReport(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //按故障类型，进行位置和供应商的设备维护统计，统计总数
//    @Select("select count(analysCode) from (  " +
//            "select analysCode,analysName,strname,COUNT(relation_id) as asset_count," +
//            "SUM(device_fault_times) as device_fault_times,SUM(device_fault_minutes) as device_fault_minutes," +
//            "SUM(engine_fault_times) as engine_fault_times,SUM(engine_fault_minutes) as engine_fault_minutes," +
//            "SUM(belt_fault_times) as belt_fault_times,SUM(belt_fault_minutes) as belt_fault_minutes," +
//            "SUM(roll_fault_times) as roll_fault_times,SUM(roll_fault_minutes) as roll_fault_minutes," +
//            "SUM(bearing_fault_times) as bearing_fault_times,SUM(bearing_fault_minutes) as bearing_fault_minutes," +
//            "SUM(other_fault_times) as other_fault_times,SUM(other_fault_minutes) as other_fault_minutes " +
//            "from ( " +
//            "select f.facilitycode as analysCode,f.title as analysName,wd.relation_id,dv.strname," +
//            "SUM(case when wd.fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=1 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as device_fault_minutes," +
//            "SUM(case when wd.fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=2 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as engine_fault_minutes," +
//            "SUM(case when wd.fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=3 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as belt_fault_minutes," +
//            "SUM(case when wd.fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=4 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as roll_fault_minutes," +
//            "SUM(case when wd.fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=5 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as bearing_fault_minutes," +
//            "SUM(case when wd.fault_type=6 then 1 else 0 end) as other_fault_times," +
//            "COALESCE(SUM(case when wd.fault_type=6 then (EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60 else 0 end),0) as other_fault_minutes " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset dv on dv.supplier=f.id " +
//            "left join ${schema_name}._sc_works_detail wd on dv._id=wd.relation_id " +
//            "left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset_category act on dv.category_id=act.id " +
//            "where dv.supplier is not null and wt.business_type_id=1 and wd.relation_type=2 " +
//            "${facilityTypeCondition} ${facilityCondition} ${dateCondition} ${categoryCondition} " +
//            "group by f.facilitycode,f.title,wd.relation_id,dv.strname " +
//            "order by f.facilitycode,f.title " +
//            ") AS AnalysTotal " +
//            "where device_fault_times>0 or engine_fault_times>0 or belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 " +
//            "group by analysCode,analysName,strname " +
//            ") TTotal ")
//    int GetSupplierRepairTypeReportCount(@Param("schema_name") String schema_name, @Param("dateCondition") String dateCondition, @Param("facilityTypeCondition") String facilityTypeCondition, @Param("facilityCondition") String facilityCondition, @Param("categoryCondition") String categoryCondition);
//
//
//    //获取维修的时效数据统计
//    @Select("select ${analysColumn},COALESCE(SUM((EXTRACT(EPOCH from r.distribute_time)-EXTRACT(EPOCH from r.occur_time))/60),0)/count(r.repair_code) as distributeMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.repairbegin_time)-EXTRACT(EPOCH from r.occur_time))/60),0)/count(r.repair_code) as arriveMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.repairbegin_time))/60),0)/count(r.repair_code) as dealMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.occur_time))/60),0)/count(r.repair_code) as stopMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.audit_time)-EXTRACT(EPOCH from r.finished_time))/60),0)/count(r.repair_code) as auditMinute," +
//            "count(r.repair_code) as doTimes " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.receive_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on r.facility_id=f.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where r.status=60  ${condition} " +
//            "group by ${groupColumn} " +
//            "order by ${groupColumn} desc limit ${page} offset ${begin}")
//    List<AnalysRepairWorkResult> GetRepairHoursReport(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("analysColumn") String analysColumn, @Param("groupColumn") String groupColumn, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //获取维修的时效数据统计的总数
//    @Select("select count(Tcount) from ( " +
//            "select count(r.repair_code) as Tcount " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.receive_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on r.facility_id=f.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where r.status=60  ${condition} " +
//            "group by ${groupColumn} " +
//            ") AS totalTableTemp")
//    int GetRepairHoursReportCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("groupColumn") String groupColumn);
//
//    //获取保养的时效数据统计
//    @Select("select ${analysColumn}," +
//            "COALESCE(SUM((EXTRACT(EPOCH from m.finished_time)-EXTRACT(EPOCH from m.begin_time))/60),0)/count(m.maintain_code) as dealMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from m.audit_time)-EXTRACT(EPOCH from m.finished_time))/60),0)/count(m.maintain_code) as auditMinute,count(m.maintain_code) as doTimes " +
//            "from ${schema_name}._sc_maintain m " +
//            " left join ${schema_name}._sc_user u on m.maintain_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on m.facility_id=f.id " +
//            " left join ${schema_name}._sc_asset dv on m.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where m.status=60  ${condition} " +
//            "group by ${groupColumn} " +
//            "order by ${groupColumn} desc limit ${page} offset ${begin}")
//    List<AnalysRepairWorkResult> GetMaintainHoursReport(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("analysColumn") String analysColumn, @Param("groupColumn") String groupColumn, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //获取保养的时效数据统计的总数
//    @Select("select count(Tcount) from ( " +
//            "select count(m.maintain_code) as Tcount " +
//            "from ${schema_name}._sc_maintain m " +
//            " left join ${schema_name}._sc_user u on m.maintain_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on m.facility_id=f.id " +
//            " left join ${schema_name}._sc_asset dv on m.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where m.status=60  ${condition} " +
//            "group by ${groupColumn} " +
//            ") AS totalTableTemp")
//    int GetMaintainHoursReportCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("groupColumn") String groupColumn);
//
//    //获取巡检的时效数据统计
//    @Select("select ${analysColumn}," +
//            "COALESCE(SUM((EXTRACT(EPOCH from s.inspection_time)-EXTRACT(EPOCH from s.begin_time))/60),0)/count(s.inspection_code) as dealMinute," +
//            "count(s.inspection_code) as doTimes " +
//            "from ${schema_name}._sc_inspection s " +
//            " left join ${schema_name}._sc_user u on s.inspection_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on s.facility_id=f.id " +
//            " left join ${schema_name}._sc_inspection_area ae on s.area_code=ae.area_code " +
//            "where s.status=60 ${condition} " +
//            "group by ${groupColumn} " +
//            "order by ${groupColumn} desc limit ${page} offset ${begin}")
//    List<AnalysRepairWorkResult> GetInspectionHoursReport(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("analysColumn") String analysColumn, @Param("groupColumn") String groupColumn, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //获取巡检的时效数据统计的总数
//    @Select("select count(Tcount) from ( " +
//            "select count(s.inspection_code) as Tcount " +
//            "from ${schema_name}._sc_inspection s " +
//            " left join ${schema_name}._sc_user u on s.inspection_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on s.facility_id=f.id " +
//            " left join ${schema_name}._sc_inspection_area ae on s.area_code=ae.area_code " +
//            "where s.status=60 ${condition} " +
//            "group by ${groupColumn} " +
//            ") AS totalTableTemp")
//    int GetInspectionHoursReportCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("groupColumn") String groupColumn);
//
//    //获取点检的时效数据统计
//    @Select("select ${analysColumn}," +
//            "COALESCE(SUM((EXTRACT(EPOCH from s.spot_time)-EXTRACT(EPOCH from s.begin_time))/60),0)/count(s.spot_code) as dealMinute," +
//            "count(s.spot_code) as doTimes " +
//            "from ${schema_name}._sc_spotcheck s " +
//            " left join ${schema_name}._sc_user u on s.spot_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on s.facility_id=f.id " +
//            " left join ${schema_name}._sc_inspection_area ae on s.area_code=ae.area_code " +
//            "where s.status=60 ${condition} " +
//            "group by ${groupColumn} " +
//            "order by ${groupColumn} desc limit ${page} offset ${begin}")
//    List<AnalysRepairWorkResult> GetSpotHoursReport(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("analysColumn") String analysColumn, @Param("groupColumn") String groupColumn, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //获取点检的时效数据统计的总数
//    @Select("select count(Tcount) from ( " +
//            "select count(s.spot_code) as Tcount " +
//            "from ${schema_name}._sc_spotcheck s " +
//            " left join ${schema_name}._sc_user u on s.spot_account=u.account " +
//            " left join ${schema_name}._sc_facilities f on s.facility_id=f.id " +
//            " left join ${schema_name}._sc_inspection_area ae on s.area_code=ae.area_code " +
//            "where s.status=60 ${condition} " +
//            "group by ${groupColumn} " +
//            ") AS totalTableTemp")
//    int GetSpotHoursReportCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("groupColumn") String groupColumn);
//
//    //获取备件统计
//    @Select("select b.*,s.stock_name,f.title,c.customer_name,sum(mb.use_count) as maintainCount,sum(rb.use_count) as repairCount from ${schema_name}._sc_bom b " +
//            "left join ${schema_name}._sc_maintain_bom mb " +
//            "on b.bom_model=mb.bom_model and b.facility_id=mb.facility_id and b.stock_code=mb.stock_code " +
//            "left join ${schema_name}._sc_stock s on b.stock_code=s.stock_code " +
//            "left join ${schema_name}._sc_facilities f on b.facility_id=f.id " +
//            "left join ${schema_name}._sc_customers c on b.supplier=c.id " +
//            "left join ${schema_name}._sc_repair_bom rb  " +
//            "on  b.bom_model=rb.bom_model  " +
//            "and b.facility_id=rb.facility_id " +
//            "and b.stock_code=rb.stock_code  " +
//            "${condition}" +
//            "group by b.id ,s.stock_code,f.id,c.id " +
//            "limit ${page} offset ${begin}")
//    List<BomData> GetBom(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    @Select("select count(*) from (select count(*) from ${schema_name}._sc_bom b " +
//            "left join ${schema_name}._sc_maintain_bom mb " +
//            "on b.bom_model=mb.bom_model and b.facility_id=mb.facility_id and b.stock_code=mb.stock_code " +
//            "left join ${schema_name}._sc_stock s on b.stock_code=s.stock_code " +
//            "left join ${schema_name}._sc_facilities f on b.facility_id=f.id " +
//            "left join ${schema_name}._sc_customers c on b.supplier=c.id " +
//            "left join ${schema_name}._sc_repair_bom rb  " +
//            "on  b.bom_model=rb.bom_model  " +
//            "and b.facility_id=rb.facility_id " +
//            "and b.stock_code=rb.stock_code  " +
//            "${condition} " +
//            "group by b.id ,s.stock_code,f.id,c.id ) a")
//    int getBomCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //分析，获取维修的时效数据的总平均值，根据条件
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from r.distribute_time)-EXTRACT(EPOCH from r.begin_time))/60),0) as distributeMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.begin_time)-EXTRACT(EPOCH from r.receive_time))/60),0) as arriveMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.begin_time))/60),0) as dealMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.receive_time))/60),0) as stopMinute," +
//            "COALESCE(SUM((EXTRACT(EPOCH from r.audit_time)-EXTRACT(EPOCH from r.finished_time))/60),0) as auditMinute,count(r.sub_work_code) as doTimes " +
//            "from ${schema_name}._sc_works_detail r " +
//            " left join ${schema_name}._sc_user u on r.receive_account=u.account " +
//            " left join ${schema_name}._sc_works w on r.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on r.work_type_id=wt.id " +
//            " left join ${schema_name}._sc_facilities f on w.facility_id=f.id " +
//            " left join ${schema_name}._sc_asset dv on r.relation_id=dv._id " +
//            "where wt.business_type_id=1 and r.status=60 ${condition} ")
//    AnalysRepairWorkResult GetRepairHoursByFacilityForChart(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //分析，获取维修的百台设备维修次数，按次数，取前20个，根据条件
//    @Select("select analysName,round(sum(res.doTimes)::numeric/sum(res.totalDevices)::numeric,4) as unionTargetValue from ( " +
//            "select (case when pf.title is null then f.title else pf.title end) as analysName,f.title,f.id,COALESCE(ast.totalDevices,0) as totalDevices,COALESCE(r.doTimes,0) as doTimes " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_facilities pf on f.parentid=pf.id " +
//            "left join (select intsiteid,count(_id) as totalDevices from ${schema_name}._sc_asset group by intsiteid ) ast on f.id=ast.intsiteid " +
//            "left join (select facility_id,count(repair_code) as doTimes from ${schema_name}._sc_repair where status=60 ${condition} group by facility_id ) r on f.id=r.facility_id  " +
//            ") res " +
//            "where res.doTimes>0 " +
//            "group by analysName " +
//            "order by unionTargetValue desc " +
//            "LIMIT 20 offset 0 ")
//    List<AnalysRepairWorkResult> GetRepairTimesByPercentForChart(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //分析，单个设备的平均维修时效
//    @Select("select round(res.dealMinute::numeric/res.doTimes::numeric,4) as unionTargetValue from ( " +
//            " select COALESCE(SUM((EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.begin_time))/60),0) as dealMinute,count(r.sub_work_code) as doTimes  " +
//            " from ${schema_name}._sc_asset ast  " +
//            " left join (" +
//            "  select d.sub_work_code,d.begin_time,d.finished_time,d.relation_id,wt.business_type_id " +
//            "from ${schema_name}._sc_works_detail d " +
//            " left join ${schema_name}._sc_work_type wt on d.work_type_id=wt.id " +
//            " where wt.business_type_id=1 and d.relation_type=2 and d.status=60 " +
//            ") r on ast._id=r.relation_id   " +
//            " where ast._id = '${asset_id}' " +
//            " group by ast._id   " +
//            " ) res " +
//            " where res.dotimes>0")
//    AnalysRepairWorkResult GetRepairHourAndTimesByAssetId(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    //分析，单个设备的平均故障间隔
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from w.occur_time)-EXTRACT(EPOCH from lr.finished_time))/60/60),0) as dealMinute,count(w.work_code) as doTimes " +
//            "from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join (" +
//            "select lw.work_code,ld.finished_time from ${schema_name}._sc_works lw " +
//            "left join ${schema_name}._sc_works_detail ld on (lw.work_code=ld.work_code and ld.is_main=1) " +
//            "left join ${schema_name}._sc_work_type lwt on lw.work_type_id=lwt.id " +
//            "where lw.status=60 and lwt.business_type_id=1 and lw.relation_type=2 " +
//            ") lr on lr.work_code=(select max(uw.work_code) " +
//            "from ${schema_name}._sc_works as uw " +
//            "left join ${schema_name}._sc_works_detail ud on uw.work_code=ud.work_code " +
//            "where uw.relation_id = w.relation_id and uw.status=60 and ud.finished_time<w.occur_time ) " +
//            "left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            "where w.status=60 and wt.business_type_id=1 and w.relation_type=2 and w.relation_id='${asset_id}'")
//    AnalysRepairWorkResult GetRepairHourIntervalByAssetId(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    //获取设备监控数据
////    @Select("select m.*,dv.strname as asset_name,dv.intsiteid,dv.supplier,c.customer_name,f.title from ${schema_name}._sc_asset_monitor m " +
////            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
////            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
////            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
////            "where 1=1 ${condition} " +
////            "order by gather_time desc limit ${page} offset ${begin} ")
//    @Select("select m.*,dv.strname as asset_name,dv.intsiteid,dv.supplier,c.customer_name,f.title from ${schema_name}._sc_asset_monitor_current m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
//            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where 1=1 ${condition} " +
//            "order by gather_time desc limit ${page} offset ${begin} ")
//    List<AssetMonitorData> findAllAssetMonitorList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    @Select("select DISTINCT m.*,dv.strname as asset_name,dv.intsiteid,dv.supplier,c.title as customer_name, '' as title,t.monitor_name as monitor_name_new from ${schema_name}._sc_asset_monitor_current m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
////            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_facilities c on dv.supplier=c.id " +
//            "left join ${schema_name}._sc_metadata_monitors t on t.monitor_key=m.monitor_name " +
//            "where t.show_type in('1','4') and  m.asset_code= #{assetCode}  " +
//            "order by gather_time desc")
//    List<AssetMonitorData> findAllAssetMonitorListByCode(@Param("schema_name") String schema_name, @Param("assetCode") String assetCode);
//
//
//    /*
//     * 获取设备监控数据总数
//     */
//    @Select("select count(m.id) from ${schema_name}._sc_asset_monitor_current m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
//            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where 1=1  ${condition} ")
//    int findAllAssetMonitorListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    @Select("select count(m.id) from ${schema_name}._sc_asset_monitor_current m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
//            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where m.asset_code=#{assetCode} ")
//    int findAllAssetMonitorListCountByCode(@Param("schema_name") String schema_name, @Param("assetCode") String assetCode);
//
//
//    @Insert("insert into ${schema_name}._sc_asset_monitor_demo (asset_code, monitor_value1, monitor_value2, " +
//            "monitor_value3, monitor_value4, monitor_value5, monitor_value6, current_timestamp)" +
//            "values(#{assetCode}, #{v1}, #{v2}, #{v3}, #{v4}, #{v4},#{v5}, to_char(CURRENT_TIMESTAMP, '" + SqlConstant.SQL_DATE_TIME_FMT + "'))")
//    int saveMonitorValue(@Param("schema_name") String schema_name, @Param("assetCode") String assetCode,
//                         @Param("v1") String v1, @Param("v2") String v2,
//                         @Param("v3") String v3, @Param("v4") String v4,
//                         @Param("v5") String v5, @Param("v6") String v6);
//
//    @Select("select asset_code, monitor_value1, monitor_value2, monitor_value3, monitor_value4, monitor_value5, " +
//            "monitor_value6, gather_time from ${schema_name}._sc_asset_monitor_demo ")
//    List<Map<String, String>> searchMonitorValue(@Param("schema_name") String schema_name);
//
//    //获取设备监控信息
//    @Select("select m.*,dv.strname as asset_name,dv.intsiteid,dv.supplier,c.customer_name,f.title " +
//            "from ${schema_name}._sc_asset_monitor_current m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
//            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where m.id = ${id}")
//    List<AssetMonitorData> analysAssetMonitorListById(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    //获取设备监控信息根据id
//    @Select("select m.*,dv.strname as asset_name,dv.intsiteid,dv.supplier,c.customer_name,f.title " +
//            "from ${schema_name}._sc_asset_monitor_current m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
//            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where m.id = ${id}")
//    AssetMonitorData analysAssetMonitorDataById(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    //统计设备监控设备的历史记录
//    @Select("select m.*,dv.strname as asset_name,dv.intsiteid,dv.supplier,c.customer_name,f.title from ${schema_name}._sc_asset_monitor m " +
//            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode " +
//            "left join ${schema_name}._sc_facilities f on dv.intsiteid=f.id " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "where m.asset_code = '${assetCode}' and m.monitor_name = '${monitorName}' " +
//            "order by gather_time desc limit ${page} offset ${begin} ")
//    List<AssetMonitorData> searchHistoryAssetMonitorListByAssetCode(@Param("schema_name") String schema_name, @Param("assetCode") String assetCode, @Param("monitorName") String monitorName, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //统计设备监控设备的历史记录信息数量
//    @Select("select count(*) from ${schema_name}._sc_asset_monitor m " +
//            "where m.asset_code = '${assetCode}' and m.monitor_name = '${monitorName}'")
//    int searchHistoryAssetMonitorListByAssetCodeCount(@Param("schema_name") String schema_name, @Param("assetCode") String assetCode, @Param("monitorName") String monitorName);
//
//
//    //统计设备监控设备的历史记录信息数量
//    @Select("SELECT count(dv._id) from ${schema_name}._sc_asset as dv " +
//            "left join ${schema_name}._sc_maintain mt on dv._id = mt.asset_id ${maintainCondition} " +
//            "left join ${schema_name}._sc_facilities f on f.id=mt.facility_id " +
//            "left join ${schema_name}._sc_repair r_run on r_run.asset_id=dv._id  ${repairCondition} " +
//            "where 1=1 and r_run.status=60 ${facilityCondition}")
//    int failurTimes(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition);
//
//    @Select("SELECT count(dv._id) from ${schema_name}._sc_asset as dv " +
//            "left join ${schema_name}._sc_maintain mt on dv._id = mt.asset_id ${maintainCondition} " +
//            "left join ${schema_name}._sc_facilities f on f.id=mt.facility_id " +
//            "left join ${schema_name}._sc_repair r_run on r_run.asset_id=dv._id ${repairCondition} " +
//            "where 1=1  ${facilityCondition}")
//    int allTime(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition);
//
//    //故障间隔函数效率低，已经修改为新方法，看板指标中有，如需此函数，请重构 2019-03-20 yaozj
//    @Select("select foo.finished_time,foo.occur_time   from " +
//            "(SELECT  r_run.finished_time as finished_time,${schema_name}.get_latest_next_repair_occur_time(r_run.asset_id,r_run.repair_code) as occur_time " +
//            " FROM ${schema_name}._sc_repair r_run  " +
//            " left join ${schema_name}._sc_asset dv on r_run.asset_id=dv._id  " +
//            " where 1=1 and r_run.status=60 and (r_run.finished_time is not null) ${facilityCondition} ${repairCondition} ) foo where foo.occur_time is not null")
//    List<RepairData> MTBF(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition);
//
//    @Select("SELECT  r_run.finished_time as finished_time,r_run.occur_time as occur_time,r_run.createtime as createtime ,r_run.repairbegin_time as repairbegin_time,r_run.receive_time as receive_time " +
//            "FROM ${schema_name}._sc_repair r_run " +
//            "left join ${schema_name}._sc_asset dv on r_run.asset_id=dv._id " +
//            "left join ${schema_name}._sc_maintain mt on dv._id = mt.asset_id " +
//            "where 1=1 and r_run.status=60  and (r_run.finished_time is not null) and (r_run.occur_time is not null) ${facilityCondition} ${repairCondition}")
//    List<RepairData> meanTimeToRestore(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition);
//
//
//    //获取员工的任务完成数排名
//    @Select("select username,total_count,finished_count,unfinished_count,delay_count,finish_rate " +
//            " from ( " +
//            " select username,user_account,sum(finished_count) as finished_count," +
//            "sum(unfinished_count) as unfinished_count,sum(delay_count) as delay_count,sum(total_count) as total_count," +
//            "(case when sum(total_count)=0 then 0 else (sum(finished_count)/sum(total_count)::NUMERIC) end) as finish_rate " +
//            " from ( " +
//            " select task.code,task.user_account,u.username,task.facility_id,task.createtime,task.status," +
//            "(case when task.status=60 then 1 else 0 end) as finished_count, " +
//            "(case when task.status in (10,20,30,40,50,70,80) then 1 else 0 end) as unfinished_count," +
//            "(case when task.status=100 then 1 else 0 end) as delay_count, " +
//            "(case when task.status!=900 then 1 else 0 end) as total_count " +
//            " from ( " +
//            "  select rp.facility_id,rp.createtime,rp.repair_code as code,rp.receive_account as user_account,rp.status " +
//            "from ${schema_name}._sc_repair rp where rp.status!=900 " +
//            " union " +
//            " select mt.facility_id,mt.createtime,mt.maintain_code as code,mt.maintain_account as user_account,mt.status " +
//            "from ${schema_name}._sc_maintain mt where mt.status!=900 " +
//            " union " +
//            " select ip.facility_id,ip.createtime,ip.inspection_code as code,ip.inspection_account as user_account,ip.status " +
//            "from ${schema_name}._sc_inspection ip where ip.status!=900 " +
//            " union " +
//            " select sp.facility_id,sp.spot_time as createtime,sp.spot_code as code,sp.spot_account as user_account,sp.status " +
//            "from ${schema_name}._sc_spotcheck sp where sp.status!=900 " +
//            " ) task " +
//            " left join ${schema_name}._sc_user u on task.user_account=u.account " +
//            " where 1=1 ${condition} " +
////            " and task.createtime>='2018-08-26 00:00:00.0' and task.createtime<='2018-09-01 23:59:00.0' " +
////            "-- <%if(!!obj.beginTime) {%> and task.createtime>='${obj.beginTime} 00:00:00.0'<%} if(!!obj.endTime) {%> and task.createtime<='${obj.endTime} 23:59:59.999'<%}  " +
////            "--  if(!!obj.facility && obj.facility.indexOf('-1') == -1) {%> and task.facility_id in (${obj.facility})<%}%> " +
//            " ) foo " +
//            "  group by username,user_account " +
//            ") topfoo " +
//            "  order by finish_rate desc " +
//            " limit 50")
//    List<AnalysRepariMaintainTaskCountResult> getUserTaskCount(@Param("schema_name") String schema_name, @Param("condition") String condition);

}
