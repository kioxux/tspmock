package com.gengyun.senscloud.mapper;


import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 客户管理
 */
@Mapper
public interface CustomersMapper {

    String workSql = "(select count(1) ,a.relation_id facility_id,'4' from ${schema_name}._sc_works a where a.relation_type='4'  " +
            " and a.status  &lt;" + StatusConstant.COMPLETED + " and a.status &gt; " + StatusConstant.DRAFT +
            " group by a.relation_id " +
            " union select count(1),b.cus_id::varchar facility_id,'2' from ${schema_name}._sc_works a join ${schema_name}._sc_asset_cus b on a.relation_id= b.asset_id " +
            " where a.relation_type= '2' " +
            " and a.status &lt;" + StatusConstant.COMPLETED + " and a.status &gt; " + StatusConstant.DRAFT +
            " group by b.cus_id " +
            " union select count(1),c.cus_id::varchar,'1' from ${schema_name}._sc_works a join ${schema_name}._sc_asset_cus c on a.relation_id = c.position_code where a.relation_type='1' " +
            " and a.status &lt;" + StatusConstant.COMPLETED + " and a.status &gt; " + StatusConstant.DRAFT +
            " group by c.cus_id )";

    /**
     * 位置限定sql
     */
    String positionFilterSql = "(select distinct sr.asset_position_code from " +
            " ${schema_name}._sc_role_asset_position sr " +
            " join ${schema_name}._sc_role f on sr.role_id = f.id " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id " +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id} )";

    /**
     * 查询客户列表
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " SELECT count(distinct cu.facility_id) " +
//            " FROM  ${schema_name}._sc_facilities f " +
            " FROM ${schema_name}._sc_customer cu " +
            " LEFT join ${schema_name}._sc_role_asset_position bb on bb.asset_position_code = cu.position_code " +
            " LEFT join (select distinct f.id from " +
            " ${schema_name}._sc_role f " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id" +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id}) m on m.id = bb.role_id" +
            " LEFT JOIN ${schema_name}._sc_user u ON cu.create_user_id = u.id " +
            " left join ${schema_name}._sc_pollute_fee pf on cu.facility_id=pf.facility_id " +
            " WHERE cu.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='pm.importantLevel!=null and pm.importantLevel!=\"\"'> " +
            " AND  cu.important_level_id IN (${pm.importantLevel}) " +
            " </when> " +
            " <when test='pm.positionCode!=null and pm.positionCode!=\"\"'> " +
            " AND  cu.position_code IN (${pm.positionCode}) " +
            " </when> " +
            " <when test='pm.polluteFeeStatus!=null and pm.polluteFeeStatus!=\"\"'> " +
            " AND  pf.status IN (${pm.polluteFeeStatus}) " +
            " </when> " +
            " <when test='pm.customerType!=null and pm.customerType!=\"\"'> " +
            " AND  cu.customer_type_id IN (${pm.customerType}) " +
            " </when> " +
            " <when test='pm.unloadingGroup!=null and pm.unloadingGroup!=\"\"'> " +
            " AND  cu.unloading_group_id IN (${pm.unloadingGroup}) " +
            " </when> " +
            " <when test='pm.isUseSearch!=null and pm.isUseSearch!=\"\" '> " +
            " AND  cu.is_use = #{pm.isUseSearch} " +
            " </when> " +
            " <when test='pm.orgTypeSearch!=null and pm.orgTypeSearch!=\"\"'> " +
            " AND  cu.org_type_id IN (${pm.orgTypeSearch}) " +
            " </when> " +
            " <when test='pm.sewage_destination!=null and pm.sewage_destination!=\"\"'> " +
            " AND  cu.sewage_destination IN (${pm.sewage_destination}) " +
            " </when> " +
            " <when test='pm.collection_method!=null and pm.collection_method!=\"\"'> " +
            " AND  cu.collection_method IN (${pm.collection_method}) " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( cu.title iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR cu.inner_code iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR cu.short_title iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when>  " +
            " </script>")
    int findCustomersListCount(@Param("schema_name") String schema_name, @Param("user_id") String user_id, @Param("pm") Map<String, Object> pm);

    /**
     * 查询客户列表
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " SELECT distinct cu.facility_id as id,cu.title,cu.parent_id,cu.create_time,cu.location::text,cu.org_type_id,cu.inner_code," +
            " cu.address,cu.layer_path,cu.is_use,cu.short_title,cu.create_user_id,u.account AS create_user_account," +
            " cu.remark,cu.currency_id,cu.unloading_group_id,cu.position_code,cu.pipeline_code,cu.sewage_destination," +
            " cu.main_products,cu.province,cu.city,cu.zone,cu.important_level_id,cu.customer_type_id,cu.sp_code " +
//            " (select string_agg(concat(b.begin_time::text,'-',b.end_time::text),',') from ${schema_name}._sc_customer_unloading_time b where b.facility_id = f.id) as unloading_time " +
//            " FROM  ${schema_name}._sc_facilities f " +
            " FROM ${schema_name}._sc_customer cu " +
            " LEFT join ${schema_name}._sc_role_asset_position bb on bb.asset_position_code = cu.position_code " +
            " LEFT join (select distinct f.id from " +
            " ${schema_name}._sc_role f " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id" +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id}) m on m.id = bb.role_id" +
            " LEFT JOIN ${schema_name}._sc_user u ON cu.create_user_id = u.id " +
            " left join ${schema_name}._sc_pollute_fee pf on cu.facility_id=pf.facility_id " +
            " WHERE cu.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='pm.importantLevel!=null and pm.importantLevel!=\"\"'> " +
            " AND  cu.important_level_id IN (${pm.importantLevel}) " +
            " </when> " +
            " <when test='pm.positionCode!=null and pm.positionCode!=\"\"'> " +
            " AND  cu.position_code IN (${pm.positionCode}) " +
            " </when> " +
            " <when test='pm.polluteFeeStatus!=null and pm.polluteFeeStatus!=\"\"'> " +
            " AND  pf.status IN (${pm.polluteFeeStatus}) " +
            " </when> " +
            " <when test='pm.customerType!=null and pm.customerType!=\"\"'> " +
            " AND  cu.customer_type_id IN (${pm.customerType}) " +
            " </when> " +
            " <when test='pm.unloadingGroup!=null and pm.unloadingGroup!=\"\"'> " +
            " AND  cu.unloading_group_id IN (${pm.unloadingGroup}) " +
            " </when> " +
            " <when test='pm.isUseSearch!=null and pm.isUseSearch!=\"\" '> " +
            " AND  cu.is_use = #{pm.isUseSearch} " +
            " </when> " +
            " <when test='pm.orgTypeSearch!=null and pm.orgTypeSearch!=\"\"'> " +
            " AND  cu.org_type_id IN (${pm.orgTypeSearch}) " +
            " </when> " +
            " <when test='pm.sewage_destination!=null and pm.sewage_destination!=\"\"'> " +
            " AND  cu.sewage_destination IN (${pm.sewage_destination}) " +
            " </when> " +
            " <when test='pm.collection_method!=null and pm.collection_method!=\"\"'> " +
            " AND  cu.collection_method IN (${pm.collection_method}) " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( cu.title iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR cu.inner_code iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR cu.short_title iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " <when test='pm.ids!=null'> " +
            " AND  cu.id IN " +
            " <foreach collection='pm.ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when> " +
            " ORDER BY cu.create_time DESC  ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findCustomersList(@Param("schema_name") String schema_name, @Param("user_id") String user_id, @Param("pm") Map<String, Object> pm);

    @Insert(" INSERT into ${schema_name}._sc_customer (facility_id,unloading_group_id,position_code,pipeline_code," +
            " sewage_destination,main_products,province,city,zone,important_level_id,customer_type_id,create_user_id," +
            " title,remark,short_title,inner_code,location,org_type_id,is_use)  VALUES  " +
            " (#{pm.facility_id},#{pm.unloading_group_id},#{pm.position_code},#{pm.pipeline_code}," +
            "#{pm.sewage_destination},#{pm.main_products},#{pm.province},#{pm.city}," +
            "#{pm.zone},#{pm.important_level_id},#{pm.customer_type_id},#{pm.create_user_id}," +
            "#{pm.title},#{pm.remark},#{pm.short_title},#{pm.inner_code},#{pm.location} ::point,#{pm.org_type_id} ::int,#{pm.is_use})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertCustomer(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据id查询客户详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 厂商详情
     */
    @Select(" SELECT cu.facility_id as id,title,parent_id,cu.create_time,location,org_type_id::varchar,inner_code,address," +
            " layer_path,short_title,cu.create_user_id,remark,currency_id,unloading_group_id," +
            " position_code,pipeline_code,sewage_destination,main_products,province,city," +
            " zone,important_level_id,customer_type_id,cu.collection_method,cu.sp_code,cu.sewage_value," +
            " cu.siren_value,cu.abnormal_value " +
//            " FROM ${schema_name}._sc_facilities f  " +
            " FROM ${schema_name}._sc_customer cu " +
            " WHERE cu.facility_id = #{id} ")
    Map<String, Object> findByID(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * @param schema_name
     * @param pm
     */
    /*@Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer " +
            " SET id= id " +
            " <when test='pm.unloading_group_id!=null'> " +
            " ,unloading_group_id=#{pm.unloading_group_id}" +
            " </when> " +
            " <when test='pm.position_code!=null'> " +
            " ,position_code=#{pm.position_code}" +
            " </when> " +
            " <when test='pm.pipeline_code!=null'> " +
            " ,pipeline_code=#{pm.pipeline_code}" +
            " </when> " +
            " <when test='pm.sewage_destination!=null'> " +
            " ,sewage_destination=#{pm.sewage_destination}" +
            " </when> " +
            " <when test='pm.main_products!=null'> " +
            " ,main_products=#{pm.main_products}" +
            " </when> " +
            " <when test='pm.province!=null'> " +
            " ,province=#{pm.province}" +
            " </when> " +
            " <when test='pm.city!=null'> " +
            " ,city=#{pm.city}" +
            " </when> " +
            " <when test='pm.zone!=null'> " +
            " ,zone=#{pm.zone}" +
            " </when> " +
            " <when test='pm.important_level_id!=null'> " +
            " ,important_level_id=#{pm.important_level_id}" +
            " </when> " +
            " <when test='pm.customer_type_id!=null'> " +
            " ,customer_type_id=#{pm.customer_type_id}" +
            " </when> " +
            " where facility_id=#{pm.id}::int" +
            " </script>")*/
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer " +
            " SET is_use=is_use" +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.parent_id!=null'> " +
            " ,unloading_group_id=#{pm.unloading_group_id}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,position_code=#{pm.position_code}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,pipeline_code=#{pm.pipeline_code}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,sewage_destination=#{pm.sewage_destination}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,main_products=#{pm.main_products}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,province=#{pm.province}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,city=#{pm.city}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,zone=#{pm.zone}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,important_level_id=#{pm.important_level_id}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,customer_type_id=#{pm.customer_type_id}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesTop\" '> " +
            " ,title=#{pm.title}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesTop\" '> " +
            " ,inner_code=#{pm.inner_code}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,remark=#{pm.remark}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,location=#{pm.location} ::point" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,address=#{pm.address}" +
            " </when> " +
            " <when test=\"null != pm.fileIdsChange and !'' == pm.fileIdsChange\"> " +//类型更新修改设备附件信息
            " ,file_ids=#{pm.file_ids}" +
            " </when>" +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,collection_method=#{pm.collection_method}" +
            " </when>" +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,sp_code=#{pm.sp_code}" +
            " </when>" +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,sewage_value=#{pm.sewage_value}::numeric" +
            " </when>" +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,siren_value=#{pm.siren_value}::numeric" +
            " </when>" +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,abnormal_value=#{pm.abnormal_value}::numeric" +
            " </when>" +
            " where facility_id=#{pm.id}::int and status=1" +
            " </script>")
    void updateCustomers(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询客户列表
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " SELECT cu.facility_id as id,cu.title,cu.parent_id,cu.create_time,cu.location,cu.org_type_id,cu.inner_code," +
            " cu.address,cu.layer_path,cu.is_use,cu.short_title," +
            " cu.remark,cu.currency_id,cu.unloading_group_id,cu.position_code,cu.pipeline_code,cu.sewage_destination," +
            " cu.main_products,cu.province,cu.city,cu.zone,cu.important_level_id,cu.customer_type_id, " +
            " (select ut.begin_time || '-' || ut.end_time from ${schema_name}._sc_customer_unloading_time ut where ut.facility_id=cu.facility_id and ut.status > -1000 ORDER BY ut.begin_time desc,ut.end_time desc limit 1) as ut_time " +
//            " (select string_agg(concat(b.begin_time::text,'-',b.end_time::text),',') from ${schema_name}._sc_customer_unloading_time b where b.facility_id = f.id) as unloading_time " +
//            " FROM  ${schema_name}._sc_facilities f " +
            " FROM ${schema_name}._sc_customer cu " +
            " WHERE cu.status > " + StatusConstant.STATUS_DELETEED +
            " AND  cu.unloading_group_id = #{pm.unloadingGroup} " +
            " AND cu.facility_id!=#{pm.id}::int " +
            " <when test='pm.keywordSearch!=null'> " +
            " and (cu.title ilike concat('%',#{pm.keywordSearch},'%') " +
            " or cu.inner_code ilike concat('%',#{pm.keywordSearch},'%')) " +
            " </when> " +
            " ORDER BY cu.create_time DESC " +
            " </script>")
    List<Map<String, Object>> findRelationCustomersList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 关联设备列表
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select("<script>" +
            "SELECT sa.id,sa.asset_code,sa.asset_name,sa.status,sa.category_id,sa.asset_model_id,sa.position_code " +
            "FROM  ${schema_name}._sc_asset sa " +
            "JOIN ${schema_name}._sc_asset_organization ao on sa.id = ao.asset_id " +
            "WHERE ao.org_id = #{pm.id}::int" +
            "</script>")
    List<Map<String, Object>> findRelationAssetList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("<script>" +
            "SELECT * FROM ${schema_name}._sc_asset_cus " +
            "<where> cus_id = #{pm.id}::int " +
            " <if test=\"pm.keywordSearch !=null and pm.keywordSearch !='' \"> " +
            " and (asset_name ilike concat('%', #{pm.keywordSearch}, '%') " +
            " or asset_code ilike concat('%', #{pm.keywordSearch}, '%') " +
            " or position_name ilike concat('%', #{pm.keywordSearch}, '%')) " +
            " </if> " +
            " <if test=\"pm.pagination !=null and pm.pagination !='' \"> " +
            " ${pm.pagination}" +
            " </if> " +
            " </where> " +
            "</script>")
    List<Map<String, Object>> findRelationCusAssetList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 污染因子列表
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select("<script>" +
            "select pf.id,pf.facility_id,pf.factor_id,pf.limit_value,pf.limit_unit,pf.remark " +
            "from ${schema_name}._sc_customer_pollute_factor pf " +
            "<where>" +
            " <when test='pm.factor_id!=null'> " +
            " and pf.factor_id = #{pm.factor_id} " +
            " </when> " +
            " <when test='pm.facility_id!=null'> " +
            " and pf.facility_id = #{pm.facility_id}::int " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " and (pf.factor_id ilike concat('%',#{pm.keywordSearch},'%') " +
            " or pf.remark ilike concat('%',#{pm.keywordSearch},'%') " +
            " or pf.limit_value ::text ilike concat('%',#{pm.keywordSearch},'%') ) " +
            " </when> " +
            " AND pf.status >" + StatusConstant.STATUS_DELETEED +
            " </where> " +
            " order by pf.id " +
            " </script>")
    List<Map<String, Object>> findPolluteFactorList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增污染因子
     *
     * @param schema_name
     * @param pm
     */
    @Insert(" INSERT into ${schema_name}._sc_customer_pollute_factor (facility_id,factor_id,limit_value,limit_unit," +
            "begin_date,end_date,status,create_user_id,remark)  VALUES  " +
            " (#{pm.facility_id}::int8,#{pm.factor_id},#{pm.limit_value}::numeric,#{pm.limit_unit}," +
            "#{pm.begin_date}::timestamp,#{pm.end_date}::timestamp,#{pm.status}::int,#{pm.create_user_id},#{pm.remark})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void InsertPolluteFactor(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 编辑污染因子
     *
     * @param schema_name
     * @param pm
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer_pollute_factor " +
            " SET id=#{pm.id}::int " +
            " <when test='pm.facility_id!=null'> " +
            " ,facility_id=#{pm.facility_id}::int8" +
            " </when> " +
            " <when test='pm.factor_id!=null'> " +
            " ,factor_id =#{pm.factor_id}" +
            " </when> " +
            " <when test='pm.limit_value!=null'> " +
            " ,limit_value=#{pm.limit_value}::numeric" +
            " </when> " +
            " <when test='pm.limit_unit!=null'> " +
            " ,limit_unit=#{pm.limit_unit}" +
            " </when> " +
            " <when test='pm.begin_date!=null'> " +
            " ,begin_date=#{pm.begin_date}::timestamp" +
            " </when> " +
            " <when test='pm.end_date!=null'> " +
            " ,end_date=#{pm.end_date}::timestamp" +
            " </when> " +
            " <when test='pm.status!=null'> " +
            " ,status=#{pm.status}::int" +
            " </when> " +
            " <when test='pm.create_user_id!=null'> " +
            " ,create_user_id=#{pm.create_user_id}" +
            " </when> " +
            " ,remark=#{pm.remark}" +
            " where id= #{pm.id}::int " +
            " </script>")
    void updatePolluteFactor(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 根据id查询污染因子详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 厂商详情
     */
    @Select(" SELECT f.id,f.facility_id,f.factor_id,f.limit_value,f.limit_unit,f.begin_date,f.end_date,f.remark " +
            " FROM ${schema_name}._sc_customer_pollute_factor f  " +
            " WHERE f.id = #{id} ")
    Map<String, Object> findPolluteFactorByID(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 排放时间列表
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select("<script>" +
            "select ut.id,ut.facility_id,ut.begin_time,ut.end_time,ut.begin_date,ut.end_date " +
            "from ${schema_name}._sc_customer_unloading_time ut " +
            "WHERE ut.facility_id = #{pm.id}::int " +
            "AND ut.status >" + StatusConstant.STATUS_DELETEED +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( ut.begin_time ::text ilike CONCAT('%',#{pm.keywordSearch},'%') " +
            " OR ut.end_time ::text ilike CONCAT('%',#{pm.keywordSearch},'%')) " +
            " </when> " +
            "order by ut.id " +
            "</script>")
    List<Map<String, Object>> findUnloadingTimeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增排污时间
     *
     * @param schema_name
     * @param pm
     */
    @Insert(" INSERT into ${schema_name}._sc_customer_unloading_time (facility_id,begin_time,end_time," +
            " begin_date,end_date,status,create_user_id)  VALUES  " +
            " (#{pm.facility_id}::int8,#{pm.begin_time}::time,#{pm.end_time}::time," +
            " #{pm.begin_date}::timestamp,#{pm.end_date}::timestamp,#{pm.status}::int,#{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void InsertUnloadingTime(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 编辑排污时间
     *
     * @param schema_name
     * @param pm
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer_unloading_time " +
            " SET id=#{pm.id}::int " +
            " <when test='pm.facility_id!=null'> " +
            " ,facility_id=#{pm.facility_id}::int8" +
            " </when> " +
            " <when test='pm.begin_time!=null'> " +
            " ,begin_time=#{pm.begin_time}::time" +
            " </when> " +
            " <when test='pm.end_time!=null'> " +
            " ,end_time=#{pm.end_time}::time" +
            " </when> " +
            " <when test='pm.begin_date!=null'> " +
            " ,begin_date=#{pm.begin_date}::timestamp" +
            " </when> " +
            " <when test='pm.end_date!=null'> " +
            " ,end_date=#{pm.end_date}::timestamp" +
            " </when> " +
            " <when test='pm.status!=null'> " +
            " ,status=#{pm.status}::int" +
            " </when> " +
            " <when test='pm.create_user_id!=null'> " +
            " ,create_user_id=#{pm.create_user_id}" +
            " </when> " +
            " where id = #{pm.id}::int " +
            " </script>")
    void updateUnloadingTime(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 根据id查询排污时间详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 厂商详情
     */
    @Select(" SELECT f.id,f.facility_id,f.begin_time,f.end_time,f.begin_date,f.end_date " +
            " FROM ${schema_name}._sc_customer_unloading_time f  " +
            " WHERE f.id = #{id} ")
    Map<String, Object> findUnloadingTimeByID(@Param("schema_name") String schema_name, @Param("id") Integer id);


    /**
     * 查询客户地图信息
     *
     * @param schemaName
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " SELECT cu.facility_id as id,cu.title,cu.parent_id,cu.create_time,cu.location::varchar,cu.org_type_id,cu.inner_code," +
            " cu.address,cu.layer_path,cu.is_use,cu.short_title,cu.create_user_id,u.account AS create_user_account," +
            " cu.remark,cu.currency_id,cu.unloading_group_id,cu.position_code,cu.pipeline_code,cu.sewage_destination," +
            " cu.main_products,cu.province,cu.city,cu.zone,cu.important_level_id,cu.customer_type_id,cu.sp_code, " +
//            " (select string_agg(concat(b.begin_time::text,'-',b.end_time::text),',') from ${schema_name}._sc_customer_unloading_time b where b.facility_id = f.id) as unloading_time, " +
            " COALESCE(d.works,0) works " +
//            " FROM  ${schema_name}._sc_facilities f " +
            " FROM ${schema_name}._sc_customer cu " +
            " LEFT JOIN ${schema_name}._sc_user u ON cu.create_user_id = u.id " +
            " LEFT JOIN (select sum(count) works,c.facility_id from " + workSql + " c group by c.facility_id) d " +
            " on cu.facility_id = d.facility_id::int" +
            " WHERE cu.status > " + StatusConstant.STATUS_DELETEED +
            " and cu.position_code in " +
            positionFilterSql +
            " </script>")
    List<Map<String, Object>> findCustomerPositionForMapList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * 查询客户下设备的地图
     *
     * @param schemaName
     * @param userId
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select distinct a.location::varchar,a.asset_name,a.asset_code, " +
            " a.status,a.asset_icon,a.file_ids,a.properties,a.create_time, " +
            " a.asset_id,a.maintain_begin_time,a.enable_time,a.supplier_id, " +
            " a.parent_id,a.quantity,a.unit_id,a.create_user_id,a.category_id, " +
            " a.importment_level_id,a.price,a.tax_rate,a.tax_price,a.currency_id, " +
            " a.position_code,a.buy_date,a.install_date,a.manufacturer_id,a.asset_model_id, " +
            " a.install_price,a.use_year,a.install_currency_id,a.buy_currency_id,a.guarantee_status_id, " +
            " a.running_status_id,a.data_order,a.remark,a.cus_id,a.category_name,a.model_name,a.position_name, " +
            " a.bind_status,tab.properties ::jsonb ->> 'monitorSiteCode' as monitor_site_code,cu.title, " +
            " cast((select cm.indication from ${schema_name}._sc_meter_item cm where cm.asset_id=a.asset_id and cm.facility_id=a.cus_id and cm.check_item='0' ORDER BY cm.create_time desc limit 1) as decimal(10,2)) as indication, " +
            " cast((select sum(cm.current_flow) from ${schema_name}._sc_meter_item cm where cm.asset_id=a.asset_id and cm.facility_id=a.cus_id group by cm.asset_id) as decimal(10,2)) as final_flow, " +
            " cu.sp_code " +
            " from ${schema_name}._sc_asset_cus a " +
            " join (select tb.cus_id,tb.asset_id,json_object_agg (tb.code,tb.value) ::text as properties from " +
            " (select *,jsonb_array_elements(properties) ->> 'field_code' as code,jsonb_array_elements(properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus) tb group by tb.cus_id,tb.asset_id " +
            " union " +
            " \n" +
            " select cus_id,asset_id,null as properties from ${schema_name}._sc_asset_cus where (properties='[]' or properties is null)) tab " +
            " on a.asset_id=tab.asset_id and a.cus_id=tab.cus_id " +
            " LEFT JOIN ${schema_name}._sc_customer cu ON a.cus_id = cu.facility_id " +
            " join ${schema_name}._sc_asset_position d on cu.position_code = d.position_code " +
            " LEFT JOIN ${schema_name}._sc_user u ON cu.create_user_id = u.id " +
            " WHERE cu.status > " + StatusConstant.STATUS_DELETEED + "and a.bind_status = 1 " +
            " and cu.position_code in " +
            positionFilterSql +
            " </script>")
    List<Map<String, Object>> findAllCustomerPositionForMapList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    @Select(" <script>" +
            " select distinct a.location::varchar,a.asset_name,a.asset_code, " +
            " a.status,a.asset_icon,a.file_ids,a.properties,a.create_time, " +
            " a.asset_id,a.maintain_begin_time,a.enable_time,a.supplier_id, " +
            " a.parent_id,a.quantity,a.unit_id,a.create_user_id,a.category_id, " +
            " a.importment_level_id,a.price,a.tax_rate,a.tax_price,a.currency_id, " +
            " a.position_code,a.buy_date,a.install_date,a.manufacturer_id,a.asset_model_id, " +
            " a.install_price,a.use_year,a.install_currency_id,a.buy_currency_id,a.guarantee_status_id, " +
            " a.running_status_id,a.data_order,a.remark,a.cus_id,a.category_name,a.model_name,a.position_name, " +
            " a.bind_status,tab.properties ::jsonb ->> 'monitorSiteCode' as monitor_site_code,cu.title, " +
            " cast((select cm.indication from ${schema_name}._sc_meter_item cm where cm.asset_id=a.asset_id and cm.facility_id=a.cus_id and cm.check_item='0' ORDER BY cm.create_time desc limit 1) as decimal(10,2)) as indication, " +
            " cast((select sum(cm.current_flow) from ${schema_name}._sc_meter_item cm where cm.asset_id=a.asset_id and cm.facility_id=a.cus_id group by cm.asset_id) as decimal(10,2)) as final_flow, " +
            " cu.sp_code " +
            " from ${schema_name}._sc_asset_cus a " +
            " join (select tb.cus_id,tb.asset_id,json_object_agg (tb.code,tb.value) ::text as properties from " +
            " (select *,jsonb_array_elements(properties) ->> 'field_code' as code,jsonb_array_elements(properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus) tb group by tb.cus_id,tb.asset_id " +
            " union " +
            " \n" +
            " select cus_id,asset_id,null as properties from ${schema_name}._sc_asset_cus where (properties='[]' or properties is null)) tab " +
            " on a.asset_id=tab.asset_id and a.cus_id=tab.cus_id " +
            " LEFT JOIN ${schema_name}._sc_customer cu ON a.cus_id = cu.facility_id " +
            " LEFT JOIN ${schema_name}._sc_user u ON cu.create_user_id = u.id " +
            " WHERE cu.facility_id=#{pm.id}::int and cu.status > " + StatusConstant.STATUS_DELETEED + "and a.bind_status = 1 " +
            " </script>")
    List<Map<String, Object>> findAllAssetForMapList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    @Select("<script>" +
            " select distinct tab.cus_id as facility_id,tab.title,tab.asset_id,tab.asset_name,tab.sid_url, " +
            " coalesce(tab.sewage_value,(select code::numeric from ${schema_name}._sc_cloud_data " +
            " where data_type='pollute_price_seting' and reserve1='sewage_value' limit 1)) as sewage_value, " +
            " coalesce(tab.siren_value,(select code::numeric from ${schema_name}._sc_cloud_data " +
            " where data_type='pollute_price_seting' and reserve1='siren_value' limit 1)) as siren_value, " +
            " coalesce(tab.abnormal_value,(select code::numeric from ${schema_name}._sc_cloud_data " +
            " where data_type='pollute_price_seting' and reserve1='abnormal_value' limit 1)) as abnormal_value, " +
            " properties ::jsonb ->> 'GRM' as grm, properties ::jsonb ->> 'PASS' as pass " +
            " from (select tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sid_url,tb.sewage_value,tb.siren_value,tb.abnormal_value, " +
            " json_object_agg (tb.code,tb.value) ::text as properties " +
            " from (select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,ac.sid_url, " +
            " coalesce(ac.sewage_value,cu.sewage_value) as sewage_value, " +
            " coalesce(ac.siren_value,cu.siren_value) as siren_value, " +
            " coalesce(ac.abnormal_value,cu.abnormal_value) as abnormal_value, " +
            " jsonb_array_elements(ac.properties) ->> 'field_code' as code, " +
            " jsonb_array_elements(ac.properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu " +
            " on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and cu.facility_id=#{pm.id}::int) tb group by tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sid_url,tb.sewage_value,tb.siren_value,tb.abnormal_value " +
            " union " +
            " select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,null as sid_url, " +
            " coalesce(ac.sewage_value,cu.sewage_value) as sewage_value, " +
            " coalesce(ac.siren_value,cu.siren_value) as siren_value, " +
            " coalesce(ac.abnormal_value,cu.sewage_value) as abnormal_value, " +
            " null as properties " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu " +
            " on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and cu.facility_id=#{pm.id}::int and (ac.properties='[]' or ac.properties is null)) tab " +
            "</script>")
    List<Map<String, Object>> findAssetForMapByCusList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);


    @Select(" <script>" +
            " SELECT cu.title" +
            " FROM  ${schema_name}._sc_customer cu" +
            " WHERE  cu.facility_id IN  " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " and cu.status > -1000" +
            " </script>")
    List<String> findCusNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    @Select(" <script> " +
            " select count(1) from ${schema_name}._sc_asset_cus where cus_id in " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " and bind_status = 1" +
            " </script>")
    int findAssetCustomerCount(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    @Select(" <script> " +
            " select count(1) from ${schema_name}._sc_pollute_fee where facility_id in " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " and status not in (1,2)" +
            " </script>")
    int findPolluteCustomerCount(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    @Select(" <script>" +
            " update ${schema_name}._sc_customer cu set status = -1000" +
            " WHERE  cu.facility_id IN  " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " and cu.status > -1000" +
            " </script>")
    void deleteCusByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    @Select("select * from ${schema_name}._sc_customer where facility_id=#{facility_id}")
    Map<String, Object> findCustomerById(@Param("schema_name") String schema_name, @Param("facility_id") Integer facility_id);

    @Select(" <script> " +
            " SELECT t.id as file_id,t.file_original_name,Round(t.file_size/1024,4) AS file_size,t.file_type_id,t.file_name,t.file_url,ft.type_name," +
            " to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time_str," +
            " t.remark, t.file_category_id,fc.file_category_name AS file_category_name   " +
            " FROM ${schema_name}._sc_files t " +
            " LEFT JOIN ${schema_name}._sc_file_type  ft ON t.file_type_id = ft.id " +
            " LEFT JOIN ${schema_name}._sc_file_category  fc ON t.file_category_id = fc.id " +
            " WHERE t.id in (${file_ids}) " +
            " <choose>" +
            "    <when test='keywordSearch != null'> " +
            "       and (t.file_original_name ilike CONCAT('%', #{keywordSearch}, '%') or t.remark ilike CONCAT('%', #{keywordSearch}, '%'))  " +
            "    </when> " +
            " </choose>" +
            " ORDER BY t.id desc " +
            " </script> ")
    List<Map<String, Object>> findCustomerFileList(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids, @Param("keywordSearch") String keywordSearch);

    @Insert(" INSERT INTO ${schema_name}._sc_customer_contact(org_id, contact_name,contact_mobile,contact_email,wei_xin,create_user_id,job, create_time)" +
            " VALUES (#{pm.org_id}::int, #{pm.contact_name},#{pm.contact_mobile}, #{pm.contact_email},#{pm.wei_xin},#{pm.create_user_id},#{pm.job},current_timestamp )")
    void insertCustomerContact(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除厂商联系人
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete(" DELETE FROM  ${schema_name}._sc_customer_contact WHERE ID = #{id} ")
    void deleteOrganizationContact(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 查询厂商联系人列表
     */
    @Select(" <script>" +
            " SELECT id,org_id, contact_name, contact_mobile,contact_email, wei_xin,job" +
            " FROM ${schema_name}._sc_customer_contact " +
            " WHERE org_id = #{pm.org_id}::int" +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( contact_name iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR contact_mobile iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR contact_email iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR wei_xin iLIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR job ilike concat('%',#{pm.keywordSearch},'%') ) " +
            " </when> " +
            " ORDER BY id" +
            " </script> ")
    List<Map<String, Object>> findOrganizationContactList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 厂商联系人详情
     */
    @Select(" SELECT id,org_id, contact_name, contact_mobile,contact_email, wei_xin,job" +
            " FROM ${schema_name}._sc_customer_contact " +
            " WHERE id = #{id}")
    Map<String, Object> findOrganizationContactInfo(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 编辑厂商联系人
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer_contact " +
            " SET remark=remark" +
            " ,contact_name = #{pm.contact_name}" +
            " ,contact_mobile = #{pm.contact_mobile}" +
            " ,contact_email = #{pm.contact_email}" +
            " ,wei_xin = #{pm.wei_xin}" +
            " ,job = #{pm.job}" +
            " where id = #{pm.id} ::int" +
            " </script>")
    void updateOrganizationContact(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("select * from ${schema_name}._sc_customer where status > -1000")
    List<Map<String, Object>> findAllCustomers(@Param("schema_name") String schema_name);

    @Insert(" INSERT INTO ${schema_name}._sc_customer_record(type_id,remark,create_user_id,facility_id)" +
            " VALUES (#{pm.type_id}::int,#{pm.remark},#{pm.create_user_id},#{pm.facility_id})")
    int insertCustomerRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer_record " +
            " SET id=id,update_time=current_timestamp " +
            " <when test=\"pm.type_id !=null and pm.type_id !=''\"> " +
            " ,type_id = #{pm.type_id} ::int" +
            " </when> " +
            " <when test=\"pm.remark !=null and pm.remark !=''\"> " +
            " ,remark = #{pm.remark}" +
            " </when> " +
            " <when test=\"pm.update_user_id !=null and pm.update_user_id !=''\"> " +
            " ,update_user_id = #{pm.update_user_id}" +
            " </when> " +
            " <when test=\"pm.status !=null and pm.status !=''\"> " +
            " ,status = #{pm.status}" +
            " </when> " +
            " where id =#{pm.id} ::int" +
            " </script>")
    void updateCustomerRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> param);

    @Select(" <script>" +
            " select tab.* from (" +
            " SELECT id as record_id,remark,status,facility_id,type_id, " +
            " case when update_user_id is null then create_user_id else update_user_id end as opt_user_id, " +
            " case when update_time is null then create_time else update_time end as opt_update_time " +
            " FROM ${schema_name}._sc_customer_record " +
            " WHERE facility_id = #{pm.id}::int and status > -1000) tab " +
            " left join ${schema_name}._sc_user us on us.id = tab.opt_user_id " +
            " left join ${schema_name}._sc_cloud_data cd on cd.code = tab.type_id ::varchar " +
            " <where> " +
            " cd.data_type='work_source_type_edit' " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( tab.remark ILIKE CONCAT('%', #{pm.keywordSearch}, '%') " +
            " or us.user_name ILIKE CONCAT('%', #{pm.keywordSearch}, '%') " +
            " or cd.name ILIKE CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " <when test='pm.begin_time != null and pm.end_time != null'> " +
            " AND tab.opt_update_time between #{pm.begin_time} ::timestamp and #{pm.end_time} ::timestamp" +
            " </when> " +
            " </where> " +
            " ORDER BY tab.opt_update_time desc" +
            " </script> ")
    List<Map<String, Object>> findCustomerRecordList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select(" select * from ${schema_name}._sc_customer_record where id = #{pm.record_id} ::int and status > -1000")
    Map<String, Object> findCustomerRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("<script>" +
            " select * from ${schema_name}._sc_asset_cus where cus_id=#{pm.cus_id}::int and asset_id=#{pm.asset_id} " +
            "</script>")
    Map<String, Object> findRelationAsset(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_asset_cus set status=status " +
            " <when test=\"pm.sewage_value !=null and pm.sewage_value !=''\">" +
            " ,sewage_value=#{pm.sewage_value}::numeric " +
            " </when>" +
            " <when test=\"pm.siren_value !=null and pm.siren_value !=''\">" +
            " ,siren_value=#{pm.siren_value}::numeric " +
            " </when>" +
            " <when test=\"pm.abnormal_value !=null and pm.abnormal_value !=''\">" +
            " ,abnormal_value=#{pm.abnormal_value}::numeric " +
            " </when>" +
            " where cus_id=#{pm.cus_id}::int and asset_id=#{pm.asset_id} " +
            "</script>")
    void updateRelationAsset(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);
}
