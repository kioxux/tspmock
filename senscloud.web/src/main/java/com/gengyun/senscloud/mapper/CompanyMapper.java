package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.entity.CompanyEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 系统公司表映射
 * User: 蒙立坤
 * Date: 2017/11/02
 * Time: 上午10:07
 */
@Mapper
public interface CompanyMapper {
    /**
     * 通过企业编号获得企业信息
     *
     * @param id 企业编号
     * @return 企业信息
     */
//    @Select("select address, company_name, is_use, mobile, id, email, " +
//            "system_name, company_logo_id, is_company_logo, " +
//            "client_customer_code, is_open_sms, is_open_email " +
//            "from public._sc_company where id = #{id}")
//    CompanyEntity findCompanyById(@Param("id") long id);

    /**
     * 根据登录信息获取企业信息
     *
     * @param user 登录信息
     * @return 企业信息
     */
//    @Select("select company_name, id " +
//            "from public._sc_company where id in ( " +
//            "select company_id from public.user_tenant_relation where account=#{user} or mobile=#{user}  " +
//            ") and is_use='1' ")
//    List<Map<String, Object>> findCompanyListByUser(@Param("user") String user);

//    /**
//     * 根据手机号获取企业信息（后台用）
//     *
//     * @param phone 手机号
//     * @return 企业信息
//     */
//    @Select("select company_name, id, is_open_sms " +
//            "from public._sc_company where id in ( " +
//            "select company_id from public.user_tenant_relation where mobile=#{phone}  " +
//            ") and is_use='1' ")
//    List<Map<String, Object>> findCompanyListByPhone(@Param("phone") String phone);

//    /**
//     * 根据手机号获取企业信息（返回客户端）
//     *
//     * @param phone 手机号
//     * @return 企业信息
//     */
//    @Select("select company_name, id " +
//            "from public._sc_company where id in ( " +
//            "select company_id from public.user_tenant_relation where mobile=#{phone}  " +
//            ") and is_use='1' and is_open_sms = true ")
//    List<Map<String, Object>> findCompanyListForPageByPhone(@Param("phone") String phone);

    /**
     * 根据登录用户信息查询企业数量
     *
     * @param account   账号
     * @param companyId 企业编号
     * @return 企业数量
     */
//    @Select("select count(1) " +
//            "from public._sc_company where id in ( " +
//            "select company_id from public.user_tenant_relation where account=#{account} " +
//            ") and is_use='1'and id=#{id}")
//    int countCompanyListForLogin(@Param("account") String account, @Param("id") long companyId);

    /**
     * 根据企业编号获取语言包
     *
     * @param id 企业编号
     * @return 语言包
     */
//    @Select("select resource from public.company_resource where company_id=#{id}")
//    String resourceById(@Param("id") Long id);

//    /**
//     * 获取全部有效企业列表
//     *
//     * @return 企业列表
//     */
//    @Select("select create_time, update_time, address, company_name, is_use, mobile, id, email, contact_man, " +
//            "company_account, system_name, company_logo_id, is_company_logo, register_file, bank_no, bank_name, " +
//            "tax_no, contract_file, client_customer_code, create_user_account, is_open_sms, is_open_email " +
//            "from public._sc_company where is_use='1'")
//    List<Map<String, Object>> findAll();

//    /**
//     * 将用户信息同步到公共表中
//     *
//     * @param pm 参数
//     */
//    @Insert("insert into public.user_tenant_relation (account,mobile,company_id,user_id) values " +
//            "(#{pm.account},#{pm.mobile},#{pm.company_id},#{pm.user_id})")
//    void insertUserTenantRelation(@Param("pm") Map<String, Object> pm);
}
