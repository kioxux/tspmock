package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator on 2018/4/16.
 */
@Mapper
public interface SpotMapper {
//    //获取需要点检数据列表（包含待处理）
//    @Select("select spot_code, spot_account, status, fault_number, facility_id, spot_time, area_code, begin_time from ${schema_name}._sc_spotcheck as r  order by r.spot_time limit ${page} offset ${begin} ")
//    List<SpotcheckData> getToDealSoptList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("user_account") String account, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //获取需要点检数据列表（包含待确认）
//    @Select("select r.spot_code, r.spot_account, r.status, r.fault_number, r.facility_id, r.spot_time, r.area_code, " +
//            "r.begin_time,u.username as spot_account_name," +
//            "s.status as status_name,ft.title as facility_name,pf.title as parent_name,a.area_name " +
//            " from ${schema_name}._sc_spotcheck r " +
//            " left join ${schema_name}._sc_inspection_area a on r.area_code=a.area_code  " +
//            " left join ${schema_name}._sc_user u on r.spot_account=u.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " where 1=1 ${condition} " +
//            " order by r.spot_code desc "+
//            "limit ${page} offset ${begin} ")
//    List<SpotcheckData> getToAuditSpotList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //获取需要点检数据列表（包含待确认）
//    @Select("select count(r.spot_code) " +
//            " from ${schema_name}._sc_spotcheck r " +
//            " left join ${schema_name}._sc_inspection_area a on r.area_code=a.area_code  " +
//            " left join ${schema_name}._sc_user u on r.spot_account=u.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " where 1=1 ${condition} ")
//    int getToAuditSpotListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查找单条点检单据
//    @Select("select r.*,c.username as spot_account_name," +
//            "s.status as status_name,ft.title as facility_name,a.area_name " +
//            "from ${schema_name}._sc_spotcheck r " +
//            " left join ${schema_name}._sc_user c on r.spot_account=c.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_inspection_area a on r.area_code=a.area_code  " +
//            "where r.spot_code=#{spot_code} ")
//    SpotcheckData getSpotInfo(@Param("schema_name") String schema_name, @Param("spot_code") String spotCode);
//
//
//    @Update("UPDATE ${schema_name}._sc_spotcheck  set status=900 where spot_code = '${spot_code}'")
//    int cancleSpotListCount(@Param("schema_name") String schema_name, @Param("spot_code") String spot_code);

}
