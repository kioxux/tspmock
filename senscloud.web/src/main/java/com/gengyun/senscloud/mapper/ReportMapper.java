package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReportMapper {

    @Select("select username,unfinished_count from " +
            "(select u.user_name,sum(case when task.status in (10,20,30,40,50,70,80) then 1 else 0 end) as unfinished_count " +
            "from " +
            "(select u.user_name,u.account,gp.group_name as title " +
            "from ${schema_name}._sc_user u   " +
            "left join ${schema_name}._sc_user_group ug on u.id=ug.user_id " +
            "left join ${schema_name}._sc_group gp on ug.group_id=gp.id " +
            "left join ${schema_name}._sc_group_org org on gp.id=org.group_id " +
            "left join ${schema_name}._sc_facilities f on f.id=org.org_id " +
            " group by u.user_name,u.account,gp.group_name order by gp.group_name" +
            ") u  " +
            "left join (" +
            "select wk.occur_time,wkd.receive_account,wk.status " +
            "from ${schema_name}._sc_works wk " +
            "left join ${schema_name}._sc_works_detail wkd on wk.work_code=wkd.work_code " +
            "left join ${schema_name}._sc_asset dv on wk.relation_id=dv._id " +
            "left join ${schema_name}._sc_asset_category cate on cate.id=dv.category_id " +
            "left join ${schema_name}._sc_asset_organization ar on ar.asset_id=dv._id " +
            "left join ${schema_name}._sc_facilities cus on ar.org_id=cus.id and cus.org_type=4 " +
            "where wk.status!=900 and wkd.is_main=1 and  wkd.work_type_id in (select wt.id from ${schema_name}._sc_work_type as wt where wt.business_type_id =1 ) " +
            "and wk.occur_time>='${begin_time}' and wk.occur_time<'${end_time}' " +
            ") task " +
            "on task.receive_account=u.account group BY u.user_name " +
            ") topfoo " +
            "where unfinished_count >0 order by unfinished_count desc LIMIT 3")
    List<Map<String, Object>> queryRepairTaskFinish(@Param("schema_name") String schema_name, @Param("begin_time") String begin_time,  @Param("end_time") String end_time);

    @Select("select username,round(cast((repairMinute/case total_count when 0 then 1 else total_count end ) as numeric),2) as repair_rate " +
            "from (" +
            "select u.username,count(u.account) as total_count,COALESCE(SUM(case when wkd.status=60 then (EXTRACT(EPOCH from wkd.finished_time)-EXTRACT(EPOCH from wkd.begin_time))/60 else 0 end),0) as repairMinute " +
            "from ${schema_name}._sc_works_detail as  wkd " +
            "left join ${schema_name}._sc_asset dv on wkd.relation_id=dv._id " +
            "left join ${schema_name}._sc_asset_category as cate on cate.id=dv.category_id " +
            "left join ${schema_name}._sc_work_type  wt on wkd.work_type_id=wt.id " +
            "left join  ${schema_name}._sc_works  wk on wkd.work_code=wk.work_code " +
            "left join ${schema_name}._sc_asset_organization ar on ar.asset_id=dv._id " +
            "left join ${schema_name}._sc_facilities cus on ar.org_id=cus.id and cus.org_type=4 " +
            "left join ${schema_name}._sc_facilities f on f.id=wk.facility_id " +
            "left join  ${schema_name}._sc_group_org  as grp on grp.org_id=f.id " +
            "left join  ${schema_name}._sc_group  as g on g.id=grp.group_id " +
            "left join ${schema_name}._sc_user u on wkd.receive_account=u.account " +
            "where wkd.status=60 and wt.business_type_id=1 and wk.occur_time>='${begin_time}' and wk.occur_time<'${end_time}' group by u.user_name " +
            ") topfoo where total_count >0 order by repair_rate desc LIMIT 3")
    List<Map<String, Object>> queryRepairAvgTimeTask(@Param("schema_name") String schema_name, @Param("begin_time") String begin_time,  @Param("end_time") String end_time);

    @Select("select user_name,total_count from (" +
            "select u.user_name, sum(case when task.status!=900 and task.status is not null then 1 else 0 end) as total_count " +
            "from (" +
            " select u.user_name,u.account,gp.group_name as title " +
            "from ${schema_name}._sc_user u " +
            "left join ${schema_name}._sc_user_group ug on u.id=ug.user_id " +
            "left join ${schema_name}._sc_group gp on ug.group_id=gp.id " +
            "left join ${schema_name}._sc_group_org org on gp.id=org.group_id " +
            "left join ${schema_name}._sc_facilities f on f.id=org.org_id " +
            " group by u.user_name,u.account,gp.group_name order by gp.group_name " +
            ") u  " +
            "left join (" +
            "select DISTINCT wk.occur_time,wkd.receive_account,wk.status,wk.create_user_account " +
            "from ${schema_name}._sc_works wk " +
            "left join ${schema_name}._sc_works_detail wkd on wk.work_code=wkd.work_code " +
            "left join ${schema_name}._sc_asset dv on wk.relation_id=dv._id " +
            "left join ${schema_name}._sc_asset_category cate on cate.id=dv.category_id " +
            "left join ${schema_name}._sc_asset_organization ar on ar.asset_id=dv._id " +
            "left join ${schema_name}._sc_facilities cus on ar.org_id=cus.id and cus.org_type=4 " +
            "where wk.status!=900 and wkd.is_main=1 and  wkd.work_type_id in (select wt.id from ${schema_name}._sc_work_type as wt where wt.business_type_id =1 ) " +
            " and wk.occur_time>='${begin_time}' and wk.occur_time<'${end_time}' " +
            ") task " +
            "on task.create_user_account=u.account group BY u.user_name " +
            ") topfoo where total_count >0 order by total_count desc LIMIT 3")
    List<Map<String, Object>> queryReportPerson(@Param("schema_name") String schema_name, @Param("begin_time") String begin_time,  @Param("end_time") String end_time);

    @Select("select user_name,delay_count " +
            "from (select u.user_name,sum(case when (task.finished_time is not null AND task.finished_time > NOW()) OR (task.finished_time is null AND task.deadline_time < NOW() ) then 1 else 0 end) as delay_count " +
            "from (" +
            " select u.user_name,u.account,gp.group_name as title " +
            "from ${schema_name}._sc_user u " +
            "left join ${schema_name}._sc_user_group ug on u.id=ug.user_id " +
            "left join ${schema_name}._sc_group gp on ug.group_id=gp.id " +
            "left join ${schema_name}._sc_group_org org on gp.id=org.group_id " +
            "left join ${schema_name}._sc_facilities f on f.id=org.org_id " +
            " group by u.user_name,u.account,gp.group_name order by gp.group_name " +
            ") u " +
            "left join (" +
            "select wk.occur_time,wkd.receive_account,wk.status,wk.deadline_time,wkd.finished_time " +
            "from ${schema_name}._sc_works wk " +
            "left join ${schema_name}._sc_works_detail wkd on wk.work_code=wkd.work_code " +
            "left join ${schema_name}._sc_asset dv on wk.relation_id=dv._id " +
            "left join ${schema_name}._sc_asset_category cate on cate.id=dv.category_id " +
            "left join ${schema_name}._sc_asset_organization ar on ar.asset_id=dv._id " +
            "left join ${schema_name}._sc_facilities cus on ar.org_id=cus.id and cus.org_type=4 " +
            "where wk.status!=900 and wkd.is_main=1 and wk.occur_time>='${begin_time}' and wk.occur_time<'${end_time}' " +
            ") task " +
            "on task.receive_account=u.account group BY u.user_name " +
            ") topfoo where delay_count >0 order by delay_count desc LIMIT 3")
    List<Map<String, Object>> queryTaskFinish(@Param("schema_name") String schema_name, @Param("begin_time") String begin_time,  @Param("end_time") String end_time);

    @Select("select date,times,round(cast(MTBF as numeric),2)  as duration from " +
            "(SELECT to_char(date_trunc('month',wr_run.occur_time),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') as date,count(distinct r_run.work_code ) as times," +
            "COALESCE(SUM(case when r_run.status=60 then (EXTRACT(EPOCH from r_run.finished_time)-EXTRACT(EPOCH from wr_run .occur_time))/(60*60) else 0 end),0) as MTBF\n" +
            "from ${schema_name}._sc_works_detail r_run " +
            "left join  ${schema_name}._sc_work_type as  wt on r_run.work_type_id=wt.id " +
            "left join  ${schema_name}._sc_works as  wr_run on r_run.work_code=wr_run.work_code " +
            "left join  ${schema_name}._sc_asset as  dv on r_run.relation_id=dv._id " +
            "LEFT JOIN ${schema_name}._sc_asset_category ma ON dv.category_id = ma. ID " +
            "LEFT JOIN ${schema_name}._sc_works_detail_asset_org wdao ON r_run.sub_work_code = wdao.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_facilities wdaof ON wdaof. ID = wdao.facility_id " +
            "where wt.business_type_id=1 and  r_run.status=60  and r_run.is_main=1 " +
            "and wr_run.occur_time>='${begin_time}' and wr_run.occur_time<='${end_time}' " +
            "group by to_char(date_trunc('month',wr_run.occur_time),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') " +
            ") as foo order by date asc")
    List<Map<String, Object>> queryFaultAnalysis(@Param("schema_name") String schema_name, @Param("begin_time") String begin_time,  @Param("end_time") String end_time);

    @Select("SELECT to_char(date_trunc('month',r_run.create_time),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') as date,COUNT(DISTINCT wd.sub_work_code) AS totalPlan,SUM(CASE r_run.status WHEN 60 THEN 1 ELSE 0 END) AS finishedPlan " +
            "FROM ${schema_name}._sc_works r_run " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd ON r_run.work_code = wd.work_code " +
            "LEFT JOIN ${schema_name}._sc_works_detail_receive_group wdrg ON wdrg.sub_work_code = wd.sub_work_code " +
            "INNER JOIN ${schema_name}._sc_user u ON wdrg.receive_account = u.account " +
            "LEFT JOIN ${schema_name}._sc_group g ON g.id = wdrg.group_id " +
            "WHERE r_run.status < 900 and r_run.plan_code is not null and r_run.create_time>='${begin_time}' and r_run.create_time<'${end_time}' " +
            "GROUP BY to_char(date_trunc('month',r_run.create_time),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') ORDER BY date DESC")
    List<Map<String, Object>> queryPMCForMonth(@Param("schema_name") String schema_name, @Param("begin_time") String begin_time,  @Param("end_time") String end_time);

    @Select("select to_char(daytime,'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') as report_name," +
            "to_char(daytime,'" + SqlConstant.SQL_DATE_FMT_YEAR + "') as year,to_char(daytime,'MM') as month," +
            "to_char(now(), '" + SqlConstant.SQL_DATE_TIME_FMT + "') as generation_time " +
            "from generate_series('${begin_time}'::date, '${end_time}'::date, '1 month') s(daytime) " +
            "order by report_name desc " +
            "${pagination}")
    List<Map<String, Object>> getReportDetailList(@Param("pagination") String pagination, @Param("begin_time") String begin_time, @Param("end_time") String end_time);


    @Select("select count(to_char(daytime,'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')) " +
            "from generate_series('${begin_time}'::date, '${end_time}'::date, '1 month') s(daytime) ")
    int getReportDetailCount(@Param("begin_time") String begin_time, @Param("end_time") String end_time);
}
