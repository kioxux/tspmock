package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 */
@Mapper
public interface CloudDataMapper {
    /**
     * 新增数据项类型表
     */
    @Insert(" INSERT INTO ${schema_name}._sc_cloud_data_type(data_type, data_type_name, is_lang, create_user_id, create_time, data_order, remark )" +
            " VALUES (#{pm.data_type}, #{pm.data_type_name}, #{pm.is_lang}::int, #{pm.create_user_id},now(), #{pm.data_order}::int, #{pm.remark}) ")
    void insertCloudDataType(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 查询数据项类型表
     */
    @Select(" <script>" +
            " SELECT u.user_name AS  create_user_name,cdt.id,cdt.data_type, cdt.data_type_name, cdt.is_lang::varchar, cdt.create_user_id, " +
            " to_char(cdt.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') AS  create_time, cdt.data_order, cdt.remark  " +
            " FROM ${schema_name}._sc_cloud_data_type cdt" +
            " LEFT JOIN ${schema_name}._sc_user u ON u.id = cdt.create_user_id " +
            " WHERE 1=1  " +
            " <when test='pm.searchWord!=null'>" +
            " ${pm.searchWord} "+
            " </when>" +
            " ORDER BY  data_order " +
            " </script>")
    List<Map<String, Object>> findCloudDataTypeList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 查询数据项类型详情
     */
    @Select(" <script>" +
            " SELECT data_type, data_type_name, is_lang::varchar, create_user_id, create_time, data_order, remark  FROM ${schema_name}._sc_cloud_data_type" +
            " WHERE id=#{id}  " +
            " </script>")
    Map<String, Object> findCloudDataTypeInfo(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 更新数据项类型表
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_cloud_data_type" +
            " SET data_type = #{pm.data_type}" +
            " <when test='pm.data_type_name!=null'>" +
            " , data_type_name= #{pm.data_type_name}" +
            " </when> " +
            " <when test='pm.is_lang!=null'>" +
            " , is_lang= #{pm.is_lang}::int" +
            " </when> " +
            " <when test='pm.data_order!=null'>" +
            " , data_order= #{pm.data_order}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " , remark= #{pm.remark}" +
            " </when> " +
            " <when test='pm.remark==null'>" +
            " , remark= ''" +
            " </when> " +
            " WHERE id = #{pm.id}::int " +
            " </script>")
    void updateCloudDataType(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 删除数据项类型表
     */
    @Delete(" DELETE FROM ${schema_name}._sc_cloud_data_type WHERE id = #{id} ")
    void deleteCloudDataType(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 根据数据项类型获取数据项列表
     */
    @Select(" SELECT cd.id, cd.code, cd.name, cd.data_type, cd.is_lang::varchar, cd.create_user_id, " +
            " to_char(cd.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') AS  create_time, cd.data_order, cd.remark, " +
            " cd.type_remark, cd.parent_code, cd.reserve1,u.user_name AS create_user_name " +
            " FROM ${schema_name}._sc_cloud_data cd  " +
            " LEFT JOIN ${schema_name}._sc_user u ON cd.create_user_id = u.id" +
            " WHERE data_type = #{pm.data_type} ORDER BY data_order ")
    List<Map<String, Object>> findCloudDataListByDataType(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 新增数据项表
     */
    @Insert(" INSERT INTO ${schema_name}._sc_cloud_data" +
            " (code, name, data_type, is_lang, create_user_id, create_time, data_order, remark, type_remark, parent_code, reserve1) " +
            " VALUES (#{pm.code}, #{pm.name}, #{pm.data_type}, #{pm.is_lang}::int, #{pm.create_user_id}, now(), #{pm.data_order}::int, " +
            " #{pm.remark}, #{pm.type_remark}, #{pm.parent_code}, #{pm.reserve1})")
    void insertCloudData(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 更新数据项表
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_cloud_data" +
            " SET id = #{pm.id}::int" +
            " <when test='pm.code!=null'>" +
            " , code= #{pm.code}" +
            " </when> " +
            " <when test='pm.is_lang!=null'>" +
            " , is_lang= #{pm.is_lang}::int" +
            " </when> " +
            " <when test='pm.data_order!=null'>" +
            " , data_order= #{pm.data_order}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " , remark= #{pm.remark}" +
            " </when> " +
            " <when test='pm.reserve1!=null'>" +
            " , reserve1= #{pm.reserve1}" +
            " </when> " +
            " <when test='pm.parent_code!=null'>" +
            " , parent_code= #{pm.parent_code}" +
            " </when> " +
            " <when test='pm.type_remark!=null'>" +
            " , type_remark= #{pm.type_remark}" +
            " </when> " +
            " <when test='pm.data_type!=null'>" +
            " , data_type= #{pm.data_type}" +
            " </when> " +
            " <when test='pm.name!=null'>" +
            " , name= #{pm.name}" +
            " </when> " +
            " WHERE id = #{pm.id}::int " +
            " </script>")
    void updateCloudData(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 删除数据项表
     */
    @Delete(" DELETE FROM ${schema_name}._sc_cloud_data WHERE id = #{id}  ")
    void deleteCloudData(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 查询数据项表详情
     */
    @Select(" SELECT id, code, name, data_type, is_lang::varchar, create_user_id, create_time, data_order, remark, type_remark, parent_code, reserve1   " +
            " FROM ${schema_name}._sc_cloud_data WHERE id=#{id}  ")
    Map<String, Object> findCloudDataInfo(@Param("schema_name") String schemaName, @Param("id") Integer id);

}
