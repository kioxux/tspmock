package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetGuaranteeMapper {
//    @Select("select gu.*,f.short_title as company_name from ${schema_name}._sc_asset_guarantee gu " +
//            "left join ${schema_name}._sc_facilities f on gu.company_id=f.id " +
//            "where gu.asset_id='${asset_id}' and gu.isuse=true order by gu.id ")
//    List<AssetGuaranteeData> findAllAssetGuaranteeList(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    /**
//     * 新增资产保修信息
//     *
//     * @param schema_name
//     * @param asset_id
//     * @param buy_date
//     * @param expired_date
//     * @param serial_no
//     * @param create_user_account
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_asset_guarantee (asset_id,buy_date,expired_date,serial_no,isuse,create_time,create_user_account,contract_type,company_id) " +
//            "values(#{asset_id},#{buy_date},#{expired_date},#{serial_no},TRUE,current_timestamp,#{create_user_account},#{contract_type},#{company_id})")
//    int AddAssetGuarantee(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("buy_date") Date buy_date, @Param("expired_date") Date expired_date, @Param("serial_no") String serial_no, @Param("contract_type") Integer contract_type, @Param("company_id") Integer company_id, @Param("create_user_account") String create_user_account);
//
//    //修改资产保修信息
//    @Update("update ${schema_name}._sc_asset_guarantee set buy_date=#{buy_date}, expired_date=#{expired_date}, " +
//            "serial_no=#{serial_no},contract_type=#{contract_type},isuse=TRUE,company_id=#{company_id} " +
//            "where id=${id}")
//    int UpdateAssetGuarantee(@Param("schema_name") String schema_name, @Param("buy_date") Date buy_date, @Param("expired_date") Date expired_date, @Param("serial_no") String serial_no, @Param("contract_type") Integer contract_type, @Param("company_id") Integer company_id, @Param("id") Integer id);
//
//    //查找资产保修，按id查找，获取单个保修数据
//    @Select("select * from ${schema_name}._sc_asset_guarantee where id=${id}")
//    AssetGuaranteeData findAssetGuarantee(@Param("schema_name") String schema_name, @Param("id") Integer id);
//
//    //删除资产保修信息
//    @Update("update ${schema_name}._sc_asset_guarantee set isuse=#{isuse} where id=#{id} ")
//    int DeleteAssetGuarantee(@Param("schema_name") String schema_name, @Param("id") Integer id, @Param("isuse") Boolean isuse);
//
//
//    /**
//     * 获取资产保修信息
//     *
//     * @param schema_name
//     * @param asset_id
//     * @param contractType
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_asset_guarantee where asset_id=#{asset_id} and contract_type=#{contractType}::int and isuse=true order by id ")
//    List<Map<String, Object>> findAssetPeriodInfo(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("contractType") String contractType);
//
//    /**
//     * 自动同步设备的供应商和制造商到设备关联组织中去，韵达定制
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("insert into ${schema_name}._sc_asset_organization(asset_id,org_id) " +
//            "select ast._id,ast.supplier from ${schema_name}._sc_asset ast " +
//            "left join ${schema_name}._sc_asset_organization ag on ast._id=ag.asset_id and ast.supplier=ag.org_id " +
//            "where ast.supplier is not null and ast.supplier >0 and (ag.org_id<0 or ag.org_id is null) ")
//    int autoUpdateSupplyInfoToDeviceOrganization(@Param("schema_name") String schema_name);


}
