package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 科华公告
 */
@Mapper
public interface NoticeMapper {
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = NoticeMapper.NoticeMapperProvider.class, method = "queryForApp")
//    List<Map<String, Object>> queryForApp(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                          @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                          @Param("searchKey") String searchKey);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = NoticeMapper.NoticeMapperProvider.class, method = "countByConditionForApp")
//    int countByConditionForApp(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey);
//
//
//    /**
//     * 根据ID查询公告详情
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_notice where id=#{id} ")
//    Map<String, Object> findById(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = NoticeMapperProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                    @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                    @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = NoticeMapperProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
//
//    /**
//     * 新增数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @InsertProvider(type = NoticeMapperProvider.class, method = "insert")
//    int insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//
//    /**
//     * 根据主键查询工作安排
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_notice w where id = #{id} ")
//    Map<String, Object> queryNoticeById(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_notice set " +
//            "notice_title=#{s.notice_title}, " +
//            "notice_content=#{s.notice_content}, " +
//            "begin_date=#{s.begin_date}::timestamp, " +
//            "end_date=#{s.end_date}::timestamp, " +
//            "important_level=#{s.important_level}::int, " +
//            "is_use=#{s.is_use}::bool " +
//            "where id=#{s.id}::int ")
//    int update(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 切换启用
//     *
//     * @param schemaName
//     * @return
//     */
//    @Update("update ${schema_name}._sc_notice set " +
//            "is_use=false::bool ")
//    int updateUse(@Param("schema_name") String schemaName);
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Update("DELETE FROM ${schema_name}._sc_notice where id=#{id} ")
//    int delete(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//
//    /**
//     * 实现类
//     */
//    class NoticeMapperProvider {
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param searchKey
//         * @return
//         */
//        public String queryForApp(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                  @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                  @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT(" a.*,c.username ");
//                FROM(schemaName + "._sc_notice a " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("upper(a.notice_title) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey
//                            + "%') or upper(a.notice_content) like upper('%" + searchKey + "%') ");
//                }
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String countByConditionForApp(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_notice a " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("upper(a.notice_title) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey
//                            + "%') or upper(a.notice_content) like upper('%" + searchKey + "%') ");
//                }
//            }}.toString();
//        }
//
//
//        /**
//         * 新增数据
//         *
//         * @param schemaName
//         * @param paramMap
//         * @return
//         */
//        public String insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_notice");
//                VALUES("notice_title ", "#{s.notice_title}");
//                VALUES("notice_content ", "#{s.notice_content}");
//                VALUES("begin_date ", "#{s.begin_date}::timestamp");
//                VALUES("end_date ", "#{s.end_date}::timestamp");
//                VALUES("important_level ", "#{s.important_level}::int");
//                VALUES("is_use ", "#{s.is_use}::bool");
//                VALUES("create_user_account ", "#{s.create_user_account}");
//                VALUES("create_time ", "current_timestamp");
//            }}.toString();
//        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param searchKey
//         * @return
//         */
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime) {
//            return new SQL() {{
//                String condition = "1=1 ";
//                SELECT(" a.*,c.username");
//                FROM(schemaName + "._sc_notice a " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition +=
//                            "and upper(a.notice_title) like upper('%" + searchKey
//                                    + "%') or upper(c.username) like upper('%" + searchKey
//                                    + "%') or upper(a.notice_content) like upper('%" + searchKey + "%') ";
//                }
//                if (null != beginTime && !"".equals(beginTime) && null != endTime && !"".equals(endTime)) {
//                    condition += " and a.begin_date>='" + Timestamp.valueOf(beginTime + " 00:00:00") + "' and a.begin_date<'" + Timestamp.valueOf(endTime + " 00:00:00") + "'";
//                }
//                WHERE(condition);
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime) {
//            return new SQL() {{
//                String condition = "1=1 ";
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_notice a " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition +=
//                            "and upper(a.notice_title) like upper('%" + searchKey
//                                    + "%') or upper(c.username) like upper('%" + searchKey
//                                    + "%') or upper(a.notice_content) like upper('%" + searchKey + "%') ";
//                }
//                if (null != beginTime && !"".equals(beginTime) && null != endTime && !"".equals(endTime)) {
//                    condition += " and a.begin_date>='" + Timestamp.valueOf(beginTime + " 00:00:00") + "' and a.begin_date<'" + Timestamp.valueOf(endTime + " 00:00:00") + "'";
//                }
//                WHERE(condition);
//            }}.toString();
//        }
//    }
}
