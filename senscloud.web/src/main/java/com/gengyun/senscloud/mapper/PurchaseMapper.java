package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PurchaseMapper {
//     /*
//    * 备件需求
//     */
//     //查找保养单列表(按，条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//     @Select("select r.*,u.username as create_user_name,c.username as appname," +
//             "ft.title as needfacilityname " +
//             "from ${schema_name}._sc_buys r " +
//             " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//             " left join ${schema_name}._sc_user c on r.appaccount=c.account " +
//             " left join ${schema_name}._sc_facilities ft on r.needfacilityid=ft.id " +
//             "where 1=1 and r.isuse=true ${condition} " +
//             " group by r.id, u.username, c.username,ft.title " +
//             " order by r.createtime desc limit ${page} offset ${begin}")
//     List<PurchaseData> getPurchaseList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //查找保养单列表的总数(按条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    @Select("select count(r.id) " +
//            "from ${schema_name}._sc_buys r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.appaccount=c.account " +
//            " left join ${schema_name}._sc_facilities ft on r.needfacilityid=ft.id " +
//            "where 1=1 and r.isuse=true ${condition} " )
//    int getPurchaseListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //获取备件列表
//    @Select("select r.* from ${schema_name}._sc_bom r " +
//            " where 1=1 ${condition}")
//    List<BomData> findAllForPage(@Param("schema_name") String schema_name,@Param("condition") String condition);
//
//    //根据id备件列表
//    @Select("select r.* from ${schema_name}._sc_bom r " +
//            " where r.id=${id}")
//    BomData findBomDataById(@Param("schema_name") String schema_name,@Param("id") int id);
//
//    /**
//     * 插入数据
//     */
//    @Insert("insert into ${schema_name}._sc_buys ( " +
//            "buytypeid, assetcode, assetname, assetnote, appaccount, needfacilityid, needcount, needdate, expireddate, remark, isuse, createtime, create_user_account)  values  " +
//            " (#{buytypeid},#{assetcode},#{assetname},#{assetnote},#{appaccount},#{needfacilityid},#{needcount},#{needdate},#{expireddate},#{remark},#{isuse},#{createtime},#{create_user_account})")
//    int InsertPurchaseData(PurchaseData purchaseData);
//
//
//    /**
//     * 更新数据为删除状态
//     */
//    @Update("update ${schema_name}._sc_buys  " +
//            "set isuse=false,create_user_account=#{create_user_account},createtime=#{createtime}  " +
//            " where id=#{id}")
//    int UpdatePurchaseData(@Param("schema_name") String schema_name,@Param("id") int id,@Param("create_user_account") String create_user_account,@Param("createtime") Timestamp createtime);
}
