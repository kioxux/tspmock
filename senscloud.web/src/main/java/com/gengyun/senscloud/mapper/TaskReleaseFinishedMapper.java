package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 5:54 2019/9/17 0017
 */
@Mapper
public interface TaskReleaseFinishedMapper {

//    @Insert("insert into ${schema_name}._sc_work_task (task_title,task_content, begin_date, end_date, task_rate, important_level, deliverables," +
//            "deliverable_day, deliverable_time, audit_man, is_use, create_user_account, create_time, task_require) values (" +
//            "#{task_title},#{task_content},#{begin_date}::timestamp,#{end_date}::timestamp,#{task_rate}::int,#{important_level}::int," +
//            "#{deliverables},#{deliverable_day},#{deliverable_time}::time," +
//            "#{audit_man},#{is_use}::bool,#{create_user_account},#{create_time}::timestamp,#{task_require})")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int doSubmit(Map<String, Object> paramMap);
//
//    /**
//     * @param schemaName
//     * @return
//     * @Description：查询所有任务当前时间大于开始时间小于结束时间任务模板
//     */
//    @SelectProvider(type = TaskReleaseProvider.class, method = "selectAllRelease")
//    List<Map<String, Object>> selectAllRelease(@Param("schema_name") String schemaName);
//
//    @Select("SELECT DISTINCT " +
//            "u.account as account,u.username as username FROM " +
//            "${schema_name}._sc_work_task_receiver AS tr " +
//            "LEFT JOIN ${schema_name}._sc_user_group AS ug ON ug.group_id = tr.group_id " +
//            "LEFT JOIN ${schema_name}._sc_user_role AS r ON r.role_id = tr.role_id " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = r.user_id " +
//            "AND u.ID = ug.user_id " +
//            "WHERE " +
//            "u.is_out = FALSE " +
//            "AND u.status = 1 " +
//            "AND ug.user_id = r.user_id " +
//            "AND task_id = ${taskId}")
//    List<Map<String, Object>> searchReceiveAccountInfo(@Param("schema_name") String schemaName, @Param("taskId") int taskId);
//
//    @SelectProvider(type = TaskReleaseProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                    @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                    @Param("searchKey") String searchKey, @Param("groups") String groups, @Param("idPrefix") String idPrefix);
//
//    /**
//     * @param schemaName
//     * @param searchKey
//     * @param groups
//     * @return
//     * @Description:任务发布列表工单数据量统计
//     */
//    @SelectProvider(type = TaskReleaseProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("groups") String groups);
//
//    /**
//     * @param schemaName
//     * @param paramMap
//     * @Description: 任务接受
//     */
//    @InsertProvider(type = TaskReleaseProvider.class, method = "insertReceiver")
//    void doSubmitReceiver(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schemaName
//     * @param paramMap
//     * @Description:生成完成工单
//     */
//    @InsertProvider(type = TaskReleaseProvider.class, method = "addFinished")
//    int addFinished(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @return
//     * @Description:任务提交，执行流程
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "updateSubmit")
//    int updateSubmit(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @return
//     * @Description:任务评价
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "updateEvaluate")
//    int updateEvaluate(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @return
//     * @Description:任务评价结束流程
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "updateFinished")
//    int updateFinished(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param id
//     * @param is_use
//     * @return
//     * @Description: 禁用启用
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "update")
//    int doSubmitTaskRelease(@Param("schema_name") String schema_name, @Param("id") String id, @Param("is_use") boolean is_use);
//
//    @SelectProvider(type = TaskReleaseProvider.class, method = "queryTaskReleaseById")
//    Map<String, Object> queryTaskReleaseById(@Param("schema_name") String schemaName, @Param("code") Integer code);
//
//    @SelectProvider(type = TaskReleaseProvider.class, method = "findSingleWtInfo")
//    Map<String, Object> findSingleWtInfo(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code);
//
//    @SelectProvider(type = TaskReleaseProvider.class, method = "getUnfinishedTask")
//    int getUnfinishedTask(@Param("schema_name") String schemaName, @Param("account") String account);
//
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @param id
//     * @return
//     * @Description: 更新表单操作
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "doUpdate")
//    int doUpdate(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap, @Param("id") String id);
//
//    @DeleteProvider(type = TaskReleaseProvider.class, method = "deleteReceive")
//    int deleteReceive(@Param("schema_name") String schema_name, @Param("id") String id);
//
//    /**
//     * 根据任务编号获取用户组角色信息
//     *
//     * @param schemaName
//     * @param taskId
//     * @return
//     */
//    @Select("select t.group_id, t.role_id, g.group_name, r.name as role_name " +
//            "from ${schema_name}._sc_work_task_receiver t " +
//            "LEFT JOIN ${schema_name}._sc_group g on t.group_id = g.id " +
//            "LEFT JOIN ${schema_name}._sc_role r on t.role_id = r.id and r.available = true " +
//            "where t.task_id = #{taskId} " +
//            "ORDER BY t.group_id, t.role_id ")
//    List<Map<String, Object>> getGroupRoleListByTask(@Param("schema_name") String schemaName, @Param("taskId") Integer taskId);
//
//
//    class TaskReleaseProvider {
//
//        public String insertReceiver(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_work_task_receiver");
//                VALUES("task_id ", "#{s.task_id}::int");
//                VALUES("group_id ", "#{s.group_id}::int");
//                VALUES("role_id ", "#{s.role_id}");
//            }}.toString();
//        }
//
//        public String addFinished(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_work_task_finished");
//                VALUES("sub_work_code ", "#{s.sub_work_code}");
//                VALUES("task_id ", "#{s.task_id}::int");
//                VALUES("deliverables ", "#{s.deliverables}");
//                VALUES("create_user_account ", "#{s.create_user_account}");
//                VALUES("create_time ", "#{s.create_time}::timestamp");
//                VALUES("deadline_time ", "#{s.deadline_time}::timestamp");
//                VALUES("duty_man ", "#{s.duty_man}");
//            }}.toString();
//        }
//
//        public String update(@Param("schema_name") String schema_name, @Param("id") String id, @Param("is_use") boolean is_use) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task");
//                SET("is_use = #{is_use}::bool");
//                if (id != null) {
//                    WHERE("id = #{id}::int");
//                }
//            }}.toString();
//        }
//
//        public String updateSubmit(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task_finished");
//                SET("deliverables_files = #{s.deliverables_files}");
//                SET("finished_content = #{s.finished_content}");
//                SET("finished_time = #{s.finished_time}::timestamp");
//                SET("is_finished = #{s.is_finished}::int");
//                WHERE("sub_work_code = #{s.sub_work_code}");
//            }}.toString();
//        }
//
//        public String updateEvaluate(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task_finished");
//                SET("audit_time = #{s.audit_time}::timestamp");
//                SET("is_finished = #{s.is_finished}::int");
//                WHERE("sub_work_code = #{s.sub_work_code}");
//            }}.toString();
//        }
//
//        public String updateFinished(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task_finished");
//                SET("body_property = #{s.body_property}::jsonb");
//                WHERE("id = #{s.id}::int");
//            }}.toString();
//        }
//
//        public String deleteReceive(@Param("schema_name") String schema_name, @Param("id") String id) {
//            return new SQL() {{
//                DELETE_FROM(schema_name + "._sc_work_task_receiver");
//                WHERE("task_id = #{id}::int");
//            }}.toString();
//        }
//
//        public String queryTaskReleaseById(@Param("schema_name") String schemaName, @Param("code") Integer code) {
//            return new SQL() {{
//                SELECT("a.*");
//                FROM(schemaName + "._sc_work_task a ");
//                WHERE("id = #{code}::int");
//            }}.toString();
//        }
//
//        public String findSingleWtInfo(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code) {
//            return new SQL() {{
//                SELECT("tf.important_level,tf.task_rate,tf.task_title,tf.duty_man,c.username AS duty_man_name,tf.audit_man," +
//                        "tf.sub_work_code,tf.is_finished,tf.create_time,tf.create_time as begin_date,tf.deadline_time as end_date,tf.deliverable_day, " +
//                        "tf.deliverables,tf.deliverable_time,tf.task_content,wt.template_files,tf.deliverables_files,tf.finished_content,tf.sub_work_code," +
//                        "tf.task_require,tf.duty_man,tf.deliverables_files,tf.finished_content ");
//                FROM(schemaName + "._sc_work_task_finished as tf " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=tf.duty_man "+
//                        "LEFT JOIN " + schemaName + "._sc_work_task wt ON wt.id=tf.task_id ");
//                WHERE("tf.sub_work_code = #{sub_work_code}");
//            }}.toString();
//        }
//
//        public String getUnfinishedTask(@Param("schema_name") String schemaName, @Param("account") String account) {
//            return new SQL() {{
//                SELECT("count(1)");
//                FROM(schemaName + "._sc_work_task_finished as tf  ");
//                WHERE("((finished_time is not null and finished_time > deadline_time) or (finished_time is null)) and duty_man= #{account} ");
//            }}.toString();
//        }
//
//        public String selectAllRelease(@Param("schema_name") String schemaName) {
//            return new SQL() {{
//                SELECT("t.id,t.task_title,t.task_content,t.begin_date，t.end_date,t.task_rate,t.important_level,t.deliverables,t.deliverable_day,t.deliverable_time,t.create_user_account,array_to_string(array_agg(r.role_id),',') as role_ids ");
//                FROM(schemaName + "._sc_work_task as t " +
//                        "left join " + schemaName + "._sc_work_task_receiver as r on r.task_id=t.id");
//                WHERE("t.is_use = true and now()>= t.begin_date and now()<t.end_date");
//                GROUP_BY("t.id,t.task_title,t.task_content,t.begin_date,t.end_date,t.task_rate,t.important_level,t.deliverables,t.deliverable_day,t.deliverable_time,t.create_user_account");
//            }}.toString();
//        }
//
//        public String searchReceiveAccount(@Param("schema_name") String schemaName, @Param("taskId") int taskId) {
//            return new SQL() {{
//                SELECT("DISTINCT u.account, u.* ");
//                FROM(schemaName + "._sc_work_task_receiver as t " +
//                        "left join " + schemaName + "._sc_user_group as ug on ug.group_id=t.group_id " +
//                        "left join " + schemaName + "._sc_user_role as r on r.role_id = t.role_id " +
//                        "left join " + schemaName + "._sc_user as u on u.id= r.user_id and u.id = ug.user_id ");
//                WHERE(" u.is_out=false and status=1 and ug.user_id=r.user_id and task_id = #{taskId}::int");
//            }}.toString();
//        }
//
//        public String doUpdate(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap, @Param("id") String id) {
//            return new SQL() {{
//                UPDATE(schemaName + "._sc_work_task");
//                SET("task_title = #{s.task_title}");
//                SET("task_content = #{s.task_content}");
//                SET("begin_date = #{s.begin_date}::timestamp");
//                SET("end_date = #{s.end_date}::timestamp");
//                SET("task_rate = #{s.task_rate}::int");
//                SET("important_level = #{s.important_level}::int");
//                SET("deliverables = #{s.deliverables}");
//                SET("deliverable_day = #{s.deliverable_day}::bool");
//                SET("deliverable_time = #{s.deliverable_time}::time");
//                SET("audit_man = #{s.audit_man}");
//                SET("is_use = #{s.is_use}::bool");
//                SET("task_require = #{s.task_require}");
//                if (id != null) {
//                    WHERE("id = #{id}::int");
//                }
//            }}.toString();
//        }
//
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("searchKey") String searchKey,
//                            @Param("groups") String groups, @Param("idPrefix") String idPrefix) {
//            return new SQL() {{
//                String condition = "1=1 ";
//                SELECT("tf.important_level,tf.task_rate,tf.task_title,c.username AS duty_man_name,tf.sub_work_code,tf.is_finished,tf.create_time,tf.finished_time,array_to_string(array_agg(DISTINCT g.group_name),',') as group_name");
//                FROM(schemaName + "._sc_work_task_finished as tf " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=tf.duty_man " +
//                        "Left join " + schemaName + "._sc_user_group as ug on ug.user_id = c.id  " +
//                        "left join " + schemaName + "._sc_group as g on g.id = ug.group_id ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition += "and upper(tf.task_title) like upper('%" + searchKey
//                            + "%') or upper(tf.task_content) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey + "%') ";
//                }
//
//                if (null != groups && !"".equals(groups) && !"0".equals(groups)) {
//                    condition += " and ug.group_id=" + groups;
//                }
//                WHERE(condition);
//                GROUP_BY("tf.important_level,tf.task_rate,tf.task_title,C.username,tf.sub_work_code,tf.is_finished,tf.create_time,tf.finished_time");
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey,
//                                       @Param("groups") String groups) {
//            return new SQL() {{
//                String condition = "1=1 ";
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_work_task_finished as tf " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=tf.duty_man ");
//                // 查询条件
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition += "and ( " +
//                            "upper(tf.task_title) like upper('%" + searchKey + "%') " +
//                            "or upper(tf.task_content) like upper('%" + searchKey + "%') " +
//                            "or upper(c.username) like upper('%" + searchKey + "%') " +
//                            ") ";
//                }
////                if (null != beginTime && !"".equals(beginTime)) {
////                    condition += " and wt.begin_date>='" + Timestamp.valueOf(beginTime + " 00:00:00") + "' ";
////                }
////                if(null != deliverable_type && !"".equals(deliverable_type)&&!"0".equals(deliverable_type)){
////                    condition+=" and a.deliverable_type="+deliverable_type;
////                }
//                WHERE(condition);
//            }}.toString();
//        }
//    }
}
