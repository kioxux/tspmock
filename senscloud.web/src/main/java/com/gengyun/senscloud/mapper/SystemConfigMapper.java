package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.model.SystemConfigData;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * 系统配置信息处理
 */
public interface SystemConfigMapper {
    /**
     * 根据系统配置名获取配置值
     *
     * @param schema_name 数据库
     * @param config_name 系统配置名
     * @return 配置值
     */
    @Select("select setting_value from ${schema_name}._sc_system_config where config_name=#{config_name} limit 1")
    String findSysCfgValueByCfgName(@Param("schema_name") String schema_name, @Param("config_name") String config_name);

    /**
     * 查询基础信息系统配置列表
     */
    @Select("SELECT sc.config_name,sc.remark,sc.setting_value,sc.config_title,sc.group_title,sc.source_data,sc.control_type," +
            "sc.data_order FROM ${schema_name}._sc_system_config sc WHERE sc.group_title = #{group_title}  ORDER BY sc.data_order ")
    List<Map<String, Object>> findSysConfigList(@Param("schema_name") String schema_name, @Param("group_title") String group_title);

    /**
     * 查询地图类型系统配置
     */
    @Select("SELECT sc.config_name,sc.remark,sc.setting_value,sc.config_title,sc.group_title,sc.source_data,sc.control_type," +
            "sc.data_order FROM ${schema_name}._sc_system_config sc WHERE sc.config_name = 'map_type' ")
    Map<String, Object> findMapTypeSysConfig(@Param("schema_name") String schema_name);

    /**
     * 查询顶层地图类型系统配置
     */
    @Select("SELECT sc.config_name,sc.remark,sc.setting_value,sc.config_title,sc.group_title,sc.source_data,sc.control_type," +
            "sc.data_order FROM ${schema_name}._sc_system_config sc WHERE sc.config_name = 'top_map_type' ")
    Map<String, Object> findTopMapTypeSysConfig(@Param("schema_name") String schema_name);

    /**
     * 更新系统配置
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_system_config SET config_name = #{pm.config_name}" +
            " <when test='pm.remark!=null'> " +
            " ,remark=#{pm.remark}" +
            " </when> " +
            " <when test='pm.setting_value!=null'> " +
            " ,setting_value=#{pm.setting_value}" +
            " </when> " +
            " <when test='pm.config_title!=null'> " +
            " ,config_title=#{pm.config_title}" +
            " </when> " +
            " <when test='pm.group_title!=null'> " +
            " ,group_title=#{pm.group_title}" +
            " </when> " +
            " <when test='pm.source_data!=null'> " +
            " ,source_data=#{pm.source_data}" +
            " </when> " +
            " <when test='pm.control_type!=null'> " +
            " ,control_type=#{pm.control_type}::int" +
            " </when> " +
            " <when test='pm.data_order!=null'> " +
            " ,data_order=#{pm.data_order}::int" +
            " </when>" +
            " WHERE config_name = #{pm.config_name}  " +
            " </script>")
    void updateSysConfig(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询系统配置想详情
     */
    @Select("SELECT sc.config_name,sc.remark,sc.setting_value,sc.config_title,sc.group_title,sc.source_data,sc.control_type," +
            "sc.data_order FROM ${schema_name}._sc_system_config sc WHERE sc.config_name = #{config_name}")
    SystemConfigData findSysConfigInfo(@Param("schema_name") String schema_name, @Param("config_name") String config_name);

    /**
     * 查询系统配置想详情
     */
    @Select("SELECT sc.config_name,sc.remark,sc.setting_value,sc.config_title,sc.group_title,sc.source_data,sc.control_type," +
            "sc.data_order FROM ${schema_name}._sc_system_config sc WHERE sc.config_name = #{config_name}")
    Map<String, Object> findMapSysConfigInfo(@Param("schema_name") String schema_name, @Param("config_name") String config_name);

    /**
     * 查询系统配置组名称列表
     *
     * @param schema_name 数据库
     * @return 组名称列表
     */
    @Select("SELECT distinct sc.group_title FROM ${schema_name}._sc_system_config sc ORDER BY sc.group_title ")
    String[] findGroupTitleList(@Param("schema_name") String schema_name);
}
