package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 工单
 */
@Mapper
public interface WorksMapper {

    /**
     * 新增工单负责人组
     *
     * @param schema_name     入参
     * @param sub_work_code   入参
     * @param receive_user_id 入参
     */
    @Insert(" INSERT INTO ${schema_name}._sc_works_detail_receive_group ( sub_work_code, receive_user_id, group_id ) " +
            " SELECT #{sub_work_code} AS sub_work_code,#{receive_user_id} AS receive_user_id,g.ID " +
            " FROM ${schema_name}._sc_group AS g" +
            " INNER JOIN ${schema_name}._sc_position AS p ON g.id = p.group_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON p.id = up.position_id " +
            " WHERE up.user_id = #{receive_user_id} ")
    void insertWorkReceiveGroup(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("receive_user_id") String receive_user_id);


    /**
     * 根据工单明细主键删除数据
     *
     * @param schemaName  入参
     * @param subWorkCode 入参
     */
    @Delete("DELETE FROM ${schema_name}.${tableName} WHERE sub_work_code = #{subWorkCode} ")
    void deleteBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode);

//    /**
//     * 根据主键查询备件数据（仅库房信息）
//     *
//     * @param schemaName  入参
//     * @param subWorkCode 入参
//     * @return 备件数据（仅库房信息）
//     */
//    @Select(" SELECT wb.*, s.stock_name,b.bom_name FROM ${schema_name}._sc_work_bom wb " +
//            " LEFT JOIN ${schema_name}._sc_bom b ON wb.bom_code = b.bom_code and wb.material_code = b.material_code " +
//            " LEFT JOIN ${schema_name}._sc_stock s ON wb.stock_code = s.stock_code " +
//            " WHERE sub_work_code = #{subWorkCode} ")
//    List<Map<String, Object>> queryWbsListBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 新增工单表
//     */
//    @Insert(" INSERT INTO  ${schema_name}._sc_works(work_code, work_type_id, work_template_code, pool_id, facility_id, relation_type, relation_id, title, problem_note, " +
//            " problem_img, occur_time, deadline_time, status, remark, create_time, create_user_id, plan_code, customer_id, position_code, work_request_code," +
//            " last_finished_time) VALUES (work_code, work_type_id, work_template_code, pool_id, facility_id, relation_type, relation_id, title, problem_note," +
//            " problem_img, occur_time, deadline_time, status, remark, create_time, create_user_id, plan_code, customer_id, position_code, work_request_code," +
//            " last_finished_time ")
//    void insertWorks(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据工单编号查询工单表详情
     */
    @Select("SELECT w.work_code,w.work_type_id,w.work_template_code,w.pool_id,w.facility_id,w.relation_type,w.relation_id,w.title,w.problem_note,w.problem_img," +
            "w.occur_time,w.deadline_time,w.status,w.remark,w.create_time,w.create_user_id,w.work_calendar_code,w.customer_id,w.position_code,w.work_request_code," +
            "w.last_finished_time FROM  ${schema_name}._sc_works AS w WHERE w.work_code = #{work_code} ")
    Map<String, Object> findWorksByCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 更新工单表
     */
    @Update(" <script>" +
            " UPDATE  ${schema_name}._sc_works  " +
            " SET work_code = #{pm.work_code}" +
            " <when test='pm.work_type_id!=null'>" +
            " , work_type_id= #{pm.work_type_id}::int" +
            " </when> " +
            " <when test='pm.work_template_code!=null'>" +
            " , work_template_code= #{pm.work_template_code}" +
            " </when> " +
            " <when test='pm.pool_id!=null'>" +
            " , pool_id= #{pm.pool_id}" +
            " </when> " +
            " <when test='pm.facility_id!=null'>" +
            " , facility_id= #{pm.facility_id}" +
            " </when> " +
            " <when test='pm.relation_type!=null'>" +
            " , relation_type= #{pm.relation_type}::int" +
            " </when> " +
            " <when test='pm.relation_id!=null'>" +
            " , relation_id= #{pm.relation_id}" +
            " </when> " +
            " <when test='pm.title!=null'>" +
            " , title= #{pm.title}" +
            " </when> " +
            " <when test='pm.problem_note!=null'>" +
            " , problem_note= #{pm.problem_note}" +
            " </when> " +
            " <when test='pm.problem_img!=null'>" +
            " , problem_img= #{pm.problem_img}" +
            " </when> " +
            " <when test='pm.occur_time!=null'>" +
            " , occur_time= #{pm.occur_time}" +
            " </when> " +
            " <when test='pm.deadline_time!=null'>" +
            " , deadline_time= #{pm.deadline_time}" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " , status= #{pm.status}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " , remark= #{pm.remark}" +
            " </when> " +
            " <when test='pm.create_time!=null'>" +
            " , create_time= #{pm.create_time}" +
            " </when> " +
            " <when test='pm.create_user_id!=null'>" +
            " , create_user_id= #{pm.create_user_id}" +
            " </when> " +
            " <when test='pm.plan_code!=null'>" +
            " , plan_code= #{pm.plan_code}" +
            " </when> " +
            " <when test='pm.customer_id!=null'>" +
            " , customer_id= #{pm.customer_id}" +
            " </when> " +
            " <when test='pm.position_code!=null'>" +
            " , position_code= #{pm.position_code}" +
            " </when> " +
            " <when test='pm.work_request_code!=null'>" +
            " , work_request_code= #{pm.work_request_code}" +
            " </when> " +
            " <when test='pm.last_finished_time!=null'>" +
            " , last_finished_time= #{pm.last_finished_time}" +
            " </when> " +
            " WHERE work_code = #{pm.work_code} " +
            " </script>")
    void updateWorks(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询工单详情
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     * @return 工单详情
     */
    @Select(" SELECT wd.sub_work_code,wd.work_code,wd.duty_user_id,wd.work_type_id,wd.work_template_code,wd.relation_type," +
            " wd.relation_id,wd.title,wd.problem_note,wd.problem_img,wd.running_status_id,wd.fault_type_id," +
            " wd.repair_type_id,wd.priority_level,wd.before_img,wd.result_note,wd.after_img,wd.file_ids," +
            " wd.status,wd.remark,wd.comment_list,wd.from_code,wd.waiting,wd.bom_app_result," +
            " wd.receive_user_id,wd.pref_id,wd.receive_time,wd.distribute_time,wd.begin_time,wd.finished_time,wd.audit_time," +
            " wd.fault_number,wd.is_main,wd.distribute_user_id,wd.plan_arrive_time,wd.service_score,wd.repair_code," +
            " wd.is_finished,wd.finished_id,wd.is_redo,wd.total_deal_time,w.pool_id,wt.business_type_id," +
            " a.asset_name,w.position_code,w.create_user_id " +
            " FROM ${schema_name}._sc_works_detail wd " +
            " LEFT JOIN ${schema_name}._sc_works w on wd.work_code=w.work_code" +
            " LEFT JOIN ${schema_name}._sc_work_type wt on w.work_type_id=wt.id" +
            " LEFT JOIN ${schema_name}._sc_asset a on w.relation_id=a.id" +
            " WHERE wd.sub_work_code=#{sub_work_code} AND wd.is_main = 1 ")
    Map<String, Object> findWorksDetailByCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    @Select("<script>" +
            " select unnest(tab.cc_users) as cc_users from (select string_to_array(field_value,',') as cc_users " +
            " from ${schema_name}._sc_works_detail_column where sub_work_code=#{sub_work_code} and field_code='carbon_copy' " +
            " and field_value is not null order by create_time desc limit 1) tab" +
            "</script>")
    List<String> findWorksCCUsersByCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    @Select("<script>" +
            " select field_value as cc_user from ${schema_name}._sc_works_detail_column " +
            " where sub_work_code=#{sub_work_code} and field_code='carbon_copy' " +
            " and field_value is not null order by create_time desc limit 1 " +
            "</script>")
    String findCCUserByCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    /**
     * 查询工单详情
     *
     * @param schema_name 入参
     * @return 工单详情
     */
    @Select(" SELECT wd.sub_work_code,wd.work_code,wd.work_type_id,wd.work_template_code,wd.relation_type," +
            " wd.relation_id,wd.title,wd.problem_note,wd.problem_img,wd.running_status_id,wd.fault_type_id," +
            " wd.repair_type_id,wd.priority_level,wd.before_img,wd.result_note,wd.after_img,wd.file_ids," +
            " wd.status,wd.remark,wd.comment_list,wd.from_code,wd.waiting,wd.bom_app_result," +
            " wd.receive_user_id,wd.receive_time,wd.distribute_time,wd.begin_time,wd.finished_time,wd.audit_time," +
            " wd.fault_number,wd.is_main,wd.distribute_user_id,wd.plan_arrive_time,wd.service_score,wd.repair_code," +
            " wd.is_finished,wd.finished_id,wd.is_redo,wd.total_deal_time" +
            " FROM ${schema_name}._sc_works_detail wd " +
            " WHERE wd.work_code=#{work_code}  ")
    List<Map<String, Object>> findWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 查询工单详情
     *
     * @param schema_name 入参
     * @return 工单详情
     */
    @Select(" SELECT wd.sub_work_code,wd.work_code,wd.work_type_id,wd.work_template_code,wd.relation_type," +
            " wd.relation_id,wd.title,wd.problem_note,wd.problem_img,wd.running_status_id,wd.fault_type_id," +
            " wd.repair_type_id,wd.priority_level,wd.before_img,wd.result_note,wd.after_img,wd.file_ids," +
            " wd.status,wd.remark,wd.comment_list,wd.from_code,wd.waiting,wd.bom_app_result," +
            " wd.receive_user_id,wd.receive_time,wd.distribute_time,wd.begin_time,wd.finished_time,wd.audit_time," +
            " wd.fault_number,wd.is_main,wd.distribute_user_id,wd.plan_arrive_time,wd.service_score,wd.repair_code," +
            " wd.is_finished,wd.finished_id,wd.is_redo,wd.total_deal_time" +
            " FROM ${schema_name}._sc_works_detail wd " +
            " WHERE wd.work_code=#{work_code} AND wd.is_main = 1 ")
    Map<String, Object> findMainWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 更新工单详情
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_works_detail " +
            " SET work_code = #{pm.work_code}" +
            " <when test='pm.work_type_id!=null'>" +
            " ,work_type_id =#{pm.work_type_id}::int" +
            " </when> " +
            " <when test='pm.work_template_code!=null'>" +
            " ,work_template_code = #{pm.work_template_code}" +
            " </when> " +
            " <when test='pm.relation_type!=null'>" +
            " ,relation_type =#{pm.relation_type}::int" +
            " </when> " +
            " <when test='pm.relation_id!=null'>" +
            " ,relation_id =#{pm.relation_id}" +
            " </when> " +
            " <when test='pm.title!=null'>" +
            " ,title = #{pm.title}" +
            " </when> " +
            " <when test='pm.problem_note!=null'>" +
            " ,problem_note = #{pm.problem_note}" +
            " </when> " +
            " <when test='pm.problem_img!=null'>" +
            " ,problem_img = #{pm.problem_img}" +
            " </when> " +
            " <when test='pm.running_status_id!=null'>" +
            " ,running_status_id = #{pm.running_status_id}::int" +
            " </when> " +
            " <when test='pm.fault_type_id!=null'>" +
            " ,fault_type_id =#{pm.fault_type_id}::int" +
            " </when> " +
            " <when test='pm.repair_type_id!=null'>" +
            " ,repair_type_id = #{pm.repair_type_id}::int" +
            " </when> " +
            " <when test='pm.priority_level!=null'>" +
            " ,priority_level = #{pm.priority_level}::int" +
            " </when> " +
            " <when test='pm.before_img!=null'>" +
            " ,before_img = #{pm.before_img}" +
            " </when> " +
            " <when test='pm.result_note!=null'>" +
            " ,result_note = #{pm.result_note}" +
            " </when> " +
            " <when test='pm.after_img!=null'>" +
            " ,after_img = #{pm.after_img}" +
            " </when> " +
            " <when test='pm.file_ids!=null'>" +
            " ,file_ids = #{pm.file_ids}" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " ,status = #{pm.status}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " ,remark = #{pm.remark}" +
            " </when> " +
            " <when test='pm.comment_list!=null'>" +
            " ,comment_list = #{pm.comment_list}" +
            " </when> " +
            " <when test='pm.from_code!=null'>" +
            " ,from_code = #{pm.from_code}" +
            " </when> " +
            " <when test='pm.waiting!=null'>" +
            " ,waiting = #{pm.waiting}" +
            " </when> " +
            " <when test='pm.bom_app_result!=null'>" +
            " ,bom_app_result = #{pm.bom_app_result}" +
            " </when> " +
            " <when test='pm.receive_user_id!=null'>" +
            " ,receive_user_id = #{pm.receive_user_id}" +
            " </when> " +
            " <when test='pm.receive_time!=null'>" +
            " ,receive_time = #{pm.receive_time}" +
            " </when> " +
            " <when test='pm.distribute_time!=null'>" +
            " ,distribute_time = #{pm.distribute_time}" +
            " </when> " +
            " <when test='pm.begin_time!=null'>" +
            " ,begin_time = #{pm.begin_time}" +
            " </when> " +
            " <when test='pm.sign_in_time!=null'>" +
            " ,sign_in_time = #{pm.sign_in_time}" +
            " </when> " +
            " <when test='pm.finished_time!=null'>" +
            " ,finished_time = #{pm.finished_time}" +
            " </when> " +
            " <when test='pm.audit_time!=null'>" +
            " ,audit_time = #{pm.audit_time}" +
            " </when> " +
            " <when test='pm.fault_number!=null'>" +
            " ,fault_number = #{pm.fault_number}" +
            " </when> " +
            " <when test='pm.is_main!=null'>" +
            " ,is_main = #{pm.is_main}::int" +
            " </when> " +
            " <when test='pm.distribute_user_id!=null'>" +
            " ,distribute_user_id = #{pm.distribute_user_id}" +
            " </when> " +
            " <when test='pm.plan_arrive_time!=null'>" +
            " ,plan_arrive_time = #{pm.plan_arrive_time}" +
            " </when> " +
            " <when test='pm.service_score!=null'>" +
            " ,service_score = #{pm.service_score}" +
            " </when> " +
            " <when test='pm.repair_code!=null'>" +
            " ,repair_code = #{pm.repair_code}" +
            " </when> " +
            " <when test='pm.is_finished!=null'>" +
            " ,is_finished = #{pm.is_finished}" +
            " </when> " +
            " <when test='pm.finished_id!=null'>" +
            " ,finished_id = #{pm.finished_id}" +
            " </when> " +
            " <when test='pm.is_redo!=null'>" +
            " ,is_redo = #{pm.is_redo}::int" +
            " </when> " +
            " <when test='pm.total_deal_time!=null'>" +
            " ,total_deal_time = #{pm.total_deal_time} " +
            " </when> " +
            " <when test='pm.duty_user_id!=null'>" +
            " ,duty_user_id = #{pm.duty_user_id} " +
            " </when> " +
            " <when test='pm.close_time!=null'>" +
            " ,close_time = #{pm.close_time} " +
            " </when> " +
            " WHERE sub_work_code = #{pm.sub_work_code}" +
            " </script>")
    void updateWorksDetailByCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新工单详情
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_works_detail " +
            " SET work_code = #{pm.work_code}" +
            " <when test='pm.work_type_id!=null'>" +
            " ,work_type_id =#{pm.work_type_id}::int" +
            " </when> " +
            " <when test='pm.work_template_code!=null'>" +
            " ,work_template_code = #{pm.work_template_code}" +
            " </when> " +
            " <when test='pm.relation_type!=null'>" +
            " ,relation_type =#{pm.relation_type}::int" +
            " </when> " +
            " <when test='pm.relation_id!=null'>" +
            " ,relation_id =#{pm.relation_id}" +
            " </when> " +
            " <when test='pm.title!=null'>" +
            " ,title = #{pm.title}" +
            " </when> " +
            " <when test='pm.problem_note!=null'>" +
            " ,problem_note = #{pm.problem_note}" +
            " </when> " +
            " <when test='pm.problem_img!=null'>" +
            " ,problem_img = #{pm.problem_img}" +
            " </when> " +
            " <when test='pm.running_status_id!=null'>" +
            " ,running_status_id = #{pm.running_status_id}::int" +
            " </when> " +
            " <when test='pm.fault_type_id!=null'>" +
            " ,fault_type_id =#{pm.fault_type_id}::int" +
            " </when> " +
            " <when test='pm.repair_type_id!=null'>" +
            " ,repair_type_id = #{pm.repair_type_id}::int" +
            " </when> " +
            " <when test='pm.priority_level!=null'>" +
            " ,priority_level = #{pm.priority_level}::int" +
            " </when> " +
            " <when test='pm.before_img!=null'>" +
            " ,before_img = #{pm.before_img}" +
            " </when> " +
            " <when test='pm.result_note!=null'>" +
            " ,result_note = #{pm.result_note}" +
            " </when> " +
            " <when test='pm.after_img!=null'>" +
            " ,after_img = #{pm.after_img}" +
            " </when> " +
            " <when test='pm.file_ids!=null'>" +
            " ,file_ids = #{pm.file_ids}" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " ,status = #{pm.status}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " ,remark = #{pm.remark}" +
            " </when> " +
            " <when test='pm.comment_list!=null'>" +
            " ,comment_list = #{pm.comment_list}" +
            " </when> " +
            " <when test='pm.from_code!=null'>" +
            " ,from_code = #{pm.from_code}" +
            " </when> " +
            " <when test='pm.waiting!=null'>" +
            " ,waiting = #{pm.waiting}" +
            " </when> " +
            " <when test='pm.bom_app_result!=null'>" +
            " ,bom_app_result = #{pm.bom_app_result}" +
            " </when> " +
            " <when test='pm.receive_user_id!=null'>" +
            " ,receive_user_id = #{pm.receive_user_id}" +
            " </when> " +
            " <when test='pm.receive_time!=null'>" +
            " ,receive_time = #{pm.receive_time}" +
            " </when> " +
            " <when test='pm.distribute_time!=null'>" +
            " ,distribute_time = #{pm.distribute_time}" +
            " </when> " +
            " <when test='pm.begin_time!=null'>" +
            " ,begin_time = #{pm.begin_time}::timestamp" +
            " </when> " +
            " <when test='pm.finished_time!=null'>" +
            " ,finished_time = #{pm.finished_time}" +
            " </when> " +
            " <when test='pm.audit_time!=null'>" +
            " ,audit_time = #{pm.audit_time}" +
            " </when> " +
            " <when test='pm.fault_number!=null'>" +
            " ,fault_number = #{pm.fault_number}" +
            " </when> " +
            " <when test='pm.is_main!=null'>" +
            " ,is_main = #{pm.is_main}::int" +
            " </when> " +
            " <when test='pm.distribute_user_id!=null'>" +
            " ,distribute_user_id = #{pm.distribute_user_id}" +
            " </when> " +
            " <when test='pm.plan_arrive_time!=null'>" +
            " ,plan_arrive_time = #{pm.plan_arrive_time}" +
            " </when> " +
            " <when test='pm.service_score!=null'>" +
            " ,service_score = #{pm.service_score}" +
            " </when> " +
            " <when test='pm.repair_code!=null'>" +
            " ,repair_code = #{pm.repair_code}" +
            " </when> " +
            " <when test='pm.is_finished!=null'>" +
            " ,is_finished = #{pm.is_finished}" +
            " </when> " +
            " <when test='pm.finished_id!=null'>" +
            " ,finished_id = #{pm.finished_id}" +
            " </when> " +
            " <when test='pm.is_redo!=null'>" +
            " ,is_redo = #{pm.is_redo}::int" +
            " </when> " +
            " <when test='pm.total_deal_time!=null'>" +
            " ,total_deal_time = #{pm.total_deal_time} " +
            " </when> " +
            " <when test='pm.duty_user_id!=null'>" +
            " ,duty_user_id = #{pm.duty_user_id} " +
            " </when> " +
            " <when test='pm.close_time!=null'>" +
            " ,close_time = #{pm.close_time} " +
            " </when> " +
            " WHERE work_code = #{pm.work_code}" +
            " </script>")
    void updateWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据工单详情表编号查询工单详情字段列表
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     */
    @Select(" SELECT wdc.field_form_code,wdc.field_code,wdc.field_name,wdc.save_type,wdc.change_type,wdc.field_value,wdc.is_required,wdc.is_table_key,wdc.db_table_type," +
            " wdc.field_remark,wdc.field_data_base,wdc.field_view_type,wdc.field_section_type,wdc.field_validate_method,wdc.create_time,wdc.create_user_id" +
            " FROM ${schema_name}._sc_works_detail_column AS wdc" +
            " WHERE wdc.sub_work_code = #{sub_work_code} ")
    List<Map<String, Object>> findWorksDetailColumnList(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据工单详情表编号查询工单详情字段列表(剔除子页面)
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     */
    @Select(" SELECT wdc.field_form_code,wdc.field_code,wdc.field_name,wdc.save_type,wdc.change_type,wdc.field_value,wdc.is_required,wdc.is_table_key,wdc.db_table_type," +
            " wdc.field_remark,wdc.field_data_base,wdc.field_view_type,wdc.field_section_type,wdc.field_validate_method,wdc.create_time,wdc.create_user_id" +
            " FROM ${schema_name}._sc_works_detail_column AS wdc" +
            " WHERE wdc.sub_work_code = #{sub_work_code} and wdc.parent_id is null")
    List<Map<String, Object>> findWorksDetailColumnListNoParentId(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    @Select("SELECT wdc.field_form_code,string_agg ( DISTINCT wdc.field_value,',' ) field_value FROM " +
            " ${schema_name}._sc_works_detail_column AS wdc WHERE wdc.sub_work_code = #{sub_work_code} GROUP BY wdc.field_form_code")
    List<Map<String, Object>> findWorksDetailColumnListGroup(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据工单详情编号和字段编码更新工单详情字段
     */
    @Update("<script>" +
            " UPDATE ${schema_name}._sc_works_detail_column " +
            " SET field_code = #{pm.field_code}" +
            " <when test='pm.field_name!=null'>" +
            " ,field_name = #{pm.field_name}" +
            " </when> " +
            " <when test='pm.save_type!=null'>" +
            " ,save_type = #{pm.save_type}" +
            " </when> " +
            " <when test='pm.change_type!=null'>" +
            " ,change_type = #{pm.change_type}" +
            " </when> " +
            " <when test='pm.field_value!=null'>" +
            " ,field_value = #{pm.field_value}" +
            " </when> " +
            " <when test='pm.is_required!=null'>" +
            " ,is_required = #{pm.is_required}" +
            " </when> " +
            " <when test='pm.is_table_key!=null'>" +
            " ,is_table_key = #{pm.is_table_key}" +
            " </when> " +
            " <when test='pm.db_table_type!=null'>" +
            " ,db_table_type = #{pm.db_table_type}" +
            " </when> " +
            " <when test='pm.field_remark!=null'>" +
            " ,field_remark = #{pm.field_remark}" +
            " </when> " +
            " <when test='pm.field_data_base!=null'>" +
            " ,field_data_base = #{pm.field_data_base}" +
            " </when> " +
            " <when test='pm.field_view_type!=null'>" +
            " ,field_view_type = #{pm.field_view_type}" +
            " </when> " +
            " <when test='pm.field_section_type!=null'>" +
            " ,field_section_type = #{pm.field_section_type}" +
            " </when> " +
            " <when test='pm.field_validate_method!=null'>" +
            " ,field_validate_method = #{pm.field_validate_method}" +
            " </when> " +
            " WHERE field_form_code = #{pm.field_form_code} AND sub_work_code = #{pm.sub_work_code}" +
            " </script>")
    void updateWorksDetailColumnByCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据工单详情编号和字段编码更新工单详情扩展字段
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_works_detail_column_ext " +
            " SET sub_work_code = sub_work_code" +
            " <when test='pm.field_write_type!=null'>" +
            " ,field_write_type = #{pm.field_write_type}" +
            " </when> " +
            " <when test='pm.field_value!=null'>" +
            " ,field_value = #{pm.field_value} " +
            " </when> " +
            " WHERE field_form_code = #{pm.field_form_code} AND sub_work_code = #{pm.sub_work_code} AND field_name = #{pm.field_name}" +
            " </script>")
    void updateWorksDetailColumnExtByCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增工单详情字段
     */
    @Insert("INSERT INTO ${schema_name}._sc_works_detail_column( parent_id,sub_work_code, field_form_code, field_code, field_name, save_type, change_type," +
            "field_value, is_required, is_table_key, db_table_type, field_remark, field_data_base, field_view_type, field_section_type, field_validate_method," +
            " create_time, create_user_id) VALUES (   #{pm.parent_id}::int, #{pm.sub_work_code}, #{pm.field_form_code}, #{pm.field_code}, #{pm.field_name}, #{pm.save_type}, #{pm.change_type}," +
            "#{pm.field_value}, #{pm.is_required}, #{pm.is_table_key}, #{pm.db_table_type}, #{pm.field_remark}, #{pm.field_data_base}, #{pm.field_view_type}, " +
            "#{pm.field_section_type}, #{pm.field_validate_method}," +
            "#{pm.create_time},  #{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertWorksDetailColumn(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增工单详情字段扩展
     */
    @Insert("INSERT INTO ${schema_name}._sc_works_detail_column_ext(sub_work_code, field_form_code, field_name,  field_write_type, field_value)" +
            " VALUES (#{pm.sub_work_code}, #{pm.field_form_code}, #{pm.field_name},  #{pm.field_write_type}, #{pm.field_value}) ")
    void insertWorksDetailColumnExt(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增工单详情字段联动
     */
    @Insert(" INSERT INTO ${schema_name}._sc_works_detail_column_linkage( sub_work_code, field_form_code, linkage_type) " +
            " VALUES (#{pm.sub_work_code}, #{pm.field_form_code}, #{pm.linkage_type}::int)")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertWorksDetailColumnLinkage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增工单详情字段联动条件
     */
    @Insert(" INSERT INTO  ${schema_name}._sc_works_detail_column_linkage_condition(sub_work_code, linkage_id, link_field_form_code, interface_address," +
            " self_defined, condition) " +
            " VALUES (#{pm.sub_work_code}, #{pm.linkage_id}, #{pm.link_field_form_code}, #{pm.interface_address}, #{pm.self_defined}, #{pm.condition}::jsonb)")
    void insertWorksDetailColumnLinkageCondition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据工单详情编码和字段主键获取工单详情扩展字段列表
     */
    @Select(" SELECT wdce.id,wdce.sub_work_code,wdce.field_form_code,wdce.field_name,wdce.field_write_type,wdce.field_value" +
            " FROM ${schema_name}._sc_works_detail_column_ext AS wdce" +
            " WHERE wdce.sub_work_code = #{sub_work_code} AND wdce.field_form_code = #{field_form_code} ")
    List<Map<String, Object>> findWorksDetailColumnExtListByFileFormCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("field_form_code") String field_form_code);

    /**
     * 根据工单详情联动表id获取工单详情联动条件列表
     */
    @Select(" SELECT wdclc.id,wdclc.linkage_id,wdclc.link_field_form_code,wdclc.interface_address,wdclc.condition" +
            " FROM ${schema_name}._sc_works_detail_column_linkage_condition AS wdclc" +
            " WHERE wdclc.linkage_id = #{linkage_id} AND wdclc.sub_work_code = #{sub_work_code}")
    Map<String, Object> findWorksDetailColumnLinkageConditionByLinkageId(@Param("schema_name") String schema_name, @Param("linkage_id") Integer linkage_id, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据工单详情编号和字段主键获取东单联动条件列表
     */
    @Select(" SELECT wdcl.id,wdcl.sub_work_code,wdcl.linkage_type" +
            " FROM ${schema_name}._sc_works_detail_column_linkage AS wdcl" +
            " WHERE wdcl.sub_work_code = #{sub_work_code} AND field_form_code = #{field_form_code} ")
    List<Map<String, Object>> findWorksDetailColumnLinkageList(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("field_form_code") String field_form_code);

    /**
     * 根据工单详情字段联动条件表id更新根据工单详情字段联动条件表
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_works_detail_column_linkage_condition " +
            " SET sub_work_code = #{pm.sub_work_code}" +
            " <when test='pm.linkage_id!=null'>" +
            " ,linkage_id = #{pm.linkage_id}" +
            " </when> " +
            " <when test='pm.link_field_form_code!=null'>" +
            " ,link_field_form_code = #{pm.link_field_form_code}" +
            " </when> " +
            " <when test='pm.interface_address!=null'>" +
            " ,interface_address = #{pm.interface_address}" +
            " </when> " +
            " <when test='pm.self_defined!=null'>" +
            " ,self_defined = #{pm.self_defined}" +
            " </when> " +
            " <when test='pm.condition!=null'>" +
            " ,condition = #{pm.condition}::jsonb " +
            " </when> " +
            " WHERE id = #{pm.id}" +
            " </script>")
    void updateWorksDetailColumnLinkageConditionById(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增数据
     */
    @InsertProvider(type = WorksMapper.WorkSheetHandleMapperProvider.class, method = "insert")
    void insert(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap, String tableName);

    /**
     * 更新数据
     */
    @InsertProvider(type = WorksMapper.WorkSheetHandleMapperProvider.class, method = "update")
    void update(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap, String tableName, String key);

    /**
     * 根据work_code修改工单表状态
     *
     * @param schema_name 入参
     * @param status      入参
     * @param workCode    入参
     */
    @Update("Update ${schema_name}._sc_works set status = #{status} where work_code = #{work_code}")
    void updateWorkStatusByCode(@Param("schema_name") String schema_name, @Param("status") Integer status, @Param("work_code") String workCode);

    /**
     * 根据sub_work_code查询工单详情表是否有为待处理状态的detail
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     */
    @Select("Select count(1) from ${schema_name}._sc_works_detail as wd  where wd.status= " + StatusConstant.PENDING + " and  wd.sub_work_code= #{sub_work_code}")
    int findCountWorkDetailBySwcStatus(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据sub_work_code修改工单detail表分配人及状态
     *
     * @param schema_name     入参
     * @param receive_user_id 入参
     * @param sub_work_code   入参
     */
    @Update("Update ${schema_name}._sc_works_detail set duty_user_id = #{receive_user_id},status = #{status}  where sub_work_code = #{sub_work_code}")
    void updateWorksDetailReceiveUserAndStatusBySwc(@Param("schema_name") String schema_name, @Param("receive_user_id") String receive_user_id, @Param("status") Integer status, @Param("sub_work_code") String sub_work_code);

    @Select("SELECT count(1) FROM ${schema_name}._sc_works_detail WHERE work_code = #{work_code} AND status != " + StatusConstant.UNDISTRIBUTED + " and sub_work_code != #{sub_work_code} ")
    int findCountUndistributedWorkByCode(@Param("schema_name") String schema_name, @Param("work_code") String workCode, @Param("sub_work_code") String sub_work_code);


    /**
     * 实现类
     */
    class WorkSheetHandleMapperProvider {
        /**
         * 新增数据
         *
         * @param schemaName 入参
         * @param paramMap   入参
         * @param tableName  入参
         * @return 更新记录数
         */
        public String insert(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap, String tableName) {
            return new SQL() {{
                INSERT_INTO(schemaName + "." + tableName);
                Set<String> keys = paramMap.keySet();
                for (String key : keys) {
                    if (key.contains("@")) {
                        String[] columnInfo = key.split("@");
                        String type = columnInfo[0];
                        if (!"-1".equals(type)) {
                            String column = columnInfo[1];
//                            if ("create_time".equals(column)) {
//                                Date now = new Date();
//                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                String today = dateFormat.format(now);
//                                paramMap.put(column, Timestamp.valueOf(today));
//                                VALUES(column, "#{t." + column + "}");
//                                continue;
//                            }
                            if ("varchar".equals(type)) {
                                VALUES(column, "#{t." + column + "}");
                            } else if (SqlConstant.DB_TYPE_NORMAL.contains("," + type + ",")) {
                                VALUES(column, "#{t." + column + "}::" + type);
                            } else if (SqlConstant.DB_TYPE_TIMESTAMP.contains("," + type + ",")) {
                                VALUES(column, "TO_TIMESTAMP(#{t." + column + "}, 'YYYY-MM-DD hh24:mi:ss')");
                            } else if ("null".equals(type)) {
                                VALUES(column, "null");
                            } else if ("now".equals(type)) {
                                VALUES(column, "current_timestamp");
                            } else if ("loginUser".equals(type)) {
                                VALUES(column, "#{t.loginUser}");
                            } else if ("nowKey".equals(type)) {
                                VALUES(column, "#{t.nowKey}");
                            }
                        }
                    }
                }
            }}.toString();
        }

        /**
         * 查询列表
         *
         * @param schemaName 入参
         * @param searchKey  入参
         * @return 列表
         */
        public String query(@Param("schema_name") String schemaName,
                            @Param("searchKey") String searchKey) {
            return new SQL() {{
                SELECT(" * ");
                FROM(schemaName + "._sc_works_detail");
                // 模板名称
                if (null != searchKey && !"".equals(searchKey)) {
                    WHERE("sub_work_code like CONCAT('%', #{searchKey}, '%')");
                }
            }}.toString();
        }

        /**
         * 更新
         *
         * @param schemaName 入参
         * @param paramMap   入参
         * @param tableName  入参
         * @param key        入参
         * @return 更新记录数
         */
        public String update(@Param("schema_name") String schemaName,
                             @Param("t") Map<String, Object> paramMap, String tableName, String key) {
            return new SQL() {{
                StringBuffer sb = new StringBuffer();
                Set<String> keys = paramMap.keySet();
                for (String key : keys) {
                    sb = doChangeUpdateColumn(key, sb, "t");
                }
                if (RegexUtil.optNotNull(sb.toString()).isPresent()) {
                    UPDATE(schemaName + "." + tableName + " set " + sb.toString().substring(1));
                    WHERE(" " + key + "=#{t." + key + "} ");
                }
            }}.toString();
        }

        /**
         * 更新用数据库字段类型转换工具
         *
         * @param key    入参
         * @param sb     入参
         * @param prefix 入参
         * @return 更新记录数
         */
        private static StringBuffer doChangeUpdateColumn(String key, StringBuffer sb, String prefix) {
            if (key.contains("@")) {
                String[] columnInfo = key.split("@");
                String type = columnInfo[0];
                String column = columnInfo[1];
                if (!"-1".equals(type)) {
                    sb.append(",");
                    sb.append(column);
                    if ("varchar".equals(type)) {
                        sb.append("=#{");
                        sb.append(prefix);
                        sb.append(".");
                        sb.append(column);
                        sb.append("}");
                    } else if (SqlConstant.DB_TYPE_NORMAL.contains("," + type + ",")) {
                        sb.append("=#{");
                        sb.append(prefix);
                        sb.append(".");
                        sb.append(column);
                        sb.append("}::");
                        sb.append(type);
                    } else if (SqlConstant.DB_TYPE_TIMESTAMP.contains("," + type + ",")) {
                        sb.append("=TO_TIMESTAMP(#{");
                        sb.append(prefix);
                        sb.append(".");
                        sb.append(column);
                        sb.append("}, 'YYYY-MM-DD hh24:mi:ss')");
                    } else if ("null".equals(type)) {
                        sb.append("=null");
                    } else if ("now".equals(type)) {
                        sb.append("=current_timestamp");
                    } else if ("loginUser".equals(type)) {
                        sb.append("=#{t.loginUser}");
                    } else if ("nowKey".equals(type)) {
                        sb.append("=#{t.nowKey}");
                    }
                }
            }
            return sb;
        }
    }

    /**
     * 新增工单组织
     *
     * @param schema_name   入参
     * @param sub_work_code 入参
     * @param assetId       入参
     * @return 更新记录数
     */
    @Insert(" INSERT INTO ${schema_name}._sc_works_detail_asset_org ( sub_work_code, asset_id, facility_id ) " +
            " SELECT #{sub_work_code} AS sub_work_code,#{assetId} AS asset_id,ao.org_id AS facility_id" +
            " FROM ${schema_name}._sc_asset_organization AS ao" +
            " LEFT JOIN ${schema_name}._sc_facilities AS f ON ao.org_id=f.id" +
            " WHERE  ao.asset_id=#{assetId} AND f.org_type_id IN (" + SqlConstant.FACILITY_INSIDE + ")")
    int insertWorkAssetOrg(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("assetId") String assetId);

    /**
     * 更新支援人员请求单号
     *
     * @param schemaName      入参
     * @param workRequestCode 入参
     * @return 更新记录数
     */
    @Update("UPDATE ${schema_name}._sc_work_dutyman_hour SET work_request_code = #{workRequestCode} WHERE sub_work_code = #{subWorkCode} ")
    int updateWorkDutymanHourInfoByCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode, @Param("workRequestCode") String workRequestCode);

    /**
     * 更新子工单状态
     *
     * @param schemaName 入参
     * @param status     入参
     * @param workCode   入参
     */
    @Update("UPDATE ${schema_name}._sc_works_detail SET status = #{status} WHERE work_code = #{workCode} ")
    void updateWorkDetailStatusByWorkCode(@Param("schema_name") String schemaName, @Param("status") Integer status, @Param("workCode") String workCode);

    /**
     * 根据工单主键删除数据
     *
     * @param schemaName      入参
     * @param workRequestCode 入参
     */
    @Delete("DELETE FROM ${schema_name}.${tableName} WHERE work_request_code = #{workRequestCode} ")
    void deleteByWorkRequestCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("workRequestCode") String workRequestCode);

    /**
     * 查询工单列表数量
     *
     * @param schemaName 入参
     * @param pm         入参
     * @param user_id    当前登录用户
     * @return 工单列表数量
     */
    @Select(" <script>" +
            " SELECT" +
            " COUNT(w.work_code) " +
            " FROM ${schema_name}._sc_works AS w" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code AND wd.is_main = 1" +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = A.id AND w.relation_type = 2" +
            " LEFT JOIN ${schema_name}._sc_importment_level AS il ON a.importment_level_id = il.id " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.relation_id = ap.position_code AND w.relation_type = 1" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id" +
            " LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id =a.category_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id= wd.duty_user_id " + SqlConstant.WORK_AUTH_SQL +
            " WHERE 1=1 " +
            " <when test='pm.positionCodeSearch==null " +
            "  and pm.assetCategoryIdSearch==null " +
            "  and pm.workTypeIdSearch==null " +
            "  and pm.keywordSearch==null " +
            "  and pm.beginTime==null " +
            "  and pm.statusSearch==null " +
            "  and pm.sectionType==null " +
            "  and pm.finishedTime==null" +
            "  and pm.startCreateDateSearch==null" +
            "  and pm.endCreateDateSearch==null" +
            "  and pm.overtimeSearch==null'>" +
            //当前处理人是自己
            "  OR wd.duty_user_id = #{user_id} " +
            " </when>" +
            //筛选条件
            " <when test='pm.positionCodeSearch!=null'>" +
            "  AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END IN " +
            " (WITH RECURSIVE re (position_code, parent_code) AS ( " +
            " SELECT t.position_code, t.parent_code FROM ${schema_name}._sc_asset_position t where t.position_code IN" +
            "  <foreach collection='pm.positionCodeSearch' item='id' open='(' close=')' separator=','> #{id} </foreach>" +
            " UNION ALL" +
            " SELECT t2.position_code, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e WHERE e.position_code = t2.parent_code" +
            " ) select position_code from re) " +
            " </when>" +
            " <when test='pm.assetCategoryIdSearch!=null'>" +
            " AND a.category_id IN (WITH RECURSIVE re (id, parent_id) AS (" +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_asset_category t where t.id IN " +
            "  <foreach collection='pm.assetCategoryIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " UNION ALL " +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_asset_category t2 , re e WHERE e.id = t2.parent_id" +
            " ) select id from re )" +
            " </when>" +
            " <when test='pm.workTypeIdSearch!=null'>" +
            " AND w.work_type_id IN " +
            "  <foreach collection='pm.workTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.statusSearch!=null'>" +
            " AND w.status  IN " +
            "  <foreach collection='pm.statusSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " a.asset_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR w.work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR wd.sub_work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR a.asset_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u2.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR il.level_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when>" +
            " <when test='pm.startCreateDateSearch!=null and pm.startCreateDateSearch!=\"\"'>" +
            " AND w.create_time &gt;=#{pm.startCreateDateSearch}::TIMESTAMP " +
            " </when>" +
            " <when test='pm.endCreateDateSearch!=null and pm.endCreateDateSearch!=\"\"'>" +
            " AND w.create_time &lt;=#{pm.endCreateDateSearch}::TIMESTAMP  " +
            " </when>" +
            " <when test='pm.beginTime!=null and pm.beginTime!=\"\" and pm.finishedTime!=\"\" and pm.finishedTime!=null'>" +
            " AND w.occur_time &gt;=#{pm.beginTime}::TIMESTAMP AND w.occur_time &lt;=#{pm.finishedTime}::TIMESTAMP  " +
            " </when>" +
            " <when test=\"pm.overtimeSearch!=null and '1'.toString() == pm.overtimeSearch\">" +
            " AND w.deadline_time is not null AND w.status &lt;" + StatusConstant.COMPLETED + " and w.status &gt; " + StatusConstant.DRAFT + " AND to_char(w.deadline_time, '" + Constants.DATE_FMT + "') &lt; to_char(now(),'" + Constants.DATE_FMT + "')" +
            " </when>" +
            " <when test=\"pm.sectionType!=null and 'reportToday' == pm.sectionType\">" +//今日上报  只查询今日生成的维修工单
            " AND w.create_time &gt;= current_date AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1001 + "::int " +
            " </when>" +
            " <when test=\"pm.sectionType!=null and 'completedToday' == pm.sectionType\">" +//今日完成
            " AND TO_CHAR( wd.finished_time, 'YYYY-MM-DD' ) =  TO_CHAR( now(), 'YYYY-MM-DD' )  " +
            " </when>" +
            " <when test=\"pm.sectionType!=null and 'timeoutTask' == pm.sectionType\">" +//超时任务
            " AND w.deadline_time is not null AND w.status &lt;" + StatusConstant.COMPLETED + " and w.status &gt; " + StatusConstant.DRAFT + " AND to_char(w.deadline_time, '" + Constants.DATE_FMT + "') &lt; to_char(now(),'" + Constants.DATE_FMT + "')" +
            " </when>" +

            " </script> ")
    Integer findWorksListCount(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm, @Param("user_id") String user_id);

    /**
     * 分页工单管理列表
     *
     * @param schemaName 入参
     * @param pm         入参
     * @param user_id    当前登录用户
     * @return 工单管理列表
     */
    @Select(" <script>" +
            " SELECT" +
            //" ( CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END ) AS position_code," +
            " wd.position_code,ap.position_name,w.create_time," +
//            " ( CASE WHEN w.relation_type = '1' THEN ap.position_name WHEN w.relation_type = '2'  THEN " +
//            " (SELECT ap.position_name FROM ${schema_name}._sc_asset AS a LEFT JOIN ${schema_name}._sc_asset_position AS ap ON a.position_code = ap.position_code " +
//            " WHERE a.id = w.relation_id)  ELSE'' END ) AS position_name ," +
            " w.work_code,w.work_type_id,wd.priority_level,w.relation_type,w.relation_id,w.title,a.asset_code,a.asset_name,a.category_id,w.status," +
            " w.create_user_id,wd.duty_user_id,to_char(w.occur_time, 'YYYY-MM-DD hh24:mi:ss') AS occur_time,to_char(wd.begin_time, 'YYYY-MM-DD hh24:mi:ss') AS begin_time,u.user_name,wd.receive_user_id,wt.type_name AS work_type_name,ac.category_name," +
            " u2.user_name AS duty_user_name ,wd.sub_work_code, bt.business_name ,s.status AS status_name,bt.business_type_id,il.level_name" +
            " FROM ${schema_name}._sc_works AS w" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code AND wd.is_main = 1" +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = A.id AND w.relation_type = 2" +
            " LEFT JOIN ${schema_name}._sc_importment_level AS il ON a.importment_level_id = il.id " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON wd.position_code = ap.position_code" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id" +
            " LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id =a.category_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id= wd.duty_user_id " +
            " LEFT JOIN ${schema_name}._sc_business_type AS bt ON bt.business_type_id = wt.business_type_id " +
            " LEFT JOIN ${schema_name}._sc_status AS s ON s.id = w.status " + SqlConstant.WORK_AUTH_SQL +
            " WHERE  1=1 " +
            " <when test='pm.positionCodeSearch==null " +
            "  and pm.assetCategoryIdSearch==null " +
            "  and pm.workTypeIdSearch==null " +
            "  and pm.keywordSearch==null " +
            "  and pm.beginTime==null " +
            "  and pm.statusSearch==null " +
            "  and pm.finishedTime==null" +
            "  and pm.sectionType==null" +
            "  and pm.startCreateDateSearch==null" +
            "  and pm.endCreateDateSearch==null" +
            "  and pm.overtimeSearch==null'>" +
            //当前处理人是自己
            "  OR wd.duty_user_id = #{user_id} " +
            " </when>" +
            //筛选条件
            " <when test='pm.positionCodeSearch!=null'>" +
            "  AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END IN " +
            " (WITH RECURSIVE re (position_code, parent_code) AS ( " +
            " SELECT t.position_code, t.parent_code FROM ${schema_name}._sc_asset_position t where t.position_code IN" +
            "  <foreach collection='pm.positionCodeSearch' item='id' open='(' close=')' separator=','> #{id} </foreach>" +
            " UNION ALL" +
            " SELECT t2.position_code, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e WHERE e.position_code = t2.parent_code" +
            " ) select position_code from re) " +
            " </when>" +
            " <when test='pm.assetCategoryIdSearch!=null'>" +
            " AND a.category_id IN (WITH RECURSIVE re (id, parent_id) AS (" +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_asset_category t where t.id IN " +
            "  <foreach collection='pm.assetCategoryIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " UNION ALL " +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_asset_category t2 , re e WHERE e.id = t2.parent_id" +
            " ) select id from re )" +
            " </when>" +
            " <when test='pm.workTypeIdSearch!=null'>" +
            " AND w.work_type_id IN " +
            "  <foreach collection='pm.workTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.statusSearch!=null'>" +
            " AND w.status  IN " +
            "  <foreach collection='pm.statusSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " a.asset_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR w.work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR wd.sub_work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR a.asset_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u2.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR il.level_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when>" +
            " <when test='pm.startCreateDateSearch!=null and pm.startCreateDateSearch!=\"\"'>" +
            " AND w.create_time &gt;=#{pm.startCreateDateSearch}::TIMESTAMP " +
            " </when>" +
            " <when test='pm.endCreateDateSearch!=null and pm.endCreateDateSearch!=\"\"'>" +
            " AND w.create_time &lt;=#{pm.endCreateDateSearch}::TIMESTAMP  " +
            " </when>" +
            " <when test='pm.beginTime!=null and pm.beginTime!=\"\" and pm.finishedTime!=\"\" and pm.finishedTime!=null'>" +
            " AND w.occur_time &gt;=#{pm.beginTime}::TIMESTAMP AND w.occur_time &lt;=#{pm.finishedTime}::TIMESTAMP  " +
            " </when>" +
            " <when test=\"pm.overtimeSearch!=null and '1'.toString() == pm.overtimeSearch\">" +
            " AND w.deadline_time is not null AND w.status &lt;" + StatusConstant.COMPLETED + " and w.status &gt; " + StatusConstant.DRAFT + " AND to_char(w.deadline_time, '" + Constants.DATE_FMT + "') &lt; to_char(now(),'" + Constants.DATE_FMT + "')" +
            " </when>" +
            " <when test=\"pm.sectionType!=null and 'reportToday' == pm.sectionType\">" +//今日上报  只查询今日生成的维修工单
            " AND w.create_time &gt;= current_date AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1001 + "::int " +
            " </when>" +
            " <when test=\"pm.sectionType!=null and 'completedToday' == pm.sectionType\">" +//今日完成
            " AND TO_CHAR( wd.finished_time, 'YYYY-MM-DD' ) =  TO_CHAR( now(), 'YYYY-MM-DD' )  " +
            " </when>" +
            " <when test=\"pm.sectionType!=null and 'timeoutTask' == pm.sectionType\">" +//超时任务
            " AND w.deadline_time is not null AND w.status &lt;" + StatusConstant.COMPLETED + " and w.status &gt; " + StatusConstant.DRAFT + " AND to_char(w.deadline_time, '" + Constants.DATE_FMT + "') &lt; to_char(now(),'" + Constants.DATE_FMT + "')" +
            " </when>" +
            " ORDER BY w.create_time DESC ${pm.pagination} " +
            " </script> ")
    List<Map<String, Object>> findWorksList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm, @Param("user_id") String user_id);

    /**
     * 根据工单概要表主键获取详情表信息列表
     *
     * @param schemaName 入参
     * @param work_code  入参
     * @return 详情表信息列表
     */
    @Select(" SELECT wd.sub_work_code,wd.work_code,wd.work_type_id,wd.work_template_code,wd.relation_type,wd.relation_id,wd.title,wd.problem_note,wd.problem_img," +
            " wd.running_status_id,wd.fault_type_id,wd.repair_type_id,wd.priority_level,wd.before_img,wd.result_note,wd.after_img,wd.file_ids,wd.status,wd.remark," +
            " wd.comment_list,wd.from_code,wd.waiting,wd.bom_app_result,wd.receive_time,wd.distribute_time,wd.begin_time,wd.finished_time,wd.audit_time," +
            " wd.fault_number,wd.is_main,wd.plan_arrive_time,wd.service_score,wd.repair_code,wd.is_finished,wd.finished_id,wd.is_redo,wd.total_deal_time," +
            " wd.distribute_user_id,wd.receive_user_id" +
            " FROM ${schema_name}._sc_works_detail AS wd" +
            " WHERE wd.work_code = #{work_code}")
    List<Map<String, Object>> findWorksDetailListByWorkCode(@Param("schema_name") String schemaName, @Param("work_code") String work_code);

    @Select(" <script>" +
            " SELECT" +
            " ( CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END ) AS position_code," +
            " ap.position_code,w.work_code,w.work_type_id,w.relation_type,w.relation_id,w.title,a.asset_code,a.asset_name,a.category_id,w.status," +
            " w.create_user_id,wd.begin_time,u.user_name,wd.receive_user_id " +
            " FROM ${schema_name}._sc_works AS w" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code AND wd.is_main = 1" +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = A.id AND w.relation_type = 2" +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.relation_id = ap.position_code AND w.relation_type = 1" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.id " +
            " WHERE  w.work_code = #{work_code} " +
            //当前用户的工单类型数据权限
            " AND w.work_type_id IN (SELECT wt.id   " +
            " FROM  ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.id = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id} )" +
            //当前用户的设备位置数据权限
            " AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END " +
            " IN (SELECT ap.position_code " +
            " FROM  ${schema_name}._sc_asset_position AS ap" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id}) " +
            //当前用户的设备类型数据权限
            " AND CASE WHEN  w.relation_type = '2' THEN a.category_id IN  (SELECT ac.id " +
            " FROM  ${schema_name}._sc_asset_category AS ac" +
            " INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.id = rat.category_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id})  ELSE 1=1 END " +
            " </script> ")
    Map<String, Object> checkUserWorksPermission(@Param("schema_name") String schemaName,
                                                 @Param("work_code") String work_code,
                                                 @Param("user_id") String user_id);

    /**
     * 根据表名 主键 主键值查询
     *
     * @param schemaName  入参
     * @param table_name  入参
     * @param table_key   入参
     * @param table_value 入参
     * @return 数据
     */
    @Select("SELECT * FROM ${schema_name}.${table_name} WHERE  ${table_key} = #{table_value} ")
    Map<String, Object> query(@Param("schema_name") String schemaName,
                              @Param("table_name") String table_name,
                              @Param("table_key") String table_key,
                              @Param("table_value") String table_value);

    /**
     * 更新子工单（未完成数据）
     *
     * @param schemaName      入参
     * @param receive_user_id 入参
     * @param work_code       入参
     */
    @Update("UPDATE ${schema_name}._sc_works_detail SET receive_user_id =#{receive_user_id},duty_user_id =#{receive_user_id} WHERE work_code = #{work_code} and status < 50 ")
    void updateWaitWorkDetailByWorkCode(@Param("schema_name") String schemaName, @Param("receive_user_id") String receive_user_id, @Param("work_code") String work_code);

    /**
     * 今日上报数
     *
     * @param schemaName 入参
     * @param user_id    入参
     * @return 今日上报数
     */
    @Select(" SELECT COUNT( w.work_code ) " +
            " FROM ${schema_name}._sc_works AS w" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code AND wd.is_main = 1" +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = A.id AND w.relation_type = 2" +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.relation_id = ap.position_code AND w.relation_type = 1" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id" +
            " LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id =a.category_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id= wd.duty_user_id " +
            " WHERE " +
            //当前用户的工单类型数据权限
            "  w.work_type_id IN (SELECT wt.id   " +
            " FROM  ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.id = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id} )" +
            //当前用户的设备位置数据权限
            " AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END " +
            " IN (SELECT ap.position_code " +
            " FROM  ${schema_name}._sc_asset_position AS ap" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id}) " +
            //当前用户的设备类型数据权限
            " AND CASE WHEN  w.relation_type = '2' THEN a.category_id IN  (SELECT ac.id " +
            " FROM  ${schema_name}._sc_asset_category AS ac" +
            " INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.id = rat.category_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id})  ELSE 1=1 END " +
            " AND w.create_time >= CURRENT_DATE " +
            " AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1001 + "::int ")
    int todayReportWorksCount(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    /**
     * 今日完成数
     *
     * @param schemaName 入参
     * @param user_id    入参
     * @return 今日完成数
     */
//    @Select(" SELECT COUNT( w.work_code ) " +
//            " FROM ${schema_name}._sc_works w " +
//            " LEFT JOIN   ${schema_name}._sc_works_detail wd ON w.work_code =wd.work_code AND wd.is_main = 1" +
//            " WHERE wd.finished_time >= CURRENT_DATE " +
//            " AND w.status=" + StatusConstant.COMPLETED)
    @Select(" SELECT COUNT( w.work_code ) " +
            " FROM ${schema_name}._sc_works AS w" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code AND wd.is_main = 1" +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = A.id AND w.relation_type = 2" +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.relation_id = ap.position_code AND w.relation_type = 1" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id" +
            " LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id =a.category_id " +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id= wd.duty_user_id " +
            " WHERE " +
            //当前用户的工单类型数据权限
            "  w.work_type_id IN (SELECT wt.id   " +
            " FROM  ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.id = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id} )" +
            //当前用户的设备位置数据权限
            " AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN a.position_code ELSE'' END " +
            " IN (SELECT ap.position_code " +
            " FROM  ${schema_name}._sc_asset_position AS ap" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id}) " +
            //当前用户的设备类型数据权限
            " AND CASE WHEN  w.relation_type = '2' THEN a.category_id IN  (SELECT ac.id " +
            " FROM  ${schema_name}._sc_asset_category AS ac" +
            " INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.id = rat.category_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id})  ELSE 1=1 END " +
            " AND wd.finished_time >= CURRENT_DATE " +
            " AND w.status=" + StatusConstant.COMPLETED)
    int todayFinishedWorksCount(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    /**
     * 根据工单详情编号删除子页面数据
     *
     * @param schemaName    入参
     * @param table_name    入参
     * @param sub_work_code 入参
     */
    @Delete("DELETE FROM ${schema_name}.${table_name} WHERE sub_work_code  = #{sub_work_code}")
    void deleteSubTableBySubWorkCode(@Param("schema_name") String schemaName, @Param("table_name") String table_name, @Param("sub_work_code") String sub_work_code);

    @Delete("DELETE FROM ${schema_name}.${table_name} WHERE ${file_name}  = #{code}")
    void deleteWorkTable(@Param("schema_name") String schemaName, @Param("table_name") String table_name, @Param("file_name") String fileName, @Param("code") String work_code);

    /**
     * 根据工单详情编号和子页面父类删除子页面字段
     *
     * @param schemaName    入参
     * @param parent_id     入参
     * @param sub_work_code 入参
     */
    @Delete("DELETE FROM ${schema_name}._sc_works_detail_column WHERE sub_work_code  = #{sub_work_code} AND parent_id  = #{parent_id}")
    void deleteSubWorksDetailColumnBySubWorkCode(@Param("schema_name") String schemaName, @Param("parent_id") Integer parent_id, @Param("sub_work_code") String sub_work_code);

    @Delete("DELETE FROM ${schema_name}._sc_works_detail_column WHERE sub_work_code  = #{sub_work_code} AND field_form_code  = #{field_form_code}")
    void deleteWorksDetailColumnBySubWorkCodeAndCode(@Param("schema_name") String schemaName, @Param("field_form_code") String field_form_code, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据工单详情编号和子页面父类删除子页面字段
     *
     * @param schemaName    入参
     * @param parent_id     入参
     * @param sub_work_code 入参
     */
    @Update("UPDATE ${schema_name}._sc_works_detail_column SET field_value=#{field_value}   WHERE sub_work_code  = #{sub_work_code} AND parent_id  = #{parent_id}")
    void updateSubWorksDetailColumnBySubWorkCode(@Param("schema_name") String schemaName, @Param("parent_id") Integer parent_id, @Param("sub_work_code") String sub_work_code, @Param("field_value") String field_value);

    /**
     * 根据工单详情编号和字段编码更新工单详情字段
     */
    @Select(" SELECT * FROM ${schema_name}._sc_works_detail_column " +
            " WHERE field_code = #{field_code} AND sub_work_code = #{sub_work_code} limit 1")
    Map<String, Object> findWorksDetailColumnBySubWorkCodeAndFieldCode(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code, @Param("field_code") String field_code);

    /**
     * 派案选人
     *
     * @param schemaName    数据库
     * @param role_id       角色
     * @param position_code 设备位置
     * @param work_type_id  工单类型
     * @param stock_id      库房
     * @param category_id   设备类型
     * @return 人员列表
     */
    @Select(" <script>" +
            "SELECT distinct up.user_id FROM ${schema_name}._sc_user u " +
            "INNER JOIN ${schema_name}._sc_user_position up on u.id = up.user_id " +
            "INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "INNER JOIN ( " +
            "    SELECT distinct up.user_id FROM ${schema_name}._sc_user u " +
            "    INNER JOIN ${schema_name}._sc_user_position up on u.id = up.user_id " +
            "    INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "    INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "    where ur.id = #{role_id} " +
            ") ru on up.user_id = ru.user_id " +
            "LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "  <if test = 'null != position_code'> " +
            "LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "  </if> " +
            "  <if test = 'null != category_id'> " +
            "LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "  </if> " +
            "  <if test = 'null != stock_id'> " +
            "LEFT JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "  </if> " +
            "WHERE u.status = 1 AND u.is_use = '1' and wt.work_type_id = #{work_type_id} " +
            "  <if test = 'null != position_code'> " +
            "   and p.asset_position_code = #{position_code} " +
            "  </if> " +
            "  <if test = 'null != category_id'> " +
            "   and at.category_id = #{category_id} " +
            "  </if> " +
            "  <if test = 'null != stock_id'> " +
            "   and s.stock_id = #{stock_id} " +
            "  </if> " +
            " </script>")
    List<String> findUserIdListByPermission(@Param("schema_name") String schemaName,
                                            @Param("role_id") String role_id,
                                            @Param("position_code") String position_code,
                                            @Param("work_type_id") Integer work_type_id,
                                            @Param("stock_id") Integer stock_id,
                                            @Param("category_id") Integer category_id);

    /**
     * 获取工单使用备件列表
     *
     * @param schemaName    入参
     * @param sub_work_code 入参
     * @return 工单使用备件列表
     */
    @Select(" SELECT wb.id,wb.sub_work_code,wb.bom_model,wb.bom_code,wb.stock_code,wb.facility_id,wb.goods_shelves," +
            " wb.fetch_man,wb.use_count,wb.material_code,wb.is_from_service_supplier,wb.price,wb.bom_source_type," +
            " wb.currency_id,wb.bom_name,wb.bom_id,wb.stock_id,wd.work_type_id,wt.type_name,wt.type_name AS work_type_name,wd.work_code " +
            " FROM ${schema_name}._sc_work_bom AS wb " +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON wb.sub_work_code = wd.sub_work_code" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt on wd.work_type_id = wt.id  " +
            " WHERE wb.sub_work_code = #{sub_work_code} ")
    List<Map<String, Object>> findWorkBomList(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据工单类型和设备id查询是否有未完成的工单
     */
    @Select(" <script> SELECT * FROM ${schema_name}._sc_works " +
            " WHERE relation_type = 2 AND status &lt;&gt; " + StatusConstant.COMPLETED + "::int  AND work_type_id = #{work_type_id}::int " +
            " AND relation_id = #{relation_id} LIMIT 1" +
            "  </script>")
    Map<String, Object> noEndWorksByWorkTypeANdAssetId(@Param("schema_name") String schemaName,
                                                       @Param("work_type_id") Integer work_type_id,
                                                       @Param("relation_id") String relation_id);

    @Update(" update  ${schema_name}._sc_works_detail set begin_time = null where  sub_work_code = #{sub_work_code} ")
    void updateBeginTimeIsNull(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code);

//    /**
//     * 查询字典数据以及国际化列表
//     *
//     * @param schemaName
//     * @param company_id
//     * @param userLang
//     * @return
//     */
//    @Select("SELECT COALESCE(( SELECT ( C.resource ->> c1.NAME ) :: jsonb ->> #{userLang} FROM PUBLIC.company_resource C WHERE C.company_id = #{company_id} ),c1.NAME ) AS type_name, c1.code " +
//            "FROM ${schema_name}._sc_cloud_data c1 WHERE c1.data_type = #{data_type} ")
//    List<Map<String, Object>> queryCloudDataList(@Param("schema_name") String schemaName, @Param("company_id") Long company_id, @Param("userLang") String userLang, @Param("data_type") String dataType);

    @Select("SELECT c1.NAME AS type_name, c1.code " +
            "FROM ${schema_name}._sc_cloud_data c1 WHERE c1.data_type = #{data_type} ")
    List<Map<String, Object>> queryCloudDataList(@Param("schema_name") String schemaName, @Param("data_type") String dataType);

    /**
     * 根据工单对象查下有效工单信息
     *
     * @param schema_name      数据库
     * @param relation_id      工单对象
     * @param business_type_id 业务类型
     * @return 有效工单信息
     */
    @Select("select string_agg(work_code||'' , ',') as work_codes from ${schema_name}._sc_works_detail as wd " +
            "INNER JOIN ${schema_name}._sc_work_type wt on wd.work_type_id = wt.id " +
            "where wd.status > " + StatusConstant.DRAFT + " and wd.status < " + StatusConstant.COMPLETED + " and wd.relation_id= #{relation_id} and wt.business_type_id = #{business_type_id}::int ")
    String selectWorkByRelationId(@Param("schema_name") String schema_name, @Param("relation_id") String relation_id, @Param("business_type_id") String business_type_id);
}