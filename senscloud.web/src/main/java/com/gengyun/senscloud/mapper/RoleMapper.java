package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.response.*;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoleMapper {

    /**
     * 根据角色id获取角色
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 角色
     */
    @Select("SELECT * FROM  ${schema_name}._sc_role WHERE id = #{roleId}")
    Map<String, Object> findByRoleId(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 新增角色
     *
     * @param schema_name 入参
     * @param params      入参
     */
    @Insert(" INSERT INTO ${schema_name}._sc_role(id,name, create_time, description, sys_role, is_free, create_user_id,data_order )" +
            " VALUES (#{pm.id},#{pm.name}, #{pm.create_time}, #{pm.description},'3', false, #{pm.create_user_id},#{pm.data_order})")
    void insert(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> params);

    /**
     * 删除角色
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role  WHERE id = #{id}")
    void deleteById(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 删除角色菜单权限关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role_permission  WHERE role_id = #{roleId}")
    void deleteRolePermission(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 删除角色设备位置关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role_asset_position  WHERE role_id = #{roleId}")
    void deleteRoleAssetPosition(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 删除角色库存关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role_stock  WHERE role_id = #{roleId}")
    void deleteRoleStockPosition(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 删除角色设备类型关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role_asset_type  WHERE role_id = #{roleId}")
    void deleteRoleAssetCategory(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 删除角色工单类型关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role_work_type  WHERE role_id = #{roleId}")
    void deleteRoleWorkType(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 删除角色创建工单关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_role_data_permission WHERE data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND role_id = #{roleId}")
    void deleteRoleNewWork(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 删除岗位角色关联信息
     *
     * @param schema_name 入参
     * @param roleId      入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_position_role  WHERE role_id = #{roleId}")
    void deletePositionRole(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 获取工单类型角色列表
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 工单类型角色列表
     */
    @Select(" SELECT t1.id,t1.type_name,t1.data_order,t1.is_use,t1.create_time,t1.create_user_id,u.account as create_user_account," +
            " t1.business_type_id," +
            " CASE WHEN t2.role_id IS NULL THEN '2' ELSE'1' END AS isSelect " +
            " FROM ${schema_name}._sc_work_type AS t1 " +
            " LEFT JOIN ${schema_name}._sc_user u ON u.id = t1.create_user_id " +
            " LEFT JOIN ${schema_name}._sc_role_work_type AS t2 ON t1.id = t2.work_type_id AND t2.role_id = #{roleId}" +
            " WHERE t1.is_use = '1' ")
    List<WorkTypeRoleResult> findWorkTypeRoleList(@Param("schema_name") String schema_name,
                                                  @Param("roleId") String roleId);

    /**
     * 获取创建工单类型角色列表
     *
     * @param schema_name 数据库
     * @param roleId      角色主键
     * @return 创建工单类型角色列表
     */
    @Select(" SELECT t1.id,t1.type_name,CASE WHEN t2.role_id IS NULL THEN 2 ELSE 1 END AS \"isSelect\" " +
            " FROM ${schema_name}._sc_work_type AS t1 " +
            " LEFT JOIN ${schema_name}._sc_role_data_permission AS t2 ON t2.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND t1.id = t2.data_id AND t2.role_id = #{roleId}" +
            " WHERE t1.is_use = '1' ")
    List<Map<String, Object>> findNewWorkTypeRoleList(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 获取角色库房列表
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 角色库房列表
     */
    @Select(" SELECT t1.id AS stock_id,t1.stock_code,t1.stock_name,CASE WHEN t2.role_id IS NULL THEN '2' ELSE'1' END AS isSelect" +
            " FROM ${schema_name}._sc_stock AS t1 " +
            " LEFT JOIN ${schema_name}._sc_role_stock AS t2 ON t1.id = t2.stock_id " +
            " AND t2.role_id = #{roleId}" +
            " WHERE t1.status = 1 ")
    List<StockRoleResult> findStockRoleList(@Param("schema_name") String schema_name,
                                            @Param("roleId") String roleId);

    /**
     * 查询非系统角色列表
     *
     * @param schema_name   入参
     * @param keywordSearch 入参
     * @return 非系统角色列表
     */
    @Select(" <script>" +
            " SELECT t1.name,t1.create_time,t1.id,t1.description,t1.sys_role,t1.is_free,t1.create_user_id" +
            " FROM ${schema_name}._sc_role AS t1 WHERE t1.sys_role = '3' " +
            " <when test='keywordSearch!=null'> " +
            " AND  t1.name  like CONCAT('%', #{keywordSearch}, '%')  " +
            " </when>  ORDER BY t1.data_order  ASC ,t1.name DESC " +
            " </script>")
    List<Map<String, Object>> findNoSysRoleList(@Param("schema_name") String schema_name,
                                                @Param("keywordSearch") String keywordSearch);

    /**
     * 查询系统角色列表
     *
     * @param schema_name   入参
     * @param keywordSearch 入参
     * @return 系统角色列表
     */
    @Select(" <script>" +
            " SELECT t1.name,t1.create_time,t1.id,t1.description,t1.sys_role,t1.is_free,t1.create_user_id" +
            " FROM ${schema_name}._sc_role AS t1 WHERE t1.sys_role = '2' " +
            " <when test='keywordSearch!=null'> " +
            " AND  t1.name  like CONCAT('%', #{keywordSearch}, '%')  " +
            " </when>  ORDER BY t1.data_order  ASC ,t1.name DESC " +
            " </script>")
    List<Map<String, Object>> findSysRoleList(@Param("schema_name") String schema_name, @Param("keywordSearch") String keywordSearch);

    /**
     * 查询角色详情
     *
     * @param schema_name 入参
     * @param role_id     入参
     * @return 角色详情
     */
    @Select(" SELECT t1.name,t1.create_time,t1.id,t1.description,t1.sys_role,t1.is_free,t1.create_user_id,u.account as create_user_account " +
            " FROM ${schema_name}._sc_role AS t1" +
            " LEFT JOIN ${schema_name}._sc_user u ON u.id = t1.create_user_id " +
            " WHERE t1.id = #{role_id} ")
    Map<String, Object> findRoleInfo(@Param("schema_name") String schema_name,
                                     @Param("role_id") String role_id);

    /**
     * 更新角色
     *
     * @param schema_name 入参
     * @param params      入参
     */
    @Update("UPDATE ${schema_name}._sc_role  SET name = #{pm.name}, description = #{pm.description}" +
            " WHERE id = #{pm.id}  ")
    void update(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> params);

    /**
     * 设备类型角色类型列表
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 类型角色类型列表
     */
    @Select(" SELECT t1.id,t1.category_name,t1.data_order,t1.is_use,t1.type,CASE WHEN t2.role_id IS NULL THEN '2' ELSE'1' END  AS  isSelect ," +
            " CASE WHEN t1.parent_id = 0 THEN  null ELSE t1.parent_id END  AS  parent_id" +
            " FROM ${schema_name}._sc_asset_category AS t1" +
            " LEFT JOIN ${schema_name}._sc_role_asset_type AS t2 ON t1.id = t2.category_id AND t2.role_id = #{roleId} ")
    List<AssetCategoryRoleResult> findAssetCategoryRoleList(@Param("schema_name") String schema_name,
                                                            @Param("roleId") String roleId);

    /**
     * 获取一级设备位置
     *
     * @param schema_name 入参
     * @param roleId      入参
     * @return 一级设备位置
     */
    @Select(" SELECT t1.position_code,t1.position_name,t1.position_type_id,t1.location, " +
            " t1.layer_path,t1.create_user_id,CASE WHEN t2.role_id IS NULL THEN '2' ELSE'1' END  AS  isSelect," +
            " CASE WHEN t1.parent_code ='0' THEN null ELSE t1.parent_code END  " +
            " FROM ${schema_name}._sc_asset_position AS t1" +
            " LEFT JOIN ${schema_name}._sc_role_asset_position AS t2 ON t1.position_code = t2.asset_position_code " +
            " AND t2.role_id = #{roleId} " +
            " WHERE t1.status = 1 ")
    List<AssetPositionRoleResult> findAssetPositionRoleList(@Param("schema_name") String schema_name,
                                                            @Param("roleId") String roleId);

//    /**
//     * 根据企业号和角色获取功能权限列表
//     *
//     * @param schema_name 入参
//     * @param company_id  入参
//     * @param type        入参
//     * @param roleId      入参
//     * @return 功能权限列表
//     */
//    @Select(" <script>" +
//            " select t1.id,t1.menu_name,t1.key,t1.type,t1.parent_id,t1.key,t1.url,t1.icon," +
//            " CASE WHEN t2.role_id IS NULL THEN '2' ELSE'1' END  AS  isSelect" +
//            " from public.system_permission t1 " +
//            " INNER JOIN public.company_menu AS c ON t1.id = c.menu_id AND c.company_id = #{company_id} " +
//            " LEFT JOIN ${schema_name}._sc_role_permission AS t2 ON t1.id = t2.permission_id AND t2.role_id = #{roleId}" +
//            " where  t1.available = 't' AND t1.id in (select menu_id from company_menu where company_id = #{company_id}) " +
//            " <when test='type!=null'>" +
//            " and t1.type=#{type} " +
//            " </when>" +
//            " order by t1.index" +
//            " </script>")
//    List<PermissionRoleResult> findByCompanyIdAndRoleId(@Param("schema_name") String schema_name,
//                                                        @Param("company_id") Long company_id,
//                                                        @Param("type") Integer type, @Param("roleId") String roleId);

    @Select(" select t2.permission_id,t2.role_id from ${schema_name}._sc_role_permission AS t2 where t2.role_id = #{roleId}")
    List<Map<String,Object>> findByCompanyIdAndRoleId(@Param("schema_name") String schema_name, @Param("roleId") String roleId);

    /**
     * 克隆角色
     *
     * @param schema_name 入参
     * @param newRoleId   入参
     * @param oldRoleId   入参
     */
//    @Select(" INSERT INTO ${schema_name}._sc_role( name, create_time, id, description, sys_role, is_free, " +
//            " create_user_id,data_order)" +
//            " SELECT CONCAT(name,'_clone') AS name ,create_time, #{newRoleId}, description, sys_role, is_free," +
//            " create_user_id,data_order " +
//            " FROM ${schema_name}._sc_role WHERE id = #{oldRoleId} ;" +
//            " INSERT INTO ${schema_name}._sc_role_permission ( role_id, permission_id )" +
//            " SELECT #{newRoleId},permission_id" +
//            " FROM ${schema_name}._sc_role_permission WHERE role_id = #{oldRoleId} ; " +
//            " INSERT INTO ${schema_name}._sc_role_asset_position ( role_id, asset_position_code ) " +
//            " SELECT #{newRoleId},asset_position_code " +
//            " FROM ${schema_name}._sc_role_asset_position WHERE role_id = #{oldRoleId} ;" +
//            " INSERT INTO ${schema_name}._sc_role_asset_type  ( role_id, category_id ) " +
//            " SELECT #{newRoleId},category_id" +
//            " FROM ${schema_name}._sc_role_asset_type WHERE role_id = #{oldRoleId} ;" +
//            " INSERT INTO ${schema_name}._sc_role_work_type ( role_id, work_type_id ) " +
//            " SELECT #{newRoleId},work_type_id" +
//            " FROM ${schema_name}._sc_role_work_type WHERE role_id = #{oldRoleId};" +
//            " INSERT INTO ${schema_name}._sc_role_stock ( role_id, stock_id ) " +
//            " SELECT #{newRoleId},stock_id" +
//            " FROM ${schema_name}._sc_role_stock WHERE role_id = #{oldRoleId}")
    @Select(" INSERT INTO ${schema_name}._sc_role( name, create_time, id, description, sys_role, is_free, " +
            " create_user_id,data_order)" +
            " SELECT CONCAT(name,'_clone') AS name ,create_time, #{newRoleId}, description, sys_role, is_free," +
            " create_user_id,data_order " +
            " FROM ${schema_name}._sc_role WHERE id = #{oldRoleId} ")
    void cloneRole(@Param("schema_name") String schema_name,
                   @Param("newRoleId") String newRoleId, @Param("oldRoleId") String oldRoleId);

    @Select(" INSERT INTO ${schema_name}._sc_role_stock ( role_id, stock_id ) " +
            " SELECT #{newRoleId},stock_id" +
            " FROM ${schema_name}._sc_role_stock WHERE role_id = #{oldRoleId}")
    void cloneRoleStock(@Param("schema_name") String schema_name,
                        @Param("newRoleId") String newRoleId, @Param("oldRoleId") String oldRoleId);

    @Select(" INSERT INTO ${schema_name}._sc_role_work_type ( role_id, work_type_id ) " +
            " SELECT #{newRoleId},work_type_id" +
            " FROM ${schema_name}._sc_role_work_type WHERE role_id = #{oldRoleId};")
    void cloneRoleWorkType(@Param("schema_name") String schema_name,
                           @Param("newRoleId") String newRoleId, @Param("oldRoleId") String oldRoleId);

    @Select(" INSERT INTO ${schema_name}._sc_role_asset_type  ( role_id, category_id ) " +
            " SELECT #{newRoleId},category_id" +
            " FROM ${schema_name}._sc_role_asset_type WHERE role_id = #{oldRoleId} ;")
    void cloneRoleAssetType(@Param("schema_name") String schema_name,
                            @Param("newRoleId") String newRoleId, @Param("oldRoleId") String oldRoleId);

    @Select(" INSERT INTO ${schema_name}._sc_role_asset_position ( role_id, asset_position_code ) " +
            " SELECT #{newRoleId},asset_position_code " +
            " FROM ${schema_name}._sc_role_asset_position WHERE role_id = #{oldRoleId} ;")
    void cloneRoleAssetPosition(@Param("schema_name") String schema_name,
                                @Param("newRoleId") String newRoleId, @Param("oldRoleId") String oldRoleId);

    @Select(" INSERT INTO ${schema_name}._sc_role_permission ( role_id, permission_id )" +
            " SELECT #{newRoleId},permission_id" +
            " FROM ${schema_name}._sc_role_permission WHERE role_id = #{oldRoleId} ; ")
    void cloneRolePermission(@Param("schema_name") String schema_name,
                             @Param("newRoleId") String newRoleId, @Param("oldRoleId") String oldRoleId);

//    /**
//     * 根据菜单id获取菜单名称列表
//     *
//     * @param schema_name 入参
//     * @param ids         入参
//     * @return 菜单名称列表
//     */
//    @Select(" <script> SELECT DISTINCT sp.key FROM public.system_permission AS sp " +
//            " WHERE  sp.id IN " +
//            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach>" +
//            " </script>  ")
//    List<String> findPermissionNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

//    /**
//     * 根据角色是id获取菜单名称列表
//     *
//     * @param schema_name 入参
//     * @param id          入参
//     * @return 菜单名称列表
//     */
//    @Select(" SELECT DISTINCT sp.key FROM ${schema_name}._sc_role_permission AS rp" +
//            " INNER JOIN public.system_permission AS sp ON rp.permission_id = sp.id " +
//            " WHERE rp.role_id = #{id} ")
//    List<String> findPermissionNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    @Select(" SELECT DISTINCT rp.permission_id FROM ${schema_name}._sc_role_permission AS rp" +
            " WHERE rp.role_id = #{id} ")
    List<String> findPermissionNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据设备位置id设备位置名称列表
     *
     * @param schema_name 入参
     * @param ids         入参
     * @return 设备位置名称列表
     */
    @Select(" <script> SELECT t.position_name FROM ${schema_name}._sc_asset_position AS t " +
            " WHERE  t.position_code IN " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach>" +
            " </script>  ")
    List<String> findAssetPositonNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 根据库房code获取名称列表
     *
     * @param schema_name 入参
     * @param ids         入参
     * @return 设备位置名称列表
     */
    @Select(" <script> SELECT t.stock_name FROM ${schema_name}._sc_stock AS t " +
            " WHERE  t.id IN " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </script>  ")
    List<String> findStockNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 根据角色是id获取设备位置名称列表
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 设备位置名称列表
     */
    @Select(" SELECT ap.position_name FROM ${schema_name}._sc_role_asset_position AS rap" +
            " INNER JOIN ${schema_name}._sc_asset_position AS ap ON rap.asset_position_code = ap.position_code " +
            " WHERE rap.role_id = #{id} ")
    List<String> findAssetPositonNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据角色是id获取设备位置名称列表
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 设备位置名称列表
     */
    @Select(" SELECT s.stock_name FROM ${schema_name}._sc_role_stock AS rs" +
            " INNER JOIN ${schema_name}._sc_stock AS s ON rs.stock_id = s.id " +
            " WHERE rs.role_id = #{id} ")
    List<String> findStockNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据设备类型id设备类型名称列表
     *
     * @param schema_name 入参
     * @param ids         入参
     * @return 设备类型名称列表
     */
    @Select(" <script> SELECT t.category_name FROM ${schema_name}._sc_asset_category AS t " +
            " WHERE  t.id IN " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </script>  ")
    List<String> findAssetCategoryNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 根据角色是id获取设备类型名称列表
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 获取设备类型名称列表
     */
    @Select(" SELECT ac.category_name FROM ${schema_name}._sc_role_asset_type AS rac" +
            " INNER JOIN ${schema_name}._sc_asset_category AS ac ON rac.category_id = ac.id " +
            " WHERE rac.role_id = #{id} ")
    List<String> findAssetCategoryNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据工单类型id设备类型名称列表
     *
     * @param schema_name 入参
     * @param ids         入参
     * @return 设备类型名称列表
     */
    @Select(" <script> SELECT t.type_name FROM ${schema_name}._sc_work_type AS t " +
            " WHERE  t.id IN " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </script>  ")
    List<String> findWorkTypeNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 根据角色是id获取工单类型名称列表
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 工单类型名称列表
     */
    @Select(" SELECT wt.type_name FROM ${schema_name}._sc_role_work_type AS rwt" +
            " INNER JOIN ${schema_name}._sc_work_type AS wt ON rwt.work_type_id = wt.id " +
            " WHERE rwt.role_id = #{id} ")
    List<String> findWorkTypeNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据角色是id获取创建工单列表
     *
     * @param schema_name 数据库
     * @param id          角色id
     * @return 创建工单列表
     */
    @Select(" SELECT t1.type_name FROM ${schema_name}._sc_work_type AS t1 " +
            " INNER JOIN ${schema_name}._sc_role_data_permission AS t2 ON t2.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND t1.id = t2.data_id AND t2.role_id = #{id} ")
    List<String> findNewWorkNamesByRoleId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据角色id列表获取角色名称列表
     *
     * @param schema_name 入参
     * @param roleIds     入参
     * @return 获取角色名称列表
     */
    @Select(" <script> SELECT t.name FROM ${schema_name}._sc_role AS t " +
            " WHERE  t.id IN " +
            "  <foreach collection='roleIds' item='id' open='(' close=')' separator=','> #{id} </foreach>" +
            " </script>  ")
    List<String> findRoleNamesByRoleIds(@Param("schema_name") String schema_name, @Param("roleIds") List<String> roleIds);

    /**
     * 获取当前最大序号数
     *
     * @param schema_name 入参
     * @return 最大序号数
     */
    @Select("SELECT CASE WHEN MAX(data_order) IS NULL THEN 0 ELSE MAX(data_order) END  FROM ${schema_name}._sc_role")
    int findMaxOrder(@Param("schema_name") String schema_name);
}