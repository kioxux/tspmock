package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.model.MethodParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface GuacamoleWebMapper {

    @Select("SELECT facility_id::varchar,hostname,username,password,protocol,port " +
            "FROM ${schema_name}._sc_facility_guacamole_config WHERE facility_id = #{facility_id}::int ")
    Map<String, String> findFacilityGuacamole(@Param("schema_name") String schemaName,@Param("facility_id") String facility_id);

    @Select("SELECT code,name,is_lang " +
            "FROM ${schema_name}._sc_cloud_data cd " +
            "INNER JOIN ${schema_name}._sc_asset_position ap ON ap.scada_config = cd.parent_code " +
            "WHERE data_type = #{data_type} AND ap.position_code = #{position_code} " +
            "ORDER BY data_order ")
    List<Map<String, String>> findAssetPositionGuacamoleList(@Param("schema_name") String schemaName, @Param("data_type") String scada_config,@Param("position_code")  String position_code);
}
