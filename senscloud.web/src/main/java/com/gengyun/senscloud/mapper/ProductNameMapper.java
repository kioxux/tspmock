package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 4:00 2019/10/25 0025
 */
@Mapper
public interface ProductNameMapper {
//
//    /**
//     * @Description:查询产品名称列表
//     * @param schema_name
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_asset_use_product where 1=1 ${condition} order by id desc limit ${page} offset ${begin}   ")
//    List<Map<String , Object>> query(@Param("schema_name")String schema_name,@Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);// , @Param("page") int pageSize, @Param("begin") int begin
//
//    @Select("select count(1) from ${schema_name}._sc_asset_use_product where 1=1 ${condition}")
//    int queryList(@Param("schema_name")String schema_name,@Param("condition") String condition);
//    /**
//     * @Description：查询产品名称数量
//     * @param schema_name
//     * @return
//     */
//    @Select("Select count(1) from ${schema_name}._sc_asset_use_product")
//    int queryCount(@Param("schema_name")String schema_name);
//
//    /**
//     * @Description: 新增产品名称
//     * @param schema_name
//     * @param product_name
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_asset_use_product (product_name) values (#{product_name})")
//    int add(@Param("schema_name") String schema_name ,@Param("product_name") String product_name);
//
//    /**
//     * @Description：编辑产品名称
//     * @param schema_name
//     * @param id
//     * @param product_name
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_use_product set product_name=#{product_name} where id =#{id}::int")
//    int edit(@Param("schema_name") String schema_name ,@Param("id") int id, @Param("product_name") String product_name);
//
//    /**
//     * @Description:删除产品名称
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Delete("delete from  ${schema_name}._sc_asset_use_product where id=#{id}::int")
//    int dele(@Param("schema_name") String schema_name ,@Param("id") int id);

}
