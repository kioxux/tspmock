package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RolePermissionMapper {
//    @Insert("delete from ${schema_name}._sc_role_permission where role_id=#{role_id}")
//    void deleteRolePermission(@Param("schema_name") String schema_name,
//                              @Param("role_id") String role_id);
//
//    @Insert("insert into ${schema_name}._sc_role_permission(role_id,permission_id)" +
//            " values(#{role_id},#{permission_id})")
//    void insertRolePermission(@Param("schema_name") String schema_name,
//                              @Param("role_id") String role_id,
//                              @Param("permission_id") String permission_id);
//
//    @Select("SELECT distinct t1.name as roleName, t1.id as roleId,t3.id as permissionId,t3.name as permissionName," +
//            "t3.key as permissionKey,t3.url as permission_url " +
//            "FROM ${schema_name}._sc_role t1,${schema_name}._sc_role_permission t2,public.system_permission t3,public.company_menu t4 " +
//            "where (t1.id in (${role_id}) and t2.role_id=t1.id and t2.permission_id=t3.id) and t4.company_id=#{company_id} and t4.menu_id=t3.id")
//    List<Map<String, Object>> getRolePermissions(@Param("schema_name") String schema_name, @Param("company_id") long company_id,
//                                                 @Param("role_id") String role_id);
//
//    @Select("SELECT t2.permission_id " +
//            "FROM ${schema_name}._sc_role t1,${schema_name}._sc_role_permission t2,public.system_permission t3,public.company_menu t4 " +
//            "where (t1.id=#{role_id} and t2.role_id=t1.id and t2.permission_id=t3.id) and t4.company_id=#{company_id} and t4.menu_id=t3.id")
//    List<String> getRolePermissionIdList(@Param("schema_name") String schema_name, @Param("company_id") long company_id,
//                                         @Param("role_id") String role_id);
}
