package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 设备监控异常报警sql
 * Created by DwD on 2018/8/6.
 */
@Mapper
public interface AlarmAnomalySendInfoMapper {
//
//    @Insert("insert into ${schema_name}._sc_asset_alarm_data ( alarm_code,asset_code,monitor_name," +
//            "monitor_value_type,monitor_value_unit,monitor_value," +
//            "gather_time,alarm_up,alarm_down,exception_up,exception_down," +
//            "alarm_msg,exception_msg,alarm_up_task_code," +
//            "alarm_down_task_code,sms_times,status,create_time,send_sms_time) " +
//            "values( #{alarm.alarm_code},#{alarm.asset_code},#{alarm.monitor_name},#{alarm.monitor_value_type}," +
//            "#{alarm.monitor_value_unit},#{alarm.monitor_value},#{alarm.gather_time},#{alarm.alarm_up},#{alarm.alarm_down}," +
//            "#{alarm.exception_up},#{alarm.exception_down},#{alarm.alarm_msg},#{alarm.exception_msg}," +
//            "#{alarm.alarm_up_task_code},#{alarm.alarm_down_task_code},#{alarm.sms_times}," +
//            "#{alarm.status},#{alarm.create_time},#{alarm.create_time})")
//    public int addAlarmAnomalySendInfo(@Param("schema_name") String schema_name, @Param("alarm") AlarmAnomalySendInfoResult alarmAnomalySendInfo);
//
//    @Update("update ${schema_name}._sc_asset_alarm_data set sms_times=sms_times+1 and send_sms_time=LOCALTIMESTAMP where alarm_code=#{alarm_code} and monitor_name=#{monitor_name}")
//    public int updateAlarmAnomalySendInfoSum(@Param("schema_name") String schema_name, @Param("alarm_code") String alarm_code, @Param("monitor_name") String monitor_name);
//
//    @Update("update ${schema_name}._sc_asset_alarm_data set status=#{status} where alarm_code=#{alarm_code}")
//    public int updateStatusByAlarm_code(@Param("schema_name") String schema_name, @Param("alarm_code") String alarm_code, @Param("status") int status);
//
//    @Select("select distinct s.alarm_code,s.asset_code,fc.code_name as monitor_name,s.monitor_value_type," +
//            "s.monitor_value_unit,s.monitor_value,s.gather_time,s.alarm_up,s.alarm_down," +
//            "s.exception_up,s.exception_down,s.alarm_msg,s.exception_msg,s.send_task," +
//            "s.alarm_up_task_code,s.alarm_down_task_code,s.sms_times,s.status,s.create_time," +
//            "m.strname as strname, m.intstatus as intstatus,t.monitor_name as monitor_name_new,ast.status as asset_status " +
//            "from ${schema_name}._sc_asset_alarm_data as s " +
//            "left join ${schema_name}._sc_asset as m on s.asset_code = m.strcode " +
//            "left join ${schema_name}._sc_metadata_monitors t on t.monitor_key=s.monitor_name and m.category_id=t.asset_category_id " +
//            "left join ${schema_name}._sc_asset_status ast on m.intstatus=ast.id " +
//            "left join ${schema_name}._sc_asset_category_fault f on f.category_id=m.category_id " +
//            "inner join ${schema_name}._sc_fault_code fc on fc.fault_code=s.monitor_value and f.fault_code=fc.code " +
//            "where 1=1 ${condition} " +
//            "order by gather_time desc")
//    public List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoDataList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//
//    @Select("select count(s.alarm_code) from ${schema_name}._sc_asset_alarm_data as s " +
//            "left join ${schema_name}._sc_asset as m on s.asset_code = m.strcode " +
//            "where 1=1 ${condition}")
//    public int getAlarmAnomalySendInfoDataListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    @Select("select alarm_code,asset_code,monitor_name,monitor_value_type," +
//            "monitor_value_unit,monitor_value,gather_time,alarm_up,alarm_down," +
//            "exception_up,exception_down,alarm_msg,exception_msg,send_task," +
//            "alarm_up_task_code,alarm_down_task_code,sms_times,status,create_time,m.strname as strname, m.intstatus as intstatus " +
//            "from ${schema_name}._sc_asset_alarm_data as s " +
//            "left join ${schema_name}._sc_asset as m on s.asset_code = m.strcode " +
//            "where s.alarm_code=#{alarm_code}")
//    public List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoDataListByAlarmCode(@Param("schema_name") String schema_name, @Param("alarm_code") String alarm_code);
//
//
//    @Select("select alarm_code,asset_code,monitor_name,monitor_value_type," +
//            "monitor_value_unit,monitor_value,gather_time,alarm_up,alarm_down," +
//            "exception_up,exception_down,alarm_msg,exception_msg,send_task," +
//            "alarm_up_task_code,alarm_down_task_code,sms_times,s.status,create_time,m.strname as strname, m.intstatus as intstatus,r.fault_note as fault_note " +
//            "from ${schema_name}._sc_asset_alarm_data as s " +
//            "left join ${schema_name}._sc_asset as m on s.asset_code = m.strcode " +
//            "left join ${schema_name}._sc_repair as r on s.asset_code = r.from_code " +
//            "where s.alarm_msg=1 and s.alarm_code=#{alarm_code} and s.monitor_name=#{monitor_name} " +
//            "order by create_time desc")
//    public List<AlarmAnomalySendInfoResult> getAlarmHistoryData(@Param("schema_name") String schema_name, @Param("alarm_code") String alarm_code, @Param("monitor_name") String monitorName);
//
//    @Select("select count(s.alarm_code) from ${schema_name}._sc_asset_alarm_data as s " +
//            "left join ${schema_name}._sc_asset as m on s.asset_code = m.strcode " +
//            "where s.alarm_msg=1 and s.alarm_code=#{alarm_code} and s.monitor_name = #{monitor_name}")
//    public int getAlarmHistoryDataCount(@Param("schema_name") String schema_name, @Param("alarm_code") String alarm_code, @Param("monitor_name") String monitorName);
//
//
//    @Select("select count(alarm_code) from ${schema_name}._sc_asset_alarm_data where  alarm_msg=1 and asset_code=#{asset_code} and monitor_name=#{monitor_name}")
//    public int isAlarmInfo(@Param("schema_name") String schema_name, @Param("asset_code") String asset_code, @Param("monitor_name") String monitor_name);
//
//    @Select("select alarm_code,asset_code,monitor_name,monitor_value_type," +
//            "monitor_value_unit,monitor_value,gather_time,alarm_up,alarm_down," +
//            "exception_up,exception_down,alarm_msg,exception_msg,send_task," +
//            "alarm_up_task_code,alarm_down_task_code,sms_times,status,create_time " +
//            "from ${schema_name}._sc_asset_alarm_data where asset_code=#{asset_code} and monitor_name=#{monitor_name} order by create_time desc")
//    public List<AlarmAnomalySendInfoResult> getAlarmAnomalySendInfoData(@Param("schema_name") String schema_name, @Param("asset_code") String asset_code, @Param("monitor_name") String monitor_name);
//
//    @Select("select alarm_code,asset_code,monitor_name,monitor_value_type," +
//            "monitor_value_unit,monitor_value,gather_time,alarm_up,alarm_down," +
//            "exception_up,exception_down，alarm_msg,exception_msg,send_task," +
//            "alarm_up_task_code,alarm_down_task_code,sms_times,status,create_time " +
//            "from ${schema_name}._sc_asset_alarm_data order by create_time desc limit 1")
//    public List<AlarmAnomalySendInfoResult> getExceptionDate(@Param("schema_name") String schema_name);
//
//    @Select("select s.alarm_code,s.asset_code,s.monitor_name,s.monitor_value_type," +
//            "s.monitor_value_unit,s.monitor_value,s.gather_time,s.alarm_up,s.alarm_down," +
//            "s.exception_up,s.exception_down,s.alarm_msg,s.exception_msg,s.send_task," +
//            "s.alarm_up_task_code,s.alarm_down_task_code,s.sms_times,s.status,s.create_time,m.strname as strname,s.send_sms_time " +
//            "from ${schema_name}._sc_asset_alarm_data as s " +
//            "left join ${schema_name}._sc_asset as m on s.asset_code = m.strcode " +
//            "where status=1 order  by create_time desc")
//    public List<AlarmAnomalySendInfoResult> getAlarmDateUnDeal(@Param("schema_name") String schema_name);
//
//
//    @Update("update ${schema_name}._sc_asset_alarm_data set sms_times=sms_times+1 where alarm_code = #{alarm_code}")
//    public int updateExceptionSendInfoNum(@Param("schema_name") String schema_name, @Param("alarm_code") String alarm_code);


}
