package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 设备型号管理
 */
@Mapper
public interface AssetModelMapper {
    /**
     * 根据设备型号主键获取型号相关文件主键
     *
     * @param schema_name 数据库
     * @param model_id    设备型号主键
     * @return 以逗号分隔的文件主键
     */
    @Select("select string_agg(t.file_id::VARCHAR, ',') as file_ids from ${schema_name}._sc_asset_model_docs t " +
            "where t.model_id=#{model_id}")
    String findAssetModelFileIdsById(@Param("schema_name") String schema_name, @Param("model_id") int model_id);

    /**
     * 新增型号
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("insert into ${schema_name}._sc_asset_model ( " +
            "properties,model_name,category_id,is_use,level_id,remark,create_time,create_user_id)  values  " +
            " (#{pm.properties}::jsonb,#{pm.model_name},#{pm.category_id}::int,#{pm.is_use}," +
            " #{pm.level_id}::int,#{pm.remark},current_timestamp,#{pm.create_user_id}) RETURNING id")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertAssetModel(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新设备型号
     *
     * @param schemaName 入参
     * @param paramMap   入参
     */
    @Update(" <script>" +
            " update ${schema_name}._sc_asset_model " +
            " set id=#{s.id}::int" +
            " <when test='s.model_name!=null'>" +
            " ,model_name=#{s.model_name} " +
            " </when>  " +
            " <when test='s.icon!=null'>" +
            " ,icon=#{s.icon} " +
            " </when>  " +
            " <when test='s.properties!=null'>" +
            " ,properties=#{s.properties}::jsonb " +
            " </when>  " +
            " <when test='s.category_id!=null'>" +
            " ,category_id=#{s.category_id}::int " +
            " </when>  " +
            " <when test='s.level_id!=null'>" +
            " ,level_id=#{s.level_id}::int " +
            " </when>  " +
            " <when test='s.remark!=null'>" +
            " ,remark=#{s.remark} " +
            " </when>  " +
            " <when test='s.file_id!=null'>" +
            " ,file_id=#{s.file_id} " +
            " </when>  " +
            " where id=#{s.id}::int " +
            " </script>")
    void updateAssetModel(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);

    /**
     * 删除设备型号
     *
     * @param schemaName 入参
     * @param ids        入参
     */
    @Update("DELETE FROM ${schema_name}._sc_asset_model where id in ( ${ids} ) ")
    void deleteAssetModel(@Param("schema_name") String schemaName, @Param("ids") String ids);

    /**
     * 根据型号id列表获取型号名称列表
     *
     * @param schemaName 入参
     * @param ids        入参
     * @return 型号名称列表
     */
    @Select("SELECT  model_name   FROM ${schema_name}._sc_asset_model where id in ( ${ids} ) ")
    List<String> findNamesByIds(@Param("schema_name") String schemaName, @Param("ids") String ids);

    /**
     * 根据型号id列表获取型号名称列表
     *
     * @param schemaName 入参
     * @param ids        入参
     * @return 型号名称列表
     */
    @Select("SELECT  t.*  FROM ${schema_name}._sc_asset t where t.status > " + StatusConstant.STATUS_DELETEED + " AND t.asset_model_id in ( ${ids} ) limit 1 ")
    Map<String, Object> findAssetByModelIds(@Param("schema_name") String schemaName, @Param("ids") String ids);

    /**
     * 查询列表数量
     *
     * @param schemaName 入参
     * @param paramMap   入参
     * @return 列表数量
     */
    @Select(" <script>" +
            " SELECT  COUNT(am.id) " +
            " FROM ${schema_name}._sc_asset_model am " +
            " LEFT JOIN ${schema_name}._sc_asset_category ac ON am.category_id = ac.id " +
            " LEFT JOIN ${schema_name}._sc_asset_model_customer amc ON amc.model_id = am.id " +
            " LEFT JOIN ${schema_name}._sc_facilities f ON f.id = amc.org_id " +
            " WHERE 1=1 " +
            " <when test='pm.keywordSearch!=null'>" +
            " AND ( am.model_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            " OR  ac.category_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            " OR f.short_title like CONCAT('%', #{pm.keywordSearch}, '%')  ) " +
            " </when>  " +
            " <when test='pm.categorySearch!=null'>" +
            " AND am.category_id IN (${pm.categorySearch})  " +
            " </when>  " +
            " <when test='pm.orgSearch!=null'>" +
            " AND amc.org_id IN (${pm.orgSearch})  " +
            " </when>  " +
            " </script>")
    int findAssetModelListTotal(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 查询列表
     *
     * @param schemaName 入参
     * @param paramMap   入参
     * @return 列表
     */
    @Select(" <script>" +
            " SELECT am.id,am.model_name,am.category_id," +
            " to_char(am.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time, " +
            " ac.category_name,f.short_title as title " +
            " FROM ${schema_name}._sc_asset_model am " +
            " LEFT JOIN ${schema_name}._sc_asset_category ac ON am.category_id = ac.id " +
            " LEFT JOIN ${schema_name}._sc_asset_model_customer amc ON amc.model_id = am.id " +
            " LEFT JOIN ${schema_name}._sc_facilities f ON f.id = amc.org_id " +
            " WHERE 1=1 " +
            " <when test='pm.keywordSearch!=null'>" +
            " AND ( am.model_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            " OR  ac.category_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            " OR f.short_title like CONCAT('%', #{pm.keywordSearch}, '%')  ) " +
            " </when> " +
            " <when test='pm.categorySearch!=null'>" +
            " AND am.category_id IN (${pm.categorySearch})  " +
            " </when>  " +
            " <when test='pm.orgSearch!=null'>" +
            " AND amc.org_id IN (${pm.orgSearch})  " +
            " </when>  " +
            "  ORDER BY am.create_time DESC  ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findAssetModelListPage(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 查询设备型号详情
     *
     * @param schemaName 入参
     * @param id         入参
     * @return 设备型号详情
     */
    @Select(" <script>" +
            " SELECT am.id, am.model_name,am.icon,am.category_id,am.create_time,am.remark,ac.category_name,am.properties::text,f.short_title as title,f.id AS org_id " +
            " FROM ${schema_name}._sc_asset_model am " +
            " LEFT JOIN ${schema_name}._sc_asset_category ac ON am.category_id = ac.id " +
            " LEFT JOIN ${schema_name}._sc_asset_model_customer amc ON amc.model_id = am.id " +
            " LEFT JOIN ${schema_name}._sc_facilities  f ON amc.org_id = f.id " +
            " WHERE am.id =#{id} " +
            " </script>")
    Map<String, Object> findById(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 查询设备型号分页数量
     *
     * @param schemaName 入参
     * @param paramMap   入参
     * @return 设备型号分页数量
     */
    @Select(" <script>" +
            "  SELECT  count(amd.model_id) " +
            "  FROM   ${schema_name}._sc_asset_model_docs AS amd " +
            "  LEFT JOIN ${schema_name}._sc_files AS f ON amd.file_id = f.id " +
            "  LEFT JOIN ${schema_name}._sc_file_type AS ft ON f.file_type_id = ft.id " +
            "  LEFT JOIN ${schema_name}._sc_file_category AS fc ON f.file_category_id= fc.id " +
            "  WHERE amd.model_id = #{pm.model_id} " +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND ( f.file_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            "  OR  f.file_original_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            "  OR ft.type_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            "  OR fc.file_category_name like CONCAT('%', #{pm.keywordSearch}, '%') )  " +
            "  </when>  ${pm.pagination} " +
            " </script>")
    int findAssetModelDocsListTotal(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 查询设备型号附件文档分页列表
     *
     * @param schemaName 入参
     * @param paramMap   入参
     * @return 设备型号分页列表
     */
    @Select(" <script>" +
            "  SELECT  amd.model_id, amd.file_id,f.file_category_id,f.file_type_id, " +
            "   f.file_name, f.file_original_name,Round(f.file_size/1024,4) AS file_size, ft.type_name, fc.file_category_name " +
            "  FROM   ${schema_name}._sc_asset_model_docs AS amd " +
            "  LEFT JOIN ${schema_name}._sc_files AS f ON amd.file_id = f.id " +
            "  LEFT JOIN ${schema_name}._sc_file_type AS ft ON f.file_type_id = ft.id " +
            "  LEFT JOIN ${schema_name}._sc_file_category AS fc ON f.file_category_id= fc.id " +
            "  WHERE amd.model_id= #{pm.model_id} " +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND ( f.file_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            "  OR  f.file_original_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            "  OR ft.type_name like CONCAT('%', #{pm.keywordSearch}, '%')  " +
            "  OR fc.file_category_name like CONCAT('%', #{pm.keywordSearch}, '%') )  " +
            "  </when> ORDER BY f.create_time DESC  ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findAssetModelDocsList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 删除设备型号附件文档根据文档
     *
     * @param schemaName 入参
     * @param model_id   入参
     * @param fileId     入参
     */
    @Delete("DELETE FROM  ${schema_name}._sc_asset_model_docs  WHERE model_id =#{modelId} AND file_id = #{fileId}")
    void deleteAssetModelDocs(@Param("schema_name") String schemaName, @Param("modelId") Integer model_id, @Param("fileId") Integer fileId);

    /**
     * 新增类型关联企业
     *
     * @param schemaName 入参
     * @param paramMap   入参
     */
    @Insert("insert into ${schema_name}._sc_asset_model_customer ( " +
            "model_id,org_id,create_time,create_user_id)  values  " +
            " (#{pm.model_id}::int,#{pm.org_id}::int,current_timestamp,#{pm.create_user_id}) RETURNING id")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insertModelCustomer(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 删除设备型号企业数据
     *
     * @param schemaName 入参
     * @param ids        入参
     */
    @Delete("DELETE FROM ${schema_name}._sc_asset_model_customer where model_id in (${ids})   ")
    void deleteModelCustomer(@Param("schema_name") String schemaName, @Param("ids") String ids);

    /**
     * 删除设备型号企业数据
     *
     * @param schemaName 入参
     * @param id         入参
     */
    @Delete("DELETE FROM ${schema_name}._sc_asset_model_customer where model_id = #{id}   ")
    void deleteModelCustomerById(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 更新设备型号的关联企业
     *
     * @param schemaName 入参
     * @param model_id   入参
     * @param org_id     入参
     */
    @Update("UPDATE  ${schema_name}._sc_asset_model_customer SET org_id = #{org_id} where model_id = #{model_id}   ")
    void updateModelCustomer(@Param("schema_name") String schemaName, @Param("model_id") Integer model_id, @Param("org_id") Integer org_id);

    /**
     * 设备型号的关联企业
     *
     * @param schemaName 入参
     * @param model_id   入参
     */
    @Select("SELECT * FROM  ${schema_name}._sc_asset_model_customer where  model_id = #{model_id}   ")
    Map<String, Object> findModelCustomer(@Param("schema_name") String schemaName, @Param("model_id") Integer model_id);


    /**
     * 新增设备类型附件文档
     *
     * @param schemaName 入参
     * @param paramMap   入参
     */
    @Insert(" INSERT INTO ${schema_name}._sc_asset_model_docs (model_id, file_id, create_user_id, create_time)" +
            " VALUES(#{pm.model_id}, #{pm.file_id}, #{pm.create_user_id}, current_timestamp)")
    void insertAssetModelDocs(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 获得设备类型故障履历列表数量
     *
     * @param schemaName 入参
     * @param pm         入参
     * @return 设备类型故障履历列表数量
     */
    @Select("<script>" +
            " SELECT COUNT(t1.work_code)" +
            " FROM ${schema_name}._sc_works AS t1" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS t2 ON t1.work_code = t2.work_code " +
            " AND ( SELECT max ( finished_time ) FROM ${schema_name}._sc_works_detail " +
            " WHERE work_code = t1.work_code ) = t2.finished_time" +
            " LEFT JOIN ${schema_name}._sc_fault_type AS t3 ON t2.fault_type_id = t3.ID " +
            " LEFT JOIN ${schema_name}._sc_fault_code AS t4 ON t2.repair_code = t4.code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS t5 ON t1.work_type_id = t5.id " +
            " LEFT JOIN ${schema_name}._sc_business_type AS t6 ON t5.business_type_id = t6.business_type_id " +
            " LEFT JOIN ${schema_name}._sc_asset AS t7 ON t1.relation_id = t7.id " +
            " WHERE t1.relation_type = 2 AND t6.business_type_id = 1 AND t1.status = 60 " +
            " AND t7.asset_model_id  = #{pm.id}" +
            "    <when test='pm.keywordSearch != null'> " +
            "       and (t1.work_code like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "       t3.type_name like CONCAT('%', #{pm.keywordSearch}, '%') or" +
            "       t4.code_name like CONCAT('%', #{pm.keywordSearch}, '%') )" +
            "    </when>  " +
            " </script>")
    int findFaultRecordListTotal(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 获得设备类型故障履历列表
     *
     * @param schemaName 入参
     * @param pm         入参
     * @return 设备类型故障履历列表
     */
    @Select("<script>" +
            " SELECT t7.strcode,t1.work_code,t1.work_type_id,t1.relation_type,t1.relation_id,t2.fault_type,t2.repair_code," +
            " t3.type_name,t4.code_name ,to_char(t2.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as finished_time" +
            " FROM ${schema_name}._sc_works AS t1" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS t2 ON t1.work_code = t2.work_code " +
            " AND ( SELECT max ( finished_time ) FROM ${schema_name}._sc_works_detail " +
            " WHERE work_code = t1.work_code ) = t2.finished_time" +
            " LEFT JOIN ${schema_name}._sc_fault_type AS t3 ON t2.fault_type = t3.ID " +
            " LEFT JOIN ${schema_name}._sc_fault_code AS t4 ON t2.repair_code = t4.code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS t5 ON t1.work_type_id = t5.id " +
            " LEFT JOIN ${schema_name}._sc_business_type AS t6 ON t5.business_type_id = t6.business_type_id " +
            " LEFT JOIN ${schema_name}._sc_asset AS t7 ON t1.relation_id = t7._id " +
            " WHERE t1.relation_type = 2 AND t6.business_type_id = 1 AND t1.status = 60 " +
            " AND t7.asset_model_id  = #{pm.id}" +
            "    <when test='pm.keywordSearch != null'> " +
            "       and (t1.work_code like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "       t3.type_name like CONCAT('%', #{pm.keywordSearch}, '%') or" +
            "       t4.code_name like CONCAT('%', #{pm.keywordSearch}, '%') )" +
            "    </when> order by  t2.finished_time desc ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findFaultRecordList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 根据型号id获取自定义字段
     *
     * @param schemaName 入参
     * @param id         入参
     * @return 自定义字段
     */
    @Select("SELECT  amd.properties  FROM   ${schema_name}._sc_asset_model_docs WHERE id =#{id}")
    String findPropertiesById(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 根据设备型号查询设备 判断该型号下是否绑定设备
     *
     * @param schemaName 入参
     * @param id         入参
     */
    @Select("SELECT asset_model_id FROM  ${schema_name}._sc_asset WHERE status = 1 and asset_model_id = #{id} LIMIT 1 ")
    Map<String, Object> findAssetByModelId(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 验证备件型号是否存在
     *
     * @param schemaName     入参
     * @param assetModelList 入参
     * @return 验证备件型号是否存在
     */
    @Select("<script>select distinct id, model_name from ${schemaName}._sc_asset_model " +
            "where is_use = '" + StatusConstant.IS_USE_YES + "' and model_name in " +
            "<foreach collection='assetModels' item='assetModel' open='(' close=')' separator=','> " +
            "#{assetModel} " +
            "</foreach></script>")
    @MapKey("model_name")
    Map<String, Map<String, Object>> queryAssetModelByCode(@Param("schemaName") String schemaName, @Param("assetModels") List<String> assetModelList);

    /**
     * 根据设备型号名称查询设备型号
     *
     * @param schemaName 入参
     * @param model_name 入参
     * @return 设备型号
     */
    @Select(" SELECT id,model_name,category_id,level_id,contact_department,is_use,remark,create_user_id,create_time,properties,icon" +
            " from ${schemaName}._sc_asset_model WHERE model_name=#{model_name} limit 1 ")
    Map<String, Object> findAssetModelByModelName(@Param("schemaName") String schemaName, @Param("model_name") String model_name);

    /**
     * 根据设备类型重置设备型号的自定义字段
     *
     * @param schemaName
     * @param category_id
     */
    @Update(" <script>" +
            " update ${schema_name}._sc_asset_model " +
            " set properties='[]'::jsonb " +
            " where category_id=#{category_id} " +
            " </script>")
    void updateModelCustomFieldsResetByCategoryId(@Param("schema_name")String schemaName,@Param("category_id") Integer category_id);

    /**
     * 删除指定设备类型的指定field_code的自定义字段
     *
     * @param schemaName
     * @param category_id
     * @param field_code
     */
    @Update("UPDATE ${schema_name}._sc_asset_model i " +
            "SET properties = i2.properties " +
            "FROM (SELECT id,jsonb_agg(elems) as properties " +
            "FROM ${schema_name}._sc_asset_model,jsonb_array_elements(properties::jsonb) as elems " +
            "WHERE category_id = #{category_id} AND properties is not null and elems ->> 'field_code' != #{field_code} " +
            "GROUP BY id) i2 " +
            "WHERE i2.id = i.id " +
            "and i.category_id = #{category_id} AND i.properties is not null ")
    void updateCustomFieldsByCategoryIdAndFieldCode(@Param("schema_name")String schemaName,@Param("category_id")Integer category_id,@Param("field_code") String field_code);

    /**
     * 添加设备指定设备类型的自定义字段
     *
     * @param schemaName
     * @param category_id
     * @param field
     */
    @Update("UPDATE ${schema_name}._sc_asset_model " +
            "SET properties = case when properties is null " +
            "then ('['||#{field}||']')::jsonb " +
            "else  properties || #{field}::jsonb " +
            "end " +
            "WHERE category_id = #{category_id} ")
    void updateCustomFieldsByCategoryIdAndField(@Param("schema_name")String schemaName,@Param("category_id")Integer category_id,@Param("field")  String field);
}
