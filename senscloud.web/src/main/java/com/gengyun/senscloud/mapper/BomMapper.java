package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/28.
 */
public interface BomMapper {

    //    @Select("SELECT count(1) FROM (SELECT b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,u.unit_name,b.is_use, " +
//            "COALESCE(string_agg(f.title,','),'') title,COALESCE(sum(bs.quantity),0) total " +
//            "FROM ${schema_name}._sc_bom b " +
//            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
//            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id=u.id " +
//            "LEFT JOIN ${schema_name}._sc_bom_supplies s ON s.bom_id = b.id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id=s.supplier_id " +
//            "${searchWord} " +
//            "GROUP BY b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,b.is_use,u.unit_name " +
//            "${havingWord} )t")
    @Select("SELECT count(1) FROM (SELECT b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,u.unit_name,b.is_use, " +
            "COALESCE(array_to_string(array_agg(DIStINCt f.short_title),','),'') AS title, " +
            "COALESCE(sum(bs.quantity),0) total " +
            "FROM ${schema_name}._sc_bom b " +
            "LEFT JOIN ${schema_name}._sc_bom_supplies s ON s.bom_id = b.ID " +
            "LEFt JOIN ${schema_name}._sc_facilities f ON f.ID = s.supplier_id " +
            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id=u.id " +
            "${searchWord} " +
            "GROUP BY b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,b.is_use,u.unit_name " +
            "${havingWord} )t")
    int countBomList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord, @Param("havingWord") String havingWord);//TODO

    //    @Select("SELECT b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,u.unit_name,b.code_classification,b.is_use, " +
//            "COALESCE(string_agg(f.title,','),'') title,COALESCE(sum(bs.quantity),0) total," +
//            " (SELECT id FROM ${schema_name}._sc_work_type WHERE business_type_id = 3001 AND is_use = '1' limit 1) AS work_type_id " +
//            "FROM ${schema_name}._sc_bom b " +
//            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
//            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id=u.id " +
//            "LEFT JOIN ${schema_name}._sc_bom_supplies s ON s.bom_id = b.id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id=s.supplier_id " +
//            "${searchWord} " +
//            "GROUP BY b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,b.code_classification,b.is_use,u.unit_name " +
//            "${havingWord}")
    @Select("SELECT b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,u.unit_name,b.code_classification,b.is_use,b.icon,b.data_order,b.create_time, " +
            "COALESCE(array_to_string(array_agg(DIStINCt f.short_title),','),'') AS title, " +
            "COALESCE (CASE WHEN POSITION ( ',' IN COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT f.short_title ), ',' ), '' ) ) > 0 THEN SUM ( bs.quantity ) / 2 ELSE SUM ( bs.quantity ) END,0 ) total," +
            " (SELECT id FROM ${schema_name}._sc_work_type WHERE business_type_id = 3001 AND is_use = '1' limit 1) AS work_type_id " +
            "FROM ${schema_name}._sc_bom b " +
            "LEFT JOIN ${schema_name}._sc_bom_supplies s ON s.bom_id = b.ID " +
            "LEFt JOIN ${schema_name}._sc_facilities f ON f.ID = s.supplier_id " +
            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id=u.id " +
            "${searchWord} " +
            "GROUP BY b.id,b.bom_name,b.material_code,b.bom_model,bt.type_name,b.code_classification,b.is_use,u.unit_name,b.icon,b.data_order,b.create_time " +
            "${havingWord}")
    List<Map<String, Object>> findBomList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord, @Param("havingWord") String havingWord);//TODO

    @Select("select count(1) from ${schema_name}._sc_bom where material_code = #{materialCode} and status > " + StatusConstant.STATUS_DELETEED)
    int countBomByMaterialCode(@Param("schema_name") String schemaName, @Param("materialCode") String materialCode);

    @Insert("insert into ${r.schemaName}._sc_bom (id,security_quantity_msg_interval,bom_name,bom_model,remark,is_use,create_time,type_id,unit_id,brand_name,create_user_id,material_code,is_from_service_supplier,service_life,bom_use,show_price,currency_id,manufacturer_id,code_classification,data_order)" +
            "values(#{r.id},#{r.security_quantity_msg_interval}::int,#{r.bom_name},#{r.bom_model},#{r.remark},'1',#{r.create_time},#{r.type_id}::int,#{r.unit_id}::int,#{r.brand_name},#{r.create_user_id},#{r.material_code},#{r.is_from_service_supplier},#{r.service_life}::int,#{r.bom_use},#{r.show_price}::numeric,#{r.currency_id},#{r.manufacturer_id}::int,#{r.code_classification},#{r.data_order}::int)")
    //@Options(useGeneratedKeys = true, keyProperty = "r.id", keyColumn = "id")
    int insertBom(@Param("r") Map<String, Object> data);

    @Insert("INSERT INTO ${r.schemaName}._sc_bom_supplies( bom_code, supplier_id, supply_period, is_use,bom_id) VALUES (  '', #{r.supplier_id}::int, #{r.supply_period},'1',#{r.bom_id})")
    int insertBomSupplier(@Param("r") Map<String, Object> data);

    @Insert({
            "<script>",
            "<foreach collection='bomList' item='bom' index='index' separator=';'>",
            "INSERT INTO ${schema_name}._sc_bom(id,bom_name,bom_model,material_code,is_use,create_time,create_user_id,type_id,manufacturer_id,unit_id,is_from_service_supplier,service_life,show_price,currency_id,code_classification,data_order,status,remark) ",
            "VALUES (#{bom.id},#{bom.bom_name},#{bom.bom_model},#{bom.material_code},#{bom.is_use},#{bom.create_time},#{bom.create_user_id},#{bom.type_id}::int,#{bom.manufacturer_id},#{bom.unit_id}::int,#{bom.is_from_service_supplier},#{bom.service_life},#{bom.show_price},#{bom.currency_id},#{bom.code_classification},#{bom.data_order},#{bom.status},#{bom.remark}) ",
            "ON CONFLICT (id) ",
            "DO UPDATE SET bom_name=#{bom.bom_name},bom_model=#{bom.bom_model},is_use=#{bom.is_use},type_id=#{bom.type_id}::int,manufacturer_id=#{bom.manufacturer_id},unit_id=#{bom.unit_id}::int,is_from_service_supplier=#{bom.is_from_service_supplier},service_life=#{bom.service_life},show_price=#{bom.show_price},currency_id=#{bom.currency_id},code_classification=#{bom.code_classification},data_order=#{bom.data_order} ",
            "</foreach>",
            "</script>"})
    void insertOrUpdateBom(@Param("schema_name") String schemaName, @Param("bomList") List<Map<String, Object>> bomList);

    //    @Select("SELECT b.id,bom_name,b.remark,is_use,create_user_account,create_time,bom_model,type_id,unit_id,brand_name,b.material_code, " +
//            "is_from_service_supplier,service_life, bom_use,b.show_price,b.currency_id,manufacturer_id,icon,COALESCE(sum(bs.quantity),0) quantity " +
//            "FROM ${schema_name}._sc_bom b " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.material_code = bs.material_code " +
//            "WHERE b.id = #{id}" +
//            "GROUP BY b.id,bom_name,b.remark,is_use,create_user_account,create_time,bom_model,type_id,unit_id,brand_name,b.material_code, " +
//            "is_from_service_supplier,service_life, bom_use,b.show_price,b.currency_id,manufacturer_id,icon")
//    @Select("SELECT b.id,bom_name,b.remark,b.is_use,b.create_user_id,b.create_time,bom_model,type_id,unit_id,brand_name,b.material_code,  " +
//            "is_from_service_supplier,service_life, bom_use,b.show_price,b.currency_id,manufacturer_id,icon,COALESCE(sum(bs.quantity),0) quantity, " +
//            "COALESCE(f.short_title,f.title) manufacturer_name,bt.type_name,u.unit_name " +
//            "FROM ${schema_name}._sc_bom b  " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON b.manufacturer_id=f.id  " +
//            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id=bt.id  " +
//            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id=u.id " +
//            "WHERE b.id = #{id} " +
//            "GROUP BY b.id,bom_name,b.remark,b.is_use,b.create_user_id,b.create_time,bom_model,type_id,unit_id,brand_name,b.material_code,  " +
//            "is_from_service_supplier,service_life, bom_use,b.show_price,b.currency_id,manufacturer_id,icon,manufacturer_name,bt.type_name,u.unit_name")
    @Select("SELECT b.security_quantity_msg_interval,b.id,b.code_classification,bom_name,b.remark,b.is_use,b.create_user_id,b.create_time,bom_model,type_id,unit_id,brand_name,b.material_code,  " +
            "is_from_service_supplier,service_life, bom_use,b.show_price,b.currency_id,manufacturer_id::varchar,icon,COALESCE(sum(bs.quantity),0) quantity, " +
            "COALESCE(f.short_title,f.title) manufacturer_name,bt.type_name,u.unit_name " +
            "FROM ${schema_name}._sc_bom b  " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_facilities f ON b.manufacturer_id=f.id  " +
            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id=bt.id  " +
            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id=u.id " +
            "WHERE b.id = #{id} " +
            "group by b.id,bom_name,b.remark,b.is_use,b.create_user_id,b.create_time,bom_model,type_id,unit_id,brand_name,b.material_code," +
            "is_from_service_supplier,service_life, bom_use,b.show_price,b.currency_id,manufacturer_id,icon,f.short_title,f.title,bt.type_name,u.unit_name ")
    Map<String, Object> findBomById(@Param("schema_name") String schemaName, @Param("id") String id);//TODO

    //    @Select("SELECT s.stock_name,bs.store_position,bs.quantity total,bs.security_quantity,bs.max_security_quantity " +
//            "FROM ${schema_name}._sc_bom b " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON bs.stock_id=s.id " +
//            "WHERE b.id = #{id} AND s.id IN (SELECT rs.stock_id " +
//            "FROM ${schema_name}._sc_role_stock rs " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
//            "WHERE up.user_id = #{user_id}) ORDER BY bs.id DESC ${pageCondition}")
//    @Select("SELECT s.stock_name,coalesce(bs.store_position,'') store_position,bs.quantity total,bs.security_quantity,bs.max_security_quantity " +
//    @Select("SELECT bs.id,s.stock_name,string_agg(bs.store_position,',') store_position,sum(bs.quantity) total," +
//            "sum(bs.security_quantity) security_quantity,sum(bs.max_security_quantity) max_security_quantity " +
//            "FROM ${schema_name}._sc_bom b " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON bs.stock_id=s.id " +
//            "WHERE b.id = #{id} GROUP BY bs.id,s.stock_name ORDER BY bs.id desc ${pageCondition}")
//
//            "(SELECT string_agg(DISTINCT bwdi.store_position,',') from ${schema_name}._sc_bom_works bw left join ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
//            "LEFT JOIN ${schema_name}._sc_bom_works_detail_item bwdi on bwdi.sub_work_code = bwd.sub_work_code where bw.stock_id = s.id and bwdi.bom_id = b.id and bw.status = 60 ) as store_position " +
//
    @Select("SELECT bs.id,s.stock_name,bs.quantity total,bs.security_quantity,bs.max_security_quantity,COALESCE(bs.show_price,0) as show_price, " +
            "bs.store_position " +
            "FROM ${schema_name}._sc_bom b " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_stock s ON bs.stock_id=s.id " +
            "WHERE b.id = #{id} AND s.id IN (SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id}) ORDER BY bs.id DESC ${pageCondition}")
    List<Map<String, Object>> findBomStockByBomId(@Param("schema_name") String schemaName, @Param("id") String id, @Param("user_id") String user_id, @Param("pageCondition") String pageCondition);

    //    @Select("select count(1) from (SELECT bs.id,s.stock_name,string_agg(bs.store_position,',') store_position,sum(bs.quantity) total," +
//            "sum(bs.security_quantity) security_quantity,sum(bs.max_security_quantity) max_security_quantity " +
//            "FROM ${schema_name}._sc_bom b " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
//            "JOIN ${schema_name}._sc_stock s ON bs.stock_id=s.id " +
//            "WHERE b.id = #{id} GROUP BY bs.id,s.stock_name ) t "
//            "(SELECT string_agg(DISTINCT bwdi.store_position,',') from ${schema_name}._sc_bom_works bw left join ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
//            "LEFT JOIN ${schema_name}._sc_bom_works_detail_item bwdi on bwdi.sub_work_code = bwd.sub_work_code where bw.stock_id = s.id and bwdi.bom_id = b.id and bw.status = 60 ) as store_position " +
    @Select("select count(1) from (SELECT s.stock_name,bs.quantity total,bs.security_quantity,bs.max_security_quantity, " +
            "bs.store_position " +
            "FROM ${schema_name}._sc_bom b " +
            "JOIN ${schema_name}._sc_bom_stock bs ON b.id = bs.bom_id " +
            "left JOIN ${schema_name}._sc_stock s ON bs.stock_id=s.id " +
            "WHERE b.id = #{id} AND s.id IN (SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id})) t")
    int findBomStockCountByBomId(@Param("schema_name") String schemaName, @Param("id") String id, @Param("user_id") String user_id);

    @Select("SELECT f.title,coalesce(f.short_title,'') short_title,bs.supply_period,coalesce(t.price,0.00) price,bs.is_use,bs.id,t.currency_id,bs.supplier_id::varchar " +
            "FROM ${schema_name}._sc_bom b " +
            "JOIN ${schema_name}._sc_bom_supplies bs ON b.id=bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_facilities f ON bs.supplier_id=f.id " +
            "LEFT JOIN (SELECT * FROM (SELECT ROW_NUMBER() OVER (partition BY bom_id ORDER BY in_code DESC) rowId,* " +
            "from ${schema_name}._sc_bom_in_stock_detail ) t WHERE rowId=1 )t ON bs.supplier_id=t.supplier_id " +
            "WHERE b.id = #{id} ${whereSql} ${pageCondition}")
    List<Map<String, Object>> findBomSupplierInfoByBomId(@Param("schema_name") String schemaName, @Param("id") String id, @Param("whereSql") String bomSupplierWhereString, @Param("pageCondition") String pageCondition);

    @Select("SELECT bs.bom_code,bs.supplier_id,bs.supply_period,bs.is_use,f.short_title from ${schema_name}._sc_bom_supplies bs left join ${schema_name}._sc_facilities f ON bs.supplier_id=f.id where bs.id = #{id}::int")
    Map<String, Object> findBomSupplierById(@Param("schema_name") String schemaName, @Param("id") String id);

    @Update("<script>" +
            "update ${schemaName}._sc_bom_supplies set " +
            "<when test=\"null != material_code and '' != material_code\"> " +
            " material_code = #{material_code}, " +
            "</when>" +
            "<when test=\"null != bom_code and '' != bom_code\"> " +
            "bom_code=#{bom_code}," +
            "</when>" +
            "<when test=\"null != supplier_id \"> " +
            "supplier_id=#{supplier_id}::int," +
            "</when>" +
            "<when test=\"null != supply_period and '' != supply_period\"> " +//类型更新修改设备附件信息
            "supply_period=#{supply_period}," +
            "</when>" +
            "<when test=\"null != is_use and '' != is_use\"> " +
            "is_use=#{is_use}" +
            "</when>" +
            "where id=#{id}::int" +
            "</script>")
    int updateBomSupplier(Map<String, Object> data);

    @Select("select b.* from ${schema_name}._sc_bom b left join ${schema_name}._sc_bom_supplies bs on b.id = bs.bom_id where bs.id = #{id}")
    Map<String, Object> findBomByBomSupplierId(@Param("schema_name") String schemaName, @Param("id") Integer id);

    @Delete("delete from ${schema_name}._sc_bom_supplies where id = #{id}")
    void deleteBomSupplier(@Param("schema_name") String schemaName, @Param("id") Integer id);

    @Select("SELECT count(1) " +
            "FROM ${schema_name}._sc_bom b " +
            "JOIN ${schema_name}._sc_bom_supplies bs ON b.id = bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_facilities f ON bs.supplier_id=f.id " +
            "LEFT JOIN (SELECT * FROM (SELECT ROW_NUMBER() OVER (partition BY bom_id ORDER BY in_code DESC) rowId,* " +
            "from ${schema_name}._sc_bom_in_stock_detail ) t WHERE rowId=1 )t ON bs.supplier_id=t.supplier_id " +
            "WHERE b.id = #{id} ${whereSql} ")
    int findBomSupplierCountByBomId(@Param("schema_name") String schemaName, @Param("id") String id, @Param("whereSql") String bomSupplierWhereString);

    @Select("select count(1) from ${schema_name}._sc_bom where material_code = #{materialCode} and id != #{id}")
    int countBomByMaterialCodeWithOutId(@Param("schema_name") String schemaName, @Param("materialCode") String materialCode, @Param("id") String id);

    @Update("<script>UPDATE ${schemaName}._sc_bom  " +
            "<set> " +
            "<when test=\"null != sectionType and 'bomIcon' == sectionType\"> " +
            "icon = #{icon} " +
            "</when>" +
            "<when test=\"null != sectionType and 'bomUse' == sectionType\"> " +
            "is_use = #{is_use} " +
            "</when>" +
            "<when test=\"null != sectionType and 'bomTop' == sectionType\"> " +
            "<if test=\"security_quantity_msg_interval!=null and security_quantity_msg_interval!=''\"> " +
            "security_quantity_msg_interval = #{security_quantity_msg_interval}::int, " +
            "</if> " +
            "<if test=\"bom_name!=null and bom_name!=''\"> " +
            "bom_name = #{bom_name}, " +
            "</if> " +
            "<if test=\"remark!=null and remark!=''\"> " +
            "remark = #{remark}, " +
            "</if> " +
            "<if test=\"create_user_id!=null and create_user_id!=''\"> " +
            "create_user_id = #{create_user_id}, " +
            "</if> " +
            "<if test=\"create_time!=null\"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"bom_model!=null and bom_model!=''\"> " +
            "bom_model = #{bom_model}, " +
            "</if> " +
            "<if test=\"type_id!=null \"> " +
            "type_id = #{type_id}::int, " +
            "</if> " +
            "<if test=\"unit_id!=null\"> " +
            "unit_id = #{unit_id}::int, " +
            "</if> " +
            "<if test=\"brand_name!=null and brand_name!=''\"> " +
            "brand_name = #{brand_name}, " +
            "</if> " +
            "<if test=\"material_code!=null and material_code!=''\"> " +
            "material_code = #{material_code}, " +
            "</if> " +
            "<if test=\"is_from_service_supplier!=null\"> " +
            "is_from_service_supplier = #{is_from_service_supplier}, " +
            "</if> " +
            "<if test=\"service_life!=null \"> " +
            "service_life = #{service_life}::int, " +
            "</if> " +
            "<if test=\"bom_use!=null \"> " +
            "bom_use = #{bom_use}, " +
            "</if> " +
            "<if test=\"show_price!=null\"> " +
            "show_price = #{show_price}::numeric, " +
            "</if> " +
            "<if test=\"currency_id!=null\"> " +
            "currency_id = #{currency_id}::int, " +
            "</if> " +
            "<if test=\"manufacturer_id!=null\"> " +
            "manufacturer_id = #{manufacturer_id}::int, " +
            "</if> " +
            "<if test=\"code_classification!=null and code_classification !=''\"> " +
            "code_classification = #{code_classification} , " +
            "</if> " +
            "</when>" +
            "</set> " +
            "WHERE id = #{id}   </script>")
    int updateBom(Map<String, Object> data);

    @Select("SELECT max(data_order) FROM ${schema_name}._sc_bom")
    Integer getMaxBomDataOrder(@Param("schema_name") String schemaName);

    @Update(" <script> update ${schema_name}._sc_bom set status = #{status} where id in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach> </script> ")
    int updateSelectBomStatus(@Param("schema_name") String schemaName, @Param("ids") String[] idStr, @Param("status") int statusDeleted);

    @Select("SELECT count(1) FROM ${schema_name}._sc_bom_supplies WHERE bom_id = #{bom_id} AND supplier_id = #{supplier_id} AND bom_code = '' ")
    int countBomSupplier(@Param("schema_name") String schemaName, @Param("bom_id") String id, @Param("supplier_id") Integer supplierId);

//    @Select("SELECT count(1) FROM (select DISTINCT ba.transfer_code as code,bad.bom_code,bad.bom_id,bad.quantity,bad.real_quantity,  " +
//            "ba.create_user_id,u.user_name,ba.create_time,s.id as stock_id,s.stock_name,  " +
//            "sta.status as status_name,ba.status,1 as page_type  " +
//            "from ${schema_name}._sc_bom_allocation as ba  " +
//            "JOIN ${schema_name}._sc_bom_allocation_detail bad ON bad.transfer_code = ba.transfer_code  " +
//            "JOIN ${schema_name}._sc_stock AS s ON s.ID = ba.from_stock_id  " +
//            "join ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id  " +
//            "join ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bad.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "join ${schema_name}._sc_status AS sta ON sta.ID = ba.status  " +
//            "JOIN ${schema_name}._sc_user AS u ON u.ID = ba.create_user_id  " +
//            "JOIN ${schema_name}._sc_bom AS b ON bad.bom_id = b.ID  " +
//            "where b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            "union all  " +
//            "select DISTINCT re.recipient_code,brd.bom_code,brd.bom_id,brd.quantity,brd.real_quantity,re.create_user_id,  " +
//            "u.user_name,re.create_time,s.id as stock_id,s.stock_name,  " +
//            "sta.status as status_name,re.status,2 as page_type  " +
//            "from ${schema_name}._sc_bom_recipient as re  " +
//            "JOIN ${schema_name}._sc_bom_recipient_detail brd ON brd.recipient_code = re.recipient_code  " +
//            "join ${schema_name}._sc_stock as s on s.id=re.stock_id  " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id  " +
//            "join ${schema_name}._sc_status as sta on sta.id=re.status  " +
//            "join ${schema_name}._sc_user as u on u.id=re.create_user_id  " +
//            "JOIN ${schema_name}._sc_bom as b ON brd.bom_id = b.id  " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = brd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "where b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            "union all  " +
//            "SELECT DISTINCT bd.discard_code,bdd.bom_code,bdd.bom_id,bdd.quantity,bdd.real_quantity,bd.create_user_id,  " +
//            "u.user_name,bd.create_time,s.id as stock_id,s.stock_name,  " +
//            "st.status as status_name,bd.status,3 as page_type  " +
//            "FROM ${schema_name}._sc_bom_discard bd  " +
//            "JOIN ${schema_name}._sc_bom_discard_detail bdd ON bdd.discard_code = bd.discard_code  " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bd.stock_id  " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id  " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bd.status  " +
//            "join ${schema_name}._sc_user u on bd.create_user_id=u.id  " +
//            "JOIN ${schema_name}._sc_bom as b ON bdd.bom_id = b.id  " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bdd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "where b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id})  " +
//            "union all  " +
//            "SELECT DISTINCT bis.in_code,bisd.bom_code,bisd.bom_id,bisd.quantity,bisd.real_quantity, bis.create_user_id,u.user_name,  " +
//            "bis.create_time,s.id as stock_id,s.stock_name,st.status as status_name, bis.status,4 as page_type  " +
//            "FROM ${schema_name}._sc_bom_in_stock bis  " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON bis.stock_id = bs.stock_id  " +
//            "JOIN ${schema_name}._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code  " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bis.stock_id  " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bis.status  " +
//            "join ${schema_name}._sc_user u on bis.create_user_id=u.id  " +
//            "JOIN ${schema_name}._sc_bom as b ON bisd.bom_id = b.id  " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bisd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "WHERE b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            ") t ")

    @Select("SELECT count(1) from ( select tab.* from " +
            "(select DISTINCT bw.work_code as code,bwdi.bom_id,bwdi.quantity, " +
            "bwdi.real_quantity,bw.status,wt.type_name as process_method,wt.type_name, " +
            "bw.create_user_id,u.user_name,to_char(bw.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwdi.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail_item bwdi ON bwd.sub_work_code = bwdi.sub_work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON s.ID = bw.stock_id " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bw.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwdi.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where b.id = #{id} and bw.status = " + StatusConstant.COMPLETED + " and bw.to_stock_id is null AND s.id IN (SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id}) " +
            "UNION ALL " +
            "select DISTINCT bw.work_code as code,bwar.bom_id,bwar.quantity, " +
            "bwar.real_quantity,bwar.status,bwar.type_name as process_method,bwar.type_name, " +
            "bwar.create_user_id,u.user_name,to_char(bwar.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwar.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_allot_record bwar ON bw.work_code = bwar.work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON s.ID = bwar.stock_id " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bwar.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwar.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where b.id = #{id} and bw.to_stock_id is not null AND s.id IN (SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id}) " +
            ") tab) t")
    int findCountBomInAndOut(@Param("schema_name") String schemaName, @Param("id") String id, @Param("user_id") String userId);

    //    @Select("SELECT * FROM (select DISTINCT ba.transfer_code as code,bad.bom_code,bad.bom_id,bad.quantity,bad.real_quantity,  " +
//            "ba.create_user_id,u.user_name,ba.create_time,s.id as stock_id,s.stock_name,bad.area_code_out as store_position,  " +
//            "sta.status as status_name,ba.status,coalesce((select (c.resource->>'title_aaz_r')::jsonb->>#{userLang}  " +
//            "from public.company_resource c where c.company_id = #{company_id}), '调拨')as process_method,1 as page_type  " +
//            "from ${schema_name}._sc_bom_allocation as ba  " +
//            "JOIN ${schema_name}._sc_bom_allocation_detail bad ON bad.transfer_code = ba.transfer_code  " +
//            "JOIN ${schema_name}._sc_stock AS s ON s.ID = ba.from_stock_id  " +
//            "join ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id  " +
//            "join ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bad.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "join ${schema_name}._sc_status AS sta ON sta.ID = ba.status  " +
//            "JOIN ${schema_name}._sc_user AS u ON u.ID = ba.create_user_id  " +
//            "JOIN ${schema_name}._sc_bom AS b ON bad.bom_id = b.ID  " +
//            "where b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            "union all  " +
//            "select DISTINCT re.recipient_code,brd.bom_code,brd.bom_id,brd.quantity,brd.real_quantity,re.create_user_id,  " +
//            "u.user_name,re.create_time,s.id as stock_id,s.stock_name,brd.area_code,    " +
//            "sta.status as status_name,re.status,coalesce((select (c.resource->>'btn_ac')::jsonb->>#{userLang}  " +
//            "from public.company_resource c where c.company_id = #{company_id}), '领用')as type,2 as page_type  " +
//            "from ${schema_name}._sc_bom_recipient as re  " +
//            "JOIN ${schema_name}._sc_bom_recipient_detail brd ON brd.recipient_code = re.recipient_code  " +
//            "join ${schema_name}._sc_stock as s on s.id=re.stock_id  " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id  " +
//            "join ${schema_name}._sc_status as sta on sta.id=re.status  " +
//            "join ${schema_name}._sc_user as u on u.id=re.create_user_id  " +
//            "JOIN ${schema_name}._sc_bom as b ON brd.bom_id = b.id  " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = brd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "where b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            "union all  " +
//            "SELECT DISTINCT bd.discard_code,bdd.bom_code,bdd.bom_id,bdd.quantity,bdd.real_quantity,bd.create_user_id,  " +
//            "u.user_name,bd.create_time,s.id as stock_id,s.stock_name,bdd.area_code,    " +
//            "st.status as status_name,bd.status,coalesce((select (c.resource->>'title_abaa_d')::jsonb->>#{userLang}  " +
//            "from public.company_resource c where c.company_id = #{company_id}), '报废')as type,3 as page_type  " +
//            "FROM ${schema_name}._sc_bom_discard bd  " +
//            "JOIN ${schema_name}._sc_bom_discard_detail bdd ON bdd.discard_code = bd.discard_code  " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bd.stock_id  " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id  " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bd.status  " +
//            "join ${schema_name}._sc_user u on bd.create_user_id=u.id  " +
//            "JOIN ${schema_name}._sc_bom as b ON bdd.bom_id = b.id  " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bdd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "where b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            "union all  " +
//            "SELECT DISTINCT bis.in_code,bisd.bom_code,bisd.bom_id,bisd.quantity,bisd.real_quantity, bis.create_user_id,u.user_name,  " +
//            "bis.create_time,s.id as stock_id,s.stock_name,bisd.area_code, st.status as status_name, bis.status,  " +
//            "coalesce((select (c.resource->>'btn_k')::jsonb->>#{userLang}  " +
//            "from public.company_resource c where c.company_id = #{company_id}), '入库') as type,4 as page_type  " +
//            "FROM ${schema_name}._sc_bom_in_stock bis  " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON bis.stock_id = bs.stock_id  " +
//            "JOIN ${schema_name}._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code  " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bis.stock_id  " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bis.status  " +
//            "join ${schema_name}._sc_user u on bis.create_user_id=u.id  " +
//            "JOIN ${schema_name}._sc_bom as b ON bisd.bom_id = b.id  " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bisd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id  " +
//            "WHERE b.id = #{id}::int AND s.id IN (SELECT rs.stock_id  " +
//            "FROM ${schema_name}._sc_role_stock rs  " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id  " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id  " +
//            "WHERE up.user_id = #{user_id}) " +
//            ") t ORDER BY t.create_time desc,t.bom_id desc ${pagination} ")
    @Select("select * from " +
            "(select DISTINCT bw.work_code as code,bwdi.bom_id,bwdi.quantity, " +
            "bwdi.real_quantity,bw.status,wt.type_name as process_method,wt.type_name, " +
            "bw.create_user_id,u.user_name,to_char(bw.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwdi.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail_item bwdi ON bwd.sub_work_code = bwdi.sub_work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON s.ID = bw.stock_id " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bw.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwdi.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where b.id = #{id} and bw.status = " + StatusConstant.COMPLETED + " and bw.to_stock_id is null AND s.id IN (SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id}) " +
            "UNION ALL " +
            "select DISTINCT bw.work_code as code,bwar.bom_id,bwar.quantity, " +
            "bwar.real_quantity,bwar.status,bwar.type_name as process_method,bwar.type_name, " +
            "bwar.create_user_id,u.user_name,to_char(bwar.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwar.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_allot_record bwar ON bw.work_code = bwar.work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON s.ID = bwar.stock_id " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bw.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwar.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where b.id = #{id} and bw.to_stock_id is not null AND s.id IN (SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id})) tab " +
            "ORDER BY tab.create_time desc,tab.bom_id desc ${pagination}")
    List<Map<String, Object>> findBomInAndOutList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("user_id") String userId, @Param("userLang") String userLang, @Param("company_id") Long company_id, @Param("pagination") String pagination);

    /**
     * 根据物料编码查询已存在的备件
     *
     * @param schemaName
     * @param materialCodes
     * @return
     */
    @Select("<script>select distinct id,bom_name, material_code from ${schemaName}._sc_bom " +
            "where material_code in " +
            "<foreach collection='materialCodes' item='materialCode' open='(' close=')' separator=','> " +
            "#{materialCode} " +
            "</foreach></script>")
    @MapKey("material_code")
    Map<String, Map<String, Object>> queryBomByMaterialCodes(@Param("schemaName") String schemaName, @Param("materialCodes") List<String> materialCodes);

    @Select("SELECT count(1) from (SELECT t.bom_code,b.status,bwd.remark,bwdi.in_type,cd.name " +
            "FROM ${schema_name}._sc_bom_works_detail_item bwdi " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail bwd on bwdi.sub_work_code = bwd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_bom b on b.id = bwdi.bom_id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd on cd.data_type = 'int_type' and bwdi.in_type::varchar=cd.code " +
            "JOIN ( " +
            "SELECT DISTINCT max(bw.create_time) as create_time,bwdi.bom_id,bwdi.bom_code,max(bwdi.sub_work_code) as sub_work_code " +
            "from ${schema_name}._sc_bom_works_detail_item bwdi " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail bwd on bwdi.sub_work_code = bwd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works bw ON bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "WHERE wt.business_type_id = 3001 AND bw.status = 60 and bwdi.bom_id = #{bom_id}::int and bw.stock_id in ( " +
            "SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id} " +
            ") " +
            "group by bwdi.bom_id,bwdi.bom_code) t on b.id = t.bom_id and t.sub_work_code=bwdi.sub_work_code " +
            "ORDER BY bwdi.sub_work_code desc )t ")
    int findCountBomDetailedList(@Param("schema_name") String schemaName, @Param("bom_id") String id, @Param("user_id") String userId);

//    @Select("SELECT t.bom_code,b.status,bwd.remark,bwdi.in_type, " +
//            "coalesce((select (c.resource->>cd.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd.name) as in_type_name " +
//            "FROM ${schema_name}._sc_bom_works_detail_item bwdi " +
//            "LEFT JOIN ${schema_name}._sc_bom_works_detail bwd on bwdi.sub_work_code = bwd.sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_bom b on b.id = bwdi.bom_id " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd on cd.data_type = 'int_type' and bwdi.in_type::varchar=cd.code " +
//            "JOIN ( " +
//            "SELECT DISTINCT max(bw.create_time) as create_time,bwdi.bom_id,bwdi.bom_code,max(bwdi.sub_work_code) as sub_work_code " +
//            "from ${schema_name}._sc_bom_works_detail_item bwdi " +
//            "LEFT JOIN ${schema_name}._sc_bom_works_detail bwd on bwdi.sub_work_code = bwd.sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_bom_works bw ON bw.work_code = bwd.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
//            "WHERE wt.business_type_id = 3001 AND bw.status = 60 and bwdi.bom_id = #{bom_id}::int and bw.stock_id in ( " +
//            "SELECT rs.stock_id " +
//            "FROM ${schema_name}._sc_role_stock rs " +
//            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
//            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
//            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
//            "WHERE up.user_id = #{user_id} " +
//            ") " +
//            "group by bwdi.bom_id,bwdi.bom_code) t on b.id = t.bom_id and t.sub_work_code=bwdi.sub_work_code " +
//            "ORDER BY bwdi.sub_work_code desc ${pagination} ")
//    List<Map<String, Object>> findBomDetailedList(@Param("schema_name") String schemaName, @Param("bom_id") String bn, @Param("user_id") String userId, @Param("userLang") String userLang, @Param("companyId") Long companyId, @Param("pagination") String pagination);

    @Select("SELECT t.bom_code,b.status,bwd.remark,bwdi.in_type, " +
            "cd.name as in_type_name " +
            "FROM ${schema_name}._sc_bom_works_detail_item bwdi " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail bwd on bwdi.sub_work_code = bwd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_bom b on b.id = bwdi.bom_id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd on cd.data_type = 'int_type' and bwdi.in_type::varchar=cd.code " +
            "JOIN ( " +
            "SELECT DISTINCT max(bw.create_time) as create_time,bwdi.bom_id,bwdi.bom_code,max(bwdi.sub_work_code) as sub_work_code " +
            "from ${schema_name}._sc_bom_works_detail_item bwdi " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail bwd on bwdi.sub_work_code = bwd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works bw ON bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "WHERE wt.business_type_id = 3001 AND bw.status = 60 and bwdi.bom_id = #{bom_id}::int and bw.stock_id in ( " +
            "SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id} " +
            ") " +
            "group by bwdi.bom_id,bwdi.bom_code) t on b.id = t.bom_id and t.sub_work_code=bwdi.sub_work_code " +
            "ORDER BY bwdi.sub_work_code desc ${pagination} ")
    List<Map<String, Object>> findBomDetailedList(@Param("schema_name") String schemaName, @Param("bom_id") String bn, @Param("user_id") String userId, @Param("pagination") String pagination);


}
