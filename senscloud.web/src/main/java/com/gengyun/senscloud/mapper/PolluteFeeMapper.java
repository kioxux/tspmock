package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.PolluteFeeModel;
import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface PolluteFeeMapper {

    /**
     * 位置限定sql
     */
    String postionFilterSql = "(select distinct sr.asset_position_code from " +
            " ${schema_name}._sc_role_asset_position sr " +
            " join ${schema_name}._sc_role f on sr.role_id = f.id " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id " +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id} )";


    /**
     * 缴费信息列表
     *
     * @param schema_name
     * @param userId
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select count(1) from ${schema_name}._sc_pollute_fee a " +
            " join ${schema_name}._sc_customer c on a.facility_id = c.facility_id " +
            " where c.position_code in " +
            postionFilterSql +
            " and a.belong_month between #{pm.begin_date} and #{pm.end_date}" +
            " <when test='pm.inner_code!=null and pm.inner_code!=\"\" '> " +
            " and c.inner_code ilike concat('%',#{pm.inner_code},'%') " +
            " </when> " +
            " <when test='pm.title!=null and pm.title!=\"\" '> " +
            " and c.title ilike concat('%',#{pm.title},'%')" +
            " </when>" +
            " <when test='pm.keywordSearch!=null and pm.keywordSearch!=\"\" '> " +
            " and (c.title ilike concat('%',#{pm.keywordSearch},'%') or " +
            "      c.inner_code ilike concat('%',#{pm.keywordSearch},'%'))" +
            " </when> " +
            " <when test='pm.status!=null and pm.status!=\"\" '> " +
            " and a.status = #{pm.status}::int " +
            " </when> " +
            " </script>")
    int findPolluteFeeListCount(@Param("schema_name") String schema_name, @Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * 缴费信息列表
     *
     * @param schema_name
     * @param userId
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select a.*,c.title,c.short_title,c.inner_code from ${schema_name}._sc_pollute_fee a " +
            " join ${schema_name}._sc_customer c on a.facility_id = c.facility_id " +
            " where c.position_code in " +
            postionFilterSql +
            " and a.belong_month between #{pm.begin_date} and #{pm.end_date}" +
            " <when test='pm.inner_code!=null and pm.inner_code!=\"\" '> " +
            " and c.inner_code ilike concat('%',#{pm.inner_code},'%') " +
            " </when> " +
            " <when test='pm.title!=null and pm.title!=\"\" '> " +
            " and c.title ilike concat('%',#{pm.title},'%')" +
            " </when>" +
            " <when test='pm.keywordSearch!=null and pm.keywordSearch!=\"\" '> " +
            " and (c.title ilike concat('%',#{pm.keywordSearch},'%') or " +
            "      c.inner_code ilike concat('%',#{pm.keywordSearch},'%'))" +
            " </when> " +
            " <when test='pm.status!=null and pm.status!=\"\" '> " +
            " and a.status = #{pm.status}::int " +
            " </when> " +
            " ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findPolluteFeeList(@Param("schema_name") String schema_name, @Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * 客户缴费统计信息
     *
     * @param schema_name
     * @param userId
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select  (select count(1) from ${schema_name}._sc_customer b " +
            " where b.status>" + StatusConstant.STATUS_DELETEED + " and b.is_use='" + StatusConstant.IS_USE_YES + "' " +
            " and b.position_code in " +
            postionFilterSql +
            " ) as total," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_pollute_fee m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            postionFilterSql +
            " and m.status in (0) and m.belong_month between #{pm.begin_date} and #{pm.end_date}) s) as should_pay," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_pollute_fee m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            postionFilterSql +
            " and m.status in (1) and  m.belong_month between #{pm.begin_date} and #{pm.end_date}) s) as payed," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_pollute_fee m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            postionFilterSql +
            " and m.status in (0) and  m.belong_month between #{pm.begin_date} and #{pm.end_date}) s) as audit" +
            " </script>")
    Map<String, Object> findPolluteFeeStatistics(@Param("schema_name") String schema_name, @Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * 缴费日历
     *
     * @param schema_name
     * @param userId
     * @param pm
     * @return
     */
    @Select(" <script> " +
            " select coalesce(a.day_total,0) audit,coalesce(b.day_total,0) should_pay, a.days from " +
            " (select count(distinct a.facility_id) day_total,a.belong_month days  from ${schema_name}._sc_pollute_fee a " +
            " join ${schema_name}._sc_customer b on a.facility_id = b.facility_id where " +
            "  b.position_code in " +
            postionFilterSql +
            " and a.belong_month between #{pm.begin_date} and #{pm.end_date} " +
            " and a.status = 0 group by a.belong_month ) a left join " +
            " (select count(distinct a.facility_id) day_total,a.belong_month days  from ${schema_name}._sc_pollute_fee a " +
            " join ${schema_name}._sc_customer b on a.facility_id = b.facility_id where " +
            "  b.position_code in " +
            postionFilterSql +
            " and a.belong_month between #{pm.begin_date} and #{pm.end_date}" +
            " and a.status = 1 group by a.belong_month) b  on a.days = b.days " +
            " </script>")
    List<Map<String, Object>> findsearchPolluteFeeCalendar(@Param("schema_name") String schema_name, @Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * 客户缴费记录
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script> " +
            " select * from ${schema_name}._sc_pollute_fee a " +
            " where a.facility_id = #{pm.id}::int " +
            " and a.belong_month between #{pm.begin_time} and #{pm.end_time} " +
            " order by a.belong_month desc " +
            " </script>")
    List<Map<String, Object>> findCustomerPolluteFeeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据id获取排污费
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script> " +
            " select a.*,cu.inner_code,cu.title,cu.short_title from ${schema_name}._sc_pollute_fee a " +
            " left join ${schema_name}._sc_customer cu on a.facility_id = cu.facility_id  " +
            " where a.id = #{pm.id}::int " +
            " </script>")
    Map<String, Object> findCustomerPolluteFeeById(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 排污费明细
     *
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script> " +
            " select * from ${schema_name}._sc_pollute_fee_detail a " +
            " where a.fee_id = #{pm.id}::int and a.belong_month = #{pm.belong_month}" +
            " </script>")
    List<Map<String, Object>> findCustomerPolluteFeeDetailByFeeId(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 编辑排污费用
     *
     * @param schema_name
     * @param paramMap
     */
    @Update("<script>" +
            " update ${schema_name}._sc_pollute_fee " +
            " <trim prefix='set' suffixOverrides=','> " +
            " <if test=\"pm.amount != null and pm.amount !=''\"> " +
            " amount=#{pm.amount}::numeric, " +
            " </if> " +
            " <if test=\"pm.final_amount != null and pm.final_amount !=''\"> " +
            " final_amount=#{pm.final_amount}::numeric, " +
            " </if> " +
            " <if test=\"pm.exceed_amount != null and pm.exceed_amount !=''\"> " +
            " exceed_amount=#{pm.exceed_amount}::numeric, " +
            " </if> " +
            " <if test=\"pm.normal_amount != null and pm.normal_amount !=''\"> " +
            " normal_amount=#{pm.normal_amount}::numeric, " +
            " </if> " +
            " <if test=\"pm.pay_amount != null and pm.pay_amount !=''\"> " +
            " pay_amount=#{pm.pay_amount}::numeric, " +
            " </if> " +
            " <if test=\"pm.final_flow != null and pm.final_flow !=''\"> " +
            " final_flow=#{pm.final_flow}::numeric, " +
            " </if> " +
            " <if test=\"pm.exceed_times != null and pm.exceed_times !=''\"> " +
            " exceed_times=#{pm.exceed_times}::int, " +
            " </if> " +
            " <if test=\"pm.payment_value != null and pm.payment_value !=''\"> " +
            " payment_value=#{pm.payment_value}::numeric, " +
            " </if> " +
            " <if test=\"pm.status != null and pm.status !=''\"> " +
            " status=#{pm.status}::int, " +
            " </if> " +
            " </trim> " +
            " where id=#{pm.id}::int " +
            "</script>")
    int modifyPolluteFee(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_pollute_fee_detail " +
            " <trim prefix='set' suffixOverrides=','> " +
            " <if test=\"pm.amount != null and pm.amount !=''\"> " +
            " amount=#{pm.amount}::numeric, " +
            " </if> " +
            " <if test=\"pm.final_flow != null and pm.final_flow !=''\"> " +
            " final_flow=#{pm.final_flow}::numeric, " +
            " </if> " +
            " <if test=\"pm.price != null and pm.price !=''\"> " +
            " price=#{pm.price}::numeric, " +
            " </if> " +
            " </trim> " +
            " where id=#{pm.id}::int " +
            "</script>")
    int modifyPolluteFeeDetailNormal(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_pollute_fee_detail " +
            " <trim prefix='set' suffixOverrides=','> " +
            " <if test=\"pm.final_flow != null and pm.final_flow !=''\"> " +
            " final_flow=#{pm.final_flow}::numeric, " +
            " </if> " +
            " <if test=\"pm.price != null and pm.price !=''\"> " +
            " price=#{pm.price}::numeric, " +
            " </if> " +
            " <if test=\"pm.ratio != null and pm.ratio !=''\"> " +
            " ratio=#{pm.ratio}::numeric, " +
            " </if> " +
            " <if test=\"pm.repeat_fee != null and pm.repeat_fee !=''\"> " +
            " repeat_fee=#{pm.repeat_fee}::numeric, " +
            " </if> " +
            " <if test=\"pm.repeat_times != null and pm.repeat_times !=''\"> " +
            " repeat_times=#{pm.repeat_times}::int, " +
            " </if> " +
            " </trim> " +
            " where id=#{pm.id}::int " +
            "</script>")
    int modifyPolluteFeeDetailExceed(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    /**
     * 审核排污费用
     *
     * @param schema_name
     * @param model
     */
    @Update(" <script> " +
            " UPDATE ${schema_name}._sc_pollute_fee " +
            " SET auditor = #{pm.auditor}, " +
            " status = #{pm.status}::int, " +
            " audit_time = NOW() " +
            " WHERE id = #{pm.id}" +
            " </script>")
    void auditPolluteFee(@Param("schema_name") String schema_name, @Param("pm") PolluteFeeModel model);

    @Select("select config_name,remark,setting_value,config_title,group_title from ${schema_name}._sc_system_config where group_title=#{group_title}")
    List<Map<String, Object>> polluteFeeMapper(@Param("schema_name") String schemaName, @Param("group_title") String group_title);

    @Update(" <script> " +
            " update ${schema_name}._sc_system_shedule " +
            " set expression=#{pm.expression}, " +
            " reschedule='t' ::bool" +
            " where task_class=#{pm.task_class}" +
            " </script>")
    void modfiySysShedule(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> param);

    @Select(" <script> " +
            " select count(1) from ${schema_name}._sc_pollute_fee where facility_id=#{pm.facility_id} ::int " +
            " and belong_month = #{pm.belong_month} " +
            " </script> ")
    int findPolluteFeeByCondition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select(" <script> " +
            " select count(1) from ${schema_name}._sc_pollute_fee where facility_id=#{pm.facility_id} ::int " +
            " and belong_month = #{pm.belong_month} and amount = #{pm.amount} " +
            " </script> ")
    int findPolluteFeeByUpdate(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_pollute_fee(facility_id,belong_month,begin_date,end_date,amount, " +
            "final_amount,exceed_amount,normal_amount,pay_amount,final_flow,exceed_times,payment_value,create_user_id)  " +
            "values (#{pm.facility_id}::int,#{pm.belong_month},#{pm.begin_date}::timestamp,#{pm.end_date}::timestamp,#{pm.amount}," +
            "#{pm.final_amount},#{pm.exceed_amount},#{pm.normal_amount},#{pm.pay_amount},#{pm.final_flow}, " +
            "#{pm.exceed_times},#{pm.payment_value}::numeric,#{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    int insertPolluteFee(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Update("update ${schema_name}._sc_pollute_fee set begin_date=#{pm.begin_date}::timestamp,end_date=#{pm.end_date}::timestamp, " +
            " amount=#{pm.amount},final_amount=#{pm.final_amount},exceed_amount=#{pm.exceed_amount},normal_amount=#{pm.normal_amount}, " +
            " pay_amount=#{pm.pay_amount},final_flow=#{pm.final_flow},exceed_times=#{pm.exceed_times}, " +
            " payment_value=#{pm.payment_value},modify_time=now() " +
            " where facility_id=#{pm.facility_id}::int and belong_month=#{pm.belong_month}")
    int updatePolluteFee(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_pollute_fee_detail(fee_id,fee_item_id,facility_id,belong_month,final_flow, " +
            "price,amount,fee_type,create_user_id)  " +
            "values (#{pm.fee_id},#{pm.fee_item_id},#{pm.facility_id} ::int,#{pm.belong_month},#{pm.final_flow}," +
            "#{pm.price},#{pm.amount},#{pm.fee_type},#{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    int insertPolluteFeeNormalDetail(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Update(" update ${schema_name}._sc_pollute_fee_detail set " +
            " final_flow=#{pm.final_flow},price=#{pm.price},amount=#{pm.amount},fee_type=#{pm.fee_type} " +
            " where facility_id=#{pm.facility_id}::int and belong_month=#{pm.belong_month} and fee_item_id=#{pm.fee_item_id}")
    int updatePolluteFeeNormalDetail(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_pollute_fee_detail(fee_id,fee_item_id,facility_id,belong_month,final_flow, " +
            "price,ratio,repeat_times,repeat_fee,amount,fee_type,create_user_id)  " +
            "values (#{pm.fee_id},#{pm.fee_item_id},#{pm.facility_id} ::int,#{pm.belong_month},#{pm.final_flow}," +
            "#{pm.price},#{pm.ratio},#{pm.repeat_count},#{pm.repeat_fee},#{pm.amount},#{pm.fee_type},#{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    int insertPolluteFeeExcDetail(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Update(" update ${schema_name}._sc_pollute_fee_detail set " +
            " final_flow=#{pm.final_flow},price=#{pm.price},ratio=#{pm.ratio},repeat_times=#{pm.repeat_count}, " +
            " repeat_fee=#{pm.repeat_fee},amount=#{pm.amount},fee_type=#{pm.fee_type} " +
            " where facility_id=#{pm.facility_id}::int and belong_month=#{pm.belong_month} and fee_item_id=#{pm.fee_item_id}")
    int updatePolluteFeeExcDetail(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("INSERT INTO ${schema_name}._sc_pollute_survey_record(facility_id,factor_id,limit_value, " +
            "limit_unit,survey_value,ratio,belong_date,create_user_id)  " +
            "VALUES (#{pm.facility_id} ::int,#{pm.factor_id},#{pm.limit_value}," +
            "#{pm.limit_unit},#{pm.survey_value},#{pm.ratio},#{pm.belong_date} ::timestamp,#{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    int insertPolluteSurveyRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select t.code as value, t.name as text, t.is_lang ,t.reserve1,t.parent_code AS parent " +
            "from ${schema_name}._sc_cloud_data t where t.data_type = #{dataType} " +
            "order by t.data_type, t.data_order ")
    List<Map<String, Object>> findStaticDataByType(@Param("schema_name") String schemaName, @Param("dataType") String dataType);

    @Select("<script>" +
            " select coalesce(tb.title,tb.short_title) as cus_name,tb.facility_id " +
            " from (select distinct title,short_title,facility_id from ${schema_name}._sc_customer " +
            " where status > "+ StatusConstant.STATUS_DELETEED +" and (title in " +
            " <foreach collection='cusNames' item='cusName' open='(' close=')' separator=','> " +
            " #{cusName} " +
            " </foreach>" +
            " or short_title in " +
            " <foreach collection='cusNames' item='cusName' open='(' close=')' separator=','> " +
            " #{cusName} " +
            " </foreach>" +
            " )) tb " +
            "</script>")
    @MapKey("cus_name")
    Map<String, Map<String, Object>> queryCusNameByName(@Param("schema_name") String schemaName, @Param("cusNames") List<String> cusNameList);
}
