package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.entity.LoginLogEntity;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;

/**
 * 登录日志处理
 * User: Wudang Dong
 * Date: 2019/07/15
 * Time: 上午11:42
 */
@Mapper
public interface LoginLogMapper {
    /**
     * token验证
     *
     * @param schemaName 企业库
     * @param clientName 客户端
     * @param token      token
     * @return 数量
     */
    @Select(" select count(1) from ${schema_name}._sc_login_log a " +
            " where client_name = #{clientName} and token = #{token} and extract(day FROM (now() - login_time )) < 1 " +
            " and a.id = (select max(b.id) from ${schema_name}._sc_login_log b where b.account = a.account and client_name = #{clientName}) ")
    int countTokenByInfo(@Param("schema_name") String schemaName, @Param("clientName") String clientName, @Param("token") String token);

    /**
     * 根据token查找用户信息
     *
     * @param schemaName 企业库
     * @param clientName 客户端
     * @param token      token
     * @return account
     */
    @Select("select account from ${schema_name}._sc_login_log where client_name = #{clientName} and token = #{token} order by id desc limit 1")
    String findAccountByToken(@Param("schema_name") String schemaName, @Param("clientName") String clientName, @Param("token") String token);

    /**
     * 新增登录记录
     *
     * @param schemaName 企业库
     * @param loginLog   登录记录
     * @return 成功数量
     */
    @Insert("insert into ${schema_name}._sc_login_log (account, client_name, login_time, client_ip," +
            "client_os, client_browser, token) values (#{dt.account}, #{dt.client_name}, #{dt.login_time}, " +
            "#{dt.client_ip}, #{dt.client_os}, #{dt.client_browser}, #{dt.token})")
    int insertLoginLog(@Param("schema_name") String schemaName, @Param("dt") LoginLogEntity loginLog);

    /**
     * 退出登录
     *
     * @param schemaName  企业库
     * @param token       token
     * @param logout_time 退出时间
     * @return 成功数量
     */
    @Update("update ${schema_name}._sc_login_log set loginout_time = #{logout_time} where token = #{token} ")
    int updateLoginLogForLogout(@Param("schema_name") String schemaName, @Param("token") String token, @Param("logout_time") Timestamp logout_time);
}
