package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.response.AssetPositionResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface AssetAnalysByMapMapper {


    //根据条件，获取所有的第一级位置
    //根据条件，获取所有的地图为百度地图的位置，2018-08-11
    @Select("SELECT ap.*,f.file_url,f.file_width,f.file_height ${filed} " +
            "FROM ${schema_name}._sc_asset_position ap " +
            "left join ${schema_name}._sc_files f on ap.layer_path = CAST(f.ID AS VARCHAR ( 20 ))")
    List<AssetPositionResult> findAllParentAssetPositionCustomResultList(@Param("schema_name") String schema_name, @Param("filed") String filed);

    //根据位置id，获取位置以及它的子位置信息
    @Select("with RECURSIVE re (position_code,position_name,parent_code,layer_path,location) as " +
            "(select position_code,position_name,parent_code,layer_path,location from ${schema_name}._sc_asset_position where position_code = #{position_code} " +
            "union all " +
            "select ap2.position_code,ap2.position_name,ap2.parent_code,ap2.layer_path,ap2.location from ${schema_name}._sc_asset_position ap2,re e where e.position_code=ap2.parent_code " +
            ") " +
            "SELECT ap.*,file.file_url,file_width,file_height ${filed} " +
            "FROM re as ap " +
            "left join ${schema_name}._sc_files file on ap.layer_path=cast(file.id as varchar(20)) ")
    List<AssetPositionResult> findAssetPositionAndChildrenCustomResultByPositionCode(@Param("schema_name") String schema_name, @Param("filed") String filed, @Param("position_code") String position_code);


    //根据位置id，获取位置的信息
    @Select("SELECT ap.*,file.file_url,file_width,file_height ${filed} " +
            "FROM ${schema_name}._sc_asset_position ap " +
            "left join ${schema_name}._sc_files file on ap.layer_path=cast(file.id as varchar(20)) " +
            "where ap.position_code = #{position_code} ")
    List<AssetPositionResult> findAssetPositionCustomResultByPositionCode(@Param("schema_name") String schema_name, @Param("filed") String filed, @Param("position_code") String position_code);


    @Select("<script> " +
            " SELECT ap.position_code,ap.position_name,COALESCE(ap.parent_code,'') as parent_code,ap.position_type_id,ap.location::varchar,ap.layer_path," +
            " ap.remark,ap.file_id,ap.status,COALESCE(app.position_name,'') as parent_position_name,f.file_url,f.file_width,f.file_height " +
            " FROM  ${schema_name}._sc_asset_position AS ap " +
            " LEFT JOIN ${schema_name}._sc_files f on ap.layer_path=cast(f.id as varchar(20)) " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS app ON ap.parent_code = app.position_code " +
            "<where> " +
            "   <choose> " +
            "       <when test=\"position_code !=null and position_code != ''\"> " +
            "           and ap.position_code = #{position_code} " +
            "       </when> " +
            "       <otherwise> " +
            "           and ap.position_code = (SELECT position_code from ${schema_name}._sc_asset_position WHERE parent_code = '' or parent_code is null and status>" + StatusConstant.STATUS_DELETEED + " ORDER BY create_time asc limit 1) " +
            "       </otherwise> " +
            "   </choose> " +
            "</where> " +
            "</script> ")
    Map<String, Object> findAssetPositionInfoByPositionCode(@Param("schema_name") String schema_name, @Param("position_code") String positionCode);

    @Select("<script>SELECT a.asset_name as name,a.status,a.running_status_id,a.id as id,a.parent_id as parent," +
            "COALESCE(a.location::varchar,'')as location,'asset' as type," +
            "<choose> " +
            "   <when test = \"condition != null and condition != ''\"> " +
            "       case when a.asset_name like CONCAT('%', #{condition}, '%') then 1 else 0 end as high_light," +
            "   </when>" +
            "   <otherwise>" +
            "       0 as high_light," +
            "   </otherwise> " +
            "</choose> " +
            "<choose> " +
            "   <when test = \" '0.1' == type \"> " +
            "      case when (SELECT COUNT(DISTINCT wd.work_code) FROM ${schema_name}._sc_asset sa " +
            "      INNER JOIN ${schema_name}._sc_works_detail wd ON wd.relation_id = sa.id " +
            "      INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
            "      WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status &lt; " + StatusConstant.COMPLETED +
            "      and sa.id = a.id AND wd.relation_type = 2 AND wt.business_type_id = " + Constants.BUSINESS_TYPE_REPAIR_TYPE +
            "      ) > 0 then 1 else 0 end as is_fault, " +
            "   </when>" +
            "   <otherwise>" +
            "       0 as is_fault," +
            "   </otherwise> " +
            "</choose> " +
            "<choose> " +
            "   <when test = \"'1' == type\"> " +
            "       (SELECT COUNT(DISTINCT wd.work_code) FROM ${schema_name}._sc_asset sa " +
            "       INNER JOIN ${schema_name}._sc_works_detail wd ON wd.relation_id = sa.id " +
            "       INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
            "       WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status &lt; " + StatusConstant.COMPLETED +
            "       and sa.id = a.id AND wd.relation_type = 2" +
            "       <choose> " +
            "           <when test = \"workType != null and workType == 1\"> " +
            "               AND wt.business_type_id = " + Constants.BUSINESS_TYPE_REPAIR_TYPE +
            "           </when>" +
            "           <when test = \"workType != null and workType == 2\"> " +
            "               AND wt.business_type_id = " + Constants.BUSINESS_TYPE_MAINTAIN_TYPE +
            "           </when>" +
            "           <otherwise>" +
            "               AND wt.business_type_id in (" + Constants.BUSINESS_TYPE_REPAIR_TYPE + "," + Constants.BUSINESS_TYPE_MAINTAIN_TYPE + ")" +
            "           </otherwise> " +
            "       </choose> " +
            "       ) as work_count " +
            "   </when>" +
            "   <otherwise>" +
            "       0 as work_count " +
            "   </otherwise> " +
            "</choose> " +
            "FROM ${schema_name}._sc_asset a " +
            "LEFT JOIN ${schema_name}._sc_asset_iot_setting ais ON a.id = ais.asset_id " +
            "WHERE a.position_code = #{position_code} AND a.status > " + StatusConstant.STATUS_DELETEED + " " +
            "<when test = \"'0' == type and assetType != null and assetType == 1 \"> " +
            "   AND ais.iot_status in (" + Constants.ASSET_IOT_SETTING_STATUS_ONLINE + "," + Constants.ASSET_IOT_SETTING_STATUS_OFFLINE + ") " +
            "</when> " +
            "union all " +
            "SELECT ap.position_name,ap.status,0 AS running_status_id,ap.position_code,ap.parent_code," +
            "COALESCE(ap.location::varchar,'')as location,'position' as type," +
            "<choose> " +
            "   <when test = \"condition != null and condition != ''\"> " +
            "       case when ap.position_name like CONCAT('%', #{condition}, '%') then 1 else 0 end as high_light," +
            "   </when>" +
            "   <otherwise>" +
            "       0 as high_light," +
            "   </otherwise> " +
            "</choose> " +
            "<choose> " +
            "   <when test = \" '0.1' == type \"> " +
            "    case when (SELECT COUNT(DISTINCT wd.work_code) FROM ${schema_name}._sc_works w " +
            "       INNER JOIN ${schema_name}._sc_works_detail wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
            "       WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status &lt; " + StatusConstant.COMPLETED +
            "       and w.position_code in (" +
            "   WITH RECURSIVE re (position_code, parent_code) AS ( " +
            " SELECT t.position_code, t.parent_code FROM ${schema_name}._sc_asset_position t where t.position_code = ap.position_code AND t.status  > " + StatusConstant.STATUS_DELETEED +
            " UNION ALL" +
            " SELECT t2.position_code, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e WHERE e.position_code = t2.parent_code AND t2.status > " + StatusConstant.STATUS_DELETEED +
            " ) select position_code from re " +
            "       ) AND wt.business_type_id = " + Constants.BUSINESS_TYPE_REPAIR_TYPE +
            "      ) > 0 then 1 else 0 end as is_fault, " +
            "   </when>" +
            "   <otherwise>" +
            "       0 as is_fault," +
            "   </otherwise> " +
            "</choose> " +
            "<choose> " +
            "   <when test = \"'1' == type\"> " +
            "       (SELECT COUNT(DISTINCT wd.work_code) FROM ${schema_name}._sc_works w " +
            "       INNER JOIN ${schema_name}._sc_works_detail wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
            "       WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status &lt; " + StatusConstant.COMPLETED +
            "       and w.position_code = ap.position_code " +
            "       <choose> " +
            "           <when test = \"workType != null and workType == 1\"> " +
            "               AND wt.business_type_id = " + Constants.BUSINESS_TYPE_REPAIR_TYPE +
            "           </when>" +
            "           <when test = \"workType != null and workType == 2\"> " +
            "               AND wt.business_type_id = " + Constants.BUSINESS_TYPE_MAINTAIN_TYPE +
            "           </when>" +
            "           <otherwise>" +
            "               AND wt.business_type_id in (" + Constants.BUSINESS_TYPE_REPAIR_TYPE + "," + Constants.BUSINESS_TYPE_MAINTAIN_TYPE + ")" +
            "           </otherwise> " +
            "       </choose> " +
            "       ) as work_count " +
            "   </when>" +
            "   <otherwise>" +
            "       0 as work_count " +
            "   </otherwise> " +
            "</choose> " +
            "FROM ${schema_name}._sc_asset_position ap " +
            "WHERE ap.parent_code = #{position_code} " +
            "AND ap.status > " + StatusConstant.STATUS_DELETEED +
            "</script> ")
    List<Map<String, Object>> findAssetAndPositionByPositionCode(@Param("schema_name") String schema_name, @Param("position_code") String db_position_code, @Param("type") String type, @Param("assetType") Integer assetType, @Param("workType") Integer workType, @Param("condition") String keywordSearch);


//
////    //根据位置id，获取位置的信息
////    @Select("SELECT fac.*,file.file_url,file_width,file_height " +
////            "FROM ${schema_name}._sc_facilities fac " +
////            "left join ${schema_name}._sc_files file on fac.layerpath=cast(file.id as varchar(20)) " +
////            "where fac.id = #{facilityId} ")
////    FacilitiesResult GetFacilitiesResultById(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId);
//
//    //根据位置id，获取位置的信息
//    @Select("SELECT fac.*,file.file_url,file_width,file_height ${filed} " +
//            "FROM ${schema_name}._sc_facilities fac " +
//            "left join ${schema_name}._sc_files file on fac.layerpath=cast(file.id as varchar(20)) " +
//            "where fac.maptype = 'bd' and fac.id = ${facilityId} ")
//    List<FacilitiesResult> GetFacilitiesCustomResultById(@Param("schema_name") String schema_name, @Param("filed") String filed, @Param("facilityId") Integer facilityId);
//
//    //根据位置id，获取位置以及它的子位置信息
//    @Select("with RECURSIVE re (id,title,parentid,layerpath,maptype,location) as " +
//            "( " +
//            "select id,title,parentid,layerpath,maptype,location from ${schema_name}._sc_facilities where id = ${facilityId} " +
//            "union all " +
//            "select f2.id,f2.title,f2.parentid,f2.layerpath,f2.maptype,f2.location from ${schema_name}._sc_facilities f2,re e where e.id=f2.parentid " +
//            ") " +
//            "SELECT fac.*,file.file_url,file_width,file_height ${filed} " +
//            "FROM re as fac " +
//            "left join ${schema_name}._sc_files file on fac.layerpath=cast(file.id as varchar(20)) " +
//            "where fac.maptype = 'bd' ")
//    List<FacilitiesResult> GetFacilityAndChildrenCustomResultById(@Param("schema_name") String schema_name, @Param("filed") String filed, @Param("facilityId") Integer facilityId);
//
////    //根据条件，获取所有的子位置
////    @Select("SELECT fac.*,file.file_url,file_width,file_height " +
////            "FROM ${schema_name}._sc_facilities fac " +
////            "left join ${schema_name}._sc_files file on fac.layerpath=cast(file.id as varchar(20)) " +
////            "where fac.parentid = #{facilityId} ")
////    List<FacilitiesResult> GetAllFacilitiesResultList(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId);
//
////    //根据条件，获取所有的第一级位置
////    //根据条件，获取所有的地图为百度地图的位置，2018-08-11
////    @Select("SELECT fac.*,file.file_url,file_width,file_height " +
////            "FROM ${schema_name}._sc_facilities fac " +
////            "left join ${schema_name}._sc_files file on fac.layerpath=cast(file.id as varchar(20)) " +
////            "where fac.maptype = 'bd' ")
////    List<FacilitiesResult> GetAllParentFacilitiesResultList(@Param("schema_name") String schema_name);
//
//    //根据条件，获取所有的第一级位置
//    //根据条件，获取所有的地图为百度地图的位置，2018-08-11
//    @Select("SELECT fac.*,file.file_url,file_width,file_height ${filed} " +
//            "FROM ${schema_name}._sc_facilities fac " +
//            "left join ${schema_name}._sc_files file on fac.layerpath=cast(file.id as varchar(20)) " +
//            "where fac.maptype = 'bd' ")
//    List<FacilitiesResult> GetAllParentFacilitiesCustomResultList(@Param("schema_name") String schema_name, @Param("filed") String filed);
//
////    //根据位置ids，统计设备的总数，包含类型数量、维护数量
////    @Select(
//////            "SELECT meta.asset_alias as assetTypeName,COUNT(asset._id) as totalCount " +
//////            "FROM ${schema_name}._sc_metadata_assets meta  " +
//////            "left join ${schema_name}._sc_asset asset on meta.asset_name=asset.asset_type " +
//////            "where asset.intsiteid in (${facilityId}) and asset.intstatus>0 " +
//////            "group by meta.asset_alias " +
//////            "union " +
////            "SELECT '待维修' as assetTypeName,COUNT(r.repair_code) as totalCount " +
////            "FROM ${schema_name}._sc_repair r " +
////            "left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
////            "where r.status in (20,30,40,50) and r.facility_id in (${facilityId})  " +
////            "union " +
////            "SELECT '待保养' as assetTypeName,COUNT(m.maintain_code) as totalCount " +
////            "FROM ${schema_name}._sc_maintain m " +
////            "left join ${schema_name}._sc_asset dv on m.asset_id=dv._id " +
////            "where m.status in (70,80) and m.facility_id in (${facilityId})  " +
////            "union " +
////            "SELECT '待巡检' as assetTypeName,COUNT(inspection_code) as totalCount " +
////            "FROM ${schema_name}._sc_inspection " +
////            "where status in (20,30,40,50) and facility_id in (${facilityId}) ")
////    List<AssetTypeTotalResult> GetAssetTotalByFacility(@Param("schema_name") String schema_name, @Param("facilityId") String facilityId);
//
//    //根据位置id，统计设备所有待办
//    @Select("select * from ( " +
//            " SELECT r.repair_code as orderno,lsx.strcode,lsx.strname,'维修' as ordertype,s.status,r.createtime,s.id " +
//            " FROM ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_asset lsx on r.asset_id=lsx._id " +
//            " left join ${schema_name}._sc_status s on s.id=r.status " +
//            " where r.status!=60 and r.status!=900 and r.facility_id in (${facilityId}) " +
//            " union " +
//            "SELECT m.maintain_code as orderno,lsx.strcode,lsx.strname,'保养' as ordertype,s.status,m.createtime,s.id " +
//            " FROM ${schema_name}._sc_maintain m " +
//            "  left join ${schema_name}._sc_asset lsx on m.asset_id=lsx._id " +
//            "  left join ${schema_name}._sc_status s on s.id=m.status " +
//            "  where m.status in (70,80) and m.facility_id in (${facilityId}) " +
//            " union " +
//            "SELECT m.inspection_code as orderno,lsx.area_code as strcode,lsx.area_name as strname,'巡检' as ordertype,s.status,m.createtime,s.id " +
//            " FROM ${schema_name}._sc_inspection m " +
//            "  left join ${schema_name}._sc_inspection_area lsx on m.area_code=lsx.area_code " +
//            "  left join ${schema_name}._sc_status s on s.id=m.status " +
//            "  where m.status!=60 and m.status!=900 and m.facility_id in (${facilityId})  " +
//            ") a order by createtime desc")
//    List<ToDoDataListResult> GetAssetToDoByFacility(@Param("schema_name") String schema_name, @Param("facilityId") String facilityId);
//
////    //根据位置id，查询所有的设备
////    @Select("SELECT dv.*,s.status FROM ${schema_name}._sc_asset dv " +
////            " left join ${schema_name}._sc_asset_status s on s.id=dv.intstatus " +
////            "where dv.intsiteid = #{facilityId} and dv.intstatus in (${status}) ")
////    List<Asset> GetAssetByFacility(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId,@Param("status") String status);
////
////    @Select("select dv.*,m.monitor_name as monitor_name, m.monitor_value_unit as monitor_value_unit, m.monitor_value as monitor_value  from ${schema_name}._sc_asset_monitor_current m " +
////            "left join ${schema_name}._sc_asset dv on m.asset_code =dv.strcode  " +
////            "where  dv.intsiteid = #{facilityId}")
////    List<Asset> GetAssetByFacilityAndMonitor(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId);
////
////
////    //场地监控，根据位置id，查询所有的设备
////    @Select("SELECT dv.*,s.status,m.monitor_name as monitor_name, m.monitor_value_unit as monitor_value_unit, m.monitor_value as monitor_value FROM ${schema_name}._sc_asset dv " +
////            " left join ${schema_name}._sc_asset_status s on s.id=dv.intstatus " +
////            " left join ${schema_name}._sc_asset_monitor_current m on m.asset_code = dv.strcode" +
////            " where dv.intsiteid = #{facilityId} and dv.intstatus in (${status}) ")
////    List<Asset> GetFacilityMonitorAssetByFacilityId(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId,@Param("status") String status);
////
////    /**
////     * 根据组织id，查询设备数量
////     * @param schema_name
////     * @param type  1、维修 2、保养
////     * @param facilityId
////     * @return
////     */
////    @Select("SELECT COUNT(DISTINCT a._id ) FROM ${schema_name}._sc_asset a  " +
////            "INNER JOIN ${schema_name}._sc_works_detail wd ON wd.relation_id = a._id " +
////            "INNER JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = a._id " +
////            "INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
////            "INNER JOIN (" +
////            "   SELECT cf.* FROM ${schema_name}._sc_facilities pf " +
////            "   INNER JOIN ${schema_name}._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
////            "   WHERE pf.id = #{facilityId} " +
////            ") f ON ao.org_id = f.id " +
////            "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.COMPLETED + " AND wt.business_type_id = #{type} AND wd.relation_type = 2 ")
////    int countAssetByFacilityId(@Param("schema_name") String schema_name, @Param("type") int type, @Param("facilityId") Long facilityId);
//
//    /**
//     * 根据组织id，查询设备列表
//     * @param schema_name
//     * @param type 1、维修 2、保养
//     * @param facilityNo
//     * @return
//     */
//    @Select("SELECT DISTINCT a._id,a.strname,a.strcode FROM ${schema_name}._sc_asset a  " +
//            "INNER JOIN ${schema_name}._sc_works_detail wd ON wd.relation_id = a._id " +
//            "INNER JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = a._id " +
//            "INNER JOIN ${schema_name}._sc_facilities f ON f.id = ao.org_id " +
//            "WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status < " + StatusConstant.COMPLETED +
//            " AND wd.work_type_id = #{type} AND wd.relation_type = 2 AND f.facility_no LIKE '${facilityNo}%' ")
//    List<Asset> getAssetByFacilityId(@Param("schema_name") String schema_name, @Param("type") int type, @Param("facilityNo") String facilityNo);
//
//    /**
//     * 根据组织id，查询过期的设备数量
//     * @param schema_name
//     * @param facilityNo
//     * @return
//     */
//    @Select("SELECT COUNT(DISTINCT a._id ) FROM ${schema_name}._sc_asset a  " +
//            "INNER JOIN ${schema_name}._sc_works_detail wd ON wd.relation_id = a._id " +
//            "INNER JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = a._id " +
//            "INNER JOIN ${schema_name}._sc_facilities f ON f.id = ao.org_id " +
//            "WHERE wd.work_type_id IN ( "+ Constants.BUSINESS_TYPE_REPAIR_TYPE+", "+Constants.BUSINESS_TYPE_MAINTAIN_TYPE+" ) AND wd.status = "+Constants.WORK_STATUS_EXPIRE+" AND wd.relation_type = 2 AND f.facility_no LIKE '${facilityNo}%' ")
//    int countExpireAssetByFacilityId(@Param("schema_name") String schema_name, @Param("facilityNo") String facilityNo);
//
//    /**
//     *  获取子组织（含有监控设备的组织）坐标信息以及监控设备数量
//     * @param schema_name
//     * @param facilityId
//     * @return
//     */
////    @Select("SELECT tb.id,tb.location,tb.count FROM ( " +
////            "   SELECT A.id,CAST(a.location as varchar) AS location,( " +
////            "       SELECT COUNT(T.asset_code) FROM (" +
////            "           WITH RECURSIVE re ( ID, title, parentid ) AS (" +
////            "               SELECT ID,title,parentid FROM ${schema_name}._sc_facilities WHERE parentid = A.ID AND org_type <= 2 " +
////            "               UNION ALL " +
////            "               SELECT f2.ID,f2.title,f2.parentid FROM ${schema_name}._sc_facilities f2,re e WHERE	e.ID = f2.parentid AND f2.org_type <= 2 " +
////            "           ) " +
////            "           SELECT mc.asset_code FROM ${schema_name}._sc_asset_monitor_current AS mc " +
////            "           INNER JOIN ${schema_name}._sc_asset AS t1 ON t1.strcode = mc.asset_code " +
////            "           INNER JOIN ${schema_name}._sc_asset_organization o ON o.asset_id = t1._id " +
////            "           INNER JOIN re e ON e.ID = o.org_id " +
////            "           UNION " +
////            "           SELECT mc.asset_code FROM ${schema_name}._sc_asset_monitor_current AS mc " +
////            "           INNER JOIN ${schema_name}._sc_asset AS t1 ON t1.strcode = mc.asset_code " +
////            "           INNER JOIN ${schema_name}._sc_asset_position_organization AS po ON po.position_code = t1.position_code " +
////            "           INNER JOIN re e ON e.ID = po.org_id " +
////            "       ) T " +
////            "   ) as count " +
////            "   FROM ${schema_name}._sc_facilities A WHERE A.isuse = TRUE AND A.org_type <= 2 AND A.parentid = #{facilityId} " +
////            ") tb " +
////            "WHERE tb.count > 0 ORDER BY tb.ID DESC")
//    @Select("SELECT tt.ID,tt.COUNT,CAST(ff.LOCATION AS VARCHAR) AS LOCATION " +
//            "FROM ( " +
//            "   SELECT A.ID,COUNT(T.strcode) AS COUNT " +
//            "   FROM ${schema_name}._sc_facilities	A " +
//            "   INNER JOIN ( " +
//            "       SELECT t1.strcode,f.ID,f.facility_no " +
//            "       FROM ${schema_name}._sc_asset_iot_setting AS mc " +
//            "       INNER JOIN ${schema_name}._sc_asset AS t1 ON t1._id = mc.asset_id AND mc.iot_status<>1 AND mc.out_code NOTNULL " +
//            "       INNER JOIN ${schema_name}._sc_asset_organization o ON o.asset_id = t1._id " +
//            "       INNER JOIN ${schema_name}._sc_facilities f ON f.ID = o.org_id " +
//            "       UNION " +
//            "       SELECT t1.strcode,f.ID,f.facility_no " +
//            "       FROM ${schema_name}._sc_asset_iot_setting AS mc " +
//            "       INNER JOIN ${schema_name}._sc_asset AS t1 ON t1._id = mc.asset_id AND mc.iot_status<>1 AND mc.out_code NOTNULL " +
//            "       INNER JOIN ${schema_name}._sc_asset_position_organization AS po ON po.position_code = t1.position_code " +
//            "       INNER JOIN ${schema_name}._sc_facilities f ON f.ID = po.org_id " +
//            "   ) T ON T.facility_no LIKE A.facility_no || '%' " +
//            "   WHERE A.isuse = TRUE AND A.org_type in ("+ SqlConstant.FACILITY_INSIDE +") AND (A.parentid = #{facilityId} OR   A.id = #{facilityId}) GROUP BY A.ID " +
//            ") tt " +
//            "INNER JOIN ${schema_name}._sc_facilities ff ON ff.ID = tt.ID ")
//    List<Map<String, Object>> getChildFacilityWithMonitor(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId);
}
