package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface PolluteSurveyRecordMapper {

    /**
     *
     * @param schema_name
     * @param check_meter_id
     * @return
     */
    @Select(" <script>" +
            " select * from ${schema_name}._sc_pollute_survey_record s " +
            " where check_meter_id = #{check_meter_id} " +
            " </script>")
    List<Map<String,Object>> findPolluteSurveyRecordByCheckMeterId(@Param("schema_name") String schema_name, @Param("check_meter_id") Integer check_meter_id);

    /**
     *
     * @param schema_name
     * @param paramMap
     * @return
     */
    @Select(" <script>" +
            " select s.* from ${schema_name}._sc_pollute_survey_record s " +
            " where s.ratio >1 " +
            " and to_char(s.belong_date,'yyyy-MM') = #{pm.belong_month} and s.facility_id = #{pm.facility_id} " +
            " </script>")
    List<Map<String, Object>> findPolluteSurveyGroupList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Select(" <script>" +
            " select s.* from ${schema_name}._sc_pollute_survey_record s " +
            " where s.ratio > 0 and s.check_meter_id is not null " +
            " and to_char(s.belong_date,'yyyy-MM-dd') between #{pm.begin_time} and #{pm.end_time} and s.facility_id = #{pm.facility_id} ::int " +
            " </script>")
    List<Map<String, Object>> findPolluteSurveyGroupListByDay(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);
}
