package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 5:54 2019/9/17 0017
 */
@Mapper
public interface TaskReleaseMapper {

//    @Insert("insert into ${schema_name}._sc_work_task (task_title,task_content, begin_date, end_date, task_rate, important_level, deliverables," +
//            "deliverable_day, deliverable_time, audit_man, is_use, create_user_account, create_time, task_require,template_files) values (" +
//            "#{task_title},#{task_content},#{begin_date}::timestamp,#{end_date}::timestamp,#{task_rate}::int,#{important_level}::int," +
//            "#{deliverables},#{deliverable_day},#{deliverable_time}::time," +
//            "#{audit_man},#{is_use}::bool,#{create_user_account},#{create_time}::timestamp,#{task_require},#{template_files})")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int doSubmit(Map<String, Object> paramMap);
//
//    /**
//     * @param schemaName
//     * @return
//     * @Description：查询所有任务当前时间大于开始时间小于结束时间任务模板
//     */
//    @SelectProvider(type = TaskReleaseProvider.class, method = "selectAllRelease")
//    List<Map<String, Object>> selectAllRelease(@Param("schema_name") String schemaName);
//
//    @Select("SELECT DISTINCT " +
//            "u.account as account,u.username as username FROM " +
//            "${schema_name}._sc_work_task_receiver AS tr " +
//            "LEFT JOIN ${schema_name}._sc_user_group AS ug ON ug.group_id = tr.group_id " +
//            "LEFT JOIN ${schema_name}._sc_user_role AS r ON r.role_id = tr.role_id " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = r.user_id " +
//            "AND u.ID = ug.user_id " +
//            "WHERE " +
//            "u.is_out = FALSE " +
//            "AND u.status = 1 " +
//            "AND ug.user_id = r.user_id " +
//            "AND task_id = ${taskId}")
//    List<Map<String, Object>> searchReceiveAccountInfo(@Param("schema_name") String schemaName, @Param("taskId") int taskId);
//
//    @SelectProvider(type = TaskReleaseProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                    @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                    @Param("searchKey") String searchKey, @Param("groups") String groups, @Param("idPrefix") String idPrefix);
//
//    /**
//     * @param schemaName
//     * @param searchKey
//     * @param groups
//     * @return
//     * @Description:任务发布列表工单数据量统计
//     */
//    @SelectProvider(type = TaskReleaseProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("groups") String groups);
//
//    /**
//     * @param schemaName
//     * @param paramMap
//     * @Description: 任务接受
//     */
//    @InsertProvider(type = TaskReleaseProvider.class, method = "insertReceiver")
//    void doSubmitReceiver(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schemaName
//     * @param paramMap
//     * @Description:生成完成工单
//     */
//    @InsertProvider(type = TaskReleaseProvider.class, method = "addFinished")
//    int addFinished(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @return
//     * @Description:任务提交，执行流程
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "updateSubmit")
//    int updateSubmit(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @return
//     * @Description:任务评价结束流程
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "updateFinished")
//    int updateFinished(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * @param schema_name
//     * @param id
//     * @param is_use
//     * @return
//     * @Description: 禁用启用
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "update")
//    int doSubmitTaskRelease(@Param("schema_name") String schema_name, @Param("id") String id, @Param("is_use") boolean is_use);
//
//    @SelectProvider(type = TaskReleaseProvider.class, method = "queryTaskReleaseById")
//    Map<String, Object> queryTaskReleaseById(@Param("schema_name") String schemaName, @Param("code") Integer code);
//
//
//    /**
//     * @param schema_name
//     * @param paramMap
//     * @param id
//     * @return
//     * @Description: 更新表单操作
//     */
//    @UpdateProvider(type = TaskReleaseProvider.class, method = "doUpdate")
//    int doUpdate(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap, @Param("id") String id);
//
//    @DeleteProvider(type = TaskReleaseProvider.class, method = "deleteReceive")
//    int deleteReceive(@Param("schema_name") String schema_name, @Param("id") String id);
//
//    /**
//     * 根据任务编号获取用户组角色信息
//     *
//     * @param schemaName
//     * @param taskId
//     * @return
//     */
//    @Select("select t.group_id, t.role_id, g.group_name, r.name as role_name " +
//            "from ${schema_name}._sc_work_task_receiver t " +
//            "LEFT JOIN ${schema_name}._sc_group g on t.group_id = g.id " +
//            "LEFT JOIN ${schema_name}._sc_role r on t.role_id = r.id and r.available = true " +
//            "where t.task_id = #{taskId} " +
//            "ORDER BY t.group_id, t.role_id ")
//    List<Map<String, Object>> getGroupRoleListByTask(@Param("schema_name") String schemaName, @Param("taskId") Integer taskId);
//
//
//    class TaskReleaseProvider {
//        public String insertReceiver(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_work_task_receiver");
//                VALUES("task_id ", "#{s.task_id}::int");
//                VALUES("group_id ", "#{s.group_id}::int");
//                VALUES("role_id ", "#{s.role_id}");
//            }}.toString();
//        }
//
//        public String addFinished(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_work_task_finished");
//                VALUES("sub_work_code ", "#{s.sub_work_code}");
//                VALUES("task_id ", "#{s.task_id}::int");
//                VALUES("deliverables ", "#{s.deliverables}");
//                VALUES("create_user_account ", "#{s.create_user_account}");
//                VALUES("create_time ", "#{s.create_time}::timestamp");
//                VALUES("deadline_time ", "#{s.deadline_time}::timestamp");
//                VALUES("duty_man ", "#{s.duty_man}");
//                VALUES("audit_man ", "#{s.audit_man}");
//                VALUES("task_content ", "#{s.task_content}");
//                VALUES("task_rate ", "#{s.task_rate}");
//                VALUES("important_level ", "#{s.important_level}");
//                VALUES("task_require ", "#{s.task_require}");
//                VALUES("deliverable_day ", "#{s.deliverable_day}");
//                VALUES("deliverable_time ", "#{s.deliverable_time}");
//                VALUES("is_finished ", "0");
//                VALUES("task_title ", "#{s.task_title}");
//            }}.toString();
//        }
//
//
//        public String update(@Param("schema_name") String schema_name, @Param("id") String id, @Param("is_use") boolean is_use) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task");
//                SET("is_use = #{is_use}::bool");
//                if (id != null) {
//                    WHERE("id = #{id}::int");
//                }
//            }}.toString();
//        }
//
//        public String updateSubmit(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task_finished");
//                SET("deliverables = #{s.deliverables}");
//                SET("deliverables_files = #{s.deliverables_files}");
//                SET("finished_content = #{s.finished_content}");
//                SET("deadline_time = #{s.deadline_time}::timestamp");
//                WHERE("id = #{s.id}::int");
//            }}.toString();
//        }
//
//        public String updateFinished(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_work_task_finished");
//                SET("body_property = #{s.body_property}::jsonb");
//                WHERE("id = #{s.id}::int");
//            }}.toString();
//        }
//
//        public String deleteReceive(@Param("schema_name") String schema_name, @Param("id") String id) {
//            return new SQL() {{
//                DELETE_FROM(schema_name + "._sc_work_task_receiver");
//                WHERE("task_id = #{id}::int");
//            }}.toString();
//        }
//
//        public String queryTaskReleaseById(@Param("schema_name") String schemaName, @Param("code") Integer code) {
//            return new SQL() {{
//                SELECT("a.*");
//                FROM(schemaName + "._sc_work_task a ");
//                WHERE("id = #{code}::int");
//            }}.toString();
//        }
//
//        public String selectAllRelease(@Param("schema_name") String schemaName) {
//            return new SQL() {{
//                SELECT("t.id,t.task_title,t.audit_man,t.task_content,t.begin_date,t.end_date,t.task_rate,t.important_level," +
//                        "t.deliverables,t.task_require,t.deliverable_day,t.deliverable_time,t.create_user_account," +
//                        "array_to_string(array_agg(r.role_id),',') as role_ids ");
//                FROM(schemaName + "._sc_work_task as t " +
//                        "left join " + schemaName + "._sc_work_task_receiver as r on r.task_id=t.id");
//                WHERE("t.is_use = true and now()>= t.begin_date and now()<t.end_date");
//                GROUP_BY("t.id,t.task_title,t.audit_man,t.task_content,t.begin_date,t.end_date,t.task_rate,t.important_level," +
//                        "t.deliverables,t.task_require,t.deliverable_day,t.deliverable_time,t.create_user_account");
//            }}.toString();
//        }
//
//        public String searchReceiveAccount(@Param("schema_name") String schemaName, @Param("taskId") int taskId) {
//            return new SQL() {{
//                SELECT("DISTINCT u.account, u.* ");
//                FROM(schemaName + "._sc_work_task_receiver as t " +
//                        "left join " + schemaName + "._sc_user_group as ug on ug.group_id=t.group_id " +
//                        "left join " + schemaName + "._sc_user_role as r on r.role_id = t.role_id " +
//                        "left join " + schemaName + "._sc_user as u on u.id= r.user_id and u.id = ug.user_id ");
//                WHERE(" u.is_out=false and status=1 and ug.user_id=r.user_id and task_id = #{taskId}::int");
//            }}.toString();
//        }
//
//        public String doUpdate(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap, @Param("id") String id) {
//            return new SQL() {{
//                UPDATE(schemaName + "._sc_work_task");
//                SET("task_title = #{s.task_title}");
//                SET("task_content = #{s.task_content}");
//                SET("begin_date = #{s.begin_date}::timestamp");
//                SET("end_date = #{s.end_date}::timestamp");
//                SET("task_rate = #{s.task_rate}::int");
//                SET("important_level = #{s.important_level}::int");
//                SET("deliverables = #{s.deliverables}");
//                SET("deliverable_day = #{s.deliverable_day}");
//                SET("deliverable_time = #{s.deliverable_time}::time");
//                SET("audit_man = #{s.audit_man}");
//                SET("is_use = #{s.is_use}::bool");
//                SET("template_files = #{s.template_files}");
//                SET("task_require = #{s.task_require}");
//                if (id != null) {
//                    WHERE("id = #{id}::int");
//                }
//            }}.toString();
//        }
//
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("searchKey") String searchKey,
//                            @Param("groups") String groups, @Param("idPrefix") String idPrefix) {
//            return new SQL() {{
//                String condition = "is_use='t' ";
//                SELECT("#{idPrefix} || a.id as task_id,a.id,a.task_title, a.task_content, a.begin_date, a.end_date, a.task_rate, " +
//                        "a.important_level, a.deliverables, a.deliverable_day, a.deliverable_time, a.task_require, a.audit_man, " +
//                        "a.is_use, a.create_user_account, a.create_time,c.username as create_user,d.username as audit_man_name," +
//                        "array_to_string(array_agg(DISTINCT g.group_name),',') as group_name");
//                FROM(schemaName + "._sc_work_task as a " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account " +
//                        "LEFT JOIN " + schemaName + "._sc_user d ON d.account=a.audit_man " +
//                        "left join " + schemaName + "._sc_work_task_receiver as wr on wr.task_id = a.id " +
//                        "left join " + schemaName + "._sc_group as g on g.id=wr.group_id ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition += "and upper(a.task_title) like upper('%" + searchKey
//                            + "%') or upper(a.task_content) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey + "%') " +
//                            " or upper(d.username) like upper('%" + searchKey + "%') ";
//                }
////                if (null != beginTime && !"".equals(beginTime)) {
////                    condition += " and a.begin_date>='" + Timestamp.valueOf(beginTime + " 00:00:00") + "'";
////                }
//                if (null != groups && !"".equals(groups) && !"0".equals(groups)) {
//                    condition += " and wr.group_id=" + groups;
//                }
//                WHERE(condition);
//                GROUP_BY(" a.id, a.task_title, a.task_content, a.begin_date, a.end_date, a.task_rate, " +
//                        "a.important_level, a.deliverables, a.deliverable_day,a.deliverable_time, a.task_require, a.audit_man, " +
//                        "a.is_use, a.create_user_account, a.create_time,c.username,d.username ");
//                ORDER_BY("a." + orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey,
//                                       @Param("groups") String groups) {
//            return new SQL() {{
//                String condition = "is_use='t' ";
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_work_task a " +
//                        "LEFT JOIN " + schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition += "and upper(a.task_title) like upper('%" + searchKey
//                            + "%') or upper(a.task_content) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey + "%') ";
//                }
//                /*if (null != beginTime && !"".equals(beginTime)) {
//                    condition += " and a.begin_date>='" + Timestamp.valueOf(beginTime + " 00:00:00") + "' ";
//                }*/
////                if(null != deliverable_type && !"".equals(deliverable_type)&&!"0".equals(deliverable_type)){
////                    condition+=" and a.deliverable_type="+deliverable_type;
////                }
//                WHERE(condition);
//            }}.toString();
//        }
//    }
//
//    @Select("select colldata.group_name,colldata.username,colldata.roles_name,colldata.user_code,colldata.duty_man, " +
//            "sum(case when colldata.finished_time is not null then 1 else 0 end) as finished_times, " +
//            "sum(case when colldata.finished_time is null then 1 else 0 end) as ongoing_times, " +
//            "sum(case when colldata.is_finished=1 then 1 else 0 end) as qualified_times, " +
//            "sum(case when colldata.is_finished=2 then 1 else 0 end) as unqualified_times, " +
//            "sum(case when colldata.finished_time is not null and substr(TO_CHAR(colldata.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "'), 11,17)>TO_CHAR(colldata.deliverable_time,'" + SqlConstant.SQL_TIME_FMT + "') then 1 else 0 end) as delay_times " +
//            "from ( " +
//            "select ARRAY_TO_STRING(ARRAY_AGG(DISTINCT sg.group_name), ',') as group_name, ARRAY_TO_STRING(ARRAY_AGG(DISTINCT sr.name), ',') as roles_name, " +
//            "su.username, wtf.finished_time,wtf.duty_man,wtf.deliverable_day,wtf.deliverable_time,wtf.task_rate,su.user_code, " +
//            "wtf.is_finished,wtf.deadline_time,wtf.sub_work_code  " +
//            "from ${schema_name}._sc_work_task_finished as wtf " +
//            "left join ${schema_name}._sc_user as su on su.account=wtf.duty_man " +
//            "left join ${schema_name}._sc_user_role as ur on su.id=ur.user_id " +
//            "left join ${schema_name}._sc_role as sr on sr.id=ur.role_id " +
//            "left join ${schema_name}._sc_user_group as ug on su.id=ug.user_id " +
//            "left join ${schema_name}._sc_group as sg on sg.id=ug.group_id " +
//            "left join ${schema_name}._sc_work_task as wt on wt.id = wtf.task_id " +
//            "where 1=1 ${condition} " +
//            "group by wtf.create_user_account,wtf.sub_work_code,su.id,wtf.finished_time,wtf.deadline_time, " +
//            "wtf.is_finished,wtf.deliverable_day, wtf.deliverable_time, wtf.task_rate,wtf.duty_man " +
//            ") as colldata " +
//            "GROUP BY colldata.group_name,colldata.username,colldata.roles_name,colldata.user_code,colldata.duty_man " +
//            "limit ${pageSize} offset ${begin}")
//    List<Map<String, Object>> findWorkTaskAnalys(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("pageSize") int pageSize, @Param("begin") int begin);
//
//    @Select("select count(*) from " +
//            "( " +
//            "select colldata.group_name,colldata.username,colldata.roles_name,colldata.user_code,colldata.duty_man from ( " +
//            "select ARRAY_TO_STRING(ARRAY_AGG(DISTINCT sg.group_name), ',') as group_name,ARRAY_TO_STRING(ARRAY_AGG(DISTINCT sr.name), ',') as roles_name," +
//            "su.username, wtf.finished_time,wtf.duty_man,wtf.deliverable_day,wtf.deliverable_time,wtf.task_rate,su.user_code," +
//            "wtf.is_finished,wtf.deadline_time,wtf.sub_work_code " +
//            "from ${schema_name}._sc_work_task_finished as wtf " +
//            "left join ${schema_name}._sc_user as su on su.account=wtf.duty_man " +
//            "left join ${schema_name}._sc_user_role as ur on su.id=ur.user_id " +
//            "left join ${schema_name}._sc_role as sr on sr.id=ur.role_id " +
//            "left join ${schema_name}._sc_user_group as ug on su.id=ug.user_id " +
//            "left join ${schema_name}._sc_group as sg on sg.id=ug.group_id " +
//            "left join ${schema_name}._sc_work_task as wt on wt.id = wtf.task_id " +
//            "where 1=1 ${condition} " +
//            "group by wtf.create_user_account,wtf.sub_work_code,su.id,wtf.finished_time,wtf.deadline_time, " +
//            "wtf.is_finished,wtf.deliverable_day,wtf.deliverable_time,wtf.task_rate,wtf.duty_man " +
//            ") as colldata " +
//            "GROUP BY colldata.group_name,colldata.username,colldata.roles_name,colldata.user_code,colldata.duty_man " +
//            ") as rets")
//    int countWorkTaskAnalys(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     * 任务分析的详情信息
//     *
//     * @param schema_name
//     * @param duty_man
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("select wtf.sub_work_code,su.username,su.user_code,ARRAY_TO_STRING(ARRAY_AGG(DISTINCT sg.group_name), ',') as group_name, " +
//            "wtf.deliverables,wtf.is_finished,wtf.deadline_time " +
//            "from ${schema_name}._sc_work_task_finished as wtf " +
//            "left join ${schema_name}._sc_user as su on su.account= wtf.duty_man " +
//            "left join ${schema_name}._sc_user_group as sug on sug.user_id=su.id " +
//            "left join ${schema_name}._sc_group as sg on sg.id=sug.group_id " +
//            "where wtf.duty_man=#{duty_man} ${condition} " +
//            "GROUP BY wtf.sub_work_code,su.username,su.user_code,wtf.deliverables,wtf.is_finished,wtf.deadline_time " +
//            "order by wtf.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> findWorkTaskAnalysDetail(@Param("schema_name") String schema_name, @Param("duty_man") String duty_man, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 任务分析列表总数
//     *
//     * @param schema_name
//     * @param duty_man
//     * @param condition
//     * @return
//     */
//    @Select("select count(task_d.sub_work_code) from (" +
//            "select wtf.sub_work_code,su.username,su.user_code,ARRAY_TO_STRING(ARRAY_AGG(DISTINCT sg.group_name), ',') as group_name, " +
//            "wtf.deliverables,wtf.is_finished,wtf.deadline_time " +
//            "from ${schema_name}._sc_work_task_finished as wtf " +
//            "left join ${schema_name}._sc_user as su on su.account= wtf.duty_man " +
//            "left join ${schema_name}._sc_user_group as sug on sug.user_id=su.id " +
//            "left join ${schema_name}._sc_group as sg on sg.id=sug.group_id " +
//            "where wtf.duty_man=#{duty_man} ${condition} " +
//            "GROUP BY wtf.sub_work_code,su.username,su.user_code,wtf.deliverables,wtf.is_finished,wtf.deadline_time" +
//            ") task_d")
//    int countWorkTaskAnalysDetail(@Param("schema_name") String schema_name, @Param("duty_man") String duty_man, @Param("condition") String condition);
}
