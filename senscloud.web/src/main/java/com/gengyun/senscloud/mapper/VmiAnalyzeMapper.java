package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VmiAnalyzeMapper {
//
//    /**
//     * 设备列表
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT f.short_title,f.facilitycode,asset.strname,asset.strcode,am.model_name,asset._id as assetId,f.id as facilitiesId, " +
//            "(COALESCE(i.tem_value1,0) + COALESCE(i.tem_value2,0) + COALESCE(i.tem_value3,0)) AS capacity, " +
//            "(SELECT monitor_value FROM ${schema_name}._sc_asset_monitor_current WHERE monitor_name = '"+Constants.ASSET_MONITOR_AMOUNT_TANK_VOLUME+"' AND asset_code = asset.strcode ORDER BY gather_time DESC LIMIT 1 ) AS tank_volume, " +
//            "ARRAY_TO_STRING(ARRAY_AGG(avac.greater || ',' || avac.less ||',' || avac.greater_value ||',' || avac.less_value ||',' || avac.equation), '|') AS colorlist " +
//            " FROM ${schema_name}._sc_asset as asset " +
//            " LEFT JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_iot_setting i ON i.asset_id = asset._id " +
//            " LEFT JOIN ${schema_name}._sc_asset_model am ON am.id = asset.asset_model_id " +
//            " LEFT JOIN ${schema_name}._sc_asset_vmi_alarm_color avac ON avac.asset_id = asset._id " +
//            " WHERE ac.category_name = '" + Constants.ASSET_CATEGORY_NAME_BTS + "' " +
//            " AND f.isuse = TRUE AND f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") " +
//            " ${condition} " +
//            " GROUP BY f.short_title,f.facilitycode,asset.strname,asset.strcode,am.model_name,assetId,facilitiesId,capacity,tank_volume " +
//            "order by asset.createtime desc,asset.strcode desc limit ${page} offset ${begin}")
//    public List<VmiAssetModel> findVmiAssetList(@Param("schema_name") String schema_name , @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 设备列表总数
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) from (" +
//            " SELECT asset.strcode " +
//            " FROM ${schema_name}._sc_asset as asset " +
//            " LEFT JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_iot_setting i ON i.asset_id = asset._id " +
//            " WHERE ac.category_name = '" + Constants.ASSET_CATEGORY_NAME_BTS + "' " +
//            " AND f.isuse = TRUE AND f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") " +
//            "${condition} " +
//            ") t")
//    public int findVmiAssetListNum(@Param("schema_name") String schema_name ,@Param("condition") String condition);
//
//    /**
//     * 查询设备、客户的详情信息
//     * @param schema_name
//     * @param facilitiesId
//     * @param assetId
//     * @return
//     */
//    @Select("SELECT f.short_title,f.facilitycode,asset.strname,asset.strcode,am.model_name,asset._id as assetId,asset.category_id,f.id as facilitiesId,ac.category_name, " +
//            "(COALESCE(i.tem_value1,0) + COALESCE(i.tem_value2,0) + COALESCE(i.tem_value3,0)) AS capacity, " +
//            "(SELECT monitor_value FROM ${schema_name}._sc_asset_monitor_current WHERE monitor_name = '"+Constants.ASSET_MONITOR_AMOUNT_TANK_VOLUME+"' AND asset_code = asset.strcode ORDER BY gather_time DESC LIMIT 1 ) AS tank_volume " +
//            " FROM ${schema_name}._sc_asset as asset " +
//            " LEFT JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_iot_setting i ON i.asset_id = asset._id " +
//            " LEFT JOIN ${schema_name}._sc_asset_model am ON am.id = asset.asset_model_id " +
//            " WHERE asset._id = '${assetId}' and f.id = ${facilitiesId} ")
//    public VmiAssetModel findVmiAssetByFacilitiesIdAndAssetId(@Param("schema_name") String schema_name,@Param("facilitiesId") Long facilitiesId,@Param("assetId") String assetId);
//
//    /**
//     * 报表列表
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT f.short_title,f.facilitycode,asset.strname,asset.strcode,am.model_name,asset._id as assetId,f.id as facilitiesId" +
//            "   ,COALESCE ((SUM(CAST( m.monitor_value AS NUMERIC )) ),0 ) AS total_receive_actual " +
//            "FROM ${schema_name}._sc_asset as asset " +
//            "   LEFT JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            "   LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            "   LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id " +
//            "   LEFT JOIN ${schema_name}._sc_asset_model am ON am.id = asset.asset_model_id " +
//            "   LEFT JOIN ${schema_name}._sc_asset_monitor m ON m.asset_code = asset.strcode AND m.monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"' ${dateCondition} " +
//            "WHERE ac.category_name = '" + Constants.ASSET_CATEGORY_NAME_BTS + "' " +
//            "   AND f.isuse = TRUE AND f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") " +
//            "    ${condition} " +
//            "GROUP BY f.short_title,f.facilitycode,asset.strname,asset.strcode,am.model_name,asset._id,f.ID " +
//            "ORDER BY asset.createtime desc,asset.strcode desc limit ${page} offset ${begin}")
//    public List<VmiAssetModel> findVmiReportList(@Param("schema_name") String schema_name , @Param("condition") String condition,@Param("dateCondition") String dateCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 报表列表总数
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) from (" +
//            " SELECT asset.strcode " +
//            " FROM ${schema_name}._sc_asset as asset " +
//            " LEFT JOIN ${schema_name}._sc_asset_category as ac ON asset.category_id = ac.id " +
//            " LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id " +
//            " AND f.isuse = TRUE AND f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") " +
//            " WHERE ac.category_name = '" + Constants.ASSET_CATEGORY_NAME_BTS + "' " +
//            "${condition} " +
//            ") t")
//    public int findVmiReportListNum(@Param("schema_name") String schema_name ,@Param("condition") String condition);
//
//    /**
//     * 更新vmi物流信息同步状态
//     * @param schema_name
//     * @param assetId
//     * @param orderNo
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_asset_vmi_transfer SET is_sync = true WHERE asset_id = #{assetId} AND order_no = #{orderNo} AND is_sync = false ")
//    public int updateAssetVmiTransfer(@Param("schema_name") String schema_name ,@Param("assetId") String assetId, @Param("orderNo") String orderNo);
//
//    /**
//     * 查询设备物流信息列表
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT avt.order_no,avt.user_total,iot.in_code,avt.asset_id,avt.buy_date FROM ${schema_name}._sc_asset_vmi_transfer avt " +
//            "INNER JOIN ${schema_name}._sc_asset_iot_setting iot ON iot.asset_id = avt.asset_id " +
//            "WHERE avt.is_sync = false " +
//            "ORDER BY avt.create_time DESC limit ${page} offset ${begin}")
//    public List<Map<String, Object>> getVmiIotList(@Param("schema_name") String schema_name, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询设备物流信息
//     * @param schema_name
//     * @param orderNo
//     * @return
//     */
//    @Select("SELECT avt.order_no,avt.user_total,iot.in_code,avt.asset_id,avt.buy_date FROM ${schema_name}._sc_asset_vmi_transfer avt " +
//            "INNER JOIN ${schema_name}._sc_asset_iot_setting iot ON iot.asset_id = avt.asset_id " +
//            "WHERE avt.order_no=#{order_no} AND avt.is_sync = false ")
//    public Map<String, Object> getVmiIot(@Param("schema_name") String schema_name, @Param("order_no") String orderNo);
//
//    /**
//     * 查询设备物流信息列表数量
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_asset_vmi_transfer avt " +
//            "INNER JOIN ${schema_name}._sc_asset_iot_setting iot ON iot.asset_id = avt.asset_id WHERE avt.is_sync = false ")
//    public int countVmiIotList(@Param("schema_name") String schema_name);
//
//    /**
//     * 根据设备code，查询设备物流信息列表
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Select("SELECT avt.* FROM ${schema_name}._sc_asset_vmi_transfer avt " +
//            "LEFT JOIN ${schema_name}._sc_asset a ON avt.asset_id = a._id " +
//            "WHERE a.strcode = #{assetCode} ORDER BY avt.create_time DESC limit ${page} offset ${begin}")
//    public List<Map<String, Object>> getAssetVmiTransferList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据设备code，查询设备物流信息列表总数
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Select("SELECT count(1) " +
//            "FROM ${schema_name}._sc_asset_vmi_transfer avt " +
//            "LEFT JOIN ${schema_name}._sc_asset a ON avt.asset_id = a._id " +
//            "WHERE a.strcode = #{assetCode} ")
//    public int countAssetVmiTransferList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode);
//
//    /**
//     * 根据设备code，查询设备入库信息列表
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
////    @Select("SELECT DISTINCT a.monitor_value as receive_start_time,b.monitor_value as receive_actual " +
////            "FROM (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_START_TIME+"' AND asset_code = #{assetCode} ) a " +
////            "INNER JOIN (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"' AND asset_code = #{assetCode} ) b " +
////            "ON a.gather_time = b.gather_time ORDER BY receive_start_time DESC limit ${page} offset ${begin}")
//    @Select("SELECT " +
//            "   MAX(CASE WHEN monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_START_TIME+"' THEN monitor_value ELSE '' END) AS receive_start_time, " +
//            "   MAX(CASE WHEN monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"' THEN monitor_value ELSE '' END) AS receive_actual " +
//            "FROM ${schema_name}._sc_asset_monitor " +
//            "WHERE (monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_START_TIME+"' OR monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"') AND asset_code = #{assetCode} " +
//            "GROUP BY gather_time ORDER BY receive_start_time DESC " +
//            "LIMIT ${page} OFFSET ${begin} ")
//    public List<Map<String, Object>> getAssetStockInList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据设备code，查询设备入库信息列表总数
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
////    @Select("SELECT count(DISTINCT a.monitor_value) " +
////            "FROM (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_START_TIME+"' AND asset_code = #{assetCode} ) a " +
////            "INNER JOIN (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"' AND asset_code = #{assetCode} ) b " +
////            "ON a.gather_time = b.gather_time")
//    @Select("SELECT count(1) FROM (" +
//            "   SELECT gather_time " +
//            "   FROM ${schema_name}._sc_asset_monitor " +
//            "   WHERE (monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_START_TIME+"' OR monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"') AND asset_code = #{assetCode} " +
//            "   GROUP BY gather_time " +
//            ") t ")
//    public int countAssetStockInList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode);
//
//    /**
//     * 根据设备code，查询设备入库量合计
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Select("SELECT SUM(CAST(monitor_value AS NUMERIC)) FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_RECEIVE_ACTUAL+"' AND asset_code = #{assetCode}")
//    public String totalAssetStockInList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode);
//
//    /**
//     * 根据设备code，字段名称查询一段时间的数据量最大值
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT substring (gather_time from 1 for 13 )as gather_time, MAX(CAST(monitor_value AS NUMERIC))as val FROM ${schema_name}._sc_asset_monitor  WHERE 1=1 ${condition} group by substring (gather_time from 1 for 13) order by gather_time")
//    public List<Map<String,Object>> getTotalByTime (@Param("schema_name") String schema_name ,@Param("condition") String condition);
//
//    /**
//     * 根据设备code，查询设备出库信息列表
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
////    @Select("SELECT DISTINCT a.monitor_value as loading_start_time,b.monitor_value as loading_total " +
////            "FROM (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_LOADING_START_TIME+"' AND asset_code = #{assetCode} ) a " +
////            "INNER JOIN (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_LOADING_TOTAL+"' AND asset_code = #{assetCode} ) b " +
////            "ON a.gather_time = b.gather_time ORDER BY loading_start_time DESC limit ${page} offset ${begin}")
//    @Select("SELECT " +
//            "   MAX(CASE WHEN monitor_name = '"+Constants.ASSET_MONITOR_LOADING_START_TIME+"' THEN monitor_value ELSE '' END) AS loading_start_time, " +
//            "   MAX(CASE WHEN monitor_name = '"+Constants.ASSET_MONITOR_LOADING_TOTAL+"' THEN monitor_value ELSE '' END) AS loading_total " +
//            "FROM ${schema_name}._sc_asset_monitor " +
//            "WHERE (monitor_name = '"+Constants.ASSET_MONITOR_LOADING_START_TIME+"' OR monitor_name = '"+Constants.ASSET_MONITOR_LOADING_TOTAL+"') AND asset_code = #{assetCode} " +
//            "GROUP BY gather_time ORDER BY loading_start_time DESC " +
//            "LIMIT ${page} OFFSET ${begin} ")
//    public List<Map<String, Object>> getAssetStockOutList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据设备code，查询设备出库信息列表总数
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
////    @Select("SELECT count(DISTINCT a.monitor_value) " +
////            "FROM (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_LOADING_START_TIME+"' AND asset_code = #{assetCode} ) a " +
////            "INNER JOIN (SELECT * FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_LOADING_TOTAL+"' AND asset_code = #{assetCode} ) b " +
////            "ON a.gather_time = b.gather_time")
//    @Select("SELECT count(1) FROM (" +
//            "   SELECT gather_time " +
//            "   FROM ${schema_name}._sc_asset_monitor " +
//            "   WHERE (monitor_name = '"+Constants.ASSET_MONITOR_LOADING_START_TIME+"' OR monitor_name = '"+Constants.ASSET_MONITOR_LOADING_TOTAL+"') AND asset_code = #{assetCode} " +
//            "   GROUP BY gather_time " +
//            ") t ")
//    public int countAssetStockOutList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode);
//
//    /**
//     * 根据设备code，查询设备出库量合计
//     * @param schema_name
//     * @param assetCode
//     * @return
//     */
//    @Select("SELECT SUM(CAST(monitor_value AS NUMERIC)) FROM ${schema_name}._sc_asset_monitor WHERE monitor_name = '"+Constants.ASSET_MONITOR_LOADING_TOTAL+"' AND asset_code = #{assetCode}")
//    public String totalAssetStockOutList(@Param("schema_name") String schema_name ,@Param("assetCode") String assetCode);
//
//    /**
//     *  录入vmi物流信息
//     * @param schema_name
//     * @param assetId
//     * @param facilitiesId
//     * @param user_total
//     * @param buy_date
//     * @param create_time
//     * @param create_user_account
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_asset_vmi_transfer (order_no, asset_id,org_id,user_total,buy_date,create_time,create_user_account) values " +
//            "(#{orderNo},#{assetId},#{facilitiesId}::int,#{user_total},#{buy_date}::timestamp,#{create_time},#{create_user_account})")
//    int addVmiLogistics(@Param("schema_name") String schema_name, @Param("orderNo") String orderNo, @Param("assetId") String assetId, @Param("facilitiesId") String facilitiesId, @Param("user_total")Double user_total,
//                        @Param("buy_date")String buy_date, @Param("create_time") Timestamp create_time, @Param("create_user_account") String create_user_account);
//
//    /**
//     * 物流发货
//     * @param schema_name
//     * @param order_no
//     * @param send_date
//     * @param arrive_date
//     * @param send_account
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_vmi_transfer set send_date=#{send_date}::timestamp,arrive_date=#{arrive_date}::timestamp,send_account=#{send_account} where order_no=#{order_no}")
//    int sendVmiLogistics(@Param("schema_name") String schema_name, @Param("order_no") String order_no, @Param("send_date") String send_date, @Param("arrive_date") String arrive_date, @Param("send_account")String send_account);
//
//    /**
//     * 物流签收
//     * @param schema_name
//     * @param order_no
//     * @param sign_date
//     * @param remark
//     * @param reveive_account
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_vmi_transfer set sign_date=#{sign_date}::timestamp,remark=#{remark},reveive_account=#{reveive_account} where order_no=#{order_no}")
//    int receiveVmiLogistics(@Param("schema_name") String schema_name, @Param("order_no") String order_no, @Param("sign_date") String sign_date, @Param("remark") String remark, @Param("reveive_account")String reveive_account);
//
//    /**
//     * 查询vmi设备颜色配置列表
//     * @param schema_name
//     * @param assetId
//     * @return
//     */
//    @Select("select greater,less,greater_value,less_value,equation from ${schema_name}._sc_asset_vmi_alarm_color where asset_id=#{assetId} order by id")
//    List<Map<String, Object>> getAssetColorListByAssetId(@Param("schema_name") String schema_name, @Param("assetId") String assetId);
//
//    /**
//     * 删除vmi设备颜色表
//     * @param schema_name
//     * @param assetId
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_asset_vmi_alarm_color where asset_id=#{assetId} ")
//    int deleteVmiAssetColorByAssetId(@Param("schema_name") String schema_name, @Param("assetId") String assetId);
}
