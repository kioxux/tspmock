package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @description 菜单数据处理类
 * 用于对菜单数据表的读取 插入 删除 更新
 */

@Mapper
public interface IndexMapper {
//    /*
//    * 查询公司的所有菜单
//     */
//    @Select("select _system.menu.* from _system.company_menu left join _system.menu on _system.company_menu.menu_id=_system.menu.id " +
//            "where _system.company_menu.company_id=${companyId} and _system.menu.id in (${menuId}) " +
//            "order by _system.menu.order;")
//    List<MenuData> findUserMenu(@Param("companyId") long companyId, @Param("menuId") String menuId);
//
//    /*
//    * 查询用户所属的组(组id字符串,json格式)
//     */
//    @Select("select user_group from ${schema_name}._sc_user where account='${account}';")
//    String findUserGroup(@Param("schema_name") String schema_name, @Param("account") String account);
//
//    /*
//    * 查询组的所有菜单(菜单id字符串集合,json格式)
//    * groupId：组的id集合，以逗号隔开。
//     */
//    @Select("select group_menu from ${schema_name}._sc_user_group where id in ( ${groupId} );")
//    List<String> findGroupMenu(@Param("schema_name") String schema_name, @Param("groupId") String groupId);
//
//    /*
//    * 待办事项 app,查询指定人
//     */
//    @Select("  select * from ( " +
//            "       SELECT r.repair_code as orderno,lsx.strcode,lsx.strname,'${repairTitle}' as ordertype,s.status,r.createtime,s.id " +
//            " FROM ${schema_name}._sc_repair r " +
//            "    left join ${schema_name}._sc_asset lsx on r.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=r.status " +
//            "    where ( ${repairCondition} )  or (r.receive_account='${account}' " +
//            " and (r.status=30 or r.status=40))  or (r.create_user_account='${account}' and r.status=50) " +
//            "     union  " +
//            "    SELECT m.maintain_code as orderno,lsx.strcode,lsx.strname,'${maintainTitle}' as ordertype,s.status,m.createtime,s.id " +
//            " FROM ${schema_name}._sc_maintain m " +
//            "    left join ${schema_name}._sc_asset lsx on m.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=m.status " +
//            "    where (m.maintain_account='${account}' and m.status=70) or ( ${maintainCondition} )  " +
//            "                 ) a order by createtime desc limit ${page} offset ${begin} ")
//    List<ToDoDataListResult> toDoDataList(@Param("schema_name") String schema_name, @Param("account") String account,
//                                          @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition,
//                                          @Param("page") int pageSize, @Param("begin") int begin,
//                                          @Param("repairTitle") String repairTitle, @Param("maintainTitle") String maintainTitle);
//
//
//    /*
//    * 待办事项总数 app,查询指定人
//     */
//    @Select("  select count(orderno) from ( " +
//            "       SELECT r.repair_code as orderno,lsx.strcode,lsx.strname,'${repairTitle}' as ordertype,s.status,r.createtime,s.id " +
//            " FROM ${schema_name}._sc_repair r " +
//            "    left join ${schema_name}._sc_asset lsx on r.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=r.status " +
//            "    where ( ${repairCondition} )  or (r.receive_account='${account}' " +
//            " and (r.status=30 or r.status=40))  or (r.create_user_account='${account}' and r.status=50) " +
//            "     union  " +
//            "    SELECT m.maintain_code as orderno,lsx.strcode,lsx.strname,'${maintainTitle}' as ordertype,s.status,m.createtime,s.id " +
//            " FROM ${schema_name}._sc_maintain m " +
//            "    left join ${schema_name}._sc_asset lsx on m.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=m.status " +
//            "    where (m.maintain_account='${account}' and m.status=70) or ( ${maintainCondition} )  " +
//            "                 ) a ")
//    int toDoDataListCount(@Param("schema_name") String schema_name, @Param("account") String account, @Param("repairCondition") String repairCondition,
//                          @Param("maintainCondition") String maintainCondition, @Param("repairTitle") String repairTitle,
//                          @Param("maintainTitle") String maintainTitle);
//
//
//    /*
//* 待办事项,数量
// */
//    @Select("  select * from ( " +
//            "       SELECT r.repair_code as orderno,lsx.strcode,lsx.strname,'${repairTitle}' as ordertype,s.status,r.createtime,s.id " +
//            " FROM ${schema_name}._sc_repair r " +
//            "    left join ${schema_name}._sc_asset lsx on r.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=r.status " +
//            "    where ( ${repairCondition} ) or (r.receive_account='${account}' " +
//            " and (r.status=30 or r.status=40))  or (r.create_user_account='${account}' and r.status=50) " +
//            "     union  " +
//            "    SELECT m.maintain_code as orderno,lsx.strcode,lsx.strname,'${maintainTitle}' as ordertype,s.status,m.createtime,s.id " +
//            " FROM ${schema_name}._sc_maintain m " +
//            "    left join ${schema_name}._sc_asset lsx on m.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=m.status " +
//            "    where (m.maintain_account='${account}' and m.status=70) or ( ${maintainCondition} )  " +
//            "                 ) a   ")
//    List<ToDoDataListResult> toDoDataListForNumb(@Param("schema_name") String schema_name, @Param("account") String account,
//                                                 @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition,
//                                                 @Param("repairTitle") String repairTitle, @Param("maintainTitle") String maintainTitle);
//
//
//    /*
//    * 待办事项 pc   ,查询指定人
//     */
//    /*@Select("  select * from ( " +
//            "  SELECT rwd.work_code as orderno,lsx.strcode,lsx.strname,'维修' as ordertype,s.status,rw.create_time,rw.deadline_time,s.id," +
//            "f.title,rwd.receive_account,rw.create_user_account " +
//            " FROM ${schema_name}._sc_works_detail rwd " +
//            " left join ${schema_name}._sc_works rw on rw.work_code=rwd.work_code " +
//            "    left join ${schema_name}._sc_asset lsx on rwd.work_code=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=rwd.status " +
//            "     left join  ${schema_name}._sc_facilities f on f.id=rw.facility_id " +
//            "    where rwd.work_type_id = 20 and  rwd.status!=60 and rwd.status!=100 and rwd.status!=900 " +
//            "     union  " +
//            "    SELECT wd.sub_work_code as orderno,lsx.strcode,lsx.strname,'保养' as ordertype,s.status,w.create_time,w.deadline_time,s.id," +
//            "f.title,wd.receive_account,w.create_user_account  " +
//            " FROM ${schema_name}._sc_works_detail wd " +
//            "   left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "    left join ${schema_name}._sc_asset lsx on wd.relation_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=wd.status " +
//            "     left join  ${schema_name}._sc_facilities f on f.id=w.facility_id " +
//            "    where wd.status=70 and wd.work_type_id=40 " +
//            "                 ) a order by create_time desc limit ${page} offset ${begin} ")
//    List<ToDoDataListResult> toDoDataListInPC(@Param("schema_name") String schema_name, @Param("account") String account, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition, @Param("page") int pageSize, @Param("begin") int begin);
//*/
//
//    /*
//* 待办事项 数量 pc   ,查询指定人
// */
//    /*@Select("  select count(1) from ( " +
//            "  SELECT rwd.work_code as orderno,rlsx.strcode,rlsx.strname,'维修' as ordertype,rs.status,rw.create_time,rw.deadline_time,rs.id," +
//            "rf.title,rwd.receive_account,rw.create_user_account " +
//            " FROM ${schema_name}._sc_works_detail rwd " +
//            " left join ${schema_name}._sc_works rw on rw.work_code=rwd.work_code " +
//            "    left join ${schema_name}._sc_asset rlsx on rwd.relation_id=rlsx._id " +
//            "    left join ${schema_name}._sc_status rs on rs.id=rwd.status " +
//            "     left join  ${schema_name}._sc_facilities rf on rf.id=rw.facility_id " +
//            "    where rwd.work_type_id = 20 and  rwd.status!=60 and rwd.status!=100 and rwd.status!=900 " +
//            "     union  " +
//            "    SELECT wd.sub_work_code as orderno,lsx.strcode,lsx.strname,'保养' as ordertype,s.status,w.create_time,w.deadline_time,s.id," +
//            "f.title,wd.receive_account,w.create_user_account  " +
//            " FROM ${schema_name}._sc_works_detail wd " +
//            "   left join ${schema_name}._sc_works w on w.work_code=wd.work_code " +
//            "    left join ${schema_name}._sc_asset lsx on wd.relation_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=wd.status " +
//            "     left join  ${schema_name}._sc_facilities f on f.id=w.facility_id " +
//            "    where wd.status=70 and wd.work_type_id=40 " +
//            "                 ) a ")
//    int toDoDataListCount(@Param("schema_name") String schema_name, @Param("account") String account, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition);
//*/
//
//    /*
//* 待办事项,只要类型，状态，数量
//*/
//    @Select("  select * from ( " +
//            "       SELECT '${repairTitle}' as ordertype,s.status,s.id,count(1) as  qty" +
//            " FROM ${schema_name}._sc_repair r " +
//            "    left join ${schema_name}._sc_asset lsx on r.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=r.status " +
//            "    where ( ${repairCondition} ) or (r.receive_account='${account}' " +
//            " and (r.status=30 or r.status=40))  or (r.create_user_account='${account}' and r.status=50) " +
//            "  group by s.status,s.id" +
//            "     union  " +
//            "    SELECT '${maintainTitle}' as ordertype,s.status,s.id,count(1) as  qty  " +
//            " FROM ${schema_name}._sc_maintain m " +
//            "    left join ${schema_name}._sc_asset lsx on m.asset_id=lsx._id " +
//            "    left join ${schema_name}._sc_status s on s.id=m.status " +
//            "    where (m.maintain_account='${account}' and m.status=70) or ( ${maintainCondition} )  " +
//            "  group by s.status,s.id" +
//            "                 ) a order by ordertype ,id  ")
//    List<ToDoDataListResult> toDoDataListForNumblitter(@Param("schema_name") String schema_name, @Param("account") String account,
//                                                       @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition,
//                                                       @Param("repairTitle") String repairTitle, @Param("maintainTitle") String maintainTitle);
//
//
//    /*
//    * 今日上报
//     */
//    @Select("select COUNT (1) " +
//            "FROM " +
//            "${schema_name}._sc_works as w " +
//            "LEFT JOIN ${schema_name}._sc_asset AS dv ON dv._id = w.relation_id " +
//            "where  " +
//            "w.create_time >= '${beginDate}' " +
//            "AND w.create_time <= '${endDate}' " +
//            "AND ( " +
//            "( " +
//            "w.relation_type = 2 " +
//            ")" +
//            "OR w.relation_type = 1 " +
//            "OR w.relation_type = 3 " +
//            "OR w.relation_type = 9 " +
//            ")" +
//            "AND (${repairCondition}) AND (w.work_type_id = 1)")
//    int toDoDataQuestionToday(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition, @Param("beginDate") Timestamp beginDate, @Param("endDate") Timestamp endDate);
//
//
//    /*
//    * 今日完成
//     */
//    @Select("SELECT " +
//            "COUNT (1) " +
//            "FROM " +
//            "${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_asset as dv on dv._id = wd.relation_id " +
//            "left join ${schema_name}._sc_works as w on w.work_code=wd.work_code " +
//            "WHERE " +
//            "wd.is_main = 1 " +
//            "AND wd.finished_time >= '${beginDate}' " +
//            "AND wd.finished_time <= '${endDate}' " +
//            "AND wd.status = 60 and ((wd.relation_type=2 ) or wd.relation_type=1 or wd.relation_type=3 or wd.relation_type=9 ) " +
//            "AND ( " +
//            "(${repairCondition}) " +
//            "OR (${maintainCondition}) " +
//            "OR (${inspectionCondition}) " +
//            "OR (${spotCondition}) " +
//            ")")
//    int toDoDataFinishedToday(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition, @Param("inspectionCondition") String inspectionCondition, @Param("spotCondition") String spotCondition, @Param("beginDate") Timestamp beginDate, @Param("endDate") Timestamp endDate);
//
//    /*
//    * 今日逾期
//     */
//    @Select("SELECT " +
//            "COUNT (1) " +
//            "FROM " +
//            "${schema_name}._sc_works w " +
//            "LEFT JOIN ${schema_name}._sc_asset AS dv ON dv._id = w.relation_id " +
//            "where w.status=100 and  w.deadline_time >='${beginDate}' and w.deadline_time <='${endDate}' " +
//            "AND ( " +
//            "( " +
//            "w.relation_type = 2 " +
//            ") " +
//            "OR w.relation_type = 1 " +
//            "OR w.relation_type = 3 " +
//            "OR w.relation_type = 9 " +
//            ") " +
//            "and (${maintainCondition})")
//    int outOfTimeMaintainTotal(@Param("schema_name") String schema_name, @Param("maintainCondition") String maintainCondition, @Param("beginDate") Timestamp beginDate, @Param("endDate") Timestamp endDate);
//
//    /*@Select("select count(maintain_code) FROM ${schema_name}._sc_maintain m " +
//            "where m.status=100 and m.deadline_time >='${beginDate}' and m.deadline_time <='${endDate}' and ( ${maintainCondition}) ")
//    int outOfTimeMaintainTotal(@Param("schema_name") String schema_name, @Param("maintainCondition") String maintainCondition, @Param("beginDate") Timestamp beginDate, @Param("endDate") Timestamp endDate);*/
//
//
//    /*
//    * 查询所有的域名
//     */
//    @Select("select * from public.domain_port_url where domain_name=#{domain}")
//    List<DomainPortUrlData> findAllDomain(@Param("domain") String domain);
//
//    /**
//     * 首页按条件获取,工单信息total sql
//     *
//     * @param schema_name
//     * @param condition
//     * @return int
//     */
//    @Select("select count(1) from (" +
//            "select distinct w.work_code,w.occur_time,w.work_type_id, w.work_template_code, w.pool_id, w.relation_type, " +
//            "w.relation_id, w.title, w.problem_note, w.problem_img,  w.deadline_time, w.status, w.remark, w.create_time, " +
//            "w.create_user_account, w.plan_code, w.customer_id, w.position_code, w.work_request_code, w.last_finished_time, " +
//            "w.facility_id,u.username as create_user_name, c.username as receive_name,s.status as status_name, " +
//            "t.type_name as type_name,w2.priority_level,dv.strname as asset_name,w2.receive_account,w2.sub_work_code, " +
//            "ap.position_name, ma.category_name as asset_type " +
//            " from ${schema_name}._sc_works w " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            "where 1=1 " +
//            " ${condition} " +
//            "order by w.occur_time desc,w.work_code desc " +
//            ") t ")
//    int getWorkSheetListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//
////    "select TT.*,"+
////    "CASE WHEN w.position_code IS NULL or '' = w.position_code THEN "+
////    "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT wdaof.short_title ), ',' ) AS short_title "+
////    "ELSE ap.position_name END AS facility_name "+
//
//    /**
//     * 首页按条件获取,工单信息total sql
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return List<WorkSheet>
//     */
//    @Select("select TT.work_code,TT.occur_time, TT.work_code, TT.occur_time,TT.work_type_id, TT.work_template_code, TT.pool_id, TT.relation_type, " +
//            "TT.relation_id, TT.title, TT.problem_note, TT.problem_img,  TT.deadline_time, TT.status, TT.remark, TT.create_time," +
//            "TT.create_user_account, TT.plan_code, TT.customer_id, TT.position_code, TT.work_request_code, TT.last_finished_time," +
//            "TT.facility_id,TT.create_user_name,TT.receive_name,TT.status_name,TT.type_name,TT.priority_level,TT.asset_name," +
//            "TT.receive_account,TT.sub_work_code,TT.position_name,TT.asset_type," +
//            "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT wdaof.short_title ), ',' ) AS facility_name " +
//            "from ( " +
//            "select distinct w.work_code,w.occur_time,w.work_type_id, w.work_template_code, w.pool_id, w.relation_type, " +
//            "w.relation_id, w.title, w.problem_note, w.problem_img,  w.deadline_time, w.status, w.remark, w.create_time, " +
//            "w.create_user_account, w.plan_code, w.customer_id, w.position_code, w.work_request_code, w.last_finished_time, " +
//            "w.facility_id,u.username as create_user_name, c.username as receive_name,s.status as status_name, " +
//            "t.type_name as type_name,w2.priority_level,dv.strname as asset_name,w2.receive_account,w2.sub_work_code, " +
//            "ap.position_name, ma.category_name as asset_type " +
//            " from ${schema_name}._sc_works w " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            "where 1=1 " +
//            " ${condition} " +
//            "order by w.occur_time desc,w.work_code desc " +
//            "limit ${page} offset ${begin} " +
//            ") TT " +
//            " left join ${schema_name}._sc_works_detail_asset_org wdao ON TT.sub_work_code = wdao.sub_work_code " +
//            " left join ${schema_name}._sc_facilities wdaof ON wdaof.id = wdao.facility_id and wdaof.org_type<3 " +
//            "GROUP BY TT.work_code,TT.occur_time, TT.work_code, TT.occur_time,TT.work_type_id, TT.work_template_code, TT.pool_id, TT.relation_type," +
//            "TT.relation_id, TT.title, TT.problem_note, TT.problem_img,  TT.deadline_time, TT.status, TT.remark, TT.create_time," +
//            "TT.create_user_account, TT.plan_code, TT.customer_id, TT.position_code, TT.work_request_code, TT.last_finished_time," +
//            "TT.facility_id,TT.create_user_name,TT.receive_name,TT.status_name,TT.type_name,TT.priority_level,TT.asset_name," +
//            "TT.receive_account,TT.sub_work_code,TT.position_name,TT.asset_type")
//    List<WorkSheet> getWorkSheetList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);

}
