package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MaintainMapper {
//
//    //新增保养，可以由保养人直接完成一个保养结果，然后提交
//    @Insert("insert into ${schema_name}._sc_maintain (maintain_code, asset_type, asset_id, maintain_account, deadline_time," +
//            "receive_time, spend_hour, total_work_hour, before_img, maintain_note, after_img, " +
//            "finished_time, audit_account, audit_time, audit_word, status, fault_number, facility_id, createtime," +
//            " create_user_account, maintain_result, reamark)" +
//            "values(#{r.maintain_code}, #{r.asset_type}, #{r.asset_id}, #{r.maintain_account}," +
//            " #{r.deadline_time},#{r.receive_time}, #{r.spend_hour}, #{r.total_work_hour}, #{r.before_img}, #{r.maintain_note}," +
//            " #{r.after_img}, #{r.finished_time}, #{r.audit_account}, #{r.audit_time}, #{r.audit_word}, #{r.status}, " +
//            "#{r.fault_number},#{r.facility_id}, #{r.createtime} , #{r.create_user_account}, #{r.maintain_result}, #{r.remark})")
//    int saveMaintain(@Param("schema_name") String schema_name, @Param("r") MaintainData maintainData);
//
//    //新增支援人员，初始工时为0，支援人员中包含_sc_maintain表中的接受人员
//    @Insert("insert into ${schema_name}._sc_maintain_work_hour (maintain_code, operate_account, work_hour)" +
//            "values(#{w.maintain_code}, #{w.operate_account}, #{w.work_hour})")
//    int insertMaintainWorkHour(@Param("schema_name") String schema_name, @Param("w") MaintainWorkHourData maintainWorkHourData);
//
//
//    //编辑保养信息,点击保存按钮，或者草稿状态，重新保存或提交
//    @Update("update ${schema_name}._sc_maintain set " +
//            "facility_id=#{r.facility_id}, " +
//            "asset_type=#{r.asset_type}, " +
//            "asset_id=#{r.asset_id}, " +
//            "maintain_account=#{r.maintain_account}, " +
//            "receive_time=#{r.receive_time}, " +
//            "before_img=#{r.before_img}, " +
//            "remark=#{r.remark}, " +
//            "status=#{r.status} " +
//            "where maintain_code=#{r.maintain_code} ")
//    int EditMaintain(@Param("schema_name") String schema_name, @Param("r") MaintainData maintainData);
//
//    //更新分配人
//    @Update("update ${schema_name}._sc_maintain set " +
//            "maintain_account=#{r.maintain_account} " +
//            "where maintain_code=#{r.maintain_code} ")
//    int EditMaintainMan(@Param("schema_name") String schema_name, @Param("r") MaintainData maintainData);
//
//    //删除援人员，在重新保存和提交保养单时，先全部删除工时表中的人，再重新新增支援人员进来
//    @Delete("delete from ${schema_name}._sc_maintain_work_hour where maintain_code='${maintain_code}'")
//    int deleteMaintainWorkHour(@Param("schema_name") String schema_name, @Param("maintain_code") String maintain_code);
//
//    //更新编辑开始保养时间，为手机上点击设备的保养开始
//    @Update("update ${schema_name}._sc_maintain set " +
//            "begin_time=#{r.begin_time} " +
//            "where maintain_code=#{r.maintain_code} ")
//    int EditMaintainBeginTime(@Param("schema_name") String schema_name, @Param("r") MaintainData maintainData);
//
//    //设置维修开始计时,缺表，待建 2018-03-29
//
//
//    //保存和提交保养结果
//    @Update("<script>update ${schema_name}._sc_maintain set " +
//            "spend_hour=#{r.spend_hour}, " +
//            "total_work_hour=#{r.total_work_hour}, " +
//            "before_img=#{r.before_img}, " +
//            "maintain_result=#{r.maintain_result}, " +
//            "maintain_note=#{r.maintain_note}, " +
//            "after_img=#{r.after_img}, " +
//            "finished_time=#{r.finished_time}, " +
//            "fault_number=#{r.fault_number}, " +
//            "<when test='r.beginTime != null'>" +
//            "begin_time=#{r.begin_time}," +
//            "</when>" +
//            "status=#{r.status} " +
//            "where maintain_code=#{r.maintain_code} " +
//            "</script>")
//    int SaveMaintainResult(@Param("schema_name") String schema_name, @Param("r") MaintainData maintainData);
//
//    //新增保养领用的备件
//    @Insert("insert into ${schema_name}._sc_maintain_bom (maintain_code, bom_model, bom_code, stock_code,facility_id,huojia," +
//            "fetch_man,use_count,material_code,is_from_service_supplier)" +
//            "values(#{b.maintain_code}, #{b.bom_model}, #{b.bom_code}, #{b.stock_code}, #{b.facility_id}, #{b.huojia}, " +
//            "#{b.fetch_man}, #{b.use_count}, #{b.material_code}, #{b.is_from_service_supplier})")
//    int insertMaintainBom(@Param("schema_name") String schema_name, @Param("b") MaintainBomData repairBomData);
//
//    //删除保养领用的备件,用于保养结果重新保存或提交时
//    @Delete("delete from ${schema_name}._sc_maintain_bom where maintain_code='${maintain_code}'")
//    int deleteMaintainBom(@Param("schema_name") String schema_name, @Param("maintain_code") String maintain_code);
//
//    //保养结果审核
//    @Update("update ${schema_name}._sc_maintain set " +
//            "audit_account=#{r.audit_account}, " +
//            "audit_time=#{r.audit_time}, " +
//            "audit_word=#{r.audit_word}, " +
//            "check_result=#{r.check_result}, " +
//            "status=#{r.status} " +
//            "where maintain_code=#{r.maintain_code} ")
//    int AuditMaintainResult(@Param("schema_name") String schema_name, @Param("r") MaintainData maintainData);
//
//
//    //获取最近一条保养记录
//    @Select("select r.*,y.cycle_count,d.strcode as asset_code " +
//            "from ${schema_name}._sc_maintain r " +
//            "left join ${schema_name}.._sc_asset d on d._id=r.asset_id " +
//            "left join ${schema_name}.._sc_maintain_cycle y on r.asset_type=y.asset_type_id " +
//            " where r.finished_time = (select max(b.finished_time) from ${schema_name}._sc_maintain as b where r.asset_id = b.asset_id and ( b.status=80 or b.status=70 ) and b.asset_id = '${asset_id}' ) ")
//    List<MaintainData> getLatestMaintainList(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    //查找保养单列表(按，条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    @Select("select r.maintain_code, r.asset_type, r.maintain_account, r.receive_time, r.begin_time, r.spend_hour, r.total_work_hour," +
//            "r.before_img,r.after_img, r.finished_time, r.audit_account, r.audit_time, r.status, r.fault_number, r.facility_id," +
//            "r.createtime, r.create_user_account, r.asset_id, r.deadline_time," +
//            "u.username as create_user_name,c.username as maintain_name,d.username as audit_name,s.status as status_name," +
//            "dv.strname as asset_name,dv.strcode as asset_code,ft.title as facility_name,pf.title as parent_name " +
//            ",string_agg(w.operate_account, ',') as operators " +
//            "from ${schema_name}._sc_maintain r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.maintain_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.audit_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_maintain_work_hour w on r.maintain_code=w.maintain_code " +
//            "where 1=1 ${condition} " +
//            " group by r.maintain_code, u.username, c.username, d.username,s.status,dv.strname,dv.strcode,ft.title,pf.title " +
//            " order by r.maintain_code desc limit ${page} offset ${begin}")
//    List<MaintainData> getMaintainList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //查找保养单列表的总数(按条件用户角色可查看的范围，查询所有的维保单，可用于按设备查询设备的保养记录中)
//    @Select("select count(r.maintain_code) " +
//            "from ${schema_name}._sc_maintain r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.maintain_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.audit_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            "where 1=1 ${condition} ")
//    int getMaintainListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //按位置，查找所有的设备，以及其最新的一条保养数据（不管是否执行完成，只要生成了）, 供系统自动生成保养单用
//    @Select("select d.strcode as asset_code,d.intstatus as asset_status,d.asset_type,d._id as asset_id," +
//            "d.strname as asset_name,y.cycle_count,r.deadline_time,d.intsiteid as facility_id,d.maintain_begin_time " +
//            "from ${schema_name}._sc_asset d " +
//            "left join (" +
//            "select asset_id,max(deadline_time) as deadline_time from ${schema_name}._sc_maintain where status!=900 group by asset_id " +
//            ") r on d._id=r.asset_id " +
////            "${schema_name}._sc_maintain r on d._id=r.asset_id and r.createtime = (select max(b.createtime) from ${schema_name}._sc_maintain as b where r.asset_id = b.asset_id ) " +
//            "left join ${schema_name}._sc_maintain_cycle y on d.asset_type=y.asset_type_id " +
//            " where (r.deadline_time is null or date_part('day',now()-r.deadline_time)>50 ) and d.intsiteid = #{facility_id} " +
//            " order by d.strcode")
//    List<MaintainData> findAllAssetAndLastestMaintain(@Param("schema_name") String schema_name, @Param("facility_id") int facility_id);
//
//    //过期一天的保养单，进行关闭操作
//    @Update("update ${schema_name}._sc_maintain set status=100 " +
//            "where status not in (60,100) and date_part('day',now()-deadline_time)>0 ")
//    int CloseMaintainBillForDue(@Param("schema_name") String schema_name);
//
//    //查找单条保养单据
//    @Select("select r.*,u.username as create_user_name,c.username as maintain_name,d.username as audit_name,s.status as status_name," +
//            "dv.strname as asset_name,dv.strcode as asset_code,ft.title as facility_name,cus.customer_name as suppilerName,cy.cycle_count  " +
//            "from ${schema_name}._sc_maintain r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.maintain_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.audit_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers cus on dv.supplier=cus.id " +
//            " left join ${schema_name}._sc_maintain_cycle cy on dv.asset_type=cy.asset_type_id " +
//            "where maintain_code='${maintain_code}' ")
//    MaintainData getMaintainInfo(@Param("schema_name") String schema_name, @Param("maintain_code") String maintainCode);
//
//    //获取备件列表，用于保养的备件
//    @Select("select mb.*,b.bom_name,s.stock_name from ${schema_name}._sc_maintain_bom mb " +
//            " left join ${schema_name}._sc_stock s on s.stock_code=mb.stock_code " +
//            " left join ${schema_name}._sc_bom b on b.bom_code=mb.bom_code and b.material_code=mb.material_code and b.bom_model=mb.bom_model and b.stock_code=mb.stock_code " +
//            "where maintain_code='${maintain_code}' ")
//    List<MaintainBomData> getMaintainBomList(@Param("schema_name") String schema_name, @Param("maintain_code") String maintainCode);
//
//    //获取保养工时列表
//    @Select("select w.*,u.username as operate_name from ${schema_name}._sc_maintain_work_hour w " +
//            "left join ${schema_name}._sc_user u on w.operate_account=u.account " +
//            "where w.maintain_code='${maintain_code}' ")
//    List<MaintainWorkHourData> getMaintainWorkHourList(@Param("schema_name") String schema_name, @Param("maintain_code") String maintainCode);
//
//    //获取设备的保养项，按设备的类型
//    @Select("SELECT id, item_name, item_code, check_note, deal_note, \"order\", createtime, create_user_account, asset_type_id FROM ${schema_name}._sc_maintain_item " +
//            "where asset_type_id='${asset_type_id}' ")
//    List<MaintainItemData> getMaintainItemByAssetId(@Param("schema_name") String schema_name, @Param("asset_type_id") String asset_type_id);
//
//    //获取所有的设备保养项
//    @Select("SELECT id, item_name, item_code, check_note, deal_note, \"order\", createtime, create_user_account, asset_type_id " +
//            "FROM ${schema_name}._sc_maintain_item ")
//    List<MaintainItemData> getAllMaintainItemList(@Param("schema_name") String schema_name);
//
//    //获取设备的保养确认项，按设备的类型
//    @Select("SELECT id, check_item_name, \"order\", createtime, create_user_account, asset_type_id FROM ${schema_name}._sc_maintain_check_item " +
//            "where asset_type_id='${asset_type_id}' ")
//    List<MaintainCheckItemData> getMaintainCheckItem(@Param("schema_name") String schema_name, @Param("asset_type_id") String asset_type_id);
//
//
//    //查找保养单，查到需保养的设备
//    @Select("select d.* from ${schema_name}._sc_asset d " +
//            "left join ${schema_name}._sc_maintain r on d._id=r.asset_id " +
//            "where r.maintain_code=#{maintiancode}")
//    Asset getAssetCommonDataByMaintainCode(@Param("schema_name") String schema_name, @Param("maintiancode") String maintainCode);
//
//
//    //查找最近一条未过期保养单
//    @Select("select r.* FROM ${schema_name}._sc_maintain r " +
//            "where asset_id = #{asset_id} and status != 100 order by finished_time desc limit 1")
//    MaintainData getLastOneMaintain(@Param("schema_name") String schema_name, @Param("asset_id") String assetId);
//
//    @Select("select r.maintain_code, r.asset_type, r.maintain_account, r.receive_time, r.begin_time, r.spend_hour, r.total_work_hour, " +
//            "r.before_img,r.after_img, r.finished_time, r.audit_account, r.audit_time, r.status, r.fault_number, r.facility_id, " +
//            "r.createtime, r.create_user_account, r.asset_id, r.deadline_time," +
//            "u.username as create_user_name,c.username as maintain_name,d.username as audit_name,s.status as status_name," +
//            "dv.strname as asset_name,dv.strcode as asset_code,ft.title as facility_name " +
//            " ,string_agg(w.operate_account, ',') as operators " +
//            "from ${schema_name}._sc_maintain r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.maintain_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.audit_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_maintain_work_hour w on r.maintain_code=w.maintain_code " +
//            "where r.asset_id=(select a._id from ${schema_name}._sc_asset a where a.strcode=#{device_code}) " +
//            " group by r.maintain_code, u.username, c.username, d.username,s.status,dv.strname,dv.strcode,ft.title order by maintain_code desc  ")
//        //@Select("select r.* from ${schema_name}._sc_maintain r where r.asset_id=(select a._id from ${schema_name}._sc_asset a where a.strcode=#{device_code})")
//    List<MaintainData> getMaintainListByCode(@Param("schema_name") String schema_name, @Param("device_code") String device_code);
//
//    //备件统计保养使用数量明细查询by bom_model and stock_code
//    @Select("select mb.*,u.username AS fetch_man_name from ${schema_name}._sc_maintain_bom mb " +
//            "left join ${schema_name}._sc_user u on mb.fetch_man=u.account " +
//            "where mb.bom_model='${bom_model}' and mb.stock_code='${stock_code}'")
//    List<MaintainBomData> getMaintainDetailByBomModelAndStockCode(@Param("schema_name") String schema_name, @Param("bom_model") String bom_model, @Param("stock_code") String stock_code);
//
//    //作废保养
//    @Update("update ${schema_name}._sc_maintain set " +
//            "status=900 " +
//            "where maintain_code=#{maintain_code} ")
//    int invalidRepair(@Param("schema_name") String schema_name, @Param("maintain_code") String maintainCode);
//
//    //更新保养截止日期
//    @Update("update ${schema_name}._sc_maintain set " +
//            "deadline_time=#{deadlineTime} " +
//            "where maintain_code=#{maintain_code} ")
//    int updateMaintainDeadlineTime(@Param("schema_name") String schema_name, @Param("maintain_code") String maintainCode, @Param("deadlineTime") Timestamp deadlineTime);
//
//    //根据维修编号查询记录
//    @Delete("delete from ${schema_name}._sc_maintain_bom mb where mb.maintain_code='${bill_code}'")
//    int deleteMaintianByMaterial_code(@Param("schema_name") String schema_name, @Param("bill_code") String bill_code);
//
//    //新增保养，可以由保养人直接完成一个保养结果，然后提交
//    @Insert("insert into ${schema_name}._sc_maintain_bom (maintain_code, bom_model, fetch_man, use_count, bom_code,material_code ) " +
//            "values (#{b.maintain_code}, #{b.bom_model}, #{b.fetch_man}, #{b.use_count}," +
//            " #{b.bom_code},#{b.material_code})")
//    int saveMaintainBom(@Param("schema_name") String schema_name, @Param("b") MaintainBomData maintainBomData);
//
//
//
//    /*默认0，禁止10，不通过*/
//    @Update("update ${schema_name}._sc_maintain set " +
//            "waiting=10 " +
//            "where maintain_code=#{maintain_code} ")
//    int updateMaintainApplyStaus(@Param("schema_name") String schema_name, @Param("maintain_code") String code);
//
//    @Update("update ${schema_name}._sc_maintain set " +
//            "waiting=#{status} " +
//            "where maintain_code=#{maintain_code} ")
//    int updateOpenMaintainApplyStaus(@Param("schema_name") String schema_name, @Param("maintain_code") String code,@Param("status") int status);
//
//    //查询物料编号是否存在保养备件信息
//    @Select("select r.* FROM ${schema_name}._sc_maintain_bom r " +
//            "where maintain_code = #{maintain_code} ")
//    List<MaintainBomData> selectMaintainByMaterialCode(@Param("schema_name") String schema_name, @Param("maintain_code") String code);
//
//    @Delete("delete from  ${schema_name}._sc_maintain_bom where is_from_service_supplier=1 and maintain_code=#{maintain_code} ")
//    int udpateMaintainBomBymaterial(@Param("schema_name") String schema_name, @Param("maintain_code") String bill_code);
//
//
//    //查询物料编号是否存在保养备件信息
//    @Select("select \n" +
//            "SUM(case when r.status<>900 and r.status<>60 and r.status<>100  then 1 else 0 end) as plannedMaintain,\n" +
//            "SUM(case when r.status=60 and r.finished_time>current_date  then 1 else 0 end) as completePlannedMaintain\n" +
//            "FROM ${schema_name}._sc_maintain r")
//    FullScreenData getPlannedAndCompleteMaintain(@Param("schema_name") String schema_name);
}
