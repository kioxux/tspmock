package com.gengyun.senscloud.mapper;

/**
 * Created by Administrator on 2018/12/27.
 */
public interface SetMonitorMapper {
//
//    //新增
//    @Insert("insert into ${schema_name}._sc_metadata_monitors (" +
//            "monitor_name, monitor_key, asset_category_id, monitor_category, unit_type, unit_id, gather_value_type, " +
//            "record_interval, show_type, show_charts, is_judge_parameter, judge_value, exception_down, exception_up, " +
//            "alarm_down, alarm_up, show_count,\"order\", create_time, create_user_account) " +
//            "values ( " +
//            "#{m.monitor_name},#{m.monitor_key},#{m.asset_category_id},#{m.monitor_category},#{m.unit_type},#{m.unit_id},#{m.gather_value_type}," +
//            "#{m.record_interval},#{m.show_type},#{m.show_charts},#{m.is_judge_parameter},#{m.judge_value},#{m.exception_down},#{m.exception_up}," +
//            "#{m.alarm_down},#{m.alarm_up},#{m.show_count},#{m.order}::int,#{m.create_time},#{m.create_user_account})")
//    public int addSetMonitorList(@Param("schema_name") String schema_name,@Param("m") MetadataMonitorsModel m);
//
//    //编辑
//    @Update("update ${schema_name}._sc_metadata_monitors set " +
//            "   monitor_name = #{m.monitor_name}, monitor_key = #{m.monitor_key}, asset_category_id = #{m.asset_category_id}, " +
//            "   monitor_category = #{m.monitor_category}, unit_type = #{m.unit_type}, unit_id = #{m.unit_id}, " +
//            "   gather_value_type = #{m.gather_value_type}, record_interval = #{m.record_interval}, show_type = #{m.show_type}, " +
//            "   show_charts = #{m.show_charts}, is_judge_parameter = #{m.is_judge_parameter}, judge_value = #{m.judge_value}, " +
//            "   exception_down = #{m.exception_down}, exception_up = #{m.exception_up}, alarm_down = #{m.alarm_down}, " +
//            "   alarm_up = #{m.alarm_up}, show_count = #{m.show_count},\"order\"=#{m.order} where id = #{m.id}")
//    public int editSetMonitorList(@Param("schema_name") String schema_name,@Param("m") MetadataMonitorsModel m);
//
//    //修改行数
//    @Update("update ${schema_name}._sc_metadata_monitors set " +
//            "\"order\"=${order} where id = ${id}")
//    public int editSetMonitorOrder(@Param("schema_name") String schema_name,@Param("id") long id,@Param("order") long order);
//
//    //列表
//    @Select("Select m.monitor_name,m.id,m.monitor_key,m.monitor_category,m.unit_type,m.gather_value_type, " +
//            "m.record_interval,m.is_judge_parameter,m.judge_value,m.exception_down,m.exception_up,m.alarm_down,m.exception_up,m.order, " +
//            "u.unit_name,c.category_name from ${schema_name}._sc_metadata_monitors as m " +
//            " left join ${schema_name}._sc_unit as u on m.unit_id = u.id " +
//            " left join ${schema_name}._sc_asset_category as c on m.asset_category_id = c.id where 1=1 ${condition} " +
//            " order by m.order  limit ${page} offset ${begin}")
//    public List<MetadataMonitorsModel> findSetMonitorList(@Param("schema_name") String schema_name ,@Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //列表总数
//    @Select("Select count(id) from ${schema_name}._sc_metadata_monitors as m where 1=1 ${condition}")
//    public int findSetMonitorListNum(@Param("schema_name") String schema_name ,@Param("condition") String condition);
//
//    //按设备类型查询监控项
//    @Select("Select * from ${schema_name}._sc_metadata_monitors where asset_category_id=${category_id} order by \"order\"   ")
//    public List<Map<String, Object>> findSetMonitorListByCategoryId(@Param("schema_name") String schema_name , @Param("category_id") String category_id);
//
//    //详情
//    @Select("Select * from ${schema_name}._sc_metadata_monitors where id=#{id}")
//    public MetadataMonitorsModel findSetMonitorListById(@Param("schema_name") String schema_name, @Param("id") int id);
//
////    //查询有效的监控项
////    @Select("Select * from ${schema_name}._sc_metadata_monitors where isuse=1 and see_all_asset=1 limit 1")
////    public MetadataMonitorsModel findValidSetMonitor(@Param("schema_name") String schema_name);
//
//    //删除
//    @Delete("delete from ${schema_name}._sc_metadata_monitors where id=#{id}")
//    public int delSetMonitorList(@Param("schema_name") String schema_name,@Param("id") int id);
//
//    //删除
//    @Update("update ${schema_name}._sc_metadata_monitors set isuse = #{isuse} where id=#{id}")
//    public int isuseSetMonitor(@Param("schema_name") String schema_name,@Param("id") int id , @Param("isuse")int isuse);
//
//    //查询有效的监控项
//    @Select("SELECT fa.* FROM ${schema_name}._sc_fault_code fa " +
//            "LEFT JOIN ${schema_name}._sc_asset_category_fault cf ON cf.fault_code=fa.code " +
//            "WHERE fa.isuse AND cf.category_id=${category_id}" )
//    public List<Map<String,Object>> findFaultData(@Param("schema_name") String schema_name,@Param("category_id") String category_id);
}
