package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.model.ScheduleData;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

@Mapper
public interface ScheduleMapper {
    @InsertProvider(type = ScheduleMapperProvider.class, method = "insert")
    void insert(@Param("schema_name") String schema_name, @Param("sd") ScheduleData scheduleData);

    @UpdateProvider(type = ScheduleMapperProvider.class, method = "update")
    void update(@Param("schema_name") String schema_name, @Param("sd") ScheduleData scheduleData);

    @DeleteProvider(type = ScheduleMapperProvider.class, method = "delete")
    void delete(@Param("schema_name") String schema_name, @Param("id") Integer id);

    @SelectProvider(type = ScheduleMapperProvider.class, method = "find")
    List<ScheduleData> find(@Param("schema_name") String schema_name, @Param("id") Integer id,
                            @Param("shedule_name") String shedule_name,
                            @Param("is_use") String is_use, @Param("reschedule") Boolean reschedule,
                            @Param("is_running") Boolean is_running, @Param("run_channel") Integer run_channel);

    class ScheduleMapperProvider {
        public String insert(@Param("schema_name") String schema_name, @Param("sd") ScheduleData scheduleData) {
            return new SQL() {{
                INSERT_INTO(schema_name + "._sc_system_shedule");
                VALUES("shedule_name", "#{sd.shedule_name}");
                VALUES("task_class", "#{sd.task_class}");
                VALUES("task_type", "#{sd.task_type}");
                VALUES("expression", "#{sd.expression}");
                VALUES("is_use", "#{sd.is_use}");
                VALUES("reschedule", "#{sd.reschedule}");
                VALUES("is_running", "#{sd.is_running}");
                VALUES("run_channel", "#{sd.run_channel}");
                VALUES("createtime", "#{sd.createtime}");
                VALUES("create_user_account", "#{sd.create_user_account}");
            }}.toString();
        }

        public String update(@Param("schema_name") String schema_name, @Param("sd") ScheduleData scheduleData) {
            return new SQL() {{
                UPDATE(schema_name + "._sc_system_shedule");
                if (RegexUtil.optIsPresentStr(scheduleData.getShedule_name())) {
                    SET("shedule_name = #{sd.shedule_name}");
                }
                if (RegexUtil.optIsPresentStr(scheduleData.getTask_class())) {
                    SET("task_class = #{sd.task_class}");
                }
                if (scheduleData.getTask_type() != null) {
                    SET("task_type = #{sd.task_type}");
                }
                if (RegexUtil.optIsPresentStr(scheduleData.getExpression())) {
                    SET("expression = #{sd.expression}");
                }
                if (RegexUtil.optIsPresentStr(scheduleData.getIs_use())) {
                    SET("is_use = #{sd.is_use}");
                }
                if (scheduleData.getReschedule() != null) {
                    SET("reschedule = #{sd.reschedule}");
                }
                if (scheduleData.getIs_running() != null) {
                    SET("is_running = #{sd.is_running}");
                }
                if (scheduleData.getRun_channel() != null) {
                    SET("run_channel = #{sd.run_channel}");
                }
                if (scheduleData.getId() != null) {
                    WHERE("id = #{sd.id}");
                }
            }}.toString();
        }

        public String delete(@Param("schema_name") String schema_name, @Param("id") Integer id) {
            return new SQL() {{
                DELETE_FROM(schema_name + "._sc_system_shedule");
                WHERE("id = #{id}");
            }}.toString();
        }

        public String find(@Param("schema_name") String schema_name, @Param("id") Integer id,
                           @Param("shedule_name") String shedule_name,
                           @Param("is_use") String is_use, @Param("reschedule") Boolean reschedule,
                           @Param("is_running") Boolean is_running,@Param("run_channel") Integer run_channel) {
            return new SQL() {{
                SELECT("*");
                FROM(schema_name + "._sc_system_shedule");
                if (RegexUtil.optIsPresentStr(shedule_name)) {
                    WHERE("shedule_name like CONCAT('%', #{shedule_name}, '%')");
                }
                if (id != null) {
                    WHERE("id = #{id}");
                }
                if (RegexUtil.optIsPresentStr(is_use)) {
                    WHERE("is_use = #{is_use}");
                }
                if (reschedule != null) {
                    WHERE("reschedule = #{reschedule}");
                }
                if (is_running != null) {
                    WHERE("is_running = #{is_running}");
                }
                if (run_channel != null) {
                    WHERE("run_channel = #{run_channel}");
                }
            }}.toString();
        }
    }
}
