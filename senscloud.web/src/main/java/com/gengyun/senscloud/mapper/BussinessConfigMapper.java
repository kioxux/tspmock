package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 业务配置
 */
@Mapper
public interface BussinessConfigMapper {

    /**
     * 查询工单类型列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 工单类型列表
     */
    @Select(" <script>" +
            " SELECT wt.id,wt.type_name,wt.data_order,wt.is_use," +
            " wt.business_type_id,bt.business_name" +
            " FROM ${schema_name}._sc_work_type AS wt" +
            " LEFT JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "  OR wt.type_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  OR bt.business_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY wt.data_order ASC " +
            " </script>")
    List<Map<String, Object>> findWorkTypeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 根据id和表名删除维护配置信息
     *
     * @param schema_name 入参
     * @param table_name  入参
     * @param id          入参
     */
    @Delete("DELETE FROM ${schema_name}.${table_name} WHERE id = #{id}")
    void deleteById(@Param("schema_name") String schema_name, @Param("table_name") String table_name, @Param("id") Integer id);

    /**
     * 根据id和表名更新维护配置信息
     *
     * @param schema_name 入参
     * @param table_name  入参
     * @param pm          入参
     */
    @Delete(" <script>" +
            "UPDATE  ${schema_name}.${table_name}" +
            " set id = #{pm.id}::int " +
            "  <when test='pm.type_name!=null'>" +
            "  ,type_name = #{pm.type_name}" +
            "  </when>" +
            "  <when test='pm.business_type_id!=null'>" +
            "  ,business_type_id = #{pm.business_type_id}::int" +
            "  </when>" +
            "  <when test='pm.data_order!=null'>" +
            "  ,data_order = #{pm.data_order}::int" +
            "  </when>" +
            "  <when test='pm.is_use!=null'>" +
            "  ,is_use = #{pm.is_use}" +
            "  </when>" +
            "  <when test='pm.running_status!=null'>" +
            "  ,running_status = #{pm.running_status}" +
            "  </when>" +
            "  <when test='pm.level_name!=null'>" +
            "  ,level_name = #{pm.level_name}" +
            "  </when>" +
            "  <when test='pm.type_code!=null'>" +
            "  ,type_code = #{pm.type_code}" +
            "  </when>" +
            "  <when test='pm.parent_id!=null'>" +
            "  ,parent_id = #{pm.parent_id}::int" +
            "  </when>" +
            "  <when test='pm.unit_name!=null'>" +
            "  ,unit_name = #{pm.unit_name}" +
            "  </when>" +
            "  <when test='pm.finished_name!=null'>" +
            "  ,finished_name = #{pm.finished_name}" +
            "  </when>" +
            "  <when test='pm.is_finished!=null'>" +
            "  ,is_finished = #{pm.is_finished}::int" +
            "  </when>" +
            "  <when test='pm.currency_name!=null'>" +
            "  ,currency_name = #{pm.currency_name}" +
            "  </when>" +
            "  <when test='pm.currency_code!=null'>" +
            "  ,currency_code = #{pm.currency_code}" +
            "  </when>" +
            "  <when test='pm.priority_level!=null'>" +
            "  ,priority_level = #{pm.priority_level}::int" +
            "  </when>" +
            "  WHERE id = #{pm.id}::int" +
            "  </script>")
    void updateById(@Param("schema_name") String schema_name, @Param("table_name") String table_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询维修类型列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 维修类型列表
     */
    @Select(" <script>" +
            " SELECT rt.id,rt.type_name,rt.is_use,rt.data_order FROM ${schema_name}._sc_repair_type rt" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND  rt.type_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY rt.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findRepairTypeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询设备运行状态列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 设备运行状态列表
     */
    @Select(" <script>" +
            " SELECT ars.id,ars.priority_level::varchar,ars.running_status,ars.is_use,ars.data_order FROM ${schema_name}._sc_asset_running_status ars" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND  ars.running_status LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY ars.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findAssetRuningStatusList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 查询设备重要级别列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 设备重要级别列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.level_name,t.is_use,t.data_order FROM ${schema_name}._sc_importment_level t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND  t.level_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findImportmentLevelList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询备件类型列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 备件类型列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.type_code,t.parent_id,t.type_name,t.is_use,t.data_order FROM ${schema_name}._sc_bom_type t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "   OR  t.type_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "   OR  t.type_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findBomTypeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询备件类型列表
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 备件类型列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.type_code,t.parent_id,t.type_name,t.is_use,t.data_order FROM ${schema_name}._sc_bom_type t" +
            " WHERE t.parent_id = #{id} ORDER BY t.data_order ASC LIMIT 1 " +
            " </script> ")
    Map<String, Object> findBomTypeChildren(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 查询位置类型列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 位置类型列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.type_name,t.is_use,t.data_order FROM ${schema_name}._sc_facility_type t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "   OR  t.type_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findFacilityTypeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 查询文档类型列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 文档类型列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.type_name,t.is_use,t.data_order FROM ${schema_name}._sc_file_type t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "   OR  t.type_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findFileTypeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询单位列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 单位列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.unit_name,t.is_use,t.data_order FROM ${schema_name}._sc_Unit t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "   OR  t.unit_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findUnitList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 查询完修说明列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 完修说明列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.is_finished::varchar,t.finished_name,t.is_use,t.data_order FROM ${schema_name}._sc_work_finished_type t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "   OR  t.unit_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findWorkFinishedTypeList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询货币种类列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 货币种类列表
     */
    @Select(" <script>" +
            " SELECT t.id,t.currency_name,t.currency_code,t.is_use,t.data_order FROM ${schema_name}._sc_currency t" +
            " <where>" +
            "  <when test='pm.keywordSearch!=null'>" +
            "   OR  t.currency_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "   OR  t.currency_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  </when>" +
            " </where> ORDER BY t.data_order ASC" +
            " </script> ")
    List<Map<String, Object>> findCurrencyList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增工单类型
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_type" +
            "( type_name, data_order, is_use, create_time, create_user_id," +
            " business_type_id) " +
            "VALUES " +
            "( #{pm.type_name}, #{pm.data_order}::int, #{pm.is_use}, current_timestamp, " +
            " #{pm.create_user_id}," +
            " #{pm.business_type_id}::int ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertWorkType(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增维修类型
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_repair_type" +
            "( type_name, data_order, is_use) " +
            "VALUES " +
            "( #{pm.type_name}, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertRepairType(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增设备运行状态
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_asset_running_status" +
            "( running_status,priority_level, data_order, is_use) " +
            "VALUES " +
            "( #{pm.running_status},#{pm.priority_level}::int, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertAssetRunningStatus(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增设备级别
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_importment_level" +
            "( level_name,need_time, data_order, is_use) " +
            "VALUES " +
            "( #{pm.level_name},#{pm.need_time}::float, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertImportmentLevel(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增备件类型
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_bom_type" +
            "( type_name,parent_id,type_code, data_order, is_use) " +
            "VALUES " +
            "( #{pm.type_name},#{pm.parent_id}::int,#{pm.type_code}, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertBomType(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增位置类型
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_facility_type" +
            "( type_name,create_user_id,create_time, data_order, is_use) " +
            "VALUES " +
            "( #{pm.type_name},#{pm.create_user_id},now(), #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertFacilityType(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增文档类型
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_file_type" +
            "( type_name, data_order, is_use) " +
            "VALUES " +
            "( #{pm.type_name}, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertFileType(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增单位管理
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_unit" +
            "( unit_name, data_order, is_use) " +
            "VALUES " +
            "( #{pm.unit_name}, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertUnit(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增完修说明
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_finished_type" +
            "( finished_name,is_finished,operate_user_id,operate_time, data_order, is_use) " +
            "VALUES " +
            "( #{pm.finished_name},#{pm.is_finished}::int,#{pm.create_user_id},#{pm.operate_time}, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertWorkFinishedType(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 新增货币种类
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_currency" +
            "( currency_name, data_order, is_use) " +
            "VALUES " +
            "( #{pm.currency_name}, #{pm.data_order}::int, #{pm.is_use} ) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertCurrency(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询系统配置
     *
     * @param schema_name 入参
     * @param table_name  入参
     * @param id          入参
     * @return 系统配置
     */
    @Select("SELECT * FROM ${schema_name}.${table_name} WHERE id = #{id} ")
    Map<String, Object> findSystemById(@Param("schema_name") String schema_name, @Param("table_name") String table_name,
                                       @Param("id") Integer id);

    /**
     * 业务类型详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 系统配置
     */
    @Select("SELECT * FROM ${schema_name}._sc_business_type WHERE business_type_id = #{id} ")
    Map<String, Object> findBusinessById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 根据业务类型查询第一的工单类型
     *
     * @param schema_name 入参
     * @param business_id 入参
     * @return 入参
     */
    @Select("SELECT * FROM ${schema_name}._sc_work_type WHERE is_use = '1' AND business_type_id = #{business_id} ORDER BY ID ASC LIMIT 1 ")
    Map<String, Object> findWorkTypeByBusiness(@Param("schema_name") String schema_name, @Param("business_id") Integer business_id);


    /**
     * 根据业务类型获取启用的工单类型ids
     *
     * @param schema_name 入参
     * @param business_id 入参
     * @return 工单类型ids
     */
    @Select("SELECT id::varchar FROM ${schema_name}._sc_work_type WHERE is_use = '1' AND business_type_id = #{business_id} ")
    List<String> findWorkTypeIdsByBusinessId(@Param("schema_name") String schema_name, @Param("business_id") Integer business_id);


}
