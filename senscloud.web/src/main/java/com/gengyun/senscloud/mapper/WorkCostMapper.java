package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkCostMapper {
//    //按设备类型查看费用报表详情
//    @Select( "select p.id, p.title,sf.category_name,sf.currency_code,sum(sf.fee_amount)as fee_amount,sf.customer_name from ${schema_name}._sc_facilities p " +
//            "left join ("+
//            "select a.id, a.title,a.parentid,c.category_name,e.currency_code,sum(d.fee_amount)as fee_amount,f.short_title " +
//            "from ${schema_name}._sc_facilities a " +
//            " left join ${schema_name}._sc_works w on  w.facility_id=a.id " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " left join ${schema_name}._sc_asset b on b._id=w2 .relation_id " +
//            " left join ${schema_name}._sc_asset_category c on b.category_id=c.id   " +
//            " left join ${schema_name}._sc_work_fee d on d.sub_work_code=w2.sub_work_code  " +
//            " left join ${schema_name}._sc_currency e on e.id= d.currency  " +
//            " left join ${schema_name}._sc_facilities f on w.customer_id= f.id  " +
//            "where d.fee_amount NOTNULL ${condition}  "+
//            "group by a.id, a.title,a.parentid,c.category_name,e.currency_code,f.customer_name ) sf on sf.parentid=p.id or sf.id=p.id " +
//            "where p.parentid=0 and sf.fee_amount NOTNULL  "+
//            "group by p.id, p.title,sf.category_name,sf.currency_code,sf.customer_name "+
//            "order by p.id desc limit ${page} offset ${begin}")
//    List<Map<String,Object>> getWorkCostListDetailforCustomer(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//    //按设备类型查看费用报表的总数
//    @Select("select count(1) as total, sum(fee_amount)as cost from ( " +
//            "select p.id, p.title,sf.category_name,sf.currency_code,sum(sf.fee_amount)as fee_amount,sf.customer_name from ${schema_name}._sc_facilities p " +
//            "left join ("+
//            "select a.id, a.title,a.parentid,c.category_name,e.currency_code,sum(d.fee_amount)as fee_amount,f.short_title " +
//            "from ${schema_name}._sc_facilities a " +
//            " left join ${schema_name}._sc_works w on  w.facility_id=a.id " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " left join ${schema_name}._sc_asset b on b._id=w2 .relation_id " +
//            " left join ${schema_name}._sc_asset_category c on b.category_id=c.id   " +
//            " left join ${schema_name}._sc_work_fee d on d.sub_work_code=w2.sub_work_code  " +
//            " left join ${schema_name}._sc_currency e on e.id= d.currency  " +
//            " left join ${schema_name}._sc_facilities f on w.customer_id= f.id  " +
//            "where d.fee_amount NOTNULL ${condition}  "+
//            "group by a.id, a.title,a.parentid,c.category_name,e.currency_code,f.customer_name ) sf on sf.parentid=p.id or sf.id=p.id " +
//            "where p.parentid=0 and sf.fee_amount NOTNULL  "+
//            "group by p.id, p.title,sf.category_name,sf.currency_code,sf.customer_name )s ")
//    Map<String, Object>   getWorkCostListDetailCountforCustomer(@Param("schema_name") String schema_name, @Param("condition") String condition);
//    //按设备类型查看费用报表的总数
//    @Select("select count(1) as total, sum(count_cost)as cost from ( " +
//            "select a.short_title,a.facilitycode,a.id,a.remark,sum(d.fee_amount)as count_cost,e.currency_code as unit,g.category_name,g.id as category_id " +
//            "from ${schema_name}._sc_facilities a " +
//            " left join ${schema_name}._sc_works b on b.customer_id=a.id " +
//            " left join ${schema_name}._sc_works_detail c on  c.work_code=b.work_code and c.is_main=1 " +
//            " left join ${schema_name}._sc_work_fee d on d.sub_work_code=c.sub_work_code  " +
//            " left join ${schema_name}._sc_currency e on e.id= d.currency  " +
//            " left join ${schema_name}._sc_asset f on f._id=c .relation_id " +
//            " left join ${schema_name}._sc_asset_category g on f.category_id=g.id   " +
//            "where d.fee_amount NOTNULL and a.org_type = "+ SqlConstant.FACILITY_SUPPLIER +" ${condition} " +
//            "group by a.id,a.short_title,a.facilitycode,e.currency_code,g.category_name,g.id)s")
//    Map<String, Object>   getWorkCostListforCustomerCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //按设备类型查看费用报表的总数
//    @Select("select a.short_title,a.facilitycode,a.id,a.remark,sum(d.fee_amount)as count_cost,e.currency_code as unit,g.category_name,g.id as category_id " +
//            "from ${schema_name}._sc_facilities a " +
//            " left join ${schema_name}._sc_works b on b.customer_id=a.id " +
//            " left join ${schema_name}._sc_works_detail c on  c.work_code=b.work_code and c.is_main=1 " +
//            " left join ${schema_name}._sc_work_fee d on d.sub_work_code=c.sub_work_code  " +
//            " left join ${schema_name}._sc_currency e on e.id= d.currency  " +
//            " left join ${schema_name}._sc_asset f on f._id=c .relation_id " +
//            " left join ${schema_name}._sc_asset_category g on f.category_id=g.id   " +
//            "where d.fee_amount NOTNULL and a.org_type = "+ SqlConstant.FACILITY_SUPPLIER +" ${condition} " +
//            "group by a.id,a.short_title,a.facilitycode,e.currency_code,g.category_name,g.id " +
//            "order by a.id  desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getWorkCostListfoCustomer(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按设备类型查看费用报表的总数
//    @Select("select count(1) as total, sum(count_cost)as cost from ( " +
//            "SELECT " +
//            "A .username, " +
//            "A .account, " +
//            "e.currency_code AS unit, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "G .id AS category_id, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_user A " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .receive_account = A .account " +
//            "LEFT JOIN ${schema_name}._sc_works b ON b.customer_id IS NULL " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category G ON f.category_id = G . ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "A.username, " +
//            "A.account, " +
//            "e.currency_code, " +
//            "G.category_name, " +
//            "G.id,  "+
//            "G.category_code )s" )
//    Map<String, Object>   getWorkCostListforEmployeeCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //按设备类型查看费用报表的总数
//    @Select("SELECT " +
//            "A .username, " +
//            "A .account, " +
//            "e.currency_code AS unit, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "G .id AS category_id, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_user A " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .receive_account = A .account " +
//            "LEFT JOIN ${schema_name}._sc_works b ON b.customer_id IS NULL " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category G ON f.category_id = G . ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "A .username, " +
//            "A .account, " +
//            "e.currency_code, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "G .id "+
//            "ORDER BY " +
//            " e.currency_code, A .account DESC LIMIT ${ page } OFFSET ${begin} " )
//    List<Map<String, Object>> getWorkCostListforEmployee(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按设备类型查看费用报表详情
//    @Select( "SELECT " +
//            "A .username  , " +
//            "A .account, " +
//            "e.currency_code AS unit, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "fa.title, " +
//            "fa.facilitycode, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_user A " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .receive_account = A .account " +
//            "LEFT JOIN ${schema_name}._sc_works b ON b.customer_id IS NULL " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category G ON f.category_id = G . ID " +
//            "LEFT JOIN ${schema_name}.get_facility_with_center() fwc ON fwc.fid = b.facility_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities fa ON fwc.fcid = fa. ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "A .username, " +
//            "A .account, " +
//            "e.currency_code, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "fa.title, " +
//            "fa.facilitycode " +
//            "ORDER BY " +
//            "facilitycode " +
//            " LIMIT ${ page } OFFSET ${begin} " )
//    List<Map<String,Object>> getWorkCostListDetailforEmployee(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//    //按设备类型查看费用报表的总数
//    @Select("select count(1) as total, sum(count_cost)as cost from ( " +
//            "SELECT " +
//            "A .username, " +
//            "A .account, " +
//            "e.currency_code AS unit, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "fa.title, " +
//            "fa.facilitycode, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_user A " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .receive_account = A .account " +
//            "LEFT JOIN ${schema_name}._sc_works b ON b.customer_id IS NULL " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category G ON f.category_id = G . ID " +
//            "LEFT JOIN ${schema_name}.get_facility_with_center() fwc ON fwc.fid = b.facility_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities fa ON fwc.fcid = fa. ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "A .username, " +
//            "A .account, " +
//            "e.currency_code, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "fa.title, " +
//            "fa.facilitycode " +
//            "ORDER BY " +
//            "facilitycode )s" )
//    Map<String, Object>   getWorkCostListDetailCountforEmployee(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //按组织查询查看费用报表的总数
//    @Select("select count(1) as total, sum(count_cost)as cost from ( " +
//            "SELECT " +
//            "fa .id  , " +
//            "fa .title  , " +
//            "fa .facilitycode, " +
//            "e.currency_code AS unit, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_facilities A " +
//            "LEFT JOIN ${schema_name}._sc_works b ON  " +
//            " A .id = b.facility_id " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}.get_facility_with_center() fwc ON fwc.fid = A.id " +
//            "LEFT JOIN ${schema_name}._sc_facilities fa ON fwc.fcid = fa. ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "fa .id, " +
//            "fa .title, " +
//            "fa .facilitycode, " +
//            "e.currency_code " +
//            "ORDER BY " +
//            " fa .facilitycode)s" )
//    Map<String, Object>   getWorkCostListforOrganizationCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //按设备类型查看费用报表的总数
//    @Select(  "SELECT " +
//            "fa .id  , " +
//            "fa .title  , " +
//            "fa .facilitycode, " +
//            "e.currency_code AS unit, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_facilities A " +
//            "LEFT JOIN ${schema_name}._sc_works b ON  " +
//            " A .id = b.facility_id " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}.get_facility_with_center() fwc ON fwc.fid = A.id " +
//            "LEFT JOIN ${schema_name}._sc_facilities fa ON fwc.fcid = fa. ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "fa .id, " +
//            "fa .title, " +
//            "fa .facilitycode, " +
//            "e.currency_code " +
//            "ORDER BY " +
//            " fa .facilitycode DESC LIMIT ${ page } OFFSET ${begin} " )
//    List<Map<String, Object>> getWorkCostListforOrganization(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按设备类型查看费用报表详情
//    @Select( "SELECT " +
//            "fa .id  , " +
//            "fa .title  , " +
//            "fa .facilitycode, " +
//            "e.currency_code AS unit, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_facilities A " +
//            "LEFT JOIN ${schema_name}._sc_works b ON  " +
//            " A .id = b.facility_id " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category G ON f.category_id = G . ID " +
//            "LEFT JOIN ${schema_name}.get_facility_with_center() fwc ON fwc.fid = A.id " +
//            "LEFT JOIN ${schema_name}._sc_facilities fa ON fwc.fcid = fa. ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "fa .id, " +
//            "fa .title, " +
//            "fa .facilitycode, " +
//            "e.currency_code, " +
//            "G .category_name, " +
//            "G .category_code " +
//            "ORDER BY " +
//            "fa.facilitycode DESC " +
//            " LIMIT ${ page } OFFSET ${begin} " )
//    List<Map<String,Object>> getWorkCostListDetailforOrganization(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//    //按设备类型查看费用报表的总数
//    @Select("select count(1) as total, sum(count_cost)as cost from ( " +
//            "SELECT " +
//            "fa .id  , " +
//            "fa .title  , " +
//            "fa .facilitycode, " +
//            "e.currency_code AS unit, " +
//            "G .category_name, " +
//            "G .category_code, " +
//            "COALESCE (SUM(d.fee_amount), 0) AS count_cost " +
//            "FROM " +
//            "${schema_name}._sc_facilities A " +
//            "LEFT JOIN ${schema_name}._sc_works b ON  " +
//            " A .id = b.facility_id " +
//            "LEFT JOIN ${schema_name}._sc_works_detail C ON C .is_main = 1 " +
//            "AND C .work_code = b.work_code " +
//            "LEFT JOIN ${schema_name}._sc_work_fee d ON d.sub_work_code = C .sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_currency e ON e. ID = d.currency " +
//            "LEFT JOIN ${schema_name}._sc_asset f ON f._id = C .relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category G ON f.category_id = G . ID " +
//            "LEFT JOIN ${schema_name}.get_facility_with_center() fwc ON fwc.fid = A.id " +
//            "LEFT JOIN ${schema_name}._sc_facilities fa ON fwc.fcid = fa. ID " +
//            "WHERE d.fee_amount NOTNULL ${ condition } " +
//            "GROUP BY " +
//            "fa .id, " +
//            "fa .title, " +
//            "fa .facilitycode, " +
//            "e.currency_code, " +
//            "G .category_name, " +
//            "G .category_code " +
//            "ORDER BY " +
//            "fa.facilitycode DESC)s" )
//    Map<String, Object>   getWorkCostListDetailCountforOrganization(@Param("schema_name") String schema_name, @Param("condition") String condition);

}