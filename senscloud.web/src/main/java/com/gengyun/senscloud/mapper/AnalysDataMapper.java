package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnalysDataMapper {
//
//    /*
//    * 查询资产的总数，看板指标：total
//     */
//    @Select("select count(distinct dv._id) from ${schema_name}._sc_asset dv " +
//            "left join ${schema_name}._sc_asset_organization org on dv._id=org.asset_id " +
//            "left join ${schema_name}._sc_facilities f on org.org_id=f.id " +
//            "where intstatus>0 ${condition} and f.id is not null")
//    int getDashboardAssetTotal(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /*
//    * 查询维修单总数
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r  " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on r.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and r.status!=900 and r.create_time>=#{beginTime} and r.create_time<#{endTime} " +
//            "and f.id is not null ${condition}")
//    int getDashboardRepairTotal(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime, @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询维修单总数, 按状态
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r  " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on r.work_code=wd.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and r.status in (${status}) " +
//            "and f.id is not null and r.create_time>=#{beginTime} and r.create_time<#{endTime} ${condition}")
//    int getDashboardRepairTotalByStatus(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                        @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime, @Param("status") String status, @Param("leftCondition") String leftCondition);
//
//    /*
//   * 查询维修单总数, 查询已经完成的维修单
//    */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r  " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and wd.is_main = 1 and r.status = 60 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardRepairTotalFinished(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                        @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                        @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询保养单总数
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r  " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 2 and r.status!=900 and r.create_time>=#{beginTime} and r.create_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardMaintainTotal(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                  @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime, @Param("leftCondition") String leftCondition);
//
//    /*
//     * 查询保养，点检，巡检完成总数
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = #{business_type_id} and r.status!=900 " +
//            "and r.create_time>=#{beginTime} and r.create_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardFinishedTotal(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                  @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                  @Param("leftCondition") String leftCondition, @Param("business_type_id") int business_type_id);
//
//    /*
//   * 查询保养单总数，按状态、创建时间，查询已经完成的维修单
//    */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 2 and r.status =60 " +
//            "and r.create_time>='${beginTime}' and r.create_time<'${endTime}' and f.id is not null ${condition}")
//    int getDashboardMaintainTotalFinishedByCreateTime(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                                      @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                                      @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询保养总数，按状态
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 2 and r.status in (${status}) " +
//            "and r.create_time>=#{beginTime} and r.create_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardMaintainTotalByStatus(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                          @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                          @Param("status") String status, @Param("leftCondition") String leftCondition);
//
//    /*
//   * 查询保养单总数，按状态，查询已经完成的维修单
//    */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 2 and wd.is_main = 1 and r.status =60 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardMaintainTotalFinished(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                          @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                          @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询计划点检总数
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 4 and r.status!=900 and r.create_time>=#{beginTime} and r.create_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardSpotTotal(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                              @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                              @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询计划巡检总数
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 3 and r.status!=900 and r.create_time>=#{beginTime} and r.create_time<#{endTime} and f.id is not null ${condition}")
//    int getDashboardInspectionTotal(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                    @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                    @Param("leftCondition") String leftCondition);
//
//
//    /*
//    * 查询维修单总时长
//     */
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60),0) from " +
//            "(select distinct r.work_code,wd.begin_time,wd.finished_time " +
//            "from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and wd.is_main = 1 and r.status=60 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} and f.id is not null ${condition}" +
//            ") total")
//    double getDashboardRepairTotalMinute(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                         @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                         @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询维修到场总时长
//     */
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from wd.begin_time)-EXTRACT(EPOCH from r.occur_time))/60),0) from " +
//            "(select distinct r.work_code,wd.begin_time,r.occur_time " +
//            "from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and wd.is_main = 1 and r.status=60 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} and f.id is not null ${condition}" +
//            ") total")
//    double getDashboardRepairArriveTotalMinute(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                               @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                               @Param("leftCondition") String leftCondition);
//
//    /*
//    * 故障停机总时长
//     */
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from occur_time)-EXTRACT(EPOCH from last_finished_time))/60),0) from (" +
//            "select distinct w.work_code,occur_time,last_finished_time,w.relation_id " +
//            "from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=w.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=w.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and w.status=60 " +
////            "and wd.asset_running_status=3 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} " +
//            "and f.id is not null ${condition} " +
//            "group by w.work_code,occur_time,last_finished_time,w.relation_id  " +
//            "order by w.relation_id" +
//            ") total")
//    double getDashboardRepairStopTotalMinute(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                             @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                             @Param("leftCondition") String leftCondition);
//
//
//    @Select("select count (relation_id) from (" +
//            "select distinct w.work_code,occur_time,last_finished_time,w.relation_id " +
//            "from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=w.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=w.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and w.status=60 " +
////            "and wd.asset_running_status=3 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} " +
//            "and f.id is not null ${condition} " +
//            "group by w.work_code,occur_time,last_finished_time,w.relation_id  " +
//            "order by w.relation_id" +
//            ") total")
//    double getDashboardRepairStopTotalMinuteCount(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                                  @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                                  @Param("leftCondition") String leftCondition);
//
//    /*
//    * 故障停机总次数
//     */
//    @Select("select count(distinct r.work_code) from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 1 and wd.is_main = 1 and r.status=60 and wd.asset_running_status=3 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} and f.id is not null ${condition}")
//    double getDashboardRepairStopTotalTimes(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                            @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                            @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询保养单总时长
//     */
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from wd.begin_time))/60),0) from " +
//            "(select distinct r.work_code,wd.begin_time,wd.finished_time " +
//            "from ${schema_name}._sc_works r " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            "where wt.business_type_id = 2 and wd.is_main = 1 and r.status=60 " +
//            "and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} and f.id is not null ${condition}" +
//            ") total")
//    double getDashboardMaintainTotalMinute(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                           @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                           @Param("leftCondition") String leftCondition);
//
//    /*
//    * 查询保养单总延迟天数
//     */
//    @Select("select COALESCE(SUM((EXTRACT(EPOCH from wd.finished_time)-EXTRACT(EPOCH from r.deadline_time))/60/60/24),0) from " +
//            "(select distinct r.work_code,r.deadline_time,wd.finished_time " +
//            "from ${schema_name}._sc_works r  " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=r.work_type_id " +
//            "left join ${schema_name}._sc_asset dv on dv._id=r.relation_id " +
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=r.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org worg on wd.sub_work_code=worg.sub_work_code and wd.relation_id=worg.asset_id " +
//            "left join ${schema_name}._sc_facilities f on f.id=worg.facility_id " +
//            "${leftCondition}" +
//            " where wt.business_type_id = 2 and wd.is_main = 1 and r.status=60 " +
//            "and r.deadline_time<wd.finished_time and wd.finished_time>=#{beginTime} and wd.finished_time<#{endTime} " +
//            "and f.id is not null ${condition}" +
//            ") total")
//    double getDashboardMaintainTotalDelayMinute(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                                @Param("beginTime") Timestamp beginTime, @Param("endTime") Timestamp endTime,
//                                                @Param("leftCondition") String leftCondition);
//
//    @Select("select sum(cast(chang as integer)) from (" +
//            "select distinct dv.* from " +
//            "(" +
//            "select * from ${schema_name}._sc_asset, jsonb_to_record(properties) AS property(chang varchar)" +
//            ") as dv " +
//            "left join ${schema_name}._sc_asset_organization org on dv._id=org.asset_id " +
//            "left join ${schema_name}._sc_facilities f on org.org_id=f.id " +
//            "where intstatus > 0 and chang != '' and chang != 'null' and f.id is not null ${condition}" +
//            ") total")
//    double getDashboardChangTotal(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    @Select("select count(distinct _id) from ${schema_name}._sc_asset dv " +
//            "left join ${schema_name}._sc_asset_organization org on dv._id=org.asset_id " +
//            "left join ${schema_name}._sc_facilities f on org.org_id=f.id " +
//            "where intstatus > 0 and importment_level_id = #{importantLevelId} and f.id is not null ${condition}")
//    double getDashboardAssetTotalByImportantLevelId(@Param("schema_name") String schema_name, @Param("importantLevelId") Integer importantLevelId, @Param("condition") String condition);
}
