package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 生产作业单
 */
@Mapper
public interface ProductTaskMapper {
    /**
     * 新增生产作业单
     */
    @Insert(" INSERT  INTO ${schema_name}._sc_product_task (product_task_code, remark,create_time,create_user_id) " +
            " VALUES (#{pm.product_task_code}, #{pm.remark},#{pm.create_time},#{pm.create_user_id})")
    void insertProductTask(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增生产作业下井
     */
    @Insert(" <script>" +
            " INSERT  INTO ${schema_name}._sc_product_task_well (task_well_code,product_task_code,task_code,well_no,max_temperature," +
            " bottom_pressure,go_down_count,asset_out_code,create_time,create_user_id," +
            " is_contain_hydrogen_sulfide,cable_joint_location,total_vehicle_kilometers," +
            " branch_office,project_department,ranks,party_a,task_type,operation_purpose," +
            " well_type,construction_technology,is_use_source,bridge_plug_type,bit_size," +
            " maximum_slope,casing_size,ground_instrument_series,operation_items,well_depth," +
            " success_times,downhole_instrument_failures_times,survey_section_from,survey_section_to," +
            " measuring_meter,several_cores_are_designed,core_harvested_number,total_shooting_thickness," +
            " perforation_section,gun_type,one_way_kilometers,actual_arrival_time,departure_time,return_time," +
            " installation_time,demolition_time,well_occupation_time,total_time,breakdown_time,task_notes) " +
            " VALUES " +
            " <foreach collection='pms' item='pm' separator=','> " +
            "(#{pm.task_well_code},#{pm.product_task_code},#{pm.task_code},#{pm.well_no},#{pm.max_temperature},#{pm.bottom_pressure}," +
            " #{pm.go_down_count},#{pm.asset_out_code},#{pm.create_time},#{pm.create_user_id}," +
            " #{pm.is_contain_hydrogen_sulfide},#{pm.cable_joint_location},#{pm.total_vehicle_kilometers},#{pm.branch_office}," +
            " #{pm.project_department},#{pm.ranks},#{pm.party_a},#{pm.task_type},#{pm.operation_purpose},#{pm.well_type},#{pm.construction_technology}," +
            " #{pm.is_use_source},#{pm.bridge_plug_type},#{pm.bit_size},#{pm.maximum_slope},#{pm.casing_size},#{pm.ground_instrument_series}," +
            " #{pm.operation_items},#{pm.well_depth},#{pm.success_times},#{pm.downhole_instrument_failures_times},#{pm.survey_section_from}," +
            " #{pm.survey_section_to},#{pm.measuring_meter},#{pm.several_cores_are_designed},#{pm.core_harvested_number}," +
            " #{pm.total_shooting_thickness},#{pm.perforation_section},#{pm.gun_type},#{pm.one_way_kilometers},#{pm.actual_arrival_time}," +
            " #{pm.departure_time},#{pm.return_time},#{pm.installation_time},#{pm.demolition_time},#{pm.well_occupation_time}," +
            " #{pm.total_time},#{pm.breakdown_time},#{pm.task_notes})" +
            " </foreach> </script>")
    void insertProductTaskWell(@Param("schema_name") String schema_name, @Param("pms") List<Map<String, Object>> pms);

    /**
     * 新增生产作业下井次数
     */
    @Insert(" <script> INSERT  INTO ${schema_name}._sc_product_task_well_item (task_well_item_code,task_well_code,go_down_time,come_out_time,total_tension_jamming,net_tension_jamming,depth_of_encounter) " +
            " VALUES " +
            " <foreach collection='pms' item='pm' separator=','> " +
            " (#{pm.task_well_item_code},#{pm.task_well_code},#{pm.go_down_time},#{pm.come_out_time},#{pm.total_tension_jamming},#{pm.net_tension_jamming},#{pm.depth_of_encounter})" +
            " </foreach> </script>")
    void insertProductTaskWellItem(@Param("schema_name") String schema_name, @Param("pms") List<Map<String, Object>> pms);

    /**
     * 新增生产作业下井设备
     */
    @Insert(" INSERT INTO ${schema_name}._sc_product_task_well_item_asset (asset_id,task_well_item_code) " +
            " VALUES (#{pm.asset_id},#{pm.task_well_item_code})")
    void insertProductTaskWellItemAsset(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 获取生产作业单列表
     */
    @Select(" <script>" +
            " SELECT p.product_task_code,p.remark,to_char(p.create_time, 'YYYY-MM-DD hh24:mi:ss') AS create_time,p.create_user_id,u.user_name AS create_user_name " +
            ",(((SELECT count(1) FROM ${schema_name}._sc_product_task_well ptw WHERE ptw.product_task_code = p.product_task_code AND ptw.max_temperature is null )) + " +
            " (SELECT count(1) FROM ${schema_name}._sc_product_task_well ptw LEFT JOIN ${schema_name}._sc_product_task_well_item ptwi ON ptw.task_well_code = ptwi.task_well_code " +
            " WHERE ptw.product_task_code = p.product_task_code AND  ptwi.come_out_time is null)) as un_do_count" +
            " FROM ${schema_name}._sc_product_task AS p " +
            " LEFT JOIN ${schema_name}._sc_user AS u ON p.create_user_id = u.id " +
            " WHERE 1=1 " +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " p.product_task_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when>" +
            " ORDER BY p.create_time DESC ${pm.pagination} </script>")
    List<Map<String, Object>> findProductTaskPage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 获取生产作业单列表数量
     */
    @Select(" <script>" +
            " SELECT count(p.product_task_code)" +
            " FROM ${schema_name}._sc_product_task AS p" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON p.create_user_id = u.id " +
            " WHERE 1=1 " +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " p.product_task_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " ) " +
            " </when>" +
            " </script>")
    Integer findProductTaskPageCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 获取生产作业单详情
     */
    @Select(" SELECT u.user_name AS create_user_name,p.product_task_code,p.remark,to_char(p.create_time, 'YYYY-MM-DD hh24:mi:ss') AS create_time,p.create_user_id" +
            " FROM ${schema_name}._sc_product_task AS p" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON u.id = p.create_user_id " +
            " WHERE p.product_task_code = #{product_task_code}")
    Map<String, Object> findProductTaskInfo(@Param("schema_name") String schema_name, @Param("product_task_code") String product_task_code);

    /**
     * 生产作业下井列表
     */
    @Select(" SELECT ptw.task_well_code AS id,'' AS parent_id,NULL AS go_down_time,NULL AS come_out_time,ptw.product_task_code,ptw.task_code," +
            " ptw.well_no,ptw.max_temperature,ptw.bottom_pressure," +
            " (SELECT COUNT(task_well_code) FROM ${schema_name}._sc_product_task_well_item WHERE task_well_code=ptw.task_well_code) AS go_down_count," +
            " ptw.asset_out_code," +
            " ((case when ptw.max_temperature is NULL then 1 else 0 end ) + (SELECT count(1) FROM ${schema_name}._sc_product_task_well_item ptwi " +
            " WHERE ptw.task_well_code = ptwi.task_well_code AND ptwi.come_out_time is NULL)) as un_do_count, " +
            " ptw.is_contain_hydrogen_sulfide,ptw.cable_joint_location,ptw.total_vehicle_kilometers," +
            " '' AS total_tension_jamming,'' AS net_tension_jamming,'' AS depth_of_encounter," +
            " ptw.is_contain_hydrogen_sulfide,ptw.cable_joint_location,ptw.total_vehicle_kilometers,ptw.branch_office,ptw.project_department," +
            " ptw.ranks,ptw.party_a,ptw.task_type,ptw.operation_purpose,ptw.well_type,ptw.construction_technology,ptw.is_use_source,ptw.bridge_plug_type," +
            " ptw.bit_size,ptw.maximum_slope,ptw.casing_size,ptw.ground_instrument_series,ptw.operation_items,ptw.well_depth,ptw.success_times," +
            " ptw.downhole_instrument_failures_times,ptw.survey_section_from,ptw.survey_section_to,ptw.measuring_meter,ptw.several_cores_are_designed," +
            " ptw.core_harvested_number,ptw.total_shooting_thickness,ptw.perforation_section,ptw.gun_type,ptw.one_way_kilometers,ptw.actual_arrival_time," +
            " ptw.departure_time,ptw.return_time,ptw.installation_time,ptw.demolition_time,ptw.well_occupation_time,ptw.total_time,ptw.breakdown_time," +
            " ptw.task_notes " +
            " FROM ${schema_name}._sc_product_task_well AS ptw " +
            " WHERE ptw.product_task_code = #{product_task_code} " +
            " UNION" +
            " SELECT ptwi.task_well_item_code AS id,ptwi.task_well_code AS parent_id," +
            " to_char(ptwi.go_down_time, 'YYYY-MM-DD hh24:mi:ss') AS go_down_time," +
            " to_char(ptwi.come_out_time, 'YYYY-MM-DD hh24:mi:ss') AS come_out_time," +
            " ptw.product_task_code," +
            " ptw.task_code,ptw.well_no,NULL as max_temperature,NULL as bottom_pressure,1 AS go_down_count," +
            " ptw.asset_out_code,case when ptwi.come_out_time is NULL then 1 else 0 end as un_do_count ," +
            " '' AS is_contain_hydrogen_sulfide,'' AS cable_joint_location,'' AS total_vehicle_kilometers," +
            " ptwi.total_tension_jamming, ptwi.net_tension_jamming, ptwi.depth_of_encounter," +
            " '' AS is_contain_hydrogen_sulfide,'' AS cable_joint_location,'' AS total_vehicle_kilometers,'' AS branch_office,'' AS project_department," +
            " '' AS ranks,'' AS party_a,'' AS task_type,'' AS operation_purpose,'' AS well_type,'' AS construction_technology,'' AS is_use_source,'' AS bridge_plug_type," +
            " '' AS bit_size,'' AS maximum_slope,'' AS casing_size,'' AS ground_instrument_series,'' AS operation_items,'' AS well_depth,'' AS success_times," +
            " '' AS downhole_instrument_failures_times,'' AS survey_section_from,'' AS survey_section_to,'' AS measuring_meter,'' AS several_cores_are_designed," +
            " '' AS core_harvested_number,'' AS total_shooting_thickness,'' AS perforation_section,'' AS gun_type,'' AS one_way_kilometers,NULL AS actual_arrival_time," +
            " NULL AS departure_time,NULL AS return_time,NULL AS installation_time,NULL AS demolition_time,'' AS well_occupation_time,'' AS total_time,'' AS breakdown_time," +
            " '' AS task_notes " +
            " FROM ${schema_name}._sc_product_task_well_item AS ptwi" +
            " LEFT JOIN ${schema_name}._sc_product_task_well AS ptw ON ptwi.task_well_code=ptw.task_well_code " +
            " WHERE ptw.product_task_code = #{product_task_code}")
    List<Map<String, Object>> findProductTaskWellList(@Param("schema_name") String schema_name, @Param("product_task_code") String product_task_code);

    /**
     * 更新生产作业单主
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_product_task" +
            " SET product_task_code = #{pm.product_task_code}" +
            " <when test='pm.remark!=null'>" +
            " ,remark = #{pm.remark}" +
            " </when>" +
            " <when test='pm.create_time!=null'>" +
            " ,create_time = #{pm.create_time}::timestamp " +
            " </when>" +
            " <when test='pm.create_user_id!=null'>" +
            " ,create_user_id = #{pm.create_user_id}" +
            " </when>" +
            " WHERE product_task_code = #{pm.product_task_code}" +
            " </script>")
    void updateProductTask(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 更新生产作业下井表
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_product_task_well " +
            " SET task_well_code = #{pm.id}" +
            " <when test='pm.asset_out_code!=null'>" +
            " ,asset_out_code = #{pm.asset_out_code}" +
            " </when>" +
            " <when test='pm.max_temperature!=null'>" +
            " ,max_temperature = #{pm.max_temperature}::float" +
            " </when>" +
            " <when test='pm.bottom_pressure!=null'>" +
            " ,bottom_pressure = #{pm.bottom_pressure}::float" +
            " </when>" +
            " <when test='pm.is_contain_hydrogen_sulfide!=null'>" +
            " ,is_contain_hydrogen_sulfide = #{pm.is_contain_hydrogen_sulfide}" +
            " </when>" +
            " <when test='pm.cable_joint_location!=null'>" +
            " ,cable_joint_location = #{pm.cable_joint_location}" +
            " </when>" +
            " <when test='pm.total_vehicle_kilometers!=null'>" +
            " ,total_vehicle_kilometers = #{pm.total_vehicle_kilometers}" +
            " </when>" +
            " WHERE task_well_code = #{pm.id}" +
            " </script>")
    void updateProductTaskWell(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新生产作业下井次数
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_product_task_well_item " +
            " SET task_well_item_code = #{pm.id}" +
            " <when test='pm.go_down_time!=null'>" +
            " ,go_down_time = #{pm.go_down_time}::timestamp " +
            " </when>" +
            " <when test='pm.come_out_time!=null'>" +
            " ,come_out_time = #{pm.come_out_time}::timestamp " +
            " </when>" +
            " <when test='pm.depth_of_encounter!=null'>" +
            " ,depth_of_encounter = #{pm.depth_of_encounter} " +
            " </when>" +
            " <when test='pm.net_tension_jamming!=null'>" +
            " ,net_tension_jamming = #{pm.net_tension_jamming} " +
            " </when>" +
            " <when test='pm.total_tension_jamming!=null'>" +
            " ,total_tension_jamming = #{pm.total_tension_jamming} " +
            " </when>" +
            " WHERE task_well_item_code = #{pm.id}" +
            " </script>")
    void updateProductTaskWellItem(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据下井次数编码获取生产作业下井设备列表
     */
    @Select(" SELECT ptwia.asset_id,ptwia.task_well_item_code,a.asset_name,a.category_id,ac.category_name,a.asset_code,a.properties::text " +
            " FROM  ${schema_name}._sc_product_task_well_item_asset ptwia" +
            " LEFT JOIN ${schema_name}._sc_asset a ON ptwia.asset_id = a.id" +
            " LEFT JOIN ${schema_name}._sc_asset_category ac ON a.category_id = ac.id " +
            " WHERE task_well_item_code = #{task_well_item_code} ")
    List<Map<String, Object>> findProductTaskWellItemAssetByCode(@Param("schema_name") String schema_name, @Param("task_well_item_code") String task_well_item_code);

    /**
     * 获取生产作业下井详情
     */
    @Select(" SELECT task_well_code AS id,max_temperature,bottom_pressure,asset_out_code," +
            "is_contain_hydrogen_sulfide,cable_joint_location,total_vehicle_kilometers,well_occupation_time,total_time " +
            " FROM ${schema_name}._sc_product_task_well " +
            " WHERE task_well_code = #{task_well_code} ")
    Map<String, Object> findProductTaskWellInfo(@Param("schema_name") String schema_name, @Param("task_well_code") String task_well_item_code);

    /**
     * 获取生产作业下井次数详情
     */
    @Select(" SELECT  t.task_well_item_code AS id," +
            " to_char(t.go_down_time, 'YYYY-MM-DD hh24:mi:ss') AS go_down_time," +
            " to_char(t.come_out_time, 'YYYY-MM-DD hh24:mi:ss') AS come_out_time,total_tension_jamming,net_tension_jamming,depth_of_encounter" +
            " FROM ${schema_name}._sc_product_task_well_item t" +
            " WHERE t.task_well_item_code = #{task_well_item_code} ")
    Map<String, Object> findProductTaskWellItemInfo(@Param("schema_name") String schema_name, @Param("task_well_item_code") String task_well_item_code);

    /**
     * 根据生产作业信息编码查询生产作业下井次数信息
     */
    @Select("SELECT *  FROM ${schema_name}._sc_product_task_well_item WHERE task_well_code = #{task_well_code} limit 1 ")
    Map<String, Object> findProductTaskWellItemByTaskWellCode(@Param("schema_name") String schema_name, @Param("task_well_code") String task_well_code);

    /**
     * 删除生产作业单
     */
    @Delete(" DELETE  FROM ${schema_name}._sc_product_task WHERE product_task_code = #{product_task_code}")
    void deleteProductTask(@Param("schema_name") String schema_name, @Param("product_task_code") String product_task_code);

    /**
     * 删除生产作业下井信息
     */
    @Delete(" DELETE  FROM ${schema_name}._sc_product_task_well WHERE task_well_code = #{task_well_code}")
    void deleteProductTaskWell(@Param("schema_name") String schema_name, @Param("task_well_code") String task_well_code);

    /**
     * 删除生产作业下井次数信息
     */
    @Delete(" DELETE  FROM ${schema_name}._sc_product_task_well_item WHERE task_well_item_code = #{task_well_item_code}")
    void deleteProductTaskWellItem(@Param("schema_name") String schema_name, @Param("task_well_item_code") String task_well_item_code);

    /**
     * 删除生产作业下井次数设备
     */
    @Delete(" DELETE  FROM ${schema_name}._sc_product_task_well_item_asset WHERE task_well_item_code = #{task_well_item_code}")
    void deleteProductTaskWellItemAsset(@Param("schema_name") String schema_name, @Param("task_well_item_code") String task_well_item_code);

    /**
     * 删除生产作业下井次数设备
     */
    @Delete(" DELETE  FROM ${schema_name}._sc_product_task_well_item_asset WHERE task_well_item_code = #{task_well_item_code} AND asset_id = #{asset_id}")
    void deleteProductTaskWellItemAssetById(@Param("schema_name") String schema_name, @Param("task_well_item_code") String task_well_item_code, @Param("asset_id") String asset_id);

    /**
     * 删除生产作业下井次数设备根据生产作业单编码
     */
    @Delete(" DELETE FROM ${schema_name}._sc_product_task_well_item_asset AS ptwia" +
            " where ptwia.task_well_item_code in (SELECT task_well_item_code FROM   ${schema_name}._sc_product_task_well_item AS ptwi " +
            " LEFT JOIN ${schema_name}._sc_product_task_well AS ptw ON ptwi.task_well_code = ptw.task_well_code" +
            " WHERE ptw.product_task_code=#{product_task_code} )")
    void deleteProductTaskWellItemAssetByProductTaskCode(@Param("schema_name") String schema_name, @Param("product_task_code") String product_task_code);

    /**
     * 删除生产作业下井次数信息根据生产作业单编码
     */
    @Delete(" DELETE   FROM  ${schema_name}._sc_product_task_well_item AS ptwi " +
            " WHERE ptwi.task_well_code IN (SELECT task_well_code FROM ${schema_name}._sc_product_task_well AS ptw " +
            " WHERE ptw.product_task_code=#{product_task_code})")
    void deleteProductTaskWellItemByProductTaskCode(@Param("schema_name") String schema_name, @Param("product_task_code") String product_task_code);

    /**
     * 删除生产作业下井信息根据生产作业单编码
     */
    @Delete(" DELETE FROM   ${schema_name}._sc_product_task_well AS ptw " +
            " WHERE ptw.product_task_code=#{product_task_code}")
    void deleteProductTaskWellByProductTaskCode(@Param("schema_name") String schema_name, @Param("product_task_code") String product_task_code);

    /**
     * 根据任务单号查询是否已经有任务
     */
    @Select(" SELECT * FROM ${schema_name}._sc_product_task_well WHERE task_code = #{task_code} limit 1")
    Map<String, Object> findProductTaskWellByTaskCode(@Param("schema_name") String schema_name, @Param("task_code") String task_code);

    /**
     * 根据任务清单code获取设备列表
     *
     * @param schemaName     入参
     * @param task_well_code 入参
     * @return 获取设备列表
     */
    @Select("SELECT ptwia.* " +
            "FROM ${schema_name}._sc_product_task_well_item_asset AS ptwia " +
            "JOIN ${schema_name}._sc_product_task_well_item AS ptwi ON ptwia.task_well_item_code = ptwi.task_well_item_code " +
            "WHERE ptwi.task_well_code = #{task_well_code} ")
    List<Map<String, Object>> findProductTaskWellItemAssetByTaskWellCode(@Param("schema_name") String schemaName, @Param("task_well_code") String task_well_code);

    /**
     * 查询出库单下设备列表列表
     *
     * @param schemaName 入参
     * @param pm         入参
     * @return 设备列表列表
     */
    @Select("<script>" +
            "SELECT a.id as asset_id,a.asset_code,a.asset_name,s.status,ac.category_name,ars.running_status,a.properties::text " +
            "FROM ${schema_name}._sc_asset_works aw  " +
            "JOIN ${schema_name}._sc_asset_works_detail awd ON aw.work_code = awd.work_code " +
            "JOIN ${schema_name}._sc_asset_works_detail_item awdi ON awd.sub_work_code = awdi.sub_work_code " +
            "JOIN ${schema_name}._sc_asset a ON awdi.asset_id = a.id " +
            "LEFT JOIN ${schema_name}._sc_asset_status s ON a.status = s.id " +
            "LEFT JOIN ${schema_name}._sc_asset_category ac ON a.category_id = ac.id " +
            "LEFT JOIN ${schema_name}._sc_asset_running_status ars ON a.running_status_id = ars.ID " +
            "WHERE aw.work_code = #{pm.asset_out_code} AND aw.status = 60 AND awd.is_main = 1 " +
            "<if test=\"pm.keywordSearch!=null and pm.keywordSearch!=''\"> " +
            " and (lower(a.asset_name) like CONCAT('%', lower(#{pm.keywordSearch}), '%')  " +
            " or lower(a.asset_code) like CONCAT('%', lower(#{pm.keywordSearch}), '%') ) " +
            " </if> " +
            " ORDER BY a.create_time DESC  </script>")
    List<Map<String, Object>> findAssetListByAssetOutCode(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    @Select(" SELECT ptw.task_well_code AS id,ptw.max_temperature,ptw.bottom_pressure,ptw.asset_out_code " +
            " FROM ${schema_name}._sc_product_task_well ptw" +
            " LEFT JOIN ${schema_name}._sc_product_task_well_item ptwi on ptw.task_well_code=ptwi.task_well_code " +
            " WHERE ptwi.task_well_item_code = #{task_well_item_code} ")
    Map<String, Object> findProductTaskWellInfoByTaskWellItemCode(@Param("schema_name") String schemaName, @Param("task_well_item_code") String task_well_item_code);

    /**
     * 根据生产下井作业编码获取所有的设备
     */
    @Select(" SELECT ptw.task_well_code,ptwi.task_well_item_code,ptwia.asset_id,a.asset_code,a.file_ids,a.properties,a.asset_name" +
            " FROM ${schema_name}._sc_product_task_well_item_asset AS ptwia" +
            " LEFT JOIN ${schema_name}._sc_product_task_well_item AS ptwi ON ptwia.task_well_item_code = ptwi.task_well_item_code" +
            " LEFT JOIN ${schema_name}._sc_product_task_well AS ptw ON ptwi.task_well_code = ptw.task_well_code" +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON ptwia.asset_id =a.id" +
            " WHERE ptw.task_well_code = #{task_well_code} ")
    List<Map<String, Object>> findProductTaskWellItemAssetList(@Param("schema_name") String schemaName, @Param("task_well_code") String task_well_code);
}
