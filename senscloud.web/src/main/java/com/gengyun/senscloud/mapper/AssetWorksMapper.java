package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 设备工单
 */
@Mapper
public interface AssetWorksMapper {
    /**
     * 根据设备工单编码获取设备工单详情
     */
    @Select(" SELECT * FROM ${schema_name}._sc_asset_works WHERE work_code = #{work_code}")
    Map<String, Object> findAssetWorksInfoByCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据设备工单编码获取设备工单详情列表
     */
    @Select(" SELECT * FROM ${schema_name}._sc_asset_works_detail WHERE work_code = #{work_code}")
    List<Map<String, Object>> findAssetWorksDetailListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);


    /**
     * 根据设备工单编码获取设备工单详情列表
     */
    @Select(" SELECT awd.*,u.user_name AS duty_user_name " +
            " FROM ${schema_name}._sc_asset_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 AND awd.status <> 110 ")
    List<Map<String, Object>> findSubAssetWorksDetailListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据设备工单编码获取设备工单自己为处理人的子工单
     */
    @Select(" SELECT awd.sub_work_code,awd.work_code,aw.work_type_id,awd.asset_id,u.user_name AS duty_user_name,a.asset_name,a.asset_code,a.category_id,ac.category_name " +
            " FROM ${schema_name}._sc_asset_works_detail awd" +
            " LEFT JOIN ${schema_name}._sc_asset_works_detail_item awdi ON awdi.sub_work_code = awd.sub_work_code" +
            " LEFT JOIN ${schema_name}._sc_asset_works aw ON aw.work_code = awd.work_code" +
            " LEFT JOIN ${schema_name}._sc_asset a ON a.id = awdi.asset_id" +
            " LEFT JOIN ${schema_name}._sc_asset_category ac ON ac.id = a.category_id " +
            " LEFT JOIN ${schema_name}._sc_user u ON u.id = awd.duty_user_id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 AND awd.status <> 110 AND awd.duty_user_id = #{user_id}")
    List<Map<String, Object>> findSubAssetWorksDetailListByWorkCodeUserId(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("user_id") String user_id);


    @Select(" SELECT DISTINCT tb.sub_work_code,tb.work_code,tb.work_type_id,tb.duty_user_name, " +
            "COALESCE(array_to_string(array_agg(DIStINCt (case when detc.field_code='position_code' then detc.value else null end )),','),'') as position_code, " +
            "COALESCE(array_to_string(array_agg(DIStINCt (case when detc.field_code='category_id' then detc.value else null end )),','),'') as category_id FROM " +
            "(SELECT awd.sub_work_code,awd.work_code,aw.work_type_id,u.user_name AS duty_user_name FROM " +
            "${schema_name}._sc_asset_works_detail awd " +
            "LEFT JOIN ${schema_name}._sc_asset_works aw ON aw.work_code = awd.work_code " +
            "LEFT JOIN ${schema_name}._sc_user u ON u.ID = awd.duty_user_id WHERE awd.work_code = #{work_code} " +
            "AND awd.is_main = - 1 AND awd.status <> 110 AND awd.status <> 60 AND awd.status <> 10 AND awd.status <> 900 AND awd.duty_user_id = #{user_id} ) tb " +
            "LEFT JOIN (select tb.sub_work_code,tb.field_form_code,tb.value,wcs.field_code " +
            "from (SELECT wc.sub_work_code, " +
            "to_jsonb ( jsonb_each_text ( jsonb_array_elements ( wc.field_value :: jsonb ) ) ) ->> 'key' as field_form_code, " +
            "to_jsonb ( jsonb_each_text ( jsonb_array_elements ( wc.field_value :: jsonb ) ) ) ->> 'value' as value " +
            "FROM ${schema_name}._sc_works_detail_column wc WHERE wc.field_code = 'inventory_table') tb " +
            "INNER JOIN ${schema_name}._sc_work_columns wcs on tb.field_form_code = wcs.field_form_code " +
            "where field_code in ('category_id','position_code')) detc ON detc.sub_work_code = tb.sub_work_code " +
            "INNER JOIN ${schema_name}.ACT_RU_VARIABLE arv ON arv.NAME_ = 'sub_work_code' " +
            "AND arv.TYPE_ = 'string' AND arv.TEXT_ = tb.sub_work_code " +
            "INNER JOIN ${schema_name}.ACT_RU_TASK ART ON ART.ID_ = arv.TASK_ID_ " +
            "OR ART.PROC_INST_ID_ = arv.EXECUTION_ID_ GROUP BY tb.sub_work_code,tb.work_code,tb.work_type_id,tb.duty_user_name")
    List<Map<String, Object>> findSubAssetWorksDetailListByWorkCodeUserIdNew(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("user_id") String user_id);

    /**
     * 查询所有子工单是否还有未完成的
     */
    @Select(" SELECT count(1) " +
            " FROM ${schema_name}._sc_asset_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 AND awd.status <> 110 AND awd.status <> 60  LIMIT 1 ")
    int findNoEndSubAssetWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据设备工单编码获取设备工单详情列表
     */
    @Select(" SELECT t1.*,t2.status,t3.work_type_id::varchar FROM ${schema_name}._sc_asset_works_detail_item t1 " +
            " LEFT JOIN ${schema_name}._sc_asset_works_detail t2 ON t1.sub_work_code = t2.sub_work_code " +
            " LEFT JOIN ${schema_name}._sc_asset_works t3 ON t2.work_code = t3.work_code " +
            " WHERE t2.work_code = #{work_code} AND t2.is_main = -1 ")
    List<Map<String, Object>> findSubAssetWorksDetailItemListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    @Select(" SELECT t1.*,t2.status,t3.work_type_id::varchar FROM ${schema_name}._sc_asset_works_detail_item t1 " +
            " LEFT JOIN ${schema_name}._sc_asset_works_detail t2 ON t1.sub_work_code = t2.sub_work_code " +
            " LEFT JOIN ${schema_name}._sc_asset_works t3 ON t2.work_code = t3.work_code " +
            " WHERE t2.work_code = #{work_code} AND t2.is_main = 1 ")
    List<Map<String, Object>> findAssetWorksDetailItemListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 根据设备工单编码获取设备工单详情列表
     */
    @Select(" SELECT aw.*,awd.* FROM ${schema_name}._sc_asset_works_detail awd" +
            " LEFT JOIN ${schema_name}._sc_asset_works aw on awd.work_code = aw.work_code" +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = 1 ")
    Map<String, Object> findMainAssetWorksDetailListByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 更新子工单（未完成数据）
     *
     * @param schemaName      入参
     * @param receive_user_id 入参
     * @param sub_work_code   入参
     */
    @Update("UPDATE ${schema_name}._sc_asset_works_detail SET duty_user_id =#{receive_user_id} WHERE sub_work_code = #{sub_work_code} and status <= 50 ")
    void updateWaitAssetWorkDetailBySubWorkCode(@Param("schema_name") String schemaName, @Param("receive_user_id") String receive_user_id, @Param("sub_work_code") String sub_work_code);

    /**
     * 根据设备工单详情编码获取详情
     */
    @Select(" SELECT aw.*,awd.* FROM ${schema_name}._sc_asset_works_detail awd" +
            " LEFT JOIN ${schema_name}._sc_asset_works aw on awd.work_code=aw.work_code" +
            " WHERE awd.sub_work_code = #{sub_work_code}")
    Map<String, Object> findAssetWorksDetailInfoByCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);


    /**
     * 查询设备工单分页数量
     */
    @Select(" <script>" +
            " SELECT count(1)" +
            " FROM ${schema_name}._sc_asset_works AS aw" +
            " LEFT JOIN  ${schema_name}._sc_asset_works_detail AS awd ON aw.work_code = awd.work_code " +
            " LEFT JOIN ${schema_name}._sc_user AS u ON aw.create_user_id = u.id" +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON awd.duty_user_id = u2.id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = aw.work_type_id" +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON ap.position_code = aw.position_code" +
            " inner join (select d.* from (select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w, ${schema_name}._sc_asset a " +
            " ,(SELECT distinct p.asset_position_code, at.category_id, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{pm.user_id} and p.asset_position_code is not null and at.category_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = a.id and a.position_code = p.asset_position_code and a.category_id = p.category_id and w.work_type_id = p.work_type_id " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{pm.user_id} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = p.asset_position_code and w.work_type_id = p.work_type_id " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{pm.user_id} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.is_main = 1 and (d.asset_id is null or position(',' in d.asset_id) &gt; 0) and d.work_code = w.work_code and w.work_type_id = p.work_type_id ) d) pd on pd.sub_work_code = awd.sub_work_code " +
            " WHERE  awd.is_main = 1" +
            //当前处理人是自己
            //" OR aw.create_user_id = #{pm.user_id} " +
            " <when test='pm.workTypeIdSearch!=null'>" +
            " AND aw.work_type_id IN " +
            "  <foreach collection='pm.workTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.statusSearch!=null'>" +
            " AND aw.status IN " +
            "  <foreach collection='pm.statusSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
//            " <when test='pm.assetCategoryIdSearch!=null'>" +
//            " AND awd.asset_category_id IN " +
//            "  <foreach collection='pm.assetCategoryIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
//            " <when test='pm.positionCodeSearch!=null'>" +
//            " AND aw.position_code IN " +
//            "  <foreach collection='pm.positionCodeSearch' item='id' open='(' close=')' separator=','> #{id}::varchar </foreach>" +
//            " </when>" +
//            " <when test='pm.assetModelIdSearch!=null'>" +
//            " AND awd.asset_model_id IN " +
//            "  <foreach collection='pm.assetModelIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
//            " <when test='pm.manufacturerIdSearch!=null'>" +
//            " AND awd.manufacturer_id IN " +
//            "  <foreach collection='pm.manufacturerIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
//            " <when test='pm.supplierIdSearch!=null'>" +
//            " AND aw.supplier_id IN " +
//            "  <foreach collection='pm.supplierIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
            " <when test='pm.beginTime!=null and pm.beginTime!=\"\" and pm.finishedTime!=\"\" and pm.finishedTime!=null'>" +
            " AND aw.create_time &gt;=#{pm.beginTime}::TIMESTAMP AND aw.create_time &lt;=#{pm.finishedTime}::TIMESTAMP  " +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " aw.work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u2.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR aw.position_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR ap.position_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR aw.remark LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when>" +
            " </script>")
    int findAssetWorksCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     *
     */
    @Select(" <script>" +
            " SELECT  aw.remark,awd.duty_user_id,aw.work_code,awd.sub_work_code,s2.status AS status_name,aw.status,aw.work_code,aw.position_code,aw.create_user_id," +
            " to_char(aw.create_time, 'YYYY-MM-DD hh24:mi:ss') AS create_time,ap.position_name," +
            " u.user_name AS create_user_name , u2.user_name AS duty_user_name,aw.work_type_id , wt.type_name AS work_type_name ," +
            " (CASE WHEN (SELECT count(1) FROM ${schema_name}._sc_asset_works_detail  WHERE work_code = aw.work_code AND status &lt;&gt; 60 AND status &lt;&gt; 110 AND is_main = -1  AND duty_user_id = #{pm.user_id}) > 0 THEN 1 ELSE -1 END ) AS is_have_sub " +
            " FROM ${schema_name}._sc_asset_works AS aw" +
            " LEFT JOIN  ${schema_name}._sc_asset_works_detail AS awd ON aw.work_code = awd.work_code " +
            " LEFT JOIN ${schema_name}._sc_user AS u ON aw.create_user_id = u.id" +
            " LEFT JOIN ${schema_name}._sc_user AS u2 ON awd.duty_user_id = u2.id" +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = aw.work_type_id" +
            " LEFT JOIN ${schema_name}._sc_status AS s2 ON s2.id =aw.status " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON ap.position_code =aw.position_code " +
            " inner join (select d.* from (select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w, ${schema_name}._sc_asset a " +
            " ,(SELECT distinct p.asset_position_code, at.category_id, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{pm.user_id} and p.asset_position_code is not null and at.category_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = a.id and a.position_code = p.asset_position_code and a.category_id = p.category_id and w.work_type_id = p.work_type_id " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{pm.user_id} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = p.asset_position_code and w.work_type_id = p.work_type_id " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{pm.user_id} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.is_main = 1 and (d.asset_id is null or position(',' in d.asset_id) &gt; 0) and d.work_code = w.work_code and w.work_type_id = p.work_type_id ) d) pd on pd.sub_work_code = awd.sub_work_code " +
            " WHERE awd.is_main = 1 " +
            //当前处理人是自己
            //" OR aw.create_user_id = #{pm.user_id} " +
            " <when test='pm.workTypeIdSearch!=null'>" +
            " AND aw.work_type_id IN " +
            "  <foreach collection='pm.workTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
            " <when test='pm.statusSearch!=null'>" +
            " AND aw.status IN " +
            "  <foreach collection='pm.statusSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when>" +
//            " <when test='pm.assetCategoryIdSearch!=null'>" +
//            " AND awd.asset_category_id IN " +
//            "  <foreach collection='pm.assetCategoryIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
//            " <when test='pm.positionCodeSearch!=null'>" +
//            " AND aw.position_code IN " +
//            "  <foreach collection='pm.positionCodeSearch' item='id' open='(' close=')' separator=','> #{id}::varchar </foreach>" +
//            " </when>" +
//            " <when test='pm.assetModelIdSearch!=null'>" +
//            " AND awd.asset_model_id IN " +
//            "  <foreach collection='pm.assetModelIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
//            " <when test='pm.manufacturerIdSearch!=null'>" +
//            " AND awd.manufacturer_id IN " +
//            "  <foreach collection='pm.manufacturerIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
//            " <when test='pm.supplierIdSearch!=null'>" +
//            " AND aw.supplier_id IN " +
//            "  <foreach collection='pm.supplierIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
//            " </when>" +
            " <when test='pm.beginTime!=null and pm.beginTime!=\"\" and pm.finishedTime!=\"\" and pm.finishedTime!=null'>" +
            " AND aw.create_time &gt;=#{pm.beginTime}::TIMESTAMP AND aw.create_time &lt;=#{pm.finishedTime}::TIMESTAMP  " +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            " aw.work_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u2.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR aw.position_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR ap.position_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR aw.remark LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " )" +
            " </when> order by aw.create_time DESC ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findAssetWorksPage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新设备工单概要表
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_asset_works " +
            " SET work_code = #{pm.work_code}" +
            " <when test='pm.position_code!=null'>" +
            " ,position_code =#{pm.position_code}" +
            " </when> " +
            " <when test='pm.work_type_id!=null'>" +
            " ,work_type_id = #{pm.work_type_id}::int" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " ,status =#{pm.status}::int" +
            " </when> " +
            " <when test='pm.remark!=null'>" +
            " ,remark =#{pm.remark}" +
            " </when> " +
            " <when test='pm.create_user_id!=null'>" +
            " ,create_user_id = #{pm.create_user_id}" +
            " </when> " +
            " WHERE work_code = #{pm.work_code}" +
            " </script>")
    void updateAssetWorksByWorkCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新设备工单详情
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_asset_works_detail " +
            " SET sub_work_code = #{pm.sub_work_code}" +
            " <when test='pm.asset_id!=null'>" +
            " ,asset_id =#{pm.asset_id}" +
            " </when> " +
            " <when test='pm.asset_name!=null'>" +
            " ,asset_name = #{pm.asset_name}" +
            " </when> " +
            " <when test='pm.asset_code!=null'>" +
            " ,asset_code =#{pm.asset_code}" +
            " </when> " +
            " <when test='pm.asset_category_id!=null'>" +
            " ,asset_category_id =#{pm.asset_category_id}::int" +
            " </when> " +
            " <when test='pm.asset_model_id!=null'>" +
            " ,asset_model_id = #{pm.asset_model_id}::int" +
            " </when> " +
            " <when test='pm.manufacturer_id!=null'>" +
            " ,manufacturer_id = #{pm.manufacturer_id}::int" +
            " </when> " +
            " <when test='pm.supplier_id!=null'>" +
            " ,supplier_id = #{pm.supplier_id}::int" +
            " </when> " +
            " <when test='pm.price!=null'>" +
            " ,price = #{pm.price}" +
            " </when> " +
            " <when test='pm.tax_rate!=null'>" +
            " ,tax_rate =#{pm.tax_rate}" +
            " </when> " +
            " <when test='pm.tax_price!=null'>" +
            " ,tax_price = #{pm.tax_price}" +
            " </when> " +
            " <when test='pm.currency_id!=null'>" +
            " ,currency_id = #{pm.currency_id}::int" +
            " </when> " +
            " <when test='pm.file_ids!=null'>" +
            " ,file_ids = #{pm.file_ids}" +
            " </when> " +
            " <when test='pm.status!=null'>" +
            " ,status = #{pm.status}::int" +
            " </when> " +
            " <when test='pm.work_code!=null'>" +
            " ,work_code = #{pm.work_code}" +
            " </when> " +
            " <when test='pm.duty_user_id!=null'>" +
            " ,duty_user_id = #{pm.duty_user_id}" +
            " </when> " +
            " WHERE sub_work_code = #{pm.sub_work_code}" +
            " </script>")
    void updateAssetWorksDetailBySubWorkCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据work_code查询设备子工单列表
     */
    @Select(" SELECT awd.*,a.asset_code,awdi.asset_id,a.asset_name " +
            " FROM ${schema_name}._sc_asset_works_detail AS awd" +
            " LEFT JOIN ${schema_name}._sc_asset_works_detail_item AS awdi ON awd.sub_work_code = awdi.sub_work_code " +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON awdi.asset_id = a.id" +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 ")
    List<Map<String, Object>> findSubAssetWorksDetailListBySubWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 获取子工单的item表单条信息
     */
    @Select("SELECT * FROM ${schema_name}._sc_asset_works_detail_item WHERE sub_work_code = #{sub_work_code} LIMIT 1  ")
    Map<String, Object> finSubAssetWorksDetailItemBySubWorkCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
//
//    @Insert(" INSERT INTO ${schema_name}._sc_asset_works_detail_item (sub_work_code, asset_id, asset_name, asset_code, asset_category_id, " +
//            " asset_model_id, manufacturer_id, supplier_id, price, tax_rate, tax_price, currency_id, file_ids, inner_code )" +
//            " VALUES (#{pm.sub_work_code}, #{pm.asset_id}, #{pm.asset_name}, #{pm.asset_code}, #{pm.asset_category_id}," +
//            " #{pm.asset_model_id}, #{pm.manufacturer_id, #{pm.supplier_id, #{pm.price}, " +
//            " #{pm.tax_rate}, #{pm.tax_price}, #{pm.currency_id}, #{pm.file_ids}, #{pm.inner_code} )")
//    void insertAssetWorksDetailItem(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Update(" UPDATE ${schema_name}._sc_asset_works " +
            " SET status = #{status}" +
            " WHERE work_code = #{work_code}")
    void updateAssetWorksStatus(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("status") Integer status);

    @Update(" UPDATE ${schema_name}._sc_asset_works_detail " +
            " SET status = #{status}" +
            " WHERE sub_work_code = #{sub_work_code}")
    void updateAssetWorksDetailStatus(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("status") Integer status);

    /**
     * 批量新增设备工单详情清单信息
     * @param schemaName
     * @param assetWorkDetailItemList
     */
    @Insert({
            "<script>",
            "INSERT INTO ${schema_name}._sc_asset_works_detail_item",
            "(sub_work_code,asset_id,asset_name,asset_code,asset_category_id) ",
            "VALUES",
            "<foreach collection='assetWorkDetailItemList' item='assetItem' separator=','>",
            "(#{assetItem.sub_work_code},#{assetItem.relation_id},#{assetItem.relation_name},#{assetItem.asset_code},#{assetItem.category_id}) ",
            "</foreach>",
            "</script>"})
    void batchInsertAssetWorksDetailItem(@Param("schema_name") String schemaName, @Param("assetWorkDetailItemList") List<Map<String, Object>> assetWorkDetailItemList);

    /**
     * 批量新增设备工单详情清单信息-设备调拨
     * @param schemaName
     * @param assetWorkDetailItemList
     */
    @Insert({
            "<script>",
            "INSERT INTO ${schema_name}._sc_asset_works_detail_item",
            "(sub_work_code,asset_id,asset_name,asset_category_id) ",
            "VALUES",
            "<foreach collection='assetWorkDetailItemList' item='assetItem' separator=','>",
            "(#{assetItem.sub_work_code},#{assetItem.asset_id},#{assetItem.asset_name},#{assetItem.asset_category_id}::integer) ",
            "</foreach>",
            "</script>"})
    void batchInsertAssetWorksDetailItemNew(@Param("schema_name") String schemaName, @Param("assetWorkDetailItemList") List<Map<String, Object>> assetWorkDetailItemList);

    /**
     * 查询一共有多少子工单
     */
    @Select(" SELECT count(1) " +
            " FROM ${schema_name}._sc_asset_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 LIMIT 1 ")
    int findAllSubAssetWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);

    /**
     * 查询一共有多少已完成的子工单
     */
    @Select(" SELECT count(1) " +
            " FROM ${schema_name}._sc_asset_works_detail awd " +
            " LEFT JOIN ${schema_name}._sc_user u ON awd.duty_user_id = u.id " +
            " WHERE awd.work_code = #{work_code} AND awd.is_main = -1 AND awd.status = 60 LIMIT 1 ")
    int findEndSubAssetWorksDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);
}
