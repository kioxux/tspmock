package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;


@Mapper
public interface TaskItemMapper {

    /**
     * 新增任务项
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert(" INSERT INTO ${schema_name}._sc_task_item(task_item_code, task_item_name, result_type," +
            "  requirements, create_time, create_user_id,result_verification) VALUES " +
            " (#{pm.task_item_code}, #{pm.task_item_name},  #{pm.result_type}, " +
            " #{pm.requirements}, current_timestamp, #{pm.create_user_id},#{pm.result_verification})")
    void insert(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据任务项编码删除任务项
     *
     * @param schema_name    入参
     * @param task_item_code 入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_task_item WHERE task_item_code = #{task_item_code}")
    void deleteByCode(@Param("schema_name") String schema_name, @Param("task_item_code") String task_item_code);

    /**
     * 编辑任务项
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Delete(" <script>" +
            " UPDATE ${schema_name}._sc_task_item" +
            " SET task_item_code = #{pm.task_item_code}" +
            "  <when test='pm.task_item_name!=null'>" +
            " ,task_item_name = #{pm.task_item_name}" +
            " </when> " +
            "  <when test='pm.result_type!=null'>" +
            " ,result_type = #{pm.result_type}" +
            " </when> " +
            "  <when test='pm.requirements!=null'>" +
            " ,requirements = #{pm.requirements}" +
            " </when> " +
            "  <when test='pm.file_ids!=null'>" +
            " ,file_ids = #{pm.file_ids}" +
            " </when> " +
            "  <when test='pm.result_verification!=null'>" +
            " ,result_verification = #{pm.result_verification}" +
            " </when> " +
            " WHERE task_item_code = #{pm.task_item_code}" +
            " </script>")
    void updateByCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 批量新增或更新任务项
     *
     * @param schemaName 数据库
     * @param itemList   任务项信息
     */
    @Insert({"<script>",
            "<foreach collection='itemList' item='pm' index='index' separator=';'>",
            "INSERT INTO ${schema_name}._sc_task_item(task_item_code, task_item_name, result_type, requirements, create_time, create_user_id, result_verification) ",
            "VALUES (#{pm.task_item_code}, #{pm.task_item_name}, #{pm.result_type}, #{pm.requirements}, current_timestamp, #{pm.create_user_id}, '1') ",
            "ON CONFLICT (task_item_code) ",
            "DO UPDATE SET task_item_name=#{pm.task_item_name},result_type=#{pm.result_type},requirements=#{pm.requirements} ",
            "</foreach>",
            "</script>"})
    void insertOrUpdateItem(@Param("schema_name") String schemaName, @Param("itemList") List<Map<String, Object>> itemList);

    /**
     * 获取任务项详情
     *
     * @param schema_name    入参
     * @param task_item_code 入参
     */
    @Select(" SELECT  task_item_code,file_ids, task_item_name, result_type::varchar, requirements, result_verification,create_time, create_user_id " +
            "   FROM ${schema_name}._sc_task_item WHERE task_item_code = #{task_item_code}")
    Map<String, Object> findTaskItemInfo(@Param("schema_name") String schema_name, @Param("task_item_code") String task_item_code);

    /**
     * 查询分页
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 任务项数量
     */
    @Select(" <script>" +
            " SELECT COUNT(1) FROM ${schema_name}._sc_task_item " +
            " <where> " +
            " <when test='pm.keywordSearch!=null'> " +
            " OR (task_item_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR task_item_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " <when test='pm.task_item_codes!=null'> " +
            "   AND task_item_code in " +
            "  <foreach collection='pm.task_item_codes' item='task_item_code' open='(' close=')' separator=','> #{task_item_code} </foreach>" +
            " </when> " +
            " </where>  " +
            " </script>")
    int findTaskItemListCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 查询任务项分页数量
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 任务项分页列表
     */
    @Select(" <script>" +
            " SELECT task_item_code, task_item_name, result_verification,result_type::varchar, requirements, create_time, create_user_id" +
            " FROM ${schema_name}._sc_task_item " +
            " <where> " +
            " <when test='pm.keywordSearch!=null'> " +
            " OR (task_item_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR task_item_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " <when test='pm.task_item_codes!=null'> " +
            "   AND task_item_code in " +
            "  <foreach collection='pm.task_item_codes' item='task_item_code' open='(' close=')' separator=','> #{task_item_code} </foreach>" +
            " </when> " +
            " </where> ORDER BY task_item_code ASC ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findTaskItemList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


//    /**
//     * 查询任务项分页数量
//     *
//     * @param schema_name 入参
//     * @param pm          入参
//     * @return 任务项分页列表
//     */
//    @Select(" <script>" +
//            " SELECT i.task_item_code, i.task_item_name, i.result_verification,i.result_type::varchar, i.requirements, i.create_time, i.create_user_id ," +
//            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then coalesce((select (c.resource->>t.name)::jsonb->>#{user_lang} from public.company_resource c where c.company_id = #{company_id}), t.name) else t.name end as result_type_name " +
//            " FROM ${schema_name}._sc_task_item i " +
//            " LEFT JOIN ${schema_name}._sc_cloud_data t on i.result_type::varchar = t.code and  t.data_type = 'resultTypes' " +
//            " <where> " +
//            " AND task_item_code NOT IN (SELECT task_item_code FROM ${schema_name}._sc_task_template_item WHERE task_template_code = #{pm.task_template_code})" +
//            " <when test='pm.keywordSearch!=null'> " +
//            " AND( lower(task_item_name) LIKE  CONCAT('%', lower(#{pm.keywordSearch}), '%') " +
//            " OR lower(task_item_code) LIKE  CONCAT('%', lower(#{pm.keywordSearch}), '%') ) " +
//            " </when> " +
//            " </where> ORDER BY task_item_code ASC " +
//            " </script>")
//    List<Map<String, Object>> findChoseTaskItemList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm, @Param("user_lang") String user_lang, @Param("company_id") Long company_id);

    @Select(" <script>" +
            " SELECT i.task_item_code, i.task_item_name, i.result_verification,i.result_type::varchar, i.requirements, i.create_time, i.create_user_id ," +
            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then t.name else t.name end as result_type_name " +
            " FROM ${schema_name}._sc_task_item i " +
            " LEFT JOIN ${schema_name}._sc_cloud_data t on i.result_type::varchar = t.code and  t.data_type = 'resultTypes' " +
            " <where> " +
            " AND task_item_code NOT IN (SELECT task_item_code FROM ${schema_name}._sc_task_template_item WHERE task_template_code = #{pm.task_template_code})" +
            " <when test='pm.keywordSearch!=null'> " +
            " AND( lower(task_item_name) LIKE  CONCAT('%', lower(#{pm.keywordSearch}), '%') " +
            " OR lower(task_item_code) LIKE  CONCAT('%', lower(#{pm.keywordSearch}), '%') ) " +
            " </when> " +
            " </where> ORDER BY task_item_code ASC " +
            " </script>")
    List<Map<String, Object>> findChoseTaskItemList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据任务模板编码查询任务项列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 任务项分页列表
     */
    @Select(" <script>" +
            " SELECT ti.task_item_code,ti.task_item_name,ti.result_type::varchar,ti.file_ids,ti.requirements,ti.create_time," +
            " ti.create_user_id,ti.result_verification,tti.task_id,tti.task_template_code,tti.group_name,tti.data_order " +
            " FROM ${schema_name}._sc_task_item AS ti" +
            " INNER JOIN ${schema_name}._sc_task_template_item AS tti ON ti.task_item_code = tti.task_item_code" +
            " WHERE tti.task_template_code IN " +
            " <foreach collection='pm.task_template_codes' item='task_template_code' open='(' close=')' separator=','> #{task_template_code} </foreach>" +
            " <when test='pm.keywordSearch!=null'> " +
            " AND( ti.task_item_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR ti.task_item_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> ORDER BY tti.task_template_code,tti.group_name,tti.data_order ASC  " +
            " </script>")
    List<Map<String, Object>> findTaskItemListByTempLateCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm, @Param("user_lang") String user_lang, @Param("company_id") Long company_id);

    /**
     * 获取任务项附件文档信息
     *
     * @param schema_name 请求参数
     * @param file_ids    请求参数
     * @return 列表数据
     */
    @Select(" <script> " +
            " SELECT t.*, to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time_str " +
            " FROM ${schema_name}._sc_files t " +
            " WHERE id in (${file_ids}) " +
            "    <when test='keywordSearch != null'> " +
            "       and t.file_original_name like CONCAT('%', #{keywordSearch}, '%')  " +
            "    </when> " +
            " ORDER BY id desc " +
            " </script> ")
    List<Map<String, Object>> findTaskItemFileList(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids, @Param("keywordSearch") String keywordSearch);

    /**
     * 查询任务项是否绑定任务模板
     *
     * @param schema_name    请求参数
     * @param task_item_code 请求参数
     * @return 任务项绑定任务模板数据
     */
    @Select("SELECT * FROM ${schema_name}._sc_task_template_item WHERE task_item_code = #{task_item_code} LIMIT 1 ")
    Map<String, Object> findTaskItemBindingTemplate(@Param("schema_name") String schema_name, @Param("task_item_code") String task_item_code);

    /**
     * 验证任务项编号是否存在
     *
     * @param schema_name 入参
     * @return 入参
     */
    @Select(" <script>" +
            " SELECT distinct task_item_code, task_item_name,result_verification,result_type,file_ids,create_user_id,requirements,create_time " +
            " FROM ${schema_name}._sc_task_item " +
            " WHERE task_item_code in " +
            "<foreach collection='itemCodes' item='itemCode' open='(' close=')' separator=','> " +
            "#{itemCode} " +
            "</foreach></script>")
    @MapKey("task_item_code")
    Map<String, Map<String, Object>> findItemsByItemCode(@Param("schema_name") String schema_name, @Param("itemCodes") List<String> itemCodeList);

    /**
     * 验证任务项编号是否存在
     *
     * @param schema_name  数据库
     * @param itemCodeList 任务项编号
     * @return 任务项编号集合
     */
    @Select(" <script> SELECT distinct task_item_code FROM ${schema_name}._sc_task_item WHERE task_item_code in " +
            " <foreach collection='itemCodes' item='itemCode' open='(' close=')' separator=','>#{itemCode}</foreach></script>")
    List<String> findItemCodesByCodes(@Param("schema_name") String schema_name, @Param("itemCodes") List<String> itemCodeList);
}
