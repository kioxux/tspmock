package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.CheckMeterModel;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface CheckMeterMapper {

    /**
     * 位置限定sql
     */
    String positionFilterSql = "(select distinct sr.asset_position_code from " +
            " ${schema_name}._sc_role_asset_position sr " +
            " join ${schema_name}._sc_role f on sr.role_id = f.id " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id " +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id} )";


    /**
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select count(1) from (select cu.inner_code,cu.title,scm.asset_id,sa.asset_code,sa.asset_name,scm.last_indication,scm.final_flow,scm.check_type," +
            " coalesce(scm.status,0) status,scm.check_time,scm.water_quality,scm.unusual_type " +
//            " from ${schema_name}._sc_facilities sf " + facility_id
            " from ${schema_name}._sc_customer cu" +
            " join ${schema_name}._sc_check_meter scm on cu.facility_id = scm.facility_id and scm.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp" +
            " left join ${schema_name}._sc_asset_cus sa on scm.asset_id =  sa.asset_id " +
            " where cu.status> " + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " +
            positionFilterSql +
            " ) s" +
            " where 1=1 " +
            " <when test='pm.inner_code!=null and pm.inner_code!=\"\" '> " +
            " AND  s.inner_code = #{pm.inner_code} " +
            " </when> " +
            " <when test='pm.title!=null and pm.title!=\"\" '> " +
            " AND  s.title like CONCAT('%',#{pm.title},'%')" +
            " </when> " +
            " <when test='pm.keywordSearch!=null and pm.keywordSearch!=\"\" '> " +
            " AND  (s.title like CONCAT('%',#{pm.keywordSearch},'%') OR " +
            "       s.inner_code = #{pm.keywordSearch})" +
            " </when> " +
            " <when test='pm.status!=null and pm.status!=\"\" '> " +
            " AND  s.status = #{pm.status}::int " +
            " </when>" +
            " <when test='pm.exceed!=null and pm.exceed==\"1\"'> " +
            " AND  s.water_quality != '" + StatusConstant.CHECK_METER_STATUS_NORMAL+"'" +
            " </when> " +
            " <when test='pm.unusual!=null and pm.unusual==\"1\"'> " +
            " AND  s.unusual_type != '" + StatusConstant.CHECK_METER_TYPE_NORMAL+"'" +
            " </when> " +
            " <when test='pm.normal!=null and pm.normal==\"1\"'> " +
            " AND  s.unusual_type = '" + StatusConstant.CHECK_METER_TYPE_NORMAL+"'" +
            " AND  s.water_quality = '" + StatusConstant.CHECK_METER_STATUS_NORMAL+"'" +
            " </when> " +
            " </script> ")
    int findCheckMetersListCount(@Param("schema_name") String schema_name,@Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    @Select(" <script>" +
            " select * from (select scm.id, cu.inner_code,cu.title,scm.asset_id,sa.asset_code,sa.asset_name, " +
//            " scm.last_indication, " +
            " case when scm.check_type='3' then 0 " +
            " else COALESCE((select pre.indication from ${schema_name}._sc_check_meter pre where pre.facility_id=scm.facility_id and pre.asset_id=scm.asset_id and pre.check_type != '3' and pre.check_time &lt; scm.check_time ORDER BY pre.check_time desc limit 1),0) end as last_indication, " +
//            " scm.final_flow, " +
            " case when scm.check_type='3' then scm.final_flow " +
            " else scm.indication - COALESCE((select pre.indication from ${schema_name}._sc_check_meter pre where pre.facility_id=scm.facility_id and pre.asset_id=scm.asset_id and pre.check_type != '3' and pre.check_time &lt; scm.check_time ORDER BY pre.check_time desc limit 1),0)  end as final_flow, " +
            " case when scm.check_type='3' then 0 " +
            " else scm.indication end as indication, " +
            " scm.check_type, " +
            " coalesce(scm.status,0) status,scm.check_time,scm.water_quality,scm.unusual_type,scm.facility_id,scm.remark " +
//            " from ${schema_name}._sc_facilities sf " +
            " from ${schema_name}._sc_customer cu " +
            " join ${schema_name}._sc_check_meter scm on cu.facility_id = scm.facility_id and scm.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp" +
            " left join ${schema_name}._sc_asset_cus sa on scm.asset_id = sa.asset_id " +
            " where cu.status> " + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " +
            positionFilterSql +
            " ) s" +
            " where 1=1  " +
            " <when test='pm.inner_code!=null and pm.inner_code!=\"\" '> " +
            " AND  s.inner_code = #{pm.inner_code} " +
            " </when> " +
            " <when test='pm.title!=null and pm.title!=\"\" '> " +
            " AND  s.title like CONCAT('%',#{pm.title},'%')" +
            " </when>" +
            " <when test='pm.keywordSearch!=null and pm.keywordSearch!=\"\" '> " +
            " AND  (s.title like CONCAT('%',#{pm.keywordSearch},'%') OR " +
            "       s.inner_code = #{pm.keywordSearch})" +
            " </when> " +
            " <when test='pm.status!=null and pm.status!=\"\" '> " +
            " AND  s.status = #{pm.status}::int " +
            " </when> " +
            " <when test='pm.exceed!=null and pm.exceed==\"1\"'> " +
            " AND  s.water_quality != '" + StatusConstant.CHECK_METER_STATUS_NORMAL+"'" +
            " </when> " +
            " <when test='pm.unusual!=null and pm.unusual==\"1\"'> " +
            " AND  s.unusual_type != '" + StatusConstant.CHECK_METER_TYPE_NORMAL+"'" +
            " </when> " +
            " <when test='pm.normal!=null and pm.normal==\"1\"'> " +
            " AND  s.unusual_type = '" + StatusConstant.CHECK_METER_TYPE_NORMAL+"'" +
            " AND  s.water_quality = '" + StatusConstant.CHECK_METER_STATUS_NORMAL+"'" +
            " </when> " +
            " ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findCheckMetersList(@Param("schema_name") String schema_name,@Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select (select count(1) from ${schema_name}._sc_customer b " +
            " where b.status>" + StatusConstant.STATUS_DELETEED + " and b.is_use='" + StatusConstant.IS_USE_YES + "' " +
            " and b.position_code in " +
            positionFilterSql +
            " ) as total," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_check_meter m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            positionFilterSql +
            " and m.check_time between  #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp) s) as checked," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_check_meter m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            positionFilterSql +
            " and m.check_time between  #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and m.unusual_type!='" + StatusConstant.CHECK_METER_STATUS_NORMAL + "') s) as unusual," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_check_meter m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            positionFilterSql +
            " and m.check_time between  #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and m.water_quality!='" + StatusConstant.CHECK_METER_STATUS_NORMAL + "') s) as exceed," +
            "(select count(1) from (select distinct m.facility_id from ${schema_name}._sc_check_meter m " +
            " join ${schema_name}._sc_customer b on m.facility_id = b.facility_id where " +
            " b.position_code in " +
            positionFilterSql +
            " and m.check_time between  #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and m.unusual_type='" + StatusConstant.CHECK_METER_TYPE_NORMAL + "' and m.water_quality='" + StatusConstant.CHECK_METER_STATUS_NORMAL + "') s) as normal" +
            " </script>")
    Map<String, Object> findCheckMeterStatistics(@Param("schema_name") String schema_name,@Param("user_id") String userId, @Param("pm") Map<String, Object> pm);

    /**
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select * from ${schema_name}._sc_check_meter s" +
            " where s.asset_id = #{pm.asset_id} " +
            " and s.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp  " +
            " order by s.check_time desc " +
            " ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findAssetCheckMeterList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * @param schema_name
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select count(1) from ${schema_name}._sc_check_meter s" +
            " where s.asset_id = #{pm.asset_id} " +
            " and s.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp  " +
            " </script>")
    int findAssetCheckMeterListCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select(" <script>" +
            " select s.*,s.status::text status_code,cu.inner_code,cu.title,cu.short_title,acu.asset_code " +
            " from ${schema_name}._sc_check_meter s " +
//            " left join ${schema_name}._sc_facilities sf on s.facility_id = sf.id  " +
//            " left join ${schema_name}._sc_asset sa on s.asset_id = sa.id  " +
            " left join ${schema_name}._sc_customer cu on s.facility_id = cu.facility_id  " +
            " left join ${schema_name}._sc_asset_cus acu on s.asset_id = acu.asset_id  " +
            " where s.id = #{id} " +
            " </script>")
    Map<String, Object> findCheckMeterById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 查询客户历史排污量
     *
     * @param schemaName
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select n.total," +
            " cast((select sum(b.final_flow) from ${schema_name}._sc_check_meter b " +
            " where b.facility_id= #{pm.id}::int " +
            " and to_char(b.check_time,'yyyy-mm') = n.lastmonth" +
            " group by  to_char(b.check_time,'yyyy-mm') " +
            " ) as decimal(18,2)) last_month_total," +
            " cast((select sum(b.final_flow) from ${schema_name}._sc_check_meter b " +
            " where b.facility_id = #{pm.id}::int " +
            " and to_char(b.check_time,'yyyy-mm') = n.lastyearmonth " +
            " group by  to_char(b.check_time,'yyyy-mm') " +
            " ) as decimal(18,2)) last_year_month_total," +
            " n.year_month " +
            " from (select sum(a.final_flow) total," +
            " to_char(a.check_time,'yyyy-mm') year_month ," +
            " max(to_char(date_trunc('month',a.check_time) - interval '0 month' - interval '1 day','yyyy-mm'))  lastmonth," +
            " max(to_char((SELECT a.check_time+ '-1 year'),'yyyy-mm')) lastyearmonth " +
            " from ${schema_name}._sc_check_meter a where a.facility_id=  #{pm.id}::int " +
            " and a.check_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " group by to_char(a.check_time,'yyyy-mm')) n " +
            " </script>")
    List<Map<String, Object>> findCustomerCheckMeterHistory(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    @Select(" <script>" +
            " select n.total," +
            " (select sum(b.final_flow) from ${schema_name}._sc_check_meter b " +
            " where b.facility_id= #{pm.id}::int and b.water_quality != '2' " +
            " and to_char(b.check_time,'yyyy-mm') = n.year_month" +
            " group by  to_char(b.check_time,'yyyy-mm') " +
            " ) normal," +
            " (select sum(b.final_flow) from ${schema_name}._sc_check_meter b " +
            " where b.facility_id = #{pm.id}::int and b.water_quality = '2' " +
            " and to_char(b.check_time,'yyyy-mm') = n.year_month " +
            " group by  to_char(b.check_time,'yyyy-mm') " +
            " ) exc," +
            " n.year_month " +
            " from (select sum(a.final_flow) total," +
            " to_char(a.check_time,'yyyy-mm') year_month " +
            " from ${schema_name}._sc_check_meter a where a.facility_id=  #{pm.id}::int " +
            " and a.check_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " group by to_char(a.check_time,'yyyy-mm')) n " +
            " </script>")
    List<Map<String, Object>> findCustomerAllCheckMeterHistoryByCondition(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 查询设备最后一条抄表记录
     *
     * @param schemaName
     * @param pm
     * @return
     */
    @Select(" <script> " +
            " select * from  ${schema_name}._sc_check_meter a " +
            " where a.status >" + StatusConstant.STATUS_DELETEED +
            " and a.asset_id = #{pm.asset_id} and a.check_type !='3' and to_char(a.belong_date,'yyyy-MM-dd') &lt; to_char(#{pm.check_time}::timestamp,'yyyy-MM-dd') " +
            " order by a.check_time desc limit 1 offset 0 " +
            " </script>")
    Map<String, Object> findLastAssetCheckMeter(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    @Select(" <script> " +
            " select * from  ${schema_name}._sc_check_meter a " +
            " where a.status >" + StatusConstant.STATUS_DELETEED +
            " and a.asset_id = #{pm.asset_id} and a.check_type !='3' and to_char(a.belong_date,'yyyy-MM-dd') > to_char(#{pm.check_time}::timestamp, 'yyyy-MM-dd') " +
            " order by a.check_time asc limit 1 offset 0 " +
            " </script>")
    Map<String, Object> findAfterAssetCheckMeter(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 新建修改抄表记录
     *
     * @param schemaName
     * @param model
     */
    @Insert(" <script> " +
            " insert into ${schema_name}._sc_check_meter " +
            " (facility_id,asset_id,last_indication,indication,flow,final_flow,water_quality," +
            "  check_type,unusual_type,batch_no,remark,unit,status,sub_work_code,create_user_id,check_time,belong_date) values " +
            "  (#{pm.facility_id},#{pm.asset_id},#{pm.last_indication},#{pm.indication},#{pm.flow}," +
            "   #{pm.final_flow},#{pm.water_quality},#{pm.check_type},#{pm.unusual_type},#{pm.batch_no}," +
            "   #{pm.remark},#{pm.unit},#{pm.status},#{pm.sub_work_code},#{pm.create_user_id},#{pm.check_time}::timestamp,#{pm.belong_date}::timestamp) " +
            " ON CONFLICT (facility_id,asset_id,belong_date) "+
            " DO UPDATE SET last_indication=#{pm.last_indication},indication=#{pm.indication},check_time=#{pm.check_time}::timestamp,flow=#{pm.flow},final_flow=#{pm.final_flow}" +
            " </script> ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    int insert(@Param("schema_name") String schemaName, @Param("pm") CheckMeterModel model);

    /**
     * 抄表计量日历数据
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select (m.total - a.day_total) unchecked,coalesce(b.day_total,0) unusual,coalesce(c.day_total,0) water_quality, a.days from " +
            " (select count(distinct a.facility_id) day_total,to_char(a.check_time,'yyyy-mm-dd') days  from ${schema_name}._sc_check_meter a join ${schema_name}._sc_customer b on a.facility_id = b.facility_id where " +
            "  b.position_code in " +
            positionFilterSql +
            " and a.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp group by to_char(a.check_time,'yyyy-mm-dd')) a left join " +
            " (select count(distinct a.facility_id) day_total,to_char(a.check_time,'yyyy-mm-dd') days  from ${schema_name}._sc_check_meter a join ${schema_name}._sc_customer b on a.facility_id = b.facility_id where " +
            "  b.position_code in " +
            positionFilterSql +
            " and a.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and a.unusual_type!='" + StatusConstant.CHECK_METER_TYPE_NORMAL + "' group by to_char(a.check_time,'yyyy-mm-dd')) b  on a.days = b.days left join " +
            " (select count(distinct a.facility_id) day_total,to_char(a.check_time,'yyyy-mm-dd') days  from ${schema_name}._sc_check_meter a join ${schema_name}._sc_customer b on a.facility_id = b.facility_id where " +
            "  b.position_code in " +
            positionFilterSql +
            " and a.check_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and a.water_quality!='" + StatusConstant.CHECK_METER_STATUS_NORMAL + "' group by to_char(a.check_time,'yyyy-mm-dd')) c  on a.days =c.days, " +
            " (select  count(1) total from ${schema_name}._sc_facilities a join ${schema_name}._sc_customer b on a.id = b.facility_id and a.status>" + StatusConstant.STATUS_DELETEED +
            "  and b.position_code in " +
            positionFilterSql +
            ") m " +
            " </script>")
    List<Map<String, Object>> findCheckMeterCalendar(@Param("schema_name") String schemaName,  @Param("user_id") String userId, @Param("pm") Map<String, Object> paramMap);

    /**
     * 客户排污计量柱状图年
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select sum(a.final_flow) flow,to_char(a.check_time,'yyyy-MM') days " +
            " from ${schema_name}._sc_check_meter a where a.check_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " and a.facility_id = #{pm.id}::int " +
            " and a.status >" + StatusConstant.STATUS_DELETEED +
            " group by to_char(a.check_time,'yyyy-MM') order by to_char(a.check_time,'yyyy-MM')" +
            " </script>")
    List<Map<String, Object>> findCustomerCheckMeterPicYear(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 客户排污计量柱状图月
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select sum(a.final_flow) flow,to_char(a.check_time,'yyyy-MM-dd') days " +
            " from ${schema_name}._sc_check_meter a where a.check_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " and a.facility_id = #{pm.id}::int " +
            " and a.status >" + StatusConstant.STATUS_DELETEED +
            " group by to_char(a.check_time,'yyyy-MM-dd') order by to_char(a.check_time,'yyyy-MM-dd')" +
            " </script>")
    List<Map<String, Object>> findCustomerCheckMeterPicMonth(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 编辑抄表记录
     * @param schemaName
     * @param model
     */
    @Update(" <script> " +
            " UPDATE ${schema_name}._sc_check_meter " +
            " SET indication = #{pm.indication}, " +
            " last_indication = #{pm.last_indication}, " +
            " remark = #{pm.remark}," +
            " modify_user_id = #{pm.modify_user_id} " +
            " WHERE id = #{pm.id}" +
            " </script>")
    void modifyCheckMeterFinalFlow(@Param("schema_name") String schemaName,@Param("pm") CheckMeterModel model);

    /**
     * 查询客户排污量
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select * from ${schema_name}._sc_check_meter a where " +
            " a.facility_id = #{pm.id}::int " +
            " and a.status >" + StatusConstant.STATUS_DELETEED +
            " order by a.check_time desc ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findCustomerCheckMeterList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script> " +
            " select count(1) from (" +
            " select to_char(tb.check_time, 'yyyy-MM-dd' ) as date, " +
            " count(distinct tb.valid_asset) as valid_asset, " +
            " count(distinct tb.has_exceeded_asset) as has_exceeded_asset, " +
            " count(distinct tb.un_exceeded_asset) as un_exceeded_asset, " +
            " count(distinct tb.odd_asset) as odd_asset, " +
            " cast(count(distinct tb.has_exceeded_asset)/count(distinct tb.valid_asset) as decimal(18,2))*100 as success_rate " +
            " from (SELECT asset_id AS valid_asset, " +
            " case when status = 1 then asset_id else null end as has_exceeded_asset, " +
            " case when status = 0 then asset_id else null end as un_exceeded_asset, " +
            " case when unusual_type != '0' then asset_id else null end as odd_asset, " +
            " check_time FROM ${schema_name}._sc_check_meter) tb GROUP BY to_char(tb.check_time, 'yyyy-MM-dd' )) tab" +
            "</script>")
    int findCheckMetersMonthListCount(@Param("schema_name") String schema_name,@Param("pm") Map<String, Object> pm);

    @Select("<script> " +
            " select to_char(tb.check_time, 'yyyy-MM-dd' ) as date, " +
            " count(distinct tb.valid_asset) as valid_asset, " +
            " count(distinct tb.has_exceeded_asset) as has_exceeded_asset, " +
            " count(distinct tb.un_exceeded_asset) as un_exceeded_asset, " +
            " count(distinct tb.odd_asset) as odd_asset, " +
            " cast(count(distinct tb.has_exceeded_asset)/count(distinct tb.valid_asset) as decimal(18,2))*100 as success_rate " +
            " from (SELECT asset_id AS valid_asset, " +
            " case when status = 1 then asset_id else null end as has_exceeded_asset, " +
            " case when status = 0 then asset_id else null end as un_exceeded_asset, " +
            " case when unusual_type != '0' then asset_id else null end as odd_asset, " +
            " check_time FROM ${schema_name}._sc_check_meter) tb GROUP BY to_char(tb.check_time, 'yyyy-MM-dd' ) ${pm.pagination} " +
            "</script>")
    List<Map<String, Object>> findCheckMetersMonthList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("<script> " +
            " select * from ${schema_name}._sc_customer where scada_code = #{pm.spCode} and status > -1000 limit 1" +
            "</script>")
    Map<String, Object> findCusByScadaCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("<script> " +
            " select * from ${schema_name}._sc_asset_cus where scada_code = #{pm.monitorSiteCode} and bind_status = 1 limit 1" +
            "</script>")
    Map<String, Object> findAssetByScadaCode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("<script> " +
            " select count(1) from ${schema_name}._sc_check_meter where " +
            " facility_id = #{pm.facility_id} ::int and asset_id = #{pm.asset_id} and belong_date = #{pm.belong_date} ::timestamp " +
            "</script>")
    int findIsExistCheckMeter(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Insert("<script> "+
            " INSERT INTO ${schema_name}._sc_check_meter(facility_id,asset_id,last_indication,indication,flow,final_flow,water_quality,check_type,unusual_type,belong_date,check_time,status,create_user_id) "+
            " VALUES (#{checkMeter.facility_id}::int,#{checkMeter.asset_id},#{checkMeter.last_indication} ::numeric,#{checkMeter.indication} ::numeric,#{checkMeter.flow} ::numeric,#{checkMeter.final_flow} ::numeric,#{checkMeter.water_quality},#{checkMeter.check_type},#{checkMeter.unusual_type},#{checkMeter.belong_date} ::timestamp,#{checkMeter.check_time} ::timestamp,#{checkMeter.status},#{checkMeter.create_user_id}) "+
            " </script> ")
    @Options(useGeneratedKeys = true, keyProperty = "checkMeter.id", keyColumn = "id")
    int insertCheckMeter(@Param("schema_name") String schemaName, @Param("checkMeter") Map<String, Object> checkMeter);

    @Insert("INSERT INTO ${schema_name}._sc_pollute_survey_record(facility_id,asset_id,factor_id,limit_value,check_meter_id, " +
            "limit_unit,survey_value,survey_unit,ratio,belong_date,create_user_id)  " +
            "VALUES (#{pm.facility_id} ::int,#{pm.asset_id},#{pm.factor_id},#{pm.limit_value},#{pm.check_meter_id}," +
            "#{pm.limit_unit},#{pm.survey_value},#{pm.survey_unit},#{pm.ratio},#{pm.belong_date} ::timestamp,#{pm.create_user_id})")
    int insertPolluteSurveyRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select(" <script> " +
            " select tab.id,tab.facility_id,tab.asset_id, " +
            " tab.last_indication, " +
            " tab.indication, " +
            " COALESCE((tab.indication - tab.last_indication), 0) as flow ," +
            " COALESCE((tab.indication - tab.last_indication), 0) as final_flow " +
            " from (select scm.id,scm.facility_id,scm.asset_id," +
            " COALESCE((select pre.indication from ${schema_name}._sc_check_meter pre where " +
            " pre.facility_id=scm.facility_id and pre.asset_id=scm.asset_id and pre.check_type !='3' and " +
            " to_char(pre.belong_date,'yyyy-MM-dd') &lt; to_char(scm.belong_date,'yyyy-MM-dd') " +
            " order by pre.check_time desc limit 1 ),0) as last_indication," +
            " scm.indication" +
            " from ${schema_name}._sc_check_meter scm where scm.facility_id=#{pm.facility_id} ::int and scm.asset_id=#{pm.asset_id} and scm.check_type !='3' " +
            " and to_char(scm.belong_date,'yyyy-MM-dd') >= to_char(#{pm.check_time} ::timestamp,'yyyy-MM-dd') order by scm.check_time asc) tab " +
            " </script> ")
    List<Map<String, Object>> findAfterCheckMeter(@Param("schema_name") String schema_name, @Param("pm") CheckMeterModel model);

    @Select(" <script> " +
            "<foreach collection='check_list' item='check' index='index' separator=';'>" +
            " update ${schema_name}._sc_check_meter " +
            " set last_indication = #{check.last_indication}, " +
            " flow = #{check.flow}, " +
            " final_flow = #{check.final_flow} " +
            " where id = #{check.id} " +
            " </foreach>" +
            " </script> ")
    void updateCheckMeter(@Param("schema_name") String schema_name, @Param("check_list") List<Map<String,Object>> list);

    @Select(" <script> " +
            " select tab.facility_id,1 as is_create_pollute " +
            " from (select * from ${schema_name}._sc_pollute_fee " +
            " where to_char(#{pm.begin_date} ::timestamp, 'yyyy-MM-dd') between to_char(begin_date,'yyyy-MM-dd') " +
            " and to_char(end_date,'yyyy-MM-dd') " +
            " union " +
            " select * from ${schema_name}._sc_pollute_fee " +
            " where to_char(#{pm.end_date} ::timestamp, 'yyyy-MM-dd') between to_char(begin_date,'yyyy-MM-dd') " +
            " and to_char(end_date,'yyyy-MM-dd')) tab GROUP BY tab.facility_id " +
            " </script> ")
    List<Map<String, Object>> findIsCreatePollute(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);
}
