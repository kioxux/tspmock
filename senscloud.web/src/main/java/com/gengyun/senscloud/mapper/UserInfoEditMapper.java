package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 用户个人配置
 */
@Mapper
public interface UserInfoEditMapper {
    /**
     * 保存用户个人配置信息
     *
     * @param schemaName  数据库
     * @param userId      用户主键
     * @param languageTag 语言类型
     * @return 成功数量
     */
    @Update("update ${schemaName}._sc_user set language_tag=#{languageTag} where id=#{userId}")
    int updateUserConfig(@Param("schemaName") String schemaName, @Param("userId") String userId, @Param("languageTag") String languageTag);
}
