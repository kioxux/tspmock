package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.model.WorkColumnExAdd;
import net.sf.json.JSONObject;
import org.apache.ibatis.annotations.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface WorkTemplateMapper {

    @Select("SELECT count(1) FROM ${schema_name}._sc_work_template t ${searchWord} ")
    int countWorkTemplateList(@Param("schema_name")String schemaName,@Param("searchWord") String searchWord);

    @Select("SELECT work_template_code, work_template_name, is_have_sub,is_use,create_time,remark," +
            "CASE WHEN COALESCE(work_template_name, '') = '' then COALESCE(remark, '') " +
            "     WHEN COALESCE(remark, '') = '' then COALESCE(work_template_name, '') " +
            "     ELSE COALESCE(work_template_name, '') || '[' || COALESCE(remark, '') || ']' END as work_template_show_name " +
            " FROM ${schema_name}._sc_work_template t ${searchWord} ")
    List<Map<String, Object>> findWorkTemplateList(@Param("schema_name")String schemaName,@Param("searchWord") String searchWord);

    @Insert("INSERT INTO ${schema_name}._sc_work_template(work_template_code, work_template_name, is_use, remark," +
            " create_time, create_user_id, is_have_sub) VALUES (#{work_template_code}, #{work_template_name}, #{is_use}," +
            " #{remark}, #{create_time}, #{create_user_id}, #{is_have_sub})")
    int insertWorkTemplate(Map<String, Object> data);

    /**
     * 通过work_template_code查询工单模板信息
     *
     * @param schemaName
     * @param work_template_code
     * @return
     */
    @Select("SELECT work_template_code,work_template_name,is_have_sub,is_use,create_time,remark FROM ${schema_name}._sc_work_template t where work_template_code = #{work_template_code} ")
    Map<String, Object> findWorkTemplateInfoByCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Select("SELECT count(1) " +
            "FROM ${schema_name}._sc_work_flow_node_column wfc " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfc.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfc.field_section_type = c2.code AND c2.data_type = 'block' " +
            "WHERE wfc.node_id = #{work_template_code} ${searchWord}")
    int countWorkTemplateColumnList(@Param("schema_name")String schemaName,@Param("searchWord") String searchWord,@Param("work_template_code")String work_template_code);

//    /**
//     * 查询工单模板字段列表
//     *
//     * @param schemaName
//     * @param searchWord
//     * @param work_template_code
//     * @param company_id
//     * @param userLang
//     * @return
//     */
////    @Select("SELECT wfnc.id,coalesce(wfce.field_value,'')as source,wfnc.field_form_code,wfnc.field_code,wfnc.field_view_type,wfnc.field_section_type,  " +
//    @Select("SELECT wfnc.id,wfnc.field_value,wfnc.field_form_code,wfnc.field_code,wfnc.field_view_type,wfnc.field_section_type,  " +
//            "coalesce((select (c.resource->>wfnc.field_name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{company_id}), wfnc.field_name) as field_name, " +
//            "wfnc.create_time,wfnc.create_user_id,c1.name as field_view_type_name,c2.name as field_section_type_name,wfnc.field_is_show, " +
//            "wfnc.save_type,wfnc.field_right,wfnc.is_required,coalesce(wfnc.field_remark,'') field_remark,wfnc.node_id as work_template_code,wfnc.data_key,wfnc.data_key as source " +
//            "FROM ${schema_name}._sc_work_flow_node_column wfnc " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block' " +
//            "WHERE wfnc.node_id = #{work_template_code} ${searchWord}")
//    List<Map<String, Object>> findWorkTemplateColumnList(@Param("schema_name")String schemaName,@Param("searchWord") String searchWord,@Param("work_template_code") String work_template_code,@Param("company_id")Long company_id,@Param("userLang")String userLang);

    @Select("SELECT wfnc.id,wfnc.field_value,wfnc.field_form_code,wfnc.field_code,wfnc.field_view_type,wfnc.field_section_type,  " +
            "wfnc.field_name as field_name, " +
            "wfnc.create_time,wfnc.create_user_id,c1.name as field_view_type_name,c2.name as field_section_type_name,wfnc.field_is_show, " +
            "wfnc.save_type,wfnc.field_right,wfnc.is_required,coalesce(wfnc.field_remark,'') field_remark,wfnc.node_id as work_template_code,wfnc.data_key,wfnc.data_key as source " +
            "FROM ${schema_name}._sc_work_flow_node_column wfnc " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block' " +
            "WHERE wfnc.node_id = #{work_template_code} ${searchWord}")
    List<Map<String, Object>> findWorkTemplateColumnList(@Param("schema_name")String schemaName,@Param("searchWord") String searchWord,@Param("work_template_code") String work_template_code);

    @Select("select (" +
            "SELECT count(1) FROM ${schema_name}._sc_work_flow_info wfi WHERE wfi.work_template_code = #{work_template_code} " +
            " )+(" +
            "SELECT count(1) FROM ${schema_name}._sc_work_flow_node_column_ext wfnce " +
            "JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfnce.node_id = wfnc.node_id " +
            "JOIN ${schema_name}._sc_cloud_data cd ON wfnc.field_view_type = cd.code AND cd.data_type = 'work_column' " +
            "WHERE wfnc.field_view_type = 'subpage' AND wfnce.field_value = #{work_template_code}" +
            ")")
    int findCountUseByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_template WHERE work_template_code = #{work_template_code}")
    void deleteTemplateByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column WHERE node_id = #{work_template_code}")
    void deleteTemplateColumnByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_ext WHERE node_id = #{work_template_code}")
    void deleteTemplateColumnExtByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_linkage WHERE node_id = #{work_template_code}")
    void deleteTemplateColumnLinkageByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "using ${schema_name}._sc_work_flow_node_column_linkage l " +
            "where l.id=c.linkage_id and l.node_id = #{work_template_code}")
    void deleteTemplateColumnLinkageConditionByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Select("select ( " +
            "SELECT count(1) FROM ${schema_name}._sc_work_flow_info wfi " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfi.pref_id=wfnc.node_id " +
            "WHERE wfi.work_template_code = #{work_template_code} AND wfnc.field_form_code = #{field_form_code} " +
            ")+( " +
            "SELECT count(1) FROM ${schema_name}._sc_work_flow_info wfi " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node wfn ON wfi.pref_id = wfn.pref_id " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfn.node_id=wfnc.node_id  AND wfn.id = wfnc.word_flow_node_id " +
            "WHERE wfi.work_template_code = #{work_template_code} AND wfnc.field_form_code = #{field_form_code} and wfn.node_type != '1' " +
            ")+( " +
            "SELECT count(1) FROM ${schema_name}._sc_work_flow_node_column_ext wfnce " +
            "JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfnce.node_id = wfnc.node_id " +
            "JOIN ${schema_name}._sc_cloud_data cd ON wfnc.field_view_type = cd.code AND cd.data_type = 'work_column' " +
            "WHERE wfnc.field_view_type = 'subpage' " +
            "AND wfnce.field_value = #{work_template_code} AND wfnc.field_form_code = #{field_form_code} " +
            ") ")
    int findColumnCountUseByWorkTemplateCodeAndFieldFormCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code, @Param("field_form_code")String field_form_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column WHERE node_id = #{work_template_code} AND field_form_code = #{field_form_code} ")
    void deleteTemplateColumnByWorkTemplateCodeAndFieldFormCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code, @Param("field_form_code")String field_form_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_ext WHERE node_id = #{work_template_code} AND field_form_code = #{field_form_code} ")
    void deleteTemplateColumnExtByWorkTemplateCodeAndFieldFormCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code, @Param("field_form_code") String field_form_code);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "using ${schema_name}._sc_work_flow_node_column_linkage l " +
            "where l.id=c.linkage_id and l.node_id = #{work_template_code} AND l.field_form_code = #{field_form_code} ")
    void deleteTemplateColumnLinkageConditionByWorkTemplateCodeAndFieldFormCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code, @Param("field_form_code")String field_form_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_linkage WHERE node_id = #{work_template_code} AND field_form_code = #{field_form_code} ")
    void deleteTemplateColumnLinkageByWorkTemplateCodeAndFieldFormCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code, @Param("field_form_code") String field_form_code);

    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node_column(node_id, field_form_code, field_code, field_name, save_type, change_type, field_right, " +
            "field_value, is_required, is_table_key, db_table_type, field_remark, field_data_base, field_view_type, field_section_type, field_validate_method, " +
            "create_time, create_user_id,field_is_show,data_order,data_key) " +
            "SELECT #{work_template_code},c.field_form_code,c.field_code, c.field_name, c.save_type, c.change_type, c.field_right, c.field_value, " +
            "c.is_required, c.is_table_key, c.db_table_type, c.field_remark, c.field_data_base, c.field_view_type, c.field_section_type, " +
            "c.field_validate_method,#{create_time}, #{create_user_id} ,'show',#{sort},c.data_key " +
            "FROM ${schema_name}._sc_work_columns c WHERE c.field_form_code = #{field_form_code}")
    @Options(useGeneratedKeys = true, keyProperty = "column_id", keyColumn = "id")
    int insertWorkTemplateColumn(Map<String, Object> data);

    @Update("update ${schema_name}._sc_work_flow_node_column " +
            " set field_form_code=#{field_form_code},field_code = #{field_code}," +
            " field_name=#{field_name} where id = " +
            " (select id from ${schema_name}._sc_work_flow_node_column " +
            " where node_id=#{work_template_code} and (field_form_code = #{field_form_code} or field_code = #{field_code})) ")
    int isHandleWorkTemplateColumn(Map<String,Object> data);

    @Select("INSERT INTO ${schema_name}._sc_work_flow_node_column_ext(node_id, field_form_code, field_name, field_write_type, field_value, " +
            "create_time,create_user_id,column_id) " +
            "SELECT #{work_template_code},field_form_code, field_name, field_write_type, field_value,#{create_time}, #{create_user_id},#{column_id} " +
            "FROM ${schema_name}._sc_work_column_ext WHERE field_form_code = #{field_form_code}")
    void insertWorkTemplateColumnExt(Map<String, Object> data);

    @Select("SELECT wfnc.*,c1.name as field_view_type_name,c2.name as field_section_type_name " +
            "FROM ${schema_name}._sc_work_template t " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfnc.node_id = t.work_template_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block' " +
            "WHERE t.work_template_code = #{work_template_code} AND wfnc.field_form_code = #{field_form_code}")
    Map<String, Object> findWorkTemplateColumnInfoByCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code, @Param("field_form_code")String field_form_code);

    /**
     * 通过id修改node表数据
     * @param data
     * @return
     */
    @Update("<script> UPDATE ${schema_name}._sc_work_flow_node_column  " +
            "<set> " +
//            "<if test=\"field_code!=null and field_code !=''\"> " +
//            "field_code = #{field_code}, " +
//            "</if> " +
            "<if test=\"field_name!=null and field_name !=''\"> " +
            "field_name = #{field_name}, " +
            "</if> " +
            "<if test=\"save_type!=null and save_type !=''\"> " +
            "save_type = #{save_type}, " +
            "</if> " +
            "<if test=\"change_type!=null and change_type !=''\"> " +
            "change_type = #{change_type}, " +
            "</if> " +
            "<if test=\"field_right!=null and field_right !=''\"> " +
            "field_right = #{field_right}, " +
            "</if> " +
            "<if test=\"is_required!=null and is_required !=''\"> " +
            "is_required = #{is_required}, " +
            "</if> " +
            "<if test=\"is_table_key!=null and is_table_key !=''\"> " +
            "is_table_key = #{is_table_key}, " +
            "</if> " +
            "<if test=\"db_table_type!=null and db_table_type !=''\"> " +
            "db_table_type = #{db_table_type}, " +
            "</if> " +
            "<if test=\"field_remark!=null and field_remark !=''\"> " +
            "field_remark = #{field_remark}, " +
            "</if> " +
            "<if test=\"field_data_base!=null and field_data_base !=''\"> " +
            "field_data_base = #{field_data_base}, " +
            "</if> " +
            "<if test=\"field_view_type!=null and field_view_type !=''\"> " +
            "field_view_type = #{field_view_type}, " +
            "</if> " +
            "<if test=\"field_section_type!=null and field_section_type !=''\"> " +
            "field_section_type = #{field_section_type}, " +
            "</if> " +
            "<if test=\"field_validate_method!=null and field_validate_method !=''\"> " +
            "field_validate_method = #{field_validate_method}, " +
            "</if> " +
            "<if test=\"create_time!=null \"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"create_user_id!=null and create_user_id !=''\"> " +
            "create_user_id = #{create_user_id} , " +
            "</if> " +
            "<if test=\"field_is_show!=null and field_is_show !=''\"> " +
            "field_is_show = #{field_is_show} , " +
            "</if> " +
            "<if test=\"data_order!=null \"> " +
            "data_order = #{data_order} , " +
            "</if> " +
//            "<if test=\"field_value!=null and field_value !=''\"> " +
            "field_value = #{field_value}, " +
//            "</if> " +
//            "<if test=\"data_key!=null\"> " +
            "data_key = #{data_key} " +
//            "</if> " +
            "</set> " +
            "WHERE " +
            " id = #{id}::int </script>")
//            "field_form_code = #{field_form_code} AND node_id = #{work_template_code} </script>")
    int updateWorkTemplateColumn(Map<String, Object> data);

    @Insert("<script> INSERT INTO ${schema_name}._sc_work_flow_node_column_ext(node_id,field_form_code, field_name, field_write_type, " +
            "field_value, create_time, create_user_id,column_id) " +
            "VALUES " +
            "<foreach collection ='list' item='ex' separator =','>" +
            "(#{ex.node_id},#{ex.field_form_code}, #{ex.field_name}, #{ex.field_write_type}, #{ex.field_value}, #{ex.create_time}, #{ex.create_user_id},#{ex.column_id}) " +
            "</foreach > </script>")
    int insertWorkTemplateColumnExBatch(@Param("schema_name")String schemaName, @Param("list")List<WorkColumnExAdd> extList);

    /**
     * 插入字段联动数据
     *
     * @param linkObject
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node_column_linkage(node_id, field_form_code, linkage_type," +
            " create_time, create_user_id,column_id) VALUES (#{node_id}, #{field_form_code}, #{linkage_type}::int, #{create_time}::timestamp, #{create_user_id},#{column_id})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertWorkTemplateColumnLinkage(Map<String,Object> linkObject);

    /**
     * 插入字段联动条件数据
     *
     * @param linkObject
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node_column_linkage_condition(linkage_id, link_field_form_code," +
            " interface_address, self_defined, condition,sub_page_template_code,edit_sub_page_template_code,scan_fail_return_template_code,is_data_from_page) VALUES (#{id}, #{link_field_form_code}," +
            " #{interface_address}, #{self_defined}, #{condition, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler},#{sub_page_template_code},#{edit_sub_page_template_code},#{scan_fail_return_template_code},#{is_data_from_page} )")
//            " interface_address, self_defined, condition) VALUES (#{id}, #{link_field_form_code}, #{interface_address}, #{interface_address}, #{conditions}::text::jsonb )")
    int insertWorkTemplateColumnLinkageCondition(Map<String,Object> linkObject);

    @Select("SELECT id,node_id,field_form_code,field_name,field_write_type,field_value,create_time,create_user_id " +
            "FROM ${schema_name}._sc_work_flow_node_column_ext WHERE field_form_code = #{field_form_code} AND node_id = #{work_template_code} order by id")
    List<Map<String, Object>> findWorkTemplateColumnExtByCode(@Param("schema_name")String schemaName, @Param("field_form_code")String field_form_code, @Param("work_template_code")String work_template_code);

    @Select("SELECT l.id,node_id,field_form_code,linkage_type::varchar,create_time,create_user_id,linkage_id,link_field_form_code,interface_address,self_defined,sub_page_template_code,condition::text " +
            "FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition c ON l.id=c.linkage_id " +
            "WHERE field_form_code = #{field_form_code} AND node_id = #{work_template_code} order by l.id")
    List<Map<String, Object>> findWorkTemplateColumnLinkageByCode(@Param("schema_name")String schemaName, @Param("field_form_code")String field_form_code, @Param("work_template_code")String work_template_code);

    @Select("SELECT id,linkage_id,link_field_form_code,interface_address,self_defined,condition::text,sub_page_template_code " +
            "FROM ${schema_name}._sc_work_flow_node_column_linkage_condition WHERE linkage_id = #{id} ")
    Map<String, Object> findWorkTemplateColumnLinkageInfo(@Param("schema_name")String schemaName, @Param("id")Integer id);

    @Insert("INSERT INTO ${schema_name}._sc_work_template(work_template_code, work_template_name, is_use, remark," +
            " create_time, create_user_id, is_have_sub) " +
            "SELECT #{work_template_code},work_template_name||'_clone', is_use, remark, #{create_time}, #{create_user_id} ,is_have_sub " +
            "FROM ${schema_name}._sc_work_template WHERE work_template_code = #{old_work_template_code}")
    int insertWorkTemplateClone(Map<String, Object> data);

    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node_column(node_id, field_form_code, field_code, field_name, save_type, change_type, field_right, " +
            "field_value, is_required, is_table_key, db_table_type, field_remark, field_data_base, field_view_type, field_section_type, field_validate_method, " +
            "create_time, create_user_id,field_is_show,data_order,data_key) " +
            "SELECT #{work_template_code},c.field_form_code,c.field_code, c.field_name, c.save_type, c.change_type, c.field_right, c.field_value, " +
            "c.is_required, c.is_table_key, c.db_table_type, c.field_remark, c.field_data_base, c.field_view_type, c.field_section_type, " +
            "c.field_validate_method,#{create_time}, #{create_user_id},'show',c.data_order,c.data_key " +
            "FROM ${schema_name}._sc_work_flow_node_column c WHERE c.node_id = #{old_work_template_code}")
    void insertWorkTemplateColumnClone(Map<String, Object> data);

    @Select("INSERT INTO ${schema_name}._sc_work_flow_node_column_ext(node_id, field_form_code, field_name, field_write_type, field_value, " +
            "create_time,create_user_id) " +
            "SELECT #{work_template_code},field_form_code, field_name, field_write_type, field_value,#{create_time}, #{create_user_id} " +
            "FROM ${schema_name}._sc_work_flow_node_column_ext WHERE node_id = #{old_work_template_code}")
    void insertWorkTemplateColumnExtClone(Map<String, Object> data);

    @Select("SELECT l.id,node_id, field_form_code, linkage_type::varchar, create_time, create_user_id,linkage_id, " +
            "link_field_form_code, interface_address, self_defined, condition::text,sub_page_template_code " +
            "FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition c ON l.id=c.linkage_id " +
            "WHERE node_id = #{work_template_code}")
    List<Map<String, Object>> findWorkTemplateColumnLinkageAndConditionListByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);

    @Select("SELECT wfnc.*,c1.name as field_view_type_name,c2.name as field_section_type_name FROM ${schema_name}._sc_work_template t " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfnc.node_id = t.work_template_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block' " +
            "WHERE wfnc.id = #{id}")
    Map<String, Object> findWorkTemplateColumnInfoById(@Param("schema_name")String schemaName, @Param("id")Integer id);

    @Update("<script> UPDATE ${schema_name}._sc_work_template  " +
            "<set> " +
            "<if test=\"work_template_name!=null and work_template_name !=''\"> " +
            "work_template_name = #{work_template_name}, " +
            "</if> " +
            "<if test=\"is_use!=null and is_use !=''\"> " +
            "is_use = #{is_use}, " +
            "</if> " +
            "<if test=\"remark!=null and remark !=''\"> " +
            "remark = #{remark}, " +
            "</if> " +
            "<if test=\"create_time!=null \"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"create_user_id!=null and create_user_id !=''\"> " +
            "create_user_id = #{create_user_id} , " +
            "</if> " +
            "</set> " +
            "WHERE " +
            " work_template_code = #{work_template_code} </script>")
    int updateWorkTemplate(Map<String, Object> data);

    @Select("SELECT wfnc.*,c1.name as field_view_type_name,c2.name as field_section_type_name,wfnc.data_key as source " +
            "FROM ${schema_name}._sc_work_flow_node_column wfnc " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block' " +
            "WHERE wfnc.id = #{id}")
    Map<String, Object> findWorkColumnById(@Param("schema_name")String schemaName, @Param("id")Integer id);


    @Select("SELECT id,node_id,field_form_code,field_name,field_write_type,field_value,create_time,create_user_id " +
            "FROM ${schema_name}._sc_work_flow_node_column_ext WHERE column_id = #{column_id} order by id")
    List<Map<String, Object>> findWorkTemplateColumnExtByColumnId(@Param("schema_name")String schemaName, @Param("column_id")Integer id);

    /**
     * 获取模板最大的排序
     *
     * @param schemaName
     * @param work_template_code
     * @return
     */
    @Select("SELECT max(wfnc.data_order) FROM ${schema_name}._sc_work_flow_node_column wfnc WHERE wfnc.node_id = #{work_template_code}")
    Integer findMaxDataOrderByWorkTemplateCode(@Param("schema_name")String schemaName,@Param("work_template_code") String work_template_code);

    /**
     * 通过id修改node表数据
     * @param data
     * @return
     */
    @Update("<script> UPDATE ${schema_name}._sc_work_flow_node_column  " +
            "<set> " +
            "<if test=\"field_right!=null and field_right !=''\"> " +
            "field_right = #{field_right}, " +
            "</if> " +
            "<if test=\"is_required!=null and is_required !=''\"> " +
            "is_required = #{is_required}, " +
            "</if> " +
            "<if test=\"field_is_show!=null and field_is_show !=''\"> " +
            "field_is_show = #{field_is_show} , " +
            "</if> " +
            "<if test=\"data_order!=null \"> " +
            "data_order = #{data_order} , " +
            "</if> " +
            "<if test=\"field_section_type!=null and field_section_type !=''\"> " +
            "field_section_type = #{field_section_type}, " +
            "</if> " +
            "</set> " +
            "WHERE " +
            " id = #{id}::int </script>")
    int updateWorkTemplateColumnUse(Map<String, Object> data);


    @Select("select id,node_id,field_form_code,field_code,field_name,save_type,change_type,field_right,field_value,is_required, " +
            "is_table_key,db_table_type,field_remark,field_data_base,field_view_type,field_section_type, " +
            "create_time,create_user_id,field_is_show,data_order,field_validate_method,word_flow_node_id,data_Key,data_Key as source  " +
            "from ${schema_name}._sc_work_flow_node_column where node_id = #{node_id} order by data_order")
    List<Map<String, Object>> findColumnListByNodeId(@Param("schema_name") String schemaName,@Param("node_id") String work_template_code);


//    @Select("INSERT INTO ${schema_name}._sc_work_flow_node_column_linkage(node_id, field_form_code, linkage_type, create_time, create_user_id) " +
//            "SELECT #{work_template_code},field_form_code, linkage_type,#{create_time}, #{create_user_id} " +
//            "FROM ${schema_name}._sc_work_flow_node_column_linkage WHERE node_id = #{old_work_template_code}")
//    int insertWorkTemplateColumnLinkageClone(Map<String, Object> data);
//
//    @Select("SELECT id,node_id, field_form_code, linkage_type::varchar, create_time, create_user_id FROM ${schema_name}._sc_work_flow_node_column_linkage WHERE node_id = #{work_template_code}")
//    List<Map<String, Object>> findWorkTemplateColumnLinkageListByWorkTemplateCode(@Param("schema_name")String schemaName, @Param("work_template_code")String work_template_code);


}
