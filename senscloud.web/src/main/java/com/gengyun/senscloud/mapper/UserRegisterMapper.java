package com.gengyun.senscloud.mapper;

public interface UserRegisterMapper {
//
//    //检查注册的用户是否手机号，账号，工号重复
//    @Select("select count(1) from ${schema_name}._sc_user where status>-2000 and (mobile=#{mobile} or account=#{mobile} or user_code=#{mobile} ) ")
//    int checkUserReduplicate(@Param("schema_name") String schema_name, @Param("mobile") String mobile);
//
//    //检查注册的用户是否已经提交过申请
//    @Select("select count(1) from ${schema_name}._sc_user_register where status<60 and user_mobile=#{mobile} ")
//    int checkUserRegister(@Param("schema_name") String schema_name, @Param("mobile") String mobile);
//
//    //用户注册信息提交
//    @Insert("INSERT INTO ${schema_name}._sc_user_register " +
//            "(app_code, user_name, password, user_mobile, status, body_property, create_time) " +
//            "VALUES " +
//            "(#{user.app_code}, #{user.user_name}, #{user.password}, #{user.user_mobile}, #{user.status},#{user.body_property}::jsonb, #{user.create_time})")
//    int addUserRegister(@Param("schema_name") String schema_name, @Param("user") UserRegisrerData userRegister);
//
//    /**
//     * 根据主键查询用户注册信息数据
//     *
//     * @param schemaName
//     * @param code
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_user_register where app_code = #{code} ")
//    Map<String, Object> queryUserRegisterByCode(@Param("schema_name") String schemaName, @Param("code") String code);
//
//    /**
//     * 根据主键更新用户注册信息数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_user_register set audit_time=#{pm.audit_time}, audit_person=#{pm.audit_person}, " +
//            "audit_content=#{pm.audit_content}, status=#{pm.status},body_property=#{pm.body_property}::jsonb " +
//            "where app_code = #{code}")
//    int updateUserRegisterByCode(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap, @Param("code") String code);
//
//
//    /**
//     * 查询用户注册信息列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("select t.app_code, t.user_name, t.user_mobile,to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time,t.audit_time, " +
//            "t.audit_person, t.audit_content, au.username as audit_person_name, s.status " +
//            "from ${schema_name}._sc_user_register t " +
//            "LEFT JOIN ${schema_name}._sc_user au on t.audit_person = au.account " +
//            "LEFT JOIN ${schema_name}._sc_status s on s.id = t.status " +
//            "WHERE 1=1 ${condition} " +
//            "order by t.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> findUserRegisterList(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                                   @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    /**
//     * 用户信息列表计数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) " +
//            "from ${schema_name}._sc_user_register t " +
//            "LEFT JOIN ${schema_name}._sc_user au on t.audit_person = au.account " +
//            "LEFT JOIN ${schema_name}._sc_status s on s.id = t.status " +
//            "WHERE 1=1 ${condition} ")
//    int countUserResgisterList(@Param("schema_name") String schema_name, @Param("condition") String condition);

}
