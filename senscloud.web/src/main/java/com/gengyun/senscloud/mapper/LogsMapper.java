package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * 日志信息
 */
@Mapper
public interface LogsMapper {
    /**
     * 新增日志信息
     *
     * @param schema_name    数据库
     * @param log_type       模块类别
     * @param business_no    业务数据编号
     * @param remark         日志信息
     * @param create_user_id 创建人
     * @param create_time    创建时间
     */
    @Insert("insert into ${schema_name}._sc_logs (log_type,business_no,remark,is_use,create_time,create_user_id) " +
            "values(#{log_type},#{business_no},#{remark},1,#{create_time},#{create_user_id})")
    void insertLog(@Param("schema_name") String schema_name, @Param("log_type") String log_type, @Param("business_no") String business_no,
                   @Param("remark") String remark, @Param("create_user_id") String create_user_id, @Param("create_time") Timestamp create_time);

    /**
     * 查询日志
     *
     * @param schema_name 数据库
     * @param log_type    模块类别
     * @param businessNo  业务数据编号
     * @param noneData    无数据处理
     * @return 日志列表
     */
    @Select(" SELECT L.remark, L.create_user_id,to_char( L.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "' ) AS create_time," +
            " U.CREATE_USER_NAME " +
            " FROM  ${schema_name}._sc_logs L" +
            " INNER JOIN " +
            "   (SELECT LA.ID," +
            "    STRING_AGG ( U.user_name, ',' ) AS CREATE_USER_NAME," +
            "    STRING_AGG ( U.mobile, ',' ) AS mobile " +
            "    FROM( SELECT L.ID,REGEXP_SPLIT_TO_TABLE( L.create_user_id, ',' ) AS create_user_id " +
            "             FROM ${schema_name}._SC_LOGS l " +
            "             WHERE L.BUSINESS_NO = #{businessNo}  ) LA " +
            "     LEFT JOIN ${schema_name}._SC_USER U ON LA.create_user_id = U.ID " +
            "     GROUP BY LA.ID ) u ON l.ID = u.ID " +
            " WHERE " +
            " l.log_type = #{log_type} " +
            " AND L.BUSINESS_NO = #{businessNo} " +
            " AND L.ID = U.ID " +
            " ORDER BY  l.ID DESC ")
    List<Map<String, Object>> findLog(@Param("schema_name") String schema_name, @Param("log_type") String log_type, @Param("businessNo") String businessNo, @Param("noneData") String noneData);

    @Select(" <script> " +
            " SELECT L.remark, L.create_user_id,to_char( L.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "' ) AS create_time," +
            " U.CREATE_USER_NAME " +
            " FROM  ${schema_name}._sc_logs L" +
            " INNER JOIN " +
            "   (SELECT LA.ID," +
            "    STRING_AGG ( U.user_name, ',' ) AS CREATE_USER_NAME," +
            "    STRING_AGG ( U.mobile, ',' ) AS mobile " +
            "    FROM( SELECT L.ID,REGEXP_SPLIT_TO_TABLE( L.create_user_id, ',' ) AS create_user_id " +
            "             FROM ${schema_name}._SC_LOGS l " +
            "             WHERE L.log_type = #{log_type}  ) LA " +
            "     LEFT JOIN ${schema_name}._SC_USER U ON LA.create_user_id = U.ID " +
            "     GROUP BY LA.ID ) u ON l.ID = u.ID " +
            " WHERE " +
            " l.log_type = #{log_type} " +
            " AND L.ID = U.ID " +
            " <choose> " +
            " <when test=\"log_type == "+SensConstant.BUSINESS_NO_6003+"\"> " +
            " and l.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp " +
            " and l.business_no=#{pm.facility_id}||'-'||#{pm.asset_id} " +
            " </when> " +
            " <otherwise> " +
            " and to_char(l.create_time, 'yyyy-MM') between #{pm.begin_date} and #{pm.end_date} " +
            " </otherwise> " +
            " </choose> " +
            " ORDER BY l.ID DESC " +
            " </script> ")
    List<Map<String, Object>> findLogByLogType(@Param("schema_name") String schema_name, @Param("log_type") String log_type, @Param("pm") Map<String, Object> pm);

    /**
     * 新增错误日志信息
     *
     * @param schema_name    数据库
     * @param error_code     业务数据编号
     * @param remark         日志信息
     * @param create_user_id 创建人
     * @param create_time    创建时间
     */
    @Insert("insert into ${schema_name}._sc_error_logs (error_code,remark,create_time,create_user_id) " +
            "values(#{error_code},#{remark},#{create_time},#{create_user_id})")
    void insertErrorLog(@Param("schema_name") String schema_name, @Param("error_code") String error_code,
                        @Param("remark") String remark, @Param("create_user_id") String create_user_id, @Param("create_time") Timestamp create_time);

    /**
     * 查询错误日志列表
     *
     * @param schema_name 数据库
     * @param whereString 条件
     * @return 错误日志列表
     */
    @Select(" SELECT L.error_code, L.remark, to_char( L.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "' ) AS create_time, U.user_name " +
            " FROM  ${schema_name}._sc_error_logs L" +
            " LEFT JOIN ${schema_name}._SC_USER U ON L.create_user_id = U.ID " +
            " ${whereString} ")
    List<Map<String, Object>> findErrLogList(@Param("schema_name") String schema_name, @Param("whereString") String whereString);

    /**
     * 日志列表总数统计
     *
     * @param schema_name 数据库
     * @param whereString 条件
     * @return 统计数量
     */
    @Select(" SELECT count(1) FROM  ${schema_name}._sc_error_logs L" +
            " LEFT JOIN ${schema_name}._SC_USER U ON L.create_user_id = U.ID ${whereString} ")
    int countErrLogList(@Param("schema_name") String schema_name, @Param("whereString") String whereString);

    /**
     * 查询日志总数
     *
     * @param schema_name 数据库
     * @return 统计数量
     */
    @Select(" SELECT count(1) FROM  ${schema_name}._sc_logs L WHERE ${pm.whereSql} ")
    int countLogList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

//    /**
//     * 查询日志列表
//     *
//     * @param schema_name 数据库
//     * @return 日志列表
//     */
//    @Select(" SELECT L.remark, to_char( L.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "' ) AS create_time, U.user_name, " +
//            " coalesce((select (c.resource->>d.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{company_id}), d.name) as log_type_name " +
//            " FROM  ${schema_name}._sc_logs L " +
//            " LEFT JOIN ${schema_name}._SC_USER U ON L.create_user_id = U.ID " +
//            " LEFT JOIN ${schema_name}._sc_cloud_data d ON d.code = L.log_type AND d.data_type = 'import_type' " +
//            " WHERE ${pm.whereSql} ORDER BY L.create_time DESC ${pm.pagination} ")
//    List<Map<String, Object>> findLogList(@Param("schema_name") String schema_name, @Param("userLang")String userLang, @Param("company_id") Long company_id, @Param("pm") Map<String, Object> pm);

    /**
     * 查询日志列表
     *
     * @param schema_name 数据库
     * @return 日志列表
     */
    @Select(" SELECT L.remark, to_char( L.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "' ) AS create_time, U.user_name, " +
            " d.name as log_type_name " +
            " FROM  ${schema_name}._sc_logs L " +
            " LEFT JOIN ${schema_name}._SC_USER U ON L.create_user_id = U.ID " +
            " LEFT JOIN ${schema_name}._sc_cloud_data d ON d.code = L.log_type AND d.data_type = 'import_type' " +
            " WHERE ${pm.whereSql} ORDER BY L.create_time DESC ${pm.pagination} ")
    List<Map<String, Object>> findLogList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);
}
