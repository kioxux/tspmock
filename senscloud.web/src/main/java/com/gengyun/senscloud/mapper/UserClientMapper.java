package com.gengyun.senscloud.mapper;

/**
 * @Author: Wudang Dong
 * @Description: 客户用户表数据操作mybatis 语句
 * @Date: Create in 下午 2:09 2019/7/18 0018
 */
public interface UserClientMapper {
//
//    //查询自己添加客户用户信息列表信息
//    @Select("select u.*,f.title as client_name ,st.status as status_name from ${schema_name}._sc_client_user u " +
//            "left join ${schema_name}._sc_facilities f on u.client_id=f.id " +
//            "left join ${schema_name}._sc_status as st on st.id = u.status " +
//            "where u.create_user_account=#{account} order by u.create_time desc  limit ${page} offset ${begin} ")
//    List<UserClientModal> getUserClientListByAccout(@Param("schema_name") String schema_name, @Param("account") String account, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //查询自己添加客户用户信息列表信息总数
//    @Select("select count(1) from ${schema_name}._sc_client_user where create_user_account='${account}'")
//    int getUserClientListByAccoutCount(@Param("schema_name") String schema_name, @Param("account") String account);
//
//    /**
//     * 查询用户信息列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("select t.app_code, t.user_name, t.user_account, t.user_mobile, t.client_id, t.remark, " +
//            "to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time, t.create_user_account, t.audit_time, " +
//            "t.audit_person, t.audit_content, u.username as create_user_name, au.username as audit_person_name, s.status " +
//            "from ${schema_name}._sc_client_user t " +
//            "INNER JOIN ${schema_name}._sc_user u on t.create_user_account = u.account " +
//            "LEFT JOIN ${schema_name}._sc_user au on t.audit_person = au.account " +
//            "LEFT JOIN ${schema_name}._sc_status s on s.id = t.status WHERE 1=1 ${condition}" +
//            "order by t.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> findUserClientList(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                                 @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //检查添加的用户是否重复
//    @Select("select count(1) from ${schema_name}._sc_client_user where status!=140 and (user_account=#{account} or user_mobile=#{mobile}) ")
//    int checkClientUser(@Param("schema_name") String schema_name, @Param("account") String account, @Param("mobile") String mobile);
//
//    /**
//     * 用户信息列表计数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) from ${schema_name}._sc_client_user t " +
//            "INNER JOIN ${schema_name}._sc_user u on t.create_user_account = u.account " +
//            "LEFT JOIN ${schema_name}._sc_user au on t.audit_person = au.account " +
//            "LEFT JOIN ${schema_name}._sc_status s on s.id = t.status WHERE 1=1 ${condition}")
//    int countUserClientList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //用户信息添加客户信息
//    @Insert("insert into ${schema_name}._sc_client_user (user_name, " +
//            "user_account, " +
//            "user_mobile, " +
//            "client_id, " +
//            "remark, " +
//            "create_time, " +
//            "create_user_account, " +
//            "audit_time, " +
//            "audit_person, " +
//            "audit_content, status, app_code) values(#{client.user_name}, " +
//            "#{client.user_account}, " +
//            "#{client.user_mobile}, " +
//            "#{client.client_id}, " +
//            "#{client.remark}, " +
//            "#{client.create_time}, " +
//            "#{client.create_user_account}, " +
//            "#{client.audit_time}, " +
//            "#{client.audit_person}, " +
//            "#{client.audit_content}," +
//            "#{client.status}," +
//            "#{client.app_code})")
//    int addUserClient(@Param("schema_name") String schema_name, @Param("client") UserClientModal userClientModal);
//
//    /**
//     * 根据主键查询用户信息数据
//     *
//     * @param schemaName
//     * @param code
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_client_user where app_code = #{code} ")
//    Map<String, Object> queryUserClientById(@Param("schema_name") String schemaName, @Param("code") String code);
//
//    /**
//     * 根据主键更新用户信息数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_client_user set audit_time=#{pm.audit_time}, audit_person=#{pm.audit_person}, audit_content=#{pm.audit_content}, status=#{pm.status} where app_code = #{code}")
//    int updateUserClientById(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap, @Param("code") String code);
}
