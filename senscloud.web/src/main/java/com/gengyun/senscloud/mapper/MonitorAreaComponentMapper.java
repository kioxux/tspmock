package com.gengyun.senscloud.mapper;

/**
 * 备件报废
 */
public interface MonitorAreaComponentMapper {
//
//    /**
//     * 查找区域组件列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT cm.id, cm.component_name,cm.iot_code, " +
//            "p. position_name,ma.monitor_area_name,cm.is_iot_asset,us.username as create_user_name,(SELECT COUNT (*) FROM ${schema_name}._sc_monitor_area_component_asset WHERE component_id = cm.id ) AS asset_num " +
//            "FROM ${schema_name}._sc_monitor_area_component cm " +
//            "LEFT JOIN ${schema_name}._sc_monitor_area ma ON cm.monitor_area_code = ma.monitor_area_code " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p ON p.position_code = ma.asset_position_code  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position_organization rp ON p.position_code = rp.position_code  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = rp.org_id  " +
//            "LEFT join ${schema_name}._sc_user us on ma.create_user_account=us.account " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY  cm.id,cm.component_name,cm.iot_code,p.position_name,ma.monitor_area_name,cm.is_iot_asset,us.username,cm.createtime  " +
//            "ORDER BY cm.createtime desc  limit ${page} offset ${begin}")
//    List<Map<String, Object>> getMonitorAreaComponentList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查找区域组件列表总数
//     *
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM( " +
//            "SELECT cm.id, cm.component_name,cm.iot_code, " +
//            "p.position_name,ma.monitor_area_name,cm.is_iot_asset,us.username as create_user_name,(SELECT COUNT (*) FROM ${schema_name}._sc_monitor_area_component_asset WHERE component_id = cm.id ) AS asset_num " +
//            "FROM ${schema_name}._sc_monitor_area_component cm " +
//            "LEFT JOIN ${schema_name}._sc_monitor_area ma ON cm.monitor_area_code = ma.monitor_area_code " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p ON p.position_code = ma.asset_position_code  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position_organization rp ON p.position_code = rp.position_code  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = rp.org_id  " +
//            "LEFT join ${schema_name}._sc_user us on ma.create_user_account=us.account " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY  cm.id,cm.component_name,cm.iot_code,p.position_name,ma.monitor_area_name,cm.is_iot_asset,us.username,cm.createtime  )s")
//    int countMonitorAreaComponentList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     * 新建区域组件
//     *
//     * @param
//     * @param
//     * @return
//     */
//    @Insert("INSERT INTO  ${schema_name}._sc_monitor_area_component" +
//            "(component_name,monitor_area_code,is_iot_asset,iot_code,create_user_account,createtime) " +
//            "VALUES " +
//            "(#{component_name},#{monitor_area_code},#{is_iot_asset},#{iot_code},#{create_user_account},current_timestamp)  RETURNING id ")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int addMonitorAreaComponent(Map<String, Object> monitorAreaComponent);
//
//
//    /**
//     * 新建区域组件关联的设备
//     *
//     * @param
//     * @param
//     * @return
//     */
//    @Insert("INSERT INTO  ${schema_name}._sc_monitor_area_component_asset" +
//            "(component_id,asset_id) " +
//            "VALUES " +
//            "(#{component_id}::int,#{asset_id} )")
//    int addMonitorAreaComponentAsset(@Param("schema_name") String schemaName, @Param("component_id") int component_id,@Param("asset_id") String asset_id);
//
//    /**
//     * 根据主键查删除区域组件关联设备数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("DELETE FROM ${schema_name}._sc_monitor_area_component_asset  where component_id = #{id} ")
//    int deleteMonitorAreaComponentAssetById(@Param("schema_name") String schemaName, @Param("id") int id);
//    /**
//     * 修改区域组件
//     *
//     * @param
//     * @param
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_monitor_area_component " +
//            "SET " +
//            "component_name = #{component_name}, " +
//            "monitor_area_code = #{monitor_area_code}, " +
//            "is_iot_asset = #{is_iot_asset}::int, " +
//            "iot_code = #{iot_code} " +
//            "WHERE  id= #{id}::int")
//    int updateMonitorAreaComponent(Map<String, Object> monitorArea);
//
//    /**
//     * 根据主键查删除区域组件数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("DELETE FROM ${schema_name}._sc_monitor_area_component  where id = #{id} ")
//    int deleteMonitorAreaComponentById(@Param("schema_name") String schemaName, @Param("id") int id);
//
//    /**
//     *根据区域组件主键查查询区域组件关联设备数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT a._id,a.strname,ai.out_code as iot_code FROM ${schema_name}._sc_monitor_area_component cm " +
//            "LEFT JOIN ${schema_name}._sc_monitor_area_component_asset am on cm.id=am.component_id " +
//            "LEFT JOIN ${schema_name}._sc_asset a on am.asset_id=_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_iot_setting ai on a._id=ai.asset_id "  +
//            "WHERE cm.id = #{id} and a._id NOTNULL")
//    List<Map<String, Object>> getmonitorAreaComponentAssetById (@Param("schema_name") String schemaName, @Param("id") int id);
//
//    /**
//     * 根据主键查查询区域组件数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT cm.id,cm.component_name,cm.monitor_area_code, " +
//            "cm.is_iot_asset,cm.iot_code,cm.createtime,cm.create_user_account,array_to_string(array_agg(ca.asset_id),',')as asset_ids  " +
//            "FROM ${schema_name}._sc_monitor_area_component cm " +
//            "LEFT JOIN ${schema_name}._sc_monitor_area_component_asset ca on cm.id=ca.component_id  " +
//            "where cm.id = #{id}" +
//            "GROUP BY cm.id,cm.component_name,cm.monitor_area_code,cm.is_iot_asset,cm.iot_code,cm.createtime,cm.create_user_account  ")
//    Map<String, Object> getmonitorAreaComponentById (@Param("schema_name") String schemaName, @Param("id") int id);
//
//    /**
//     * 根据主键查查询区域组件数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("DELETE ${schema_name}._sc_monitor_area_component_asset  " +
//            "WHERE component_id = #{id}")
//    int delAssetByComponentId (@Param("schema_name") String schemaName, @Param("id") int id);
//
//    /**
//     *根据区域组件主键查查询区域组件关联设备数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT a._id,a.strname,ai.out_code FROM ${schema_name}._sc_monitor_area_component_asset cm" +
//            "LEFT JOIN ${schema_name}._sc_asset a" +
//            "LEFT JOIN ${schema_name}._sc_asset_iot_setting ai" +
//            "LEFT JOIN ${schema_name}._sc_facility ai" +
//            "WHERE ai.iot_status<>1 ${condition}")
//    List<Map<String, Object>> getIotAsset(@Param("schema_name") String schemaName, @Param("condition") String condition);


}
