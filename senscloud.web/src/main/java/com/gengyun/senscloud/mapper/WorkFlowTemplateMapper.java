package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface WorkFlowTemplateMapper {

    /**
     * 获取流程基本信息
     *
     * @param schemaName
     * @param pref_id
     * @return
     */
    @Select("SELECT i.id,i.pref_id,i.flow_name,i.show_name as flow_show_name,i.work_type_id,i.relation_type::text,i.work_template_code,i.create_time,i.create_user_id,n.remark,n.page_type, " +
            "i.flow_code,n.node_id,n.node_type,n.node_name,n.show_name as node_show_name,t.type_name,te.work_template_name,c1.name as relation_type_name,deal_type_id::text,deal_role_id,n.id as word_flow_node_id " +
            "FROM ${schema_name}._sc_work_flow_info i " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id " +
            "LEFT JOIN ${schema_name}._sc_work_type t ON i.work_type_id = t.id " +
            "LEFT JOIN ${schema_name}._sc_work_template te ON i.work_template_code=te.work_template_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON i.relation_type::text= c1.code AND c1.data_type = 'relationTypes' " +
            "WHERE n.page_type = 'base' AND i.pref_id = #{pref_id} AND n.node_id = #{pref_id}")
    Map<String, Object> findFlowInfo(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    /**
     * 获取节点信息
     *
     * @param schemaName
     * @param pref_id
     * @param node_id
     * @param page_type
     * @return
     */
//    @Select("SELECT i.id,n.id as word_flow_node_id,n.pref_id,n.node_id,n.node_name,n.show_name as node_show_name,i.flow_name,i.show_name as flow_show_name,i.work_type_id,i.relation_type::text,i.work_template_code, " +
//            "n.create_time,n.create_user_id,n.node_type,t.type_name,te.work_template_name,c1.name as relation_type_name,deal_type_id::text,deal_role_id,n.remark,i.flow_code,n.page_type  " +
//            "FROM ${schema_name}._sc_work_flow_info i " +
//            "LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id " +
//            "LEFT JOIN ${schema_name}._sc_work_type t ON i.work_type_id = t.id " +
//            "LEFT JOIN ${schema_name}._sc_work_template te ON i.work_template_code=te.work_template_code " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON i.relation_type::text= c1.code AND c1.data_type = 'relationTypes' " +
//            "WHERE n.page_type = #{page_type} AND i.pref_id = #{pref_id} AND n.node_id = #{node_id}")
    @Select("SELECT i.id,n.id as word_flow_node_id,i.pref_id,#{node_id} as node_id,n.node_name,n.show_name as node_show_name,i.flow_name,i.show_name as flow_show_name,i.work_type_id,i.relation_type::text,i.work_template_code, " +
            "n.create_time,n.create_user_id,n.node_type,n.node_user,t.type_name,te.work_template_name,c1.name as relation_type_name,deal_type_id::text,deal_role_id,n.remark,i.flow_code,#{page_type} as page_type  " +
            "FROM ${schema_name}._sc_work_flow_info i " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id AND n.page_type = #{page_type} AND n.node_id = #{node_id} " +
            "LEFT JOIN ${schema_name}._sc_work_type t ON i.work_type_id = t.id " +
            "LEFT JOIN ${schema_name}._sc_work_template te ON i.work_template_code=te.work_template_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON i.relation_type::text= c1.code AND c1.data_type = 'relationTypes' " +
            "WHERE i.pref_id = #{pref_id}")
    Map<String, Object> findNodeInfo(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id, @Param("node_id") String node_id, @Param("page_type") String page_type);

    /**
     * flow表插入数据
     *
     * @param data
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_info(pref_id, flow_name, show_name, work_type_id, relation_type, work_template_code, " +
            "create_time, create_user_id, flow_code) " +
            "VALUES (#{pref_id}, #{flow_name}, #{show_name}, #{work_type_id}::int, #{relation_type}::int," +
            " #{work_template_code}, #{create_time},#{create_user_id}, #{flow_code})")
    int insertWorkFlowInfo(Map<String, Object> data);

    /**
     * node表插入数据
     *
     * @param data
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node(node_id, pref_id, node_name, show_name, node_type, deal_type_id,  " +
            "deal_role_id, remark, create_time, create_user_id,page_type,node_user)  " +
            "VALUES (#{node_id}, #{pref_id}, #{node_name}, #{show_name}, #{node_type}, #{deal_type_id}::int, #{deal_role_id}, " +
            "#{remark}, #{create_time},#{create_user_id},#{page_type},#{node_user})")
    int insertWorkFlowNode(Map<String, Object> data);

    /**
     * 通过pref_id修改流程表信息
     *
     * @param data
     * @return
     */
    @Update("<script> UPDATE ${schema_name}._sc_work_flow_info  " +
            "<set> " +
            "<if test=\"flow_name !=null and flow_name !=''\"> " +
            "flow_name = #{flow_name}, " +
            "</if> " +
            "<if test=\"show_name!=null and show_name !=''\"> " +
            "show_name = #{show_name}, " +
            "</if> " +
            "<if test=\"work_type_id!=null and work_type_id !=''\"> " +
            "work_type_id = #{work_type_id}::int, " +
            "</if> " +
            "<if test=\"relation_type!=null and relation_type !=''\"> " +
            "relation_type = #{relation_type}::int, " +
            "</if> " +
            "<if test=\"work_template_code!=null and work_template_code !=''\"> " +
            "work_template_code = #{work_template_code}, " +
            "</if> " +
            "<if test=\"create_time!=null \"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"create_user_id!=null and create_user_id !=''\"> " +
            "create_user_id = #{create_user_id}, " +
            "</if> " +
            "<if test=\"flow_code!=null and flow_code !=''\"> " +
            "flow_code = #{flow_code}, " +
            "</if> " +
            "</set> " +
            "WHERE pref_id = #{pref_id} </script>")
    int updateWorkFlowInfo(Map<String, Object> data);

    /**
     * 通过pref_id，node_id，node_type修改node表数据
     *
     * @param data
     * @return
     */
    @Update("<script> UPDATE ${schema_name}._sc_work_flow_node  " +
            "<set> " +
            "node_user = #{node_user}, " +
            "<if test=\"node_name !=null and node_name !=''\"> " +
            "node_name = #{node_name}, " +
            "</if> " +
            "<if test=\"show_name!=null and show_name !=''\"> " +
            "show_name = #{show_name}, " +
            "</if> " +
            "<if test=\"deal_type_id!=null and deal_type_id !=''\"> " +
            "deal_type_id = #{deal_type_id}::int, " +
            "</if> " +
            "<if test=\"deal_role_id!=null and deal_role_id !=''\"> " +
            "deal_role_id = #{deal_role_id}, " +
            "</if> " +
            "<if test=\"remark!=null and remark !=''\"> " +
            "remark = #{remark}, " +
            "</if> " +
            "<if test=\"create_time!=null \"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"create_user_id!=null and create_user_id !=''\"> " +
            "create_user_id = #{create_user_id}, " +
            "</if> " +
            "<if test=\"node_type!=null and node_type !=''\"> " +
            "node_type = #{node_type}, " +
            "</if> " +
            "</set> " +
            "WHERE pref_id = #{pref_id} AND node_id=#{node_id} AND page_type=#{page_type} </script>")
    int updateWorkFlowNode(Map<String, Object> data);

    /**
     * 通过pref_id 删除字段联动条件表数据
     *
     * @param schemaName
     * @param pref_id
     */
    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "using ${schema_name}._sc_work_flow_node_column_linkage l  " +
            "WHERE l.id=c.linkage_id and l.node_id IN (SELECT node_id FROM ${schema_name}._sc_work_flow_node WHERE pref_id = #{pref_id})")
    void deleteColumnLinkageConditionsByPrefIdForWorkTemplate(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    /**
     * 通过pref_id删除字段联动表数据
     *
     * @param schemaName
     * @param pref_id
     */
    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "WHERE l.node_id IN (SELECT node_id FROM ${schema_name}._sc_work_flow_node WHERE pref_id = #{pref_id})")
    void deleteColumnLinkagesByPrefIdForWorkTemplate(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    /**
     * 通过prefer_id删除字段特殊属性表数据
     *
     * @param schemaName
     * @param pref_id
     */
    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_ext e " +
            "WHERE e.node_id IN (SELECT node_id FROM ${schema_name}._sc_work_flow_node WHERE pref_id = #{pref_id})")
    void deleteColumnExtByPrefIdForWorkTemplate(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    /**
     * 通过pref_id删除字段表数据
     *
     * @param schemaName
     * @param pref_id
     */
    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column c " +
            "WHERE c.node_id IN (SELECT node_id FROM ${schema_name}._sc_work_flow_node WHERE pref_id = #{pref_id})")
    void deleteColumnsByPrefIdForWorkTemplate(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    @Select("SELECT count(1) " +
            "FROM ${schema_name}._sc_work_flow_node wfn  " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column  wfnc ON wfn.id = wfnc.word_flow_node_id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column'   " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block'  " +
            "WHERE wfn.pref_id = #{pref_id} AND wfn.node_id = #{node_id} AND wfn.page_type = #{page_type}")
    int countWorkFlowTemplateColumnList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord, @Param("pref_id") String pref_id, @Param("node_id") String node_id, @Param("page_type") String node_type);

//    /**
//     * 查询流程、节点字段信息列表
//     *
//     * @param schemaName
//     * @param searchWord
//     * @param pref_id
//     * @param node_id
//     * @param page_type  base （基本） detail（详情）
//     * @param company_id 公司id
//     * @param userLang   用户语音类型
//     * @return
//     */
//    @Select("SELECT wfnc.id,wfnc.field_form_code,wfnc.field_code,wfnc.field_view_type,wfnc.field_section_type,wfnc.create_time, " +
//            "coalesce((select (c.resource->>wfnc.field_name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{company_id}), wfnc.field_name) as field_name, " +
//            "wfnc.create_user_id,wfnc.save_type,wfnc.field_right,wfnc.is_required,wfnc.field_remark,wfnc.data_key as source,wfnc.data_key,wfnc.field_value, " +
//            "c1.name as field_view_type_name,c2.name as field_section_type_name,wfn.node_id,wfnc.field_is_show,wfnc.data_order " +
//            "FROM ${schema_name}._sc_work_flow_node wfn  " +
//            "JOIN ${schema_name}._sc_work_flow_node_column  wfnc ON wfn.id = wfnc.word_flow_node_id " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column'   " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block'  " +
//            "WHERE wfn.pref_id = #{pref_id} AND wfn.node_id = #{node_id} AND wfn.page_type = #{page_type} ${searchWord} ")
//    List<Map<String, Object>> findWorkFlowTemplateColumnList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord, @Param("pref_id") String pref_id, @Param("node_id") String node_id, @Param("page_type") String page_type, @Param("company_id") Long company_id, @Param("userLang") String userLang);

    @Select("SELECT wfnc.id,wfnc.field_form_code,wfnc.field_code,wfnc.field_view_type,wfnc.field_section_type,wfnc.create_time, " +
            "wfnc.field_name as field_name, " +
            "wfnc.create_user_id,wfnc.save_type,wfnc.field_right,wfnc.is_required,wfnc.field_remark,wfnc.data_key as source,wfnc.data_key,wfnc.field_value, " +
            "c1.name as field_view_type_name,c2.name as field_section_type_name,wfn.node_id,wfnc.field_is_show,wfnc.data_order " +
            "FROM ${schema_name}._sc_work_flow_node wfn  " +
            "JOIN ${schema_name}._sc_work_flow_node_column  wfnc ON wfn.id = wfnc.word_flow_node_id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column'   " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block'  " +
            "WHERE wfn.pref_id = #{pref_id} AND wfn.node_id = #{node_id} AND wfn.page_type = #{page_type} ${searchWord} ")
    List<Map<String, Object>> findWorkFlowTemplateColumnList(@Param("schema_name") String schemaName, @Param("searchWord") String searchWord, @Param("pref_id") String pref_id, @Param("node_id") String node_id, @Param("page_type") String page_type);

    /**
     * 插入流程字段 通过
     * 查询工单模板的字段信息
     *
     * @param data
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node_column(node_id, field_form_code, field_code, field_name, save_type, change_type, field_right, " +
            "field_value,is_required, is_table_key, db_table_type, field_remark, field_data_base, field_view_type, field_section_type, field_validate_method,  " +
            "create_time,create_user_id, field_is_show, data_order, word_flow_node_id,data_key)  " +
            "SELECT #{node_id},c.field_form_code,c.field_code, c.field_name, c.save_type, c.change_type, c.field_right, c.field_value,   " +
            "c.is_required, c.is_table_key, c.db_table_type, c.field_remark, c.field_data_base, c.field_view_type, c.field_section_type,    " +
            "c.field_validate_method,#{create_time}, #{create_user_id},c.field_is_show ,#{sort}, #{word_flow_node_id}::int,c.data_key " +
            "FROM ${schema_name}._sc_work_flow_node_column c WHERE c.field_form_code = #{field_form_code} AND node_id = #{work_template_code}")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertWorkFlowColumn(Map<String, Object> data);

    /**
     * 插入流程字段特殊属性 通过
     * 查询工单模板的字段信息
     *
     * @param data
     */
    @Select("INSERT INTO ${schema_name}._sc_work_flow_node_column_ext(node_id, field_form_code, field_name, field_write_type, field_value, " +
            "create_time,create_user_id,column_id) " +
            "SELECT #{node_id},field_form_code, field_name, field_write_type, field_value,#{create_time}, #{create_user_id} ,#{id}" +
            "FROM ${schema_name}._sc_work_flow_node_column_ext WHERE field_form_code = #{field_form_code} AND node_id = #{work_template_code} ")
    void insertWorkFlowColumnExt(Map<String, Object> data);

    /**
     * 通过工单模板及字段编码查询字段联动条件
     *
     * @param schemaName
     * @param work_template_code
     * @param fieldFormCode
     * @return
     */
    @Select("SELECT l.id,node_id, field_form_code, linkage_type::varchar, create_time, create_user_id,linkage_id, " +
            "link_field_form_code, interface_address, self_defined, condition::text,sub_page_template_code " +
            "FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition c ON l.id=c.linkage_id " +
            "WHERE node_id = #{work_template_code} and field_form_code = #{field_form_code} order by l.id")
    List<Map<String, Object>> findWorkTemplateColumnLinkageAndConditionListByNodeIdAndFieldFormCode(@Param("schema_name") String schemaName, @Param("work_template_code") String work_template_code, @Param("field_form_code") String fieldFormCode);

    @Select("SELECT * FROM ${schema_name}._sc_work_flow_node_column WHERE id = #{id}")
    Map<String, Object> findColumnInfoById(@Param("schema_name") String schemaName, @Param("id") Integer id);

    @Update("UPDATE ${schema_name}._sc_work_flow_node_column  " +
            "SET data_order = #{top_order} " +
            "WHERE id = #{id}::int ")
    void upOrder(Map<String, Object> data);

    @Update("UPDATE ${schema_name}._sc_work_flow_node_column  " +
            "SET data_order = #{down_order} " +
            "WHERE id = #{id}::int ")
    void downOrder(Map<String, Object> data);

    @Select("SELECT * FROM ${schema_name}._sc_work_flow_node_column WHERE node_id = #{node_id} and data_order=#{data_order}")
    Map<String, Object> findColumnInfoByOrder(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("data_order") int data_order);

    @Select("<script> " +
            "SELECT * FROM ${schema_name}._sc_work_flow_node_column " +
            "WHERE node_id = #{node_id} and data_order &lt;= #{data_order} " +
            "and id != #{id}::int " +
            "<if test = \"word_flow_node_id!=null and word_flow_node_id != ''\"> " +
            "AND word_flow_node_id = #{word_flow_node_id}::int " +
            "</if> " +
            "ORDER BY data_order desc " +
            "</script>")
    List<Map<String, Object>> findColumnListForDescOrder(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("data_order") Integer data_order,@Param("id") String id,@Param("word_flow_node_id")String word_flow_node_id);

    @Select("<script> " +
            "SELECT * FROM ${schema_name}._sc_work_flow_node_column " +
            "WHERE node_id = #{node_id} and data_order >= #{data_order} " +
            "and id != #{id}::int " +
            "<if test = \"word_flow_node_id!=null and word_flow_node_id != ''\"> " +
            "AND word_flow_node_id = #{word_flow_node_id}::int " +
            "</if> " +
            "ORDER BY data_order asc " +
            "</script>")
    List<Map<String, Object>> findColumnListForAscOrder(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("data_order") Integer data_order,@Param("id") String id,@Param("word_flow_node_id")String word_flow_node_id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_ext WHERE column_id = #{column_id}")
    void deleteFlowColumnExtByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "using ${schema_name}._sc_work_flow_node_column_linkage l " +
            "where l.id=c.linkage_id and l.column_id = #{column_id} ")
    void deleteFlowColumnLinkageConditionByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer id);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_linkage WHERE column_id = #{column_id} ")
    void deleteFlowColumnLinkageByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer id);

    @Select("SELECT wfn.* FROM ${schema_name}._sc_work_flow_node wfn " +
            "JOIN ${schema_name}._sc_work_flow_node_column wfnc ON wfn.id=wfnc.word_flow_node_id " +
            "WHERE wfnc.id = #{id}")
    Map<String, Object> findNodeInfoByColumnId(@Param("schema_name") String schemaName, @Param("id") Integer id);

    @Select("select ((SELECT count(1) from ( " +
            "SELECT jsonb_array_elements(t.top ) as top from ( " +
            "SELECT condition->'judge' as top  " +
            "FROM ${schema_name}._sc_work_flow_node_column wfnc  " +
            "JOIN ${schema_name}._sc_work_flow_node_column_linkage wfncl ON wfnc.id=wfncl.column_id " +
            "JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition wfnclc ON wfncl.id=wfnclc.linkage_id  " +
            "WHERE wfnc.word_flow_node_id = #{word_flow_node_id}::int " +
            ") t) t WHERE top->>'field' = #{field_form_code}) + " +
            "(SELECT count(1) FROM ${schema_name}._sc_work_flow_node_column wfnc  " +
            "JOIN ${schema_name}._sc_work_flow_node_column_linkage wfncl ON wfnc.id=wfncl.column_id " +
            "JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition wfnclc ON wfncl.id=wfnclc.linkage_id  " +
            "WHERE wfnc.word_flow_node_id = #{word_flow_node_id}::int and wfnclc.link_field_form_code = #{field_form_code}) )::int as count")
    int findColumnCountUseByWordFlowNodeId(@Param("schema_name") String schemaName, @Param("word_flow_node_id") String word_flow_node_id, @Param("field_form_code") String field_form_code);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column WHERE id = #{id} ")
    void deleteTemplateColumnById(@Param("schema_name") String schemaName, @Param("id") Integer id);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_ext WHERE column_id = #{column_id}  ")
    void deleteTemplateColumnExtByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "using ${schema_name}._sc_work_flow_node_column_linkage l " +
            "where l.id=c.linkage_id and l.column_id = #{column_id} ")
    void deleteTemplateColumnLinkageConditionByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer id);

    @Delete("DELETE FROM ${schema_name}._sc_work_flow_node_column_linkage WHERE column_id = #{column_id} ")
    void deleteTemplateColumnLinkageByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer id);

    /**
     * 获取节点最大的排序
     *
     * @param schemaName
     * @param pref_id
     * @param node_id
     * @param page_type
     * @return
     */
    @Select("SELECT max(wfnc.data_order) " +
            "FROM ${schema_name}._sc_work_flow_node wfn   " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column  wfnc ON wfn.id = wfnc.word_flow_node_id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON wfnc.field_view_type = c1.code AND c1.data_type = 'work_column'   " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c2 ON wfnc.field_section_type = c2.code AND c2.data_type = 'block' " +
            "WHERE wfn.pref_id = #{pref_id} AND wfn.node_id = #{node_id} AND wfn.page_type = #{page_type} " +
            "GROUP BY wfn.pref_id,wfn.node_id,wfn.page_type")
    Integer findMaxDataOrderByNode(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id, @Param("node_id") String node_id, @Param("page_type") String page_type);

    /**
     * 根据流程、节点类型获取克隆对应模板
     *
     * @param schemaName
     * @param node_type_condition
     * @return
     */
    @Select("SELECT wfi.id,wfi.flow_name,wfi.flow_name||'/'||wfn.node_name as node_name,case when node_type = 'flow' then true else false end as is_parent , " +
            "wfn.pref_id,wfn.node_id,wfn.node_type,wfn.id as word_flow_node_id " +
            "FROM ${schema_name}._sc_work_flow_info wfi " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node wfn ON wfi.pref_id=wfn.pref_id " +
            "where wfn.page_type = 'base' " +
            "${node_type_condition}  " +
            "ORDER BY wfi.pref_id ,is_parent desc")
    List<Map<String, Object>> getWorkFlowTemplateListForClone(@Param("schema_name") String schemaName, @Param("node_type_condition") String node_type_condition);

    /**
     * 根据主键获取node表详情数据
     *
     * @param schemaName
     * @param id
     * @return
     */
    @Select("select * from ${schema_name}._sc_work_flow_node where id = #{id}::int")
    Map<String, Object> findNodeInfoById(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 根据node表id查询插入flow_info表
     *
     * @param data
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_info(pref_id, flow_name, show_name, work_type_id, relation_type, work_template_code, " +
            "create_time, create_user_id, flow_code)  " +
            "SELECT #{pref_id},#{flow_name}, #{flow_name}, -1, i.relation_type, i.work_template_code,#{create_time}, " +
            "#{create_user_id}, #{flow_code} FROM ${schema_name}._sc_work_flow_info i JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id = n.pref_id WHERE n.id = #{word_flow_node_id}::int")
    int insertWorkFlowClone(Map<String, Object> data);

    /**
     * 根据node表id查询插入node表
     *
     * @param data
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node(node_id, pref_id, node_name, show_name, node_type, deal_type_id, deal_role_id, " +
            "remark, create_time, create_user_id,page_type) " +
            "SELECT #{node_id}, #{pref_id}, #{node_name}, #{node_name}, #{node_type}, n.deal_type_id, n.deal_role_id, n.remark, " +
            "#{create_time},#{create_user_id},n.page_type FROM ${schema_name}._sc_work_flow_node n WHERE n.id = #{word_flow_node_id}::int")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertWorkFlowNodeClone(Map<String, Object> data);

//    /**
//     * 根据node表子节点id查询该节点数据插入node表
//     * @param data
//     * @return
//     */
//    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node(node_id, pref_id, node_name, show_name, node_type, deal_type_id, deal_role_id, " +
//            "remark, create_time, create_user_id) " +
//            "SELECT #{node_id},#{pref_id}, #{node_name}, #{node_name}, n.node_type, n.deal_type_id, n.deal_role_id, n.remark, " +
//            "#{create_time},#{create_user_id} FROM ${schema_name}._sc_work_flow_node n WHERE n.id = (SELECT wfn1.id  " +
//            "FROM ${schema_name}._sc_work_flow_node wfn1  " +
//            "JOIN (SELECT * from ${schema_name}._sc_work_flow_node WHERE id=#{word_flow_node_id}::int) wfn2 ON wfn1.pref_id=wfn2.pref_id AND wfn1.node_id=wfn2.node_id WHERE wfn1.node_type = '2' )")
//    int insertWorkFlowNodeCloneBySub(Map<String, Object> data);

    @Select("SELECT id,node_id,field_form_code,field_name,field_write_type,field_value,create_time,create_user_id " +
            "FROM ${schema_name}._sc_work_flow_node_column_ext WHERE column_id = #{column_id}::int order by id")
    List<Map<String, Object>> findWorkFlowColumnExtByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer column_id);

    @Select("SELECT l.id,node_id,field_form_code,linkage_type::varchar,create_time,create_user_id,linkage_id,link_field_form_code,interface_address,self_defined,condition::text,sub_page_template_code,edit_sub_page_template_code,scan_fail_return_template_code,is_data_from_page " +
            "FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition c ON l.id=c.linkage_id " +
            "WHERE l.column_id =  #{column_id}::int order by l.id")
    List<Map<String, Object>> findWorkFlowColumnLinkageByColumnId(@Param("schema_name") String schemaName, @Param("column_id") Integer column_id);

    @Select("select id,node_id,field_form_code,field_code,field_name,save_type,change_type,field_right,field_value,is_required, " +
            "is_table_key,db_table_type,field_remark,field_data_base,field_view_type,field_section_type, " +
            "create_time,create_user_id,field_is_show,data_order,field_validate_method,word_flow_node_id,data_Key,data_Key as source  " +
            "from ${schema_name}._sc_work_flow_node_column where word_flow_node_id = #{word_flow_node_id}::int order by data_order")
    List<Map<String, Object>> findColumnListByWordFlowNodeId(@Param("schema_name") String schemaName, @Param("word_flow_node_id") String word_flow_node_id);

    @Select("select id,node_id,field_form_code,field_code,field_name,save_type,change_type,field_right,field_value,is_required, " +
            "is_table_key,db_table_type,field_remark,field_data_base,field_view_type,field_section_type, " +
            "create_time,create_user_id,field_is_show,data_order,field_validate_method,word_flow_node_id,data_key,data_key as source  " +
            "from ${schema_name}._sc_work_flow_node_column where node_id = #{node_id} order by data_order")
    List<Map<String, Object>> findColumnListByNodeId(@Param("schema_name") String schemaName, @Param("node_id") String node_id);

    @Insert("INSERT INTO ${schema_name}._sc_work_flow_node_column(node_id, field_form_code, field_code, field_name, save_type, change_type, field_right, " +
            "field_value,is_required, is_table_key, db_table_type, field_remark, field_data_base, field_view_type, field_section_type, field_validate_method,  " +
            "create_time,create_user_id, field_is_show, data_order, word_flow_node_id,data_key)  " +
            "select #{node_id},c.field_form_code,c.field_code, c.field_name, c.save_type, c.change_type, c.field_right, c.field_value,   " +
            "c.is_required, c.is_table_key, c.db_table_type, c.field_remark, c.field_data_base, c.field_view_type, c.field_section_type,    " +
            "c.field_validate_method,#{create_time}, #{create_user_id},c.field_is_show ,c.data_order, #{word_flow_node_id}::int,c.data_key " +
            "FROM ${schema_name}._sc_work_flow_node_column c WHERE c.id = #{id}::int ")
    @Options(useGeneratedKeys = true, keyProperty = "column_id", keyColumn = "id")
    int insertWorkFlowColumnAdd(Map<String, Object> data);

    @Select("INSERT INTO ${schema_name}._sc_work_flow_node_column_ext(node_id, field_form_code, field_name, field_write_type, field_value, " +
            "create_time,create_user_id,column_id) " +
            "SELECT #{node_id},field_form_code, field_name, field_write_type, field_value,#{create_time}, #{create_user_id} ,#{column_id}" +
            "FROM ${schema_name}._sc_work_flow_node_column_ext WHERE column_id = #{id}::int ")
    void insertWorkFlowColumnExtAdd(Map<String, Object> data);

    @Select("SELECT l.id,node_id, field_form_code, linkage_type::varchar, create_time, create_user_id,linkage_id, " +
            "link_field_form_code, interface_address, self_defined, condition::text,sub_page_template_code," +
            "edit_sub_page_template_code,scan_fail_return_template_code,is_data_from_page " +
            "FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node_column_linkage_condition c ON l.id=c.linkage_id " +
            "WHERE l.column_id = #{column_id}::int order by l.id")
    List<Map<String, Object>> findWorkTemplateColumnLinkageAndConditionListByColumnId(@Param("schema_name") String schemaName, @Param("column_id") String column_id);

    @Select("SELECT * FROM ${schema_name}._sc_work_flow_node n WHERE n.id = (SELECT wfn1.id  " +
            "FROM ${schema_name}._sc_work_flow_node wfn1  " +
            "JOIN (SELECT * from ${schema_name}._sc_work_flow_node " +
            "WHERE id=#{id}::int) wfn2 ON wfn1.pref_id=wfn2.pref_id AND wfn1.node_id=wfn2.node_id WHERE wfn1.page_type = 'detail' )")
    Map<String, Object> findWorkFlowNodeInfoBySub(@Param("schema_name") String schemaName, @Param("id") String id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_info where pref_id = #{pref_id}  ")
    void deleteWorkFlowByPrefId(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node where id = #{id}::int ")
    void deleteWorkNodeById(@Param("schema_name") String schemaName, @Param("id") String word_flow_node_id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_ext e " +
            "using ${schema_name}._sc_work_flow_node_column c " +
            "where c.id=e.column_id and c.word_flow_node_id = #{word_flow_node_id}::int ")
    void deleteColumnsExtsByWordFlowNodeId(@Param("schema_name") String schemaName, @Param("word_flow_node_id") String word_flow_node_id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_linkage_condition c " +
            "using ${schema_name}._sc_work_flow_node_column_linkage l join ${schema_name}._sc_work_flow_node_column nc on l.column_id=nc.id " +
            "where l.id=c.linkage_id and nc.word_flow_node_id = #{word_flow_node_id}::int ")
    void deleteColumnsLinkageConditionsByWordFlowNodeId(@Param("schema_name") String schemaName, @Param("word_flow_node_id") String word_flow_node_id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column_linkage l " +
            "using ${schema_name}._sc_work_flow_node_column c " +
            "where c.id=l.column_id and c.word_flow_node_id = #{word_flow_node_id}::int ")
    void deleteColumnsLinkagesByWordFlowNodeId(@Param("schema_name") String schemaName, @Param("word_flow_node_id") String word_flow_node_id);

    @Delete("delete FROM ${schema_name}._sc_work_flow_node_column where word_flow_node_id = #{word_flow_node_id}::int ")
    void deleteColumnsByWordFlowNodeId(@Param("schema_name") String schemaName, @Param("word_flow_node_id") String word_flow_node_id);

    @Select("SELECT count(1) FROM ${schema_name}._sc_work_flow_info WHERE pref_id != #{pref_id} AND work_type_id = #{work_type_id}::int")
    int findCountWorkType(@Param("schema_name") String schemaName, @Param("work_type_id") int work_type_id, @Param("pref_id") String pref_id);

    /**
     * 获取节点信息
     *
     * @param schemaName
     * @param flow_code
     * @param node_id
     * @param page_type
     * @return
     */
    @Select("SELECT i.id,n.id as word_flow_node_id,n.pref_id,n.node_id,n.node_name,n.show_name as node_show_name,i.flow_name,i.show_name as flow_show_name,i.work_type_id,i.relation_type::text,i.work_template_code, " +
            "n.create_time,n.create_user_id,n.node_type,t.type_name,te.work_template_name,c1.name as relation_type_name,deal_type_id::text,deal_role_id,n.remark,i.flow_code,n.page_type  " +
            "FROM ${schema_name}._sc_work_flow_info i " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id " +
            "LEFT JOIN ${schema_name}._sc_work_type t ON i.work_type_id = t.id " +
            "LEFT JOIN ${schema_name}._sc_work_template te ON i.work_template_code=te.work_template_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON i.relation_type::text= c1.code AND c1.data_type = 'relationTypes' " +
            "WHERE n.page_type = #{page_type} AND i.flow_code = #{flow_code} AND n.node_id = #{node_id}")
    Map<String, Object> findNodeInfoWithFlowCode(@Param("schema_name") String schemaName, @Param("flow_code") String flow_code, @Param("node_id") String node_id, @Param("page_type") String page_type);

    /**
     * 根据流程表主键id获取节点表start节点的主键id
     *
     * @param schemaName schema
     * @param id         流程主键id
     * @return
     */
    @Select("select n.* from ${schema_name}._sc_work_flow_info i LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id where n.node_type = 'start' and i.id = #{id}::int")
    Map<String, Object> findWorkFlowNodeByWorkFlowId(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 根据流程表主键id获取节点表start节点的主键id
     *
     * @param schemaName schema
     * @param workTypeId 工单类型id
     * @return
     */
    @Select("select n.* from ${schema_name}._sc_work_flow_info i LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id where n.node_type = 'start' and i.work_type_id = #{workTypeId}::int limit 1")
    Map<String, Object> findWorkFlowNodeByWorkTypeId(@Param("schema_name") String schemaName, @Param("workTypeId") String workTypeId);

    /**
     * 获取流程start节点数据
     *
     * @param schemaName schema
     * @param pref_id    流程id
     * @return
     */
    @Select("SELECT i.id,n.id as word_flow_node_id,n.pref_id,n.node_id,n.node_name,n.show_name as node_show_name,i.flow_name,i.show_name as flow_show_name, " +
            "i.work_type_id,i.relation_type::text,i.work_template_code,n.create_time,n.create_user_id,n.node_type,t.type_name,te.work_template_name, " +
            "c1.name as relation_type_name,deal_type_id::text,deal_role_id,n.remark,i.flow_code,n.page_type " +
            "FROM ${schema_name}._sc_work_flow_info i  " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id  " +
            "LEFT JOIN ${schema_name}._sc_work_type t ON i.work_type_id = t.id  " +
            "LEFT JOIN ${schema_name}._sc_work_template te ON i.work_template_code=te.work_template_code  " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON i.relation_type::text= c1.code AND c1.data_type = 'relationTypes'  " +
            "WHERE n.page_type = 'base' AND i.pref_id = #{pref_id} AND n.node_type=#{node_type} " +
            "ORDER BY n.create_time desc limit 1 ")
    Map<String, Object> findBaseNodeInfoByNodeTypeAndPrefId(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id, @Param("node_type") String node_type);

    /**
     * 通过work_type_id查询流程节点信息
     *
     * @param schemaName
     * @param work_type_id
     * @return
     */
    @Select("SELECT i.id,i.pref_id,i.flow_name,i.show_name as flow_show_name,i.work_type_id,i.relation_type::varchar,i.work_template_code,i.create_time,i.create_user_id,n.remark,n.page_type, " +
            "i.flow_code,n.node_id,n.node_type,n.node_name,n.show_name as node_show_name,t.type_name,te.work_template_name,c1.name as relation_type_name,deal_type_id::text,deal_role_id,n.id as word_flow_node_id " +
            "FROM ${schema_name}._sc_work_flow_info i " +
            "LEFT JOIN ${schema_name}._sc_work_flow_node n ON i.pref_id=n.pref_id " +
            "LEFT JOIN ${schema_name}._sc_work_type t ON i.work_type_id = t.id " +
            "LEFT JOIN ${schema_name}._sc_work_template te ON i.work_template_code=te.work_template_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON i.relation_type::text= c1.code AND c1.data_type = 'relationTypes' " +
            "WHERE n.page_type = 'base' and n.pref_id=n.node_id AND i.work_type_id = #{work_type_id}")
    Map<String, Object> findWorkFlowNodeTemplateInfoByWorkTypeId(@Param("schema_name") String schemaName, @Param("work_type_id") Integer work_type_id);

    @Select("SELECT work_type_id,work_template_code FROM ${schema_name}._sc_work_flow_info ")
    List<Map<String, Object>> findWorkFlowInfoList(@Param("schema_name") String schemaName);

    @Select("SELECT count(1) FROM ${schema_name}._sc_work_flow_node WHERE pref_id = #{pref_id} AND node_id != #{node_id} AND node_type = #{node_type}")
    int findCountNodeTypeByNodeIdAndPrefId(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("pref_id") String pref_id, @Param("node_type") String node_type);

    @Select("SELECT DISTINCT node_id FROM ${schema_name}._sc_work_flow_node WHERE pref_id = #{pm.prefId} AND node_type = #{pm.nodeType} limit 1")
    String findNodeId(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    @Update("UPDATE ${schema_name}._sc_work_flow_node_column_linkage  " +
            "SET node_id = #{node_id} " +
            "WHERE  column_id in (SELECT id FROM ${schema_name}._sc_work_flow_node_column WHERE word_flow_node_id = #{word_flow_node_id}::int)")
    void updateWorkFlowColumnLinkage(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("word_flow_node_id") String word_flow_node_id);

    @Update("UPDATE ${schema_name}._sc_work_flow_node_column_ext " +
            "SET node_id = #{node_id} " +
            "WHERE column_id in (SELECT id FROM ${schema_name}._sc_work_flow_node_column WHERE word_flow_node_id = #{word_flow_node_id}::int)")
    void updateWorkFlowColumnExt(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("word_flow_node_id") String word_flow_node_id);

    @Update("UPDATE ${schema_name}._sc_work_flow_node_column " +
            "SET node_id = #{node_id} " +
            "WHERE word_flow_node_id = #{word_flow_node_id}::int")
    void updateWorkFlowColumn(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("word_flow_node_id") String word_flow_node_id);

    @Update("UPDATE ${schema_name}._sc_work_flow_node " +
            "SET node_id = #{node_id} " +
            "WHERE id = #{word_flow_node_id}::int")
    void updateWorkFlowNodeById(@Param("schema_name") String schemaName, @Param("node_id") String node_id, @Param("word_flow_node_id") String word_flow_node_id);

    @Select(" SELECT wfi.id,wfi.pref_id,wfi.flow_name,wfi.show_name,wfi.work_type_id,wfi.relation_type,wfi.work_template_code," +
            " wfi.create_time,wfi.create_user_id,wfi.flow_code " +
            " FROM ${schema_name}._sc_work_flow_info AS wfi WHERE wfi.pref_id = #{pref_id} ")
    Map<String, Object> findWorkFlowInfoByPrefId(@Param("schema_name") String schemaName, @Param("pref_id") String pref_id);

    @Select(" SELECT t.id,t.node_id,t.field_form_code,t.field_code,t.field_name,t.save_type,t.change_type,t.field_right,t.field_value,t.is_required," +
            " t.is_table_key,t.db_table_type,t.field_remark,t.field_data_base,t.field_view_type,t.field_section_type,t.field_validate_method," +
            " t.create_time,t.create_user_id,t.field_is_show,t.data_order,t.word_flow_node_id" +
            " FROM ${schema_name}._sc_work_flow_node_column AS  t WHERE t.node_id = #{workTemplateCode}")
    List<Map<String, Object>> findColumnListByWorkTemplateCode(@Param("schema_name") String schemaName, @Param("workTemplateCode") String workTemplateCode);

    @Update("<script> " +
            "UPDATE ${schema_name}._sc_work_flow_node_column " +
            "SET data_order = data_order -1 " +
            "WHERE node_id = #{node_id} AND data_order &lt;= #{data_order} AND id != #{id} " +
            "<if test = \"word_flow_node_id!=null\"> " +
            "AND word_flow_node_id = #{word_flow_node_id} " +
            "</if> " +
            "</script>")
    int reduceOrderByData(Map<String, Object> info);

    @Update("<script> " +
            "UPDATE ${schema_name}._sc_work_flow_node_column " +
            "SET data_order = data_order +1 " +
            "WHERE node_id = #{node_id} AND data_order >= #{data_order} AND id != #{id} " +
            "<if test = \"word_flow_node_id!=null\"> " +
            "AND word_flow_node_id = #{word_flow_node_id} " +
            "</if> " +
            "</script>")
    int plusOrderByData(Map<String, Object> info);


    @Update(" <script> " +
            "<foreach collection='flow_list' item='flow' index='index' separator=';'>" +
            " update ${schema_name}._sc_work_flow_info set flow_name = #{flow.flow_name}, show_name = #{flow.flow_show_name} where pref_id = #{flow.pref_id} " +
            " </foreach>" +
            " </script> ")
    int batchUpdateFlow(@Param("schema_name") String schema_name, @Param("flow_list") List<Map<String, Object>> flowList);

    @Update(" <script> " +
            "<foreach collection='node_list' item='node' index='index' separator=';'>" +
            " update ${schema_name}._sc_work_flow_node set node_name = #{node.node_name}, show_name = #{node.node_show_name} where pref_id = #{node.pref_id} AND node_id=#{node.node_id} " +
            " </foreach>" +
            " </script> ")
    int batchUpdateNode(@Param("schema_name") String schema_name, @Param("node_list") List<Map<String, Object>> nodeList);
}
