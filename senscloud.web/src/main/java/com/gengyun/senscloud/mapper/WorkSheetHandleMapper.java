package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * 工单处理
 * User: sps
 * Date: 2018/11/22
 * Time: 下午15:00
 */
@Mapper
public interface WorkSheetHandleMapper {
//    @InsertProvider(type = WorkSheetHandleMapper.WorkSheetHandleMapperProvider.class, method = "insert")
//    void insert(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap, String tableName);
//
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = WorkSheetHandleMapper.WorkSheetHandleMapperProvider.class, method = "query")
//    @Results({@Result(property = "options", column = "options", typeHandler = JsonTypeHandler.class),
//            @Result(property = "table_columns", column = "table_columns", typeHandler = JsonTypeHandler.class),
//            @Result(property = "query_conditions", column = "query_conditions", typeHandler = JsonTypeHandler.class)})
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName,
//                                    @Param("searchKey") String searchKey);
//
//    /**
//     * 查询所有可用的模板名称和编号
//     *
//     * @param schemaName
//     * @param type
//     * @return
//     */
//    @Select("select work_template_code as code, work_template_name as desc, relation_type as relation " +
//            "from ${schema_name}._sc_work_template " +
//            "where isuse = '1' and parent_code ${type} '1' order by work_template_name ")
//    List<Map<String, Object>> queryCodeAndDesc(@Param("schema_name") String schemaName, @Param("type") String type);
//
////    /**
////     * 查询所有可用的模板名称和编号
////     *
////     * @param schemaName
////     * @return
////     */
////    @Select("select id as code, type_name as desc, flow_template_code as relation " +
////            "from ${schema_name}._sc_work_type " +
////            "where isuse = '1' ")
////    List<Map<String, Object>> queryFlowInfo(@Param("schema_name") String schemaName);
//
//    /**
//     * 根据主键查询工单详情数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_works_detail where sub_work_code = #{subWorkCode} ")
//    Map<String, Object> queryById(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
    /**
     * 根据主键查询工单详情业务数据
     *
     * @param schemaName
     * @param subWorkCode
     * @return
     */
    @Select("select sub_work_code, work_code, work_type_id, work_template_code, relation_type, relation_id, title, " +
            "problem_note, problem_img, asset_running_status, fault_type, repair_type, priority_level, status, remark, " +
            "from_code, waiting, bom_app_result, receive_account, receive_time, before_img, result_note, after_img, " +
            "file_ids, comment_list, distribute_time, begin_time, finished_time, audit_time, is_main, fault_number, service_score, repair_code, total_deal_time " +
            "from ${schema_name}._sc_works_detail where sub_work_code = #{subWorkCode} ")
    Map<String, Object> queryBusinessDetailById(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);


//    @Select("<script>" +
//            "select company.company_name as company_name, company.address as company_address, " +
//            "<when test='customerId != null'>" +
//            "customer.short_title as customer_name, customer.address as customer_address," +
//            "</when>" +
//            "detail.work_code as sub_work_code, work.occur_time as occur_time, facility.address as facility_address, work.problem_note as remark, u.username as username," +
//            "u.mobile as phone, u.email as email from " +
//            "${schema_name}._sc_works_detail detail left join ${schema_name}._sc_works work on detail.work_code = work.work_code " +
//            "left join ${schema_name}._sc_facilities facility on facility.id = work.facility_id, " +
//            "<when test='customerId != null'>" +
//            "${schema_name}._sc_facilities customer, " +
//            "</when>" +
//            "public._sc_company company, ${schema_name}._sc_user u " +
//            "where detail.sub_work_code = #{subWorkCode} and company._id = #{companyId} and u.account = #{account} " +
//            "<when test='customerId != null'>" +
//            "and customer.id = #{customerId}" +
//            "</when>" +
//            "</script>")
//    Map<String, Object> queryEmailDetail(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode, @Param("customerId") Long customerId,
//                                         @Param("companyId") Long companyId, @Param("account") String account);
//
//    /**
//     * 获取工单详情（_sc_works表）
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select work_code, create_user_account, status " +
//            "from ${schema_name}._sc_works where work_code = #{workCode} ")
//    Map<String, Object> queryWorksDetailByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主键查询主工单详情业务数据
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select sub_work_code, work_code, work_type_id, work_template_code, relation_type, relation_id, title, " +
//            "problem_note, problem_img, asset_running_status, fault_type, repair_type, priority_level, status, remark, " +
//            "from_code, waiting, bom_app_result, receive_account, receive_time, before_img, result_note, after_img, " +
//            "file_ids, comment_list, distribute_time, begin_time, finished_time, audit_time, is_main, fault_number, service_score, repair_code, total_deal_time  " +
//            "from ${schema_name}._sc_works_detail where work_code = #{workCode} and is_main = 1 ")
//    Map<String, Object> queryMainDetailByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据工单号查询主工单详情业务数据
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_works_detail where work_code = #{workCode} and is_main = 1 ")
//    Map<String, Object> queryMainDetailInfo(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主键查询子工单详情业务数据
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_works_detail where work_code = #{workCode} and is_main = -1 and status = 60 limit 1 ")
//    Map<String, Object> querySubDetailByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主表主键查询主工单详情信息
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select sub_work_code, status from ${schema_name}._sc_works_detail where work_code = #{workCode}")
//    List<Map<String, Object>> querySubWorkCodeListByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主表主键查询工单负责人信息
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select string_agg(receive_account,',') as receive_account from ${schema_name}._sc_works_detail where work_code = #{workCode} and is_main = -1 and status = 60")
//    Map<String, Object> queryReceiveAccountsByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主表主键统计子工单未完成数量
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select count(1) from ${schema_name}._sc_works_detail where work_code = #{workCode} and is_main = -1 and status = 40")
//    int countWaitWorkByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主表主键统计子工单数量
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select count(1) from ${schema_name}._sc_works_detail where work_code = #{workCode} and is_main = -1")
//    int countSubWorkByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//
//    /**
//     * 根据主键查询工单主表数据
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Select("select work_code,work_type_id,work_template_code,pool_id,relation_type,relation_id," +
//            "occur_time,deadline_time,status,remark,create_time,create_user_account,title,problem_note,problem_img," +
//            "plan_code,customer_id,position_code,work_request_code," +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN ltrim(to_char(w.facility_id, '" + SqlConstant.FACILITY_ID_LEN + "')) ELSE w.position_code END AS facility_id " +
//            " from ${schema_name}._sc_works w where work_code = #{workCode} ")
//    Map<String, Object> queryWorkById(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    // LSP工单处理变更-20190529-sps start
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
////    @Select("select * from ${schema_name}._sc_works w, ${schema_name}._sc_works_detail d where w.work_code = d.work_code and d.sub_work_code = #{subWorkCode} ")
////    @Select("select w.customer_id,w.pool_id,w.facility_id,w.occur_time,w.deadline_time,w.create_time,w.create_user_account,d.sub_work_code,d.work_code," +
////            "d.work_type_id,d.work_template_code,d.relation_type,d.relation_id,coalesce(d.title, w.title) as title,COALESCE(d.problem_note, w.problem_note) AS problem_note,d.problem_img,d.asset_running_status,d.fault_type," +
////            "d.repair_type,d.priority_level,d.status,coalesce(d.remark, w.remark) as remark,d.body_property,d.from_code,d.waiting,d.bom_app_result,d.receive_account,d.receive_time," +
////            "d.before_img,d.result_note,d.after_img,d.file_ids,d.comment_list,d.distribute_time,d.begin_time,d.finished_time,d.audit_time,d.is_main," +
////            "d.fault_number " +
////            " from ${schema_name}._sc_works w, ${schema_name}._sc_works_detail d where w.work_code = d.work_code and d.sub_work_code = #{subWorkCode} ")
//    @Select("select w.customer_id,w.pool_id,w.occur_time,w.deadline_time,w.create_time,w.create_user_account,d.sub_work_code,d.work_code," +
//            "d.work_type_id,d.work_template_code,d.relation_type,d.relation_id,coalesce(d.title, w.title) as title,COALESCE(d.problem_note, w.problem_note) AS problem_note,d.problem_img,d.asset_running_status,d.fault_type," +
//            "d.repair_type,d.priority_level,d.status,coalesce(d.remark, w.remark) as remark,d.body_property,d.from_code,d.waiting,d.bom_app_result,d.receive_account,d.receive_time," +
//            "d.before_img,d.result_note,d.after_img,d.file_ids,d.comment_list,d.distribute_time,d.begin_time,d.finished_time,d.audit_time,d.is_main," +
//            "d.fault_number, d.service_score, d.total_deal_time, " +
//            "${field} " +
//            " from ${schema_name}._sc_works w, ${schema_name}._sc_works_detail d where w.work_code = d.work_code and d.sub_work_code = #{subWorkCode} ")
//    Map<String, Object> queryWorkInfoById(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode, @Param("field") String field);
//    // LSP工单处理变更-20190529-sps end
//
//
//    /**
//     * 根据主键查询工单对象，对象类型，PM code等数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select wd.relation_type,wd.relation_id,w.plan_code " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "where wd.sub_work_code = #{subWorkCode} ")
//    Map<String, Object> queryWorkRelationInfoById(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @param tableName
//     * @param key
//     * @return
//     */
//    @UpdateProvider(type = WorkSheetHandleMapper.WorkSheetHandleMapperProvider.class, method = "update")
//    int update(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap, String tableName, String key);
//
//    /**
//     * 更新子工单（未完成数据）
//     *
//     * @param schemaName
//     * @param receiveAccount
//     * @param workCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_works_detail set receive_account=#{receive_account} where work_code = #{workCode} and status < 50 ")
//    int updateWaitWorkDetailByWorkCode(@Param("schema_name") String schemaName, @Param("receive_account") String receiveAccount, @Param("workCode") String workCode);
//
//    /**
//     * 更新子工单状态
//     *
//     * @param schemaName
//     * @param status
//     * @param workCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_works_detail set status = #{status} where work_code = #{workCode} ")
//    int updateWorkDetailByWorkCode(@Param("schema_name") String schemaName, @Param("status") Integer status, @Param("workCode") String workCode);
//
//    /**
//     * 更新主工单子表状态
//     *
//     * @param schemaName
//     * @param status
//     * @param workCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_works_detail set status = #{status} where work_code = #{workCode} and is_main = 1")
//    int updateMainWorkDetailByWorkCode(@Param("schema_name") String schemaName, @Param("status") Integer status, @Param("workCode") String workCode);
//
//    /**
//     * 按条件查询设备信息
//     *
//     * @param schema_name
//     * @param whereString
//     * @return
//     */
////    @Select("select t1.*, t2.title as site_name, t3.category_name as category_name from ${schema_name}._sc_asset t1 " +
////            " LEFT JOIN ${schema_name}._sc_facilities t2 on t1.intsiteid=t2.id " +
////            " LEFT JOIN ${schema_name}._sc_asset_category t3 on t1.category_id=t3.id " +
////            " ${whereString} order by t1.strcode")
//    @Select(" select t._id, t.strcode, t.strname, string_agg(t.site_name, ',') AS site_name, t.enable_time, " +
//            "   t.int_status_name, t.model_name, t.category_name, t.currency, t.asset_currency from ( "
//            + "   SELECT DISTINCT t1._id, t1.strcode, t1.strname, t2.title AS site_name, t1.enable_time, " +
//            "           sas.status as int_status_name, am.model_name, ac.category_name as category_name, " +
//            "           t1.currency, cur.currency_sign as asset_currency "
//            + "     from ${schema_name}._sc_asset t1 "
//            + " LEFT JOIN ${schema_name}._sc_asset_category ac on t1.category_id = ac.id "
//            + " LEFT JOIN ${schema_name}._sc_asset_model am on t1.asset_model_id = am.id "
//            + " LEFT JOIN ${schema_name}._sc_asset_status sas ON t1.intstatus = sas.id "
//            + " LEFT JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = t1._id "
//            + " LEFT JOIN ${schema_name}._sc_facilities t2 ON t2.ID = ao.org_id AND t2.isuse = TRUE AND t2.status > 0 " // 设备组织
//            + " LEFT JOIN ${schema_name}._sc_asset_position ap ON ap.position_code = t1.position_code "
//            + " LEFT JOIN ${schema_name}._sc_asset_position_organization apo ON ap. position_code = apo.position_code "
//            + " LEFT JOIN ${schema_name}._sc_facilities t3 ON t3.ID = apo.org_id AND t3.isuse = TRUE AND t3.status > 0 " // 设备位置组织
//            + " LEFT JOIN ${schema_name}._sc_currency cur ON t1.currency = cur.id "
//            + " ${whereString} ) t " +
//            "group by t._id, t.strcode, t.strname, t.enable_time, t.int_status_name, t.model_name, t.category_name, " +
//            "t.currency, t.asset_currency order by strcode ${pageInfo} ")
//    List<Map<String, Object>> queryAssetList(@Param("schema_name") String schema_name, @Param("whereString") String whereString, @Param("pageInfo") String pageInfo);
//
//    /**
//     * 查找设备资产名称 根据设备编号
//     *
//     * @param schema_name
//     * @param device_code
//     * @return
//     */
//    @Select("select a.*, a._id as relation_id, f.facility_name, f.facility_id, c.short_title as supplier_name, " +
//            "ac.category_name as category_name, a.strname as relation_name, a.strcode as relation_id_text_value, " +
//            "f.facility_name as facility_id_text_value, f.facility_name || a.strname || a.strcode as title " +
//            "from ${schema_name}._sc_asset a " +
//            "left join ${schema_name}._sc_asset_category ac on a.category_id=ac.id  " +
//            "left join ${schema_name}._sc_facilities c on a.supplier=c.id "
//            + " LEFT JOIN ${schema_name}.get_asset_facility_with_code(#{device_code}) f ON f.str_code_rn = a.strcode "
//            + " where a.strcode=#{device_code} and a.intstatus > 0 ${condition} limit 1")
//    Map<String, Object> getAssetCommonDataByCode(@Param("schema_name") String schema_name, @Param("device_code") String device_code, @Param("condition") String condition);

    /**
     * 查找设备资产名称 根据设备编号
     *
     * @param schema_name
     * @param device_code
     * @return
     */
    @Select("SELECT t1.ID AS relation_id,t1.asset_code AS relation_id_text_value,t1.asset_name AS relation_name,t1.asset_model_id,t1.running_status_id, " +
            "t1.status,t1.enable_time,t1.category_id,ac.category_name AS category_name,t1.position_code,ap.position_name " +
            "FROM ${schema_name}._sc_asset t1 "
            + SqlConstant.ASSET_DATA_PRM_SQL +
            "LEFT JOIN ${schema_name}._sc_asset_category ac ON t1.category_id = ac.ID " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON ap.position_code = t1.position_code " +
            "WHERE t1.asset_code = #{device_code} AND t1.status > " + StatusConstant.STATUS_DELETEED)
    Map<String, Object> getAssetCommonDataByCode(@Param("schema_name") String schema_name, @Param("userId") String userId, @Param("device_code") String device_code);
//
//    /**
//     * 查找设备位置 根据设备位置编号
//     *
//     * @param schema_name
//     * @param position_code
//     * @return
//     */
//    @Select("select a.*,a.position_code as _id,a.position_name as strname,a.position_code as strcode, a.position_code as relation_id, f.title as facility_name,f.id as facility_id,f.short_title AS supplier_name from ${schema_name}._sc_asset_position a " +
//            "left join ${schema_name}._sc_asset_position_organization ap on a.position_code=ap.position_code  " +
//            "left join ${schema_name}._sc_facilities f on ap.org_id=f.id "
//            + " where a.position_code=#{position_code} ${condition} limit 1")
//    Map<String, Object> getAssetPositionByCode(@Param("schema_name") String schema_name, @Param("position_code") String position_code, @Param("condition") String condition);
//
//    /**
//     * 查找设备相关任务项
//     *
//     * @param schema_name
//     * @param modelId
//     * @param workTypeId
//     * @return
//     */
//    @Select("SELECT i.* FROM (select task_template_code from ${schema_name}._sc_asset_model m, ${schema_name}._sc_asset_model_task t " +
//            "where m.id = t.model_id and model_id = #{modelId} and work_type_id = #{workTypeId} limit 1) m, " +
//            "${schema_name}._sc_task_item i, ${schema_name}._sc_task_template_item t " +
//            "where i.task_item_code = t.task_item_code and t.task_template_code = m.task_template_code " +
//            "order by i.\"order\"")
//    List<Map<String, Object>> getAssetTaskList(@Param("schema_name") String schema_name, @Param("modelId") Integer modelId, @Param("workTypeId") Integer workTypeId);
//
//    /**
//     * 查找设备位置相关任务项
//     *
//     * @param schema_name
//     * @param position_code
//     * @param workTypeId
//     * @return
//     */
//    @Select("SELECT i.* FROM (select task_template_code from ${schema_name}._sc_asset_position m, ${schema_name}._sc_asset_position_task t " +
//            "where m.position_code = t.position_code and m.position_code = #{position_code} and work_type_id = #{workTypeId} limit 1) m, " +
//            "${schema_name}._sc_task_item i, ${schema_name}._sc_task_template_item t " +
//            "where i.task_item_code = t.task_item_code and t.task_template_code = m.task_template_code " +
//            "order by i.\"order\"")
//    List<Map<String, Object>> getAssetPositionTaskList(@Param("schema_name") String schema_name, @Param("position_code") String position_code, @Param("workTypeId") Integer workTypeId);
//
//    /**
//     * 根据工单明细主键删除数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Delete("delete from ${schema_name}.${tableName} where sub_work_code = #{subWorkCode} ")
//    void deleteBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 根据工单主键删除数据
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Delete("delete from ${schema_name}.${tableName} where work_request_code = #{workRequestCode} ")
//    void deleteByWorkRequestCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("workRequestCode") String workRequestCode);
//
//    /**
//     * 根据请求主键删除数据
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Delete("delete from ${schema_name}.${tableName} where work_code = #{workCode} ")
//    void deleteByWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("workCode") String workCode);
//
//    /**
//     * 根据主键查询备件数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
////    @Select("select array_to_json(array_agg(row_to_json(_sc_work_bom))) as bom_content " +
////            "from ${schema_name}._sc_work_bom where sub_work_code = #{subWorkCode} ")
////    @Select("select array_to_json(array_agg(row_to_json(t))) as bom_content " +
////            " from ( select wb.*, b.bom_name, s.stock_name from ${schema_name}._sc_work_bom wb, ${schema_name}._sc_bom b, ${schema_name}._sc_stock s " +
////            " where wb.bom_code = b.bom_code and wb.material_code = b.material_code and wb.stock_code = s.stock_code and sub_work_code = #{subWorkCode}) t ")
////    Map<String, Object> queryWorkBomBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//    @Select("select array_to_json(array_agg(row_to_json(t))) as bom_content " +
//            " from ( select wb.*, s.stock_name from ${schema_name}._sc_work_bom wb " +
//            "LEFT JOIN ${schema_name}._sc_bom b ON wb.bom_code = b.bom_code and wb.material_code = b.material_code " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON wb.stock_code = s.stock_code " +
//            " where sub_work_code = #{subWorkCode}) t ")
//    Map<String, Object> queryWorkBomBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 根据主键查询备件数据（仅库房信息）
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select wb.*, s.stock_name,b.bom_name from ${schema_name}._sc_work_bom wb " +
//            "left join ${schema_name}._sc_bom b on wb.bom_code = b.bom_code and wb.material_code = b.material_code " +
//            "left join ${schema_name}._sc_stock s on wb.stock_code = s.stock_code " +
//            " where sub_work_code = #{subWorkCode} ")
//    List<Map<String, Object>> queryWbsListBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 根据主键查询备件数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select wb.*, s.stock_name from ${schema_name}._sc_work_bom wb " +
//            "LEFT JOIN ${schema_name}._sc_bom b ON wb.bom_code = b.bom_code and wb.material_code = b.material_code " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON wb.stock_code = s.stock_code " +
//            " where sub_work_code = #{subWorkCode} ")
//    List<Map<String, Object>> queryWorkBomListBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//
//    /**
//     * 根据主键查询工单费用数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select array_to_json(array_agg(row_to_json(t))) as bom_content from (" +
//            "  select fee.*, cur.currency_sign as currency_name " +
//            "  from ${schema_name}._sc_work_fee fee " +
//            "  LEFT JOIN ${schema_name}._sc_currency cur ON fee.currency = cur.id " +
//            " where sub_work_code = #{subWorkCode} " +
//            ") t")
//    Map<String, Object> queryWorkFeeBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//
//    /**
//     * 根据主键查询支援人员数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_work_dutyman_hour where sub_work_code = #{subWorkCode} and is_support = true ")
//    List<Map<String, Object>> queryWorkDutymanHourInfoBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 根据主键查询支援人员数据（请求单）
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_work_dutyman_hour where work_request_code = #{workRequestCode} and is_support = true ")
//    List<Map<String, Object>> queryWorkDutymanHourInfoByWorkRequestCode(@Param("schema_name") String schemaName, @Param("workRequestCode") String workRequestCode);
//
//    /**
//     * 更新支援人员请求单号
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_work_dutyman_hour set work_request_code = #{workRequestCode} where sub_work_code = #{subWorkCode} ")
//    int updateWorkDutymanHourInfoByCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode, @Param("workRequestCode") String workRequestCode);
//
//    /**
//     * 根据主键查询支援人员数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("select u.username from ${schema_name}._sc_work_dutyman_hour h, ${schema_name}._sc_user u " +
//            "where h.operate_account = u.account and sub_work_code = #{subWorkCode} and is_support = true ")
//    List<Map<String, Object>> queryWorkDutymanHourUserBySubWorkCode(@Param("schema_name") String schemaName, @Param("subWorkCode") String subWorkCode);
//
//    /**
//     * 根据主键查询支援人员数据（请求单）
//     *
//     * @param schemaName
//     * @param workRequestCode
//     * @return
//     */
//    @Select("select u.username from ${schema_name}._sc_work_dutyman_hour h, ${schema_name}._sc_user u " +
//            "where h.operate_account = u.account and work_request_code = #{workRequestCode} and is_support = true ")
//    List<Map<String, Object>> queryWorkDutymanHourUserByWorkRequestCode(@Param("schema_name") String schemaName, @Param("workRequestCode") String workRequestCode);
//
//    /**
//     * 更新子工单问题个数
//     *
//     * @param schemaName
//     * @param workCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_works_detail set fault_number=( " +
//            "    select sum(t.fault_number) from ${schema_name}._sc_works_detail t where t.is_main = -1 and work_code = #{workCode}" +
//            ") where work_code = #{workCode} and is_main = 1 ")
//    int updateWorkFaultNumberByWorkCode(@Param("schema_name") String schemaName, @Param("workCode") String workCode);
//
//    /**
//     * 查询所有可用的模板名称和编号
//     *
//     * @param sql
//     * @return
//     */
//    @Insert("${sqlString}")
//    void insertBatch(@Param("sqlString") String sql);
//
//
//    //按位置，周期，找到最近已经执行的工作，和还未开始进行执行的设备（或点巡检）
//
//
//    // LSP工单处理变更-20190529-sps start
//
//    /**
//     * 按条件查询巡检信息
//     *
//     * @param schema_name
//     * @param whereString
//     * @return
//     */
////    @Select("select * from ${schema_name}._sc_inspection_area where is_use=true " +
////            " ${whereString} order by area_code")
//    @Select("select pa.*,pa.position_code as code,pa.position_name as desc,pa.position_code as value,pa.position_name as text from ${schema_name}._sc_asset_position pa" +
//            " left join ${schema_name}._sc_asset_position_organization apo on pa.position_code = apo.position_code " +
//            " left join ${schema_name}._sc_facilities f on apo.org_id = f.ID " +
//            " where f.isuse=true AND (apo.position_code like ${whereString} or f.facility_no like ${whereString}) order by pa.position_code")
//    List<Map<String, Object>> queryInspectionArea(@Param("schema_name") String schema_name, @Param("whereString") String whereString);
//    // LSP工单处理变更-20190529-sps end
//
//
//    //过期一天的工单，进行关闭操作，包含保养、点检、巡检三类单据
//    @Update("update ${schema_name}._sc_works set status=100 where status not in (60,100,900) " +
//            "and work_code in (" +
//            "select w.work_code from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where w.status not in (60,100,900) and now()>w.deadline_time " +
//            "and wt.business_type_id in (2,3,4) " +
//            ")")
//    int closeWorkBillForDue(@Param("schema_name") String schema_name);
//
//    //过期一天的工单详情，进行关闭操作，包含保养、点检、巡检三类单据
//    @Update("update ${schema_name}._sc_works_detail set status=100 where status not in (60,100,900) " +
//            "and work_code in (" +
//            "select w.work_code from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where w.status not in (60,100,900) and now()>w.deadline_time " +
//            "and wt.business_type_id in (2,3,4) " +
//            ")")
//    int closeWorkDetailBillForDue(@Param("schema_name") String schema_name);
//
//    //过期一天的工单详情，进行关闭操作，包含保养、点检、巡检三类单据
//    @Select("select wd.sub_work_code from ${schema_name}._sc_works_detail wd " +
//            "left join (" +
//            "select w.work_code from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "where w.status not in (60,100,900) and now()>w.deadline_time " +
//            "and wt.business_type_id in (2,3,4) " +
//            ") T on wd.work_code=T.work_code " +
//            "where wd.status not in (60,100,900) and T.work_code is not NULL")
//    List<WorkListDetailModel> getNeedCloseWorkDetailBillForDue(@Param("schema_name") String schema_name);
//
//    //按字段新增一条工单数据
//    @Insert("INSERT INTO ${schema_name}._sc_works (work_code, work_type_id, work_template_code, pool_id, facility_id, position_code, " +
//            "relation_type, relation_id, occur_time, deadline_time, status, create_time, create_user_account, " +
//            "title,plan_code,work_request_code) " +
//            "VALUES " +
//            "(#{work_code},#{work_type_id},#{work_template_code},#{pool_id},#{facility_id},#{position_code}," +
//            "#{relation_type},#{relation_id},#{occur_time},#{deadline_time},#{status}," +
//            "#{create_time},#{create_user_account},#{title},#{plan_code},#{work_request_code})")
//    int insertWorks(@Param("schema_name") String schema_name, @Param("work_code") String work_code,
//                    @Param("work_type_id") int work_type_id, @Param("work_template_code") String work_template_code,
//                    @Param("pool_id") String pool_id, @Param("facility_id") Integer facility_id,
//                    @Param("position_code") String position_code, @Param("relation_type") int relation_type,
//                    @Param("relation_id") String relation_id, @Param("occur_time") Timestamp occur_time,
//                    @Param("deadline_time") Timestamp deadline_time, @Param("status") int status,
//                    @Param("create_time") Timestamp create_time, @Param("create_user_account") String create_user_account,
//                    @Param("title") String title, @Param("plan_code") String plan_code, @Param("work_request_code") String work_request_code);
//
//    //按字段新增一条工单详情数据
//    @Insert("INSERT INTO ${schema_name}._sc_works_detail (sub_work_code, work_code, work_type_id, work_template_code, " +
//            "relation_type, relation_id, title, status,body_property, receive_account, receive_time, " +
//            "distribute_time,is_main,begin_time,fault_number,after_img,finished_time) " +
//            "VALUES " +
//            "(#{sub_work_code}, #{work_code}, #{work_type_id}, #{work_template_code}, #{relation_type}, #{relation_id}, " +
//            "#{title}, #{status}, #{body_property}::jsonb, #{receive_account}, #{receive_time}, #{distribute_time}, " +
//            "#{is_main}, #{begin_time}, #{fault_number}, #{after_img},#{finished_time})")
//    int insertWorkDetail(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code,
//                         @Param("work_code") String work_code, @Param("work_type_id") int work_type_id,
//                         @Param("work_template_code") String work_template_code, @Param("relation_type") int relation_type,
//                         @Param("relation_id") String relation_id, @Param("title") String title,
//                         @Param("status") int status, @Param("body_property") String body_property,
//                         @Param("receive_account") String receive_account, @Param("receive_time") Timestamp receive_time,
//                         @Param("distribute_time") Timestamp distribute_time, @Param("is_main") int is_main,
//                         @Param("begin_time") Timestamp begin_time, @Param("fault_number") int fault_number,
//                         @Param("after_img") String after_img, @Param("finished_time") Timestamp finished_time);
//
//    //按字段新增一条工单请求数据
//    @Insert("INSERT INTO ${schema_name}._sc_work_request (work_request_code, work_type_id, work_template_code,facility_id," +
//            "position_code, relation_type, relation_id, title, body_property, problem_note, " +
//            "occur_time,deadline_time,status,remark,create_time,create_user_account,plan_code,source_type) " +
//            "VALUES " +
//            "(#{work_request_code},#{work_type_id},#{work_template_code},#{facility_id},#{position_code}," +
//            "#{relation_type},#{relation_id},#{title},#{body_property}::jsonb,#{problem_note}," +
//            "#{occur_time},#{deadline_time},#{status},#{remark},#{create_time},#{create_user_account},#{plan_code},#{source_type})")
//    int insertWorkRequest(@Param("schema_name") String schema_name, @Param("work_request_code") String work_request_code,
//                          @Param("work_type_id") int work_type_id, @Param("work_template_code") String work_template_code,
//                          @Param("facility_id") Integer facility_id, @Param("position_code") String position_code,
//                          @Param("relation_type") int relation_type, @Param("relation_id") String relation_id,
//                          @Param("title") String title, @Param("body_property") String body_property,
//                          @Param("problem_note") String problem_note, @Param("occur_time") Timestamp occur_time,
//                          @Param("deadline_time") Timestamp deadline_time, @Param("status") int status,
//                          @Param("remark") String remark, @Param("create_time") Timestamp create_time,
//                          @Param("create_user_account") String create_user_account, @Param("plan_code") String plan_code,
//                          @Param("source_type") Integer source_type);
//
//
//    /**
//     * 查询技能王信息
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("select '【' || title || '】' || username || '（${repairCntTitle}' || count_cp || '）' as data_desc " +
//            "from " +
//            "( " +
//            "select t.receive_account, u.username, u.title, count(1) as count_cp " +
//            "from ${schema_name}._sc_works_detail t, ${schema_name}._sc_work_type wt, " +
//            "(" +
//            "select account, username, t2.title from ${schema_name}._sc_user t1, ${schema_name}._sc_facilities t2, " +
//            "( " +
//            "select t1.user_id, min (t2. id) id from ${schema_name}._sc_user_group t1, ${schema_name}._sc_group_org gog, ${schema_name}._sc_facilities t2, " +
//            "(" +
//            "select user_id, min (parentid) as parentid from ${schema_name}._sc_user_group t1,${schema_name}._sc_group_org gog, ${schema_name}._sc_facilities t2 " +
//            "where t1.group_id=gog.group_id and t2.id = gog.org_id group by user_id " +
//            ") t3 " +
//            "where t3.user_id = t1.user_id and t2.parentid = t3.parentid and t1.group_id=gog.group_id and t2.id = gog.org_id group by t1.user_id " +
//            ") t3 where t1. id = t3.user_id and t2. id = t3.id " +
//            ") as u " +
//            "where t.status = 60 and t.is_main = 1 and t.receive_account is not null and t.receive_account <> '-1' " +
//            "and t.work_type_id = wt.id and wt.business_type_id = '1' and t.receive_account = u.account " +
//            "and to_char(t.finished_time, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON + "') " +
//            "group by t.receive_account, u.username, u.title " +
//            "order by count_cp desc " +
//            ") as t limit 3")
//    List<Map<String, Object>> queryMaxRepairList(@Param("schema_name") String schema_name, @Param("repairCntTitle") String repairCntTitle);
//
//    /**
//     * 查询任务狂信息
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("select round(count_cp::numeric/count_total::numeric,2)*count_cp as data_flag, " +
//            "'【' || u.title || '】' || u.username || '（${maintainCntTitle}' || round(count_cp::numeric/count_total::numeric,2)*count_cp || '）' as data_desc " +
//            "from " +
//            "( " +
//            "select t.receive_account, count(1) as count_cp from ${schema_name}._sc_works_detail t, ${schema_name}._sc_work_type wt " +
//            "where t.status = 60 and t.is_main = 1 and t.receive_account is not null and t.receive_account <> '-1' " +
//            "and t.work_type_id = wt.id and wt.business_type_id = '2' and to_char(t.receive_time, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON + "') = to_char(current_timestamp, SqlConstant.SQL_DATE_FMT_YEAR_MON) " +
//            "group by t.receive_account " +
//            ") as cp, " +
//            "(" +
//            "select t.receive_account, count(1) as count_total from ${schema_name}._sc_works_detail t, ${schema_name}._sc_work_type wt " +
//            "where t.status < 900 and t.is_main = 1 and t.receive_account is not null and t.receive_account <> '-1' " +
//            "and t.work_type_id = wt.id and wt.business_type_id = '2' and to_char(t.receive_time, '" + SqlConstant.SQL_DATE_FMT_YEAR_MON + "') = to_char(current_timestamp, SqlConstant.SQL_DATE_FMT_YEAR_MON) " +
//            "group by t.receive_account " +
//            ") as ct, " +
//            "(" +
//            "select account, username, t2.title from ${schema_name}._sc_user t1, ${schema_name}._sc_facilities t2, " +
//            "( " +
//            "select t1.user_id, min (t2. id) id from ${schema_name}._sc_user_group t1, ${schema_name}._sc_group_org gog, ${schema_name}._sc_facilities t2, " +
//            "(" +
//            "select user_id, min (parentid) as parentid from ${schema_name}._sc_user_group t1,${schema_name}._sc_group_org gog, ${schema_name}._sc_facilities t2 " +
//            "where t1.group_id=gog.group_id and t2.id = gog.org_id group by user_id " +
//            ") t3 " +
//            "where t3.user_id = t1.user_id and t2.parentid = t3.parentid and t1.group_id=gog.group_id and t2.id = gog.org_id group by t1.user_id " +
//            ") t3 where t1. id = t3.user_id and t2. id = t3.id " +
//            ") as u " +
//            "where cp.receive_account = ct.receive_account and cp.receive_account = u.account " +
//            "order by data_flag desc")
//    List<Map<String, Object>> queryMaxMaintainList(@Param("schema_name") String schema_name, @Param("maintainCntTitle") String maintainCntTitle);
//
//    // 工单组织
//    @Insert("INSERT INTO ${schema_name}._sc_works_detail_asset_org (sub_work_code, asset_id, facility_id) " +
//            "select * from ( " +
//            "select #{sub_work_code} as sub_work_code, ao.asset_id, f.id as facility_id from ${schema_name}._sc_asset a, ${schema_name}._sc_asset_organization ao, ${schema_name}._sc_facilities f " +
//            "where a._id = ao.asset_id and f.id = ao.org_id and f.isuse = true and f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") and a._id = #{assetId} " +
//            "UNION select #{sub_work_code} as sub_work_code, a._id as asset_id, f.id as facility_id from ${schema_name}._sc_asset a, ${schema_name}._sc_asset_position_organization apo, ${schema_name}._sc_facilities f " +
//            "where a.position_code = apo.position_code and f.id = apo.org_id and f.isuse = true and f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") and a._id = #{assetId}" +
//            "UNION select #{sub_work_code} as sub_work_code, a.position_code as asset_id, f.id as facility_id from ${schema_name}._sc_asset_position a, ${schema_name}._sc_asset_position_organization apo, ${schema_name}._sc_facilities f " +
//            "where a.position_code = apo.position_code and f.id = apo.org_id and f.isuse = true and f.status > 0 AND f.org_type in (" + SqlConstant.FACILITY_INSIDE + ") and a.position_code = #{assetId}" +
//            " ) a")
//    int insertWorkAssetOrg(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("assetId") String assetId);
//
//    // 工单组织插入，在asset为多个时，给is_main = 1 的子工单插入
//    @Insert("INSERT INTO ${schema_name}._sc_works_detail_asset_org (sub_work_code, asset_id, facility_id) " +
//            "VALUES (#{sub_work_code},#{assetId},#{facility_id})")
//    int insertWorkBaseMainAssetOrg(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("assetId") String assetId, @Param("facility_id") int facilityId);
//
//    // 工单负责人组
//    @Insert("INSERT INTO ${schema_name}._sc_works_detail_receive_group (sub_work_code, receive_account, group_id) " +
//            "select * from ( " +
//            "select #{sub_work_code} as sub_work_code, #{receive_account} as receive_account, g.ID as group_id from ${schema_name}._sc_user u, ${schema_name}._sc_user_group ug, ${schema_name}._sc_group g " +
//            "where u.id = ug.user_id and ug.group_id = g.id and u.account = #{receive_account} ) a")
//    int insertWorkReceiveGroup(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("receive_account") String receive_account);
//
//
//    @Update("UPDATE ${schema_name}._sc_works_detail SET comment_list = coalesce(comment_list, '[]') ||#{comment}::jsonb where sub_work_code = #{sub_work_code}")
//    int updateComment(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code, @Param("comment") String comment);
//
//    /**
//     * 实现类
//     */
//    class WorkSheetHandleMapperProvider {
//        /**
//         * 新增数据
//         *
//         * @param schemaName
//         * @param paramMap
//         * @param tableName
//         * @return
//         */
//        public String insert(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap, String tableName) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "." + tableName);
//                Set<String> keys = paramMap.keySet();
//                for (String key : keys) {
//                    if (key.contains("@")) {
//                        String[] columnInfo = key.split("@");
//                        String type = columnInfo[0];
//                        if (!"-1".equals(type)) {
//                            String column = columnInfo[1];
////                            if ("create_time".equals(column)) {
////                                Date now = new Date();
////                                SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
////                                String today = dateFormat.format(now);
////                                paramMap.put(column, Timestamp.valueOf(today));
////                                VALUES(column, "#{t." + column + "}");
////                                continue;
////                            }
//                            if ("varchar".equals(type)) {
//                                VALUES(column, "#{t." + column + "}");
//                            } else if (SqlConstant.DB_TYPE_NORMAL.contains("," + type + ",")) {
//                                VALUES(column, "#{t." + column + "}::" + type);
//                            } else if (SqlConstant.DB_TYPE_TIMESTAMP.contains("," + type + ",")) {
//                                VALUES(column, "TO_TIMESTAMP(#{t." + column + "}, '" + SqlConstant.SQL_DATE_TIME_FMT + "')");
//                            } else if ("null".equals(type)) {
//                                VALUES(column, "null");
//                            } else if ("now".equals(type)) {
//                                VALUES(column, "current_timestamp");
//                            } else if ("loginUser".equals(type)) {
//                                VALUES(column, "#{t.loginUser}");
//                            } else if ("nowKey".equals(type)) {
//                                VALUES(column, "#{t.nowKey}");
//                            }
//                        }
//                    }
//                }
//            }}.toString();
//        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String query(@Param("schema_name") String schemaName,
//                            @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT(" * ");
//                FROM(schemaName + "._sc_works_detail");
//                // 模板名称
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("sub_work_code like CONCAT('%', #{searchKey}, '%')");
//                }
//            }}.toString();
//        }
//
//        /**
//         * 更新
//         *
//         * @param schemaName
//         * @param paramMap
//         * @param tableName
//         * @param key
//         * @return
//         */
//        public String update(@Param("schema_name") String schemaName,
//                             @Param("t") Map<String, Object> paramMap, String tableName, String key) {
//            return new SQL() {{
//                StringBuffer sb = new StringBuffer();
//                Set<String> keys = paramMap.keySet();
//                for (String key : keys) {
//                    sb = doChangeUpdateColumn(key, sb, "t");
//                }
//                if (RegexUtil.isNotNull(sb.toString())) {
//                    UPDATE(schemaName + "." + tableName + " set " + sb.toString().substring(1));
//                    WHERE(" " + key + "=#{t." + key + "} ");
//                }
//            }}.toString();
//        }
//
//        /**
//         * 更新用数据库字段类型转换工具
//         *
//         * @param key
//         * @param sb
//         * @param prefix
//         * @return
//         */
//        public static StringBuffer doChangeUpdateColumn(String key, StringBuffer sb, String prefix) {
//            if (key.contains("@")) {
//                String[] columnInfo = key.split("@");
//                String type = columnInfo[0];
//                String column = columnInfo[1];
//                if (!"-1".equals(type)) {
//                    sb.append(",");
//                    sb.append(column);
//                    if ("varchar".equals(type)) {
//                        sb.append("=#{");
//                        sb.append(prefix);
//                        sb.append(".");
//                        sb.append(column);
//                        sb.append("}");
//                    } else if (SqlConstant.DB_TYPE_NORMAL.contains("," + type + ",")) {
//                        sb.append("=#{");
//                        sb.append(prefix);
//                        sb.append(".");
//                        sb.append(column);
//                        sb.append("}::");
//                        sb.append(type);
//                    } else if (SqlConstant.DB_TYPE_TIMESTAMP.contains("," + type + ",")) {
//                        sb.append("=TO_TIMESTAMP(#{");
//                        sb.append(prefix);
//                        sb.append(".");
//                        sb.append(column);
//                        sb.append("}, '" + SqlConstant.SQL_DATE_TIME_FMT + "')");
//                    } else if ("null".equals(type)) {
//                        sb.append("=null");
//                    } else if ("now".equals(type)) {
//                        sb.append("=current_timestamp");
//                    } else if ("loginUser".equals(type)) {
//                        sb.append("=#{t.loginUser}");
//                    } else if ("nowKey".equals(type)) {
//                        sb.append("=#{t.nowKey}");
//                    }
//                }
//            }
//            return sb;
//        }
//    }
//
//    //查找所有保养完成，且保养时长超过规定最大值的，前一天的保养工单
//    @Select("select d.sub_work_code,d.work_code " +
//            "from ${schema_name}._sc_works_detail d " +
//            "left join ${schema_name}._sc_asset st on d.relation_id=st._id " +
//            "where d.status =60 and st.category_id not in (4,5) " +
//            "and date_part('day',#{day}-d.finished_time)=1 " +
//            "and d.begin_time is not null " +
//            "and (EXTRACT(EPOCH from d.finished_time)-EXTRACT(EPOCH from d.begin_time))/60 > #{max_maintain_minutes} " +
//            "and d.work_type_id = 2 ")
//    List<WorkListDetailModel> getNeedDiscardWorkDetailBill(@Param("schema_name") String schema_name, @Param("day") Timestamp day, @Param("max_maintain_minutes") int max_maintain_minutes);
//
//    //查找所有保养完成，且保养时长超过规定最大值的，前一天的保养工单
//    @Select("select d.sub_work_code,d.work_code " +
//            "from ${schema_name}._sc_works_detail d " +
//            "left join ${schema_name}._sc_asset st on d.relation_id=st._id " +
//            "where d.status =60 and st.category_id in (4,5) " +
//            "and date_part('day',#{day}-d.finished_time)=1 " +
//            "and d.begin_time is not null " +
//            "and (EXTRACT(EPOCH from d.finished_time)-EXTRACT(EPOCH from d.begin_time))/60 > #{max_maintain_minutes} " +
//            "and d.work_type_id = 2 ")
//    List<WorkListDetailModel> getNeedDiscardWorkDetailBillForIT(@Param("schema_name") String schema_name, @Param("day") Timestamp day, @Param("max_maintain_minutes") int max_maintain_minutes);
//
//    //作废工单
//    @Update("update ${schema_name}._sc_works_detail set status=900 where sub_work_code = #{sub_work_code} ")
//    int discardWorkDetailBill(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
//
//    //作废工单
//    @Update("update ${schema_name}._sc_works set status=900 where work_code = #{work_code} ")
//    int discardWorkBill(@Param("schema_name") String schema_name, @Param("work_code") String work_code);
//
//    /**
//     * 根据子工单主键查询工单主表数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Select("SELECT w.work_code,wd.sub_work_code,w.create_time,u.username,u.mobile,f.short_title,wd.result_note,ru.username as receiver,wd.result_note,wd.remark " +
//            " FROM ${schema_name}._sc_works w " +
//            " LEFT JOIN ${schema_name}._sc_works_detail wd ON wd.work_code = w.work_code " +
//            " LEFT JOIN ${schema_name}._sc_facilities f ON f.id = w.facility_id " +
//            " LEFT JOIN ${schema_name}._sc_user u ON u.account = w.create_user_account " +
//            " LEFT JOIN ${schema_name}._sc_user ru ON ru.account = wd.receive_account " +
//            "WHERE wd.sub_work_code = #{sub_work_code} ")
//    Map<String, Object> queryWorkInfoBySubWorkCode(@Param("schema_name") String schemaName, @Param("sub_work_code") String subWorkCode);
//
//    /**
//     * 根据信息获取工单子页面列表
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Select("select wd.sub_work_code, wd.problem_note, a.strcode, a.strname, wd.before_img from ${schema_name}._sc_works_detail wd, ${schema_name}._sc_asset a " +
//            "where wd.relation_id = a._id and wd.work_code = #{t.work_code} and wd.is_main = #{t.search_is_main}::int ")
//    List<Map<String, Object>> querySubPageListByInfo(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap);
//
//    /**
//     * 根据信息获取工单子页面列表
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Select("SELECT wd2.sub_work_code,wd2.problem_note,A.strcode,A.strname,wd2.before_img,wd2.problem_note,wd2.remark,wd2.result_note,wd2.finished_time " +
//            "FROM ${schema_name}._sc_works_detail wd,${schema_name}._sc_works_detail wd2,${schema_name}._sc_asset A " +
//            "WHERE wd.work_code = wd2.work_code AND wd2.relation_id = A._id AND wd.sub_work_code = #{t.sub_work_code} AND wd2.is_main = #{t.search_is_main}:: INT")
//    List<Map<String, Object>> querySubPageListBySubWorkInfo(@Param("schema_name") String schemaName, @Param("t") Map<String, Object> paramMap);
//
//    /**
//     * 查询指定表格的bodyProperty字段中的指定field值
//     * @param schemaName
//     * @param tableName
//     * @param subWorkCode
//     * @param fieldCode
//     * @return
//     */
//    @Select("SELECT j ->> 'fieldValue' FROM ( " +
//            "   SELECT jsonb_array_elements ( T.body_property ) j FROM ${schema_name}.${tableName} T WHERE sub_work_code = '${subWorkCode}' " +
//	        ") T " +
//            "WHERE ( j ->> 'fieldCode' ) = '${fieldCode}' ")
//    String getBodyPropertyFieldValueBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode, @Param("fieldCode") String fieldCode);
//
//    /**
//     * 查询指定表格的bodyProperty字段中的指定field值
//     * @param schemaName
//     * @param tableName
//     * @param subWorkCode
//     * @param fieldCode
//     * @return
//     */
//    @Select("SELECT j ->> '${columnName}' FROM ( " +
//            "   SELECT jsonb_array_elements ( T.body_property ) j FROM ${schema_name}.${tableName} T WHERE sub_work_code = '${subWorkCode}' " +
//            ") T " +
//            "WHERE ( j ->> 'fieldCode' ) = '${fieldCode}' ")
//    String getBodyPropertyFieldColumnBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode, @Param("fieldCode") String fieldCode, @Param("columnName") String columnName);
//
//    /**
//     * 查询指定表格的bodyProperty字段中的多个field值
//     * @param schemaName
//     * @param tableName
//     * @param subWorkCode
//     * @param fieldCodes
//     * @return
//     */
//    @Select("SELECT j ->> 'fieldCode' as code,j ->> 'fieldValue' as value " +
//            "FROM ( " +
//            "        SELECT jsonb_array_elements ( T.body_property ) j FROM ${schema_name}.${tableName} T WHERE sub_work_code = '${subWorkCode}' " +
//            "    ) T " +
//            "WHERE ( j ->> 'fieldCode' ) in (${fieldCodes})")
//    List<Map<String, Object>> getBodyPropertyFieldListValueBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode, @Param("fieldCodes") String fieldCodes);
//
//    /**
//     * 查询is_main类型为-2的子工单关联设备列表
//     * @param schemaName
//     * @param work_code
//     * @return
//     */
//    @Select("SELECT wd.relation_id,a.strname,count(1) as count,u.unit_name " +
//            "FROM ${schema_name}._sc_works_detail wd " +
//            "INNER JOIN ${schema_name}._sc_asset a ON wd.relation_id = a._id " +
//            "LEFT JOIN ${schema_name}._sc_unit u ON a.unit_id = u.id " +
//            "WHERE wd.work_code = #{work_code} AND wd.is_main = "+ Constants.WORKSHEET_IS_MAIN_ASSET+" GROUP BY wd.relation_id,a.strname,u.unit_name ")
//    List<Map<String, Object>> querySwissSubWorkSheetAsset(@Param("schema_name") String schemaName, @Param("work_code") String work_code);

}
