package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 1:43 2019/12/4 0004
 */
@Mapper
public interface ClockInMapper {


//    @Select("SELECT " +
//            "u.username, u.user_code," +
//            "string_agg(ro.name, ',') as department,extract(epoch from (C .sign_out - C .sign_in)) AS time_value , " +
//            "c.* " +
//            "FROM " +
//            "${schema_name}._sc_user_sign_in AS c " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON u.account = c.create_user_account " +
//            "left join ${schema_name}._sc_user_role as r on r.user_id=u.id " +
//            "left join ${schema_name}._sc_role as ro on ro.id=r.role_id " +
//            "WHERE " +
//            "1 = 1 ${condition} " +
//            "GROUP BY c.create_time,u.username,c.id,u.user_code order by c.create_time desc limit ${page} offset ${begin}")
//    List<Map<String ,Object>> query(@Param("schema_name") String schema_name,@Param("condition") String condition,@Param("page") int pageSize,@Param("begin") int begin);
//
//    @Select("SELECT  count(1)  FROM " +
//            "${schema_name}._sc_user_sign_in as c " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON u.account = c.create_user_account " +
//            "WHERE " +
//            "1 = 1 ${condition}")
//    int queryList(@Param("schema_name") String schema_name,@Param("condition")  String condition);
//
//    /**
//     * 查询某一天的打卡记录
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("Select * from ${schema_name}._sc_user_sign_in where 1=1 ${condition} ${limitCondation}")
//    List<Map<String ,Object>> getTodayClockInHistory(@Param("schema_name") String schema_name,@Param("condition") String condition,@Param("limitCondation") String limitCondation);
//
//    /**
//     * 查询过往所有记录
//     * @param schema_name
//     * @param account
//     * @return
//     */
//    @Select("Select to_char(create_time,'" + SqlConstant.SQL_DATE_FMT + "') as create_day   from ${schema_name}._sc_user_sign_in where create_user_account= #{account} GROUP BY create_day")
//    List<Map<String ,Object>> historyClockInDay(@Param("schema_name") String schema_name, @Param("account") String account);
//
//    @Select("select count(1) days from (select to_char(create_time,'" + SqlConstant.SQL_DATE_FMT + "') as create_day from ${schema_name}._sc_user_sign_in where create_user_account= #{account} GROUP BY create_day) as foo")
//    int getClockInDay(@Param("schema_name") String schema_name, @Param("account") String account);
//
//    /**
//     * 签到
//     * @param schema_name
//     * @param account
//     * @param sign_in
//     * @param create_time
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_user_sign_in ( " +
//            "sign_in," +
//            "create_time," +
//            "create_user_account " +
//            ") " +
//            "VALUES ( " +
//            "#{sign_in}::time, " +
//            "#{create_time}, " +
//            "#{account} " +
//            ")")
//    int add(@Param("schema_name")String schema_name,@Param("account") String account,@Param("sign_in") String sign_in,@Param("create_time") Timestamp create_time);
//
//    /**
//     * 更新签退时间
//     * @param schema_name
//     * @param sign_out
//     * @param id
//     * @return
//     */
//    @Update("update ${schema_name}._sc_user_sign_in set sign_out=#{sign_out}::time where id=#{id}::int ")
//    int updateSignOut(@Param("schema_name") String schema_name,@Param("sign_out") String sign_out,@Param("id") String id);

}
