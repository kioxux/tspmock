package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.response.GroupResult;
import com.gengyun.senscloud.response.PositionResult;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 部门
 */
@Mapper
public interface GroupMapper {

    /**
     * 新增部门
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("insert into ${schema_name}._sc_group (sys_group,group_name,group_code,parent_id," +
            "remark,create_user_id,create_time,is_out,org_id ) values (" +
            " '2',#{pm.group_name},#{pm.group_code},#{pm.parent_id}," +
            "#{pm.remark},#{pm.create_user_id},#{pm.create_time},#{pm.is_out},#{pm.org_id}) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertGroup(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除部门
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete("DELETE FROM ${schema_name}._sc_group WHERE id = #{id} ")
    void deleteGroup(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 查询部门详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 部门详情
     */
    @Select(" SELECT g.id,g.org_id,f.title,g.group_name,g.group_code,g.parent_id,g.remark,g.create_user_id,u.account as create_user_account," +
            " g.create_time,g.is_out " +
            " FROM ${schema_name}._sc_group g" +
            " LEFT JOIN ${schema_name}._sc_facilities  f ON g.org_id = f.id" +
            " LEFT JOIN ${schema_name}._sc_user u ON g.create_user_id = u.id " +
            " where g.id = ${id}")
    GroupResult findGroupById(@Param("schema_name") String schema_name, @Param("id") int id);

    /**
     * 查询部门详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 部门详情
     */
    @Select("SELECT g.id,g.group_name,g.group_code,g.parent_id,g.remark,g.create_user_id, u.account as create_user_account,g.create_time,g.is_out " +
            "FROM ${schema_name}._sc_group g " +
            "LEFT JOIN ${schema_name}._sc_user u ON u.id = g.create_user_id " +
            "where g.id = ${id}")
    Map<String, Object> findGroup(@Param("schema_name") String schema_name, @Param("id") int id);


    /**
     * 获取部门列表
     *
     * @param schema_name 入参
     * @return 部门列表
     */
    @Select("SELECT  true AS is_group, g.id,g.group_name,g.group_code,g.remark,g.create_user_id,u.account as create_user_account,g.create_time," +
            "g.is_out , CASE WHEN g.parent_id =0 THEN null ELSE g.parent_id END AS parent_id " +
            "FROM ${schema_name}._sc_group g " +
            "LEFT JOIN ${schema_name}._sc_user u ON u.id = g.create_user_id")
    List<GroupResult> findGroupList(@Param("schema_name") String schema_name);

    /**
     * 获取部门下拉框
     *
     * @param schema_name 入参
     * @return 部门列表
     */
    @Select(" <script>" +
            " SELECT  true AS is_group,g.org_id,f.title, g.id,g.group_name,g.group_code,g.remark,g.create_user_id," +
            " g.create_time,g.is_out , CASE WHEN g.parent_id =0 THEN null ELSE g.parent_id END AS parent_id " +
            " FROM ${schema_name}._sc_group g " +
            " LEFT JOIN ${schema_name}._sc_facilities  f ON g.org_id = f.id" +
            " WHERE g.sys_group = '2' " +
            " <when test='is_out!=null'>" +
            " AND g.id NOT IN  " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach>" +
            " </when>" +
            " <when test='is_out!=null'>" +
            " AND  is_out = #{is_out} " +
            " </when> ORDER BY g.id DESC " +
            " </script>")
    List<GroupResult> findGroupOption(@Param("schema_name") String schema_name, @Param("is_out") Boolean is_out, @Param("id") Integer id, @Param("ids") List<Integer> ids);

    @Select("WITH RECURSIVE re (id, parent_id) AS ( " +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_group t where t.id = #{id}  " +
            " UNION " +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_group t2, re e WHERE e.id = t2.parent_id" +
            " ) select id from re ")
    List<Integer> findGroupIds(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 编辑修改部门
     *
     * @param schema_name 入参
     * @param group       入参
     */
    @Update(" <script>" +
            " update ${schema_name}._sc_group set id = #{g.id} " +
            " <when test='g.group_name!=null'>" +
            " ,group_name = #{g.group_name}" +
            " </when> " +
            " <when test='g.parent_id!=null'>" +
            " ,parent_id = #{g.parent_id}" +
            " </when> " +
            " <when test='g.parent_id==null'>" +
            " ,parent_id = null " +
            " </when> " +
            " <when test='g.group_code!=null'>" +
            " ,group_code=#{g.group_code}" +
            " </when> " +
            " <when test='g.remark!=null'>" +
            " ,remark = #{g.remark}" +
            " </when> " +
            " <when test='g.is_out!=null'>" +
            " ,is_out=#{g.is_out} " +
            " </when> " +
            " <when test='g.org_id!=null'>" +
            " ,org_id=#{g.org_id} " +
            " </when> " +
            " where id = #{g.id}" +
            " </script>")
    void updateGroup(@Param("schema_name") String schema_name, @Param("g") Map<String, Object> group);


    /**
     * 新增岗位
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_position(sys_position,position_name, group_id, remark, create_time, create_user_id)" +
            " VALUES ('2',#{pm.position_name}, #{pm.group_id}, #{pm.remark}, #{pm.create_time}, #{pm.create_user_id}) ")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertPosition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 删除岗位
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete("DELETE FROM ${schema_name}._sc_position WHERE id = #{id} ")
    void deletePosition(@Param("schema_name") String schema_name, @Param("id") Integer id);


    /**
     * 岗位详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 岗位详情
     */
    @Select(" SELECT position_name,id,group_id,remark,create_time,create_user_id" +
            " FROM ${schema_name}._sc_position where id = ${id}")
    PositionResult findPositionById(@Param("schema_name") String schema_name, @Param("id") int id);

    /**
     * 查询岗位详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 岗位详情
     */
    @Select(" SELECT position_name,id,group_id,remark,create_time,create_user_id" +
            " FROM ${schema_name}._sc_position where id = ${id}")
    Map<String, Object> findPosition(@Param("schema_name") String schema_name, @Param("id") int id);

    /**
     * 根据部门查询岗位列表
     *
     * @param schema_name 入参
     * @param group_id    入参
     * @return 岗位列表
     */
    @Select(" SELECT position_name,id,id as positionId, group_id,remark,create_time,create_user_id" +
            " FROM ${schema_name}._sc_position where group_id = ${group_id} AND sys_position = '2' ORDER BY id DESC")
    List<PositionResult> findPositionList(@Param("schema_name") String schema_name, @Param("group_id") int group_id);

    /**
     * 编辑修改岗位
     *
     * @param schema_name 入参
     * @param group       入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_position SET" +
            " id = #{g.id}" +
            " <when test='g.position_name!=null'>" +
            " ,position_name = #{g.position_name}" +
            " </when>" +
            " <when test='g.group_id!=null'>" +
            " ,group_id = #{g.group_id}" +
            " </when>" +
            " <when test='g.remark!=null'>" +
            " ,remark = #{g.remark}  " +
            " </when>" +
            " WHERE id = #{g.id}" +
            " </script>")
    void updatePosition(@Param("schema_name") String schema_name, @Param("g") Map<String, Object> group);


    /**
     * 删除岗位所有角色关联信息
     *
     * @param schema_name 入参
     * @param position_id 入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_position_role WHERE position_id = #{position_id}")
    void deletePositionRole(@Param("schema_name") String schema_name, @Param("position_id") Integer position_id);

    /**
     * 删除岗位所有角色关联信息
     *
     * @param schema_name 入参
     * @param position_id 入参
     * @param role_id     入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_position_role WHERE position_id = #{position_id} AND  role_id = #{role_id} ")
    void deletePositionRoleById(@Param("schema_name") String schema_name, @Param("position_id") Integer position_id,
                                @Param("role_id") String role_id);

    /**
     * 根据部门id获取他的所有父类部门信息
     *
     * @param schema_name 入参
     * @param parent_id   入参
     * @return 所有父类部门信息
     */
    @Select(" SELECT g.id,g.group_name,g.group_code,g.remark,g.create_user_id,u.account as create_user_account,g.create_time," +
            " g.is_out , CASE WHEN g.parent_id =0 THEN null ELSE g.parent_id END AS parent_id " +
            " FROM ${schema_name}._sc_group g " +
            " LEFT JOIN ${schema_name}._sc_user u ON u.id = g.create_user_id " +
            " WHERE g.id = #{parent_id} ")
    List<GroupResult> findParentGroupList(@Param("schema_name") String schema_name, @Param("parent_id") Integer parent_id);

    /**
     * 根据部门id获取他的所有子类部门信息
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 所有子类部门信息
     */
    @Select(" SELECT g.id,g.group_name,g.group_code,g.remark,g.create_user_id,g.create_time," +
            " g.is_out , CASE WHEN g.parent_id =0 THEN null ELSE g.parent_id END AS parent_id " +
            " FROM ${schema_name}._sc_group g WHERE g.parent_id = #{id} ")
    List<GroupResult> findChildrenGroupList(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 根据岗位id查询所有的角色id
     *
     * @param schema_name 入参
     * @param position_id 入参
     * @return 所有的角色id
     */
    @Select(" SELECT r.id,r.description,r.name FROM ${schema_name}._sc_position_role pr " +
            " INNER JOIN ${schema_name}._sc_role as  r ON  r.id = pr.role_id" +
            " WHERE pr.position_id = #{position_id} ")
    List<Map<String, Object>> findRoleIdsByPositionId(@Param("schema_name") String schema_name,
                                                      @Param("position_id") Integer position_id);

    /**
     * 根据岗位id获取他的部门信息
     *
     * @param schema_name 入参
     * @param position_id 入参
     * @return 部门信息
     */
    @Select(" SELECT g.id,g.group_name,g.parent_id,g.remark,g.create_user_id,u.account as create_user_account,g.group_code " +
            " FROM ${schema_name}._sc_group  AS g " +
            " LEFT JOIN ${schema_name}._sc_position AS p ON g.id = p.group_id" +
            " LEFT JOIN ${schema_name}._sc_user u ON u.id = g.create_user_id" +
            " WHERE p.id = #{position_id}")
    GroupResult findGroupByPositionId(@Param("schema_name") String schema_name, @Param("position_id") Integer position_id);

    /**
     * 查询不在这些角色中的角色列表
     *
     * @param schema_name   入参
     * @param position_id   入参
     * @param keywordSearch 入参
     * @return 不在这些角色中的角色列表
     */
    @Select(" <script>" +
            " SELECT id,name,description  FROM ${schema_name}._sc_role WHERE id NOT IN  " +
            " (SELECT role_id FROM ${schema_name}._sc_position_role WHERE position_id = #{position_id} )" +
            " <when test='keywordSearch!=null'>" +
            " OR name LIKE  CONCAT('%', #{keywordSearch}, '%') " +
            " </when> " +
            " </script>")
    List<Map<String, Object>> findRolesNotInPosition(@Param("schema_name") String schema_name,
                                                     @Param("position_id") Integer position_id,
                                                     @Param("keywordSearch") String keywordSearch);


    /**
     * 根据用户id获取所有的岗位列表
     *
     * @param schema_name 入参
     * @param userId      入参
     * @return 岗位列表
     */
    @Select(" SELECT position_name,id,group_id,remark,create_time,create_user_id" +
            " FROM ${schema_name}._sc_position p " +
            " LEFT JOIN ${schema_name}._sc_user_position up ON p.id = up.position_id " +
            " WHERE up.user_id = #{userId} AND p.sys_position = '2'")
    List<PositionResult> findByUserId(@Param("schema_name") String schema_name, @Param("userId") String userId);

    @Select("SELECT g.group_name , g.id FROM ${schema_name}._sc_group g ORDER BY g.id ")
    @MapKey("group_name")
    Map<String, Map<String, Object>> findGroupListMap(@Param("schema_name") String schemaName);

    @Select("SELECT g.group_name || '/' || p.position_name as position_name ,p.id " +
            "FROM  ${schema_name}._sc_position p  " +
            "INNER JOIN ${schema_name}._sc_group G ON g.id = p.group_id " +
            "ORDER BY p.id ")
    @MapKey("position_name")
    Map<String, Map<String, Object>> findPositionListMap(@Param("schema_name") String schemaName);
}
