package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 厂商管理
 */
@Mapper
public interface FacilitiesMapper {
    /**
     * 新增厂商
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert(" INSERT into ${schema_name}._sc_facilities ( title,parent_id,create_time,location,inner_code,address," +
            " layer_path,is_use,short_title,create_user_id,remark,currency_id,org_type_id)  VALUES  " +
            " (#{pm.title},#{pm.parent_id}::int,current_timestamp,#{pm.location} :: point,#{pm.inner_code}," +
            " #{pm.address},#{pm.layer_path},#{pm.is_use},#{pm.short_title},#{pm.create_user_id},#{pm.remark}," +
            " #{pm.currency_id}::int,#{pm.org_type_id}::int)")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void InsertFacilities(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据id查询厂商详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 厂商详情
     */
    @Select(" SELECT id,title,parent_id,create_time,location,org_type_id::varchar,inner_code,address," +
            " layer_path,short_title,create_user_id,remark,currency_id" +
            " FROM  ${schema_name}._sc_facilities f WHERE f.id = #{id} ")
    Map<String, Object> findByID(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 根据编码查询厂商详情
     *
     * @param schema_name 入参
     * @param code        入参
     * @return 厂商详情
     */
    @Select(" SELECT id,title,parent_id,create_time,location,org_type_id,inner_code,address," +
            " layer_path,is_use,short_title,create_user_id,remark,currency_id" +
            " FROM  ${schema_name}._sc_facilities f WHERE f.status > " + StatusConstant.STATUS_DELETEED + " AND f.inner_code = #{code} LIMIT 1")
    Map<String, Object> findByCode(@Param("schema_name") String schema_name, @Param("code") String code);

    /**
     * 根据名称查询厂商详情
     *
     * @param schema_name 入参
     * @param title       入参
     * @return 厂商详情
     */
    @Select(" SELECT id,title,parent_id,create_time,location,org_type_id,inner_code,address," +
            " layer_path,is_use,short_title,create_user_id,remark,currency_id" +
            " FROM  ${schema_name}._sc_facilities f WHERE  f.status > " + StatusConstant.STATUS_DELETEED + " AND f.title = #{title} LIMIT 1 ")
    Map<String, Object> findByTitle(@Param("schema_name") String schema_name, @Param("title") String title);

    /**
     * 根据名称查询厂商详情
     *
     * @param schema_name 入参
     * @param short_title 入参
     * @return 厂商详情
     */
    @Select(" SELECT id,title,parent_id,create_time,location,org_type_id,inner_code,address," +
            " layer_path,is_use,short_title,create_user_id,remark,currency_id" +
            " FROM  ${schema_name}._sc_facilities f WHERE  f.status > " + StatusConstant.STATUS_DELETEED + " AND f.short_title = #{short_title} LIMIT 1 ")
    Map<String, Object> findByShortTitle(@Param("schema_name") String schema_name, @Param("short_title") String short_title);

    /**
     * 根据查询厂商列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 厂商列表
     */
    @Select(" <script>" +
            " SELECT f.id,f.title,f.parent_id,f.create_time,f.location,f.org_type_id,f.inner_code," +
            " f.address,f.layer_path,f.is_use,f.short_title,f.create_user_id,u.account AS create_user_account," +
            " f.remark,f.currency_id" +
            " FROM  ${schema_name}._sc_facilities f" +
            " LEFT JOIN ${schema_name}._sc_user u ON f.create_user_id = u.id " +
            " WHERE f.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='pm.isUseSearch!=null and pm.isUseSearch!=\" \" '> " +
            " AND  f.is_use = #{pm.isUseSearch} " +
            " </when> " +
            " <when test='pm.orgTypeSearch!=null and pm.orgTypeSearch!=\" \"'> " +
            " AND  f.org_type_id IN (${pm.orgTypeSearch}) " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( f.title LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR f.inner_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR f.short_title LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> ORDER BY f.create_time DESC  ${pm.pagination} " +
            " </script>")
    List<Map<String, Object>> findFacilitiesList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据查询厂商列表数量
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 厂商列表
     */
    @Select(" <script>" +
            " SELECT count(f.id)" +
            " FROM  ${schema_name}._sc_facilities f" +
            " WHERE f.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='pm.isUseSearch!=null and pm.isUseSearch!=\" \" '> " +
            " AND  f.is_use  = #{pm.isUseSearch} " +
            " </when> " +
            " <when test='pm.orgTypeSearch!=null and pm.orgTypeSearch!=\" \"'> " +
            " AND  f.org_type_id IN (${pm.orgTypeSearch}) " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( f.title LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR f.inner_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR f.short_title LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " </script>")
    Integer findFacilitiesListCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新厂商管理
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_facilities " +
            " SET id=#{pm.id}::int " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesTop\" and  pm.title!=null'> " +
            " ,title=#{pm.title}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.parent_id!=null'> " +
            " ,parent_id=#{pm.parent_id}::int" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.org_type_id!=null'> " +
            " ,org_type_id=#{pm.org_type_id}::int" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesTop\" and pm.inner_code!=null'> " +
            " ,inner_code=#{pm.inner_code}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,address=#{pm.address}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.layer_path!=null'> " +
            " ,layer_path=#{pm.layer_path}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.facility_no!=null'> " +
            " ,facility_no=#{pm.facility_no}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.location!=null'> " +
            " ,location=#{pm.location} :: point" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.short_title!=null'> " +
            " ,short_title=#{pm.short_title}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" '> " +
            " ,remark=#{pm.remark}" +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.currency_id!=null'> " +
            " ,currency_id=#{pm.currency_id}::int " +
            " </when> " +
            " <when test=' pm.sectionType!=null and pm.sectionType==\"facilitiesBase\" and pm.is_use!=null'> " +
            " ,is_use=#{pm.is_use}::int " +
            " </when> " +
            " where id=#{pm.id}::int" +
            " </script>")
    void updateFacilities(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 启用禁用厂商
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_facilities " +
            " SET is_use=#{pm.is_use} " +
            " where id=#{pm.id}::int" +
            " </script>")
    void changeIsUseFacilities(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除厂商
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Update(" UPDATE  ${schema_name}._sc_facilities  SET status = " + StatusConstant.STATUS_DELETEED + " WHERE id = #{id}")
    void deleteFacilities(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 新增厂商附件
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert(" INSERT INTO ${schema_name}._sc_organization_doc (file_id, org_id, create_user_id, create_time) VALUES" +
            "(#{pm.file_id}::int,#{pm.org_id}::int,#{pm.create_user_id},current_timestamp)")
    void insertOrganizationDoc(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除厂商附件
     *
     * @param schema_name 入参
     */
    @Delete(" DELETE FROM ${schema_name}._sc_organization_doc WHERE file_id = #{file_id} AND org_id = #{org_id}")
    void deleteOrganizationDoc(@Param("schema_name") String schema_name, @Param("org_id") Integer org_id, @Param("file_id") Integer file_id);

    /**
     * 获取厂商附件列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 厂商附件列表
     */
    @Select(" <script>" +
            " SELECT od.remark,f.file_name,od.org_id,od.file_id,f.file_type_id,Round(f.file_size/1024,4) AS file_size,f.file_original_name,f.file_url,f.file_category_id," +
            " to_char(f.create_time, '" + SqlConstant.SQL_DATE_FMT + "') AS create_time,f.create_user_id,u.user_name AS create_user_name FROM ${schema_name}._sc_organization_doc AS od" +
            " LEFT JOIN ${schema_name}._sc_files AS f ON od.file_id = f.id" +
            " LEFT JOIN ${schema_name}._sc_user AS u ON u.id = f.create_user_id" +
            " WHERE od.org_id = #{pm.org_id}::int" +
            " <when test='pm.keywordSearch!=null'> " +
            " and f.file_original_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " </when> " +
            " </script>")
    List<Map<String, Object>> findOrganizationDocList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 新增厂商联系人
     */
    @Insert(" INSERT INTO ${schema_name}._sc_organization_contact(org_id, contact_name,contact_mobile,contact_email,wei_xin,create_user_id, create_time)" +
            " VALUES (#{pm.org_id}::int, #{pm.contact_name},#{pm.contact_mobile}, #{pm.contact_email},#{pm.wei_xin},#{pm.create_user_id},current_timestamp )")
    void insertOrganizationContact(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除厂商联系人
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete(" DELETE FROM   ${schema_name}._sc_organization_contact WHERE ID = #{id} ")
    void deleteOrganizationContact(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 查询厂商联系人列表
     */
    @Select(" <script>" +
            " SELECT id,org_id, contact_name, contact_mobile,contact_email, wei_xin" +
            " FROM ${schema_name}._sc_organization_contact " +
            " WHERE org_id = #{pm.org_id}::int" +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( contact_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR contact_mobile LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR contact_email LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR wei_xin LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " ORDER BY id" +
            " </script> ")
    List<Map<String, Object>> findOrganizationContactList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 厂商联系人详情
     */
    @Select(" SELECT id,org_id, contact_name, contact_mobile,contact_email, wei_xin" +
            " FROM ${schema_name}._sc_organization_contact " +
            " WHERE id = #{id}")
    Map<String, Object> findOrganizationContactInfo(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 编辑厂商联系人
     */
    @Select(" SELECT id,org_id, contact_name, contact_mobile,contact_email, wei_xin" +
            " FROM ${schema_name}._sc_organization_contact " +
            " WHERE id = #{id}")
    /**
     * 更新厂商管理
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_organization_contact " +
            " SET id=#{pm.id}::int " +
            " <when test=' pm.contact_name!=null '> " +
            " ,contact_name=#{pm.contact_name}" +
            " </when> " +
            " <when test=' pm.contact_mobile!=null '> " +
            " ,contact_mobile= #{pm.contact_mobile}" +
            " </when> " +
            " <when test=' pm.contact_email!=null '> " +
            " ,contact_email=#{pm.contact_email}" +
            " </when> " +
            " <when test=' pm.wei_xin!=null '> " +
            " ,wei_xin=#{pm.wei_xin}" +
            " </when> " +
            " where id =#{pm.id}::int" +
            " </script>")
    void updateOrganizationContact(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 根据id列表获取厂商名称列表
     *
     * @param schema_name 入参
     * @param ids         入参
     * @return 厂商名称列表
     */
    @Select(" <script>" +
            " SELECT title" +
            " FROM  ${schema_name}._sc_facilities f" +
            " WHERE  f.id IN  " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " </script>")
    List<String> findNamesByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 根据id列表批量删除厂商
     *
     * @param schema_name 入参
     * @param ids         入参
     */
    @Select(" <script>" +
            " DELETE FROM  ${schema_name}._sc_facilities f" +
            " WHERE  f.id IN  " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " </script>")
    void deleteByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 获取外部组织
     *
     * @param schemaName
     * @return
     */
    @Select("SELECT a.id, a.short_title FROM ${schemaName}._sc_facilities a where org_type_id in (" + SqlConstant.FACILITY_OUTSIDE + ") ")
    @MapKey("short_title")
    Map<String, Map<String, Object>> queryOuterSuppliesList(@Param("schemaName") String schemaName);

    /**
     * 供应商
     *
     * @param schemaName
     * @return
     */
    @Select("SELECT a.id, a.short_title FROM ${schemaName}._sc_facilities a where org_type_id in (" + SqlConstant.FACILITY_SUPPLIER + ") ")
    @MapKey("short_title")
    Map<String, Map<String, Object>> queryOuterSupplierList(@Param("schemaName") String schemaName);

    /**
     * 制造商
     *
     * @param schemaName
     * @return
     */
    @Select("SELECT a.id, a.short_title FROM ${schemaName}._sc_facilities a where org_type_id in (" + SqlConstant.FACILITY_MANUFACTURER + ") ")
    @MapKey("short_title")
    Map<String, Map<String, Object>> queryOuterManufacturerList(@Param("schemaName") String schemaName);

    /**
     * 查询供应商
     *
     * @param schema_name
     * @return
     */
    @Select("SELECT a.short_title as desc,a.parent_id as parentid,a.id as code, a.id::varchar as value, a.short_title as text FROM " +
            "${schema_name}._sc_facilities a LEFT JOIN " +
            "${schema_name}._sc_facilities b ON a.parent_id=b.id and b.is_use='1' and b.status > 0 AND b.org_type_id in (3,4,5,6 ) " +
            "WHERE a.is_use='1' and a.status > 0 and a.org_type_id in (3,4,5,6 ) " +
            "ORDER BY a.id,a.parent_id desc")
    @MapKey("value")
    Map<String, Map<String, Object>> queryCustomerFacilities(@Param("schema_name") String schema_name);

    /**
     * 根据id列表批量删除厂商,更改状态
     *
     * @param schema_name 入参
     * @param ids         入参
     */
    @Update(" <script>" +
            " UPDATE  ${schema_name}._sc_facilities f " +
            " SET status = " +StatusConstant.STATUS_DELETEED  +
            " WHERE  f.id IN  " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> " +
            " </script>")
    void deleteToStatusByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);
}
