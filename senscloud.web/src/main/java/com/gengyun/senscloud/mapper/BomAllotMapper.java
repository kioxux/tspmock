package com.gengyun.senscloud.mapper;

/**
 * 备件入库
 */
public interface BomAllotMapper {
//
//    /**
//     * 备件调拨列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(DISTINCT ba.transfer_code) " +
//            "from ${schema_name}._sc_bom_allocation as ba " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON ba.from_stock_code = bs.stock_code  " +
//            "left JOIN (" +
//            "select distinct st.stock_code,st.stock_name from ${schema_name}._sc_stock st " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org on org.stock_code=st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg on sg.stock_code=st.stock_code " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            ") s on bs.stock_code = s.stock_code " +
//            "left join ${schema_name}._sc_stock as sb on sb.stock_code=ba.to_stock_code " +
//            "left join ${schema_name}._sc_status as sta on sta.id=ba.status " +
//            "left join ${schema_name}._sc_user as u on u.account=ba.create_user_account  " +
//            "where 1=1 ${condition} and s.stock_code is not null ")
//    int countBomAllotList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition);
//
//    /**
//     * 查找备件调拨列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//
//    @Select("select DISTINCT ba.transfer_code,sb.stock_name as to_stock_name,s.stock_name as from_stock_name,sta.status as status_name,u.username,ba.from_stock_code, " +
//            "ba.to_stock_code,ba.send_file_ids,ba.receive_file_ids,ba.status,ba.remark,ba.create_time,ba.create_user_account,ba.body_property,brt.quantity_total " +
//            "from ${schema_name}._sc_bom_allocation as ba " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON ba.from_stock_code = bs.stock_code  " +
//            "left JOIN (" +
//            "select distinct st.stock_code,st.stock_name from ${schema_name}._sc_stock st " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org on org.stock_code=st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg on sg.stock_code=st.stock_code " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            ") s on bs.stock_code = s.stock_code " +
//            "LEFT JOIN ( SELECT transfer_code,sum(quantity) as quantity_total " +
//            "FROM ${schema_name}._sc_bom_allocation_detail group by transfer_code" +
//            ") brt ON brt.transfer_code = ba.transfer_code " +
//            "left join ${schema_name}._sc_stock as sb on sb.stock_code=ba.to_stock_code " +
//            "left join ${schema_name}._sc_status as sta on sta.id=ba.status " +
//            "left join ${schema_name}._sc_user as u on u.account=ba.create_user_account  " +
//            "where 1=1 ${condition} and s.stock_code is not null " +
//            "order by ba.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getBomAllotList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据主键查询工单请求数据
//     *
//     * @param schemaName
//     * @param inCode
//     * @return
//     */
//    @Select("select w.* from ${schema_name}._sc_bom_allocation w " +
//            "where w.transfer_code = #{inCode} ")
//    Map<String, Object> queryBomAllotById(@Param("schema_name") String schemaName, @Param("inCode") String inCode);
//
//    @Insert("insert into ${schema_name}._sc_bom_allocation (transfer_code,from_stock_code,to_stock_code,send_file_ids,receive_file_ids,status,remark,create_time,create_user_account,body_property) " +
//            "values (#{allot.transfer_code},#{allot.from_stock_code},#{allot.to_stock_code},#{allot.send_file_ids},#{allot.receive_file_ids},#{allot.status}::int,#{allot.remark},#{allot.create_time}::timestamp,#{allot.create_user_account},#{allot.body_property}::jsonb)")
//    int insert(@Param("schema_name") String schemaName, @Param("allot") Map<String, Object> businessInfo);
//
//    @Update("update ${schema_name}._sc_bom_allocation set " +
//            "from_stock_code = #{allot.from_stock_code}," +
//            "to_stock_code = #{allot.to_stock_code}," +
//            "send_file_ids = #{allot.send_file_ids}," +
//            "receive_file_ids = #{allot.receive_file_ids}," +
//            "remark = #{allot.remark}," +
//            "body_property = #{allot.body_property}::jsonb " +
//            "where transfer_code = #{allot.transfer_code}")
//    int update(@Param("schema_name") String schemaName, @Param("allot") Map<String, Object> businessInfo);
//
//    /**
//     * @param schemaName
//     * @param inCode
//     * @return
//     * @Description：调出备件附件
//     */
//    @Update("update ${schema_name}._sc_bom_allocation set send_file_ids=#{send_file_ids},body_property=#{body_property}::jsonb where transfer_code = #{inCode} ")
//    int updateBomAllotSendStatus(@Param("schema_name") String schemaName, @Param("send_file_ids") String send_file_ids, @Param("body_property") String body_property, @Param("inCode") String inCode);
//
//    /**
//     * @param schemaName
//     * @param inCode
//     * @return
//     * @Description：接受备件附件
//     */
//    @Update("update ${schema_name}._sc_bom_allocation set receive_file_ids=#{receive_file_ids},status = #{status}::int,body_property=#{body_property}::jsonb where transfer_code = #{inCode} ")
//    int updateBomAllotReceiveStatus(@Param("schema_name") String schemaName, @Param("receive_file_ids") String receive_file_ids, @Param("status") int status, @Param("body_property") String body_property, @Param("inCode") String inCode);
//
//    @Update("update ${schema_name}._sc_bom_allocation_detail set quantity=#{quantity}::int where transfer_code=#{transfer_code} and bom_code=#{bom_code} and material_code=#{material_code}")
//    int updateBomDetailInfoQuantity(@Param("schema_name") String schemaName, @Param("bom_code") String bom_code, @Param("transfer_code") String transfer_code, @Param("material_code") String material_code, @Param("quantity") int quantity);
//
//    /**
//     * @param schema_name
//     * @param body_property
//     * @param inCode
//     * @return
//     * @Description:更新备件调拨备件
//     */
//    @Update("update ${schema_name}._sc_bom_allocation set body_property=#{body_property}::jsonb,status = #{status}::int where transfer_code = #{inCode}")
//    int updatebody(@Param("schema_name") String schema_name, @Param("status") int status, @Param("body_property") String body_property, @Param("inCode") String inCode);
//
//    @Update("update ${schema_name}._sc_bom_allocation set status = #{status}::int where transfer_code = #{inCode}")
//    int updateStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("inCode") String inCode);
//
//    /**
//     * 功能：删除备件调拨编号关联备件详情信息，重新修改
//     *
//     * @param schema_name
//     * @param transfer_code
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_bom_allocation_detail where transfer_code=#{transfer_code}")
//    int deleAllotDetail(@Param("schema_name") String schema_name, @Param("transfer_code") String transfer_code);
//
//    /**
//     * @param schema_name
//     * @param bom_code
//     * @param stock_code
//     * @return
//     * @Description:备件库房中备件的显存数据和位置。
//     */
//    @Select("select * from ${schema_name}._sc_bom_stock where bom_code=#{bom_code} and stock_code=#{stock_code} and material_code=#{material_code}")
//    Map<String, Object> countBomStockQuantity(@Param("schema_name") String schema_name, @Param("bom_code") String bom_code, @Param("stock_code") String stock_code, @Param("material_code") String material_code);
//
//    @Select("select * from ${schema_name}._sc_bom_allocation_detail where transfer_code=#{transfer_code} ")
//    List<Map<String, Object>> selectBomAllotDetail(@Param("schema_name") String schema_name, @Param("transfer_code") String transfer_code);
//
//    /**
//     * @param schema_name
//     * @param bom_code
//     * @param stock_code
//     * @param quantity
//     * @return
//     * @Description:更新库存备件信息
//     */
//    @Update("update ${schema_name}._sc_bom_stock set quantity=quantity-#{quantity}::int where bom_code=#{bom_code} and stock_code=#{stock_code}  and material_code=#{material_code}")
//    int updatBomStockFromQuuantityCut(@Param("schema_name") String schema_name, @Param("bom_code") String bom_code, @Param("stock_code") String stock_code, @Param("quantity") int quantity, @Param("material_code") String material_code);
//
//    //调入库存新增
//    @Update("update ${schema_name}._sc_bom_stock set quantity=quantity+#{quantity}::int where bom_code=#{bom_code} and stock_code=#{stock_code} and material_code=#{material_code}")
//    int updatBomStockFromQuuantityAdd(@Param("schema_name") String schema_name, @Param("bom_code") String bom_code, @Param("stock_code") String stock_code, @Param("quantity") int quantity, @Param("material_code") String material_code);
//
//    @Insert("insert into ${schema_name}._sc_bom_stock (bom_code,quantity,stock_code,material_code,security_quantity,max_security_quantity) values (#{bom_code},#{quantity}::int,#{stock_code},#{material_code},0,0)")
//    int inertBomStock(@Param("schema_name") String schema_name, @Param("bom_code") String bom_code, @Param("stock_code") String stock_code, @Param("quantity") int quantity, @Param("material_code") String material_code);
}
