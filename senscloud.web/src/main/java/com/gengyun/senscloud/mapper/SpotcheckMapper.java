package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SpotcheckMapper {
//
//    //以下代码根据小程序需求，写的，姚志军 2018-04-23
//
//    /**
//     * 查询点检项
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_spotcheck_item  " +
//            " order by \"order\"")
//    List<SpotcheckItemData> SpotCheckItemList(@Param("schema_name") String schema_name);
//
//    //查找单条点检单据,根据点检编号
//    @Select("select r.*,u.username as spot_account_name," +
//            "s.status as status_name,ft.title as facility_name,a.area_name " +
//            " from ${schema_name}._sc_spotcheck r " +
//            " left join ${schema_name}._sc_inspection_area a on r.area_code=a.area_code " +
//            " left join ${schema_name}._sc_user u on r.spot_account=u.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            "where spot_code=#{spot_code} ")
//    SpotcheckData getSpotCheckInfo(@Param("schema_name") String schema_name, @Param("spot_code") String spotCode);
//
//    //插入点检
//    @Insert("INSERT INTO ${schema_name}._sc_spotcheck(spot_code,spot_account, status, fault_number, facility_id, " +
//            "remark, spot_time, spot_result, area_code, begin_time) VALUES " +
//            "(#{s.spot_code},#{s.spot_account},#{s.status}, #{s.fault_number}," +
//            "#{s.facility_id}, #{s.remark},#{s.spot_time},#{s.spot_result}::jsonb,#{s.area_code},#{s.begin_time})")
//    int insertSpotCheckData(@Param("schema_name") String schema_name, @Param("s") SpotcheckData spotcheckData);
//
//
//    /**
//     * 查询点检表最新开始的日期，借用实体InspectionListAppResult
//     */
//    @Select("SELECT distinct to_char(ins.spot_time,'" + SqlConstant.SQL_DATE_FMT + "') as insdate " +
//            " FROM ${schema_name}._sc_spotcheck ins " +
//            " where ${condition} " +
//            " group by ins.spot_time" +
//            " order by insdate desc" +
//            " limit ${page} offset ${begin} ")
//    List<SpotCheckGroupByDateResponse> spotListAppResult(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据日期查询list
//     */
//    @Select("SELECT ins.spot_code, ins.spot_account, ins.status, ins.fault_number, ins.facility_id, ins.spot_time, ins.area_code, ins.begin_time," +
//            "u.username as spot_account_name," +
//            "s.status as status_name,ft.title as facility_name,a.area_name " +
//            " from ${schema_name}._sc_spotcheck ins " +
//            " left join ${schema_name}._sc_status s on ins.status=s.id " +
//            " left join ${schema_name}._sc_inspection_area a on ins.area_code=a.area_code  " +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join ${schema_name}._sc_user u on ins.spot_account=u.account " +
//            " where to_char(ins.spot_time,'" + SqlConstant.SQL_DATE_FMT + "') ='${insdate}' " +
//            " and ${condition} " +
//            " order by ins.spot_time desc "+
//            " limit ${page} offset ${begin} " )
//    List<SpotcheckData> SpotCheckDataList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("insdate") String insdate, @Param("page") int pageSize, @Param("begin") int begin);

}
