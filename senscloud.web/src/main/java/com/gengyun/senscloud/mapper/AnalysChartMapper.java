package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnalysChartMapper {
//
//    //位置维修时效、故障率、保养完成率、保养效率的图表分析
//    @Select("select * from (select parent_title,COALESCE(sum(repairDeviceCount),0) as repairDeviceCount,COALESCE(sum(deviceCount),0) as deviceCount," +
//            "(case when COALESCE(sum(deviceCount),0)=0 then 0 else COALESCE(sum(repairDeviceCount),0)/COALESCE(sum(deviceCount),0) end) as repairTimesPerPercent," +
//            "COALESCE(SUM(finishedMaintainTimes),0) as finishedMaintainTimes,COALESCE(SUM(totalMaintainMinute),0) as totalMaintainMinute," +
//            "COALESCE(SUM(noFinishedMaintainTimes),0) as noFinishedMaintainTimes," +
//            "(case when (COALESCE(sum(finishedMaintainTimes),0)+COALESCE(sum(noFinishedMaintainTimes),0))=0 then 0 else COALESCE(sum(finishedMaintainTimes),0)/(COALESCE(sum(finishedMaintainTimes),0)+COALESCE(sum(noFinishedMaintainTimes),0)) end) as maintainFinishRate," +
////            "(case when (COALESCE(sum(totalRepairMinute),0)+COALESCE(sum(totalMaintainMinute),0))=0 then 0 else COALESCE(sum(totalMaintainMinute),0)/(COALESCE(sum(totalMaintainMinute),0)+COALESCE(sum(totalRepairMinute),0)) end) as maintainRate," +
//            "( (case when (COALESCE(sum(totalRepairMinute),0)+COALESCE(sum(totalMaintainMinute),0))=0 then 0 else COALESCE(sum(totalMaintainMinute),0)/(COALESCE(sum(totalMaintainMinute),0)+COALESCE(sum(totalRepairMinute),0)) end) * (case when (COALESCE(sum(finishedMaintainTimes),0)+COALESCE(sum(noFinishedMaintainTimes),0))=0 then 0 else COALESCE(sum(finishedMaintainTimes),0)/(COALESCE(sum(finishedMaintainTimes),0)+COALESCE(sum(noFinishedMaintainTimes),0)) end) ) as maintainRate," +
//            "COALESCE(SUM(repairTimes),0) as repairTimes,COALESCE(SUM(totalRepairMinute),0) as totalRepairMinute " +
//            "from (" +
//            "select f.id,f.title,COALESCE(pf.title,f.title) as parent_title,SUM(rpdcount.repairDeviceCount) as repairDeviceCount, SUM(D.deviceCount) as deviceCount," +
//            "SUM(mt.finishedMaintainTimes) as finishedMaintainTimes,SUM(mt.totalMaintainMinute) as totalMaintainMinute," +
//            "SUM(mt.noFinishedMaintainTimes) as noFinishedMaintainTimes," +
//            "SUM(rp.repairTimes) as repairTimes,SUM(rp.totalRepairMinute) as totalRepairMinute " +
//            "from ${schema_name}._sc_facilities f " +
//            "left join (" +
//            "select wm.facility_id,SUM(case when mt.status=60 then 1 else 0 end) as finishedMaintainTimes," +
//            "COALESCE(SUM(case when mt.status=60 then (EXTRACT(EPOCH from mt.finished_time)-EXTRACT(EPOCH from mt.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when mt.status=100 then 1 else 0 end) as noFinishedMaintainTimes " +
//            "from ${schema_name}._sc_works_detail mt " +
//            "left join ${schema_name}._sc_works wm on mt.work_code=wm.work_code " +
//            "left join ${schema_name}._sc_asset dv on mt.relation_id=dv._id " +
//            "left join ${schema_name}._sc_work_type tm on mt.work_type_id=tm.id " +
//            "where tm.business_type_id=2  ${maintainCondition} " +
//            "group by wm.facility_id " +
//            ") mt on f.id=mt.facility_id " +
//            "left join (" +
//            "select wr.facility_id,SUM(case when r.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when r.status=60 then (EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.begin_time))/60 else 0 end),0) as totalRepairMinute " +
//            "from ${schema_name}._sc_works_detail r " +
//            "left join ${schema_name}._sc_works wr on r.work_code=wr.work_code " +
//            "left join ${schema_name}._sc_asset dv on r.relation_id=dv._id  " +
//            "left join ${schema_name}._sc_work_type tr on r.work_type_id=tr.id " +
//            "where tr.business_type_id=1  ${repairCondition} " +
//            "group by wr.facility_id " +
//            ") rp on f.id=rp.facility_id " +
//            "left join (" +
//            "select wr.facility_id,count(DISTINCT(r.relation_id)) as repairDeviceCount " +
//            "from ${schema_name}._sc_works_detail r " +
//            "left join ${schema_name}._sc_works wr on r.work_code=wr.work_code " +
//            "left join ${schema_name}._sc_asset dv on r.relation_id=dv._id " +
//            "left join ${schema_name}._sc_work_type tr on r.work_type_id=tr.id " +
//            "where tr.business_type_id=1  ${repairCondition} " +
//            "group by wr.facility_id " +
//            ") rpdcount on f.id=rpdcount.facility_id " +
//            "left join (" +
//            "select intsiteid,count(_id) as deviceCount from ${schema_name}._sc_asset " +
//            "where intstatus>0 " +
//            "group by intsiteid " +
//            ") D on D.intsiteid=f.id " +
//            "left join ${schema_name}._sc_facilities pf on f.parentid=pf.id " +
//            "where (finishedMaintainTimes>0 or noFinishedMaintainTimes>0 or repairTimes>0) ${facilityCondition} " +
//            "group by f.title,pf.title,f.id " +
//            ") AS DeviceAnalysTotal " +
//            "group by parent_title) T " +
//            "order by ${orderBy} desc")
//    List<RepairAndMaintainChartAnalysIndicator> GetFacilityAnalysChart(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition, @Param("orderBy") String orderBy);
//
//
//    //获取供应商设备故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、平均维修时长统计数据
//    @Select("select customer_name,fault_rate,device_fault_rate,engine_fault_rate,belt_fault_rate,roll_fault_rate,bearing_fault_rate," +
//            "other_fault_rate " +
//            "from (" +
//            "select cust_device.supplier,cust_device.customer_name,(case when deviceCount=0 then 0 else repairTimes/(deviceCount::numeric) end) as fault_rate," +
//            "(case when deviceCount=0 then 0 else device_fault_times/(deviceCount::numeric) end) as device_fault_rate," +
//            "(case when deviceCount=0 then 0 else engine_fault_times/(deviceCount::numeric) end) as engine_fault_rate," +
//            "(case when deviceCount=0 then 0 else belt_fault_times/(deviceCount::numeric) end) as belt_fault_rate," +
//            "(case when deviceCount=0 then 0 else roll_fault_times/(deviceCount::numeric) end) as roll_fault_rate," +
//            "(case when deviceCount=0 then 0 else bearing_fault_times/(deviceCount::numeric) end) as bearing_fault_rate," +
//            "(case when deviceCount=0 then 0 else other_fault_times/(deviceCount::numeric) end) as other_fault_rate," +
//            "device_fault_times,engine_fault_times,belt_fault_times,roll_fault_times,bearing_fault_times,other_fault_times " +
//            "from (" +
//            "select c.id as supplier,c.customer_name,count(dv._id) as deviceCount " +
//            "from ${schema_name}._sc_customers c " +
//            "left join ${schema_name}._sc_asset dv on c.id=dv.supplier " +
//            "where dv.intstatus>0 ${facilityCondition} " +
//            "group by c.id,c.customer_name " +
//            "having count(dv._id)>0 " +
//            ") cust_device " +
//            "left join (" +
//            "select dv.supplier,count(r_run.relation_id) repairTimes " +
//            "from ${schema_name}._sc_asset dv " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//            "left join (" +
//            "select distinct wk.work_type_id,wk.facility_id,wk.relation_id " +
//            "from ${schema_name}._sc_works_detail wkd " +
//            "left join ${schema_name}._sc_works wk on wk.work_code=wkd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wkd.work_type_id=wt.id " +
//            "where wkd.status=60 and wt.business_type_id=1 ${repairCondition}" +
//            ") r_run on r_run.relation_id=dv._id " +
//            "left join ${schema_name}._sc_facilities f on r_run.facility_id=f.id " +
//            "where dv.intstatus>0 ${facilityCondition} " +
//            "group by dv.supplier " +
//            ") total_repair on cust_device.supplier=total_repair.supplier " +
//            "left join ( " +
//            "select dv.supplier,c.customer_name," +
//            "SUM(case when r_run.fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "SUM(case when r_run.fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "SUM(case when r_run.fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "SUM(case when r_run.fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "SUM(case when r_run.fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "SUM(case when r_run.fault_type=6 then 1 else 0 end) as other_fault_times " +
//            "from ${schema_name}._sc_asset dv " +
//            "left join ${schema_name}._sc_customers c on dv.supplier=c.id " +
//
//            "left join (" +
//            "select distinct wk.work_type_id,wk.relation_id,wk.facility_id,wkd.fault_type " +
//            "from ${schema_name}._sc_works_detail wkd " +
//            "left join ${schema_name}._sc_works wk on wk.work_code=wkd.work_code " +
//            "left join ${schema_name}._sc_work_type wt on wkd.work_type_id=wt.id " +
//            "where wkd.status=60 and wt.business_type_id=1 ${repairCondition}" +
//            ") r_run on r_run.relation_id=dv._id " +
//
//            "left join ${schema_name}._sc_fault_type ft on r_run.fault_type=ft.id " +
//            "left join ${schema_name}._sc_facilities f on r_run.facility_id=f.id " +
//            "where dv.intstatus>0 ${facilityCondition} " +
//            "group by dv.supplier,c.customer_name " +
//            ") repair on cust_device.supplier=repair.supplier " +
//            ") AnalysTotal " +
//            "where supplier>0 and (device_fault_times>0 or engine_fault_times>0 or belt_fault_times>0 or roll_fault_times>0 or " +
//            "bearing_fault_times>0 or other_fault_times>0) " +
//            "order by ${orderBy} desc " +
//            "limit ${pageNumber} ")
//    List<SupplierChartResult> GetSupplierAnalysChart(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("orderBy") String orderBy, @Param("pageNumber") int pageNumber);
//
//
//    //设备维修时效、保养时效、故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、维修次数、保养次数数据
//    @Select("select strname,deviceCount,maintainTimes,repairTimes,maintainMinutePreTime,repairMinutePreTime,fault_rate," +
//            "device_fault_rate, engine_fault_rate, belt_fault_rate, roll_fault_rate, bearing_fault_rate, other_fault_rate " +
//            "from ( " +
//            "select dv.strname,count(dv._id) as deviceCount," +
//            "COALESCE(SUM(mt.maintainTimes),0) as maintainTimes,COALESCE(SUM(mt.totalMaintainMinute),0) as totalMaintainMinute," +
//            "COALESCE(SUM(rp.repairTimes),0) as repairTimes,COALESCE(SUM(rp.totalRepairMinute),0) as totalRepairMinute," +
//            "(case when COALESCE(SUM(mt.maintainTimes),0)=0 then 0 else COALESCE(SUM(mt.totalMaintainMinute),0)/SUM(mt.maintainTimes) end) as maintainMinutePreTime, " +
//            "(case when COALESCE(SUM(rp.repairTimes),0)=0 then 0 else SUM(rp.totalRepairMinute)/SUM(rp.repairTimes) end) as repairMinutePreTime, " +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.repairTimes),0)/(count(dv._id)::numeric) end) as fault_rate," +
//            "COALESCE(SUM(rp.device_fault_times),0) as device_fault_times," +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.device_fault_times),0)/(count(dv._id)::numeric) end) as device_fault_rate," +
//            "COALESCE(SUM(rp.engine_fault_times),0) as engine_fault_times," +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.engine_fault_times),0)/(count(dv._id)::numeric) end) as engine_fault_rate," +
//            "COALESCE(SUM(rp.belt_fault_times),0) as belt_fault_times," +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.belt_fault_times),0)/(count(dv._id)::numeric) end) as belt_fault_rate," +
//            "COALESCE(SUM(rp.roll_fault_times),0) as roll_fault_times," +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.roll_fault_times),0)/(count(dv._id)::numeric) end) as roll_fault_rate," +
//            "COALESCE(SUM(rp.bearing_fault_times),0) as bearing_fault_times," +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.bearing_fault_times),0)/(count(dv._id)::numeric) end) as bearing_fault_rate," +
//            "COALESCE(SUM(rp.other_fault_times),0) as other_fault_times," +
//            "(case when count(dv._id)=0 then 0 else COALESCE(SUM(rp.other_fault_times),0)/(count(dv._id)::numeric) end) as other_fault_rate " +
//            "from ${schema_name}._sc_asset dv " +
//            "left join (" +
//            "select mt.relation_id,SUM(case when mt.status=60 then 1 else 0 end) as maintainTimes," +
//            "COALESCE(SUM(case when mt.status=60 then (EXTRACT(EPOCH from mt.finished_time)-EXTRACT(EPOCH from mt.begin_time))/60 else 0 end),0) as totalMaintainMinute " +
//            "from ${schema_name}._sc_works_detail mt " +
//            "left join ${schema_name}._sc_works w on w.work_code=mt.work_code " +
//            "left join ${schema_name}._sc_work_type tm on mt.work_type_id=tm.id " +
//            " where tm.business_type_id=2  ${maintainCondition} group by mt.relation_id " +
//            ") mt on dv._id=mt.relation_id " +
//            "left join ( " +
//            "select r.relation_id,SUM(case when r.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when r.status=60 then (EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.begin_time))/60 else 0 end),0) as totalRepairMinute," +
//            "SUM(case when r.status=60 and r.fault_type=1 then 1 else 0 end) as device_fault_times," +
//            "SUM(case when r.status=60 and r.fault_type=2 then 1 else 0 end) as engine_fault_times," +
//            "SUM(case when r.status=60 and r.fault_type=3 then 1 else 0 end) as belt_fault_times," +
//            "SUM(case when r.status=60 and r.fault_type=4 then 1 else 0 end) as roll_fault_times," +
//            "SUM(case when r.status=60 and r.fault_type=5 then 1 else 0 end) as bearing_fault_times," +
//            "SUM(case when r.status=60 and r.fault_type=6 then 1 else 0 end) as other_fault_times " +
//            "from ${schema_name}._sc_works_detail r " +
//            "left join ${schema_name}._sc_works w on w.work_code=r.work_code " +
//            "left join ${schema_name}._sc_work_type tr on r.work_type_id=tr.id " +
//            "where tr.business_type_id=1 ${repairCondition} group by r.relation_id " +
//            ") rp on dv._id=rp.relation_id " +
//            "where dv.intstatus>0 ${facilityCondition} " +
//            "group by dv.strname ) as Foo " +
//            "where repairTimes>0 or maintainTimes>0 or device_fault_times>0 or engine_fault_times>0 or " +
//            "belt_fault_times>0 or roll_fault_times>0 or bearing_fault_times>0 or other_fault_times>0 " +
//            "order by ${orderBy} desc " +
//            "limit ${pageNumber} ")
//    List<SupplierChartResult> GetDeviceAnalysChart(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition, @Param("orderBy") String orderBy, @Param("pageNumber") int pageNumber);
//
//
//    //获取员工维修时效、保养时效、工作饱和度、保养时效占比、保养完成率、维修次数、保养次数数据
//    @Select("select username,repairMinutePreTime,maintainMinutePreTime,totalMinute,maintainMinutePercent,maintainFinishedPercent,repairTimes,maintainTimes,maintainRate " +
//            "from ( " +
//            "select u.username," +
//            "COALESCE(SUM(mt.maintainTimes),0) as maintainTimes,COALESCE(SUM(mt.totalMaintainMinute),0) as totalMaintainMinute," +
//            "COALESCE(SUM(mt.noFinishedMaintainTimes),0) as noFinishedMaintainTimes," +
//            "COALESCE(SUM(rp.repairTimes),0) as repairTimes,COALESCE(SUM(rp.totalRepairMinute),0) as totalRepairMinute," +
//            "(COALESCE(SUM(rp.totalRepairMinute),0) + COALESCE(SUM(mt.totalMaintainMinute),0)) as totalMinute," +
//            "COALESCE((case when SUM(mt.maintainTimes)=0 then 0 else COALESCE(SUM(mt.totalMaintainMinute),0)/SUM(mt.maintainTimes) end),0) as maintainMinutePreTime," +
//            "COALESCE((case when SUM(rp.repairTimes)=0 then 0 else COALESCE(SUM(rp.totalRepairMinute),0)/SUM(rp.repairTimes) end),0) as repairMinutePreTime," +
//            "(case when (COALESCE(SUM(mt.totalMaintainMinute),0)+COALESCE(SUM(rp.totalRepairMinute),0))=0 then 0 else COALESCE(SUM(mt.totalMaintainMinute),0)/(COALESCE(SUM(rp.totalRepairMinute),0)+COALESCE(SUM(mt.totalMaintainMinute),0)) end) as maintainMinutePercent," +
//            "(case when (COALESCE(SUM(mt.maintainTimes),0)+COALESCE(SUM(mt.noFinishedMaintainTimes),0))=0 then 0 else COALESCE(SUM(mt.maintainTimes),0)/(COALESCE(SUM(mt.maintainTimes),0)+COALESCE(SUM(mt.noFinishedMaintainTimes),0)) end) as maintainFinishedPercent," +
//            "( (case when (COALESCE(sum(mt.totalMaintainMinute),0)+COALESCE(sum(rp.totalRepairMinute),0))=0 then 0 else COALESCE(sum(mt.totalMaintainMinute),0)/(COALESCE(sum(mt.totalMaintainMinute),0)+COALESCE(sum(rp.totalRepairMinute),0)) end) * (case when (COALESCE(sum(mt.maintainTimes),0)+COALESCE(sum(mt.noFinishedMaintainTimes),0))=0 then 0 else COALESCE(sum(mt.maintainTimes),0)/(COALESCE(sum(mt.maintainTimes),0)+COALESCE(sum(mt.noFinishedMaintainTimes),0)) end) ) as maintainRate " +
//            "from ${schema_name}._sc_user u " +
//            "left join (" +
//            "select mt.receive_account,SUM(case when mt.status=60 then 1 else 0 end) as maintainTimes," +
//            "COALESCE(SUM(case when mt.status=60 then (EXTRACT(EPOCH from mt.finished_time)-EXTRACT(EPOCH from mt.begin_time))/60 else 0 end),0) as totalMaintainMinute," +
//            "SUM(case when mt.status=100 then 1 else 0 end) as noFinishedMaintainTimes " +
//            "from ${schema_name}._sc_works_detail mt " +
//            "left join ${schema_name}._sc_asset dv on mt.relation_id=dv._id " +
//            "left join ${schema_name}._sc_works wmt on mt.work_code=wmt.work_code " +
//            "left join ${schema_name}._sc_work_type tm on mt.work_type_id=tm.id " +
//            "where tm.business_type_id=2  ${maintainCondition} " +
//            "group by mt.receive_account " +
//            ") mt on u.account=mt.receive_account " +
//            "left join (" +
//            "select r.receive_account,SUM(case when r.status=60 then 1 else 0 end) as repairTimes," +
//            "COALESCE(SUM(case when r.status=60 then (EXTRACT(EPOCH from r.finished_time)-EXTRACT(EPOCH from r.begin_time))/60 else 0 end),0) as totalRepairMinute " +
//            "from ${schema_name}._sc_works_detail r " +
//            "left join ${schema_name}._sc_asset dv on r.relation_id=dv._id " +
//            "left join ${schema_name}._sc_works wr on r.work_code=wr.work_code " +
//            "left join ${schema_name}._sc_work_type tr on r.work_type_id=tr.id " +
//            "where tr.business_type_id=1  ${repairCondition} " +
//            "group by r.receive_account " +
//            ") rp on u.account=rp.receive_account " +
//            "group by u.username ) as Foo " +
//            "where repairTimes>0 or maintainTimes>0 " +
//            "order by ${orderBy} desc " +
//            "limit ${pageNumber}")
//    List<UserRepairChartResult> GetUserAnalysChart(@Param("schema_name") String schema_name, @Param("repairCondition") String repairCondition, @Param("maintainCondition") String maintainCondition, @Param("orderBy") String orderBy, @Param("pageNumber") int pageNumber);
//
//
//    //按工单类型，查询工单的完成数和未完成数量
////    select distinct wd.relation_id,wd.sub_work_code,wd.status,wd.finished_time,
////    wd.receive_account,wd.work_code,w.create_user_account,w.deadline_time,w.occur_time,wt.business_type_id,ac.id
////    from _sc_works_detail wd
////    left join  _sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id
////    left join _sc_works w on wd.work_code=w.work_code
////    left join _sc_work_type wt on w.work_type_id=wt.id
////    left join _sc_asset ast on wd.relation_id =ast._id
////    left join _sc_asset_category ac on ac.id=ast.category_id
////    where wd.status<>900
//    @Select("select " +
//            "sum(case sen.status when 60 then 1 else 0 end) as finished_count," +
//            "sum(case sen.status when 60 then 0 else 1 end) as processing_count " +
//            "from (" +
//            "select distinct wd.relation_id,wd.sub_work_code,wd.status,wd.finished_time," +
//            "wd.receive_account,wd.work_code,w.create_user_account,w.deadline_time,w.occur_time,wt.business_type_id,ac.id " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status<>900 ${condition} " +
//            ") sen ")
//    Map<String, Object> getFinishedAndProcessingWorkTotal(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//
//    //按工单类型，查询个各工单的完成数和未完成数量
//    @Select("select sen.title,sen.id," +
//            "sum(case sen.status when 60 then 1 else 0 end) as finished_count," +
//            "sum(case sen.status when 60 then 0 else 1 end) as processing_count " +
//            "from (" +
//            "select distinct f.title,f.id,wd.relation_id,wd.sub_work_code,wd.status,wd.finished_time," +
//            "wd.receive_account,wd.work_code,w.create_user_account,w.deadline_time,w.occur_time,wt.business_type_id " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status<>900 ${condition} " +
//            ") sen " +
//            "group by sen.title,sen.id " +
//            "order by sen.id " +
//            "limit ${page} offset ${begin} ")
//    List<Map<String, Object>> getFacilityFinishedAndProcessingWorkTotal(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //按从场地和设备位置，查询个工单的完成数和未完成数量
//    @Select("select distinct w.title,wd.sub_work_code,wd.work_code,w.status,s.status as status_name,wd.finished_time," +
//            "wd.receive_account,u.username,w.deadline_time,w.occur_time,wt.business_type_id," +
//            "ast.strname,ast.strcode,wd.work_type_id,wd.relation_type,wd.is_main " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_user u on wd.receive_account =u.account " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "left join ${schema_name}._sc_status s on w.status=s.id " +
//            "where wd.status<>900 ${condition} " +
//            "order by wd.sub_work_code desc " +
//            "limit ${page} offset ${begin} ")
//    List<Map<String, Object>> getFacilityFinishedAndProcessingWorkListForAsset(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按日期，统计所有费用
//    @Select("select " +
//            "sum(case total_wd.business_type_id when 1 then fe.fee_amount else 0 end) as repair_total_fee, " +
//            "sum(case total_wd.business_type_id when 2 then fe.fee_amount else 0 end) as maintain_total_fee " +
//            "from ${schema_name}._sc_work_fee fe " +
//            "left join " +
//            "(" +
//            "select distinct wd.relation_id,wd.sub_work_code,wt.business_type_id " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_user u on wd.receive_account =u.account " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status=60 ${condition} " +
//            ") total_wd on fe.sub_work_code=total_wd.sub_work_code " +
//            "where total_wd.sub_work_code is not null ")
//    Map<String, Object> getWorkFeeTotalByWorkType(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //按日期，统计个各位置的费用
//    @Select("select total_wd.id,total_wd.title," +
//            "sum(case total_wd.business_type_id when 1 then fe.fee_amount else 0 end) as repair_total_fee, " +
//            "sum(case total_wd.business_type_id when 2 then fe.fee_amount else 0 end) as maintain_total_fee " +
//            "from ${schema_name}._sc_work_fee fe " +
//            "left join " +
//            "(" +
//            "select distinct f.id,f.title,wd.relation_id,wd.sub_work_code,wt.business_type_id " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_user u on wd.receive_account =u.account " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status=60 ${condition} " +
//            ") total_wd on fe.sub_work_code=total_wd.sub_work_code " +
//            "where total_wd.sub_work_code is not null " +
//            "group by total_wd.id,total_wd.title " +
//            "order by total_wd.id " +
//            "limit ${page} offset ${begin} ")
//    List<Map<String, Object>> getWorkFeeTotalListByFacility(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    //按位置，统计各工单费用
//    @Select("select total_wd.title,total_wd.relation_id,total_wd.sub_work_code,total_wd.business_type_id,total_wd.finished_time," +
//            "total_wd.strname,total_wd.strcode,total_wd.username,total_wd.work_type_id,total_wd.relation_type,total_wd.is_main," +
//            "sum(fe.fee_amount) as fee_total " +
//            "from ${schema_name}._sc_work_fee fe " +
//            "left join " +
//            "(" +
//            "select distinct w.title,wd.relation_id,wd.sub_work_code,wt.business_type_id,wd.finished_time," +
//            "ast.strname,ast.strcode,u.username,wd.work_type_id,wd.relation_type,wd.is_main " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_user u on wd.receive_account =u.account " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status=60 ${condition} " +
//            ") total_wd on fe.sub_work_code=total_wd.sub_work_code " +
//            "where total_wd.sub_work_code is not null " +
//            "group by total_wd.title,total_wd.relation_id,total_wd.sub_work_code,total_wd.business_type_id,total_wd.finished_time," +
//            "total_wd.strname,total_wd.strcode,total_wd.username,total_wd.work_type_id,total_wd.relation_type,total_wd.is_main " +
//            "order by total_wd.sub_work_code desc " +
//            "limit ${page} offset ${begin} ")
//    List<Map<String, Object>> getWorkFeeListForAsset(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //按工单类型，查询个各天的完成数和未完成数量
//    @Select("select ${select_day}," +
//            "sum(case sen.status when 60 then 1 else 0 end) as finished_count," +
//            "sum(case sen.status when 60 then 0 else 1 end) as processing_count " +
//            "from (" +
//            "select distinct f.title,f.id,wd.relation_id,wd.sub_work_code,wd.status,wd.finished_time," +
//            "wd.receive_account,wd.work_code,w.create_user_account,wt.business_type_id," +
//            "to_char(w.create_time,'" + SqlConstant.SQL_DATE_FMT + "') as create_time," +
//            "to_char(w.occur_time,'" + SqlConstant.SQL_DATE_FMT + "') as occur_time " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status<>900 ${condition} " +
//            ") sen " +
//            "group by ${select_day} " +
//            "order by ${select_day} ")
//    List<Map<String, Object>> getFinishedAndProcessingWorkTotalByDay(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("select_day") String select_day);
//
//    //按位置，统计各天费用
//    @Select("select total_wd.finished_time," +
//            "sum(fe.fee_amount) as work_fee " +
//            "from ${schema_name}._sc_work_fee fe " +
//            "left join " +
//            "(" +
//            "select distinct f.id,f.title,wd.relation_id,wd.sub_work_code,wt.business_type_id," +
//            "to_char(wd.finished_time,'" + SqlConstant.SQL_DATE_FMT + "') as finished_time " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_works_detail_asset_org org on org.sub_work_code=wd.sub_work_code and org.asset_id=wd.relation_id " +
//            "left join ${schema_name}._sc_facilities f on org.facility_id = f.id " +
//            "left join ${schema_name}._sc_works w on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "left join ${schema_name}._sc_asset ast on wd.relation_id =ast._id " +
//            "left join ${schema_name}._sc_user u on wd.receive_account =u.account " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=ast.category_id " +
//            "where wd.status=60 ${condition} " +
//            ") total_wd on fe.sub_work_code=total_wd.sub_work_code " +
//            "where total_wd.sub_work_code is not null " +
//            "group by total_wd.finished_time " +
//            "order by total_wd.finished_time ")
//    List<Map<String, Object>> getWorkFeeTotalListByDay(@Param("schema_name") String schema_name, @Param("condition") String condition);

}
