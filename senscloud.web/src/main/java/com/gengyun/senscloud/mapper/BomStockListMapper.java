package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 备件清单
 */
public interface BomStockListMapper {
    /**
     * 新增备件库存表信息
     */
    @Insert(" INSERT INTO ${schema_name}._sc_bom_stock( bom_code, quantity, remark, security_quantity," +
            " max_security_quantity,show_price, currency_id, store_position, bom_id, stock_id )" +
            " VALUES ( #{pm.bom_code}, #{pm.quantity}::float, #{pm.remark}, #{pm.security_quantity}::float," +
            " #{pm.max_security_quantity}::float," +
            " #{pm.show_price}::float, #{pm.currency_id}::int, #{pm.store_position}, #{pm.bom_id}, " +
            " #{pm.stock_id}::int)")
    void insertBomStock(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 新增备件库存信息详情表
     */
    @Insert(" INSERT INTO ${schema_name}._sc_bom_stock_detail( bom_code, store_position,quantity, stock_id, bom_id)" +
            " VALUES ( #{pm.bom_code}, #{pm.store_position},#{pm.quantity}::float, #{pm.stock_id}::int, #{pm.bom_id})")
    void insertBomStockDetail(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 根据条件获取备件清单
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select("select bs.id, bs.quantity,s.stock_name from ${schema_name}._sc_bom_stock bs " +
            "left join ${schema_name}._sc_stock s on bs.stock_code = s.stock_code " +
            "where bs.bom_code = #{pm.bom_code} and bs.material_code = #{pm.material_code} " +
            "and bs.stock_code = #{pm.stock_code}")
    Map<String, Object> queryBslIdByInfo(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 根据条件获取备件清单（个人库存）
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select("select sum(bs.quantity) as quantity from ${schema_name}._sc_bom_recipient_detail bs " +
            "INNER JOIN ${schema_name}._sc_bom_recipient br ON br.recipient_code = bs.recipient_code and br.status = 60 " +
            "where bs.bom_code = #{pm.bom_code} and bs.material_code = #{pm.material_code} " +
            "and br.create_user_account = #{pm.stock_code}")
    Map<String, Object> queryReceiverBslByInfo(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 更新备件清单数量
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Update("update ${schema_name}._sc_bom_stock set quantity=#{pm.quantity}::int where id = #{code}")
    int updateBslQuantityByInfo(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap, @Param("code") Integer code);

    /**
     * 根据bom_id更新备件清单数量
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Update(" update ${schema_name}._sc_bom_stock set quantity=quantity + #{pm.quantity}::float," +
            " store_position = (select string_agg(s.m, ',') " +
            "                   from (select distinct regexp_split_to_table(concat(${schema_name}._sc_bom_stock.store_position,',',#{pm.store_position}),',') as m) s where s.m!='') " +
            " where bom_id = #{pm.bom_id} AND stock_id = #{pm.stock_id}::int")
    void updateBomStockQuantityByBomId(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 根据bom_id更新备件清单数量
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Update(" update ${schema_name}._sc_bom_stock_detail set quantity=quantity + #{pm.quantity}::float," +
            " store_position = (select string_agg(s.m, ',') " +
            "                   from (select distinct regexp_split_to_table(concat(${schema_name}._sc_bom_stock_detail.store_position,',',#{pm.store_position}),',') as m) s where s.m!='') " +
            " where bom_id = #{pm.bom_id} AND stock_id = #{pm.stock_id}::int  AND bom_code = #{pm.bom_code}")
    void updateBomStockDetailQuantityByBomId(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 根据备件id和库房id获取备件库房记录
     */
    @Select("SELECT id, quantity, remark, security_quantity," +
            "max_security_quantity,show_price, currency_id, store_position, bom_id, stock_id FROM  ${schemaName}._sc_bom_stock WHERE bom_id = #{bom_id} AND stock_id = #{stock_id} ")
    Map<String, Object> findBomStockByBomIdStockId(@Param("schemaName") String schemaName, @Param("bom_id") String bom_id, @Param("stock_id") Integer stock_id);


    /**
     * 根据条件获取备件信息
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select("select id from ${schema_name}._sc_bom where bom_code = #{pm.bom_code} and material_code = #{pm.material_code}")
    int queryBomIdByInfo(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);


    /**
     * 验证库房是否存在
     *
     * @param schemaName
     * @param stockList
     * @return
     */
    @Select("<script>select distinct stock_name, stock_code, id from ${schemaName}._sc_stock s " +
            "where s.stock_name in " +
            "<foreach collection='stocks' item='stock' open='(' close=')' separator=','> " +
            "#{stock} " +
            "</foreach> and s.status > -1000 </script>")
    @MapKey("stock_name")
    Map<String, Map<String, Object>> queryStockByCode(@Param("schemaName") String schemaName, @Param("stocks") List<String> stockList);

    /**
     * 验证备件类型是否存在
     *
     * @param schemaName
     * @param bomTypeList
     * @return
     */
    @Select("<script>select distinct id, type_name from ${schemaName}._sc_bom_type s " +
            "where s.type_name in " +
            "<foreach collection='bomTypes' item='bomType' open='(' close=')' separator=','> " +
            "#{bomType} " +
            "</foreach></script>")
    @MapKey("type_name")
    Map<String, Map<String, Object>> queryBomTypeByCode(@Param("schemaName") String schemaName, @Param("bomTypes") List<String> bomTypeList);

    /**
     * 验证备件单位是否存在
     *
     * @param schemaName
     * @param bomUnitList
     * @return
     */
    @Select("<script>select distinct id, unit_name from ${schemaName}._sc_unit s " +
            "where s.unit_name in " +
            "<foreach collection='bomUnits' item='bomUnit' open='(' close=')' separator=','> " +
            "#{bomUnit} " +
            "</foreach></script>")
    @MapKey("unit_name")
    Map<String, Map<String, Object>> queryBomUnitByCode(@Param("schemaName") String schemaName, @Param("bomUnits") List<String> bomUnitList);

    /**
     * 根据bomid，bom_code,stocl_id获取备件详情数量（用于备件一物一码）
     */
    @Select(" SELECT * FROM ${schemaName}._sc_bom_stock_detail " +
            " WHERE bom_id=#{bom_id} AND bom_code = #{bom_code} AND stock_id = #{stock_id}")
    Map<String, Object> findBomStockDetailByBomIdCodeStockId(@Param("schemaName") String schemaName,
                                                             @Param("bom_id") Integer bom_id,
                                                             @Param("bom_code") String bom_code,
                                                             @Param("stock_id") Integer stock_id);

    /**
     * 根据备件id获取备件编码
     *
     * @param schemaName
     * @param bom_id
     * @param user_id
     * @return
     */
    @Select("SELECT DISTINCT bom_code " +
            "FROM ${schemaName}._sc_bom_stock_detail bsd " +
            "WHERE bom_id = #{bom_id} " +
            "AND stock_id in " +
            "(SELECT rs.stock_id " +
            "FROM ${schemaName}._sc_role_stock rs " +
            "JOIN ${schemaName}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schemaName}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schemaName}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id}) ")
    List<Map<String, Object>> findBomCodeByBomIdWithStockPermission(@Param("schemaName") String schemaName, @Param("bom_id") String bom_id, @Param("user_id") String user_id);


    /**
     * 查询库存低于安全库存的备件库存信息
     */
    @Select(" SELECT bs.id,bs.bom_code,bs.quantity,bs.security_quantity,bs.max_security_quantity,bs.show_price," +
            " bs.currency_id,bs.store_position,bs.bom_id,bs.stock_id,b.bom_name,b.is_use,b.create_user_id," +
            " b.create_time,b.bom_model,b.type_id,b.unit_id,b.brand_name,b.material_code,b.is_from_service_supplier," +
            " b.service_life,b.bom_use,b.show_price,b.currency_id,b.manufacturer_id,b.code_classification,b.data_order," +
            " b.status,s.stock_name,s.security_quantity_msg_time,s.security_quantity_msg_interval " +
            " FROM " +
            " ${schemaName}._sc_bom_stock AS bs" +
            " LEFT JOIN ${schemaName}._sc_bom AS b ON bs.bom_id = b.ID " +
            " LEFT JOIN ${schemaName}._sc_stock AS s ON s.id = bs.stock_id " +
            " WHERE" +
            " b.status >-1000 AND bs.quantity < bs.security_quantity")
    List<Map<String, Object>> findBomLowerSecurityQuantityList(@Param("schemaName") String schemaName);

    /**
     * 更新备件库存发送信息时间
     */
    @Update(" UPDATE ${schemaName}._sc_bom set security_quantity_msg_time = NOW() where id = #{id}")
    void updateBomStockSendMsgTime(@Param("schemaName") String schemaName, @Param("id") Integer id);

    /**
     * 更新库房发送信息时间
     */
    @Update(" UPDATE ${schemaName}._sc_stock set security_quantity_msg_time = NOW() where id = #{id}")
    void updateStockSendMsgTime(@Param("schemaName") String schemaName, @Param("id") Integer id);

    @Insert({
            "<script>",
            "<foreach collection='bomStockList' item='bomStock' index='index' separator=';'>",
            "INSERT INTO ${schema_name}._sc_bom_stock (quantity,security_quantity,max_security_quantity,show_price,currency_id,store_position,bom_id,stock_id) ",
            "VALUES (#{bomStock.quantity}::decimal,#{bomStock.security_quantity}::decimal,#{bomStock.max_security_quantity}::decimal,#{bomStock.show_price}::decimal,#{bomStock.currency_id}::int,#{bomStock.store_position},#{bomStock.bom_id},#{bomStock.stock_id}::int) ",
            "ON CONFLICT (bom_id,stock_id) ",
            "DO UPDATE SET quantity=${schema_name}._sc_bom_stock.quantity+#{bomStock.quantity}::decimal,security_quantity = #{bomStock.security_quantity}::decimal,max_security_quantity =#{bomStock.max_security_quantity}::decimal," ,
            " show_price = #{bomStock.show_price}::decimal,currency_id = #{bomStock.currency_id}::int," ,
            " store_position = (select string_agg(s.m, ',') " ,
            "      from (select distinct regexp_split_to_table(concat(${schema_name}._sc_bom_stock.store_position,',',#{bomStock.store_position}),',') as m) s where s.m!='' )," +
                    " bom_id = #{bomStock.bom_id},stock_id = #{bomStock.stock_id}::int ",
            "</foreach>",
            "</script>"})
    void insertOrUpdateBomStock(@Param("schema_name") String schemaName, @Param("bomStockList") List<Map<String, Object>> bomStockList);

    @Insert({
            "<script>",
            "<foreach collection='bomImportList' item='bomImport' index='index' separator=';'>",
            "INSERT INTO ${schema_name}._sc_bom_import (id,bom_id,stock_id,create_user_id,bom_name,material_code,bom_type,bom_model,bom_unit,quantity,show_price,currency,supplier,supply_cycle,manufacturer,service_life,bom_stock,security_quantity,max_security_quantity,remark,store_position) ",
            "VALUES (#{bomImport.id},#{bomImport.bom_id},#{bomImport.stock_id}::int,#{bomImport.create_user_id},#{bomImport.bom_name},#{bomImport.material_code},#{bomImport.bom_type},#{bomImport.bom_model},#{bomImport.bom_unit}," +
                    "#{bomImport.quantity} ::decimal,#{bomImport.show_price}::decimal,#{bomImport.currency},#{bomImport.supplier},#{bomImport.supply_cycle},#{bomImport.manufacturer},#{bomImport.service_life},#{bomImport.bom_stock}," +
                    "#{bomImport.security_quantity},#{bomImport.max_security_quantity},#{bomImport.remark},#{bomImport.store_position}) ",
            "</foreach>",
            "</script>"})
    void insertBomImport(@Param("schema_name") String schemaName, @Param("bomImportList") List<Map<String, Object>> bomImportList);

    @Insert({
            "<script>",
            "<foreach collection='bomStockDetailList' item='bomStock' index='index' separator=';'>",
            "INSERT INTO ${schema_name}._sc_bom_stock_detail (bom_code,store_position,quantity,stock_id,bom_id) ",
            "VALUES (#{bomStock.bom_code},#{bomStock.store_position},#{bomStock.quantity}::int,#{bomStock.stock_id}::int,#{bomStock.bom_id}) ",
            "ON CONFLICT (bom_id,stock_id,bom_code) ",
            "DO UPDATE SET quantity=${schema_name}._sc_bom_stock_detail.quantity+#{bomStock.quantity}::decimal," ,
            " bom_code = #{bomStock.bom_code},bom_id = #{bomStock.bom_id},stock_id = #{bomStock.stock_id}::int," ,
            " store_position = (select string_agg(s.m, ',') " +
                    "      from (select distinct regexp_split_to_table(concat(${schema_name}._sc_bom_stock_detail.store_position,',',#{bomStock.store_position}),',') as m) s) " ,
            "</foreach>",
            "</script>"})
    void insertOrUpdateBomStockDetail(@Param("schema_name") String schemaName, @Param("bomStockDetailList") List<Map<String, Object>> bomStockDetailList);
}
