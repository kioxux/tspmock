package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 科华安全日历
 */
@Mapper
public interface CalendarMapper {
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = CalendaMapperProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                    @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                    @Param("searchKey") String searchKey, @Param("accident_type") String accident_type, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = CalendaMapperProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("accident_type") String accident_type, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
//
//    /**
//     * 新增数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @InsertProvider(type = CalendaMapperProvider.class, method = "insert")
//    int insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 根据主键查询工作安排
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_production_accident w where id = #{id} ")
//    Map<String, Object> queryCalendayId(@Param("schema_name") String schemaName, @Param("id") Integer id);
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_production_accident set " +
//            "accident_title=#{s.accident_title}, " +
//            "accident_type=#{s.accident_type}::int , " +
//            "accident_content=#{s.accident_content}::text, " +
//            "occur_time=#{s.occur_time}::timestamp " +
//            "where id=#{s.id}::int ")
//    int update(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Update("DELETE FROM ${schema_name}._sc_production_accident where id=#{id} ")
//    int delete(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//    /**
//     * 实现类
//     */
//    class CalendaMapperProvider {
//        /**
//         * 新增数据
//         *
//         * @param schemaName
//         * @param paramMap
//         * @return
//         */
//        public String insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_production_accident");
//                VALUES("accident_title ", "#{s.accident_title}");
//                VALUES("accident_type ", "#{s.accident_type}::int");
//                VALUES("accident_content ", "#{s.accident_content}::text");
//                VALUES("occur_time ", "#{s.occur_time}::timestamp");
//                VALUES("create_user_account ", "#{s.create_user_account}");
//                VALUES("create_time ", "current_timestamp");
//            }}.toString();
//        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param searchKey
//         * @return
//         */
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("searchKey") String searchKey, @Param("accident_type") String accident_type, @Param("beginTime") String beginTime, @Param("endTime") String endTime) {
//            return new SQL() {{
//                String condition="1=1  ";
//                SELECT(" a.*,c.username  ");
//                FROM(schemaName + "._sc_production_accident a "+
//                        "LEFT JOIN " +schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition+="and upper(a.accident_title) like upper('%" + searchKey
//                            + "%') or upper(a.accident_content) like upper('%" + searchKey
//                            + "%') or upper(c.create_user_account) like upper('%" + searchKey + "%') ";
//                }
//                if(null != beginTime && !"".equals(beginTime)&&null != endTime && !"".equals(endTime)){
//                    condition+=" and a.occur_time>='"+ Timestamp.valueOf(beginTime + " 00:00:00")+"' and a.occur_time<'"+Timestamp.valueOf(endTime + " 00:00:00")+"'";
//                }
//                if(null != accident_type && !"".equals(accident_type)&& !"0".equals(accident_type)){
//                    condition+=" and a.accident_type="+accident_type;
//                }
//                WHERE(condition);
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("accident_type") String accident_type, @Param("beginTime") String beginTime, @Param("endTime") String endTime) {
//            return new SQL() {{
//                String condition="1=1  ";
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_production_accident a "+
//                        "LEFT JOIN " +schemaName + "._sc_user c ON c.account=a.create_user_account ");
//                // 查询条件
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition+="and upper(a.accident_title) like upper('%" + searchKey
//                            + "%') or upper(a.accident_content) like upper('%" + searchKey
//                            + "%') or upper(c.create_user_account) like upper('%" + searchKey + "%') ";
//                }
//                if(null != beginTime && !"".equals(beginTime)&&null != endTime && !"".equals(endTime)){
//                    condition+=" and a.occur_time>='"+ Timestamp.valueOf(beginTime + " 00:00:00")+"' and a.occur_time<'"+Timestamp.valueOf(endTime + " 00:00:00")+"'";
//                }
//                if(null != accident_type && !"".equals(accident_type)&& !"0".equals(accident_type)){
//                    condition+=" and a.accident_type="+accident_type;
//                }
//                WHERE(condition);
//            }}.toString();
//        }
//
//    }
}
