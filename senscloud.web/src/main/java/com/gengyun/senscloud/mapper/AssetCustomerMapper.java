package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetCustomerMapper {
//    /**
//     * 查询设备的关联厂商
//     *
//     * @param schema_name
//     * @param assetId
//     * @return
//     */
//    @Select("SELECT a.title,a.id,a.facility_no,a.org_type,a.address,a.isuse " +
//            "FROM ${schema_name}._sc_asset_organization t " +
//            "left join ${schema_name}._sc_facilities a on t.org_id=a.id " +
//            "where t.asset_id=#{assetId} and a.org_type in ("+ SqlConstant.FACILITY_OUTSIDE +") " +
//            "order by id desc")
//    List<Facility> getAssetFacilitiesSupplierList(@Param("schema_name") String schema_name, @Param("assetId") String assetId);
//
//    /**
//     * 新增资产关联企业信息
//     *
//     * @param schema_name
//     * @param asset_id
//     * @param org_id
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_asset_organization (asset_id,org_id) " +
//            "values(#{asset_id},#{org_id})")
//    int AddAssetSupplier(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("org_id") Integer org_id);
//
//    //查找资产关联企业，按查找
//    @Select("SELECT a.title,a.id,a.facility_no,a.org_type,a.address " +
//            "FROM ${schema_name}._sc_asset_organization t " +
//            "left join ${schema_name}._sc_facilities a on t.org_id = a.id " +
//            "where a.id=#{id} ")
//    Facility findAssetSupplierById(@Param("schema_name") String schema_name, @Param("id") Integer id);
//
//    //查找资产关联企业，按查找
//    @Select("SELECT a.title,a.id,a.facility_no,a.org_type,a.address " +
//            "FROM ${schema_name}._sc_asset_organization t " +
//            "left join ${schema_name}._sc_facilities a on t.org_id = a.id " +
//            "where t.org_id=#{org_id} and t.asset_id=#{asset_id} " +
//            "order by org_id")
//    List<Facility> findAssetSupplier(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("org_id") Integer org_id);
//
//    @Update("delete from ${schema_name}._sc_asset_organization where org_id=#{org_id} and asset_id=#{asset_id} ")
//    int DeleteAssetSupplier(@Param("schema_name") String schema_name, @Param("org_id") Integer org_id, @Param("asset_id") String asset_id);
}
