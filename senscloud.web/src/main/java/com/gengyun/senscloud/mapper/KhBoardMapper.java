package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface KhBoardMapper {

//    //所有设备类型和设备的数量
//    @Select("select wt.id,wt.category_name as name,count(a._id) as value from " +
//            "${schema_name}._sc_asset_category wt  " +
//            "left join ${schema_name}._sc_asset a on wt.id=a.category_id " +
//            "where a.intstatus<>'-1000' "+
//            "group by wt.id,wt.category_name "+
//            "order by wt.id  ")
//    List<Map<String,Object>> getCategoryAndBillCountList(@Param("schema_name") String schema_name);
//
//    //所有设备状态和设备的数量
//    @Select("select wt.id,wt.status as name,count(a._id) as value from " +
//            "${schema_name}._sc_asset_status wt  " +
//            "left join ${schema_name}._sc_asset a on wt.id=a.intstatus " +
//            "where a.intstatus<>'-1000' "+
//            "group by wt.id,wt.status "+
//            "order by wt.id  ")
//    List<Map<String,Object>> getStatusAndBillCountList(@Param("schema_name") String schema_name);
//
//    //所有产线和产线的数量
//    @Select("select substring(f.facilitycode from 1 for 2) as name,count(f.id) as value from " +
//            "${schema_name}._sc_facilities f  " +
//            "where f.status<>'-1000' "+
//            "group by substring(f.facilitycode from 1 for 2)  ")
//    List<Map<String,Object>> getLineAndBillCountList(@Param("schema_name") String schema_name);
//
//    //查询所有异常产线集合
//    @Select("select DISTINCT (f.id) from " +
//            "${schema_name}._sc_works w  " +
//            "left join ${schema_name}._sc_facilities f on f.id=w.facility_id " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=w.work_type_id " +
//            "where w.status<60 and w.status>10 and wt.business_type_id=1 and w.facility_id NOTNULL and f.status<>'-1000'  ")
//    List<String> getLineErrorList(@Param("schema_name") String schema_name);
//
//    //查找产线列表
//    @Select("SELECT\n" +
//            "\tf. ID,\n" +
//            "\tf.title,\n" +
//            "\tCOUNT (w.work_code) AS errorTimes,\n" +
//            "\tround(COALESCE (\n" +
//            "\t\tSUM (\n" +
//            "\t\t\tCASE\n" +
//            "\t\t\tWHEN w.status = 60 THEN\n" +
//            "\t\t\t\t(\n" +
//            "\t\t\t\t\tEXTRACT (EPOCH FROM wr.finished_time) - EXTRACT (EPOCH FROM w.occur_time)\n" +
//            "\t\t\t\t):: NUMERIC / 60\n" +
//            "\t\t\tELSE\n" +
//            "\t\t\t\t0\n" +
//            "\t\t\tEND\n" +
//            "\t\t),\n" +
//            "\t\t0\n" +
//            "\t) ,0)AS consumeTime,\n" +
//            "\tround(COALESCE (\n" +
//            "\t\tAVG  (\n" +
//            "\t\t\tCASE\n" +
//            "\t\t\tWHEN w.status = 60 THEN\n" +
//            "\t\t\t\t(round(\n" +
//            "\t\t\t\t\n" +
//            "\t\t\t\t\t\t((EXTRACT (EPOCH FROM wr.finished_time) - EXTRACT (EPOCH FROM w.occur_time)):: NUMERIC / 60)\n" +
//            "\t\t\t\t\t / \n" +
//            "\t\t\t\t\t\t((EXTRACT (EPOCH FROM now()) - EXTRACT (\n" +
//            "\t\t\t\t\t\t\tEPOCH\n" +
//            "\t\t\t\t\t\t\tFROM\n" +
//            "\t\t\t\t\t\t\t\t(\n" +
//            "\t\t\t\t\t\t\t\t\tto_date(\n" +
//            "\t\t\t\t\t\t\t\t\t\tconcat (\n" +
//            "\t\t\t\t\t\t\t\t\t\t\tto_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR + "'),\n" +
//            "\t\t\t\t\t\t\t\t\t\t\t'-01-01'\n" +
//            "\t\t\t\t\t\t\t\t\t\t),\n" +
//            "\t\t\t\t\t\t\t\t\t\t'" + SqlConstant.SQL_DATE_FMT + "'\n" +
//            "\t\t\t\t\t\t\t\t\t)\n" +
//            "\t\t\t\t\t\t\t\t)\n" +
//            "\t\t\t\t\t\t)):: NUMERIC / 60)*100,4 )\n" +
//            "\t\t\t\t\t\n" +
//            "\t\t\t\t) \n" +
//            "\t\t\tELSE\n" +
//            "\t\t\t\t0\n" +
//            "\t\t\tEND\n" +
//            "\t\t),\n" +
//            "\t\t0\n" +
//            "\t) ,2) AS errorRate\n" +
//            "FROM\n" +
//            "\t${schema_name}._sc_facilities f\n" +
//            "LEFT JOIN ${schema_name}._sc_works w ON w.facility_id = f.id\n" +
//            "LEFT JOIN ${schema_name}._sc_works_detail wr ON w.work_code = wr.work_code\n" +
//            "LEFT JOIN ${schema_name}._sc_work_type wt ON wt. ID = w.work_type_id\n" +
//            "WHERE\n" +
//            "\t1 = 1\n" +
//            "AND wt.business_type_id = 1 AND to_char(w.occur_time,'" + SqlConstant.SQL_DATE_FMT_YEAR + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT_YEAR + "')\n AND f.status<>'-1000' " +
//            "GROUP BY\n" +
//            "\tf. ID,f. title\n" +
//            "\n" +
//            "ORDER BY f. ID\n" +
//            "LIMIT 50 OFFSET 0\n" )
//    List<Map<String,Object>> getLineList(@Param("schema_name") String schema_name);
//
//    //所有设备类型和设备的数量
//    @Select("select s.id,s.status,count(w.work_code)::int from " +
//            "${schema_name}._sc_status s  " +
//            "left join ${schema_name}._sc_works w on w.status=s.id " +
//            "left join ${schema_name}._sc_work_type wt on wt.id=w.work_type_id " +
//            "where 1=1 ${condition}  "+
//            "group by s.id,s.status  ")
//    List<Map<String,Object>> getWorkSheetStatusAndBillCountList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //重点产线监护
//    @Select("select f.id,f.title,max(wr.priority_level)as priority_level "+
//            " from ${schema_name}._sc_facilities f " +
//            " left join ${schema_name}._sc_works w on w.facility_id=f.id " +
//            " left join ${schema_name}._sc_works_detail wr on w.work_code=wr.work_code   " +
//            " left join ${schema_name}._sc_work_type wt on wt.id=w.work_type_id  and wt.business_type_id=1 " +
//            "where 1=1 and facilitytypeid='8' AND f.status<>'-1000' " +
//            "group by f.id,f.title  " +
//            "order by f.id desc limit 50 offset 0")
//    List<Map<String,Object>> getLineImportantList(@Param("schema_name") String schema_name);
//
//    //查询当天班信息
//    @Select("select DISTINCT  to_char(b.Begin_time,'hh24') as checktime,u.username,substring(f.facilitycode from 1 for 2) as facilities_name,u.mobile from ${schema_name}._sc_work_schedule b " +
//            "left join ${schema_name}._sc_user u on b.user_account=u.account " +
//            "LEFT JOIN ${schema_name}._sc_group_org go ON go.group_id = b.group_id " +
//            "left join ${schema_name}._sc_facilities as f on f.id = go.org_id " +
//            "where b.Begin_time>=current_date and b.End_time<(current_date+interval '1d') and substring(f.facilitycode from 1 for 2) in('NR','WK','ST','PV') AND f.status<>'-1000'")
//    List<Map<String,Object>> getArrangementList(@Param("schema_name") String schema_name);
//
//    //每月故障率
//    @Select("select date as name,\n" +
//            "round(faulttimes::numeric/allassetnum::numeric,4)*100 as value from \n" +
//            "\t(SELECT to_char(date_trunc('month',wr_run.occur_time),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') as date,\n" +
//            "\t\t\tcount(distinct strcode) as faulttimes,\n" +
//            "\t\t\tSUM(case when r_run.status=60 then 1 else 0 end) as times,\n" +
//            "\t\t\t(select count(distinct strcode) from sc_com_47._sc_asset as  asset) as allassetnum,\n" +
//            "\t\t\tCOALESCE(SUM((EXTRACT(EPOCH from r_run.finished_time)-EXTRACT(EPOCH from r_run.begin_time))/60),0) as mttr \t\t\t\n" +
//            "\t\t\tfrom sc_com_47._sc_works_detail r_run \n" +
//            "\t\t\tleft join  sc_com_47._sc_work_type as  wt on r_run.work_type_id=wt.id \n" +
//            "\t\t\tleft join  sc_com_47._sc_works as  wr_run on r_run.work_code=wr_run.work_code \n" +
//            "\t\t\tleft join  sc_com_47._sc_asset as  dv on r_run.relation_id=dv._id \n" +
//            "            where 1=1 and wt.business_type_id=1 and  r_run.status=60    \n" +
//            "\t\t\tgroup by to_char(date_trunc('month',wr_run.occur_time),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')\n" +
//            "\t\t\t) as foo  order by date asc" )
//    List<Map<String, Object>> getFailureRate(@Param("schema_name") String schema_name);
//
//    //每月故障次数
//    @Select("SELECT\n" +
//            "\tTIME AS name,\n" +
//            "\tMBTR AS value\n" +
//            "FROM\n" +
//            "\t(\n" +
//            "\t\tSELECT\n" +
//            "\t\t\tto_char(\n" +
//            "\t\t\t\tdate_trunc('month', sw.occur_time),\n" +
//            "\t\t\t\t'mm'\n" +
//            "\t\t\t) AS TIME,\n" +
//            "\t\t\tSUM (\n" +
//            "\t\t\t\tCASE\n" +
//            "\t\t\t\tWHEN sw.status = 60 THEN\n" +
//            "\t\t\t\t\t1\n" +
//            "\t\t\t\tELSE\n" +
//            "\t\t\t\t\t0\n" +
//            "\t\t\t\tEND\n" +
//            "\t\t\t) AS MBTR\n" +
//            "\t\tFROM\n" +
//            "\t\t\t${schema_name}.\"_sc_works\" AS sw\n" +
//            "\t\tWHERE\n" +
//            "\t\t\tsw.status = 60\n" +
//            "\t\tAND sw.work_type_id = 1\n" +
//            "\t\tAND sw.occur_time > (\n" +
//            "\t\t\tCURRENT_TIMESTAMP - INTERVAL '1 year'\n" +
//            "\t\t)\n" +
//            "\t\tGROUP BY\n" +
//            "\t\t\tto_char(\n" +
//            "\t\t\t\tdate_trunc('month', sw.occur_time),\n" +
//            "\t\t\t\t'mm'\n" +
//            "\t\t\t)\n" +
//            "\t) AS foo\n" +
//            "ORDER BY\n" +
//            "\tTIME ASC")
//    List<Map<String, Object>> getFailureNum(@Param("schema_name") String schema_name);
//
//    //故障间隔
//    @Select(" select time as name,\n" +
//            "MTBF/totalTimes as value from (\n" +
//            "\n" +
//            "select to_char(date_trunc('month',r_run.occur_time),'mm') as time,\n" +
//            "\t\t\tCOALESCE(SUM(case when r_run.status=60 then (EXTRACT(EPOCH from  ${schema_name}.get_latest_next_repair_occur_time(r_run.relation_id,r_run.work_code))-EXTRACT(EPOCH from r_run.occur_time))/60 else 0 end),0) as MTBF, \n" +
//            "\t\t\tSUM(case when r_run.status=60 then 1 else 0 end) as totalTimes \n" +
//            "\t\t\tfrom  ${schema_name}._sc_works r_run \n" +
//            "\t\tLEFT JOIN  ${schema_name}._sc_work_type wt on r_run.work_type_id=wt.id\n" +
//            "\t\t\twhere 1=1 and r_run.status=60 and r_run.occur_time>(current_timestamp - interval '1 year') and wt.business_type_id=1\n" +
//            "\t\t\tgroup by to_char(date_trunc('month',r_run.occur_time),'mm')\n" +
//            "\t) as foo order by time asc " )
//    List<Map<String, Object>> getMtbf(@Param("schema_name") String schema_name);
//
//    //故障修复时间
//    @Select(" select time as name,\n" +
//            "MTBF/totalTimes as value from (\n" +
//            "\n" +
//            "select to_char(date_trunc('month',r_run.occur_time),'mm') as time,\n" +
//            "\t\t\tCOALESCE(SUM(case when r_run.status=60 then (EXTRACT(EPOCH from wr.finished_time)-EXTRACT(EPOCH from r_run.occur_time))/60 else 0 end),0) as MTBF, \n" +
//            "\t\t\tSUM(case when r_run.status=60 then 1 else 0 end) as totalTimes \n" +
//            "\t\t\tfrom  ${schema_name}._sc_works r_run \n" +
//            "LEFT JOIN ${schema_name}.\"_sc_works_detail\" wr ON r_run.work_code = wr.work_code" +
//            "\t\tLEFT JOIN  ${schema_name}._sc_work_type wt on r_run.work_type_id=wt.id\n" +
//            "\t\t\twhere 1=1 and r_run.status=60 and r_run.occur_time>(current_timestamp - interval '1 year') and wt.business_type_id=1\n" +
//            "\t\t\tgroup by to_char(date_trunc('month',r_run.occur_time),'mm')\n" +
//            "\t) as foo order by time asc ")
//    List<Map<String, Object>> getMttr(@Param("schema_name") String schema_name);
//
//    //保养完成率
//    @Select("   select (case when (select (case when count(1)=0 then 0 else count(1) end) from ${schema_name}._sc_works_detail as wd \n" +
//            "LEFT JOIN ${schema_name}._sc_works  as w on w.work_code=wd.work_code \n" +
//            "left join ${schema_name}._sc_work_type wt on wt.id = w.work_type_id \n" +
//            "where wt.business_type_id = 2 and wd.status in (60,70,80,100) and wd.work_type_id=2  ${condition}\n" +
//            ")=0 then 0 else  round(count(1) ::numeric/ (select (case when count(1)=0 then 0 else count(1) end) from ${schema_name}._sc_works_detail as wd \n" +
//            "LEFT JOIN ${schema_name}._sc_works  as w on w.work_code=wd.work_code \n" +
//            "left join ${schema_name}._sc_work_type wt on wt.id = w.work_type_id \n" +
//            "where wd.status <> 900 and wd.work_type_id=2  ${condition}\n" +
//            "and  wt.business_type_id = 2 ) ::numeric,4)*100 end ) as \"value\" ,'保养完成率' as \"name\"  from ${schema_name}._sc_works_detail as swd \n" +
//            "LEFT JOIN ${schema_name}._sc_works  as w on w.work_code=swd.work_code \n" +
//            "left join ${schema_name}._sc_work_type wt on wt.id = w.work_type_id \n" +
//            "where swd.status =60 and wt.business_type_id = 2  ${condition}\n" )
//    List<Map<String,Object>> getMaintainRate(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //设备总人数
//    @Select("select substring(f.facilitycode from 1 for 2) as name,count(DISTINCT ug.user_id) as value from " +
//            " ${schema_name}._sc_facilities f "+
//            "left join ${schema_name}._sc_group_org  go on go.org_id=f.id  " +
//            "left join ${schema_name}._sc_user_group  ug on ug.group_id=go.group_id  " +
//            "where substring(f.facilitycode from 1 for 2) in('NR','WK','ST','PV') and f.status<>'-1000' "+
//            "group by substring(f.facilitycode from 1 for 2)  ")
//    List<Map<String,Object>> getLineManNum(@Param("schema_name") String schema_name);
//
//    //设备总出勤人数
//    @Select("select substring(f.facilitycode from 1 for 2) as name,count(DISTINCT b.user_account) as value from " +
//            " ${schema_name}._sc_facilities f "+
//            "LEFT JOIN ${schema_name}._sc_group_org go ON go.org_id = f. ID "+
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.group_id = go. group_id " +
//            "LEFT JOIN ${schema_name}._sc_user u ON u.id = ug.user_id "+
//            "LEFT JOIN ${schema_name}._sc_work_schedule b ON b.user_account = u.account "+
//            "where  b.Begin_time>=current_date and b.End_time<(current_date+interval '1d') and substring(f.facilitycode from 1 for 2) in('NR','WK','ST','PV')  and f.status<>'-1000'"+
//            "group by substring(f.facilitycode from 1 for 2) ")
//    List<Map<String,Object>> getLineOndutyNum(@Param("schema_name") String schema_name);
//
//    //安全日历
//    @Select("select to_char(occur_time,'" + SqlConstant.SQL_DATE_FMT + "') as occur_time,accident_content,accident_type from ${schema_name}._sc_production_accident "+
//            "where 1=1 ${condition}"+
//         "")
//    List<Map<String,Object>> getCalendarList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //安全日历
//    @Select("select count(w.work_code) from ${schema_name}._sc_works w   "+
//            "left join ${schema_name}.\"_sc_works_detail\" wd on  wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_work_type wt on  wt.id=w.work_type_id " +
//            "where w.status!=60 and to_char(wd.finished_time,'" + SqlConstant.SQL_DATE_FMT + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT + "')")
//    int getTodyWork(@Param("schema_name") String schema_name);
//
//    //连续安全生产天数
//    @Select("select round((EXTRACT(EPOCH from now())-EXTRACT(EPOCH from (select max(w.occur_time) from ${schema_name}._sc_production_accident w " +
//            "where w.accident_type=2 )))/(60*60*24)) as run")
//    Map<String,Object> getSafetyInProduction(@Param("schema_name") String schema_name);
//
//    //按条件查询工作任务
//    @Select("select a.task_content,u.username as duty_name, uh.username as help_name from ${schema_name}._sc_work_task a " +
//            "left join ${schema_name}._sc_user  u on a.duty_man=u.account  "+
//            "left join ${schema_name}._sc_user  uh on a.help_man=uh.account  "+
//            "where 1=1 ${condition}"+
//            "")
//    List<Map<String,Object>> getArrangement(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查询启用的公告
//    @Select("select notice_content from ${schema_name}._sc_notice   " +
//            "where is_use=true")
//    Map<String,Object> getNotice(@Param("schema_name") String schema_name);
//
//
//    //查询产线总人数
//    @Select("select count(DISTINCT a.account) from ${schema_name}._sc_user a   " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = A . ID "+
//            "LEFT JOIN ${schema_name}._sc_group_org go ON go.group_id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = go.org_id "+
//            "where f.id NOTNULL and substring(f.facilitycode from 1 for 2) in('NR','WK','ST','PV') and f.status<>'-1000'")
//    int getLineManCount(@Param("schema_name") String schema_name);
//
//    //查询产线在线总人数
//    @Select("select count(DISTINCT u.account) from ${schema_name}._sc_user u   " +
//            "left join ${schema_name}._sc_work_schedule  b on b.user_account=u.account "+
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u. ID "+
//            "LEFT JOIN ${schema_name}._sc_group_org go ON go.group_id = ug. group_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id =go.org_id "+
//            "where f.id NOTNULL and b.Begin_time>=current_date and b.End_time<(current_date+interval '1d') and substring(f.facilitycode from 1 for 2) in('NR','WK','ST','PV')  and f.status<>'-1000'")
//    int getLineOnDutyCount(@Param("schema_name") String schema_name);
//
//    //按紧急度查询维修工单数量
//    @Select("select w.priority_level as name,count(w.sub_work_code) as value from ${schema_name}._sc_works_detail w " +
//            "left join ${schema_name}._sc_works  wr on wr.work_code=w.work_code  "+
//            "left join ${schema_name}._sc_work_type  wt on wt.id=w.work_type_id  "+
//            "where wr.status <> 900 and w.is_main=1 and wt.business_type_id=1 ${condition} " +
//            "group by w.priority_level")
//    List<Map<String,Object>> getRepairNumByLevelCondition(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//
//    //按设备类型统计维修数量
//    @Select("select category_name as xdata, " +
//            "total_count as data1, " +
//            "finished_count as data2 " +
//            "from " +
//            "(  " +
//            "select  a.id,a.category_name, " +
//            "sum(case when task.status=60 then 1 else 0 end) as finished_count,  " +
//            "sum(case when task.status!=900 and task.status is not null then 1 else 0 end) as total_count  " +
//            "from " +
//            "( " +
//            "select a.id,a.category_name  " +
//            "from ${schema_name}._sc_asset_category a   " +
//            "where a.isuse='1' " +
//            ") a " +
//            "left join ( " +
//            "select wk.occur_time,wkd.receive_account,wk.status,dv.category_id " +
//            "from ${schema_name}._sc_works wk " +
//            "left join ${schema_name}._sc_works_detail wkd on wk.work_code=wkd.work_code " +
//            "left join ${schema_name}._sc_asset dv on wk.relation_id=dv._id  " +
//            "left join ${schema_name}._sc_work_type  wt on wt.id=wk.work_type_id  "+
//            "where wk.status!=900 and wkd.is_main=1 and to_char(wk.occur_time,'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "') and  wt.business_type_id=1 " +
//            ") task " +
//            "on task.category_id=a.id " +
//            "group BY a.id,a.category_name" +
//            ") topfoo " +
//            "where total_count >0 " +
//            "order by total_count desc " )
//    List<Map<String,Object>> getCategoryAndRepairCountList(@Param("schema_name") String schema_name);
//
//    //按客户查询维修工单数量
//    @Select("select f.title  as xdata,count(wr.work_code) as data from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_works  wr on wr.facility_id=f.id  "+
//            "left join ${schema_name}._sc_work_type  wt on wt.id=wr.work_type_id  "+
//            "where wr.status <> 900 and wt.business_type_id=1 ${condition} " +
//            "group by f.title")
//    List<Map<String,Object>> getRepairNumByFacilityCondition(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查询维修中人员信息
//    @Select("select s.*,up.location from (select a.username,a.account,sa.status,f.title as tasks " +
//            "from ${schema_name}._sc_user a   " +
//            "left join ${schema_name}._sc_works_detail wd on wd.receive_account=a.account " +
//            "left join ${schema_name}._sc_works wr on wd.work_code=wr.work_code " +
//            "left join ${schema_name}._sc_facilities f on  f.id=wr.facility_id " +
//            "left join ${schema_name}._sc_status sa on sa.id=wr.status " +
//            "where wr.status!=900 and wd.is_main=1 and wr.status!=60  and a.status<>-1 )s " +
//            "left join " +
//            "(select a.user_account,a.LOCATION FROM ${schema_name}._sc_user_trace " +
//            "A INNER JOIN (SELECT user_account,MAX (create_time) as create_time FROM ${schema_name}._sc_user_trace group by user_account) " +
//            "b ON A.user_account = b.user_account and A.create_time=b.create_time)up on up.user_account=s.account ")
//    List<Map<String,Object>> getUserAndRepairCountList(@Param("schema_name") String schema_name);
//
//    //查询维修人员轨迹
//    @Select("select u.username,ut.user_account, ARRAY_TO_STRING(ARRAY_AGG ( ut.location),'&')as locations,to_char(max(ut.create_time), '" + SqlConstant.SQL_TIME_FMT + "') as last_time from ${schema_name}._sc_user_trace ut  " +
//            "left join ${schema_name}._sc_user  u on ut.user_account=u.account  "+
//            "where to_char(ut.create_time,'" + SqlConstant.SQL_DATE_FMT + "')=to_char(now(),'" + SqlConstant.SQL_DATE_FMT + "') ${condition} " +
//            "group by ut.user_account,u.username ")
//    List<Map<String,Object>> getPoint(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//
//    //按客户查询维修工单数量
//    @Select("select (to_char(w.create_time,'" + SqlConstant.SQL_DATE_TIME_FMT + "' ) || '-' || u.username||'上报了'||w.title||' 编号'||w.work_code)as news " +
//            "from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type  wt on wt.id=w.work_type_id  "+
//            "left join ${schema_name}._sc_works_detail wd on wd.work_code=w.work_code " +
//            "left join ${schema_name}._sc_user  u on u.account=w.create_user_account  "+
//            "where wt.business_type_id=1 and w.status<20 and wd.receive_account ISNULL ${condition} " )
//    List<Map<String,Object>> getPendingDisposalCondition(@Param("schema_name") String schema_name, @Param("condition") String condition);
}