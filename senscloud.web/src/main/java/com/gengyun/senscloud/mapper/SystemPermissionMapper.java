package com.gengyun.senscloud.mapper;


import com.gengyun.senscloud.model.SystemPermissionResult;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SystemPermissionMapper {

//    /**
//     * 获取所有的菜单权限
//     *
//     * @return 菜单权限
//     */
//    @Select(" SELECT sp.menu_name,sp.icon,sp.url,sp.key,sp.id,sp.type,sp.icon,sp.parent_id" +
//            " FROM public.system_permission AS sp " +
//            " WHERE sp.type BETWEEN  1 AND 3 ORDER BY sp.index ASC ")
//    List<SystemPermissionResult> findAll();

//    /**
//     * 根据用户id获取所拥有的菜单权限
//     *
//     * @param schema_name 入参
//     * @param id          入参
//     * @return 菜单权限
//     */
//    @Select(" SELECT sp.menu_name,sp.key,sp.url,sp.icon,sp.type,sp.id,sp.parent_id,sp.lang_key FROM (SELECT DISTINCT rp.permission_id " +
//            " FROM ${schema_name}._sc_position_role AS pr " +
//            " LEFT JOIN ${schema_name}._sc_role_permission AS rp ON pr.role_id = rp.role_id " +
//            " AND pr.position_id IN ( SELECT position_id FROM ${schema_name}._sc_user_position WHERE user_id = #{id} )) AS t " +
//            " INNER JOIN   public.system_permission AS sp ON t.permission_id = sp.id " +
//            " INNER JOIN PUBLIC.company_menu AS cm ON cm.menu_id = t.permission_id and cm.company_id=#{company_id} " +
//            " WHERE  sp.available = 't' AND ( sp.type = 1 OR  sp.type = 3 )" +
//            " ORDER BY sp.index ASC ")
//    List<SystemPermissionResult> findByUserId(@Param("schema_name") String schema_name, @Param("id") String id, @Param("company_id") Long company_id);

    /**
     * 根据用户id获取所拥有的菜单权限
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 菜单权限
     */
    @Select(" SELECT DISTINCT rp.permission_id " +
            " FROM ${schema_name}._sc_position_role AS pr " +
            " LEFT JOIN ${schema_name}._sc_role_permission AS rp ON pr.role_id = rp.role_id AND pr.position_id " +
            " IN ( SELECT position_id FROM ${schema_name}._sc_user_position WHERE user_id =#{id}) ")
    List<String> findByUserId(@Param("schema_name") String schema_name, @Param("id") String id);

}
