package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 模板页面自定义下拉框选项数据管理
 */
@Mapper
public interface SelectOptionCustomMapper {

//    /**
//     * 根据库房code、角色id，查询所有库房管理员
//     *
//     * @param schema_name
//     * @param role_id
//     * @param stock_code
//     * @return
//     */
//    @Select("SELECT distinct u.account as code,u.username as desc, u.account as value, u.username as text, u.mobile as mobile FROM ${schema_name}._sc_user u " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_group_org uf ON uf.group_id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_stock_org so ON so.facility_id = uf.org_id " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON so.stock_code = s.stock_code and s.status=1 " +
//            "LEFT JOIN ${schema_name}._sc_user_role ur ON u.id = ur.user_id " +
//            "WHERE u.status=1 and s.stock_code = '${stock_code}' AND ur.role_id = '${role_id}' " +
//            "UNION " +
//            "SELECT distinct u.account as code,u.username as desc, u.account as value, u.username as text, u.mobile as mobile FROM ${schema_name}._sc_user u " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg ON sg.group_id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON sg.stock_code = s.stock_code and s.status=1 " +
//            "LEFT JOIN ${schema_name}._sc_user_role ur ON u.id = ur.user_id " +
//            "WHERE u.status=1 and s.stock_code = '${stock_code}' AND ur.role_id = '${role_id}' ")
//    List<Map<String, Object>> findAllStockUserListByRoleIdAndStockCode(@Param("schema_name") String schema_name, @Param("role_id") String role_id, @Param("stock_code") String stock_code);
//
//    /**
//     * 根据库房code、角色id，查询对应的用户 根据用户组找人
//     *
//     * @param schema_name
//     * @param role_id
//     * @param stock_code
//     * @return
//     */
//    @Select("SELECT distinct u.account as code,u.username as desc, u.account as value, u.username as text, u.mobile as mobile FROM ${schema_name}._sc_user u " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg ON sg.group_id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_stock s ON sg.stock_code = s.stock_code and s.status=1 " +
//            "LEFT JOIN ${schema_name}._sc_user_role ur ON u.id = ur.user_id " +
//            "WHERE u.status=1 and s.stock_code = '${stock_code}' AND ur.role_id = '${role_id}' ")
//    List<Map<String, Object>> findStockUserListByRoleIdAndStockCode(@Param("schema_name") String schema_name, @Param("role_id") String role_id, @Param("stock_code") String stock_code);
//
//    /**
//     * 库房（用户组、客户）下拉框列表信息
//     *
//     * @param schema_name
//     * @param facilityCondition
//     * @param groupCondition
//     * @return
//     */
//    @Select("SELECT * FROM (SELECT 'f' || CAST ( ID AS VARCHAR ) AS code, 'f' || CAST ( ID AS VARCHAR ) AS value, 'f' || CAST ( parentid AS VARCHAR ) AS parentid, short_title AS DESC, short_title AS text, 'f' || CAST ( ID AS VARCHAR ) AS relation " +
//            "FROM ${schema_name}._sc_facilities f WHERE isuse = TRUE AND status > 0 AND org_type IN ( " + SqlConstant.FACILITY_INSIDE + ") " +
//            "AND EXISTS (SELECT s.stock_code FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_org org ON s.stock_code = org.stock_code WHERE s.status = 1 AND org.facility_id = f.ID ${facilityCondition}) " +
//            "ORDER BY ID " +
//            ") A " +
//            "UNION ALL SELECT * FROM ( " +
//            "SELECT 'g' || CAST ( ID AS VARCHAR ) AS code, 'g' || CAST ( ID AS VARCHAR ) AS value, 'g' || CAST ( parent_id AS VARCHAR ) AS parentid, group_name AS DESC, group_name AS text, 'g' || CAST ( ID AS VARCHAR ) AS relation " +
//            "FROM ${schema_name}._sc_group G WHERE EXISTS ( " +
//            "SELECT s.stock_code FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_group sg ON s.stock_code = sg.stock_code WHERE s.status = 1 AND sg.group_id = G.ID ${groupCondition} " +
//            ") ORDER BY group_code " +
//            ") b " +
//            "UNION ALL SELECT * FROM ( " +
//            "SELECT 's' || s.stock_code AS code, 's' || s.stock_code AS value, 'f' || CAST ( org.facility_id AS VARCHAR ) AS parentid, s.stock_name AS DESC, s.stock_name AS text, 's' || s.stock_code AS relation " +
//            "FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_org org ON s.stock_code = org.stock_code WHERE s.status = 1 ${facilityCondition} ORDER BY s.stock_code " +
//            ") C " +
//            "UNION ALL SELECT * FROM ( " +
//            "SELECT 's' || s.stock_code AS code, 's' || s.stock_code AS value, 'g' || CAST ( sg.group_id AS VARCHAR ) AS parentid, s.stock_name AS DESC, s.stock_name AS text, 's' || s.stock_code AS relation " +
//            "FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_group sg ON s.stock_code = sg.stock_code WHERE s.status = 1 ${groupCondition} ORDER BY s.stock_code " +
//            ") d")
//    List<Map<String, Object>> findStockFacilityGroupRight(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition, @Param("groupCondition") String groupCondition);
//
//    /**
//     * 库房（用户组、客户）下拉框列表信息【仅库房可以选择】
//     *
//     * @param schema_name
//     * @param facilityCondition
//     * @param groupCondition
//     * @return
//     */
//    @Select("SELECT t.*, t.code as value, t.desc as text FROM ( SELECT t.*, " +
//            "(" +
//            "select count(cnt.code) from " +
//            "( SELECT * FROM (SELECT 'f' || CAST ( ID AS VARCHAR ) AS code, 'f' || CAST ( parentid AS VARCHAR ) AS parentid, short_title AS DESC " +
//            "FROM ${schema_name}._sc_facilities f WHERE isuse = TRUE AND status > 0 AND org_type IN ( " + SqlConstant.FACILITY_INSIDE + " ) " +
//            "AND EXISTS (SELECT s.stock_code FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_org org ON s.stock_code = org.stock_code WHERE s.status = 1 AND org.facility_id = f.ID ${facilityCondition}) " +
//            ") A " +
//            "UNION SELECT * FROM ( " +
//            "SELECT 'g' || CAST ( ID AS VARCHAR ) AS code, 'g' || CAST ( parent_id AS VARCHAR ) AS parentid, group_name AS DESC " +
//            "FROM ${schema_name}._sc_group G WHERE EXISTS ( " +
//            "SELECT s.stock_code FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_group sg ON s.stock_code = sg.stock_code WHERE s.status = 1 AND sg.group_id = G.ID ${groupCondition} " +
//            ")) b " +
//            "UNION SELECT * FROM ( " +
//            "SELECT s.stock_code AS code,'f' || CAST ( org.facility_id AS VARCHAR ) AS parentid, s.stock_name AS DESC " +
//            "FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_org org ON s.stock_code = org.stock_code WHERE s.status = 1 ${facilityCondition} " +
//            ") C " +
//            "UNION SELECT * FROM ( " +
//            "SELECT s.stock_code AS code, 'g' || CAST ( sg.group_id AS VARCHAR ) AS parentid, s.stock_name AS DESC " +
//            "FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_group sg ON s.stock_code = sg.stock_code WHERE s.status = 1 ${groupCondition}" +
//            ") d ) cnt ${cntCdn} " +
//            ")" +
//            "as child_count " +
//            "FROM ( SELECT * FROM (SELECT 'f' || CAST ( ID AS VARCHAR ) AS code, 'f' || CAST ( parentid AS VARCHAR ) AS parentid, short_title AS DESC, 'f' || CAST ( ID AS VARCHAR ) AS relation, 'disabled'::VARCHAR AS disabled " +
//            "FROM ${schema_name}._sc_facilities f WHERE isuse = TRUE AND status > 0 AND org_type IN ( " + SqlConstant.FACILITY_INSIDE + " ) " +
//            "AND EXISTS (SELECT s.stock_code FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_org org ON s.stock_code = org.stock_code WHERE s.status = 1 AND org.facility_id = f.ID ${facilityCondition}) " +
//            "ORDER BY ID " +
//            ") A ${condition} " +
//            "UNION ALL SELECT * FROM ( " +
//            "SELECT 'g' || CAST ( ID AS VARCHAR ) AS code, 'g' || CAST ( parent_id AS VARCHAR ) AS parentid, group_name AS DESC, 'g' || CAST ( ID AS VARCHAR ) AS relation, 'disabled'::VARCHAR AS disabled " +
//            "FROM ${schema_name}._sc_group G WHERE EXISTS ( " +
//            "SELECT s.stock_code FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_group sg ON s.stock_code = sg.stock_code WHERE s.status = 1 AND sg.group_id = G.ID ${groupCondition} " +
//            ") ORDER BY group_code " +
//            ") b ${condition} " +
//            "UNION ALL SELECT * FROM ( " +
//            "SELECT s.stock_code AS code,'f' || CAST ( org.facility_id AS VARCHAR ) AS parentid, s.stock_name AS DESC,'s' || s.stock_code AS relation, 'select'::VARCHAR AS disabled " +
//            "FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_org org ON s.stock_code = org.stock_code WHERE s.status = 1 ${facilityCondition} ORDER BY s.stock_code " +
//            ") C ${condition} " +
//            "UNION ALL SELECT * FROM ( " +
//            "SELECT s.stock_code AS code, 'g' || CAST ( sg.group_id AS VARCHAR ) AS parentid, s.stock_name AS DESC, 's' || s.stock_code AS relation, 'select'::VARCHAR AS disabled " +
//            "FROM ${schema_name}._sc_stock s INNER JOIN ${schema_name}._sc_stock_group sg ON s.stock_code = sg.stock_code WHERE s.status = 1 ${groupCondition} ORDER BY s.stock_code " +
//            ") d ${condition} ) t ) t where t.disabled <> 'disabled' or t.child_count > 0 ")
//    List<Map<String, Object>> findStockTreeRight(@Param("schema_name") String schema_name, @Param("facilityCondition") String facilityCondition,
//                                                 @Param("groupCondition") String groupCondition, @Param("condition") String condition, @Param("cntCdn") String cntCdn);
//
//    /**
//     * 取货币单位
//     *
//     * @param schemaName
//     * @param companyId
//     * @param langKey
//     * @param sqlWhere
//     * @return
//     */
//    @Select("select t.id as code, t.id as value, coalesce(t.currency_code_lang, t.currency_code) as desc, t.currency_code, t.currency_sign, t.currency_sign as text " +
//            "from " +
//            "   (" +
//            "      select t.*, " +
//            "           (select (c.resource->>t.currency_code)::jsonb->>'${langKey}' from public._sc_company c where c._id = #{companyId}) " +
//            "       as currency_code_lang " +
//            "      from ${schema_name}._sc_currency t ${sqlWhere} " +
//            "   ) t " +
//            "order by t.id ")
//    List<Map<String, Object>> getCurrencyList(@Param("schema_name") String schemaName, @Param("companyId") Long companyId, @Param("langKey") String langKey, @Param("sqlWhere") String sqlWhere);
//
//
//    /**
//     * 根据主键获取用户首个位置信息
//     *
//     * @param schema_name
//     * @param userId
//     * @return
//     */
//    @Select("select u.account, u.username, u.mobile, t2.id as fid, t2.parentid as fp_id, t2.title as fnm,t2.facility_no as fno " +
//            "from ${schema_name}._sc_user u, " +
//            "${schema_name}._sc_user_group t1, " +
//            "${schema_name}._sc_group sg, " +
//            "${schema_name}._sc_group_org sgo, " +
//            "${schema_name}._sc_facilities t2 " +
//            "WHERE t1.group_id = sg.ID AND sg.ID = sgo.group_id AND sgo.org_id = t2.ID AND t1.user_id = u.id AND u.id=#{userId} order by t2.id desc limit 1")
//    Map<String, Object> getUserFirstFacilityInfoById(@Param("schema_name") String schema_name, @Param("userId") String userId);
}
