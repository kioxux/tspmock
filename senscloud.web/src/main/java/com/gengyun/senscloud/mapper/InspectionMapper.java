package com.gengyun.senscloud.mapper;

public interface InspectionMapper {
//    /**
//     * 查询巡检检测项
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_inspection_item  " +
//            "where 1=1 " +
//            " order by \"order\"")
//    List<InspectionItemData> inspectionItemData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询巡检表根据设备编号
//     */
//    @Select("SELECT ins.*,ft.title as facility_name " +
//            " FROM ${schema_name}._sc_inspection ins " +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join  ${schema_name}._sc_inspection_area lsx on ins.area_code=lsx.area_code" +
//            " where lsx.area_code='${strcode}'" +
//            " and ins.status in (0,90)" +
//            " and ins.inspection_account='${account}'" +
//            " order by ins.inspection_time asc" +
//            " limit 1  ")
//    InspectionData inspectionData(@Param("schema_name") String schema_name, @Param("strcode") String strcode, @Param("account") String account);
//
//    /**
//     * 插入巡检表
//     */
//    @Insert("INSERT INTO ${schema_name}._sc_inspection(" +
//            "inspection_code,  inspection_account, status, fault_number, facility_id,  inspection_time, " +
//            "inspection_result, createtime, create_user_account, area_code, inspection_no, begin_time,is_abnormal,abnormal_files)" +
//            "VALUES (#{i.inspection_code}, #{i.inspection_account}, #{i.status}, #{i.fault_number}," +
//            "#{i.facility_id}, #{i.inspection_time}::timestamp, #{i.inspection_result}::jsonb," +
//            "#{i.createtime}, #{i.create_user_account}, #{i.area_code}, #{i.inspection_no}, " +
//            "#{i.begin_time},#{i.is_abnormal},#{i.abnormal_files} )")
//    int insertInspectionData(@Param("schema_name") String schema_name,
//                             @Param("i") InspectionData inspectionData);
//
//    /**
//     * 查询巡检表根据单号
//     */
//    @Select("SELECT ins.*,ft.title as facility_name,u.username as inspection_account_name,area.area_name " +
//            " FROM ${schema_name}._sc_inspection ins  " +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join ${schema_name}._sc_user u on ins.inspection_account=u.account " +
//            " left join ${schema_name}._sc_inspection_area area on ins.area_code=area.area_code " +
//            " where ins.inspection_code='${inspection_code}' ")
//    InspectionData inspectionDataByNo(@Param("schema_name") String schema_name, @Param("inspection_code") String inspection_code);
//
//    /**
//     * 更新巡检表
//     */
//    @Update("update ${schema_name}._sc_inspection " +
//            " set inspection_account= '${inspection_account}', status=${status}, fault_number='${fault_number}'," +
//            " inspection_result='${inspection_result}'::jsonb , is_abnormal='${is_abnormal}', abnormal_files='${abnormal_files}'" +
//            " where inspection_code='${inspection_code}'")
//    int updateInspectionData(InspectionData inspectionData);
//
//
//    //更新编辑开始巡检时间，为手机上点击设备的巡检开始
//    @Update("update ${schema_name}._sc_inspection set " +
//            "begin_time=#{r.begin_time} " +
//            "where maintain_code=#{r.maintain_code} ")
//    int EditInspectionBeginTime(@Param("schema_name") String schema_name, @Param("r") InspectionData inspectionData);
//
//
//    //更新巡检人员
//    @Update("update ${schema_name}._sc_inspection set " +
//            "inspection_account=#{r.inspection_account} " +
//            "where maintain_code=#{r.maintain_code} ")
//    int EditInspectionMan(@Param("schema_name") String schema_name, @Param("r") InspectionData inspectionData);
//
//    /**
//     * 查询巡检表最新开始的日期
//     */
//    @Select("SELECT distinct to_char(ins.inspection_time,'" + SqlConstant.SQL_DATE_FMT + "') as insdate " +
//            " FROM ${schema_name}._sc_inspection ins" +
//            " where inspection_time is not null and  ${condition} " +
//            " group by ins.inspection_time" +
//            " order by insdate desc" +
//            " limit ${page} offset ${begin} ")
//    List<InspectionListAppResult> inspectionListAppResult(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    /**
//     * 根据日期查询是否有未提交的
//     */
//    @Select("SELECT ins.inspection_code, ins.inspection_account, ins.status, ins.fault_number, ins.facility_id, ins.remark, ins.inspection_time," +
//            "ins.createtime,ins.create_user_account, ins.area_code, ins.begin_time, ins.inspection_no, ins.is_abnormal, ins.abnormal_files," +
//            "ft.title as facility_name ,area.area_code,area.area_name" +
//            " FROM ${schema_name}._sc_inspection ins" +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join  ${schema_name}._sc_inspection_area area on ins.area_code=area.area_code" +
//            " where to_char(ins.inspection_time,'" + SqlConstant.SQL_DATE_FMT + "') ='${insdate}'" +
//            " and ins.status in (0,90)" +
//            " and ${condition} ")
//    List<InspectionData> InspectionDataByDate(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("insdate") String insdate);
//
//    /**
//     * 根据日期查询list
//     */
//    @Select("SELECT ins.inspection_code, ins.inspection_account, ins.status, ins.fault_number, ins.facility_id, ins.remark, ins.inspection_time," +
//            "ins.createtime,ins.create_user_account, ins.area_code, ins.begin_time, ins.inspection_no, ins.is_abnormal, ins.abnormal_files," +
//            "lsx.area_code,lsx.area_name,ft.title as facility_name,u.username as inspection_account_name " +
//            " FROM ${schema_name}._sc_inspection ins " +
//            " left join ${schema_name}._sc_inspection_area lsx on ins.area_code=lsx.area_code" +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join ${schema_name}._sc_user u on ins.inspection_account=u.account " +
//            " where to_char(ins.inspection_time,'" + SqlConstant.SQL_DATE_FMT + "') ='${insdate}'" +
//            " and ${condition} " +
//            " order by ins.fault_number desc" +
//            " limit ${page} offset ${begin}")
//    List<InspectionData> InspectionDataList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("insdate") String insdate, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    ////////////////////////////////web//////////////////////////////////
//
//    /**
//     * 插入巡更点
//     */
//    @Insert("INSERT INTO ${schema_name}._sc_inspection_area(" +
//            "area_code, area_name, facility_id, location_xy, \"Remark\", createtime, create_user_account, is_use)" +
//            "VALUES ('${area_code}','${area_name}', ${facility_id},  '${location_xy}', '${Remark}', '${createtime}', '${create_user_account}', ${is_use})")
//    int insertInspectionAreaData(InspectionAreaData inspectionAreaData);
//
//
//    /**
//     * 根据设备地图根xy查询是否有存在
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_inspection_area  " +
//            "where facility_id=${facility_id}  and location_xy[0]=${x}  and location_xy[1]=${y} ")
//    InspectionAreaData InspectionAreaDatabyxy(@Param("schema_name") String schema_name, @Param("facility_id") Integer facility_id, @Param("x") Double x, @Param("y") Double y);
//
//    /**
//     * 根据设备地图根xy查询是否有存在
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_inspection_area  " +
//            "where area_code='${area_code}'   ")
//    InspectionAreaData InspectionAreaDatabyarea(@Param("schema_name") String schema_name, @Param("area_code") String area_code);
//
//
//    /**
//     * 根据设备地图查询所有点
//     */
//    @Select("SELECT i.*,ft.title as site_name  FROM ${schema_name}._sc_inspection_area i  " +
//            "left join ${schema_name}._sc_facilities ft on i.facility_id=ft.id " +
//            "where facility_id=${facility_id}   ")
//    List<InspectionAreaData> InspectionAreaDatabyfac(@Param("schema_name") String schema_name, @Param("facility_id") Integer facility_id);
//
//    /**
//     * 计划排程查询场地查询点巡检点
//     */
//    @Select("SELECT i.*,ft.title as site_name  FROM ${schema_name}._sc_inspection_area i  " +
//            "left join ${schema_name}._sc_facilities ft on i.facility_id=ft.id " +
//            "where 1=1 ${condition} limit ${page} offset ${begin}")
//    List<InspectionAreaData> queryByPlanInspecctionAreaList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    /**
//     * 查询所有点
//     */
//    @Select("SELECT i.*,ft.title as site_name FROM ${schema_name}._sc_inspection_area i  " +
//            "left join ${schema_name}._sc_facilities ft on i.facility_id=ft.id " +
//            "where is_use = true")
//    List<InspectionAreaData> findAllInspectionAreaData(@Param("schema_name") String schema_name);
//
//
//    /**
//     * 更新巡更点表
//     */
//    @Update("update ${schema_name}._sc_inspection_area " +
//            " set area_name= '${area_name}', \"Remark\"='${Remark}'," +
//            " is_use=${is_use} " +
//            " where area_code='${area_code}'")
//    int updateInspectionAreaData(InspectionAreaData inspectionAreaData);
//
//    /**
//     * 删除巡更点
//     */
//    @Delete("Delete from ${schema_name}._sc_inspection_area  " +
//            "where facility_id=${facility_id} and  area_code='${area_code}' ")
//    int DeleteionAreaDatabyfac(@Param("schema_name") String schema_name, @Param("facility_id") Integer facility_id, @Param("area_code") String area_code);
//
//    /**
//     * 更新巡更点表的xy
//     */
//    @Update("update ${schema_name}._sc_inspection_area " +
//            "\t set location_xy='${location_xy}'" +
//            "  where facility_id=${facility_id}  and location_xy[0]=${x}  and location_xy[1]=${y} ")
//    int updateInspectionAreaxy(@Param("schema_name") String schema_name, @Param("facility_id") Integer facility_id, @Param("x") Double x, @Param("y") Double y, @Param("location_xy") String location_xy);
//
//    /**
//     * 关闭过期巡更
//     */
//    @Update("update ${schema_name}._sc_inspection set status=100 " +
//            "where status!=60 and date_part('epoch',current_timestamp - #{endTime})>0 ")
//    int closeInspectionForDue(@Param("schema_name") String schema_name,
//                              @Param("endTime") Timestamp endTime);
//
//    /**
//     * 查询巡更轮次
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_inspection_times")
//    List<InspectionTimes> findAllInspectionTimes(@Param("schema_name") String schema_name);
//
//    /**
//     * 按轮次统计当天巡更单
//     */
//    @Select("SELECT COUNT(*) FROM ${schema_name}._sc_inspection " +
//            "where to_char(createtime, '" + SqlConstant.SQL_DATE_FMT + "') = to_char(current_timestamp, '" + SqlConstant.SQL_DATE_FMT + "') " +
//            "and inspection_no = #{inspectionNo}")
//    int countInspectionsTodayByNo(@Param("schema_name") String schema_name,
//                                  @Param("inspectionNo") int inspectionNo);
//
//
//
//    /**
//     * 根据各个条件查询list
//     */
//    @Select("SELECT ins.inspection_code, ins.inspection_account, ins.status, ins.fault_number, ins.facility_id, ins.remark, ins.inspection_time," +
//            "ins.createtime,ins.create_user_account, ins.area_code, ins.begin_time, ins.inspection_no, ins.is_abnormal, ins.abnormal_files," +
//            "lsx.area_code,lsx.area_name,ft.title as facility_name,pf.title as parent_name,u.username as inspection_account_name " +
//            " FROM ${schema_name}._sc_inspection ins " +
//            " left join ${schema_name}._sc_inspection_area lsx on ins.area_code=lsx.area_code" +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " left join ${schema_name}._sc_user u on ins.inspection_account=u.account " +
//            " where 1=1" +
//            " and ${condition} " +
//            " order by ins.inspection_code desc" +
//            " limit ${page} offset ${begin}")
//    List<InspectionData> InspectionDataListweb(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//
//    /**
//     * 根据查询list总数
//     */
//    @Select("SELECT count(ins.inspection_code) " +
//            " FROM ${schema_name}._sc_inspection ins " +
//            " left join ${schema_name}._sc_inspection_area lsx on ins.area_code=lsx.area_code" +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join ${schema_name}._sc_user u on ins.inspection_account=u.account " +
//            " where 1=1" +
//            " and ${condition} ")
//    int InspectionDataListwebtotal(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     * 根据inscode查询list
//     */
//    @Select("SELECT ins.*, lsx.area_code,lsx.area_name,ft.title as facility_name,u.username as inspection_account_name " +
//            " FROM ${schema_name}._sc_inspection ins " +
//            " left join ${schema_name}._sc_inspection_area lsx on ins.area_code=lsx.area_code" +
//            " left join ${schema_name}._sc_facilities ft on ins.facility_id=ft.id " +
//            " left join ${schema_name}._sc_user u on ins.inspection_account=u.account " +
//            " where ins.inspection_code='${inspection_code}' ")
//    InspectionData InspectionDatabycode(@Param("schema_name") String schema_name, @Param("inspection_code") String inspection_code);
//
//    /*作废巡检单据*/
//    @Update("UPDATE ${schema_name}._sc_inspection  set status=900 where inspection_code = '${inspection_code}'")
//    int cancelInspection(@Param("schema_name") String schema_name, @Param("inspection_code") String inspection_code);
//
//
//    /*巡检完成数量*/
//    @Select("SELECT SUM(case when ins.status=60 and ins.inspection_time> current_date then 1 else 0 end) as inspection " +
//            " FROM ${schema_name}._sc_inspection ins " )
//    FullScreenData getPlannedAndCompleteInspection(@Param("schema_name") String schema_name);
//    /*巡检完成数量*/
//    @Select("SELECT SUM(case when spot.status=60 and spot.spot_time> current_date  then 1 else 0 end) as spot " +
//            " FROM ${schema_name}._sc_spotcheck spot " )
//    FullScreenData getPlannedAndCompleteSpotInspection(@Param("schema_name") String schema_name);
//
//    /**
//     * 根据设备地图查询所有点
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_inspection_area i  " +
//            "left join ${schema_name}._sc_facilities ft on i.facility_id=ft.id " +
//            "where facility_id in (${facilityIds}) and area_code = #{area_code}")
//    int getInspectionAreaData(@Param("schema_name") String schema_name, @Param("facilityIds") String facilityIds, @Param("area_code") String area_code);
}




