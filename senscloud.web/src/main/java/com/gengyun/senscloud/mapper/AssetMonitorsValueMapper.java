package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 初始化元数据表
 */
@Mapper
public interface AssetMonitorsValueMapper {
//
//    @Select("select * from ${schema_name}._sc_asset_monitor_demo")
//    List<AssetMonitorHistoryData> getAssetMonitorValues(@Param("schema_name") String schema_name);
//
//    @Insert("insert into ${schema_name}._sc_asset_monitor_demo(asset_code,monitor_value1,monitor_value2,monitor_value3,monitor_value4,monitor_value5,monitor_value6,gather_time) " +
//            "values( " +
//            "#{r.asset_code},#{r.monitor_value1},#{r.monitor_value2},#{r.monitor_value3},#{r.monitor_value4},#{r.monitor_value5},#{r.monitor_value6},#{r.gather_time})")
//    int insertAssetMonitorValue(@Param("schema_name") String schema_name,@Param("r") AssetMonitorHistoryData monitor);
//
//    @Select("   SELECT  * FROM ${schema_name}._sc_asset_monitor where asset_code=#{assetcode}    and monitor_name=#{monitorname}\n" +
//            "    and   gather_time   >= #{begtime} and    gather_time <=#{endtime}")
//    List<AssetMonitorData> getAssetMonitorValuesByName(@Param("schema_name") String schema_name, @Param("assetcode") String assetcode,
//                                                       @Param("monitorname") String monitorname , @Param("begtime") String begtime, @Param("endtime") String endtime);
//
//    @Select("   SELECT   distinct * FROM ${schema_name}._sc_asset_monitor_current where asset_code=#{assetcode}  ")
//    List<AssetMonitorData> getAssetMonitorName(@Param("schema_name") String schema_name,@Param("assetcode") String assetcode);


    @Select("SELECT DISTINCT a.id,a.asset_name,am.asset_code,am.monitor_name,am.monitor_value_unit,am.monitor_value,am.gather_time " +
            "FROM ${schema_name}._sc_asset A " +
            "LEFT JOIN ${schema_name}._sc_asset_monitor_current am ON a.poc_device_id::VARCHAR = am.asset_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON A.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_metadata_monitors mm ON mm.asset_category_id = A.category_id AND mm.is_use = '1' " +
            "LEFT JOIN ${schema_name}._sc_unit u ON u.ID = mm.unit_id " +
            "WHERE A.position_code = #{positionCode} AND A.category_id = #{positionId}::int ")
    List<Map<String, Object>> findIotAssetPoints(@Param("schema_name") String schema_name, @Param("positionCode") String positionCode, @Param("positionId") String categoryId);

    /**
     * 查询设备监控实时数据列表总条数
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECT count(DISTINCT m.id) " +
            "FROM ${schema_name}._sc_asset_monitor_current m " +
            "${condition}")
    int findCountAssetCurrentMonitorList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);

    /**
     * 查询设备监控实时数据列表
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECt DIStINCt m.id,m.monitor_name, m.monitor_value, m.monitor_value_unit,m.gather_time " +
            "FROM ${schema_name}._sc_asset_monitor_current m  " +
            "${condition}")
    List<Map<String, Object>> findAssetCurrentMonitorList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);

    /**
     * 查询设备监控历史数据列表总条数
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECT count(DISTINCT m.id) " +
            "FROM ${schema_name}._sc_asset_monitor m " +
            "${condition}")
    int findCountAssetHistoryMonitorList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);

    /**
     * 查询设备监控历史数据列表
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECt DIStINCt m.id,m.monitor_name, m.monitor_value, m.monitor_value_unit,m.gather_time " +
            "FROM ${schema_name}._sc_asset_monitor m  " +
            "${condition}")
    List<Map<String, Object>> findAssetHistoryMonitorList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);

    /**
     * 根据设备id查询设备的orgId,deployment,grouopid,
     *
     * @param schema_name
     * @param assetId
     * @param name
     * @return
     */
    @Select(" SELECT DISTINCT d.group_code as org_Id,ap.poc_deployment_id,a.poc_device_id,a.poc_controller_id,f.control_point_id as point_id, f.is_state as is_state " +
            " FROM ${schema_name}._sc_asset a " +
            " INNER JOIN ${schema_name}._sc_asset_position ap ON a.position_code = ap.position_code " +
            " INNER JOIN ${schema_name}._sc_role_asset_position a1  ON a1.asset_position_code = ap.position_code " +
            " INNER JOIN ${schema_name}._sc_position_role b on  a1.role_id = b.role_id " +
            " INNER JOIN ${schema_name}._sc_position c on b.position_id = c.id " +
            " INNER JOIN ${schema_name}._sc_group d on c.group_id = d.id " +
            " INNER JOIN ${schema_name}._sb_poc_points f ON f.device_id = a.poc_device_id and f.deployment_id=ap.poc_deployment_id " +
            " WHERE a.id = #{assetId} and f.name = #{name} ")
    List<Map<String, Object>> findIotAssetsPointsByAssetId(@Param("schema_name") String schema_name, @Param("assetId") String assetId, @Param("name") String name);

    /**
     * 根据设备id查询设备的orgId,deployment,grouopid,
     *
     * @param schema_name
     * @param assetId
     * @param name
     * @return
     */
    @Select(" SELECT DISTINCT d.group_code as org_Id,ap.poc_deployment_id,a.poc_device_id,a.poc_controller_id,f.id as point_id " +
            " FROM ${schema_name}._sc_asset a " +
            " INNER JOIN ${schema_name}._sc_asset_position ap ON a.position_code = ap.position_code " +
            " INNER JOIN ${schema_name}._sc_role_asset_position a1  ON a1.asset_position_code = ap.position_code " +
            " INNER JOIN ${schema_name}._sc_position_role b on  a1.role_id = b.role_id " +
            " INNER JOIN ${schema_name}._sc_position c on b.position_id = c.id " +
            " INNER JOIN ${schema_name}._sc_group d on c.group_id = d.id " +
            " INNER JOIN sc_com_190._sb_poc_points f ON f.device_id = A.poc_device_id " +
            " WHERE a.id = #{assetId} and f.name = #{name} ")
    List<Map<String, Object>> findIotAssetByAssetId(@Param("schema_name") String schema_name, @Param("assetId") String assetId, @Param("name") String name);
    /**
     * 根据设备id查询设备的orgId,deployment
     *
     * @param schema_name
     * @param assetId
     * @return
     */@Select(" SELECT is_state from FROM ${schema_name}._sb_poc_points where device_id = #{assetId}::int")
    String findIotAssetsIsStateByAssetId(@Param("schema_name") String schema_name, @Param("assetId") String assetId);


}
