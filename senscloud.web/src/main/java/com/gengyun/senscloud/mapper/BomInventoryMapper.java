package com.gengyun.senscloud.mapper;

public interface BomInventoryMapper {
//
//    /**
//     * 根据code查询备件盘点数据
//     *
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_inventory where inventory_code = #{inventoryCode} ")
//    Map<String, Object> queryBomInventoryByCode(@Param("schema_name") String schemaName, @Param("inventoryCode") String inventoryCode);
//
//    /**
//     *  根据主键code，查询盘点位置信息
//     * @param schemaName
//     * @param subInventoryCode
//     * @return
//     */
//    @Select("select bis.*,s.stock_name from ${schema_name}._sc_bom_inventory_stock bis " +
//            "left join ${schema_name}._sc_stock s on bis.stock_code = s.stock_code " +
//            "where bis.sub_inventory_code = #{subInventoryCode} ")
//    Map<String, Object> queryBomInventoryStockByCode(@Param("schema_name") String schemaName, @Param("subInventoryCode") String subInventoryCode);
//
//    /**
//     *  根据主键code，查询盘点位置信息列表
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_inventory_stock where inventory_code = #{inventoryCode} ")
//    List<Map<String, Object>> queryBomInventoryStockListByCode(@Param("schema_name") String schemaName, @Param("inventoryCode") String inventoryCode);
//
//    /**
//     * 备件盘点列表查询
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT bi.* FROM ${schema_name}._sc_bom_inventory bi where 1=1 ${condition} order by bi.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getBomInventoryList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 备件盘点列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) FROM ${schema_name}._sc_bom_inventory bi where 1=1 ${condition} ")
//    int countBomInventoryList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     *修改备件盘点状态、完成时间
//     * @param schema_name
//     * @param inventoryCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom_inventory set status = #{status},finished_time = #{finishedTime} where inventory_code = #{inventoryCode}")
//    int updateBomInventoryStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("inventoryCode") String inventoryCode, @Param("finishedTime")Timestamp finishedTime);
//
//    /**
//     * 修改盘点位置信息状态、完成时间
//     * @param schema_name
//     * @param status
//     * @param condition
//     * @param finishedTime
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom_inventory_stock set status = #{status},finished_time = #{finishedTime} where 1=1 ${condition}")
//    int updateBomInventoryStockStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("finishedTime")Timestamp finishedTime, @Param("condition")String condition);
//
//    /**
//     * 备件盘点位置列表查询
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT bs.inventory_code,bs.sub_inventory_code,bs.finished_time,bs.record_count,bs.inventory_count,bs.balance_count,bs.status,bi.inventory_name,bi.begin_time,s.stock_name,s.stock_code,bs.duty_man, " +
//            " array_to_string(array_agg(f.short_title),',') as facility_name " +
//            " FROM ${schema_name}._sc_bom_inventory_stock bs " +
//            " LEFT JOIN ${schema_name}._sc_user u ON bs.duty_man = u.account " +
//            " LEFT JOIN ${schema_name}._sc_bom_inventory bi ON bi.inventory_code = bs.inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_stock s ON s.stock_code = bs.stock_code " +
//            " LEFT JOIN ${schema_name}._sc_stock_org so ON so.stock_code = s.stock_code " +
//            " LEFT JOIN ${schema_name}._sc_facilities f  ON f.id = so.facility_id " +
//            " where bs.inventory_code = '${inventoryCode}' ${condition} " +
//            " GROUP BY bs.inventory_code,bs.sub_inventory_code,bs.duty_man,bs.finished_time,bs.record_count,bs.inventory_count,bs.balance_count,bs.status,bi.inventory_name,bi.begin_time,s.stock_name,s.stock_code,bs.create_time " +
//            " order by bs.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getBomInventoryStockList(@Param("schema_name") String schema_name, @Param("inventoryCode") String inventoryCode, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 备件盘点位置列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) FROM ${schema_name}._sc_bom_inventory_stock bs " +
//            " LEFT JOIN ${schema_name}._sc_user u ON bs.duty_man = u.account " +
//            " where bs.inventory_code = '${inventoryCode}' ${condition} ")
//    int countBomInventoryStockList(@Param("schema_name") String schema_name, @Param("inventoryCode") String inventoryCode, @Param("condition") String condition);
//
//    /**
//     * 备件盘点位置列表查询
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT sd.sub_inventory_code,i.inventory_name,sd.finished_time,u.username,b.bom_model,b.bom_name,sd.bom_code,sd.in_status,sd.deal_result,sd.material_code,sd.stock_code," +
//            "   sd.record_count,sd.inventory_count,b.brand_name,f.short_title as supplier_name,bt.type_name,ss.currency_id,cur.currency_sign as bom_currency,un.unit_name,ss.stock_name,bs.quantity,bte.id as bom_model_id " +
//            " FROM ${schema_name}._sc_bom_inventory_stock_detail sd " +
//            " LEFT JOIN ${schema_name}._sc_bom_inventory i ON i.inventory_code = sd.inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_bom_inventory_stock s ON s.sub_inventory_code = sd.sub_inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_user u ON u.account = s.duty_man " +
//            " LEFT JOIN ${schema_name}._sc_bom b ON b.bom_code = sd.bom_code " +
//            " LEFT JOIN ${schema_name}._sc_facilities f ON b.supplier = f.id " +
//            " LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
//            " LEFT JOIN ${schema_name}._sc_unit un ON un.id = b.unit_id " +
//            " LEFT JOIN ${schema_name}._sc_bom_stock bs ON bs.bom_code = b.bom_code AND bs.material_code = b.material_code " +
//            " LEFT JOIN ${schema_name}._sc_stock ss ON ss.stock_code = bs.stock_code " +
//            " LEFT JOIN ${schema_name}._sc_currency cur ON ss.currency_id = cur.id " +
//            " LEFT JOIN ${schema_name}._sc_bom_template bte ON bte.bom_model = b.bom_model " +
//            " WHERE sd.sub_inventory_code = '${subInventoryCode}' ${condition} " +
//            " order by sd.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getBomInventoryStockDetailList(@Param("schema_name") String schema_name, @Param("subInventoryCode") String subInventoryCode, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 备件盘点位置列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT COUNT(1) FROM ${schema_name}._sc_bom_inventory_stock_detail sd " +
//            " LEFT JOIN ${schema_name}._sc_bom_inventory i ON i.inventory_code = sd.inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_bom_inventory_stock s ON s.sub_inventory_code = sd.sub_inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_user u ON u.account = s.duty_man " +
//            " LEFT JOIN ${schema_name}._sc_bom b ON b.bom_code = sd.bom_code " +
//            " LEFT JOIN ${schema_name}._sc_currency cur ON b.currency_id = cur.id " +
//            " LEFT JOIN ${schema_name}._sc_bom_stock bs ON bs.bom_code = b.bom_code AND bs.material_code = b.material_code " +
//            " WHERE sd.sub_inventory_code = '${subInventoryCode}' ${condition} ")
//    int countBomInventoryStockDetailList(@Param("schema_name") String schema_name, @Param("subInventoryCode") String subInventoryCode, @Param("condition") String condition);
//
//    /**
//     * 查询库房在库备件总数
//     * @param schema_name
//     * @param stockCode
//     * @return
//     */
//    @Select("SELECT SUM(t.quantity) FROM ( " +
//            "   SELECT DISTINCT bs.quantity,bs.bom_code,bs.material_code,bs.stock_code " +
//            "   FROM ${schema_name}._sc_bom_stock bs " +
//            "   LEFT JOIN ${schema_name}._sc_bom b ON bs.bom_code = b.bom_code AND bs.material_code = b.material_code " +
//            "   WHERE bs.stock_code IN (${stockCode}) AND b.isuse is true ${condition} " +
//	        ") t ")
//    int countStockBomQuantity(@Param("schema_name") String schema_name, @Param("stockCode") String stockCode, @Param("condition") String condition);
//
//    /**
//     * 更新备件盘点位置负责人
//     * @param schemaName
//     * @param dutyMan
//     * @param status
//     * @param subInventoryCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom_inventory_stock set duty_man = #{dutyMan},status=#{status} where sub_inventory_code = #{subInventoryCode} ")
//    int updateBomStockDutyMan(@Param("schema_name") String schemaName, @Param("dutyMan") String dutyMan, @Param("status") int status, @Param("subInventoryCode") String subInventoryCode);
//
//    /**
//     * 查询设备详情信息
//     * @param schemaName
//     * @param subInventoryCode
//     * @param stockCode
//     * @param bomCode
//     * @param materialCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_inventory_stock_detail where sub_inventory_code = #{subInventoryCode} " +
//            "and stock_code = #{stockCode} and bom_code = #{bomCode} and material_code = #{materialCode}")
//    Map<String, Object> queryBomInventoryStockDetailByCode(@Param("schema_name") String schemaName, @Param("subInventoryCode") String subInventoryCode,
//                                                           @Param("stockCode") String stockCode, @Param("bomCode") String bomCode, @Param("materialCode") String materialCode);
//
//    /**
//     * 更新盘点备件信息
//     * @param schemaName
//     * @param map
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_bom_inventory_stock_detail SET in_status = ${m.in_status} ,inventory_count=${m.inventory_count},finished_time=#{m.finished_time} " +
//            "WHERE inventory_code = '${m.inventory_code}' AND sub_inventory_code='${m.sub_inventory_code}' AND bom_code='${m.bom_code}' AND material_code='${m.material_code}'")
//    int updateBomInventoryStockDetail(@Param("schema_name") String schemaName, @Param("m") Map<String, Object> map);
//
//    /**
//     * 更新备件盘点表累计盘点数量
//     * @param schemaName
//     * @param inventoryCode
//     * @param inventoryCount
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_bom_inventory SET inventory_count = inventory_count + ${inventoryCount} ,balance_count=balance_count - ${inventoryCount} WHERE inventory_code = '${inventoryCode}' ")
//    int updateBomInventoryCount(@Param("schema_name") String schemaName, @Param("inventoryCode") String inventoryCode, @Param("inventoryCount") BigDecimal inventoryCount);
//
//    /**
//     * 更新盘点位置表累计盘点数量
//     * @param schemaName
//     * @param subInventoryCode
//     * @param inventoryCount
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_bom_inventory_stock SET inventory_count = inventory_count + ${inventoryCount} ,balance_count=balance_count - ${inventoryCount} WHERE sub_inventory_code = '${subInventoryCode}' ")
//    int updateBomInventoryStockCount(@Param("schema_name") String schemaName, @Param("subInventoryCode") String subInventoryCode, @Param("inventoryCount") BigDecimal inventoryCount);
//
//    /**
//     * 更新盘点备件出入库处理结果
//     * @param schemaName
//     * @param map
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_bom_inventory_stock_detail SET deal_result = ${m.deal_result}, finished_time= NOW() " +
//            "WHERE sub_inventory_code='${m.sub_inventory_code}' AND bom_code='${m.bom_code}' AND material_code='${m.material_code}'")
//    int updateBomInventoryStockDetailDealResult(@Param("schema_name") String schemaName, @Param("m") Map<String, Object> map);
//
//    /**
//     * 根据subInventoryCode, 查询所有盘点备件信息
//     * @param schema_name
//     * @param subInventoryCode
//     * @return
//     */
//    @Select("SELECT sd.* FROM ${schema_name}._sc_bom_inventory_stock_detail sd WHERE sd.sub_inventory_code = '${subInventoryCode}' ")
//    List<Map<String, Object>> queryBomInventoryStockDetailBySubInventoryCode(@Param("schema_name") String schema_name, @Param("subInventoryCode") String subInventoryCode);
//
//    /**
//     * 查询未完成的盘点位置信息数量
//     * @param schema_name
//     * @param inventoryCode
//     * @return
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_bom_inventory_stock WHERE inventory_code = #{inventoryCode} AND status <> " + StatusConstant.COMPLETED)
//    int countUnfinishedBomInventoryStock(@Param("schema_name") String schema_name, @Param("inventoryCode") String inventoryCode);
//
//    /**
//     * 查询指定的库房是否有盘点中的数据
//     * @param schema_name
//     * @param stockCodes
//     * @return
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_bom_inventory_stock WHERE stock_code in (${stockCodes}) AND (status = " + StatusConstant.PENDING +" OR status = "+ StatusConstant.PENDING_ORDER+ ") ")
//    int countDoingBomInventoryStock(@Param("schema_name") String schema_name, @Param("stockCodes") String stockCodes);
}
