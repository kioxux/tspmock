package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 客户表公共sql
 */
@Mapper
public interface FacilitiesCommonMapper {
//    /**
//     * 权限场地(默认查询org_type为1、2)
//     */
//    @Select(SqlConstant.FACILITY_INSIDE_LIST_BY_RIGHT_SQL + " ${condition} ORDER BY code, parentid DESC")
//    List<Map<String, Object>> getFacilityInsideListByRight(@Param("schema_name") String schemaName, @Param("facCdn") String facilityCondition, @Param("allFacCdn") String facilityAllCondition, @Param("condition") String condition);
//
//    /**
//     * 权限场地
//     */
//    @Select(SqlConstant.FACILITY_LIST_BY_RIGHT_SQL + " ${condition} ORDER BY code, parentid DESC")
//    List<Map<String, Object>> getFacilityListByRight(@Param("schema_name") String schemaName, @Param("facCdn") String facilityCondition, @Param("allFacCdn") String facilityAllCondition, @Param("condition") String condition);
//
//    /**
//     * 权限场地(不包含position，默认查询org_type为1、2)
//     */
//    @Select(SqlConstant.FACILITY_INSIDE_WITHOUT_POSITION_LIST_BY_RIGHT_SQL + " ${condition} ORDER BY code, parentid DESC")
//    List<Map<String, Object>> getFacilityInsideWithoutPositionListByRight(@Param("schema_name") String schemaName, @Param("facCdn") String facilityCondition, @Param("condition") String condition);
//
//    /**
//     * 权限场地(不包含position)
//     */
//    @Select(SqlConstant.FACILITY_WITHOUT_POSITION_LIST_BY_RIGHT_SQL + " ${condition} ORDER BY code, parentid DESC")
//    List<Map<String, Object>> getFacilityWithoutPositionListByRight(@Param("schema_name") String schemaName, @Param("facCdn") String facilityCondition, @Param("condition") String condition);
//
//    /**
//     * 权限场地（第一层）(默认查询org_type为1、2)
//     */
//    @Select(" select * from ( " + SqlConstant.FACILITY_INSIDE_LIST_BY_RIGHT_SQL
//            + " ) a where not EXISTS ( select code from ( " + SqlConstant.FACILITY_INSIDE_LIST_BY_RIGHT_SQL
//            + " ) b where a.parentid = b.code) ${condition} ORDER BY code, parentid DESC ")
//    List<Map<String, Object>> getFirstLevelFacilityListByRight(@Param("schema_name") String schemaName, @Param("facCdn") String facilityCondition, @Param("allFacCdn") String facilityAllCondition, @Param("condition") String condition);
//
//    /**
//     * 权限场地（第一层）(不包含position，默认查询org_type为1、2)
//     */
//    @Select(" select * from ( " + SqlConstant.FACILITY_INSIDE_WITHOUT_POSITION_LIST_BY_RIGHT_SQL
//            + " ) a where not EXISTS ( select code from ( " + SqlConstant.FACILITY_INSIDE_WITHOUT_POSITION_LIST_BY_RIGHT_SQL
//            + " ) b where a.parentid = b.code) ${condition} ORDER BY code, parentid DESC ")
//    List<Map<String, Object>> getFirstLevelFacilityInsideWithoutPositionListByRight(@Param("schema_name") String schemaName, @Param("facCdn") String facilityCondition, @Param("condition") String condition);
}
