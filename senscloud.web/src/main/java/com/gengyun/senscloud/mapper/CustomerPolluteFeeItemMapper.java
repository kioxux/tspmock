package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface CustomerPolluteFeeItemMapper {

    @Select(" <script>" +
            " select count(f.id) " +
            " FROM  ${schema_name}._sc_customer_pollute_fee_item f " +
            " <where>" +
            " <when test='pm.begin_date!=null and pm.begin_date!=\"\" and pm.end_date!=null and pm.end_date!=\"\"'> " +
            " AND  ((f.begin_date between #{pm.begin_date}::timestamp and  #{pm.end_date}::timestamp) or " +
            " (f.end_date between #{pm.begin_date}::timestamp and  #{pm.end_date}::timestamp)) " +
            " </when> " +
            " <when test='pm.is_use!=null and pm.is_use!=\"\" '> " +
            " AND  f.is_use= #{pm.is_use}  " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( f.fee_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR f.fee_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " and f.status > " + StatusConstant.STATUS_DELETEED +
            " </where>" +
            " </script>")
    int findPolluteFeeItemListCount(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select(" <script>" +
            " select f.id,f.fee_code,f.fee_name,f.begin_date,f.end_date,f.begin_over_times,f.end_over_times," +
            " f.price,f.ratio_cal_type,f.repeat_price,f.is_use,f.status " +
            " FROM  ${schema_name}._sc_customer_pollute_fee_item f " +
            " <where>" +
            " <when test='pm.begin_date!=null and pm.begin_date!=\"\" and pm.end_date!=null and pm.end_date!=\"\"'> " +
            " AND  ((f.begin_date between #{pm.begin_date}::timestamp and  #{pm.end_date}::timestamp) or " +
            " (f.end_date between #{pm.begin_date}::timestamp and  #{pm.end_date}::timestamp) or " +
            " (#{pm.begin_date}::timestamp between f.begin_date and f.end_date )) " +
            " </when> " +
            " <when test='pm.is_use!=null and pm.is_use!=\"\" '> " +
            " AND  f.is_use= #{pm.is_use}  " +
            " </when> " +
            " <when test='pm.keywordSearch!=null'> " +
            " AND ( f.fee_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR f.fee_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')) " +
            " </when> " +
            " and f.status > " + StatusConstant.STATUS_DELETEED +
            " </where> " +
            " order by f.id " +
            " </script>")
    List<Map<String, Object>> findPolluteFeeItemList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Insert(" INSERT into ${schema_name}._sc_customer_pollute_fee_item " +
            "(fee_code,fee_name,begin_date,end_date,begin_over_times,end_over_times,price,ratio_cal_type,repeat_price,is_use,status,create_user_id)  " +
            " VALUES  " +
            " (#{pm.fee_code},#{pm.fee_name},#{pm.begin_date}::timestamp,#{pm.end_date}::timestamp,#{pm.begin_over_times}::int,#{pm.end_over_times}::int," +
            " #{pm.price}::decimal,#{pm.ratio_cal_type},#{pm.repeat_price}::decimal,#{pm.is_use},#{pm.status}::int4,#{pm.create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "pm.id", keyColumn = "id")
    void insertPolluteFeeItem(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_customer_pollute_fee_item" +
            " <set> id =id " +
            " <when test='pm.fee_code!=null'> " +
            " ,fee_code=#{pm.fee_code}" +
            " </when> " +
            " <when test='pm.fee_name!=null'> " +
            " ,fee_name=#{pm.fee_name}" +
            " </when> " +
            " <when test='pm.begin_date!=null'> " +
            " ,begin_date=#{pm.begin_date}::timestamp" +
            " </when> " +
            " <when test='pm.end_date!=null'> " +
            " ,end_date=#{pm.end_date}::timestamp" +
            " </when> " +
            " <when test='pm.begin_over_times!=null'> " +
            " ,begin_over_times=#{pm.begin_over_times}::int" +
            " </when> " +
            " <when test='pm.end_over_times!=null'> " +
            " ,end_over_times=#{pm.end_over_times}::int" +
            " </when> " +
            " <when test='pm.price!=null'> " +
            " ,price=#{pm.price}::decimal" +
            " </when> " +
            " <when test='pm.ratio_cal_type!=null'> " +
            " ,ratio_cal_type=#{pm.ratio_cal_type}" +
            " </when> " +
            " <when test='pm.repeat_price!=null'> " +
            " ,repeat_price=#{pm.repeat_price}::decimal" +
            " </when> " +
            " <when test='pm.is_use!=null'> " +
            " ,is_use=#{pm.is_use}" +
            " </when> " +
            " <when test='pm.status!=null'> " +
            " ,status=#{pm.status}::int4" +
            " </when> " +
            " </set>" +
            " where id =#{pm.id}::int " +
            " </script>")
    void updatePolluteFeeItem(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select(" <script>" +
            " select f.id,f.fee_code,f.fee_name,f.begin_date,f.end_date,f.begin_over_times,f.end_over_times," +
            " f.price,f.ratio_cal_type,f.repeat_price,f.is_use,f.status " +
            " FROM ${schema_name}._sc_customer_pollute_fee_item f " +
            " <where>" +
            " f.id = #{id}" +
            " </where>" +
            " </script>")
    Map<String, Object> findById(@Param("schema_name") String schema_name, @Param("id") Integer id);
}
