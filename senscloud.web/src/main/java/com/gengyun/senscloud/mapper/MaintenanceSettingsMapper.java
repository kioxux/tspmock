package com.gengyun.senscloud.mapper;

public interface MaintenanceSettingsMapper {
//
//    /**
//     * 查询维修类型
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_repair_type  order by \"order\" ")
//    List<RepairType> repairTypeData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询故障类型
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_fault_type order by \"order\" ")
//    List<FaultType> faultType(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询设备状态
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_asset_status order by \"order\" ")
//    List<AssetStatus> assetStatus(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询保养项
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_maintain_item where asset_type_id='${asset_name}'  order by \"order\" ")
//    List<MaintainItemData> maintainItemData(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name);
//
//    /**
//     * 查询保养确认
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_maintain_check_item where asset_type_id='${asset_name}'  order by \"order\"")
//    List<MaintainCheckItemData> maintainCheckItemData(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name);
//
//    /**
//     * 查询巡检
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_inspection_item where 1=1  order by \"order\"")
//    List<InspectionItemData> inspectionItemData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询点检
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_spotcheck_item where 1=1  order by \"order\"")
//    List<SpotcheckItemData> spotcheckItemData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询周期
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_maintain_cycle where asset_type_id='${asset_name}'  order by \"order\"")
//    List<MaintainCycleData> maintainCycleData(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name);
//
//    /**
//     * 查找所有的保养周期
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_maintain_cycle order by \"order\"")
//    List<MaintainCycleData> getMaintainCycleDataList(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询设备类型汇总
//     */
//    @Select("select * from ${schema_name}._sc_metadata_assets order by asset_name")
//    List<MetaDataAsset> findAllMetaDataAsset(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询故障代码
//     */
//    @Select("SELECT f.code,f.fault_code,f.code_name,f.order,f.isuse," +
////            "f.is_generate_request, " +
//            "ARRAY_TO_STRING(ARRAY_AGG (ac.category_name ), ',' ) AS category_name," +
//            "ARRAY_TO_STRING(ARRAY_AGG (ac.id ), ',' ) AS category_id," +
//            "f.remark " +
//            "FROM ${schema_name}._sc_fault_code f " +
//            "left join ${schema_name}._sc_asset_category_fault cf on cf.fault_code=f.code " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id=cf.category_id " +
//            "where f.isuse='t' " +
//            "group by f.code,f.fault_code,f.code_name,f.order,f.isuse," +
////            "f.is_generate_request," +
//            "f.remark " +
//            "order by \"order\" ")
//    List<FaultCode> faultCodeData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询预防措施
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_preventive_type  order by \"order\" ")
//    List<PreventiveType> preventiveTypeData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询设备重要级别
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_importment_level  order by \"order\" ")
//    List<ImportmentLevel> importmentLevelData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询位置类型
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_facility_type  order by create_time ")
//    List<FacilityType> facilityTypeData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询单位表
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_unit")
//    List<UnitData> unitData(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询完修类型表
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_work_finished_type")
//    List<Map<String, Object>> getWorkFinishedTypeList(@Param("schema_name") String schema_name);
//
//    /**
//     * 更新维修类型
//     */
//
//    @Update("update ${schema_name}._sc_repair_type " +
//            "set type_name=#{type_name},\"order\" =#{order} " +
//            " where id=#{id}")
//    int UpdaterepairTypeData(@Param("schema_name") String schema_name, @Param("type_name") String type_name, @Param("order") int order, @Param("id") int id);
//
//    /**
//     * 插入维修类型
//     */
//    @Insert("insert into ${schema_name}._sc_repair_type ( " +
//            "type_name,\"order\",isuse)  values  (#{type_name},#{order},'1')")
//    int InsertrepairTypeData(RepairType repairType);
//
//    /**
//     * 更新故障类型
//     */
//
//    @Update("update ${schema_name}._sc_fault_type  " +
//            "set type_name=#{type_name},\"order\" =#{order} " +
//            " where id=#{id}")
//    int UpdateFaultType(@Param("schema_name") String schema_name, @Param("type_name") String type_name, @Param("order") int order, @Param("id") int id);
//
//    /**
//     * 插入故障类型
//     */
//    @Insert("insert into ${schema_name}._sc_fault_type ( " +
//            "type_name,\"order\",isuse)  values  (#{type_name},#{order},'1')")
//    int InsertFaultType(FaultType faultType);
//
//    /**
//     * 更新设备状态类型
//     */
//
//    @Update("update ${schema_name}._sc_asset_status  " +
//            "set status=#{status},\"order\" =#{order} " +
//            " where id=#{id}")
//    int UpdateAssetStatus(@Param("schema_name") String schema_name, @Param("status") String status, @Param("order") int order, @Param("id") int id);
//
//    /**
//     * 插入设备状态类型
//     */
//    @Insert("insert into ${schema_name}._sc_asset_status ( " +
//            "status,\"order\",isuse)  values  (#{status},#{order},'1')")
//    int InsertAssetStatus(AssetStatus assetStatus);
//
//    /**
//     * 更新保养类型
//     */
//
//    @Update("update ${schema_name}._sc_maintain_item  " +
//            "set item_name=#{item_name} ,check_note=#{check_note},\"order\" =#{order} " +
//            " where id=#{id}")
//    int UpdateMaintainItem(@Param("schema_name") String schema_name, @Param("item_name") String item_name, @Param("id") int id, @Param("check_note") String check_note, @Param("order") int order);
//
//    /**
//     * 插入保养类型
//     */
//    @Insert("insert into ${schema_name}._sc_maintain_item ( " +
//            "asset_type_id,item_name,create_user_account,createtime,check_note,item_code,\"order\")  values  (#{asset_type_id},#{item_name},#{create_user_account},#{createtime},#{check_note},#{item_code},#{order})")
//    int InsertMaintainItem(MaintainItemData maintainItemData);
//
//    /**
//     * 更新保养确认项类型
//     */
//
//    @Update("update ${schema_name}._sc_maintain_check_item  " +
//            "set check_item_name=#{item_name},\"order\" =#{order} " +
//            " where id=#{id}")
//    int UpdateMaintainCheckItem(@Param("schema_name") String schema_name, @Param("item_name") String item_name, @Param("id") int id, @Param("order") int order);
//
//    /**
//     * 插入保养确认项类型
//     */
//    @Insert("insert into ${schema_name}._sc_maintain_check_item ( " +
//            "asset_type_id,check_item_name,create_user_account,createtime,\"order\")  values ( #{asset_type_id},#{check_item_name},#{create_user_account},#{createtime},#{order})")
//    int InsertMaintainCheckItem(MaintainCheckItemData maintainCheckItemData);
//
//    /**
//     * 更新巡检类型
//     */
//
//    @Update("update ${schema_name}._sc_inspection_item  " +
//            "set item_name=#{item_name} ,check_note=#{check_note},\"order\" =#{order},result_type=#{resultType} " +
//            " where id=#{id}")
//    int UpdateInspection(@Param("schema_name") String schema_name, @Param("item_name") String item_name, @Param("id") int id, @Param("check_note") String check_note, @Param("order") int order, @Param("resultType") int resultType);
//
//    /**
//     * 插入巡检类型
//     */
//    @Insert("insert into ${schema_name}._sc_inspection_item ( " +
//            "item_name,create_user_account,createtime,check_note,item_code,\"order\",result_type)  values  (#{item_name},#{create_user_account},#{createtime},#{check_note},#{item_code},#{order},#{result_type}) ")
//    int InsertInspection(InspectionItemData inspectionItemData);
//
//    /**
//     * 更新点检类型
//     */
//
//    @Update("update ${schema_name}._sc_spotcheck_item  " +
//            "set item_name=#{item_name} ,check_note=#{check_note},\"order\" =#{order},result_type=#{resultType} " +
//            " where id=#{id}")
//    int UpdateSpotcheck(@Param("schema_name") String schema_name, @Param("item_name") String item_name, @Param("id") int id, @Param("check_note") String check_note, @Param("order") int order, @Param("resultType") int resultType);
//
//    /**
//     * 插入点检类型
//     */
//    @Insert("insert into ${schema_name}._sc_spotcheck_item ( " +
//            "item_name,create_user_account,createtime,check_note,item_code,\"order\",result_type)  values  (#{item_name},#{create_user_account},#{createtime},#{check_note},#{item_code},#{order},#{result_type})")
//    int InsertSpotcheck(SpotcheckItemData spotcheckItemData);
//
//    /**
//     * 更新周期
//     */
//
//    @Update("update ${schema_name}._sc_maintain_cycle  " +
//            "set cycle_count=#{cycle_count} ,\"order\" =#{order} " +
//            " where id=#{id}")
//    int UpdateMaintainCycleData(@Param("schema_name") String schema_name, @Param("cycle_count") int cycle_count, @Param("id") int id, @Param("order") int order);
//
//    /**
//     * 插入周期
//     */
//    @Insert("insert into ${schema_name}._sc_maintain_cycle ( " +
//            "asset_type_id,cycle_count,create_user_account,createtime,\"order\")  values  (#{asset_type_id},#{cycle_count},#{create_user_account},#{createtime},#{order})")
//    int InsertMaintainCycleData(MaintainCycleData maintainCycleData);
//
//    /**
//     * 更新故障代码
//     */
//
//    @Update("update ${schema_name}._sc_fault_code  " +
//            "set code_name=#{code_name},\"order\" =#{order},remark=#{remark},fault_code=#{fault_code} " +
//            " where code=#{code}")
//    int updateFaultCode(@Param("schema_name") String schema_name, @Param("code_name") String code_name, @Param("order") int order, @Param("code") String code,@Param("fault_code") String fault_code, @Param("remark") String remark);
//
//    /**
//     * 删除故障代码关联的设备类型
//     */
//
//    @Delete("delete from ${schema_name}._sc_asset_category_fault  " +
//            "where fault_code=#{code}")
//    int deleteFaultCodeWithCategory(@Param("schema_name") String schema_name, @Param("code") String code);
//
//    /**
//     * 插入故障代码关联的设备类型
//     */
//
//    @Insert("Insert into  ${schema_name}._sc_asset_category_fault  " +
//            "(category_id,fault_code) values" +
//            "(#{category_id}::int,#{code})")
//    int addFaultCodeWithCategory(@Param("schema_name") String schema_name, @Param("code") String code, @Param("category_id") int category_id);
//
//    /**
//     * 插入故障代码
//     */
//    @Insert("insert into ${schema_name}._sc_fault_code (code,fault_code, " +
//            "code_name,\"order\",isuse,remark)  values  (#{code},#{fault_code},#{code_name},#{order},'1',#{remark}) ")
//    int insertFaultCode(FaultCode faultCode);
//
//    /**
//     * 更新预防措施
//     */
//
//    @Update("update ${schema_name}._sc_preventive_type  " +
//            "set type_name=#{type_name},\"order\" =#{order} " +
//            " where id=#{id}")
//    int updatePreventiveType(@Param("schema_name") String schema_name, @Param("type_name") String type_name, @Param("order") int order, @Param("id") int id);
//
//    /**
//     * 插入预防措施
//     */
//    @Insert("insert into ${schema_name}._sc_preventive_type (" +
//            "type_name,\"order\",isuse)  values  (#{type_name},#{order},'1')")
//    int insertPreventiveType(PreventiveType preventiveType);
//
//    /**
//     * 更新设备重要级别
//     */
//
//    @Update("update ${schema_name}._sc_importment_level  " +
//            "set level_name=#{level_name},\"order\" =#{order} " +
//            " where id=#{id}")
//    int updateImportmentLevel(@Param("schema_name") String schema_name, @Param("level_name") String level_name, @Param("order") int order, @Param("id") int id);
//
//    /**
//     * 插入设备重要级别
//     */
//    @Insert("insert into ${schema_name}._sc_importment_level (" +
//            "level_name,\"order\",isuse)  values  (#{level_name},#{order},'1')")
//    int insertImportmentLevel(ImportmentLevel importmentLevel);
//
//
//    /**
//     * 更新位置类型
//     */
//
//    @Update("update ${schema_name}._sc_facility_type  " +
//            "set type_name=#{type_name}" +
//            " where id=#{id}")
//    int updateFacilityType(@Param("schema_name") String schema_name, @Param("type_name") String type_name, @Param("id") int id);
//
//    /**
//     * 插入位置类型
//     */
//    @Insert("insert into ${schema_name}._sc_facility_type (" +
//            "type_name,isuse, create_time, create_user_account)  values  (#{type_name},'1',#{create_time},#{create_user_account})")
//    int insertFacilityType(FacilityType facilityType);
//
//    /**
//     * 更新单位表
//     */
//
//    @Update("update ${schema_name}._sc_unit  " +
//            "set unit_name=#{unit_name}" +
//            " where id=#{id}")
//    int updateUnitData(@Param("schema_name") String schema_name, @Param("unit_name") String unit_name, @Param("id") int id);
//
//    /**
//     * 插入单位表
//     */
//    @Insert("insert into ${schema_name}._sc_unit (" +
//            "unit_name,isuse)  values  (#{unit_name},'1')")
//    int insertUnitData(UnitData unitData);
//
//
//    /**
//     * 更新完修类型
//     */
//
//    @Update("update ${schema_name}._sc_work_finished_type " +
//            "set finished_name=#{finished_name},is_finished =#{is_finished},operate_account=#{operate_account},operate_time=#{operate_time} " +
//            " where id=#{id}")
//    int updateWorkFinishedTypeData(@Param("schema_name") String schema_name, @Param("finished_name") String finished_name, @Param("is_finished") int is_finished,
//                                   @Param("operate_account") String operate_account, @Param("operate_time") Timestamp operate_time, @Param("id") int id);
//
//    /**
//     * 插入完修类型
//     */
//    @Insert("insert into ${schema_name}._sc_work_finished_type ( " +
//            "finished_name,is_finished,operate_account,operate_time) " +
//            "values (#{finished_name},#{is_finished},#{operate_account},#{operate_time})")
//    int insertWorkFinishedTypeData(@Param("schema_name") String schema_name, @Param("finished_name") String finished_name,
//                                   @Param("is_finished") int is_finished,
//                                   @Param("operate_account") String operate_account, @Param("operate_time") Timestamp operate_time);
//
//
//    /**
//     * 删除维修类型
//     */
//    @Update("update ${schema_name}._sc_repair_type set isuse='${isUse}' where id= #{id}")
//    int deleteRepairData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    /**
//     * 删除故障类型
//     */
//    @Update("Update ${schema_name}._sc_fault_type set isuse='${isUse}' where id= #{id}")
//    int deleteFaultData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    /**
//     * 删除状态类型
//     */
//    @Update("Update ${schema_name}._sc_asset_status set isuse='${isUse}' where id= #{id}")
//    int deleteAssetStatusData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    /**
//     * 删除保养项类型
//     */
//    @Delete("delete from ${schema_name}._sc_maintain_item where id= #{id}")
//    int deleteMaintainData(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    /**
//     * 删除保养确认类型
//     */
//    @Delete("delete from ${schema_name}._sc_maintain_check_item where id= #{id}")
//    int deleteMaintainCheckData(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    /**
//     * 删除巡检类型
//     */
//    @Delete("delete from ${schema_name}._sc_inspection_item where id= #{id}")
//    int deleteInspectionData(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    /**
//     * 删除点检类型
//     */
//    @Delete("delete from ${schema_name}._sc_spotcheck_item where id= #{id}")
//    int deleteSpotcheckData(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    /**
//     * 删除周期
//     */
//    @Delete("delete from ${schema_name}._sc_maintain_cycle where id= #{id}")
//    int deleteMaintainCycleData(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    /**
//     * 删除故障代码
//     */
//    @Update("Update ${schema_name}._sc_fault_code set isuse='${isUse}' where code= #{id}")
//    int deleteFaultCodeData(@Param("schema_name") String schema_name, @Param("id") String id, @Param("isUse") String isUse);
//
//    /**
//     * 删除预防措施
//     */
//    @Update("Update ${schema_name}._sc_preventive_type set isuse='${isUse}' where id= #{id}")
//    int deletePreventiveTypeData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    /**
//     * 删除设备重要级别
//     */
//    @Update("Update ${schema_name}._sc_importment_level set isuse='${isUse}' where id= #{id}")
//    int deleteImportmentLevelData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    /**
//     * 删除位置类型
//     */
//    @Update("Update ${schema_name}._sc_facility_type set isuse='${isUse}' where id= #{id}")
//    int deleteFacilityTypeData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    /**
//     * 删除单位
//     */
//    @Update("Update ${schema_name}._sc_unit set isuse='${isUse}' where id= #{id}")
//    int deleteUnitData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    //设备运行状态
//    @Select("SELECT * FROM ${schema_name}._sc_asset_running_status order by \"order\" ")
//    List<AssetRunningData> getAssetRunningDataList(@Param("schema_name") String schema_name);
//
//    //设备运行状态最大id
//    @Select("SELECT COALESCE(max(id),0) FROM ${schema_name}._sc_asset_running_status")
//    int getAssetRunningMaxId(@Param("schema_name") String schema_name);
//
//    //设备运行状态新增
//    @Insert("insert into ${schema_name}._sc_asset_running_status ( " +
//            "id,running_status,isuse,\"order\",priority_level)  values (#{d.id},#{d.running_status},'1',#{d.order},#{d.priority_level})")
//    int addAssetRunningData(@Param("schema_name") String schema_name, @Param("d") AssetRunningData data);
//
//    //设备运行状态修改
//    @Update("update ${schema_name}._sc_asset_running_status  " +
//            "set running_status=#{running_status},\"order\" =#{order},priority_level=#{priority_level} " +
//            " where id=#{id}")
//    int updateAssetRunningData(@Param("schema_name") String schema_name, @Param("running_status") String running_status, @Param("order") int order, @Param("id") int id, @Param("priority_level") int priority_level);
//
//    //启用禁用设备运行状态
//    @Update("update ${schema_name}._sc_asset_running_status set isuse='${isUse}' where id=#{id}")
//    int disableAssetRunningData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    //工单类型
//    @Select("SELECT wt.*,b.business_name as business_type_name,wp.work_template_name FROM ${schema_name}._sc_work_type wt " +
//            "left join ${schema_name}._sc_business_type b on wt.business_type_id=b.business_type_id " +
//            "left join ${schema_name}._sc_work_template wp on wt.work_template_code=wp.work_template_code " +
//            "order by wt.order ")
//    List<WorkTypeData> getWorkTypeDataList(@Param("schema_name") String schema_name);
//
//    //工单类型
//    @Select("SELECT wt.* FROM ${schema_name}._sc_work_type wt where wt.business_type_id=#{business_type_id}")
//    List<WorkTypeData> getWorkTypeDataListByBusiessTypeId(@Param("schema_name") String schema_name, @Param("business_type_id") int business_type_id);
//
//    //工单类型最大id
//    @Select("SELECT COALESCE(max(id),0) FROM ${schema_name}._sc_work_type")
//    int getWorkTypeMaxId(@Param("schema_name") String schema_name);
//
//    //工单类型新增
//    @Insert("insert into ${schema_name}._sc_work_type " +
//            "(id,type_name,flow_template_code,work_template_code,business_type_id,create_user_account,create_time,\"order\",isuse) " +
//            " values (#{d.id},#{d.type_name},#{d.flow_template_code},#{d.work_template_code},#{d.business_type_id},#{d.create_user_account},#{d.create_time},#{d.order},'1')")
//    int addWorkTypeData(@Param("schema_name") String schema_name, @Param("d") WorkTypeData data);
//
//    //工单类型修改
//    @Update("update ${schema_name}._sc_work_type  " +
//            "set type_name=#{type_name},flow_template_code=#{flow_template_code},work_template_code=#{work_template_code}," +
//            "business_type_id=#{business_type_id},\"order\" =#{order} " +
//            " where id=#{id}")
//    int updateWorkTypeData(@Param("schema_name") String schema_name, @Param("type_name") String type_name, @Param("flow_template_code") String flow_template_code, @Param("work_template_code") String work_template_code, @Param("business_type_id") int business_type_id, @Param("order") int order, @Param("id") int id);
//
//    //启用、禁用工单类型
//    @Delete("update ${schema_name}._sc_work_type set isuse='${isUse}' where id=#{id}")
//    int disableWorkTypeData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//    //文档类型
//    @Select("SELECT * FROM ${schema_name}._sc_file_type " +
//            " order by \"order\" ")
//    List<FileTypeData> getFileTypeDataList(@Param("schema_name") String schema_name);
//
//    //文档类型最大id
//    @Select("SELECT COALESCE(max(id),0) FROM ${schema_name}._sc_file_type")
//    int getFileTypeMaxId(@Param("schema_name") String schema_name);
//
//    //文档类型新增
//    @Insert("insert into ${schema_name}._sc_file_type ( " +
//            "id,type_name,isuse,\"order\")  values  (#{d.id},#{d.type_name},'1',#{d.order})")
//    int addFileTypeData(@Param("schema_name") String schema_name, @Param("d") FileTypeData data);
//
//    //文档类型修改
//    @Update("update ${schema_name}._sc_file_type  " +
//            "set type_name=#{type_name},\"order\" =#{order}  " +
//            " where id=#{id}")
//    int updateFileTypeData(@Param("schema_name") String schema_name, @Param("type_name") String type_name, @Param("order") int order, @Param("id") int id);
//
//    //启用、禁用文档类型
//    @Delete("update ${schema_name}._sc_file_type set isuse='${isUse}' where id=#{id}")
//    int disablFileTypeData(@Param("schema_name") String schema_name, @Param("id") int id, @Param("isUse") String isUse);
//
//
//    //寻找所有有效的工单类型
//    @Select("SELECT wt.* FROM ${schema_name}._sc_work_type wt " +
//            "where isuse='1' " +
//            "order by wt.order ")
//    List<WorkTypeData> getWorkTypeListInUse(@Param("schema_name") String schema_name);
//
//
//    //查找工单状态
//    @Select("SELECT * FROM ${schema_name}._sc_status order by id")
//    List<WorkStatus> getWorkStatus(@Param("schema_name") String schema_name);
//
//    //查找工单业务
//    @Select("select * from ${schema_name}._sc_business_type order by business_type_id asc")
//    List<Map<String, Object>> findAllBusinessType(@Param("schema_name") String schema_name);

}
