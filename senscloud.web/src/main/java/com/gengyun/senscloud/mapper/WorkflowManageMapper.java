package com.gengyun.senscloud.mapper;


import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.model.WorkflowSearchParam;
import org.apache.ibatis.annotations.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 工作流管理
 * User: sps
 * Date: 2021/02/01
 * Time: 下午15:00
 */
@Mapper
public interface WorkflowManageMapper {
    /**
     * 获取工作流部署总数
     *
     * @param param 请求参数
     * @return 总数
     */
    int selectDeploymentCount(Map<String, Object> param);

    /**
     * 获取工作流部署列表
     *
     * @param param 请求参数
     * @return 部署列表
     */
    List<Map<String, Object>> selectDeploymentList(Map<String, Object> param);

    /**
     * 获取工作流部署列表（分页）
     *
     * @param param 请求参数
     * @return 部署列表
     */
    List<Map<String, Object>> selectDeploymentPageList(Map<String, Object> param);

    /**
     * 获取工作流定义总数
     *
     * @param param 请求参数
     * @return 总数
     */
    int selectDefinitionCount(Map<String, Object> param);

    /**
     * 获取工作流定义列表
     *
     * @param param 请求参数
     * @return 定义列表
     */
    List<Map<String, Object>> selectDefinitionList(Map<String, Object> param);

    /**
     * 获取工作流定义列表（分页）
     *
     * @param param 请求参数
     * @return 定义列表
     */
    List<Map<String, Object>> selectDefinitionPageList(Map<String, Object> param);

    /**
     * 获取工作流定义总数（最新）
     *
     * @param param 请求参数
     * @return 总数
     */
    int selectLatestDefinitionCount(Map<String, Object> param);

    /**
     * 获取工作流定义列表（最新）
     *
     * @param param 请求参数
     * @return 定义列表
     */
    List<Map<String, Object>> selectLatestDefinitionList(Map<String, Object> param);

    /**
     * 获取工作流定义列表（分页、最新）
     *
     * @param param 请求参数
     * @return 定义列表
     */
    List<Map<String, Object>> selectLatestDefinitionPageList(Map<String, Object> param);

    /**
     * 获取工作流实例总数
     *
     * @param param 请求参数
     * @return 总数
     */
    int selectInstanceCount(Map<String, Object> param);

    /**
     * 获取工作流实例列表（分页）
     *
     * @param param 请求参数
     * @return 部署列表
     */
    List<Map<String, Object>> selectInstancePageList(Map<String, Object> param);

    /**
     * 获取工作流实例主键列表（分页）
     *
     * @param param 请求参数
     * @return 部署列表
     */
    List<String> selectInstanceIdPageList(Map<String, Object> param);

    /**
     * 获取工作流任务列表
     *
     * @param schema_name        数据库
     * @param userId             用户
     * @param mineOnly           只查询我的待办
     * @param subWorkCodesSearch 查询编码
     * @return 列表
     */
    @Select("<script> select d.* from ( " +
            " select d.sub_work_code from ${schema_name}._sc_works_detail d, " +
            "  (SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id  " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.relation_type = 1 and d.relation_id = p.asset_position_code and d.work_type_id = p.work_type_id and status &gt; 10 and status &lt; 60 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " UNION select d.sub_work_code from ${schema_name}._sc_works_detail d, ${schema_name}._sc_asset a " +
            " ,(SELECT distinct p.asset_position_code, at.category_id, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and p.asset_position_code is not null and at.category_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.relation_type = 2 and d.relation_id = a.id and a.position_code = p.asset_position_code and a.category_id = p.category_id and d.work_type_id = p.work_type_id and d.status &gt; 10 and d.status &lt; 60 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " UNION select d.sub_work_code from ${schema_name}._sc_works_detail d, " +
            "  (SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.relation_type not in (1, 2) and d.work_type_id = p.work_type_id and status &gt; 10 and status &lt; 60 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " UNION select d.sub_work_code from ${schema_name}._sc_works_detail d," +
            "  (SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "  INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "  INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "  LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "  WHERE up.user_id = #{userId} and wt.work_type_id is not null " +
            "  ) p " +
            "  where is_main = 1 and (relation_id is null or position(',' in relation_id) &gt; 0) and status &gt; 10 and status &lt; 60 and status != 40 and d.work_type_id = p.work_type_id " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w, ${schema_name}._sc_asset a " +
            " ,(SELECT distinct p.asset_position_code, at.category_id, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and p.asset_position_code is not null and at.category_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = a.id and a.position_code = p.asset_position_code and a.category_id = p.category_id and w.work_type_id = p.work_type_id and d.status &gt; 10 and d.status &lt; 60 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and (w.position_code = p.asset_position_code or d.relation_id = P.asset_position_code) and w.work_type_id = p.work_type_id and d.status &gt; 10 and d.status &lt; 60 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.is_main = 1 and (d.asset_id is null or position(',' in d.asset_id) &gt; 0) and d.work_code = w.work_code and w.work_type_id = p.work_type_id and d.status &gt; 10 and d.status &lt; 60 and d.status != 40 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " union select d.sub_work_code from ${schema_name}._sc_bom_works_detail d, ${schema_name}._sc_bom_works w " +
            " ,( SELECT distinct wt.work_type_id, s.stock_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "   WHERE up.user_id = #{userId} and s.stock_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and (w.stock_id = p.stock_id or w.to_stock_id = p.stock_id or d.relation_id = P.stock_id ::varchar) and w.work_type_id = p.work_type_id and d.status &gt; 10 and d.status &lt; 60 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            " union select d.sub_work_code from ${schema_name}._sc_bom_works_detail d, ${schema_name}._sc_bom_works w " +
            " ,( SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{userId} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.is_main = 1 and (d.bom_id is null or position(',' in d.bom_id::varchar) &gt; 0) and d.work_code = w.work_code and w.work_type_id = p.work_type_id and d.status &gt; 10 and d.status &lt; 60 and d.status != 40 " +
            "  <if test = 'null != subWorkCodesSearch'> " +
            "    and d.sub_work_code in <foreach collection='subWorkCodesSearch' item='code' open='(' close=')' separator=','> #{code} </foreach> " +
            "  </if> " +
            ") d " +
            "INNER JOIN ${schema_name}.ACT_RU_VARIABLE arv ON arv.NAME_= 'sub_work_code' and arv.TYPE_ = 'string' and arv.TEXT_ = d.sub_work_code " +
            "INNER JOIN ${schema_name}.ACT_RU_TASK ART ON ART.ID_ = arv.TASK_ID_ or ART.PROC_INST_ID_ = arv.EXECUTION_ID_ " +
            "<if test = 'mineOnly'> " +
            " where ART.assignee_ = #{userId} " +
            "</if> order by sub_work_code desc limit 1000 </script> ")
    List<String> findFlowTaskCodeList(@Param("schema_name") String schema_name, @Param("userId") String userId, @Param("mineOnly") Boolean mineOnly, @Param("subWorkCodesSearch") List<String> subWorkCodesSearch);

    /**
     * 获取工作流任务列表总数
     *
     * @param schema_name 数据库
     * @param wfParam     页面参数
     * @param upCodeList  有权限工单编号
     * @param userId      用户
     * @return 总数
     */
    @Select("<script> SELECT count(1) " + SqlConstant.FLOW_TASK_SQL + "</script> ")
    int countFlowTask(@Param("schema_name") String schema_name, @Param("wfParam") WorkflowSearchParam wfParam, @Param("upCodeList") List<String> upCodeList, @Param("userId") String userId);

    /**
     * 获取工作流任务列表（分页）
     *
     * @param schema_name   数据库
     * @param wfParam       页面参数
     * @param upCodeList    有权限工单编号
     * @param userId        用户
     * @param paginationSql 分页信息
     * @return 列表
     */
    @Select("<script> SELECT * " + SqlConstant.FLOW_TASK_SQL + " order by priority_level desc, deadline_time asc, process_create_time desc ${paginationSql} </script> ")
    List<Map<String, Object>> findFlowTaskList(@Param("schema_name") String schema_name, @Param("wfParam") WorkflowSearchParam wfParam, @Param("upCodeList") List<String> upCodeList, @Param("userId") String userId, @Param("paginationSql") String paginationSql);


    /**
     * 获取按天分组工作流任务列表
     *
     * @param schema_name 数据库
     * @param wfParam     页面参数
     * @param upCodeList  有权限工单编号
     * @param userId      用户
     * @return 列表
     */
    @Select("<script> SELECT process_create_date as create_time, count(distinct task_id) as count " + SqlConstant.FLOW_TASK_SQL + " group by process_create_date ORDER BY create_time ASC </script> ")
    List<Map<String, Object>> findFlowTaskGroupList(@Param("schema_name") String schema_name, @Param("wfParam") WorkflowSearchParam wfParam, @Param("upCodeList") List<String> upCodeList, @Param("userId") String userId);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_linkage_condition where linkage_id in " +
            " (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select distinct node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}))")
    void deleteWorkFlowNodeColumLinkageCondition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select distinct node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) ")
    void deleteWorkFlowNodeColumLinkage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_ext where node_id in " +
            " (select distinct node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) ")
    void deleteWorkFlowNodeColumExt(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) ")
    void deleteWorkFlowNodeColum(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id} ")
    void deleteWorkFlowNode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} ")
    void deleteWorkFlowInfo(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select * from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} ")
    List<LinkedHashMap<String, Object>> findWorkFlowInfo(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select * from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id} ")
    List<LinkedHashMap<String, Object>> findWorkFlowNode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select * from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) ")
    List<LinkedHashMap<String, Object>> findWorkFlowNodeColum(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select * from ${schema_name}._sc_work_flow_node_column_ext where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})")
    List<LinkedHashMap<String, Object>> findWorkFlowNodeColumExt(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select * from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) ")
    List<LinkedHashMap<String, Object>> findWorkFlowNodeColumLinkage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("select * from ${schema_name}._sc_work_flow_node_column_linkage_condition where linkage_id in " +
            " (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) ")
    List<LinkedHashMap<String, Object>> findWorkFlowNodeColumLinkageCondition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Select("<script>select * from ${schema_name}._sc_work_template where work_template_code in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach></script>")
    List<LinkedHashMap<String, Object>> findWorkTemplate(@Param("schema_name") String schema_name, @Param("wtCodes") List<String> wtCodes);

    @Select("<script>select * from ${schema_name}._sc_work_flow_node_column where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach></script>")
    List<LinkedHashMap<String, Object>> findWorkTemplateColum(@Param("schema_name") String schema_name, @Param("wtCodes") List<String> wtCodes);

    @Select("<script>select * from ${schema_name}._sc_work_flow_node_column_ext where column_id in " +
            " (select id from ${schema_name}._sc_work_flow_node_column where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach>)</script>")
    List<LinkedHashMap<String, Object>> findWorkTemplateColumExt(@Param("schema_name") String schema_name, @Param("wtCodes") List<String> wtCodes);

    @Select("<script>select * from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach></script>")
    List<LinkedHashMap<String, Object>> findWorkTemplateColumLinkage(@Param("schema_name") String schema_name, @Param("wtCodes") List<String> wtCodes);

    @Select("<script>select * from ${schema_name}._sc_work_flow_node_column_linkage_condition where linkage_id in " +
            " (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach>)</script>")
    List<LinkedHashMap<String, Object>> findWorkTemplateColumLinkageCondition(@Param("schema_name") String schema_name, @Param("wtCodes") List<String> wtCodes);

    @Select("select tab.* from (select * from ${schema_name}._sc_cloud_data where data_type in " +
            " (select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) " +
            " union " +
            " select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}))) " +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) and field_name='source' " +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where column_id in ( " +
            " select id from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            "  union " +
            "  select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))) and field_name='source') " +
            " union " +
            " select * from ${schema_name}._sc_cloud_data where parent_code in " +
            " (select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) " +
            " union " +
            " select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))" +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) and field_name='source' " +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where column_id in ( " +
            " select id from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            "  union " +
            "  select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))) and field_name='source')) tab")
    List<LinkedHashMap<String, Object>> findDataSource(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_work_flow_info (pref_id,flow_name,show_name,work_type_id,relation_type,work_template_code," +
            " create_time,create_user_id,flow_code) values (#{pm.pref_id},#{pm.flow_name},#{pm.show_name},#{pm.work_type_id}::int," +
            " #{pm.relation_type}::int,#{pm.work_template_code},#{pm.create_time}::timestamp,#{pm.create_user_id},#{pm.flow_code}) ")
    void insertWorkFlowInfo(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_work_flow_node (id,node_id,pref_id,node_name,show_name,node_type,deal_type_id,deal_role_id," +
            " remark,create_time,create_user_id,page_type,node_user) values (#{pm.id},#{pm.node_id},#{pm.pref_id},#{pm.node_name}," +
            " #{pm.show_name},#{pm.node_type},#{pm.deal_type_id}::int,#{pm.deal_role_id},#{pm.remark},#{pm.create_time}::timestamp," +
            " #{pm.create_user_id},#{pm.page_type},#{pm.node_user})")
    void insertWorkFlowNode(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_work_flow_node_column (id,node_id,field_form_code,field_code,field_name,save_type,change_type," +
            " field_right,field_value,is_required,is_table_key,db_table_type,field_remark,field_data_base,field_view_type,field_section_type," +
            " field_validate_method,create_time,create_user_id,field_is_show,data_order,word_flow_node_id,data_key) values (#{pm.id},#{pm.node_id}," +
            " #{pm.field_form_code},#{pm.field_code},#{pm.field_name},#{pm.save_type},#{pm.change_type},#{pm.field_right},#{pm.field_value}," +
            " #{pm.is_required},#{pm.is_table_key},#{pm.db_table_type},#{pm.field_remark},#{pm.field_data_base},#{pm.field_view_type}," +
            " #{pm.field_section_type},#{pm.field_validate_method},#{pm.create_time}::timestamp,#{pm.create_user_id},#{pm.field_is_show},#{pm.data_order}::int," +
            " #{pm.word_flow_node_id},#{pm.data_key}) ")
    void insertWorkFlowNodeColum(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_work_flow_node_column_ext (node_id,field_form_code,field_name,field_write_type,field_value," +
            " create_time,create_user_id,column_id) values (#{pm.node_id},#{pm.field_form_code},#{pm.field_name},#{pm.field_write_type}," +
            " #{pm.field_value},#{pm.create_time}::timestamp,#{pm.create_user_id},#{pm.column_id}) ")
    void insertWorkFlowNodeColumExt(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_work_flow_node_column_linkage (id,node_id,field_form_code,linkage_type,create_time," +
            " create_user_id,column_id) values (#{pm.id},#{pm.node_id},#{pm.field_form_code},#{pm.linkage_type}::int,#{pm.create_time}::timestamp," +
            " #{pm.create_user_id},#{pm.column_id}) ")
    void insertWorkFlowNodeColumLinkage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_work_flow_node_column_linkage_condition (linkage_id,link_field_form_code,interface_address," +
            " self_defined,condition,sub_page_template_code,edit_sub_page_template_code,scan_fail_return_template_code,is_data_from_page) values " +
            " (#{pm.linkage_id},#{pm.link_field_form_code},#{pm.interface_address},#{pm.self_defined},#{pm.condition}::jsonb," +
            " #{pm.sub_page_template_code},#{pm.edit_sub_page_template_code},#{pm.scan_fail_return_template_code},#{pm.is_data_from_page}) ")
    void insertWorkFlowNodeColumLinkageCondition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("<script>" +
            " <foreach collection='list' item='item' index='index' separator=';'> " +
            " insert into ${schema_name}._sc_work_template (work_template_code,work_template_name,work_type_id,relation_type," +
            " parent_code,body_property,is_use,remark,create_time,create_user_id,is_common_template,is_have_sub,template_type) " +
            " values (#{item.work_template_code},#{item.work_template_name},#{item.work_type_id}::int,#{item.relation_type}::int," +
            " #{item.parent_code},#{item.body_property}::jsonb,#{item.is_use},#{item.remark},#{item.create_time}::timestamp," +
            " #{item.create_user_id},#{item.is_common_template},#{item.is_have_sub},#{item.template_type}::int) " +
            " on conflict (work_template_code) " +
            " do update set work_template_name=#{item.work_template_name},work_type_id=#{item.work_type_id}::int," +
            " relation_type=#{item.relation_type}::int,parent_code=#{item.parent_code},body_property=#{item.body_property}::jsonb," +
            " is_use=#{item.is_use},remark=#{item.remark},create_time=#{item.create_time}::timestamp," +
            " create_user_id=#{item.create_user_id},is_common_template=#{item.is_common_template}," +
            " is_have_sub=#{item.is_have_sub},template_type=#{item.template_type}::int" +
            " </foreach>" +
            " </script> ")
    void insertOrUpdateWorkTemplate(@Param("schema_name") String schema_name, @Param("list") List<LinkedHashMap> list);

    @Update("update ${schema_name}._sc_work_flow_info set work_template_code=#{pm.work_template_code} where id=#{pm.id}::int")
    void updateWorkFlowInfo(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Update("update ${schema_name}._sc_work_flow_node_column_linkage_condition set sub_page_template_code=#{pm.sub_page_template_code}, " +
            " edit_sub_page_template_code=#{pm.edit_sub_page_template_code}, scan_fail_return_template_code=#{pm.scan_fail_return_template_code} where id=#{pm.id}::int")
    void updateFlowNodeColumLinkageCodition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_linkage_condition where linkage_id in " +
            " (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}))))")
    void deleteWorkTemplateColumLinkageCondition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))")
    void deleteWorkTemplateColumLinkage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_ext where column_id in " +
            " (select id from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))) ")
    void deleteWorkTemplateColumExt(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))")
    void deleteWorkTemplateColum(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from (select * from ${schema_name}._sc_cloud_data where data_type in " +
            " (select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) " +
            " union " +
            " select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}))) " +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) and field_name='source' " +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where column_id in ( " +
            " select id from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            "  union " +
            "  select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))) and field_name='source') " +
            " union " +
            " select * from ${schema_name}._sc_cloud_data where parent_code in " +
            " (select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) " +
            " union " +
            " select distinct data_key from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage " +
            " where node_id in (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))" +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id}) and field_name='source' " +
            " union " +
            " select distinct field_value as data_key from ${schema_name}._sc_work_flow_node_column_ext where column_id in ( " +
            " select id from ${schema_name}._sc_work_flow_node_column where node_id in " +
            " (select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pm.pref_id} " +
            "  union " +
            "  select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})) " +
            "  union " +
            "  select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            "  where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "  (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pm.pref_id})))) and field_name='source')) tab")
    void deleteDataSource(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Insert("insert into ${schema_name}._sc_cloud_data (code,name,data_type,is_lang,create_user_id,create_time,data_order," +
            " remark,type_remark,parent_code,reserve1,transfer_data_name) values (#{pm.code},#{pm.name},#{pm.data_type}," +
            " #{pm.is_lang}::int,#{pm.create_user_id},#{pm.create_time}::timestamp,#{pm.data_order}::int,#{pm.remark},#{pm.type_remark}," +
            " #{pm.parent_code},#{pm.reserve1},#{pm.transfer_data_name}) ")
    void insertDataSource(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_linkage_condition where linkage_id=#{pm.linkage_id}::int")
    void deleteWorkTemplateColumLinkageConditionBy(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_linkage where id=#{pm.id}::int")
    void deleteWorkTemplateColumLinkageBy(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column_ext where column_id=#{pm.column_id}::int")
    void deleteWorkTemplateColumExtBy(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    @Delete("delete from ${schema_name}._sc_work_flow_node_column where id=#{pm.id}::int")
    void deleteWorkTemplateColumBy(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> data);

    /**
     * 查询流程主键查询流程和节点上使用的模板编号
     *
     * @param schema_name 数据库
     * @param pref_id     流程主键
     * @return 模板编号集合
     */
    @Select(" <script>select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pref_id})) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pref_id})) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            " (select node_id from ${schema_name}._sc_work_flow_node where pref_id=#{pref_id}))</script>")
    List<String> findWtCodesForFlow(@Param("schema_name") String schema_name, @Param("pref_id") String pref_id);

    /**
     * 查询流程主键查询流程和节点上使用的模板编号
     *
     * @param schema_name 数据库
     * @param wtCodes     流程主键
     * @return 模板编号集合
     */
    @Select(" <script>select distinct work_template_code from ${schema_name}._sc_work_flow_info where pref_id=#{pref_id} " +
            " union " +
            " select distinct sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach>) " +
            " union " +
            " select distinct edit_sub_page_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach>) " +
            " union " +
            " select distinct scan_fail_return_template_code as work_template_code from ${schema_name}._sc_work_flow_node_column_linkage_condition " +
            " where linkage_id in (select id from ${schema_name}._sc_work_flow_node_column_linkage where node_id in " +
            "   <foreach collection='wtCodes' item='code' open='(' close=')' separator=','> #{code} </foreach>)</script>")
    List<String> findWtCodesForWt(@Param("schema_name") String schema_name, @Param("pref_id") String pref_id, @Param("wtCodes") List<String> wtCodes);

}
