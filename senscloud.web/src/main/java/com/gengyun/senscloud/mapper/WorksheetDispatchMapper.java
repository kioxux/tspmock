package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.WorkListModel;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface WorksheetDispatchMapper {
    /**
     * 获取人员
     *
     * @param schema_name
     * @param user_id
     * @param condition
     * @return
     */
    @Select("<script> SELECT DISTINCT u.* FROM ${schema_name}._sc_position_role as t1 " +
            "INNER JOIN ( SELECT setting_value FROM ${schema_name}._sc_system_config yc WHERE yc.config_name = 'executor_role' ) as s ON t1.role_id = ANY ( STRING_TO_ARRAY( s.setting_value, ',' ) ) " +
            "INNER JOIN ${schema_name}._sc_user_position as t2 ON t1.position_id = t2.position_id " +
            "INNER JOIN ${schema_name}._sc_user as u ON t2.user_id = u.ID  " +
            "INNER JOIN ${schema_name}._sc_user_position as t3 ON t3.user_id = u.ID " +
            "INNER JOIN ${schema_name}._sc_position_role as t4 ON t3.position_id = t4.position_id " +
            "INNER JOIN ${schema_name}._sc_role_asset_position as t5 ON t5.role_id = t4.role_id " +
            "WHERE u.status > -1000 " +
            "<if test=\"condition !=null and condition != '' \"> " +
            "AND u.user_name like concat('%', #{condition}, '%') " +
            "</if> " +
            "and t5.asset_position_code in ( " +
            "SELECT asset_position_code FROM ${schema_name}._sc_user_position as ta1 " +
            "INNER JOIN ${schema_name}._sc_position_role AS ta2 ON ta1.position_id = ta2.position_id " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS ta3 ON ta2.role_id = ta3.role_id " +
            "WHERE ta1.user_id = #{user_id} ) " +
            "order by u.create_time desc </script>")
    List<Map<String, Object>> getUser(@Param("schema_name") String schema_name, @Param("user_id") String user_id, @Param("condition") String condition);

    @Select("SELECT count(DISTINCT wd.sub_work_code)  " +
            "FROM ${schema_name}._sc_works AS w " +
            "LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code AND wd.is_main = 1 " +
            "LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id " +
            "LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = a.id " +
            "LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id = a.category_id " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON w.create_user_id = u.id " +
            "LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id = wd.duty_user_id " +
            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON bt.business_type_id = wt.business_type_id " +
            "LEFT JOIN ${schema_name}._sc_status AS s ON s.ID = w.status  " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.code = wd.priority_level::varchar AND cd.data_type = 'priority_level' " +
            "WHERE w.work_type_id IN ( " +
            "SELECT wt.ID FROM ${schema_name}._sc_work_type AS wt " +
            "INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.ID = rwt.work_type_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN A.position_code ELSE'' END IN ( " +
            "SELECT ap.position_code FROM ${schema_name}._sc_asset_position AS ap " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '2' THEN A.category_id IN ( " +
            "SELECT ac.ID FROM ${schema_name}._sc_asset_category AS ac " +
            "INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.ID = rat.category_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id}  ) ELSE 1 = 1 END  " +
            "${condition} ")
    int findCountWorksheetList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("condition") String condition);

    @Select("<script>SELECT CURRENT_TIMESTAMP - w.deadline_time > interval '0 days 00:00:00' AS is_overtime, wd.duty_user_id, wd.sub_work_code " +
            "FROM ${schema_name}._sc_works AS w " +
            "LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = a.id " +
            "INNER JOIN ( " +
            "SELECT DISTINCT u.ID FROM ${schema_name}._sc_position_role as t1 " +
            "INNER JOIN ( SELECT setting_value FROM ${schema_name}._sc_system_config yc WHERE yc.config_name = 'executor_role' ) as s ON t1.role_id = ANY ( STRING_TO_ARRAY( s.setting_value, ',' ) ) " +
            "INNER JOIN ${schema_name}._sc_user_position as t2 ON t1.position_id = t2.position_id " +
            "INNER JOIN ${schema_name}._sc_user as u ON t2.user_id = u.ID  " +
            "INNER JOIN ${schema_name}._sc_user_position as t3 ON t3.user_id = u.ID " +
            "INNER JOIN ${schema_name}._sc_position_role as t4 ON t3.position_id = t4.position_id " +
            "INNER JOIN ${schema_name}._sc_role_asset_position as t5 ON t5.role_id = t4.role_id " +
            "WHERE u.status > -1000 " +
            "<if test=\"condition !=null and condition != '' \"> " +
            "AND u.user_name like concat('%', #{condition}, '%') " +
            "</if> " +
            "and t5.asset_position_code in ( " +
            "SELECT asset_position_code FROM ${schema_name}._sc_user_position as ta1 " +
            "INNER JOIN ${schema_name}._sc_position_role AS ta2 ON ta1.position_id = ta2.position_id " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS ta3 ON ta2.role_id = ta3.role_id " +
            "WHERE ta1.user_id = #{user_id} ) " +
            ") u on u.id = wd.duty_user_id " +
            "WHERE wd.status = " + StatusConstant.PENDING + " and w.work_type_id IN ( " +
            "SELECT wt.ID FROM ${schema_name}._sc_work_type AS wt " +
            "INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.ID = rwt.work_type_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN A.position_code ELSE'' END IN ( " +
            "SELECT ap.position_code FROM ${schema_name}._sc_asset_position AS ap " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '2' THEN A.category_id IN ( " +
            "SELECT ac.ID FROM ${schema_name}._sc_asset_category AS ac " +
            "INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.ID = rat.category_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id}  ) ELSE 1 = 1 END </script> ")
    List<Map<String, Object>> findWorkListForCount(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("condition") String condition);

    //            "${condition} ")
//    @Select("SELECT DISTINCT  " +
//            "w.position_code,w.work_code,w.work_type_id,w.relation_type,w.title,w.status,w.create_user_id, " +
//            "to_char(w.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time,to_char(w.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time, " +
//            "to_char(w.create_time,'yyyy-MM-dd') as create_time,to_char(wd.plan_begin_time,'yyyy-MM-dd') as plan_begin_time, " +
//            "w.work_template_code,to_char( wd.begin_time, 'yyyy-MM-dd' ) AS begin_time, " +
//            "wd.priority_level,wd.relation_id,wd.duty_user_id,wd.receive_user_id,wd.sub_work_code, " +
//            "a.asset_code,a.asset_name,a.category_id,u.user_name,u.mobile,u2.user_name AS duty_user_name,wt.type_name AS work_type_name, " +
//            "bt.business_name,bt.business_type_id,ap.position_name,ac.category_name,s.status AS status_name,wd.is_main,wd.receive_time, " +
//            "case when cd.is_lang = '" + Constants.STATIC_LANG + "' then coalesce((select (c.resource->>cd.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{company_id}), cd.name) else cd.name end as priority_level_name " +
//            "FROM ${schema_name}._sc_works AS w " +
//            "LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
//            "LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code " +
//            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id " +
//            "LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = a.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id = a.category_id " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON w.create_user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id = wd.duty_user_id " +
//            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON bt.business_type_id = wt.business_type_id " +
//            "LEFT JOIN ${schema_name}._sc_status AS s ON s.ID = w.status  " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.code = wd.priority_level::varchar AND cd.data_type = 'priority_level' " +
//            "WHERE w.work_type_id IN ( " +
//            "SELECT wt.ID FROM ${schema_name}._sc_work_type AS wt " +
//            "INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.ID = rwt.work_type_id " +
//            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id " +
//            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
//            "WHERE up.user_id = #{user_id})  " +
//            "AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN A.position_code ELSE'' END IN ( " +
//            "SELECT ap.position_code FROM ${schema_name}._sc_asset_position AS ap " +
//            "INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code " +
//            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id " +
//            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
//            "WHERE up.user_id = #{user_id})  " +
//            "AND CASE WHEN w.relation_type = '2' THEN A.category_id IN ( " +
//            "SELECT ac.ID FROM ${schema_name}._sc_asset_category AS ac " +
//            "INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.ID = rat.category_id " +
//            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id " +
//            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
//            "WHERE up.user_id = #{user_id}  ) ELSE 1 = 1 END  " +
//            "${condition} ")
//    List<WorkListModel> findWorksheetList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("userLang") String userLang, @Param("company_id") Long companyId, @Param("condition") String condition);

    @Select("SELECT DISTINCT  " +
            "w.position_code,w.work_code,w.work_type_id,w.relation_type,w.title,w.status,w.create_user_id, " +
            "to_char(w.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time,to_char(w.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time, " +
            "to_char(w.create_time,'yyyy-MM-dd') as create_time,to_char(wd.plan_begin_time,'yyyy-MM-dd') as plan_begin_time, " +
            "w.work_template_code,to_char( wd.begin_time, 'yyyy-MM-dd' ) AS begin_time, " +
            "wd.priority_level,wd.relation_id,wd.duty_user_id,wd.receive_user_id,wd.sub_work_code, " +
            "a.asset_code,a.asset_name,a.category_id,u.user_name,u.mobile,u2.user_name AS duty_user_name,wt.type_name AS work_type_name, " +
            "bt.business_name,bt.business_type_id,ap.position_name,ac.category_name,s.status AS status_name,wd.is_main,wd.receive_time, " +
            "case when cd.is_lang = '" + Constants.STATIC_LANG + "' then cd.name else cd.name end as priority_level_name " +
            "FROM ${schema_name}._sc_works AS w " +
            "LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id " +
            "LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = a.id " +
            "LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id = a.category_id " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON w.create_user_id = u.id " +
            "LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id = wd.duty_user_id " +
            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON bt.business_type_id = wt.business_type_id " +
            "LEFT JOIN ${schema_name}._sc_status AS s ON s.ID = w.status  " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.code = wd.priority_level::varchar AND cd.data_type = 'priority_level' " +
            "WHERE w.work_type_id IN ( " +
            "SELECT wt.ID FROM ${schema_name}._sc_work_type AS wt " +
            "INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.ID = rwt.work_type_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN A.position_code WHEN w.relation_type = '4' THEN w.position_code ELSE'' END IN ( " +
            "SELECT ap.position_code FROM ${schema_name}._sc_asset_position AS ap " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '2' THEN A.category_id IN ( " +
            "SELECT ac.ID FROM ${schema_name}._sc_asset_category AS ac " +
            "INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.ID = rat.category_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id}  ) ELSE 1 = 1 END  " +
            "${condition} ")
    List<WorkListModel> findWorksheetList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("condition") String condition);

    @Select(" <script> " +
            "SELECT DISTINCT  " +
            "w.position_code,w.work_code,w.work_type_id,w.relation_type,w.title,w.status,w.create_user_id, " +
            "to_char(w.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time,to_char(w.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time, " +
            "to_char(w.create_time,'yyyy-MM-dd') as create_time,to_char(wd.plan_begin_time,'yyyy-MM-dd') as plan_begin_time, " +
            "w.work_template_code,to_char( wd.begin_time, 'yyyy-MM-dd' ) AS begin_time, " +
            "wd.priority_level,wd.relation_id,wd.duty_user_id,wd.receive_user_id,wd.sub_work_code, " +
            "a.asset_code,a.asset_name,a.category_id,u.user_name,u.mobile,u2.user_name AS duty_user_name,wt.type_name AS work_type_name, " +
            "bt.business_name,bt.business_type_id,ap.position_name,ac.category_name,s.status AS status_name,wd.is_main,wd.receive_time, " +
            "case when cd.is_lang = '" + Constants.STATIC_LANG + "' then cd.name else cd.name end as priority_level_name " +
            "FROM ${schema_name}._sc_works AS w " +
            "LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id " +
            "LEFT JOIN ${schema_name}._sc_asset AS a ON w.relation_id = a.id " +
            "LEFT JOIN ${schema_name}._sc_asset_category AS ac ON ac.id = a.category_id " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON w.create_user_id = u.id " +
            "LEFT JOIN ${schema_name}._sc_user AS u2 ON u2.id = wd.duty_user_id " +
            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON bt.business_type_id = wt.business_type_id " +
            "LEFT JOIN ${schema_name}._sc_status AS s ON s.ID = w.status  " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.code = wd.priority_level::varchar AND cd.data_type = 'priority_level' " +
            "WHERE w.work_type_id IN ( " +
            "SELECT wt.ID FROM ${schema_name}._sc_work_type AS wt " +
            "INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.ID = rwt.work_type_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '1' THEN w.relation_id WHEN w.relation_type = '2' THEN A.position_code ELSE'' END IN ( " +
            "SELECT ap.position_code FROM ${schema_name}._sc_asset_position AS ap " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id})  " +
            "AND CASE WHEN w.relation_type = '2' THEN A.category_id IN ( " +
            "SELECT ac.ID FROM ${schema_name}._sc_asset_category AS ac " +
            "INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON ac.ID = rat.category_id " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id}  ) ELSE 1 = 1 END  " +
            "${condition} " +
            " <if test=\"pagination != null and pagination != ''\"> " +
            " ${pagination} " +
            " </if> " +
            " </script> ")
    List<WorkListModel> findWorksheetListByLimit(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("condition") String condition, @Param("pagination") String pagination);

//    /**
//     * 根据子工单编号查询工单信息列表
//     *
//     * @param schemaName
//     * @param idStr
//     * @return
//     */
//    @Select("<script> SELECT DISTINCT " +
//            "w.work_code,wd.sub_work_code,wt.type_name AS work_type_name,w.work_type_id,s.status AS status_name,w.status,wd.priority_level,w.title, " +
//            "case when cd.is_lang = '2' then coalesce((select (c.resource->>cd.name)::jsonb->>#{user_lang} from public.company_resource c where c.company_id = #{company_id}), cd.name) else cd.name end as priority_level_name, " +
//            "w.position_code,ap.position_name,wd.plan_begin_time,u.user_name,u.mobile,w.create_time,w.deadline_time, " +
//            "w.relation_type,w.create_user_id,w.occur_time,wd.relation_id,wd.receive_user_id,wd.begin_time,wd.is_main " +
//            "FROM ${schema_name}._sc_works_detail AS wd  " +
//            "JOIN ${schema_name}._sc_works AS w  ON w.work_code = wd.work_code  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code  " +
//            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id  " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON w.create_user_id = u.id  " +
//            "LEFT JOIN ${schema_name}._sc_status AS s ON s.ID = w.status   " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.code = wd.priority_level::varchar AND cd.data_type = 'priority_level'  " +
//            "WHERE wd.sub_work_code IN " +
//            "<foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach> </script> ")
//    List<WorkListModel> findWorkListDetailBySwc(@Param("schema_name") String schemaName, @Param("ids") String[] idStr, @Param("user_lang") String user_lang, @Param("company_id") Long company_id);

    @Select("<script> SELECT DISTINCT " +
            "w.work_code,wd.sub_work_code,wt.type_name AS work_type_name,w.work_type_id,s.status AS status_name,w.status,wd.priority_level,w.title, " +
            "case when cd.is_lang = '2' then cd.name else cd.name end as priority_level_name, " +
            "w.position_code,ap.position_name,wd.plan_begin_time,u.user_name,u.mobile,w.create_time,w.deadline_time, " +
            "w.relation_type,w.create_user_id,w.occur_time,wd.relation_id,wd.receive_user_id,wd.begin_time,wd.is_main " +
            "FROM ${schema_name}._sc_works_detail AS wd  " +
            "JOIN ${schema_name}._sc_works AS w  ON w.work_code = wd.work_code  " +
            "LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code  " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.id = wd.work_type_id  " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON w.create_user_id = u.id  " +
            "LEFT JOIN ${schema_name}._sc_status AS s ON s.ID = w.status   " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.code = wd.priority_level::varchar AND cd.data_type = 'priority_level'  " +
            "WHERE wd.sub_work_code IN " +
            "<foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach> </script> ")
    List<WorkListModel> findWorkListDetailBySwc(@Param("schema_name") String schemaName, @Param("ids") String[] idStr);

    @Select("SELECT business_type_id::varchar,business_name FROM ${schema_name}._sc_business_type")
    @MapKey("business_type_id")
    Map<String, Map<String, String>> findBusinessTypeMap(@Param("schema_name") String schema_name);
}
