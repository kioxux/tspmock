package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.sql.Timestamp;

/**
 * 工单工时记录表
 * User: sps
 * Date: 2020/04/27
 * Time: 下午15:00
 */
@Mapper
public interface WorkRecordHourMapper {
//    /**
//     * 新增工单工时记录
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @param operate_account
//     * @param begin_time
//     * @param finished_time
//     * @return
//     */
//    @Insert("INSERT INTO ${schema_name}._sc_work_record_hour (sub_work_code, operate_account, begin_time, finished_time) " +
//            "VALUES (#{sub_work_code},#{operate_account},#{begin_time},#{finished_time})")
//    int insertWorkRecordHour(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code,
//                             @Param("operate_account") String operate_account, @Param("begin_time") Timestamp begin_time,
//                             @Param("finished_time") Timestamp finished_time);
//
    /**
     * 更新工单工时记录
     *
     * @param schema_name 入参
     * @param sub_work_code 入参
     * @param operate_user_id 入参
     * @param finished_time 入参
     */
    @Update("UPDATE ${schema_name}._sc_work_record_hour SET finished_time = #{finished_time} " +
            "where sub_work_code = #{sub_work_code}  " +
            "and id = (select max(id) from ${schema_name}._sc_work_record_hour where sub_work_code = #{sub_work_code} " +
            "and operate_user_id = #{operate_user_id}) ")
    int updateWorkRecordHour(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code,
                             @Param("operate_user_id") String operate_user_id, @Param("finished_time") Timestamp finished_time);

    /**
     * 更新工单总工时
     *
     * @param schema_name 入参
     * @param sub_work_code 入参
     */
    @Update("UPDATE ${schema_name}._sc_works_detail " +
            "SET total_deal_time = (select round((extract(epoch FROM (sum(finished_time-begin_time)))/60)::numeric,2) " +
            "   from ${schema_name}._sc_work_record_hour " +
            "   where sub_work_code = #{sub_work_code} and begin_time is not null and finished_time is not null) " +
            "where sub_work_code = #{sub_work_code}")
    int updateWorkTotalHour(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);

//    /**
//     * 根据信息统计未完成工单工时记录
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @param operate_account
//     * @param isCheckNull
//     * @return
//     */
//    @Select("<script>" +
//            "select count(1) from ${schema_name}._sc_work_record_hour " +
//            "where sub_work_code = #{sub_work_code} and operate_account = #{operate_account} " +
//            "<if test='isCheckNull != null'> " +
//            "   and finished_time is not null and id = (select max(id) from ${schema_name}._sc_work_record_hour where sub_work_code = #{sub_work_code} and operate_account = #{operate_account}) " +
//            "</if>" +
//            "</script>")
//    int countWrhByInfo(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code,
//                       @Param("operate_account") String operate_account, @Param("isCheckNull") String isCheckNull);
}
