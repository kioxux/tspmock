package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface StatisticMapper {
//    /**
//     * 获取统计配置详情
//     *
//     * @param schema_name 入参
//     * @param id          入参那
//     * @return 统计配置详情
//     */
//    @Select(" SELECT s.id,s.title,s.description,s.options::text,s.query,s.create_user_id,s.create_time,s.query_conditions::text,s.table_query,s.export_url," +
//            " s.table_columns::text,s.is_show_dashboard::varchar,s.client_page,s.group_name::varchar, " +
//            "coalesce((select (c.resource->>cd.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{company_id}), cd.name)as group_method_name " +
//            "FROM ${schema_name}._sc_statistic AS s " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.data_type = 'statistic_config_group' AND cd.code = s.group_name::varchar " +
//            " WHERE s.id = #{id} ")
//    Map<String, Object> findInfoById(@Param("schema_name") String schema_name, @Param("id") Integer id, @Param("userLang") String userLang, @Param("company_id") Long company_id);

    /**
     * 获取统计配置详情
     *
     * @param schema_name 入参
     * @param id          入参那
     * @return 统计配置详情
     */
    @Select(" SELECT s.id,s.title,s.description,s.options::text,s.query,s.create_user_id,s.create_time,s.query_conditions::text,s.table_query,s.export_url," +
            " s.table_columns::text,s.is_show_dashboard::varchar,s.client_page,s.group_name::varchar, " +
            " cd.name as group_method_name " +
            "FROM ${schema_name}._sc_statistic AS s " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.data_type = 'statistic_config_group' AND cd.code = s.group_name::varchar " +
            " WHERE s.id = #{id} ")
    Map<String, Object> findInfoById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 获取统计配置数量
     *
     * @param schemaName 入参
     * @param searchWord 入参
     * @return
     */
    @Select("SELECT count(1) FROM ${schema_name}._sc_statistic s ${condition} ")
    int findCountStatisticConfigList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);

//    /**
//     * 获取统计配置列表
//     *
//     * @param schemaName 入参
//     * @param searchWord 入参
//     * @return
//     */
//    @Select("SELECT s.id,title,description,is_show_dashboard::varchar,client_page,group_name::varchar, " +
//            "coalesce((select (c.resource->>cd.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{company_id}), cd.name)as group_method_name " +
//            "FROM ${schema_name}._sc_statistic s " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.data_type = 'statistic_config_group' AND cd.code = s.group_name::varchar " +
//            "${condition} ")
//    List<Map<String, Object>> findStatisticConfigList(@Param("schema_name") String schemaName, @Param("userLang") String userLang, @Param("company_id") Long company_id, @Param("condition") String searchWord);

    @Select("SELECT s.id,title,description,is_show_dashboard::varchar,client_page,group_name::varchar, " +
            "cd.name as group_method_name " +
            "FROM ${schema_name}._sc_statistic s " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.data_type = 'statistic_config_group' AND cd.code = s.group_name::varchar " +
            "${condition} ")
    List<Map<String, Object>> findStatisticConfigList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);


    /**
     * 添加统计分析配置
     *
     * @param paramMap 入参
     */
    @Insert("INSERT INTO ${schema_name}._sc_statistic(title, description, options, query, create_user_id, create_time, " +
            "query_conditions, table_query, table_columns, is_show_dashboard, client_page,group_name,export_url) " +
            "VALUES (#{title}, #{description}, #{options, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler}, " +
            "#{query}, #{create_user_id}, #{create_time}, #{query_conditions, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler}," +
            " #{table_query}, #{table_columns, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler}, #{is_show_dashboard}::int, " +
            "#{client_page},#{group_name}::int,#{export_url}) ")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insertStatistic(Map<String, Object> paramMap);

    /**
     * 编辑统计配置
     *
     * @param paramMap
     */
    @Update("<script>" +
            "UPDATE ${schema_name}._sc_statistic " +
            "<set> " +
            "<if test=\"title !=null and title !=''\"> " +
            "title = #{title}, " +
            "</if> " +
            "<if test=\"description !=null and description !=''\"> " +
            "description = #{description}, " +
            "</if> " +
            "<if test=\"options !=null \"> " +
            "options = #{options, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler}, " +
            "</if> " +
            "<if test=\"query !=null and query !=''\"> " +
            "query = #{query}, " +
            "</if> " +
            "<if test=\"query_conditions !=null \"> " +
            "query_conditions = #{query_conditions, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler}, " +
            "</if> " +
            "<if test=\"table_query !=null and table_query !=''\"> " +
            "table_query = #{table_query}, " +
            "</if> " +
            "<if test=\"table_columns !=null \"> " +
            "table_columns = #{table_columns, typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler}, " +
            "</if> " +
            "<if test=\"is_show_dashboard !=null and is_show_dashboard !=''\"> " +
            "is_show_dashboard = #{is_show_dashboard}::int, " +
            "</if> " +
            "<if test=\"group_name !=null and group_name !=''\"> " +
            "group_name = #{group_name}::int, " +
            "</if> " +
            "<if test=\"client_page !=null and client_page !=''\"> " +
            "client_page = #{client_page}, " +
            "</if> " +
            "<if test=\"export_url !=null\"> " +
            "export_url = #{export_url}, " +
            "</if> " +
            "</set> " +
            "WHERE id = #{id}::int " +
            "</script> ")
    void updateStatistic(Map<String, Object> paramMap);

    /**
     * 删除选中统计配置
     *
     * @param schemaName
     * @param idArr
     */
    @Delete("<script> DELETE FROM ${schema_name}._sc_statistic WHERE id IN " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> </script> ")
    void deleteSelectStatistic(@Param("schema_name") String schemaName, @Param("ids") String[] idArr);

//    @Select("(SELECT s.id,s.title,s.description,'self' as group_name,coalesce((select (c.resource->>'title_ain')::jsonb->>#{userLang} " +
//            "from public.company_resource c where c.company_id = #{company_id}), '默认展示') as group_method_name  " +
//            "FROM ${schema_name}._sc_statistic s JOIN ${schema_name}._sc_statistic_self ss ON s.id=ss.statistic_id " +
//            "WHERE ss.user_id = #{user_id} ${condition} ORDER BY s.title)" +
//            "UNION ALL " +
//            "(SELECT s.id,s.title,s.description,coalesce(s.group_name::varchar,'') as group_name,case when s.group_name is null THEN '-' " +
//            "else coalesce((select (c.resource->>cd.name)::jsonb->>#{userLang} " +
//            "from public.company_resource c where c.company_id = #{company_id}), cd.name) end as group_method_name  " +
//            "FROM ${schema_name}._sc_statistic s " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.data_type = 'statistic_config_group' AND cd.code = s.group_name::varchar " +
//            "WHERE 1=1 ${condition} ORDER BY group_name,s.title)" +
//            "union all " +
//            "(select s.id,s.title,s.description,'dashboard' as group_name, '看板'as group_method_name " +
//            "FROM ${schema_name}._sc_statistic s " +
//            "WHERE s.is_show_dashboard = 1 ORDER BY s.title)")
//    List<Map<String, Object>> findStatisticList(@Param("schema_name") String schemaName, @Param("user_id") String user_id, @Param("userLang") String userLang, @Param("company_id") Long company_id, @Param("condition") String searchWord);

    @Select("(SELECT s.id,s.title,s.description,'self' as group_name," +
            "'默认展示' as group_method_name  " +
            "FROM ${schema_name}._sc_statistic s JOIN ${schema_name}._sc_statistic_self ss ON s.id=ss.statistic_id " +
            "WHERE ss.user_id = #{user_id} ${condition} ORDER BY s.title)" +
            "UNION ALL " +
            "(SELECT s.id,s.title,s.description,coalesce(s.group_name::varchar,'') as group_name,case when s.group_name is null THEN '-' " +
            "else cd.name end as group_method_name  " +
            "FROM ${schema_name}._sc_statistic s " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON cd.data_type = 'statistic_config_group' AND cd.code = s.group_name::varchar " +
            "WHERE 1=1 ${condition} ORDER BY group_name,s.title)" +
            "union all " +
            "(select s.id,s.title,s.description,'dashboard' as group_name, '看板'as group_method_name " +
            "FROM ${schema_name}._sc_statistic s " +
            "WHERE s.is_show_dashboard = 1 ORDER BY s.title)")
    List<Map<String, Object>> findStatisticList(@Param("schema_name") String schemaName, @Param("user_id") String user_id, @Param("condition") String searchWord);
//    List<Statistic> findStatisticList(@Param("schema_name")String schemaName, @Param("user_id")String user_id, @Param("userLang") String userLang, @Param("company_id") Long company_id, @Param("condition") String searchWord);

    /**
     * 删除选中看板报表
     *
     * @param schemaName
     * @param userId
     * @param id
     */
    @Delete("DELETE FROM ${schema_name}._sc_statistic_self WHERE user_id = #{user_id} and statistic_id=#{id}::int ")
    int deleteUserStatisticDash(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("id") String id);

    /**
     * 添加用户看板报表
     *
     * @param schemaName
     * @param userId
     * @param id
     */
    @Insert("insert into ${schema_name}._sc_statistic_self (statistic_id,user_id) " +
            "select #{id}::int,#{user_id} " +
            "where not exists (select 1 from ${schema_name}._sc_statistic_self where statistic_id=#{id}::int and user_id=#{user_id})")
    int insertUserStatisticDash(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("id") String id);


    /**
     * 设置非启用看板
     *
     * @param data
     */
    @Update("update ${schema_name}._sc_statistic set is_show_dashboard = 0 where id != #{id}::int ")
    void setDisableDashBoardWithOutChose(Map<String, Object> data);

    @Select("select count(1) from ${schema_name}._sc_statistic where is_show_dashboard = 1")
    int findCountShowDashboard(Map<String, Object> data);


    /**
     * 查询首页显示的图表
     * @param schemaName 入参
     * @return 首页显示的图表
     */
    @Select(" SELECT s.id,s.title,s.description::text,s.options::text,s.query::text,s.create_user_id,s.create_time,s.query_conditions::text," +
            " s.table_query::text,s.table_columns::text,s.is_show_dashboard,s.client_page,s.group_name,s.export_url" +
            " FROM ${schema_name}._sc_statistic s " +
            " WHERE s.is_show_dashboard = 1 order by id asc")
    List<Map<String, Object>> findStatisticShowDashboardList(@Param("schema_name") String schemaName);

}
