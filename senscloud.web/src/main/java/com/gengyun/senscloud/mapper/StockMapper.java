package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.StockData;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/25.
 */
@Mapper
public interface StockMapper {

    /**
     * 查询库房信息
     *
     * @param schema_name
     * @param stock_code
     * @return
     */
    @Select("select b.*,s.username from ${schema_name}._sc_stock b " +
            "left join ${schema_name}._sc_user s on b.manager_account=s.account " +
            "where stock_code= #{stock_code}")
    StockData findStockByStockCode(@Param("schema_name") String schema_name, @Param("stock_code") String stock_code);

    /**
     * 带权限的库房数量
     *
     * @param schemaName
     * @param userId
     * @param condition
     * @return
     */
    @Select("select count(DISTINCT s.id) " +
            "from ${schema_name}._sc_stock s " + SqlConstant.STOCK_AUTH_SQL +
            "left join ${schema_name}._sc_user u on s.manager_user_id=u.id " +
            "left join ${schema_name}._sc_currency c on s.currency_id=c.id " +
            "WHERE 1=1 ${condition} ")
    int findCountStockList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("condition") String condition);

    /**
     * 查询库房列表
     *
     * @param schemaName
     * @param userId
     * @param condition
     * @return
     */
    @Select("select distinct t.* from (select s.id,s.stock_code,s.stock_name,c.currency_name,s.currency_id, " +
            "s.manager_user_id,u.user_name,s.address,s.is_use " +
            "from ${schema_name}._sc_stock s " + SqlConstant.STOCK_AUTH_SQL +
            "left join ${schema_name}._sc_user u on s.manager_user_id=u.id " +
            "left join ${schema_name}._sc_currency c on s.currency_id=c.id " +
            "WHERE 1=1 ${condition} ) t ")
    List<Map<String, Object>> findStockList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("condition") String condition);


    /**
     * 添加库房
     *
     * @param data
     * @return
     */
    @Insert("INSERT INTO ${schema_name}._sc_stock(stock_code, stock_name, manager_user_id, create_time, create_user_id, is_use, address, currency_id, status,remark,security_quantity_msg_interval) " +
            "VALUES (#{stock_code}, #{stock_name}, #{manager_user_id}, #{create_time}, #{create_user_id}, #{is_use}, #{address}, #{currency_id}, #{status},#{remark},#{security_quantity_msg_interval} ::int)")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertStock(Map<String, Object> data);


    /**
     * 库房详情
     *
     * @param schemaName
     * @param id
     * @return
     */
    @Select("select s.id,s.stock_code,s.stock_name,c.currency_name,s.currency_id,  " +
            "s.manager_user_id,u.user_name,s.address,s.is_use,s.location,s.remark,s.security_quantity_msg_time,s.security_quantity_msg_interval  " +
            "from ${schema_name}._sc_stock s  " +
            "left join ${schema_name}._sc_user u on s.manager_user_id=u.id  " +
            "left join ${schema_name}._sc_currency c on s.currency_id=c.id  " +
            "WHERE s.id = #{id}::int")
    Map<String, Object> findStockById(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 编辑库房
     *
     * @param data
     * @return
     */
    @Update("<script> " +
            "UPDATE ${schema_name}._sc_stock  " +
            "<set> " +
            "<if test=\"stock_code !=null and stock_code !=''\"> " +
            "stock_code = #{stock_code}, " +
            "</if> " +
            "<if test=\"stock_name !=null and stock_name !=''\"> " +
            "stock_name = #{stock_name}, " +
            "</if> " +
            "<if test=\"manager_user_id !=null and manager_user_id !=''\"> " +
            "manager_user_id = #{manager_user_id}, " +
            "</if> " +
            "<if test=\"create_time !=null \"> " +
            "create_time = #{create_time}, " +
            "</if> " +
            "<if test=\"create_user_id !=null and create_user_id !=''\"> " +
            "create_user_id = #{create_user_id}, " +
            "</if> " +
            "<if test=\"is_use !=null and is_use !=''\"> " +
            "is_use = #{is_use}, " +
            "</if> " +
            "<if test=\"address !=null and address !=''\"> " +
            "address = #{address}, " +
            "</if> " +
            "<if test=\"currency_id !=null \"> " +
            "currency_id = #{currency_id}, " +
            "</if> " +
            "<if test=\"status !=null \"> " +
            "status = #{status}, " +
            "</if> " +
            "<if test=\"location !=null \"> " +
            "location = #{location}, " +
            "</if> " +
            "<if test=\"remark !=null and remark !=''\"> " +
            "remark = #{remark}, " +
            "</if> " +
            "<if test=\"security_quantity_msg_interval !=null \"> " +
            "security_quantity_msg_interval = #{security_quantity_msg_interval} ::int, " +
            "</if> " +
            "</set> " +
            "WHERE id = #{id}::int " +
            "</script>")
    int updateStock(Map<String, Object> data);

    /**
     * 库房备件数量
     *
     * @param schemaName
     * @param idStr
     * @return
     */
    @Select("<script> " +
            "SELECT COALESCE(sum(quantity),0) FROM (SELECT case WHEN quantity > 0 then quantity else 0 end as quantity " +
            "FROM ${schema_name}._sc_bom_stock bs WHERE bs.stock_id IN" +
            "<foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> ) t </script>")
    int findSumBomStockByStockId(@Param("schema_name") String schemaName, @Param("ids") String[] idStr);


    /**
     * 修改库房状态
     *
     * @param schemaName
     * @param idStr
     * @param status
     * @return
     */
    @Update(" <script> update ${schema_name}._sc_stock set status = #{status} where id in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> </script> ")
    int updateSelectStockStatus(@Param("schema_name") String schemaName, @Param("ids") String[] idStr, @Param("status") int status);

    /**
     * 查询库房库存清单个数
     *
     * @param schemaName
     * @param id
     * @param condition
     * @return
     */
    @Select("SELECT count(1) "+
            "FROM (SELECT distinct b.bom_name,b.material_code,b.bom_model,bt.type_name,b.type_id,b.id,bs.id as bom_stock_id, " +
            "bs.quantity,b.unit_id,u.unit_name,bs.store_position,bs.security_quantity,bs.max_security_quantity,COALESCE(bs.show_price,0) as show_price " +
            "FROM ${schema_name}._sc_stock s " +
            "JOIN ${schema_name}._sc_bom_stock bs ON s.id=bs.stock_id " +
            "JOIN ${schema_name}._sc_bom b ON bs.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id = u.id " +
            "WHERE b.status=1 and s.id = #{id}::int ${condition} ) t")
    int findCountStockBomList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("condition") String condition);

    /**
     * 查询库房库存清单个数
     *
     * @param schemaName
     * @param id
     * @param condition
     * @return
     */
    @Select("SELECT distinct b.bom_name,b.material_code,b.bom_model,bt.type_name,b.type_id,b.id,bs.id as bom_stock_id, " +
            "bs.quantity,b.unit_id,u.unit_name,bs.store_position,bs.security_quantity,bs.max_security_quantity,COALESCE(bs.show_price,0) as show_price " +
            "FROM ${schema_name}._sc_stock s " +
            "JOIN ${schema_name}._sc_bom_stock bs ON s.id=bs.stock_id " +
            "JOIN ${schema_name}._sc_bom b ON bs.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
            "LEFT JOIN ${schema_name}._sc_unit u ON b.unit_id = u.id " +
            "WHERE b.status=1 and s.id = #{id}::int ${condition} ")
    List<Map<String, Object>> findStockBomList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("condition") String condition);

    /**
     * 设置库房安全库存
     *
     * @param data
     */
    @Update("UPDATE ${schema_name}._sc_bom_stock  " +
            "SET security_quantity = #{security_quantity}, " +
            "max_security_quantity = #{max_security_quantity}," +
            "store_position = #{store_position},  " +
            "show_price = #{show_price} " +
            "WHERE id = #{id}::int ")
    void updateSafeStockByBomStockId(Map<String, Object> data);

    //    @Select("SELECT count(1) FROM (select DISTINCT ba.transfer_code as code,bad.bom_code,bad.bom_id,bad.quantity,bad.real_quantity, " +
//            "ba.create_user_id,u.user_name,ba.create_time,s.id as stock_id,s.stock_name, " +
//            "sta.status as status_name,ba.status,1 as page_type " +
//            "from ${schema_name}._sc_bom_allocation as ba " +
//            "JOIN ${schema_name}._sc_bom_allocation_detail bad ON bad.transfer_code = ba.transfer_code " +
//            "JOIN ${schema_name}._sc_stock AS s ON s.ID = ba.from_stock_id " +
//            "join ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id " +
//            "join ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bad.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "join ${schema_name}._sc_status AS sta ON sta.ID = ba.status " +
//            "JOIN ${schema_name}._sc_user AS u ON u.ID = ba.create_user_id " +
//            "JOIN ${schema_name}._sc_bom AS b ON bad.bom_id = b.ID " +
//            "where bs.stock_id = #{id}::int ${condition} " +
//            "union all " +
//            "select DISTINCT re.recipient_code,brd.bom_code,brd.bom_id,brd.quantity,brd.real_quantity,re.create_user_id, " +
//            "u.user_name,re.create_time,s.id as stock_id,s.stock_name, " +
//            "sta.status as status_name,re.status,2 as page_type " +
//            "from ${schema_name}._sc_bom_recipient as re " +
//            "JOIN ${schema_name}._sc_bom_recipient_detail brd ON brd.recipient_code = re.recipient_code " +
//            "join ${schema_name}._sc_stock as s on s.id=re.stock_id " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id " +
//            "join ${schema_name}._sc_status as sta on sta.id=re.status " +
//            "join ${schema_name}._sc_user as u on u.id=re.create_user_id " +
//            "JOIN ${schema_name}._sc_bom as b ON brd.bom_id = b.id " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = brd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "where bs.stock_id = #{id}::int ${condition} " +
//            "union all " +
//            "SELECT DISTINCT bd.discard_code,bdd.bom_code,bdd.bom_id,bdd.quantity,bdd.real_quantity,bd.create_user_id, " +
//            "u.user_name,bd.create_time,s.id as stock_id,s.stock_name, " +
//            "st.status as status_name,bd.status,3 as page_type " +
//            "FROM ${schema_name}._sc_bom_discard bd " +
//            "JOIN ${schema_name}._sc_bom_discard_detail bdd ON bdd.discard_code = bd.discard_code " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bd.stock_id " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bd.status " +
//            "join ${schema_name}._sc_user u on bd.create_user_id=u.id " +
//            "JOIN ${schema_name}._sc_bom as b ON bdd.bom_id = b.id " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bdd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "where bs.stock_id = #{id}::int ${condition} " +
//            "union all " +
//            "SELECT DISTINCT bis.in_code,bisd.bom_code,bisd.bom_id,bisd.quantity,bisd.real_quantity, bis.create_user_id,u.user_name, " +
//            "bis.create_time,s.id as stock_id,s.stock_name,st.status as status_name, bis.status,4 as page_type " +
//            "FROM ${schema_name}._sc_bom_in_stock bis " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON bis.stock_id = bs.stock_id " +
//            "JOIN ${schema_name}._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bis.stock_id " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bis.status " +
//            "join ${schema_name}._sc_user u on bis.create_user_id=u.id " +
//            "JOIN ${schema_name}._sc_bom as b ON bisd.bom_id = b.id " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bisd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "WHERE bs.stock_id = #{id}::int ${condition} " +
//            ") t   ")
    @Select("select count(1) from (select tab.* from " +
            "(select DISTINCT bw.work_code as code,bwdi.bom_id,bwdi.quantity,bwdi.material_code, " +
            "bwdi.real_quantity,bw.status,wt.type_name as process_method,wt.type_name, " +
            "bw.create_user_id,u.user_name,to_char(bw.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwdi.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail_item bwdi ON bwd.sub_work_code = bwdi.sub_work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON (s.ID = bw.stock_id or s.ID = bw.to_stock_id) " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bw.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwdi.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where s.id = #{id}::int " +
            "and bw.status = " + StatusConstant.COMPLETED + " and bw.to_stock_id is null ${condition} " +
            "UNION ALL " +
            "select DISTINCT bw.work_code as code,bwar.bom_id,bwar.quantity,bwar.material_code, " +
            "bwar.real_quantity,bwar.status,bwar.type_name as process_method,bwar.type_name, " +
            "bwar.create_user_id,u.user_name,to_char(bwar.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwar.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_allot_record bwar ON bw.work_code = bwar.work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON s.ID = bwar.stock_id " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bw.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwar.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where s.id = #{id}::int " +
            "and bw.status = " + StatusConstant.COMPLETED + " and bw.to_stock_id is not null ${condition}) tab " +
            "ORDER BY tab.create_time desc,tab.bom_id desc) t ")
    int findCountStockInAndOutList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("condition") String condition);

    @Select("select count(1) from (select bi.* " +
            "from ${schema_name}._sc_bom_import bi " +
            "left JOIN ${schema_name}._sc_bom AS b ON b.ID = bi.bom_id " +
            "left JOIN ${schema_name}._sc_stock AS s ON s.ID = bi.stock_id " +
            "where s.id = #{id}::int " +
            "${condition}) t ")
    int findCountStockBomImportList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("condition") String condition);

    @Select("select bi.*,b.bom_name as cur_bom_name,s.stock_name as cur_stock_name " +
            "from ${schema_name}._sc_bom_import bi " +
            "left JOIN ${schema_name}._sc_bom AS b ON b.ID = bi.bom_id " +
            "left JOIN ${schema_name}._sc_stock AS s ON s.ID = bi.stock_id " +
            "where s.id = #{id}::int " +
            "${condition} " +
            "ORDER BY bi.create_time desc ${pagination} ")
    List<Map<String, Object>> findStockBomImportList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("condition") String condition, @Param("pagination") String pagination);

    //    @Select("SELECT * FROM (select DISTINCT ba.transfer_code as code,bad.bom_code,bad.bom_id,bad.quantity,bad.real_quantity, " +
//            "ba.create_user_id,u.user_name,ba.create_time,s.id as stock_id,s.stock_name,bad.area_code_out as store_position, " +
//            "sta.status as status_name,ba.status,coalesce((select (c.resource->>'title_aaz_r')::jsonb->>#{userLang} " +
//            "from public.company_resource c where c.company_id = #{company_id}), '调拨')as process_method,1 as page_type " +
//            "from ${schema_name}._sc_bom_allocation as ba " +
//            "JOIN ${schema_name}._sc_bom_allocation_detail bad ON bad.transfer_code = ba.transfer_code " +
//            "JOIN ${schema_name}._sc_stock AS s ON s.ID = ba.from_stock_id " +
//            "join ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id " +
//            "join ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bad.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "join ${schema_name}._sc_status AS sta ON sta.ID = ba.status " +
//            "JOIN ${schema_name}._sc_user AS u ON u.ID = ba.create_user_id " +
//            "JOIN ${schema_name}._sc_bom AS b ON bad.bom_id = b.ID " +
//            "where bs.stock_id = #{id}::int ${condition} " +
//            "union all " +
//            "select DISTINCT re.recipient_code,brd.bom_code,brd.bom_id,brd.quantity,brd.real_quantity,re.create_user_id, " +
//            "u.user_name,re.create_time,s.id as stock_id,s.stock_name,brd.area_code, " + //bs.store_position,
//            "sta.status as status_name,re.status,coalesce((select (c.resource->>'btn_ac')::jsonb->>#{userLang} " +
//            "from public.company_resource c where c.company_id = #{company_id}), '领用')as type,2 as page_type " +
//            "from ${schema_name}._sc_bom_recipient as re " +
//            "JOIN ${schema_name}._sc_bom_recipient_detail brd ON brd.recipient_code = re.recipient_code " +
//            "join ${schema_name}._sc_stock as s on s.id=re.stock_id " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id " +
//            "join ${schema_name}._sc_status as sta on sta.id=re.status " +
//            "join ${schema_name}._sc_user as u on u.id=re.create_user_id " +
//            "JOIN ${schema_name}._sc_bom as b ON brd.bom_id = b.id " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = brd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "where bs.stock_id = #{id}::int ${condition} " +
//            "union all " +
//            "SELECT DISTINCT bd.discard_code,bdd.bom_code,bdd.bom_id,bdd.quantity,bdd.real_quantity,bd.create_user_id, " +
//            "u.user_name,bd.create_time,s.id as stock_id,s.stock_name,bdd.area_code, " + //bs.store_position,
//            "st.status as status_name,bd.status,coalesce((select (c.resource->>'title_abaa_d')::jsonb->>#{userLang} " +
//            "from public.company_resource c where c.company_id = #{company_id}), '报废')as type,3 as page_type " +
//            "FROM ${schema_name}._sc_bom_discard bd " +
//            "JOIN ${schema_name}._sc_bom_discard_detail bdd ON bdd.discard_code = bd.discard_code " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bd.stock_id " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON s.id = bs.stock_id " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bd.status " +
//            "join ${schema_name}._sc_user u on bd.create_user_id=u.id " +
//            "JOIN ${schema_name}._sc_bom as b ON bdd.bom_id = b.id " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bdd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "where bs.stock_id = #{id}::int ${condition} " +
//            "union all " +
//            "SELECT DISTINCT bis.in_code,bisd.bom_code,bisd.bom_id,bisd.quantity,bisd.real_quantity, bis.create_user_id,u.user_name, " +
//            "bis.create_time,s.id as stock_id,s.stock_name,bisd.area_code, st.status as status_name, bis.status, " +
//            "coalesce((select (c.resource->>'btn_k')::jsonb->>#{userLang} " +
//            "from public.company_resource c where c.company_id = #{company_id}), '入库') as type,4 as page_type " +
//            "FROM ${schema_name}._sc_bom_in_stock bis " +
//            "JOIN ${schema_name}._sc_bom_stock bs ON bis.stock_id = bs.stock_id " +
//            "JOIN ${schema_name}._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code " +
//            "JOIN ${schema_name}._sc_stock s on s.id=bis.stock_id " +
//            "JOIN ${schema_name}._sc_status st ON st.id = bis.status " +
//            "join ${schema_name}._sc_user u on bis.create_user_id=u.id " +
//            "JOIN ${schema_name}._sc_bom as b ON bisd.bom_id = b.id " +
//            "JOIN ${schema_name}._sc_bom_stock_detail bsd ON bsd.bom_code = bisd.bom_code AND bs.stock_id = bsd.stock_id AND bs.bom_id = bsd.bom_id " +
//            "WHERE bs.stock_id = #{id}::int ${condition} " +
//            ") t ORDER BY t.create_time desc,t.bom_id desc ${pagination}")
//    List<Map<String, Object>> findStockStockInAndOutList(@Param("schema_name")String schemaName,@Param("id") String id,@Param("userLang") String user_lang,@Param("company_id") Long company_id,@Param("condition") String condition,@Param("pagination") String pagination);
    @Select("select tab.* from " +
            "(select DISTINCT bw.work_code as code,bwdi.bom_id,bwdi.quantity,bwdi.material_code, " +
            "bwdi.real_quantity,bw.status,wt.type_name as process_method,wt.type_name, " +
            "bw.create_user_id,u.user_name,to_char(bw.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwdi.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_detail_item bwdi ON bwd.sub_work_code = bwdi.sub_work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON (s.ID = bw.stock_id or s.ID = bw.to_stock_id) " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bw.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwdi.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where s.id = #{id}::int " +
            "and bw.status = " + StatusConstant.COMPLETED + " and bw.to_stock_id is null ${condition} " +
            "UNION ALL" +
            " select DISTINCT bw.work_code as code,bwar.bom_id,bwar.quantity,bwar.material_code, " +
            "bwar.real_quantity,bwar.status,bwar.type_name as process_method,bwar.type_name, " +
            "bwar.create_user_id,u.user_name,to_char(bwar.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time, " +
            "s.id as stock_id,s.stock_name,bwar.store_position,sta.status as status_name " +
            "from ${schema_name}._sc_bom_works bw " +
            "JOIN ${schema_name}._sc_bom_works_detail bwd on bw.work_code = bwd.work_code " +
            "LEFT JOIN ${schema_name}._sc_bom_works_allot_record bwar ON bw.work_code = bwar.work_code " +
            "JOIN ${schema_name}._sc_stock AS s ON s.ID = bwar.stock_id " +
            "join ${schema_name}._sc_status AS sta ON sta.ID = bw.status " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON u.ID = bwar.create_user_id " +
            "JOIN ${schema_name}._sc_bom AS b ON bwar.bom_id = b.ID " +
            "JOIN ${schema_name}._sc_work_type wt on bw.work_type_id = wt.id " +
            "where s.id = #{id}::int " +
            "and bw.to_stock_id is not null ${condition}) tab " +
            "ORDER BY tab.create_time desc,tab.bom_id desc ${pagination} ")
    List<Map<String, Object>> findStockStockInAndOutList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("condition") String condition, @Param("pagination") String pagination);
}
