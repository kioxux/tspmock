package com.gengyun.senscloud.mapper;

/**
 * 备件报废
 */
public interface MonitorAreaMapper {
//
//    /**
//     * 查找区域配置列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT ma.id ,ma.monitor_area_code,ma.monitor_area_name, " +
//            "CASE " +
//            "WHEN P .position_name ISNULL THEN" +
//            "(select title from ${schema_name}._sc_facilities where id=cast( ma.asset_position_code as int)) " +
//            "ELSE " +
//            "P .position_name " +
//            "end AS position_name,ma.show_area,us.username as create_user_name, (SELECT COUNT (ID) FROM ${schema_name}._sc_monitor_area_component WHERE monitor_area_code = ma.monitor_area_code ) AS component_num " +
//            "FROM ${schema_name}._sc_monitor_area ma " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p ON p.position_code = ma.asset_position_code  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position_organization rp ON p.position_code = rp.position_code  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = rp.org_id  " +
//            "LEFT join ${schema_name}._sc_user us on ma.create_user_account=us.account " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY ma.id,ma.monitor_area_code,ma.monitor_area_name,p.position_name,ma.asset_position_code,ma.show_area,us.username,ma.createtime  " +
//            "ORDER BY ma.createtime desc  limit ${page} offset ${begin}")
//    List<Map<String, Object>> getMonitorAreaList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查找区域配置列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM( " +
//            "SELECT  ma.id,ma.monitor_area_code,ma.monitor_area_name, " +
//            "CASE " +
//            "WHEN P .position_name ISNULL THEN" +
//            "(select title from ${schema_name}._sc_facilities where id=cast( ma.asset_position_code as int)) " +
//            "ELSE " +
//            "P .position_name " +
//            "end AS position_name,ma.show_area,us.username as create_user_name, (SELECT COUNT (ID) FROM ${schema_name}._sc_monitor_area_component WHERE monitor_area_code = ma.monitor_area_code ) AS component_num " +
//            "FROM ${schema_name}._sc_monitor_area ma " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p ON p.position_code = ma.asset_position_code  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position_organization rp ON p.position_code = rp.position_code  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = rp.org_id  " +
//            "LEFT join ${schema_name}._sc_user us on ma.create_user_account=us.account " +
//            "WHERE 1=1 ${condition} " +
//            "GROUP BY ma.id,ma.monitor_area_code,ma.monitor_area_name,p.position_name,ma.asset_position_code,ma.show_area,us.username,ma.createtime )s")
//    int countMonitorAreaList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     * 新建区域配置
//     *
//     * @param
//     * @param
//     * @return
//     */
//    @Insert("INSERT INTO  ${schema_name}._sc_monitor_area  " +
//            "(monitor_area_code,monitor_area_name,asset_position_code,show_area,create_user_account,createtime) " +
//            "VALUES " +
//            "(#{monitor_area_code},#{monitor_area_name},#{asset_position_code},#{show_area}::int,#{create_user_account},current_timestamp)")
//    int addMonitorArea(Map<String, Object> monitorArea);
//
//
//    /**
//     * 修改区域配置
//     *
//     * @param
//     * @param
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_monitor_area " +
//            "SET " +
//            "monitor_area_name = #{monitor_area_name}, " +
//            "asset_position_code = #{asset_position_code}, " +
//            "show_area = #{show_area}::int " +
//            "WHERE  id= #{id}::int")
//    int updateMonitorArea(Map<String, Object> monitorArea);
//
//    /**
//     * 根据主键查删除区域配置数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("DELETE FROM ${schema_name}._sc_monitor_area  where id = #{id} ")
//    int deleteMonitorAreaById(@Param("schema_name") String schemaName, @Param("id") int id);
//
//    /**
//     * 根据主键查查询区域配置数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_monitor_area  where id = #{id} ")
//    Map<String, Object> getmonitorAreaById (@Param("schema_name") String schemaName, @Param("id") int id);
//
//    /**
//     * 根据主键查查询区域配置数据
//     *
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("SELECT ma.id,ma.monitor_area_code,ma.monitor_area_name,(SELECT COUNT (ID) FROM ${schema_name}._sc_monitor_area_component WHERE monitor_area_code = ma.monitor_area_code ) AS num FROM ${schema_name}._sc_monitor_area ma  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p ON p.position_code = ma.asset_position_code  " +
//            "LEFT JOIN ${schema_name}._sc_asset_position_organization rp ON p.position_code = rp.position_code  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = rp.org_id  " +
//            "where ${condition} "+
//            "GROUP BY ma.id,ma.monitor_area_code,ma.monitor_area_name " )
//    List<Map<String, Object>> getmonitorAreaListByPosition (@Param("schema_name") String schemaName, @Param("condition") String condition);
//

}
