package com.gengyun.senscloud.mapper;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 上午 9:31 2019/11/30 0030
 */
public interface AssetUseMapper {

//
//    @Select("SELECT ARRAY_TO_STRING(ARRAY_AGG(DISTINCT f.short_title), ',') as facility_name, " +
//            "asset.strname,asset.strcode,asset.enable_time,s.status as status_name,am.current_year_use," +
//            "ama.all_use,acs.source_type as last_source_type,acs.create_time as last_renew_time " +
//            "FROM ${schema_name}._sc_asset as asset  " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id  " +
//            "LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id  AND f.org_type in ("+ SqlConstant.FACILITY_INSIDE +") " +
//            "LEFT JOIN ${schema_name}._sc_asset_status as s on s.id = asset.intstatus " +
//            "LEFT JOIN (" +
//            "SELECT asset_code,SUM(to_number(monitor_value,'99999999.99')) as current_year_use from ${schema_name}._sc_asset_monitor_current  " +
//            "WHERE monitor_name='Month_Dosing_Accumulate_Total' and monitor_value_type='20' AND substring (gather_time from 1 for 4)=to_char(current_date,'" + SqlConstant.SQL_DATE_FMT_YEAR + "') " +
//            "GROUP BY asset_code " +
//            ")am ON asset.strcode=am.asset_code " +
//            "LEFT JOIN (" +
//            "SELECT asset_code,SUM(to_number(monitor_value,'99999999.99')) as all_use from ${schema_name}._sc_asset_monitor_current  " +
//            "WHERE monitor_name='Month_Dosing_Accumulate_Total' and monitor_value_type='20' " +
//            "GROUP BY asset_code " +
//            ")ama ON asset.strcode=ama.asset_code " +
//            "LEFT JOIN (" +
//            "SELECT mc.asset_code,mc.source_type,mc.create_time from ${schema_name}._sc_asset_monitor_current mc  " +
//            "INNER JOIN (" +
//            "SELECT asset_code,MAX(create_time)as create_time from ${schema_name}._sc_asset_monitor_current WHERE monitor_name='Month_Dosing_Accumulate_Total' GROUP BY asset_code,monitor_name)m ON mc.asset_code=m.asset_code " +
//            "AND m.create_time=mc.create_time " +
//            "WHERE  monitor_value_type='20' " +
//            ") acs ON asset.strcode=acs.asset_code " +
//            "WHERE asset.intstatus>0 " +
//            "${condition} " +
//            "GROUP BY asset.strname,asset.strcode,asset.enable_time,s.status,am.current_year_use,ama.all_use,acs.source_type,acs.create_time " +
//            " limit ${page} offset ${begin}")
//    List<Map<String, Object>> findUseList(@Param("schema_name") String schema_name,@Param("condition") String condition,@Param("page") int pageSize,@Param("begin") int begin);
//
//    @Select("select count(1) from (" +
//            "SELECT asset.strcode " +
//            "FROM ${schema_name}._sc_asset as asset  " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization as ao ON asset._id = ao.asset_id  " +
//            "LEFT JOIN ${schema_name}._sc_facilities as f ON ao.org_id = f.id  AND f.org_type in ("+ SqlConstant.FACILITY_INSIDE +") " +
//            "LEFT JOIN ${schema_name}._sc_asset_status as s on s.id = asset.intstatus " +
//            "LEFT JOIN (" +
//            "SELECT asset_code,SUM(to_number(monitor_value,'99999999.99')) as current_year_use from ${schema_name}._sc_asset_monitor_current  " +
//            "WHERE monitor_name='Month_Dosing_Accumulate_Total' and monitor_value_type='20' AND substring (gather_time from 1 for 4)=to_char(current_date,'" + SqlConstant.SQL_DATE_FMT_YEAR + "') " +
//            "GROUP BY asset_code " +
//            ")am ON asset.strcode=am.asset_code " +
//            "LEFT JOIN (" +
//            "SELECT asset_code,SUM(to_number(monitor_value,'99999999.99')) as all_use from ${schema_name}._sc_asset_monitor_current  " +
//            "WHERE monitor_name='Month_Dosing_Accumulate_Total' and monitor_value_type='20' " +
//            "GROUP BY asset_code " +
//            ")ama ON asset.strcode=ama.asset_code " +
//            "LEFT JOIN (" +
//            "SELECT mc.asset_code,mc.source_type,mc.create_time from ${schema_name}._sc_asset_monitor_current mc  " +
//            "INNER JOIN (" +
//            "SELECT asset_code,MAX(create_time)as create_time from ${schema_name}._sc_asset_monitor_current WHERE monitor_name='Month_Dosing_Accumulate_Total' GROUP BY asset_code,monitor_name)m ON mc.asset_code=m.asset_code " +
//            "AND m.create_time=mc.create_time " +
//            "WHERE  monitor_value_type='20' " +
//            ")acs ON asset.strcode=acs.asset_code " +
//            "WHERE asset.intstatus>0 " +
//            "${condition} " +
//            "GROUP BY asset.strname,asset.strcode,s.status,am.current_year_use,ama.all_use,acs.source_type,acs.create_time " +
//            ") s")
//    int countUseList(@Param("schema_name")String schema_name,@Param("condition")String condition);

}
