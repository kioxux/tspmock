package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 功能：工单列表
 * Created by Dong wudang on 2018/11/2.
 */
@Mapper
public interface WorkListMapper {
//
//    /*demo例子*/
//    @Insert("insert into ${schema_name}._sc_works " +
//            "(work_code,work_type_id,work_template_code,pool_id,facility_id,relation_type,relation_id,occur_time," +
//            "deadline_time,status,remark,create_time,create_user_account,title) values " +
//            "(#{t.work_code},#{t.work_type_id},#{t.work_template_code},#{t.pool_id},#{t.facility_id}," +
//            "#{t.relation_type},#{t.relation_id},#{t.occur_time},#{t.deadline_time},#{t.status},#{t.remark},#{t.create_time}," +
//            "#{t.create_user_account},#{t.works_title})")
//    public int addWorkList(@Param("schema_name") String schema_name, @Param("t") WorkListModel workListModel);
////插入工单详情表
//    @Insert("insert into ${schema_name}._sc_works_detail " +
//            "(sub_work_code,work_code,work_type_id,work_template_code,relation_type,relation_id,title,problem_note,problem_img," +
//            "asset_running_status,fault_type,repair_type,priority_level,status,remark,body_property,from_code,waiting,bom_app_result," +
//            "receive_account,distribute_time,begin_time,is_main) values " +
//            "(#{de.sub_work_code},#{de.work_code},#{de.work_type_id},#{de.work_template_code},#{de.relation_type},#{de.relation_id},#{de.title},#{de.problem_note},#{de.problem_img}," +
//            "#{de.asset_running_status},#{de.fault_type},#{de.repair_type},#{de.priority_level}," +
//            "#{de.status},#{de.remark},#{de.body_property,typeHandler=com.gengyun.senscloud.typehandler.JsonTypeHandler},#{de.from_code},#{de.waiting},#{de.bom_app_result},#{de.receive_account}," +
//            "#{de.distribute_time},#{de.begin_time},#{de.is_main})")
//    public int addWorkDetailList(@Param("schema_name") String schema_name, @Param("de") WorkListDetailModel workListModel);
//
//    /*
//    * 查询工单详情根据单据状态
//    * */
//    @Select("Select d.* ,t.flow_template_code as flow_code,w.facility_id as facility_id, w.occur_time as occur_time, w.deadline_time as deadline_time,w.title as works_title, u.mobile as phone,t.business_type_id as business_type_id ,t.type_name as work_type_name,u.username as account_name from ${schema_name}._sc_works_detail as d " +
//            "left join ${schema_name}._sc_works as w on d.work_code = w.work_code " +
//            "left join ${schema_name}._sc_user as u on u.account = w.create_user_account " +
//            "left join ${schema_name}._sc_work_type t on t.id = d.work_type_id " +
//            "where d.status=20 and d.work_code = #{work_code}")
//    public List<WorkListDetailModel> selectWorkListsDetailUndistributed(@Param("schema_name") String schema_name,@Param("work_code") String work_code);//, @Param("descStatus") String descStatus
//    @Select("Select w.* from ${schema_name}._sc_works as w " +
//            "where w.status=20 ${condition} limit ${page} offset ${begin}")
//    public List<WorkListModel> selectWorkListsUndistributed(@Param("schema_name") String schema_name, @Param("condition") String condition,@Param("page") int pageSize, @Param("begin") int begin);//, @Param("descStatus") String descStatus
//
//    @Select("Select count(w.work_code) from ${schema_name}._sc_works as w  " +
//            "where w.status=20 ${condition}")
//    public int selectWorkListsDetailUndistributedNum(@Param("schema_name") String schema_name, @Param("condition") String condition);//, @Param("descStatus") String descStatus
//
//    //查询状态
//    @Select("Select count(1) from ${schema_name}._sc_works_detail as wd  where wd.status=40 and  wd.work_code= #{sub_work_code}")
//    public int selectWorkListsDetailStatus(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
//
//    /*
//   * 根据工单ID 查询工单详情信息
//   * */
//    @Select("Select d.* ,w.facility_id as facility_id, w.occur_time as occur_time, w.deadline_time as deadline_time,w.title as works_title,u.mobile as phone, w.create_user_account as account_name from ${schema_name}._sc_works_detail as d " +
//            "left join ${schema_name}._sc_works as w on d.work_code = w.work_code " +
//            "left join ${schema_name}._sc_user as u on u.account = w.create_user_account " +
//            "where ${condition}")
//    public List<WorkListDetailModel> selectWorkListsDetailByWorkCode(@Param("schema_name") String schema_name, @Param("condition") String condition);//, @Param("descStatus") String descStatus
//    @Select("Select w.* ,w.facility_id as facility_id, w.occur_time as occur_time, w.deadline_time as deadline_time,w.title as works_title,u.mobile as phone, u.username  as account_name,t.type_name as type_name from ${schema_name}._sc_works as w " +
//            "left join ${schema_name}._sc_user as u on u.account = w.create_user_account " +
//            "left join ${schema_name}._sc_work_type as t on t.id=w.work_type_id " +
//            "where ${condition}")
//    public List<WorkListModel> selectWorkListsByWorkCode(@Param("schema_name") String schema_name, @Param("condition") String condition);//, @Param("descStatus") String descStatus
//
//    @Select("Select w.sub_work_code from ${schema_name}._sc_works_detail as w where work_code = #{work_code}")
//    public List<WorkListDetailModel> selectDetailByWorkCode(@Param("schema_name") String schema_name, @Param("work_code") String work_code);//, @Param("descStatus") String descStatus
//
//
//    /*更新工单详情状态*/
//    @Update("Update ${schema_name}._sc_works_detail set status = #{status}  where work_code = #{sub_work_code}")
//    public int updateWorkListStatus(@Param("schema_name") String schema_name, @Param("status") Integer status,@Param("sub_work_code")  String updateWorkListStatus);
//
//    @Update("Update ${schema_name}._sc_works_detail set status = #{status}  where work_code = #{sub_work_code}")
//    public int updateBackWorkListStatus(@Param("schema_name") String schema_name, @Param("status") Integer status,@Param("sub_work_code")  String updateWorkListStatus);
//
//    /*更新工单概要表状态*/
//    @Update("Update ${schema_name}._sc_works set status = #{status}  where work_code = #{work_code}")
//    public int updateWorksStatus(@Param("schema_name") String schema_name, @Param("status") Integer status,@Param("work_code")  String updateWorkStatus);
//
//    /*删除工单详情表*/
//    @Delete("DELETE from ${schema_name}._sc_works_detail where sub_work_code = #{sub_work_code}")
//    public int delWorkListStatus(@Param("schema_name") String schema_name,@Param("sub_work_code")  String sub_work_code);
//
//    /*删除工单概要表*/
//    @Delete("DELETE  from ${schema_name}._sc_works where work_code = #{work_code}")
//    public int delWorkStatus(@Param("schema_name") String schema_name,@Param("work_code")  String workCode);
//
//    /*更新工单详情状态时间*/
//    @Update("Update ${schema_name}._sc_works_detail set status = #{status} , receive_time = #{r.receive_time}   where work_code = #{sub_work_code}")
//    public int updateWorkListStatusAndTime(@Param("schema_name") String schema_name, @Param("status") Integer status, @Param("sub_work_code")  String updateWorkListStatus,@Param("r") WorkListDetailModel workListDetailModel);
//
//    /*更新模板工单状态*/
//    @Update("Update ${schema_name}._sc_works set status = #{status}     where work_code = #{work_code}")
//    public int updateWorkStatusAndTime(@Param("schema_name") String schema_name, @Param("status") Integer status, @Param("work_code")  String work_code);
//
//    /*工单池任务分配*/
//    @Update("Update ${schema_name}._sc_works_detail set receive_account = #{receive_account}  where work_code = #{sub_work_code}")
//    public int linkRepairManToWorkOrder(@Param("schema_name") String schema_name, @Param("receive_account") String receive_account,@Param("sub_work_code")  String subWorkCode);
//
//
//
//    /*更改工单池*/
//    @Update("Update ${schema_name}._sc_works set pool_id = #{pool_id}  where work_code = #{work_code}")
//    public int updateWorkPool(@Param("schema_name") String schema_name, @Param("work_code") String work_code, @Param("pool_id") Integer pool_id );
}
