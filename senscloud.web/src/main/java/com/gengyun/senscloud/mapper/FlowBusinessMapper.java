package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FlowBusinessMapper {
//
//    /**
//     * 存在数据则更新 流程业务同步表,不存在则新增一条记录
//     *
//     * @param schema_name
//     * @param subWorkCode
//     * @param taskId
//     * @param formKey
//     * @param status
//     * @param dutyAccount
//     * @return
//     */
//    @Update("INSERT INTO ${schema_name}._sc_flow_current_node(sub_work_code,task_id,form_key,status,duty_account) " +
//            "VALUES ('${subWorkCode}','${taskId}','${formKey}',${status}::int,'${dutyAccount}') " +
//            "ON CONFLICT (sub_work_code) " +
//            "DO UPDATE SET form_key='${formKey}',status=${status}::int,duty_account='${dutyAccount}' ")
//    void insertOrUpdate(@Param("schema_name") String schema_name, @Param("subWorkCode") String subWorkCode, @Param("taskId") String taskId,
//                        @Param("formKey") String formKey, @Param("status") String status, @Param("dutyAccount") String dutyAccount);
//
//    /**
//     * 根据subWorkCodes删除工单的相关数据(不会再有后续操作的数据)
//     *
//     * @param schema_name
//     * @param subWorkCodes
//     * @return
//     */
//    @Delete("DELETE FROM ${schema_name}._sc_flow_current_node " +
//            "WHERE sub_work_code IN(${subWorkCodes})")
//    int deleteFlowBusinessDataByCodes(@Param("schema_name") String schema_name, @Param("subWorkCodes") String subWorkCodes);
//
//    /**
//     * 查询需要删除数据工单相关的工单号(不会再有后续操作的数据)
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT ARRAY_TO_STRING(ARRAY_AGG('&'||fc.sub_work_code||'&'),',') FROM ${schema_name}._sc_flow_current_node fc " +
//            "LEFT JOIN ${schema_name}._sc_works_detail wd ON fc.sub_work_code=wd.sub_work_code " +
//            "WHERE wd.status>=" + StatusConstant.COMPLETED)
//    String queryInvalidSubWorkCodes(@Param("schema_name") String schema_name);
//
//    /**
//     * 根据subWorkCodes查询
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_flow_current_node  " +
//            "WHERE sub_work_code IN (${subWorkCodes})")
//    List<Map<String, Object>> queryFlowBusinessDataByCodes(@Param("schema_name") String schema_name, @Param("subWorkCodes") String subWorkCodes);
//
//
//    /**
//     * 按subWorkCodes查询key:subWorkCode value:form_key数据
//     *
//     * @param schema_name
//     * @param subWorkCodes
//     * @param keySet
//     * @param valueSet
//     * @return
//     */
//    @Select("SELECT ${keySet},${valueSet} FROM ${schema_name}._sc_flow_current_node  " +
//            "WHERE sub_work_code IN (${subWorkCodes}) ")
//    List<Map<String, Object>> queryFlowBusinessMapByCodes(@Param("schema_name") String schema_name, @Param("subWorkCodes") String subWorkCodes
//            , @Param("keySet") String keySet, @Param("valueSet") String valueSet);
//
//    /**
//     * 根据formKey查询对应业务流程信息
//     *
//     * @param schema_name
//     * @param formKey
//     * @return
//     */
//    @Select("SELECT fc.*,wd.work_code FROM ${schema_name}._sc_flow_current_node fc  " +
//            "LEFT JOIN ${schema_name}._sc_works_detail wd ON fc.sub_work_code=wd.sub_work_code " +
//            "WHERE form_key='${formKey}' ORDER BY sub_work_code DESC")
//    List<Map<String, Object>> queryFlowBusinessDataByFromKey(@Param("schema_name") String schema_name, @Param("formKey") String formKey);

}