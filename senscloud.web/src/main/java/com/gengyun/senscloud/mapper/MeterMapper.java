package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Mapper
public interface MeterMapper {
    /**
     * 位置限定sql
     */
    String positionFilterSql = "(select distinct sr.asset_position_code from " +
            " ${schema_name}._sc_role_asset_position sr " +
            " join ${schema_name}._sc_role f on sr.role_id = f.id " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id " +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id} )";

    @Select("<script>" +
            " select count(1) from (select tb.facility_id,tb.inner_code,tb.title,coalesce(sum(tb.current_flow), 0) as current_flow_total, " +
            " case when bool_and(tb.meter_status='1') then '1' when bool_and(tb.meter_status='0') then '0' else '2' end as meter_status, " +
            " case when bool_and(tb.status='1') then '1' else '2' end as status,max(tb.create_time) as check_time from " +
            " (select m.*,cu.inner_code,coalesce(cu.title,cu.short_title) as title,mt.current_flow,mt.meter_status,mt.status,mt.create_time from ${schema_name}._sc_meter m " +
            " left join ${schema_name}._sc_meter_item mt on m.facility_id=mt.facility_id and m.check_item=mt.check_item " +
            " left join ${schema_name}._sc_customer cu on m.facility_id=cu.facility_id " +
            " where mt.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and mt.is_delete>-1000 " +
            " and cu.status> " + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " + positionFilterSql + ") tb " +
            " group by tb.facility_id,tb.inner_code,tb.title) s " +
            " <where> " +
            " <when test=\"pm.title !=null and pm.title !=''\"> " +
            " and s.title ilike concat('%',#{pm.title},'%') " +
            " </when> " +
            " <when test=\"pm.inner_code !=null and pm.inner_code !=''\"> " +
            " and s.inner_code ilike concat('%',#{pm.inner_code},'%') " +
            " </when> " +
            " <when test=\"pm.meter_status !=null and pm.meter_status !=''\"> " +
            " and s.meter_status=#{pm.meter_status} " +
            " </when> " +
            " <when test=\"pm.keywordSearch !=null and pm.keywordSearch !=''\"> " +
            " and (s.title ilike concat('%',#{pm.keywordSearch},'%') or " +
            " s.inner_code ilike concat('%',#{pm.keywordSearch},'%')) " +
            " </when> " +
            " <when test='pm.exceed!=null and pm.exceed==\"1\"'> " +
            " and s.status='2' " +
            " </when> " +
            " <when test='pm.unusual!=null and pm.unusual==\"1\"'> " +
            " and s.status !='1' and s.status !='2'" +
            " </when> " +
            " <when test='pm.normal!=null and pm.normal==\"1\"'> " +
            " and s.status='1'" +
            " </when> " +
            " </where> " +
            "</script>")
    int findMetersListCount(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select s.* from (select tb.facility_id,tb.inner_code,tb.title,cast(coalesce(sum(tb.current_flow), 0) as decimal(10,2)) as current_flow_total, " +
            " cast((select final_flow from ${schema_name}._sc_pollute_fee where facility_id=tb.facility_id " +
            " and belong_month &lt;= to_char(max(tb.create_time)::timestamp,'yyyy-mm') order by belong_month desc limit 1) as decimal(10,2)) as last_flow_total, " +
            " cast((select coalesce(sum(current_flow), 0) from ${schema_name}._sc_meter_item where facility_id=tb.facility_id and is_delete>-1000 and create_time between " +
            " (select end_date from ${schema_name}._sc_pollute_fee where facility_id=tb.facility_id " +
            " and belong_month &lt;= to_char(max(tb.create_time)::timestamp,'yyyy-mm') order by belong_month desc limit 1)::timestamp " +
            " and #{pm.begin_date}::timestamp) as decimal(10,2)) as this_flow_total, " +
            " case when bool_and(tb.meter_status='1') then '1' when bool_and(tb.meter_status='0') then '0' else '2' end as meter_status, " +
            " case when bool_and(tb.status='1') then '1' else '2' end as status,max(tb.create_time) as check_time from " +
            " (select m.*,cu.inner_code,coalesce(cu.title,cu.short_title) as title,mt.current_flow,mt.meter_status,mt.status,mt.create_time " +
            " from ${schema_name}._sc_meter m " +
            " left join ${schema_name}._sc_meter_item mt on m.facility_id=mt.facility_id and m.check_item=mt.check_item " +
            " left join ${schema_name}._sc_customer cu on m.facility_id=cu.facility_id " +
            " where mt.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and mt.is_delete>-1000 " +
            " and cu.status> " + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " + positionFilterSql + ") tb " +
            " group by tb.facility_id,tb.inner_code,tb.title) s " +
            " <where> " +
            " <when test=\"pm.title !=null and pm.title !=''\"> " +
            " and s.title ilike concat('%',#{pm.title},'%') " +
            " </when> " +
            " <when test=\"pm.inner_code !=null and pm.inner_code !=''\"> " +
            " and s.inner_code ilike concat('%',#{pm.inner_code},'%') " +
            " </when> " +
            " <when test=\"pm.meter_status !=null and pm.meter_status !=''\"> " +
            " and s.meter_status=#{pm.meter_status} " +
            " </when> " +
            " <when test=\"pm.keywordSearch !=null and pm.keywordSearch !=''\"> " +
            " and (s.title ilike concat('%',#{pm.keywordSearch},'%') or " +
            " s.inner_code ilike concat('%',#{pm.keywordSearch},'%')) " +
            " </when> " +
            " <when test='pm.exceed!=null and pm.exceed==\"1\"'> " +
            " and s.status='2' " +
            " </when> " +
            " <when test='pm.unusual!=null and pm.unusual==\"1\"'> " +
            " and s.status !='1' and s.status !='2'" +
            " </when> " +
            " <when test='pm.normal!=null and pm.normal==\"1\"'> " +
            " and s.status='1'" +
            " </when> " +
            " </where> " +
            " ${pm.pagination} " +
            "</script>")
    List<Map<String, Object>> findMetersList(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select distinct ac.asset_id as value,ac.asset_name as text from ${schema_name}._sc_meter m " +
            " join ${schema_name}._sc_asset_cus ac on m.facility_id=ac.cus_id and m.asset_id=ac.asset_id " +
            " where m.facility_id=#{pm.facility_id} ::int " +
            "</script>")
    List<Map<String, Object>> findMeterAssertList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select id,factor_id,facility_id,asset_id,check_item,check_item_name,cast(indication as decimal(10,2)) as indication, " +
            " coalesce(limit_value ::varchar, '/') as limit_value, " +
            " coalesce(cast(current_flow as decimal(10,2)) ::varchar, '/') as current_flow, " +
            " cast(last_indication as decimal(10,2)) as last_indication, " +
            " unit,status,meter_status,meter_type," +
            " (select count(1) from ${schema_name}._sc_meter_record mr where mr.factor_id=mt.factor_id and mr.facility_id=mt.facility_id " +
            " and mr.asset_id=mt.asset_id and mr.check_item=mt.check_item and mr.status>-1000 " +
            " and mr.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp) as check_count,update_time " +
            " from ${schema_name}._sc_meter_item mt where facility_id=#{pm.facility_id} ::int and asset_id=#{pm.asset_id} and is_delete>-1000 " +
            " and create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp order by check_item desc " +
            "</script>")
    List<Map<String, Object>> findMeterItemList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select id,factor_id,facility_id,asset_id,check_item,check_item_name, " +
            " cast(indication as decimal(10,2)) as indication, " +
            " coalesce(limit_value ::varchar, '/') as limit_value,unit,item_status as status,meter_status,meter_type,update_time " +
            " from ${schema_name}._sc_meter_record where facility_id=#{pm.facility_id} ::int and asset_id=#{pm.asset_id} and status>-1000 " +
            " and create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp order by check_item desc " +
            "</script>")
    List<Map<String, Object>> findMeterRecordList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select tab.cus_id as facility_id,tab.title,tab.asset_id,tab.asset_name,properties ::jsonb ->> 'GRM' as grm, " +
            " properties ::jsonb ->> 'PASS' as pass from (select tb.cus_id,tb.title,tb.asset_id,tb.asset_name, " +
            " json_object_agg (tb.code,tb.value) ::text as properties from (select ac.cus_id,cu.title,ac.asset_id,ac.asset_name, " +
            " jsonb_array_elements(ac.properties) ->> 'field_code' as code, " +
            " jsonb_array_elements(ac.properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000) tb " +
            " group by tb.cus_id,tb.title,tb.asset_id,tb.asset_name " +
            " union " +
            " select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,null as properties " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and (ac.properties='[]' or ac.properties is null)) tab" +
            "</script>")
    List<Map<String, Object>> findNeedMeterCusAssetList(@Param("schema_name") String schemaName);

    @Select("<script>" +
            " select tab.cus_id as facility_id,tab.title,null as sp_code,tab.asset_id,tab.asset_name,tab.sid_url,properties ::jsonb ->> 'GRM' as grm, " +
            " properties ::jsonb ->> 'PASS' as pass,null as monitor_site_code,'water' as factor_id,null as limit_value,'0' as check_item," +
            " '水量' as check_item_name,'m3' as unit,to_char((current_timestamp+'-1 day'),'yyyy-mm-dd')::timestamp as create_time from " +
            " (select tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sid_url, " +
            " json_object_agg (tb.code,tb.value) ::text as properties from (select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,ac.sid_url, " +
            " jsonb_array_elements(ac.properties) ->> 'field_code' as code, " +
            " jsonb_array_elements(ac.properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000) tb " +
            " group by tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sid_url " +
            " union " +
            " select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,null as sid_url,null as properties " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and (ac.properties='[]' or ac.properties is null)) tab " +
            " union " +
            " select tab.cus_id as facility_id,tab.title,tab.sp_code,tab.asset_id,tab.asset_name,null as sid_url,null as grm,null as pass, " +
            " properties ::jsonb ->> 'monitorSiteCode' as monitor_site_code," +
            " cuf.factor_id,cuf.limit_value,'1' as check_item,cd.name as check_item_name,cuf.limit_unit as unit, " +
            " to_char((current_timestamp+'-1 day'),'yyyy-mm-dd')::timestamp as create_time " +
            " from (select tb.cus_id,tb.title,tb.sp_code,tb.asset_id,tb.asset_name, " +
            " json_object_agg (tb.code,tb.value) ::text as properties from (select ac.cus_id,cu.title,cu.sp_code,ac.asset_id,ac.asset_name, " +
            " jsonb_array_elements(ac.properties) ->> 'field_code' as code, " +
            " jsonb_array_elements(ac.properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000) tb " +
            " group by tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sp_code " +
            " union " +
            " select ac.cus_id,cu.title,cu.sp_code,ac.asset_id,ac.asset_name,null as properties " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and (ac.properties='[]' or ac.properties is null)) tab " +
            " left join ${schema_name}._sc_customer_pollute_factor cuf on tab.cus_id=cuf.facility_id " +
            " left join ${schema_name}._sc_cloud_data cd on cuf.factor_id=cd.code " +
            " where cuf.status>-1000 and cd.data_type='pollute_factor' " +
            "</script>")
    List<Map<String, Object>> findNeedMeterItemList(@Param("schema_name") String schemaName);

    @Select("<script>" +
            " select last_indication from ${schema_name}._sc_meter_item " +
            " where facility_id=#{pm.facility_id} and asset_id=#{pm.asset_id} and factor_id=#{pm.factor_id} and is_delete>-1000 " +
            " and to_char(create_time,'yyyy-mm-dd')=to_char((#{pm.create_time}::timestamp+'-1 day'),'yyyy-mm-dd')  " +
            "</script>")
    String getLastIndication(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Insert("<script>" +
            " <foreach collection='meterList' item='meter' index='index' separator=';'> " +
            " insert into ${schema_name}._sc_meter (facility_id,asset_id,check_item) values " +
            " (#{meter.facility_id},#{meter.asset_id},#{meter.check_item}) " +
            " on conflict (facility_id,asset_id,check_item) " +
            " do update set facility_id=#{meter.facility_id},asset_id=#{meter.asset_id},check_item=#{meter.check_item} " +
            " </foreach> " +
            "</script>")
    void insertOrUpdateMeter(@Param("schema_name") String schemaName, @Param("meterList") List<Map<String, Object>> meterList);

    @Insert("<script>" +
            " <foreach collection='meterItemList' item='pm' index='index' separator=';'> " +
            " insert into ${schema_name}._sc_meter_item (factor_id,facility_id,asset_id,check_item,check_item_name," +
            " indication,limit_value,current_flow,last_indication,unit,status,meter_status,meter_type,create_time) values " +
            " (#{pm.factor_id},#{pm.facility_id},#{pm.asset_id},#{pm.check_item},#{pm.check_item_name},#{pm.indication}, " +
            " #{pm.limit_value},#{pm.current_flow},#{pm.last_indication},#{pm.unit},#{pm.status},#{pm.meter_status}, " +
            " #{pm.meter_type},#{pm.create_time}) " +
            " on conflict (factor_id,facility_id,asset_id,create_time) " +
            " do update set factor_id=#{pm.factor_id},facility_id=#{pm.facility_id},asset_id=#{pm.asset_id}, " +
            " check_item=#{pm.check_item},check_item_name=#{pm.check_item_name},indication=#{pm.indication}, " +
            " limit_value=#{pm.limit_value},current_flow=#{pm.current_flow},last_indication=#{pm.last_indication}, " +
            " unit=#{pm.unit},status=#{pm.status},meter_status=#{pm.meter_status},meter_type=#{pm.meter_type}, " +
            " create_time=#{pm.create_time} " +
            " </foreach> " +
            "</script>")
    void insertOrUpdateMeterItem(@Param("schema_name") String schemaName, @Param("meterItemList") List<Map<String, Object>> meteritemList);

    @Select("<script>" +
            " select * from ${schema_name}._sc_meter_item where asset_id=#{pm.asset_id} and check_item='0' and is_delete>-1000 " +
            " and meter_type !='3' and to_char(create_time, 'yyyy-mm-dd')&lt;to_char(#{pm.begin_date}::timestamp, 'yyyy-mm-dd') " +
            " order by create_time desc limit 1 " +
            "</script>")
    Map<String, Object> findRecentAssetMeterItem(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select * from ${schema_name}._sc_meter_item where asset_id=#{pm.asset_id} and check_item='0' and is_delete>-1000 " +
            " and meter_type !='3' and to_char(create_time, 'yyyy-mm-dd')>to_char(#{pm.begin_date}::timestamp, 'yyyy-mm-dd') " +
            " order by create_time asc limit 1 " +
            "</script>")
    Map<String, Object> findAfterAssetMeterItem(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_meter_item " +
            " set indication=#{pm.indication}::numeric, " +
            " meter_status='1', " +
            " update_time=current_timestamp " +
            " where id=#{pm.id}::int and is_delete>-1000 " +
            "</script>")
    void modifyMeterItemIndecation(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select tb.id,tb.asset_id,tb.check_item,tb.indication,tb.last_indication, " +
            " case when tb.check_item='0' then coalesce((tb.indication-tb.last_indication),0) else null end as current_flow " +
            " from (select mt.id,mt.asset_id,mt.check_item,mt.indication, " +
            " coalesce((select pre.indication from ${schema_name}._sc_meter_item pre " +
            " where pre.asset_id=mt.asset_id and pre.check_item=mt.check_item and pre.meter_type != '3' and pre.is_delete>-1000 " +
            " and pre.factor_id=mt.factor_id and to_char(pre.create_time,'yyyy-mm-dd')&lt;to_char(mt.create_time,'yyyy-mm-dd') " +
            " order by pre.create_time desc limit 1),0) as last_indication from ${schema_name}._sc_meter_item mt " +
            " where mt.asset_id=#{pm.asset_id} and mt.meter_type !='3' and mt.is_delete>-1000 " +
            " and to_char(mt.create_time,'yyyy-mm-dd')>=to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd') order by mt.create_time asc) tb " +
            "</script>")
    List<Map<String, Object>> findAfterMeterItemList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " <foreach collection='meter_item_list' item='pm' index='index' separator=';'> " +
            " update ${schema_name}._sc_meter_item " +
            " set last_indication = #{pm.last_indication}::numeric, " +
            " current_flow = #{pm.current_flow}::numeric " +
            " where id = #{pm.id}::int and is_delete>-1000 " +
            " </foreach> " +
            "</script>")
    void updateMeterItem(@Param("schema_name") String schemaName, @Param("meter_item_list") List<Map<String, Object>> afterMeterItem);

    @Insert("<script>" +
            " insert into ${schema_name}._sc_meter_record (factor_id,facility_id,asset_id,check_item,check_item_name,indication, " +
            " limit_value,unit,item_status,meter_status,meter_type,update_time,create_time) values (#{pm.factor_id},#{pm.facility_id}::int, " +
            " #{pm.asset_id},#{pm.check_item},#{pm.check_item_name},#{pm.indication}::numeric,#{pm.limit_value}::numeric, " +
            " #{pm.unit},#{pm.status},#{pm.meter_status},#{pm.meter_type},current_timestamp,#{pm.begin_date}::timestamp) " +
            "</script>")
    void insertMeterRecord(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_meter_record set status = -1000 " +
            " where id = #{pm.id}::int " +
            "</script>")
    void deleteMeterRecord(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " with s as (select tb.facility_id,tb.inner_code,tb.title,coalesce(sum(tb.current_flow), 0) as current_flow_total, " +
            " case when bool_and(tb.meter_status='1') then '1' when bool_and(tb.meter_status='0') then '0' else '2' end as meter_status, " +
            " case when bool_and(tb.status='1') then '1' else '2' end as status,max(tb.create_time) as check_time from " +
            " (select m.*,cu.inner_code,coalesce(cu.title,cu.short_title) as title,mt.current_flow,mt.meter_status,mt.status,mt.create_time " +
            " from ${schema_name}._sc_meter m " +
            " left join ${schema_name}._sc_meter_item mt on m.facility_id=mt.facility_id and m.check_item=mt.check_item " +
            " left join ${schema_name}._sc_customer cu on m.facility_id=cu.facility_id " +
            " where mt.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and mt.is_delete>-1000 " +
            " and cu.status> " + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " + positionFilterSql + ") tb " +
            " group by tb.facility_id,tb.inner_code,tb.title) " +
            " \n" +
            " select coalesce(a.day_total,0) as normal_total,coalesce(b.day_total,0) as exceed_total, " +
            " coalesce(c.day_total,0) as abnormal_total,a.check_time as days from " +
            " (select count(distinct s.facility_id) as day_total,to_char(s.check_time,'yyyy-mm-dd') as check_time from s " +
            " where s.status='1' group by to_char(s.check_time,'yyyy-mm-dd')) a " +
            " left join (select count(distinct s.facility_id) as day_total,to_char(s.check_time,'yyyy-mm-dd') as check_time from s " +
            " where s.status='2' group by to_char(s.check_time,'yyyy-mm-dd')) b on a.check_time=b.check_time " +
            " left join (select count(distinct s.facility_id) as day_total,to_char(s.check_time,'yyyy-mm-dd') as check_time from s " +
            " where s.status !='1' and s.status !='2' group by to_char(s.check_time,'yyyy-mm-dd')) c on c.check_time=b.check_time " +
            "</script>")
    List<Map<String, Object>> findMeterCalendar(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " with s as (select mt.* from ${schema_name}._sc_meter_item mt " +
            " join ${schema_name}._sc_customer cu on mt.facility_id=cu.facility_id " +
            " where mt.is_delete>-1000 and cu.status>" + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " + positionFilterSql +
            " and mt.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp),cs as( " +
            " select tb.facility_id,tb.inner_code,tb.title, " +
            " case when bool_and(tb.meter_status='1') then '1' when bool_and(tb.meter_status='0') then '0' else '2' end as meter_status, " +
            " case when bool_and(tb.status='1') then '1' else '2' end as status,max(tb.create_time) as check_time from " +
            " (select m.*,cu.inner_code,coalesce(cu.title,cu.short_title) as title,mt.current_flow,mt.meter_status,mt.status,mt.create_time " +
            " from ${schema_name}._sc_meter m " +
            " left join ${schema_name}._sc_meter_item mt on m.facility_id=mt.facility_id and m.check_item=mt.check_item " +
            " left join ${schema_name}._sc_customer cu on m.facility_id=cu.facility_id " +
            " where mt.create_time between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp and mt.is_delete>-1000 " +
            " and cu.status> " + StatusConstant.STATUS_DELETEED + " and cu.is_use='1' " +
            " and cu.position_code in " + positionFilterSql + ") tb " +
            " group by tb.facility_id,tb.inner_code,tb.title) " +
            " \n" +
            " select (select count(distinct s.asset_id) from s where s.meter_status='1') as checked, " +
            " (select count(distinct s.asset_id) from s where s.meter_status='0') as unchecked, " +
            " (select count(distinct cs.facility_id) from cs where cs.status='2') as exceed," +
            " (select count(distinct cs.facility_id) from cs where cs.status='1') as normal_total," +
            " (select count(distinct cs.facility_id) from cs where cs.status !='1' and cs.status !='2') as abnormal_total" +
            "</script>")
    Map<String, Object> findMeterStatistics(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("pm") Map<String, Object> paramMap);

    @Select(" <script> " +
            " select tab.facility_id,1 as is_create_pollute " +
            " from (select * from ${schema_name}._sc_pollute_fee " +
            " where to_char(#{pm.begin_date} ::timestamp, 'yyyy-mm-dd') between to_char(begin_date,'yyyy-mm-dd') " +
            " and to_char(end_date,'yyyy-mm-dd') " +
            " union " +
            " select * from ${schema_name}._sc_pollute_fee " +
            " where to_char(#{pm.end_date} ::timestamp, 'yyyy-mm-dd') between to_char(begin_date,'yyyy-mm-dd') " +
            " and to_char(end_date,'yyyy-mm-dd')) tab group by tab.facility_id " +
            " </script> ")
    List<Map<String, Object>> findIsGeneratePollute(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    @Select("<script>" +
            " select * from ${schema_name}._sc_meter_item where id=#{pm.id}::int and is_delete>-1000 " +
            "</script>")
    Map<String, Object> findGuessMeterItem(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select * from ${schema_name}._sc_meter_record where id=#{pm.id}::int " +
            "</script>")
    Map<String, Object> findMeterRecord(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select coalesce(cast(final_flow/coalesce(date_part('day',end_date-begin_date),1) as decimal(10,2)),0) as indication " +
            " from ${schema_name}._sc_pollute_fee where facility_id=#{pm.facility_id}::int " +
            " and belong_month &lt;= to_char(#{pm.begin_date}::timestamp,'yyyy-mm') order by belong_month desc limit 1 " +
            "</script>")
    BigDecimal findGuessData(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_meter_item " +
            " set indication=#{pm.indication}::numeric, " +
            " meter_status='1', " +
            " meter_type='3', " +
            " update_time=current_timestamp " +
            " where id=#{pm.id}::int and is_delete>-1000 " +
            "</script>")
    void modifyGuessMeterItemIndecation(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select mt.* from ${schema_name}._sc_meter_item mt " +
            " left join ${schema_name}._sc_pollute_fee puf on mt.facility_id=puf.facility_id " +
            " where mt.facility_id=#{pm.facility_id}::int and mt.factor_id=#{pm.factor_id} " +
            " and mt.meter_status='0' and mt.is_delete>-1000 and " +
            " to_char(mt.create_time,'yyyy-mm-dd')>coalesce(to_char(puf.end_date,'yyyy-mm-dd'),'') " +
            "</script>")
    List<Map<String, Object>> findMeterItemByCusAndFac(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " <foreach collection='fac_list' item='pm' index='index' separator=';'>" +
            " update ${schema_name}._sc_meter_item set is_delete=-1000 " +
            " where id=#{pm.id}::int " +
            " </foreach>" +
            "</script>")
    void batchUpdateMeterItemFac(@Param("schema_name") String schemaName, @Param("fac_list") List<Map<String, Object>> cusAndfacList);

    /**
     * 查询客户历史排污量
     *
     * @param schemaName
     * @param pm
     * @return
     */
    @Select(" <script>" +
            " select cast(n.total as decimal(18,2)) as total," +
            " cast((select sum(b.current_flow) from ${schema_name}._sc_meter_item b " +
            " where b.is_delete>-1000 and b.facility_id= #{pm.id}::int " +
            " and to_char(b.create_time,'yyyy-mm') = n.lastmonth" +
            " group by to_char(b.create_time,'yyyy-mm') " +
            " ) as decimal(18,2)) last_month_total," +
            " cast((select sum(b.current_flow) from ${schema_name}._sc_meter_item b " +
            " where b.is_delete>-1000 and b.facility_id = #{pm.id}::int " +
            " and to_char(b.create_time,'yyyy-mm') = n.lastyearmonth " +
            " group by to_char(b.create_time,'yyyy-mm') " +
            " ) as decimal(18,2)) last_year_month_total," +
            " n.year_month " +
            " from (select sum(a.current_flow) total," +
            " to_char(a.create_time,'yyyy-mm') year_month ," +
            " max(to_char(date_trunc('month',a.create_time) - interval '0 month' - interval '1 day','yyyy-mm'))  lastmonth," +
            " max(to_char((SELECT a.create_time+ '-1 year'),'yyyy-mm')) lastyearmonth " +
            " from ${schema_name}._sc_meter_item a where a.is_delete>-1000 and a.facility_id=  #{pm.id}::int " +
            " and a.create_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " group by to_char(a.create_time,'yyyy-mm')) n " +
            " </script>")
    List<Map<String, Object>> findCustomerMeterHistory(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 客户排污计量柱状图年
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select cast(sum(a.current_flow) as decimal(18,2)) as flow,to_char(a.create_time,'yyyy-MM') days " +
            " from ${schema_name}._sc_meter_item a where a.create_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " and a.is_delete>-1000 and a.facility_id = #{pm.id}::int " +
            " group by to_char(a.create_time,'yyyy-MM') order by to_char(a.create_time,'yyyy-MM')" +
            " </script>")
    List<Map<String, Object>> findCustomerMeterPicYear(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 客户排污计量柱状图月
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select cast(sum(a.current_flow) as decimal(18,2)) as flow,to_char(a.create_time,'yyyy-MM-dd') days " +
            " from ${schema_name}._sc_meter_item a where a.create_time between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            " and a.is_delete>-1000 and a.facility_id = #{pm.id}::int " +
            " group by to_char(a.create_time,'yyyy-MM-dd') order by to_char(a.create_time,'yyyy-MM-dd')" +
            " </script>")
    List<Map<String, Object>> findCustomerMeterPicMonth(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 查询客户排污量
     *
     * @param schemaName
     * @param paramMap
     * @return
     */
    @Select(" <script> " +
            " select to_char(a.create_time,'yyyy-MM-dd') as check_time, " +
            " coalesce(cast(sum(a.current_flow) as decimal(10,2)),0) as flow, " +
            " case when bool_and(a.meter_status='1') then '1' when bool_and(a.meter_status='0') then '0' else '2' end as meter_status, " +
            " case when bool_and(a.status='1') then '1' else '2' end as status " +
            " from ${schema_name}._sc_meter_item a where " +
            " a.facility_id = #{pm.id}::int and a.check_item='0' and a.is_delete>-1000 " +
            " group by to_char(a.create_time,'yyyy-MM-dd') order by to_char(a.create_time,'yyyy-MM-dd') desc ${pm.pagination}" +
            " </script>")
    List<Map<String, Object>> findCustomerMeterList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select (select coalesce(sum(current_flow),0) from ${schema_name}._sc_meter_item " +
            " where facility_id=#{pm.facility_id}::int and to_char(create_time,'yyyy-mm-dd')::timestamp " +
            " between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp and is_delete>-1000 group by facility_id) as total," +
            " (select coalesce(sum(s.current_flow),0) from (select facility_id,sum(current_flow) as current_flow, " +
            " case when bool_and(status='1') then '1' else '2' end as status from ${schema_name}._sc_meter_item " +
            " where facility_id=#{pm.facility_id}::int and to_char(create_time,'yyyy-mm-dd')::timestamp " +
            " between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp and is_delete>-1000 " +
            " group by to_char(create_time,'yyyy-mm-dd'),facility_id) s where s.status!='1' group by s.facility_id) as exc, " +
            " (select count(distinct id) from ${schema_name}._sc_meter_record where facility_id=#{pm.facility_id}::int and status>-1000 " +
            " and to_char(create_time,'yyyy-mm-dd')::timestamp between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp) as repeat_count " +
            "</script>")
    Map<String, Object> findCusMeterInfo(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select cast(coalesce(sum((indication/limit_value)),0) as decimal(10,2)) as ratio " +
            " from ${schema_name}._sc_meter_item where facility_id=#{pm.facility_id}::int and check_item='1' and status='2' and is_delete>-1000 " +
            " and to_char(create_time,'yyyy-mm-dd')::timestamp between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            "</script>")
    BigDecimal findCusFacRatio(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select count(distinct id) from ${schema_name}._sc_meter_item " +
            " where facility_id=#{pm.facility_id}::int and check_item='1' and status='2' and is_delete>-1000 " +
            " and to_char(create_time,'yyyy-mm-dd')::timestamp between #{pm.begin_time}::timestamp and #{pm.end_time}::timestamp " +
            "</script>")
    BigDecimal findRatioCount(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select to_char(create_time,'yyyy-mm-dd') as date_time,factor_id,check_item,check_item_name, " +
            " cast(indication as decimal(10,2)) as survey_value,limit_value,unit, " +
            " cast(coalesce(indication/limit_value,0) as decimal(10,2)) as ratio " +
            " from ${schema_name}._sc_meter_item where facility_id=#{pm.facility_id}::int and is_delete>-1000 and status='2' and check_item='1' " +
            " and to_char(create_time,'yyyy-mm-dd')::timestamp between #{pm.begin_date}::timestamp and #{pm.end_date}::timestamp " +
            " order by create_time desc " +
            "</script>")
    List<Map<String, Object>> findRatioList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_asset_cus set sid_url=#{pm.sid_url} " +
            " where cus_id=#{pm.facility_id}::int and asset_id=#{pm.asset_id} " +
            "</script>")
    void updateCusAssetSid(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select distinct tab.cus_id as facility_id,tab.title,tab.asset_id,tab.asset_name,tab.sid_url, " +
            " coalesce(tab.sewage_value,(select code::numeric from ${schema_name}._sc_cloud_data " +
            " where data_type='pollute_price_seting' and reserve1='sewage_value' limit 1)) as sewage_value, " +
            " properties ::jsonb ->> 'GRM' as grm, properties ::jsonb ->> 'PASS' as pass " +
            " from (select tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sid_url,tb.sewage_value, " +
            " json_object_agg (tb.code,tb.value) ::text as properties " +
            " from (select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,ac.sid_url, " +
            " coalesce(ac.sewage_value,cu.sewage_value) as sewage_value, " +
            " jsonb_array_elements(ac.properties) ->> 'field_code' as code, " +
            " jsonb_array_elements(ac.properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu " +
            " on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000) tb group by tb.cus_id,tb.title,tb.asset_id,tb.asset_name,tb.sid_url,tb.sewage_value " +
            " union " +
            " select ac.cus_id,cu.title,ac.asset_id,ac.asset_name,null as sid_url, " +
            " coalesce(ac.sewage_value,cu.sewage_value) as sewage_value,null as properties " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu " +
            " on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and (ac.properties='[]' or ac.properties is null)) tab " +
            "</script>")
    List<Map<String, Object>> findNeedHandAssetList(@Param("schema_name") String schemaName);

    @Select("<script>" +
            " select distinct tab.cus_id as facility_id,tab.title,tab.position_code,tab.asset_id,tab.asset_name,tab.asset_code,tab.sid_url, " +
            " coalesce(tab.abnormal_value,(select code::numeric from ${schema_name}._sc_cloud_data " +
            " where data_type='pollute_price_seting' and reserve1='abnormal_value' limit 1)) as abnormal_value, " +
            " properties ::jsonb ->> 'GRM' as grm, properties ::jsonb ->> 'PASS' as pass " +
            " from (select tb.cus_id,tb.title,tb.position_code,tb.asset_id,tb.asset_name,tb.asset_code,tb.sid_url,tb.abnormal_value, " +
            " json_object_agg (tb.code,tb.value) ::text as properties " +
            " from (select ac.cus_id,cu.title,cu.position_code,ac.asset_id,ac.asset_name,ac.asset_code,ac.sid_url, " +
            " coalesce(ac.abnormal_value,cu.abnormal_value) as abnormal_value, " +
            " jsonb_array_elements(ac.properties) ->> 'field_code' as code, " +
            " jsonb_array_elements(ac.properties) ->> 'field_value' as value " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu " +
            " on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000) tb group by tb.cus_id,tb.title,tb.position_code,tb.asset_id,tb.asset_name,tb.asset_code,tb.sid_url,tb.abnormal_value " +
            " union " +
            " select ac.cus_id,cu.title,cu.position_code,ac.asset_id,ac.asset_name,ac.asset_code,null as sid_url, " +
            " coalesce(ac.abnormal_value,cu.abnormal_value) as abnormal_value,null as properties " +
            " from ${schema_name}._sc_asset_cus ac join ${schema_name}._sc_customer cu " +
            " on ac.cus_id=cu.facility_id " +
            " where ac.status>-1000 and cu.status>-1000 and (ac.properties='[]' or ac.properties is null)) tab " +
            "</script>")
    List<Map<String, Object>> findSewageAssetList(@Param("schema_name") String schemaName);

    @Select("<script>" +
            " select count(id) from ${schema_name}._sc_meter_sewage_records " +
            " where facility_id=#{pm.facility_id}::int and asset_id=#{pm.asset_id} " +
            " and (begin_date is null or begin_date is not null) and end_date is null " +
            "</script>")
    int findNoMeterSewageRecords(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select count(id) from ${schema_name}._sc_meter_sewage_records " +
            " where facility_id=#{pm.facility_id}::int and asset_id=#{pm.asset_id} " +
            " and begin_date is not null and end_date is null " +
            "</script>")
    int findHavedMeterSewageRecords(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Insert("<script>" +
            " insert into ${schema_name}._sc_meter_sewage_records (facility_id,asset_id,begin_date) " +
            " values (#{pm.facility_id}::int,#{pm.asset_id},(now()::timestamp(0)without time zone)::timestamp) " +
            "</script>")
    void insertMeterSewageRecords(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Update("<script>" +
            " update ${schema_name}._sc_meter_sewage_records set end_date=(now()::timestamp(0)without time zone)::timestamp " +
            " where facility_id=#{pm.facility_id}::int and asset_id=#{pm.asset_id} " +
            " and begin_date is not null and end_date is null " +
            "</script>")
    void updateMeterSewageRecords(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select count(1) from (select distinct msr.*,tm.begin_time,tm.end_time from ${schema_name}._sc_meter_sewage_records msr " +
            " left join ${schema_name}._sc_customer_unloading_time tm on msr.facility_id=tm.facility_id " +
            " <where> " +
            " msr.facility_id=#{pm.facility_id}::int and msr.asset_id=#{pm.asset_id} " +
            " <if test=\"pm.begin_date !=null and pm.begin_date !=''\"> " +
            " and to_char(msr.begin_date,'yyyy-mm-dd hh24:mi') >= to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " <if test=\"pm.end_date !=null and pm.end_date !=''\"> " +
            " and to_char(msr.end_date,'yyyy-mm-dd hh24:mi') &lt;= to_char(#{pm.end_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " </where>) s " +
            "</script>")
    int findMeterSewageRecordsCount(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select distinct msr.*,tm.begin_time as plan_begin_date,tm.end_time as plan_end_date from ${schema_name}._sc_meter_sewage_records msr " +
            " left join ${schema_name}._sc_customer_unloading_time tm on msr.facility_id=tm.facility_id " +
            " <where> " +
            " msr.facility_id=#{pm.facility_id}::int and msr.asset_id=#{pm.asset_id} " +
            " <if test=\"pm.begin_date !=null and pm.begin_date !=''\"> " +
            " and to_char(msr.begin_date,'yyyy-mm-dd hh24:mi') >= to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " <if test=\"pm.end_date !=null and pm.end_date !=''\"> " +
            " and to_char(msr.end_date,'yyyy-mm-dd hh24:mi') &lt;= to_char(#{pm.end_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " order by create_time desc ${pm.pagination} " +
            " </where> " +
            "</script>")
    List<Map<String, Object>> findMeterSewageRecordsList(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select timeconfig.time from " +
            " (select to_char(generate_series(to_date(to_char(#{pm.begin_date}::timestamp,'yyyymmdd'),'yyyymmdd'), " +
            " to_date(to_char(#{pm.begin_date}::timestamp+'1 day','yyyymmdd'),'yyyymmdd'),'0.5 hours'),'yyyy-mm-dd hh24:mi') as time) as timeconfig " +
            " <where> " +
            " <if test=\"pm.begin_date !=null and pm.begin_date !=''\"> " +
            " and timeconfig.time >= to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " <if test=\"pm.end_date !=null and pm.end_date !=''\"> " +
            " and timeconfig.time &lt;= to_char(#{pm.end_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " </where> " +
            "</script>")
    List<String> findTimeDay(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select to_char(msr.begin_date,'yyyy-mm-dd hh24:mi') as begin_date, " +
            " to_char(msr.end_date,'yyyy-mm-dd hh24:mi') as end_date, " +
            " to_char((to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd')::varchar ||' '|| tm.begin_time)::timestamp,'yyyy-mm-dd hh24:mi') as begin_time, " +
            " to_char((to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd')::varchar ||' '|| tm.end_time)::timestamp,'yyyy-mm-dd hh24:mi') as end_time " +
            " from ${schema_name}._sc_meter_sewage_records msr " +
            " left join ${schema_name}._sc_customer_unloading_time tm on msr.facility_id=tm.facility_id " +
            " <where> " +
            " msr.facility_id=#{pm.facility_id}::int and msr.asset_id=#{pm.asset_id} " +
            " <if test=\"pm.begin_date !=null and pm.begin_date !=''\"> " +
            " and to_char(msr.begin_date,'yyyy-mm-dd hh24:mi') >= to_char(#{pm.begin_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " <if test=\"pm.end_date !=null and pm.end_date !=''\"> " +
            " and to_char(msr.end_date,'yyyy-mm-dd hh24:mi') &lt;= to_char(#{pm.end_date}::timestamp,'yyyy-mm-dd hh24:mi') " +
            " </if> " +
            " </where> " +
            "</script>")
    List<Map<String, Object>> findMeterSewageRecordsTime(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    @Select("<script>" +
            " select distinct k.id from ${schema_name}._sc_role_asset_position sr" +
            " join ${schema_name}._sc_role f on sr.role_id=f.id" +
            " join ${schema_name}._sc_position_role g on g.role_id=f.id" +
            " join ${schema_name}._sc_position h on h.id=g.position_id" +
            " join ${schema_name}._sc_user_position i on i.position_id=g.position_id" +
            " join ${schema_name}._sc_user k on i.user_id=k.id" +
            " where sr.asset_position_code=#{pm.position_code}" +
            "</script>")
    List<String> findUserByPosition(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);
}
