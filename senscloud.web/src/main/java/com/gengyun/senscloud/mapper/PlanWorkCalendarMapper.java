package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface PlanWorkCalendarMapper {


    /**
     * 获取维保行事历总数
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECT count(1) FROM ( " +
            "SELECT pwc.work_calendar_code,pwc.title,pwc.position_code,ap.position_name,pwc.relation_id,a.asset_name," +
            "pwc.work_type_id,wt.type_name as work_type_name,to_char(pwc.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time," +
            "to_char(pwc.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time,pwc.status,pwc.auto_generate_bill " +
            "FROM ${schema_name}._sc_plan_work_calendar pwc " +
            "LEFT JOIN ${schema_name}._sc_work_type wt ON pwc.work_type_id=wt.id " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON pwc.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset a ON pwc.relation_id = a.id " +
            SqlConstant.PLAN_AUTH_SQL +
            "${condition} " +
            ") t")
    int findCountPlanWorkCalendarList(@Param("schema_name") String schemaName, @Param("condition") String searchWord, @Param("user_id") String user_id);


    /**
     * 获取维保计划名称
     *
     * @param schemaName 数据库
     * @param ids        维保计划主键
     */
    @Select("<script>" +
            "SELECT pwc.title, pwc.work_calendar_code " +
            "FROM ${schema_name}._sc_plan_work_calendar pwc " +
            "WHERE work_calendar_code in " +
            "<foreach collection='calendar_codes' item='codes' open='(' close=')' separator=','> " +
            "#{codes} " +
            "</foreach> " +
            "</script> ")
    @MapKey("work_calendar_code")
    Map<String, Map<String, Object>> findPlanWorkCalendarNameList(@Param("schema_name") String schemaName, @Param("calendar_codes") String ids[]);

    /**
     * 获取维保行事历列表
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECT pwc.work_calendar_code,pwc.plan_code,pwc.title,pwc.position_code,ap.position_name,pwc.relation_id,a.asset_name," +
            "pwc.work_type_id,wt.type_name as work_type_name,to_char(pwc.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time," +
            "to_char(pwc.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time," +
            "to_char(pwc.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time,pwc.status,pwc.auto_generate_bill,u.user_name," +
            "to_char(pwc.occur_time,'HH24:mi') begin_time,pwc.receive_user_id," +
            "case pwc.priority_level when 0 then '无' when 1 then '低' when 2 then '中' when 3 then '高'  end AS priority_level_name," +
            "case pwc.auto_generate_bill when 1 then '生产' when 2 then '不生成' END AS auto_generate_bill_name " +
            "FROM ${schema_name}._sc_plan_work_calendar pwc " +
            "LEFT JOIN ${schema_name}._sc_work_type wt ON pwc.work_type_id=wt.id " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON pwc.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset a ON pwc.relation_id = a.id " +
            "LEFT JOIN ${schema_name}._sc_user u ON pwc.receive_user_id = u.id " +
            SqlConstant.PLAN_AUTH_SQL +
            "${condition} ")
    List<Map<String, Object>> findPlanWorkCalendarList(@Param("schema_name") String schemaName, @Param("condition") String searchWord, @Param("user_id") String user_id);

    /**
     * 获取维保行事历列表
     *
     * @param schemaName
     * @param codes
     * @return
     */
    @Select(" <script> " +
            " SELECT pwc.work_calendar_code,pwc.plan_code,pwc.title,pwc.position_code,ap.position_name,pwc.relation_id,a.asset_name," +
            " pwc.work_type_id,wt.type_name as work_type_name,to_char(pwc.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time," +
            " to_char(pwc.create_time,'yyyy-MM-dd HH24:mi:ss') as create_time," +
            " to_char(pwc.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time,pwc.status,pwc.auto_generate_bill,u.user_name," +
            " to_char(pwc.occur_time,'HH24:mi') begin_time,pwc.receive_user_id ," +
            " case pwc.priority_level when 0 then '无' when 1 then '低' when 2 then '中' when 3 then '高'  end AS priority_level_name," +
            " case pwc.auto_generate_bill when 1 then '是' when 2 then '否' END AS auto_generate_bill_name " +
            " FROM ${schema_name}._sc_plan_work_calendar pwc " +
            " LEFT JOIN ${schema_name}._sc_work_type wt ON pwc.work_type_id=wt.id " +
            " LEFT JOIN ${schema_name}._sc_asset_position ap ON pwc.position_code = ap.position_code " +
            " LEFT JOIN ${schema_name}._sc_asset a ON pwc.relation_id = a.id " +
            " LEFT JOIN ${schema_name}._sc_user u ON pwc.receive_user_id = u.id " +
            " WHERE pwc.work_calendar_code IN " +
            " <foreach collection='codes' item='code' open='(' close=')' separator=','> #{code}::varchar </foreach>" +
            " order by pwc.status asc,pwc.create_time asc " +
            " </script> ")
    List<Map<String, Object>> findPlanWorkCalendarListByCodes(@Param("schema_name") String schemaName, @Param("codes") String[] codes);

    /**
     * 获取维保行事历列表
     *
     * @param schemaName
     * @param codes
     * @return
     */
    @Select(
            " SELECT STRING_agg(title,',') " +
                    " FROM ${schema_name}._sc_plan_work_calendar pwc " +
                    " WHERE pwc.work_calendar_code = #{codes} "
    )
    String findPlanWorkCalendarTitleByCodes(@Param("schema_name") String schemaName, @Param("codes") String codes);

    /**
     * 查询详情
     *
     * @param schemaName
     * @param work_calendar_code
     * @return
     */
    @Select(" SELECT work_calendar_code,c.work_type_id,c.work_template_code,c.title,c.position_code,c.relation_type," +
            " c.relation_id,c.problem_note,c.occur_time,c.deadline_time,c.plan_deal_time,c.status,c.receive_user_id," +
            " c.group_id,c.plan_code,c.priority_level,c.remark,c.create_time,c.create_user_id,c.auto_generate_bill," +
            " pwp.body_property,c.generate_time_point,c.body_property AS plan_work_property " +
            " FROM ${schema_name}._sc_plan_work_calendar c " +
            " left join ${schema_name}._sc_plan_work_property pwp ON c.plan_code = pwp.plan_code " +
            " where c.work_calendar_code = #{work_calendar_code}")
    PlanWorkCalendarModel findDetailByCode(@Param("schema_name") String schemaName, @Param("work_calendar_code") String work_calendar_code);

    /**
     * 根据主键查询数量
     *
     * @param schema_name 数据库
     * @param id          计划行事历主键
     * @return 统计数量
     */
    @Select("SELECT count(1) FROM ${schema_name}._sc_plan_work_calendar t WHERE t.work_calendar_code = #{id} and status = 2 ")
    int cntPwcWithStatusById(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据主键批量查询数量
     *
     * @param schema_name 数据库
     * @param ids         计划行事历主键
     * @return 统计数量
     */
    @Select(" <script> SELECT count(1) FROM ${schema_name}._sc_plan_work_calendar c " +
            " WHERE status = 2 and c.work_calendar_code IN " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::varchar </foreach>" +
            " </script> ")
    int cntPwcWithStatusByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    @Update("<script> " +
            "UPDATE ${schema_name}._sc_plan_work_calendar " +
            "<set> status = #{status}, " +
            "<if test=\"occur_time!=null and occur_time !='' \"> " +
            "occur_time = #{occur_time}::timestamp, " +
            "</if> " +
            "<if test=\"deadline_time!=null and deadline_time !='' \"> " +
            "deadline_time = #{deadline_time}::timestamp, " +
            "</if> " +
            "<if test=\"receive_user_id!=null and receive_user_id !='' \"> " +
            "receive_user_id = #{receive_user_id}, " +
            "</if> " +
            "<if test=\"work_type_id!=null and work_type_id !='' \"> " +
            "work_type_id = #{work_type_id}::int, " +
            "</if> " +
            "<if test=\"priority_level!=null and priority_level !='' \"> " +
            "priority_level = #{priority_level}::int, " +
            "</if> " +
            "<if test=\"auto_generate_bill!=null and auto_generate_bill !='' \"> " +
            "auto_generate_bill = #{auto_generate_bill}::int, " +
            "</if> " +
            "<if test=\"group_id!=null and group_id !='' \"> " +
            "group_id = #{group_id}::int, " +
            "</if> " +
            "<if test=\"planJson!=null and planJson !='' \"> " +
            "body_property = #{planJson}::jsonb, " +
            "</if> " +
            "<if test=\"generate_time_point!=null and generate_time_point !='' \"> " +
            "generate_time_point = #{generate_time_point}::int, " +
            "</if> " +
            "</set> " +
            "WHERE work_calendar_code = #{work_calendar_code} " +
            "</script> ")
    void updatePlanWorkCalendar(Map<String, Object> data);

    /**
     * 更新维保行事历状态
     *
     * @param schema_name 数据库
     * @param id          计划行事历主键
     * @param status      状态
     * @return 统计数量
     */
    @Update("UPDATE ${schema_name}._sc_plan_work_calendar set status = #{status} WHERE work_calendar_code = #{id} ")
    int updatePwcStatusById(@Param("schema_name") String schema_name, @Param("id") String id, @Param("status") int status);

    /**
     * 更新维保行事历状态（批量）
     *
     * @param schema_name 数据库
     * @param ids         计划行事历主键
     * @param status      状态
     * @return 统计数量
     */
    @Update(" <script> UPDATE ${schema_name}._sc_plan_work_calendar c set status = #{status} " +
            " WHERE c.work_calendar_code IN " +
            " <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::varchar </foreach>" +
            " </script> ")
    int updatePwcStatusByIds(@Param("schema_name") String schema_name, @Param("ids") String[] ids, @Param("status") int status);

//    @Select("SELECT c.work_calendar_code,c.title,c.work_type_id,c.priority_level,c.auto_generate_bill,c.position_code,c.relation_id, " +
//            "to_char(c.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time,to_char(c.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time, " +
//            "c.relation_type,a.asset_name,ap.position_name,wt.type_name,c.status,c.receive_user_id,c.plan_code,c.group_id, " +
//            "t.duty_type,t.self_or_out,t.total_hour,t.team_size,g.group_name ,u.user_name as receive_user_name, " +
//            "coalesce((select (c.resource->>cd1.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd1.name) as duty_type_name, " +
//            "coalesce((select (c.resource->>cd2.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd2.name) as self_or_out_name, " +
//            "coalesce((select (c.resource->>cd3.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd3.name) as priority_level_name, " +
//            "coalesce((select (c.resource->>cd4.name)::jsonb->>#{userLang} from public.company_resource c where c.company_id = #{companyId}), cd4.name) as auto_generate_bill_name, " +
//            "case when c.body_property is not null then c.body_property::text else (SELECT p.body_property::text from ${schema_name}._sc_plan_work_property p WHERE p.plan_code = c.plan_code) end as body_property, " +
//            "c.work_template_code,c.problem_note,c.plan_deal_time,c.remark,c.create_time,c.create_user_id,c.generate_time_point " +
//            "FROM ${schema_name}._sc_plan_work_calendar c " +
//            "LEFT JOIN ${schema_name}._sc_work_type wt on c.work_type_id = wt.id " +
//            "LEFT JOIN ${schema_name}._sc_plan_work_assist_team t on c.plan_code=t.plan_code and t.duty_type = '1' " +
//            "LEFT JOIN ${schema_name}._sc_group as g on c.group_id = g.id " +
//            "LEFT JOIN ${schema_name}._sc_user u ON c.receive_user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 ON cd1.code = t.duty_type::varchar and cd1.data_type='duty' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 ON cd2.code = t.self_or_out::varchar and cd2.data_type='team_type' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd3 ON cd3.code = c.priority_level::varchar and cd3.data_type='priority_level' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd4 ON cd4.code = c.auto_generate_bill::varchar and cd4.data_type='auto_generate_bill' " +
//            "LEFT JOIN ${schema_name}._sc_asset a on a.id = c.relation_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_position ap on ap.position_code = c.position_code " +
//            "WHERE work_calendar_code = #{work_calendar_code} ")
//    @Options(flushCache = Options.FlushCachePolicy.TRUE)
//    Map<String, Object> findPlanWorkCalendarInfoByCode(@Param("schema_name") String schemaName, @Param("work_calendar_code") String work_calendar_code, @Param("userLang") String userLang, @Param("companyId") Long companyId);

    @Select("SELECT c.work_calendar_code,c.title,c.work_type_id,c.priority_level,c.auto_generate_bill,c.position_code,c.relation_id, " +
            "to_char(c.occur_time,'yyyy-MM-dd HH24:mi:ss') as occur_time,to_char(c.deadline_time,'yyyy-MM-dd HH24:mi:ss') as deadline_time, " +
            "c.relation_type,a.asset_name,ap.position_name,wt.type_name,c.status,c.receive_user_id,c.plan_code,c.group_id, " +
            "t.duty_type,t.self_or_out,t.total_hour,t.team_size,g.group_name ,u.user_name as receive_user_name, " +
            "cd1.name as duty_type_name, " +
            "cd2.name as self_or_out_name, " +
            "cd3.name as priority_level_name, " +
            "cd4.name as auto_generate_bill_name, " +
            "case when c.body_property is not null then c.body_property::text else (SELECT p.body_property::text from ${schema_name}._sc_plan_work_property p WHERE p.plan_code = c.plan_code) end as body_property, " +
            "c.work_template_code,c.problem_note,c.plan_deal_time,c.remark,c.create_time,c.create_user_id,c.generate_time_point " +
            "FROM ${schema_name}._sc_plan_work_calendar c " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on c.work_type_id = wt.id " +
            "LEFT JOIN ${schema_name}._sc_plan_work_assist_team t on c.plan_code=t.plan_code and t.duty_type = '1' " +
            "LEFT JOIN ${schema_name}._sc_group as g on c.group_id = g.id " +
            "LEFT JOIN ${schema_name}._sc_user u ON c.receive_user_id = u.id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 ON cd1.code = t.duty_type::varchar and cd1.data_type='duty' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 ON cd2.code = t.self_or_out::varchar and cd2.data_type='team_type' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd3 ON cd3.code = c.priority_level::varchar and cd3.data_type='priority_level' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd4 ON cd4.code = c.auto_generate_bill::varchar and cd4.data_type='auto_generate_bill' " +
            "LEFT JOIN ${schema_name}._sc_asset a on a.id = c.relation_id " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap on ap.position_code = c.position_code " +
            "WHERE work_calendar_code = #{work_calendar_code} ")
    @Options(flushCache = Options.FlushCachePolicy.TRUE)
    Map<String, Object> findPlanWorkCalendarInfoByCode(@Param("schema_name") String schemaName, @Param("work_calendar_code") String work_calendar_code);
}
