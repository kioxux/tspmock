package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 科华工作安排
 */
@Mapper
public interface ArrangementMapper {
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = ArrangementProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                    @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                    @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("deliverable_type") String deliverable_type);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = ArrangementProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("deliverable_type") String deliverable_type);
//
//    /**
//     * 新增数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @InsertProvider(type = ArrangementProvider.class, method = "insert")
//    int insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_work_task set " +
//            "task_title=#{s.task_title}, " +
//            "task_content=#{s.task_content}, " +
//            "begin_date=#{s.begin_date}::timestamp, " +
//            "end_date=#{s.end_date}::timestamp, " +
//            "task_rate=#{s.task_rate}::int, " +
//            "important_level=#{s.important_level}::int, " +
//            "deliverables=#{s.deliverables}, " +
//            "deliverable_day=#{s.deliverable_day}," +
//            "deliverable_time=#{s.deliverable_time}::timestamp," +
//            "duty_man=#{s.duty_man}," +
//            "help_man=#{s.help_man}," +
//            "audit_man=#{s.audit_man}," +
//            "is_finished=#{s.is_finished}::int," +
//            "is_use=#{s.is_use}::bool " +
//            "where id=#{s.id}::int ")
//    int update(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//    /**
//     * 根据主键查询工作安排
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_work_task w where id = #{id} ")
//    Map<String, Object> queryArrangementById(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Update("DELETE FROM ${schema_name}._sc_work_task where id=#{id} ")
//    int delete(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//    /**
//     * 实现类
//     */
//    class ArrangementProvider {
//        /**
//         * 新增数据
//         *
//         * @param schemaName
//         * @param paramMap
//         * @return
//         */
//        public String insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_work_task");
//                VALUES("task_title ", "#{s.task_title}");
//                VALUES("task_content ", "#{s.task_content}");
//                VALUES("begin_date ", "#{s.begin_date}::timestamp");
//                VALUES("end_date ", "#{s.end_date}::timestamp");
//                VALUES("task_rate ", "#{s.task_rate}::int");
//                VALUES("important_level ", "#{s.important_level}::int");
//                VALUES("deliverables ", "#{s.deliverables}");
//                VALUES("deliverable_type ", "#{s.deliverable_type}::int");
//                VALUES("deliverable_day ", "#{s.deliverable_day}");
//                VALUES("deliverable_time ", "#{s.deliverable_time}::timestamp");
//                VALUES("duty_man ", "#{s.duty_man}");
//                VALUES("help_man ", "#{s.help_man}");
//                VALUES("audit_man ", "#{s.audit_man}");
//                VALUES("is_finished ", "#{s.is_finished}::int");
//                VALUES("is_use ", "#{s.is_use}");
//                VALUES("create_user_account ", "#{s.create_user_account}");
//                VALUES("create_time ", "current_timestamp");
//            }}.toString();
//        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param searchKey
//         * @return
//         */
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime,
//                @Param("deliverable_type") String deliverable_type) {
//            return new SQL() {{
//                String condition="1=1 ";
//                SELECT(" a.*,c.username as create_user,d.username as duty_name, f.username as help_name");
//                FROM(schemaName + "._sc_work_task a "+
//                        "LEFT JOIN " +schemaName + "._sc_user c ON c.account=a.create_user_account "+
//                        "LEFT JOIN " +schemaName + "._sc_user d ON d.account=a.duty_man "+
//                        "LEFT JOIN " +schemaName + "._sc_user f ON f.account=a.help_man ");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                     condition+="and upper(a.task_title) like upper('%" + searchKey
//                            + "%') or upper(a.task_content) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey + "%') ";
//                }
//                if(null != beginTime && !"".equals(beginTime)&&null != endTime && !"".equals(endTime)){
//                    condition+=" and a.begin_date>='"+ Timestamp.valueOf(beginTime + " 00:00:00")+"' and a.begin_date<'"+Timestamp.valueOf(endTime + " 00:00:00")+"'";
//                }
//                if(null != deliverable_type && !"".equals(deliverable_type)&& !"0".equals(deliverable_type)){
//                    condition+=" and a.deliverable_type="+deliverable_type;
//                }
//                WHERE(condition);
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey, @Param("beginTime") String beginTime, @Param("endTime") String endTime,
//                 @Param("deliverable_type") String deliverable_type) {
//            return new SQL() {{
//                String condition="1=1 ";
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_work_task a "+
//                        "LEFT JOIN " +schemaName + "._sc_user c ON c.account=a.create_user_account "+
//                        "LEFT JOIN " +schemaName + "._sc_user d ON d.account=a.duty_man "+
//                        "LEFT JOIN " +schemaName + "._sc_user f ON f.account=a.help_man ");
//                // 查询条件
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    condition+="and upper(a.task_title) like upper('%" + searchKey
//                            + "%') or upper(a.task_content) like upper('%" + searchKey
//                            + "%') or upper(c.username) like upper('%" + searchKey + "%') ";
//                }
//                if(null != beginTime && !"".equals(beginTime)&&null != endTime && !"".equals(endTime)){
//                    condition+=" and a.begin_date>='"+ Timestamp.valueOf(beginTime + " 00:00:00")+"' and a.begin_date<'"+Timestamp.valueOf(endTime + " 00:00:00")+"'";
//                }
//                if(null != deliverable_type && !"".equals(deliverable_type)&&!"0".equals(deliverable_type)){
//                    condition+=" and a.deliverable_type="+deliverable_type;
//                }
//                WHERE(condition);
//            }}.toString();
//        }
//
//    }
}
