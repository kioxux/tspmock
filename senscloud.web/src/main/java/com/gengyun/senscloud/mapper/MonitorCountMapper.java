package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MonitorCountMapper {
//    //查找所有设备对应年的记录
//    @Select("select a.strname,a.strcode," +
//            "a ._id," +
//            "Jan.val as jan_val," +
//            "Feb.val as feb_val," +
//            "Mar.val as mar_val," +
//            "Apr.val as apr_val," +
//            "May.val as may_val," +
//            "Jun.val as jun_val," +
//            "Jul.val as jul_val, " +
//            "Aug.val as aug_val," +
//            "Sept.val as sept_val," +
//            "Oct.val as oct_val," +
//            "Nov.val as nov_val," +
//            "Dec.val as dec_val," +
//            "(Jan.val+Feb.val+Mar.val+Apr.val+May.val+Jun.val+Jul.val+Aug.val+Sept.val+Oct.val+Nov.val+DEC.val)as total_val, " +
//            "array_to_string(array_agg(f.short_title),',')as facility_title, " +
//            "g.category_name"+
//            " from ${schema_name}._sc_asset a " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total'  group by t.asset_code,substring (t.gather_time from 1 for 7))Jan on Jan.asset_code=a.strcode " +
//            "and Jan.d=array_to_string(array['${timecondition}','-01'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Feb on Feb.asset_code=a.strcode " +
//            "and Feb.d=array_to_string(array['${timecondition}','-02'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Mar on Mar.asset_code=a.strcode " +
//            "and Mar.d=array_to_string(array['${timecondition}','-03'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Apr on Apr.asset_code=a.strcode " +
//            "and Apr.d=array_to_string(array['${timecondition}','-04'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))May on May.asset_code=a.strcode " +
//            "and May.d=array_to_string(array['${timecondition}','-05'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Jun on Jun.asset_code=a.strcode " +
//            "and Jun.d=array_to_string(array['${timecondition}','-06'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Jul on Jul.asset_code=a.strcode " +
//            "and Jul.d=array_to_string(array['${timecondition}','-07'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Aug on Aug.asset_code=a.strcode " +
//            "and Aug.d=array_to_string(array['${timecondition}','-08'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Sept on Sept.asset_code=a.strcode " +
//            "and Sept.d=array_to_string(array['${timecondition}','-09'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Oct on Oct.asset_code=a.strcode " +
//            "and Oct.d=array_to_string(array['${timecondition}','-10'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Nov on Nov.asset_code=a.strcode " +
//            "and Nov.d=array_to_string(array['${timecondition}','-11'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Dec on Dec.asset_code=a.strcode " +
//            "and Dec.d=array_to_string(array['${timecondition}','-12'], '') " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization sr on a._id=sr.asset_id  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f on sr.org_id=f.id  " +
//            "LEFT JOIN ${schema_name}._sc_asset_category g on a.category_id=g.id  " +
//            "where (f.org_type in(1,2) or f.org_type ISNULL) ${condition} "+
//            "GROUP BY  " +
//            "A ._id," +
//            "A .strname," +
//            "A .strcode," +
//            "Jan.val ," +
//            "Feb.val ," +
//            "Mar.val ," +
//            "Apr.val ," +
//            "May.val ," +
//            "Jun.val ," +
//            "Jul.val," +
//            "Aug.val ," +
//            "Sept.val ," +
//            "Oct.val," +
//            "Nov.val ," +
//            "DEC .val," +
//            "G .category_name "+
//            "limit ${page} offset ${begin}")
//    List<Map<String,Object>> getAssetMonitorCountList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("timecondition") String timecondition,@Param("page") int pageSize, @Param("begin") int begin);
//
//    //查找所有设备对应年的记录总数
//    @Select( "select count(1) from (" +
//            "select a.strname,a.strcode," +
//            "a ._id," +
//            "Jan.val as jan_val," +
//            "Feb.val as feb_val," +
//            "Mar.val as mar_val," +
//            "Apr.val as apr_val," +
//            "May.val as may_val," +
//            "Jun.val as jun_val," +
//            "Jul.val as jul_val, " +
//            "Aug.val as aug_val," +
//            "Sept.val as sept_val," +
//            "Oct.val as oct_val," +
//            "Nov.val as nov_val," +
//            "Dec.val as dec_val," +
//            "(Jan.val+Feb.val+Mar.val+Apr.val+May.val+Jun.val+Jul.val+Aug.val+Sept.val+Oct.val+Nov.val+DEC.val)as total_val, " +
//            "array_to_string(array_agg(f.title),',')as facility_title, " +
//            "g.category_name"+
//            " from ${schema_name}._sc_asset a " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total'  group by t.asset_code,substring (t.gather_time from 1 for 7))Jan on Jan.asset_code=a.strcode " +
//            "and Jan.d=array_to_string(array['${timecondition}','-01'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Feb on Feb.asset_code=a.strcode " +
//            "and Feb.d=array_to_string(array['${timecondition}','-02'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Mar on Mar.asset_code=a.strcode " +
//            "and Mar.d=array_to_string(array['${timecondition}','-03'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Apr on Apr.asset_code=a.strcode " +
//            "and Apr.d=array_to_string(array['${timecondition}','-04'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))May on May.asset_code=a.strcode " +
//            "and May.d=array_to_string(array['${timecondition}','-05'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Jun on Jun.asset_code=a.strcode " +
//            "and Jun.d=array_to_string(array['${timecondition}','-06'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Jul on Jul.asset_code=a.strcode " +
//            "and Jul.d=array_to_string(array['${timecondition}','-07'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Aug on Aug.asset_code=a.strcode " +
//            "and Aug.d=array_to_string(array['${timecondition}','-08'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Sept on Sept.asset_code=a.strcode " +
//            "and Sept.d=array_to_string(array['${timecondition}','-09'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Oct on Oct.asset_code=a.strcode " +
//            "and Oct.d=array_to_string(array['${timecondition}','-10'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Nov on Nov.asset_code=a.strcode " +
//            "and Nov.d=array_to_string(array['${timecondition}','-11'], '') " +
//            "LEFT JOIN(select MAX(CASE WHEN t.monitor_value ISNULL THEN  0 ELSE to_number(t.monitor_value,'99999999.99') END) as val,t.asset_code,substring (t.gather_time from 1 for 7) d from ${schema_name}._sc_asset_monitor_current t where t.monitor_value_type='20' and t.monitor_name='Month_Dosing_Accumulate_Total' group by t.asset_code,substring (t.gather_time from 1 for 7))Dec on Dec.asset_code=a.strcode " +
//            "and Dec.d=array_to_string(array['${timecondition}','-12'], '') " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization sr on a._id=sr.asset_id  " +
//            "LEFT JOIN ${schema_name}._sc_facilities f on sr.org_id=f.id  " +
//            "LEFT JOIN ${schema_name}._sc_asset_category g on a.category_id=g.id  " +
//            "where (f.org_type in(1,2) or f.org_type ISNULL) ${condition} "+
//            "GROUP BY  " +
//            "A ._id," +
//            "A .strname," +
//            "A .strcode," +
//            "Jan.val ," +
//            "Feb.val ," +
//            "Mar.val ," +
//            "Apr.val ," +
//            "May.val ," +
//            "Jun.val ," +
//            "Jul.val," +
//            "Aug.val ," +
//            "Sept.val ," +
//            "Oct.val," +
//            "Nov.val ," +
//            "DEC .val," +
//            "G .category_name ) s")
//    int getAssetMonitorCountListCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("timecondition") String timecondition);
//
//    //插入数据
//    @Insert("insert into ${schema_name}._sc_asset_monitor_current ( " +
//            "asset_code,monitor_value_type,monitor_value,monitor_name,gather_time,remark,create_time,source_type)  values  " +
//            "(#{asset_code},#{monitor_value_type},#{count_value},#{monitor_name},#{count_time},#{remark},#{create_time},#{source_type})")
//    int InsertMonitorCountData(Map<String,Object> s);
//
//    @Delete("DELETE FROM ${schema_name}._sc_asset_monitor_current  " +
//            "WHERE monitor_value_type='20' ${condition}  ")
//    int deleteMonitorCountData(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查找组织下所有的设备
//    @Select("select distinct a._id,a.strname,a.strcode as asset_code  " +
//            " from ${schema_name}._sc_asset a " +
//            " left join ${schema_name}._sc_asset_organization sr on a._id=sr.asset_id " +
//            " left join ${schema_name}._sc_facilities f on sr.org_id=f.id  " +
//            "where 1=1 " +
//            " ${condition} ")
//    List<Map<String,Object>> getAssetList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查找对应设备所关联的所有组织
//    @Select("select f.id " +
//            " from ${schema_name}._sc_facilities f " +
//            " left join ${schema_name}._sc_asset_organization sr on sr.org_id=f.id  " +
//            " left join ${schema_name}._sc_asset a on a._id=sr.asset_id " +
//            "where 1=1 " +
//            " ${condition} ")
//    List<String> getOraganzationList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查找相应条件下的监控值
//    @Select("select a.monitor_value,a.id  " +
//            "from ${schema_name}._sc_asset_monitor a " +
//            "where 1=1 ${condition}" +
//            "order by to_number(a.monitor_value,'99999999.99') desc  ")
//    List<Map<String,Object>> getCountVlaueByAsset(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //修改原有的监控数据
//    @Update("update  ${schema_name}._sc_asset_monitor " +
//            "set monitor_value=#{monitor_value} " +
//            "where id=#{id} ")
//    int updateMonitorCount(@Param("schema_name") String schema_name, @Param("id") long id,@Param("monitor_value") String monitor_value);
//
//    //查找组织下所有的设备
//    @Select("select a._id,a.strname,a.strcode from ${schema_name}._sc_asset a " +
//            "left join ${schema_name}._sc_asset_organization sr  on a._id=sr.asset_id " +
//            "left join  ${schema_name}._sc_facilities f  on sr.org_id=f.id "+
//            "where a.strcode NOT IN (select h.asset_code from(select count(1) as mon,s.asset_code from (select DISTINCT m.asset_code,m.gather_time,substring(m.gather_time from 1 for 7) as times,substring(m.gather_time from 1 for 4)as years from ${schema_name}._sc_asset_monitor m " +
//            "where m.monitor_value_type=20 ${timecondition1} ${timecondition2}    " +
//            "GROUP BY m.asset_code,m.gather_time)s " +
//            "GROUP BY s.asset_code)h " +
//            "where 1=1  ${timecondition3} )" +
//            " ${condition} ")
//    List<Map<String,Object>> findAssetpatch(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                            @Param("timecondition1") String timecondition1, @Param("timecondition2") String timecondition2, @Param("timecondition3") String timecondition3);
//
//    //查找所有待补录设备所属的客户组织
//    @Select("select DISTINCT f.short_title,f.id from ${schema_name}._sc_facilities f " +
//            "left join ${schema_name}._sc_asset_organization sr ON sr.org_id = f.ID " +
//            "left join  ${schema_name}._sc_asset A ON A._id = sr.asset_id "+
//            "where a.strcode NOT IN (select h.asset_code from(select count(1) as mon,s.asset_code from (select DISTINCT m.asset_code,m.gather_time,substring(m.gather_time from 1 for 7) as times,substring(m.gather_time from 1 for 4)as years from ${schema_name}._sc_asset_monitor m " +
//            "where m.monitor_value_type=20 ${timecondition1} ${timecondition2}    " +
//            "GROUP BY m.asset_code,m.gather_time)s " +
//            "GROUP BY s.asset_code)h " +
//            "where 1=1  ${timecondition3} )" +
//            " ${condition} ")
//    List<Map<String,Object>> findFacilityFromAssetpatch(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                            @Param("timecondition1") String timecondition1, @Param("timecondition2") String timecondition2, @Param("timecondition3") String timecondition3);
}