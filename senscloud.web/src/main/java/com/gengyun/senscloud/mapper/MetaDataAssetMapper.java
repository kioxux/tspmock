package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.model.AssetTemplateTaskTemplateData;
import com.gengyun.senscloud.model.MetaDataAsset;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @description 资产元数据表
 * 用于对资产元数据表的插入 删除 更新
 */
@Mapper
public interface MetaDataAssetMapper {

//    //region 表相关
//    @Update("update _sc_metadata_assets set fields = #{fields},\n" +
//            "asset_alias = #{asset_alias},description = #{description},\n" +
//            "asset_name = #{asset_name}")
//    void updateMetaDataAsset(MetaDataAsset metaDataAsset);
//
//    /*@Insert("insert into #{schema_name}._sc_metadata_assets(asset_name,asset_alias,description,fields) values(#{asset_name},#{asset_alias},#{description},#{fields})")
//    void insertMetaDataAsset(String schema_name, MetaDataAsset metaDataAsset);*/
//
//    @Update("insert into ${schema_name}._sc_metadata_assets(asset_name,asset_alias,description,fields) values(#{asset_name},#{asset_alias},#{description},#{fields}::jsonb);")
//    @Deprecated
//    void insertMetaDataAsset(@Param("schema_name") String schema_name,
//                             @Param("asset_name") String asset_name,
//                             @Param("asset_alias") String asset_alias,
//                             @Param("description") String description,
//                             @Param("fields") String fields);
//
//    @Update("insert into ${schema_name}._sc_metadata_assets(asset_name,asset_alias,description,fields) values(#{asset_name},#{asset_alias},#{description},#{fields}::jsonb);")
//    void insertMetaDataAsset2(MetaDataAsset metaDataAsset);
//
//    @Update("update ${schema_name}._sc_metadata_assets set icon=#{icon} where asset_name=#{asset_name}")
//    void updateAssetIcon(MetaDataAsset metaDataAsset);
//
//    @Select("select icon from ${schema_name}._sc_metadata_assets where asset_name=#{asset_name}")
//    String getAssetIcon(MetaDataAsset metaDataAsset);
//
//    @Update("update ${schema_name}._sc_metadata_assets set asset_alias=#{asset_alias},description=#{description} where asset_name=#{asset_name}")
//    void updateMetaDataAssetBaseInfo(MetaDataAsset metaDataAsset);
//
//    @Update("CREATE TABLE ${schema_name}.${asset_name}(_id character varying(256) NOT NULL,PRIMARY KEY (_id)) WITH (OIDS = FALSE) TABLESPACE pg_default;\n" +
//            "COMMENT ON TABLE ${schema_name}.${asset_name} IS '${description}';" +
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN strName character varying(256);" +//名称
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN strCode character varying(256);" +//设备编号
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN strModel character varying(96);" +//型号
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN intStatus integer DEFAULT 1;" +//设备状态，在线，离线，报废，删除等
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN strDescription text;" +//设备描述信息
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN intSiteID bigint;" +//所属位置ID号
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN file_ids text;" +//附件ids
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN qtyMinStockCount numeric;" +//最小库存
//            "ALTER TABLE ${schema_name}.${asset_name} ADD COLUMN qtyStockCount numeric;" //当前库存
//    )
//    @Deprecated
//    void createAssetTable(MetaDataAsset metaDataAsset);
//
////    @Select("select * from ${schema_name}._sc_asset_category order by id")
////    List<MetaDataAsset> findAllMetaDataAsset(@Param("schema_name") String schema_name);
//
//    @Select("select asset_name from ${schema_name}._sc_metadata_assets order by index")
//    List<String> findAllMetaDataAssetName(@Param("schema_name") String schema_name);
//
////    @Select("select * from ${schema_name}._sc_asset_category where id=#{category_id}")
////    MetaDataAsset findMetaDataAsset(@Param("schema_name") String schema_name, @Param("category_id") int category_id);
//
//    @Delete("delete from ${schema_name}._sc_metadata_assets where asset_name = #{asset_name}")
//    void deleteMetaDataAsset(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name);
//
//    @Update("drop table if exists ${schema_name}.${asset_name};")
//    @Deprecated
//    void dropMetaDataAssetTable(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name);
//    //endregion 表相关
//
//    //region 字段相关
//    @Update("update ${schema_name}._sc_metadata_assets set fields=#{fields}::jsonb where asset_name=#{asset_name}")
//    void metadataUpdateField(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name, @Param("fields") String fields);
//
//    @Update("ALTER TABLE ${schema_name}.${asset_name} RENAME ${old_field_name} TO ${new_field_name};")
//    void metadataRenameField(@Param("schema_name") String schema_name,
//                             @Param("asset_name") String asset_name,
//                             @Param("old_field_name") String old_field_name,
//                             @Param("new_field_name") String new_field_name);
//
//    @Update("ALTER TABLE ${schema_name}.${asset_name}  ALTER COLUMN ${field_name} TYPE ${type} USING ${field_name}::${type};")
//    void metadataAlterFieldType(@Param("schema_name") String schema_name,
//                                @Param("asset_name") String asset_name,
//                                @Param("field_name") String field_name,
//                                @Param("type") String type);
//
//    @Update("ALTER TABLE ${schema_name}.${asset_name} ${column_sql}")
//    void addField(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name, @Param("column_sql") String column);
//
//    @Update("ALTER TABLE ${schema_name}.${asset_name} DROP COLUMN IF EXISTS ${field_name}")
//    void dropField(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name, @Param("field_name") String field_name);
//    //endregion 字段相关
//
//    @Select("select count(_id) from ${schema_name}.${asset_name} where ${condition}")
//    Integer getAssetTotal(@Param("schema_name") String schema_name, @Param("asset_name") String asset_name, @Param("condition") String condition);
//    //endregion 字段相关

}
