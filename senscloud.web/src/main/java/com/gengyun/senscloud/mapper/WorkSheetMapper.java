package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkSheetMapper {

//
//    //查找维修单列表
//    @Select("select count(1) from (" +
//            "select distinct w.work_code " +
////            "select distinct w.*,u.username as create_user_name,c.username as receive_name,s.status as status_name," +
////            "t.type_name as type_name,dv.strname as asset_name " +
////            ", w2.title as title,w2.priority_level,w2.receive_time,w2.distribute_time," +
////            "w2.receive_account,w2.sub_work_code,w2.begin_time,w2.finished_time,w2.audit_time," +
////            "dv.strcode as strcode,ct.short_title as customer_name,t.flow_template_code as flow_code," +
////            "t.business_type_id as business_type_id,ma.category_name as asset_type," +
////            "ff.type_name as fault_name,tt.type_name as repair_type_name,w2.result_note, ars.running_status, " +
////            " ${dySqlMap.feeSelectSql} " +
////            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN wn.short_title ELSE ap.position_name END AS facility_name, " +
////            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN wnp.short_title ELSE apt.position_name END AS parent_name, " +
////            " floor(cast((COALESCE(case when w2.status=" + StatusConstant.COMPLETED + " then (EXTRACT(EPOCH from w2.finished_time)-EXTRACT(EPOCH from w2.begin_time))/60 else 0 end,0)) as numeric)) as interval, " +
////            " CASE WHEN fc.fault_code IS NULL  THEN '' ELSE fc.fault_code END AS fault_code, " +
////            " CASE WHEN fc.code_name IS NULL  THEN '' ELSE fc.code_name END AS fault_code_name " +
//            " from ${schema_name}._sc_works w " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            " left join ${schema_name}._sc_work_template wt on wt.work_type_id=t.id and wt.template_type=5  " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " LEFT JOIN ${schema_name}._sc_asset_running_status ars ON w2.asset_running_status = ars.id " +
//            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_fault_code fc on fc.code=w2.repair_code " +
//            " left join ${schema_name}._sc_facilities ct on ct.isuse=true and ct.status > 0 and ct.id=dv.supplier " +
//            " left join ${schema_name}._sc_repair_type tt on w2.repair_type=tt.id " +
//            " left join ${schema_name}._sc_fault_type ff on w2.fault_type=ff.id " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            " left join ${schema_name}._sc_works_detail_asset_org wdao ON w2.sub_work_code = wdao.sub_work_code " +
//            " left join ${schema_name}._sc_facilities wdaof ON wdaof.id = wdao.facility_id  " +
//            " left JOIN ( " +
//            " SELECT t1.sub_work_code,ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t2.short_title ), ',' ) AS short_title " +
//            " FROM ${schema_name}._sc_works_detail_asset_org t1 left JOIN ${schema_name}._sc_facilities t2 ON t1.facility_id = t2.ID and t2.org_type<3 GROUP BY t1.sub_work_code " +
//            " ) wn ON wn.sub_work_code = w2.sub_work_code " +
//            " LEFT JOIN ( " +
//            " SELECT t1.sub_work_code,ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT tp2.short_title ), ',' ) AS short_title FROM ${schema_name}._sc_works_detail_asset_org t1 " +
//            " left JOIN ${schema_name}._sc_facilities t2 ON t1.facility_id = t2.ID and t2.org_type<3 " +
//            " left JOIN ${schema_name}._sc_facilities tp2 ON tp2.id = t2.parentid GROUP BY t1.sub_work_code " +
//            " ) wnp ON wnp.sub_work_code = w2.sub_work_code " +
//            " ${dySqlMap.feeTableSql} " +
//            " where 1=1 ${condition} " +
//            ") t ")
//    int getWorkSheetListCount(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("dySqlMap") Map<String, String> dySqlMap);
//
//    //查找维修单列表
//    @Select("select w2.sub_work_code  " +
//            "from ${schema_name}._sc_works w " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
//            " left join ${schema_name}._sc_facilities ft on w.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            "where 1=1 ${condition} " +
//            "order by w.occur_time desc,w.work_code desc limit ${page} offset ${begin}")
//    List<String> getWorkSheetSubWorkcodeList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    // LSP工单处理变更-20190529-sps start
//    //查找维修单列表
////    @Select("select distinct w.*,u.username as create_user_name,c.username as receive_name,s.status as status_name," +
////            "t.type_name as type_name,dv.strname as asset_name " +
////            ", w2.title as title,w2.priority_level,w2.receive_time,w2.distribute_time," +
////            "w2.receive_account,w2.sub_work_code,w2.begin_time,w2.finished_time,w2.audit_time," +
////            "dv.strcode as strcode,ct.short_title as customer_name,t.flow_template_code as flow_code," +
////            "t.business_type_id as business_type_id,ma.category_name as asset_type," +
////            "ff.type_name as fault_name,tt.type_name as repair_type_name,w2.result_note, ars.running_status, " +
////            " ${dySqlMap.feeSelectSql} " +
////            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN wn.short_title ELSE ap.position_name END AS facility_name, " +
////            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN wnp.short_title ELSE apt.position_name END AS parent_name, " +
////            " floor(cast((COALESCE(case when w2.status=" + StatusConstant.COMPLETED + " then (EXTRACT(EPOCH from w2.finished_time)-EXTRACT(EPOCH from w2.begin_time))/60 else 0 end,0)) as numeric)) as interval, " +
////            " CASE WHEN fc.fault_code IS NULL  THEN '' ELSE fc.fault_code END AS fault_code, " +
////            " CASE WHEN fc.code_name IS NULL  THEN '' ELSE fc.code_name END AS fault_code_name " +
////            " from ${schema_name}._sc_works w " +
////            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
////            " left join ${schema_name}._sc_status s on w.status=s.id " +
////            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
////            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
////            " LEFT JOIN ${schema_name}._sc_asset_running_status ars ON w2.asset_running_status = ars.id " +
////            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
////            " left join ${schema_name}._sc_fault_code fc on fc.code=w2.repair_code " +
////            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
////            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
////            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
////            " left join ${schema_name}._sc_facilities ct on ct.isuse=true and ct.status > 0 and ct.id=dv.supplier and ct.org_type = " + SqlConstant.FACILITY_SUPPLIER +
////            " left join ${schema_name}._sc_repair_type tt on w2.repair_type=tt.id " +
////            " left join ${schema_name}._sc_fault_type ff on w2.fault_type=ff.id " +
////            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
////            " left join ${schema_name}._sc_works_detail_asset_org wdao ON w2.sub_work_code = wdao.sub_work_code " +
////            " left join ${schema_name}._sc_facilities wdaof ON wdaof.id = wdao.facility_id " +
////            " left JOIN ( " +
////            " SELECT t1.sub_work_code,ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t2.short_title ), ',' ) AS short_title " +
////            " FROM ${schema_name}._sc_works_detail_asset_org t1 left JOIN ${schema_name}._sc_facilities t2 ON t1.facility_id = t2.ID and t2.org_type<3 GROUP BY t1.sub_work_code " +
////            " ) wn ON wn.sub_work_code = w2.sub_work_code " +
////            " LEFT JOIN ( " +
////            " SELECT t1.sub_work_code,ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT tp2.short_title ), ',' ) AS short_title FROM ${schema_name}._sc_works_detail_asset_org t1 " +
////            " left JOIN ${schema_name}._sc_facilities t2 ON t1.facility_id = t2.ID and t2.org_type<3 " +
////            " left JOIN ${schema_name}._sc_facilities tp2 ON tp2.id = t2.parentid GROUP BY t1.sub_work_code " +
////            " ) wnp ON wnp.sub_work_code = w2.sub_work_code " +
////            " ${dySqlMap.feeTableSql} " +
////            "where 1=1 ${condition} " +
////            " limit ${page} offset ${begin}")
//    @Select("SELECT pt.* ,ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t2.short_title ), ',' ) AS facility_name, ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT tp2.short_title ), ',' ) AS parent_name " +
//            "FROM (" +
//            "select distinct w.work_code,w.work_type_id,w.work_template_code,wt.work_template_code as update_code,w.pool_id," +
//            "w.facility_id,w.relation_type,w.relation_id,w.occur_time,w.deadline_time,w.status,w.remark,w.create_time," +
//            "w.create_user_account,w.problem_note,w.problem_img,w.plan_code,w.customer_id,w.position_code,w.work_request_code," +
//            "w.last_finished_time,u.username as create_user_name,c.username as receive_name," +
//            "s.status as status_name,t.type_name as type_name,dv.strname as asset_name,w2.priority_level,w2.receive_time," +
//            "w2.distribute_time,w2.receive_account,w2.sub_work_code,w2.begin_time," +
//            "w2.finished_time,w2.audit_time,dv.strcode as strcode,ct.short_title as customer_name,t.flow_template_code as flow_code," +
//            "t.business_type_id as business_type_id,ma.category_name as asset_type," +
//            "ff.type_name as fault_name,tt.type_name as repair_type_name,w2.result_note, ars.running_status, " +
//            "CASE WHEN w2.title is null OR w2.title = '' THEN w.title ELSE w2.title END AS title, " +
//            " ${dySqlMap.feeSelectSql} " +
//            " floor(cast((COALESCE(case when w2.status=" + StatusConstant.COMPLETED + " then (EXTRACT(EPOCH from w2.finished_time)-EXTRACT(EPOCH from w2.begin_time))/60 else 0 end,0)) as numeric)) as interval, " +
//            " CASE WHEN fc.fault_code IS NULL  THEN '' ELSE fc.fault_code END AS fault_code, " +
//            " CASE WHEN fc.code_name IS NULL  THEN '' ELSE fc.code_name END AS fault_code_name " +
//            " from ${schema_name}._sc_works w " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            " left join ${schema_name}._sc_work_template wt on wt.work_type_id=t.id and wt.template_type=5  " +
//            " left join ${schema_name}._sc_works_detail w2 on (w2.work_code=w.work_code and w2.is_main=1) " +
//            " LEFT JOIN ${schema_name}._sc_asset_running_status ars ON w2.asset_running_status = ars.id " +
//            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
//            " left join ${schema_name}._sc_fault_code fc on fc.code=w2.repair_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_facilities ct on ct.isuse=true and ct.status > 0 and ct.id=dv.supplier " +
//            " left join ${schema_name}._sc_repair_type tt on w2.repair_type=tt.id " +
//            " left join ${schema_name}._sc_fault_type ff on w2.fault_type=ff.id " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            " left join ${schema_name}._sc_works_detail_asset_org wdao ON w2.sub_work_code = wdao.sub_work_code " +
//            " left join ${schema_name}._sc_facilities wdaof ON wdaof.id = wdao.facility_id " +
//            " ${dySqlMap.feeTableSql} " +
//            "where 1=1 ${condition} " +
//            " limit ${page} offset ${begin}" +
//            ") pt " +
//            "LEFT JOIN ${schema_name}._sc_works_detail_asset_org t1 ON pt.sub_work_code = t1.sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_facilities t2 ON t1.facility_id = t2.ID AND t2.org_type < 3 " +
//            "LEFT JOIN ${schema_name}._sc_facilities tp2 ON tp2.ID = t2.parentid " +
//            "GROUP BY pt.work_code,pt.work_type_id,pt.work_template_code,pt.update_code,pt.pool_id,pt.facility_id,pt.relation_type,pt.relation_id,pt.occur_time,pt.deadline_time,pt.status,pt.remark," +
//            "pt.create_time,pt.create_user_account,pt.problem_note,pt.problem_img,pt.plan_code,pt.customer_id,pt.position_code,pt.work_request_code,pt.last_finished_time,pt.create_user_name," +
//            "pt.receive_name,pt.status_name,pt.type_name,pt.asset_name,pt.title,pt.priority_level,pt.receive_time,pt.distribute_time,pt.receive_account,pt.sub_work_code,pt.begin_time,pt.finished_time," +
//            "pt.audit_time,pt.strcode,pt.customer_name,pt.flow_code,pt.business_type_id,pt.asset_type,pt.fault_name,pt.repair_type_name,pt.result_note,pt.running_status,pt.position_code,pt.interval," +
//            "pt.fault_code,pt.fault_code_name ${dySqlMap.feeSelectSqlGroupColumn} ORDER BY pt.occur_time DESC,pt.work_code DESC ")
//    List<WorkSheet> getWorkSheetList(@Param("schema_name") String schema_name, @Param("condition") String condition,
//                                     @Param("dySqlMap") Map<String, String> dySqlMap, @Param("page") int pageSize,
//                                     @Param("begin") int begin);
//
//    //查找维修单列表
////    @Select("select w.*,u.username as create_user_name,c.username as receive_name,s.status as status_name," +
////            "t.type_name as type_name,dv.strname as asset_name " +
////            ",ft.title as facility_name,pf.title as parent_name, w2.title as title,w2.priority_level,w2.receive_time,w2.sub_work_code,t.flow_template_code as flow_code,t.business_type_id as business_type_id" +
////            " from ${schema_name}._sc_works w " +
////            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
////            " left join ${schema_name}._sc_status s on w.status=s.id " +
////            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
////            " left join ${schema_name}._sc_works_detail w2 on w2.work_code=w.work_code " +
////            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
////            " left join ${schema_name}._sc_facilities ft on w.facility_id=ft.id " +
////            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
////            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
////            "where w2.is_main=1 ${condition} " +
////            "group by w.work_code, u.username,c.username,s.status,t.type_name,ft.title,pf.title,w2.title,dv.strname,w2.priority_level,w2.receive_time,w2.sub_work_code,t.flow_template_code,t.business_type_id ")
//    @Select("select w.*,u.username as create_user_name,c.username as receive_name,s.status as status_name," +
//            "t.type_name as type_name,dv.strname as asset_name,dv.strcode, " +
//            "w2.title as title,w2.priority_level,w2.receive_time,w2.sub_work_code,w2.finished_time," +
//            "t.flow_template_code as flow_code,t.business_type_id as business_type_id," +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN wdaof.short_title ELSE ap.position_name END AS facility_name, " +
//            " CASE WHEN w.position_code IS NULL or '' = w.position_code THEN pf.short_title ELSE apt.position_name END AS parent_name " +
//            " from ${schema_name}._sc_works w " +
//            " left join ${schema_name}._sc_user u on w.create_user_account=u.account " +
//            " left join ${schema_name}._sc_status s on w.status=s.id " +
//            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            " left join ${schema_name}._sc_works_detail w2 on w2.work_code=w.work_code " +
//            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
//            " LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
//            " LEFT JOIN ${schema_name}._sc_asset_position apt ON ap.parent_code = apt.position_code " +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
////            " INNER join ${schema_name}._sc_works_detail_asset_org wdao ON w2.sub_work_code = wdao.sub_work_code and w2.relation_id = wdao.asset_id " + // 点巡检无法使用
//            " left join ${schema_name}._sc_works_detail_asset_org wdao ON w2.sub_work_code = wdao.sub_work_code " +
//            " left join ${schema_name}._sc_facilities wdaof ON wdaof.id = wdao.facility_id and wdaof.org_type<3 " +
//            " left join ${schema_name}._sc_facilities pf on wdaof.parentid=pf.id " +
//            "where w2.is_main=1 ${condition} " +
////            "group by w.work_code, u.username,c.username,s.status,t.type_name,ft.short_title,pf.short_title,ap.position_name,apt.position_name,w2.title,dv.strname,w2.priority_level,w2.receive_time,w2.sub_work_code,w2.finished_time,t.flow_template_code,t.business_type_id " +
//            "")
//    List<WorkSheet> getWorkSheetListforcalendar(@Param("schema_name") String schema_name, @Param("condition") String condition);
//    // LSP工单处理变更-20190529-sps end
//
//    //按照workcode查询工单概要
////    @Select("select w.*,w2.title as title,t.type_name as type_name,dv.strname as asset_name,c.username as receive_name,w2.receive_account as receive_account,w2.sub_work_code as sub_work_code,ft.title as facility_name from ${schema_name}._sc_works w " +
////            " left join " +
////            "(SELECT * FROM ${schema_name}._sc_works_detail x1 WHERE NOT EXISTS" +
////            "(SELECT * FROM ${schema_name}._sc_works_detail x2 WHERE x2.work_code=x1.work_code AND x1.sub_work_code < x2.sub_work_code)" +
////            ") w2 on w2.work_code=w.work_code " +
////            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.business_type_id " +
////            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
////            " left join ${schema_name}._sc_facilities ft on w.facility_id=ft.id " +
////            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
////            " left join ${schema_name}._sc_user c on w2.receive_account=c.account " +
////            "where w.work_code=#{work_code}")
////    WorkSheet findByID(@Param("schema_name") String schema_name, @Param("work_code") String work_code);
//
//    // LSP工单处理变更-20190521-sps start
//    //按照workcode查询工单详情
////    @Select("select w.*,w2.facility_id as facility_id,w2.pool_id as pool_id,t.business_type_id as business_type_id,t.flow_template_code as flow_code from " +
////            "${schema_name}._sc_works_detail w " +
////            "left join ${schema_name}._sc_works w2 on w.work_code=w2.work_code " +
////            " left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
////            "where w.sub_work_code=#{sub_work_code}")
//    @Select("select w.*,w2.pool_id as pool_id,t.business_type_id as business_type_id,t.flow_template_code as flow_code,a.strname as asset_name, " +
//            " CASE WHEN w2.position_code IS NULL or '' = w2.position_code THEN ltrim(to_char(w2.facility_id, '" + SqlConstant.FACILITY_ID_LEN + "')) ELSE w2.position_code END AS facility_id " +
//            "from ${schema_name}._sc_works_detail w " +
//            "left join ${schema_name}._sc_works w2 on w.work_code=w2.work_code " +
//            "left join ${schema_name}._sc_work_type t on w.work_type_id=t.id " +
//            "left join ${schema_name}._sc_asset a on w.relation_id=a._id " +
//            "where w.sub_work_code=#{sub_work_code}")
//    WorkListDetailModel finddetilByID(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
//    // LSP工单处理变更-20190521-sps end
//
//    @Select("select wd.*,t.type_name,ac.category_name,a.strcode " +
//            "from ${schema_name}._sc_works_detail wd " +
//            "left join ${schema_name}._sc_work_type t on wd.work_type_id=t.id " +
//            "left join ${schema_name}._sc_asset a on a._id = wd.relation_id " +
//            "left join ${schema_name}._sc_asset_category ac on ac.id = a.category_id " +
//            "where wd.relation_type = " + SensConstant.RELATION_TYPE_ASSET + " and wd.sub_work_code=#{sub_work_code}")
//    Map<String, Object> findWorkSheetDetailBySubWorkCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
//
//    //修改工单概要
//    @Update("update ${schema_name}._sc_works set " +
//            "work_type_id=#{w.work_type_id}, " +
//            "work_template_code=#{w.work_template_code}, " +
//            "parent_code=#{w.parent_code}, " +
//            "pool_id=#{w.pool_id}, " +
//            "facility_id=#{w.facility_id}, " +
//            "relation_type=#{w.relation_type}, " +
//            "relation_id=#{w.relation_id}, " +
//            "occur_time=#{w.occur_time}, " +
//            "deadline_time=#{w.deadline_time}, " +
//            "status=#{w.status}, " +
//            "remark=#{w.remark}, " +
//            "create_time=#{w.create_time}, " +
//            "create_user_account=#{w.create_user_account} " +
//            "where work_code=#{w.work_code} ")
//    int updateworkList(@Param("schema_name") String schema_name, @Param("w") WorkListModel workListModel);
//
//    //修改工单概要
//    @Update("update ${schema_name}._sc_works set " +
//            "work_type_id=#{w.work_type_id}, " +
//            "work_template_code=#{w.work_template_code}, " +
//            "parent_code=#{w.parent_code}, " +
//            "pool_id=#{w.pool_id}, " +
//            "facility_id=#{w.facility_id}, " +
//            "relation_type=#{w.relation_type}, " +
//            "relation_id=#{w.relation_id}, " +
//            "occur_time=#{w.occur_time}, " +
//            "deadline_time=#{w.deadline_time}, " +
//            //"fault_img=#{r.fault_img}, " +
//            "status=#{w.status}, " +
//            "remark=#{w.remark}, " +
//            "create_time=#{w.create_time}, " +
//            "create_user_account=#{w.create_user_account} " +
//            "where work_code=#{w.work_code} ")
//    int updateworkSheet(@Param("schema_name") String schema_name, @Param("w") WorkSheet worksheet);
//
//    //修改工单详情
//    @Update("update ${schema_name}._sc_works_detail set " +
//            "sub_work_code=#{w.sub_work_code}, " +
//            "work_code=#{w.work_code}, " +
//            "work_type_id=#{w.work_type_id}, " +
//            "work_template_code=#{w.work_template_code}, " +
//            "relation_type=#{w.relation_type}, " +
//            "relation_id=#{w.relation_id}, " +
//            "title=#{w.title}, " +
//            "problem_note=#{w.problem_note}, " +
//            "problem_img=#{w.problem_img}, " +
//            //"fault_img=#{r.fault_img}, " +
//            "asset_running_status=#{w.asset_running_status}, " +
//            "repair_type=#{w.repair_type}, " +
//            "priority_level=#{w.priority_level}, " +
//            "status=#{w.status}," +
//            "remark=#{w.remark}, " +
//            "from_code=#{w.from_code}, " +
//            "waiting=#{w.waiting}," +
//            "bom_app_result=#{w.bom_app_result}, " +
//            "receive_account=#{w.receive_account}," +
//            "receive_time=#{w.receive_time} " +
//            "where sub_work_code=#{w.sub_work_code} ")
//    int updateWorkDetailList(@Param("schema_name") String schema_name, @Param("w") WorkListDetailModel workListDetailModel);
//
////    @Select("select wd.status,wd.sub_work_code,wd.title,wd.work_type_id,t.flow_template_code as flow_code ," +
////            "s.status as status_name,c.username as receive_name, wd.begin_time, wd.finished_time, " +
////            "to_char(wd.finished_time-wd.begin_time, 'ss') as finished_time_interval " +
////            "from ${schema_name}._sc_works_detail wd  " +
////            "left join ${schema_name}._sc_work_type t on wd.work_type_id=t.id " +
////            " left join ${schema_name}._sc_status s on wd.status=s.id " +
////            " left join ${schema_name}._sc_user c on wd.receive_account=c.account " +
////            "where wd.is_main=-1 and wd.work_code=#{work_code}")
////    List<WorkListDetailModel> getSubWorkList(@Param("schema_name") String schema_name, @Param("work_code") String work_code);
//
//    @Select("select wd.status,wd.sub_work_code,wd.title,wd.work_type_id,t.flow_template_code as flow_code ," +
//            "s.status as status_name,c.username as receive_name, to_char(wd.begin_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as begin_time, " +
//            "to_char(wd.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as finished_time, " +
//            "to_char(wd.finished_time-wd.begin_time, 'mm') as finished_time_interval " +
//            "from ${schema_name}._sc_works_detail wd  " +
//            "left join ${schema_name}._sc_work_type t on wd.work_type_id=t.id " +
//            " left join ${schema_name}._sc_status s on wd.status=s.id " +
//            " left join ${schema_name}._sc_user c on wd.receive_account=c.account " +
//            "where wd.is_main=-1 and wd.work_code=#{work_code}")
//    List<Map<String, Object>> getSubWorkMapList(@Param("schema_name") String schema_name, @Param("work_code") String work_code);
//
//    //查找工单类型，及其工单总数
//    @Select("select wt.id,wt.type_name,wt.business_type_id,count(distinct w.work_code) as workBillCount from " +
//            "${schema_name}._sc_work_type wt " +
//            "left join ${schema_name}._sc_works w on wt.id=w.work_type_id " +
//            "left join ${schema_name}._sc_works_detail w2 on w2.work_code=w.work_code " +
//            "left join ${schema_name}._sc_works_detail_asset_org wdao ON w2.sub_work_code = wdao.sub_work_code " +
//            "left join ${schema_name}._sc_facilities wdaof ON wdaof.id = wdao.facility_id" +
//            " left join ${schema_name}._sc_asset dv on w.relation_id=dv._id " +
//            " left join ${schema_name}._sc_asset_category ma on dv.category_id=ma.id " +
//            "where wt.business_type_id<10 and w.work_code is not null ${condition}" +
//            "group by wt.id,wt.type_name,wt.business_type_id " +
//            "order by wt.business_type_id  ")
//    List<WorkTypeData> getWorkTypeAndBillCountList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    @Select("select count(w.work_code) from ${schema_name}._sc_works w " +
//            "left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            "where status in (20,40,50) and t.business_type_id=1 and w.relation_id=#{asset_id} and (EXTRACT(EPOCH from '${current}'::timestamp)-EXTRACT(EPOCH from w.create_time))<3600")
//    int getRepairCountByAssetIdRecently(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("current") Timestamp current);
//
//    @Select("select count(w.work_request_code) from ${schema_name}._sc_work_request w " +
//            "left join ${schema_name}._sc_work_type t on w.work_type_id=t.id  " +
//            "where status in (15,20,40) and t.business_type_id=1 and w.relation_id=#{asset_id} and (EXTRACT(EPOCH from '${current}'::timestamp)-EXTRACT(EPOCH from w.create_time))<3600")
//    int getRepairServiceCountByAssetIdRecently(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("current") Timestamp current);
//
//    /**
//     * 查询工单下的任务项列表数据
//     *
//     * @param schemaName
//     * @param sub_work_code
//     * @return
//     */
//    @Select("SELECT j ->> 'taskContent' taskContent " +
//            "FROM ( " +
//            "   SELECT json_array_elements(j) j " +
//            "   FROM ( " +
//            "       SELECT array_to_json (ARRAY_AGG(j)) j " +
//            "       FROM ( " +
//            "           SELECT jsonb_array_elements(body_property) j FROM ${schema_name}._sc_works_detail A " +
//            "           WHERE A.sub_work_code = #{sub_work_code} " +
//            "       ) T " +
//            "       WHERE " +
//            "       j ->> 'fieldViewType' = '9' " +
//            "   ) T  " +
//            ") T ")
//    String getWorkSheetTaskContent(@Param("schema_name") String schemaName, @Param("sub_work_code") String sub_work_code);
//
//    /**
//     * 根据任务模板名称查询任务模板、任务项数据
//     *
//     * @param schema_name
//     * @param templateNames
//     * @return
//     */
//    @Select("SELECT tti.*,tt.task_template_name FROM ${schema_name}._sc_task_template tt " +
//            "LEFT JOIN ${schema_name}._sc_task_template_item tti ON tti.task_template_code = tt.task_template_code " +
//            "WHERE tt.task_template_name IN (${templateNames})")
//    List<Map<String, Object>> getTaskTemplateAndTaskItemByTemplateName(@Param("schema_name") String schema_name, @Param("templateNames") String templateNames);
//
//    /**
//     * 查询工单事前、事后照片
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @return
//     */
//    @Select("SELECT before_img,after_img FROM ${schema_name}._sc_works_detail WHERE sub_work_code=#{sub_work_code}")
//    Map<String, Object> findWorkSheetImgBySubWorkCode(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
}