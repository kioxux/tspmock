package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SettlementMapper {
//    @InsertProvider(type = SettlementMapper.SettlementMapperProvider.class, method = "insert")
//    void insert(@Param("schema_name") String schema_name, @Param("s") Settlement settlement);
//
//    @DeleteProvider(type = SettlementMapper.SettlementMapperProvider.class, method = "delete")
//    void delete(@Param("schema_name") String schema_name, @Param("id") String id);
//
//    @SelectProvider(type = SettlementMapper.SettlementMapperProvider.class, method = "find")
//    @Results({@Result(property = "options", column = "options", typeHandler = JsonTypeHandler.class),
//            @Result(property = "table_columns", column = "table_columns", typeHandler = JsonTypeHandler.class),
//            @Result(property = "query_conditions", column = "query_conditions", typeHandler = JsonTypeHandler.class)})
//    List<Settlement> find(@Param("schema_name") String schema_name,
//                          @Param("title") String title,
//                          @Param("id") String id, String loginUser);
//
//    @SelectProvider(type = SettlementMapper.SettlementMapperProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schema_name,
//                                    @Param("query") String query);
//
//    // 审核
//    @Update("update ${schema_name}._demo_settlement set " +
//            "status = #{r.status}, " +
//            "audit_desc = #{r.audit_desc}, " +
//            "audit_time = current_timestamp " +
//            "where id = #{r.id} ")
//    int audit(@Param("schema_name") String schema_name, @Param("r") Settlement settlement);
//
//    @InsertProvider(type = SettlementMapper.SettlementMapperProvider.class, method = "insertSubBatch")
//    void insertSubBatch(@Param("schema_name") String schema_name, Settlement settlement) ;
//
//    @SelectProvider(type = SettlementMapper.SettlementMapperProvider.class, method = "findSubList")
//    List<SettlementItem> findSubList(@Param("schema_name") String schema_name, @Param("id") String id);
//
//    class SettlementMapperProvider {
//        public String insert(@Param("schema_name") String schema_name, @Param("s") Settlement settlement) {
//            return new SQL() {{
//                INSERT_INTO(schema_name + "._demo_settlement");
//                VALUES("id", "#{s.id}");
//                VALUES("supplier", "#{s.supplier}");
//                VALUES("total_amount", "#{s.total_amount}");
//                VALUES("settlement_date", "TO_TIMESTAMP(#{s.settlement_date}, '" + SqlConstant.SQL_DATE_TIME_FMT + "')");
//                VALUES("file_ids", "#{s.file_ids}");
//                VALUES("remark", "#{s.remark}");
//                VALUES("auditor", "#{s.auditor}");
//                VALUES("status", "#{s.status}");
//                VALUES("audit_desc", "#{s.audit_desc}");
//                VALUES("audit_time", "#{s.audit_time}");
//                VALUES("create_time", "current_timestamp");
//                VALUES("create_user_account", "#{s.create_user_account}");
//            }}.toString();
//        }
//
//        public String find(@Param("schema_name") String schema_name,
//                           @Param("title") String title,
//                           @Param("id") String id, String loginUser) {
//            return new SQL() {{
//                SELECT(" *, u.username as auditor_name, us.username as create_user_account_name, c.customer_name ");
//                FROM(schema_name + "._demo_settlement t left join " + schema_name
//                        + "._sc_user u on t.auditor = u.account left join " + schema_name
//                        + "._sc_user us on t.create_user_account = us.account left join " + schema_name
//                        + "._sc_customers c on t.supplier = c.id and isuse = true");
//                if (StringUtils.isNotEmpty(title)) {
//                    WHERE("u.title like CONCAT('%', #{title}, '%')");
//                }
//                if (id != null) {
//                    WHERE("t.id = #{id}");
//                }
//
//                if (loginUser != null) {
//                    WHERE("t.create_user_account = '" + loginUser + "' or t.auditor = '" + loginUser + "'");
//                }
//            }}.toString();
//        }
//
//        public String query(@Param("schema_name") String schema_name,
//                            @Param("query") String query) {
//            return query.replaceAll("\\$\\{schema_name}", schema_name);
//        }
//
//        public String delete(@Param("schema_name") String schema_name, @Param("id") String id) {
//            return new SQL() {{
//                DELETE_FROM(schema_name + "._demo_settlement");
//                WHERE("id = #{id}");
//            }}.toString();
//        }
//
//        public String insertSubBatch(@Param("schema_name") String schema_name, Settlement settlement) {
//            return new SQL() {{
//                // 子表数据
//                List<SettlementItem> subList =  settlement.getSubList();
//                String id = settlement.getId();
//                StringBuffer sb = new StringBuffer(schema_name + "._demo_settlement_item (settlement_item_id, work_type, amount, settlement_id) values ");
//                for (SettlementItem si : subList) {
//                    sb.append("('");
//                    sb.append(UUID.randomUUID().toString());
//                    sb.append("','");
//                    sb.append(si.getWork_type());
//                    sb.append("','");
//                    sb.append( si.getAmount().toString());
//                    sb.append("','");
//                    sb.append( id);
//                    sb.append("'),");
//                }
//                INSERT_INTO(sb.substring(0, sb.length() - 1).toString());
//            }}.toString();
//        }
//
//        public String findSubList(@Param("schema_name") String schema_name, @Param("id") String id) {
//            return new SQL() {{
//                SELECT(" * ");
//                FROM(schema_name + "._demo_settlement_item t");
//                if (id != null) {
//                    WHERE("t.settlement_id = #{id}");
//                }
//            }}.toString();
//        }
//    }
}
