package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.model.PlanWorkSettingModel;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 功能：计划工单Mapper sql
 * Created by Dong wudang on 2018/11/24.
 */
public interface PlanWorkMapper {

    /**
     * 删除维保计划配置
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("DELETE FROM ${schema_name}._sc_plan_work_setting WHERE plan_code = #{plan_code}")
    int deletePlanWorkSetting(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除巡检点对象
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from  ${schema_name}._sc_plan_work_inspection where plan_code=#{plan_code}")
    int deletePlanWorkInspection(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划设备对象
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from  ${schema_name}._sc_plan_work_asset where plan_code=#{plan_code}")
    int deletePlanWorkAsset(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划位置
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from ${schema_name}._sc_plan_work_facility_position where plan_code = #{plan_code}")
    int deletePlanWorkFacilityPosition(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划设备类型
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from ${schema_name}._sc_plan_work_asset_type where plan_code = #{plan_code}")
    int deletePlanWorkAssetType(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划设备型号
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from ${schema_name}._sc_plan_work_asset_model where plan_code = #{plan_code}")
    int deletePlanWorkAssetModel(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划触发条件
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from ${schema_name}._sc_plan_work_triggle where plan_code = #{plan_code}")
    int deletePlanWorkTriggle(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划执行人
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from ${schema_name}._sc_plan_work_executor where plan_code = #{plan_code}")
    int deletePlanWorkExecutor(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 删除计划自定义配置信息
     *
     * @param schema_name 数据库信息
     * @param plan_code   计划主键
     * @return 删除成功数量
     */
    @Delete("DELETE FROM ${schema_name}._sc_plan_work_property WHERE plan_code = #{plan_code}")
    int deletePlanJson(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);


    /**
     * 删除计划协助团队
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Delete("delete from ${schema_name}._sc_plan_work_assist_team where plan_code = #{plan_code}")
    int deletePlanWorkAssistOrg(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 更新维修计划
     */
    @Update("<script> UPDATE ${schema_name}._sc_plan_work   " +
            "<set>  " +
            "<if test=\"plan_name !=null and plan_name !=''\">  " +
            "plan_name = #{plan_name},  " +
            "</if>  " +
            "<if test=\"work_type_id!=null\">  " +
            "work_type_id = #{work_type_id}::int,  " +
            "</if>  " +
            "<if test=\"pool_id!=null and pool_id !=''\">  " +
            "pool_id = #{pool_id}::int,  " +
            "</if>  " +
            "<if test=\"facility_id!=null and facility_id !=''\">  " +
            "facility_id = #{facility_id}::int,  " +
            "</if>  " +
            "<if test=\"relation_type!=null and relation_type !=''\">  " +
            "relation_type = #{relation_type}::int,  " +
            "</if>  " +
            "<if test=\"plan_hour!=null and plan_hour !=''\">  " +
            "plan_hour = #{plan_hour}::numeric,  " +
            "</if>  " +
            "<if test=\"do_type!=null and do_type !=''\">  " +
            "do_type = #{do_type}::int,  " +
            "</if>  " +
            "<if test=\"period!=null and period !=''\">  " +
            "period = #{period}::float,  " +
            "</if>  " +
            "<if test=\"begin_time!=null and begin_time !=''\">  " +
            "begin_time = #{begin_time}::timestamp,  " +
            "</if>  " +
            "<if test=\"end_time!=null and end_time !=''\">  " +
            "end_time = #{end_time}::timestamp,  " +
            "</if>  " +
            "<if test=\"remark!=null and remark !=''\">  " +
            "remark = #{remark},  " +
            "</if>  " +
            "<if test=\"is_use!=null and is_use !=''\">  " +
            "is_use = #{is_use},  " +
            "</if>  " +
            "<if test=\"status!=null\">  " +
            "status = #{status},  " +
            "</if>  " +
            "<if test=\"plan_type!=null and plan_type !=''\">  " +
            "plan_type = #{plan_type}::int,  " +
            "</if>  " +
            "<if test=\"period_type !=null and period_type !=''\">  " +
            "period_type = #{period_type}::int,  " +
            "</if>  " +
            "<if test=\"allocate_receiver_type!=null and allocate_receiver_type !=''\">  " +
            "allocate_receiver_type = #{allocate_receiver_type}::int,  " +
            "</if>  " +
            "<if test=\"generate_bill_type!=null and generate_bill_type !=''\">  " +
            "generate_bill_type = #{generate_bill_type}::int,  " +
            "</if>  " +
            "<if test=\"generate_time_point!=null and generate_time_point !=''\">  " +
            "generate_time_point = #{generate_time_point}::int,  " +
            "</if>  " +
            "<if test=\"auto_generate_bill!=null\">  " +
            "auto_generate_bill = #{auto_generate_bill},  " +
            "</if>  " +
            "<if test=\"create_time!=null \">  " +
            "create_time = #{create_time},  " +
            "</if>  " +
            "<if test=\"create_user_id!=null and create_user_id !=''\">  " +
            "create_user_id = #{create_user_id},  " +
            "</if>  " +
            "<if test=\"pre_day!=null and pre_day !=''\">  " +
            "pre_day = #{pre_day}::int,  " +
            "</if>  " +
            "<if test=\"job_status!=null \">  " +
            "job_status = #{job_status} ,  " +
            "</if>  " +
            "</set>  " +
            "WHERE plan_code = #{plan_code} </script>")
    int updatePlanSchedule(Map<String, Object> data);

    /**
     * 新增维修计划
     *
     * @param data
     * @return
     */
    @Insert("insert into ${schema_name}._sc_plan_work " +
            "(plan_code,plan_name,work_type_id,relation_type,plan_hour," +
            "do_type, begin_time,end_time,pre_day,is_use,remark,create_time," +
            "create_user_id,status,allocate_receiver_type,generate_bill_type,generate_time_point,priority_level,job_status,auto_generate_bill,work_template_code) " +
            " values (#{plan_code},#{plan_name},#{work_type_id}::int,#{relation_type}::int,#{plan_hour}, " +
            " #{do_type}::int, #{begin_time}::timestamp,#{end_time}::timestamp,#{pre_day}::int,#{is_use},#{remark},#{create_time}, " +
            " #{create_user_id},#{status}::int,#{allocate_receiver_type}::int,#{generate_bill_type}::int,#{generate_time_point}::int,#{priority_level}::int,#{job_status},#{auto_generate_bill},#{work_template_code})")
    int insertPlanSchedule(Map<String, Object> data);

    /**
     * 新增计划触发条件
     *
     * @param data
     * @return
     */
    @Insert("insert into ${schema_name}._sc_plan_work_triggle (plan_code, period_type, period_value, execute_time, execute_day, type_day_select, which_type_day, type_week_select, duration_time, duration_type,column_key,column_condition,column_value) values" +
            " (#{plan_code},#{period_type},#{period_value},#{execute_time}::time,#{execute_day},#{type_day_select},#{which_type_day},#{type_week_select},#{duration_time}, #{duration_type}, #{column_key}, #{column_condition}, #{column_value})")
    int insertPlanWorkTriggle(Map<String, Object> data);
//    int insertPlanWorkTriggle(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("triggle_type") int triggle_type, @Param("plan_type") int plan_type,
//                           @Param("period_type") int period_type, @Param("period_value") float period_value, @Param("execute_time") String execute_time,
//                           @Param("column_key") String column_key, @Param("column_condition") String column_condition, @Param("column_value") String column_value,
//                           @Param("begin_time") Date begin_time, @Param("end_time") Date end_time);

    /**
     * 新增维保计划配置
     *
     * @param planWorkSetting
     * @return
     */
    @Insert("insert into ${schema_name}._sc_plan_work_setting (plan_code) values (#{plan_code})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    long insertPlanWorkSetting(PlanWorkSettingModel planWorkSetting);

    /**
     * 新增计划位置
     */
    @Insert("insert into ${schema_name}._sc_plan_work_facility_position " +
            "(plan_code, setting_id, facility_id, position_code, is_all_asset, enable_begin_date, enable_end_date) " +
            " values (#{plan_code},#{setting_id},#{facility_id},#{position_code},#{is_all_asset},#{enable_begin_date}::timestamp,#{enable_end_date}::timestamp)")
    int insertPlanWorkFacilityPosition(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("setting_id") long setting_id,
                                       @Param("facility_id") int facility_id, @Param("position_code") String position_code, @Param("is_all_asset") boolean is_all_asset,
                                       @Param("enable_begin_date") String enable_begin_date, @Param("enable_end_date") String enable_end_date);

    /**
     * 新增计划设备类型
     */
    @Insert("insert into ${schema_name}._sc_plan_work_asset_type (plan_code, setting_id, asset_category_id) values (#{plan_code},#{setting_id},#{asset_category_id})")
    int insertPlanWorkAssetType(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("setting_id") long setting_id, @Param("asset_category_id") int asset_category_id);

    /**
     * 新增计划设备型号
     */
    @Insert("insert into ${schema_name}._sc_plan_work_asset_model (plan_code, setting_id, asset_model_id) values (#{plan_code},#{setting_id},#{asset_model_id})")
    int insertPlanWorkAssetModel(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("setting_id") long setting_id, @Param("asset_model_id") long asset_model_id);

    /**
     * 计划排程设备对象
     */
    @Insert("insert into ${schema_name}._sc_plan_work_asset (plan_code,setting_id,asset_id) " +
            " values (#{plan_code},#{setting_id},#{asset_id})")
    int insertPlanWorkAsset(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("setting_id") long setting_id, @Param("asset_id") String asset_id);


    /**
     * 计划排程定义巡检对象
     *
     * @param schema_name
     * @param plan_code
     * @param setting_id
     * @param inspection_code
     * @return
     */
    @Insert("insert into ${schema_name}._sc_plan_work_inspection (plan_code,setting_id,inspection_code) " +
            " values (#{plan_code},#{setting_id},#{inspection_code})")
    int insertPlanWorkInspection(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("setting_id") long setting_id, @Param("inspection_code") String inspection_code);

    /**
     * 查询维修计划根据计划维修编号
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select p.plan_code,p.plan_name,p.work_type_id,p.pool_id,p.facility_id,p.relation_type,p.plan_hour,p.do_type,p.period,to_char(p.begin_time, 'yyyy-MM-dd HH24:mi:ss') as begin_time,to_char(p.end_time, 'yyyy-MM-dd HH24:mi:ss') as end_time, " +
            "p.pre_day,p.is_use,p.remark,p.status,to_char(p.create_time, 'yyyy-MM-dd HH24:mi:ss') as create_time,p.create_user_id,p.plan_type,p.period_type,p.allocate_receiver_type,p.generate_bill_type, " +
            "p.priority_level,t.type_name as work_type_name,c1.name as relation_type_name,p.job_status,p.auto_generate_bill::varchar,p.generate_time_point " +
            "from ${schema_name}._sc_plan_work p " +
            "LEFT JOIN ${schema_name}._sc_work_type t ON p.work_type_id = t.id  " +
            "LEFT JOIN ${schema_name}._sc_cloud_data c1 ON p.relation_type::text= c1.code AND c1.data_type = 'relationTypes'  " +
            "where p.plan_code = #{plan_code}")
    Map<String, Object> findPlanScheduleInfoByCode(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 根据计划编码，查询计划配置信息
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("Select * from ${schema_name}._sc_plan_work_setting where plan_code=#{plan_code}")
    List<Map<String, Object>> findPlanWorkSettingByPlanCode(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 计划排程设备对象
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select p.*,a.asset_name from ${schema_name}._sc_plan_work_asset p LEFT JOIN ${schema_name}._sc_asset a on a.id = p.asset_id where p.plan_code=#{plan_code}")
    List<Map<String, Object>> findPlanWorkAsset(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 根据计划code查询位置信息
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select p.plan_code,p.setting_id,p.facility_id,p.position_code,p.is_all_asset,to_char(p.enable_begin_date, 'yyyy-MM-dd HH24:mi:ss') as enable_begin_date," +
            "to_char(p.enable_end_date, 'yyyy-MM-dd HH24:mi:ss') as enable_end_date," +
            "f.short_title as facility_name,f.facility_no,ap.position_name from ${schema_name}._sc_plan_work_facility_position p " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON ap.position_code = p.position_code " +
            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = p.facility_id where plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkFacilityPositionByPlanCode(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 根据计划code查询计划设备类型
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select t.*,c.category_name as asset_category_name from ${schema_name}._sc_plan_work_asset_type t " +
            "LEFT JOIN ${schema_name}._sc_asset_category c ON c.id = t.asset_category_id where plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkAssetTypeByPlanCode(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 根据计划code查询计划设备型号
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select p.*,m.model_name as asset_model_name from ${schema_name}._sc_plan_work_asset_model p " +
            "LEFT JOIN ${schema_name}._sc_asset_model m on m.id = p.asset_model_id where plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkAssetModelByPlanCode(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 根据计划code查询日期的触发条件
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Select("select plan_code, period_type, period_value, execute_time, execute_day, type_day_select, which_type_day, type_week_select,  " +
            "duration_time, duration_type, extract(h from execute_time) as execute_hour,extract(minute from execute_time) as execute_minute " +
            "from ${schema_name}._sc_plan_work_triggle a   " +
            "where a.period_type > 0 and a.plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkTriggleByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code);

    /**
     * 根据计划code查询点巡检点象信息
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select i.*,p.position_name as inspection_name from ${schema_name}._sc_plan_work_inspection i LEFT JOIN ${schema_name}._sc_asset_position p on i.inspection_code = p.position_code where i.plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkInspectionByPlanCode(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);

    /**
     * 查询计划执行人列表
     *
     * @param schema_name
     * @param plan_code
     * @return
     */
    @Select("select e.id,e.user_id,e.plan_code,u.account,u.user_name as executor_name,u.status " +
            "from ${schema_name}._sc_plan_work_executor as e " +
            "left join ${schema_name}._sc_user as u on e.user_id=u.id " +
            "where plan_code =#{plan_code}")
    List<Map<String, Object>> findPlanWorkExecutor(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code);


    /**
     * 查询计划协助团队列表
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Select("select t.id,t.group_id,t.team_size,t.plan_code,f.group_name as assist_org_name from ${schema_name}._sc_plan_work_assist_team as t left join ${schema_name}._sc_group as f on t.group_id = f.id where plan_code =#{plan_code}")
    List<Map<String, Object>> findPlanWorkAssistOrgByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code);

    /**
     * 查询计划自定义配置
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Select("select plan_code,body_property::text from ${schema_name}._sc_plan_work_property where plan_code = #{plan_code} ")
    Map<String, Object> findPlanWorkPropertyByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code);

    /**
     * 添加计划自定义配置信息
     *
     * @param schema_name   数据库信息
     * @param plan_code     计划主键
     * @param body_property 自定义配置信息
     * @return 新增成功数量
     */
    @Insert("INSERT INTO ${schema_name}._sc_plan_work_property (plan_code, body_property) VALUES (#{plan_code}, #{body_property}::jsonb)")
    int insertPlanJson(@Param("schema_name") String schema_name, @Param("plan_code") String plan_code, @Param("body_property") String body_property);

    /**
     * 查询维保计划列表总条数
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECT count(DISTINCT P.plan_code) " +
            "FROM ${schema_name}._sc_plan_work P " +
            "LEFT JOIN ${schema_name}._sc_work_type T ON P.work_type_id = T.ID " +
            "LEFT JOIN ${schema_name}._sc_plan_work_setting ps ON ps.plan_code = p.plan_code " +
            "LEFT JOIN ${schema_name}._sc_plan_work_facility_position pfp ON pfp.setting_id = ps.id " +
            "${condition}")
    int findCountPlanScheduleList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);

    /**
     * 查询维保计划列表
     *
     * @param schemaName
     * @param searchWord
     * @return
     */
    @Select("SELECt DIStINCt p.plan_code,p.plan_name,p.work_type_id,p.facility_id,p.relation_type,p.plan_hour,p.do_type,p.period,\n" +
            "to_char(p.begin_time, 'yyyy-MM-dd') as begin_time,to_char(p.end_time, 'yyyy-MM-dd') as end_time,\n" +
            "p.pre_day,p.is_use,p.remark,p.status,p.create_time,p.create_user_id,p.plan_type,p.period_type,p.allocate_receiver_type,p.generate_bill_type,\n" +
            "p.priority_level,p.job_status,t.type_name AS work_type_name,u.user_name as executor, " +
            "(SELECt array_to_string(array_agg(DIStINCt (CASE pfp.facility_id WHEN 0 tHEN ap.position_name ELSE f.short_title END) ),',') as facility_name " +
            "FROM ${schema_name}._sc_plan_work_setting ps LEFt JOIN ${schema_name}._sc_plan_work_facility_position pfp ON ps.id = pfp.setting_id  " +
            "LEFt JOIN ${schema_name}._sc_facilities f ON pfp.facility_id = f.id  " +
            "LEFt JOIN ${schema_name}._sc_asset_position ap ON pfp.position_code = ap.position_code WHERE p.plan_code = ps.plan_code ) AS facility_name,  " +
            "(SELECt array_to_string(array_agg(DIStINCt(CASE WHEN pfp.position_code = '' tHEN '' ELSE ap.position_name END) ),',') as position_name " +
            "FROM ${schema_name}._sc_plan_work_setting ps LEFt JOIN ${schema_name}._sc_plan_work_facility_position pfp ON ps.id = pfp.setting_id  " +
            "LEFt JOIN ${schema_name}._sc_asset_position ap ON pfp.position_code = ap.position_code WHERE p.plan_code = ps.plan_code ) AS position_name, " +
            "(SELECt array_to_string( ARRAY_AGG (f.group_name || '_' || at.team_size), ',' ) FROM ${schema_name}._sc_plan_work_assist_team at " +
            "LEFt JOIN ${schema_name}._sc_group f ON at.group_id = f.id WHERE p.plan_code = at.plan_code ) AS assist, " +
            "(SELECt array_to_string(array_agg(DIStINCT(case when a.asset_name is null then '' else a.asset_name||'('||a.asset_code||')' end) ),',') as asset_name  " +
            "FROM ${schema_name}._sc_plan_work pw  " +
            "LEFt JOIN ${schema_name}._sc_plan_work_asset pwa ON pw.plan_code = pwa.plan_code  " +
            "LEFt JOIN ${schema_name}._sc_asset a ON pwa.asset_id=a.id " +
            "WHERE p.plan_code = pw.plan_code ) AS asset_name  " +
            "FROM ${schema_name}._sc_plan_work p  " +
            "LEFt JOIN ${schema_name}._sc_work_type t ON p.work_type_id = t.ID  " +
            "LEFt JOIN ${schema_name}._sc_plan_work_setting ps ON ps.plan_code = p.plan_code  " +
            "LEFt JOIN ${schema_name}._sc_plan_work_facility_position pfp ON pfp.setting_id = ps.id  " +
            "LEFt JOIN ${schema_name}._sc_plan_work_assist_team pwat ON p.plan_code = pwat.plan_code AND pwat.duty_type = '1'  " +
            "LEFt JOIN ${schema_name}._sc_user u ON u.id = pwat.user_id  " +
            "${condition}")
    List<Map<String, Object>> findPlanScheduleList(@Param("schema_name") String schemaName, @Param("condition") String searchWord);


    /**
     * 删除计划设备对象
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from  ${schema_name}._sc_plan_work_asset where plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkAssetBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除设备类型信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from ${schema_name}._sc_plan_work_asset_type where plan_code in  " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkAssetTypeBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除设备型号信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from ${schema_name}._sc_plan_work_asset_model where plan_code in  " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkAssetModelBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除计划位置信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from ${schema_name}._sc_plan_work_facility_position where plan_code in  " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkFacilityPositionBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除触发条件信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from ${schema_name}._sc_plan_work_triggle where plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkTriggleBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 获取按照条件触发的所有条件
     *
     * @param schema_name 入参
     * @return 按照条件触发的所有条件
     */
    @Select(" SELECT plan_code, column_key,column_condition,column_value  FROM ${schema_name}._sc_plan_work_triggle " +
            " WHERE column_key IS NOT NULL ")
    List<Map<String, Object>> findAllColumnKeyListTriggleList(@Param("schema_name") String schema_name);

    /**
     * 删除点巡检点信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from  ${schema_name}._sc_plan_work_inspection where plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkInspectionBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除计划配置信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> DELETE FROM ${schema_name}._sc_plan_work_setting WHERE plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkSettingBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除计划协助团队
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from ${schema_name}._sc_plan_work_assist_team where plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkAssistOrgBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除计划执行人
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> delete from ${schema_name}._sc_plan_work_executor where plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanWorkExecutorBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除计划自定义配置信息
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> DELETE FROM ${schema_name}._sc_plan_work_property WHERE plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanJsonBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);

    /**
     * 删除维保计划
     *
     * @param schema_name
     * @param planCodes
     * @return
     */
    @Delete("<script> Delete from  ${schema_name}._sc_plan_work where plan_code in " +
            "<foreach collection='planCodes' item='arr' open='(' separator=',' close=')'> " +
            "#{arr} " +
            "</foreach> </script>")
    int deletePlanScheduleBatch(@Param("schema_name") String schema_name, @Param("planCodes") String[] planCodes);


    //新增计划执行人
    @Insert("insert into ${schema_name}._sc_plan_work_executor (plan_code, user_id,self_or_out, total_hour) values (#{plan_code},#{user_id},#{self_or_out}::int,#{total_hour}::float)")
    int insertPlanWorkExecutor(Map<String, Object> data);

    //新增计划协助团队
    @Insert("insert into ${schema_name}._sc_plan_work_assist_team (plan_code, group_id, team_size, self_or_out, total_hour,duty_type,user_id) values (#{plan_code},#{group_id}::int,#{team_size}::int,#{self_or_out}::int,#{total_hour}::float,#{duty_type},#{user_id})")
    int insertPlanWorkAssistTeam(Map<String, Object> data);

    /**
     * 查询计划维保对象信息列表
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Select("SELECT ps.id as setting_id,ps.plan_code, " +
            "fp.is_all_asset, to_char(fp.enable_begin_date, 'yyyy-MM-dd HH24:mi:ss') as enable_begin_date,to_char(fp.enable_end_date, 'yyyy-MM-dd HH24:mi:ss') as enable_end_date, " +
            "(SELECT array_to_string(array_agg(DIStINCT(pwa.asset_id) ),',') FROM ${schema_name}._sc_plan_work_asset pwa  where ps.id = pwa.setting_id) AS assets,  " +
            "(SELECT array_to_string(array_agg(DIStINCT(pwfp.position_code) ),',') FROM ${schema_name}._sc_plan_work_facility_position pwfp  where ps.id = pwfp.setting_id) AS position_codes, " +
            "(SELECT array_to_string(ARRAY_AGG(DISTINCT(pwat.asset_category_id)),',') FROM ${schema_name}._sc_plan_work_asset_type pwat WHERE ps.ID = pwat.setting_id) AS asset_category_id, " +
            "(SELECT array_to_string(ARRAY_AGG(DISTINCT(pwam.asset_model_id)),',') FROM ${schema_name}._sc_plan_work_asset_model pwam WHERE ps.ID = pwam.setting_id) AS asset_model_id " +
            "FROM ${schema_name}._sc_plan_work_setting ps " +
//            "LEFT JOIN ${schema_name}._sc_plan_work_asset_type pwat on ps.id = pwat.setting_id " +
//            "LEFT JOIN ${schema_name}._sc_plan_work_asset_model pwam ON ps.id = pwam.setting_id " +
            "LEFT JOIN (SELECT * FROM (SELECT ROW_NUMBER() OVER (partition BY setting_id ORDER BY position_code) rowId,*   " +
            "from ${schema_name}._sc_plan_work_facility_position ) t WHERE rowId=1 )fp ON ps.id=fp.setting_id " +
            "WHERE ps.plan_code = #{plan_code}")
    List<Map<String, Object>> findMaintenanceObjectByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code);

    /**
     * 参与团队列表
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    //    @Select("SELECT * FROM (select 1 as assist_type, e.id,e.user_id,e.plan_code,e.self_or_out,e.total_hour,0 as facility_id,1 as team_size,u.user_name as executor_name,u.status   " +
//            "from ${schema_name}._sc_plan_work_executor as e   " +
//            "left join ${schema_name}._sc_user as u on e.user_id=u.id   " +
//            "where plan_code = #{plan_code} " +
//            "UNION ALL " +
//            "select 2 as assist_type,t.id,'' as user_id,t.plan_code,t.self_or_out,t.total_hour, t.facility_id,t.team_size,f.short_title as assist_org_name ,f.status " +
//            "from ${schema_name}._sc_plan_work_assist_team as t left join ${schema_name}._sc_facilities as f on t.facility_id = f.id where plan_code = #{plan_code} ) t ORDER BY assist_type asc ,id asc")
//    @Select("select t.id,t.duty_type,t.user_id,t.plan_code,t.self_or_out,t.total_hour, t.group_id,t.team_size,g.group_name ,u.user_name," +
//            "coalesce((select (c.resource->>cd1.name)::jsonb->>#{user_lang} from public.company_resource c where c.company_id = #{company_id}), cd1.name) as duty_type_name," +
//            "coalesce((select (c.resource->>cd2.name)::jsonb->>#{user_lang} from public.company_resource c where c.company_id = #{company_id}), cd2.name) as self_or_out_name " +
//            "from ${schema_name}._sc_plan_work_assist_team as t  " +
//            "left join ${schema_name}._sc_group as g on t.group_id = g.id  " +
//            "LEFT JOIN ${schema_name}._sc_user u ON t.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 ON cd1.code = t.duty_type::varchar and cd1.data_type='duty' " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 ON cd2.code = t.self_or_out::varchar and cd2.data_type='team_type' " +
//            "where plan_code = #{plan_code} " +
//            "ORDER BY t.id asc")
//    List<Map<String, Object>> findAssistByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code, @Param("user_lang") String user_lang, @Param("company_id") Long company_id);

    @Select("select t.id,t.duty_type,t.user_id,t.plan_code,t.self_or_out,t.total_hour, t.group_id,t.team_size,g.group_name ,u.user_name," +
            "cd1.name as duty_type_name," +
            "cd2.name as self_or_out_name " +
            "from ${schema_name}._sc_plan_work_assist_team as t  " +
            "left join ${schema_name}._sc_group as g on t.group_id = g.id  " +
            "LEFT JOIN ${schema_name}._sc_user u ON t.user_id = u.id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 ON cd1.code = t.duty_type::varchar and cd1.data_type='duty' " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 ON cd2.code = t.self_or_out::varchar and cd2.data_type='team_type' " +
            "where plan_code = #{plan_code} " +
            "ORDER BY t.id asc")
    List<Map<String, Object>> findAssistByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code);

    @Update(" <script> update ${schema_name}._sc_plan_work set status = #{status} where plan_code in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach> </script> ")
    int setSelectStatus(@Param("schema_name") String schema_name, @Param("ids") String[] id, @Param("status") int status);

    //查询计划任务，开始根据配置的任务，进行工单生成
    @Select("SELECT pw.plan_code, pw.work_type_id, pw.relation_type, pw.pool_id, pw.priority_level,pw.auto_generate_bill,pw.status,pw.job_status, " +
            "   pw.do_type,pw.allocate_receiver_type,pw.generate_bill_type,pw.plan_name,pw.pre_day,pw.begin_time,pw.end_time,pw.generate_time_point " +
            "FROM ${schema_name}._sc_plan_work pw " +
            "WHERE pw.status > " + StatusConstant.STATUS_DELETEED + " AND pw.is_use = '" + StatusConstant.IS_USE_YES + "' " +
            "AND (pw.job_status = " + StatusConstant.PLAN_WORK_JOB_STATUS_NEW + " OR pw.job_status = " + StatusConstant.PLAN_WORK_JOB_STATUS_MODIFIED + ") " +
            " ORDER BY pw.plan_code LIMIT #{limit} ")
    List<Map<String, Object>> getExecutePlanWork(@Param("schema_name") String schema_name, @Param("limit") int limit);

    @Update("UPDATE ${schemaName}._sc_plan_work SET job_status = #{jobStatus} WHERE plan_code IN (${planWorkCodes})")
    void updatePlanWorkJobStatusByPlanWorkCodeList(@Param("schemaName") String schemaName, @Param("planWorkCodes") String planWorkCodes, @Param("jobStatus") int jobStatus);

    @Update("UPDATE ${schemaName}._sc_plan_work SET job_status = #{jobStatus} WHERE plan_code = #{planWorkCode}")
    void updatePlanWorkJobStatusByPlanWorkCode(@Param("schemaName") String schemaName, @Param("planWorkCode") String planWorkCode, @Param("jobStatus") int jobStatus);

    /**
     * 查询维保计划的所有维保对象规则列表
     *
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    @Select("SELECT pwfp.setting_id,pwfp.is_all_asset,pwfp.enable_begin_date,pwfp.enable_end_date, " +
            "   ARRAY_TO_STRING(ARRAY_AGG(pwfp.position_code), ',') AS position_code, " +
            "   ARRAY_TO_STRING(ARRAY_AGG(pwat.asset_category_id), ',') AS asset_category_id, " +
            "   ARRAY_TO_STRING(ARRAY_AGG(pwam.asset_model_id), ',') AS asset_model_id " +
            "FROM ${schemaName}._sc_plan_work_facility_position pwfp " +
            "LEFT JOIN ${schemaName}._sc_plan_work_asset_type pwat ON pwat.setting_id = pwfp.setting_id " +
            "LEFT JOIN ${schemaName}._sc_plan_work_asset_model pwam ON pwam.setting_id = pwfp.setting_id " +
            "WHERE pwfp.plan_code = #{planWorkCode} " +
            "GROUP BY pwfp.setting_id,pwfp.is_all_asset,pwfp.enable_begin_date,pwfp.enable_end_date")
    List<Map<String, Object>> getPlanWorkSettingCondition(@Param("schemaName") String schemaName, @Param("planWorkCode") String planWorkCode);

    /**
     * 批量插入维保行事历
     *
     * @param schemaName
     * @param planWorkCalendars
     */
    @Insert({"<script>",
            "INSERT INTO ${schemaName}._sc_plan_work_calendar ",
            "(work_type_id,work_template_code,title,position_code,relation_type,relation_id,occur_time,deadline_time,",
            "status,receive_user_id,group_id,plan_code,priority_level,remark,create_time,create_user_id,auto_generate_bill,generate_time_point,body_property) ",
            "VALUES ",
            "<foreach collection='list' item='p' separator=','> ",
            "(#{p.work_type_id},#{p.work_template_code},#{p.title},#{p.position_code},#{p.relation_type},#{p.relation_id},",
            "#{p.occur_time},#{p.deadline_time},#{p.status},#{p.receive_user_id},#{p.group_id},#{p.plan_code},#{p.priority_level},#{p.remark},",
            "#{p.create_time},#{p.create_user_id},#{p.auto_generate_bill},#{p.generate_time_point},#{p.plan_work_property}::jsonb) ",
            "</foreach>",
            "</script>"})
    int insertBatchPlanWorkCalendars(@Param("schemaName") String schemaName, @Param("list") List<PlanWorkCalendarModel> planWorkCalendars);

    /**
     * 新增维保行事历
     */
    @Insert("insert into ${schemaName}._sc_plan_work_calendar " +
            "(work_calendar_code,work_type_id,work_template_code,title,position_code,relation_type,relation_id,occur_time,deadline_time," +
            "status,receive_user_id,group_id,plan_code,priority_level,remark,create_time,create_user_id,auto_generate_bill,generate_time_point,body_property) " +
            "VALUES " +
            "(#{p.work_calendar_code}::int,#{p.work_type_id},#{p.work_template_code},#{p.title},#{p.position_code},#{p.relation_type},#{p.relation_id}," +
            "#{p.occur_time},#{p.deadline_time},#{p.status},#{p.receive_user_id},#{p.group_id},#{p.plan_code},#{p.priority_level},#{p.remark}," +
            "#{p.create_time},#{p.create_user_id},#{p.auto_generate_bill},#{p.generate_time_point},#{p.plan_work_property}::jsonb) " +
            " ON CONFLICT (work_calendar_code) " +
            " DO UPDATE SET work_type_id=#{p.work_type_id},work_template_code=#{p.work_template_code},title=#{p.title}," +
            " position_code=#{p.position_code},relation_type=#{p.relation_type},relation_id=#{p.relation_id}," +
            " occur_time=#{p.occur_time},deadline_time=#{p.deadline_time},receive_user_id=#{p.receive_user_id}," +
            " group_id=#{p.group_id},priority_level=#{p.priority_level},remark=#{p.remark},create_time=#{p.create_time}," +
            " create_user_id=#{p.create_user_id},auto_generate_bill=#{p.auto_generate_bill},generate_time_point=#{p.generate_time_point}," +
            " body_property=#{p.plan_work_property}::jsonb")
    // @Options(useGeneratedKeys = true, keyProperty = "p.work_calendar_code", keyColumn = "work_calendar_code")
    int insertPlanWorkCalendar(@Param("schemaName") String schemaName, @Param("p") PlanWorkCalendarModel planWorkCalendar);

    @Update({
            "<script>",
//            "<foreach collection='list' item='p' separator=','> ",
//            "UPDATE ${schemaName}._sc_plan_work_calendar SET body_property = #{p.body_property}::jsonb ",
//            "WHERE work_calendar_code = #{p.work_calendar_code} ",
//            "</foreach>",
            "UPDATE ${schemaName}._sc_plan_work_calendar SET body_property = ",
            "   (CASE work_calendar_code ",
            "       <foreach collection='list' item='p' > ",
            "           WHEN #{p.work_calendar_code} THEN #{p.body_property}::jsonb ",
            "       </foreach> ",
            "   ELSE body_property END) ",
            "WHERE work_calendar_code IN ",
            "   <foreach collection='list' item='p' separator=',' open='(' close=')'> ",
            "       #{p.work_calendar_code} ",
            "   </foreach> ",
            "</script>"})
    int updatePlanWorkCalendarTaskItems(@Param("schemaName") String schemaName, @Param("list") List<Map<String, Object>> taskItems);

    /**
     * 查询计划行事历列表
     *
     * @param schemaName
     * @param limit
     * @return
     */
//    @Select("SELECT c.*,c.body_property::text, p.body_property as plan_work_property FROM ${schemaName}._sc_plan_work_calendar c " +
//            "LEFT JOIN ${schemaName}._sc_plan_work_property p ON p.plan_code = c.plan_code " +
//            "WHERE c.status = " + Constants.PLAN_WORK_CALENDAR_STATUS_NEW + " AND deadline_time > NOW() AND c.occur_time < '${occurTime}' AND c.auto_generate_bill = #{autoGenerateBill} " +
//            "ORDER BY c.occur_time LIMIT #{limit}")
    @Select("SELECT c.*,c.body_property::text, p.body_property as plan_work_property FROM ${schemaName}._sc_plan_work_calendar c " +
            "LEFT JOIN ${schemaName}._sc_plan_work_property p ON p.plan_code = c.plan_code " +
            "WHERE c.status = " + Constants.PLAN_WORK_CALENDAR_STATUS_NEW + " AND deadline_time > NOW() AND c.auto_generate_bill = #{autoGenerateBill} " +
            "AND to_char(c.occur_time, 'yyyy-MM-dd') = to_char(CURRENT_DATE, 'yyyy-MM-dd') " +
            "AND NOW() > (to_date(to_char(NOW(), 'yyyy-MM-dd')||' 00:00:00', 'yyyy-MM-dd HH24:mi:ss') +  (CASE WHEN generate_time_point is null THEN 0 ELSE generate_time_point END) * INTERVAL '1 HOUR') " +
            "ORDER BY c.occur_time LIMIT #{limit}")
    List<PlanWorkCalendarModel> getPlanWorkCalendarListByOccurTime(@Param("schemaName") String schemaName, @Param("autoGenerateBill") int autoGenerateBill, @Param("limit") int limit);

    /**
     * 批量修改行事历状态
     *
     * @param schemaName
     * @param workCalendarCodes
     * @param status
     */
    @Update("UPDATE ${schemaName}._sc_plan_work_calendar SET status = #{status} WHERE work_calendar_code IN (${workCalendarCodes})")
    void updatePlanWorkCalendarStatusByWorkCalendarCodeList(@Param("schemaName") String schemaName, @Param("workCalendarCodes") String workCalendarCodes, @Param("status") int status);

    /**
     * @param schemaName
     * @param planWorkCode
     */
    @Delete("DELETE FROM ${schemaName}._sc_plan_work_calendar WHERE plan_code=#{planWorkCode} AND status != " + Constants.PLAN_WORK_CALENDAR_STATUS_GENERATED)
    void deletePlanWorkCalendarByPlanWorkCode(@Param("schemaName") String schemaName, @Param("planWorkCode") String planWorkCode);

    /**
     * 查询指定设备某一天的行事历数量
     *
     * @param schemaName
     * @param planWorkCode
     * @param assetId
     * @param date
     * @return
     */
    @Select("SELECT COUNT(1) FROM ${schemaName}._sc_plan_work_calendar WHERE plan_code=#{planWorkCode} and relation_id = #{assetId} AND to_char(occur_time,'yyyy-MM-dd') = #{date}")
    int countPlanWorkCalendarByAssetAndDate(@Param("schemaName") String schemaName, @Param("planWorkCode") String planWorkCode, @Param("assetId") String assetId, @Param("date") String date);

    /**
     * 行事历过期数据处理
     *
     * @param schemaName
     */
    @Select("UPDATE ${schemaName}._sc_plan_work_calendar SET status = " + Constants.PLAN_WORK_CALENDAR_STATUS_EXPIRED +
            " WHERE deadline_time IS NOT NULL AND deadline_time < NOW() AND status = " + Constants.PLAN_WORK_CALENDAR_STATUS_NEW)
    void expirePlanWorkCalendar(@Param("schemaName") String schemaName);

    /**
     * 查询任务列表
     *
     * @param schemaName
     * @param planWorkCode
     * @return
     */
    @Select("SELECT body_property FROM ${schemaName}._sc_plan_work_property WHERE plan_code = #{planWorkCode}")
    List<String> findTaskListByPlanWorkCode(@Param("schemaName") String schemaName, @Param("planWorkCode") String planWorkCode);

    /**
     * 根据计划编码获取计划触发条件数量
     */
    @Select(" SELECT COUNT(1) FROM ${schemaName}._sc_plan_work_triggle WHERE plan_code = #{plan_code}")
    Integer findCountTriggleByPlanCode(@Param("schemaName") String schemaName, @Param("plan_code") String planCode);

    /**
     * 根据设备位置 设备类型 设备型号 查询该设备是此工单的维保对象
     *
     * @param schemaName
     * @param planCode
     * @return
     */
    @Select(" SELECT t1.setting_id,t1.position_code,t1.is_all_asset,t1.enable_begin_date,t1.enable_end_date,t2.asset_model_id,t3.asset_category_id\n" +
            " FROM ${schemaName}._sc_plan_work_setting AS t" +
            " LEFT JOIN ${schemaName}._sc_plan_work_facility_position AS t1 ON t.id = t1.setting_id" +
            " LEFT JOIN ${schemaName}._sc_plan_work_asset_model AS t2 ON t.id = t2.setting_id" +
            " LEFT JOIN ${schemaName}._sc_plan_work_asset_type AS t3 ON t.id = t3.setting_id" +
            " WHERE t.plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkSettingList(@Param("schemaName") String schemaName, @Param("plan_code") String planCode);

    /**
     * 根据设备id和计划工单编码查询该设备是否是该计划工单维保计划
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Select("SELECT * FROM ${schemaName}._sc_plan_work_asset  WHERE plan_code = #{plan_code} AND asset_id = #{asset_id} LIMIT 1")
    Map<String, Object> findPlanWorkAssetByPlanCode(@Param("schemaName") String schemaName, @Param("plan_code") String plan_code, @Param("asset_id") String asset_id);

    /**
     * 查询计划条件的触发条件
     *
     * @param schemaName
     * @param plan_code
     * @return
     */
    @Select("select plan_code, period_type,column_key,column_condition,column_value   " +
            "from ${schema_name}._sc_plan_work_triggle a   " +
            "where a.period_type = 0 and a.plan_code = #{plan_code}")
    List<Map<String, Object>> findPlanWorkConditionTriggleByPlanCode(@Param("schema_name") String schemaName, @Param("plan_code") String plan_code);

    /**
     * 查询任务项
     * @param schemaName
     * @param plan_codes
     * @return
     */
    @Select("<script> " +
            " select work_calendar_code, " +
            " case when c.body_property is not null " +
            " then c.body_property ::text else " +
            " (select p.body_property ::text from ${schema_name}._sc_plan_work_property p " +
            " where p.plan_code=c.plan_code) end as body_property " +
            " from ${schema_name}._sc_plan_work_calendar c " +
            " where c.work_calendar_code in " +
            " <foreach collection='plan_codes' item='item' open='(' separator=',' close=')'> " +
            " #{item.work_calendar_code} " +
            " </foreach> " +
            " </script> ")
    List<Map<String, Object>> findPlanWorkPropertyByPlanCodes(@Param("schema_name") String schemaName, @Param("plan_codes") List<Map<String, Object>> plan_codes);

    @Select("<script> " +
            " select " +
            " case when c.body_property is not null " +
            " then c.body_property ::text else " +
            " (select p.body_property ::text from ${schema_name}._sc_plan_work_property p " +
            " where p.plan_code=c.plan_code) end as body_property " +
            " from ${schema_name}._sc_plan_work_calendar c " +
            " where c.work_calendar_code = #{work_calendar_code} " +
            " </script> ")
    Map<String, Object> findPlanWorkPropertyInfo(@Param("schema_name") String schemaName, @Param("work_calendar_code") String work_calendar_code);

    @Select("select 1+coalesce(max(\"work_calendar_code\"::int),1) as seq from ${schema_name}._sc_plan_work_calendar")
    String findPlanWorkSeq(@Param("schema_name") String schemaName);
}
