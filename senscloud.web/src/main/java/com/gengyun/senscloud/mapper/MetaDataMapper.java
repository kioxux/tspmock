package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @description 元数据表
 * 用于对元数据表的插入 删除 更新
 */
@Mapper
public interface MetaDataMapper {
//    @Select("select * from ${schema_name}._sc_metadata")
//    List<MetaData> findAll(String schema_name);
//
//    @Update("update ${schema_name}._sc_metadata set " +
//            "table_name = #{table_name},table_alias = #{table_alias},\n" +
//            "description = #{description},fields = #{fields}::jsonb \n " +
//            "where id = #{id}")
//    void updateMetaData(MetaData metaData);
//
//    @Insert("insert into ${schema_name}._sc_metadata(table_name,table_alias,description,fields) \n" +
//            "values(#{table_name},#{table_alias},#{description},#{fields}::jsonb)")
//    void insertMetaData(MetaData metaData);
//
//    @Delete("delete from _sc_metadata where table_name = #{table_name}")
//    void deleteMetaData(@Param("table_name") String table_name);
//
//    @Select("select * from ${schema_name}._sc_metadata where table_name=#{table_name}")
//    MetaData findOne(MetaData metaData);
}
