package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 设备物联信息
 */
@Mapper
public interface AssetIotSettingMapper {
//    /**
//     * 按设备信息统计数量
//     *
//     * @param schemaName
//     * @param assetId
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_asset_iot_setting where asset_id = #{assetId}")
//    Map<String, Object> selectAisByAssetId(@Param("schema_name") String schemaName, @Param("assetId") String assetId);
//
//    /**
//     * 新增设备物联信息
//     *
//     * @param schemaName
//     * @param paramMap
//     */
//    @Insert("insert into ${schema_name}._sc_asset_iot_setting(asset_id,iot_status,use_add,out_code,in_code," +
//            "tem_value1,tem_value2,tem_value3,tem_value4,tem_value5,tem_value9) values " +
//            "(#{pm.asset_id},#{pm.iot_status}::int,#{pm.use_add}::float,#{pm.out_code},#{pm.in_code}," +
//            "#{pm.tem_value1}::float,#{pm.tem_value2}::float,#{pm.tem_value3}::float,#{pm.tem_value4}::float,#{pm.tem_value5}::float,#{pm.tem_value9})")
//    int insertAssetIotSetting(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);
//
//
//    /**
//     * 更新设备物联信息
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_iot_setting set " +
//            "iot_status = #{pm.iot_status}::int, use_add = #{pm.use_add}::float, out_code = #{pm.out_code}, in_code = #{pm.in_code}, " +
//            "tem_value1 = #{pm.tem_value1}::float, tem_value2 = #{pm.tem_value2}::float, tem_value3 = #{pm.tem_value3}::float, " +
//            "tem_value4 = #{pm.tem_value4}::float, tem_value5 = #{pm.tem_value5}::float, tem_value9 = #{pm.tem_value9} " +
//            "where asset_id = #{pm.asset_id}")
//    int updateAisByAssetId(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);
//
//
//    /**
//     * 根据组织id，查询设备物联数量
//     * @param schema_name
//     * @param iotStatus
//     * @param facilityId
//     * @return
//     */
//    @Select("SELECT COUNT(DISTINCT a._id) FROM ${schema_name}._sc_asset a " +
//            "INNER JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = a._id " +
//            "INNER JOIN " +
//            "( " +
//            "SELECT cf.* FROM ${schema_name}._sc_facilities pf " +
//            "INNER JOIN ${schema_name}._sc_facilities cf ON cf.facility_no like pf.facility_no || '%' " +
//            "WHERE pf.id = ${facilityId} " +
//            ") f ON ao.org_id = f.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_iot_setting i ON a._id = i.asset_id " +
//            "WHERE i.iot_status = ${iotStatus} ")
//    int countAssetByFacilityId(@Param("schema_name") String schema_name, @Param("facilityId") Long facilityId, @Param("iotStatus") int iotStatus);

}
