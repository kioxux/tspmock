package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Wudang Dong
 * @Description:
 * @Date: Create in 下午 5:16 2019/10/29 0029
 */
@Mapper
public interface SpotInspectionReportMapper {
//
//    /**
//     * @Descrip
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select j->>'taskContent' taskContent  from ( " +
//            "SELECT json_array_elements(j) j from ( " +
//            "SELECT array_to_json(array_agg(j)) j from ( " +
//            "SELECT jsonb_array_elements(body_property) j " +
//            "from ${schema_name}._sc_works_detail a " +
//            "left join ${schema_name}._sc_work_type  as t on t.id=a.work_type_id  " +
//            "left join ${schema_name}._sc_works as w on a.work_code=w.work_code " +
//            "                    where (t.business_type_id = '3' or  t.business_type_id = '4') and a.status<>900 and a.is_main = '-1' ${condition} " +
//            ") t where j->>'fieldViewType' = '9' " +
//            ") t " +
//            ") t limit ${page} offset ${begin}  ")
//    List<Map<String,Object>> findList(@Param("schema_name") String schema_name ,@Param("condition") String condition,@Param("page") int pageSize,@Param("begin") int begin);//
//
//    @Select("select count(j)  from ( " +
//            "SELECT json_array_elements(j) j from ( " +
//            "SELECT array_to_json(array_agg(j)) j from ( " +
//            "SELECT jsonb_array_elements(body_property) j " +
//            "from ${schema_name}._sc_works_detail a " +
//            "left join ${schema_name}._sc_work_type  as t on t.id=a.work_type_id  " +
//            "left join ${schema_name}._sc_works as w on a.work_code=w.work_code " +
//            "                    where (t.business_type_id = '3' or  t.business_type_id = '4')  and a.status<>900 and a.is_main = '-1' ${condition} " +
//            ") t where j->>'fieldViewType' = '9' " +
//            ") t " +
//            ") t")
//    int count(@Param("schema_name") String schema_name ,@Param("condition") String condition);
}
