package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MessageRuleMapper {

//    /**
//     * 查询所有的消息规则
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT r.*,mf.* FROM ${schema_name}._sc_message_rule r " +
//            "LEFT JOIN ${schema_name}._sc_msg_content_config mf on r.msg_content_id=mf.id ")
//    List<Map<String, Object>> queryMessageRules(@Param("schema_name") String schema_name);
//
//
//    /**
//     * 根据规则查询对应表对应字段
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT ${judgeColumn} as judgeColumn FROM ${schema_name}.${businessTable}  " +
//            "WHERE ${keyColumn}='${subWorkCode}' ORDER BY ${judgeColumn} DESC LIMIT 1 ")
//    List<Map<String, Object>> queryJudgeColumn(@Param("schema_name") String schema_name, @Param("judgeColumn") String judgeColumn,
//                                               @Param("businessTable") String businessTable, @Param("keyColumn") String keyColumn, @Param("subWorkCode") String subWorkCode);
//
//
//    /**
//     * 根据该规则id查询消息中心表是否已经生成过了
//     *
//     * @param schema_name
//     * @return+
//     */
//    @Select("SELECT id,createtime FROM ${schema_name}._sc_message_center_init WHERE rule_id=${ruleId} AND process_id='${processId}' ORDER BY createtime desc ")
//    List<Map<String, Object>> queryMessageByRuleId(@Param("schema_name") String schema_name, @Param("ruleId") int ruleId, @Param("processId") String processId);
//
//
//    /**
//     * 插入消息中心表
//     *
//     * @param schema_name
//     * @param paramMap
//     * @return
//     */
//    @Update("INSERT INTO ${schema_name}._sc_message_center(receive_account,msg_content,receive_mobile,send_time,business_type,business_no," +
//            "send_account,is_send,is_sms,is_email,is_pc,is_app,is_weixin,process_id,rule_id) " +
//            "VALUES (#{s.receive_account},#{s.msg_content},#{s.receive_mobile},current_timestamp,#{s.business_type},#{s.business_no}" +
//            ",#{s.send_account},#{s.is_send},#{s.is_sms},#{s.is_email},#{s.is_pc},#{s.is_app},#{s.is_weixin},#{s.process_id},#{s.rule_id}) ")
//    void insertMessageColumn(@Param("schema_name") String schema_name, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 查询未发送的消息
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT id FROM ${schema_name}._sc_message_center WHERE  is_send='f' ")
//    List<Map<String, Object>> queryMessages(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询组织或位置信息
//     *
//     * @param schema_name
//     * @return
//     */
//    @Select("SELECT ${fColumn} FROM ${businessTable}  WHERE ${keyColumn}='${subWorkCode}' ")
//    List<Map<String, Object>> queryBusiness(@Param("schema_name") String schema_name,@Param("fColumn") String fColumn,
//                                            @Param("businessTable") String businessTable, @Param("keyColumn") String keyColumn, @Param("subWorkCode") String subWorkCode);

}