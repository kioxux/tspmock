package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

/**
 * 公共工具方法用
 */
@Mapper
public interface CommonUtilMapper {
    /**
     * 批量存储数据
     *
     * @param sql 语句
     */
    @Insert("${sqlString}")
    void insertBatch(@Param("sqlString") String sql);

    /**
     * 按父位置id，查询其最大的子位置编号
     *
     * @param schema_name
     * @param parentId
     * @param tableName
     * @param fieldName
     * @param parentFieldName
     * @return
     */
    @Select("SELECT MAX(${fieldName}) FROM ${schema_name}.${tableName} WHERE ${parentFieldName} = #{parentId}")
    String getMaxNoByParentId(@Param("schema_name") String schema_name, @Param("parentId") Object parentId,
                              @Param("tableName") String tableName, @Param("fieldName") String fieldName,
                              @Param("parentFieldName") String parentFieldName);

    /**
     * 根据id获取数据
     *
     * @param schema_name
     * @param id
     * @param tableName
     * @param fieldName
     * @param keyFieldName
     * @return
     */
    @Select("SELECT ${fieldName} FROM ${schema_name}.${tableName} where ${keyFieldName}=#{id} ")
    String getNoById(@Param("schema_name") String schema_name, @Param("id") Object id, @Param("tableName") String tableName,
                     @Param("fieldName") String fieldName, @Param("keyFieldName") String keyFieldName);

    /**
     * 根据id获取数据
     *
     * @param schema_name
     * @param id
     * @param parentId
     * @param tableName
     * @param fieldName
     * @param parentFieldName
     * @return
     */
    @Select("SELECT ${fieldName} FROM ${schema_name}.${tableName} where id = #{id} and ${parentFieldName} = #{parentId} ")
    String getNoByData(@Param("schema_name") String schema_name, @Param("id") Object id, @Param("parentId") Object parentId,
                       @Param("tableName") String tableName, @Param("fieldName") String fieldName,
                       @Param("parentFieldName") String parentFieldName);

    /**
     * 更新序列
     *
     * @param schema_name 数据库
     * @param seq         序列名称
     * @param col         列名称
     * @param table       表名称
     */
    @Update("select setval('${schema_name}.${seq}', (select coalesce(max(${col}), 1) from ${schema_name}.${table}))")
    void updateSeqByTbl(@Param("schema_name") String schema_name, @Param("seq") String seq, @Param("col") String col, @Param("table") String table);

    /**
     * 更新序列开始值
     *
     * @param schema_name 数据库
     * @param seq         序列名称
     * @param value       表名称
     */
    @Select("ALTER SEQUENCE ${schema_name}.${seq} START ${value}")
    void updateSeqStartByTbl(@Param("schema_name") String schema_name, @Param("seq") String seq, @Param("value") int value);

    /**
     * 取表字段最大值
     *
     * @param schema_name 数据库
     * @param col         列名称
     * @param table       表名称
     * @return 字段最大值
     */
    @Select("select coalesce(max(${col}),1) from ${schema_name}.${table}")
    String findMaxId(@Param("schema_name") String schema_name, @Param("col") String col, @Param("table") String table);

    /**
     * 查询所有的域名
     *
     * @return 所有的域名
     */
//    @Select("select distinct domain_name from public.domain_port_url")
//    List<String> getAllDomain();

    /**
     * 通过域名地址获取域名表详细数据
     *
     * @param domainName 域名
     * @return 域名详细数据
     */
//    @Select("select * from public.domain_port_url where domain_name = #{domainName}")
//    DomainPortUrlEntity getDomainEntityByName(@Param("domainName") String domainName);
}
