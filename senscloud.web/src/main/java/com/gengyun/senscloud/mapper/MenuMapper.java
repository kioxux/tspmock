package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @author 蒙立坤
 * @date 2017年11月2日 10:07
 * @description 系统菜单表映射
 */
@Mapper  //声明为mybatis Mapper类
public interface MenuMapper {
//    @Update("update _system.menu set \"order\"=${order} where id=${id}")
//    void updateMenuOrder(@Param("id") int id, @Param("order") int order);
//
//    @Select("select * from _system.menu order by \"order\"")
//    List<MenuData> getParentMenuList();
//
//    @Select("select * from _system.menu where id=#{id}")
//    MenuData findByID(int id);
//
//    @Update("update _system.menu set menu_name=#{menu_name},menu_page=#{menu_page},menu_url=#{menu_url},parent_id=#{parent_id},isshow=#{isshow},icon=#{icon},menu_type=#{menu_type} where id=#{id}")
//    void updateMenu(MenuData menuData);
//
//    @Update("insert into _system.menu(menu_name,menu_page,menu_url,parent_id,isshow,icon,createtime,\"order\",menu_type) values(#{menu_name},#{menu_page},#{menu_url},#{parent_id},#{isshow},#{icon},#{createtime},#{order},#{menu_type})")
//    void addMenu(MenuData menuData);
//
//    @Update("delete from _system.menu where id=#{id}")
//    void deleteMenu(int id);
//
//    /**
//     * 查询所有平台菜单
//     */
//    @Select("select * from _system.menu order by \"order\" asc")
//    List<MenuData> findAllMenu();
//
//    /**
//     * 查询所有平台菜单
//     */
//    @Select("select * from _system.menu where menu_type=#{menu_type} order by \"order\" asc")
//    List<MenuData> findMenuByType(int menu_type);
}
