package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: Wudang Dong
 * @Description:设备迁移DAO
 * @Date: Create in 上午 11:31 2019/4/30 0030
 */
@Mapper
public interface AssetTransferMapper {
//
//    @Select("select t.*,ARRAY_TO_STRING(ARRAY_AGG(ad.asset_id), ',') as relation_id from ${schema_name}._sc_asset_transfer as t LEFT JOIN ${schema_name}._sc_asset_transfer_detail ad ON ad.transfer_code = t.transfer_code " +
//        " where t.transfer_code=#{transferCode} GROUP BY t.transfer_code")
//    public Map<String, Object> selectDetailByTransferCode(@Param("schema_name") String schema_name , @Param("transferCode") String transferCode);
//
//    @Select("SELECT DISTINCT t.transfer_code,t.create_time,t.status,t7.username AS username, " +
//            "CASE WHEN p1.position_name IS NULL THEN f1.short_title ELSE p1.position_name END AS new_position_name, " +
//            "CASE WHEN p2.position_name IS NULL THEN f2.short_title ELSE p2.position_name END AS position_name " +
//            "FROM ${schema_name}._sc_asset_transfer AS T " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p1 ON p1.position_code = t.new_position_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p2 ON p2.position_code = t.position_id " +
//            "left join ${schema_name}._sc_facilities f1 on f1.ID::VARCHAR = t.new_position_id " +
//            "left join ${schema_name}._sc_facilities f2 on f2.ID::VARCHAR = t.position_id " +
//            "left join ${schema_name}._sc_asset_position_organization apo1 on apo1.org_id = f1.id " +
//            "left join ${schema_name}._sc_asset_position_organization apo2 on apo2.org_id = f2.id " +
//            "LEFT JOIN ${schema_name}._sc_user AS t7 ON t7.account = T.create_user_account " +
//            " where 1=1 ${condition} order by t.create_time desc")
//    public List<Map<String, Object>> selectAllDiscard(@Param("schema_name")String schena_name, @Param("condition") String condition);
//
//    @Select("select count(DISTINCT t.transfer_code) from ${schema_name}._sc_asset_transfer as t " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p1 ON p1.position_code = t.new_position_id " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p2 ON p2.position_code = t.position_id " +
//            "left join ${schema_name}._sc_facilities f1 on f1.ID::VARCHAR = t.new_position_id " +
//            "left join ${schema_name}._sc_facilities f2 on f2.ID::VARCHAR = t.position_id " +
//            "left join ${schema_name}._sc_asset_position_organization apo1 on apo1.org_id = f1.id " +
//            "left join ${schema_name}._sc_asset_position_organization apo2 on apo2.org_id = f2.id " +
//            "where 1=1 ${condition}")
//    public int selectAllDiscardNum(@Param("schema_name")String schena_name, @Param("condition") String condition);
//
//    @Update("update ${schema_name}._sc_asset_transfer set status = #{status} where transfer_code=#{transfer_code}")
//    public int updateStatus(@Param("schema_name") String schema_name ,@Param("status") int status ,@Param("transfer_code") String transfer_code);
//
//    /**
//     * 修改设备调拨申请信息
//     * @param assetTransferModel
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_transfer set transfer_code=#{transfer_code},position_id=#{position_id},new_position_id=#{new_position_id}, " +
//            "status=#{status},remark=#{remark},body_property=#{body_property}::jsonb where transfer_code=#{transfer_code}")
//    public int updateAssetTransfer(AssetTransferModel assetTransferModel);
//
//    @Insert("insert into ${schema_name}._sc_asset_transfer (transfer_code,position_id,new_position_id,status,remark,create_user_account,create_time,body_property,source_sub_code) values " +
//            "(#{transfer_code},#{position_id},#{new_position_id},#{status},#{remark},#{create_user_account},#{create_time},#{body_property}::jsonb,#{source_sub_code})")
//    int add(AssetTransferModel assetTransferModel);
//
//    /**
//     * 批量新增设备调拨详情信息
//     * @param schema_name
//     * @param assetTransferDetails
//     * @return
//     */
//    @Insert("<script>" +
//            "INSERT INTO ${schema_name}._sc_asset_transfer_detail (transfer_code,asset_id,asset_code_new) VALUES " +
//            " <foreach collection=\"assetTransferDetails\" item=\"assetTransferDetail\" separator=\",\">  " +
//                    "(#{assetTransferDetail.transfer_code},#{assetTransferDetail.asset_id},#{assetTransferDetail.asset_code_new}) " +
//            " </foreach>" +
//            "</script>")
//    public int addAssetTransferDetails(@Param("schema_name") String schema_name, @Param("assetTransferDetails") List<AssetTransferDetailModel> assetTransferDetails);
//
//    /**
//     * @discription:修改设备位置
//     * @param schema_name
//     * @param position_code
//     * @param asset_id
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset set position_code=#{position_code} where _id =#{asset_id}")
//    public int discardAssetFacility(@Param("schema_name") String schema_name ,@Param("position_code") String position_code, @Param("asset_id")String asset_id);
//
//    /**
//     * 添加设备位置信息
//     * @param schema_name
//     * @param org_id
//     * @param asset_id
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_asset_organization (org_id, asset_id) values (#{org_id},#{asset_id})")
//    public int addAssetOrg(@Param("schema_name") String schema_name ,@Param("org_id") Long org_id, @Param("asset_id")String asset_id);
//
//    /**
//     * 删除设备位置信息
//     * @param schema_name
//     * @param org_id
//     * @param asset_id
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_asset_organization where org_id = #{org_id} and asset_id = #{asset_id}")
//    public int deleteAssetOrg(@Param("schema_name") String schema_name, @Param("org_id") Long org_id, @Param("asset_id") String asset_id);
//
//    /**
//     * 批量删除设备位置信息
//     * @param schema_name
//     * @param org_id
//     * @param asset_id
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_asset_organization where org_id in (${org_id}) and asset_id = #{asset_id}")
//    public int deleteAssetOrgs(@Param("schema_name") String schema_name, @Param("org_id") String org_id, @Param("asset_id") String asset_id);
//
//    /**
//     * 更新调出附件id
//     * @param schema_name
//     * @param send_file_ids
//     * @param transfer_code
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_transfer set send_file_ids=#{send_file_ids},body_property=#{body_property}::jsonb where transfer_code = #{transfer_code}")
//    public int updateSendFile(@Param("schema_name") String schema_name, @Param("send_file_ids") String send_file_ids, @Param("body_property") String body_property, @Param("transfer_code") String transfer_code);
//
//    /**
//     * 更新bodyproperty
//     * @param schema_name
//     * @param transfer_code
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_transfer set body_property=#{body_property}::jsonb where transfer_code = #{transfer_code}")
//    public int updateBodyProperty(@Param("schema_name") String schema_name, @Param("body_property") String body_property, @Param("transfer_code") String transfer_code);
//
//    /**
//     * 更新接受附件id
//     * @param schema_name
//     * @param receive_file_ids
//     * @param transfer_code
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_transfer set receive_file_ids=#{receive_file_ids},status = #{status},body_property=#{body_property}::jsonb where transfer_code = #{transfer_code}")
//    public int updateReceiveFile(@Param("schema_name") String schema_name, @Param("receive_file_ids") String receive_file_ids, @Param("status") int status, @Param("body_property") String body_property, @Param("transfer_code") String transfer_code);
//
//    /**
//     * 批量删除设备调拨详情表信息
//     * @param schema_name
//     * @param transfer_code
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_asset_transfer_detail where transfer_code = #{transfer_code}")
//    public int deleteAssetTransferDetail(@Param("schema_name") String schema_name, @Param("transfer_code") String transfer_code);
//
//    @Select("select count(1) from ${schema_name}._sc_asset_organization where org_id = #{org_id} and asset_id = #{asset_id}")
//    public int queryAssetOrgs(@Param("schema_name") String schema_name, @Param("org_id") String org_id, @Param("asset_id") String asset_id);
}
