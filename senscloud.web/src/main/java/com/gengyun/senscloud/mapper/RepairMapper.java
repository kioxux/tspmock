package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RepairMapper {
//
//    @Select("select * from ${schema_name}._sc_repair_type order by \"order\" ")
//    List<RepairType> findAllRepairTypeList(@Param("schema_name") String schema_name);
//
//    @Select("select * from ${schema_name}._sc_fault_type order by \"order\" ")
//    List<FaultType> findAllFaultTypeList(@Param("schema_name") String schema_name);
//
//    @Select("select * from ${schema_name}._sc_asset_running_status order by \"order\" ")
//    List<AssetRunningData> findAllAssetStatusList(@Param("schema_name") String schema_name);
//
//    @Insert("insert into ${schema_name}._sc_repair (repair_code, asset_type, asset_id, asset_status, fault_type, " +
//            "priority_level, occur_time, deadline_time, fault_note, fault_img, repair_type, distribute_account, " +
//            "distribute_time, receive_account, receive_time, repair_begin_time, spend_hour, total_work_hour, before_img," +
//            "repair_note, after_img, finished_time, audit_account, audit_time, audit_word, status, facility_id, " +
//            "remark, createtime, create_user_account, from_code)" +
//            "values(#{r.repair_code}, #{r.asset_type}, #{r.asset_id}, #{r.asset_status}, #{r.fault_type}, " +
//            "#{r.priority_level}, #{r.occur_time}, #{r.deadline_time}, #{r.fault_note}, #{r.fault_img}, " +
//            "#{r.repair_type}, #{r.distribute_account}, #{r.distribute_time}, #{r.receive_account}, #{r.receive_time}, " +
//            "#{r.repair_begin_time}, #{r.spend_hour}, #{r.total_work_hour}, #{r.before_img}, #{r.repair_note}, #{r.after_img}, " +
//            "#{r.finished_time}, #{r.audit_account}, #{r.audit_time}, #{r.audit_word}, #{r.status}, " +
//            "#{r.facility_id}, #{r.remark}, #{r.createtime}, #{r.create_user_account}, #{r.from_code})")
//    int saveRepair(@Param("schema_name") String schema_name, @Param("r") RepairData repairData);
//
//    //获取最近一小时内的未完成的维修单
//    @Select("select count(repair_code) from ${schema_name}._sc_works where status in (20,40,50) and relation_id=#{asset_id} and (EXTRACT(EPOCH from '${current}'::timestamp)-EXTRACT(EPOCH from createtime))<3600")
//    int getRepairCountByAssetIdRecently(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("current") Timestamp current);
//
//    //新增支援人员，初始工时为0，支援人员中包含_sc_repair表中的接受人员
//    @Insert("insert into ${schema_name}._sc_repair_work_hour (repair_code, operate_account, work_hour)" +
//            "values(#{w.repair_code}, #{w.operate_account}, #{w.work_hour})")
//    int insertRepairWorkHour(@Param("schema_name") String schema_name, @Param("w") RepairWorkHoursData repairWorkHourData);
//
//
//    //编辑维保信息,点击保存按钮，或者草稿状态，重新保存或提交
//    @Update("update ${schema_name}._sc_repair set " +
//            "facility_id=#{r.facility_id}, " +
//            "asset_type=#{r.asset_type}, " +
//            "asset_id=#{r.asset_id}, " +
//            "asset_status=#{r.asset_status}, " +
//            "fault_type=#{r.fault_type}, " +
//            "occur_time=#{r.occurtime}, " +
//            "deadline_time=#{r.deadline_time}, " +
//            "fault_note=#{r.fault_note}, " +
//            "priority_level=#{r.priority_level}, " +
//            //"fault_img=#{r.fault_img}, " +
//            "repair_type=#{r.repair_type}, " +
//            "distribute_account=#{r.distribute_account}, " +
//            "distribute_time=#{r.distribute_time}, " +
//            "receive_account=#{r.receive_account}, " +
//            "receive_time=#{r.receive_time}, " +
//            "fault_note=#{r.fault_note}, " +
//            "status=#{r.status}, " +
//            "where repair_code=#{r.repair_code} ")
//    int EditRepair(@Param("schema_name") String schema_name, @Param("r") RepairData repairData);
//
//    //更新分配人
//    @Update("update ${schema_name}._sc_repair set " +
//            "distribute_account=#{r.distribute_account}, " +
//            "distribute_time=#{r.distribute_time}, " +
//            "receive_account=#{r.receive_account}, " +
//            "receive_time=#{r.receive_time}, " +
//            "priority_level=#{r.priority_level}, " +
//            "status=#{r.status} " +
//            "where repair_code=#{r.repair_code} ")
//    int EditRepairReceiveMan(@Param("schema_name") String schema_name, @Param("r") RepairData repairData);
//
//    //删除援人员，在重新保存和提交维修单时，先全部删除工时表中的人，再重新新增支援人员进来
//    @Delete("delete from ${schema_name}._sc_repair_work_hour where repair_code='${repair_code}'")
//    int deleteRepairWorkHour(@Param("schema_name") String schema_name, @Param("repair_code") String repair_code);
//
//    //更新编辑开始维修时间，为手机上点击设备的维修开始
//    @Update("update ${schema_name}._sc_repair set " +
//            "repair_begin_time=#{r.repair_begin_time} " +
//            "where repair_code=#{r.repair_code} ")
//    int EditRepairBeginTime(@Param("schema_name") String schema_name, @Param("r") RepairData repairData);
//
//    //设置维修开始计时,缺表，待建 2018-03-29
//
//    //保存和提交维修结果
//    @Update("<script>update ${schema_name}._sc_repair set " +
//            "spend_hour=#{r.spend_hour}, " +
//            "total_work_hour=#{r.total_work_hour}, " +
//            "before_img=#{r.before_img}, " +
//            "repair_note=#{r.repair_note}, " +
//            "after_img=#{r.after_img}, " +
//            "fault_type=#{r.fault_type}, " +
//            "repair_type=#{r.repair_type}, " +
//            "finished_time=#{r.finished_time}, " +
//            "<when test='r.repairBeginTime != null'>" +
//            "repair_begin_time=#{r.repair_begin_time}," +
//            "</when>" +
//            "status=#{r.status} " +
//            "where repair_code=#{r.repair_code} " +
//            "</script>")
//    int SaveRepairResult(@Param("schema_name") String schema_name, @Param("r") RepairData repairData);
//
//    //新增维修领用的备件
//    @Insert("insert into ${schema_name}._sc_repair_bom (repair_code, stock_code,bom_model,bom_code,facility_id,huojia," +
//            "fetch_man,use_count,material_code,is_from_service_supplier) " +
//            "values(#{b.repair_code}, #{b.stock_code}, #{b.bom_model},#{b.bom_code},#{b.facility_id}, #{b.huojia}," +
//            " #{b.fetch_man}, #{b.use_count}, #{b.material_code}, #{b.is_from_service_supplier})")
//    int insertRepairBom(@Param("schema_name") String schema_name, @Param("b") RepairBomData repairBomData);
//
//    //删除维修领用的备件,用于维修结果重新保存或提交时
//    @Delete("delete from ${schema_name}._sc_repair_bom where repair_code='${repair_code}'")
//    int deleteRepairBom(@Param("schema_name") String schema_name, @Param("repair_code") String repair_code);
//
//    //维修结果审核
//    @Update("update ${schema_name}._sc_repair set " +
//            "audit_account=#{r.audit_account}, " +
//            "audit_time=#{r.audit_time}, " +
//            "audit_word=#{r.audit_word}, " +
//            "status=#{r.status} " +
//            "where repair_code=#{r.repair_code} ")
//    int AuditRepairResult(@Param("schema_name") String schema_name, @Param("r") RepairData repairData);
//
//    //查找维修单列表
//    @Select("select count(r.repair_code) " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.receive_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.distribute_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_repair_type t on r.repair_type=t.id " +
//            " left join ${schema_name}._sc_asset_status a on r.asset_status=a.id " +
//            " left join ${schema_name}._sc_fault_type f on r.fault_type=f.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            "where 1=1 ${condition} ")
//    int getRepairListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    //查找维修单列表
//    @Select("select r.*,u.username as create_user_name,c.username as receive_name,d.username as distribute_name,s.status as status_name," +
//            "a.status as asset_status_name,f.type_name as fault_type_name,t.type_name as repair_type_name,dv.strname as asset_name,dv.strcode as asset_code, " +
//            "string_agg(w.operate_account, ',') as operators,ft.title as facility_name,pf.title as parent_name, cus.customer_name as suppilerName " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.receive_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.distribute_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_repair_type t on r.repair_type=t.id " +
//            " left join ${schema_name}._sc_asset_status a on r.asset_status=a.id " +
//            " left join ${schema_name}._sc_fault_type f on r.fault_type=f.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_facilities pf on ft.parentid=pf.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_repair_work_hour w on r.repair_code=w.repair_code " +
//            " left join ${schema_name}._sc_customers cus on dv.supplier=cus.id " +
//            "where 1=1 ${condition} " +
//            "group by r.repair_code, u.username,c.username,d.username,s.status,a.status,f.type_name,t.type_name,dv.strname,dv.strcode,ft.title,pf.title,cus.customer_name " +
//            "order by r.repair_code desc limit ${page} offset ${begin}")
//    List<RepairData> getRepairList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    //查找单条维修单据
//    @Select("select r.*,u.username as create_user_name,c.username as receive_name,d.username as distribute_name,au.username as audit_account_name," +
//            "s.status as status_name,a.status as asset_status_name,f.type_name as fault_type_name,t.type_name as repair_type_name," +
//            "dv.strname as asset_name,dv.strcode as asset_code,ft.title as facility_name,cus.customer_name as suppilerName " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.receive_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.distribute_account=d.account " +
//            " left join ${schema_name}._sc_user au on r.audit_account=au.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_repair_type t on r.repair_type=t.id " +
//            " left join ${schema_name}._sc_asset_status a on r.asset_status=a.id " +
//            " left join ${schema_name}._sc_fault_type f on r.fault_type=f.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers cus on dv.supplier=cus.id " +
//            "where repair_code='${repair_code}' ")
//    RepairData getRepariInfo(@Param("schema_name") String schema_name, @Param("repair_code") String repairCode);
//
//    //获取备件列表，用于维修的备件
//    @Select("select mb.*,b.bom_name,s.stock_name from ${schema_name}._sc_repair_bom mb " +
//            " left join ${schema_name}._sc_stock s on s.stock_code=mb.stock_code " +
//            " left join ${schema_name}._sc_bom b on b.bom_code=mb.bom_code and b.material_code=mb.material_code and b.bom_model=mb.bom_model and b.stock_code=mb.stock_code " +
//            "where mb.repair_code='${repair_code}' ")
//    List<RepairBomData> getRepariBomList(@Param("schema_name") String schema_name, @Param("repair_code") String repairCode);
//
//    //获取维修工时列表
//    @Select("select w.*,u.username as operate_name from ${schema_name}._sc_repair_work_hour w " +
//            "left join ${schema_name}._sc_user u on w.operate_account=u.account " +
//            "where w.repair_code='${repair_code}' ")
//    List<RepairWorkHoursData> getRepariWorkHourList(@Param("schema_name") String schema_name, @Param("repair_code") String repairCode);
//
//    //查找单条维修单据,根据设备id，找到待修的第一条
//    @Select("select r.*,u.username as create_user_name,c.username as receive_name,d.username as distribute_name,s.status as status_name," +
//            "a.status as asset_status_name,f.type_name as fault_type_name,t.type_name as repair_type_name,dv.strname as asset_name," +
//            "dv.strcode as asset_code,ft.title as facility_name,cus.customer_name as suppilerName " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.receive_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.distribute_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_repair_type t on r.repair_type=t.id " +
//            " left join ${schema_name}._sc_asset_status a on r.asset_status=a.id " +
//            " left join ${schema_name}._sc_fault_type f on r.fault_type=f.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers cus on dv.supplier=cus.id " +
//            "where r.asset_id='${asset_id}' and r.status=40 and r.receive_account='${account}' limit 1")
//    RepairData getRepariInfoByAssetId(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("account") String account);
//
//
//    //查找设备资产名称 根据设备编号
//    @Select("select a.*,ft.title,c.customer_name as supplier_name,ac.category_name as category_name,at.status as status,il.level_name as importment_level_name from ${schema_name}._sc_asset a " +
//            "left join ${schema_name}._sc_facilities ft on a.intsiteid=ft.id  " +
//            "left join ${schema_name}._sc_asset_category ac on a.category_id=ac.id  " +
//            "left join ${schema_name}._sc_asset_status at on a.intstatus=at.id  " +
//            "left join ${schema_name}._sc_importment_level il on a.importment_level_id=il.id  " +
//            "left join ${schema_name}._sc_customers c on a.supplier=c.id " +
//            "where a.strcode=#{device_code} limit 1")
//    Asset getAssetCommonDataByCode(@Param("schema_name") String schema_name, @Param("device_code") String device_code);
//
//    //查找维修单，查到需维修的设备
//    @Select("select d.* from ${schema_name}._sc_asset d " +
//            "left join ${schema_name}._sc_repair r on d._id=r.asset_id " +
//            "where r.repair_code=#{repaircode} limit 1")
//    Asset getAssetCommonDataByRepairCode(@Param("schema_name") String schema_name, @Param("repaircode") String repairCode);
//
//    //通过来源，查询维修单
//    @Select("select r.* " +
//            "from ${schema_name}._sc_repair r " +
//            "where r.from_code=#{from_code} " +
//            "order by r.repair_code desc ")
//    List<RepairData> getRepairListByFromCode(@Param("schema_name") String schema_name, @Param("from_code") String fromCode);
//
//    @Select("select r.*,u.username as create_user_name,c.username as receive_name,d.username as distribute_name,s.status as status_name," +
//            "a.status as asset_status_name,f.type_name as fault_type_name,t.type_name as repair_type_name,dv.strname as asset_name,dv.strcode as asset_code, " +
//            "string_agg(w.operate_account, ',') as operators,ft.title as facility_name,cus.customer_name as suppilerName " +
//            "from ${schema_name}._sc_repair r " +
//            " left join ${schema_name}._sc_user u on r.create_user_account=u.account " +
//            " left join ${schema_name}._sc_user c on r.receive_account=c.account " +
//            " left join ${schema_name}._sc_user d on r.distribute_account=d.account " +
//            " left join ${schema_name}._sc_status s on r.status=s.id " +
//            " left join ${schema_name}._sc_repair_type t on r.repair_type=t.id " +
//            " left join ${schema_name}._sc_asset_status a on r.asset_status=a.id " +
//            " left join ${schema_name}._sc_fault_type f on r.fault_type=f.id " +
//            " left join ${schema_name}._sc_facilities ft on r.facility_id=ft.id " +
//            " left join ${schema_name}._sc_asset dv on r.asset_id=dv._id " +
//            " left join ${schema_name}._sc_customers cus on dv.supplier=cus.id " +
//            " left join ${schema_name}._sc_repair_work_hour w on r.repair_code=w.repair_code " +
//            "where r.asset_id=(select a._id from ${schema_name}._sc_asset a where a.strcode=#{device_code}) " +
//            "group by r.repair_code, u.username,c.username,d.username,s.status,a.status,f.type_name,t.type_name,dv.strname,dv.strcode,ft.title,cus.customer_name "+
//            "order by repair_code "
//    )
//        //@Select("select r.* from ${schema_name}._sc_repair r where r.asset_id=(select a._id from ${schema_name}._sc_asset a where a.strcode=#{device_code})")
//    List<RepairData> getRepairListByCode(@Param("schema_name") String schema_name, @Param("device_code") String device_code);
//
//
//    //备件统计维修使用数量明细查询by bom_model and stock_code
//    @Select("select w.*,R.use_count as facility_id,c.username as receive_name " +
//            "from ${schema_name}._sc_works_detail w " +
//            "LEFT JOIN ${schema_name}._sc_works wr on w.work_code=wr.work_code " +
//            "LEFT JOIN ${schema_name}._sc_user c on w.receive_account=c.account " +
//            "LEFT JOIN ${schema_name}._sc_work_type wt on w.work_type_id=wt.id " +
//            "LEFT JOIN ${schema_name}._sc_work_bom R on R.sub_work_code=w.sub_work_code " +
//            "LEFT JOIN ${schema_name}._sc_bom b ON b.bom_code = R.bom_code and b.bom_model = R.bom_model and b.material_code=R.material_code " +
//            "where w.is_main=1  ${condition}  " +
//            "order by w.sub_work_code desc ")
//    List<WorkSheet> getRepairDetailByBomModelAndStockCode(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    @Update("update ${schema_name}._sc_repair set " +
//            "status=900 " +
//            "where repair_code=#{repair_code} ")
//    int invalidRepair(@Param("schema_name") String schema_name, @Param("repair_code") String repairCode);
//
//
//    //嘉岩更新添加数据
//    @Insert("insert into ${schema_name}._sc_repair_bom (repair_code,bom_model,bom_code,fetch_man,use_count,material_code,facility_id,stock_code,is_from_service_supplier)" +
//            "values (#{b.repair_code},#{b.bom_model},#{b.bom_code},#{b.fetch_man},#{b.use_count},#{b.material_code},#{b.facility_id},#{b.stock_code},#{b.is_from_service_supplier})")
//    int insertRepairBomJY(@Param("schema_name") String schema_name, @Param("b") RepairBomData repairBomData);
//
//
//
//    @Update("update ${schema_name}._sc_repair set " +
//            "waiting=10 " +
//            "where repair_code=#{repair_code} ")
//    int updateRepairApplyStatus(@Param("schema_name") String schema_name, @Param("repair_code") String code);
//
//    @Update("update ${schema_name}._sc_repair set " +
//            "waiting=#{status} " +
//            "where repair_code=#{repair_code} ")
//    int updateOpenRepairApplyStatus(@Param("schema_name") String schema_name, @Param("repair_code") String code , @Param("status") int status);
//
//    @Select("select r.*  from ${schema_name}._sc_repair_bom r where  r.repair_code=#{repair_code} and r.material_code=#{material_code} ")
//    List<RepairBomData> selectRepairByMaterialCode(@Param("schema_name") String schema_name, @Param("repair_code") String code, @Param("material_code") String material_code );
//
//    @Delete("delete from ${schema_name}._sc_repair_bom  where is_from_service_supplier=1 and repair_code=#{repair_code} ")
//    int updateRepairBomByMaterCode(@Param("schema_name") String schema_name, @Param("repair_code") String code);
//
//    /*全屏面板设备维修故障清单*/
//    @Select("select dv.strname as asset_name," +
//            "t4.status as asset_status_name," +
//            "t2.title as facility_name," +
//            "f.type_name as fault_type_name," +
//            "s.status as status_name," +
//            "u.username as create_user_name," +
//            "c.username as receive_name," +
//            "c.mobile as mobile," +
//            "t1.* " +
//            "from ${schema_name}._sc_repair t1 " +
//            "left join  ${schema_name}._sc_asset as  dv on t1.asset_id=dv._id " +
//            "LEFT JOIN ${schema_name}._sc_facilities t2 on dv.intsiteid=t2.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_status t4 on t1.asset_status=t4.id " +
//            "LEFT JOIN ${schema_name}._sc_fault_type f on t1.fault_type = f.id " +
//            "LEFT JOIN ${schema_name}._sc_user u on t1.create_user_account=u.account " +
//            "left join ${schema_name}._sc_status s on t1.status=s.id " +
//            "left join ${schema_name}._sc_user c on t1.receive_account=c.account " +
//            "where t1.status<60 " +
//            "order by t1.occur_time desc ")
//    /*limit ${page} offset ${begin}*/
//     List<RepairData> queryAssetFailureList(@Param("schema_name") String schema_name,@Param("page") int pageSize, @Param("begin") int begin);
//
//     /*全屏展示面板数据条数*/
//    @Select("select count(p.*) from (select dv.strname from ${schema_name}._sc_repair t1 " +
//            "left join  ${schema_name}._sc_asset as  dv on t1.asset_id=dv._id LEFT JOIN ${schema_name}._sc_facilities t2 on dv.intsiteid=t2.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_status t4 on t1.asset_status=t4.id " +
//            "LEFT JOIN ${schema_name}._sc_fault_type f on t1.fault_type = f.id " +
//            "LEFT JOIN ${schema_name}._sc_user u on t1.create_user_account=u.account " +
//            "left join ${schema_name}._sc_status s on t1.status=s.id " +
//            "left join ${schema_name}._sc_user c on t1.receive_account=c.account " +
//            "where t1.status<60 order by t1.occur_time desc) as p ")
//    public int queryAssetFailureListCount(@Param("schema_name") String schema_name);
//
//    /*故障报修（计划维修）*/
//    @Select("select \n" +
//            "SUM(case when r.status<>900 and r.status<>60 and r.status<>100  then 1 else 0 end) as failureRepair,\n" +
//            "SUM(case when r.status=60 and r.finished_time>current_date   then 1 else 0 end) as completeFailureRepair\n" +
//            "FROM ${schema_name}._sc_repair r")
//    public FullScreenData getPlannedFailureRepair(@Param("schema_name") String schema_name);
}