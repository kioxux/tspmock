package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 地址管理
 * User: sps
 * Date: 2019/04/02
 * Time: 下午15:00
 */
@Mapper
public interface AddressManageMapper {
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = AddressManageMapper.AddressManageMapperProvider.class, method = "query")
//    List<Map<String, Object>> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                                    @Param("pageSize") int pageSize, @Param("begin") int begin,
//                                    @Param("searchKey") String searchKey);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param searchKey
//     * @return
//     */
//    @SelectProvider(type = AddressManageMapper.AddressManageMapperProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey);
//
//    /**
//     * 新增数据
//     *
//     *@param paramMap
//     * @return
//     */
//    @Insert("INSERT INTO ${schema_name}._sc_address " +
//            "(address_name,address_code,postcode,area_code,location,map_type,address,remark,create_user_account,create_time) VALUES  " +
//            " (#{address_name},#{address_code},#{postcode},#{area_code},#{location}:: point,#{map_type}::int,#{address},#{remark},#{create_user_account},current_timestamp) " +
//            " RETURNING id")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int insert(Map<String, Object> paramMap);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Update("update ${schema_name}._sc_address set " +
//            "address_name=#{s.address_name}, " +
//            "address_code=#{s.address_code}, " +
//            "postcode=#{s.postcode}, " +
//            "area_code=#{s.area_code}, " +
//            "location=#{s.location}::point, " +
//            "map_type=#{s.map_type}::int, " +
//            "address=#{s.address}, " +
//            "remark=#{s.remark} " +
//            "where id=#{s.id}::int ")
//    int update(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 删除数据
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Update("DELETE FROM ${schema_name}._sc_address where id=#{id} ")
//    int delete(@Param("schema_name") String schemaName, @Param("id") Integer id);
//
//    /**
//     * 实现类
//     */
//    class AddressManageMapperProvider {
////        /**
////         * 新增数据
////         *
////         * @param schemaName
////         * @param paramMap
////         * @return
////         */
////        public String insert(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap) {
////            return new SQL() {{
////                INSERT_INTO(schemaName + "._sc_address");
////                VALUES("address_name ", "#{s.address_name}");
////                VALUES("address_code ", "#{s.address_code}");
////                VALUES("postcode ", "#{s.postcode}");
////                VALUES("area_code ", "#{s.area_code}");
////                VALUES("location ", "#{s.location}::point");
////                VALUES("map_type ", "#{s.map_type}::int");
////                VALUES("address ", "#{s.address}");
////                VALUES("remark ", "#{s.remark}");
////                VALUES("create_user_account ", "#{s.create_user_account}");
////                VALUES("create_time ", "current_timestamp");
////            }}.toString();
////        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param searchKey
//         * @return
//         */
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT(" id,address_name,address_code,postcode,area_code,location::VARCHAR as location,map_type,address,remark,create_user_account,create_time ");
//                FROM(schemaName + "._sc_address");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("upper(address_name) like upper('%" + searchKey
//                            + "%') or upper(address_code) like upper('%" + searchKey
//                            + "%') or upper(address) like upper('%" + searchKey + "%') ");
//                }
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param searchKey
//         * @return
//         */
//        public String countByCondition(@Param("schema_name") String schemaName, @Param("searchKey") String searchKey) {
//            return new SQL() {{
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_address");
//                // 查询条件
//                if (null != searchKey && !"".equals(searchKey)) {
//                    WHERE("upper(address_name) like upper('%" + searchKey
//                            + "%') or upper(address_code) like upper('%" + searchKey
//                            + "%') or upper(address) like upper('%" + searchKey + "%') ");
//                }
//            }}.toString();
//        }
//
//    }
}
