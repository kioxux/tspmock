package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Map;

/**
 * 编号生成工具
 */
@Mapper
public interface SerialServiceMapper {

    /**
     * 根据业务类型查找编号配置信息
     *
     * @param schema_name   数据库
     * @param business_type 业务类型
     * @return 编号信息
     */
    @Select("select serial_pre, number from ${schema_name}._sc_serial_bussiness where business_type = #{business_type}")
    Map<String, Object> findSerialByType(@Param("schema_name") String schema_name, @Param("business_type") String business_type);

    /**
     * 根据编号前缀查找编号信息
     *
     * @param schema_name 数据库
     * @param serial_pre  前缀
     * @return 编号信息
     */
    @Select("select \"current_date\", reset_type, is_need_pre, max_number from ${schema_name}._sc_serial_number where serial_pre=#{serial_pre}")
    Map<String, Object> findSerialNumberByType(@Param("schema_name") String schema_name, @Param("serial_pre") String serial_pre);


    /**
     * 根据业务的编号前缀，将业务编号的最大值增加一个
     *
     * @param schema_name 数据库
     * @param serial_pre  前缀
     * @param new_number  编号信息
     * @return 成功数量
     */
    @Update("update ${schema_name}._sc_serial_number set max_number=#{new_number} where serial_pre=#{serial_pre}")
    int updateSerialNumberByOne(@Param("schema_name") String schema_name, @Param("serial_pre") String serial_pre, @Param("new_number") int new_number);


    /**
     * 根据业务的编号前缀，更新业务编号的日期，并设置max_number为1
     *
     * @param schema_name  数据库
     * @param serial_pre   前缀
     * @param current_date 日期
     * @return 成功数量
     */
    @Update("update ${schema_name}._sc_serial_number set max_number=1,\"current_date\"=#{current_date} where serial_pre=#{serial_pre}")
    int resetSerialNumberForDate(@Param("schema_name") String schema_name, @Param("serial_pre") String serial_pre, @Param("current_date") String current_date);


    /**
     * 根据业务的编号前缀，将业务编号的当前日期更新
     *
     * @param schema_name  数据库
     * @param serial_pre   前缀
     * @param current_date 日期
     * @return 成功数量
     */
    @Update("update ${schema_name}._sc_serial_number set \"current_date\"=#{current_date} where serial_pre=#{serial_pre}")
    int resetSerialNumberToCurrentDate(@Param("schema_name") String schema_name, @Param("serial_pre") String serial_pre, @Param("current_date") String current_date);
}
