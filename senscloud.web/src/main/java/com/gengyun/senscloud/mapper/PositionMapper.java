package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @description 岗位数据ORM
 * 用于对岗位数据表的插入 删除 更新
 */
@Mapper
public interface PositionMapper {

//    @Update("insert into ${schema_name}._sc_position(positionname,isuse,createtime) values(#{positionName},#{isuse},#{createTime})")
//    void insertPosition(Position position);
//
//    @Update("update ${schema_name}._sc_position set positionname=#{positionName},isuse=#{isuse} where id=#{id}")
//    void updatePosition(Position position);
//
//    @Delete("delete from ${schema_name}._sc_position where id = #{id}")
//    void deletePosition(@Param("schema_name") String schema_name, @Param("id") Long id);
//
//    @Select("select * from ${schema_name}._sc_position where id=#{id}")
//    Position findPosition(@Param("schema_name") String schema_name, @Param("id") Long id);
//
//    @Select("select * from ${schema_name}._sc_position")
//    List<Position> findAllPosition(@Param("schema_name") String schema_name);
}
