package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChargeMapper {
//    @InsertProvider(type = ChargeMapperProvider.class, method = "insert")
//    void insert(@Param("schema_name") String schema_name, @Param("c") Charge charge);
//
//    @UpdateProvider(type = ChargeMapperProvider.class, method = "update")
//    void update(@Param("schema_name") String schema_name, @Param("c") Charge charge);
//
//    @DeleteProvider(type = ChargeMapperProvider.class, method = "delete")
//    void delete(@Param("schema_name") String schema_name, @Param("ids") String ids);
//
//    @SelectProvider(type = ChargeMapperProvider.class, method = "find")
//    List<Charge> find(@Param("schema_name") String schema_name,
//                          @Param("facilities") String facilities,
//                          @Param("categories") String categories,
//                          @Param("accounts") String accounts,
//                          @Param("levels") String levels);
//
//    class ChargeMapperProvider {
//        public String insert(@Param("schema_name") String schema_name, @Param("c") Charge charge) {
//            return new SQL() {{
//                INSERT_INTO(schema_name + "._sc_facility_asset_charge");
//                VALUES("facility_id", "#{c.facility_id}");
//                VALUES("asset_category_id", "#{c.asset_category_id}");
//                VALUES("charge_account", "#{c.charge_account}");
//                VALUES("charge_level", "#{c.charge_level}");
//                VALUES("createtime", "#{c.createtime}");
//                VALUES("create_user_account", "#{c.create_user_account}");
//            }}.toString();
//        }
//
//        public String find(@Param("schema_name") String schema_name,
//                               @Param("facilities") String facilities,
//                               @Param("categories") String categories,
//                               @Param("accounts") String accounts,
//                               @Param("levels") String levels) {
//            return new SQL() {{
//                SELECT("c.facility_id, c.asset_category_id, c.create_user_account," +
//                        "string_agg(c.charge_account, ',') as charge_accounts," +
//                        "string_agg(c.charge_level::VARCHAR, ',') as charge_levels," +
//                        "string_agg(u.username, ',') as charge_account_names," +
//                        "string_agg(c.id::VARCHAR, ',') as ids," +
//                        "f.title as facility_name,ac.category_name as asset_category_name");
//                FROM(schema_name + "._sc_facility_asset_charge c");
//                LEFT_OUTER_JOIN(schema_name + "._sc_user u on u.account=c.charge_account");
//                LEFT_OUTER_JOIN(schema_name + "._sc_facilities f on f.id=c.facility_id");
//                LEFT_OUTER_JOIN(schema_name + "._sc_asset_category ac on ac.id=c.asset_category_id");
//                if (StringUtils.isNotEmpty(facilities)) {
//                    WHERE("c.facility_id in (" + facilities + ")");
//                }
//                if (StringUtils.isNotEmpty(categories)) {
//                    WHERE("c.asset_category_id in (" + categories + ")");
//                }
//                if (StringUtils.isNotEmpty(accounts)) {
//                    WHERE("c.charge_account in (" + accounts + ")");
//                }
//                if (StringUtils.isNotEmpty(levels)) {
//                    WHERE("c.charge_level in (" + levels + ")");
//                }
//                GROUP_BY("c.facility_id, c.asset_category_id, c.create_user_account,facility_name,asset_category_name");
//            }}.toString();
//        }
//
//        public String delete(@Param("schema_name") String schema_name, @Param("ids") String ids) {
//            return new SQL() {{
//                DELETE_FROM(schema_name + "._sc_facility_asset_charge");
//                WHERE("id in (" + ids + ")");
//            }}.toString();
//        }
//
//        public String update(@Param("schema_name") String schema_name, @Param("c") Charge charge) {
//            return new SQL() {{
//                UPDATE(schema_name + "._sc_facility_asset_charge");
//                SET("charge_account = #{c.charge_account}");
//                SET("charge_level = #{c.charge_level}");
//                WHERE("id = #{c.id}");
//            }}.toString();
//        }
//    }
}
