package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 职安健管理
 */
@Mapper
public interface SecurityManageMapper {

    /**
     * 查询职安健列表
     */
    @Select("<script>" +
            "   select security_item_code, security_item_name, rask_note, result_type, create_time, create_user_id, standard " +
            "   from ${schema_name}._sc_security_item where 1=1 " +
            "       <when test='pm.keywordSearch != null'>" +
            "           and (security_item_code like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           security_item_name like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           rask_note like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           standard like CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            "       </when>" +
            "   ${pm.pagination}  " +
            "</script>")
    List<Map<String, Object>> querySecurityManageList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    /**
     * 查询所有职安健列表
     */
    @Select("<script>" +
            "   select security_item_code, security_item_name, rask_note, result_type, create_time, create_user_id, standard, " +
            "   CASE result_type WHEN '1' THEN '是' WHEN '2' then '否' end AS result_type_name " +
            "    from ${schema_name}._sc_security_item where 1=1 " +
            "       <when test='pm.keywordSearch != null'>" +
            "           and (security_item_code like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           security_item_name like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           rask_note like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           standard like CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            "       </when>" +
            "</script>")
    List<Map<String, Object>> findAllSecurityManageList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    /**
     * 查询职安健详情
     */
    @Select("<script>" +
            "   select security_item_code, security_item_name, rask_note, result_type, create_time, create_user_id, standard " +
            "   from ${schema_name}._sc_security_item where security_item_code = #{security_item_code} " +
            "</script>")
    Map<String, Object> getSecurityDetailByCode(@Param("schema_name") String schema_name, @Param("security_item_code") String security_item_code);

    /**
     * 查询职安健数量
     */
    @Select("<script>" +
            "   select count(1) " +
            "   from ${schema_name}._sc_security_item where 1=1 " +
            "       <when test='pm.keywordSearch != null'>" +
            "           and (security_item_code like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           security_item_name like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           rask_note like CONCAT('%', #{pm.keywordSearch}, '%') or " +
            "           standard like CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            "       </when>" +
            "</script>")
    int countSecurityManageList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    /**
     * 新增职安健
     */
    @Insert(" INSERT INTO ${schema_name}._sc_security_item ( " +
            "    security_item_code, security_item_name, rask_note, result_type, create_time, create_user_id, standard" +
            " ) VALUES (" +
            "    #{pm.security_item_code}, #{pm.security_item_name}, #{pm.rask_note}, #{pm.result_type}::int, current_timestamp, #{pm.create_user_id}, #{pm.standard}" +
            " )")
    void insertSecurityManage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    /**
     * 更新职安健
     */
    @Update(" UPDATE ${schema_name}._sc_security_item SET " +
            " security_item_name = #{pm.security_item_name}, rask_note = #{pm.rask_note}, result_type = #{pm.result_type}::int, standard = #{pm.standard} " +
            " where security_item_code = #{pm.security_item_code}")
    void updateSecurityManage(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> paramMap);

    /**
     * 删除职安健
     */
    @Delete(" DELETE FROM ${schema_name}._sc_security_item where security_item_code = #{security_item_code}")
    void deleteSecurityManage(@Param("schema_name") String schema_name, @Param("security_item_code") String security_item_code);

    /**
     * 验证职安健编号是否存在
     *
     * @param schema_name 入参
     * @return 入参
     */
    @Select(" <script>" +
            " SELECT distinct security_item_code, security_item_name,rask_note,result_type,standard,create_user_id,create_time " +
            " FROM ${schema_name}._sc_security_item " +
            " WHERE security_item_code in " +
            "<foreach collection='itemCodes' item='itemCode' open='(' close=')' separator=','> " +
            "#{itemCode} " +
            "</foreach></script>")
    @MapKey("security_item_code")
    Map<String, Map<String, Object>> findSafeItemsByItemCode(@Param("schema_name") String schema_name, @Param("itemCodes") List<String> itemCodeList);
}
