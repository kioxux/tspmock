package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.SearchParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 权限处理
 * User: sps
 * Date: 2020/12/20
 * Time: 上午11:20
 */
@Mapper
public interface PagePermissionMapper {
    /**
     * 获取用户权限主键集合
     *
     * @param schema_name 数据库
     * @param userId      用户主键
     * @return 校验数量
     */
//    @Select(" SELECT DISTINCT sp.key as key from ${schema_name}._sc_user_position up " +
//            " INNER JOIN ${schema_name}._sc_position_role pr on up.position_id = pr.position_id " +
//            " INNER JOIN ${schema_name}._sc_role ur on ur.id = pr.role_id " +
//            " INNER JOIN ${schema_name}._sc_role_permission rp on pr.role_id = rp.role_id" +
//            " INNER JOIN public.system_permission sp on sp.id = rp.permission_id and sp.available = true " +
//            " where up.user_id = #{userId} ")
//    List<String> findUserPrmListByInfo(@Param("schema_name") String schema_name, @Param("userId") String userId);

    /**
     * 获取用户权限主键集合
     *
     * @param schema_name 数据库
     * @param userId      用户主键
     * @return 校验数量
     */
    @Select(" SELECT rp.permission_id from ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position_role pr on up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur on ur.id = pr.role_id " +
            " INNER JOIN ${schema_name}._sc_role_permission rp on pr.role_id = rp.role_id" +
            " where up.user_id = #{userId} ")
    List<String> findUserPrmListByInfo(@Param("schema_name") String schema_name, @Param("userId") String userId);

//    /**
//     * 根据权限类型获取所有父级节点权限集合（父级+本身）
//     *
//     * @param key 权限类型
//     * @return 权限集合
//     */
//    @Select(" WITH RECURSIVE re (id, parent_id, key) AS ( " +
//            " SELECT t.id, t.parent_id, t.key FROM system_permission t where key = #{key} " +
//            " UNION ALL" +
//            " SELECT t2.id, t2.parent_id, t2.key FROM system_permission t2, re e WHERE e.parent_id=t2.id" +
//            " ) select key from re ")
//    List<String> findParentPrmListById(@Param("key") String key);


//    /**
//     * 根据权限类型获取所有父级节点权限集合（子级+本身）
//     *
//     * @param key 权限类型
//     * @return 权限集合
//     */
//    @Select(" WITH RECURSIVE re (id, parent_id, key) AS ( " +
//            " SELECT t.id, t.parent_id, t.key FROM system_permission t where t.key = #{key} and t.type < 3 and t.available = true " +
//            " UNION ALL" +
//            " SELECT t2.id, t2.parent_id, t2.key FROM system_permission t2, re e WHERE e.id = t2.parent_id and t2.type < 3 and t2.available = true" +
//            " ) select key from re ")
//    List<String> findSubPrmListById(@Param("key") String key);

    /**
     * 设备数据权限校验
     *
     * @param schema_name 数据库
     * @param ids         设备主键
     * @param userId      用户主键
     * @return 校验数量
     */
    @Select("<script>" +
            "SELECT count(1) FROM ${schema_name}._sc_asset t1 " + SqlConstant.ASSET_DATA_PRM_SQL
            + " where t1.id IN " +
            "       <foreach collection='ids' item='id' open='(' close=')' separator=','>#{id}</foreach>" +
            "</script>")
    int dealCheckAssetDataPrm(@Param("schema_name") String schema_name, @Param("ids") String[] ids, @Param("userId") String userId);

    /**
     * 根据设备编码校验有效设备数据权限
     *
     * @param schema_name 数据库
     * @param ids         设备主键
     * @param userId      用户主键
     * @return 校验数量
     */
    @Select("<script>" +
            "SELECT count(1) FROM ${schema_name}._sc_asset t1 " + SqlConstant.ASSET_DATA_PRM_SQL
            + " where status > " + StatusConstant.STATUS_DELETEED + " and t1.asset_code IN " +
            "       <foreach collection='ids' item='id' open='(' close=')' separator=','>#{id}</foreach>" +
            "</script>")
    int dealCheckAssetDataPrmByCode(@Param("schema_name") String schema_name, @Param("ids") String[] ids, @Param("userId") String userId);

    /**
     * 根据备件Id校验有效备件数据权限
     *
     * @param schema_name 数据库
     * @param ids         设备主键
     * @param userId      用户主键
     * @return 校验数量
     */
    @Select("<script>" +
            "SELECT count(1) FROM ${schema_name}._sc_bom t1 "
            + " where status > " + StatusConstant.STATUS_DELETEED + " and t1.id IN " +
            "       <foreach collection='ids' item='id' open='(' close=')' separator=','>#{id} </foreach>" +
            "</script>")
    int dealCheckBomDataPrmByCode(@Param("schema_name") String schema_name, @Param("ids") String[] ids, @Param("userId") String userId);

    /**
     * 获取用户有权限的设备类型
     *
     * @param schema_name 数据库
     * @param userId      用户主键
     * @return 校验数量
     */
    @Select(" SELECT distinct ac.ID as value, ac.category_name as text, ac.parent_id as parent, ac.category_code, ac.data_order " +
            " from ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position_role pr on up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur on ur.id = pr.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_type rat on pr.role_id = rat.role_id " +
            " INNER JOIN ${schema_name}._sc_asset_category ac ON rat.category_id = ac.ID AND ac.is_use = '1' " +
            " where up.user_id = #{userId} order by data_order ")
    List<Map<String, Object>> findPrmAssetCategoryList(@Param("schema_name") String schema_name, @Param("userId") String userId);

    /**
     * 取货币单位
     */
    @Select("select t.id as code, t.id as value,  t.currency_code as desc, t.currency_code, t.currency_code as text,t.currency_name as name " +
            "from ${schema_name}._sc_currency t  WHERE t.is_use = '1' " +
            "order by t.data_order ")
    List<Map<String, Object>> findCurrencyList(@Param("schema_name") String schemaName);

    /**
     * 获取设备类型下拉框（去除自己和子集）
     */
    @Select("  SELECT id as value, category_name as text , parent_id as parent from  " +
            "  ${schema_name}._sc_asset_category WHERE is_use ='1' and id NOT IN (WITH RECURSIVE re (id, parent_id) AS ( " +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_asset_category t where t.id = #{id}  " +
            " UNION ALL" +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_asset_category t2 , re e WHERE e.id = t2.parent_id" +
            " ) select id from re ) ")
    List<Map<String, Object>> findAssetCategoryList(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 获取带权限的设备类型
     */
    @Select(" SELECT distinct ac.id as value ,ac.category_name as text ,ac.parent_id as parent " +
            " FROM  ${schema_name}._sc_asset_category AS ac" +
            " INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON rat.category_id = ac.id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rat.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{id} ")
    List<Map<String, Object>> findAssetCategoryPermissionList(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 获取备件类型下拉框（去除自己和子集）
     */
    @Select("  SELECT id as value, type_name as text , parent_id as parent from  " +
            "  ${schema_name}._sc_bom_type WHERE is_use ='1' and id NOT IN (WITH RECURSIVE re (id, parent_id) AS ( " +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_bom_type t where t.id = #{id}  " +
            " UNION ALL" +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_bom_type t2 , re e WHERE e.id = t2.parent_id" +
            " ) select id from re ) ")
    List<Map<String, Object>> findBomTypeList(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 查询该设备类型下的设备型号
     *
     * @param schemaName 入参
     * @param id         入参
     * @return 设备型号列表
     */
    @Select("   SELECT a.id::varchar as value, a.model_name as text  " +
            "   FROM ${schema_name}._sc_asset_model a" +
            "   WHERE a.is_use='1'  AND a.category_id = #{id} " +
            "   ORDER BY a.id desc  ")
    List<Map<String, Object>> findAssetModelCategoryList(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 获取设备位置下拉框（去除自己和子集）
     */
    @Select("  <script> SELECT position_code as value, position_name as text , position_code as parent from  " +
            "  ${schema_name}._sc_asset_position " +
            "  WHERE status > " + StatusConstant.STATUS_DELETEED +
            "  AND position_code not in " +
            " <foreach collection='list' item='code' open='(' close=')' separator=','> " +
            " #{code} " +
            " </foreach> </script>")
    List<Map<String, Object>> findNotInAssetPositionList(@Param("schema_name") String schemaName, @Param("list") List<String> list);

    @Select(" WITH RECURSIVE re (position_code, parent_code) AS ( " +
            " SELECT t.position_code, t.parent_code FROM ${schema_name}._sc_asset_position t where t.position_code = #{id} AND t.status  > " + StatusConstant.STATUS_DELETEED +
            " UNION ALL" +
            " SELECT t2.position_code, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e WHERE e.position_code = t2.parent_code AND t2.status > " + StatusConstant.STATUS_DELETEED +
            " ) select position_code from re  ")
    List<String> findAssetPositionList(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 获取厂商下拉框（去除自己和子集）
     */
    @Select("  SELECT id as value, short_title as text , parent_id as parent from  " +
            "  ${schema_name}._sc_facilities WHERE  parent_id NOT IN (WITH RECURSIVE re (id, parent_id) AS ( " +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_facilities t where t.id = #{id}  " +
            " UNION ALL" +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_facilities t2, re e WHERE e.id = t2.parent_id" +
            " ) select id from re ) ")
    List<Map<String, Object>> findFacilitiesList(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 带权限的设备位置下拉框
     */
    @Select(" SELECT DISTINCT ap.position_code as value ,ap.position_name as text ,ap.parent_code as parent,ap.location::varchar,ap.layer_path,ap.file_id,ap.position_type_id,ap.remark " +
            " FROM  ${schema_name}._sc_asset_position AS ap" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{id}  AND ap.status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findAssetPositionPermissionList(@Param("schema_name") String schemaName, @Param("id") String id);

    @Select(" SELECT DISTINCT ap.position_code as value ,ap.remark as text,ap.remark, ap.position_name ,ap.parent_code as parent,ap.location::varchar,ap.layer_path,ap.file_id,ap.position_type_id " +
            " FROM  ${schema_name}._sc_asset_position AS ap" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{userId}  AND ap.status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findAssetPositionRemarkList(@Param("schema_name") String schemaName, @Param("userId") String userId);

    /**
     * 带权限的工单类型
     */
    @Select(" SELECT DISTINCT wt.id as value ,wt.type_name as text  " +
            " FROM  ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.id = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id} AND wt.is_use = '1'  ")
    List<Map<String, Object>> findWorkTypePermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    /**
     * 带权限的工单类型
     */
    @Select(" SELECT DISTINCT wt.id as value ,wt.type_name as text  " +
            " FROM  ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.id = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id} AND wt.is_use = '1' AND wt.business_type_id < 2000  ")
    List<Map<String, Object>> findWorkListTypePermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    /**
     * 带权限的流程类型(待办列表用)
     */
    @Select(" SELECT DISTINCT wt.id as value ,wt.type_name as text  " +
            " FROM ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON wt.ID = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_work_flow_info AS wfi ON wfi.work_type_id = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " WHERE up.user_id = #{user_id} AND wt.is_use = '1'   ")
    List<Map<String, Object>> findTaskListTypePermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    /**
     * 带权限的工单类型
     */
    @Select(" SELECT * from ( SELECT DISTINCT" +
            " concat ( 'b', bt.business_type_id :: VARCHAR ) AS value," +
            " bt.business_name AS text," +
            " 0 :: VARCHAR AS parent, min(wt.data_order) as data_order " +
            " FROM" +
            " ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_data_permission AS rwt ON rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND wt.ID = rwt.data_id " +
            " INNER JOIN ${schema_name}._sc_work_flow_info AS wfi ON wfi.work_type_id = wt.ID" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " INNER JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id " +
            " WHERE wt.is_use = '1' AND " +
            " wt.business_type_id < 2000 AND  up.user_id = #{user_id}" +
            " GROUP BY bt.business_type_id, bt.business_name" +
            " UNION" +
            " SELECT DISTINCT" +
            " wt.ID :: VARCHAR AS VALUE," +
            " wt.type_name AS TEXT," +
            " concat ( 'b', wt.business_type_id :: VARCHAR ) AS parent, wt.data_order " +
            " FROM ${schema_name}._sc_role_data_permission AS rwt " +
            " INNER JOIN ${schema_name}._sc_work_type AS wt ON rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND wt.ID = rwt.data_id " +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id " +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id " +
            " WHERE rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND " +
            " wt.business_type_id < 2000 AND up.user_id = #{user_id} AND wt.is_use = '1') t order by parent, data_order, value ")
    List<Map<String, Object>> findWorksTypePermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    @Select(" SELECT * from ( SELECT DISTINCT" +
            " concat ( 'b', bt.business_type_id :: VARCHAR ) AS value," +
            " bt.business_name AS text," +
            " 0 :: VARCHAR AS parent, min(wt.data_order) as data_order " +
            " FROM" +
            " ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_data_permission AS rwt ON rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND wt.ID = rwt.data_id " +
            " INNER JOIN ${schema_name}._sc_work_flow_info AS wfi ON wfi.work_type_id = wt.ID" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " INNER JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id " +
            " WHERE" +
            "  up.user_id = #{user_id}" +
            " AND wt.is_use = '1' AND wt.business_type_id > 2000  AND wt.business_type_id < 3000 AND up.user_id = #{user_id}" +
            " GROUP BY bt.business_type_id, bt.business_name" +
            " UNION" +
            " SELECT DISTINCT" +
            " wt.ID :: VARCHAR AS VALUE," +
            " wt.type_name AS TEXT," +
            " concat ( 'b', wt.business_type_id :: VARCHAR ) AS parent, wt.data_order " +
            " FROM ${schema_name}._sc_role_data_permission AS rwt " +
            " INNER JOIN ${schema_name}._sc_work_type AS wt ON rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND wt.ID = rwt.data_id " +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id " +
            " WHERE rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND " +
            "  up.user_id = #{user_id} AND wt.is_use = '1' AND  wt.business_type_id > 2000  AND wt.business_type_id < 3000 AND  up.user_id = #{user_id}) t order by parent, data_order, value ")
    List<Map<String, Object>> findWorksTypeWithAssetPermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    @Select(" SELECT * from ( SELECT DISTINCT " +
            " concat ( 'b', bt.business_type_id :: VARCHAR ) AS value," +
            " bt.business_name AS text," +
            " 0 :: VARCHAR AS parent, min(wt.data_order) as data_order " +
            " FROM" +
            " ${schema_name}._sc_work_type AS wt" +
            " INNER JOIN ${schema_name}._sc_role_data_permission AS rwt ON rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND wt.ID = rwt.data_id " +
            " INNER JOIN ${schema_name}._sc_work_flow_info AS wfi ON wfi.work_type_id = wt.ID" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " INNER JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id " +
            " WHERE" +
            "  up.user_id = #{user_id} and wt.is_use = '1' " +
            "  AND wt.business_type_id > 3000  AND wt.business_type_id < 4000 AND up.user_id = #{user_id} " +
            " GROUP BY bt.business_type_id, bt.business_name" +
            " UNION" +
            " SELECT DISTINCT" +
            " wt.ID :: VARCHAR AS VALUE," +
            " wt.type_name AS TEXT," +
            " concat ( 'b', wt.business_type_id :: VARCHAR ) AS parent, wt.data_order " +
            " FROM ${schema_name}._sc_role_data_permission AS rwt " +
            " INNER JOIN ${schema_name}._sc_work_type AS wt ON rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND wt.ID = rwt.data_id " +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id " +
            " WHERE rwt.data_type = '" + Constants.ROLE_DATA_PRM_TYPE + "' AND " +
            "  up.user_id = #{user_id} and wt.is_use = '1' " +
            "  AND wt.business_type_id > 3000  AND wt.business_type_id < 4000  AND  up.user_id = #{user_id}) t order by parent, data_order, value")
    List<Map<String, Object>> findWorksTypeWithBomPermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    @Select(" SELECT DISTINCT" +
            " wt.ID :: VARCHAR AS VALUE," +
            " wt.type_name AS TEXT" +
            " FROM ${schema_name}._sc_role_work_type AS rwt " +
            " INNER JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = rwt.work_type_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rwt.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id " +
            " WHERE" +
            "  up.user_id = #{user_id} and wt.is_use = '1'  " +
            "  AND wt.business_type_id > 3000  AND wt.business_type_id < 4000  AND  up.user_id = #{user_id}")
    List<Map<String, Object>> findWorkTypeWithBomPermissionList(@Param("schema_name") String schemaName, @Param("user_id") String user_id);


    /**
     * 带权限的设备位置树查询
     *
     * @param schemaName 入参
     * @param userId     入参
     * @param param      入参
     * @return 带权限的设备位置树查询
     */
    @Select("<script>" +
            "WITH RECURSIVE le as (  " +
            "select p.position_code,p.position_name,p.parent_code,p.scada_config   " +
            "from ${schema_name}._sc_asset_position p    " +
            "INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON p.position_code = rap.asset_position_code   " +
            "INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id   " +
            "INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id  " +
            "WHERE up.user_id = #{user_id}  " +
            "and p.status > " + StatusConstant.STATUS_DELETEED +
            "<when test = \"c.keywordSearch != null and c.keywordSearch!=''\">   " +
            "and lower(p.position_name) like concat('%',lower(#{c.keywordSearch}),'%')   " +
            "</when>  " +
            "union all    " +
            "select e2.position_code,e2.position_name,e2.parent_code,e2.scada_config   " +
            "from ${schema_name}._sc_asset_position e2,le e3 where e3.parent_code = e2.position_code )    " +
            "select distinct position_code as value,position_name as text ,parent_code as parent,position_name as label, " +
            "case when scada_config is not null and scada_config != '' then '1' else '0' end as is_scada " +
            " from le order by parent_code asc, position_code " +
            "</script>")
    List<Map<String, Object>> findAssetPositionTree(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("c") SearchParam param);

    @Select(" <script>" +
            " SELECT a.*,a.id AS value,a.asset_name AS text FROM ${schema_name}._sc_asset a " +
            " <when test='words!=null'>  " +
            " where a.asset_name like CONCAT('%', #{words}, '%')  " +
            " OR  a.asset_code like CONCAT('%', #{words}, '%')  " +
            " </when>  " +
            " </script>")
    List<Map<String, Object>> testAssetList(@Param("schema_name") String schemaName, @Param("words") String words);

    /**
     * 根据角色id获取用户列表
     *
     * @param schemaName 入参
     * @param roleId     入参
     * @return 用户列表
     */
    @Select(" SELECT DISTINCT u.user_name AS text,u.id::varchar AS value " +
            " FROM ${schema_name}._sc_user AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.id = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " WHERE pr.role_id = #{roleId} AND u.status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findUsersByRoleId(@Param("schema_name") String schemaName, @Param("roleId") String roleId);


    /**
     * 根据井号获取设备出库单号
     *
     * @param schemaName   入参
     * @param selectExtKey 入参
     * @return 设备出库单号
     */
    @Select("SELECT DISTINCT work_code as value,work_code as text FROM ${schema_name}._sc_asset_works aw where bak_value1 = #{well_no}")
    List<Map<String, Object>> findAssetOutCodeByWellNo(@Param("schema_name") String schemaName, @Param("well_no") String selectExtKey);

    /**
     * 根据井号获取设备出库单号
     *
     * @param schemaName   入参
     * @param selectExtKey 入参
     * @return 设备出库单号
     */
    @Select(" <script>" +
            " SELECT DISTINCT bak_value1 as value,bak_value1 as text " +
            " FROM ${schema_name}._sc_asset_works aw " +
            " where status=60 AND bak_value1 IS NOT NULL " +
            " <when test='selectExtKey!=null'>  " +
            " AND work_code like CONCAT('%', #{selectExtKey}, '%') " +
            " </when>  " +
            " </script>")
    List<Map<String, Object>> findAssetWorksWellNo(@Param("schema_name") String schemaName, @Param("selectExtKey") String selectExtKey);

    /**
     * 根据角色id和设备位置，设备类型，工单类型，库房，任意条件进行筛选用户
     *
     * @param schemaName    入参
     * @param role_id       入参
     * @param position_code 入参
     * @param work_type_id  入参
     * @param stock_id      入参
     * @param category_id   入参
     * @return 用户列表
     */
    @Select(" <script>" +
            " SELECT DISTINCT u.id AS value,u.user_name AS text" +
            " FROM ${schema_name}._sc_user AS u " +
            " <when test='role_id!=null'>" +
            " INTERSECT" +
            " SELECT DISTINCT u.id AS value,u.user_name AS text" +
            " FROM ${schema_name}._sc_user AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.id = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " WHERE u.status =1 AND is_use='1' AND pr.role_id=#{role_id}" +
            " </when>" +
            " <when test='position_code!=null'>" +
            " INTERSECT" +
            " SELECT DISTINCT u.id AS value,u.user_name AS text  " +
            " FROM ${schema_name}._sc_user AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON pr.role_id = rap.role_id " +
            " WHERE u.status = 1  AND u.is_use = '1'  AND rap.asset_position_code IN " +
            "  (WITH RECURSIVE re (position_code, parent_code) AS ( " +
            " SELECT t.position_code, t.parent_code FROM ${schema_name}._sc_asset_position t where t.position_code = #{position_code} AND t.status > " + StatusConstant.STATUS_DELETEED +
            " UNION ALL" +
            " SELECT t2.position_code, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e WHERE e.position_code = t2.parent_code AND t2.status > " + StatusConstant.STATUS_DELETEED +
            " ) select position_code from re ) " +
            " </when> " +
            " <when test='work_type_id!=null'>" +
            " INTERSECT " +
            " SELECT DISTINCT u.id AS value,u.user_name AS text   " +
            " FROM ${schema_name}._sc_user AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_work_type AS rwt ON pr.role_id = rwt.role_id " +
            " WHERE u.status = 1 AND u.is_use = '1'  AND rwt.work_type_id =#{work_type_id} " +
            " </when> " +
            " <when test='category_id!=null'>" +
            " INTERSECT" +
            " SELECT DISTINCT u.id AS value,u.user_name AS text   " +
            " FROM ${schema_name}._sc_user  AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_asset_type AS rat ON pr.role_id = rat.role_id " +
            " WHERE u.status = 1 AND u.is_use = '1' AND rat.category_id IN " +
            "  (WITH RECURSIVE re (id, parent_id) AS ( " +
            " SELECT t.id, t.parent_id FROM ${schema_name}._sc_asset_category t where t.id = #{category_id}  " +
            " UNION ALL" +
            " SELECT t2.id, t2.parent_id FROM ${schema_name}._sc_asset_category t2, re e WHERE e.id = t2.parent_id  " +
            " ) select id from re ) " +
            " </when> " +
            " <when test='stock_id!=null'>" +
            " INTERSECT" +
            " SELECT DISTINCT u.id AS value,u.user_name AS text   " +
            " FROM ${schema_name}._sc_user  AS u" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON u.ID = up.user_id" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON up.position_id = pr.position_id" +
            " INNER JOIN ${schema_name}._sc_role_stock AS rs ON pr.role_id = rs.role_id " +
            " WHERE u.status = 1 AND u.is_use = '1' AND rs.stock_id =#{stock_id}" +
            " </when> " +
            " </script>")
    List<Map<String, Object>> findUserByPermission(@Param("schema_name") String schemaName,
                                                   @Param("role_id") String role_id,
                                                   @Param("position_code") String position_code,
                                                   @Param("work_type_id") Integer work_type_id,
                                                   @Param("stock_id") Integer stock_id,
                                                   @Param("category_id") Integer category_id);

//    @Select("<script> " +
//            " SELECT parent_id as parent, f.id as value, short_title as text,f.org_type_id,f.inner_code, " +
//            " case when cd.is_lang = '" + Constants.STATIC_LANG + "' then coalesce((select (c.resource->>cd.name)::jsonb->>#{langKey} from public.company_resource c where c.company_id = #{companyId}), cd.name) else cd.name end as org_type_name " +
//            " FROM ${schema_name}._sc_facilities f " +
//            " LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
//            " where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED +
//            " AND not exists (SELECT 1 FROM ${schema_name}._sc_asset_organization o WHERE o.asset_id = #{asset_id} AND o.org_id = f.id) " +
//            " <if test=\"keyword != null and keyword != ''\"> " +
//            " AND( lower(f.short_title) like concat('%',lower(#{keyword}), '%') or " +
//            " lower(f.title) like concat('%',lower(#{keyword}), '%') or " +
//            " lower(f.inner_code) like concat('%',lower(#{keyword}), '%')) " +
//            " </if> " +
//            " <if test=\"org_type != null and org_type != ''\"> " +
//            " and f.org_type_id = #{org_type}::int " +
//            " </if> " +
//            " ORDER BY f.id " +
//            "</script> ")
//    List<Map<String, Object>> findAllFacilityWithType(@Param("schema_name") String schemaName, @Param("companyId") Long companyId, @Param("langKey") String langKey, @Param("asset_id") String asset_id, @Param("keyword") String keyword, @Param("org_type") String org_type);

    @Select("<script> " +
            " SELECT parent_id as parent, f.id as value, short_title as text,f.org_type_id,f.inner_code, " +
            " case when cd.is_lang = '" + Constants.STATIC_LANG + "' then cd.name else cd.name end as org_type_name " +
            " FROM ${schema_name}._sc_facilities f " +
            " LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
            " where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED +
            " AND not exists (SELECT 1 FROM ${schema_name}._sc_asset_organization o WHERE o.asset_id = #{asset_id} AND o.org_id = f.id) " +
            " <if test=\"keyword != null and keyword != ''\"> " +
            " AND( lower(f.short_title) like concat('%',lower(#{keyword}), '%') or " +
            " lower(f.title) like concat('%',lower(#{keyword}), '%') or " +
            " lower(f.inner_code) like concat('%',lower(#{keyword}), '%')) " +
            " </if> " +
            " <if test=\"org_type != null and org_type != ''\"> " +
            " and f.org_type_id = #{org_type}::int " +
            " </if> " +
            " ORDER BY f.id " +
            "</script> ")
    List<Map<String, Object>> findAllFacilityWithType(@Param("schema_name") String schemaName, @Param("asset_id") String asset_id, @Param("keyword") String keyword, @Param("org_type") String org_type);

//    @Select("<script> " +
//            " SELECT parent_id as parent, f.id as value, short_title as text,f.org_type_id,f.inner_code, " +
//            " case when cd.is_lang = '" + Constants.STATIC_LANG + "' then coalesce((select (c.resource->>cd.name)::jsonb->>#{langKey} from public.company_resource c where c.company_id = #{companyId}), cd.name) else cd.name end as org_type_name " +
//            " FROM ${schema_name}._sc_facilities f " +
//            " LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
//            " where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED +
//            " AND f.org_type_id = 2 " +
//            " ORDER BY f.id " +
//            "</script> ")
//    List<Map<String, Object>> findInnerFacilityWithType(@Param("schema_name") String schemaName, @Param("companyId") Long companyId, @Param("langKey") String userLang);

    @Select("<script> " +
            " SELECT parent_id as parent, f.id as value, short_title as text,f.org_type_id,f.inner_code, " +
            " case when cd.is_lang = '" + Constants.STATIC_LANG + "' then cd.name else cd.name end as org_type_name " +
            " FROM ${schema_name}._sc_facilities f " +
            " LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
            " where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED +
            " AND f.org_type_id = 2 " +
            " ORDER BY f.id " +
            "</script> ")
    List<Map<String, Object>> findInnerFacilityWithType(@Param("schema_name") String schemaName);

    @Select("<script> " +
            " SELECT DISTINCT u.account,u.user_name as text,u.user_code,u.id as value " +
            " FROM ${schema_name}._sc_user u " +
            " INNER JOIN ${schema_name}._sc_user_position up on u.id=up.user_id " +
            " INNER JOIN ${schema_name}._sc_position_role pr on up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur on ur.id = pr.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_type rat on pr.role_id = rat.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_position rap on pr.role_id = rap.role_id " +
            " INNER JOIN ${schema_name}._sc_asset a ON a.position_code = rap.asset_position_code AND a.category_id = rat.category_id " +
            " where a.id = #{asset_id} and u.account is not null and u.status> " + StatusConstant.STATUS_DELETEED +
            " <if test=\"keyword != null and keyword != ''\"> " +
            " AND( lower(u.user_name) like concat('%',lower(#{keyword}), '%') or " +
            " lower(u.account) like concat('%',lower(#{keyword}), '%')) " +
            " </if> " +
            " ORDER BY u.id " +
            "</script> ")
    List<Map<String, Object>> findUserWithAssetPermission(@Param("schema_name") String schemaName, @Param("asset_id") String asset_id, @Param("keyword") String keyword);


//    /**
//     * 获取有安群库存短信接收权限的人
//     */
//    @Select(" SELECT DISTINCT u.account,u.user_name,u.mobile,u.id,u.user_code,sp.key" +
//            " FROM" +
//            " ${schema_name}._sc_role_permission rp" +
//            " INNER JOIN public.system_permission AS  sp on sp.id = rp.permission_id" +
//            " INNER JOIN ${schema_name}._sc_position_role AS pr ON rp.role_id = pr.role_id" +
//            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
//            " INNER JOIN ${schema_name}._sc_user AS u ON up.user_id = u.ID " +
//            " WHERE" +
//            " SP.KEY = 'bom_limit_sms' AND u.status>-1000 AND u.is_use = '1' ")
//    List<Map<String, Object>> findBomSmsPermissionUserList(@Param("schema_name") String schemaName);

    @Select(" SELECT DISTINCT u.account,u.user_name,u.mobile,u.id,u.user_code,rp.permission_id,'bom_limit_sms' as key" +
            " FROM" +
            " ${schema_name}._sc_role_permission rp" +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON rp.role_id = pr.role_id" +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id" +
            " INNER JOIN ${schema_name}._sc_user AS u ON up.user_id = u.ID " +
            " INNER JOIN ${schema_name}._sc_role_stock AS rs ON rs.role_id = pr.role_id and rs.stock_id = #{stock_id} ::int " +
            " WHERE u.status>-1000 AND u.is_use = '1' ")
    List<Map<String, Object>> findBomSmsPermissionUserList(@Param("schema_name") String schemaName, @Param("stock_id") int stockId);

    /**
     * 模糊搜索备件流程权限库房
     *
     * @param schemaName    数据库
     * @param keywordSearch 关键字
     * @param userId        当前用户
     * @param workTypeId    　工单类型主键
     * @return 权限库房列表
     */
    @Select(" <script>SELECT s.id::varchar as value, s.stock_name as text, stock_name, s.id as stock_id " +
            " FROM ${schema_name}._sc_stock s " +
            " INNER JOIN ( SELECT distinct wt.work_type_id, s.stock_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "   WHERE up.user_id = #{userId} and s.stock_id is not null and wt.work_type_id = #{workTypeId} " +
            "  ) p on s.id = p.stock_id " +
            " where s.status=1 AND s.is_use ='1'" +
            " <if test=\"keywordSearch != null and keywordSearch != ''\"> " +
            " and lower(s.stock_name) like concat('%', lower(#{keywordSearch}),'%') " +
            " </if> </script>")
    List<Map<String, Object>> findBwPrmStockList(@Param("schema_name") String schemaName, @Param("keywordSearch") String keywordSearch,
                                                 @Param("userId") String userId, @Param("workTypeId") Integer workTypeId);

    /**
     * 带权限的库房
     *
     * @param schemaName    数据库
     * @param userId        当前用户
     * @return 权限库房列表
     */
    @Select(" <script>SELECT s.id::varchar as value, s.stock_name as text, stock_name, s.id as stock_id " +
            " FROM ${schema_name}._sc_stock s " +
            " INNER JOIN ( SELECT distinct s.stock_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "   WHERE up.user_id = #{userId} and s.stock_id is not null " +
            "  ) p on s.id = p.stock_id " +
            " where s.status=1 AND s.is_use ='1'" +
            " </script>")
    List<Map<String, Object>> findBomPositionPermissionList(@Param("schema_name") String schemaName, @Param("userId") String userId);

    /**
     * 备件盘点库房列表选择
     *
     * @param schemaName    数据库
     * @param keywordSearch 关键字
     * @param userId        当前用户
     * @param workTypeId    　工单类型主键
     * @param bomTypeIdStr    　备件类型
     * @return 权限库房下备件列表
     */
    @Select(" <script>" +
            "select tb.*,inv.user_id inventory_user_id from (" +
            "SELECT stock.stock_id relation_id,stock.stock_name,COALESCE ( SUM ( stock.quantity ), 0 ) stock_count," +
            "ARRAY_TO_STRING(array_agg(DISTINCT bm.type_id), ',') bom_type_id " +
            "FROM (" +
            "   SELECT s.ID :: VARCHAR AS VALUE,s.stock_name AS TEXT,stock_name,s.ID AS stock_id,bs.bom_id,bs.quantity FROM ${schema_name}._sc_stock s " +
            "       INNER JOIN (SELECT DISTINCT wt.work_type_id,s.stock_id FROM ${schema_name}._sc_user_position up " +
            "       INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "       INNER JOIN ${schema_name}._sc_role ur ON ur.ID = pr.role_id " +
            "       LEFT JOIN ${schema_name}._sc_role_work_type wt ON ur.ID = wt.role_id " +
            "       LEFT JOIN ${schema_name}._sc_role_stock s ON ur.ID = s.role_id " +
            "   WHERE up.user_id = #{userId} AND s.stock_id IS NOT NULL AND wt.work_type_id = #{workTypeId} " +
            ") P ON s.ID = P.stock_id " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON bs.stock_id = s.ID " +
            "WHERE s.status = 1 AND s.is_use = '1' " +

            ") stock LEFT JOIN (SELECT b.ID,b.bom_name,b.material_code,b.type_id FROM ${schema_name}._sc_bom b " +
            "WHERE b.status = 1 AND b.is_use = '1' " +

            ") bm ON bm.ID = stock.bom_id " +
            "<where>" +
                " <if test=\"keywordSearch != null and keywordSearch != ''\"> " +
                "   and lower(stock.stock_name) like concat('%', lower(#{keywordSearch}),'%') " +
                " </if>" +
                " <if test=\"bomTypeId != null and bomTypeId != ''\"> " +
                "   and bm.type_id IN " +
                "   <foreach collection='bomTypeId.split(\",\")' item='item' open='(' close=')' separator=','> " +
                "   #{item} :: int " +
                "   </foreach>" +
                " </if>" +
            "</where> " +
            "GROUP BY " +
            "stock.stock_id,stock.stock_name" +
            ") tb LEFT JOIN (SELECT DISTINCT (array_agg (up.user_id ))[1] user_id,s.stock_id FROM ${schema_name}._sc_user_position up " +
            "INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "INNER JOIN ${schema_name}._sc_role ur ON ur.ID = pr.role_id " +
            "LEFT JOIN ${schema_name}._sc_role_work_type wt ON ur.ID = wt.role_id " +
            "LEFT JOIN ${schema_name}._sc_role_stock s ON ur.ID = s.role_id " +
            "WHERE s.stock_id IS NOT NULL AND wt.work_type_id = #{workTypeId} AND ur.ID = #{dealRoleId} " +
            "GROUP BY s.stock_id" +
            ") inv ON inv.stock_id = tb.relation_id " +
            "</script>")
    List<Map<String, Object>> findBwPrmStockBomList(@Param("schema_name") String schemaName, @Param("keywordSearch") String keywordSearch,
                                                 @Param("userId") String userId, @Param("workTypeId") Integer workTypeId, @Param("bomTypeId") String bomTypeIdStr, @Param("dealRoleId") String dealRoleId);

    /**
     * 备件盘点库房列表选择
     *
     * @param schemaName    数据库
     * @param keywordSearch 关键字
     * @param userId        当前用户
     * @param bomId    　库房ID
     * @param bomTypeId   　备件类型
     * @return 权限库房下备件列表
     */
    @Select("<script>" +
            "SELECT b.ID bom_id,b.bom_name,b.material_code,bt.id bom_type_id,bt.type_name,COALESCE ( SUM ( bs.quantity ), 0 ) quantity,COALESCE ( SUM ( bs.quantity ), 0 ) inv_count,'0' profit_and_loss " +
            "FROM ${schema_name}._sc_bom b LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.ID " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.ID = bs.bom_id " +
            "WHERE b.status = 1 AND b.is_use = '1' and bs.stock_id = #{bomId}" +
            " <if test=\"bomTypeId != null and bomTypeId != ''\"> " +
            "   and b.type_id IN " +
            "   <foreach collection='bomTypeId.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </if>" +
            " <if test=\"keywordSearch != null and keywordSearch != ''\"> " +
            "   and lower(b.bom_name) like concat('%', lower(#{keywordSearch}),'%') " +
            " </if>" +
            "GROUP BY b.ID,b.bom_name,b.material_code,bt.id,bt.type_name" +
            "</script>")
    List<Map<String, Object>> findStockBomListByStockAndType(@Param("schema_name") String schemaName, @Param("keywordSearch") String keywordSearch,
                                                    @Param("userId") String userId, @Param("bomId") Integer bomId, @Param("bomTypeId") String bomTypeId);


    @Select("<script>" +
            "SELECT DISTINCT b.ID bom_id,b.bom_name,b.material_code,b.type_id as bom_type_id,b.bom_model," +
            "COALESCE (CASE WHEN POSITION ( ',' IN COALESCE ( array_to_string( ARRAY_AGG ( DISTINCT sup.supplier_id ), ',' ), '' ) ) > 0 THEN SUM ( bs.quantity ) / 2 ELSE SUM ( bs.quantity ) END,0 ) now_quantity," +
            "'0' profit_and_loss,b.unit_id as unit,b.currency_id,COALESCE(array_to_string(array_agg(DISTINCT sup.supplier_id),','),'') AS supplier_id," +
            "COALESCE(array_to_string(array_agg(DISTINCT b.manufacturer_id),','),'') AS manufacturer_id " +
            "FROM ${schema_name}._sc_bom b " +
            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON b.ID = bs.bom_id " +
            "LEFT JOIN ${schema_name}._sc_bom_supplies sup on b.id = sup.bom_id " +
            "WHERE b.status = 1 AND b.is_use = '1' " +
            " <if test=\"stock_id !=null and stock_id != ''\"> " +
            " and bs.stock_id = #{stock_id} " +
            " </if>" +
            " <if test=\"bomTypeId != null and bomTypeId != ''\"> " +
            "   and b.type_id IN " +
            "   <foreach collection='bomTypeId.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </if>" +
            " <if test=\"supplierId != null and supplierId != ''\"> " +
            "   and sup.supplier_id IN " +
            "   <foreach collection='supplierId.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </if>" +
            " <if test=\"manufacturerId != null and manufacturerId != ''\"> " +
            "   and b.manufacturer_id IN " +
            "   <foreach collection='manufacturerId.split(\",\")' item='item' open='(' close=')' separator=','> " +
            "   #{item} :: int " +
            "   </foreach>" +
            " </if>" +
            " <if test=\"keywordSearch != null and keywordSearch != ''\"> " +
            "   and lower(b.bom_name) like concat('%', lower(#{keywordSearch}),'%') " +
            " </if>" +
            "GROUP BY b.ID,b.bom_name,b.material_code" +
            "</script>")
    List<Map<String, Object>> findBomInListByStockAndType(@Param("schema_name") String schemaName, @Param("keywordSearch") String keywordSearch,
                                                             @Param("userId") String userId, @Param("stock_id") Integer stock_id, @Param("bomTypeId") String bomTypeId, @Param("supplierId") String supplier_id, @Param("manufacturerId") String manufacturer_id);

    /**
     * 带权限的客户
     * @param schemaName
     * @param id
     * @return
     */
    @Select(" SELECT distinct cu.parent_id as parent, cu.facility_id as value, cu.short_title as text, cu.position_code FROM " +
//            " ${schema_name}._sc_facilities a, " +
            " ${schema_name}._sc_customer cu " +
            " join ${schema_name}._sc_role_asset_position bb on bb.asset_position_code = cu.position_code " +
            " join (select f.id from " +
            " ${schema_name}._sc_role f " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id" +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{id}) m on m.id = bb.role_id" +
            " where cu.is_use='1' and cu.status > " + StatusConstant.STATUS_DELETEED + " and cu.org_type_id in ('1')" +
            " ORDER BY cu.facility_id,cu.parent_id desc ")
    List<Map<String, Object>> findOnlyCustomer(@Param("schema_name") String schemaName,@Param("id") String id);

    /**
     * 查询所有可抄送的有效用户信息
     * */
    @Select("<script>" +
            " select id::varchar as value,('【内部人员】'||user_name) as text from ${schema_name}._sc_user where status=1 " +
            " union " +
            " select id::varchar as value,('【外部人员】'||contact_name) as text from ${schema_name}._sc_customer_contact where org_id=#{pm.org_id}::int " +
            "</script>")
    List<Map<String, Object>> findCCAllUser(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> paramMap);

    /**
     * 带权限的客户名下设备
     * @param schemaName
     * @param org_id
     * @param userId
     * @return
     */
    @Select(" select distinct  a.asset_id as value,a.asset_code,a.asset_name as text " +
//            " from ${schema_name}._sc_asset a " +
            " from ${schema_name}._sc_asset_cus a " +
//            " join ${schema_name}._sc_role_asset_type e on a.category_id = e.category_id " +
//            " join ${schema_name}._sc_role_asset_position  bb on bb.asset_position_code = a.position_code " +
//            " join (select f.id from " +
//            " ${schema_name}._sc_role f " +
//            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
//            " join ${schema_name}._sc_position h on h.id = g.position_id " +
//            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
//            " join ${schema_name}._sc_user k on i.user_id = k.id " +
//            " where k.id= #{user_id}) m on e.role_id =m.id and m.id = bb.role_id ," +
//            " ${schema_name}._sc_asset_organization b " +
            " where a.cus_id= #{org_id}::int ")
    List<Map<String, Object>> findCustomerAssetList(@Param("schema_name") String schemaName,@Param("org_id") String org_id, @Param("user_id") String userId);
}