package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetQueryMapper {
//    // LSP工单处理变更-20190521-sps start
//    //按设备位置，获取第一级设备,支持分页
////    @Select("select * from ${schema_name}._sc_asset where intstatus>0 and (parent_id is null or parent_id='') and intsiteid=#{facilityId} " +
////            "order by strcode limit ${page_size} offset ${offset};")
//    @Select("select * from ${schema_name}._sc_facilities t2, ${schema_name}._sc_asset_organization ao, ${schema_name}._sc_asset t1 " +
//            "where t1.intstatus>0 and (t1.parent_id is null or t1.parent_id='') and t2.id=#{facilityId} AND t2.id = ao.org_id " +
//            "and ao.asset_id = t1._id order by strcode limit ${page_size} offset ${offset}; ")
//    List<Asset> findRootAssetListByFacilityId(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId, @Param("page_size") int page_size, @Param("offset") int offset);
//
//    //按设备位置和设备，获取子设备
////    @Select("select * from ${schema_name}._sc_asset where intstatus>0 and parent_id=#{assetCode} and intsiteid=#{facilityId} order by strcode")
//    @Select("select * from ${schema_name}._sc_facilities t2, ${schema_name}._sc_asset_organization ao, ${schema_name}._sc_asset t1 " +
//            "where t1.intstatus>0 and t1.parent_id=#{assetCode} and t2.id=#{facilityId} AND t2.id = ao.org_id and ao.asset_id = t1._id order by strcode")
//    List<Asset> findSubAssetList(@Param("schema_name") String schema_name, @Param("facilityId") Integer facilityId, @Param("assetCode") String assetCode);
//    // LSP工单处理变更-20190521-sps end
}
