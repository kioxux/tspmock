package com.gengyun.senscloud.mapper;

/**
 * @Author: Wudang Dong
 * @Description:设备报废DAO
 * @Date: Create in 上午 11:30 2019/4/30 0030
 */
public interface AssetDiscardMapper {
//    @Select("select t.*,t1.strcode,t1.strname,tm.model_name,t6.category_name as asset_type,t1.supplier as supplier," +
//            "t2.title as facilities_name,t5.status as asset_status_name,t7.username as username from ${schema_name}._sc_asset_discard  as t " +
//            " LEFT JOIN ${schema_name}._sc_asset t1 on t.asset_id=t1._id" +
//            " LEFT JOIN ${schema_name}._sc_facilities t2 on t1.intsiteid=t2.id" +
//            " LEFT JOIN ${schema_name}._sc_asset_category t6 on t6.id = t1.category_id " +
//            " LEFT JOIN ${schema_name}._sc_asset_status t5 on t5.id = t1.intstatus " +
//            " LEFT JOIN ${schema_name}._sc_asset_model tm on tm.id = t1.asset_model_id " +
//            " LEFT JOIN ${schema_name}._sc_user as t7 on t7.account=t.create_user_account " +
//            " where t.id=#{id}")
//    public AssetDiscardModel selectDetailById(@Param("schema_name") String schema_name, @Param("id") int id);
//
//    @Select("select t.*,ARRAY_TO_STRING(ARRAY_AGG(ad.asset_id), ',') as relation_id from ${schema_name}._sc_asset_discard as t LEFT JOIN ${schema_name}._sc_asset_discard_detail ad ON ad.discard_code = t.discard_code " +
//            " where t.discard_code=#{discard_code} GROUP BY t.discard_code")
//    public Map<String, Object> selectDetailByDiscardCode(@Param("schema_name") String schema_name, @Param("discard_code") String discard_code);
//
//    @Select("select count(DISTINCT t.discard_code) from ${schema_name}._sc_asset_discard as t " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p1 ON p1.position_code = t.position_id " +
//            "left join ${schema_name}._sc_facilities f1 on f1.ID::VARCHAR = t.position_id " +
//            "left join ${schema_name}._sc_asset_position_organization apo1 on apo1.org_id = f1.id " +
//            "where 1=1 ${condition}")
//    public int selectAllDiscardNum(@Param("schema_name") String schena_name, @Param("condition") String condition);
//
//    @Select("SELECT DISTINCT t.discard_code,t.create_time,t.status,t7.username AS username, " +
//            "CASE WHEN p1.position_name IS NULL THEN " +
//            "(SELECT f.title FROM ${schema_name}._sc_facilities f WHERE CAST(f.id AS VARCHAR) = T.position_id) " +
//            "ELSE p1.position_name END AS position_name " +
//            "FROM ${schema_name}._sc_asset_discard AS T " +
//            "LEFT JOIN ${schema_name}._sc_asset_position p1 ON p1.position_code = t.position_id " +
//            "left join ${schema_name}._sc_facilities f1 on f1.ID::VARCHAR = t.position_id " +
//            "left join ${schema_name}._sc_asset_position_organization apo1 on apo1.org_id = f1.id " +
//            "LEFT JOIN ${schema_name}._sc_user AS t7 ON t7.account = T.create_user_account " +
//            " where 1=1 ${condition} order by t.create_time desc")
//    public List<Map<String, Object>> selectAllDiscard(@Param("schema_name") String schena_name, @Param("condition") String condition);
//
//    //    @Select("select count(DISTINCT t.transfer_code) from ${schema_name}._sc_asset_transfer as t " +
////            " LEFT JOIN ${schema_name}._sc_asset t1 on t.asset_id=t1._id" +
////            " LEFT JOIN ${schema_name}._sc_facilities t2 on t1.intsiteid=t2.id" +
////            " LEFT JOIN ${schema_name}._sc_asset_category t6 on t6.id = t1.category_id " +
////            " LEFT JOIN ${schema_name}._sc_asset_status t5 on t5.id = t1.intstatus " +
////            " LEFT JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = t1._id " +
////            " LEFT JOIN ${schema_name}._sc_facilities f ON f.id = ao.org_id " +
//// =
//
//    @Update("update ${schema_name}._sc_asset_discard set status = #{status} where id=#{id}")
//    public int cancel(@Param("schema_name") String schema_name, @Param("status") int status, @Param("id") int id);
//
//    @Update("update ${schema_name}._sc_asset_discard set status = #{status},remark = #{remark}  where discard_code=#{discardCode}")
//    public int deal(@Param("schema_name") String schema_name, @Param("status") int status, @Param("remark") String remark, @Param("discardCode") String discardCode);
//
//    @Insert("insert into ${schema_name}._sc_asset_discard (discard_code,position_id,status,remark,create_user_account,create_time,body_property,source_sub_code) values " +
//            "(#{discard_code},#{position_id},#{status},#{remark},#{create_user_account},#{create_time},#{body_property}::jsonb,#{source_sub_code})")
////    @Options(useGeneratedKeys=true, keyProperty="discard_code")
//    int add(AssetDiscardModel assetDiscardModel);
//
//    @Update("update ${schema_name}._sc_asset_discard set position_id=#{position_id}, " +
//            "status=#{status},remark=#{remark},body_property=#{body_property}::jsonb where discard_code=#{discard_code}")
//    public int updateAssetDiscard(AssetDiscardModel assetDiscardModel);
//
//    @Update("update ${schema_name}._sc_asset set intstatus=" + StatusConstant.ASSET_INTSTATUS_DISCARD + " where _id =#{asset_id}")
//    public int discardAsset(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    @Delete("delete from ${schema_name}._sc_asset_discard_detail where discard_code = #{discard_code}")
//    public int deleteAssetDiscardDetail(@Param("schema_name") String schema_name, @Param("discard_code") String discard_code);
//
//    @Insert("insert into ${schema_name}._sc_asset_discard_detail (discard_code,asset_id,leave_cost,file_ids,currency_id) values " +
//            "(#{discard_code},#{_id},#{leave_cost}::float,#{file_ids},#{currency}::int)")
////    @Options(useGeneratedKeys=true, keyProperty="discard_code")
//    int add_discard_detail(Map<String, Object> s);
//
//    @Update("update ${schema_name}._sc_asset_discard set status = #{status},body_property=#{body_property}::jsonb where discard_code=#{discard_code}")
//    public int updateStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("discard_code") String discard_code, @Param("body_property") String body_property);
//
//    @Update("update ${schema_name}._sc_asset set intstatus = #{status} where _id=#{id}")
//    public int updateAssetStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("id") String id);


}
