package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface SuezReportMapper {

    @Select(" <script> " +
            "select count(1) from (SELECT r.id,r.report_name,r.begin_time,r.end_time, " +
            "string_agg ( f.position_name, ',' ) title,string_agg ( wt.type_name, ',' ) work_type_name FROM " +
            "(SELECT a.id,report_name,query_propertity ->> 'begin_time' AS begin_time,query_propertity ->> 'end_time' AS end_time,to_char(create_time,'yyyy-MM-dd HH24:mi:ss') as create_time," +
            "b.position_code,b.work_type_id " +
            "FROM ${schema_name}._sc_report a " +
            "left join (select m.id , replace(jsonb_array_elements ( query_propertity -> 'position_code' )::VARCHAR, '\"', '') AS position_code, replace(jsonb_array_elements ( query_propertity -> 'work_type_id' )::VARCHAR, '\"', '') AS work_type_id " +
            "from ${schema_name}._sc_report m) b on a.id = b.id " +
            " ${condition} " +
            ") r " +
            "LEFT JOIN ${schema_name}._sc_asset_position f on f.position_code = r.position_code " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on wt.id::varchar = r.work_type_id " +
            "GROUP BY r.id,r.report_name,r.begin_time,r.end_time ) t " +
            " </script>")
    int findSuezReportListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);

    @Select(" <script> " +
            "SELECT r.id,r.report_name,r.begin_time,r.end_time,r.file_ids, " +
            "string_agg ( f.position_name, ',' ) position_name,string_agg ( a.asset_name, ',' ) asset_name, " +
            "string_agg ( wt.type_name, ',' ) work_type_name,r.create_time " +
            "FROM " +
            "(SELECT a.id,report_name,a.file_ids,query_propertity ->> 'begin_time' AS begin_time,query_propertity ->> 'end_time' AS end_time,to_char(create_time,'yyyy-MM-dd HH24:mi:ss') as create_time," +
            "b.position_code,b.asset_id,b.work_type_id " +
            "FROM ${schema_name}._sc_report a " +
            "left join (select m.id , replace(jsonb_array_elements ( query_propertity -> 'position_code' )::VARCHAR, '\"', '') AS position_code, " +
            "replace(jsonb_array_elements ( query_propertity -> 'select_asset' )::VARCHAR, '\"', '') AS asset_id, " +
            "replace(jsonb_array_elements ( query_propertity -> 'work_type_id' )::VARCHAR, '\"', '') AS work_type_id " +
            "from ${schema_name}._sc_report m) b on a.id = b.id " +
            " ${condition} " +
            ") r " +
            "LEFT JOIN ${schema_name}._sc_asset_position f on f.position_code = r.position_code " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on wt.id::varchar = r.work_type_id " +
            "LEFT JOIN ${schema_name}._sc_asset a on a.id::varchar = r.asset_id " +
            "GROUP BY r.id,r.report_name,r.begin_time,r.end_time,r.file_ids,r.create_time " +
            "order by r.create_time desc ${pagination} " +
            " </script>")
    List<Map<String, Object>> findSuezReportList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("pagination") String pagination);

    @Select(" <script> " +
            "select count(1) from (SELECT r.id,r.report_name,r.begin_time,r.end_time, " +
            "string_agg ( f.short_title, ',' ) title,string_agg ( wt.type_name, ',' ) work_type_name FROM " +
            "(SELECT a.id,report_name,query_propertity ->> 'begin_time' AS begin_time,query_propertity ->> 'end_time' AS end_time,to_char(create_time,'yyyy-MM-dd HH24:mi:ss') as create_time," +
            "b.facility_id,b.work_type_id " +
            "FROM ${schema_name}._sc_report_nt a " +
            "left join (select m.id , jsonb_array_elements(query_propertity -> 'facility_id')::text AS facility_id, jsonb_array_elements(query_propertity -> 'work_type_id')::text AS work_type_id " +
            "from ${schema_name}._sc_report_nt m) b on a.id = b.id " +
            " ${condition} " +
            ") r " +
            "LEFT JOIN ${schema_name}._sc_facilities f on f.id::varchar = r.facility_id " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on wt.id::varchar = r.work_type_id " +
            "GROUP BY r.id,r.report_name,r.begin_time,r.end_time ) t " +
            " </script>")
    int findSuezReportListCountNT(@Param("schema_name") String schema_name, @Param("condition") String condition);

    @Select(" <script> " +
            "SELECT r.id,cd3.name report_name,r.begin_time,r.end_time," +
            "string_agg ( f.short_title, ',' ) position_name, " +
            "string_agg ( wt.type_name, ',' ) work_type_name,r.create_time " +
            "FROM " +
            "(SELECT a.id,report_name,query_propertity ->> 'begin_time' AS begin_time,query_propertity ->> 'end_time' AS end_time,to_char(create_time,'yyyy-MM-dd HH24:mi:ss') as create_time," +
            "b.facility_id,b.work_type_id " +
            "FROM ${schema_name}._sc_report_nt a " +
            "left join (select m.id , jsonb_array_elements(query_propertity -> 'facility_id')::text AS facility_id, jsonb_array_elements(query_propertity -> 'work_type_id')::text AS work_type_id " +
            "from ${schema_name}._sc_report_nt m) b on a.id = b.id " +
            " ${condition} " +
            ") r " +
            "LEFT JOIN ${schema_name}._sc_facilities f on f.id::varchar = r.facility_id " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on wt.id::varchar = r.work_type_id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd3 on cd3.data_type = 'report_name' and cd3.code = r.report_name " +
            "GROUP BY r.id,cd3.name,r.begin_time,r.end_time,r.create_time " +
            "order by r.create_time desc ${pagination} " +
            " </script>")
    List<Map<String, Object>> findSuezReportListNT(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("pagination") String pagination);

    @Insert("insert into ${schema_name}._sc_report (report_name,query_propertity,create_time,create_user_id) " +
            "values(#{report_name},#{property}::jsonb,#{create_time},#{create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insertReport(Map<String, Object> data);

    @Delete(" <script> delete from ${schema_name}._sc_report where id in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> </script> ")
    void deleteSuezReport(@Param("schema_name") String schemaName, @Param("ids") String[] idArr);

    @Insert("insert into ${schema_name}._sc_report_nt (report_name,query_propertity,create_time,create_user_id) " +
            "values(#{report_name},#{property}::jsonb,#{create_time},#{create_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void insertReportNT(Map<String, Object> data);

    @Delete(" <script> delete from ${schema_name}._sc_report_nt where id in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id}::int </foreach> </script> ")
    void deleteSuezReportNT(@Param("schema_name") String schemaName, @Param("ids") String[] idArr);

    /**
     * 更新日常报告文件主键
     *
     * @param schemaName 数据库
     * @param file_ids   文件主键
     * @param id         数据主键
     */
    @Update("update ${schema_name}._sc_report set file_ids = #{file_ids} where id = #{id}::int ")
    void updateSuezReportFileIdsById(@Param("schema_name") String schemaName, @Param("file_ids") String file_ids, @Param("id") String id);

    @Select("SELECT report_name,file_ids, " +
            "substring(query_propertity ->> 'begin_time', 1, 10) AS begin_time, " +
            "substring(query_propertity ->> 'end_time',1,10) AS end_time, " +
            "query_propertity ->> 'position_codes' AS position_codes, " +
            "query_propertity ->> 'select_assets' AS select_assets, " +
            "query_propertity ->> 'work_type_ids' AS work_type_ids " +
            "FROM ${schema_name}._sc_report WHERE id = #{id} ")
    Map<String, Object> findReportById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    @Select("SELECT a1.id,a1.report_name," +
            "a1. begin_time, " +
            "a1.end_time, " +
            "string_agg(t.facility_id, ',') facility_ids," +
            "string_agg(t.work_type_id, ',') work_type_ids, " +
            "string_agg(t.stock_id, ',') stock_ids, " +
            "string_agg(t.bom_type_id, ',') bom_type_ids, " +
            "string_agg(t.asset_type_ids, ',') asset_type_ids, " +
            "string_agg(t.asset_position_codes, ',') asset_position_codes " +
            "FROM " +
            "(SELECT a.id,a.report_name," +
            "substring(a.query_propertity ->> 'begin_time', 1, 10) AS begin_time, " +
            "substring(a.query_propertity ->> 'end_time',1,10) AS end_time " +
            "FROM ${schema_name}._sc_report_nt a WHERE id = #{id}) a1  left join " +
            "(SELECT id," +
            "jsonb_array_elements(query_propertity -> 'facility_id')::text AS facility_id, " +
            "jsonb_array_elements(query_propertity -> 'work_type_id')::text AS work_type_id, " +
            "jsonb_array_elements(query_propertity -> 'stock_ids')::text AS stock_id, " +
            "jsonb_array_elements(query_propertity -> 'bom_type_ids')::text AS bom_type_id, " +
            "jsonb_array_elements(query_propertity -> 'asset_type_ids')::text AS asset_type_ids, " +
            "jsonb_array_elements(query_propertity -> 'positions')::text AS asset_position_codes " +
            "FROM ${schema_name}._sc_report_nt WHERE id = #{id}) t " +
            "on a1.id = t.id " +
            "GROUP BY a1.id,a1.report_name,a1.begin_time,a1.end_time ")
    Map<String, Object> findReportByIdNT(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 日常报告数量统计
     *
     * @param schemaName 数据库
     * @param condition  条件
     * @return 数量
     */
    @Select("SELECT count(1) FROM ${schema_name}._sc_works w " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd ON wd.work_code=w.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset a ON wd.relation_id=a.id " +
            "LEFT JOIN ${schema_name}._sc_work_type wt ON w.work_type_id = wt.id " +
            "LEFT JOIN (select wp.sub_work_code,Max(wp.id) max_id FROM ${schema_name}._sc_work_process wp group by wp.sub_work_code) T ON T.sub_work_code = wd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_work_process wp on wp.id = T.max_id and wp.status >=  " + StatusConstant.TO_BE_AUDITED +
            "WHERE ${condition} ")
    int countReportOnMaintenanceMatters(@Param("schema_name") String schemaName, @Param("condition") String condition);

    /**
     * 日常报告数据获取
     *
     * @param schemaName 数据库
     * @param condition  条件
     * @return 日常报告数据
     */
    @Select("SELECT to_char(w.create_time,'YYYY/mm') as report_day, '' as duty_type_name, w.work_code,a.asset_code, " +
            "a.asset_name,ac.category_name,ac.category_code,il.level_name,ap.remark as inner_code, s.status,u.user_name,u.user_code, " +
            "bt.business_name,wt.type_name,cd1.name as result_note, cd0.name as equipment_situation, cd5.name as repair_type, " +
            "cd2.name as fault_code_name, cd3.name as parent_fault_code_name, cd4.name as problem_cau, " +
            "CASE WHEN ap.position_name IS NULL THEN " +
            "   case when w.relation_id like 'P%' then (SELECT p.position_name FROM ${schema_name}._sc_asset_position p WHERE p.position_code = w.relation_id ) " +
            "   else(SELECT p.position_name FROM ${schema_name}._sc_asset a JOIN ${schema_name}._sc_asset_position p ON a.position_code = p.position_code WHERE a.id = w.relation_id) " +
            "   end " +
            "ELSE ap.position_name END AS position_name, " +
            "coalesce(w.position_code, case when w.relation_id like 'P%' THEN w.relation_id ELSE '' end ) as position_code, " +
            "(SELECT name FROM ${schema_name}._sc_cloud_data WHERE data_type='equipment_type' AND code=(SELECT pp->> 'field_value' FROM jsonb_array_elements(a.properties) as pp where pp->>'field_code' = 'basic3')) as asset_prf_type, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'problem_note') as problem_note, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'task_item') as task_item, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'measures_note') as measures_note, " +
            "case (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'work_level') when '1' then 'C' WHEN '2' THEN 'B' WHEN '3' THEN 'C' ELSE '' END as repair_priority_level, " +
            "(SELECT name FROM ${schema_name}._sc_cloud_data WHERE data_type='work_level' AND code::text = " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'work_level')) as repair_priority, " +
            "(SELECT name FROM ${schema_name}._sc_cloud_data WHERE data_type='solutions' AND code::text = " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'measures')) as measure_name, " +
            "(SELECT string_agg(p.position_name, ',') FROM ${schema_name}._sc_user_position up LEFT JOIN ${schema_name}._sc_position p on up.position_id = p.id where u.id = up.user_id) as department_name, " +
            "to_char(w.create_time, '" + Constants.DATE_FMT_DATE_2 + "') as create_time,to_char(w.occur_time, '" + Constants.DATE_FMT_DATE_2 + "') as occur_time,to_char(w.deadline_time,'" + Constants.DATE_FMT_DATE_2 + "') as deadline_time, " +
            "to_char(wd.begin_time,'" + Constants.DATE_FMT_HM_2 + "') as begin_time,to_char(wd.finished_time,'" + Constants.DATE_FMT_HM_2 + "') as finished_time, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'downtime') as downtime, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'repair_time') as repair_time, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'adv_start_tiame') as adv_start_tiame, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'repair_note') as repair_note, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'cost_control')::text as work_fee, " +
            "(SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'person_info')::text as person_info " +
            "FROM ${schema_name}._sc_works w " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd ON wd.work_code=w.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset a ON wd.relation_id=a.id " +
            "LEFT JOIN ${schema_name}._sc_asset_category ac ON a.category_id=ac.id " +
            "LEFT JOIN ${schema_name}._sc_works_detail_asset_org wdao on wdao.asset_id = a.id and wdao.sub_work_code = wd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_facilities f ON wdao.facility_id = f.ID " +
            "LEFT JOIN ${schema_name}._sc_importment_level il ON a.importment_level_id=il.id " +
            "LEFT JOIN ${schema_name}._sc_work_type wt ON w.work_type_id = wt.id " +
            "LEFT JOIN ${schema_name}._sc_business_type bt ON wt.business_type_id=bt.business_type_id " +
            "LEFT JOIN ${schema_name}._sc_preventive_type pt ON pt.id::text = (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'measures') " +
            "LEFT JOIN ${schema_name}._sc_user u ON wd.receive_user_id = u.id " +
            "LEFT JOIN ${schema_name}._sc_status s ON w.status=s.id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd0 on cd0.data_type = 'equipment_condition' and cd0.code = (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'equipment_situation') " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd1 ON cd1.data_type = 'deal_result' and cd1.code = (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'deal_result') " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd2 ON cd2.data_type = 'repair_code' and cd2.code = (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'repair_code') " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd3 on cd3.data_type = 'repair_code' and cd3.code = cd2.parent_code " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd4 ON cd4.data_type = 'fault_reason' and cd4.code = (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'fault_reason') " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd5 ON cd5.data_type = 'repair_type1' and cd5.code = (SELECT wdc.field_value from ${schema_name}._sc_works_detail_column wdc where wdc.sub_work_code = wd.sub_work_code and wdc.field_code = 'repair_type_id') " +
            "LEFT JOIN (select wp.sub_work_code,Max(wp.id) max_id FROM ${schema_name}._sc_work_process wp group by wp.sub_work_code) T ON T.sub_work_code = wd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_work_process wp on wp.id = T.max_id and wp.status >=  " + StatusConstant.TO_BE_AUDITED +
            " WHERE ${condition} " +
            "ORDER BY w.work_code DESC")
    List<Map<String, Object>> findReportOnMaintenanceMatters(@Param("schema_name") String schemaName, @Param("condition") String condition);

    @Select("SELECT field_form_code,field_code FROM ${schema_name}._sc_work_columns WHERE field_code in('fee_type','fee_belong','fee_attribute','object_name','object_model','object_code','stock_name','remark','use_count','price','name','begin_time','end_time','work_hours')")
    List<Map<String, Object>> findWorkColumns(@Param("schema_name") String schema_name);

    @Select("WITH RECURSIVE r AS (SELECT position_code,parent_code,position_name FROM ${schema_name}._sc_asset_position WHERE position_code in (${position_code}) " +
            "union ALL SELECT f1.position_code,f1.parent_code,f1.position_name FROM ${schema_name}._sc_asset_position f1, r WHERE f1.parent_code = r.position_code " +
            ")SELECT position_code from r")
    List<String> queryPositionCode(@Param("schema_name") String schema_name, @Param("position_code") String substring);

//    /**
//     * 静态字典选中项查询（已国际化）
//     *
//     * @param schema_name 数据库
//     * @param companyId   企业
//     * @param langKey     国际化类型
//     * @param dataType    字典类型
//     * @return 选中项信息
//     */
//    @Select("select t.code as value, " +
//            " case when t.is_lang = '" + Constants.STATIC_LANG + "' then " +
//            "   coalesce((select (c.resource->>t.name)::jsonb->>#{langKey} from public.company_resource c where c.company_id = #{companyId}), t.name) " +
//            " else t.name end as text, t.reserve1 " +
//            "from ${schema_name}._sc_cloud_data t where t.data_type = #{dataType} ")
//    @MapKey("value")
//    Map<String, Map<String, Object>> findCloudData(@Param("schema_name") String schema_name, @Param("companyId") Long companyId, @Param("langKey") String langKey, @Param("dataType") String dataType);

    @Select("SELECT task_template_code,task_template_name  FROM ${schema_name}._sc_task_template ")
    List<Map<String, Object>> findTaskTemplate(@Param("schema_name") String schema_name);

    /**
     *
     * @param schemaName
     * @param condition
     * @return
     */
    @Select("SELECT w.work_code,w.work_type_id::text,wd.sub_work_code," +
            "wd.audit_time,wd.finished_time,wd.begin_time " +
            "FROM ${schema_name}._sc_works w " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd ON wd.work_code=w.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset a ON wd.relation_id=a.id " +
            "LEFT JOIN ${schema_name}._sc_work_type wt ON w.work_type_id = wt.id " +
            "LEFT JOIN ${schema_name}._sc_works_detail_asset_org wdao on wdao.asset_id = a.id and wdao.sub_work_code = wd.sub_work_code " +
            "WHERE ap.status > -1000  ${condition} "
    )
    List<Map<String, Object>> findReportWorks(@Param("schema_name") String schemaName, @Param("condition") String condition);

    /**
     * @param schemaName
     * @param status
     * @param beginTime
     * @param endTime
     * @param facilityIds
     * @param workTypeIds
     * @return
     */
    @Select("<script>" +
            "SELECT  distinct a1.field_value as relation_name,a2.field_value as repair_note ,a3.field_value as occur_time," +
            "a4.field_value as repair_type_id,a5.field_value as measures," +
            "a6.field_value as measures_note,a7.field_value as receive_user_id,a8.field_value as approve_user_id," +
            "a9.field_value as approve_user_id2,a10.field_value as before_img,a11.field_value as result_img," +
            "w.work_code,w.work_type_id::text,wd.sub_work_code,wd.audit_time,wd.finished_time,wd.begin_time," +
            "w.create_time,w.create_user_id," +
            "wd.audit_time,wd.finished_time,wd.begin_time,w.occur_time,a12.group_name, " +
            "w.title,a.asset_name,a.asset_code,a13.field_value as fault_reason,a14.field_value::text as task " +
            "FROM ${schema_name}._sc_works w " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd ON wd.work_code=w.work_code " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON w.position_code = ap.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset a ON wd.relation_id=a.id " +
            "LEFT JOIN ${schema_name}._sc_work_type wt ON w.work_type_id = wt.id " +
            "LEFT JOIN ${schema_name}._sc_works_detail_asset_org wdao on wdao.asset_id = a.id and wdao.sub_work_code = wd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a1 on a1.sub_work_code=wd.sub_work_code and a1.field_code='relation_name'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a2 on a2.sub_work_code=wd.sub_work_code and a2.field_code='repair_note'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a3 on a3.sub_work_code=wd.sub_work_code and a3.field_code='occur_time'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a4 on a4.sub_work_code=wd.sub_work_code and a4.field_code='repair_type_id'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a5 on a5.sub_work_code=wd.sub_work_code and a5.field_code='measures'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a6 on a6.sub_work_code=wd.sub_work_code and a6.field_code='measures_note'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a7 on a7.sub_work_code=wd.sub_work_code and a7.field_code='receive_user_id'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a8 on a8.sub_work_code=wd.sub_work_code and a8.field_code='approve_user_id'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a9 on a9.sub_work_code=wd.sub_work_code and a9.field_code='approve_user_id2'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a10 on a10.sub_work_code=wd.sub_work_code and a10.field_code='before_img'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a11 on a11.sub_work_code=wd.sub_work_code and a11.field_code='result_img'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a13 on a13.sub_work_code=wd.sub_work_code and a13.field_code='fault_reason'" +
            "LEFT JOIN ${schema_name}._sc_works_detail_column a14 on a14.sub_work_code=wd.sub_work_code and a14.field_code='task_item'" +
            "LEFT JOIN (select max(s2.group_name) as group_name ,s0.user_id as user_id from ${schema_name}._sc_user_position s0 " +
            "           inner join ${schema_name}._sc_position s1 on s0.position_id= s1.id" +
            "           inner join ${schema_name}._sc_group s2 on s2.id = s1.group_id  group by s0.user_id) a12 " +
            "           on a12.user_id = w.create_user_id " +
            "WHERE ap.status > -1000  and wd.status = #{status} " +
            "<when test='beginTime!=null and beginTime!=\"\"'> " +
            "and to_char(w.create_time,'yyyy-mm-dd') &gt;= #{beginTime} " +
            "</when> " +
            "<when test='endTime!=null and endTime!=\"\"'> " +
            "and to_char(w.create_time,'yyyy-mm-dd') &lt;= #{endTime} " +
            "</when> " +
            "<when test='facilityIds!=null and  facilityIds.size()&gt;0'> " +
            " AND wdao.facility_id in " +
            " <foreach collection='facilityIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "<when test='workTypeIds!=null and workTypeIds.size()&gt;0'> " +
            " AND wt.id in " +
            " <foreach collection='workTypeIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "<when test='assetTypeIds!=null and assetTypeIds.size()&gt;0'> " +
            " AND a.category_id in " +
            " <foreach collection='assetTypeIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "<when test='assetPostionCodes!=null and assetPostionCodes.size()&gt;0'> " +
            " AND a.position_code in " +
            " <foreach collection='assetPostionCodes' item='id' open='(' close=')' separator=','> #{id}</foreach>" +
            "</when> " +
            "</script> "
    )
    List<Map<String, Object>> findReportWorksNew(@Param("schema_name") String schemaName, @Param("status") int status,
                                                 @Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                                 @Param("facilityIds") List<String> facilityIds, @Param("workTypeIds") List<String> workTypeIds,
                                                 @Param("assetTypeIds") List<String> assetTypeIds, @Param("assetPostionCodes") List<String> assetPostionCodes);


    /**
     * 技术规格报告
     *
     * @param schemaName
     * @param facilityIds
     * @param beginTime
     * @param endTime
     * @return
     */
    /*      "<when test='facilityIds!=null and  facilityIds.size()&gt;0'> " +
            " AND fac.id in " +
            " <foreach collection='facilityIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach> " +
            "</when> " +
            "left join (select max(m.org_id) org_id,m.asset_id from ${schema_name}._sc_asset_organization m group by m.asset_id) k on ass.id = k.asset_id " +
            "left join ${schema_name}._sc_facilities fac on k.org_id = fac.id " +
            */
    @Select("<script>" +
            "select distinct  ass.status,ass.asset_code,ass.asset_name,assm.model_name,ass.price," +
            "ass.enable_time,assp.position_name,ass.properties::text,ass.install_date " +
            "from ${schema_name}._sc_asset ass " +
            "left join ${schema_name}._sc_asset_position assp on ass.position_code=assp.position_code " +
            "left join ${schema_name}._sc_asset_model assm on ass.asset_model_id= assm.id " +
            "left join ${schema_name}._sc_asset_organization  so on ass.id = so.asset_id " +
            "where ass.status =1 " +
            "<when test='beginTime!=null and beginTime!=\"\"'> " +
            "and to_char(ass.enable_time,'yyyy-mm-dd') &gt;= #{beginTime} " +
            "</when> " +
            "<when test='endTime!=null and endTime!=\"\"'> " +
            "and to_char(ass.enable_time,'yyyy-mm-dd') &lt;= #{endTime} " +
            "</when> " +
            "<when test='facilityIds!=null and facilityIds.size()&gt;0'> " +
            " AND so.org_id in " +
            " <foreach collection='facilityIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "<when test='assetTypeIds!=null and assetTypeIds.size()&gt;0'> " +
            " AND ass.category_id in " +
            " <foreach collection='assetTypeIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "<when test='assetPostionCodes!=null and assetPostionCodes.size()&gt;0'> " +
            " AND ass.position_code in " +
            " <foreach collection='assetPostionCodes' item='id' open='(' close=')' separator=','> #{id}</foreach>" +
            "</when> " +
            "</script>")
    List<Map<String, Object>> findAssetReport(@Param("schema_name") String schemaName, @Param("facilityIds") List<String> facilityIds,
                                              @Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                              @Param("assetTypeIds") List<String> assetTypeIds, @Param("assetPostionCodes") List<String> assetPostionCodes);


    /**
     * 查询设备工单记录
     *
     * @param schemaName
     * @param status
     * @param beginTime
     * @param endTime
     * @param facilityIds
     * @param workTypeIds
     * @return
     */
    @Select("<script>" +
            "select distinct  saw.create_time ,d1.field_value as new_position_code," +
            "d2.field_value as cost_note,d3.field_value as approve_user_id," +
            "m.position_name " +
            " from ${schema_name}._sc_asset_works saw " +
            "left join ${schema_name}._sc_asset_works_detail awd on saw.work_code = awd.work_code " +
            "left join ${schema_name}._sc_works_detail_column d1 on awd.sub_work_code = d1.sub_work_code and d1.field_code='new_position_code' " +
            "left join ${schema_name}._sc_works_detail_column d2 on awd.sub_work_code = d2.sub_work_code and d2.field_code='cost_note' " +
            "left join ${schema_name}._sc_works_detail_column d3 on awd.sub_work_code = d3.sub_work_code and d3.field_code='approve_user_id' " +
            "left join ${schema_name}._sc_asset_position m on d1.field_value = m.position_code " +
            "left join ${schema_name}._sc_asset_works_detail_item n on n.sub_work_code = awd.sub_work_code " +
            "left join ${schema_name}._sc_asset_organization f on f.asset_id = n.asset_id " +
            "WHERE saw.status = #{status} " +
            "<when test='beginTime!=null and beginTime!=\"\"'> " +
            "and to_char(saw.create_time,'yyyy-mm-dd') &gt;= #{beginTime} " +
            "</when> " +
            "<when test='endTime!=null and endTime!=\"\"'> " +
            "and to_char(saw.create_time,'yyyy-mm-dd') &lt;= #{endTime} " +
            "</when> " +
            "<when test='workTypeIds!=null and workTypeIds.size()&gt;0'> " +
            " AND saw.work_type_id in " +
            " <foreach collection='workTypeIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "<when test='facilityIds!=null and facilityIds.size()&gt;0'> " +
            " AND f.org_id in " +
            " <foreach collection='facilityIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "</script>")
    List<Map<String, Object>> findAssetWorks(@Param("schema_name") String schemaName, @Param("status") int status,
                                             @Param("beginTime") String beginTime, @Param("endTime") String endTime,
                                             @Param("facilityIds") List<String> facilityIds, @Param("workTypeIds") List<String> workTypeIds);

    /**
     * @param schemaName
     * @param beginTime
     * @param endTime
     * @param facilityIds
     * @param bomTypeIds
     * @param stockIds
     * @return
     */
    @Select("<script>" +
            "select sbm.bom_name,sbm.material_code,sbm.bom_model,fac.title,stk.quantity,stk.stock_name," +
            "coalesce(ins.quantity,0) in_quantity, ins.create_time max_in_time,coalesce(outs.quantity,0) out_quantity,coalesce(uses.use_count,0) use_count " +
            "from ${schema_name}._sc_bom sbm " +
            " join " +
            "(select sum(b.quantity) quantity,string_agg(cc.stock_name,',') stock_name, b.bom_id " +
            "from ${schema_name}._sc_bom_stock b  " +
            "left join ${schema_name}._sc_stock cc on b.stock_id = cc.id " +
            "where 1=1 " +
            "<when test='stockIds!=null and stockIds.size()&gt;0'> " +
            " AND cc.id in " +
            " <foreach collection='stockIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "group by b.bom_id " +
            ") stk on stk.bom_id = sbm.id " +
            "left join ${schema_name}._sc_facilities fac on sbm.manufacturer_id = fac.id " +
            "left join " +
            "(select sum(k.quantity) as quantity,max(m.create_time) create_time ,k.bom_id " +
            "from ${schema_name}._sc_bom_works m " +
            " left join ${schema_name}._sc_bom_works_detail n on m.work_code = n.work_code " +
            " left join ${schema_name}._sc_bom_works_detail_item k on k.sub_work_code = n.sub_work_code " +
            " where 1=1 " +
            "<when test='beginTime!=null and beginTime!=\"\"'> " +
            "and to_char(m.create_time,'yyyy-mm-dd') &gt;= #{beginTime} " +
            "</when> " +
            "<when test='endTime!=null and endTime!=\"\"'> " +
            "and to_char(m.create_time,'yyyy-mm-dd') &lt;= #{endTime} " +
            "</when> " +
            " and m.work_type_id=95 " +
            " group by k.bom_id) ins " +
            " on sbm.id = ins.bom_id " +
            " left join " +
            "(select sum(k.quantity) as quantity ,k.bom_id " +
            "from ${schema_name}._sc_bom_works m " +
            " left join ${schema_name}._sc_bom_works_detail n on m.work_code = n.work_code " +
            " left join ${schema_name}._sc_bom_works_detail_item k on k.sub_work_code = n.sub_work_code " +
            " where 1=1 " +
            "<when test='beginTime!=null and beginTime!=\"\"'> " +
            "and to_char(m.create_time,'yyyy-mm-dd') &gt;= #{beginTime} " +
            "</when> " +
            "<when test='endTime!=null and endTime!=\"\"'> " +
            "and to_char(m.create_time,'yyyy-mm-dd') &lt;= #{endTime} " +
            "</when> " +
            " and m.work_type_id=83 " +
            " group by k.bom_id) outs " +
            " on sbm.id = outs.bom_id " +
            " left join " +
            " (select sum(a1.use_count) as use_count,a1.bom_id " +
            "  from ${schema_name}._sc_work_bom a1 " +
            "  left join ${schema_name}._sc_works_detail b1 on a1.sub_work_code =  b1.sub_work_code " +
            " where 1=1 " +
            "<when test='beginTime!=null and beginTime!=\"\"'> " +
            "and to_char(b1.audit_time,'yyyy-mm-dd') &gt;= #{beginTime} " +
            "</when> " +
            "<when test='endTime!=null and endTime!=\"\"'> " +
            "and to_char(b1.audit_time,'yyyy-mm-dd') &lt;= #{endTime} " +
            "</when> " +
            "  group by a1.bom_id) uses on sbm.id = uses.bom_id " +
            " where sbm.status >-1000 " +
            "<when test='bomTypeIds!=null and bomTypeIds.size()&gt;0'> " +
            " AND sbm.type_id in " +
            " <foreach collection='bomTypeIds' item='id' open='(' close=')' separator=','> #{id}::int</foreach>" +
            "</when> " +
            "</script>")
    List<Map<String, Object>> findBomReports(@Param("schema_name") String schemaName, @Param("beginTime") String beginTime,
                                             @Param("endTime") String endTime,
                                             @Param("facilityIds") List<String> facilityIds,
                                             @Param("bomTypeIds") List<String> bomTypeIds,
                                             @Param("stockIds") List<String> stockIds);
}
