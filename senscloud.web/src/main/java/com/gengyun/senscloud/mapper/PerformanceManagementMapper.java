package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 个人绩效管理
 */
@Mapper
public interface PerformanceManagementMapper {
//
//    /**
//     * 查询个人绩效的考核项（动态查询）
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("SELECT DISTINCT pmd.id,pmd.type,pmd.inspection_items FROM ${schema_name}._sc_performance_management_detail pmd " +
//            "INNER JOIN " +
//                    "(SELECT pm.id FROM " +
//                        "${schema_name}._sc_performance_management pm " +
//                        "LEFT JOIN ${schema_name}._sc_user u ON u.id = pm.user_id " +
//                        "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = pm.user_id " +
////                        "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
////                        "LEFT JOIN ${schema_name}._sc_group g2 ON g2.ID = pm.group_id " +
////                        "LEFT JOIN ${schema_name}._sc_group_org go ON g.id = go.group_id " +
////                        "LEFT JOIN ${schema_name}._sc_group_org go2 ON g2.id = go2.group_id " +
//                        "WHERE ${condition} LIMIT 1 " +
//                    ") t ON t.id = pmd.pm_id ORDER BY pmd.id ")
//    List<Map<String, Object>> queryPerformanceOptions(@Param("schema_name") String schemaName, @Param("condition") String condition);
//
//    /**
//     * 查询个人绩效列表（绩效项动态按列查询）
//     * @param schemaName
//     * @param condition
//     * @param queryFileds
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT ${queryFileds} t.id,t.user_id,t.user_code,t.has_score,t.username,t.groupname,t.rolename,t.total_points,t.period " +
//            "FROM ( " +
//                "SELECT pmd.inspection_items,pmd.score,pm.id,pm.user_id,pm.user_code,pm.has_score,u.username,pm.total_points,pm.period," +
//                "(CASE WHEN pm.group_id IS NULL THEN ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT G.group_name ), ',' ) ELSE G2.group_name END) AS groupname, " +
//                "ARRAY_TO_STRING(ARRAY_AGG(DISTINCT r.name), ',') AS rolename " +
//                "FROM ${schema_name}._sc_performance_management pm " +
//                "LEFT JOIN ${schema_name}._sc_user u ON u.id = pm.user_id " +
//                "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = pm.user_id " +
//                "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//                "LEFT JOIN ${schema_name}._sc_user_role ur ON ur.user_id = u.id " +
//                "LEFT JOIN ${schema_name}._sc_role r ON r.id = ur.role_id " +
//                "LEFT JOIN ${schema_name}._sc_performance_management_detail pmd ON pm.id = pmd.pm_id " +
//                "LEFT JOIN ${schema_name}._sc_group G2 ON G2.ID = pm.group_id " +
//                "LEFT JOIN ${schema_name}._sc_group_org go ON g.id = go.group_id " +
//                "LEFT JOIN ${schema_name}._sc_group_org go2 ON g2.id = go2.group_id " +
//                "WHERE ${condition} " +
//                "GROUP BY pm.id,u.username,pmd.inspection_items,pmd.score,G2.group_name,pm.period " +
//            ") t " +
//            "GROUP BY t.id,t.user_id,t.user_code,t.has_score,t.username,t.groupname,t.rolename,t.total_points,t.period " +
//            "ORDER BY t.id DESC LIMIT ${page} OFFSET ${begin} ")
//    List<Map<String, Object>> getPerformanceList(@Param("schema_name") String schemaName, @Param("condition") String condition, @Param("queryFileds") String queryFileds, @Param("page") int pageSize, @Param("begin") int begin);
//
//    @Select("SELECT COUNT(1) FROM (" +
//                "SELECT t.id " +
//                "FROM ( " +
//                    "SELECT pmd.inspection_items,pmd.score,pm.id,pm.user_id,pm.user_code,u.username " +
//                    "FROM ${schema_name}._sc_performance_management pm " +
//                    "LEFT JOIN ${schema_name}._sc_user u ON u.id = pm.user_id " +
//                    "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = pm.user_id " +
//                    "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//                    "LEFT JOIN ${schema_name}._sc_performance_management_detail pmd ON pm.id = pmd.pm_id " +
//                    "LEFT JOIN ${schema_name}._sc_group G2 ON G2.ID = pm.group_id " +
//                    "LEFT JOIN ${schema_name}._sc_group_org go ON g.id = go.group_id " +
//                    "LEFT JOIN ${schema_name}._sc_group_org go2 ON g2.id = go2.group_id " +
//                    "WHERE ${condition} " +
//                    "GROUP BY pm.id,u.username,pmd.inspection_items,pmd.score " +
//                ") t " +
//                "GROUP BY t.id,t.user_id,t.user_code,t.username " +
//            ") st ")
//    int countPerformanceList(@Param("schema_name") String schemaName, @Param("condition") String condition);
//
//    /**
//     * 通过ID，查询个人绩效的考核项（动态查询）
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT pmd.type,pmd.inspection_items,pmd.weightiness,pmd.template_options_id " +
//            "FROM ${schema_name}._sc_performance_management_detail pmd " +
//            "LEFT JOIN ${schema_name}._sc_performance_management pm ON pm.id = pmd.pm_id " +
//            "WHERE pmd.pm_id = ${id} AND pmd.can_fill_in IS TRUE ORDER BY pmd.id ")
//    List<Map<String, Object>> queryPerformanceOptionsById(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 评分页面-通过ID，查询人员绩效详情列表  注意：这里的分数是没有经过公式折算的原始评分分数
//     * @param schemaName
//     * @param ids
//     * @return
//     */
//    @Select("SELECT ${queryFileds} T.ID,T.has_score,T.user_code,T.username,T.rolename,T.assessment_type,T.groupname,T.period " +
//            "FROM ( " +
//                "SELECT pmd.ID AS pmd_id,pmd.inspection_items,pmd.initial_score as score,pm.ID,pm.period,pm.has_score,pm.user_code,pm.assessment_type,u.username, " +
//                    "(CASE WHEN pm.group_id IS NULL THEN ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT G.group_name ), ',' ) ELSE G2.group_name END) AS groupname, " +
//                    "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT r.NAME ), ',' ) AS rolename " +
//                "FROM ${schema_name}._sc_performance_management pm " +
//                "LEFT JOIN ${schema_name}._sc_user u ON u.ID = pm.user_id " +
//                "LEFT JOIN ${schema_name}._sc_user_role ur ON ur.user_id = u.ID " +
//                "LEFT JOIN ${schema_name}._sc_role r ON r.ID = ur.role_id " +
//                "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = pm.user_id " +
//                "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//                "LEFT JOIN ${schema_name}._sc_performance_management_detail pmd ON pm.ID = pmd.pm_id " +
//                "LEFT JOIN ${schema_name}._sc_group G2 ON G2.ID = pm.group_id " +
//                "WHERE pm.ID IN ( ${ids} ) " +
//                "GROUP BY pmd.ID,pm.ID,pm.period,u.username,pmd.inspection_items,pmd.score,G2.group_name,pm.assessment_type " +
//            ") T " +
//            "GROUP BY T.ID,T.has_score,T.user_code,T.username,T.rolename,T.assessment_type,T.groupname,T.period ORDER BY T.ID DESC ")
//    List<Map<String, Object>> queryPerformanceDetailsByIds(@Param("schema_name") String schemaName, @Param("queryFileds") String queryFileds, @Param("ids") String ids);
//
//    /**
//     * 根据ID查询绩效详情
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT pm.total_points,pm.id,pm.user_id,pm.user_code,u.username,pm.period, " +
//            "ARRAY_TO_STRING(ARRAY_AGG(DISTINCT r.name), ',') AS rolename " +
//            "FROM ${schema_name}._sc_performance_management pm " +
//            "LEFT JOIN ${schema_name}._sc_user u ON u.id = pm.user_id " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = pm.user_id " +
//            "LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_user_role ur ON ur.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_role r ON r.id = ur.role_id " +
//            "WHERE pm.id = ${id} " +
//            "GROUP BY pm.id,u.username "
//        )
//    Map<String, Object> queryPerformanceById(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     *
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT pmd.* FROM ${schema_name}._sc_performance_management_detail pmd WHERE pmd.pm_id = ${id} ORDER BY pmd.type,pmd.id")
//    List<Map<String, Object>> findPerformanceItemsById(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 查询绩效模板列表
//     * @param schemaName
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT t.id,t.template_name,t.period,t.assessment_type,t.isuse FROM ${schema_name}._sc_performance_management_template t " +
//            "WHERE 1=1 ${condition} " +
//            "ORDER BY t.id DESC LIMIT ${page} OFFSET ${begin} ")
//    List<Map<String, Object>> getPerformanceTemplateList(@Param("schema_name") String schemaName, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询绩效模板列表总数
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_performance_management_template t WHERE 1=1 ${condition}")
//    int countPerformanceTemplateList(@Param("schema_name") String schemaName, @Param("condition") String condition);
//
//    /**
//     * 修改绩效模板状态
//     * @param schemaName
//     * @param id
//     * @param isuse
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_performance_management_template SET isuse = ${isuse} WHERE id = ${id} ")
//    int enableOrDisablePerformanceTemplateById(@Param("schema_name") String schemaName, @Param("id") long id, @Param("isuse") boolean isuse);
//
//    /**
//     * 新增考核模板
//     * @param performanceTemplate
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_performance_management_template (template_name,period,assessment_type,remark,isuse,create_user_account,create_time) " +
//            "values (#{template_name},#{period},#{assessment_type},#{remark},#{isuse},#{create_user_account},#{create_time})")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int addPerformanceTemplate(PerformanceTemplate performanceTemplate);
//
//    /**
//     * 修改考核模板
//     * @param performanceTemplate
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_performance_management_template SET template_name=#{template_name},period=#{period},assessment_type=#{assessment_type},remark=#{remark} " +
//            "WHERE id=#{id}")
//    int updatePerformanceTemplate(PerformanceTemplate performanceTemplate);
//
//    /**
//     * 删除考核模板-考核项
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("DELETE FROM  ${schema_name}._sc_performance_management_template_options WHERE template_id=#{id}")
//    int deletePerformanceTemplateOptions(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 删除考核模板-考核项-考核标准
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("DELETE FROM  ${schema_name}._sc_performance_management_template_options_equation WHERE template_id=#{id}")
//    int deletePerformanceTemplateOptionsEquation(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 新增考核模板-考核项
//     * @param options
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_performance_management_template_options " +
//            "(template_id,type,assessment_latitude,dashboard_id,inspection_items,challenge_value,target_value,minimum_value,weightiness,can_fill_in) " +
//            "values (#{template_id},#{type},#{assessment_latitude},#{dashboard_id},#{inspection_items},#{challenge_value},#{target_value}," +
//            "#{minimum_value},#{weightiness},#{can_fill_in})")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int addPerformanceTemplateOption(PerformanceTemplateOptions options);
//
//    /**
//     *  通过ID查询绩效模板
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_performance_management_template WHERE id = ${id}")
//    PerformanceTemplate queryPerformanceTemplateById(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 通过绩效模板ID，查询绩效模板考核项
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT o.*,d.snippet as definition FROM ${schema_name}._sc_performance_management_template_options o LEFT JOIN ${schema_name}._sc_dashboard d ON o.dashboard_id = d.id WHERE template_id = ${id}")
//    List<PerformanceTemplateOptions> queryPerformanceTemplateOptionsByTemplateId(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 通过绩效模板ID，查询绩效模板考核项考核标准
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_performance_management_template_options_equation WHERE template_id = ${id}")
//    List<PerformanceTemplateOptionsEquation> queryPerformanceTemplateOptionsEquationByTemplateId(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 查询绩效模板考核项考核标准
//     * @param schemaName
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_performance_management_template_options_equation ")
//    List<PerformanceTemplateOptionsEquation> queryPerformanceTemplateOptionsEquation(@Param("schema_name") String schemaName);
//
//    /**
//     * 通过绩效模板考核项ID，查询绩效模板考核项考核标准
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Select("SELECT * FROM ${schema_name}._sc_performance_management_template_options_equation WHERE template_option_id = ${id}")
//    List<PerformanceTemplateOptionsEquation> queryPerformanceTemplateOptionsEquationByTemplateOptionId(@Param("schema_name") String schemaName, @Param("id") long id);
//
//    /**
//     * 查询指定ID人员绩效的客观（不可编辑的）考核项得分
//     * @param schemaName
//     * @param pmIds
//     * @return
//     */
//    @Select("SELECT pm.id,pm.template_id,pm.has_score,SUM(pmd.weight_score) AS weight_score FROM ${schema_name}._sc_performance_management_detail pmd " +
//            "LEFT JOIN ${schema_name}._sc_performance_management pm ON pmd.pm_id = pm.id " +
//            "WHERE pm.id IN (${pmIds}) AND pmd.can_fill_in is FALSE GROUP BY pm.id")
//    List<Map<String, Object>> queryPerformanceScore(@Param("schema_name") String schemaName, @Param("pmIds") String pmIds);
//
//    /**
//     * 查询人员绩效表
//     * @param schemaName
//     * @param pmId
//     * @return
//     */
//    @Select("SELECT pm.id,pm.template_id,pm.has_score FROM ${schema_name}._sc_performance_management pm WHERE pm.id = ${pmId} ")
//    Map<String, Object> queryPerformanceManagementById(@Param("schema_name") String schemaName, @Param("pmId") long pmId);
//
//    /**
//     * 修改人员绩效评分状态
//     * @param schemaName
//     * @param pmId
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_performance_management SET total_points=${total_points}, has_score = ${has_score} WHERE id = ${pmId}")
//    int updatePerformanceScoreStatus(@Param("schema_name") String schemaName, @Param("total_points") BigDecimal total_points, @Param("has_score") boolean has_score, @Param("pmId") long pmId);
//
//    /**
//     * 修改评分
//     * @param schemaName
//     * @param id
//     * @param score
//     * @param weightScore
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_performance_management_detail SET initial_score = ${initial_score},score=${score},weight_score=${weight_score} WHERE id = ${id}")
//    int updatePerformanceScore(@Param("schema_name") String schemaName, @Param("id") long id, @Param("initial_score") BigDecimal initialScore, @Param("score") BigDecimal score, @Param("weight_score") BigDecimal weightScore);
//
//    /**
//     *  考核计划列表查询
//     * @param schemaName
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @param rolesTitle
//     * @param groupTitle
//     * @return
//     */
//    @Select("select " +
//            "#{idPrefix} || p.id as id ,p.template_id,p.begin_date,p.end_date,p.plan_name,p.isuse,p.create_user_account,p.create_time,t.template_name as template_name, " +
//            "string_agg(DISTINCT gr.group_name , ',') as group_name, " +
//            "string_agg(DISTINCT ro.name , ',') as role_name " +
//            "from ${schema_name}._sc_performance_management_plan as p " +
//            "left join ${schema_name}._sc_performance_management_template as t on t.id = p.template_id " +
//            "left join ${schema_name}._sc_performance_management_plan_group as pg on pg.plan_id = p.id " +
//            "left join ${schema_name}._sc_performance_management_plan_role as pr on pr.plan_id = p.id " +
//            "left join ${schema_name}._sc_group as gr on gr.id =  pg.group_id " +
//            "left join ${schema_name}._sc_role as ro on ro.id = pr.role_id " +
//            "where 1 = 1 ${condition} " +
//            "GROUP BY p.id,p.template_id,p.begin_date,p.end_date,p.plan_name,p.isuse,p.create_user_account,p.create_time,t.template_name " +
//            "ORDER BY p.create_time DESC limit ${page} offset ${begin}")
//    List<Map<String, Object>> getPerformancePlanList(@Param("schema_name") String schemaName, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin,@Param("rolesTitle")  String rolesTitle,@Param
//            ("groupTitle") String groupTitle, @Param("idPrefix") String idPrefix);
//
//    /**
//     * 考核计划列表总数
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) from ${schema_name}._sc_performance_management_plan as p  where 1 = 1 ${condition}")
//    int countPerformancePlanList(@Param("schema_name") String schemaName, @Param("condition") String condition);
//
//    /**
//     * 修改考核计划状态
//     * @param schemaName
//     * @param id
//     * @param isuse
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_performance_management_plan SET isuse = ${isuse} WHERE id = ${id} ")
//    int enableOrDisablePerformancePlanById(@Param("schema_name") String schemaName, @Param("id") long id, @Param("isuse") boolean isuse);
//
//    /**
//     * 获取考核计划详情
//     * @param schemaName
//     * @param sub_work_code
//     * @return
//     */
//    @Select("Select * from ${schema_name}._sc_performance_management_plan where id = #{sub_work_code}::int")
//    Map<String, Object> queryPlanById(@Param("schema_name") String schemaName,@Param("sub_work_code") String sub_work_code);
//
//    @Insert("insert into ${schema_name}._sc_performance_management_plan (template_id,begin_date,end_date,plan_name,isuse,create_user_account,create_time, body_property) values (" +
//            "#{template_id}::int,#{begin_date}::timestamp,#{end_date}::timestamp,#{plan_name},#{isuse}::bool,#{create_user_account},#{create_time}::timestamp,#{body_property}::jsonb)")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int doSubmit(Map<String, Object> paramMap);
//
//    @Update("update ${schema_name}._sc_performance_management_plan set " +
//            "template_id=#{s.template_id}::int ,begin_date=#{s.begin_date}::timestamp,end_date=#{s.end_date}::timestamp, plan_name=#{s.plan_name}, body_property=#{s.body_property}::jsonb  " +
//            "where id = #{s.id}::int")
//    int doUpdate(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap, @Param("id")  String id);
//
//    /**
//     * 更新清空考核计划角色
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_performance_management_plan_role where plan_id=#{id}::int")
//    int deleteRoles(@Param("schema_name") String schemaName, @Param("id")  String id);
//
//    /**
//     * 更新清空考核计划用户组
//     * @param schemaName
//     * @param id
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_performance_management_plan_group where plan_id=#{id}::int")
//    int deleteGroups(@Param("schema_name") String schemaName, @Param("id")  String id);
//
//    /**
//     * 新增考核计划角色关联表
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_performance_management_plan_role (plan_id,role_id ) values (#{s.plan_id}::int,#{s.role_id} )")
//    int doSubmitRoles(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 新增考核计划用户组关联表
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_performance_management_plan_group (plan_id,group_id) values (#{s.plan_id}::int,#{s.group_id}::int )")
//    int doSubmitGroups(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);
//
//    /**
//     * 查询考核计划需要生成任务的计划列表信息
//     * @param schemaName
//     * @return
//     */
//    @Select("select " +
//            "DISTINCT u.username,u.id,u.user_code,u.account,p.template_id,p.begin_date,p.end_date,p.create_user_account,p.create_time,t.period,t.assessment_type " +
//            "from ${schema_name}._sc_performance_management_plan as p " +
//            "LEFT JOIN ${schema_name}._sc_performance_management_template as t on t.id = p.template_id " +
//            "LEFT JOIN ${schema_name}._sc_performance_management_plan_group as pg on pg.plan_id = p.id " +
//            "LEFT JOIN ${schema_name}._sc_performance_management_plan_role as pr on pr.plan_id = p.id " +
//            "LEFT JOIN ${schema_name}._sc_user_group as ug on ug.group_id = pg.group_id " +
//            "LEFT JOIN ${schema_name}._sc_user_role as ur on ur.role_id = pr.role_id " +
//            "INNER JOIN ${schema_name}._sc_user as u on u.id = ur.user_id AND ug.user_id=u.id " +
//            "LEFT JOIN ${schema_name}._sc_performance_management as pm on pm.user_id =u.id and ((pm.period=#{month} and t.period=1) or (pm.period=#{year} and t.period=2) ) AND pm.template_id = p.template_id " +
//            "WHERE p.isuse=true and t.isuse = true and p.end_date>now() and u.status=1  and p.begin_date<=now() and t.assessment_type=1 and pm.id is null " +
//            "order by p.create_time desc")
//    List<Map<String, Object>> getPerformancePlanPersonDetail(@Param("schema_name") String schemaName,@Param("month")String month,@Param("year")String year);
//
//
//    @Select("select " +
//            "pg.group_id ,p.template_id,p.begin_date,p.end_date,p.create_user_account,p.create_time,t.period,t.assessment_type " +
//            "from ${schema_name}._sc_performance_management_plan as p  " +
//            "LEFT JOIN ${schema_name}._sc_performance_management_template as t on t.id = p.template_id " +
//            "LEFT JOIN ${schema_name}._sc_performance_management_plan_group as pg on pg.plan_id = p.id " +
//            "LEFT JOIN ${schema_name}._sc_performance_management as pm on pm.group_id=pg.group_id and ((pm.period=#{month} and t.period=1) or (pm.period=#{year} and t.period=2) ) AND pm.template_id = p.template_id " +
//            "WHERE p.isuse=true and t.isuse = true and p.end_date>now()  and p.begin_date<=now() and t.assessment_type=2 and pm.id is null " +
//            "order by p.create_time desc")
//    List<Map<String, Object>> getPerformancePlanTeamDetail(@Param("schema_name") String schemaName,@Param("month")String month,@Param("year")String year);
//
//    /**
//     * 查询所有绩效考核模板详情信息
//     * @param schemaName
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_performance_management_template_options")
//    List<Map<String, Object>> getManagementDetail(@Param("schema_name") String schemaName);
//
//    /**
//     * 批量保存绩效查询主表信息
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Insert({
//            "<script>",
//            "insert into ${schema_name}._sc_performance_management( user_id, user_code, template_id, period, total_points, has_score, create_user_account, create_time, assessment_type, group_id, user_account) values ",
//            "<foreach collection='list' item='s' index='index' separator=','>" ,
//            "( #{s.user_id},#{s.user_code},#{s.template_id},#{s.period},#{s.total_points},#{s.has_score},#{s.create_user_account},#{s.create_time},#{s.assessment_type},#{s.group_id},#{s.user_account})",
//            "</foreach>",
//            "</script>"
//    })
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int addPerformanceManagement(@Param("schema_name") String schemaName, @Param("list") List<Map<String, Object>> paramMap);
//
//    /**
//     * 批量保存绩效查询详情表信息
//     * @param schemaName
//     * @param paramMap
//     * @return
//     */
//    @Insert({
//            "<script>",
//            "insert into ${schema_name}._sc_performance_management_detail( pm_id, type, weightiness, score, weight_score, inspection_items, can_fill_in, initial_score, template_options_id ) values ",
//            "<foreach collection='list' item='s' index='index' separator=','>" ,
//            "(#{s.pm_id}::int, #{s.type}, #{s.weightiness}, #{s.score}, #{s.weight_score}, #{s.inspection_items}, #{s.can_fill_in}, #{s.initial_score}, #{s.template_options_id})",
//            "</foreach>",
//            "</script>"
//    })
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    int addPerformanceManageDetail(@Param("schema_name") String schemaName, @Param("list") List<Map<String, Object>> paramMap);
//
//    /**
//     * 查询绩效报告列表
//     * @param schemaName
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT s.num,s.total_avg_points,s.subjective_avg_score,s.objective_avg_score,s.group_id,s.group_name " +
//            "FROM ( " +
//            "   SELECT count(t.id) AS num,g.id AS group_id,g.group_name, " +
//            "       CAST(AVG(t.weight_score) AS DECIMAL(10,0)) AS total_avg_points, " +
//            "       CAST(AVG(t.subjective_score) AS DECIMAL(10,0)) AS subjective_avg_score, " +
//            "       CAST(AVG(t.objective_score) AS DECIMAL(10,0)) AS objective_avg_score " +
//            "   FROM ( " +
//            "       SELECT p.id,p.user_id,p.group_id, " +
//            "           SUM(pd.weight_score) AS weight_score, " +
//            "           SUM(CASE WHEN pd.type = 2 THEN pd.weight_score ELSE 0 END) AS subjective_score, " +
//            "           SUM(CASE WHEN pd.type = 1 THEN pd.weight_score ELSE 0 END) AS objective_score " +
//            "       FROM ${schema_name}._sc_performance_management p " +
//            "       LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON p.id = pd.pm_id " +
//            "       WHERE p.has_score = true ${condition} GROUP BY p.id,p.user_id,p.group_id " +
//            "   ) t " +
//            "   LEFT JOIN ${schema_name}._sc_user_group ug ON t.user_id = ug.user_id " +
//            "   LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//            "   LEFT JOIN ${schema_name}._sc_user u ON t.user_id = u.id " +
//            "   WHERE t.group_id IS NULL ${condition2} GROUP BY g.id,g.group_name " +
//            "   UNION ALL " +
//            "   SELECT count(t.id) AS num,g.id AS group_id,g.group_name," +
//            "       CAST(AVG(t.weight_score) AS DECIMAL(10,0)) AS total_avg_points, " +
//            "       CAST(AVG(t.subjective_score) AS DECIMAL(10,0)) AS subjective_avg_score, " +
//            "       CAST(AVG(t.objective_score) AS DECIMAL(10,0)) AS objective_avg_score " +
//            "   FROM ( " +
//            "       SELECT p.id,p.user_id,p.group_id, " +
//            "           SUM(pd.weight_score) AS weight_score, " +
//            "           SUM(CASE WHEN pd.type = 2 THEN pd.weight_score ELSE 0 END) AS subjective_score, " +
//            "           SUM(CASE WHEN pd.type = 1 THEN pd.weight_score ELSE 0 END) AS objective_score " +
//            "       FROM ${schema_name}._sc_performance_management p " +
//            "       LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON p.id = pd.pm_id " +
//            "       WHERE p.has_score = true ${condition} GROUP BY p.id,p.user_id,p.group_id " +
//            "   ) t " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON t.id = pd.pm_id " +
//            "   LEFT JOIN ${schema_name}._sc_group g ON g.id = t.group_id " +
//            "   WHERE t.user_id IS NULL GROUP BY g.id,g.group_name " +
//            ") s " +
//            "WHERE 1=1 ${condition3} AND s.group_id IS NOT NULL " +
//            "ORDER BY s.group_id DESC LIMIT ${page} OFFSET ${begin} ")
//    List<Map<String, Object>> findPerformanceReportList(@Param("schema_name") String schemaName, @Param("condition") String condition,
//                                                        @Param("condition2") String condition2, @Param("condition3") String condition3,
//                                                        @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询绩效报告列表
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(s.group_id) " +
//            "FROM ( " +
//            "   SELECT count(t.id) AS num,g.id AS group_id,g.group_name, " +
//            "       CAST(AVG(t.weight_score) AS DECIMAL(10,2)) AS total_avg_points, " +
//            "       CAST(AVG(t.subjective_score) AS DECIMAL(10,2)) AS subjective_avg_score, " +
//            "       CAST(AVG(t.objective_score) AS DECIMAL(10,2)) AS objective_avg_score " +
//            "   FROM ( " +
//            "       SELECT p.id,p.user_id,p.group_id, " +
//            "           SUM(pd.weight_score) AS weight_score, " +
//            "           SUM(CASE WHEN pd.type = 2 THEN pd.weight_score ELSE 0 END) AS subjective_score, " +
//            "           SUM(CASE WHEN pd.type = 1 THEN pd.weight_score ELSE 0 END) AS objective_score " +
//            "       FROM ${schema_name}._sc_performance_management p " +
//            "       LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON p.id = pd.pm_id " +
//            "       WHERE p.has_score = true ${condition} GROUP BY p.id,p.user_id,p.group_id " +
//            "   ) t " +
//            "   LEFT JOIN ${schema_name}._sc_user_group ug ON t.user_id = ug.user_id " +
//            "   LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//            "   LEFT JOIN ${schema_name}._sc_user u ON t.user_id = u.id " +
//            "   WHERE t.group_id IS NULL ${condition2} GROUP BY g.id,g.group_name " +
//            "   UNION ALL " +
//            "   SELECT count(t.id) AS num,g.id AS group_id,g.group_name," +
//            "       CAST(AVG(t.weight_score) AS DECIMAL(10,2)) AS total_avg_points, " +
//            "       CAST(AVG(t.subjective_score) AS DECIMAL(10,2)) AS subjective_avg_score, " +
//            "       CAST(AVG(t.objective_score) AS DECIMAL(10,2)) AS objective_avg_score " +
//            "   FROM ( " +
//            "       SELECT p.id,p.user_id,p.group_id, " +
//            "           SUM(pd.weight_score) AS weight_score, " +
//            "           SUM(CASE WHEN pd.type = 2 THEN pd.weight_score ELSE 0 END) AS subjective_score, " +
//            "           SUM(CASE WHEN pd.type = 1 THEN pd.weight_score ELSE 0 END) AS objective_score " +
//            "       FROM ${schema_name}._sc_performance_management p " +
//            "       LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON p.id = pd.pm_id " +
//            "       WHERE p.has_score = true ${condition} GROUP BY p.id,p.user_id,p.group_id " +
//            "   ) t " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON t.id = pd.pm_id " +
//            "   LEFT JOIN ${schema_name}._sc_group g ON g.id = t.group_id " +
//            "   WHERE t.user_id IS NULL GROUP BY g.id,g.group_name " +
//            ") s " +
//            "WHERE 1=1 ${condition3} AND s.group_id IS NOT NULL ")
//    int countPerformanceReportList(@Param("schema_name") String schemaName, @Param("condition") String condition,
//                                                        @Param("condition2") String condition2, @Param("condition3") String condition3);
//
//    /**
//     * 查询绩效报告用户列表
//     * @param schemaName
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT P.id,P.user_id,P.period,g.group_name,u.username,u.user_code,p.template_id,pt.template_name,SUM(pd.weight_score) AS total_score, " +
//            "   SUM(CASE WHEN pd.TYPE = 2 THEN pd.weight_score ELSE 0 END ) AS subjective_score, " +
//            "   SUM(CASE WHEN pd.TYPE = 1 THEN pd.weight_score ELSE 0 END ) AS objective_score " +
//            "FROM ${schema_name}._sc_performance_management P " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON P.ID = pd.pm_id " +
//            "   LEFT JOIN ${schema_name}._sc_user u ON u.id = p.user_id " +
//            "   LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "   LEFT JOIN ${schema_name}._sc_group g ON g.id = ug.group_id " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_template pt ON pt.id = p.template_id " +
//            "WHERE p.has_score = true ${condition} " +
//            "   GROUP BY P.id,P.user_id,P.period,g.group_name,u.username,u.user_code,p.template_id,pt.template_name " +
//            "   ORDER BY P.user_id,P.id DESC LIMIT ${page} OFFSET ${begin} ")
//    List<Map<String, Object>> findPerformanceReportUserList(@Param("schema_name") String schemaName, @Param("condition") String condition,
//                                                        @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询绩效报告用户列表
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM (" +
//            "SELECT P.user_id,P.period,p.template_id " +
//            "FROM ${schema_name}._sc_performance_management P " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON P.ID = pd.pm_id " +
//            "   LEFT JOIN ${schema_name}._sc_user u ON u.id = p.user_id " +
//            "   LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_template pt ON pt.id = p.template_id " +
//            "WHERE p.has_score = true ${condition} " +
//            "   GROUP BY P.user_id,P.period,p.template_id " +
//            ") t")
//    int countPerformanceReportUserList(@Param("schema_name") String schemaName, @Param("condition") String condition);
//
//    /**
//     * 查询绩效报告用户组列表
//     * @param schemaName
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT P.id,P.user_id,P.period,p.template_id,pt.template_name,g2.group_name,SUM ( pd.weight_score ) AS total_score, " +
//            "   SUM ( CASE WHEN pd.TYPE = 2 THEN pd.weight_score ELSE 0 END ) AS subjective_score, " +
//            "   SUM ( CASE WHEN pd.TYPE = 1 THEN pd.weight_score ELSE 0 END ) AS objective_score " +
//            "FROM ${schema_name}._sc_performance_management P " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON P.ID = pd.pm_id " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_template pt ON pt.id = p.template_id " +
//            "   LEFT JOIN ${schema_name}._sc_group g2 ON g2.id = p.group_id " +
//            "WHERE p.has_score = true ${condition} " +
//            "   GROUP BY P.id,P.user_id,P.period,p.template_id,pt.template_name,g2.group_name " +
//            "   ORDER BY P.user_id,P.id DESC LIMIT ${page} OFFSET ${begin} ")
//    List<Map<String, Object>> findPerformanceReportGroupList(@Param("schema_name") String schemaName, @Param("condition") String condition,
//                                                            @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询绩效报告用户列表
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM (" +
//            "SELECT P.user_id,P.period,p.template_id " +
//            "FROM ${schema_name}._sc_performance_management P " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_detail pd ON P.ID = pd.pm_id " +
//            "   LEFT JOIN ${schema_name}._sc_performance_management_template pt ON pt.id = p.template_id " +
//            "WHERE p.has_score = true ${condition} " +
//            "   GROUP BY P.user_id,P.period,p.template_id " +
//            ") t")
//    int countPerformanceReportGroupList(@Param("schema_name") String schemaName, @Param("condition") String condition);

}
