package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.Timestamp;

/**
 * Created by Administrator on 2018/12/14.
 */
@Mapper
public interface WorkProcessMapper {
    @Insert("insert into ${schema_name}._sc_work_process (sub_work_code,status,node_id,operate_user_id,operate_time) values " +
            "(#{sub_work_code},#{status},#{node_id},#{operate_user_id},#{operate_time})")
    int saveWorkProccess(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code,
                         @Param("status") int status, @Param("node_id") String node_id,
                         @Param("operate_user_id") String operate_user_id, @Param("operate_time") Timestamp operate_time);

    /**
     * 按节点号取操作人
     *
     * @return 操作人
     */
    @Select("select operate_user_id from ${schema_name}._sc_work_process " +
            "where sub_work_code = #{sub_work_code} and node_id = #{node_id} " +
            "order by id desc limit 1 ")
    String findOperateUserIdByNodeId(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code,
                                     @Param("node_id") String node_id);

//    /**
//     * 按节点号去最新的进度
//     *
//     * @param schema_name
//     * @param sub_work_code
//     * @return
//     */
//    @Select("SELECT id FROM ${schema_name}._sc_work_process " +
//            "WHERE sub_work_code = #{sub_work_code}  " +
//            "ORDER BY operate_time DESC LIMIT 1 ")
//    public int getIdByTaskId(@Param("schema_name") String schema_name, @Param("sub_work_code") String sub_work_code);
}
