package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssetDutyManMapper {
//
//    @Select("select dm.*,u.username,u.mobile,u.email,wt.type_name as duty_name " +
//            "from ${schema_name}._sc_asset_duty_man dm " +
//            "left join ${schema_name}._sc_work_type wt on dm.duty_id=wt.id " +
//            "left join ${schema_name}._sc_user u on dm.user_account=u.account " +
//            "where dm.asset_id='${asset_id}' order by dm.id")
//    List<AssetDutyManData> findAllAssetDutyManList(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id);
//
//    /**
//     * 新增资产维保人员信息
//     *
//     * @param schema_name
//     * @param asset_id
//     * @param user_account
//     * @param duty_id
//     * @param create_user_account
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_asset_duty_man (asset_id,user_account,duty_id,isuse,remark,create_time,create_user_account) " +
//            "values(#{asset_id},#{user_account},#{duty_id},TRUE,#{remark},current_timestamp,#{create_user_account})")
//    int AddAssetDutyMan(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id,
//                        @Param("user_account") String user_account, @Param("duty_id") Integer duty_id, @Param("remark") String remark, @Param("create_user_account") String create_user_account);
//
//    //如果添加的资产负责人有重复，那么则重新覆盖原来的数据，禁用状态和备注
//    @Update("update ${schema_name}._sc_asset_duty_man set isuse=TRUE,remark=#{remark} where asset_id=#{asset_id} and user_account=#{user_account} and duty_id=#{duty_id} ")
//    int UpdateAssetDutyMan(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id,
//                           @Param("user_account") String user_account, @Param("duty_id") Integer duty_id, @Param("remark") String remark);
//
//    //查找资产负责，按职责查找
//    @Select("select * from ${schema_name}._sc_asset_duty_man where id=${id}")
//    AssetDutyManData findAssetDutyManById(@Param("schema_name") String schema_name, @Param("id") Integer id);
//
//    //查找资产负责，按职责查找
//    @Select("select * from ${schema_name}._sc_asset_duty_man where asset_id='${asset_id}' and user_account='${user_account}' and duty_id=${duty_id} order by id ")
//    List<AssetDutyManData> findAssetDutyMan(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("user_account") String user_account, @Param("duty_id") Integer duty_id);
//
//    //查找资产负责，按职责查找
//    @Select("select * from ${schema_name}._sc_asset_duty_man where asset_id='${asset_id}' and duty_id = ${duty_id} order by id ")
//    List<AssetDutyManData> findAssetDutyManByDuty(@Param("schema_name") String schema_name, @Param("asset_id") String asset_id, @Param("duty_id") Integer duty_id);
//
//    @Update("delete from ${schema_name}._sc_asset_duty_man where id=#{id} ")
//    int DeleteAssetDutyMan(@Param("schema_name") String schema_name, @Param("id") Integer id, @Param("isuse") Boolean isuse);
//
//    //按设备，查询设备的负责人员：保养、巡检、点检、维修
//    @Select("select DISTINCT u.*,uf.facility_id from ${schema_name}._sc_asset_duty_man ad " +
//            "left join ${schema_name}._sc_user u on ad.user_account=u.account " +
//            "LEFT JOIN ${schema_name}._sc_group_org uf ON uf.group_id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_stock_org so ON so.facility_id = uf.org_id " +
//            "where u.status=1 and ad.isuse=TRUE and ad.duty_id=#{duty_id} and ad.asset_id=#{asset_id} and uf.org_id=#{facility_id}")
//    List<User> findDutyUserByAssetId(@Param("schema_name") String schema_name, @Param("duty_id") Integer duty_id, @Param("asset_id") String asset_id, @Param("facility_id") long facility_id);
}
