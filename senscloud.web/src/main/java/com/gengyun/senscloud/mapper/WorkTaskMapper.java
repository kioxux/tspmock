package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.flowable.CustomTaskQueryImpl;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface WorkTaskMapper {

    /**
     * 按天分组查询指定时间范围内的待办任务项
     *
     * @param query 请求参数
     * @return
     */
    List<Map<String, Object>> selectTasksGroupCount(Map<String, Object> query);

//    @Insert("insert into ${schema_name}._sc_task_item  " +
//            "(task_item_code,task_item_name,check_way,check_method,result_type,work_hours,file_ids,isuse,requirements,create_time," +
//            "create_user_account,\"order\",standard ) values " +
//            "(#{t.task_item_code},#{t.task_item_name},#{t.check_way},#{t.check_method},#{t.result_type},#{t.work_hours},#{t.file_ids}," +
//            "#{t.isuse},#{t.requirements},#{t.create_time},#{t.create_user_account},#{t.order},#{t.standard})" )
//    public int addWorkTask(@Param("schema_name") String schema_name, @Param("t") WorkTask workTask);
//    //查找维修单列表
//    @Select("select count(t.task_item_code) " +
//            "from ${schema_name}._sc_task_item t " +
//            "where 1=1 ${condition} ")
//    int getWorkTaskListCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//    //查找维修单列表
//    @Select("select t.*"+
//            " from ${schema_name}._sc_task_item t " +
//            "where 1=1 ${condition} " +
//            "order by t.order limit ${page} offset ${begin}")
//    List<WorkTask> getWorkTaskList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//    @Select("select *"+
//            " from ${schema_name}._sc_task_item " +
//            "where task_item_code=#{task_item_code} " )
//    WorkTask findByID(@Param("schema_name") String schema_name, @Param("task_item_code") String task_item_code);
//    @Update("update ${schema_name}._sc_task_item set " +
//            "task_item_name=#{w.task_item_name}, " +
//            "check_way=#{w.check_way}, " +
//            "check_method=#{w.check_method}, " +
//            "result_type=#{w.result_type}, " +
//            "work_hours=#{w.work_hours}, " +
//            "file_ids=#{w.file_ids}, " +
//            "isuse=#{w.isuse}, " +
//            "requirements=#{w.requirements}, " +
//            "create_user_account=#{w.create_user_account}, " +
//            "create_time=#{w.create_time}, " +
//            "\"order\"=#{w.order}, " +
//            "standard=#{w.standard} " +
//            "where task_item_code=#{w.task_item_code} ")
//    int updateworktask(@Param("schema_name") String schema_name, @Param("w")  WorkTask workTask);
//    //查找所有任务项
//    @Select("select t.*"+
//            " from ${schema_name}._sc_task_item t " +
//            "where 1=1 ${condition} " +
//            "order by t.order ")
//    List<WorkTask> getAllWorkTaskList(@Param("schema_name") String schema_name,@Param("condition") String condition);
//    @Select("select t.*"+
//            " from ${schema_name}._sc_task_item t " +
//            "where 1=1 ${condition}  " )
//    List<WorkTask> getAllTemplateTaskList(@Param("schema_name") String schema_nam,@Param("condition") String condition);
//    //查找比我order小的中最大的一个
//
//    /**
//     * 查询任务项bom列表
//     * @param schema_name
//     * @param taskItemCode
//     * @return
//     */
//    @Select("SELECT ab.ID,ab.bom_model,ab.bom_code,ab.material_code,b.bom_name,b.brand_name,bt.type_name AS bom_type_name,sp.title AS supplier_name " +
//            "FROM ${schema_name}._sc_task_item_bom ab " +
//            "LEFT JOIN ${schema_name}._sc_bom b ON b.material_code = ab.material_code AND ab.bom_code = b.bom_code " +
//            "LEFT JOIN ${schema_name}._sc_bom_type bt ON bt.ID = b.type_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities sp ON sp.ID = b.supplier " +
//            "WHERE ab.task_item_code = #{taskItemCode} ORDER BY ab.ID DESC ")
//    List<Map<String, Object>> getTaskItemBomList(@Param("schema_name") String schema_name, @Param("taskItemCode") String taskItemCode);
//
//    /**
//     * 新增任务项备件
//     * @param schema_name
//     * @param taskItemCode
//     * @param bomModel
//     * @param bomCode
//     * @param materialCode
//     * @param createTime
//     * @param account
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_task_item_bom (task_item_code,bom_model,bom_code,material_code,create_time,create_user_account) " +
//            "values(#{taskItemCode},#{bomModel},#{bomCode},#{materialCode},#{createTime},#{account})")
//    int addTaskItemBom(@Param("schema_name") String schema_name, @Param("taskItemCode") String taskItemCode, @Param("bomModel") String bomModel, @Param("bomCode") String bomCode,
//                        @Param("materialCode") String materialCode, @Param("createTime") Timestamp createTime, @Param("account") String account);
//
//    /**
//     * 通过code查询任务项详情
//     * @param schema_name
//     * @param taskItemCode
//     * @return
//     */
//    @Select("select isuse,standard,task_item_name,check_method,result_type,file_ids,task_item_code,requirements,work_hours,\"order\" from ${schema_name}._sc_task_item where task_item_code = #{taskItemCode}")
//    WorkTask getTaskItemDetailByCode(@Param("schema_name") String schema_name, @Param("taskItemCode") String taskItemCode);
//
//    /**
//     * 删除任务项bom
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_task_item_bom where id=#{id}")
//    int deleteTaskItemBom(@Param("schema_name") String schema_name, @Param("id") long id);
//
//    /**
//     * 查询任务项工具列表
//     * @param schema_name
//     * @param taskItemCode
//     * @return
//     */
//    @Select("SELECT tit.ID,t.tool_name FROM ${schema_name}._sc_task_item_tool tit LEFT JOIN ${schema_name}._sc_tool t ON t.id = tit.tool_id " +
//            "WHERE tit.task_item_code = #{taskItemCode} ORDER BY tit.ID DESC")
//    List<Map<String, Object>> getTaskItemToolList(@Param("schema_name") String schema_name, @Param("taskItemCode") String taskItemCode);
//
//    /**
//     * 新增任务项工具
//     * @param schema_name
//     * @param taskItemCode
//     * @param toolId
//     * @param createTime
//     * @param account
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_task_item_tool (task_item_code,tool_id,create_time,create_user_account) " +
//            "values(#{taskItemCode},#{toolId},#{createTime},#{account})")
//    int addTaskItemTool(@Param("schema_name") String schema_name, @Param("taskItemCode") String taskItemCode, @Param("toolId") long toolId,
//                       @Param("createTime") Timestamp createTime, @Param("account") String account);
//
//    /**
//     * 删除任务项工具
//     * @param schema_name
//     * @param id
//     * @return
//     */
//    @Delete("delete from ${schema_name}._sc_task_item_tool where id=#{id}")
//    int deleteTaskItemTool(@Param("schema_name") String schema_name, @Param("id") long id);
}