package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.entity.UserEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 当前模式下的用户基础表
 */
@Mapper
public interface UserMapper {

    /**
     * 新增用户
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert(" insert into ${schema_name}._sc_user (is_use,status,sys_user,id,account,user_name,user_code," +
            " password,mobile,email,is_charge,gender_tag,remark,join_date,hour_fee,nfc_code) " +
            " values " +
            " (#{pm.is_use},1,'2',#{pm.id},#{pm.account},#{pm.user_name},#{pm.user_code},#{pm.password}," +
            " #{pm.mobile},#{pm.email},#{pm.is_charge}," +
            " #{pm.gender_tag},#{pm.remark},#{pm.join_date},#{pm.hour_fee}::real,#{pm.nfc_code})")
    void insertUser(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

//    /**
//     * 将用户更新入public表中，进行统一记录
//     */
//    @Update("update public.user_tenant_relation " +
//            "set mobile = #{pm.mobile}" +
//            " ,account = #{pm.newAccount}" +
//            "WHERE account= #{pm.account} and company_id=#{pm.company_id} ")
//    void userCompanySyncUpdate(@Param("pm") Map<String, Object> pm);

//    /**
//     * 注销企业表用户
//     */
//    @Delete("delete FROM public.user_tenant_relation " +
//            "WHERE account= #{pm.account} AND company_id =#{pm.company_id} ")
//    void userCompanySyncDelete(@Param("pm") Map<String, Object> pm);

//    /**
//     * 注销企业表选中用户
//     *
//     * @param schema_name 数据库
//     * @param ids         选中主键
//     * @return 成功数量
//     */
//    @Delete(" <script> delete FROM public.user_tenant_relation where company_id =#{pm.company_id} and user_id in " +
//            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{ids} </foreach> </script> ")
//    int userCompanySyncDeleteBySelect(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

//    /**
//     * 新增企业表用户
//     */
//    @Insert("INSERT INTO   public.user_tenant_relation " +
//            " (account,company_id,mobile,user_id) " +
//            " VALUES (#{pm.account},#{pm.company_id}::int,#{pm.mobile},#{pm.user_id})")
//    void insertUserCompanySync(@Param("pm") Map<String, Object> pm);

    /**
     * 删除用户
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Update("UPDATE  ${schema_name}._sc_user  SET status = " + StatusConstant.STATUS_DELETEED + " WHERE id=#{id}")
    void deleteUserById(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 删除选中用户
     *
     * @param schema_name 数据库
     * @param ids         选中主键
     * @return 成功数量
     */
    @Update(" <script> update ${schema_name}._sc_user set status = " + StatusConstant.STATUS_DELETEED + " where id in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{ids} </foreach> </script> ")
    int deleteSelectUser(@Param("schema_name") String schema_name, @Param("ids") String[] ids);

    /**
     * 启用禁用用户
     *
     * @param schema_name 入参
     * @param is_use      入参
     */
    @Update("UPDATE  ${schema_name}._sc_user  SET is_use = #{is_use} " +
            "WHERE id=#{user_id}")
    void changeIsUse(@Param("schema_name") String schema_name, @Param("user_id") String user_id, @Param("is_use") String is_use);

    /**
     * 更新用户表信息
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update("  <script> " +
            " update ${schema_name}._sc_user " +
            " set id= #{pm.id} " +
            " <when test='pm.account!=null'> " +
            " ,account = #{pm.account}" +
            " </when> " +
            " <when test='pm.hour_fee!=null'> " +
            " ,hour_fee = #{pm.hour_fee}::real" +
            " </when> " +
            " <when test='pm.remark!=null'> " +
            " ,remark = #{pm.remark}" +
            " </when> " +
            " <when test='pm.user_name!=null'> " +
            " ,user_name = #{pm.user_name}" +
            " </when> " +
            " <when test='pm.user_code!=null'> " +
            " ,user_code=#{pm.user_code}" +
            " </when> " +
            " <when test='pm.status!=null'> " +
            " ,status = #{pm.status}" +
            " </when> " +
            " <when test='pm.gender_tag!=null'> " +
            " ,gender_tag = #{pm.gender_tag}::int" +
            " </when> " +
            " <when test='pm.mobile!=null'> " +
            " ,mobile = #{pm.mobile}" +
            " </when> " +
            " <when test='pm.email!=null'> " +
            " ,email=#{pm.email}" +
            " </when> " +
            " <when test='pm.is_charge!=null'> " +
            " ,is_charge =#{pm.is_charge}" +
            " </when> " +
            " <when test='pm.is_use!=null'> " +
            " ,is_use =#{pm.is_use}" +
            " </when> " +
            " <when test='pm.nfc_code!=null'> " +
            " ,nfc_code =#{pm.nfc_code}" +
            " </when> " +
            " <when test='pm.join_date!=null'> " +
            " ,join_date =to_date ( #{pm.join_date}, '" + SqlConstant.SQL_DATE_FMT + "' )" +
            " </when> " +
            " WHERE id= #{pm.id}" +
            "  </script>")
    void updateUserWithoutPassword(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 更新用户表信息
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update("UPDATE ${schema_name}._sc_user " +
            "SET password = #{pm.password}" +
            " WHERE id= #{pm.id}")
    void updateUserPassword(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    /**
     * 根据id查询用户记录
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 用户记录
     */
    @Select(" SELECT account,sys_user,user_name,mobile,is_use,create_time," +
            " email,user_code,id,is_charge,language_tag,sys_user," +
            " gender_tag::varchar,join_date,remark,hour_fee,is_on_duty,nfc_code " +
            " FROM ${schema_name}._sc_user" +
            "  WHERE id= #{id}")
    Map<String, Object> findById(@Param("schema_name") String schema_name, @Param("id") String id);

    @Select(" select tb.* from (select user_name,email,mobile from ${schema_name}._sc_user where id=#{id} " +
            " union " +
            " select contact_name as user_name,contact_email as email,contact_mobile as mobile " +
            " from ${schema_name}._sc_customer_contact where id::varchar=#{id}) tb limit 1 ")
    Map<String, Object> findUserInfoById(@Param("schema_name") String schema_name, @Param("id") String id);

    @Select("<script>" +
            " select array_to_string(array_agg(distinct tab.user_name),',') as user_name from " +
            " (select ('【内部人员】'||user_name) as user_name from ${schema_name}._sc_user " +
            " where id in " +
            " <foreach collection='ids.split(\",\")' item='item' open='(' close=')' separator=','> " +
            " #{item} " +
            " </foreach>" +
            " union " +
            " select ('【外部人员】'||contact_name) as user_name " +
            " from ${schema_name}._sc_customer_contact " +
            " where id::varchar in " +
            " <foreach collection='ids.split(\",\")' item='item' open='(' close=')' separator=','> " +
            " #{item} " +
            " </foreach>" +
            " ) tab " +
            "</script>")
    String findAllCCUser(@Param("schema_name") String schema_name, @Param("ids") String ids);

    /**
     * 分页查询用户列表
     *
     * @param schema_name 入参
     * @param params      入参
     * @return 用户列表
     */
    @Select(" <script>" +
            " SELECT  u.id,u.account, u.user_name,u.sys_user,u.mobile,u.is_use," +
            " to_char(u.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time, " +
            " u.email,u.user_code,u.is_on_duty,u.nfc_code " +
            " FROM ${schema_name}._sc_user AS u" +
            " <where> " +
            " u.sys_user <![CDATA[ <> ]]> '1' AND u.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='pm.isUseSearch!=null'> " +
            " AND u.is_use=#{pm.isUseSearch}::varchar  " +
            " </when> " +
            " <when test='pm.positionSearch!=null'>" +
            " AND u.id in (SELECT user_id FROM ${schema_name}._sc_user_position WHERE position_id IN (${pm.positionSearch})) " +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND ( u.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.account LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.mobile LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.id in (" +
            " SELECT   user_id from ( SELECT P.ID FROM ${schema_name}._sc_group AS G LEFT JOIN ${schema_name}._sc_position AS P " +
            " ON G.ID = P.group_id " +
            " WHERE G.group_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')" +
            " UNION SELECT p2.ID  FROM ${schema_name}._sc_position AS p2 " +
            " WHERE P2.position_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')" +
            " ) t1 " +
            " LEFT JOIN ${schema_name}._sc_user_position up ON up.position_id = t1.ID ) )" +
            " </when>" +
            " </where>" +
            " ORDER BY u.create_time  DESC ${pm.pagination} " +
            " </script>  ")
    List<Map<String, Object>> findUserListForPage(@Param("schema_name") String schema_name,
                                                  @Param("pm") Map<String, Object> params);

    /**
     * 分页查询用户列表
     *
     * @param schema_name 入参
     * @param params      入参
     * @return 用户列表
     */
    @Select(" <script>" +
            " SELECT COUNT(1) FROM ( " +
            " SELECT DISTINCT u.id " +
            " FROM ${schema_name}._sc_user AS u" +
            " <where>" +
            " u.sys_user <![CDATA[ <> ]]> '1' AND u.status > " + StatusConstant.STATUS_DELETEED +
            " <when test='pm.isUseSearch!=null'> " +
            " AND u.is_use = #{pm.isUseSearch}::varchar  " +
            " </when> " +
            " <when test='pm.positionSearch!=null'>" +
            " AND u.id in (SELECT user_id FROM ${schema_name}._sc_user_position WHERE position_id IN (${pm.positionSearch})) " +
            " </when>" +
            " <when test='pm.keywordSearch!=null'>" +
            " AND (" +
            "  u.user_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.account LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.mobile LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            " OR u.id IN (" +
            " SELECT user_id from ( SELECT P.ID FROM ${schema_name}._sc_group AS G LEFT JOIN ${schema_name}._sc_position AS" +
            " P ON G.ID = P.group_id " +
            " WHERE G.group_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')" +
            " UNION SELECT p2.ID  FROM ${schema_name}._sc_position AS p2 " +
            " WHERE P2.position_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%')" +
            " ) t1 " +
            " LEFT JOIN ${schema_name}._sc_user_position up ON up.position_id = t1.ID ) ) " +
            " </when>" +
            " </where>" +
            " ) t" +
            " </script>  ")
    int findUserListTotal(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> params);

    /**
     * 根据用户id删除用户岗位关联表
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete("DELETE FROM ${schema_name}._sc_user_position WHERE user_id = #{id}")
    void deleteUserPositionByUserId(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据用户账号查询用户
     *
     * @param schema_name 入参
     * @param account     入参
     * @return 用户
     */
    @Select("SELECT * FROM ${schema_name}._sc_user WHERE id <> #{userId}  AND account = #{account} AND status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findIsUseUserByAccountAndId(@Param("schema_name") String schema_name, @Param("account") String account, @Param("userId") String userId);

    /**
     * 根据用户账号查询用户
     *
     * @param schema_name 入参
     * @param account     入参
     * @return 用户
     */
    @Select("SELECT * FROM ${schema_name}._sc_user WHERE account = #{account} AND status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findIsUseUserByAccount(@Param("schema_name") String schema_name, @Param("account") String account);


    /**
     * 根据用户手机号查询用户
     *
     * @param schema_name 入参
     * @param mobile      入参
     * @return 用户
     */
    @Select("SELECT * FROM ${schema_name}._sc_user WHERE  mobile = #{mobile} AND status  > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findIsUseUserByMobile(@Param("schema_name") String schema_name, @Param("mobile") String mobile);

    /**
     * 根据用户手机号用户id查询用户
     *
     * @param schema_name 入参
     * @param mobile      入参
     * @return 用户
     */
    @Select("SELECT * FROM ${schema_name}._sc_user WHERE id <> #{userId} AND mobile = #{mobile} AND status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findIsUseUserByMobileAndId(@Param("schema_name") String schema_name, @Param("mobile") String mobile, @Param("userId") String userId);


    /**
     * 通过用户名获取登录账号（用于登录）
     *
     * @param schemaName 数据库
     * @param account    用户名
     * @param password   密码
     * @return account
     */
    @Select("select account from ${schema_name}._sc_user t WHERE status > " + StatusConstant.STATUS_DELETEED + " and is_use = '1' and (account = #{account} or user_code = #{account} or mobile = #{account}) and password = #{password}")
    String findUserAccountForLogin(@Param("schema_name") String schemaName, @Param("account") String account, @Param("password") String password);

    /**
     * 通过员工工号获取用户信息
     *
     * @param schemaName 数据库
     * @param userCode   工号
     * @return 用户信息
     */
    @Select("select * from ${schema_name}._sc_user t WHERE status  > " + StatusConstant.STATUS_DELETEED + " and user_code = #{userCode}")
    UserEntity findUserByUserCode(@Param("schema_name") String schemaName, @Param("userCode") String userCode);

    /**
     * 通过员工NFC卡号获取用户信息（用于NFC登录）
     *
     * @param schemaName 数据库
     * @param nfcCode    NFC卡号
     * @return 用户信息
     */
    @Select("select * from ${schema_name}._sc_user t WHERE status  > " + StatusConstant.STATUS_DELETEED + " and nfc_code = #{nfcCode}")
    UserEntity findUserByNfcCode(@Param("schema_name") String schemaName, @Param("nfcCode") String nfcCode);

    /**
     * 通过用户账号获取用户信息（用于登录后缓存在系统）
     *
     * @param schemaName 数据库
     * @param account    账号
     * @return 用户信息
     */
    @Select("select t.*, wx.mini_program_open_id, wx.official_open_id, wx.union_id, wx.wei_xin_code " +
            "from ${schema_name}._sc_user t left join ${schema_name}._sc_user_wei_xin_login_info wx on t.id = wx.user_id " +
            "WHERE status  > " + StatusConstant.STATUS_DELETEED + " and is_use = '1' and account = #{account} ")
    UserEntity findUserForSystemLoginCache(@Param("schema_name") String schemaName, @Param("account") String account);

    /**
     * 通过手机号获取用户信息
     *
     * @param schemaName 数据库
     * @param phone      手机号
     * @return 用户信息
     */
    @Select("select * from ${schema_name}._sc_user t WHERE status  > " + StatusConstant.STATUS_DELETEED + " and mobile = #{phone}")
    UserEntity findUserByPhone(@Param("schema_name") String schemaName, @Param("phone") String phone);


    /**
     * 根据角色获取有操作权限的用户 派案策略变更新方法
     * 加入设备位置为参数作为派案中间连接条件
     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
     *
     * @param schema_name  入参
     * @param roleIds      入参
     * @param positionCode 入参
     * @param categoryId   入参
     * @return 用户列表
     */
    @Select(" <script>" +
            " select DISTINCT u.mobile,u.account,u.user_name, nr.roles as role_names from " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) ur " +
            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
            "  left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            "  INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            "  group by urn.user_id ) nr on u.id = nr.user_id, " +
            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_position sgp " +
            " where u.status=1 and u.mobile is not null and u.mobile != '' AND u.id = t1.user_id and t1.group_id = sg.ID AND sg.ID = sgp.group_id " +
            " <when test=\"positionCode!=null and positionCode!=''\"> " +
            "  AND sgp.position_code = #{positionCode}  " +
            " </when>" +
            " <when test='roleIds!=null'> " +
            " and ur.role_id in " +
            " <foreach collection='roleIds' item='id' index='index' open='(' close=')' separator=','>" +
            " #{id}" +
            " </foreach>" +
            " </when>" +
            " </script>")
    List<Map<String, Object>> getUsersByRoles(@Param("schema_name") String schema_name, @Param("roleIds") List<String> roleIds, @Param("positionCode") String positionCode, @Param("category_id") int categoryId);

    /**
     * 根据角色列表查询用户列表
     *
     * @param schema_name 入参
     * @param roleIds     入参
     * @param facilityId  入参
     * @param categoryId  入参
     * @return 用户列表
     */
    @Select(" <script>" +
            " select DISTINCT u.mobile,u.account,u.user_name, nr.roles as role_names from " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) ur " +
            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
            "  left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            "  INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            "  group by urn.user_id ) nr on u.id = nr.user_id, " +
            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_org sgo " +
            " left join ${schema_name}._sc_facilities f on sgo.org_id = f.ID " +
            " left join ${schema_name}._sc_asset_position_organization apo on f.ID = apo.org_id  " +
            " where u.status=1 and u.mobile is not null and u.mobile != '' AND u.id = t1.user_id and t1.group_id = sg.ID AND sg.ID = sgo.group_id " +
            " <when test=\"facilityId!=null and facilityId!=''\"> " +
            "  AND (apo.position_code = #{facilityId} or f.facility_no = #{facilityId}) " +
            " </when>" +
            " <when test='roleIds!=null'> " +
            " and ur.role_id in " +
            " <foreach collection='roleIds' item='id' index='index' open='(' close=')' separator=','>" +
            " #{id}" +
            "</foreach>" +
            " </when>" +
            " </script>")
    List<Map<String, Object>> findUsersByRoles(@Param("schema_name") String schema_name, @Param("roleIds") List<String> roleIds, @Param("facilityId") String facilityId, @Param("category_id") int categoryId);

    /**
     * 根据服务商获取有操作权限的用户 派案策略变更新方法
     * 只能用设备服务商为参数作为派案中间连接条件
     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
     *
     * @param schema_name 入参
     * @param roleIds     入参
     * @param facilityNo  入参
     * @param categoryId  入参
     * @return 用户列表
     */
    @Select(" <script>" +
            " select DISTINCT u.mobile,u.account,u.user_name, nr.roles as role_names from " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) ur " +
            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
            "  left join (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            "  INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            "  group by urn.user_id ) nr on u.id = nr.user_id, " +
            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_org sgp " +
            " left join ${schema_name}._sc_facilities f on sgp.org_id = f.ID " +
            " where u.status=1 and u.mobile is not null and u.mobile != '' AND u.id = t1.user_id and t1.group_id = sg.ID AND sg.ID = sgp.group_id " +
            " <when test=\"facilityNo!=null and facilityNo!=''\"> " +
            "  AND f.facility_no = #{facilityNo}  " +
            " </when>" +
            " <when test='roleIds!=null'> " +
            " and ur.role_id in " +
            " <foreach collection='roleIds' item='id' index='index' open='(' close=')' separator=','>" +
            " #{id}" +
            " </foreach>" +
            " </when>" +
            " </script>")
    List<Map<String, Object>> getUsersByRolesAndCustomer(@Param("schema_name") String schema_name, @Param("roleIds") List<String> roleIds, @Param("facilityNo") String facilityNo, @Param("category_id") int categoryId);

//    /**
//     * 根据功能权限获取用户 派案策略变更新方法
//     * 加入设备位置为参数作为派案中间连接条件
//     * 后续会将设备类型权限关联到用户组 -zys 2020/03/25
//     *
//     * @param schema_name  入参
//     * @param functionKey  入参
//     * @param positionCode 入参
//     * @param categoryId   入参
//     * @return 用户列表
//     */
//    @Select(" <script>" +
//            " select DISTINCT u.account,u.password,u.mobile,u.status,u.createtime,u.positionid," +
//            " u.departmentname,u.email,u.user_code,u.token,u.logintime,u.id , " +
//            " u.user_name, nr.roles as role_names " +
//            " from public.system_permission sp " +
//            " left join ${schema_name}._sc_role_permission rp on sp.id=rp.permission_id " +
//            " left join " +
//            " (" +
//            " select role_id,user_id from ${schema_name}._sc_user_role " +
//            " <when test=\"category_id>0\"> " +
//            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
//            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            " </when>" +
//            " ) " +
//            " ur on rp.role_id=ur.role_id " +
//            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
//            " left join " +
//            " (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            " group by urn.user_id " +
//            " ) nr on u.id = nr.user_id, " +
//            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_position sgp " +
//            " where sp.key=#{functionKey} sgp.position_code = #{positionCode}  " +
//            " AND u.id = t1.user_id and u.status=1 and t1.group_id = sg.ID AND sg.ID = sgp.group_id " +
//            " </script>")
//    List<Map<String, Object>> getUserByFunctionStrKey(@Param("schema_name") String schema_name, @Param("functionKey") String functionKey, @Param("positionCode") String positionCode, @Param("categoryId") int categoryId);

    @Select(" <script>" +
            " select DISTINCT u.account,u.password,u.mobile,u.status,u.createtime,u.positionid," +
            " u.departmentname,u.email,u.user_code,u.token,u.logintime,u.id , " +
            " u.user_name, nr.roles as role_names, rp.permission_id " +
            " from ${schema_name}._sc_role_permission rp " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            " ur on rp.role_id=ur.role_id " +
            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
            " left join " +
            " (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id " +
            " ) nr on u.id = nr.user_id, " +
            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_position sgp " +
            " where sp.key=#{functionKey} sgp.position_code = #{positionCode}  " +
            " AND u.id = t1.user_id and u.status=1 and t1.group_id = sg.ID AND sg.ID = sgp.group_id " +
            " </script>")
    List<Map<String, Object>> getUserByFunctionStrKey(@Param("schema_name") String schema_name, @Param("functionKey") String functionKey, @Param("positionCode") String positionCode, @Param("categoryId") int categoryId);

//    /**
//     * 按功能权限查询人员
//     *
//     * @param schema_name 入参
//     * @param functionKey 入参
//     * @param facilityNO  入参
//     * @param category_id 入参
//     * @return 用户列表
//     */
//    @Select(" <script>" +
//            " select DISTINCT u.account,u.password,u.mobile,u.status,u.createtime,u.positionid," +
//            " u.departmentname,u.email,u.user_code,u.token,u.logintime,u.id , " +
//            " u.user_name, nr.roles as role_names " +
//            " from public.system_permission sp " +
//            " left join ${schema_name}._sc_role_permission rp on sp.id=rp.permission_id " +
//            " left join " +
//            " (" +
//            " select role_id,user_id from ${schema_name}._sc_user_role " +
//            " <when test=\"category_id>0\"> " +
//            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
//            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
//            " </when>" +
//            " ) " +
//            " ur on rp.role_id=ur.role_id " +
//            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
//            " left join " +
//            " (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
//            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
//            " group by urn.user_id " +
//            " ) nr on u.id = nr.user_id, " +
//            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_org sgo " +
//            " left join ${schema_name}._sc_facilities f on sgo.org_id = f.ID " +
//            " left join ${schema_name}._sc_asset_position_organization apo on f.ID = apo.org_id " +
//            " where sp.key=#{functionKey} and (apo.position_code =#{facility_no} or f.facility_no=#{facility_no}) " +
//            " AND u.id = t1.user_id and u.status=1 and t1.group_id = sg.ID AND sg.ID = sgo.group_id " +
//            " </script>")
//    List<Map<String, Object>> findUserByFunctionStrKey(@Param("schema_name") String schema_name, @Param("functionKey") String functionKey, @Param("facility_no") String facilityNO, @Param("category_id") int category_id);

    @Select(" <script>" +
            " select DISTINCT u.account,u.password,u.mobile,u.status,u.createtime,u.positionid," +
            " u.departmentname,u.email,u.user_code,u.token,u.logintime,u.id , " +
            " u.user_name, nr.roles as role_names, rp.permission_id " +
            " from ${schema_name}._sc_role_permission rp " +
            " left join " +
            " (" +
            " select role_id,user_id from ${schema_name}._sc_user_role " +
            " <when test=\"category_id>0\"> " +
            " where role_id in ( select role_id from ${schema_name}._sc_role_asset_type where category_id=#{category_id} ) " +
            " or role_id not in ( select role_id from ${schema_name}._sc_role_asset_type) " +
            " </when>" +
            " ) " +
            " ur on rp.role_id=ur.role_id " +
            " left join ${schema_name}._sc_user u on ur.user_id=u.id " +
            " left join " +
            " (select urn.user_id, string_agg(name||'' , ',') roles from ${schema_name}._sc_user_role urn " +
            " INNER JOIN ${schema_name}._sc_role rn on urn.role_id = rn.id " +
            " group by urn.user_id " +
            " ) nr on u.id = nr.user_id, " +
            " ${schema_name}._sc_user_group t1, ${schema_name}._sc_group sg, ${schema_name}._sc_group_org sgo " +
            " left join ${schema_name}._sc_facilities f on sgo.org_id = f.ID " +
            " left join ${schema_name}._sc_asset_position_organization apo on f.ID = apo.org_id " +
            " where sp.key=#{functionKey} and (apo.position_code =#{facility_no} or f.facility_no=#{facility_no}) " +
            " AND u.id = t1.user_id and u.status=1 and t1.group_id = sg.ID AND sg.ID = sgo.group_id " +
            " </script>")
    List<Map<String, Object>> findUserByFunctionStrKey(@Param("schema_name") String schema_name, @Param("functionKey") String functionKey, @Param("facility_no") String facilityNO, @Param("category_id") int category_id);

    /**
     * 根据userid列表获取用户信息列表
     *
     * @param schema_name 入参
     * @param userIds     入参
     * @return 用户信息列表
     */
    @Select(" <script>" +
            " SELECT id,user_name FROM ${schema_name}._sc_user " +
            " WHERE status > " + StatusConstant.STATUS_DELETEED +
            " AND is_use = '1' " +
            " AND id IN " +
            " <foreach collection='userIds' item='id' index='index' open='(' close=')' separator=','>" +
            " #{id}" +
            " </foreach>" +
            " </script>")
    List<Map<String, Object>> findUserListByIds(@Param("schema_name") String schema_name, @Param("userIds") List<String> userIds);

//    /**
//     * 查询微信公众号用户信息
//     *
//     * @param schemaName 数据库
//     * @param paramMap   参数
//     * @return 用户列表
//     */
//    @Select("select count(1) from public.wei_xin_office_user where wei_xin_office_user=#{s.wx_code} and open_id=#{s.open_id}")
//    int countWxGzhUser(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);

//    /**
//     * 新增关注的公众号用户
//     *
//     * @param schemaName 数据库
//     * @param paramMap   参数
//     * @return 成功数量
//     */
//    @Insert("insert into public.wei_xin_office_user (id,wei_xin_office_user,open_id,union_id,wei_xin_mobile,create_time) " +
//            "values (#{s.id},#{s.wx_code},#{s.open_id},#{s.union_id},#{s.wx_mobile},CURRENT_TIMESTAMP)")
//    int insertWxGzhUser(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);

//    /**
//     * 删除关注的公众号用户
//     *
//     * @param wx_code 微信号
//     * @param openId  微信公众号open_id
//     * @return 成功数量
//     */
//    @Insert("DELETE FROM public.wei_xin_office_user where wei_xin_office_user=#{wx_code} and open_id= #{openId}")
//    int deleteWxGzhUser(@Param("wx_code") String wx_code, @Param("openId") String openId);

    /**
     * 更新用户微信公众号openid
     *
     * @param schemaName 数据库
     * @param unionId    微信union_id
     * @param openId     微信公众号open_id
     * @return 成功数量
     */
    @Update("update ${schema_name}._sc_user_wei_xin_login_info set official_open_id = #{openId} where union_id = #{unionId}")
    int updateUserWxGzhOpenId(@Param("schema_name") String schemaName, @Param("unionId") String unionId, @Param("openId") String openId);

    /**
     * 清空用户微信公众号openid
     *
     * @param schemaName 数据库
     * @param openId     微信公众号open_id
     * @return 成功数量
     */
    @Update("update ${schema_name}._sc_user_wei_xin_login_info set official_open_id = '' where official_open_id = #{openId}")
    int deleteUserWxGzhOpenId(@Param("schema_name") String schemaName, @Param("openId") String openId);

    /**
     * 根据用户id获取所有岗位和部门名称拼接字符串
     *
     * @param schemaName 数据库
     * @param user_id    用户id
     * @return 岗位和部门名称拼接字符串
     */
    @Select(" SELECT string_agg(distinct g.group_name || '/' || p.position_name,  ',')  as gp_name" +
            " FROM ${schema_name}._sc_user_position up" +
            " INNER JOIN ${schema_name}._sc_position P ON up.position_id = P.id" +
            " INNER JOIN ${schema_name}._sc_group G ON g.id = p.group_id" +
            " WHERE up.user_id = #{user_id} ")
    String findGroupPositionNameString(@Param("schema_name") String schemaName, @Param("user_id") String user_id);

    /**
     * 根据用户主键获取岗位和部门列表
     *
     * @param schemaName 数据库
     * @param userId     用户主键
     * @return 用户岗位和部门列表
     */
    @Select(" SELECT DISTINCT g.group_name || '/' || p.position_name as gp_name, g.group_name, p.position_name, g.id as group_id, p.id as position_id " +
            " FROM ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position P ON up.position_id = P.id " +
            " INNER JOIN ${schema_name}._sc_group G ON g.id = p.group_id " +
            " WHERE up.user_id = #{userId} " +
            " order by group_id, position_id ")
    List<Map<String, Object>> findUserGpList(@Param("schema_name") String schemaName, @Param("userId") String userId);

    /**
     * 根据userId和密码判断是否存在
     *
     * @param schemaName 入参
     * @param userId     入参
     * @param psd        入参
     * @return 入参
     */
    @Select("select count(account) from ${schema_name}._sc_user where id = #{user_id} and password = #{password} and status  > " + StatusConstant.STATUS_DELETEED)
    int findUserInfoByUserIdAndPwd(@Param("schema_name") String schemaName, @Param("user_id") String userId, @Param("password") String psd);

    /**
     * 查询所有启用，在线的用户
     *
     * @param schema_name 入参
     * @return 入参
     */
    @Select("SELECT account,id,user_name,mobile,user_code,hour_fee FROM ${schema_name}._sc_user WHERE status > " + StatusConstant.STATUS_CANCELLED)
    @MapKey("id")
    Map<String, Map<String, Object>> queryUsers(@Param("schema_name") String schema_name);

    /**
     * 验证用户名称是否存在
     *
     * @param schema_name 入参
     * @return 入参
     */
    @Select("<script>select distinct u.id,u.user_name,u.user_code, " +
            "(SELECT p.group_id FROM ${schema_name}._sc_user_position up INNER JOIN ${schema_name}._sc_position p ON p.id = up.position_id WHERE up.user_id = u.id LIMIT 1) as group_id " +
            " from ${schema_name}._sc_user u " +
            "where u.status > " + StatusConstant.STATUS_CANCELLED + " and u.user_name in " +
            "<foreach collection='userNames' item='userName' open='(' close=')' separator=','> " +
            "#{userName} " +
            "</foreach></script>")
    @MapKey("user_name")
    Map<String, Map<String, Object>> queryUsersByName(@Param("schema_name") String schema_name, @Param("userNames") List<String> userNameList);

    /**
     * 查询用户微信账号信息
     *
     * @param schema_name 数据库
     * @param userId      用户主键
     * @return 微信账号信息
     */
    @Select("select * from ${schema_name}._sc_user_wei_xin_login_info where user_id = #{userId} limit 1")
    Map<String, Object> queryUserWxLoginInfo(@Param("schema_name") String schema_name, @Param("userId") String userId);

    /**
     * 查询用户微信账号信息
     *
     * @param schema_name
     * @param account
     * @return
     */
    @Select("select uw.* from ${schema_name}._sc_user_wei_xin_login_info uw LEFT JOIN ${schema_name}._sc_user u ON uw.user_id = u.id where account = #{account}")
    List<Map<String, Object>> queryUserWxLoginInfoByAccount(@Param("schema_name") String schema_name, @Param("account") String account);

    /**
     * 更新用户微信主键
     *
     * @param schema_name
     * @param userId
     * @param wxOpenId
     * @return
     */
    @Update("update ${schema_name}._sc_user_wei_xin_login_info set mini_program_open_id = #{wxOpenId},union_id = #{unionId}, nick_name=#{nickName} where user_id = #{userId}")
    int updateUserWxOpenId(@Param("schema_name") String schema_name, @Param("wxOpenId") String wxOpenId, @Param("unionId") String unionId, @Param("nickName") String nickName, @Param("userId") String userId);

    /**
     * 更新用户微信主键
     *
     * @param schema_name
     * @param userId
     * @param wxOpenId
     * @return
     */
    @Update("update ${schema_name}._sc_user_wei_xin_login_info set mini_program_open_id = #{wxOpenId},union_id = #{unionId}, official_open_id = #{oaOpenId}, nick_name = #{nickName} where user_id = #{userId}")
    int updateUserWxOpenIdAndOaOpenId(@Param("schema_name") String schema_name, @Param("wxOpenId") String wxOpenId, @Param("unionId") String unionId, @Param("oaOpenId") String oaOpenId, @Param("nickName") String nickName, @Param("userId") String userId);

    /**
     * 更新用户微信主键
     *
     * @param schema_name
     * @param userId
     * @return
     */
    @Update("update ${schema_name}._sc_user_wei_xin_login_info set mini_program_open_id = '',union_id = '', official_open_id = '',nick_name = '' where user_id = #{userId}")
    int clearWxOpenIdAndOaOpenId(@Param("schema_name") String schema_name, @Param("userId") String userId);

    /**
     * 更新用户微信主键
     *
     * @param schema_name
     * @param userId
     * @param wxOpenId
     * @return
     */
    @Insert("insert into ${schema_name}._sc_user_wei_xin_login_info (user_id,mini_program_open_id,union_id,official_open_id,nick_name) " +
            "values(#{userId},#{wxOpenId},#{unionId},#{oaOpenId},#{nickName})")
    int insertUserWxOpenId(@Param("schema_name") String schema_name, @Param("userId") String userId, @Param("wxOpenId") String wxOpenId, @Param("unionId") String unionId, @Param("oaOpenId") String oaOpenId, @Param("nickName") String nickName);

//    /**
//     * 查询微信公众号用户信息
//     *
//     * @param unionId
//     * @return
//     */
//    @Select("select * from public.wei_xin_office_user where union_id=#{unionId}")
//    List<Map<String, Object>> queryWxoaUserByUnionId(@Param("unionId") String unionId);

//    /**
//     * 查看当前公司最大可用用户数
//     *
//     * @param company_id 入参
//     * @return 当前公司最大可用用户数
//     */
//    @Select("SELECT user_total FROM public.company_hire_info WHERE company_id = #{company_id} ")
//    int findCompanyMaxUserTotal(@Param("company_id") Long company_id);

    /**
     * 查看当前启用的用户数
     *
     * @param schema_name 入参
     * @return 当前启用的用户数
     */
    @Select("SELECT COUNT(ID) FROM ${schema_name}._sc_user WHERE status > -1000 AND is_use = '1' ")
    int findNowCompanyUserTotal(@Param("schema_name") String schema_name);

    /**
     * 更新上下班状态
     *
     * @param schema_name
     * @param user_id
     * @param is_on_duty
     */
    @Update(" UPDATE ${schema_name}._sc_user " +
            " set is_on_duty = #{is_on_duty} " +
            " where id = #{user_id} ")
    void changeUserIsOnDuty(@Param("schema_name") String schema_name, @Param("user_id") String user_id, @Param("is_on_duty") String is_on_duty);

    /**
     * 根据id字符串获取用户信息
     *
     * @param schemaName
     * @param ids
     * @return
     */
    @Select(" <script>" +
            " SELECT  u.id,u.account, u.user_name,u.sys_user,u.mobile,u.is_use," +
            " to_char(u.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time, " +
            " u.email,u.user_code,u.nfc_code " +
            " FROM ${schema_name}._sc_user AS u " +
            " where u.status > " + StatusConstant.STATUS_DELETEED +
            " and u.id in (${ids}) " +
            " ORDER BY u.create_time DESC " +
            " </script>  ")
    List<Map<String, Object>> findUserListForIds(@Param("schema_name") String schemaName, @Param("ids") String ids);

    /**
     * 查询全部账号
     *
     * @param schemaName
     * @return
     */
    @Select("SELECT id,user_name,user_code,account,mobile from ${schema_name}._sc_user where status > " + StatusConstant.STATUS_DELETEED + " ORDER BY id ")
    @MapKey("account")
    Map<String, Map<String, Object>> findAccountListMap(@Param("schema_name") String schemaName);

    /**
     * 查询全部手机号
     *
     * @param schemaName
     * @return
     */
    @Select("SELECT id,user_name,user_code,account,mobile from ${schema_name}._sc_user where status > " + StatusConstant.STATUS_DELETEED + " ORDER BY id ")
    @MapKey("mobile")
    Map<String, Map<String, Object>> findMobileListMap(@Param("schema_name") String schemaName);

    @Select("<script>select distinct id,user_name,account,mobile from ${schema_name}._sc_user " +
            "where status>-1000 and mobile in " +
            "<foreach collection='list' item='mobile' open='(' close=')' separator=','> " +
            "#{mobile} " +
            "</foreach></script>")
    @MapKey("mobile")
    Map<String, Map<String, Object>> findUserByMobile(@Param("schema_name") String schemaName, @Param("list") List<String> mobileList);

    @Select("<script>select distinct id,user_name,account,mobile from ${schema_name}._sc_user " +
            "where status>-1000 and account in " +
            "<foreach collection='list' item='account' open='(' close=')' separator=','> " +
            "#{account} " +
            "</foreach></script>")
    @MapKey("account")
    Map<String, Map<String, Object>> findUserByAccount(@Param("schema_name") String schemaName, @Param("list") List<String> accountList);

    @Select("SELECT id,user_name,user_code,account,mobile,account||'-'||mobile as account_mobile from ${schema_name}._sc_user where status > " + StatusConstant.STATUS_DELETEED + " ORDER BY id")
    @MapKey("account_mobile")
    Map<String, Map<String, Object>> findAccountMobileListMap(@Param("schema_name") String schemaName);
}
