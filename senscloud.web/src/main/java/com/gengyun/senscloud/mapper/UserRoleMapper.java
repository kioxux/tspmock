package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleMapper {
//    /**
//     * 按条件查询记录数
//     */
//    @Select("select count(1) from ${schema_name}._sc_role where available = 'true' ${condition} ")
//    int getUserRoleCount(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     * 按条件查询记录
//     */
//    @Select("select * from ${schema_name}._sc_role where available = 'true' ${condition} " +
//            "order by id desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getUserRoleList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 查询所有记录
//     */
//    @Select("select * from ${schema_name}._sc_role")
//    List<UserRole> findAll(@Param("schema_name") String schema_name);
//
//    /**
//     * 查询所有用户自定义角色
//     */
//    @Select("select * from ${schema_name}._sc_role where sys_role=false")
//    List<UserRole> findAllCustomRole(@Param("schema_name") String schema_name);
//
//    @Select("select * from ${schema_name}._sc_role where id in (${roleId}) ")
//    List<UserRole> findById(@Param("schema_name") String schema_name, @Param("roleId") String roleId);
//
//    @Select("select * from ${schema_name}._sc_role where name=#{roleName}")
//    UserRoleBase findByName(@Param("schema_name") String schema_name, @Param("roleName") String roleName);
//
//    @Insert("insert into ${schema_name}._sc_role(id,name,description,available,createtime) " +
//            "values(#{role.id},#{role.name},#{role.description},#{role.available},#{role.createtime})")
//    void insertRole(@Param("schema_name") String schema_name, @Param("role") UserRole userRole);
//
//    @Update("update ${schema_name}._sc_role set name=#{role.name},description=#{role.description},available=#{role.available} where id=#{role.id}")
//    void updateRole(@Param("schema_name") String schema_name, @Param("role") UserRole userRole);
//
//    @Delete("delete from ${schema_name}._sc_role where id=#{roleId}")
//    void deleteRole(@Param("schema_name") String schema_name, @Param("roleId") String roleId);
//
//    @Delete("delete from ${schema_name}._sc_user_role where role_id=#{roleId}")
//    void deleteUserRole(@Param("schema_name") String schema_name, @Param("roleId") String roleId);
//
//    @Delete("delete from ${schema_name}._sc_user_role where user_id=#{user_id}")
//    void deleteUserRoleByUserId(@Param("schema_name") String schema_name, @Param("user_id") String roleId);
//
//    @Select("select id,name from ${schema_name}._sc_role where id not in (select DISTINCT role_id from #{schema_name}._sc_user_role )")
//    List<Map<String, Object>> checkUserByRole(@Param("schema_name") String schema_name);
//
//    @Select("select id,name from ${schema_name}._sc_role where id  in (select DISTINCT role_id from ${schema_name}._sc_user_role where user_id = #{u.id} )")
//    List<Map<String, Object>> checkUserIdByRole(@Param("schema_name") String schema_name, @Param("u") User user);
//
//    /**
//     * 删除角色设备类型
//     *
//     * @param schema_name
//     * @param role_id
//     */
//    @Insert("delete from ${schema_name}._sc_role_asset_type where role_id=#{role_id}")
//    void deleteRoleAssetCategory(@Param("schema_name") String schema_name, @Param("role_id") String role_id);
//
//
//    /**
//     * 取角色设备类型
//     *
//     * @param schema_name
//     * @param role_id
//     * @return
//     */
//    @Select("SELECT t1.category_id FROM ${schema_name}._sc_role_asset_type t1, ${schema_name}._sc_asset_category t2 " +
//            "where t1.role_id=#{role_id} and t1.category_id=t2.id and t2.isuse = '1' ")
//    List<String> getRoleAcList(@Param("schema_name") String schema_name, @Param("role_id") String role_id);
}
