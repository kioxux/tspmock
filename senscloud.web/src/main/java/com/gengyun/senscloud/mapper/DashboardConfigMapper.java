package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DashboardConfigMapper {
//
//    @Select("select * from ${schema_name}._sc_dashboard order by id ")
//    List<DashboardConfigData> findAllDashboardConfigList(@Param("schema_name") String schema_name);
//
//    /**
//     * 新增看板配置信息
//     *
//     * @param schema_name
//     * @param title
//     * @param snippet
//     * @param description
//     * @param datasource
//     * @return
//     */
//    @Insert("insert into ${schema_name}._sc_dashboard (title,snippet,showtype,datetype,description,datasource,isshow,boardtype) " +
//            "values('${title}','${snippet}','${showtype}','${datetype}','${description}','${datasource}',FALSE,'${boardtype}')")
//    int AddDashboardConfig(@Param("schema_name") String schema_name, @Param("title") String title, @Param("snippet") String snippet,
//                           @Param("description") String description, @Param("showtype") String showtype, @Param("datetype") String datetype, @Param("datasource") String datasource,@Param("boardtype") String boardtype);
//
//    /**
//     * 修改看板配置信息
//     *
//     * @param schema_name
//     * @param id
//     * @param title
//     * @param snippet
//     * @param description
//     * @return
//     */
//    @Update("update ${schema_name}._sc_dashboard set title='${title}',snippet='${snippet}',showtype='${showtype}',datetype='${datetype}',description='${description}',boardtype='${boardtype}' where id='${id}'")
//    int EditDashboardConfig(@Param("schema_name") String schema_name, @Param("id") Integer id, @Param("title") String title,
//                            @Param("snippet") String snippet, @Param("showtype") String showtype, @Param("datetype") String datetype, @Param("description") String description,@Param("boardtype") String boardtype);
//
//    /**
//     * 修改看板配置条件
//     *
//     * @param schema_name
//     * @param id
//     * @param datasource
//     * @return
//     */
//    @Update("update ${schema_name}._sc_dashboard set datasource='${datasource}' where id='${id}'")
//    int EditDashboardDatasource(@Param("schema_name") String schema_name, @Param("id") Integer id, @Param("datasource") String datasource);
//
//
//    @Delete("delete from ${schema_name}._sc_dashboard where id=${id} ")
//    int DeleteDashboardConfig(@Param("schema_name") String schema_name, @Param("id") Integer id);
//
//    /*
//     * 查询看板配置
//     */
//    @Select("select * from ${schema_name}._sc_dashboard where id=${id}")
//    DashboardConfigData findDashboardConfigData(@Param("schema_name") String schema_name, @Param("id") Integer id);
//
//    /*
//     * 更新看板显示状态：是否显示
//     */
//    @Update("update ${schema_name}._sc_dashboard set isshow=${isshow} where id='${id}'")
//    int UpdateDashboardIsShow(@Param("schema_name") String schema_name, @Param("id") Integer id, @Param("isshow") boolean isshow);
}
