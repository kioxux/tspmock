package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 工单模板
 * User: sps
 * Date: 2018/11/16
 * Time: 下午14:00
 */
@Mapper
public interface MetadataWorkMapper {
//    @InsertProvider(type = MetadataWorkMapper.MetadataWorkMapperProvider.class, method = "insert")
//    void insert(@Param("schema_name") String schemaName, @Param("s") MetadataWork metadataWork);
//
//    /**
//     * 查询列表（分页）
//     *
//     * @param schemaName
//     * @param orderBy
//     * @param pageSize
//     * @param begin
//     * @param workTemplateName
//     * @param type
//     * @return
//     */
//    @SelectProvider(type = MetadataWorkMapper.MetadataWorkMapperProvider.class, method = "query")
//    List<MetadataWork> query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                             @Param("pageSize") int pageSize, @Param("begin") int begin,
//                             @Param("workTemplateName") String workTemplateName, String type);
//
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param condition
//     * @return
//     */
//    @SelectProvider(type = MetadataWorkMapper.MetadataWorkMapperProvider.class, method = "queryByCondition")
//    List<MetadataWork> queryByCondition(@Param("schema_name") String schemaName, @Param("condition") String condition);
//
//    /**
//     * 查询列表（统计）
//     *
//     * @param schemaName
//     * @param workTemplateName
//     * @param type
//     * @return
//     */
//    @SelectProvider(type = MetadataWorkMapper.MetadataWorkMapperProvider.class, method = "countByCondition")
//    int countByCondition(@Param("schema_name") String schemaName,
//                         @Param("workTemplateName") String workTemplateName, String type);
//
//    /**
//     * 查询列表
//     *
//     * @param schemaName
//     * @param type
//     * @return
//     */
//    @Select("select work_template_code AS workTemplateCode, isuse AS isuse, " +
//            "work_template_name AS workTemplateName, relation_type AS relationType, " +
//            "create_time AS createTime, create_user_account AS createUserAccount, " +
//            "is_have_sub AS isHaveSub, " +
//            "work_type_id AS workTypeId, " +
//            "template_type AS templateType " +
//            "from ${schema_name}._sc_work_template " +
//            "where isuse = '1' and parent_code = '0' and is_common_template = '0' " +
//            "order by createTime desc ")
//    List<MetadataWork> queryAll(@Param("schema_name") String schemaName, String type);
//
//    /**
//     * 查询所有可用的模板名称和编号
//     *
//     * @param schemaName
//     * @param type
//     * @param isCommonTemplate
//     * @return
//     */
//    @Select("select work_template_code as code, work_template_name as desc, relation_type as relation " +
//            "from ${schema_name}._sc_work_template " +
//            "where isuse = '1' and parent_code ${type} '0' and is_common_template = #{isCommonTemplate}::bit order by work_template_name ")
//    List<Map<String, Object>> queryCodeAndDesc(@Param("schema_name") String schemaName, @Param("type") String type, @Param("isCommonTemplate") String isCommonTemplate);
//
//    /**
//     * 查询所有可用的公共模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    @Select("select body_property " +
//            "from ${schema_name}._sc_work_template " +
//            "where isuse = '1' and is_common_template = '1' and body_property is not null")
//    List<Map<String, Object>> commonTemplateList(@Param("schema_name") String schemaName);
//
//    /**
//     * 查询可用的系统模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_work_template " +
//            "where isuse = '1' and parent_code = '-1' and body_property is not null")
//    Map<String, Object> getSystemTemplate(@Param("schema_name") String schemaName);
//
//    /**
//     * 根据主键查询模板信息
//     *
//     * @param schemaName
//     * @return
//     */
//    @SelectProvider(type = MetadataWorkMapper.MetadataWorkMapperProvider.class, method = "queryById")
//    MetadataWork queryById(@Param("schema_name") String schemaName, @Param("workTemplateCode") String workTemplateCode);
//
//    /**
//     * 根据主键查询模板信息（不包含模板字段body信息）
//     *
//     * @param schemaName
//     * @return
//     */
//    @Select("select work_template_code AS workTemplateCode," +
//            "work_template_name AS workTemplateName," +
//            "relation_type AS relationType,parent_code AS parentCode," +
//            "work_type_id AS workTypeId,template_type AS templateType, " +
//            "isuse AS isuse,remark AS remark," +
//            "create_time AS createTime,create_user_account AS createUserAccount, " +
//            "is_have_sub AS isHaveSub " +
//            "from ${schema_name}._sc_work_template " +
//            "where work_template_code = #{workTemplateCode}")
//    MetadataWork queryGeneralById(@Param("schema_name") String schemaName, @Param("workTemplateCode") String workTemplateCode);
//
//    /**
//     * 更新数据
//     *
//     * @param schemaName
//     * @param metadataWork
//     * @return
//     */
//    @Update("update ${schema_name}._sc_work_template set " +
//            "work_template_name=#{s.workTemplateName}, " +
//            "relation_type=#{s.relationType}, " +
//            "parent_code=#{s.parentCode}, " +
//            "template_type=#{s.templateType}, " +
//            "work_type_id=#{s.workTypeId}, " +
//            "body_property=#{s.bodyProperty}::jsonb, " +
//            "isuse=#{s.isuse}::bit, " +
//            "is_have_sub=#{s.isHaveSub}, " +
//            "remark=#{s.remark} " +
//            "where work_template_code=#{s.workTemplateCode} ")
//    int update(@Param("schema_name") String schemaName, @Param("s") MetadataWork metadataWork);
//
//    /**
//     * 更新使用状态
//     *
//     * @param schemaName
//     * @param isuse
//     * @param workTemplateCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_work_template set isuse=#{isuse}::bit " +
//            "where work_template_code=#{workTemplateCode} ")
//    int updateStatus(@Param("schema_name") String schemaName, @Param("isuse") String isuse,
//                     @Param("workTemplateCode") String workTemplateCode);
//
//    /**
//     * 按业务编号和详情类型获取模板信息
//     *
//     * @param schemaName
//     * @param businessTypeId
//     * @param templateType
//     * @return
//     */
//    @Select("select t.* from ${schema_name}._sc_work_template t " +
//            "INNER JOIN ${schema_name}._sc_work_type wt on wt.business_type_id = #{businessTypeId} and t.work_type_id = wt.id " +
//            "where t.isuse = '1' and t.template_type = #{templateType} " +
//            "limit 1")
//    Map<String, Object> getWdTemplateByBusinessType(@Param("schema_name") String schemaName, @Param("businessTypeId") Integer businessTypeId, @Param("templateType") Integer templateType);
//
//    /**
//     * 按字段信息获取模板信息（单条数据）
//     *
//     * @param schemaName
//     * @param code
//     * @param value
//     * @return
//     */
//    @Select("select * from (select jsonb_array_elements(t.body_property) j, t.work_type_id, t.work_template_code from ${schema_name}._sc_work_template t where t.isuse = '1') t " +
//            "where j->>'fieldCode' = #{code} and j->>'fieldValue' = #{value} limit 1 ")
//    Map<String, Object> getWdtByCodeInfo(@Param("schema_name") String schemaName, @Param("code") String code, @Param("value") String value);
//
//    /**
//     * 实现类
//     */
//    class MetadataWorkMapperProvider {
//
//        /**
//         * 新增数据
//         *
//         * @param schemaName
//         * @param metadataWork
//         * @return
//         */
//        public String insert(@Param("schema_name") String schemaName, @Param("s") MetadataWork metadataWork) {
//            return new SQL() {{
//                INSERT_INTO(schemaName + "._sc_work_template");
//                VALUES("work_template_code", "#{s.workTemplateCode}");
//                VALUES("work_template_name", "#{s.workTemplateName}");
//                VALUES("work_type_id", "#{s.workTypeId}");
//                VALUES("template_type", "#{s.templateType}");
//                VALUES("relation_type", "#{s.relationType}");
//                VALUES("parent_code", "#{s.parentCode}");
//                VALUES("body_property", "#{s.bodyProperty}::jsonb");
//                VALUES("isuse", "#{s.isuse}::bit");
//                VALUES("remark", "#{s.remark}");
//                VALUES("create_time", "current_timestamp");
//                VALUES("create_user_account", "#{s.createUserAccount}");
//                VALUES("is_common_template", "#{s.isCommonTemplate}::bit");
//                VALUES("is_have_sub", "#{s.isHaveSub}");
//            }}.toString();
//        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param orderBy
//         * @param pageSize
//         * @param begin
//         * @param workTemplateName
//         * @param type
//         * @return
//         */
//        public String query(@Param("schema_name") String schemaName, @Param("orderBy") String orderBy,
//                            @Param("pageSize") int pageSize, @Param("begin") int begin,
//                            @Param("workTemplateName") String workTemplateName, String type) {
//            return new SQL() {{
//                SELECT(" work_template_code AS workTemplateCode, isuse AS isuse, " +
//                        "work_template_name AS workTemplateName, relation_type AS relationType, " +
//                        "work_type_id AS workTypeId,template_type AS templateType, " +
//                        "create_time AS createTime, create_user_account AS createUserAccount, " +
//                        "parent_code AS parentCode, is_have_sub AS isHaveSub ");
//                FROM(schemaName + "._sc_work_template");
//                WHERE("is_common_template = '0'");
////                WHERE("is_common_template = '1'");
////                // 工单模板
////                if (Constants.METADATA_WORK.equals(type)) {
////                    WHERE("parent_code = '1'");
////                }
////                // 表单模板
////                if (Constants.METADATA_FORM.equals(type)) {
////                    WHERE("parent_code <> '1'");
////                }
//                // 模板名称
//                if (null != workTemplateName && !"".equals(workTemplateName)) {
//                    WHERE("work_template_name like CONCAT('%', #{workTemplateName}, '%')");
//                }
//                ORDER_BY(orderBy + " limit " + pageSize + " offset " + begin);
//            }}.toString();
//        }
//
//        /**
//         * 查询列表计数
//         *
//         * @param schemaName
//         * @param workTemplateName
//         * @param type
//         * @return
//         */
//        public String countByCondition(@Param("schema_name") String schemaName,
//                                       @Param("workTemplateName") String workTemplateName, String type) {
//            return new SQL() {{
//                SELECT("count(1) ");
//                FROM(schemaName + "._sc_work_template");
//                WHERE("is_common_template = '0'");
////                WHERE("is_common_template = '1'");
////                // 工单模板
////                if (Constants.METADATA_WORK.equals(type)) {
////                    WHERE("parent_code = '1'");
////                }
////                // 表单模板
////                if (Constants.METADATA_FORM.equals(type)) {
////                    WHERE("parent_code <> '1'");
////                }
//                // 模板名称
//                if (null != workTemplateName && !"".equals(workTemplateName)) {
//                    WHERE("work_template_name like CONCAT('%', #{workTemplateName}, '%')");
//                }
//            }}.toString();
//        }
//
//        /**
//         * 根据主键查询数据
//         *
//         * @param schemaName
//         * @param workTemplateCode
//         * @return
//         */
//        public String queryById(@Param("schema_name") String schemaName,
//                                @Param("workTemplateCode") String workTemplateCode) {
//            return new SQL() {{
//                SELECT(" work_template_code AS workTemplateCode," +
//                        "work_template_name AS workTemplateName," +
//                        "relation_type AS relationType,parent_code AS parentCode," +
//                        "work_type_id AS workTypeId,template_type AS templateType, " +
//                        "body_property AS bodyProperty,isuse AS isuse,remark AS remark," +
//                        "create_time AS createTime,create_user_account AS createUserAccount, " +
//                        "is_have_sub AS isHaveSub ");
//                FROM(schemaName + "._sc_work_template");
//                if (null != workTemplateCode && !"".equals(workTemplateCode)) {
//                    WHERE("work_template_code = #{workTemplateCode}");
//                }
//            }}.toString();
//        }
//
//        /**
//         * 查询列表
//         *
//         * @param schemaName
//         * @param condition
//         * @return
//         */
//        public String queryByCondition(@Param("schema_name") String schemaName, @Param("condition") String condition) {
//            return new SQL() {{
//                SELECT(" a.work_template_code AS workTemplateCode, a.isuse AS isuse, " +
//                        "a.work_template_name AS workTemplateName, a.relation_type AS relationType, " +
//                        "work_type_id AS workTypeId,template_type AS templateType, " +
//                        "a.create_time AS createTime, a.create_user_account AS createUserAccount, " +
//                        "a.parent_code AS parentCode, is_have_sub AS isHaveSub ");
//                FROM(schemaName + "._sc_work_template a ");
////                WHERE("a.is_common_template = '0'");
//                // 模板名称
//                if (null != condition && !"".equals(condition)) {
//                    WHERE("a.work_template_name like CONCAT('%', #{condition}, '%')");
//                }
//                ORDER_BY("a.create_time desc");
//            }}.toString();
//        }
//    }
}
