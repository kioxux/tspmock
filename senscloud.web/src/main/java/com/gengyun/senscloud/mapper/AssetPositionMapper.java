package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 设备位置
 */
@Mapper
public interface AssetPositionMapper {

    /**
     * 新增设备位置
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Insert(" INSERT INTO ${schema_name}._sc_asset_position (status,position_code,position_name,file_id,parent_code,position_type_id," +
            " location,remark,create_user_id,create_time,layer_path,inner_code) " +
            " VALUES(1,#{pm.position_code},#{pm.position_name},#{pm.file_id},#{pm.parent_id},#{pm.position_type_id}:: int," +
            " #{pm.location}::point,#{pm.remark},#{pm.create_user_id},current_timestamp,#{pm.layer_path},#{pm.inner_code}) ")
    void insertAssetPosition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 获取当前最大设备位置编码
     *
     * @param schema_name 入参
     * @return 入参
     */
    @Select("SELECT MAX(CAST (substring ( position_code, 2 ) AS INT ))  FROM  ${schema_name}._sc_asset_position   ")
    String findMaxPositionCode(@Param("schema_name") String schema_name);

    /**
     * 修改设备位置
     *
     * @param schema_name 入参
     * @param pm          入参
     */
    @Update(" <script>" +
            " UPDATE ${schema_name}._sc_asset_position" +
            " SET position_code=#{pm.position_code}" +
            " ,scada_config=#{pm.scada_config}" +
            "  <when test='pm.position_name!=null'>" +
            " ,position_name=#{pm.position_name}  " +
            " </when> " +
            "  <when test='pm.position_type_id!=null'>" +
            " ,position_type_id=#{pm.position_type_id}::int" +
            " </when> " +
            "  <when test='pm.parent_id!=null'>" +
            " ,parent_code=#{pm.parent_id}" +
            " </when> " +
            "  <when test='pm.location!=null'>" +
            " ,location=#{pm.location}::point" +
            " </when> " +
            "  <when test='pm.file_id!=null'>" +
            " ,file_id=#{pm.file_id}" +
            " </when> " +
            "  <when test='pm.inner_code!=null'>" +
            " ,file_id=#{pm.inner_code}" +
            " </when> " +
            "  <when test='pm.remark!=null'>" +
            " ,remark=#{pm.remark}" +
            " </when> " +
            "  <when test='pm.remark==null'>" +
            " ,remark=''" +
            " </when> " +
            "  <when test='pm.layer_path!=null'>" +
            " ,layer_path=#{pm.layer_path} " +
            " </when> " +
//            "  <when test='pm.scada_config!=null'>" +
//            " ,scada_config=#{pm.scada_config} " +
//            " </when> " +
            " WHERE position_code=#{pm.position_code}" +
            " </script>")
    void updateAssetPosition(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 删除设备位置
     *
     * @param schema_name   入参
     * @param position_code 入参
     */
    @Update(" UPDATE  ${schema_name}._sc_asset_position SET status = " + StatusConstant.STATUS_DELETEED +
            " WHERE position_code=#{position_code}")
    void deleteAssetPosition(@Param("schema_name") String schema_name, @Param("position_code") String position_code);

    /**
     * 查询设备位置详情
     */
    @Select(" SELECT t.inner_code,t.position_code AS id,t.position_name,t.file_id,t.parent_code AS parent_id,t.position_type_id,t.location,t.remark," +
            " t.create_user_id,t.create_time,t.layer_path,t.scada_config FROM ${schema_name}._sc_asset_position t" +
            " WHERE  t.position_code=#{position_code}")
    Map<String, Object> findAssetPositionInfo(@Param("schema_name") String schema_name, @Param("position_code") String position_code);

    /**
     * 查询设备位置列表
     *
     * @param schema_name 入参
     * @param pm          入参
     * @return 设备位置列表
     */
    @Select(" <script>" +
            " WITH RECURSIVE r AS (" +
            " SELECT inner_code,status,position_code,position_name,file_id,parent_code,position_type_id,remark,create_user_id,create_time,layer_path" +
            " FROM ${schema_name}._sc_asset_position" +
            "  WHERE status > " + StatusConstant.STATUS_DELETEED +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND ( position_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  OR position_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%')" +
            "  OR remark LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " <when test='pm.positionTypeIdSearch!=null'>" +
            "  AND position_type_id IN " +
            "  <foreach collection='pm.positionTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when> " +
            " UNION ALL" +
            " SELECT t.inner_code,T.status,T.position_code,T.position_name,T.file_id,T.parent_code,T.position_type_id,T.remark," +
            " T.create_user_id,T.create_time,T.layer_path " +
            " FROM ${schema_name}._sc_asset_position T,r " +
            " WHERE T.position_code = r.parent_code)" +
            " SELECT  DISTINCT inner_code,status,position_code,position_code AS ID,position_name,file_id,parent_code AS parent_id,position_type_id::varchar," +
            " remark,create_user_id,create_time,layer_path " +
            " FROM r " +
            " ORDER BY position_code " +
            " </script>")
    List<Map<String, Object>> findAssetPositionList(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);


    @Select("SELECT ap.position_code,ap.inner_code, ap.position_name, ap.remark ,ft.type_name,ap.position_type_id " +
            "FROM ${schema_name}._sc_asset_position ap " +
            "LEFT JOIN ${schema_name}._sc_facility_type ft ON ap.position_type_id = ft.id " +
            "WHERE ap.position_code = #{id}")
    Map<String, Object> findPositionInfoForAssetMap(@Param("schema_name") String schemaName, @Param("id") String id);

    @Select("SELECT ap.inner_code, ap.position_code,ap.position_name,ap.position_type_id,ap.location::varchar,ap.layer_path,ft.type_name " +
            "FROM ${schema_name}._sc_asset_position ap " +
            "LEFT JOIN ${schema_name}._sc_facility_type ft ON ap.position_type_id = ft.id " +
            "WHERE ap.parent_code = #{id} AND ap.status > " + StatusConstant.STATUS_DELETEED)
    List<Map<String, Object>> findChildAssetPositionByPositionCode(@Param("schema_name") String schemaName, @Param("id") String id);

    @Select(" <script> " +
            " SELECT ap.inner_code, ap.position_code,ap.position_name,ap.position_type_id,ap.location::varchar,ap.layer_path,ft.type_name " +
            " FROM ${schema_name}._sc_asset_position ap " +
            " LEFT JOIN ${schema_name}._sc_facility_type ft ON ap.position_type_id = ft.id " +
            " WHERE ap.parent_code = #{id} AND ap.status > " + StatusConstant.STATUS_DELETEED +
            " <if test=\"pagination != null and pagination != ''\"> " +
            " ${pagination} " +
            " </if> " +
            " </script> ")
    List<Map<String, Object>> findChildAssetPositionByPositionCodeLimit(@Param("schema_name") String schemaName, @Param("id") String id, @Param("pagination") String pagination);

    /**
     * 验证备件位置是否存在
     *
     * @param schemaName       入参
     * @param positionCodeList 入参
     */
    @Select("<script>select distinct position_code, position_name from ${schemaName}._sc_asset_position " +
            "where status = " + StatusConstant.STATUS_ENABLE + " and position_name in " +
            "<foreach collection='positionCodes' item='positionCode' open='(' close=')' separator=','> " +
            "#{positionCode} " +
            "</foreach></script>")
    @MapKey("position_name")
    Map<String, Map<String, Object>> queryPositionByPositionCodes(@Param("schemaName") String schemaName, @Param("positionCodes") List<String> positionCodeList);

    @Select("<script> SELECT distinct ap.inner_code,ap.position_code,ap.position_name,ap.parent_code,ap.position_type_id::varchar,ap.location::varchar,ap.layer_path," +
            " ap.remark,ap.file_id,ap.status,COALESCE(app.position_name,'') as parent_position_name,'position'as type, " +
            "<choose> " +
            "   <when test = \"condition != null and condition != ''\"> " +
            "       case when ap.position_name like CONCAT('%', #{condition}, '%') then 1 else 0 end as high_light," +
            "   </when>" +
            "   <otherwise>" +
            "       0 as high_light," +
            "   </otherwise> " +
            "</choose> " +
            "<choose> " +
            "   <when test = \" '0.1' == type \"> " +
            "    case when (SELECT COUNT(DISTINCT wd.work_code) FROM ${schema_name}._sc_works w " +
            "       INNER JOIN ${schema_name}._sc_works_detail wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
            "       WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status &lt; " + StatusConstant.COMPLETED +
            "       and w.position_code in (" +
            "   WITH RECURSIVE re (position_code, parent_code) AS ( " +
            " SELECT t.position_code, t.parent_code FROM ${schema_name}._sc_asset_position t where t.position_code = ap.position_code AND t.status  > " + StatusConstant.STATUS_DELETEED +
            " UNION ALL" +
            " SELECT t2.position_code, t2.parent_code FROM ${schema_name}._sc_asset_position t2, re e WHERE e.position_code = t2.parent_code AND t2.status > " + StatusConstant.STATUS_DELETEED +
            " ) select position_code from re " +
            "       ) AND wt.business_type_id = " + Constants.BUSINESS_TYPE_REPAIR_TYPE +
            "      ) > 0 then 1 else 0 end as is_fault, " +
            "   </when>" +
            "   <otherwise>" +
            "       0 as is_fault," +
            "   </otherwise> " +
            "</choose> " +
            "<choose> " +
            "   <when test = \"'1' == type\"> " +
            "       (SELECT COUNT(DISTINCT wd.work_code) FROM ${schema_name}._sc_works w " +
            "       INNER JOIN ${schema_name}._sc_works_detail wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_work_type wt ON wt.id = wd.work_type_id " +
            "       WHERE wd.status > " + StatusConstant.DRAFT + " AND wd.status &lt; " + StatusConstant.COMPLETED +
            "       and w.position_code = ap.position_code " +
            "       <choose> " +
            "           <when test = \"workType != null and workType == 1\"> " +
            "               AND wt.business_type_id = " + Constants.BUSINESS_TYPE_REPAIR_TYPE +
            "           </when>" +
            "           <when test = \"workType != null and workType == 2\"> " +
            "               AND wt.business_type_id = " + Constants.BUSINESS_TYPE_MAINTAIN_TYPE +
            "           </when>" +
            "           <otherwise>" +
            "               AND wt.business_type_id in (" + Constants.BUSINESS_TYPE_REPAIR_TYPE + "," + Constants.BUSINESS_TYPE_MAINTAIN_TYPE + ")" +
            "           </otherwise> " +
            "       </choose> " +
            "       ) as work_count " +
            "   </when>" +
            "   <otherwise>" +
            "       0 as work_count " +
            "   </otherwise> " +
            "</choose> " +
            " FROM  ${schema_name}._sc_asset_position AS ap " +
            " INNER JOIN ${schema_name}._sc_role_asset_position AS rap ON ap.position_code = rap.asset_position_code " +
            " INNER JOIN ${schema_name}._sc_position_role AS pr ON pr.role_id = rap.role_id " +
            " INNER JOIN ${schema_name}._sc_user_position AS up ON pr.position_id = up.position_id " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS app ON ap.parent_code = app.position_code " +
            " WHERE up.user_id = #{userId} AND ap.parent_code is null " +
            " and ap.status >" + StatusConstant.STATUS_DELETEED +
            "</script> ")
    List<Map<String, Object>> findTheBiggestAssetPosition(@Param("schema_name") String schemaName, @Param("userId") String userId, @Param("condition") String keywordSearch, @Param("type") String type);

    @Select(" <script>" +
            " SELECT * FROM (WITH RECURSIVE r AS (" +
            " SELECT inner_code,status,position_code,position_name,file_id,parent_code,position_type_id,remark,create_user_id,create_time,layer_path" +
            " FROM ${schema_name}._sc_asset_position" +
            "  WHERE status > " + StatusConstant.STATUS_DELETEED +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND ( position_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "  OR position_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%')" +
            "  OR remark LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " <when test='pm.positionTypeIdSearch!=null'>" +
            "  AND position_type_id IN " +
            "  <foreach collection='pm.positionTypeIdSearch' item='id' open='(' close=')' separator=','> #{id}::int </foreach>" +
            " </when> " +
            " UNION ALL" +
            " SELECT t.inner_code,T.status,T.position_code,T.position_name,T.file_id,T.parent_code,T.position_type_id,T.remark," +
            " T.create_user_id,T.create_time,T.layer_path " +
            " FROM ${schema_name}._sc_asset_position T,r " +
            " WHERE T.position_code = r.parent_code)" +
            " SELECT  DISTINCT inner_code,status,position_code,position_code AS ID,position_name,file_id,parent_code AS parent_id,position_type_id::varchar," +
            " remark,create_user_id,create_time,layer_path " +
            " FROM r  ) p " +
            " WHERE 1=1 " +
            " <when test='positions!=null'>" +
            "  AND p.position_code IN " +
            "  <foreach collection='positions' item='id' open='(' close=')' separator=','> #{id}::varchar </foreach>" +
            " </when> " +
            " ORDER BY p.position_code " +
            " </script>")
    List<Map<String, Object>> findAssetPositionListByPositions(@Param("schema_name") String schemaName,
                                                               @Param("positions") String[] positions, @Param("pm") Map<String, Object> pm);

    /**
     * 获取该设备位置可选择的任务模板列表
     *
     * @param schemaName 系统参数
     * @param pm         系统参数
     * @return 任务模板列表
     */
    @Select(" <script> " +
            " SELECT " +
            " tt.task_template_code, " +
            " tt.task_template_name, " +
            " tt.remark, " +
            " apt.position_code, " +
            " apt.task_template_code  " +
            "FROM " +
            " ${schema_name}._sc_task_template  AS tt " +
            " LEFT JOIN ${schema_name}._sc_asset_position_task  AS apt " +
            " ON tt.task_template_code = apt.task_template_code and position_code = #{pm.position_code} and apt.work_type_id = #{pm.work_type_id}::int " +
            "WHERE " +
            " tt.is_use = '1'   " +
            " AND apt.task_template_code IS NULL" +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND (tt.task_template_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "    OR   tt.task_template_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " </script> ")
    List<Map<String, Object>> findChooseTaskTemplateListByPositionCode(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 获取该设备位置已选择的任务模板列表
     *
     * @param schemaName 系统参数
     * @param pm         系统参数
     * @return 任务模板列表
     */
    @Select(" <script>" +
            " SELECT " +
            " tt.task_template_code, " +
            " tt.task_template_name, " +
            " tt.is_use, " +
            " tt.remark, " +
            " apt.position_code," +
            " apt.work_type_id, " +
            " apt.task_template_code, " +
            " ( SELECT COUNT ( 1 ) FROM ${schema_name}._sc_task_template_item WHERE is_use = '1' AND task_item_code = tt.task_template_code ) AS taskCount  " +
            "FROM " +
            " ${schema_name}._sc_asset_position_task AS apt " +
            " LEFT JOIN ${schema_name}._sc_task_template AS tt ON tt.task_template_code = apt.task_template_code  " +
            "WHERE " +
            "  tt.is_use = '1' and apt.position_code = #{pm.position_code}" +
            "  <when test='pm.keywordSearch!=null'>" +
            "  AND (tt.task_template_code LIKE  CONCAT('%', #{pm.keywordSearch}, '%') " +
            "    OR   tt.task_template_name LIKE  CONCAT('%', #{pm.keywordSearch}, '%') ) " +
            " </when> " +
            " </script> ")
    List<Map<String, Object>> findTaskTemplateListByPositionCode(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 新增设备位置任务模板关联
     */
    @Select(" <script>insert into ${schema_name}._sc_asset_position_task (work_type_id,position_code,task_template_code,create_user_id,create_time) " +
            " values " +
            "  <foreach collection='pm.template_codes' item='task_template_code' separator=','>" +
            " (#{pm.work_type_id}::int,#{pm.position_code},#{task_template_code},#{pm.create_user_id},now()) " +
            "  </foreach>" +
            "</script>")
    void insertPositionTemplate(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);


    /**
     * 设备位置任务模板关联重复验证
     */
    @Select(" <script>select string_agg(t1.task_template_name , ',') as name " +
            " from ${schema_name}._sc_asset_position_task t, ${schema_name}._sc_task_template t1 " +
            " where t.task_template_code = t1.task_template_code and position_code = #{pm.position_code} " +
            " and work_type_id = #{pm.work_type_id}::int and t.task_template_code in " +
            "  <foreach collection='pm.template_codes' item='task_template_code' open='(' close=')' separator=','>" +
            " #{task_template_code} " +
            "  </foreach>" +
            "</script>")
    String findTaskTemplateNamesForAddTask(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 新增设备位置任务模板关联
     */
    @Select(" <script> " +
            " delete from  ${schema_name}._sc_asset_position_task " +
            " where position_code = #{pm.position_code}" +
            " <when test='pm.work_type_id!=null'>" +
            "  AND work_type_id = #{pm.work_type_id}::int" +
            " </when> " +
            " <when test='pm.template_codes!=null'>" +
            "  AND task_template_code IN " +
            "  <foreach collection='pm.template_codes' item='id' open='(' close=')' separator=','> #{id}::varchar </foreach>" +
            " </when> " +
            " </script>")
    void deletePositionTemplate(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 根据位置编码和工单类型获取任务模板列表
     *
     * @param schemaName 系统参数
     * @param pm         系统参数
     * @return 任务模板列表
     */
    @Select(" <script>" +
            " SELECT " +
            " tt.task_template_code, " +
            " tt.task_template_name, " +
            " tt.is_use, " +
            " tt.remark, " +
            " apt.position_code," +
            " apt.work_type_id, " +
            " apt.task_template_code, " +
            " tt1.task_item_code," +
            " tt1.task_item_name," +
            " tt1.result_type," +
            " tt1.requirements," +
            " tt1.result_verification," +
            " tti.group_name ," +
            " tti.task_id ," +
            " ( SELECT COUNT ( 1 ) FROM ${schema_name}._sc_task_template_item WHERE is_use = '1' AND task_item_code = tt.task_template_code ) AS taskCount  " +
            "FROM " +
            " ${schema_name}._sc_asset_position_task AS apt " +
            " LEFT JOIN ${schema_name}._sc_task_template AS tt ON tt.task_template_code = apt.task_template_code  " +
            " LEFT JOIN ${schema_name}._sc_task_template_item AS tti ON tti.task_template_code = apt.task_template_code  " +
            " LEFT JOIN ${schema_name}._sc_task_item AS tt1 ON tt1.task_item_code = tti.task_item_code  " +
            "WHERE " +
            "  tt.is_use = '1' " +
            "  and apt.position_code = #{pm.position_code}" +
            "  and apt.work_type_id = #{pm.work_type_id}::int" +
            " </script> ")
    List<Map<String, Object>> findTaskTemplateListByPositionCodeAndWorkTypeId(@Param("schema_name") String schemaName, @Param("pm") Map<String, Object> pm);

    /**
     * 根据位置编码获取该位置内的设备的故障状态
     */
    @Select(" SELECT A.ID, A.asset_name,a.location::varchar, " +
            " CASE " +
            "  WHEN ( " +
            "  SELECT COUNT ( 1 )  " +
            "  FROM ${schema_name}._sc_works AS w " +
            "   INNER JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = w.work_type_id  " +
            "  WHERE " +
            "   w.relation_id = A.ID  " +
            "   AND w.relation_type = 2  " +
            "   AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1001 +
            "   AND w.status < " + StatusConstant.COMPLETED +
            "  ) > 0 " +
            "  THEN 1 ELSE - 1  " +
            "  END AS isRepair  " +
            " FROM ${schema_name}._sc_asset A  " +
            " WHERE A.status > " + StatusConstant.STATUS_DELETEED + " and a.location is not null " + " AND A.position_code = #{position_code} ")
    List<Map<String, Object>> findAssetRepairStatusByPositionCode(@Param("schema_name") String schemaName, @Param("position_code") String position_code);

    /**
     * 获取所有位置的设备故障数
     */
    @Select(" SELECT SUM " +
            " ( T.repairCount ) AS repairCount, " +
            " T.position_code, " +
            " T.position_name  " +
            " FROM " +
            " ( " +
            " SELECT A.asset_code, " +
            "  A.ID, " +
            "  A.position_code, " +
            "  ap.position_name, " +
            "  ( " +
            "  SELECT COUNT " +
            "   ( 1 )  " +
            "  FROM " +
            "   ${schema_name}._sc_works AS w " +
            "   INNER JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.ID  " +
            "  WHERE " +
            "   AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1001 +
            "   AND w.status < " + StatusConstant.COMPLETED +
            "   AND w.relation_id = A.ID  " +
            "  ) AS repairCount  " +
            " FROM " +
            "  ${schema_name}._sc_asset AS A " +
            "  INNER JOIN ${schema_name}._sc_asset_position AS ap ON A.position_code = ap.position_code  " +
            " WHERE " +
            "  A.status > " + StatusConstant.STATUS_DELETEED +
            "  AND ap.status > " + StatusConstant.STATUS_DELETEED +
            " ) T  " +
            " GROUP BY " +
            " T.position_code, " +
            " T.position_name ")
    List<Map<String, Object>> findAllPositionCodeAssetRepairStatusBy(@Param("schema_name") String schemaName);

}
