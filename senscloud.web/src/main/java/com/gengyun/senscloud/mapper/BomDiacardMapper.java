package com.gengyun.senscloud.mapper;

/**
 * 备件报废
 */
public interface BomDiacardMapper {
//
//    /**
//     * 查找备件报废列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT DISTINCT br.discard_code,s.stock_name, " +
//            "br.create_time,br.create_user_account,us.username as create_user_name, st.status as status_name,br.status, " +
//            "brt.quantity_total " +
//            "FROM ${schema_name}._sc_bom_discard br " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON br.stock_code = bs.stock_code  " +
//            "left JOIN (" +
//            "select distinct st.stock_code,st.stock_name from ${schema_name}._sc_stock st " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org on org.stock_code=st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg on sg.stock_code=st.stock_code " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            ") s on bs.stock_code = s.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_status st ON st.id = br.status " +
//            "LEFT JOIN ( SELECT discard_code,sum(quantity) as quantity_total " +
//            "FROM ${schema_name}._sc_bom_discard_detail group by discard_code" +
//            ") brt ON brt.discard_code = br.discard_code " +
//            "LEFT join ${schema_name}._sc_user us on br.create_user_account=us.account " +
//            "where 1=1 ${condition} and s.stock_code is not null " +
//            "order by br.create_time desc,br.discard_code desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getBomDiacardList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 备件报废列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) FROM( " +
//            "SELECT DISTINCT br.discard_code,s.stock_name, " +
//            "br.create_time,br.create_user_account,us.username as create_user_name, st.status as status_name,br.status " +
//            "FROM ${schema_name}._sc_bom_discard br " +
//            "LEFT JOIN ${schema_name}._sc_bom_stock bs ON br.stock_code = bs.stock_code  " +
//            "left JOIN (" +
//            "select distinct st.stock_code,st.stock_name from ${schema_name}._sc_stock st " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org on org.stock_code=st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg on sg.stock_code=st.stock_code " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            ") s on bs.stock_code = s.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_status st ON st.id = br.status " +
//            "LEFT join ${schema_name}._sc_user us on br.create_user_account=us.account " +
//            "where 1=1 ${condition} and s.stock_code is not null) s" )
//    int countBomDiacardList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition);
//
//    /**
//     * 根据主键查询备件报废数据
//     *
//     * @param schemaName
//     * @param discardCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_discard w " +
//            "where w.discard_code = #{discardCode} ")
//    Map<String, Object> queryBomDiacardById(@Param("schema_name") String schemaName, @Param("discardCode") String discardCode);
//
//    @Delete("delete from ${schema_name}.${tableName} where discard_code = #{subWorkCode} ")
//    void deleteBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode);
//
//    @Select("select * from ${schema_name}._sc_bom_discard w where w.discard_code = #{discard_code} ")
//    Map<String, Object> queryBomDiscardById(@Param("schema_name") String schemaName, @Param("discard_code") String discard_code);
//
//    @Update("update ${schema_name}._sc_bom_discard set status = #{status} where  discard_code= #{discard_code}")
//    int updateBomDiscardStatus(@Param("schema_name") String schema_name ,@Param("status") int status, @Param("discard_code") String discard_code);
//
//    @Update("update ${schema_name}._sc_bom_discard set body_property=#{body_property}::jsonb where discard_code=#{discard_code}")
//    int updateBomDiscardBodyProperty(@Param("schema_name") String schema_name ,@Param("discard_code") String discard_code,@Param("body_property") String body_property);
//
//    /**
//     * 根据主键查询备件报废详情数据
//     *
//     * @param schemaName
//     * @param discard_code
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_discard_detail w where w.discard_code = #{discard_code} ")
//    List<Map<String, Object>> queryBomDiscardkDetailById(@Param("schema_name") String schemaName, @Param("discard_code") String discard_code);

}
