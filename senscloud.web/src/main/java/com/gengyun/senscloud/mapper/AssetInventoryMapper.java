package com.gengyun.senscloud.mapper;


import org.apache.ibatis.annotations.Mapper;

/**
 * 设备盘点
 */
@Mapper
public interface AssetInventoryMapper {
//    /**
//     * 根据code查询设备盘点数据
//     *
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_asset_inventory where inventory_code = #{inventoryCode} ")
//    Map<String, Object> queryAssetInventoryByCode(@Param("schema_name") String schemaName, @Param("inventoryCode") String inventoryCode);
//
//    /**
//     *  根据主键code，查询盘点位置信息
//     * @param schemaName
//     * @param subInventoryCode
//     * @return
//     */
//    @Select("select aio.*,f.short_title as orgName from ${schema_name}._sc_asset_inventory_org aio " +
//            "left join ${schema_name}._sc_facilities f ON f.ID = aio.facility_id " +
//            "where aio.sub_inventory_code = #{subInventoryCode} ")
//    Map<String, Object> queryAssetInventoryOrgByCode(@Param("schema_name") String schemaName, @Param("subInventoryCode") String subInventoryCode);
//
//    /**
//     *  根据主键code，查询盘点位置信息列表
//     * @param schemaName
//     * @param inventoryCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_asset_inventory_org where inventory_code = #{inventoryCode} ")
//    List<Map<String, Object>> queryAssetInventoryOrgListByCode(@Param("schema_name") String schemaName, @Param("inventoryCode") String inventoryCode);
//
//    /**
//     * 设备盘点列表查询
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT bi.* FROM ${schema_name}._sc_asset_inventory bi where 1=1 ${condition} order by bi.create_time desc limit ${page} offset ${begin}")
//    List<Map<String, Object>> getAssetInventoryList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 设备盘点列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) FROM ${schema_name}._sc_asset_inventory bi where 1=1 ${condition} ")
//    int countAssetInventoryList(@Param("schema_name") String schema_name, @Param("condition") String condition);
//
//    /**
//     *修改设备盘点状态、完成时间
//     * @param schema_name
//     * @param inventoryCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_inventory set status = #{status},finished_time = #{finishedTime} where inventory_code = #{inventoryCode}")
//    int updateAssetInventoryStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("inventoryCode") String inventoryCode, @Param("finishedTime") Timestamp finishedTime);
//
//    /**
//     * 修改盘点位置信息状态、完成时间
//     * @param schema_name
//     * @param status
//     * @param condition
//     * @param finishedTime
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_inventory_org set status = #{status},finished_time = #{finishedTime} where 1=1 ${condition}")
//    int updateAssetInventoryOrgStatus(@Param("schema_name") String schema_name, @Param("status") int status, @Param("finishedTime")Timestamp finishedTime, @Param("condition")String condition);
//
//    /**
//     * 设备盘点位置列表查询
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT bs.inventory_code,bs.sub_inventory_code,bs.finished_time,bs.record_count,bs.inventory_count, " +
//            " bs.balance_count,bs.status,bi.inventory_name,bi.begin_time,bs.duty_man,bs.facility_id,f.short_title AS facility_name " +
//            " FROM ${schema_name}._sc_asset_inventory_org bs " +
//            " LEFT JOIN ${schema_name}._sc_user u ON bs.duty_man = u.account " +
//            " LEFT JOIN ${schema_name}._sc_asset_inventory bi ON bi.inventory_code = bs.inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_facilities f ON f.ID = bs.facility_id " +
//            " WHERE bs.inventory_code = '${inventoryCode}' ${condition} " +
//            " ORDER BY bs.create_time DESC limit ${page} offset ${begin}")
//    List<Map<String, Object>> getAssetInventoryOrgList(@Param("schema_name") String schema_name, @Param("inventoryCode") String inventoryCode, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 设备盘点位置列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("select count(1) FROM ${schema_name}._sc_asset_inventory_org bs " +
//            " LEFT JOIN ${schema_name}._sc_user u ON bs.duty_man = u.account " +
//            " LEFT JOIN ${schema_name}._sc_asset_inventory bi ON bi.inventory_code = bs.inventory_code " +
//            " LEFT JOIN ${schema_name}._sc_facilities f ON f.ID = bs.facility_id " +
//            " WHERE bs.inventory_code = '${inventoryCode}' ${condition} ")
//    int countAssetInventoryOrgList(@Param("schema_name") String schema_name, @Param("inventoryCode") String inventoryCode, @Param("condition") String condition);
//
//    /**
//     * 设备盘点位置列表查询
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT iod.sub_inventory_code,i.inventory_name,iod.finished_time,u.username,iod.asset_name, " +
//                    "iod.asset_code,iod.asset_id,iod.in_status,iod.deal_result,t.currency,cur.currency_sign as asset_currency " +
//            "FROM ${schema_name}._sc_asset_inventory_org_detail iod " +
//            "LEFT JOIN ${schema_name}._sc_asset_inventory i ON i.inventory_code = iod.inventory_code " +
//            "LEFT JOIN ${schema_name}._sc_asset_inventory_org io ON io.sub_inventory_code = iod.sub_inventory_code " +
//            "LEFT JOIN ${schema_name}._sc_user u ON u.account = io.duty_man " +
//            " LEFT JOIN ${schema_name}._sc_asset t ON t._id = iod.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_currency cur ON t.currency = cur.id " +
//            "WHERE iod.sub_inventory_code = '${subInventoryCode}' ${condition} " +
//            "ORDER BY iod.create_time DESC limit ${page} offset ${begin}")
//    List<Map<String, Object>> getAssetInventoryOrgDetailList(@Param("schema_name") String schema_name, @Param("subInventoryCode") String subInventoryCode, @Param("condition") String condition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 设备盘点位置列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT COUNT(1) FROM ${schema_name}._sc_asset_inventory_org_detail iod " +
//            "LEFT JOIN ${schema_name}._sc_asset_inventory i ON i.inventory_code = iod.inventory_code " +
//            "LEFT JOIN ${schema_name}._sc_asset_inventory_org io ON io.sub_inventory_code = iod.sub_inventory_code " +
//            "LEFT JOIN ${schema_name}._sc_user u ON u.account = io.duty_man " +
//            " LEFT JOIN ${schema_name}._sc_asset t ON t._id = iod.asset_id " +
//            " LEFT JOIN ${schema_name}._sc_currency cur ON t.currency = cur.id " +
//            " WHERE iod.sub_inventory_code = '${subInventoryCode}' ${condition} ")
//    int countAssetInventoryOrgDetailList(@Param("schema_name") String schema_name, @Param("subInventoryCode") String subInventoryCode, @Param("condition") String condition);
//
//    /**
//     * 更新设备盘点位置负责人
//     * @param schemaName
//     * @param dutyMan
//     * @param status
//     * @param subInventoryCode
//     * @return
//     */
//    @Update("update ${schema_name}._sc_asset_inventory_org set duty_man = #{dutyMan},status=#{status} where sub_inventory_code = #{subInventoryCode} ")
//    int updateAssetOrgDutyMan(@Param("schema_name") String schemaName, @Param("dutyMan") String dutyMan, @Param("status") int status, @Param("subInventoryCode") String subInventoryCode);
//
//    /**
//     * 查询设备详情信息
//     * @param schemaName
//     * @param subInventoryCode
//     * @param facilityId
//     * @param assetId
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_asset_inventory_org_detail where sub_inventory_code = #{subInventoryCode} " +
//            "and facility_id = #{facilityId} and asset_id = #{assetId} ")
//    Map<String, Object> queryAssetInventoryOrgDetailByCode(@Param("schema_name") String schemaName, @Param("subInventoryCode") String subInventoryCode,
//                                                           @Param("facilityId") int facilityId, @Param("assetId") String assetId);
//
//    /**
//     * 更新盘点设备信息
//     * @param schemaName
//     * @param map
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_asset_inventory_org_detail SET in_status = ${m.in_status} ,finished_time=#{m.finished_time} " +
//            "WHERE inventory_code = '${m.inventory_code}' AND sub_inventory_code='${m.sub_inventory_code}' AND asset_id='${m.asset_id}' ")
//    int updateAssetInventoryOrgDetail(@Param("schema_name") String schemaName, @Param("m") Map<String, Object> map);
//
//    /**
//     * 更新设备盘点表累计盘点数量
//     * @param schemaName
//     * @param inventoryCode
//     * @param inventoryCount
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_asset_inventory SET inventory_count = inventory_count + ${inventoryCount} ,balance_count=balance_count - ${inventoryCount} WHERE inventory_code = '${inventoryCode}' ")
//    int updateAssetInventoryCount(@Param("schema_name") String schemaName, @Param("inventoryCode") String inventoryCode, @Param("inventoryCount") BigDecimal inventoryCount);
//
//    /**
//     * 更新盘点位置表累计盘点数量
//     * @param schemaName
//     * @param subInventoryCode
//     * @param inventoryCount
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_asset_inventory_org SET inventory_count = inventory_count + ${inventoryCount} ,balance_count=balance_count - ${inventoryCount} WHERE sub_inventory_code = '${subInventoryCode}' ")
//    int updateAssetInventoryOrgCount(@Param("schema_name") String schemaName, @Param("subInventoryCode") String subInventoryCode, @Param("inventoryCount") BigDecimal inventoryCount);
//
//    /**
//     * 更新盘点设备调拨处理结果
//     * @param schemaName
//     * @param map
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_asset_inventory_org_detail SET deal_result = ${m.deal_result}, finished_time= NOW() " +
//            "WHERE sub_inventory_code='${m.sub_inventory_code}' AND asset_id='${m.asset_id}' ")
//    int updateAssetInventoryOrgDetailDealResult(@Param("schema_name") String schemaName, @Param("m") Map<String, Object> map);
//
//    /**
//     * 根据subInventoryCode, 查询所有盘点设备信息
//     * @param schema_name
//     * @param subInventoryCode
//     * @return
//     */
//    @Select("SELECT sd.* FROM ${schema_name}._sc_asset_inventory_org_detail sd WHERE sd.sub_inventory_code = '${subInventoryCode}' ")
//    List<Map<String, Object>> queryAssetInventoryOrgDetailBySubInventoryCode(@Param("schema_name") String schema_name, @Param("subInventoryCode") String subInventoryCode);
//
//    /**
//     * 查询未完成的盘点位置信息数量
//     * @param schema_name
//     * @param inventoryCode
//     * @return
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_asset_inventory_org WHERE inventory_code = #{inventoryCode} AND status <> " + StatusConstant.COMPLETED)
//    int countUnfinishedAssetInventoryOrg(@Param("schema_name") String schema_name, @Param("inventoryCode") String inventoryCode);
//
//    /**
//     * 查询指定的组织是否有盘点中的数据
//     * @param schema_name
//     * @param facilityIds
//     * @return
//     */
//    @Select("SELECT count(1) FROM ${schema_name}._sc_asset_inventory_org WHERE facility_id in (${facilityIds}) AND (status = " + StatusConstant.PENDING +" OR status = "+ StatusConstant.PENDING_ORDER+ ") ")
//    int countDoingAssetInventoryOrg(@Param("schema_name") String schema_name, @Param("facilityIds") String facilityIds);
//
//    /**
//     *  根据分拨中心ID，查询库房备件列表
//     * @param schemaName
//     * @param facilityIds
//     * @return
//     */
//    @Select("SELECT a._id as asset_id,a.strcode as asset_code,a.strname as asset_name,f.short_title,f.id as facility_id " +
//            "FROM ${schema_name}._sc_asset a " +
//            "LEFT JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = a._id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = ao.org_id " +
//            "WHERE a.intstatus>0 AND a.intstatus <> "+StatusConstant.ASSET_INTSTATUS_DISCARD+" AND f.id IN (${facilityIds}) ${condition} ")
//    List<Map<String, Object>> queryAssetByFacilityIds(@Param("schema_name") String schemaName, @Param("facilityIds") String facilityIds, @Param("condition") String condition);
//
//    /**
//     *查询分拨中心下，需要盘点的设备总数
//     * @param schemaName
//     * @param facilityIds
//     * @param condition
//     * @return
//     */
//    @Select("SELECT COUNT(DISTINCT a._id) FROM ${schema_name}._sc_asset a " +
//        "LEFT JOIN ${schema_name}._sc_asset_organization ao ON ao.asset_id = a._id " +
//        "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = ao.org_id " +
//        "WHERE a.intstatus>0 AND a.intstatus <> "+StatusConstant.ASSET_INTSTATUS_DISCARD+" AND f.id IN (${facilityIds}) ${condition} ")
//    int getAssetInventoryQuantity(@Param("schema_name") String schemaName, @Param("facilityIds") String facilityIds, @Param("condition") String condition);

}

