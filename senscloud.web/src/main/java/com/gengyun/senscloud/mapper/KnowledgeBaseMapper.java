package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface KnowledgeBaseMapper {
    @Select("<script>" +
            " <choose> " +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_2000 + "\"> " +
            "   select count(1) from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset t1 on t1.id::varchar=f.business_no_id " + SqlConstant.ASSET_DATA_PRM_SQL + " ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_3000 + "\"> " +
            "   select count(1) from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_bom bm on bm.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_6001 + "\"> " +
            "   select count(1) from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_facilities fc on fc.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_7009 + "\"> " +
            "   select count(1) from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset_model amd on amd.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_7004 + "\"> " +
            "   select count(1) from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_task_item tsk on tsk.task_item_code::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_10001 + "\"> " +
            "   select count(1) from (select f.*,wk.work_code,wk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_works wk on wk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_works_detail wd on wk.work_code = wd.work_code and wd.is_main = 1" + SqlConstant.WORK_AUTH_SQL + "" +
            "   union " +
            "   select f.*,awk.work_code,awk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset_works awk on awk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_asset_works_detail awd on awk.work_code = awd.work_code and awd.is_main = 1" + SqlConstant.ASSET_WORK_AUTH_SQL + "" +
            "   union " +
            "   select f.*,bwk.work_code,bwk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_bom_works bwk on bwk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_bom_works_detail bwd on bwk.work_code = bwd.work_code and bwd.is_main = 1" + SqlConstant.BOM_WORK_AUTH_SQL + "" +
            "   ) f ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_9000 + "\"> " +
            "   select count(1) from ${schema_name}._sc_files f ${whereString} " +
            "  </when>" +
            "  <otherwise>" +
            "   select 0 " +
            "  </otherwise> " +
            " </choose> " +
            "</script>")
    int countKnowledgeBaseList(@Param("schema_name") String schema_name, @Param("whereString") String whereString, @Param("userId") String userId, @Param("businessNo") String business_no, @Param("user_id") String user_id);

    @Select("<script>" +
            " <choose> " +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_2000 + "\"> " +
            "   select f.*,t1.asset_name,t1.asset_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset t1 on t1.id::varchar=f.business_no_id" + SqlConstant.ASSET_DATA_PRM_SQL + "${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_3000 + "\"> " +
            "   select f.*,bm.bom_name,bm.material_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_bom bm on bm.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_6001 + "\"> " +
            "   select f.*,fc.short_title,fc.inner_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_facilities fc on fc.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_7009 + "\"> " +
            "   select f.*,amd.model_name,amd.category_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset_model amd on amd.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_7004 + "\"> " +
            "   select f.*,tsk.task_item_name,tsk.task_item_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_task_item tsk on tsk.task_item_code::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_10001 + "\"> " +
            "   select f.* from (select f.*,wk.work_code,wk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_works wk on wk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_works_detail wd on wk.work_code = wd.work_code and wd.is_main = 1" + SqlConstant.WORK_AUTH_SQL + "" +
            "   union " +
            "   select f.*,awk.work_code,awk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset_works awk on awk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_asset_works_detail awd on awk.work_code = awd.work_code and awd.is_main = 1" + SqlConstant.ASSET_WORK_AUTH_SQL + "" +
            "   union " +
            "   select f.*,bwk.work_code,bwk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_bom_works bwk on bwk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_bom_works_detail bwd on bwk.work_code = bwd.work_code and bwd.is_main = 1" + SqlConstant.BOM_WORK_AUTH_SQL + "" +
            "   ) f ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_9000 + "\"> " +
            "   select f.* from ${schema_name}._sc_files f ${whereString} " +
            "  </when>" +
            "  <otherwise>" +
            "  </otherwise> " +
            " </choose> " +
            "</script>")
    List<Map<String, Object>> findKnowledgeBaseList(@Param("schema_name") String schema_name, @Param("whereString") String whereString, @Param("userId") String userId, @Param("businessNo") String business_no, @Param("user_id") String user_id);

    @Select(" select f.*,at.asset_name,at.asset_code from ${schema_name}._sc_files f " +
            " left join ${schema_name}._sc_asset at on at.id::varchar=f.business_no_id where f.id=#{id}::int")
    Map<String, Object> findknowledgeBaseDetailById(@Param("schema_name") String schema_name, @Param("id") String id);

    @Select("<script>" +
            " <choose> " +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_2000 + "\"> " +
            "   select f.*,t1.asset_name,t1.asset_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset t1 on t1.id::varchar=f.business_no_id" + SqlConstant.ASSET_DATA_PRM_SQL + "${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_3000 + "\"> " +
            "   select f.*,bm.bom_name,bm.material_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_bom bm on bm.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_6001 + "\"> " +
            "   select f.*,fc.short_title,fc.inner_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_facilities fc on fc.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_7009 + "\"> " +
            "   select f.*,amd.model_name,amd.category_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset_model amd on amd.id::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_7004 + "\"> " +
            "   select f.*,tsk.task_item_name,tsk.task_item_code from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_task_item tsk on tsk.task_item_code::varchar=f.business_no_id ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_10001 + "\"> " +
            "   select f.* from (select f.*,wk.work_code,wk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_works wk on wk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_works_detail wd on wk.work_code = wd.work_code and wd.is_main = 1" + SqlConstant.WORK_AUTH_SQL + "" +
            "   union " +
            "   select f.*,awk.work_code,awk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_asset_works awk on awk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_asset_works_detail awd on awk.work_code = awd.work_code and awd.is_main = 1" + SqlConstant.ASSET_WORK_AUTH_SQL + "" +
            "   union " +
            "   select f.*,bwk.work_code,bwk.work_type_id from ${schema_name}._sc_files f join " +
            "   ${schema_name}._sc_bom_works bwk on bwk.work_code::varchar=f.business_no_id " +
            "   left join ${schema_name}._sc_bom_works_detail bwd on bwk.work_code = bwd.work_code and bwd.is_main = 1" + SqlConstant.BOM_WORK_AUTH_SQL + "" +
            "   ) f ${whereString} " +
            "  </when>" +
            "  <when test = \"businessNo == " + SensConstant.BUSINESS_NO_9000 + "\"> " +
            "   select f.* from ${schema_name}._sc_files f ${whereString} " +
            "  </when>" +
            "  <otherwise>" +
            "  </otherwise> " +
            " </choose> " +
            "</script>")
    List<Map<String, Object>> findExportKnowledgeBaseList(@Param("schema_name") String schema_name, @Param("whereString") String whereString, @Param("userId") String userId, @Param("businessNo") String business_no, @Param("user_id") String user_id);
}