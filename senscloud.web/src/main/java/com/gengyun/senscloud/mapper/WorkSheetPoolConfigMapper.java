package com.gengyun.senscloud.mapper;

/**
 * 工单派工配置
 * Created by Dong wudang on 2018/11/20.
 */
public interface WorkSheetPoolConfigMapper {
//    //新增工单派工配置
//    @Insert("insert into ${schema_name}._sc_work_pool " +
//            "(id,pool_name,role_id,distribute_type,distribute_sms_minutes,receive_sms_minute,create_time,create_user_account,isuse) values " +
//            "(#{t.id},#{t.pool_name},#{t.role_id},#{t.distribute_type},#{t.distribute_sms_minutes},#{t.receive_sms_minute},#{t.create_time},#{t.create_user_account},#{t.isuse})")
//    public int addWorkSheetPoolConfig(@Param("schema_name")String schema_name, @Param("t")WorkSheetPoolConfigModel workSheetPoolConfigModel);
//
//    //更新工单派工配置
//    @Update("update ${schema_name}._sc_work_pool set " +
//            "pool_name=#{w.pool_name}, " +
//            "role_id=#{w.role_id}, " +
//            "distribute_type=#{w.distribute_type}," +
//            "distribute_sms_minutes=#{w.distribute_sms_minutes}," +
//            "receive_sms_minute=#{w.receive_sms_minute}," +
//            "create_time=#{w.create_time}," +
//            "create_user_account=#{w.create_user_account}," +
//            "isuse=#{w.isuse} " +
//            " where id=#{w.id} ")
//    public int updateWorkSheetPoolConfig(@Param("schema_name")String schema_name, @Param("w")WorkSheetPoolConfigModel workSheetPoolConfigModel);
//
//    //删除工单派工配置列表数据
//    @Update("Update ${schema_name}._sc_work_pool set isuse = #{isuse}  where id = #{id}")
//    public int delWorkSheetPoolConfig(@Param("schema_name") String schema_name, @Param("id")String id , @Param("isuse")Boolean isuse);
//
//    //查询工单派工配置列表
//    @Select("select r.name as role_name,w.*  from ${schema_name}._sc_work_pool w " +
//            " left join ${schema_name}._sc_user u on u.account = w.create_user_account " +
//            " left join ${schema_name}._sc_role r on w.role_id=r.id " +
//
//            " where w.isuse = true ${condition} limit ${page} offset ${begin}")
//    public List<WorkSheetPoolConfigModel> selectWorkSheetPoolConfig(@Param("schema_name")String schema_name,@Param("condition")String condition,@Param("page")int pageSize,@Param("begin")int begin);
//
//    //查询工单派工配置列表
//    @Select("select DISTINCT(id),w.*  from ${schema_name}._sc_work_pool as w " +
//            " left join  ${schema_name}._sc_work_pool_facility as f on f.pool_id = w.id" +
//            " where w.isuse = true  ${condition}")
//    public List<WorkSheetPoolConfigModel> selectWorkSheetPoolConfigValid(@Param("schema_name")String schema_name , @Param("condition")String condition);
//
//    //查询工单派工配置列表总条数
//    @Select("select count(w.id) from ${schema_name}._sc_work_pool w " +
//            " left join ${schema_name}._sc_user u on u.account = w.create_user_account " +
//            " left join ${schema_name}._sc_role r on w.role_id=r.id " +
//            " where w.isuse = true ${condition}")
//    public int selectWorkSheetPoolConfigCount(@Param("schema_name")String schema_name,@Param("condition")String condition);
//
// //查询工单派工配置列表
//    @Select("select w.* from ${schema_name}._sc_work_pool w " +
//            " where w.id = #{id}")
//    public WorkSheetPoolConfigModel selectWorkSheetPoolConfigById(@Param("schema_name")String schema_name,@Param("id")String id);
//
//    /*查询工单池关联位置*/
//    @Select("select f.* ,fa.title as facility_name from ${schema_name}._sc_work_pool_facility f " +
//            " left join ${schema_name}._sc_facilities fa on fa.id = f.facility_id " +
//            " where f.pool_id = #{pool_id}")
//    public List<WorkSheetPoolConfigFacilityModel> selectWorkSheetPoolConfigFacilityById(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id);
//
//    /*查询工单池关联人员*/
//    @Select("select m.*, r.name as role_name from ${schema_name}._sc_work_pool_role m " +
//            " left join ${schema_name}._sc_role r on m.role_id = r.id " +
//            " where m.pool_id = #{pool_id}")
//    public List<WorkSheetPoolConfigRoleModel> selectWorkSheetPoolConfigRoleById(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id);
//
//    //查询所有工单池
//    @Select("select w.*,u.username as create_user_name,fa.title as facility_name from ${schema_name}._sc_work_pool w " +
//            " left join ${schema_name}._sc_user u on u.account = w.create_user_account " +
//            " left join ${schema_name}._sc_work_pool_facility  f on f.pool_id = w.id " +
//            " left join ${schema_name}._sc_facilities fa on fa.id = f.facility_id " +
//            " where w.isuse = true ")
//    public List<WorkSheetPoolConfigModel> selectWorkSheetPool(@Param("schema_name")String schema_name);
//
//
//    @Insert("insert into ${schema_name}._sc_work_pool_role " +
//            "(pool_id,role_id) values " +
//            "(#{pool_id},#{role_id})")
//    public int addWorkSheetPoolRole(@Param("schema_name")String schema_name,@Param("pool_id")String pool_id,@Param("role_id")String role_id);
//
//    @Insert("insert into ${schema_name}._sc_work_pool_facility " +
//            "(pool_id,facility_id) values " +
//            "(#{pool_id},#{facility_id})")
//    public int addWorkSheetPoolFacility(@Param("schema_name")String schema_name,@Param("pool_id")String pool_id,@Param("facility_id")Integer member);
//
//
//    /*查询所有角色信息*/
//    @Select("select w.* from ${schema_name}._sc_work_pool_role w " +
//            " where w.pool_id = #{pool_id}")
//     List<WorkSheetPoolConfigRoleModel> selectPoolmember(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id);
//
//    /*判断是否存在记录 角色*/
//    @Select("select count(w.pool_id) from ${schema_name}._sc_work_pool_role w " +
//            " where w.pool_id = #{pool_id} and w.role_id =#{role_id}")
//     int selectPoolmemberByRoleId(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id,@Param("role_id")String role_id);
//
//    /*判断是否存在记录 位置*/
//    @Select("select count(f.pool_id) from ${schema_name}._sc_work_pool_facility f " +
//            " where f.pool_id = #{pool_id} and f.facility_id =#{facility_id}")
//    int selectPoolmemberByFacilityId(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id,@Param("facility_id")Integer facility_id);
//
//    /*更新计划工单的负责角色*/
//    @Update("update ${schema_name}._sc_work_pool_role r set r.role_id=#{role_id} where r.pool_id=#{pool_id} ")
//    int  updateWorkSheetPoolRole(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id,@Param("role_id")String role_id);
//
//    /*更新计划工单的负责位置*/
//    @Update("update ${schema_name}._sc_work_pool_facility f set f.facility_id=#{facility_id} where f.pool_id=#{pool_id} ")
//    int updateWorkSheetPoolFacility(@Param("schema_name")String schema_name, @Param("pool_id")String pool_id,@Param("facility_id")Integer facility_id);
//
//    @Delete("delete from ${schema_name}._sc_work_pool_role where pool_id =#{pool_id} ")
//    public int delWorkSheetPoolRoleByPoolId(@Param("schema_name")String schema_name,@Param("pool_id")String pool_id);
//
//    @Delete("delete from ${schema_name}._sc_work_pool_facility where pool_id =#{pool_id} ")
//    public int delWorkSheetPoolFacilityByPoolId(@Param("schema_name")String schema_name,@Param("pool_id")String pool_id);
//
//    //查询工单池人员
////    @Select("select  distinct u.* from ${schema_name}._sc_work_pool p " +
////            "LEFT JOIN ${schema_name}._sc_work_pool_facility e on e.pool_id=p.id " +
////            "LEFT JOIN ${schema_name}._sc_user_facility f on e.facility_id=f.facility_id " +
////            "LEFT JOIN ${schema_name}._sc_user u on u.id=f.user_id " +
////            "LEFT JOIN(select u.* from ${schema_name}._sc_work_pool p " +
////            "LEFT JOIN ${schema_name}._sc_work_pool_role r on p.id=r.pool_id " +
////            "LEFT JOIN ${schema_name}._sc_user_role d on d.role_id=r.role_id " +
////            "LEFT JOIN ${schema_name}._sc_user u on u.id=d.user_id " +
////            "where 1=1 ${condition})z on z.id=u.id " +
////            "where 1=1 ${condition} ")
//    @Select("SELECT DISTINCT u.* FROM ${schema_name}._sc_user u " +
//            "LEFT JOIN ${schema_name}._sc_user_group ug ON ug.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_group_org gf on gf.group_id = ug.group_id " +
//            "LEFT JOIN ${schema_name}._sc_work_pool_facility e ON e.facility_id = gf.org_id " +
//            "LEFT JOIN ${schema_name}._sc_work_pool p ON p.id = e.pool_id " +
//            "LEFT JOIN ${schema_name}._sc_user_role r ON r.user_id = u.id " +
//            "LEFT JOIN ${schema_name}._sc_work_pool_role wr ON wr.role_id = r.role_id " +
//            "LEFT JOIN ${schema_name}._sc_work_pool P2 ON p2.id = wr.pool_id " +
//            "LEFT JOIN ${schema_name}._sc_role_permission rp ON rp.role_id = r.role_id " +
//            "LEFT JOIN public.system_permission sp ON sp.id = rp.permission_id  " +
//            "WHERE u.status=1 ${condition} order by u.id DESC ")
//     List<User> getPoolUser(@Param("schema_name")String schema_name,@Param("condition")String  condition);
}
