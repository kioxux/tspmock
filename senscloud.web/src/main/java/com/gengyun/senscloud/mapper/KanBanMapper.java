package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 看板
 */
@Mapper
public interface KanBanMapper {
    /**
     * 今日完成  本周完成  本月完成
     *
     * @param schema_name 入参
     */
    @Select(" SELECT COUNT " +
            " ( 1 ) AS monthCount, " +
            " ( " +
            " SELECT COUNT " +
            "  ( 1 )  " +
            " FROM " +
            "  ${schema_name}._sc_works_detail wd  " +
            " WHERE " +
            "  wd.finished_time >= ( " +
            "  SELECT CURRENT_DATE " +
            "   + CAST ( - 1 * ( TO_NUMBER( to_char( CURRENT_DATE, 'D' ), '99' ) - 2 ) || ' days' AS INTERVAL )  " +
            "  )  " +
            "  AND wd.finished_time <= ( " +
            "  SELECT CURRENT_DATE " +
            "   + CAST ( - 1 * ( TO_NUMBER( to_char( CURRENT_DATE, 'D' ), '99' ) - 2 ) + 6 || ' days' AS INTERVAL )  " +
            "  )  " +
            " ) AS weekCount, " +
            " ( SELECT COUNT ( 1 ) FROM ${schema_name}._sc_works_detail wd WHERE wd.finished_time >= CURRENT_DATE ) AS dayCount  " +
            " FROM " +
            " ${schema_name}._sc_works_detail wd  " +
            " WHERE " +
            " wd.finished_time >= date_trunc( 'month', now( ) ) ")
    Map<String, Object> findFinishedTaskCount(@Param("schema_name") String schema_name);

    /**
     * 当前上班人员列表
     *
     * @param schema_name 入参
     */
    @Select(" SELECT" +
            " u.user_name, " +
            " u.mobile, " +
            " u.account, " +
            " u.email, " +
            " u.id, " +
            " u.gender_tag, " +
            " u.user_code, " +
            " u.is_on_duty," +
            " (SELECT COUNT(1) FROM ${schema_name}._sc_works_detail WHERE is_main = 1 AND duty_user_id = u.id AND status IN (10, 15, 20, 30, 40, 50, 70, 80 ) ) AS taskCount " +
            " FROM" +
            " ${schema_name}._sc_user AS u" +
            " WHERE u.is_on_duty = '1'  ")
    List<Map<String, Object>> findOnDutyUserList(@Param("schema_name") String schema_name);


    /**
     * 故障任务总数
     * 当前的故障任务数   今日新增故障任务数   总数
     *
     * @param schema_name 入参
     */
    @Select(" SELECT COUNT " +
            " ( 1 ) AS finishedCount, " +
            " ( " +
            " SELECT COUNT " +
            "  ( 1 )  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  LEFT JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.id  " +
            " WHERE " +
            " wt.business_type_id = 1001 AND w.status  NOT IN ( 60, 110, 900, 120, 140 ) " +
            " ) AS noFinishedCount, " +
            " ( " +
            "  COUNT ( 1 ) + ( " +
            "  SELECT COUNT " +
            "   ( 1 )  " +
            "  FROM " +
            "   ${schema_name}._sc_works AS w " +
            "   LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "   LEFT JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.id  " +
            "  WHERE " +
            " (wt.business_type_id = 1001 AND w.status  NOT IN ( 60, 110, 900, 120, 140 )" +
            " ))) AS total  " +
            " FROM " +
            " ${schema_name}._sc_works AS w " +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.id  " +
            " WHERE   w.create_time >= CURRENT_DATE and " +
            " w.status  IN ( 60, 110, 900, 120, 140 )  " +
            " AND wt.business_type_id = 1001 ")
    Map<String, Object> findRepairFinishedCount(@Param("schema_name") String schema_name);

    /**
     * 维护任务总数
     * 当前的维护任务数   今日新增维护任务数   总数
     *
     * @param schema_name 入参那
     */
    @Select(" SELECT COUNT " +
            " ( 1 ) AS finishedCount, " +
            " ( " +
            " SELECT COUNT " +
            "  ( 1 )  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  LEFT JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.id  " +
            " WHERE " +
            " wt.business_type_id = 1002 AND w.status  NOT IN ( 60, 110, 900, 120, 140 ) " +
            " ) AS noFinishedCount, " +
            " ( " +
            "  COUNT ( 1 ) + ( " +
            "  SELECT COUNT " +
            "   ( 1 )  " +
            "  FROM " +
            "   ${schema_name}._sc_works AS w " +
            "   LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "   LEFT JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.id  " +
            "  WHERE " +
            " (wt.business_type_id = 1002 AND w.status  NOT IN ( 60, 110, 900, 120, 140 )" +
            " ))) AS total  " +
            " FROM " +
            " ${schema_name}._sc_works AS w " +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON w.work_type_id = wt.id  " +
            " WHERE   w.create_time >= CURRENT_DATE and " +
            " w.status  IN ( 60, 110, 900, 120, 140 )  " +
            " AND wt.business_type_id = 1002 ")
    Map<String, Object> findMaintainFinishedCount(@Param("schema_name") String schema_name);

    /**
     * 获取未完成维修工单列表
     */
    @Select(" SELECT " +
            " w.work_code, " +
            " w.work_type_id, " +
            " w.relation_id, " +
            " w.relation_type, " +
            " w.title, " +
            " w.occur_time, " +
            " w.deadline_time, " +
            " w.status, " +
            " w.create_time, " +
            " w.create_user_id, " +
            " w.position_code, " +
            " wd.sub_work_code, " +
            " wd.is_main, " +
            " wd.duty_user_id , " +
            " a.asset_name,a.asset_code,  " +
            " u.user_name,u.user_name AS  assignee_name ," +
            " ap.position_name  " +
            " FROM " +
            " ${schema_name}._sc_works AS w " +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = w.work_type_id  " +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON wd.relation_id = a.id  " +
            " LEFT JOIN ${schema_name}._sc_user AS u ON u.id = wd.duty_user_id  " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code  " +
            " WHERE " +
            " wd.is_main = 1  " +
            " AND a.status > " + StatusConstant.STATUS_DELETEED +
            " AND wt.business_type_id = 1001  " +
            " AND (w.status IN ( 10, 15, 20, 30, 40, 50, 70, 80 ))")
    List<Map<String, Object>> findRepairWorksList(@Param("schema_name") String schema_name);

    /**
     * 获取未保养维修工单列表
     */
    @Select(" SELECT " +
            " w.work_code, " +
            " w.work_type_id, " +
            " w.relation_id, " +
            " w.relation_type, " +
            " w.title, " +
            " w.occur_time, " +
            " w.deadline_time, " +
            " w.status, " +
            " w.create_time, " +
            " w.create_user_id, " +
            " w.position_code, " +
            " wd.sub_work_code, " +
            " wd.is_main, " +
            " wd.duty_user_id , " +
            " a.asset_name,a.asset_code,  " +
            " u.user_name,u.user_name AS  assignee_name, " +
            " ap.position_name  " +
            " FROM " +
            " ${schema_name}._sc_works AS w " +
            " LEFT JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = w.work_type_id  " +
            " LEFT JOIN ${schema_name}._sc_asset AS a ON wd.relation_id = a.id AND wd.relation_type = 2 " +
            " LEFT JOIN ${schema_name}._sc_user AS u ON u.id = wd.duty_user_id  " +
            " LEFT JOIN ${schema_name}._sc_asset_position AS ap ON w.position_code = ap.position_code  " +
            " WHERE " +
            " wd.is_main = 1  " +
            " AND a.status > " + StatusConstant.STATUS_DELETEED +
            " AND wt.business_type_id = 1002  " +
            " AND w.status IN ( 10, 15, 20, 30, 40, 50, 70, 80 )")
    List<Map<String, Object>> findMaintainWorksList(@Param("schema_name") String schema_name);

    /**
     * 本月运行时间 本月故障时间 本月维护时间
     */

    @Select(" SELECT COALESCE " +
            " ( repairTime, 0 ) AS repairTime, " +
            " COALESCE ( mainTime, 0 ) AS mainTime, " +
            " runningTime - repairTime - mainTime AS runTime  " +
            " FROM " +
            " ( " +
            " SELECT " +
            "  ( " +
            "  SELECT SUM " +
            "   ( " +
            "    round( date_part( 'epoch', CAST ( wd.finished_time AS TIMESTAMP ) - CAST ( wd.begin_time AS TIMESTAMP ) ) / 3600 )  " +
            "   )  " +
            "  FROM " +
            "   ${schema_name}._sc_works_detail AS wd " +
            "   LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON wd.relation_id = ao.asset_id " +
            "   LEFT JOIN ${schema_name}._sc_facilities AS f ON ao.org_id = f. " +
            "   ID LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = wd.work_type_id " +
            "   LEFT JOIN ${schema_name}._sc_asset AS A ON A.ID = ao.asset_id  " +
            "  WHERE " +
            "   wd.relation_type = 2  " +
            "   AND a.status > " + StatusConstant.STATUS_DELETEED +
            "   AND wd.status = 60  " +
            "   AND wt.business_type_id = 1001  " +
            "   ) + ( " +
            "  SELECT COALESCE " +
            "   ( " +
            "    SUM ( " +
            "     round( date_part( 'epoch', CAST ( wd.finished_time AS TIMESTAMP ) - CAST ( wd.begin_time AS TIMESTAMP ) ) / 3600 )  " +
            "    ), " +
            "    0  " +
            "   )  " +
            "  FROM " +
            "   ${schema_name}._sc_works_detail AS wd " +
            "   LEFT JOIN ${schema_name}._sc_asset AS A ON wd.relation_id = A.position_code " +
            "   LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON A.ID = ao.asset_id " +
            "   LEFT JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id " +
            "   LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = wd.work_type_id  " +
            "  WHERE " +
            "   wd.relation_type = 1  " +
            "   AND a.status > " + StatusConstant.STATUS_DELETEED +
            "   AND wd.status = 60  " +
            "   AND wt.business_type_id = 1001  " +
            "  ) AS repairTime, " +
            "  ( " +
            "  SELECT COALESCE " +
            "   ( " +
            "    SUM ( " +
            "     round( date_part( 'epoch', CAST ( wd.finished_time AS TIMESTAMP ) - CAST ( wd.begin_time AS TIMESTAMP ) ) / 3600 )  " +
            "    ), " +
            "    0  " +
            "   )  " +
            "  FROM " +
            "   ${schema_name}._sc_works_detail AS wd " +
            "   LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON wd.relation_id = ao.asset_id " +
            "   LEFT JOIN ${schema_name}._sc_facilities AS f ON ao.org_id = f. " +
            "   ID LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = wd.work_type_id " +
            "   LEFT JOIN ${schema_name}._sc_asset AS A ON A.ID = ao.asset_id  " +
            "  WHERE " +
            "   wd.relation_type = 2  " +
            "   AND a.status > " + StatusConstant.STATUS_DELETEED +
            "   AND wd.status = 60  " +
            "   AND wt.business_type_id = 1002  " +
            "   ) + ( " +
            "  SELECT COALESCE " +
            "   ( " +
            "    SUM ( " +
            "     round( date_part( 'epoch', CAST ( wd.finished_time AS TIMESTAMP ) - CAST ( wd.begin_time AS TIMESTAMP ) ) / 3600 )  " +
            "    ), " +
            "    0  " +
            "   )  " +
            "  FROM " +
            "   ${schema_name}._sc_works_detail AS wd " +
            "   LEFT JOIN ${schema_name}._sc_asset AS A ON wd.relation_id = A.position_code " +
            "   LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON A.ID = ao.asset_id " +
            "   LEFT JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id " +
            "   LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = wd.work_type_id  " +
            "  WHERE " +
            "   wd.relation_type = 1  " +
            "   AND a.status > " + StatusConstant.STATUS_DELETEED +
            "   AND wd.status = 60  " +
            "   AND wt.business_type_id = 1002  " +
            "  ) AS mainTime, " +
            "  COALESCE ( " +
            "   SUM ( " +
            "    round( " +
            "     date_part( " +
            "      'epoch', " +
            "      CAST ( date_trunc( 'hour', now( ) ) AS TIMESTAMP ) - CAST ( date_trunc( 'month', now( ) ) AS TIMESTAMP )  " +
            "     ) / 3600  " +
            "    )  " +
            "   ), " +
            "   0  " +
            "  ) AS runningTime  " +
            " FROM " +
            "  ${schema_name}._sc_asset " +
            "  AS A LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON A.ID = ao.asset_id " +
            "  LEFT JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            " WHERE " +
            " a.status > " + StatusConstant.STATUS_DELETEED +
            " ) A ")
    Map<String, Object> findAssetRunningTime(@Param("schema_name") String schema_name);

    /**
     * 查询组织的利用率
     */
    @Select(" SELECT " +
            " T2.INNER_CODE, " +
            " COALESCE ( T3.utilizationrate, 0 ) AS utilizationrate, " +
            " COALESCE ( T3.repairtime, 0 ) AS repairtime, " +
            " COALESCE ( T3.runningtime, 0 ) AS runningtime, " +
            " COALESCE ( T3.maintime, 0 ) AS maintime  " +
            "FROM " +
            " ( SELECT 'AD' AS inner_code UNION SELECT 'CU' AS inner_code UNION SELECT 'SD' AS inner_code UNION SELECT 'SM' AS inner_code ) t2 " +
            " LEFT JOIN ( " +
            " SELECT " +
            "  ( " +
            "   ( " +
            "    ( " +
            "     ( " +
            "     SELECT COUNT " +
            "      ( 1 )  " +
            "     FROM " +
            "      ${schema_name}._sc_asset_organization ao " +
            "      INNER JOIN ${schema_name}._sc_asset A ON ao.asset_id = A. " +
            "      ID INNER JOIN ${schema_name}._sc_facilities f ON f.ID = ao.org_id  " +
            "     WHERE " +
            "      f.inner_code = T.inner_code  " +
            "      AND A.status >- 1000  " +
            "      ) * date_part( " +
            "      'epoch', " +
            "      CAST ( date_trunc( 'hour', now( ) ) AS TIMESTAMP ) - CAST ( date_trunc( 'month', now( ) ) AS TIMESTAMP )  " +
            "     )  " +
            "     ) / 3600 - SUM ( " +
            "    CASE " +
            "       " +
            "      WHEN T.business_type_id = 1001 THEN " +
            "      round( date_part( 'epoch', CAST ( T.finished_time AS TIMESTAMP ) - CAST ( T.begin_time AS TIMESTAMP ) ) / 3600 ) ELSE 0  " +
            "     END  " +
            "      ) - SUM ( " +
            "     CASE " +
            "        " +
            "       WHEN T.business_type_id = 1002 THEN " +
            "       round( date_part( 'epoch', CAST ( T.finished_time AS TIMESTAMP ) - CAST ( T.begin_time AS TIMESTAMP ) ) / 3600 ) ELSE 0  " +
            "      END  " +
            "      )  " +
            "      ) / ( " +
            "      ( " +
            "       ( " +
            "       SELECT COUNT " +
            "        ( 1 )  " +
            "       FROM " +
            "        ${schema_name}._sc_asset_organization ao " +
            "        INNER JOIN ${schema_name}._sc_asset A ON ao.asset_id = A. " +
            "        ID INNER JOIN ${schema_name}._sc_facilities f ON f.ID = ao.org_id  " +
            "       WHERE " +
            "        f.inner_code = T.inner_code  " +
            "        AND A.status >- 1000  " +
            "        ) * date_part( " +
            "        'epoch', " +
            "        CAST ( date_trunc( 'hour', now( ) ) AS TIMESTAMP ) - CAST ( date_trunc( 'month', now( ) ) AS TIMESTAMP )  " +
            "       )  " +
            "      ) / 3600  " +
            "     )  " +
            "    ) AS utilizationrate, " +
            "    T.inner_code, " +
            "    ( " +
            "     ( " +
            "     SELECT COUNT " +
            "      ( 1 )  " +
            "     FROM " +
            "      ${schema_name}._sc_asset_organization ao " +
            "      INNER JOIN ${schema_name}._sc_asset A ON ao.asset_id = A. " +
            "      ID INNER JOIN ${schema_name}._sc_facilities f ON f.ID = ao.org_id  " +
            "     WHERE " +
            "      f.inner_code = T.inner_code  " +
            "      AND A.status >- 1000  " +
            "      ) * date_part( " +
            "      'epoch', " +
            "      CAST ( date_trunc( 'hour', now( ) ) AS TIMESTAMP ) - CAST ( date_trunc( 'month', now( ) ) AS TIMESTAMP )  " +
            "     )  " +
            "    ) / 3600 AS runningTime, " +
            "    SUM ( " +
            "    CASE " +
            "       " +
            "      WHEN T.business_type_id = 1001 THEN " +
            "      round( date_part( 'epoch', CAST ( T.finished_time AS TIMESTAMP ) - CAST ( T.begin_time AS TIMESTAMP ) ) / 3600 ) ELSE 0  " +
            "     END  " +
            "     ) AS repairtime, " +
            "     SUM ( " +
            "     CASE " +
            "        " +
            "       WHEN T.business_type_id = 1002 THEN " +
            "       round( date_part( 'epoch', CAST ( T.finished_time AS TIMESTAMP ) - CAST ( T.begin_time AS TIMESTAMP ) ) / 3600 ) ELSE 0  " +
            "      END  " +
            "      ) AS maintime  " +
            "     FROM " +
            "      ( " +
            "      SELECT A " +
            "       .ID, " +
            "       w.status, " +
            "       w.create_time, " +
            "       TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "       wd.begin_time, " +
            "       wd.finished_time, " +
            "       w.relation_id, " +
            "       w.relation_type, " +
            "       f.inner_code, " +
            "       wt.business_type_id  " +
            "      FROM " +
            "       ${schema_name}._sc_works AS w " +
            "       INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "       INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "       INNER JOIN ${schema_name}._sc_asset_organization AS ao ON A.ID = ao.asset_id " +
            "       INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            "      WHERE " +
            "       wd.is_main = 1  " +
            "       AND wt.business_type_id IN ( 1001, 1002 )  " +
            "       AND wd.relation_type = 2  " +
            "       AND A.status >- 1000 UNION ALL " +
            "      SELECT A " +
            "       .ID, " +
            "       w.status, " +
            "       w.create_time, " +
            "       TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "       wd.begin_time, " +
            "       wd.finished_time, " +
            "       w.relation_id, " +
            "       w.relation_type, " +
            "       f.inner_code, " +
            "       wt.business_type_id  " +
            "      FROM " +
            "       ${schema_name}._sc_works AS w " +
            "       INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "       INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "       INNER JOIN ${schema_name}._sc_asset_organization AS ao ON A.ID = ao.asset_id " +
            "       INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            "      WHERE " +
            "       wd.is_main = 1  " +
            "       AND wt.business_type_id IN ( 1001, 1002 )  " +
            "       AND wd.relation_type = 2  " +
            "       AND A.status >- 1000  " +
            "      ) T  " +
            "     WHERE " +
            "      TO_CHAR( T.create_time, 'YYYY-MM' ) = TO_CHAR( now( ), 'YYYY-MM' )  " +
            "     GROUP BY " +
            "     inner_code  " +
            " ) T3 ON T2.INNER_CODE = T3.INNER_CODE ")
    List<Map<String, Object>> findAssetByOrgUtilizationRate(@Param("schema_name") String schema_name);

    /**
     * 根据开始时间喝结束时间获取这段时间内所有设备的利用率
     *
     * @param schema_name 入参
     * @param start_time  入参
     * @param end_time    入参
     * @return 设备利用率
     */
    @Select(" <script>" +
            " SELECT " +
            " breakdownTime, " +
            " totalTime," +
            " totalTime - breakdownTime AS effectiveTime," +
            " CAST ( ( ( totalTime - breakdownTime ) / totalTime ) AS DECIMAL ( 18, 4 ) ) AS rate  " +
            " FROM " +
            " ( " +
            " SELECT " +
            "  ( " +
            "   ( " +
            "   SELECT COALESCE " +
            "    ( " +
            "     SUM ( " +
            "      round( date_part( 'epoch', CAST ( wd.finished_time AS TIMESTAMP ) - CAST ( wd.begin_time AS TIMESTAMP ) ) / 3600 )  " +
            "     ), " +
            "     0  " +
            "    )  " +
            "   FROM " +
            "    ${schema_name}._sc_works_detail AS wd " +
            "    LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON wd.relation_id = ao.asset_id " +
            "    LEFT JOIN ${schema_name}._sc_facilities AS f ON ao.org_id = f.id " +
            "    LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = wd.work_type_id  " +
            "    LEFT JOIN ${schema_name}._sc_asset AS a ON a.id = wd.relation_id  " +
            "   WHERE " +
            "    wd.relation_type = 2  " +
            "   AND a.status > " + StatusConstant.STATUS_DELETEED +
            "    AND wt.business_type_id IN ( 1001, 1002 )  " +
            "    AND wd.finished_time &gt;= #{start_time}::TIMESTAMP " +
            "    AND wd.finished_time &lt;= #{end_time}::TIMESTAMP " +
            "    ) + ( " +
            "   SELECT COALESCE " +
            "    ( " +
            "     SUM ( " +
            "      round( date_part( 'epoch', CAST ( wd.finished_time AS TIMESTAMP ) - CAST ( wd.begin_time AS TIMESTAMP ) ) / 3600 )  " +
            "     ), " +
            "     0  " +
            "    )  " +
            "   FROM " +
            "    ${schema_name}._sc_works_detail AS wd " +
            "    LEFT JOIN ${schema_name}._sc_asset AS A ON wd.relation_id = A.position_code " +
            "    LEFT JOIN ${schema_name}._sc_asset_organization AS ao ON A.ID = ao.asset_id " +
            "    LEFT JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id " +
            "    LEFT JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = wd.work_type_id  " +
            "   WHERE " +
            "    wd.relation_type = 1  " +
            "    AND a.status > " + StatusConstant.STATUS_DELETEED +
            "    AND wt.business_type_id IN ( 1001, 1002 )" +
            "    AND wd.finished_time &gt;= #{start_time}::TIMESTAMP " +
            "    AND wd.finished_time &lt;= #{end_time}::TIMESTAMP " +
            "   )  " +
            "  ) AS breakdownTime, " +
            "  ( " +
            "   COUNT ( 1 ) * round( date_part( 'epoch', CAST ( '${end_time}' AS TIMESTAMP ) - CAST ( '${start_time}'  AS TIMESTAMP ) ) / 3600 )  " +
            "  ) AS totalTime  " +
            " FROM " +
            "  ${schema_name}._sc_asset AS A  " +
            " WHERE " +
            "   a.status > " + StatusConstant.STATUS_DELETEED +
            " ) T" +
            " </script>")
    Map<String, Object> findMonthAssetRate(@Param("schema_name") String schema_name, @Param("start_time") String start_time, @Param("end_time") String end_time);

    /**-----------------------故障情况-------------------------------------*/
    /**
     * 今日故障数本周故障数本月故障数
     */
    @Select(" SELECT SUM " +
            " ( CASE WHEN TO_CHAR( T.create_time, 'YYYY-MM-dd' ) = TO_CHAR( NOW( ), 'YYYY-MM-dd' ) THEN 1 ELSE 0 END ) AS toDayRepairCount, " +
            " SUM ( " +
            " CASE " +
            "    " +
            "   WHEN EXTRACT ( YEAR FROM T.create_time ) = EXTRACT ( YEAR FROM NOW( ) )  " +
            "   AND EXTRACT ( WEEK FROM T.create_time ) = EXTRACT ( WEEK FROM NOW( ) ) THEN " +
            "    1 ELSE 0  " +
            "   END  " +
            "   ) AS nowWeekRepairCount, " +
            "   SUM ( CASE WHEN TO_CHAR( T.create_time, 'YYYY-MM' ) = TO_CHAR( NOW( ), 'YYYY-MM' ) THEN 1 ELSE 0 END ) AS nowMonthRepairCount  " +
            "  FROM " +
            "   ( " +
            "   SELECT A " +
            "    .ID, " +
            "    w.status, " +
            "    w.create_time, " +
            "    TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "    wd.begin_time, " +
            "    wd.finished_time, " +
            "    w.relation_id, " +
            "    w.relation_type  " +
            "   FROM " +
            "    ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "    INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "    INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "   WHERE " +
            "    wd.is_main = 1  " +
            "    AND wt.business_type_id = 1001  " +
            "    AND wd.relation_type = 2  " +
            "    AND a.status >-1000 UNION ALL " +
            "   SELECT A " +
            "    .ID, " +
            "    w.status, " +
            "    w.create_time, " +
            "    TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "    wd.begin_time, " +
            "    wd.finished_time, " +
            "    w.relation_id, " +
            "    w.relation_type  " +
            "   FROM " +
            "    ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "    INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "    INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "   WHERE " +
            "    wd.is_main = 1  " +
            "    AND wt.business_type_id = 1001  " +
            "    AND wd.relation_type = 2  " +
            "   AND a.status >-1000  " +
            " ) T ")
    Map<String, Object> findRepairCount(@Param("schema_name") String schema_name);

    /**
     * 当天修复情况
     */
    @Select(" SELECT " +
            " t2.inner_code, " +
            " COALESCE ( t1.finishedcount, 0 ) AS finishedcount, " +
            " COALESCE ( t1.nofinishedcount, 0 ) AS nofinishedcount  " +
            "FROM " +
            " ( SELECT 'AD' AS inner_code UNION SELECT 'CU' AS inner_code UNION SELECT 'SD' AS inner_code UNION SELECT 'SM' AS inner_code ) AS t2 " +
            " LEFT JOIN ( " +
            " SELECT T " +
            "  .inner_code, " +
            "  SUM ( " +
            "  CASE " +
            "     " +
            "    WHEN T.status = 60  " +
            "    AND TO_CHAR( T.finished_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' ) THEN " +
            "     1 ELSE 0  " +
            "    END  " +
            "    ) AS finishedCount, " +
            "    SUM ( " +
            "    CASE " +
            "       " +
            "      WHEN T.status IN ( 10, 15, 20, 30, 40, 50, 55, 60, 70, 80, 90 )  " +
            "      AND TO_CHAR( T.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' ) THEN " +
            "       1 ELSE 0  " +
            "      END  " +
            "      ) AS noFinishedCount  " +
            "     FROM " +
            "      ( " +
            "      SELECT A " +
            "       .ID, " +
            "       w.status, " +
            "       w.create_time, " +
            "       TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "       wd.begin_time, " +
            "       wd.finished_time, " +
            "       w.relation_id, " +
            "       w.relation_type, " +
            "       f.inner_code  " +
            "      FROM " +
            "       ${schema_name}._sc_works AS w " +
            "       INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "       INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "       INNER JOIN ${schema_name}._sc_asset_organization AS ao ON ao.asset_id = A. " +
            "       ID INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            "      WHERE " +
            "       wd.is_main = 1  " +
            "       AND wt.business_type_id = 1001  " +
            "       AND wd.relation_type = 2  " +
            "       AND f.inner_code IN ( 'AD', 'CU', 'SD', 'SM' )  " +
            "       AND ( " +
            "        TO_CHAR( W.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "        OR TO_CHAR( WD.finished_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "       )  " +
            "       AND A.status >- 1000 UNION ALL " +
            "      SELECT A " +
            "       .ID, " +
            "       w.status, " +
            "       w.create_time, " +
            "       TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "       wd.begin_time, " +
            "       wd.finished_time, " +
            "       w.relation_id, " +
            "       w.relation_type, " +
            "       f.inner_code  " +
            "      FROM " +
            "       ${schema_name}._sc_works AS w " +
            "       INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "       INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "       INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "       INNER JOIN ${schema_name}._sc_asset_organization AS ao ON ao.asset_id = A. " +
            "       ID INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            "      WHERE " +
            "       wd.is_main = 1  " +
            "       AND wt.business_type_id = 1001  " +
            "       AND wd.relation_type = 2  " +
            "       AND A.status >- 1000  " +
            "       AND f.inner_code IN ( 'AD', 'CU', 'SD', 'SM' )  " +
            "       AND ( " +
            "        TO_CHAR( W.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "        OR TO_CHAR( WD.finished_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "       )  " +
            "      ) T  " +
            "     GROUP BY " +
            "     T.inner_code  " +
            " ) T1 ON t1.inner_code = t2.inner_code ")
    List<Map<String, Object>> findTodayRepairSituation(@Param("schema_name") String schema_name);

    /**
     * 当天完好率
     */
    @Select(" SELECT COUNT " +
            " ( 1 ) AS repairAssetCount, " +
            " ( SELECT COUNT ( 1 ) FROM ${schema_name}._sc_asset WHERE status > -1000 ) AS goodAssetCount, " +
            " CAST ( " +
            "  ( ( SELECT COUNT ( 1 ) FROM ${schema_name}._sc_asset WHERE status > -1000 ) - COUNT ( 1 ) ) / ( SELECT COUNT ( 1 ) FROM ${schema_name}._sc_asset WHERE status > -1000 ) AS DECIMAL ( 18, 4 )  " +
            " ) AS goodAssetRate  " +
            "FROM " +
            " ( " +
            " SELECT A " +
            "  .ID, " +
            "  w.status, " +
            "  w.create_time, " +
            "  TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "  wd.begin_time, " +
            "  wd.finished_time, " +
            "  w.relation_id, " +
            "  w.relation_type, " +
            "  f.inner_code  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "  INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "  INNER JOIN ${schema_name}._sc_asset_organization AS ao ON ao.asset_id = A. " +
            "  ID INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            " WHERE " +
            "  wd.is_main = 1  " +
            "  AND wt.business_type_id = 1001  " +
            "  AND wd.relation_type = 2  " +
            "  AND TO_CHAR( W.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "  AND a.status >-1000 UNION ALL " +
            " SELECT A " +
            "  .ID, " +
            "  w.status, " +
            "  w.create_time, " +
            "  TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "  wd.begin_time, " +
            "  wd.finished_time, " +
            "  w.relation_id, " +
            "  w.relation_type, " +
            "  f.inner_code  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "  INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "  INNER JOIN ${schema_name}._sc_asset_organization AS ao ON ao.asset_id = A. " +
            "  ID INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            " WHERE " +
            "  wd.is_main = 1  " +
            "  AND wt.business_type_id = 1001  " +
            "  AND wd.relation_type = 2  " +
            "  AND a.status >-1000  " +
            " AND TO_CHAR( W.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            " ) T ")
    Map<String, Object> findTodayIntactRate(@Param("schema_name") String schema_name);

    /**
     * 当天故障分布
     */
    @Select(" SELECT CAST " +
            " ( " +
            "  ( " +
            "   COUNT ( 1 ) / ( " +
            "   SELECT COUNT " +
            "    ( 1 )  " +
            "   FROM " +
            "    ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "    INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "   WHERE " +
            "    WD.IS_MAIN = 1  " +
            "    AND WT.business_type_id = 1001  " +
            "    AND to_char( w.create_time, 'YYYY-MM-dd' ) = to_char( now( ), 'YYYY-MM-dd' )  " +
            "    AND w.status NOT IN ( 900 )  " +
            "   )  " +
            "  ) AS DECIMAL ( 18, 4 )  " +
            " ) AS RATE, " +
            " T.repair_type_id, " +
            " T.repair_type_name  " +
            " FROM " +
            " ( " +
            " SELECT " +
            " CASE " +
            "    " +
            "  WHEN " +
            "   wd.repair_type_id IS NULL THEN " +
            "    0 ELSE wd.repair_type_id  " +
            "    END AS repair_type_id, " +
            "  CASE " +
            "     " +
            "    WHEN wd.repair_type_id IS NULL THEN " +
            "    'title_aam_y' ELSE r.type_name  " +
            "   END AS repair_type_name  " +
            "  FROM " +
            "   ${schema_name}._sc_works AS w " +
            "   INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "   INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "   LEFT JOIN ${schema_name}._sc_repair_type AS r ON r.ID = wd.repair_type_id  " +
            "  WHERE " +
            "   to_char( w.create_time, 'YYYY-MM-dd' ) = to_char( now( ), 'YYYY-MM-dd' )  " +
            "   AND w.status NOT IN ( 900 )  " +
            "   AND wt.business_type_id = 1001  " +
            "  ) T  " +
            " GROUP BY " +
            " T.repair_type_id, " +
            " T.repair_type_name ")
    List<Map<String, Object>> findFaultDistribution(@Param("schema_name") String schema_name);

    /**
     * 故障趋势
     */
    @Select(" SELECT A " +
            " .click_date, " +
            " COALESCE ( B.finishedCount, 0 ) AS finishedCount, " +
            " COALESCE ( B.finishedRate, 0 ) AS finishedRate, " +
            " COALESCE ( B.noFinishedCount, 0 ) AS noFinishedCount  " +
            "FROM " +
            " ( " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE, 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '1 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '2 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '3 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '4 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '5 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '6 day', 'YYYY-MM-DD' ) AS click_date  " +
            " ) " +
            " A LEFT JOIN ( " +
            " SELECT T " +
            "  .create_date, " +
            "  ( " +
            "   SUM ( " +
            "   CASE " +
            "      " +
            "     WHEN T.STATUS = 60  " +
            "     AND TO_CHAR( T.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "     AND T.finished_time - T.create_time < INTERVAL '24 hour' THEN " +
            "      1 ELSE 0  " +
            "     END  " +
            "     )  " +
            "    ) / COUNT ( 1 ) AS finishedRate, " +
            "    SUM ( " +
            "    CASE " +
            "       " +
            "      WHEN T.STATUS = 60  " +
            "      AND TO_CHAR( T.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "      AND T.finished_time - T.create_time < INTERVAL '24 hour' THEN " +
            "       1 ELSE 0  " +
            "      END  " +
            "      ) AS finishedCount, " +
            "      SUM ( " +
            "      CASE " +
            "         " +
            "        WHEN T.STATUS IN ( 10, 15, 20, 30, 40, 50, 55, 60, 70, 80, 90 ) THEN " +
            "        1 ELSE 0  " +
            "       END  " +
            "       ) AS noFinishedCount  " +
            "      FROM " +
            "       ( " +
            "       SELECT A " +
            "        .ID, " +
            "        w.status, " +
            "        w.create_time, " +
            "        TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "        wd.begin_time, " +
            "        wd.finished_time, " +
            "        w.relation_id, " +
            "        w.relation_type  " +
            "       FROM " +
            "        ${schema_name}._sc_works AS w " +
            "        INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "        INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id  " +
            "       WHERE " +
            "        wd.is_main = 1  " +
            "        AND wd.relation_type = 2  " +
            "        AND a.status >- 1000 UNION ALL " +
            "       SELECT A " +
            "        .ID, " +
            "        w.status, " +
            "        w.create_time, " +
            "        TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "        wd.begin_time, " +
            "        wd.finished_time, " +
            "        w.relation_id, " +
            "        w.relation_type  " +
            "       FROM " +
            "        ${schema_name}._sc_works AS w " +
            "        INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "        INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id  " +
            "       WHERE " +
            "        wd.is_main = 1  " +
            "        AND wd.relation_type = 2  " +
            "        AND a.status > - 1000  " +
            "       ) AS T  " +
            "      WHERE " +
            "       T.create_time BETWEEN ( SELECT CURRENT_DATE - INTERVAL '7 day' )  " +
            "       AND ( SELECT CURRENT_DATE )  " +
            "      GROUP BY " +
            "       create_date  " +
            "      ORDER BY " +
            "       create_date  " +
            "      ) AS B ON A.click_date = b.create_date  " +
            "    ORDER BY " +
            " A.click_date ASC ")
    List<Map<String, Object>> findFaultTrend(@Param("schema_name") String schema_name);


/**-----------------------维护情况-------------------------------------*/
    /**
     * 今日维护数本周维护数本月维护数
     */
    @Select(" SELECT SUM " +
            " ( CASE WHEN TO_CHAR( T.create_time, 'YYYY-MM-dd' ) = TO_CHAR( NOW( ), 'YYYY-MM-dd' ) THEN 1 ELSE 0 END ) AS toDayMainCount, " +
            " SUM ( " +
            " CASE " +
            "    " +
            "   WHEN EXTRACT ( YEAR FROM T.create_time ) = EXTRACT ( YEAR FROM NOW( ) )  " +
            "   AND EXTRACT ( WEEK FROM T.create_time ) = EXTRACT ( WEEK FROM NOW( ) ) THEN " +
            "    1 ELSE 0  " +
            "   END  " +
            "   ) AS nowWeekMainCount, " +
            "   SUM ( CASE WHEN TO_CHAR( T.create_time, 'YYYY-MM' ) = TO_CHAR( NOW( ), 'YYYY-MM' ) THEN 1 ELSE 0 END ) AS nowMonthMainCount  " +
            "  FROM " +
            "   ( " +
            "   SELECT A " +
            "    .ID, " +
            "    w.status, " +
            "    w.create_time, " +
            "    TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "    wd.begin_time, " +
            "    wd.finished_time, " +
            "    w.relation_id, " +
            "    w.relation_type  " +
            "   FROM " +
            "    ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "    INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "    INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "   WHERE " +
            "    wd.is_main = 1  " +
            "    AND wt.business_type_id = 1002  " +
            "    AND wd.relation_type = 2  " +
            "    AND a.status >-1000 UNION ALL " +
            "   SELECT A " +
            "    .ID, " +
            "    w.status, " +
            "    w.create_time, " +
            "    TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "    wd.begin_time, " +
            "    wd.finished_time, " +
            "    w.relation_id, " +
            "    w.relation_type  " +
            "   FROM " +
            "    ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "    INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "    INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "   WHERE " +
            "    wd.is_main = 1  " +
            "    AND wt.business_type_id = 1002  " +
            "    AND wd.relation_type = 2  " +
            "   AND a.status >-1000  " +
            " ) T ")
    Map<String, Object> findMainCount(@Param("schema_name") String schema_name);

    /**
     * 当天设备点检
     */
    @Select(" SELECT T " +
            " .inner_code, " +
            " SUM ( " +
            " CASE " +
            "    " +
            "   WHEN T.status = 60  " +
            "   AND TO_CHAR( T.finished_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' ) THEN " +
            "    1 ELSE 0  " +
            "   END  " +
            "    ) / SUM ( " +
            "   CASE " +
            "      " +
            "     WHEN TO_CHAR( T.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' ) THEN " +
            "      1 ELSE 0  " +
            "     END  " +
            "     ) AS finishedRate  " +
            "    FROM " +
            "     ( " +
            "     SELECT A " +
            "      .ID, " +
            "      w.status, " +
            "      w.create_time, " +
            "      TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "      wd.begin_time, " +
            "      wd.finished_time, " +
            "      w.relation_id, " +
            "      w.relation_type, " +
            "      f.inner_code  " +
            "     FROM " +
            "      ${schema_name}._sc_works AS w " +
            "      INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "      INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "      INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "      INNER JOIN ${schema_name}._sc_asset_organization AS ao ON ao.asset_id = A. " +
            "      ID INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            "     WHERE " +
            "      wd.is_main = 1  " +
            "      AND wt.business_type_id = 1004  " +
            "      AND wd.relation_type = 2  " +
            "      AND f.inner_code IN ( 'AD', 'CU', 'SD', 'SM' )  " +
            "      AND ( " +
            "       TO_CHAR( W.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "       OR TO_CHAR( WD.finished_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "      )  " +
            "      AND a.status >- 1000 UNION ALL " +
            "     SELECT A " +
            "      .ID, " +
            "      w.status, " +
            "      w.create_time, " +
            "      TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "      wd.begin_time, " +
            "      wd.finished_time, " +
            "      w.relation_id, " +
            "      w.relation_type, " +
            "      f.inner_code  " +
            "     FROM " +
            "      ${schema_name}._sc_works AS w " +
            "      INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "      INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "      INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id " +
            "      INNER JOIN ${schema_name}._sc_asset_organization AS ao ON ao.asset_id = A. " +
            "      ID INNER JOIN ${schema_name}._sc_facilities AS f ON f.ID = ao.org_id  " +
            "     WHERE " +
            "      wd.is_main = 1  " +
            "      AND wt.business_type_id = 1004  " +
            "      AND wd.relation_type = 2  " +
            "      AND a.status >- 1000  " +
            "      AND f.inner_code IN ( 'AD', 'CU', 'SD', 'SM' )  " +
            "      AND ( " +
            "       TO_CHAR( W.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "       OR TO_CHAR( WD.finished_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "      )  " +
            "     ) T  " +
            "   GROUP BY " +
            " T.inner_code ")
    List<Map<String, Object>> findTodayEquipmentSpotCheck(@Param("schema_name") String schema_name);

    /**
     * 当天常规维护
     */
    @Select("SELECT SUM " +
            " ( CASE WHEN T.STATUS = 60 AND T.finished_time - T.create_time < INTERVAL '24 hour' THEN 1 ELSE 0 END ) / COUNT ( 1 ) AS finishedRate  " +
            "FROM " +
            " ( " +
            " SELECT A " +
            "  .ID, " +
            "  w.status, " +
            "  w.create_time, " +
            "  TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "  wd.begin_time, " +
            "  wd.finished_time, " +
            "  w.relation_id, " +
            "  w.relation_type  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "  INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            " WHERE " +
            "  wd.is_main = 1  " +
            "  AND wt.business_type_id = 1002  " +
            "  AND wd.relation_type = 2  " +
            "  AND TO_CHAR( w.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "  AND a.status >- 1000 UNION ALL " +
            " SELECT A " +
            "  .ID, " +
            "  w.status, " +
            "  w.create_time, " +
            "  TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "  wd.begin_time, " +
            "  wd.finished_time, " +
            "  w.relation_id, " +
            "  w.relation_type  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "  INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            " WHERE " +
            "  wd.is_main = 1  " +
            "  AND wt.business_type_id = 1002  " +
            "  AND wd.relation_type = 2  " +
            "  AND a.status >- 1000  " +
            " AND TO_CHAR( w.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            " ) T")
    Map<String, Object> findTodayRoutineMaintenance(@Param("schema_name") String schema_name);

    /**
     * 当天常规维护
     */
    @Select("SELECT SUM " +
            " ( CASE WHEN T.STATUS = 60 AND T.finished_time - T.create_time < INTERVAL '24 hour' THEN 1 ELSE 0 END ) / COUNT ( 1 ) AS finishedRate  " +
            "FROM " +
            " ( " +
            " SELECT A " +
            "  .ID, " +
            "  w.status, " +
            "  w.create_time, " +
            "  TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "  wd.begin_time, " +
            "  wd.finished_time, " +
            "  w.relation_id, " +
            "  w.relation_type  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "  INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            " WHERE " +
            "  wd.is_main = 1  " +
            "  AND wt.business_type_id = 1002  " +
            "  AND wd.relation_type = 2  " +
            "  AND TO_CHAR( w.create_time, 'YYYY-MM' ) = TO_CHAR( NOW( ), 'YYYY-MM' )  " +
            "  AND a.status >- 1000 UNION ALL " +
            " SELECT A " +
            "  .ID, " +
            "  w.status, " +
            "  w.create_time, " +
            "  TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "  wd.begin_time, " +
            "  wd.finished_time, " +
            "  w.relation_id, " +
            "  w.relation_type  " +
            " FROM " +
            "  ${schema_name}._sc_works AS w " +
            "  INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "  INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "  INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            " WHERE " +
            "  wd.is_main = 1  " +
            "  AND wt.business_type_id = 1002  " +
            "  AND wd.relation_type = 2  " +
            "  AND a.status >- 1000  " +
            " AND TO_CHAR( w.create_time, 'YYYY-MM' ) = TO_CHAR( NOW( ), 'YYYY-MM' )  " +
            " ) T")
    Map<String, Object> findMonthRoutineMaintenance(@Param("schema_name") String schema_name);

    /**
     * 维护趋势
     */
    @Select(" SELECT A " +
            " .click_date, " +
            " COALESCE ( B.finishedCount, 0 ) AS finishedCount, " +
            " COALESCE ( B.finishedRate, 0 ) AS finishedRate, " +
            " COALESCE ( B.noFinishedCount, 0 ) AS noFinishedCount  " +
            "FROM " +
            " ( " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE, 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '1 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '2 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '3 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '4 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '5 day', 'YYYY-MM-DD' ) AS click_date UNION ALL " +
            " SELECT " +
            "  TO_CHAR( CURRENT_DATE - INTERVAL '6 day', 'YYYY-MM-DD' ) AS click_date  " +
            " ) " +
            " A LEFT JOIN ( " +
            " SELECT T " +
            "  .create_date, " +
            "  ( " +
            "   SUM ( " +
            "   CASE " +
            "      " +
            "     WHEN T.STATUS = 60  " +
            "     AND TO_CHAR( T.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "     AND T.finished_time - T.create_time < INTERVAL '24 hour' THEN " +
            "      1 ELSE 0  " +
            "     END  " +
            "     )  " +
            "    ) / COUNT ( 1 ) AS finishedRate, " +
            "    SUM ( " +
            "    CASE " +
            "       " +
            "      WHEN T.STATUS = 60  " +
            "      AND TO_CHAR( T.create_time, 'YYYY-MM-DD' ) = TO_CHAR( NOW( ), 'YYYY-MM-DD' )  " +
            "      AND T.finished_time - T.create_time < INTERVAL '24 hour' THEN " +
            "       1 ELSE 0  " +
            "      END  " +
            "      ) AS finishedCount, " +
            "      SUM ( " +
            "      CASE " +
            "         " +
            "        WHEN T.STATUS IN ( 10, 15, 20, 30, 40, 50, 55, 60, 70, 80, 90 ) THEN " +
            "        1 ELSE 0  " +
            "       END  " +
            "       ) AS noFinishedCount  " +
            "      FROM " +
            "       ( " +
            "       SELECT A " +
            "        .ID, " +
            "        w.status, " +
            "        w.create_time, " +
            "        TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "        wd.begin_time, " +
            "        wd.finished_time, " +
            "        w.relation_id, " +
            "        w.relation_type  " +
            "       FROM " +
            "        ${schema_name}._sc_works AS w " +
            "        INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "        INNER JOIN ${schema_name}._sc_asset AS A ON A.ID = wd.relation_id " +
            "        INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "       WHERE " +
            "        wd.is_main = 1  " +
            "        AND wd.relation_type = 2  " +
            "        AND a.status >- 1000  " +
            "        AND wt.business_type_id = 1002 UNION ALL " +
            "       SELECT A " +
            "        .ID, " +
            "        w.status, " +
            "        w.create_time, " +
            "        TO_CHAR( w.create_time, 'YYYY-MM-DD' ) AS create_date, " +
            "        wd.begin_time, " +
            "        wd.finished_time, " +
            "        w.relation_id, " +
            "        w.relation_type  " +
            "       FROM " +
            "        ${schema_name}._sc_works AS w " +
            "        INNER JOIN ${schema_name}._sc_works_detail AS wd ON w.work_code = wd.work_code " +
            "        INNER JOIN ${schema_name}._sc_asset AS A ON A.position_code = wd.relation_id " +
            "        INNER JOIN ${schema_name}._sc_work_type AS WT ON WT.ID = W.work_type_id  " +
            "       WHERE " +
            "        wd.is_main = 1  " +
            "        AND wd.relation_type = 2  " +
            "        AND a.status > - 1000  " +
            "        AND wt.business_type_id = 1002  " +
            "       ) AS T  " +
            "      WHERE " +
            "       T.create_time BETWEEN ( SELECT CURRENT_DATE - INTERVAL '7 day' )  " +
            "       AND ( SELECT CURRENT_DATE )  " +
            "      GROUP BY " +
            "       create_date  " +
            "      ORDER BY " +
            "       create_date  " +
            "      ) AS B ON A.click_date = b.create_date  " +
            "    ORDER BY " +
            " A.click_date ASC ")
    List<Map<String, Object>> findMainTrend(@Param("schema_name") String schema_name);

    /**
     * 获取设备的故障状态（大屏用）
     *
     * @param schemaName 数据库
     * @return 设备列表
     */
    @Select(" SELECT A.asset_name, a.location::varchar, " +
            " CASE " +
            "  WHEN ( " +
            "   SELECT COUNT ( 1 )  " +
            "   FROM ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = w.work_type_id  " +
            "   WHERE " +
            "    w.relation_id = A.ID  " +
            "    AND w.relation_type = 2  " +
            "    AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1001 +
            "    AND w.status < " + StatusConstant.COMPLETED +
            "   ) > 0 " +
            "  THEN 1 " +
            "  WHEN ( " +
            "   SELECT COUNT ( 1 )  " +
            "   FROM ${schema_name}._sc_works AS w " +
            "    INNER JOIN ${schema_name}._sc_work_type AS wt ON wt.ID = w.work_type_id  " +
            "   WHERE " +
            "    w.relation_id = A.ID  " +
            "    AND w.relation_type = 2  " +
            "    AND wt.business_type_id = " + SensConstant.BUSINESS_NO_1002 +
            "    AND w.status < " + StatusConstant.COMPLETED +
            "   ) > 0 " +
            "  THEN 2 " +
            "  ELSE -1 " +
            "  END AS asset_status  " +
            " FROM ${schema_name}._sc_asset A  " +
            " WHERE A.status > " + StatusConstant.STATUS_DELETEED + " and a.location is not null ")
    List<Map<String, Object>> findAssetRepairStatusForScreen(@Param("schema_name") String schemaName);
}
