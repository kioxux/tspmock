package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.response.AssetCategoryResult;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface AssetCategoryMapper {


    /**
     * 新增数据
     */
    @Insert(" insert into ${schema_name}._sc_asset_category(remark,category_name,parent_id,data_order,is_use,category_code," +
            " fields,create_time,create_user_id,type) " +
            " values(#{s.remark},#{s.category_name},#{s.parent_id},#{s.data_order},#{s.is_use},#{s.category_code}," +
            " #{s.fields}::jsonb,current_timestamp,#{s.create_user_id},#{s.type}::int)")
    @Options(useGeneratedKeys = true, keyProperty = "s.id", keyColumn = "id")
    int insertAssetCategory(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);

    /**
     * 获取设备类型列表信息
     */
    @Select(" <script>" +
            " WITH RECURSIVE r AS (" +
            " SELECT t2.type_name, t1.id, t1.category_name,t1.parent_id,t1.category_code,t1.is_use, t1.remark," +
            " to_char(t1.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time " +
            " from ${schema_name}._sc_asset_category t1  " +
            " left join ${schema_name}._sc_asset_category_type t2 on t1.type = t2.id" +
            " ${whereString}" +
            " UNION ALL" +
            " SELECT t2.type_name, t.id, t.category_name,t.parent_id,t.category_code,t.is_use, t.remark, " +
            " to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time" +
            " FROM ${schema_name}._sc_asset_category T" +
            " left join ${schema_name}._sc_asset_category_type t2 on t.type = t2.id " +
            " ,r " +
            " WHERE T.id = r.parent_id)" +
            " SELECT  DISTINCT type_name, id, category_name,parent_id,category_code,is_use, remark,create_time " +
            " FROM r " +
            " ORDER BY create_time DESC " +
            " </script>")
    List<Map<String, Object>> findAssetCategoryList(@Param("schema_name") String schemaName,
                                                    @Param("whereString") String whereString);

    /**
     * 根据category_id搜索自定义字段信息
     *
     * @param schemaName 数据库
     * @param id         设备类型主键
     * @return 设备类型自定义类型信息
     */
    @Select("SELECT fields FROM ${schema_name}._sc_asset_category WHERE id = #{id}")
    Object findAssetCategoryFields(@Param("schema_name") String schemaName, @Param("id") Integer id);

    /**
     * 获取出自己外的设备类型列表
     */
    @Select("SELECT id, category_name ,category_code FROM  ${schema_name}._sc_asset_category WHERE  is_use = '1' ORDER BY ID DESC ")
    List<Map<String, Object>> findAssetCategoryLists(@Param("schema_name") String schemaName);

    /**
     * 获取出自己外的设备类型列表
     */
    @Select("SELECT DISTINCT c.id, c.category_name ,c.category_code " +
            "FROM ${schema_name}._sc_asset_category c " +
            "INNER JOIN ${schema_name}._sc_metadata_monitors mm ON mm.asset_category_id = c.id " +
            "WHERE c.is_use = '1' ORDER BY ID DESC")
    List<Map<String, Object>> findIotAssetCategoryLists(@Param("schema_name") String schemaName);

    /**
     * 编辑设备类型
     */
    @Update(" <script> update ${schema_name}._sc_asset_category set " +
            " id=#{s.asset_category_id}::int " +
            " <when test='s.category_name!=null'> " +
            " ,category_name=#{s.category_name} " +
            " </when> " +
            " ,parent_id=#{s.parent_id} " +
            " <when test='s.data_order!=null'> " +
            " ,data_order=#{s.data_order}::int " +
            " </when> " +
            " <when test='s.is_use!=null'> " +
            " ,is_use=#{s.is_use} " +
            " </when> " +
            " <when test='s.remark!=null'> " +
            " ,remark=#{s.remark} " +
            " </when> " +
            " <when test='s.category_code!=null'> " +
            " ,category_code=#{s.category_code} " +
            " </when> " +
            " <when test='s.fields!=null'> " +
            " ,fields=#{s.fields}::jsonb " +
            " </when> " +
            " <when test='s.type!=null'> " +
            " ,type=#{s.type}::int  " +
            " </when> " +
            " where id=#{s.asset_category_id}::int </script> ")
    void updateAssetCategory(@Param("schema_name") String schemaName, @Param("s") Map<String, Object> paramMap);

    /**
     * 删除设备类型
     *
     * @param schema_name 入参
     * @param id          入参
     */
    @Delete("delete from ${schema_name}._sc_asset_category  where id=#{id}")
    void deleteAssetCategoryById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 根据id获取设备类型详情
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 返回数据
     */
    @Select(" SELECT t1.id,t1.category_name,t1.parent_id,t1.data_order,t1.is_use,t1.category_code,t1.category_name,t1.remark," +
            " t1.fields::varchar,t1.create_time,t1.create_user_id,t1.type,t2.category_name AS parentCategoryName," +
            " t3.type_name" +
            " FROM ${schema_name}._sc_asset_category AS t1" +
            " LEFT JOIN ${schema_name}._sc_asset_category AS t2 ON t1.parent_id = t2.id" +
            " LEFT JOIN ${schema_name}._sc_asset_category_type AS t3 ON t1.type = t3.id" +
            " WHERE t1.id = #{id}")
    Map<String, Object> findAssetCategoryById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    @Select("select " +
            " jsonb_array_elements(fields) ->> 'field_code' as field_code, " +
            " jsonb_array_elements(fields) ->> 'field_name' as field_name " +
            " from ${schema_name}._sc_asset_category where is_use='1' GROUP BY field_code,field_name ORDER BY field_code ")
    List<Map<String, Object>> findAssetFields(@Param("schema_name") String schema_name);

    /**
     * 根据id获取设备类型是否有子设备类型
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 返回数据
     */
    @Select(" SELECT t1.id,t1.category_name,t1.parent_id,t1.data_order,t1.is_use,t1.category_code,t1.remark," +
            " t1.fields,t1.create_time,t1.create_user_id,t1.type,t2.category_name AS parentCategoryName," +
            " t3.type_name" +
            " FROM ${schema_name}._sc_asset_category AS t1" +
            " LEFT JOIN ${schema_name}._sc_asset_category AS t2 ON t1.parent_id = t2.id" +
            " LEFT JOIN ${schema_name}._sc_asset_category_type AS t3 ON t1.type = t3.id" +
            " WHERE t1.parent_id = #{id} ORDER BY  t1.id LIMIT 1 ")
    Map<String, Object> findAssetCategoryChildById(@Param("schema_name") String schema_name, @Param("id") Integer id);


    /**
     * 根据设备类型id查询是否有设备型号绑定
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 返回数据
     */
    @Select(" SELECT t.id " +
            " FROM ${schema_name}._sc_asset_model t " +
            " WHERE t.category_id = #{id} ORDER BY  t.id LIMIT 1 ")
    Map<String, Object> findAssetModelByCategoryId(@Param("schema_name") String schema_name, @Param("id") Integer id);


    /**
     * 根据设备类型id查询是否绑定设备
     *
     * @param schema_name 入参
     * @param id          入参
     * @return 返回数据
     */
    @Select(" SELECT t.id " +
            " FROM ${schema_name}._sc_asset t" +
            " WHERE t.category_id = #{id} and t.status > " + StatusConstant.STATUS_DELETEED + " ORDER BY  t.id LIMIT 1 ")
    Map<String, Object> findAssetByAssetCategoryId(@Param("schema_name") String schema_name, @Param("id") Integer id);

    //获取所有设备类型
    @Select("select fields from ${schema_name}._sc_asset_category where id = #{id}")
    List<AssetCategoryResult> findAll(@Param("schema_name") String schema_name);

    //根据类型id获取自定义字段
    String findfieldsById(@Param("schema_name") String schema_name, @Param("id") Integer id);

    /**
     * 验证备件类型是否存在
     *
     * @param schemaName
     * @param assetTypeList
     * @return
     */
    @Select("<script>select distinct id, category_name from ${schemaName}._sc_asset_category " +
            "where is_use = '" + StatusConstant.IS_USE_YES + "' and category_name in " +
            "<foreach collection='assetTypes' item='assetType' open='(' close=')' separator=','> " +
            "#{assetType} " +
            "</foreach></script>")
    @MapKey("category_name")
    Map<String, Map<String, Object>> queryAssetTypeByCode(@Param("schemaName") String schemaName, @Param("assetTypes") List<String> assetTypeList);

    /**
     * 根据设备类型名称查询设备类型
     */
    @Select("SELECT * FROM ${schemaName}._sc_asset_category WHERE category_name = #{category_name} limit 1")
    Map<String, Object> findAssetTypeByName(@Param("schemaName") String schemaName, @Param("category_name") String category_name);

    /**
     * 根据设备类型编码查询设备类型
     */
    @Select("SELECT * FROM ${schemaName}._sc_asset_category WHERE category_code = #{category_code} limit 1")
    Map<String, Object> findAssetTypeByCode(@Param("schemaName") String schemaName, @Param("category_code") String category_code);


    /**
     * 根据设备类型id获取类型父级拼接字符串
     *
     * @param schemaName 数据库
     * @return 设备类型id获取类型父级拼接字符串
     */
    @Select(" select id, category_name, " +
            " ( select * from ( WITH RECURSIVE re (id, parent_id) AS ( " +
            " SELECT t.id, t.parent_id,t.category_name  FROM ${schema_name}._sc_asset_category t where id = ta.id " +
            " UNION ALL " +
            " SELECT t2.id, t2.parent_id,t2.category_name  FROM ${schema_name}._sc_asset_category t2, re e WHERE e.parent_id=t2.id " +
            " ) select string_agg ( category_name ,'/') from re ) t ) as all_name " +
            " from ${schema_name}._sc_asset_category ta ")
    @MapKey("id")
    Map<Integer, Map<String, Object>> findCategoryNameString(@Param("schema_name") String schemaName);

    @Select("select * from ${schema_name}._sc_asset_category ")
    List<Map<String, Object>> selectall(@Param("schema_name") String schemaName);

    @Update(" <script> update ${schemaName}._sc_asset_category set " +
            " fields=#{fields}::jsonb " +
            " where id=#{id}::int </script> ")
    void update(Map<String, Object> category);
}
