package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface AssetMapper {
    /**
     * 新增数据
     *
     * @param assetMap 数据
     * @return 成功数量
     */
    @Insert("INSERT INTO ${schemaName}._sc_asset " +
            "(id,running_status_id,asset_name,asset_code,enable_time,status,file_ids,supplier_id,remark," +
            "parent_id,properties,category_id,create_time,asset_model_id,importment_level_id,create_user_id," +
            "quantity,unit_id,price,tax_rate,tax_price,currency_id,position_code,buy_date,install_date,manufacturer_id," +
            "install_price,use_year,buy_currency_id,install_currency_id) " +
            "values (#{id},#{running_status_id}::int,#{asset_name},#{asset_code},to_date ( #{enable_time}, '" + SqlConstant.SQL_DATE_FMT + "' ),#{status}::int,#{file_ids},#{supplier_id}::int,#{remark}," +
            "#{parent_id},#{properties}::jsonb,#{category_id}::int,#{create_time},#{asset_model_id}::int,#{importment_level_id}::int,#{create_user_id}," +
            "#{quantity}::float,#{unit_id}::int,#{price}::numeric,#{tax_rate}::numeric,#{tax_price}::numeric,#{currency_id}::int,#{position_code},to_date ( #{buy_date}, '" + SqlConstant.SQL_DATE_FMT + "' ),to_date ( #{install_date}, '" + SqlConstant.SQL_DATE_FMT + "' ),#{manufacturer_id}::int," +
            "#{install_price}::numeric,#{use_year}::numeric,#{buy_currency_id}::int,#{install_currency_id}::int)")
    int insertAsset(Map<String, Object> assetMap);

    /**
     * 更新设备信息
     *
     * @param assetMap 更新信息
     * @return 成功数量
     */
    @Update("<script>" +
            "update ${schemaName}._sc_asset set " +
            "<when test=\"null != sectionType and 'assetIcon' == sectionType\"> " +
            " asset_icon = #{asset_icon} " +
            "</when>" +
            "<when test=\"null != sectionType and 'assetTop' == sectionType\"> " +
            "asset_name=#{asset_name},asset_code=#{asset_code}," +
            "running_status_id=#{running_status_id},status=#{status},guarantee_status_id=#{guarantee_status_id}" +
            "</when>" +
            "<when test=\"null != sectionType and 'assetBase' == sectionType\"> " +
            "category_id=#{category_id},asset_model_id=#{asset_model_id}, " +
            "position_code=#{position_code},importment_level_id=#{importment_level_id}," +
            "enable_time=to_date ( #{enable_time}, '" + SqlConstant.SQL_DATE_FMT + "' ),use_year=#{use_year},remark=#{remark}," +
            "parent_id=#{parent_id},unit_id=#{unit_id}" +
            "</when>" +
            "<when test=\"null != modelChange and 'is' == modelChange\"> " +
            ",file_ids=#{file_ids}" +
            "</when>" +
            "<when test=\"null != fileIdsChange and !'' == fileIdsChange\"> " +//类型更新修改设备附件信息
            "file_ids=#{file_ids}" +
            "</when>" +
            "<when test=\"null != dyInfoChange and '1'.toString() == dyInfoChange\"> " +
            "properties=#{properties}::json" +
            "</when>" +
            "<when test=\"null != dyInfoChange and '2'.toString() == dyInfoChange\"> " +
            ",properties=#{properties}::json" +
            "</when>" +
            "<when test=\"null != sectionType and 'assetBuy' == sectionType\"> " +
            "buy_date= to_date ( #{buy_date}, '" + SqlConstant.SQL_DATE_FMT + "' ) ,tax_rate=#{tax_rate}," +
            "tax_price=#{tax_price},price=#{price},currency_id=#{currency_id},buy_currency_id=#{buy_currency_id}::int" +
            "</when>" +
            "<when test=\"null != sectionType and 'assetInstall' == sectionType\"> " +
            "install_date=to_date ( #{install_date}, '" + SqlConstant.SQL_DATE_FMT + "' ),install_price=#{install_price},install_currency_id=#{install_currency_id}::int " +
            "</when>" +
            "<when test=\"null != sectionType and 'assetStatusOnly' == sectionType\"> " +
            "status=#{status} " +
            "</when>" +
            "where id=#{id}" +
            "</script>")
    int updateAsset(Map<String, Object> assetMap);

    @Update("<script>" +
            " update ${schemaName}._sc_asset" +
            "<set>" +
            "<if test=\"null != position_code and '' != position_code\"> " +
            "position_code = #{position_code}," +
            "</if>" +
            "<if test=\"null != status\"> " +
            "status = #{status}," +
            "</if>" +
            "</set>" +
            " where id=#{id}" +
            "</script>")
    void updateAssetPositionCode(Map<String, Object> assetMap);

    @Update(" update ${schema_name}._sc_asset " +
            " set properties = #{pm.properties}::jsonb " +
            " where id=#{pm.id}")
    void updateAssetProperties(@Param("schema_name") String schema_name, @Param("pm") Map<String, Object> pm);

    /**
     * 更新子设备的位置(递归处理)
     *
     * @param schema_name  数据库
     * @param id           设备主键
     * @param positionCode 设备位置编码
     */
    @Update("update ${schema_name}._sc_asset set position_code=#{positionCode} " +
            "where id in ( " +
            "   with RECURSIVE re (id,parent_id) as " +
            "   ( " +
            "       select id,parent_id from ${schema_name}._sc_asset where id = #{id} " +
            "       union all " +
            "       select a2.id,a2.parent_id from ${schema_name}._sc_asset a2,re e where e.id=a2.parent_id " +
            "   ) " +
            "   select id from re" +
            ") ")
    void updateSubPosition(@Param("schema_name") String schema_name, @Param("id") String id, @Param("positionCode") String positionCode);

    @Update(" <script> update ${schema_name}._sc_asset set status = #{status} where id in " +
            "  <foreach collection='ids' item='id' open='(' close=')' separator=','> #{id} </foreach> </script> ")
    int setSelectStatus(@Param("schema_name") String schema_name, @Param("ids") String[] id, @Param("status") int status);

    @Update("update ${schema_name}._sc_asset set ${field_name}=#{field_value} where id=#{id};")
    @Deprecated
    void updateAssetFieldValue(@Param("schema_name") String schema_name, @Param("field_name") String field_name,
                               @Param("field_value") Object value, @Param("id") String id);


    /**
     * 查询设备列表
     *
     * @param schemaName  数据库
     * @param whereString 条件
     * @param userId      用户主键
     * @return 列表
     */
    @Select("SELECT t.id,t.asset_code,t.asset_name,t.position_code,t.asset_model_id,t.category_id,t.status,t.enable_time,t.create_time,t.supplier_id," +
            "t.importment_level_id::varchar,t.manufacturer_id,t9.model_name AS model_name,t9.model_name AS asset_model_name,t4.status AS status_name,t6.category_name AS category_name,p1.position_name, " +
            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t5.short_title ), ',' ) AS supplier_name, " +
            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t5_1.short_title ), ',' ) AS manufacturer_name, " +
//            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t2.title ), ',' ) AS site_name, " +
//            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT f.title ), ',' ) AS parent_site_name, " +
            "t.running_status_id, t11.running_status as running_status_name, t.guarantee_status_id::varchar, t13.level_name as importment_level_name, " +
            "t.asset_code AS qci_first, t.asset_name AS qci_second, t9.model_name as qci_fourth, " +
//            "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t2.title ), ',' ) AS qci_third " +
            "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT p1.position_name ), ',' ) AS qci_third ," +
            "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT p1.position_name ), ',' ) AS asset_position_name, " +
            "max(sf_c.title) as customer_name " +
            SqlConstant.ASSET_SEARCH_LIST_SQL +
//            "WHERE f.org_type_id =4 "+
            "GROUP BY t.id,t.asset_name,t.asset_code,t.status,t.enable_time,t.create_time,t.supplier_id," +
            "t.category_id,t.importment_level_id,t.position_code,t.manufacturer_id,t.asset_model_id," +
            "t9.model_name,t4.status," +
            "t6.category_name,p1.position_name,t.running_status_id,t11.running_status,t.guarantee_status_id,t13.level_name,t.parent_id " +
//            ",t2.title,f.title " +
            "ORDER BY t.asset_code,t.parent_id")
    List<Map<String, Object>> findAssetList(@Param("schema_name") String schemaName, @Param("whereString") String whereString, @Param("userId") String userId);


    /**
     * 查询设备列表
     *
     * @param schemaName  数据库
     * @param whereString 条件
     * @param ids         用户主键
     * @return 列表
     */
    @Select("<script>SELECT T.ID || (pp ->> 'field_code') as key, pp ->> 'field_value' as field_value, pp ->> 'field_view_type' as field_view_type, pp ->> 'field_source' as field_source " +
            " FROM ( SELECT T.ID, jsonb_array_elements ( properties ) pp " +
            "       FROM ( SELECT T.ID, T.properties FROM ${schema_name}._sc_asset T where id in " +
            "           <foreach collection='ids' item='id' open='(' separator=',' close=')'> " +
            "               #{id} " +
            "           </foreach> " +
            "      ) T " +
            " ) T ${whereString} </script>")
    @MapKey("key")
    Map<String, Map<String, Object>> findAssetBodyInfo(@Param("schema_name") String schemaName, @Param("whereString") String whereString, @Param("ids") String[] ids);


    /**
     * 获得设备故障履历列表
     *
     * @param schemaName    入参
     * @param id            入参
     * @param keywordSearch 入参
     * @return 设备故障履历列表
     */
    @Select("<script>" +
            " SELECT t1.work_code,t2.sub_work_code,t1.work_type_id,t1.relation_type,t1.relation_id,t2.fault_type_id,t2.repair_code," +
            "t3.type_name,t4.code_name ,to_char(t2.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as finished_time" +
            " FROM ${schema_name}._sc_works AS t1" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS t2 ON t1.work_code = t2.work_code " +
            " AND ( SELECT max ( finished_time ) FROM ${schema_name}._sc_works_detail " +
            " WHERE work_code = t1.work_code ) = t2.finished_time" +
            " LEFT JOIN ${schema_name}._sc_fault_type AS t3 ON t2.fault_type_id = t3.ID " +
            " LEFT JOIN ${schema_name}._sc_fault_code AS t4 ON t2.repair_code = t4.code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS t5 ON t1.work_type_id = t5.id " +
            " LEFT JOIN ${schema_name}._sc_business_type AS t6 ON t5.business_type_id = t6.business_type_id " +
            " WHERE t1.relation_type = 2 AND t6.business_type_id = 1001 AND t1.status = 60 " +
            " AND t1.relation_id  = #{id}" +
            "    <when test='keywordSearch != null'> " +
            "       and (t1.work_code like CONCAT('%', #{keywordSearch}, '%') or " +
            "       t3.type_name like CONCAT('%', #{keywordSearch}, '%') or" +
            "       t4.code_name like CONCAT('%', #{keywordSearch}, '%') )" +
            "    </when> order by  t2.finished_time desc" +
            " </script>")
    List<Map<String, Object>> findFaultRecordList(@Param("schema_name") String schemaName, @Param("id") String id, @Param("keywordSearch") String keywordSearch);

    /**
     * 获取设备历史工作信息
     *
     * @param schemaName    入参
     * @param id            入参
     * @param keywordSearch 入参
     * @return 设备历史工作信息
     */
    @Select("<script>" +
            " SELECT t1.work_code,t2.sub_work_code,t1.work_type_id,t1.relation_type,t1.relation_id,t2.fault_type_id,t2.repair_code,t3.type_name,t4.code_name " +
            " ,t2.receive_user_id ,to_char(t2.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') AS finished_time," +
            " t5.type_name as work_type_name ,t7.user_name,t1.title,t6.business_type_id " +
            "FROM ${schema_name}._sc_works AS t1" +
            " LEFT JOIN ${schema_name}._sc_works_detail AS t2 ON t1.work_code = t2.work_code " +
            " LEFT JOIN ${schema_name}._sc_fault_type AS t3 ON t2.fault_type_id = t3.ID " +
            " LEFT JOIN ${schema_name}._sc_fault_code AS t4 ON t2.repair_code = t4.code " +
            " LEFT JOIN ${schema_name}._sc_work_type AS t5 ON t1.work_type_id = t5.id " +
            " LEFT JOIN ${schema_name}._sc_business_type AS t6 ON t5.business_type_id = t6.business_type_id " +
            " LEFT JOIN ${schema_name}._sc_user AS t7 ON t2.receive_user_id = t7.id " +
            " WHERE t1.relation_type = 2 AND  t6.business_type_id  <![CDATA[ < ]]> " + SensConstant.BUSINESS_NO_2000 + "  AND t1.status =60 " +
            " AND t2.relation_id = #{id} " +
            "    <when test='keywordSearch != null'> " +
            "       and (t1.work_code like CONCAT('%', #{keywordSearch}, '%') or " +
            "       t3.type_name like CONCAT('%', #{keywordSearch}, '%') or " +
            "       t4.code_name like CONCAT('%', #{keywordSearch}, '%') or " +
            "       t5.type_name like CONCAT('%', #{keywordSearch}, '%') ) " +
            "    </when>" +
            "    <when test='startDateSearch != null'> " +
            "       and to_char(t2.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') <![CDATA[ >= ]]> #{startDateSearch} " +
            "    </when>" +
            "    <when test='endDateSearch != null'> " +
            "       and to_char(t2.finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') <![CDATA[ <= ]]> #{endDateSearch} " +
            "    </when>" +
            "order by t2.finished_time desc " +
            " </script>")
    List<Map<String, Object>> findAssetHistoryWorkInfo(@Param("schema_name") String schemaName, @Param("id") String id, @Param("keywordSearch") String keywordSearch,
                                                       @Param("startDateSearch") String startDateSearch, @Param("endDateSearch") String endDateSearch);

    /**
     * 获取设备下一步工作信息
     *
     * @return 列表数据
     */
//    @Select("<script>" +
//            " SELECT t1.work_code,t5.type_name AS work_type_name, t1.work_type_id,t1.relation_type,t1.relation_id,t2.fault_type_id,t2.repair_code,t3.type_name,t4.code_name " +
//            " ,t2.receive_user_id,to_char(t2.begin_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') AS begin_time,t5.type_name as work_type_name , t7.user_name FROM ${schema_name}._sc_works AS t1" +
//            " LEFT JOIN ${schema_name}._sc_works_detail AS t2 ON t1.work_code = t2.work_code " +
//            " AND ( SELECT max ( finished_time ) FROM ${schema_name}._sc_works_detail " +
//            " WHERE work_code = t1.work_code ) = t2.finished_time" +
//            " LEFT JOIN ${schema_name}._sc_fault_type AS t3 ON t2.fault_type_id = t3.ID " +
//            " LEFT JOIN ${schema_name}._sc_fault_code AS t4 ON t2.repair_code = t4.code " +
//            " LEFT JOIN ${schema_name}._sc_work_type AS t5 ON t1.work_type_id = t5.id " +
//            " LEFT JOIN ${schema_name}._sc_business_type AS t6 ON t5.business_type_id = t6.business_type_id " +
//            " LEFT JOIN ${schema_name}._sc_user AS t7 ON t2.receive_user_id = t7.id " +
//            " WHERE t1.relation_type = 2 AND  t6.business_type_id  <![CDATA[ < ]]> 5  " +
//            " AND t1.status <![CDATA[ < ]]> 60  AND t1.status > 10 AND t1.work_type_id <![CDATA[ < ]]> 20  " +
//            " AND t1.relation_id  = #{id}  order by     t2.begin_time  desc " +
//            " </script>")
//    @Select("SELECT pwc.work_calendar_code,pwc.title,pwc.work_type_id,pwc.relation_type, " +
//            "to_char( pwc.occur_time, 'yyyy-MM-dd' ) AS occur_day, " +
//            "to_char( pwc.occur_time, 'HH24:mi:ss' ) AS occur_time, " +
//            "to_char( pwc.deadline_time, 'yyyy-MM-dd HH24:mi:ss' ) AS deadline_time, " +
//            "wt.type_name as work_type_name,u.user_name,pwc.relation_id " +
//            "FROM ${schema_name}._sc_plan_work_calendar pwc " +
//            "JOIN ${schema_name}._sc_works w on pwc.work_calendar_code = w.work_calendar_code " +
//            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON pwc.work_type_id = wt.ID " +
//            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id " +
//            "LEFT JOIN ${schema_name}._sc_user AS u ON pwc.receive_user_id = u.ID " +
//            "WHERE pwc.relation_id = #{id} AND w.status < " + StatusConstant.COMPLETED +
//            " ORDER BY pwc.occur_time asc ")
    @Select("SELECT * from (SELECT pwc.work_calendar_code,pwc.title,pwc.work_type_id,pwc.relation_type,   " +
            "to_char( pwc.occur_time, 'yyyy-MM-dd' ) AS occur_day,   " +
            "to_char( pwc.occur_time, 'HH24:mi:ss' ) AS occur_time, " +
            "to_char( pwc.deadline_time, 'yyyy-MM-dd HH24:mi:ss' ) AS deadline_time, " +
            "pwc.occur_time as time,wt.type_name as work_type_name,u.user_name,pwc.relation_id, " +
            "'' as fault_code, '' as fault_note,1 as type " +
            "FROM ${schema_name}._sc_plan_work_calendar pwc   " +
            "JOIN ${schema_name}._sc_works w on pwc.work_calendar_code = w.work_calendar_code " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON pwc.work_type_id = wt.ID   " +
            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id   " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON pwc.receive_user_id = u.ID   " +
            "WHERE pwc.relation_id = #{id} AND w.status < 60  " +
            "union all " +
            "SELECT  " +
            "wd.sub_work_code,wd.title,wd.work_type_id,wd.relation_type, " +
            "to_char( w.occur_time, 'yyyy-MM-dd' ) AS occur_day,   " +
            "to_char( w.occur_time, 'HH24:mi:ss' ) AS occur_time,   " +
            "to_char( w.deadline_time, 'yyyy-MM-dd HH24:mi:ss' ) AS deadline_time, " +
            "w.occur_time,wt.type_name as work_type_name,u.user_name,wd.relation_id, " +
            "'' as fault_code, '' as fault_note,2 as type " +
            "FROM ${schema_name}._sc_works w  " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd on w.work_code = wd.work_code  " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wd.work_type_id = wt.ID   " +
            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id   " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.ID   " +
            "WHERE wd.relation_id = #{id} AND w.status < 60 and bt.business_type_id in (1002,1003,1004) and w.work_calendar_code is null " +
            "union all " +
            "SELECT " +
            "wd.sub_work_code,wd.title,wd.work_type_id,wd.relation_type, " +
            "to_char( w.occur_time, 'yyyy-MM-dd' ) AS occur_day,   " +
            "to_char( w.occur_time, 'HH24:mi:ss' ) AS occur_time,   " +
            "to_char( w.deadline_time, 'yyyy-MM-dd HH24:mi:ss' ) AS deadline_time, " +
            "w.occur_time,wt.type_name as work_type_name,u.user_name,wd.relation_id, " +
            "ft.code_name,wdc2.field_value as note,3 as type " +
            "FROM ${schema_name}._sc_works w  " +
            "LEFT JOIN ${schema_name}._sc_works_detail wd on w.work_code = wd.work_code " +
            "LEFT JOIN ${schema_name}._sc_works_detail_column wdc on wd.sub_work_code = wdc.sub_work_code and wdc.field_code = 'fault_code' " +
            "LEFT JOIN ${schema_name}._sc_fault_code ft on wdc.field_value = ft.code " +
            "LEFT JOIN ${schema_name}._sc_works_detail_column wdc2 on wd.sub_work_code = wdc2.sub_work_code and wdc.field_code = 'fault_note' " +
            "LEFT JOIN ${schema_name}._sc_work_type AS wt ON wd.work_type_id = wt.ID   " +
            "LEFT JOIN ${schema_name}._sc_business_type AS bt ON wt.business_type_id = bt.business_type_id   " +
            "LEFT JOIN ${schema_name}._sc_user AS u ON wd.receive_user_id = u.ID   " +
            "WHERE 1=1  " +
            "and wd.relation_id = #{id}  " +
            "AND w.status < 60  " +
            "and bt.business_type_id = 1001  " +
            "and w.work_calendar_code is null " +
            ") t ORDER BY t.time")
    List<Map<String, Object>> findAssetWorkInfo(@Param("schema_name") String schemaName, @Param("id") String id);


    /**
     * 查询设备列表（导出用）
     *
     * @param schemaName  数据库
     * @param whereString 条件
     * @param userId      用户主键
     * @return 列表
     */
    @Select("SELECT t.id,t.asset_name,t.asset_code,t.asset_icon,t.remark,t.file_ids,t.create_time,t.maintain_begin_time,t.enable_time,t.buy_date,t.install_date,t.supplier_id," +
            "t.parent_id,t.properties,t.quantity,t.unit_id,t.create_user_id,t.category_id,t.importment_level_id,t.price,t.tax_rate,t.tax_price,t.currency_id,t.position_code,t.manufacturer_id," +
            "t.asset_model_id,t.install_price,t.use_year,t10.out_code,t10.in_code,t10.tem_value1,t10.tem_value2,t10.tem_value3,t10.tem_value4,t10.tem_value5,t10.tem_value9," +
            "t9.model_name AS model_name,t4.status,t4.status as assets_status,t6.category_code,t6.fields,t6.category_name AS category_name,p1.position_name, " +
            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t5.short_title ), ',' ) AS supplier_name, " +
            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t5_1.short_title ), ',' ) AS manufacturer_name, " +
//            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT t2.title ), ',' ) AS site_name, " +
//            "   ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT f.title ), ',' ) AS parent_site_name, " +
            "t.running_status_id,t11.running_status as running_status_name, t.guarantee_status_id, t13.level_name as importment_level_name, " +
            "t.asset_code AS qci_first, t.asset_name AS qci_second, t9.model_name as qci_fourth, " +
            "t10.iot_status, t10.use_add, t.buy_currency_id, t.install_currency_id, unt.unit_name, " +
            "cur.currency_name as asset_currency,cur_buy.currency_name AS buy_currency,cur_install.currency_name AS install_currency, " +
//            "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT p1.position_name ), ',' ) AS qci_third " +
            "ARRAY_TO_STRING( ARRAY_AGG ( DISTINCT p1.position_name ), ',' ) AS qci_third " +
            SqlConstant.ASSET_SEARCH_INFO_SQL +
            "GROUP BY t.id,t.asset_name,t.asset_code,t.asset_icon,t.remark,t.file_ids,t.create_time,t.maintain_begin_time,t.enable_time,t.buy_date,t.install_date,t.supplier_id,t.parent_id," +
            "t.properties,t.quantity,t.unit_id,t.create_user_id,t.category_id,t.importment_level_id,t.price,t.tax_rate,t.tax_price,t.currency_id,t.position_code,t.manufacturer_id,t.asset_model_id," +
            "t.install_price,t.use_year,t.ID,t10.iot_status, t10.use_add,t10.out_code,t10.in_code,t10.tem_value1,t10.tem_value2,t10.tem_value3,t10.tem_value4,t10.tem_value5,t10.tem_value9,t9.model_name,t4.status," +
            "t6.category_code,t6.category_name,t6.fields,p1.position_name,t.running_status_id,t11.running_status,t.guarantee_status_id,t13.level_name, t.buy_currency_id, " +
            "t.install_currency_id,cur.currency_name,cur_buy.currency_name,cur_install.currency_name, unt.unit_name " +
//            ",t2.title,f.title " +
            "ORDER BY t.category_id,t.asset_code,t.parent_id")
    List<Map<String, Object>> findExportAssetList(@Param("schema_name") String schemaName, @Param("whereString") String whereString, @Param("userId") String userId);

    /**
     * 设备列表总数统计
     *
     * @param schema_name 数据库
     * @param whereString 条件
     * @param userId      用户主键
     * @return 统计数量
     */
    @Select("SELECT count(1) FROM ${schema_name}._sc_asset t1" + SqlConstant.ASSET_DATA_PRM_SQL + " ${whereString} ")
    int countAssetList(@Param("schema_name") String schema_name, @Param("whereString") String whereString, @Param("userId") String userId);

    /**
     * 根据设备编码查询有效设备数量
     *
     * @param schema_name 数据库
     * @param asset_code  设备编码
     * @return 统计数量
     */
    @Select("SELECT count(1) FROM ${schema_name}._sc_asset where status > " + StatusConstant.STATUS_DELETEED + " and asset_code = #{asset_code} ")
    int countAssetById(@Param("schema_name") String schema_name, @Param("asset_code") String asset_code);

    /**
     * 根据设备编码、设备主键查询有效设备数量
     *
     * @param schema_name 数据库
     * @param asset_code  设备编码
     * @param id          设备主键
     * @return 统计数量
     */
    @Select("SELECT count(1) FROM ${schema_name}._sc_asset where status > " + StatusConstant.STATUS_DELETEED + " and asset_code = #{asset_code} and id <> #{id} ")
    int countAssetByInfo(@Param("schema_name") String schema_name, @Param("asset_code") String asset_code, @Param("id") String id);

    /**
     * 根据主键获取设备信息
     *
     * @param schema_name 数据库
     * @param id          设备主键
     * @return 设备信息
     */
    @Select(" select asset_name,asset_code,status,asset_icon AS asset_icon,remark,file_ids,properties::text,create_time," +
            " id,maintain_begin_time,enable_time,supplier_id::varchar,parent_id,quantity,unit_id,create_user_id,category_id," +
            " importment_level_id,price,tax_rate,tax_price,currency_id,position_code,buy_date,id as asset_id," +
            " install_date,manufacturer_id::varchar,asset_model_id::varchar,install_price,use_year,install_currency_id,install_currency_id AS install_currency," +
            " buy_currency_id,buy_currency_id AS buy_currency,guarantee_status_id::varchar,running_status_id from ${schema_name}._sc_asset " +
            " where id=#{id}")
    Map<String, Object> findAssetById(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 根据主键获取设备详细信息信息
     *
     * @param schema_name 数据库
     * @param id          设备主键
     * @return 设备信息
     */
    @Select(" select a.asset_name,a.parent_id,a.asset_code,a.status,a.asset_icon AS asset_icon,a.remark,a.file_ids,a.properties::text,a.create_time," +
            " a.id,a.maintain_begin_time,a.enable_time,a.supplier_id::varchar,a.parent_id,a.quantity,a.unit_id,a.create_user_id,a.category_id," +
            " a.importment_level_id,a.price,a.tax_rate,a.tax_price,a.currency_id,a.position_code,a.buy_date," +
            " a.install_date,a.manufacturer_id::varchar,a.asset_model_id::varchar,a.install_price,a.use_year,a.install_currency_id,a.install_currency_id AS install_currency," +
            " a.buy_currency_id,a.buy_currency_id AS buy_currency,a.guarantee_status_id::varchar,a.running_status_id," +
            " p.position_name,s.status as status_name,c.category_name,m.model_name,l.level_name,sf.short_title as supplier,mf.short_title as manufacturer,cu.currency_code " +
            " from ${schema_name}._sc_asset a" +
            " LEFT JOIN ${schema_name}._sc_asset_position p ON a.position_code = p.position_code " +
            " LEFT JOIN ${schema_name}._sc_asset_status s ON s.id = a.status " +
            " LEFT JOIN ${schema_name}._sc_asset_category c ON c.id = a.category_id " +
            " LEFT JOIN ${schema_name}._sc_asset_model m ON m.id = a.asset_model_id " +
            " LEFT JOIN ${schema_name}._sc_importment_level l ON l.id = a.asset_model_id " +
            " LEFT JOIN ${schema_name}._sc_facilities sf ON sf.id = a.supplier_id " +
            " LEFT JOIN ${schema_name}._sc_facilities mf ON mf.id = a.manufacturer_id " +
            " LEFT JOIN ${schema_name}._sc_currency cu ON cu.id = a.buy_currency_id " +
            " where a.id=#{id}")
    Map<String, Object> findAssetDetailById(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 获取设备附件文档信息
     *
     * @param schema_name 请求参数
     * @param file_ids    请求参数
     * @return 列表数据
     */
    @Select(" <script> " +
            " SELECT t.id,t.file_original_name,Round(t.file_size/1024,4) AS file_size,t.file_type_id,t.file_name,t.file_url,ft.type_name," +
            " to_char(t.create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time_str,fc.id AS file_category_id,fc.file_category_name AS file_category_name   " +
            " FROM ${schema_name}._sc_files t " +
            " LEFT JOIN ${schema_name}._sc_file_type  ft ON t.file_type_id = ft.id " +
            " LEFT JOIN ${schema_name}._sc_file_category  fc ON t.file_category_id = fc.id " +
            " WHERE t.id in (${file_ids}) " +
            " <choose>" +
            "    <when test='keywordSearch != null'> " +
            "       and t.file_original_name like CONCAT('%', #{keywordSearch}, '%')  " +
            "    </when> " +
            " </choose>" +
            " ORDER BY t.id desc " +
            " </script> ")
    List<Map<String, Object>> findAssetFileList(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids, @Param("keywordSearch") String keywordSearch);

    /**
     * 获取维保计划查询设备对象数量
     *
     * @param schemaName
     * @param planAssetWhereString
     * @return
     */
    @Select("select count(1) from ( " +
            "SELECT t1.id " +
            "FROM ${schema_name}._sc_asset t1 " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON t1.position_code = ap.position_code " +
            "WHERE t1.status>0 and t1.status <> " + StatusConstant.ASSET_INTSTATUS_DISCARD + "  ${condition} " +
            "GROUP BY t1.id,t1.asset_name,t1.asset_code,t1.status,t1.supplier_id,t1.create_time,t1.enable_time ) t0 ")
    int findAssetPlanCount(@Param("schema_name") String schemaName, @Param("condition") String planAssetWhereString);

    /**
     * 获取维保计划查询设备对象
     *
     * @param schemaName
     * @param planAssetWhereString
     * @return
     */
    @Select("select t0.id,t0.asset_name,t0.asset_code,t7.model_name,t0.status,t0.supplier_id,t0.create_time,t0.enable_time,t4.status AS status_name,  " +
            "t5.title AS supplier_name,t6.category_name AS category_name, t0.position_name  " +
            "from (  " +
            "SELECT t1.id,t1.asset_name,t1.asset_code,t1.status,t1.supplier_id,t1.create_time,t1.enable_time,t1.category_id,t1.asset_model_id,  " +
            "ARRAY_TO_STRING(ARRAY_AGG(DISTINCT ap.position_name ), ',' ) AS position_name   " +
            "FROM ${schema_name}._sc_asset t1   " +
            "LEFT JOIN ${schema_name}._sc_asset_position ap ON t1.position_code = ap.position_code   " +
            "WHERE t1.status>0 and t1.status <> " + StatusConstant.ASSET_INTSTATUS_DISCARD + " ${condition}   " +
            "GROUP BY t1.id,t1.asset_name,t1.asset_code,t1.status,t1.supplier_id,t1.create_time,t1.enable_time,t1.category_id,t1.asset_model_id  " +
            "order by t1.id asc limit #{page} offset #{begin}  " +
            ") t0  " +
            "LEFT JOIN ${schema_name}._sc_asset_status t4 on t0.status=t4.id  " +
            "LEFT JOIN ${schema_name}._sc_facilities t5 on t0.supplier_id=t5.id  " +
            "LEFT JOIN ${schema_name}._sc_asset_category t6 on t6.id = t0.category_id  " +
            "LEFT JOIN ${schema_name}._sc_asset_model t7 on t7.id = t0.asset_model_id  " +
            "ORDER BY t0.create_time desc ")
    List<Map<String, Object>> findAssetPlanList(@Param("schema_name") String schemaName, @Param("condition") String planAssetWhereString, @Param("page") int page, @Param("begin") int begin);

    /**
     * 根据设备编码获取设备主键
     *
     * @param schema_name 数据库
     * @param asset_code  设备主键
     * @return 设备信息
     */
    @Select("select id FROM ${schema_name}._sc_asset where asset_code = #{asset_code} and status > " + StatusConstant.STATUS_DELETEED + " limit 1")
    String findAssetIdByCode(@Param("schema_name") String schema_name, @Param("asset_code") String asset_code);

    /**
     * 根据设备编码获取有效设备主键
     *
     * @param schema_name 数据库
     * @param asset_code  设备主键
     * @return 设备信息
     */
    @Select("select id FROM ${schema_name}._sc_asset where asset_code = #{asset_code} and status > " + StatusConstant.STATUS_DELETEED + " limit 1")
    String findValidAssetIdByCode(@Param("schema_name") String schema_name, @Param("asset_code") String asset_code);

    /**
     * 获取符合筛选条件的设备列表（维保计划）
     *
     * @param schemaName
     * @param condition
     * @return
     */
    @Select("SELECT DISTINCT id,position_code FROM ${schemaName}._sc_asset a WHERE status = " + StatusConstant.ASSET_INTSTATUS_USING + " ${condition} ")
    List<Map<String, Object>> findAssetPlanByCondition(@Param("schemaName") String schemaName, @Param("condition") String condition);

    @Select("SELECT DISTINCT a.id, a.position_code " +
            "FROM ${schemaName}._sc_plan_work_asset pwa " +
            "LEFT JOIN ${schemaName}._sc_asset a ON pwa.asset_id = a.id " +
            "WHERE pwa.plan_code = #{planWorkCode}")
    List<Map<String, Object>> getAssetPlanByPlanWorkCode(@Param("schemaName") String schemaName, @Param("planWorkCode") String planWorkCode);

//    @Select("SELECT a.id,a.asset_code,a.asset_name,a.status,case when a.status < 0 then '已删除' else s.status end as status_name,a.running_status_id, " +
//            "ars.running_status as running_status_name,a.guarantee_status_id, " +
//            "coalesce((select(c.resource->>cd.name)::jsonb->>#{user_lang} from public.company_resource c where c.company_id = #{company_id}), cd.name) as result_type_name " +
//            "FROM ${schema_name}._sc_asset a " +
//            "LEFT JOIN ${schema_name}._sc_asset_running_status ars ON a.running_status_id = ars.id " +
//            "LEFT JOIN ${schema_name}._sc_asset_status s ON a.status = s.id " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON a.guarantee_status_id::varchar = cd.code AND cd.data_type = 'guarantee_status' " +
//            "WHERE a.id = #{id}")
//    Map<String, Object> findAssetInfoForAssetMap(@Param("schema_name") String schema_name, @Param("id") String id, @Param("user_lang") String user_lang, @Param("company_id") Long company_id);

    @Select("SELECT a.id,a.asset_code,a.asset_name,a.status,case when a.status < 0 then '已删除' else s.status end as status_name,a.running_status_id, " +
            "ars.running_status as running_status_name,a.guarantee_status_id, " +
            "cd.name as result_type_name " +
            "FROM ${schema_name}._sc_asset a " +
            "LEFT JOIN ${schema_name}._sc_asset_running_status ars ON a.running_status_id = ars.id " +
            "LEFT JOIN ${schema_name}._sc_asset_status s ON a.status = s.id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON a.guarantee_status_id::varchar = cd.code AND cd.data_type = 'guarantee_status' " +
            "WHERE a.id = #{id}")
    Map<String, Object> findAssetInfoForAssetMap(@Param("schema_name") String schema_name, @Param("id") String id);

    /**
     * 批量修改设备信息
     *
     * @param schemaName
     * @param assetlist
     */
    @Update("<script>" +
            "UPDATE ${schemaName}._sc_asset set " +
            " asset_name = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.asset_name} " +
            "   </foreach> " +
            " else asset_name end), " +
            " asset_code = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.asset_code} " +
            "   </foreach> " +
            " else asset_code end), " +
            " category_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.category_id} " +
            "   </foreach> " +
            " else category_id end), " +
            " asset_model_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.asset_model_id} " +
            "   </foreach> " +
            " else asset_model_id end), " +
            " position_code = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.position_code} " +
            "   </foreach> " +
            " else position_code end), " +
            " status = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.status} " +
            "   </foreach> " +
            " else status end), " +
            " importment_level_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.importment_level_id} " +
            "   </foreach> " +
            " else importment_level_id end), " +
            " enable_time = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.enable_time} " +
            "   </foreach> " +
            " else enable_time end), " +
            " use_year = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.use_year} " +
            "   </foreach> " +
            " else use_year end), " +
            " buy_date = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.buy_date} " +
            "   </foreach> " +
            " else buy_date end), " +
            " supplier_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.supplier_id} " +
            "   </foreach> " +
            " else supplier_id end), " +
            " manufacturer_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.manufacturer_id} " +
            "   </foreach> " +
            " else manufacturer_id end), " +
            " price = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.price} " +
            "   </foreach> " +
            " else price end), " +
            " buy_currency_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.buy_currency_id} " +
            "   </foreach> " +
            " else buy_currency_id end), " +
            " tax_rate = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.tax_rate} " +
            "   </foreach> " +
            " else tax_rate end), " +
            " install_date = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.install_date} " +
            "   </foreach> " +
            " else install_date end), " +
            " install_price = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.install_price} " +
            "   </foreach> " +
            " else install_price end), " +
            " install_currency_id = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.install_currency_id} " +
            "   </foreach> " +
            " else install_currency_id end), " +
            " tax_price = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.tax_price} " +
            "   </foreach> " +
            " else tax_price end), " +
            " properties = (case " +
            "   <foreach collection='assetList' item='a'> " +
            "   when id=#{a.id} then #{a.properties}::jsonb " +
            "   </foreach> " +
            " else properties end) " +
            "WHERE id in " +
            "   <foreach collection='assetList' item='a' separator=',' open='(' close=')'> " +
            "       #{a.id} " +
            "   </foreach> " +
            "</script>")
    void updateAssetBatch(@Param("schemaName") String schemaName, @Param("assetList") List<Map<String, Object>> assetlist);

    /**
     * 根据设备类型重置设备的自定义字段
     *
     * @param schemaName
     * @param category_id
     */
    @Update(" <script>" +
            " update ${schema_name}._sc_asset " +
            " set properties='[]'::jsonb " +
            " where category_id=#{category_id} " +
            " </script>")
    void updateCustomFieldsResetByCategoryId(@Param("schema_name") String schemaName, @Param("category_id") Integer category_id);

    /**
     * 删除指定设备类型的指定field_code的自定义字段
     *
     * @param schemaName
     * @param category_id
     * @param field_code
     */
    @Update("UPDATE ${schema_name}._sc_asset i " +
            "SET properties = i2.properties " +
            "FROM (SELECT id,jsonb_agg(elems) as properties " +
            "FROM ${schema_name}._sc_asset,jsonb_array_elements(properties::jsonb) as elems " +
            "WHERE category_id = #{category_id} AND properties is not null and elems ->> 'field_code' != #{field_code} " +
            "GROUP BY id) i2 " +
            "WHERE i2.id = i.id " +
            "and i.category_id = #{category_id} AND i.properties is not null ")
    void updateCustomFieldsByCategoryIdAndFieldCode(@Param("schema_name") String schemaName, @Param("category_id") Integer category_id, @Param("field_code") String field_code);

    /**
     * 添加设备指定设备类型的自定义字段
     *
     * @param schemaName
     * @param category_id
     * @param field
     */
    @Update("UPDATE ${schema_name}._sc_asset " +
            "SET properties = case when properties is null " +
            "then ('['||#{field}||']')::jsonb " +
            "else  properties || #{field}::jsonb " +
            "end " +
            "WHERE category_id = #{category_id} ")
    void updateCustomFieldsByCategoryIdAndField(@Param("schema_name") String schemaName, @Param("category_id") Integer category_id, @Param("field") String field);


//    /**
//     * 查询设备厂商列表
//     *
//     * @param schemaName
//     * @param dataId
//     * @param keywordSearch
//     * @param companyId
//     * @param langKey
//     * @return
//     */
//    @Select("<script> " +
//            "SELECT f.title,f.short_title,f.inner_code,f.address,f.id as org_id, " +
//            "case when cd.is_lang = '" + Constants.STATIC_LANG + "' then coalesce((select (c.resource->>cd.name)::jsonb->>#{langKey} from public.company_resource c where c.company_id = #{companyId}), cd.name) else cd.name end as org_type_name " +
//            "FROM ${schema_name}._sc_asset_organization ao " +
//            "JOIN ${schema_name}._sc_asset a ON ao.asset_id = a.id " +
//            "JOIN ${schema_name}._sc_facilities f ON ao.org_id=f.id " +
//            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
//            "WHERE a.id = #{id} " +
//            "<if test = \"condition!=null and condition != '' \"> " +
//            "AND( lower(f.short_title) like concat('%',lower(#{condition}), '%') or " +
//            "lower(f.title) like concat('%',lower(#{condition}), '%') or " +
//            "lower(f.inner_code) like concat('%',lower(#{condition}), '%')) " +
//            "</if> " +
//            "ORDER BY f.id </script>")
//    List<Map<String, Object>> findAssetFacilityList(@Param("schema_name") String schemaName, @Param("id") String dataId, @Param("condition") String keywordSearch, @Param("companyId") Long companyId, @Param("langKey") String langKey, @Param("condition") String condition);

    /**
     * 查询设备厂商列表
     *
     * @param schemaName
     * @param dataId
     * @param keywordSearch
     * @return
     */
    @Select("<script> " +
            "SELECT f.title,f.short_title,f.inner_code,f.address,f.id as org_id, " +
            "case when cd.is_lang = '" + Constants.STATIC_LANG + "' then cd.name else cd.name end as org_type_name " +
            "FROM ${schema_name}._sc_asset_organization ao " +
            "JOIN ${schema_name}._sc_asset a ON ao.asset_id = a.id " +
            "JOIN ${schema_name}._sc_facilities f ON ao.org_id=f.id " +
            "LEFT JOIN ${schema_name}._sc_cloud_data cd ON f.org_type_id::varchar = cd.code AND cd.data_type = 'org_type' " +
            "WHERE a.id = #{id} " +
            "<if test = \"condition!=null and condition != '' \"> " +
            "AND( lower(f.short_title) like concat('%',lower(#{condition}), '%') or " +
            "lower(f.title) like concat('%',lower(#{condition}), '%') or " +
            "lower(f.inner_code) like concat('%',lower(#{condition}), '%')) " +
            "</if> " +
            "ORDER BY f.id </script>")
    List<Map<String, Object>> findAssetFacilityList(@Param("schema_name") String schemaName, @Param("id") String dataId, @Param("condition") String keywordSearch, @Param("condition") String condition);

    /**
     * 删除设备厂商
     *
     * @param schemaName
     * @param dataId
     * @param org_id
     */
    @Delete("delete from ${schema_name}._sc_asset_organization ao where asset_id = #{id} and org_id =#{org_id}::int ")
    void deleteAssetFacility(@Param("schema_name") String schemaName, @Param("id") String dataId, @Param("org_id") String org_id);

    /**
     * 根据设备主键删除设备厂商
     *
     * @param schemaName 数据库
     * @param dataId     设备主键
     */
    @Delete("delete from ${schema_name}._sc_asset_organization ao where asset_id = #{id}")
    void deleteAssetOrg(@Param("schema_name") String schemaName, @Param("id") String dataId);

    /**
     * 添加设备厂商
     *
     * @param schemaName
     * @param assetId
     * @param org_ids
     */
    @Insert("<script> " +
            "INSERT INTO ${schema_name}._sc_asset_organization ( asset_id, org_id ) " +
            "VALUES " +
            "<foreach collection='org_ids' item='p' separator=','> " +
            "(#{asset_id},#{p}::int) " +
            "</foreach> " +
            "</script> ")
    void insertAssetOrg(@Param("schema_name") String schemaName, @Param("asset_id") String assetId, @Param("org_ids") String[] org_ids);

    /**
     * 根据org_id查询厂商简称
     *
     * @param schemaName 企业
     * @param org_id     厂商id
     * @return 厂商简称
     */
    @Select("select f.short_title " +
            "from ${schema_name}._sc_facilities AS f " +
            "where f.id = #{org_id}"
    )
    String findFacilitiesTitleById(@Param("schema_name") String schemaName, @Param("org_id") Integer org_id);

    /**
     * 获取设备关联人员列表
     *
     * @param schemaName
     * @param assetId
     * @param keywordSearch
     * @param user_id
     * @return
     */
    @Select("<script> " +
            "select dm.*,u.user_name,u.mobile,u.email,wt.type_name as duty_name " +
            "from ${schema_name}._sc_asset_duty_man dm " +
            "left join ${schema_name}._sc_work_type wt on dm.duty_id=wt.id " +
            "left join ${schema_name}._sc_user u on dm.user_id=u.id " +
            "where dm.asset_id=#{id} " +
            "AND wt.id in (select DISTINCT wt.work_type_id " +
            "from ${schema_name}._sc_user_position up " +
            "INNER JOIN ${schema_name}._sc_position_role pr on up.position_id = pr.position_id " +
            "INNER JOIN ${schema_name}._sc_role ur on ur.id = pr.role_id " +
            "INNER JOIN ${schema_name}._sc_role_work_type wt ON ur.id=wt.role_id " +
            "where up.user_id = #{user_id}) " +
            "<if test = \"condition!=null and condition != '' \"> " +
            " AND( lower(u.user_name) like concat('%',lower(#{condition}), '%') or " +
            " lower(u.account) like concat('%',lower(#{condition}), '%')) " +
            "</if> " +
            "order by dm.id " +
            "</script> ")
    List<Map<String, Object>> findAssetDutyManList(@Param("schema_name") String schemaName, @Param("id") String assetId, @Param("condition") String keywordSearch, @Param("user_id") String user_id);

    /**
     * 新增设备关联人员
     *
     * @param data
     */
    @Insert("INSERT INTO ${schema_name}._sc_asset_duty_man(duty_id, is_use, create_user_id, create_time, remark, asset_id, user_id) " +
            "VALUES ( #{duty_id}::int, #{is_use}, #{create_user_id}, #{create_time}, #{remark}, #{asset_id}, #{user_id})")
    void addAssetDutyMan(Map<String, Object> data);

    /**
     * 设备关联人员删除
     *
     * @param schemaName
     * @param asset_id
     * @param duty_man_id
     */
    @Delete("delete from ${schema_name}._sc_asset_duty_man where asset_id = #{asset_id} and id =#{duty_man_id}::int ")
    void deleteAssetDutyMan(@Param("schema_name") String schemaName, @Param("asset_id") String asset_id, @Param("duty_man_id") String duty_man_id);

    /**
     * 获得设备的备件列表
     *
     * @param schemaName
     * @param assetId
     * @param keywordSearch
     * @param userId
     * @return
     */
    @Select("<script> " +
            "SELECT ab.id,b.bom_name,ab.bom_code,ab.material_code,ab.bom_model,bt.type_name,ab.use_count,u.unit_name,b.type_id,ab.bom_id, " +
            "coalesce((SELECT string_agg(f.title,',') FROM ${schema_name}._sc_bom_supplies bs LEFT JOIN ${schema_name}._sc_facilities f ON bs.supplier_id = f.ID WHERE bs.bom_id = b.ID),'') AS supplier_name " +
            "FROM ${schema_name}._sc_asset_bom ab " +
            "LEFT JOIN ${schema_name}._sc_bom b ON ab.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_bom_type bt ON b.type_id = bt.id " +
            "LEFT JOIN ${schema_name}._sc_unit u on ab.unit_type = u.id " +
            "WHERE ab.asset_id = #{asset_id} " +
            "<if test = \"condition!=null and condition != '' \"> " +
            "AND (lower(b.bom_name) like concat('%',lower(#{condition}),'%') or lower(ab.bom_model) like concat('%',lower(#{condition}),'%')) " +
            "</if> " +
            "ORDER BY ab.id asc " +
            "</script> ")
    List<Map<String, Object>> findAssetBomList(@Param("schema_name") String schemaName, @Param("asset_id") String assetId, @Param("condition") String keywordSearch, @Param("user_id") String userId);


    /**
     * 设备关联备件新增
     *
     * @param data
     */
    @Insert("INSERT INTO ${schema_name}._sc_asset_bom(create_user_id, create_time, asset_id, bom_model, bom_code, material_code, part_id, use_days, maintenance_days, unit_type, use_count,bom_id) " +
            "SELECT #{create_user_id}, #{create_time},#{asset_id},bom_model,#{bom_code},material_code,null,null,null,unit_id,#{use_count}::float,id " +
            "FROM ${schema_name}._sc_bom WHERE id = #{bom_id} ")
    void insertAssetBom(Map<String, Object> data);

    /**
     * 新增关联子设备（可批量）
     *
     * @param asset_id 设备主键
     */

    @Update("<script>" +
            "UPDATE ${schema_name}._sc_asset " +
            "SET parent_id = #{asset_id} " +
            "WHERE id in " +
            "   <foreach collection='select_id' item='id' separator=',' open='(' close=')'> " +
            "       #{id} " +
            "   </foreach> " +
            "</script>"
    )
    void insertAssetParent(@Param("schema_name") String schemaName, @Param("asset_id") String asset_id, @Param("select_id") String[] select_asset_id);

    /**
     * 查询新增子设备的设备名称
     *
     * @param schemaName      数据库
     * @param select_asset_id 选中设备id
     * @return asset_name 新增设备名称
     */
    @Select("<script>" +
            "SELECT STRING_AGG(asset_name,',')" +
            "FROM ${schema_name}._sc_asset " +
            "WHERE id in " +
            "<foreach collection='select_id' item='id' separator=',' open='(' close=')'> " +
            "       #{id} " +
            "</foreach> " +
            "</script>"
    )
    String selectAddAssetName(@Param("schema_name") String schemaName, @Param("select_id") String[] select_asset_id);

    /**
     * 删除关联子设备
     *
     * @param strId 选中删除关联设备的主键
     */
    @Update("UPDATE ${schema_name}._sc_asset SET parent_id = '' WHERE id = #{select_id}")
    void deleteAssetParent(@Param("schema_name") String schemaName, @Param("select_id") String strId);

    /**
     * 查询删除子设备的设备名称
     *
     * @param select_asset_id 当前选中设备id
     */
    @Select("SELECT asset_name FROM ${schema_name}._sc_asset WHERE id = #{select_id} ")
    String selectDeleteAssetName(@Param("schema_name") String schemaName, @Param("select_id") String select_asset_id);

    /**
     * 子设备列表信息查询
     *
     * @param asset_id 主设备id
     */
    @Select("<script> " +
            "SELECT sa.asset_code, sa.asset_name, position_name, category_name, model_name,sa.id " +
            "FROM ${schema_name}._sc_asset sa " +
            "LEFT JOIN ${schema_name}._sc_asset_position sap on sap.position_code = sa.position_code " +
            "LEFT JOIN ${schema_name}._sc_asset_category sac on sac.id = sa.category_id " +
            "LEFT JOIN ${schema_name}._sc_asset_model sam on sam.id = sa.asset_model_id " +
            "WHERE sa.parent_id = #{asset_id} " +
            "And sa.status =" + StatusConstant.STATUS_ENABLE +
            "<if test = \"keywordSearch!=null and keywordSearch != '' \"> " +
            "AND (lower(sa.asset_name) like concat('%',lower(#{keywordSearch}),'%') or lower(sa.asset_code) like concat('%',lower(#{keywordSearch}),'%')) " +
            "</if> " +
            "ORDER BY category_code, asset_model_id, asset_code " +
            "</script>"
    )
    List<Map<String, Object>> findAssetParentList(@Param("schema_name") String schemaName, @Param("asset_id") String asset_id, @Param("keywordSearch") String keywordSearch);

    /**
     * 设备关联备件删除
     *
     * @param schemaName
     * @param asset_id
     * @param asset_bom_id
     */
    @Delete("delete from ${schema_name}._sc_asset_bom where asset_id = #{asset_id} and id =#{id}::int ")
    void deleteAssetBom(@Param("schema_name") String schemaName, @Param("asset_id") String asset_id, @Param("id") String asset_bom_id);

    @Select("SELECT wd.work_code,wd.sub_work_code,wt.type_name as work_type_name,coalesce(wb.use_count,0) as use_count, " +
            "u.unit_name,to_char(wd.begin_time, 'yyyy-MM-dd HH24:mi:ss' ) AS begin_time," +
            "COALESCE (wb.use_count,0) * COALESCE (wb.price,0) as show_price,wb.price,wb.bom_id,wb.bom_code " +
            "FROM ${schema_name}._sc_work_bom wb " +
            "JOIN ${schema_name}._sc_works_detail wd on wb.sub_work_code = wd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on wd.work_type_id = wt.id " +
            "LEFT JOIN ${schema_name}._sc_bom b on wb.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_unit u on b.unit_id = u.id " +
            "WHERE b.id = #{bom_id} and wb.stock_id in ( " +
            "SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id} " +
            ") " +
            "ORDER BY wd.sub_work_code desc ${pagination} ")
    List<Map<String, Object>> findBomUsageRecordList(@Param("schema_name") String schemaName, @Param("bom_id") String bom_id, @Param("user_id") String user_id, @Param("pagination") String pagination);

    @Select("select count(1) from (SELECT wd.work_code,wd.sub_work_code,wt.type_name as work_type_name,coalesce(wb.use_count,0) as use_count, " +
            "u.unit_name,to_char(wd.begin_time, 'yyyy-MM-dd HH24:mi:ss' ) AS begin_time,b.show_price,wb.bom_id,wb.bom_code " +
            "FROM ${schema_name}._sc_work_bom wb " +
            "JOIN ${schema_name}._sc_works_detail wd on wb.sub_work_code = wd.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_work_type wt on wd.work_type_id = wt.id " +
            "LEFT JOIN ${schema_name}._sc_bom b on wb.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_unit u on b.unit_id = u.id " +
            "WHERE b.id = #{bom_id} and wb.stock_id in ( " +
            "SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id} " +
            ") ) t ")
    int findCountBomUsageRecordList(@Param("schema_name") String schemaName, @Param("bom_id") String bom_id, @Param("user_id") String userId);

    @Select("select count(1) from (SELECT bi.* " +
            "FROM ${schema_name}._sc_bom_import bi " +
            "LEFT JOIN ${schema_name}._sc_bom b on bi.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_stock s on bi.stock_id = s.id " +
            "WHERE bi.bom_id = #{bom_id} and (bi.stock_id in ( " +
            "SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id} " +
            ") or bi.stock_id = 0) ) t ")
    int findCountBomImportRecordList(@Param("schema_name") String schemaName, @Param("bom_id") String bom_id, @Param("user_id") String userId);

    @Select("SELECT bi.*,b.bom_name as cur_bom_name,s.stock_name as cur_stock_name " +
            "FROM ${schema_name}._sc_bom_import bi " +
            "LEFT JOIN ${schema_name}._sc_bom b on bi.bom_id = b.id " +
            "LEFT JOIN ${schema_name}._sc_stock s on bi.stock_id = s.id " +
            "WHERE bi.bom_id = #{bom_id} and (bi.stock_id in ( " +
            "SELECT rs.stock_id " +
            "FROM ${schema_name}._sc_role_stock rs " +
            "JOIN ${schema_name}._sc_role r ON rs.role_id = r.id " +
            "JOIN ${schema_name}._sc_position_role pr ON pr.role_id = r.id " +
            "JOIN ${schema_name}._sc_user_position up ON up.position_id = pr.position_id " +
            "WHERE up.user_id = #{user_id} " +
            ") or bi.stock_id = 0)" +
            " ORDER BY bi.create_time desc ${pagination}")
    List<Map<String, Object>> findBomImportRecordList(@Param("schema_name") String schemaName, @Param("bom_id") String bom_id, @Param("user_id") String userId, @Param("pagination") String pagination);

    /**
     * 添加设备厂商
     *
     * @param schemaName
     * @param assetId
     * @param org_ids
     */
    @Insert("<script> " +
            "INSERT INTO ${schema_name}._sc_asset_organization ( asset_id, org_id ) " +
            "VALUES " +
            "<foreach collection='org_ids' item='p' separator=','> " +
            "(#{asset_id},#{p}::int) " +
            "</foreach> " +
            "</script> ")
    void insertAssetOrgByList(@Param("schema_name") String schemaName, @Param("asset_id") String assetId, @Param("org_ids") List<String> org_ids);


    /**
     * 获取运行的设备运行状态的id
     *
     * @param schemaName
     * @return
     */
    @Select("SELECT id::varchar FROM ${schema_name}._sc_asset_running_status WHERE running_status like '运行%' ORDER BY data_order limit 1 ")
    String findAssetRunningStatusForRun(@Param("schema_name") String schemaName);


    /**
     * 批量添加设备厂商
     *
     * @param schemaName
     * @param list
     */
    @Insert("<script> " +
            "INSERT INTO ${schema_name}._sc_asset_organization ( asset_id, org_id ) " +
            "VALUES " +
            "<foreach collection='list' item='p' separator=','> " +
            "(#{p.asset_id},#{p.org_id}::int) " +
            "</foreach> " +
            "</script> ")
    void insertAssetOrgBatch(@Param("schema_name") String schemaName, @Param("list") List<Map<String, Object>> list);

    @Select(" SELECT * FROM ${schema_name}._sc_asset WHERE position_code = #{position_code}")
    List<Map<String, Object>> findAssetListByPositionCode(@Param("schema_name") String schemaName, @Param("position_code") String position_code);

    @Select(" SELECT * FROM ${schema_name}._sc_asset " +
            " WHERE asset_code = #{id} OR id = #{id} ORDER BY status ASC LIMIT 1 ")
    Map<String, Object> findAssetByIdOrCode(@Param("schema_name") String schemaName, @Param("id") String id);

    @Select("<script> SELECT " +
            "t1.work_code,t1.work_type_id,t1.relation_type,t1.relation_id,t2.fault_type_id, " +
            "t2.repair_code,t3.type_name,t4.code_name,a.asset_name,a.asset_code, " +
            "to_char(t2.finished_time, 'yyyy-MM-dd HH24:mi:ss') as finished_time " +
            "FROM ${schema_name}._sc_works AS t1  " +
            "LEFT JOIN ${schema_name}._sc_works_detail AS t2 ON t1.work_code = t2.work_code   " +
            "AND ( SELECT max ( finished_time ) FROM ${schema_name}._sc_works_detail   " +
            "WHERE work_code = t1.work_code ) = t2.finished_time  " +
            "LEFT JOIN ${schema_name}._sc_fault_type AS t3 ON t2.fault_type_id = t3.ID   " +
            "LEFT JOIN ${schema_name}._sc_fault_code AS t4 ON t2.repair_code = t4.code   " +
            "LEFT JOIN ${schema_name}._sc_work_type AS t5 ON t1.work_type_id = t5.id   " +
            "LEFT JOIN ${schema_name}._sc_business_type AS t6 ON t5.business_type_id = t6.business_type_id " +
            "LEFT JOIN ${schema_name}._sc_asset a on t1.relation_id = a.id " +
            "WHERE t1.relation_type = 2 AND t6.business_type_id = 1001 AND t1.status = 60 " +
            "AND t1.relation_id in " +
            "<foreach collection='ids' item='id' open='(' close=')' separator=','> " +
            "#{id} " +
            "</foreach> " +
            "</script>")
    List<Map<String, Object>> findFaultRecordListByAssetIds(@Param("schema_name") String schemaName, @Param("ids") List<String> idList);

    @Update(" UPDATE ${schema_name}._sc_asset" +
            " set location=#{location}::point" +
            " WHERE id = #{id}  ")
    void updateAssetMapLocation(@Param("schema_name") String schemaName, @Param("id") String id, @Param("location") String location);

    @Select(" <script>" +
            " select * from ${schema_name}._sc_asset where id=#{id} " +
            " <if test=\"running_status_id != null\"> " +
            " and running_status_id = #{running_status_id}" +
            " </if>" +
            " </script>")
    Map<String, Object> findAssetByCondition(@Param("schema_name") String schemaName, @Param("id") String id, @Param("running_status_id") Integer runningStatusId);

    @Select(" UPDATE ${schema_name}._sc_asset " +
            " set running_status_id = #{running_status_id} where id=#{id} ")
    void updateAssetByStatusID(@Param("schema_name") String schemaName, @Param("id") String id, @Param("running_status_id") Integer runningStatusId);

    @Select(" <script> " +
            " select ap.code,ap.value from (SELECT " +
            " jsonb_array_elements_text ( properties ) :: jsonb ->> 'field_code' AS code, " +
            " jsonb_array_elements_text ( properties ) :: jsonb ->> 'field_value' AS value " +
            " FROM ${schema_name}._sc_asset WHERE id = #{id}) ap where ap.code = 'basic2' " +
            " <if test = \"value != null and value != ''\"> " +
            " and ap.value = #{value}" +
            " </if>" +
            " </script>")
    Map<String, Object> findAssetSituation(@Param("schema_name") String schemaName, @Param("id") String id, @Param("value") String value);

    @Update(" with _sc_asset_p as (" +
            " select('{'||index-1||',field_value}')::text[] as path " +
            " from ${schema_name}._sc_asset ,jsonb_array_elements(properties) with ordinality arr(pp,index) where pp ->> 'field_code' = 'basic2' and id = #{id} ) " +
            " update ${schema_name}._sc_asset set properties = jsonb_set(properties, _sc_asset_p.path, #{value}::jsonb, false) " +
            " from _sc_asset_p " +
            " where id = #{id}")
    int updateAssetByProperties(@Param("schema_name") String schemaName, @Param("id") String id, @Param("value") String value);

}
