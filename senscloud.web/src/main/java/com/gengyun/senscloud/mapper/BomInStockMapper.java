package com.gengyun.senscloud.mapper;

/**
 * 备件入库
 */
public interface BomInStockMapper {
//
//    /**
//     * 备件入库列表总数
//     *
//     * @param schema_name
//     * @param condition
//     * @return
//     */
//    @Select("SELECT count(1) FROM (" +
//            "SELECT t.*,st.stock_name,ARRAY_TO_STRING(ARRAY_AGG(f.short_title), ',') AS facility_name," +
//            "ARRAY_TO_STRING(ARRAY_AGG(DISTINCT g.group_name), ',') AS group_name," +
//            "u.username " +
//            "FROM " +
//            "( " +
//            "SELECT bis.in_code,bis.stock_code,SUM(bisd.quantity) AS quantity,bis.create_user_account,bis.create_time,bis.status " +
//            " FROM ${schema_name}._sc_bom_in_stock bis " +
//            " LEFT JOIN ${schema_name}._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code " +
//            " WHERE 1=1 ${condition} " +
//            " GROUP BY bis.in_code,bis.stock_code,bis.create_user_account,bis.create_time,bis.status " +
//            ") t " +
//            "LEFT JOIN ${schema_name}._sc_stock st ON st.stock_code = t.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org ON t.stock_code = org.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg ON sg.stock_code = st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON org.facility_id = f.id " +
//            "LEFT JOIN ${schema_name}._sc_group g ON sg.group_id = g.ID " +
//            "LEFT JOIN ${schema_name}._sc_user u ON t.create_user_account = u.account " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            "GROUP BY t.in_code,t.stock_code,t.create_user_account,t.create_time,t.status,t.quantity,st.stock_name,u.username " +
//            ") tb")
//    int countBomInStockList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition);
//
//    /**
//     * 查找备件入库列表
//     *
//     * @param schema_name
//     * @param condition
//     * @param pageSize
//     * @param begin
//     * @return
//     */
//    @Select("SELECT t.*,st.stock_name,ARRAY_TO_STRING(ARRAY_AGG(DISTINCT f.short_title), ',') AS facility_name," +
//            "ARRAY_TO_STRING(ARRAY_AGG(DISTINCT g.group_name), ',') AS group_name," +
//            "u.username " +
//            "FROM " +
//            "( " +
//            "SELECT bis.in_code,bis.stock_code,SUM(bisd.quantity) AS quantity,bis.create_user_account,bis.create_time,bis.status " +
//            " FROM ${schema_name}._sc_bom_in_stock bis " +
//            " LEFT JOIN ${schema_name}._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code " +
//            " WHERE 1=1 ${condition} " +
//            " GROUP BY bis.in_code,bis.stock_code,bis.create_user_account,bis.create_time,bis.status " +
//            ") t " +
//            "LEFT JOIN ${schema_name}._sc_stock st ON st.stock_code = t.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_org org ON t.stock_code = org.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_stock_group sg ON sg.stock_code = st.stock_code " +
//            "LEFT JOIN ${schema_name}._sc_group_org gorg ON gorg.group_id = sg.group_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON org.facility_id = f.ID OR gorg.org_id = f.ID " +
//            "LEFT JOIN ${schema_name}._sc_group g ON sg.group_id = g.ID " +
//            "LEFT JOIN ${schema_name}._sc_user u ON t.create_user_account = u.account " +
//            "WHERE 1=1 ${stockPermissionCondition} " +
//            "GROUP BY t.in_code,t.stock_code,t.create_user_account,t.create_time,t.status,t.quantity,st.stock_name,u.username " +
//            "ORDER BY t.create_time DESC,t.in_code desc limit ${page} offset ${begin} ")
//    List<Map<String, Object>> getBomInStockList(@Param("schema_name") String schema_name, @Param("condition") String condition, @Param("stockPermissionCondition") String stockPermissionCondition, @Param("page") int pageSize, @Param("begin") int begin);
//
//    /**
//     * 根据主键查询备件入库数据
//     *
//     * @param schemaName
//     * @param inCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_in_stock w where w.in_code = #{inCode} ")
//    Map<String, Object> queryBomInStockByInCode(@Param("schema_name") String schemaName, @Param("inCode") String inCode);
//
//    /**
//     * 根据入库表主键删除数据
//     *
//     * @param schemaName
//     * @param subWorkCode
//     * @return
//     */
//    @Delete("delete from ${schema_name}.${tableName} where in_code = #{subWorkCode} ")
//    void deleteBySubWorkCode(@Param("schema_name") String schemaName, @Param("tableName") String tableName, @Param("subWorkCode") String subWorkCode);
//
//    @Update("update ${schema_name}._sc_bom_in_stock set status = #{status} where in_code=#{in_code}")
//    public int updateStatus(@Param("schema_name") String schema_name ,@Param("status") int status ,@Param("in_code") String in_code);
//
//    @Update("update ${schema_name}._sc_bom_in_stock set status = #{status},body_property=#{body_property}::jsonb where in_code=#{in_code}")
//    public int updateStatusAndBodyProperty(@Param("schema_name") String schema_name ,@Param("status") int status, @Param("body_property") String body_property, @Param("in_code") String in_code);
//
//    /**
//     * 更新接受附件id
//     * @param schema_name
//     * @param file_ids
//     * @param in_code
//     * @return
//     */
//    @Update("update ${schema_name}._sc_bom_in_stock set file_ids=#{file_ids},status = #{status},body_property=#{body_property}::jsonb where in_code = #{in_code}")
//    public int updateBomInStockStatus(@Param("schema_name") String schema_name, @Param("file_ids") String file_ids, @Param("status") int status, @Param("body_property") String body_property, @Param("in_code") String in_code);
//
//    /**
//     * 根据主键查询备件入库详情数据
//     *
//     * @param schemaName
//     * @param inCode
//     * @return
//     */
//    @Select("select * from ${schema_name}._sc_bom_in_stock_detail w where w.in_code = #{inCode} ")
//    List<Map<String, Object>> queryBomInStockDetailById(@Param("schema_name") String schemaName, @Param("inCode") String inCode);
//
//    /**
//     * 修改备件对外价格及单位
//     *
//     * @param schema_name
//     * @param bomStockData
//     * @return
//     */
//    @Update("UPDATE ${schema_name}._sc_bom_stock " +
//            "SET show_price=#{bs.show_price}::numeric," +
//            "currency_id=#{bs.currency_id}::int " +
//            "WHERE bom_code=#{bs.bom_code} AND material_code=#{bs.material_code} AND stock_code=#{bs.stock_code} ")
//    int synchronizePrice(@Param("schema_name") String schema_name, @Param("bs") Map<String,Object> bomStockData);
}
