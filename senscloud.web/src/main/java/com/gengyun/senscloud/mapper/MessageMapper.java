package com.gengyun.senscloud.mapper;

import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * 短信处理
 */
@Mapper
public interface MessageMapper {
    /**
     * 查询消息配置信息
     *
     * @param schemaName 数据库
     * @param typeId     配置类别
     * @return 配置信息
     */
    @Select("select * from ${schema_name}._sc_usual_config where id = #{id}")
    Map<String, String> findMsgConfig(@Param("schema_name") String schemaName, @Param("id") String typeId);

    /**
     * 查询微信token配置信息
     *
     * @param schemaName 数据库
     * @param id         配置主键
     * @return 微信token配置信息
     */
    @Select("select t.*, round(extract(epoch FROM (CURRENT_TIMESTAMP - reserve2 :: TIMESTAMP - reserve3 :: INTERVAL ))::numeric,0)::varchar as time_interval " +
            "from ${schema_name}._sc_usual_config t where id = #{id}")
    Map<String, String> findWxTokenConfig(@Param("schema_name") String schemaName, @Param("id") String id);

    /**
     * 更新微信token
     *
     * @param schemaName 数据库
     * @param wxAppToken wxAppToken
     * @param id         配置主键
     * @return 更新数量
     */
    @Update("update ${schema_name}._sc_usual_config set reserve1=#{wxAppToken}, reserve2=CURRENT_TIMESTAMP where id=#{id} ")
    int updateWxToken(@Param("schema_name") String schemaName, @Param("wxAppToken") String wxAppToken, @Param("id") String id);

    /**
     * 查询短信内容模板
     *
     * @param schemaName 数据库
     * @param msgId      消息主键
     * @return 短信内容
     */
    @Select("select id " +
            ",sms_pattern " +
            ",mail_pattern " +
            ",pattern_description " +
            ",remark " +
            ",sms_reserve1 " +
            ",sms_reserve2 " +
            ",wx_msg_pattern " +
            ",wx_msg_template_code " +
            ",wx_oa_msg_pattern " +
            ",wx_oa_msg_template_code " +
            ",log_pattern_key " +
            ",security_quantity_msg_interval::varchar  from ${schema_name}._sc_msg_content_config where id = #{id}")
    Map<String, String> findMsgContentTemp(@Param("schema_name") String schemaName, @Param("id") String msgId);

    @Insert("insert into ${schema_name}._sc_message_record(receive_user_id, msg_content, receive_mobile, send_time, business_type, business_no, send_user_id) " +
            "values (#{w.receive_user_id}, #{w.msg_content}, #{w.receive_mobile}, #{w.send_time},  #{w.business_type}, #{w.business_no}, #{w.send_user_id})")
    @Options(useGeneratedKeys = true, keyProperty = "w.id", keyColumn = "id")
    int insertMessage(@Param("schema_name") String schemaName, @Param("w") Map<String, Object> messageData);

//    /**
//     * 获取全局数据信息
//     *
//     * @param id 主键
//     * @return 全局数据信息
//     */
//    @Select("select * from public._sc_global_data t where id = #{id}")
//    Map<String, String> findGlobalInfo(@Param("id") String id);


//    /**
//     * 更新全局微信token
//     *
//     * @param wxAppToken wxAppToken
//     * @param id         配置主键
//     * @return 更新数量
//     */
//    @Update("update public._sc_global_data set reserve1 = #{wxAppToken} where id = #{id} ")
//    int updateGlobalWxToken(@Param("wxAppToken") String wxAppToken, @Param("id") String id);

//    /**
//     * 根据备用字段信息查询全局数据
//     *
//     * @param reserveName 字段名称
//     * @param value       字段值
//     * @return 全局数据
//     */
//    @Select("select * from public._sc_global_data t where ${reserveName} = #{value} limit 1")
//    Map<String, Object> findGlobalInfoByReserve(@Param("reserveName") String reserveName, @Param("value") String value);


    /**
     * 更新消息发送日志
     *
     * @param schemaName 入参
     * @param id         入参
     * @param isSuccess  入参
     * @param sendTime   入参
     */
    @Update("update ${schema_name}._sc_message_record set " +
            "send_time=#{sendTime}, is_success=#{isSuccess} " +
            "where id=#{id} ")
    int updateMessageData(@Param("schema_name") String schemaName, @Param("id") int id, @Param("isSuccess") String isSuccess, @Param("sendTime") Timestamp sendTime);


//    /**
//     * 根据条件查询全局数据
//     * @param weixinCode
//     * @return
//     */
//    @Select("select * from public._sc_global_data t where reserve3 = #{weixinCode} or reserve5 = #{weixinCode}")
//    List<Map<String, Object>> queryGlobalInfoListByWeixinCode(@Param("weixinCode") String weixinCode);
}
