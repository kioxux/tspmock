package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.Constants;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface MultipleLanguagesMapper {
//    /**
//     * 基础多语言计数
//     *
//     * @param keywordSearch 查询条件
//     * @return 数量
//     */
//    @Select(" <script> " +
//            " select count(1) from ( " +
//            "  select key from public.company_language where type is null " +
//            "   <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "      AND (value LIKE '%' || #{keywordSearch} || '%' or key LIKE '%' || #{keywordSearch} || '%'|| '%' or remark_en LIKE '%' || #{keywordSearch} || '%'|| '%' or remark_cn LIKE '%' || #{keywordSearch} || '%') " +
//            "   </if> " +
//            "  UNION " +
//            "  select key from public.company_language where company_id = " + Constants.DEFAULT_COMPANY + " and type is not null " +
//            "   <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "      AND (value LIKE '%' || #{keywordSearch} || '%' or key LIKE '%' || #{keywordSearch} || '%') " +
//            "   </if> " +
//            " ) a " +
//            " </script> ")
//    int countLangForBase(@Param("keywordSearch") String keywordSearch);
//
//    /**
//     * 查询多语言列表（基础）
//     *
//     * @param keywordSearch   关键字搜索
//     * @param companyIdSearch 企业主键
//     * @param pagination      分页
//     * @return 列表
//     */
//    @Select(" <script> " +
//            " select a.*, b.company_name as from_name from ( " +
//            "   select company_id, key, use_type, from_id, string_agg(type||':'||value,'" + Constants.SCD_SPLIT_TYPE + "') as value from ( " +
//            "     select company_id, key, use_type, from_id, 'en_US' as type, remark_en as value from public.company_language where type is null " +
//            "     UNION " +
//            "     select company_id, key, use_type, from_id, 'zh_CN' as type, remark_cn as value from public.company_language where type is null " +
//            "     UNION " +
//            "     select company_id, key, use_type, from_id, type, value from public.company_language where type is not null and company_id = " + Constants.DEFAULT_COMPANY +
//            "   ) t " +
//            "   GROUP BY company_id, key, use_type, from_id " +
//            " ) a LEFT JOIN public._sc_company b on a.from_id = b.id " +
//            " <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "    WHERE (value LIKE '%' || #{keywordSearch} || '%' or a.key LIKE '%' || #{keywordSearch} || '%') " +
//            " </if> " +
//            " ORDER BY a.key ${pagination} " +
//            " </script> ")
//    List<Map<String, Object>> findLanguageListForBase(@Param("keywordSearch") String keywordSearch, @Param("companyIdSearch") String companyIdSearch, @Param("pagination") String pagination);
//
//    /**
//     * 多语言计数
//     *
//     * @param keywordSearch   查询条件
//     * @param companyIdSearch 查询企业
//     * @return 数量
//     */
//    @Select(" <script> " +
//            " select count(1) from ( " +
//            "   select #{companyIdSearch} as company_id, key, use_type, string_agg(type||':'||value,'" + Constants.SCD_SPLIT_TYPE + "') as value from ( " +
//            "     select a.key, a.use_type, 'en_US' as type, a.remark_en as value from public.company_language a " +
//            "       inner join public.company_language b on a.key = b.key and b.company_id = #{companyIdSearch}::int " +
//            "     where a.type is null " +
//            "       and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'en_US') " +
//            "     UNION " +
//            "     select a.key, a.use_type, 'zh_CN' as type, a.remark_cn as value from public.company_language a " +
//            "       inner join public.company_language b on a.key = b.key and b.company_id = #{companyIdSearch}::int " +
//            "     where a.type is null " +
//            "       and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'zh_CN') " +
//            "     UNION " +
//            "     select a.key, a.use_type, a.type, a.value from public.company_language a " +
//            "       inner join public.company_language b on a.key = b.key and b.company_id = #{companyIdSearch}::int " +
//            "     where a.type is not null and a.company_id = " + Constants.DEFAULT_COMPANY +
//            "       and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = a.type) " +
//            "     UNION " +
//            "     select key, use_type, type, value from public.company_language where company_id = #{companyIdSearch}::int and type is not null " +
//            "   ) t " +
//            "   <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "      WHERE (value LIKE '%' || #{keywordSearch} || '%' or key LIKE '%' || #{keywordSearch} || '%') " +
//            "   </if> " +
//            "   GROUP BY key, use_type " +
//            " ) a " +
//            " </script> ")
//    int countLang(@Param("keywordSearch") String keywordSearch, @Param("companyIdSearch") String companyIdSearch);
//
//    /**
//     * 查询多语言列表
//     *
//     * @param keywordSearch   关键字搜索
//     * @param companyIdSearch 企业主键
//     * @param pagination      分页
//     * @return 列表
//     */
//    @Select(" <script> " +
//            " select * from ( select #{companyIdSearch} as company_id, key, use_type, string_agg(type||':'||value,'" + Constants.SCD_SPLIT_TYPE + "') as value from ( " +
//            "   select a.key, a.use_type, 'en_US' as type, a.remark_en as value from public.company_language a " +
//            "     inner join public.company_language b on a.key = b.key and b.company_id = #{companyIdSearch}::int " +
//            "   where a.type is null " +
//            "     and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'en_US') " +
//            "   UNION " +
//            "   select a.key, a.use_type, 'zh_CN' as type, a.remark_cn as value from public.company_language a " +
//            "     inner join public.company_language b on a.key = b.key and b.company_id = #{companyIdSearch}::int " +
//            "   where a.type is null " +
//            "     and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'zh_CN') " +
//            "   UNION " +
//            "   select a.key, a.use_type, a.type, a.value from public.company_language a " +
//            "     inner join public.company_language b on a.key = b.key and b.company_id = #{companyIdSearch}::int " +
//            "   where a.type is not null and a.company_id = " + Constants.DEFAULT_COMPANY +
//            "     and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = a.type) " +
//            "   UNION " +
//            "   select key, use_type, type, value from public.company_language where company_id = #{companyIdSearch}::int and type is not null " +
//            " ) t " +
//            " GROUP BY key, use_type ) t " +
//            " <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "    WHERE (value LIKE '%' || #{keywordSearch} || '%' or key LIKE '%' || #{keywordSearch} || '%') " +
//            " </if> " +
//            " ORDER BY key ${pagination} " +
//            " </script> ")
//    List<Map<String, Object>> findLanguageList(@Param("keywordSearch") String keywordSearch, @Param("companyIdSearch") String companyIdSearch, @Param("pagination") String pagination);
//
//    /**
//     * 多语言计数（企业）
//     *
//     * @param keywordSearch   查询条件
//     * @param companyIdSearch 查询企业
//     * @return 数量
//     */
//    @Select(" <script> " +
//            " select count(1) from ( " +
//            "   select key, use_type, string_agg(type||':'||value,'" + Constants.SCD_SPLIT_TYPE + "') as value from ( " +
//            "     select a.key, a.use_type, 'en_US' as type, a.remark_en as value from public.company_language a " +
//            "     where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'en_US') " +
//            "     UNION " +
//            "     select a.key, a.use_type, 'zh_CN' as type, a.remark_cn as value from public.company_language a " +
//            "     where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'zh_CN') " +
//            "     UNION " +
//            "     select a.key, a.use_type, a.type, a.value from public.company_language a where a.type is not null and a.company_id = " + Constants.DEFAULT_COMPANY +
//            "       and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = a.type) " +
//            "     UNION " +
//            "     select key, use_type, type, value from public.company_language where company_id = #{companyIdSearch}::int and type is not null " +
//            "   ) t " +
//            "   <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "      WHERE (value LIKE '%' || #{keywordSearch} || '%' or key LIKE '%' || #{keywordSearch} || '%') " +
//            "   </if> " +
//            "   GROUP BY key, use_type " +
//            " ) a " +
//            " </script> ")
//    int countLangForCompany(@Param("keywordSearch") String keywordSearch, @Param("companyIdSearch") String companyIdSearch);
//
//    /**
//     * 查询多语言列表（企业）
//     *
//     * @param keywordSearch   关键字搜索
//     * @param companyIdSearch 企业主键
//     * @param pagination      分页
//     * @return 列表
//     */
//    @Select(" <script> " +
//            " select * from ( select key, use_type, string_agg(type||':'||value,'" + Constants.SCD_SPLIT_TYPE + "') as value from ( " +
//            "   select a.key, a.use_type, 'en_US' as type, a.remark_en as value from public.company_language a " +
//            "   where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'en_US') " +
//            "   UNION " +
//            "   select a.key, a.use_type, 'zh_CN' as type, a.remark_cn as value from public.company_language a " +
//            "   where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'zh_CN') " +
//            "   UNION " +
//            "   select a.key, a.use_type, a.type, a.value from public.company_language a where a.type is not null and a.company_id = " + Constants.DEFAULT_COMPANY +
//            "     and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = a.type) " +
//            "   UNION " +
//            "   select key, use_type, type, value from public.company_language where company_id = #{companyIdSearch}::int and type is not null " +
//            " ) t " +
//            " GROUP BY key, use_type ) t " +
//            " <if test=\" null != keywordSearch and '' != keywordSearch \"> " +
//            "    WHERE (value LIKE '%' || #{keywordSearch} || '%' or key LIKE '%' || #{keywordSearch} || '%') " +
//            " </if> " +
//            " ORDER BY key ${pagination} " +
//            " </script> ")
//    List<Map<String, Object>> findLangListForCompany(@Param("keywordSearch") String keywordSearch, @Param("companyIdSearch") String companyIdSearch, @Param("pagination") String pagination);
//
//    /**
//     * 新增多语言
//     *
//     * @param data 数据Map集合
//     */
//    @Insert("INSERT INTO public.company_language(company_id, key, remark_cn, remark_en, type, value, use_type, from_id) " +
//            "VALUES( #{company_id}::int, #{key}, #{remark_cn}, #{remark_en}, #{type}, #{value}, #{use_type}, #{from_id}) ")
//    int insertLanguages(Map<String, Object> data);
//
//    /**
//     * 根据键值查询多语言信息（基础）
//     *
//     * @param key 键值
//     * @return 多语言信息
//     */
//    @Select("select * from public.company_language where type is null and key = #{key} ")
//    Map<String, Object> findBaseLangByKey(String key);
//
//    /**
//     * 更新多语言信息（基础）
//     *
//     * @param map 语言信息
//     * @return 成功数量
//     */
//    @Update("update public.company_language set remark_cn = #{remark_cn}, remark_en = #{remark_en} where key = #{key} ")
//    int updateBaseLang(Map<String, Object> map);
//
//    /**
//     * 根据键值、语种、企业计数
//     *
//     * @param map 语言信息
//     * @return 数量
//     */
//    @Select("select count(1) from public.company_language where company_id = #{company_id}::int and key = #{key} and type = #{type} ")
//    int existTypeCount(Map<String, Object> map);
//
//    /**
//     * 更新多语言信息
//     *
//     * @param map 语言信息
//     * @return 成功数量
//     */
//    @Update("update public.company_language set value = #{value} where company_id = #{company_id}::int and key = #{key} and type = #{type} ")
//    int updateCompanyLang(Map<String, Object> map);
//
//    /**
//     * 查询列表key
//     *
//     * @param use_type 用途描述
//     * @return 列表
//     */
//    @Select(" SELECT #{use_type} || lk.key as key from public.lang_key lk " +
//            " where lk.key not in (" +
//            "   select SUBSTRING(cl.key,LENGTH(cl.use_type)+1) from public.company_language cl where cl.key like #{use_type} ||'_%'" +
//            " ) limit 1 ")
//    String getNewKeyName(@Param("use_type") String use_type);
//
//    /**
//     * 按企业统计语言包
//     *
//     * @param companyIdSearch 企业主键
//     * @return 数量
//     */
//    @Select("select count(1) from public.company_resource where company_id = #{company_id}::int ")
//    int countCompanyResource(@Param("companyIdSearch") String companyIdSearch);
//
//    /**
//     * 发布多语言
//     *
//     * @param companyIdSearch 企业主键
//     * @return 列表
//     */
//    @Update(" update public.company_resource set resource = ( " +
//            " select '{' || string_agg ('\"' || KEY || '\": ' || value ::jsonb - 'key',',') || '}' AS info from ( " +
//            "   select KEY, '{' || string_agg ( '\"' || TYPE || '\":\"' || replace(VALUE,'\"', '\\\"') || '\"', ', ' ) || '}' as VALUE from ( " +
//            "     select a.key, 'en_US' as type, a.remark_en as value from public.company_language a " +
//            "     where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'en_US') " +
//            "     UNION " +
//            "     select a.key, 'zh_CN' as type, a.remark_cn as value from public.company_language a " +
//            "     where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'zh_CN') " +
//            "     UNION " +
//            "     select a.key, a.type, a.value from public.company_language a where a.type is not null and a.company_id = " + Constants.DEFAULT_COMPANY +
//            "       and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = a.type) " +
//            "     UNION " +
//            "     select key, type, value from public.company_language where company_id = #{companyIdSearch}::int and type is not null " +
//            "   ) t GROUP BY key " +
//            " ) t )::jsonb where company_id = #{companyIdSearch}::int ")
//    int deployLangByCompany(@Param("companyIdSearch") String companyIdSearch);
//
//    /**
//     * 发布多语言（新企业）
//     *
//     * @param companyIdSearch 企业主键
//     * @return 列表
//     */
//    @Insert(" insert into public.company_resource (company_id, resource) values (#{companyIdSearch}::int, " +
//            " select '{' || string_agg ('\"' || KEY || '\": ' || value ::jsonb - 'key',',') || '}' AS info from ( " +
//            "   select KEY, '{' || string_agg ( '\"' || TYPE || '\":\"' || replace(VALUE,'\"', '\\\"') || '\"', ', ' ) || '}' as VALUE from ( " +
//            "     select a.key, 'en_US' as type, a.remark_en as value from public.company_language a " +
//            "     where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'en_US') " +
//            "     UNION " +
//            "     select a.key, 'zh_CN' as type, a.remark_cn as value from public.company_language a " +
//            "     where a.type is null and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = 'zh_CN') " +
//            "     UNION " +
//            "     select a.key, a.type, a.value from public.company_language a where a.type is not null and a.company_id = " + Constants.DEFAULT_COMPANY +
//            "       and a.key not in (select distinct key from public.company_language where company_id = #{companyIdSearch}::int and type = a.type) " +
//            "     UNION " +
//            "     select key, type, value from public.company_language where company_id = #{companyIdSearch}::int and type is not null " +
//            "   ) t GROUP BY key " +
//            " ) t )::jsonb ")
//    int deployLangByNewCompany(@Param("companyIdSearch") String companyIdSearch);

}
