package com.gengyun.senscloud.mapper;

import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.AssetBomData;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/*
 * 集中处理设备信息控制器：设备的BOM、维修记录、性能指标等，后期设备相关的子信息，都移到这个地方来
 */

@Mapper
public interface AssetInfoMapper {

    /**
     * 验证备件编码是否存在
     *
     * @param schemaName
     * @param assetCodeList
     * @return
     */
    @Select("<script>select distinct asset_code, asset_name from ${schemaName}._sc_asset " +
            "where status >= "+ StatusConstant.ASSET_INTSTATUS_USING +" and asset_code in " +
            "<foreach collection='assetCodes' item='assetCode' open='(' close=')' separator=','> " +
            "#{assetCode} " +
            "</foreach></script>")
    @MapKey("asset_code")
    Map<String, Map<String, Object>> queryAssetCodeByCode(@Param("schemaName") String schemaName, @Param("assetCodes") List<String> assetCodeList);

    /**
     * 验证备件编码是否存在
     *
     * @param schemaName
     * @param assetNameList
     * @return
     */
    @Select("<script>select distinct asset_code, asset_name, id, position_code from ${schemaName}._sc_asset " +
            "where status >= "+ StatusConstant.ASSET_INTSTATUS_USING +" and asset_name in " +
            "<foreach collection='assetNames' item='assetName' open='(' close=')' separator=','> " +
            "#{assetName} " +
            "</foreach></script>")
    @MapKey("asset_name")
    Map<String, Map<String, Object>> queryAssetNameByName(@Param("schemaName") String schemaName, @Param("assetNames") List<String> assetNameList);
}
