package com.gengyun.senscloud.util.excel;

import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.excel.util.FileUtils;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExcelWriter {
    private static final Logger logger = LoggerFactory.getLogger(ExcelWriter.class);

    public static void writeExcel(String xml, Map<String, Object> map) {
        try {
            List<Map<String, Object>> imgList = DataChangeUtil.scdObjToList(map.get("pic_list"));
            if (null != imgList && imgList.size() > 0) {
                HSSFWorkbook wb = new HSSFWorkbook();
                File xf = new File(xml);
                writeData(wb, xf);
                Sheet sheet = wb.getSheetAt(0);
                if (null != sheet) {
                    // 画图的顶级管理器，一个sheet只能获取一个
                    Drawing patriarch = sheet.createDrawingPatriarch();
                    // 将图片写入到byteArray中
                    for (Map<String, Object> imgInfo : imgList) {
                        RegexUtil.optNotBlankStrOpt(imgInfo.get("xyz")).ifPresent(str -> {
                            String[] array = str.split(",");
                            try {
                                int a = Integer.valueOf(array[0]);
                                int b = Integer.valueOf(array[1]);
                                int c = Integer.valueOf(array[2]);
                                int d = Integer.valueOf(array[3]);
                                int e = Integer.valueOf(array[4]);
                                RegexUtil.optNotNullListStr(DataChangeUtil.scdObjToListStr(imgInfo.get("pathList"))).ifPresent(l -> {
                                    HSSFClientAnchor hssfClientAnchor;
                                    int i = 0;
                                    for (String p : l) {
                                        if (0 == e) {
                                            hssfClientAnchor = new HSSFClientAnchor(170 * i, 0, 170 * (i + 1), 255, (short) a, b, (short) a, b);
                                        } else {
                                            boolean isMore = i > 2;
                                            int col = isMore ? i - 3 : i;
                                            int row = isMore ? 1 : 0;
                                            hssfClientAnchor = new HSSFClientAnchor(0, 0, 0, 0, (short) (a + col), b + row, (short) (c + col), d + row);
                                        }
                                        // anchor存储图片的属性，包括在Excel中的位置、大小等信息
                                        hssfClientAnchor.setAnchorType(ClientAnchor.AnchorType.DONT_MOVE_AND_RESIZE);
                                        try (ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream()) {
                                            String imageType = StringUtils.substringAfterLast(p, "."); // 图片扩展名
                                            ImageIO.write(ImageIO.read(new File(p)), imageType, byteArrayOut);
                                            // 通过poi将图片写入到Excel中
                                            patriarch.createPicture(hssfClientAnchor, wb.addPicture(byteArrayOut.toByteArray(), Workbook.PICTURE_TYPE_JPEG));
                                            i++;
                                        } catch (Exception ex) {
                                            logger.error(ErrorConstant.EC_FILE_002 + xml, ex);
                                        }
                                    }
                                });
                            } catch (Exception e) {
                                logger.error(ErrorConstant.EC_FILE_004, e);
                            }
                        });
                    }
                }
                FileUtils.delete(xf);
                File file = new File(xml);
                try (FileOutputStream outputStream = new FileOutputStream(file)) {
                    wb.write(outputStream);
                } catch (Exception e) {
                    logger.error(ErrorConstant.EC_FILE_003 + xml, e);
                }
            }
        } catch (Exception e) {
            logger.error(ErrorConstant.EC_FILE_001 + xml, e);
        }
    }

    private static void writeData(Workbook wb, File xmlFile) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(xmlFile);
        Map<String, CellStyle> styleMap = ExcelXmlReader.readCellStyle(wb, document);
        List<ExcelWorksheet> excelWorksheets = ExcelXmlReader.readWorksheet(wb, document);
        for (ExcelWorksheet excelWorksheet : excelWorksheets) {
            Sheet sheet = wb.createSheet(excelWorksheet.getName());
            ExcelTable excelTable = excelWorksheet.getExcelTable();
            List<ExcelRow> excelRows = excelTable.getExcelRows();
            List<ExcelColumn> excelColumns = excelTable.getExcelColumns();
            // 填充列宽
            fillColumnWidth(sheet, excelColumns);
            List<ExcelCellRangeAddress> cellRangeAddresses = getCellRangeAddress(wb, styleMap, sheet, excelRows);
            // 添加合并单元格
            setCellRangeStyle(sheet, cellRangeAddresses);
        }
    }


    /**
     * description: 填充列宽
     *
     * @param sheet
     * @param excelColumns
     * @return void
     * @author Hlingoes 2021/4/27
     */
    private static void fillColumnWidth(Sheet sheet, List<ExcelColumn> excelColumns) {
        if (null == excelColumns) {
            return;
        }
        int columnIndex = 0;
        for (int i = 0; i < excelColumns.size(); i++) {
            ExcelColumn excelColumn = excelColumns.get(i);
            columnIndex = getCellWidthIndex(columnIndex, i, excelColumn.getIndex());
            sheet.setColumnWidth(columnIndex, (int) excelColumn.getWidth() * 50);
        }
    }

    private static int getCellWidthIndex(int columnIndex, int i, Integer index) {
        if (index != null) {
            columnIndex = index;
        }
        if (index == null && columnIndex != 0) {
            columnIndex = columnIndex + 1;
        }
        if (index == null && columnIndex == 0) {
            columnIndex = i;
        }
        return columnIndex;
    }


    /**
     * description: 添加合并单元格样式
     *
     * @param sheet
     * @param cellRangeAddresses
     * @return void
     * @author Hlingoes 2021/4/26
     */
    private static void setCellRangeStyle(Sheet sheet, List<ExcelCellRangeAddress> cellRangeAddresses) {
        if (CollectionUtils.isEmpty(cellRangeAddresses)) {
            return;
        }
        for (ExcelCellRangeAddress address : cellRangeAddresses) {
            CellRangeAddress cellRangeAddress = address.getCellRangeAddress();
            CellStyle cellStyle = address.getCellStyle();
            sheet.addMergedRegion(cellRangeAddress);
            RegionUtil.setBorderBottom(cellStyle.getBorderBottomEnum(), cellRangeAddress, sheet);
            RegionUtil.setBorderLeft(cellStyle.getBorderLeftEnum(), cellRangeAddress, sheet);
            RegionUtil.setBorderRight(cellStyle.getBorderRightEnum(), cellRangeAddress, sheet);
            RegionUtil.setBorderTop(cellStyle.getBorderTopEnum(), cellRangeAddress, sheet);
        }
    }


    private static List<ExcelCellRangeAddress> getCellRangeAddress(Workbook wb, Map<String, CellStyle> styleMap,
                                                                   Sheet sheet, List<ExcelRow> excelRows) {
        int createRowIndex = 0;
        List<ExcelCellRangeAddress> cellRangeAddresses = new ArrayList<>();
        for (int rowIndex = 0; rowIndex < excelRows.size(); rowIndex++) {
            ExcelRow excelRowInfo = excelRows.get(rowIndex);
            if (excelRowInfo == null) {
                continue;
            }
            createRowIndex = getIndex(createRowIndex, rowIndex, excelRowInfo.getIndex());
            Row row = sheet.createRow(createRowIndex);
            if (excelRowInfo.getHeight() != null) {
                Integer height = excelRowInfo.getHeight() * 20;
                row.setHeight(height.shortValue());
            }
            List<ExcelCell> excelCells = excelRowInfo.getExcelCells();
            if (CollectionUtils.isEmpty(excelCells)) {
                continue;
            }
            int startIndex = 0;
            for (int cellIndex = 0; cellIndex < excelCells.size(); cellIndex++) {
                ExcelCell excelCellInfo = excelCells.get(cellIndex);
                if (excelCellInfo == null) {
                    continue;
                }
                // 获取起始列
                startIndex = getIndex(startIndex, cellIndex, excelCellInfo.getIndex());
                Cell cell = row.createCell(startIndex);
                String styleId = excelCellInfo.getStyleID();
                CellStyle cellStyle = styleMap.get(styleId);
                setCellValue(excelCellInfo.getExcelData(), cell);
                cell.setCellStyle(cellStyle);
                // 单元格注释`
                setCellComment(sheet, excelCellInfo.getExcelComment(), cell);
                // 合并单元格
                startIndex = addCellRanges(createRowIndex, startIndex, cellRangeAddresses, excelCellInfo, cellStyle);
            }
        }
        return cellRangeAddresses;
    }

    private static int getIndex(int columnIndex, int i, Integer index) {
        if (index != null) {
            columnIndex = index - 1;
        }
        if (index == null && columnIndex != 0) {
            columnIndex = columnIndex + 1;
        }
        if (index == null && columnIndex == 0) {
            columnIndex = i;
        }
        return columnIndex;
    }


    private static void setCellComment(Sheet sheet, ExcelComment excelComment, Cell cell) {
        if (null == excelComment) {
            return;
        }
        ExcelData excelData = excelComment.getExcelData();
        Comment comment = null;
        if (sheet instanceof XSSFSheet) {
            comment = sheet.createDrawingPatriarch()
                    .createCellComment(new XSSFClientAnchor(0, 0, 0, 0, (short) 3, 3, (short) 5, 6));
            comment.setString(new XSSFRichTextString(excelData.getText()));
        } else {
            comment = sheet.createDrawingPatriarch()
                    .createCellComment(new HSSFClientAnchor(0, 0, 0, 0, (short) 3, 3, (short) 5, 6));
            comment.setString(new HSSFRichTextString(excelData.getText()));
        }
        cell.setCellComment(comment);
    }


    /**
     * description: 添加单元格合并
     *
     * @param createRowIndex
     * @param startIndex
     * @param cellRanges
     * @param excelCellInfo
     * @return int
     * @author Hlingoes 2021/5/23
     */
    private static int addCellRanges(int createRowIndex, int startIndex, List<ExcelCellRangeAddress> cellRanges, ExcelCell excelCellInfo, CellStyle cellStyle) {
        Integer mergeAcrossCount = excelCellInfo.getMergeAcross();
        Integer mergeDownCount = excelCellInfo.getMergeDown();
        if (mergeAcrossCount != null || mergeDownCount != null) {
            CellRangeAddress cellRangeAddress = null;
            if (mergeAcrossCount != null && mergeDownCount != null) {
                int mergeAcross = startIndex;
                if (mergeAcrossCount != 0) {
                    // 获取该单元格结束列数
                    mergeAcross += mergeAcrossCount;
                }
                int mergeDown = createRowIndex;
                if (mergeDownCount != 0) {
                    // 获取该单元格结束列数
                    mergeDown += mergeDownCount;
                }
                cellRangeAddress = new CellRangeAddress(createRowIndex, mergeDown, (short) startIndex,
                        (short) mergeAcross);
            } else if (mergeAcrossCount != null && mergeDownCount == null) {
                int mergeAcross = startIndex;
                if (mergeAcrossCount != 0) {
                    // 获取该单元格结束列数
                    mergeAcross += mergeAcrossCount;
                    // 合并单元格
                    cellRangeAddress = new CellRangeAddress(createRowIndex, createRowIndex, (short) startIndex,
                            (short) mergeAcross);
                }

            } else if (mergeDownCount != null && mergeAcrossCount == null) {
                int mergeDown = createRowIndex;
                if (mergeDownCount != 0) {
                    // 获取该单元格结束列数
                    mergeDown += mergeDownCount;
                    // 合并单元格
                    cellRangeAddress = new CellRangeAddress(createRowIndex, mergeDown, (short) startIndex,
                            (short) startIndex);
                }
            }
            if (mergeAcrossCount != null) {
                int length = mergeAcrossCount.intValue();
                //for (int i = 0; i < length; i++) {
                    startIndex += mergeAcrossCount;

               // }
            }
            cellRanges.add(new ExcelCellRangeAddress(cellRangeAddress, cellStyle));
        }
        return startIndex;
    }

    /**
     * 设置文本值内容
     *
     * @param excelData:
     * @param cell:
     * @return void
     */
    private static void setCellValue(ExcelData excelData, Cell cell) {
        if (null == excelData) {
            return;
        }
        if (null != excelData.getType() && "Number".equals(excelData.getType())) {
            cell.setCellType(CellType.NUMERIC);
        }
        if (excelData.getRichTextString() != null) {
            cell.setCellValue(excelData.getRichTextString());
        } else if (null != excelData.getText()) {
            if ("Number".equals(excelData.getType())) {
                cell.setCellValue(Float.parseFloat(excelData.getText().replaceAll(",", "")));
            } else {
                cell.setCellValue(excelData.getText());
            }
        }
    }

}
