package com.gengyun.senscloud.util;

//import com.fitit100.util.RegexUtil;
//import org.apache.commons.codec.binary.Hex;
//
//import java.security.MessageDigest;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Random;
//
///**
// * Title: 字符串工具类
// *
// * @author 穆书伟
// * @created 2017年10月10日 下午17:11:56
// */
public class StringUtil {
//    /**
//     * 生成含有随机盐的密码
//     */
//    public static String generate(String password) {
//        Random r = new Random();
//        StringBuilder sb = new StringBuilder(16);
//        sb.append(r.nextInt(99999999)).append(r.nextInt(99999999));
//        int len = sb.length();
//        if (len < 16) {
//            for (int i = 0; i < 16 - len; i++) {
//                sb.append("0");
//            }
//        }
//        String salt = sb.toString();
//        password = md5Hex(password + salt);
//        char[] cs = new char[48];
//        for (int i = 0; i < 48; i += 3) {
//            cs[i] = password.charAt(i / 3 * 2);
//            char c = salt.charAt(i / 3);
//            cs[i + 1] = c;
//            cs[i + 2] = password.charAt(i / 3 * 2 + 1);
//        }
//        return new String(cs);
//    }
//
//    /**
//     * 校验密码是否正确
//     */
//    public static boolean verify(String password, String md5) {
//        char[] cs1 = new char[32];
//        char[] cs2 = new char[16];
//        for (int i = 0; i < 48; i += 3) {
//            cs1[i / 3 * 2] = md5.charAt(i);
//            cs1[i / 3 * 2 + 1] = md5.charAt(i + 2);
//            cs2[i / 3] = md5.charAt(i + 1);
//        }
//        String salt = new String(cs2);
//        return md5Hex(password + salt).equals(new String(cs1));
//    }
//
//    /**
//     * 获取十六进制字符串形式的MD5摘要
//     */
//    public static String md5Hex(String src) {
//        try {
//            MessageDigest md5 = MessageDigest.getInstance("MD5");
//            byte[] bs = md5.digest(src.getBytes());
//            return new String(new Hex().encode(bs));
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static Timestamp getTime() {
//        Timestamp nowTime = new Timestamp(System.currentTimeMillis());
//        return nowTime;
//    }
//
//    /**
//     * @return
//     * @description 给定字符串数组，转成以逗号隔开的字符串
//     */
//    public static String join(String[] wordArray) {
//        if (wordArray != null && wordArray.length > 0) {
//            String result = "";
//            for (String item : wordArray) {
//                result += item + ",";
//            }
//            if (result != null && !result.isEmpty() && result.length() > 0) {
//                result = result.substring(0, result.length() - 1);
//            }
//            return result;
//        }
//        return "";
//    }

//
//    /**
//     * 字符串转换为Ascii-十六进制
//     *
//     * @param str
//     * @param separator
//     * @return
//     */
//    public static String stringToHexAscii(String str, String separator) {
//        char[] chars = "0123456789ABCDEF".toCharArray();
//        StringBuilder sb = new StringBuilder("");
//        byte[] bs = str.getBytes();
//        int bit;
//        for (int i = 0; i < bs.length; i++) {
//            bit = (bs[i] & 0x0f0) >> 4;
//            sb.append(chars[bit]);
//            bit = bs[i] & 0x0f;
//            sb.append(chars[bit]);
//        }
//        return sb.toString().trim();
//    }
//
//    /**
//     * 字符串转换为Ascii-十进制
//     *
//     * @param value
//     * @return
//     */
//    public static String stringToAscii(String value, String separator) {
//        StringBuffer sbu = new StringBuffer();
//        char[] chars = value.toCharArray();
//        for (int i = 0; i < chars.length; i++) {
//            if (i != chars.length - 1) {
//                sbu.append((int) chars[i]).append(separator);
//            } else {
//                sbu.append((int) chars[i]);
//            }
//        }
//        return sbu.toString();
//    }
//
//    /**
//     * Ascii转换为字符串
//     *
//     * @param value
//     * @return
//     */
//    public static String asciiToString(String value) {
//        StringBuffer sbu = new StringBuffer();
//        String[] chars = value.split(",");
//        for (int i = 0; i < chars.length; i++) {
//            sbu.append((char) Integer.parseInt(chars[i]));
//        }
//        return sbu.toString();
//    }
//
//    /**
//     * @param: [content]
//     * @return: int
//     * @description: 十六进制转十进制
//     */
//    public static int hexToInt(String content) {
//        int number = 0;
//        String[] HighLetter = {"A", "B", "C", "D", "E", "F"};
//        Map<String, Integer> map = new HashMap<>();
//        for (int i = 0; i <= 9; i++) {
//            map.put(i + "", i);
//        }
//        for (int j = 10; j < HighLetter.length + 10; j++) {
//            map.put(HighLetter[j - 10], j);
//        }
//        String[] str = new String[content.length()];
//        for (int i = 0; i < str.length; i++) {
//            str[i] = content.substring(i, i + 1);
//        }
//        for (int i = 0; i < str.length; i++) {
//            number += map.get(str[i]) * Math.pow(16, str.length - 1 - i);
//        }
//        return number;
//    }
//
//    public static void main(String[] args) {
//        System.out.println(hexToInt(stringToHexAscii("-1", "")));
//    }
}
