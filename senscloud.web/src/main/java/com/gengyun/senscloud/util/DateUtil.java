package com.gengyun.senscloud.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static final DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat format2= new SimpleDateFormat("yyyy年MM月dd日");

    /**
     * 日期格式转换
     * @param dateStr
     * @param source
     * @param target
     * @return
     */
    public static String transDateFormat(String dateStr,DateFormat source,DateFormat target){
         if(RegexUtil.optIsPresentStr(dateStr)){
             try {
                 return target.format(source.parse(dateStr));
             } catch (ParseException e) {
                 e.printStackTrace();
             }
         }
         return dateStr;
    }

    /**
     * 拆分时间字符串
     * @param dateStr
     * @return
     */
    public static String[] splitDateStr(String dateStr){
        if(!RegexUtil.optIsPresentStr(dateStr)) {
            return new String[]{"","",""};
        }
        if(dateStr.indexOf("/")>0){
             String[] returnStrings = dateStr.split("/");
             if(returnStrings.length!=3) {
                 return new String[]{"","",""};
             }
             return returnStrings;
        }
        if(dateStr.indexOf("-")>0){
            String[] returnStrings = dateStr.split("-");
            if(returnStrings.length!=3) {
                return new String[]{"","",""};
            }
            return returnStrings;
        }
        return new String[]{"","",""};

    }

    /**
     *
     * @param date
     * @param format
     * @return
     */
    public static String getDateStr(Date date,DateFormat format){
        if(date==null) {
            return "";
        }
            try {
                return format.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }

        return "";
    }

    public  static Date formatDate(String dateStr, DateFormat format) {
        if (RegexUtil.optIsBlankStr(dateStr))
            return null;
        try {
            return format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
