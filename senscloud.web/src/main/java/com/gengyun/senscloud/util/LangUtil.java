package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.LangConstant;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.*;

/**
 * 国际化工具
 */
public class LangUtil {
    /**
     * 日志国际化处理（数组）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @return 国际化处理完的日志信息
     */
    public static String doSetLogArrayNoParam(String remark, String key) {
        return doSetLogArray(remark, key, null);
    }

    /**
     * 日志国际化处理（数组，带自定义keys）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @return 国际化处理完的日志信息
     */
    public static String doSetLogArrayAndKeyNoParam(String remark, LinkedHashMap<String, Object> key) {
        return doSetLogArrayAndKey(remark, key, null);
    }

    /**
     * 日志国际化处理（数组，带参数，带自定义keys）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @param params 参数
     * @return 国际化处理完的日志信息
     */
    public static String doSetLogArrayAndKey(String remark, LinkedHashMap<String, Object> key, String[] params) {
        JSONArray logArr = new JSONArray();
        logArr.add(doSetLogInfoAndKey(remark, key, params));
        return logArr.toString();
    }

    /**
     * 日志国际化处理（对象）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @param params 参数
     * @return 国际化处理完的日志信息
     */
    private static String doSetLogInfoAndKey(String remark, LinkedHashMap<String, Object> key, String[] params) {
        JSONObject logInfo = new JSONObject();
        RegexUtil.optNotBlankStrOpt(remark).ifPresent(r -> logInfo.put("remark", r)); // 设备已存在：AS001
        RegexUtil.optNotNullMap(key).ifPresent(k -> {
            Set<String> keySet = k.keySet();
            keySet.forEach(c -> logInfo.put(c, k.get(c))); // msg_error_f
        });
        RegexUtil.optNotNullArrayOpt(params).ifPresent(ps -> logInfo.put("params", ps)); // [msgNm, msgVal]
        return logInfo.toString();
    }

    /**
     * 日志国际化处理（数组，带参数）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @param params 参数
     * @return 国际化处理完的日志信息
     */
    public static String doSetLogArray(String remark, String key, String[] params) {
        JSONArray logArr = new JSONArray();
        logArr.add(doSetLogInfo(remark, key, params));
        return logArr.toString();
    }

    /**
     * 日志国际化处理（数组，带参数）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @param params 参数
     * @return 国际化处理完的日志信息
     */
    public static String doSetLog(String remark, String key, String[] params) {
        return doSetLogInfo(remark, key, params);
    }

    /**
     * 日志国际化处理（对象）
     *
     * @param remark 中文描述
     * @param key    国际化key
     * @param params 参数
     * @return 国际化处理完的日志信息
     */
    private static String doSetLogInfo(String remark, String key, String[] params) {
        JSONObject logInfo = new JSONObject();
        RegexUtil.optNotBlankStrOpt(remark).ifPresent(r -> logInfo.put("remark", r)); // 设备已存在：AS001
        RegexUtil.optNotBlankStrOpt(key).ifPresent(k -> logInfo.put("key", k)); // msg_error_f
        RegexUtil.optNotNullArrayOpt(params).ifPresent(ps -> logInfo.put("params", ps)); // [msgNm, msgVal]
        return logInfo.toString();
    }

    /**
     * 对比新的参数map1和map2的区别
     *
     * @param map1 新值
     * @param map2 旧值
     * @return 对比信息
     */
    public static JSONArray compareMap(Map map1, Map map2) {
        JSONArray logArr = new JSONArray();
        boolean contain;
        for (Object o : map1.keySet()) {
            contain = map2.containsKey(o);
            if (RegexUtil.optIsPresentStr(map1.get(o))) {
                if (contain) {
                    contain = map1.get(o).equals(map2.get(o));
                }
                if (!contain) {
                    logArr.add(LangUtil.doSetLogArray("属性更新", LangConstant.LOG_A, new String[]{o.toString(), String.valueOf(map2.get(o)), String.valueOf(map1.get(o))}));
                }
            }
        }
        return logArr;
    }

    /**
     * 对比新的参数newList和oldlist的区别
     *
     * @param newList 新值
     * @param oldList 旧值
     * @return newList 比 oldlist多的list
     */
    public static List<String> compareList(List<String> newList, List<String> oldList) {
        List<String> result = new ArrayList<>();
        for (String n : newList) {
            boolean b = true;
            for (String o : oldList) {
                if (n.equals(o)) {
                    b = false;
                }
            }
            if (b) {
                result.add(n);
            }
        }
        return result;
    }
}
