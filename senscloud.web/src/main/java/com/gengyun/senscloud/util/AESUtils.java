package com.gengyun.senscloud.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * AES对称加密类
 */
public class AESUtils {
    private static final String KEY = "A-16-Byte-keyValA-16-Byte-keyVal"; // 建议为16位或32位

    /**
     * 对称加密
     *
     * @param content 加密字符串
     * @return 解密字符串
     */
    public static String decryptAES(String content) {
        try {
            if (null == content || content.isEmpty()) {
                return null;
            }
            SecretKeySpec spec = new SecretKeySpec(KEY.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding"); // "算法/模式/补码方式"
            cipher.init(Cipher.DECRYPT_MODE, spec);
            byte[] bt = parseHexStr2Byte(content);
            return null == bt ? null : new String(cipher.doFinal(bt));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 加密
     *
     * @param content 字符串
     * @return 加密字符串
     */
    public static String encryptAES(String content) {
        try {
            SecretKeySpec spec = new SecretKeySpec(KEY.getBytes(StandardCharsets.UTF_8), "AES");
            // "算法/模式/补码方式"
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, spec);
            byte[] result = cipher.doFinal(content.getBytes(StandardCharsets.UTF_8));
            return RegexUtil.optNotBlankStrOpt(parseByte2HexStr(result)).map(pb -> pb.replaceAll(System.lineSeparator(), "")).orElse(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content;
    }

    /**
     * 将二进制转换成16进制
     *
     * @param buf 字符信息
     * @return 加密字符串
     */
    private static String parseByte2HexStr(byte[] buf) {
        StringBuilder sb = new StringBuilder();
        for (byte b : buf) {
            String hex = Integer.toHexString(b & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     *
     * @param hexStr 加密字符串
     * @return 解密字符串
     */
    private static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1) {
            return null;
        }
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }
}
