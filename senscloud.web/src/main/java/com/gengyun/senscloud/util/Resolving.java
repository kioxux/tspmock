package com.gengyun.senscloud.util;
//import com.fitit100.util.RegexUtil;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * manfu二维码解析
// * zys
// * 2019-06-06
// */

public class Resolving {
//    /**
//     * 二维码字符解析
//     *
//     * @param qrcodestr
//     * @return Map<String, Object>
//     */
//    public static Map<String, Object> resolvingStr(String qrcodestr) {
//        try {
//            String[] subStr = qrcodestr.split("--");
//            if (subStr.length < 2) {
//                return new HashMap<String, Object>() {
//                    {
//                        put("serNumber", qrcodestr);
//
//                    }
//                };
//            }
//            String[] part1 = subStr[0].split(",");
//            String[] part2 = subStr[1].split(",");
//            String[] part3 = subStr[2].split(",");
//            String ConsumptionPart = (subStr[3].replace("~", "").trim());
//            //设备序列号
//            String serNumber = part1[0];
//            //plc版本号
//            String plcVersionNum = part1[1];
//            //屏幕版本号
//            String DisplayVersionNum = part1[2];
//            //plc程序版本号
//            String plcProgramVersionNum = part1[3];
//            //二维码生成时间
//            String gatherTime = part2[0];
//            //报警信息
//            Map<String, String> AlarmMap = new HashMap<String, String>();
//            int i = 1;
//            for (String str : part3) {
//                if(RegexUtil.isNotNull(str)&&!"0".equals(str)&&i<=10){
//                    AlarmMap.put("System_Alarm_Code" + i, str);
//                }
//                i++;
//            }
//            String[] Consumptions = stringToAscii(ConsumptionPart).split(",");
//            Map<String, Object> resultMap = new HashMap<String, Object>() {
//                {
//                    put("serNumber", serNumber);
//                    put("plcVersionNum", plcVersionNum);
//                    put("DisplayVersionNum", DisplayVersionNum);
//                    put("plcProgramVersionNum", plcProgramVersionNum);
//                    put("gatherTime", gatherTime);
//                    put("AlarmMap", AlarmMap);
//                    put("Consumptions", Consumptions);
//                }
//
//            };
//            return resultMap;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new HashMap<String, Object>() {
//                {
//                    put("serNumber", qrcodestr);
//
//                }
//            };
//        }
//    }
//
//
//    /**
//     * acsii码转string
//     *
//     * @param value
//     * @return String
//     */
//    public static String asciiToString(String value) {
//        StringBuffer sbu = new StringBuffer();
//        String[] chars = value.split(",");
//        for (int i = 0; i < chars.length; i++) {
//            sbu.append((char) Integer.parseInt(chars[i]));
//        }
//        return sbu.toString();
//    }
//
//    /**
//     * 用量数据特殊处理
//     *
//     * @param value
//     * @return String
//     */
//    public static String stringToAscii(String value) {
//        StringBuffer sb= new StringBuffer();
//        for (int i = 0; i < value.length(); i++) {
//            if (i>=0&&i % 2 == 1) {
//                sb.append(asciiToString(String.valueOf(Integer.parseInt(value.substring(i - 1, i + 1)) - 10))) ;
//            }
//        }
//        return sb.toString();
//    }
//
//    /**
//     * 循环左移index位字符串
//     * 思想：先部分反转，后整体反转
//     *
//     * @param from
//     * @param index
//     * @return
//     */
//    public static String leftMoveIndex(String from, int index) {
//        String first = from.substring(0, index);
//        String second = from.substring(index);
//        first = reChange(first);
//        second = reChange(second);
//        from = first + second;
//        from = reChange(from);
//        return from;
//    }
//
//    /**
//     * 循环右移index位字符串
//     * 思想：先整体反转，后部分反转
//     *
//     * @param from
//     * @param index
//     * @return
//     */
//    public static String rightMoveIndex(String from, int index) {
//        from = reChange(from);
//        String first = from.substring(0, index);
//        String second = from.substring(index);
//        first = reChange(first);
//        second = reChange(second);
//        from = first + second;
//        return from;
//    }
//
//
//    /**
//     * 反转字符串（循环交换）
//     * 其他字符串反转的方法
//     * 1、java的api：StringBuffer的reverse方法
//     * 2、利用栈的特性（先进后出）
//     * 3、反向遍历字符串
//     *
//     * @param from
//     * @return
//     */
//    public static String reChange(String from) {
//        char[] froms = from.toCharArray();
//        int length = froms.length;
//        for (int i = 0; i < length / 2; i++) {
//            char temp = froms[i];
//            froms[i] = froms[length - 1 - i];
//            froms[length - 1 - i] = temp;
//        }
//        return String.valueOf(froms);
//    }
}
