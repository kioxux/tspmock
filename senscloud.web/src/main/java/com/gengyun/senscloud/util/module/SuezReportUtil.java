package com.gengyun.senscloud.util.module;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日常报告工具类
 */
public class SuezReportUtil {
    private static Logger logger = LoggerFactory.getLogger(SuezReportUtil.class);

    /**
     * 处理工单费用列表数据
     *
     * @param orderInfo 公共值
     * @param wcMap     数据库字段信息
     * @param dicMap    字典信息
     * @param threeList 备件
     * @param fourList  材料
     * @param fiveList  其他
     */
    public static void doCheckWorkFee(Map<String, Object> orderInfo, Map<String, Object> wcMap, Map<String, Map<String, Object>> dicMap, List<Map<String, Object>> threeList, List<Map<String, Object>> fourList, List<Map<String, Object>> fiveList) {
        RegexUtil.optNotNullJa(orderInfo.get("work_fee")).ifPresent(j -> {
            for (int i = 0; i < j.size(); i++) {
                JSONObject jsonObject = j.getJSONObject(i);
                Map<String, Object> report = new HashMap<>();
                BigDecimal useCount = RegexUtil.optInitBigDecimal(jsonObject.get(RegexUtil.optStrOrVal(wcMap.get("use_count"), "use_count")), LangConstant.TITLE_AAT_V);
                BigDecimal price = RegexUtil.optInitBigDecimal(jsonObject.get(RegexUtil.optStrOrVal(wcMap.get("price"), "price")), LangConstant.TITLE_ABBBA_Z);
                report.put("use_count", useCount);
                report.put("price", price);
                report.put("totalPrice", useCount.multiply(price));
                SuezReportUtil.doSetListData(new String[]{"report_day", "work_code", "asset_code", "asset_name"},
                        new String[]{"fee_type", "object_name", "object_model", "fee_belong", "fee_attribute", "object_code", "remark"},
                        report, jsonObject, orderInfo, wcMap, dicMap);
                String feeType = report.get("fee_type").toString();
                if ("1".equals(feeType)) {//备件
                    threeList.add(report);
                } else if ("2".equals(feeType)) {//材料
                    fourList.add(report);
                } else {//其他
                    fiveList.add(report);
                }
            }
        });
    }

    /**
     * 处理人工工时列表数据
     *
     * @param orderInfo 公共值
     * @param wcMap     数据库字段信息
     * @param dicMap    字典信息
     * @param userMap   用户信息
     * @param twoList   列表数据
     */
    public static void doCheckUserHour(Map<String, Object> orderInfo, Map<String, Object> wcMap, Map<String, Map<String, Object>> dicMap, Map<String, Map<String, Object>> userMap, List<Map<String, Object>> twoList) {
        RegexUtil.optNotNullJa(orderInfo.get("person_info")).ifPresent(j -> {
            for (int i = 0; i < j.size(); i++) {
                JSONObject jsonObject = j.getJSONObject(i);
                Map<String, Object> report = new HashMap<>();
                RegexUtil.optNotBlankStrOpt(jsonObject.get(RegexUtil.optStrOrVal(wcMap.get("name"), "name"))).map(userMap::get).ifPresent(user -> {
                    BigDecimal hourFee = RegexUtil.optBigDecimalOrVal(user.get("hour_fee"), BigDecimal.ONE, "");
                    BigDecimal workHours = RegexUtil.optInitBigDecimal(jsonObject.get(RegexUtil.optStrOrVal(wcMap.get("work_hours"), "work_hours")), LangConstant.TITLE_BAAAB_N);
                    report.put("user_name", user.get("user_name"));
                    report.put("user_code", user.get("user_code"));
                    report.put("real_hour", workHours);
                    report.put("price", hourFee);
                    report.put("totalPrice", workHours.multiply(hourFee));
                });
                SuezReportUtil.doSetListData(new String[]{"report_day", "work_code", "asset_code", "asset_name", "asset_prf_type_code", "position_name", "repair_type", "asset_prf_type"},
                        new String[]{"fee_belong", "fee_attribute"}, report, jsonObject, orderInfo, wcMap, dicMap);
                twoList.add(report);
            }
        });
    }

    /**
     * 设置列表数据
     *
     * @param comFields  公共字段
     * @param dataFields 普通字段
     * @param report     目标对象
     * @param jsonObject 数据值
     * @param orderInfo  公共值
     * @param wcMap      数据库字段信息
     * @param dicMap     字典信息
     */
    private static void doSetListData(String[] comFields, String[] dataFields, Map<String, Object> report, JSONObject jsonObject, Map<String, Object> orderInfo, Map<String, Object> wcMap, Map<String, Map<String, Object>> dicMap) {
        for (String str : comFields) {
            report.put(str, orderInfo.get(str));
        }
        for (String str : dataFields) {
            RegexUtil.optNotBlankStrOpt(RegexUtil.optStrOrVal(wcMap.get(str), str)).ifPresent(dbNm -> {
                if ("fee_belong".equals(str) || "fee_attribute".equals(str)) {
                    RegexUtil.optNotBlankStrOpt(jsonObject.get(dbNm)).ifPresent(v -> RegexUtil.optNotNullMap(dicMap.get(str)).ifPresent(m -> report.put(str + "_name", m.get(v))));
                } else {
                    report.put(str, jsonObject.get(dbNm));
                }
            });
        }
    }


    /**
     * 创建excel
     *
     * @param workbook  excel
     * @param firstList 数据
     * @param twoList   数据
     * @param threeList 数据
     * @param fourList  数据
     * @param fiveList  数据
     * @param unDoList  数据
     * @param taskList  数据
     */
    public static void createSrExcel(Workbook workbook, List<Map<String, Object>> firstList, List<Map<String, Object>> twoList, List<Map<String, Object>> threeList,
                                     List<Map<String, Object>> fourList, List<Map<String, Object>> fiveList, List<Map<String, Object>> unDoList, List<Map<String, Object>> taskList, List<Map<String, Object>> finishedList) {
        String[] titleFields = {"报表日期（年/月）", "公司名称（全称）", "水厂/厂外泵站名称", "业务类型", "工单号", "设备编码", "设备名称",
                "工艺单元代码", "工艺单元名称", "设备功能类型代码", "设备功能类型名称", "设备专业类型代码", "设备专业类型名称", "设备关键度等级代码", "设备关键度等级",
                "维护类型（主项）（代码）", "维护类型（主项）", "维护类型代码（子项）", "维护类型（子项）", "工作组", "工作范围分类", "工作描述",
                "症状描述", "故障类型大项代码", "故障类型大项描述", "故障类型代码", "故障类型描述", "故障原因代码", "故障原因描述", "实施措施代码",
                "实施措施代码描述", "实施措施描述", "设备状况代码", "设备状况", "工单优先级代码", "工单优先级", "负责部门", "工单状态",
                "工单生成日期/报修日期（年/月/日）", "计划开工日期", "计划完工日期", "计划维修时间（Hrs）", "实际开工日期（年/月/日 时）",
                "实际完成日期（年/月/日 时）", "维修负责人（工号）", "实际维修时间（Hrs）", "设备不可用停机时间（Hrs）", "维修症状描述", "维修方式（自修/外委）"
        };
        String[] dataFields = {"report_day", "company_name", "root_position", "duty_type_name", "work_code", "asset_code", "asset_name",
                "inner_code", "position_name", "category_code", "category_name", "asset_prf_type@a(@a)", "asset_prf_type@b(", "level_name@a(@a)", "level_name@b(",
                "business_name_code", "business_name", "type_name@a(@a)", "type_name@b(", "asset_prf_type", "asset_prf_type", "result_note",
                "problem_note", "parent_fault_code_name@a【@a】", "parent_fault_code_name@b【", "fault_code_name@a【@a】", "fault_code_name@b【", "problem_cau@a(@a)", "problem_cau@b(", "measure_name@a(@a)",
                "measure_name@b(", "measures_note", "equipment_situation@a(@a)", "equipment_situation@b(", "repair_priority_level", "repair_priority", "department_name", "status",
                "create_time", "occur_time", "adv_start_tiame", "jhwxsj_aaaa", "begin_time",
                "finished_time", "user_code", "repair_time", "downtime", "repair_note", "repair_type"
        };
        SuezReportUtil.createSrExcelSheet(workbook, firstList, "维护事项综合报告", titleFields, dataFields);
        SuezReportUtil.createSrExcelSheet(workbook, twoList, "人工时报告",
                new String[]{"报表日期（年/月）", "工单号", "工艺单元", "设备编码",
                        "设备名称", "工作组代码", "工作组", "维修人员（工号）", "维修人员（用户名）", "维修人工时（Hrs）", "人工单价（元/小时）", "人工费合计（元）",
                        "费用类别", "费用属性", "维修方式", "外委/JV公司名称"},
                new String[]{"report_day", "work_code", "position_name", "asset_code",
                        "asset_name", "asset_prf_type", "asset_prf_type", "user_code", "user_name", "real_hour", "price", "totalPrice",
                        "fee_belong_name", "fee_attribute_name", "repair_type", "text"});
        SuezReportUtil.createSrExcelSheet(workbook, threeList, "备件消耗报告",
                new String[]{"报表日期（年/月）", "工单号", "设备编码", "设备名称", "费用类别", "费用属性", "备件编码", "备件类型", "备件名称",
                        "备件型号", "备件关键度等级", "备件数量（套）", "备件当前单价（元）", "备件费用合计（元）"},
                new String[]{"report_day", "work_code", "asset_code", "asset_name", "fee_belong_name", "fee_attribute_name", "object_code", "bjlx_aaaa", "object_name",
                        "object_model", "bjgjddj_aaaaa", "use_count", "price", "totalPrice"});
        SuezReportUtil.createSrExcelSheet(workbook, fourList, "材料消耗报告",
                new String[]{"报表日期（年/月）", "工单号", "设备编码", "设备名称", "费用类别", "费用属性", "材料名称", "材料数量", "材料单价（元）", "材料费用合计"},
                new String[]{"report_day", "work_code", "asset_code", "asset_name", "fee_belong_name", "fee_attribute_name", "object_name", "use_count", "price", "totalPrice"});
        SuezReportUtil.createSrExcelSheet(workbook, fiveList, "其他消耗报告",
                new String[]{"报表日期（年/月）", "工单号", "设备编码", "设备名称", "费用类别", "费用属性", "名称", "描述", "材料数量", "材料单价（元）", "材料费用合计"},
                new String[]{"report_day", "work_code", "asset_code", "asset_name", "fee_belong_name", "fee_attribute_name", "object_name", "remark", "use_count", "price", "totalPrice"});
        SuezReportUtil.createSrExcelSheet(workbook, unDoList, "维护事项未完成报告", titleFields, dataFields);
        SuezReportUtil.createSrExcelSheet(workbook, taskList, "任务项",
                new String[]{"报表日期（年/月）", "工单号", "任务模板名称", "任务组名称", "任务项名称", "任务结果"},
                new String[]{"report_day", "work_code", "task_template_name", "group_name", "task_item_name", "value@eresult_type"});
        SuezReportUtil.createSrExcelSheet(workbook, finishedList, "维护事项本月完成报告", titleFields, dataFields);
    }

    /**
     * 创建工作表
     *
     * @param workbook    工作簿
     * @param dataList    数据
     * @param sheetName   工作表名称
     * @param titleFields 工作表字段标题
     * @param dataFields  工作表字段名
     */
    private static void createSrExcelSheet(Workbook workbook, List<Map<String, Object>> dataList, String sheetName, String[] titleFields, String[] dataFields) {
        Sheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth(24);
        Row header = sheet.createRow(0);
        int i = -1;
        for (String title : titleFields) {
            header.createCell(++i).setCellValue(title);
        }
        RegexUtil.optNotNullList(dataList).ifPresent(l -> {
            int rowCount = 1;
            for (Map<String, Object> data : l) {
                Row courseRow = sheet.createRow(rowCount++);
                int di = -1;
                for (String dataName : dataFields) {
                    try {
                        boolean hasSetData = false;
                        String tmpData = RegexUtil.optStrOrBlank(data.get(dataName));
                        if (dataName.contains("@")) {
                            String rgType = Arrays.stream(new String[]{"@a", "@b", "@c", "@d", "@e"}).filter(dataName::contains).findFirst().orElse("");
                            String[] spNames = dataName.split(rgType);
                            String splType = SenscloudUtil.replaceBrackets(spNames[1]);
                            tmpData = RegexUtil.optStrOrBlank(data.get(spNames[0]));
                            String tmpStr = SenscloudUtil.replaceBrackets(tmpData);
                            if ("@a".equals(rgType)) {
                                tmpData = tmpStr.contains(splType) ? tmpStr.substring(tmpStr.indexOf(splType) + 1, tmpStr.indexOf(SenscloudUtil.replaceBrackets(spNames[2]))) : "";
                            } else if ("@b".equals(rgType)) {
                                tmpData = tmpStr.contains(splType) ? tmpStr.substring(0, tmpStr.indexOf(splType)) : tmpData;
                            } else if ("@c".equals(rgType)) {
                                if (tmpStr.contains(splType)) {
                                    String sprType = SenscloudUtil.replaceBrackets(spNames[2]);
                                    if (tmpStr.contains(sprType)) {
                                        tmpData = tmpStr.substring(tmpStr.lastIndexOf(splType) + 1, tmpStr.lastIndexOf(sprType));
                                    } else {
                                        tmpData = tmpStr.substring(tmpStr.lastIndexOf(splType) + 1);
                                    }
                                } else {
                                    tmpData = "";
                                }
                            } else if ("@d".equals(rgType)) {
                                tmpData = tmpStr.contains(splType) ? tmpStr.substring(0, tmpStr.lastIndexOf(splType) - 1) : tmpData;
                            } else if ("@e".equals(rgType)) {
                                String resultType = RegexUtil.optStrOrBlank(data.get(splType));
                                if ("3".equals(resultType)) {
                                    List<String> imgList = DataChangeUtil.scdObjToListStr(data.get("value"));
                                    if (null != imgList && imgList.size() > 0) {
                                        courseRow.setHeightInPoints(150);
                                        CreationHelper helper = workbook.getCreationHelper();
                                        ClientAnchor anchor = helper.createClientAnchor();
                                        anchor.setRow1(rowCount - 1);
                                        for (String fileNames : imgList) {
                                            int cellColumn = ++di;
                                            sheet.setColumnWidth(cellColumn, 35 * 256);
                                            anchor.setCol1(cellColumn);
                                            InputStream is = null;
                                            try {
                                                is = new FileInputStream(fileNames);
                                                int pictureIdx = workbook.addPicture(IOUtils.toByteArray(is), Workbook.PICTURE_TYPE_PNG);
                                                Drawing patriarch = sheet.createDrawingPatriarch();
                                                Picture pict = patriarch.createPicture(anchor, pictureIdx);
                                                pict.resize(1, 1);
                                            } catch (Exception imgExp) {
                                                courseRow.createCell(cellColumn).setCellValue("");
                                                logger.error(ErrorConstant.CODE_SUEZ_REPORT_103 + sheetName + fileNames, imgExp);
                                            } finally {
                                                SenscloudUtil.closeInputStream(is);
                                            }
                                            hasSetData = true;
                                        }
                                    }
                                }
                            }
                        } else if ("business_name_code".equals(dataName)) {
                            tmpData = RegexUtil.optStrOrBlank(data.get("business_name"));
                            tmpData = "纠正性维护".equals(tmpData) ? "CM" : "预防性维护".equals(tmpData) ? "PM" : "改进性维护".equals(tmpData) ? "MI" : "资本性项目".equals(tmpData) ? "CAP" : "";
                        } else if ("repair_type".equals(dataName)) {
                            tmpData = RegexUtil.optStrOrVal(tmpData, "自修");
                        }
                        if (!hasSetData) {
                            courseRow.createCell(++di).setCellValue(tmpData);
                        }
                    } catch (Exception e) {
                        logger.error(ErrorConstant.CODE_SUEZ_REPORT_101 + sheetName, e);
                    }
                }
            }
        });
    }
}
