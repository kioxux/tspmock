package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.LangConstant;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Map;

public class DownloadUtil {

    public static final String CONTENT_TYPE_OS = "octet-stream";
    public static final String CONTENT_TYPE_PDF = "pdf";
    public static final String CONTENT_TYPE_WORD = "msword";
    public static final String CONTENT_TYPE_EXCEL = "vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    /**
     * @param filePath   要下载的文件路径
     * @param returnName 返回的文件名
     * @param delFlag    是否删除文件
     */
    public static void download(String filePath, String returnName, boolean delFlag) {
        download(filePath, returnName, CONTENT_TYPE_OS, delFlag);
    }

    /**
     * @param filePath
     * @param returnName
     * @param ContentType
     * @param delFlag
     */
    public static void download(String filePath, String returnName, String ContentType, boolean delFlag) {
        prototypeDownload(new File(filePath), returnName, ContentType, delFlag);
    }

    public static void downloadExtr(String filePath, String returnName, String ContentType, boolean delFlag, List<Map<String, Object>> data) {
        prototypeDownloadExtr(new File(filePath), returnName, ContentType, delFlag, data);
    }

    /**
     * @param file       要下载的文件
     * @param returnName 返回的文件名
     * @param delFlag    是否删除文件
     */
    public static void prototypeDownload(File file, String returnName, String ContentType, boolean delFlag) {
        // 下载文件
        FileInputStream inputStream = null;
        ServletOutputStream outputStream = null;
        if (!file.exists()) {
            throw new SenscloudException(LangConstant.TEXT_CHOOSE_A); // 请选择文件
        }
        try {
            HttpServletResponse response = HttpRequestUtils.getResponse();
            response.reset();
            //设置响应类型	PDF文件为"application/pdf"，WORD文件为："application/msword"， EXCEL文件为："application/vnd.ms-excel"。
            response.setContentType(String.format("application/%s;charset=utf-8", ContentType));

            //设置响应的文件名称,并转换成中文编码
            //returnName = URLEncoder.encode(returnName,"UTF-8");
            returnName = response.encodeURL(new String(returnName.getBytes(), "iso8859-1"));    //保存的文件名,必须和页面编码一致,否则乱码

            //attachment作为附件下载；inline客户端机器有安装匹配程序，则直接打开；注意改变配置，清除缓存，否则可能不能看到效果
            response.addHeader("Content-Disposition", "attachment;filename=" + returnName);

            //将文件读入响应流
            inputStream = new FileInputStream(file);
            outputStream = response.getOutputStream();
            int length = 1024 * 1024;
            int readLength = 0;
            byte[] buf = new byte[length];
            readLength = inputStream.read(buf, 0, length);
            while (readLength != -1) {
                outputStream.write(buf, 0, readLength);
                readLength = inputStream.read(buf, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SenscloudUtil.closeOutputStream(outputStream);
            SenscloudUtil.closeInputStream(inputStream);
            // 删除原文件
            if (delFlag) {
                file.delete();
            }
        }
    }

    public static void prototypeDownloadExtr(File file, String returnName, String ContentType, boolean delFlag, List<Map<String, Object>> data) {
        // 下载文件
        FileInputStream inputStream = null;
        ServletOutputStream outputStream = null;
        if (!file.exists()) {
            throw new SenscloudException(LangConstant.TEXT_CHOOSE_A); // 请选择文件
        }
        try {
            HttpServletResponse response = HttpRequestUtils.getResponse();
            response.reset();
            //设置响应类型	PDF文件为"application/pdf"，WORD文件为："application/msword"， EXCEL文件为："application/vnd.ms-excel"。
            response.setContentType(String.format("application/%s;charset=utf-8", ContentType));

            //设置响应的文件名称,并转换成中文编码
            returnName = response.encodeURL(new String(returnName.getBytes(), "iso8859-1"));    //保存的文件名,必须和页面编码一致,否则乱码

            //attachment作为附件下载；inline客户端机器有安装匹配程序，则直接打开；注意改变配置，清除缓存，否则可能不能看到效果
            response.addHeader("Content-Disposition", "attachment;filename=" + returnName);

            //将文件读入响应流
            inputStream = new FileInputStream(file);
            Workbook wb = WorkbookFactory.create(inputStream);
            // 获取设备类型相关的自定义字段
            if (RegexUtil.optIsPresentList(data)) {
                // 读取了模板内所有sheet内容
                Sheet sheet = wb.getSheetAt(0);
                sheet.setForceFormulaRecalculation(true);
                for (int i = 0,size = data.size(); i < size; i++) {
                    sheet.getRow(0).createCell(19+i).setCellValue(RegexUtil.optStrOrBlank(data.get(i).get("field_code"))
                            .concat("【").concat(RegexUtil.optStrOrBlank(data.get(i).get("field_name"))).concat("】"));
                }
            }
            outputStream = response.getOutputStream();
            wb.write(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SenscloudUtil.closeOutputStream(outputStream);
            SenscloudUtil.closeInputStream(inputStream);
            // 删除原文件
            if (delFlag) {
                file.delete();
            }
        }
    }

    /**
     * @param inputStream 要下载的文件
     * @param returnName  返回的文件名
     */
    public static void prototypeDownloadByIs(InputStream inputStream, String returnName, String ContentType) {
        // 下载文件
        ServletOutputStream outputStream = null;
        try {
            HttpServletResponse response = HttpRequestUtils.getResponse();
            response.reset();
            //设置响应类型	PDF文件为"application/pdf"，WORD文件为："application/msword"， EXCEL文件为："application/vnd.ms-excel"。
            response.setContentType(String.format("application/%s;charset=utf-8", ContentType));

            //设置响应的文件名称,并转换成中文编码
            //returnName = URLEncoder.encode(returnName,"UTF-8");
            returnName = response.encodeURL(new String(returnName.getBytes(), "iso8859-1"));    //保存的文件名,必须和页面编码一致,否则乱码

            //attachment作为附件下载；inline客户端机器有安装匹配程序，则直接打开；注意改变配置，清除缓存，否则可能不能看到效果
            response.addHeader("Content-Disposition", "attachment;filename=" + returnName);

            //将文件读入响应流
            outputStream = response.getOutputStream();
            int length = 1024 * 1024;
            int readLength = 0;
            byte[] buf = new byte[length];
            readLength = inputStream.read(buf, 0, length);
            while (readLength != -1) {
                outputStream.write(buf, 0, readLength);
                readLength = inputStream.read(buf, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SenscloudUtil.closeOutputStream(outputStream);
        }
    }
}
