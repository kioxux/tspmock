package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.model.MethodParam;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 数据校验工具类
 * User: sps
 * Date: 2019/07/15
 * Time: 上午11:42
 */
public class RegexUtil {
    public static <T> Optional<T> optNotNull(T obj) {
        return Optional.ofNullable(obj);
    }

    public static Object optNotNullOrExp(Object obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }


    /**
     * {字符串}不能为空
     */
    public static Object optNotNullOrExp(Object obj, String msgKey, String[] msgPrm) {
        return RegexUtil.optNotNull(obj).orElseThrow(() -> new SenscloudException(msgKey, msgPrm));
    }

    /**
     * {具体}信息不存在
     */
    public static Object optNotNullOrExpNullInfo(Object obj, String errInfoMsg) {
        return RegexUtil.optNotNullOrExp(obj, LangConstant.TEXT_K, new String[]{errInfoMsg}); // {0}信息不存在
    }

    public static Object optNotNullOrExp(Object obj, Integer errInfoMsg) {
        return RegexUtil.optNotNull(obj).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    public static Map<String, Object> optMapOrExp(Map<String, Object> obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    public static Map<String, Object> optMapOrExpNullInfo(Map<String, Object> obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).orElseThrow(() -> new SenscloudException(LangConstant.TEXT_K, new String[]{errInfoMsg})); // {0}信息不存在
    }

    public static Map<String, Object> optMapOrNew(Map<String, Object> obj) {
        return RegexUtil.optNotNull(obj).orElseGet(HashMap::new);
    }

    public static Map<String, Object> optMapOrMap(Map<String, Object> obj, Map<String, Object> map) {
        return RegexUtil.optNotNull(obj).orElseGet(() -> map);
    }

    public static Map<String, Map<String, Object>> optMapObjOrNew(Map<String, Map<String, Object>> obj) {
        return RegexUtil.optNotNull(obj).orElseGet(HashMap::new);
    }

    public static Map<String, Long> optMapLongOrNew(Map<String, Long> obj) {
        return RegexUtil.optNotNull(obj).orElseGet(HashMap::new);
    }

    public static Map<String, String> optMapStringOrNew(Map<String, String> obj) {
        return RegexUtil.optNotNull(obj).orElseGet(HashMap::new);
    }

    public static Map<String, String> optMapStrOrExp(Map<String, String> obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    public static boolean optIsPresentMap(Map<String, Object> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).isPresent();
    }

    public static boolean optIsPresentMapStr(Map<String, String> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).isPresent();
    }

    public static Optional<Map<String, Object>> optNotNullMap(Map<String, Object> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    public static Optional<Map<String, Map<String, Object>>> optNotNullMapObj(Map<String, Map<String, Object>> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    public static Optional<Map<String, Boolean>> optNotNullMapBoolean(Map<String, Boolean> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    public static Optional<Map<String, String>> optNotNullMapString(Map<String, String> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    public static Optional<String> optNotBlankStrOpt(Object obj) {
        return RegexUtil.optNotNull(obj).map(String::valueOf).filter(StringUtils::isNotBlank).filter(e -> !"undefined".equals(e)).filter(e -> !"null".equals(e));
    }

    public static Optional<String> optNotBlankStrLowerOpt(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).map(String::toLowerCase);
    }

    public static boolean optEquals(Object obj, String val) {
        return RegexUtil.optNotBlankStrOpt(obj).filter(val::equals).isPresent();
    }

    public static Optional<String> optEqualsOpt(Object obj, String val) {
        return RegexUtil.optNotBlankStrOpt(obj).filter(val::equals);
    }

    public static Optional<String> notEqualsOpt(Object obj, String val) {
        return RegexUtil.optNotNull(obj).map(String::valueOf).filter(o -> !o.equals(val));
    }

    public static Optional<String> notEqualsOrStrOpt(Object obj, Object val) {
        return RegexUtil.optNotNull(obj).map(String::valueOf).filter(o -> !o.equals(RegexUtil.optStrOrBlank(val)));
    }

    public static void notEqualsExp(Object obj, String val, String msg) {
        RegexUtil.optNotNull(obj).map(String::valueOf).filter(o -> !o.equals(val)).ifPresent(t -> {
            throw new SenscloudException(msg);
        });
    }

    public static Optional<String> optValStrEqualsOpt(String obj, String val) {
        return RegexUtil.optNotNull(obj).filter(val::equals);
    }

    public static String optStrOrVal(Object obj, String val) {
        return RegexUtil.optNotBlankStrOpt(obj).orElse(val);
    }

    public static String optStrOrNull(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).orElse(null);
    }

    public static String optStrOrBlank(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).orElse("");
    }

    public static String optStrOrBlankWithTrim(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).map(String::trim).orElse("");
    }

    public static String optStrAndValOrVal(Object obj1, Object obj2, String val) {
        return RegexUtil.optNotBlankStrOpt(obj1).orElseGet(() -> RegexUtil.optStrOrVal(obj2, val));
    }

    public static String optStrAndValOrNull(Object obj1, Object obj2) {
        return RegexUtil.optNotBlankStrOpt(obj1).orElseGet(() -> RegexUtil.optStrOrNull(obj2));
    }

    public static String optStrAndValOrBlank(Object obj1, Object obj2) {
        return RegexUtil.optNotBlankStrOpt(obj1).orElseGet(() -> RegexUtil.optStrOrBlank(obj2));
    }

    public static Object optStrToObjAndValOrBlank(Object obj1, Object obj2, Object val) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(e -> val).orElseGet(() -> RegexUtil.optStrOrBlank(obj2));
    }

    public static String optNotBlankStrOrExp(Object obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    public static String optNotBlankStrOrExp(Object obj, Integer msgCode) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(msgCode));
    }

    public static String optNotBlankStrOrExp(Object obj, String msgKey, String[] msgPrm) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(msgKey, msgPrm));
    }

    /**
     * {字符串}不能为空
     */
    public static String optStrOrExpNotNull(Object obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(LangConstant.MSG_A, new String[]{errInfoMsg})); // {0}不能为空
    }

    /**
     * {字符串}未选择
     */
    public static String optStrOrExpNotSelect(Object obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{errInfoMsg})); // {0}未选择
    }

    /**
     * {字符串}不存在
     */
    public static String optStrOrExpNotExist(Object obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{errInfoMsg})); // {0}不存在
    }

    /**
     * 请联系管理员调整页面：{errCode}
     */
    public static String optStrOrExpToGy(Object obj, String errCode) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudException(LangConstant.MSG_HJ, new String[]{errCode})); // 请联系管理员调整页面：{d}
    }

    public static String optStrOrError(Object obj, MethodParam methodParam, String errMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudError(methodParam, errMsg));
    }

    public static String optStrOrError(Object obj, MethodParam methodParam, String errMsg, String remark, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudError(methodParam, errMsg, remark, expMsg));
    }

    public static String optStrOrErrorExp(Object obj, MethodParam methodParam, String errMsg, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudError(methodParam, errMsg, null, expMsg));
    }

    public static String optStrOrPrmError(Object obj, MethodParam methodParam, String errMsg, String remark) {
        return RegexUtil.optNotBlankStrOpt(obj).orElseThrow(() -> new SenscloudError(methodParam, errMsg, remark));
    }

    public static boolean optIsPresentStr(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).isPresent();
    }

    public static boolean optIsBlankStr(Object obj) {
        return !RegexUtil.optIsPresentStr(obj);
    }

    public static String optStrPrefix(Object obj1, Object prefix, String name) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(o -> RegexUtil.changeToBd(o, name)).map(e -> RegexUtil.optStrOrBlank(prefix) + String.format("%.2f", e)).orElse("");
    }

    public static String optStrSuffix(Object obj1, Object suffix, String name) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(o -> RegexUtil.changeToBd(o, name)).map(e -> String.format("%.2f", e) + RegexUtil.optStrOrBlank(suffix)).orElse("");
    }

    public static Integer optIntegerOrNull(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).map(Integer::valueOf).orElse(null); // NUMBER_FORMATS_ERROR
    }

    public static Integer optIntegerOrNull(Object obj, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, name)).orElse(null); // NUMBER_FORMATS_ERROR
    }

    public static Integer optIntegerOrNull(Object obj, MethodParam methodParam, String errMsg, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, expMsg)).orElse(null);
    }

    public static Integer optIntegerOrNullWithRemark(Object obj, MethodParam methodParam, String errMsg, String remark) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, remark, null)).orElse(null);
    }

    public static Integer optIntegerOrNull(Object obj, MethodParam methodParam, String errMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, null)).orElse(null);
    }

    public static Integer initIntegerWithRemark(Object obj, MethodParam methodParam, String errMsg, String remark) {
        return RegexUtil.optIntegerOrValWithRemark(obj, 0, methodParam, errMsg, remark);
    }

    public static Integer optIntegerOrValWithRemark(Object obj, Integer val, MethodParam methodParam, String errMsg, String remark) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, remark, null)).orElse(val);
    }

    public static Integer optIntegerOrVal(Object obj, Integer val, MethodParam methodParam, String errMsg, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, expMsg)).orElse(val);
    }

    public static Integer optIntegerOrVal(Object obj, Integer val, MethodParam methodParam, String errMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, null)).orElse(val);
    }

    public static Integer optIntegerOrVal(Object obj, Integer val, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, name)).orElse(val);
    }

    public static Integer optIntegerOrVal(Object obj, Integer val) {
        return RegexUtil.optNotBlankStrOpt(obj).map(Integer::valueOf).orElse(val);
    }

    public static Integer optIntegerAndValOrVal(Object obj1, Object obj2, Integer val, String name) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(o -> RegexUtil.changeToInteger(o, name)).orElseGet(() -> RegexUtil.optIntegerOrVal(obj2, val, name));
    }

    public static Integer optIntegerAndValOrNull(Object obj1, Object obj2, String name) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(o -> RegexUtil.changeToInteger(o, name)).orElseGet(() -> RegexUtil.optIntegerOrVal(obj2, null, name));
    }

    public static Integer optIntegerAndValOrNull(Object obj1, Object obj2, MethodParam methodParam, String errMsg, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, expMsg)).orElseGet(() -> RegexUtil.optIntegerOrVal(obj2, null, methodParam, errMsg, expMsg));
    }

    public static Integer optIntegerOrExp(Object obj, String name, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, name)).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    /**
     * 请联系管理员调整页面：{errCode}
     */
    public static Integer optIntegerOrExpToGy(Object obj, String errCode) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToIntegerOrExp(o, errCode)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_HJ, new String[]{errCode})); // 请联系管理员调整页面：{d}
    }

    public static Integer optIntegerOrErr(Object obj, MethodParam methodParam, String errMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, null)).orElseThrow(() -> new SenscloudError(methodParam, errMsg, null));
    }

    public static Integer optIntegerOrExp(Object obj, MethodParam methodParam, String errMsg, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, null, expMsg)).orElseThrow(() -> new SenscloudError(methodParam, errMsg, expMsg));
    }

    public static Integer optIntegerOrExp(Object obj, MethodParam methodParam, String errMsg, String remark, String expMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, remark, expMsg)).orElseThrow(() -> new SenscloudError(methodParam, errMsg, remark, expMsg));
    }

    /**
     * {数字}不能为空
     */
    public static Integer optIntegerOrExpParam(Object obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, errInfoMsg)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_A, new String[]{errInfoMsg})); // {0}不能为空
    }

    /**
     * {数字}未选择
     */
    public static Integer optSelectOrExpParam(Object obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, errInfoMsg)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{errInfoMsg})); // {0}未选择
    }

    public static Optional<Integer> optNotNullIntegerOpt(Object obj, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, name));
    }

    public static Optional<Integer> optNotNullIntegerOpt(Object obj, MethodParam methodParam, String errMsg, String remark) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToInteger(o, methodParam, errMsg, remark, null));
    }

    public static boolean optIsPresentArray(String[] obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.length > 0).isPresent();
    }

    public static Optional<String[]> optNotNullArrayOpt(String[] obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.length > 0);
    }

    public static String[] optArrayOrExp(String[] obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.length > 0).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    /**
     * {字符串}未选择
     */
    public static String[] optStrToArray(Object obj, String errInfoMsg) {
        return RegexUtil.optStrToArraySplit(obj).orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{errInfoMsg})); // {0}未选择
    }

    public static Optional<String[]> optStrToArraySplit(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).toArray(String[]::new)).filter(a -> a.length > 0);
    }

    /**
     * {字符串}未选择增加筛选(不包含自身）
     */
    public static String[] optStrToArrayNotSelf(Object obj1, Object obj2, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj1).map(ss -> Arrays.stream(ss.split(",")).filter(RegexUtil::optIsPresentStr).filter(t -> !obj1.equals(obj2)).toArray(String[]::new)).orElseThrow(() -> new SenscloudException(LangConstant.MSG_B, new String[]{errInfoMsg})); // {0}未选择
    }

    public static List<Map<String, Object>> optListOrExp(List<Map<String, Object>> obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    /**
     * {0}信息不存在
     *
     * @param obj  集合
     * @param name 具体信息名称
     * @return 集合
     */
    public static List<Map<String, Object>> optListOrExpNullInfo(List<Map<String, Object>> obj, String name) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).orElseThrow(() -> new SenscloudException(LangConstant.TEXT_K, new String[]{name})); // {0}信息不存在
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> optObjToListOrExp(Object obj, String errInfoMsg) {
        return RegexUtil.optNotNull(obj).map(o -> (List<Map<String, Object>>) o).filter(e -> e.size() > 0).orElseThrow(() -> new SenscloudException(LangConstant.TEXT_K, new String[]{errInfoMsg})); // {0}信息不存在
    }

    public static boolean optIsPresentList(List obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0).isPresent();
    }

    public static Optional<List> optNotNullListObj(List obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    public static Optional<List<Map<String, Object>>> optNotNullList(List<Map<String, Object>> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> optNotNullListOrNew(Object obj) {
        return RegexUtil.optNotNull(obj).map(o -> (List<Map<String, Object>>) o).orElseGet(ArrayList::new);
    }

    public static List<String> optNotNullListStrOrNew(Object obj) {
        return RegexUtil.optNotNull(obj).map(o -> (List<String>) o).orElseGet(ArrayList::new);
    }

    public static Optional<List<String>> optNotNullListStr(List<String> obj) {
        return RegexUtil.optNotNull(obj).filter(e -> obj.size() > 0);
    }

    public static Optional<List<String>> optNotNullListStr(Object obj) {
        return RegexUtil.optNotNullListStr(DataChangeUtil.scdObjToListStr(obj));
    }

    public static boolean optIsPresentListStr(List<String> obj) {
        return RegexUtil.optNotNullListStr(obj).isPresent();
    }

    public static List<String> objToListStrOrExp(Object obj, String errInfoMsg) {
        return RegexUtil.optNotNullListStr(obj).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    public static List<String> optListStrOrExp(List<String> obj, String errInfoMsg) {
        return RegexUtil.optNotNullListStr(obj).orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    public static List<String> optListStrOrErr(List<String> obj, MethodParam methodParam, String errMsg) {
        return RegexUtil.optNotNullListStr(obj).orElseThrow(() -> new SenscloudError(methodParam, errMsg));
    }

    public static BigDecimal optBigDecimalOrNull(Object obj, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToBd(o, name)).orElse(null);
    }

    public static BigDecimal optBigDecimalOrVal(Object obj, String val, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToBd(o, name)).orElse(RegexUtil.changeToBd(val, name));
    }

    public static BigDecimal optBigDecimalOrVal(Object obj, BigDecimal val, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToBd(o, name)).orElse(val);
    }

    public static BigDecimal optInitBigDecimal(Object obj, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToBd(o, name)).orElse(BigDecimal.ZERO);
    }

    public static Date optDateChangeOrNull(Object obj, String name, String sdf) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToDate(o, name, sdf)).orElse(null); // codeUtil-sps-1
    }

    public static String optDateOrBlank(Object obj, SimpleDateFormat sdf) {
        return RegexUtil.optNotNull(obj).map(sdf::format).orElse("");
    }

    public static Float optFloatOrNull(Object obj, String name) {
        return RegexUtil.optNotBlankStrOpt(obj).map(o -> RegexUtil.changeToBd(o, name)).map(e -> e.setScale(2, BigDecimal.ROUND_HALF_UP)).map(BigDecimal::floatValue).orElse(null);
    }

    public static boolean optBool(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).filter(Boolean::new).isPresent();
    }

    public static void intExp(int obj, String errInfoMsg) {
        RegexUtil.trueExp(obj == 0, errInfoMsg);
    }

    public static void intErr(int obj, MethodParam methodParam, String errMsg, String remark, String expMsg) {
        RegexUtil.trueErr(obj == 0, methodParam, errMsg, remark, expMsg);
    }

    public static void intExp(int obj, String msgKey, String[] msgPrm) {
        RegexUtil.trueExp(obj == 0, msgKey, msgPrm);
    }

    public static void trueExp(boolean obj, String errInfoMsg) {
        if (obj) {
            throw new SenscloudException(errInfoMsg);
        }
    }

    public static void trueExp(boolean obj, String errInfoMsg, String remak) {
        if (obj) {
            throw new SenscloudException(errInfoMsg, remak);
        }
    }

    public static void trueExp(boolean obj, String msgKey, String[] msgPrm) {
        if (obj) {
            throw new SenscloudException(msgKey, msgPrm);
        }
    }

    public static void trueErr(boolean obj, MethodParam methodParam, String errMsg, String remark, String expMsg) {
        if (obj) {
            throw new SenscloudError(methodParam, errMsg, remark, expMsg);
        }
    }

    public static void falseExp(boolean obj, String errInfoMsg) {
        RegexUtil.trueExp(!obj, errInfoMsg);
    }

    public static void falseExp(boolean obj, String errInfoMsg, String remark) {
        RegexUtil.trueExp(!obj, errInfoMsg, remark);
    }

    public static void falseExp(boolean obj, String msgKey, String[] msgPrm) {
        RegexUtil.trueExp(!obj, msgKey, msgPrm);
    }

    public static boolean booleanCheck(Boolean obj) {
        return null != obj && obj;
    }

    public static JSONArray objToJaOrNew(Object obj) {
        return RegexUtil.optNotNull(obj).map(JSONArray::fromObject).orElseGet(JSONArray::new);
    }

    public static JSONArray stringToJaOrNew(String obj) {
        return RegexUtil.optNotNull(JSONArray.fromObject(obj)).orElseGet(JSONArray::new);
    }

    public static JSONArray optJaOrNew(JSONArray obj) {
        return RegexUtil.optNotNull(obj).orElseGet(JSONArray::new);
    }


    public static Optional<JSONArray> optNotNullJa(JSONArray obj) {
        return RegexUtil.optNotNull(obj).filter(a -> a.size() > 0);
    }

    public static Optional<JSONArray> optNotNullJa(Object obj) {
        return RegexUtil.optNotBlankStrOpt(obj).map(JSONArray::fromObject).filter(a -> a.size() > 0);
    }

    /**
     * 判断数值类型，包括整数和浮点数
     *
     * @param str
     * @return
     * @author jiqinlin
     */
    public final static boolean isNumeric(String str) {
        return isFloat(str) || isInteger(str);
    }

    /**
     * 匹配正浮点数
     *
     * @param str
     * @return
     * @author jiqinlin
     */
    public final static boolean isFloat(String str) {
        return match(str, "^[-\\+]?\\d+(\\.\\d+)?$");
    }

    /**
     * 匹配非负整数（正整数+0）
     *
     * @param str
     * @return
     * @author jiqinlin
     */
    public final static boolean isInteger(String str) {
        return match(str, "^[+]?\\d+$");
    }

    /**
     * 手机号码验证
     */
    public static String isMobile(String obj) {
        return RegexUtil.optNotBlankStrOpt(obj).filter(s -> 11 == s.length())
                .filter(s -> match(s, "^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\\d{8})$"))
                .orElseThrow(() -> new SenscloudException(LangConstant.TITLE_AAW_D, new String[]{RegexUtil.optStrOrBlank(obj), LangConstant.TITLE_AAT_F})); // DD【XX】为空或格式有误
    }

    /**
     * 手机号码验证
     */
    public static String isMobile(String obj, String errInfoMsg) {
        return RegexUtil.optNotBlankStrOpt(obj).filter(s -> 11 == s.length())
                .filter(s -> match(s, "^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\\d{8})$"))
                .orElseThrow(() -> new SenscloudException(errInfoMsg));
    }

    /**
     * 匹配密码，强验证，必填字母数字及特殊字符，且以字母开头，8位以上。
     *
     * @param str 密码
     */
    public static void isStrongPwd(String str) {
        RegexUtil.falseExp(match(str, "^(?![0-9]+$)(?![^0-9]+$)(?![a-zA-Z]+$)(?![^a-zA-Z]+$)(?![a-zA-Z0-9]+$)[a-zA-Z0-9\\S]{8,}$"), LangConstant.MSG_AG);
    }

    /**
     * 正则表达式匹配
     */
    private static boolean match(String text, String reg) {
        if (StringUtils.isBlank(text) || StringUtils.isBlank(reg)) {
            return false;
        }
        return Pattern.compile(reg).matcher(text).matches();
    }

    public static Integer changeToInteger(String obj, String name) {
        try {
            return Integer.valueOf(obj);
        } catch (Exception e) {
            throw new SenscloudException(LangConstant.TEXT_AF, new String[]{obj, RegexUtil.optStrOrBlank(name), LangConstant.TITLE_AAT_X}); // DD【XX】格式错误，应该是A格式DD
        }
    }

    public static Integer changeToIntegerOrExp(String obj, String errCode) {
        try {
            return Integer.valueOf(obj);
        } catch (Exception e) {
            throw new SenscloudException(LangConstant.MSG_HJ, new String[]{errCode}); // 请联系管理员调整页面：{d}
        }
    }

    private static Integer changeToInteger(String obj, MethodParam methodParam, String errMsg, String remark, String expMsg) {
        try {
            return Integer.valueOf(obj);
        } catch (Exception e) {
            throw new SenscloudError(methodParam, errMsg, RegexUtil.optNotBlankStrOpt(remark).map(r -> r.concat("：")).orElse("") + ErrorConstant.EC_DATA_1, expMsg);
        }
    }

    public static BigDecimal changeToBd(String obj, String name) {
        try {
            return new BigDecimal(obj);
        } catch (Exception e) {
            throw new SenscloudException(LangConstant.TEXT_AF, new String[]{obj, RegexUtil.optStrOrBlank(name), LangConstant.TITLE_AAT_W}); // DD【XX】格式错误，应该是A格式DD
        }
    }

    public static Date changeToDate(String obj, String name, String sdfStr) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(sdfStr);
            return sdf.parse(obj); // codeUtil-sps-1
        } catch (Exception e) {
            throw new SenscloudException(LangConstant.TEXT_AF, new String[]{obj, RegexUtil.optStrOrBlank(name), LangConstant.TITLE_DATE_T, sdfStr}); // DD【XX】格式错误，应该是A格式DD
        }
    }

    public static void optCheckError(Object errMsg, String msgCode) {
        String remark = RegexUtil.optStrOrBlank(errMsg);
        falseExp("".equals(remark), msgCode, remark);
    }
}
