package com.gengyun.senscloud.util.excel;

/**
 * @author senscloud
 * @project freemarker-excel
 * @description: 自定义解析excel的Worksheet类
 */
public class ExcelWorksheet {

    private String Name;

    private ExcelTable excelTable;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ExcelTable getExcelTable() {
        return excelTable;
    }

    public void setExcelTable(ExcelTable excelTable) {
        this.excelTable = excelTable;
    }
}
