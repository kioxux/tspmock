package com.gengyun.senscloud.util.wx.gzh;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 微信公众号工具类
 */
public class WxGzhUtil {
    private static final Logger logger = LoggerFactory.getLogger(WxGzhUtil.class);

    /**
     * accesstoken超时过期异常
     */
    public static final int ERRCODE_ACCESS_TOKEN_EXPIRED = 42001;

    /**
     * 获取微信Token信息(access_token的有效期目前为2个小时，需定时刷新)
     *
     * @param appId  AppID
     * @param secret secret
     * @return 微信Token信息
     */
    public static String getAccessToken(String appId, String secret) {
        String result = HttpRequestUtils.requestByGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret);
        JSONObject jo = RegexUtil.optNotBlankStrOpt(result).map(JSONObject::fromObject).orElseThrow(() -> new SenscloudException(ErrorConstant.EC_WX_GZH_016));
        RegexUtil.optCheckError(jo.get("errmsg"), ErrorConstant.EC_WX_GZH_017);
        return RegexUtil.optStrOrBlank(jo.get("access_token"));
    }

    /**
     * 发送微信模板消息
     *
     * @param accessToken accessToken
     * @param jsonData    条件
     */
    public static void sendTemplateMessage(String accessToken, String jsonData) {
        jsonData = jsonData.replace("\\\\t", "\\t").replace("\\\\n", "\\n");
        String result = HttpRequestUtils.requestJsonByPost("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken, jsonData);
        JSONObject jo = RegexUtil.optNotBlankStrOpt(result).map(JSONObject::fromObject).orElseThrow(() -> new SenscloudException(ErrorConstant.EC_WX_GZH_001));
        RegexUtil.optNotBlankStrOpt(jo.get("errcode")).filter(RegexUtil::optIsPresentStr).filter("0"::equals).orElseThrow(() -> new SenscloudException(ErrorConstant.EC_WX_GZH_002, result));
    }

    /**
     * 微信公众号-拉取用户信息
     *
     * @param accessToken accessToken
     * @param openId      openId
     * @return 用户信息
     */
    public static String getUserClientInfo(String accessToken, String openId) {
        String info = HttpRequestUtils.requestByGet("https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + accessToken + "&openid=" + openId + "&lang=zh_CN");
        return RegexUtil.optNotBlankStrOpt(info).map(JSONObject::fromObject).filter(jo -> !RegexUtil.optNotBlankStrOpt(jo.get("errmsg")).map(em -> {
            logger.error("wxGzhMsgErr-1002:" + em);
            return em;
        }).isPresent()).map(jo -> info).orElse("");
    }

    public static void main(String[] args) {
//        String appId = "wxac3af1a3257faea2";
//        String secret = "b95f98eafcc1135a807aca14908de34b";
//        String openId = "oLkVst_1yTvOnllde4xx6k5sm3HM";
////        String access_token = getAccessToken(appId, secret);
//        String access_token = "36_v9BLXUWeHNGmoFYxJht2sC8z0jQpvO-IoH2jrWXnbP6x_us-Bsi6z8hl82qSDptQUqtC0plkBhLsXbO7IaDnHK50AlCCL4W7fMvfCHNZiTHCbiJ26yp3o61V_6fubUundQ0TzWOD_L2cPbbiRILhAGAZQT";
//        logger.info("access_token:" + access_token);
//        String userInfo = getUserClientInfo(access_token, openId);
//        logger.info("userInfo:" + userInfo);
//
//        Map<String, Object> map = new HashMap<>();
//        String touser = "oLkVst_1yTvOnllde4xx6k5sm3HM";
//        String template_id = "d0Aj5uDHUJUSAujXqCc3oyOcvThDsd7vPkWmELWw7d8";
//        String url = "";
//        String topcolor = "#FF0000";
//        JSONObject data = JSONObject.fromObject("{\"first\":{\"value\":\"您有新的工单需要处理！\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"WD00202008250001\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"电脑蓝屏\",\"color\":\"#173177\"},\"keyword3\":{\"value\":\"设备处\",\"color\":\"#173177\"},\"keyword4\":{\"value\":\"张三（15032681150）\",\"color\":\"#173177\"},\"keyword5\":{\"value\":\"李四\",\"color\":\"#173177\"},\"remark\":{\"value\":\"点击这里查看并接受工单任务！\",\"color\":\"#173177\"}}");
//        boolean result = sendTemplateMessage(access_token, new TemplateMessage(touser, template_id, url, topcolor, data).toString());
//        logger.info("result:" + result);
    }

}
