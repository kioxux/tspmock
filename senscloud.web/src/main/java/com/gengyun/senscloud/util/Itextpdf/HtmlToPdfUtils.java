package com.gengyun.senscloud.util.Itextpdf;

//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.pdf.BaseFont;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.xhtmlrenderer.pdf.ITextFontResolver;
//import org.xhtmlrenderer.pdf.ITextRenderer;
//
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.*;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.net.URLConnection;
//
///**
// * html转pdf工具——itextpdf
// */
public class HtmlToPdfUtils {
//
//    private static final Logger logger = LoggerFactory.getLogger(HtmlToPdfUtils.class);
//
//    public static final String FILE_SUFFIXES_PDF = "pdf";
//    public static final String FILE_SUFFIXES_HTML = "html";
//
//    /**
//     * html转换成PDF
//     * @param htmlFile html文件
//     * @param pdfPath  pdf路径
//     * @param delHtml  是否删除html页面
//     * @throws Exception 异常
//     */
//    public static void htmlToPdf(File htmlFile, String pdfPath, String baseUrl, boolean hasImage, boolean delHtml) throws Exception {
//        OutputStream os = null;
//        try {
//            os = new FileOutputStream(pdfPath);
//            ITextRenderer iTextRenderer = new ITextRenderer();
//            iTextRenderer.setDocument(htmlFile);
//            //解决中文编码
//            ITextFontResolver fontResolver = iTextRenderer.getFontResolver();
//            if ("linux".equals(getCurrentOperationSystem())) {
//                fontResolver.addFont("/usr/share/fonts/chiness/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            } else {
//                fontResolver.addFont("c:/Windows/Fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//            }
//            if(StringUtils.isNotBlank(baseUrl))
//                iTextRenderer.getSharedContext().setBaseURL(baseUrl);
//
//            // 图片base64支持，把图片转换为itext自己的图片对象
//            if(hasImage) {// 如果携带图片则加上以下代码,将图片标签转换为Itext自己的图片对象
//                iTextRenderer.getSharedContext().setReplacedElementFactory(new Base64ImgReplacedElementFactory());
//                iTextRenderer.getSharedContext().getTextRenderer().setSmoothingThreshold(0);
//            }
//            iTextRenderer.layout();
//            iTextRenderer.createPDF(os);
//            if(delHtml)
//                htmlFile.delete();//删除临时html文件
//        } catch (IOException e) {
//            logger.error("", e);
//        } catch (DocumentException e) {
//            logger.error("", e);
//        } finally {
//            if(os != null) {
//                os.flush();
//                os.close();
//            }
//        }
//    }
//
//    private static String getCurrentOperationSystem() {
//        String os = System.getProperty("os.name").toLowerCase();
//        return os;
//    }
//
//    /**
//     * 说明：根据指定URL将HTML文件下载到指定目标位置
//     *
//     * @param urlPath
//     *            下载路径
//     * @param htmlPath
//     *            文件存放目录
//     * @return 返回下载文件
//     */
//    public static File getHtmlFile(HttpServletRequest request, String urlPath, String htmlPath) {
//        File file = null;
//        try {
//            InputStream in = null;
//            BufferedInputStream bin = null;
//            OutputStream out = null;
//            try {
//                // 统一资源
//                URL url = new URL(urlPath);
//                // 连接类的父类，抽象类
//                URLConnection urlConnection = url.openConnection();
//                // http的连接类
//                HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
//                //设置超时
//                httpURLConnection.setConnectTimeout(1000*5);
//                //设置请求方式，默认是GET
//                httpURLConnection.setRequestMethod("POST");
//                // 设置字符编码
//                httpURLConnection.setRequestProperty("Charset", "UTF-8");
//                //保存session信息
//                httpURLConnection.setRequestProperty("Cookie", "JSESSIONID="+request.getRequestedSessionId());
//                // 打开到此 URL引用的资源的通信链接（如果尚未建立这样的连接）。
//                httpURLConnection.connect();
//                in = httpURLConnection.getInputStream();
//                bin = new BufferedInputStream(in);
//                // 指定文件名称、存放位置
//                file = new File(htmlPath);
//                // 校验文件夹目录是否存在，不存在就创建一个目录
//                if (!file.getParentFile().exists()) {
//                    file.getParentFile().mkdirs();
//                }
//
//                out = new FileOutputStream(file);
//                int size = 0;
//                byte[] buf = new byte[2048];
//                while ((size = bin.read(buf)) != -1) {
//                    out.write(buf, 0, size);
//                }
//            } catch (IOException e) {
//                logger.error("", e);
//            } finally {
//                // 关闭资源
//                if(out != null){
//                    out.flush();
//                    out.close();
//                }
//                if(bin != null)
//                    bin.close();
//
//                if(in != null)
//                    in.close();
//            }
//        } catch (MalformedURLException e) {
//            logger.error("", e);
//        } catch (IOException e) {
//            logger.error("", e);
//        } finally {
//            return file;
//        }
//    }
//
//    /**
//     * 页面下载pdf文件
//     * @param file
//     * @param response
//     * @param delFlag
//     * @throws Exception
//     */
//    public static void pdfDownload(File file, HttpServletResponse response, boolean delFlag) throws Exception{
//        // 下载文件
//        FileInputStream inputStream = null;
//        ServletOutputStream outputStream = null;
//        if(!file.exists())
//            return;
//
//        try {
//            response.reset();
//            //设置响应类型	PDF文件为"application/pdf"，WORD文件为："application/msword"， EXCEL文件为："application/vnd.ms-excel"。
//            response.setContentType("application/pdf;charset=utf-8");
//            //attachment作为附件下载；inline客户端机器有安装匹配程序，则直接打开；注意改变配置，清除缓存，否则可能不能看到效果
//            response.addHeader("Content-Disposition",   "attachment;filename="+file.getName());
//
//            //将文件读入响应流
//            inputStream = new FileInputStream(file);
//            outputStream = response.getOutputStream();
//            int length = 1024;
//            int readLength=0;
//            byte buf[] = new byte[1024];
//            readLength = inputStream.read(buf, 0, length);
//            while (readLength != -1) {
//                outputStream.write(buf, 0, readLength);
//                readLength = inputStream.read(buf, 0, length);
//            }
//        } catch (Exception e) {
//            logger.error("", e);
//        } finally {
//            try {
//                outputStream.flush();
//            } catch (IOException e) {
//                logger.error("", e);
//            }
//            try {
//                outputStream.close();
//            } catch (IOException e) {
//                logger.error("", e);
//            }
//            try {
//                inputStream.close();
//            } catch (IOException e) {
//                logger.error("", e);
//            }
//            //删除原文件
//            if(delFlag) {
//                file.delete();
//            }
//        }
//    }
//
//    public static void main(String[] args){
//        File file = new File("E:\\test.html");
//        String pdfPath = "E:\\testPdf.pdf";
//        try{
//            htmlToPdf(file, pdfPath, null, false, false);
//        }catch (Exception e){
//            logger.error("", e);
//        }
//    }
}
