package com.gengyun.senscloud.util;

//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.imageio.ImageIO;
//import javax.imageio.stream.ImageOutputStream;
//import java.awt.*;
//import java.awt.image.BufferedImage;
//import java.io.*;

public class imgMarkUtil {
//    /**
//     * 图片添加水印
//     *
//     * @param srcImgPath
//     *            需要添加水印的图片的路径
//     * @param outImgPath
//     *            添加水印后图片输出路径
//     * @param markContentColor
//     *            水印文字的颜色
//     * @param waterMarkContent
//     *            水印的文字
//     */
//    public static void markFromPath(String srcImgPath, String outImgPath, Color markContentColor, String waterMarkContent) {
//        try {
//            // 读取原图片信息
//            File srcImgFile = new File(srcImgPath);
//            Image srcImg = ImageIO.read(srcImgFile);
//            markFromFile(srcImgFile, outImgPath, markContentColor, waterMarkContent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    /**
//     * 图片添加水印
//     *
//     * @param srcImgFile
//     *            需要添加水印的图片的路径
//     * @param outImgPath
//     *            添加水印后图片输出路径
//     * @param markContentColor
//     *            水印文字的颜色
//     * @param waterMarkContent
//     *            水印的文字
//     */
//    public static void markFromFile(File srcImgFile, String outImgPath, Color markContentColor, String waterMarkContent) {
//        try {
//            // 读取原图片信息
//            Image srcImg = ImageIO.read(srcImgFile);
//            int srcImgWidth = srcImg.getWidth(null);
//            int srcImgHeight = srcImg.getHeight(null);
//            // 加水印
//            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
//            Graphics2D g = bufImg.createGraphics();
//            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
//            // Font font = new Font("Courier New", Font.PLAIN, 12);
//            Font font = new Font("宋体", Font.PLAIN, 20);
//            g.setColor(markContentColor); // 根据图片的背景设置水印颜色
//
//            g.setFont(font);
//            int x = srcImgWidth - getWatermarkLength(waterMarkContent, g) - 3;
//            int y = srcImgHeight - 3;
//            // int x = (srcImgWidth - getWatermarkLength(watermarkStr, g)) / 2;
//            // int y = srcImgHeight / 2;
//            g.drawString(waterMarkContent, x, y);
//            g.dispose();
//            // 输出图片
//            FileOutputStream outImgStream = new FileOutputStream(outImgPath);
//            ImageIO.write(bufImg, "jpg", outImgStream);
//            outImgStream.flush();
//            outImgStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    /**
//     * 直接给multipartFile加上文字水印再进行保存图片的操作方便省事
//     *
//     * @param multipartFile
//     *            文件上传的对象
//     * @param word
//     *            水印文件的路径 如果是相对路径请使用相对路径new Image的方法,此处用的是url
//     * @return
//     * @throws IOException
//     * @author zys
//     */
//    public static MultipartFile addWorkMarkToMutipartFile(MultipartFile multipartFile, String word,Font font,Color markContentColor) {
//
//        // 获取图片文件名 xxx.png xxx
//        String originFileName = multipartFile.getOriginalFilename();
//        // 获取原图片后缀 png
//        int lastSplit = originFileName.lastIndexOf(".");
//        String suffix = originFileName.substring(lastSplit + 1);
//        // 获取图片原始信息
//        String dOriginFileName = multipartFile.getOriginalFilename();
//        String dContentType = multipartFile.getContentType();
//        // 是图片且不是gif才加水印
//        try {
//            if (!suffix.equalsIgnoreCase("gif") && dContentType.contains("image")) {
//                // 获取水印图片
//
//                InputStream inputImg = multipartFile.getInputStream();
//                Image img = ImageIO.read(inputImg);
//                // 加图片水印
//                int imgWidth = img.getWidth(null);
//                int imgHeight = img.getHeight(null);
//
//                BufferedImage bufImg = new BufferedImage(imgWidth, imgHeight,
//                        BufferedImage.TYPE_INT_RGB);
//                //调用画文字水印的方法 imgWidth-20 imgHeight-14将字体置于右下角将坐标替换
//                markWord(bufImg, img, word, font, markContentColor, imgWidth-240, imgHeight-14);
//                ByteArrayOutputStream bs = new ByteArrayOutputStream();
//                ImageOutputStream imOut = ImageIO.createImageOutputStream(bs);
//                ImageIO.write(bufImg, suffix, imOut);
//                InputStream is = new ByteArrayInputStream(bs.toByteArray());
//
//                // 加水印后的文件上传
//                MultipartFile newMultipartFile = new MockMultipartFile(dOriginFileName, dOriginFileName, dContentType,
//                        is);
//                return newMultipartFile;
//            }
//            //返回加了水印的上传对象
//            return multipartFile;
//        } catch (IOException e) {
//            return multipartFile;
//        } catch (Exception e) {
//            return multipartFile;
//        }
//    }
//
//    /**
//     * 加文字水印
//     * @param bufImg --BufferedImage  用来画图的宽高跟需要加水印的图片一样的空白图
//     * @param img --需要加水印的图片
//     * @param text --水印文字
//     * @param font --字体
//     * @param color --颜色
//     * @param x  --水印相对于底片的x轴坐标(PS:左上角为(0,0))
//     * @param y  --水印相对于底片的y轴坐标(PS:左上角为(0,0))
//     * @author zys
//     */
//    public static void markWord(BufferedImage bufImg, Image img, String text, Font font, Color color, int x, int y) {
//        //取到画笔
//        Graphics2D g = bufImg.createGraphics();
//        //画底片
//        g.drawImage(img, 0, 0, bufImg.getWidth(), bufImg.getHeight(), null);
//        g.setColor(color);
//        g.setFont(font);
//        //位置
//        g.drawString(text, x, y);
//        g.dispose();
//    }
//
//
//    /**
//     * 获取水印文字总长度
//     *
//     * @param waterMarkContent
//     *            水印的文字
//     * @param g
//     * @return 水印文字总长度
//     */
//    public static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
//        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
//    }
}
