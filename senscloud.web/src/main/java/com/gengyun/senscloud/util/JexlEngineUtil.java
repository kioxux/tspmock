package com.gengyun.senscloud.util;
//
//import org.apache.commons.jexl3.*;
//import org.apache.commons.lang.StringUtils;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
///**
// * 公式计算工具类
// */
public class JexlEngineUtil {
//
//    /**
//     * 判断条件公式是否成立
//     * @param expression    字符串公式
//     * @param score         公式中需要替换的评分
//     * @return
//     */
//    public static Boolean compareExpression(String expression, BigDecimal score){
//        if(StringUtils.isBlank(expression) || score == null)
//            return null;
//
//        try {
//            Map<String, Object> map = new HashMap<>();
//            map.put("S", score);
//            expression = convertNumberToBigDecimal(expression);
//            return (Boolean)convertToCode(expression, map);
//        } catch (Exception e) {
//        }
//        return null;
//    }
//
//    /**
//     * 计算公式值(默认精确到小数点后两位，四舍五入)
//     * @param expression    字符串公式
//     * @param score         公式中需要替换的评分
//     * @return
//     */
//    public static BigDecimal executeExpression(String expression, BigDecimal score){
//        return executeExpression(expression, score, 2, RoundingMode.HALF_UP);
//    }
//
//    /**
//     * 计算公式值
//     * @param expression    字符串公式
//     * @param score         公式中需要替换的评分
//     * @param scale         小数点后精确位数
//     * @param roundingMode  小数的保留模式
//     * @return
//     */
//    public static BigDecimal executeExpression(String expression, BigDecimal score, int scale, RoundingMode roundingMode){
//        if(StringUtils.isBlank(expression) || score == null)
//            return null;
//
//        try {
//            Map<String, Object> map = new HashMap<>();
//            map.put("S", score);
//            expression = convertNumberToBigDecimal(expression);
//            BigDecimal result = (BigDecimal)convertToCode(expression, map);
//            return result.setScale(scale, roundingMode);
//        } catch (Exception e) {
//        }
//        return null;
//    }
//
//    /**
//     * 解析字符串，把数字类型指定为BigDecimal
//     * @param expression 字符串公式
//     * @return
//     */
//    private static String convertNumberToBigDecimal(String expression){
//        Matcher matcher = Pattern.compile("(\\d+([.]\\d+)?)[%]?").matcher(expression);
//        StringBuffer sb = new StringBuffer();
//        while (matcher.find()) {
//            String number = matcher.group();
//            if(number.endsWith("%"))
//                matcher.appendReplacement(sb, "("+number.substring(0, number.length() - 1)+"b/100b)");//“%”需要替换折算
//            else
//                matcher.appendReplacement(sb, number+"b");//如果是数字，则在数字后添加“b”，指定数字为BigDecimal类型
//        }
//        matcher.appendTail(sb);
//        return sb.toString();
//    }
//
//    /**
//     * 字符串解析、执行
//     * @param jexlExp 字符串公式
//     * @param map   替换值数组
//     * @return
//     */
//    private static Object convertToCode(String jexlExp, Map<String, Object> map) {
//        JexlEngine jexl = new JexlBuilder().create();
//        JexlExpression e = jexl.createExpression(jexlExp);
//        JexlContext jc = new MapContext();
//        for (String key : map.keySet()) {
//            jc.set(key, map.get(key));
//        }
//        if (null == e.evaluate(jc)) {
//            return "";
//        }
//        return e.evaluate(jc);
//    }
//
//    public static void main(String[] args) {
////        Map<String, Object> map = new HashMap<>();
////        map.put("k", new BigDecimal(10));
////        map.put("x", new BigDecimal(2));
////        map.put("y", new BigDecimal(4));
////        map.put("percent", new BigDecimal(100));
////        String expression = "k-(x-y)*10%".replaceAll("%", "/percent");
////        Object obj = convertToCode(expression, map);
////        System.out.println("execute result: " + obj);
////
////        Matcher matcher = Pattern.compile("\\d+[%]?").matcher("10-(2-4)*10%");
////        StringBuffer sb = new StringBuffer();
////        while (matcher.find()) {
////            String number = matcher.group();
////            if(number.endsWith("%"))
////                matcher.appendReplacement(sb, "("+number.substring(0, number.length() - 1)+"b/100b)");
////            else
////                matcher.appendReplacement(sb, number+"b");
////        }
////        matcher.appendTail(sb);
////        System.out.println("sb :"+sb.toString());
//
//
////        System.out.println(" compareExpression "+compareExpression("S>60&&S<=80", new BigDecimal(80)));
////
////        System.out.println("final score:"+executeExpression("(S-10)*70%", new BigDecimal(80)));
//
//
//        System.out.println("  " + convertNumberToBigDecimal("S*0.6"));
//    }

}
