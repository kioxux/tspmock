package com.gengyun.senscloud.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.compress.archivers.zip.ParallelScatterZipCreator;
import org.apache.commons.compress.archivers.zip.UnixStat;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.parallel.InputStreamSupplier;
import org.apache.commons.io.input.NullInputStream;

public class ZipUtils {
    private static List<String> index;
    private static int count = 0;

    public static void doCompress(List<Map<String, String>> pathnames, OutputStream out) throws IOException {
        index = new ArrayList<>();
        count = 0;
        ThreadFactory factory = new ThreadFactoryBuilder().setNameFormat("compressFileList-pool-").build();
        ExecutorService executor = new ThreadPoolExecutor(20, 100, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10000), factory);
        ParallelScatterZipCreator parallelScatterZipCreator = new ParallelScatterZipCreator(executor);

        ZipArchiveOutputStream zipArchiveOutputStream = new ZipArchiveOutputStream(out);
        for (Map<String, String> path : pathnames) {
            doCompress(new File(RegexUtil.optStrOrBlank(path.get("file_url"))), zipArchiveOutputStream, RegexUtil.optStrOrBlank(path.get("file_name")), parallelScatterZipCreator);
        }
        try {
            parallelScatterZipCreator.writeTo(zipArchiveOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            zipArchiveOutputStream.close();
        }
    }

    public static void doCompress(File file, ZipArchiveOutputStream out, String fileName, ParallelScatterZipCreator parallelScatterZipCreator) throws IOException {
        if (file.exists()) {
            final InputStreamSupplier inputStreamSupplier = () -> {
                try {
                    return new FileInputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return new NullInputStream(0);
                }
            };
            if (index.contains(fileName)) {
                count++;
                fileName = fileName.concat("(" + count + ")");
            }
            ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(RegexUtil.optStrOrVal(fileName.concat(file.getName().substring(file.getName().lastIndexOf("."))), fileName));
            zipArchiveEntry.setMethod(ZipArchiveEntry.DEFLATED);
            zipArchiveEntry.setSize(file.length());
            zipArchiveEntry.setUnixMode(UnixStat.FILE_FLAG | 436);
            parallelScatterZipCreator.addArchiveEntry(zipArchiveEntry, inputStreamSupplier);
            index.add(fileName);
        }
    }
}
