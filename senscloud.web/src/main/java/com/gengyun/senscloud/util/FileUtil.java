package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.LangConstant;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;

import java.io.*;
import java.util.List;

/**
 * 文件处理工具
 */
public class FileUtil {
    /**
     * excel导出
     *
     * @param filePath 路径
     * @param workbook 数据
     */
    public static void doExportExcel(String filePath, Workbook workbook) {
        File file = new File(filePath);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            workbook.write(fos);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
            try {
                if (null != fos) {
                    fos.close();
                    fos.flush();
                }
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * 文件压缩
     * @param zipfullPath
     * @param fileFullPaths
     */
    public static void createZip(String zipfullPath, List<String> fileFullPaths) {
        InputStream inputStream = null;
        ZipOutputStream zip = null;
        try {
            zip = new ZipOutputStream(new FileOutputStream(zipfullPath));
            zip.setEncoding("utf-8");

            for(String fullPath:fileFullPaths) {

                //创建文件
                File file = new File(fullPath);
                String fileName = file.getName();

                //读文件流
                inputStream = new BufferedInputStream(new FileInputStream(file));
                byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                inputStream.close();

                //将读取的文件输出到zip中
                zip.putNextEntry(new ZipEntry(fileName));
                zip.write(buffer);
                zip.closeEntry();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (zip != null) {
                    zip.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public static void initDir(String path){

        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }


    }

    /**
     *
     * @param dir
     */
    public static void deleteDir(String dir){
        FileDeleteStrategy strategy = FileDeleteStrategy.FORCE;
        try {
            File file = new File(dir);
            if(file!=null && file.exists()) {
                strategy.delete(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}