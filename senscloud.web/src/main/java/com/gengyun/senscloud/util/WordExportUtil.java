package com.gengyun.senscloud.util;


import com.gengyun.senscloud.util.excel.ExcelWriter;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.Map;


/**
 * word导出工具
 */
public class WordExportUtil {
    private static final Logger logger = LoggerFactory.getLogger(WordExportUtil.class);

    /**
     * 生成word
     *
     * @param map
     * @param templateName
     * @param type
     * @param basePath
     * @param filePath
     * @return
     */
    public static void createWordByData(Map<String, Object> map, String templateName, String type, String basePath, String filePath, String sep) {
        RegexUtil.optMapOrExpNullInfo(map, "");
        String templatePath = sep;//+ "template" + sep;
        if (RegexUtil.optIsPresentStr(type)) {
            templatePath = templatePath + type + sep;
        }
        Writer out = null;
        OutputStreamWriter osw = null;
        FileOutputStream fos = null;
        try {
            String encoding = "utf-8";
//            String encoding = "GB2312";
            Configuration configuration = new Configuration(new Version("2.3.26"));
            configuration.setClassicCompatible(true);
            configuration.setDefaultEncoding(encoding);
            configuration.setDirectoryForTemplateLoading(new File(basePath + templatePath));
            File outFile = new File(filePath);
            fos = new FileOutputStream(outFile);
            osw = new OutputStreamWriter(fos, encoding);
            out = new BufferedWriter(osw, 10240);
            Template template = configuration.getTemplate(templateName, encoding);
            template.process(map, out);
            out.flush();
        } catch (Exception e) {
            logger.error(filePath + "-wordCreateError");
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(fos);
            IOUtils.closeQuietly(osw);
            IOUtils.closeQuietly(out);
        }
        ExcelWriter.writeExcel(filePath, map);
    }

    /**
     * 图片转base64的字符串
     *
     * @param imageAbsolutePath 图片路径
     * @return String
     */
    public static String imageToBase64Str(String imageAbsolutePath) {
        InputStream is = null;
        byte[] data = null;
        try {
            is = new FileInputStream(imageAbsolutePath);
            data = new byte[is.available()];
            is.read(data);
            is.close();
        } catch (Exception e) {
            logger.error("imgPath2base64===:" + e);
            e.printStackTrace();
            return null;
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }


    public static boolean createWord(Map<String, Object> dataMap, String ftlFolderPath, String tempName, String outputFilePath) {
        logger.info("开始创建word到本地");
        boolean result = false;
        Configuration configuration = initConfiguration(ftlFolderPath);
        Template t = null;
        try {
            t = configuration.getTemplate(tempName, "UTF-8");
            t.setEncoding("utf-8");
//            t = configuration.getTemplate(tempName, "GB2312");
//            t.setEncoding("GB2312");

        } catch (IOException e) {
            logger.error("在路径：{}里找不到：{}", ftlFolderPath, tempName);
            e.printStackTrace();
        }
        File outFile = new File(outputFilePath);
        Writer out = null;
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(outFile);
            OutputStreamWriter oWriter = new OutputStreamWriter(fos, "UTF-8");
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
//            OutputStreamWriter oWriter = new OutputStreamWriter(fos, "GB2312");
//            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"GB2312"));

            t.process(dataMap, out);
            out.close();
            fos.close();
            result = true;
            logger.info("word创建成功");
        } catch (FileNotFoundException e) {
            logger.error("文件不存在：{}", outFile);
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            logger.error("未知的编码格式");
            e.printStackTrace();
        } catch (TemplateException e) {
            logger.error("模板异常");
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("IO异常");
            e.printStackTrace();
        }

        return result;
    }

    private static Configuration initConfiguration(String folderPath) {
        Configuration configuration = new Configuration();
        configuration.setDefaultEncoding("utf-8");
//        configuration.setDefaultEncoding("GB2312");
        try {
            configuration.setDirectoryForTemplateLoading(new File(folderPath));
        } catch (IOException e) {
            logger.error("初始化configuration失败，路径不存在：{}", folderPath);
            e.printStackTrace();
        }
        return configuration;
    }
}
