package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.barcodes.BarcodeQRCode;
import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.List;

@Api(tags = "二维码工具")
public class QRCodeUtil {
    /**
     * 二维码展示
     *
     * @param info 二维码内容
     */
    public static void doShowQRCode(String info) {
        RegexUtil.optNotNull(HttpRequestUtils.getResponse()).ifPresent(rp -> {
            ServletOutputStream stream = null;
            try {
                stream = rp.getOutputStream();
                QRCodeWriter writer = new QRCodeWriter();
                BitMatrix m = writer.encode(info, BarcodeFormat.QR_CODE, 128, 128);
                MatrixToImageWriter.writeToStream(m, "png", stream);
            } catch (Exception e) {
                e.printStackTrace();
//                throw new SenscloudException(LangConstant.QRCODE_DEAL_FAILURE); // 二维码处理未成功
            } finally {
                try {
                    if (stream != null) {
                        stream.flush();
                        stream.close();
                    }
                } catch (Exception stExp) {
                    stExp.printStackTrace();
                }
            }
        });
    }

    @ApiOperation(value = "下载二维码PDF", notes = "二维码下载")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dataList", value = "数据信息", required = true, paramType = "List<Map<String, Object>>"),
            @ApiImplicitParam(name = "qrCodePrefix", value = "二维码前缀"),
            @ApiImplicitParam(name = "companyId", value = "企业id"),
            @ApiImplicitParam(name = "companyName", value = "企业名称"),
            @ApiImplicitParam(name = "titleFirst", value = "标题1"),
            @ApiImplicitParam(name = "titleSecond", value = "标题2"),
            @ApiImplicitParam(name = "titleThird", value = "标题3"),
            @ApiImplicitParam(name = "titleFourth", value = "标题4"),
    })
    public static void doDownloadQRCodePdf(Map<String, Object> paramMap) {
        Rectangle pageSize = new Rectangle(300.0F, 160.8F);
        Document document = new Document(pageSize);
        try {
            HttpServletResponse response = HttpRequestUtils.getResponse();
            response.setContentType("application/x-download");//下面三行是关键代码，处理乱码问题
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment; filename=" + getQRCodePdfName());
            //IText API

            //初始化pdfdocument纵向A4尺寸
            //Document document = new Document(new RectangleReadOnly(300.0F, 160.8F));
            // com.itextpdf.text.pdf.PdfWriter.getInstance(document, response.getOutputStream());
            //
            com.itextpdf.text.pdf.PdfWriter.getInstance(document, response.getOutputStream());
            document.setMargins(5, 5, 5, 5);
            //设置字体
            com.itextpdf.text.pdf.BaseFont bfChinese = com.itextpdf.text.pdf.BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", com.itextpdf.text.pdf.BaseFont.EMBEDDED);
            com.itextpdf.text.Font FontChinese16 = new com.itextpdf.text.Font(bfChinese, 11, Font.BOLD);
            com.itextpdf.text.BaseColor ColorChinese = new com.itextpdf.text.BaseColor(0, 0, 0);
            com.itextpdf.text.Font FontChinese10 = new com.itextpdf.text.Font(bfChinese, 8, Font.BOLD, ColorChinese);
            //获取所有设备信息
            List<Map<String, Object>> dataList = DataChangeUtil.scdObjToList(paramMap.get("dataList"));
            Map<String, Object> fieldInfo = RegexUtil.optMapOrNew(DataChangeUtil.scdObjToMap(paramMap.get("fieldInfo")));
//            String qr_code_prefix = RegexUtil.optStrOrBlank(paramMap.get("qrCodePrefix"));
//            String companyId = RegexUtil.optStrOrBlank(paramMap.get("companyId"));
//            String qrCodePrefix = RegexUtil.optIsPresentStr(qr_code_prefix) && RegexUtil.optIsPresentStr(companyId) ? qr_code_prefix + companyId + "/" : "";
            String titleFirst = RegexUtil.optStrOrBlank(paramMap.get("titleFirst"));
            String titleSecond = RegexUtil.optStrOrBlank(paramMap.get("titleSecond"));
            String titleThird = RegexUtil.optStrOrBlank(paramMap.get("titleThird"));
            String titleFourth = RegexUtil.optStrOrBlank(paramMap.get("titleFourth"));
            String titleMain = RegexUtil.optStrOrBlank(paramMap.get("titleMain"));
            String titleSub = RegexUtil.optStrOrBlank(paramMap.get("titleSub"));
            String topLeft = RegexUtil.optStrOrBlank(paramMap.get("titleTopLeft"));
            topLeft = "titleTopLeft".equals(topLeft) || "".equals(topLeft) ? "" : topLeft + ":";
            String btmLeft = RegexUtil.optStrOrBlank(paramMap.get("titleBtmLeft"));
            btmLeft = "titleBtmLeft".equals(btmLeft) || "".equals(btmLeft) ? "" : btmLeft + ":";
            String btmRight = RegexUtil.optStrOrBlank(paramMap.get("titleBtmRight"));
            btmRight = "titleBtmRight".equals(btmRight) || "".equals(btmRight) ? "" : btmRight + ":";

            String titleFirstField = RegexUtil.optStrOrVal(fieldInfo.get("titleFirst"), "titleFirst");
            String titleSecondField = RegexUtil.optStrOrVal(fieldInfo.get("titleSecond"), "titleSecond");
            String titleThirdField = RegexUtil.optStrOrVal(fieldInfo.get("titleThird"), "titleThird");
            String titleFourthField = RegexUtil.optStrOrVal(fieldInfo.get("titleFourth"), "titleFourth");
            String titleTopLeftField = RegexUtil.optStrOrVal(fieldInfo.get("titleTopLeft"), "titleTopLeft");
            String titleBtmLeftField = RegexUtil.optStrOrVal(fieldInfo.get("titleBtmLeft"), "titleBtmLeft");
            String titleBtmRightField = RegexUtil.optStrOrVal(fieldInfo.get("titleBtmRight"), "titleBtmRight");

            String code;
            String valueFirst, valueSecond = "",
                    valueThird = "", valueFourth = "", valueTopLeft = "", valueBtmLeft = "", valueBtmRight = "";
            PdfPTable table4 = new PdfPTable(1);
            table4.setTotalWidth(290);
            table4.setLockedWidth(true);
            int index = 1;
            //打开
            document.open();
            //左边总表格
            //遍历所有设备，
            for (Map<String, Object> data : dataList) {
                valueFirst = RegexUtil.optStrOrBlank(data.get(titleFirstField));
                valueFirst = valueFirst.contains(",") ? valueFirst.split(",")[0] : valueFirst;
                valueSecond = RegexUtil.optStrOrBlank(data.get(titleSecondField));
                valueSecond = valueSecond.contains(",") ? valueSecond.split(",")[0] : valueSecond;
                valueThird = RegexUtil.optStrOrBlank(data.get(titleThirdField));
                valueThird = valueThird.contains(",") ? valueThird.split(",")[0] : valueThird;
                valueFourth = RegexUtil.optStrOrBlank(data.get(titleFourthField));
                valueFourth = valueFourth.contains(",") ? valueFourth.split(",")[0] : valueFourth;
                valueTopLeft = RegexUtil.optStrOrBlank(data.get(titleTopLeftField));
                valueTopLeft = topLeft + RegexUtil.optStrOrBlank(valueTopLeft.contains(",") ? valueTopLeft.split(",")[0] : valueTopLeft);
                valueBtmLeft = RegexUtil.optStrOrBlank(data.get(titleBtmLeftField));
                valueBtmLeft = btmLeft + RegexUtil.optStrOrBlank(valueBtmLeft.contains(",") ? valueBtmLeft.split(",")[0] : valueBtmLeft);
                valueBtmRight = RegexUtil.optStrOrBlank(data.get(titleBtmRightField));
                valueBtmRight = btmRight + RegexUtil.optStrOrBlank(valueBtmRight.contains(",") ? valueBtmRight.split(",")[0] : valueBtmRight);
                PdfPTable table1 = new PdfPTable(2);
                table1.setTotalWidth(290);
                table1.setLockedWidth(true);
                PdfPTable table2 = new PdfPTable(1);
                table2.setTotalWidth(290);
                table2.setLockedWidth(true);
                PdfPTable table3 = new PdfPTable(2);
                table3.setTotalWidth(290);
                table3.setTotalWidth(new float[]{280, 220});
                table3.setLockedWidth(true);
                PdfPTable table5 = new PdfPTable(2);
                table5.setTotalWidth(290);
                table5.setLockedWidth(true);
                //获取设备的编号
                //valueFirst = String.valueOf(data.get("qci_first"));
                //code = qrCodePrefix + String.valueOf(data.get("asset_code"));
                code = String.valueOf(data.get("id"));
                //将编号信息生成二维码图片流
                BarcodeQRCode barcodeQRCode = new BarcodeQRCode(code);
                //将图片流转换成pdf图片格式
                com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(barcodeQRCode.createAwtImage(Color.BLACK, Color.WHITE), Color.WHITE);
                image.scalePercent(400f);
                //设置二维码图片的宽高
                //将二维码图片放入段落
                PdfPCell cell1 = new PdfPCell(image);
                cell1.setFixedHeight(105);
                cell1.setPaddingTop(-2);
                cell1.setBorder(0);
                //将设备编码信息和名称放入段落
                Phrase table7C2P = new Phrase();
                if (RegexUtil.optIsPresentStr(titleFirst) && RegexUtil.optIsPresentStr(valueFirst)) {
                    table7C2P.add(new Chunk(titleFirst + ":" + valueFirst, FontChinese16));//设备编码
                    if (titleFirst.length() < 7) {
                        table7C2P.add(Chunk.NEWLINE);
                    }
                }
                if (RegexUtil.optIsPresentStr(titleSecond) && RegexUtil.optIsPresentStr(valueSecond)) {
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(new Chunk(titleSecond + ":" + valueSecond, FontChinese16));//设备名称
                    if (titleSecond.length() < 7) {
                        table7C2P.add(Chunk.NEWLINE);
                    }
                }
                //如果标题一和标题四为空  中间增加空格行 设备位置二维码
                if (!RegexUtil.optIsPresentStr(titleFirst) && !RegexUtil.optIsPresentStr(titleFourth)) {
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(Chunk.NEWLINE);
                }
                table7C2P.add(Chunk.NEWLINE);
                if (RegexUtil.optIsPresentStr(titleThird) && RegexUtil.optIsPresentStr(valueThird)) {
                    table7C2P.add(new Chunk(titleThird + ":" + valueThird, FontChinese16));//设备型号
                    if (titleThird.length() < 7) {
                        table7C2P.add(Chunk.NEWLINE);
                    }
                }
                table7C2P.add(Chunk.NEWLINE);
                if (RegexUtil.optIsPresentStr(titleFourth) && RegexUtil.optIsPresentStr(valueFourth)) {
                    table7C2P.add(new Chunk(titleFourth + ":" + valueFourth, FontChinese16));//设备组织
                }
                PdfPCell cell2 = new PdfPCell(table7C2P);
                cell2.setPaddingLeft(115);
                cell2.setPaddingTop(10);
                cell2.setFixedHeight(105);
                cell2.setBorder(0);
                //主标题
                Phrase table7C3P = new Phrase();
                table7C3P.add(new Chunk(titleMain, FontChinese16));//公司名称
                PdfPCell cell3 = new PdfPCell(table7C3P);
                cell3.setPaddingTop(-2);
                cell3.setFixedHeight(16);
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setBorder(0);
                //副标题-上右
                Phrase table7C4P = new Phrase();
                table7C4P.add(new Chunk(titleSub, FontChinese10));
                PdfPCell cell4 = new PdfPCell(table7C4P);
                cell4.setFixedHeight(12);
                cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell4.setPaddingRight(5);
                cell4.setBorder(0);
                //副标题-上左
                Phrase tableTopLeft = new Phrase();
                tableTopLeft.add(new Chunk(valueTopLeft, FontChinese10));
                PdfPCell cellTopLeft = new PdfPCell(tableTopLeft);
                cellTopLeft.setFixedHeight(12);
                cellTopLeft.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellTopLeft.setPaddingRight(5);
                cellTopLeft.setBorder(0);
                //副标题-下右
                Phrase tableBtmRight = new Phrase();
                tableBtmRight.add(new Chunk(valueBtmRight, FontChinese10));
                PdfPCell cellBtmRight = new PdfPCell(tableBtmRight);
                cellBtmRight.setFixedHeight(12);
                cellBtmRight.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cellBtmRight.setPaddingRight(5);
                cellBtmRight.setBorder(0);
                //副标题-下左
                Phrase tableBtmLeft = new Phrase();
                tableBtmLeft.add(new Chunk(valueBtmLeft, FontChinese10));
                PdfPCell cellBtmLeft = new PdfPCell(tableBtmLeft);
                cellBtmLeft.setFixedHeight(12);
                cellBtmLeft.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellBtmLeft.setPaddingRight(5);
                cellBtmLeft.setBorder(0);
                //将图片和信息放入中间表
                table5.addCell(cellBtmLeft);
                table5.addCell(cellBtmRight);
                table1.addCell(cellTopLeft);
                table1.addCell(cell4);
                table2.addCell(cell3);
                table3.addCell(cell2);
                table3.addCell(cell1);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.addElement(table1);
                pdfPCell.addElement(table2);
                pdfPCell.addElement(table3);
                pdfPCell.addElement(table5);
                table4.addCell(pdfPCell);

                if (index >= 100) {
                    document.add(table4);
                    table4.deleteBodyRows();
                }
                index++;
            }
            //当打印数量不满1000或者前面打完后面剩余不满以前的时候直接打印
            document.add(table4);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
        } finally {
//            try {
//                if (null != os) {
//                    os.flush();
//                    os.close();
//                }
//            } catch (Exception ose) {
//                ose.printStackTrace();
//            }
            document.close();
        }
    }

    @ApiOperation(value = "下载条码机PDF", notes = "条码机下载")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dataList", value = "数据信息", required = true, paramType = "List<Map<String, Object>>"),
            @ApiImplicitParam(name = "qrCodePrefix", value = "条码机前缀"),
            @ApiImplicitParam(name = "companyId", value = "企业id"),
            @ApiImplicitParam(name = "titleFirst", value = "标题1"),
            @ApiImplicitParam(name = "titleSecond", value = "标题2"),
            @ApiImplicitParam(name = "titleThird", value = "标题3"),
            @ApiImplicitParam(name = "titleFourth", value = "标题4"),
    })
    public static void doDownloadBRCodePdf(Map<String, Object> paramMap) {
        RegexUtil.optNotNullOrExp(paramMap, LangConstant.TEXT_K); // {0}信息不存在
        RegexUtil.optNotNull(HttpRequestUtils.getResponse()).ifPresent(response -> {
            // 下面三行是关键代码，处理乱码问题
            response.setContentType("application/x-download");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment; filename=" + getQRCodePdfName());
            // 初始化纵向A4尺寸
            Rectangle pageSize = new Rectangle(300.0F, 160.8F);
            Document document = new Document(pageSize);
            OutputStream os = null;
            try {
                os = response.getOutputStream();
                PdfWriter.getInstance(document, os);
                document.setMargins(5, 5, 5, 5);
                // 设置字体
                BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
                Font FontChinese16 = new Font(bfChinese, 16, Font.NORMAL);
                // 获取所有设备信息
                List<Map<String, Object>> dataList = RegexUtil.optNotNullListOrNew(paramMap.get("dataList"));
                String qrCodePrefix = RegexUtil.optStrOrBlank(paramMap.get("qrCodePrefix")) + RegexUtil.optStrOrBlank(paramMap.get("companyId")) + "/";
                String titleFirst = RegexUtil.optStrOrBlank(paramMap.get("titleFirst"));
                String titleSecond = RegexUtil.optStrOrBlank(paramMap.get("titleSecond"));
                String titleThird = RegexUtil.optStrOrBlank(paramMap.get("titleThird"));
                String titleFourth = RegexUtil.optStrOrBlank(paramMap.get("titleFourth"));
                String code;
                String valueFirst;
                // 左右表格计数器，
                int index = 1;
                // 计数器（最后未满1000条数据用到);
                // 打开
                document.open();
                // 左边总表格
                PdfPTable tableAll1 = new PdfPTable(1);
                // 设置左边边表格宽度占所有空白宽度的百分比
                tableAll1.setWidthPercentage(100);
                tableAll1.setHorizontalAlignment(100);
                //遍历所有设备，
                for (Map<String, Object> data : dataList) {
                    PdfPTable table2 = getPdfPTable(2, new int[]{40, 40});
                    // 此表功能是将上面所有表放到一个表里
                    PdfPTable table4 = new PdfPTable(1);
                    // 设置表的宽度百分比
                    table4.setWidthPercentage(100);
                    table4.setHorizontalAlignment(100);
                    table2.setWidthPercentage(100);
                    // 获取设备的编号
                    valueFirst = RegexUtil.optStrOrBlank(data.get("qci_first"));
                    // 全部码
                    code = qrCodePrefix + valueFirst;
                    // 将编号信息生成二维码图片流
                    BarcodeQRCode barcodeQRCode = new BarcodeQRCode(code);
                    // 将图片流转换成pdf图片格式
                    Image image = Image.getInstance(barcodeQRCode.createAwtImage(Color.BLACK, Color.WHITE), Color.WHITE);
                    image.scalePercent(400f);
                    // 将二维码图片放入段落
                    PdfPCell table7C1 = new PdfPCell(image);
                    table7C1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table7C1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table7C1.setPaddingRight(-80);
                    table7C1.setPaddingTop(30);
                    table7C1.setBorder(0);
                    // 将设备编码信息和名称放入段落
                    Phrase table7C2P = new Phrase();
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(new Chunk(titleFirst + "：" + valueFirst, FontChinese16)); // 设备编码
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(new Chunk(RegexUtil.optStrOrBlank(data.get("qci_second")), FontChinese16)); // 设备名称
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(Chunk.NEWLINE);
                    if (titleSecond.length() < 7) {
                        table7C2P.add(Chunk.NEWLINE);
                    }
                    table7C2P.add(new Chunk(titleThird + ":" + RegexUtil.optNotBlankStrOpt(data.get("qci_third")).map(tmpStr1 -> tmpStr1.split(",")[0]).orElse(""), FontChinese16));//设备型号
                    table7C2P.add(Chunk.NEWLINE);
                    table7C2P.add(Chunk.NEWLINE);
                    if (titleThird.length() < 7) {
                        table7C2P.add(Chunk.NEWLINE);
                    }
                    table7C2P.add(new Chunk(titleFourth + ":" + RegexUtil.optStrOrBlank(data.get("qci_fourth")), FontChinese16)); // 设备组织
                    PdfPCell table7C2 = new PdfPCell(table7C2P);
                    table7C2.setPaddingRight(-50);
                    table7C2.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table7C2.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table7C2.setBorder(0);
                    //将图片和信息放入中间表
                    table2.addCell(table7C2);
                    table2.addCell(table7C1);
                    //将表 1 2 3 添加成table4的cell
                    PdfPCell cell2 = new PdfPCell(table2);
                    cell2.setFixedHeight(60);
                    cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell2.setPaddingTop(-75);
                    cell2.setPaddingLeft(-35);
                    cell2.setPaddingBottom(-90);
                    cell2.setBorder(0);
                    PdfPCell cell6 = new PdfPCell(table4);
                    cell6.setFixedHeight(60);
                    tableAll1.addCell(cell2);
                    if (index >= 100) {
                        document.add(tableAll1);
                        tableAll1.deleteBodyRows();
                    }
                    index++;
                }
                // 当打印数量不满1000或者前面打完后面剩余不满以前的时候直接打印
                document.add(tableAll1);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new SenscloudException(LangConstant.MSG_BK); // 未知错误！
            } finally {
                try {
                    if (null != os) {
                        os.flush();
                        os.close();
                    }
                } catch (Exception ose) {
                    ose.printStackTrace();
                }
                document.close();
            }
        });
    }

    /**
     * 获取PDF名称
     *
     * @return PDF名称
     */
    private static String getQRCodePdfName() {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_FILE);
        String now = sdf.format(new Timestamp(System.currentTimeMillis()));
        return "QRCode" + now + ".pdf";
    }

    /**
     * 自定义table方法
     *
     * @param columnCnt  列数
     * @param tableWidth 宽度
     * @return 表格
     * @throws Exception 异常
     */
    private static PdfPTable getPdfPTable(int columnCnt, int[] tableWidth) throws Exception {
        PdfPTable table = new PdfPTable(columnCnt);
        table.setWidths(tableWidth);
        table.getDefaultCell().setBorder(0);
        table.setWidthPercentage(100);
        return table;
    }

    /**
     * 设置单元格
     *
     * @param phrase phrase
     * @return 单元格
     */
    private static PdfPCell getPdfPCell(Phrase phrase) {
        PdfPCell cell = RegexUtil.optNotNull(phrase).map(e -> new PdfPCell(phrase)).orElseGet(PdfPCell::new);
        cell.setBorder(0);
        return cell;
    }

    /**
     * 设置公共单元格
     *
     * @param table 表格
     * @return 单元格
     */
    private static PdfPCell getCommonCell(PdfPTable table) {
        PdfPCell cell = new PdfPCell(table);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_TOP);
        cell.setBorder(0);
        return cell;
    }

    /**
     * 设置公共表格
     *
     * @param document document
     * @param table    table
     * @param table1   table1
     * @param table2   table2
     * @throws Exception 异常
     */
    private static void setCommonTable(Document document, PdfPTable table, PdfPTable table1, PdfPTable table2) throws Exception {
        PdfPCell cell = new PdfPCell(new Phrase(""));
        cell.setBorder(0);
        table.addCell(getCommonCell(table2));
        table.addCell(cell);
        table.addCell(getCommonCell(table1));
        document.add(table);
    }
}
