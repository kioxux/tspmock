package com.gengyun.senscloud.util;


import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.model.MethodParam;

import java.util.HashMap;
import java.util.Map;

/**
 * 本系统错误消息处理
 * User: sps
 * Date: 2021/01/06
 * Time: 上午11:42
 */
public class SenscloudError extends RuntimeException {
    /**
     * 消息参数等详细内容
     */
    private ThreadLocal<Map<String, Object>> msgContent = new ThreadLocal<>();

    /**
     * 错误消息
     *
     * @param methodParam 系统参数
     * @param msg         消息
     */
    public SenscloudError(MethodParam methodParam, String msg) {
        super(msg);
        Map<String, Object> map = new HashMap<>();
        map.put(ErrorConstant.EC_SCHEMA_KEY, methodParam.getSchemaName());
        map.put(ErrorConstant.EC_USER_ID_KEY, methodParam.getUserId());
        msgContent.set(map);
    }

    /**
     * 带备注消息
     *
     * @param methodParam 系统参数
     * @param msg         消息
     * @param remark      备注
     */
    public SenscloudError(MethodParam methodParam, String msg, String remark) {
        super(msg);
        Map<String, Object> map = new HashMap<>();
        map.put(ErrorConstant.EC_SCHEMA_KEY, methodParam.getSchemaName());
        map.put(ErrorConstant.EC_USER_ID_KEY, methodParam.getUserId());
        map.put(ErrorConstant.EC_REMARK_KEY, remark);
        msgContent.set(map);
    }

    /**
     * 带备注、异常消息
     *
     * @param methodParam 系统参数
     * @param msg         消息
     * @param remark      备注
     * @param expMsg      异常
     */
    public SenscloudError(MethodParam methodParam, String msg, String remark, String expMsg) {
        super(msg);
        Map<String, Object> map = new HashMap<>();
        map.put(ErrorConstant.EC_SCHEMA_KEY, methodParam.getSchemaName());
        map.put(ErrorConstant.EC_USER_ID_KEY, methodParam.getUserId());
        map.put(ErrorConstant.EC_REMARK_KEY, remark);
        map.put(ErrorConstant.EC_EXP_KEY, expMsg);
        msgContent.set(map);
    }

    public ThreadLocal<Map<String, Object>> getMsgContent() {
        return msgContent;
    }
}
