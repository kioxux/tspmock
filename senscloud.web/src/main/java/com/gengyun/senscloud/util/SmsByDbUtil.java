package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 根据数据库配置发送短信(支持腾讯接口)
 */
public class SmsByDbUtil {
    /**
     * 短信发送
     *
     * @param methodParam 系统参数
     * @param configInfo  数据信息
     * @param provider    短信类型
     * @param phone       接收人手机号
     * @param content     短信内容
     * @return 发送结果
     * @throws Exception 发送异常
     */
    public static Map<String, Object> sendSms(MethodParam methodParam, Map<String, String> configInfo, String provider, String phone, Object content) throws Exception {
        Map<String, Object> resultMap = null;
        if (Constants.SENSCLOUD_SMS_DEFAULT.equals(provider)) {
            resultMap = sendTencentSms(methodParam, configInfo, phone, (String[]) content);
        } else if (Constants.SENSCLOUD_SMS_SIEMENS.equals(provider)) {
            resultMap = sendSiemensSms(configInfo, phone);
        }
        return resultMap;
    }

    /**
     * 西门子短信发送接口
     *
     * @param configInfo 数据信息
     * @param phone      接收人手机号
     * @return 发送结果
     */
    private static Map<String, Object> sendSiemensSms(Map<String, String> configInfo, String phone) {
        String account = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve2"), ErrorConstant.EC_PRIMARY_SMS_19);
        String password = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve3"), ErrorConstant.EC_PRIMARY_SMS_2);
        String host = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve4"), ErrorConstant.EC_PRIMARY_SMS_20);
        String port = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve5"), ErrorConstant.EC_PRIMARY_SMS_5);
        String contentDb = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve6"), ErrorConstant.EC_PRIMARY_SMS_2);
        String extno = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve7"), ErrorConstant.EC_PRIMARY_SMS_21);
        String request = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve8"), ErrorConstant.EC_PRIMARY_SMS_22);
        String url = "http://" + host + ":" + port + "?action=send&account=" + account + "&password=" + password + "&mobile=" + phone + "&content=" + contentDb + "&extno=" + extno + "&rt=json";
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", true);
        if ("get".equals(request)) {
            RegexUtil.optNotNullMap(DataChangeUtil.scdObjToMap(HttpRequestUtils.requestByGet(url))).filter(m -> !"0".equals(RegexUtil.optStrOrBlank(m.get("status")))).
                    ifPresent(r -> {
                        resultMap.put("result", false);
                        resultMap.put("errMsg", r.toString());
                    });
        } else if ("post".equals(request)) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "send");
            jsonObject.put("account", account);
            jsonObject.put("password", password);
            jsonObject.put("mobile", phone);
            jsonObject.put("content", contentDb);
            jsonObject.put("extno", extno);
            jsonObject.put("rt", "json");
            String jsonString = jsonObject.toString();
            RegexUtil.optNotNullMap(DataChangeUtil.scdObjToMap(HttpRequestUtils.requestJsonByPost(url, jsonString))).filter(m -> !"0".equals(RegexUtil.optStrOrBlank(m.get("status")))).
                    ifPresent(r -> {
                        resultMap.put("result", false);
                        resultMap.put("errMsg", r.toString());
                    });
        }
        return resultMap;
    }

    /**
     * 腾讯短信发送接口（指定模板单发）
     *
     * @param methodParam 系统参数
     * @param configInfo  数据信息
     * @param phone       接收人手机号
     * @param content     短信内容
     * @return 发送结果
     * @throws Exception 发送异常
     */
    private static Map<String, Object> sendTencentSms(MethodParam methodParam, Map<String, String> configInfo, String phone, String[] content) throws Exception {
        // 模板内容为：测试短信，{1}，{2}。
        int tmpId = RegexUtil.optIntegerOrExp(content[0], methodParam, ErrorConstant.EC_PRIMARY_SMS_1, content[0], null);
        ArrayList<String> params = new ArrayList<>(Arrays.asList(content));
        RegexUtil.falseExp(params.size() > 1, ErrorConstant.EC_PRIMARY_SMS_2);
        params.remove(0);
        int appId = RegexUtil.optIntegerOrExp(configInfo.get("reserve2"), methodParam, ErrorConstant.EC_PRIMARY_SMS_3, null, null);
        String appKey = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve3"), ErrorConstant.EC_PRIMARY_SMS_4);
        String nationCode = RegexUtil.optNotBlankStrOrExp(configInfo.get("reserve4"), ErrorConstant.EC_PRIMARY_SMS_5);
        String sign = configInfo.get("reserve6");
        String extend = configInfo.get("reserve7");
        SmsSingleSender singleSender = new SmsSingleSender(appId, appKey); // 初始化单发
        SmsSingleSenderResult result = singleSender.sendWithParam(nationCode, phone, tmpId, params, sign, extend, SenscloudUtil.gnrStrRandNumInRange(100000, 999999));
        RegexUtil.optNotNullOrExp(result, ErrorConstant.EC_PRIMARY_SMS_12);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", result.result == 0);
        resultMap.put("errMsg", RegexUtil.optNotBlankStrOpt(result.errMsg).filter(m -> !"OK".equals(m)).orElse(null));
        resultMap.put("ext", result.ext);
        resultMap.put("sid", result.sid);
        resultMap.put("fee", result.fee);
        return resultMap;
    }
}
