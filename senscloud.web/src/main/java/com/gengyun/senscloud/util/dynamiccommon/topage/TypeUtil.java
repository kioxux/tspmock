package com.gengyun.senscloud.util.dynamiccommon.topage;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.common.StatusConstant;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.sql.Timestamp;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * 字段类型处理（动态进页面前）
// * User: sps
// * Date: 2020/06/11
// * Time: 上午09:20
// */
public class TypeUtil {
//    private static final Logger logger = LoggerFactory.getLogger(TypeUtil.class);
//
//    /**
//     * 按类型设置数据
//     *
//     * @param result
//     * @param data
//     * @param strInfo
//     * @param allMap
//     * @return
//     */
//    public static boolean doSetResultByType(JSONObject result, JSONObject data, Map<String, String> strInfo, Map<String, Object> allMap) {
//        String fieldFormCode = strInfo.get("fieldFormCode");
//        Map<String, Object> pageAllValue = (Map<String, Object>) allMap.get("pageAllValue");
//        // 日期
//        if (strInfo.get("fieldCode").endsWith("_time")) {
//            if (null != allMap.get("value")) {
//                try {
//                    Timestamp ts = (Timestamp) allMap.get("value");
//                    SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);;
//                    allMap.put("value", dateFormat.format(ts));
//                    data.put("fieldValue", allMap.get("value")); // 数据回显
//                } catch (Exception e) {
//                    logger.warn("日期格式未转换：" + strInfo.get("fieldCode"));
//                }
//            }
//        }
//        List<Map<String, Object>> hideList = (List<Map<String, Object>>) allMap.get("hideList");
//        // 隐藏值
//        if ("10".equals(strInfo.get("fieldViewType"))) {
//            pageAllValue.put(fieldFormCode, data);
//            hideList.add(data);
//            return true;
//        }
//        // 按钮
//        if ("7".equals(strInfo.get("fieldViewType"))) {
//            data.put("iconType", "button");
//            pageAllValue.put(fieldFormCode, data);
//            if (strInfo.get("fieldCode").startsWith("btnExportA1")) {
//                return true;
//            }
//            List<String> btnList = (List<String>) allMap.get("btnList");
//            btnList.add(fieldFormCode);
//            return true;
//        }
//        // 子工单数据（主工单编辑状态时，子工单信息隐藏）
//        if ("14".equals(strInfo.get("fieldViewType"))) {
//            if (1 == (Integer) allMap.get("isMain") && StatusConstant.DRAFT == (Integer) allMap.get("status")) {
//                hideList.add(data);
//                return true;
//            } else {
//                try {
//                    Map<String, Object> oldData = (Map<String, Object>) allMap.get("oldData");
//                    data.put("subListContent", oldData.get("subWorkList"));
//                } catch (Exception taskExp) {
//                    logger.warn("子工单数据解析异常！" + strInfo.get("subWorkCode"));
//                }
//            }
//        }
////        if (!data.containsKey("splitType")) {
////            data.put("splitType", "：");
////        }
//        data.put("splitType", "");
//        data.put("fieldViewName", ((String) data.get("fieldName")).concat((String) data.get("splitType")));
//        if (data.containsKey("icon")) {
//            String icon = (String) data.get("icon");
//            if (RegexUtil.isNotNull(icon) && "defaultUserIcon".equals(icon)) {
//                data.put("icon", "/image/user.png");
//            }
//        }
//        // 子工单数据（主工单提交后，子工单信息显示，任务信息、点巡检对象信息隐藏）
//        if (("9".equals(strInfo.get("fieldViewType"))) && (Integer) allMap.get("subWorkCnt") > 0) {
//            hideList.add(data);
//            return true;
//        }
//        if (data.containsKey("isRequired") && !"1".equals(data.get("isRequired"))) {
//            data.put("isRequired", "");
//        }
//
//        // 纯文本
//        String cardType = (String) data.get("cardType");
//        if (RegexUtil.isNull(cardType) || !"onlyList".equals(cardType)) {
//            if ("readonly".equals(data.getString("fieldRight"))) {
//                data.put("phoneViewType", "32");
//                if (",9,12,".contains("," + strInfo.get("fieldViewType") + ",")) {
//                    data.put("fieldTextValue", strInfo.get("strNone"));
//                    String contentName = "bomContent";
//                    if ("8".equals(strInfo.get("fieldViewType"))) {
//                        contentName = "relationContent"; // 对象
//                    }
//                    if ("9".equals(strInfo.get("fieldViewType"))) {
//                        contentName = "taskContent"; // 任务
//                    }
//                    try {
//                        RegexUtil.optNotBlankStrOpt(data.get(contentName)).ifPresent(e -> {
//                            if (JSONArray.fromObject(e).size() > 0) {
//                                data.put("fieldTextValue", "");
//                            }
//                        });
//                    } catch (Exception taskExp) {
//                        logger.warn("workInfoListWarn");
//                    }
//                } else if (",15,16,".contains("," + strInfo.get("fieldViewType") + ",")) {
//                    String timeInterval = "";
//                    try {
//                        Map<String, Object> oldData = (Map<String, Object>) allMap.get("oldData");
//                        Timestamp beginTime = (Timestamp) oldData.get("begin_time");
//                        if ("16".equals(strInfo.get("fieldViewType"))) {
//                            Timestamp finishedTime = (Timestamp) oldData.get("finished_time");
//                            timeInterval = String.valueOf(Double.parseDouble(new DecimalFormat("#.00").format((finishedTime.getTime() - beginTime.getTime()) / (1000 * 60)))) + strInfo.get("strTimeMin");
//
//                        } else if ("15".equals(strInfo.get("fieldViewType"))) {
//                            Timestamp distributeTime = (Timestamp) oldData.get("distribute_time");
//                            timeInterval = String.valueOf(Double.parseDouble(new DecimalFormat("#.00").format((beginTime.getTime() - distributeTime.getTime()) / (1000 * 60)))) + strInfo.get("strTimeMin");
//                        }
//                        data.put("fieldTextValue", timeInterval);
//                    } catch (Exception ftiExp) {
//                        data.put("fieldTextValue", strInfo.get("strNone"));
//                    }
//                    data.put("fieldValue", timeInterval);
//                } else if (!",2,6,".contains("," + strInfo.get("fieldViewType") + ",")) {
//                    data.put("fieldTextValue", RegexUtil.optStrAndValOrVal(allMap.get("fieldTextValue"), allMap.get("value"), strInfo.get("strNone")));
//                }
//            } else if (",2,4,5,9,12,31,32,".contains("," + strInfo.get("fieldViewType") + ",")) {
//                data.put("phoneViewType", "32");
//                if ("5".equals(strInfo.get("fieldViewType"))) {
//                    data.put("phoneViewType", "5");
//                    data.put("fieldTextValue", RegexUtil.optStrAndValOrBlank(allMap.get("fieldTextValue"), allMap.get("value")));
//                } else if (",9,12,".contains("," + strInfo.get("fieldViewType") + ",")) {
//                    data.put("fieldTextValue", "");
//                } else {
//                    data.put("fieldTextValue", RegexUtil.optStrAndValOrVal(allMap.get("fieldTextValue"), allMap.get("value"), strInfo.get("strNone")));
//                }
//            }
//        }
//
//
//        String linkType = (String) data.get("linkType");
//        if (RegexUtil.isNotNull(linkType)) {
//            if ("page".equals(linkType)) {
//                data.put("iptCss", "scd-clr-a");
//            }
//            data.put("iptFnc", "linkClick");
//        }
//        if ("4".equals(strInfo.get("fieldViewType"))) {
//            if ("edit".equals(data.getString("fieldRight"))) {
//                data.put("iptFnc", "doOpen");
//            }
//        } else if ("35".equals(strInfo.get("fieldViewType"))) {
//            if ("edit".equals(data.getString("fieldRight"))) {
//                data.put("phoneViewType", "6");
//            }
//        } else if ("11".equals(strInfo.get("fieldViewType"))) {
//            try {
//                result.put("commentList", JSONArray.fromObject(JSONObject.fromObject(allMap.get("value")).get("value"))); // 评论
//            } catch (Exception clExp) {
//                logger.warn("评论解析未成功！");
//            }
//        } else if ("37".equals(strInfo.get("fieldViewType"))) {
//            Map<String, Object> pageView = (Map<String, Object>) allMap.get("pageView");
//            pageView.put("titleCode", fieldFormCode);
//        } else if ("38".equals(strInfo.get("fieldViewType"))) {
//            pageAllValue.put(fieldFormCode, data);
//            List<String> btnList = (List<String>) allMap.get("btnList");
//            btnList.add(fieldFormCode);
//        } else if ("39".equals(strInfo.get("fieldViewType"))) {
//            Map<String, Object> pageView = (Map<String, Object>) allMap.get("pageView");
//            pageView.put("searchTopCode", fieldFormCode);
//        } else if ("41".equals(strInfo.get("fieldViewType"))) {
//            String listId = (String) data.get("listId");
//            if (RegexUtil.isNotNull(listId)) {
//                Map<String, Object> pageListColumn = (Map<String, Object>) allMap.get("pageListColumn");
//                Map<String, Object> tmpPlfInfo = RegexUtil.optMapOrNew((Map) pageListColumn.get(listId));
//                tmpPlfInfo.put(strInfo.get("fieldCode"), RegexUtil.optStrToObjAndValOrBlank(data.get("useInfo"), data.get("dataFieldCode"), data));
//                pageListColumn.put(listId, tmpPlfInfo);
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 设置字段模块信息
//     *
//     * @param data
//     * @param strInfo
//     * @param allMap
//     */
//    public static void doSetFieldSection(JSONObject data, Map<String, String> strInfo, Map<String, Object> allMap) {
//        String fieldSectionType = (String) data.get("fieldSectionType");
//        // 进行模块分组
//        if (RegexUtil.isNotNull(fieldSectionType) && !"-1".equals(fieldSectionType)) {
//            String fieldFormCode = strInfo.get("fieldFormCode");
//            Set<String> sectionKeys = (Set<String>) allMap.get("sectionKeys");
//            sectionKeys.add(fieldSectionType);
//            Map<String, List<String>> sectionContentList = (Map<String, List<String>>) allMap.get("sectionContentList");
//            List<String> contentList = null;
//            if (sectionContentList.containsKey(fieldSectionType)) {
//                contentList = sectionContentList.get(fieldSectionType);
//            } else {
//                contentList = new ArrayList<String>();
//            }
//            contentList.add(fieldFormCode);
//            sectionContentList.put(fieldSectionType, contentList);
//            Map<String, Map<String, Object>> sectionInfo = (Map<String, Map<String, Object>>) allMap.get("sectionInfo");
//            if (data.containsKey("sectionInfo")) {
//                Map<String, Object> sectionDtlInfo = new HashMap<>();
//                sectionDtlInfo.putAll((Map<String, Object>) data.get("sectionInfo"));
//                sectionInfo.put(fieldSectionType, sectionDtlInfo);
//            } else if (!sectionInfo.containsKey(fieldSectionType)) {
//                sectionInfo.put(fieldSectionType, new HashMap<>());
//            }
//        }
//    }
}
