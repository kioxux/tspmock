package com.gengyun.senscloud.util;
//
//import com.gengyun.senscloud.model.AssetPosition;
//import com.gengyun.senscloud.model.IFacility;
//
//import java.util.LinkedHashMap;
//
///**
// * 数据库公共条件工具
// */
public class SqlConditionUtil {

//    /**
//     * 工单场地拼接用sql条件
//     * @param isAllFacility
//     * @param isSelfFacility
//     * @param condition
//     * @param facilities
//     * @param account
//     * @param facilityList
//     * @return
//     */
//    public static String getWorkFacilityCondition(boolean isAllFacility, boolean isSelfFacility, String condition, String facilities, String account, LinkedHashMap<String, IFacility> facilityList) {
//        if (null != facilities && !"".equals(facilities)) {
//            String[] fs = facilities.split(",");
//            StringBuffer fsb = new StringBuffer();
//            for (String value : fs) {
//                fsb.append(" or wdaof.facility_no like '");
//                fsb.append(value);
//                fsb.append("%' or w.position_code like '");
//                fsb.append(value);
//                fsb.append("%'");
//            }
//            String nfs = fsb.toString();
//            if (!nfs.isEmpty() && !"".equals(nfs)) {
//                nfs = nfs.substring(3);
//                condition += " and ( " + nfs + ") ";
//            }
//        } else {
//            if (isAllFacility) {
//                condition = "  ";
//            } else if (isSelfFacility) {
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    StringBuffer fsb = new StringBuffer();
//                    String value = null;
//                    for (String key : facilityList.keySet()) {
//                        value = facilityList.get(key).getFacilityNo();
//                        fsb.append(" or wdaof.facility_no like '");
//                        fsb.append(value);
//                        fsb.append("%' or w.position_code like '");
//                        fsb.append(value);
//                        fsb.append("%'");
//                    }
//                    facilityIds = fsb.toString();
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(3);
//                    condition += " and ( " + facilityIds + " or w.create_user_account='" + account + "')";
//                } else {
//                    condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + " ')";    //没有用户所属位置，则按个人权限查询
//                }
//            } else {
//                condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + "' )";
//            }
//        }
//        return condition;
//    }
//
//    /**
//     * 工单请求场地拼接用sql条件
//     * @param isAllFacility
//     * @param isSelfFacility
//     * @param condition
//     * @param facilities
//     * @param account
//     * @param facilityList
//     * @return
//     */
//    public static String getWsqFacilityCondition(boolean isAllFacility, boolean isSelfFacility, String condition, String facilities, String account, LinkedHashMap<String, IFacility> facilityList) {
//        if (null != facilities && !"".equals(facilities)) {
//            String[] fs = facilities.split(",");
//            StringBuffer fsb = new StringBuffer();
//            for (String value : fs) {
//                fsb.append(" or aof.facility_no like '");
//                fsb.append(value);
//                fsb.append("%' or apf.facility_no like '");
//                fsb.append(value);
//                fsb.append("%' or w.position_code like '");
//                fsb.append(value);
//                fsb.append("%'");
//            }
//            String nfs = fsb.toString();
//            if (!nfs.isEmpty() && !"".equals(nfs)) {
//                nfs = nfs.substring(3);
//                condition += " and ( " + nfs + ") ";
//            }
//        } else {
//            if (isAllFacility) {
//                condition = "  ";
//            } else if (isSelfFacility) {
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    StringBuffer fsb = new StringBuffer();
//                    String value = null;
//                    for (String key : facilityList.keySet()) {
//                        value = facilityList.get(key).getFacilityNo();
//                        fsb.append(" or aof.facility_no like '");
//                        fsb.append(value);
//                        fsb.append("%' or apf.facility_no like '");
//                        fsb.append(value);
//                        fsb.append("%' or w.position_code like '");
//                        fsb.append(value);
//                        fsb.append("%'");
//                    }
//                    facilityIds = fsb.toString();
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(3);
//                    condition += " and ( " + facilityIds + " or w.create_user_account='" + account + "')";
//                } else {
//                    condition = " and (w.create_user_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                }
//            } else {
//                condition = " and (w.create_user_account='" + account + "' )";
//            }
//        }
//        return condition;
//    }
//
//    /**
//     * 工单组织拼接用sql条件
//     * @param tbKeys
//     * @param condition
//     * @param facilities
//     * @param facilityList
//     * @return
//     */
//    public static String getWorkOrgCondition(boolean isAllFacility, boolean isSelfFacility, String tbKeys,String condition, String facilities, LinkedHashMap<String, IFacility> facilityList,String account) {
//        String[] tk = tbKeys.split(",");
//        if (null != facilities && !"".equals(facilities)) {
//            String[] fs = facilities.split(",");
//            StringBuffer fsb = new StringBuffer();
//            for (String value : fs) {
//                fsb=sqlCondition(tk,value,fsb,"facility_no");
//            }
//            String nfs = fsb.toString();
//            if (!nfs.isEmpty() && !"".equals(nfs)) {
//                nfs = nfs.substring(3);
//                condition += " and ( " + nfs + ") ";
//            }
//        } else {
//            if (isAllFacility) {
//                return condition;
//            }else if (isSelfFacility){
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    StringBuffer fsb = new StringBuffer();
//                    String value = null;
//                    for (String key : facilityList.keySet()) {
//                        value = facilityList.get(key).getFacilityNo();
//                        fsb=sqlCondition(tk,value,fsb,"facility_no");
//                    }
//                    facilityIds = fsb.toString();
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(3);
//                    condition += " and ( " + facilityIds + " or w.create_user_account='" + account + "' or w2.receive_account='" + account + " ')";
//                }
//            }
//        }
//        return condition;
//    }
//
//    /**
//     * 工单设备位置拼接用sql条件
//     * @param tbKeys
//     * @param condition
//     * @param positions
//     * @param positionList
//     * @return
//     */
//    public static String getWorkPositionCondition(boolean isAllFacility, boolean isSelfFacility,String tbKeys, String condition, String positions, LinkedHashMap<String, AssetPosition> positionList,String account) {
//        String[] tk = tbKeys.split(",");
//        if (null != positions && !"".equals(positions)) {
//            String[] fs = positions.split(",");
//            StringBuffer fsb = new StringBuffer();
//            for (String value : fs) {
//                fsb=sqlCondition(tk,value,fsb,"position_code");
//            }
//            String nfs = fsb.toString();
//            if (!nfs.isEmpty() && !"".equals(nfs)) {
//                nfs = nfs.substring(3);
//                condition += " and ( " + nfs + ") ";
//            }
//        } else {
//            if (isAllFacility) {
//                return condition;
//            }else if (isSelfFacility){
//                String PositionIds = "";
//                if (positionList != null && !positionList.isEmpty()) {
//                    StringBuffer fsb = new StringBuffer();
//                    String value = null;
//                    for (String key : positionList.keySet()) {
//                        value = positionList.get(key).getPosition_code();
//                        fsb=sqlCondition(tk,value,fsb,"position_code");
//                    }
//                    PositionIds = fsb.toString();
//                }
//                if (!PositionIds.isEmpty() && !PositionIds.equals("")) {
//                    PositionIds = PositionIds.substring(3);
//                    condition += " and ( " + PositionIds + " or w.create_user_account='" + account + "' or w2.receive_account='" + account + " ')";
//                }
//            }
//        }
//        return condition;
//    }
//
//    /**
//     * 工单设备位置拼接用sql条件
//     * @param tbs
//     * @param value
//     * @param fsb
//     * @param field
//     * @return
//     */
//    private static StringBuffer sqlCondition(String[] tbs,String value,StringBuffer fsb,String field){
//        for(String tbKey:tbs){
//            fsb.append(" or ");
//            fsb.append(tbKey);
//            fsb.append(".");
//            fsb.append(field);
//            fsb.append(" like '");
//            fsb.append(value);
//            fsb.append("%'");
//        }
//        return fsb;
//    }
}


