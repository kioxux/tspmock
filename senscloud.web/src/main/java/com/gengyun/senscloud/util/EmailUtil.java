package com.gengyun.senscloud.util;
//
//import freemarker.template.Template;
//import org.apache.commons.io.IOUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
//
//import javax.activation.DataHandler;
//import javax.activation.DataSource;
//import javax.activation.FileDataSource;
//import javax.mail.BodyPart;
//import javax.mail.Multipart;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//import javax.mail.internet.MimeUtility;
//import java.io.File;
//import java.io.StringReader;
//import java.io.StringWriter;
//import java.nio.charset.StandardCharsets;
//import java.util.Map;
//
//@Component
public class EmailUtil {
//    private final Logger log = LoggerFactory.getLogger(EmailUtil.class);
//    private final JavaMailSender javaMailSender;
//
//    @Value("${spring.mail.username}")
//    private String sender;
//
//    private final FreeMarkerConfigurer freeMarkerConfigurer;  //自动注入
//
//    @Autowired
//    public EmailUtil(JavaMailSender javaMailSender, FreeMarkerConfigurer freeMarkerConfigurer) {
//        this.javaMailSender = javaMailSender;
//        this.freeMarkerConfigurer = freeMarkerConfigurer;
//    }
//
//    @Async
//    public Boolean sendEmail(String to, String cc, String subject, String template, Map<String, Object> valueMap) {
//        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
//                true, true, to, subject, template);
//
//        // Prepare message using a Spring helper
//        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//        StringWriter out = new StringWriter();
//        StringReader in = null;
//        try {
//            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, StandardCharsets.UTF_8.name());
//            message.setTo(to);
//            if (StringUtils.isNotEmpty(cc)) {
//                message.setCc(cc);
//            }
//            message.setFrom(sender);
//            message.setSubject(subject);
//            in = new StringReader(template);
//            new Template("email", in, freeMarkerConfigurer.getConfiguration()).process(valueMap, out);
//            message.setText(out.toString(), true);
//            javaMailSender.send(mimeMessage);
//            log.debug("Sent email to User '{}'", to);
//            return true;
//        } catch (Exception e) {
//            if (log.isDebugEnabled()) {
//                log.warn("Email could not be sent to user '{}'", to, e);
//            } else {
//                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
//            }
//            return false;
//        } finally {
//            IOUtils.closeQuietly(in);
//            IOUtils.closeQuietly(out);
//        }
//    }
//
//    /**
//     * 发送邮件（带附件）
//     * @param toList
//     * @param cc
//     * @param subject
//     * @param content
//     * @return
//     */
//    @Async
//    public void sendEmailList(String[] toList, String cc, String subject, String content, Map<String, Object>[] fileMap) {
//        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
//                true, true, toList, subject, content);
//        if(toList != null && toList.length > 0){
//            for(String to : toList){
//                sendEmail(to, cc, subject, content, fileMap);
//            }
//        }
//    }
//
//    /**
//     * 发送邮件（带附件）
//     * @param to
//     * @param cc
//     * @param subject
//     * @param content
//     * @return
//     */
//    @Async
//    public Boolean sendEmail(String to, String cc, String subject, String content, Map<String, Object>[] fileMap) {
//        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
//                true, true, to, subject, content);
//
//        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//        try {
//            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, StandardCharsets.UTF_8.name());
//            message.setTo(to);
//            if (StringUtils.isNotEmpty(cc)) {
//                message.setCc(cc);
//            }
//            message.setFrom(sender);
//            message.setSubject(subject);
//            message.setText(content);
//            // 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
//            Multipart multipart = new MimeMultipart();
//            // 添加邮件正文
//            BodyPart contentPart = new MimeBodyPart();
//            contentPart.setContent(content, "text/html;charset=UTF-8");
//            multipart.addBodyPart(contentPart);
//            //添加附件
//            if(fileMap != null && fileMap.length > 0){
//                for(Map<String, Object> map : fileMap){
//                    if(map != null) {
//                        File attachment = (File) map.get("file");
//                        if (attachment != null && attachment.exists()) {
//                            BodyPart attachmentBodyPart = new MimeBodyPart();
//                            DataSource source = new FileDataSource(attachment);
//                            attachmentBodyPart.setDataHandler(new DataHandler(source));
//                            //MimeUtility.encodeWord可以避免文件名乱码
//                            attachmentBodyPart.setFileName(MimeUtility.encodeWord(map.get("fileNamePrefix") + attachment.getName()));
//                            multipart.addBodyPart(attachmentBodyPart);
//                        }
//                    }
//                }
//            }
//            // 将multipart对象放到message中
//            mimeMessage.setContent(multipart);
//            javaMailSender.send(mimeMessage);
//            log.debug("Sent email to User '{}'", to);
//            return true;
//        } catch (Exception e) {
//            if (log.isDebugEnabled()) {
//                log.warn("Email could not be sent to user '{}'", to, e);
//            } else {
//                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
//            }
//            return false;
//        }
//    }
}
