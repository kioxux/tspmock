package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.MethodParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 权限工具类
 * User: sps
 * Date: 2020/12/14
 * Time: 上午11:42
 */
public class PermissionUtil {
    /**
     * 获取模块权限
     *
     * @param methodParam 系统参数
     * @param menuKey     模块类型
     * @return 权限
     */
    public static Map<String, Boolean> getModelPermission(MethodParam methodParam, String menuKey) {
        Map<String, Map<String, Boolean>> userModelPrmInfo = RegexUtil.optNotNull(methodParam.getUserModelPrmInfo()).orElseGet(HashMap::new);
        return RegexUtil.optNotNullMapBoolean(userModelPrmInfo.get(menuKey)).orElseGet(() ->
                {
                    Map<String, Boolean> result = new HashMap<>();
                    Optional<List<String>> lo = RegexUtil.optNotNullListStr(methodParam.getUserPermissionList());
                    for (String str : SensConstant.PRM_TYPES) {
                        result.put(SensConstant.PRM_TYPE_INFO.get(str), lo.filter(l -> l.contains(menuKey + "_" + str)).isPresent());
                    }
                    userModelPrmInfo.put(menuKey, result);
                    return result;
                }
        );
    }

    /**
     * 获取模块类型和权限类型获取指定权限
     *
     * @param methodParam 系统参数
     * @param menuKey     模块类型
     * @param prmKey      权限类型
     * @return 权限
     */
    public static Boolean getPermissionByInfo(MethodParam methodParam, String menuKey, String prmKey) {
        Boolean b = false;
        if (RegexUtil.optIsPresentList(methodParam.getUserPermissionList())) {
            b = methodParam.getUserPermissionList().contains(menuKey + prmKey);
        }
        return b;
    }
}
