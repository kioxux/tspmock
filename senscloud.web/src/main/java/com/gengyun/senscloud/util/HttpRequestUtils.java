package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.entity.LoginLogEntity;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.net.ssl.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

public class HttpRequestUtils {
    private static final Logger logger = LoggerFactory.getLogger(HttpRequestUtils.class);
    private static final String API_USER_TOKEN = "c2RyY19hcGlfdXNlcjpKJ3RzdWtlbjMxMzM3";

    /**
     * 获取request
     *
     * @return request
     */
    public static HttpServletRequest getRequest() {
        try {
            return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        } catch (Exception reExp) {
            return null;
        }
    }

    /**
     * 获取response
     *
     * @return response
     */
    public static HttpServletResponse getResponse() {
        try {
            return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getResponse();
        } catch (Exception reExp) {
            return null;
        }
    }

    /**
     * 获取session
     *
     * @return session
     */
    public static HttpSession getSession() {
        return RegexUtil.optNotNull(HttpRequestUtils.getRequest()).map(HttpServletRequest::getSession).orElse(null);
    }

    /**
     * 获取系统信息（替代session）
     *
     * @param token token
     * @return 系统信息
     */
    public static Map<String, Object> getScdSesInfo(String token) {
        if (RegexUtil.optIsPresentStr(token)) {
            CacheUtilService cacheUtilService = SpringContextHolder.getBean("cacheUtilServiceImpl");
            return cacheUtilService.cacheScdSesInfo(token);
        } else {
            return new HashMap<>();
        }
    }


    /**
     * 获取操作系统,浏览器及浏览器版本信息
     *
     * @param loginLog 登录信息
     */
    public static void clientInfo(LoginLogEntity loginLog) {
        RegexUtil.optNotNull(HttpRequestUtils.getRequest()).ifPresent(rq ->
                RegexUtil.optNotBlankStrLowerOpt(rq.getHeader("User-Agent")).ifPresent(ual -> {
                    String ua = rq.getHeader("User-Agent");
                    // OS
                    String os = "UnKnown, More-Info: " + ua;
                    if (ual.contains("windows")) {
                        os = "Windows";
                    } else if (ual.contains("mac")) {
                        os = "Mac";
                    } else if (ual.contains("x11")) {
                        os = "Unix";
                    } else if (ual.contains("android")) {
                        os = "Android";
                    } else if (ual.contains("iphone")) {
                        os = "IPhone";
                    }
                    loginLog.setClient_os(os);
                    // Browser
                    String browser = "UnKnown, More-Info: " + ua;
                    try {
                        if (ual.contains("edge")) {
                            browser = (ua.substring(ua.indexOf("Edge")).split(" ")[0]).replace("/", "-");
                        } else if (ual.contains("msie")) {
                            String substring = ua.substring(ua.indexOf("MSIE")).split(";")[0];
                            browser = substring.split(" ")[0].replace("MSIE", "IE") + "-" + substring.split(" ")[1];
                        } else if (ual.contains("chrome")) {
                            browser = (ua.substring(ua.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
                        } else if ((ual.contains("mozilla/7.0")) || (ual.contains("netscape6")) ||
                                (ual.contains("mozilla/4.7")) || (ual.contains("mozilla/4.78")) ||
                                (ual.contains("mozilla/4.08")) || (ual.contains("mozilla/3"))) {
                            browser = "Netscape-?";
                        } else if (ual.contains("firefox")) {
                            browser = (ua.substring(ua.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
                        } else if (ual.contains("rv")) {
                            String IEVersion = (ua.substring(ua.indexOf("rv")).split(" ")[0]).replace("rv:", "-");
                            browser = "IE" + IEVersion.substring(0, IEVersion.length() - 1);
                        } else {
                            String version = ual.contains("version") ? ua.substring(ua.indexOf("Version")).split(" ")[0].split("/")[1] : "";
                            if (ual.contains("safari")) {
                                browser = (ua.substring(ua.indexOf("Safari")).split(" ")[0]).split("/")[0]
                                        + "-" + version;
                            } else if (ual.contains("opr") || ual.contains("opera")) {
                                if (ual.contains("opera")) {
                                    browser = (ua.substring(ua.indexOf("Opera")).split(" ")[0]).split("/")[0]
                                            + "-" + version;
                                } else if (ual.contains("opr")) {
                                    browser = ((ua.substring(ua.indexOf("OPR")).split(" ")[0]).replace("/", "-"))
                                            .replace("OPR", "Opera");
                                }
                            }
                        }
                    } catch (Exception ignored) {
                    }
                    loginLog.setClient_browser(browser);
                    // ip
                    String[] ipArr = new String[]{"x-forwarded-for", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_X_FORWARDED_FOR"};
                    for (String str : ipArr) {
                        if (RegexUtil.optNotBlankStrOpt(rq.getHeader(str)).filter(a -> !"unknown".equals(a)).map(b -> {
                            loginLog.setClient_ip(b);
                            return true;
                        }).orElse(false)) {
                            break;
                        }
                    }
                    loginLog.setClient_ip(RegexUtil.optStrOrVal(loginLog.getClient_ip(), RegexUtil.optNotBlankStrOpt(rq.getRemoteAddr()).filter(f -> !"unknown".equals(f)).orElse(null)));
                })
        );
    }

    /**
     * 通过Http请求远程URL资源，并获取结果（GET）
     *
     * @param url 请求地址
     * @return 资源
     */
    public static String requestByGet(String url) {
        try {
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Content-type", "application/x-www-form-urlencoded");
            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(httpGet)) {
                if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                    HttpEntity retEntity = response.getEntity();
                    if (null != retEntity) {
                        String retStr = EntityUtils.toString(retEntity, "UTF-8");
                        EntityUtils.consume(retEntity);
                        return retStr;
                    }
                }
            } catch (Exception e) {
                throw new SenscloudException(ErrorConstant.EC_HTTP_101, e.getMessage());
            }
            logger.warn("httpMsgWarn-1001");
            return null;
        } catch (SenscloudException se) {
            throw se;
        } catch (Exception e) {
            throw new SenscloudException(ErrorConstant.EC_HTTP_102, e.getMessage());
        }
    }

    /**
     * 通过Http请求远程URL资源，并获取结果（POST）
     *
     * @param url        请求地址
     * @param jsonString 条件
     * @return 资源
     */
    public static String requestJsonByPost(String url, String jsonString) {
        try {
            logger.info("jbp-in:" + url + " pm:" + jsonString);
            // 设置默认的连接超时时间 请求超时时间为30(s)
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(requestConfig);
            httpPost.addHeader("Content-type", "application/json; charset=utf-8");
            httpPost.setHeader("Accept", "application/json");
            httpPost.setEntity(new StringEntity(jsonString, Charset.forName("UTF-8")));
            try (CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
                 CloseableHttpResponse response = httpClient.execute(httpPost);
                 BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
                StringBuilder sb = new StringBuilder();
                String line;
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line).append(NL);
                }
                logger.info("jbp-out:" + url);
                return sb.toString();
            } catch (Exception e) {
                throw new SenscloudException(ErrorConstant.EC_HTTP_201, e.getMessage());
            }
        } catch (SenscloudException se) {
            throw se;
        } catch (Exception e) {
            throw new SenscloudException(ErrorConstant.EC_HTTP_202, e.getMessage());
        }
    }

    /**
     * 通过Https请求远程URL资源，并获取结果（POST）
     *
     * @param openIdSessionkeyUrl 请求地址
     * @param jsonString          条件
     * @return 资源
     */
    public static String sendHtpps(String openIdSessionkeyUrl, JSONObject jsonString) {
        String result = "";
        PrintWriter out = null;
        BufferedReader in = null;
        HttpURLConnection conn;
        try {
            trustAllHosts();
            URL realUrl = new URL(openIdSessionkeyUrl);
//            URL realUrl = new URL(null,openIdSessionkeyUrl,new sun.net.www.protocol.https.Handler());
            // 通过请求地址判断请求类型(http或者是https)
            if ("https".equals(realUrl.getProtocol().toLowerCase())) {
                HttpsURLConnection https = (HttpsURLConnection) realUrl.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) realUrl.openConnection();
            }
            // 设置通用的请求属性
//            String encoding = DatatypeConverter.printBase64Binary("sdrc_api_user:J'tsuken31337".getBytes("UTF-8")); //username password 自行修改 中间":"不可少
            String encoding = API_USER_TOKEN;//c2RyY19hcGlfdXNlcjpKJ3RzdWtlbjMxMzM3
            conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestMethod("POST");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            logger.info("new PrintWriter(conn.getOutputStream()):" + out);
            // 发送请求参数
            out.print(jsonString);
            logger.info("out.print(jsonString)---------" + jsonString);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            logger.info("out.flush();---------" + out);

            int code = conn.getResponseCode();
            logger.info("code:" + code);
            logger.info("conn.getErrorStream()::::::" + String.valueOf(conn.getErrorStream()));
//            Process pr = Runtime.getRuntime().exec("notepad.exe");
//            InputStream is = conn.getErrorStream();
//            if (is != null) {
//                for (int i = 0; i < is.available(); ++i) {
//                    logger.info("流读取：" + is.read());
//                    pr.destroy();
//                }
//            }
            InputStreamReader inStreamRead = new InputStreamReader(conn.getInputStream());
//            InputStreamReader inStreamRead = new InputStreamReader(conn.getErrorStream());
            logger.info("inStreamRead");

            in = new BufferedReader(inStreamRead);
//            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            logger.info("new BufferedReader---------");
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            logger.info("https请求---------成功");
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("catch错误---------");
        } finally {// 使用finally块来关闭输出流、输入流
            try {

                if (out != null) {
                    out.close();
                    logger.info("out.close();---------");
                }
                if (in != null) {
                    logger.info("in.close();---------");
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 通过Https请求远程URL资源，并获取结果（POST）
     *
     * @param url        请求地址
     * @param jsonString 条件
     * @return 资源
     */
    public static String sendHttps(String url, String jsonString) {
        String resData = "";
        trustAllHosts();
        try {//https 请求忽略证书
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(builder.build(), NoopHostnameVerifier.INSTANCE);
            Registry registry = RegistryBuilder.create()
                    .register("http", new PlainConnectionSocketFactory())
                    .register("https", sslConnectionSocketFactory)
                    .build();
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
            cm.setMaxTotal(100);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory)
                    .setConnectionManager(cm)
                    .build();
            //发送post请求
            HttpPost method = new HttpPost(url);
            //准备json数据发送参数
            //处理中文乱码问题
            StringEntity entity = new StringEntity(jsonString, "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            method.setEntity(entity);
            String encoding = API_USER_TOKEN;
            method.setHeader("Authorization", "Basic " + encoding);
            method.setHeader("accept", "*/*");
            method.setHeader("connection", "Keep-Alive");
            method.setHeader("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            method.setHeader("Content-Type", "application/json;charset=utf-8");
            //发送请求
            HttpResponse result = httpClient.execute(method);
            //请求结束，返回结果
            resData = EntityUtils.toString(result.getEntity());
            System.out.println("sendHttps success:" + resData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resData;
    }

    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public static String sendHtpps1(String openIdSessionkeyUrl, JSONObject jsonString) {
        String result = "";
        PrintWriter out = null;
        BufferedReader in = null;
        HttpURLConnection conn;
        try {
            trustAllHosts();
            URL realUrl = new URL(openIdSessionkeyUrl);
//            URL realUrl = new URL(null,openIdSessionkeyUrl,new sun.net.www.protocol.https.Handler());
            // 通过请求地址判断请求类型(http或者是https)
            if ("https".equals(realUrl.getProtocol().toLowerCase())) {
                HttpsURLConnection https = (HttpsURLConnection) realUrl.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection) realUrl.openConnection();
            }
            // 设置通用的请求属性
//            String encoding = DatatypeConverter.printBase64Binary("sdrc_api_user:J'tsuken31337".getBytes("UTF-8")); //username password 自行修改 中间":"不可少
            String encoding = API_USER_TOKEN;//c2RyY19hcGlfdXNlcjpKJ3RzdWtlbjMxMzM3
            conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.setRequestProperty("accept", "application/json, text/plain, */*");
            conn.setRequestProperty("x-requested-with", "XMLHttpRequest");
            conn.setRequestProperty("x-csrftoken", API_USER_TOKEN);
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) QtWebEngine/5.15.2 Chrome/87.0.4280.144 Safari/537.36");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestMethod("POST");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            logger.info("new PrintWriter(conn.getOutputStream()):" + out + "新更改http");
            // 发送请求参数
            out.print(jsonString);
            logger.info("out.print(jsonString)---------" + jsonString);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            logger.info("out.flush();---------" + out);

            int code = conn.getResponseCode();
            logger.info("code:" + code);
            logger.info("conn.getErrorStream()::::::" + String.valueOf(conn.getErrorStream()));
//            Process pr = Runtime.getRuntime().exec("notepad.exe");
//            InputStream is = conn.getErrorStream();
//            if (is != null) {
//                for (int i = 0; i < is.available(); ++i) {
//                    logger.info("流读取：" + is.read());
//                    pr.destroy();
//                }
//            }
            InputStreamReader inStreamRead = new InputStreamReader(conn.getInputStream());
//            InputStreamReader inStreamRead = new InputStreamReader(conn.getErrorStream());
            logger.info("inStreamRead");

            in = new BufferedReader(inStreamRead);
//            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            logger.info("new BufferedReader---------");
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            logger.info("https请求---------成功");
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("catch错误---------");
        } finally {// 使用finally块来关闭输出流、输入流
            try {

                if (out != null) {
                    out.close();
                    logger.info("out.close();---------");
                }
                if (in != null) {
                    logger.info("in.close();---------");
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
}
