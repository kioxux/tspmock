package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 邮件工具类
 */

@Component
public class MailUntil {
    private static final Logger logger = LoggerFactory.getLogger(MailUntil.class);
    @Resource
    LogsService logsService;
    @Resource
    SystemConfigService systemConfigService;

    /**
     * 发送邮件
     *
     * @param methodParam   入参
     * @param subject       主题
     * @param content       内容
     * @param receive_users 收件人集合
     * @param cc_users      抄送人集合
     */
    public void sendEmailMessage(MethodParam methodParam, String subject, String content, List<Map<String, Object>> receive_users, List<Map<String, Object>> cc_users) {
        try {
            //邮件地址
            //String mail_adress = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_adress");
            //邮箱服务
            String mail_host = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_host");
            //邮箱密码
            String mail_password = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_password");
            //邮箱端口
            String mail_port = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_port");
            //邮箱账号
            String mail_username = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_username");
            logger.info("----------------mail_host:" + mail_host);
            logger.info("----------------mail_password:" + mail_password);
            logger.info("----------------mail_port:" + mail_port);
            logger.info("----------------mail_username:" + mail_username);
            Properties properties = System.getProperties();
            // Setup mail server
            properties.setProperty("mail.smtp.ssl.trust", mail_host);
            properties.setProperty("mail.smtp.host", mail_host);
            properties.setProperty("mail.smtp.starttls.enable", "true");
            // Get the default Session object.
            // Session session = Session.getDefaultInstance(properties);
            Session session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mail_username, mail_password);
                }
            });
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(mail_username));
            // Set To: header field of the header.
            for (Map<String, Object> receiver : receive_users) {
                message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiver.get("email").toString()));
            }
            //抄送人
            if (RegexUtil.optNotNull(cc_users).isPresent()) {
                for (Map<String, Object> ccer : cc_users) {
                    message.addRecipient(MimeMessage.RecipientType.CC, new InternetAddress(ccer.get("email").toString()));
                }
            }
            // Set Subject: header field
            message.setSubject(subject, "GB2312");
            // Now set the actual message
            message.setText(content, "GB2312");
            // Send message
            Transport.send(message);
            logger.info("----------------Sent message successfully....发送者:" + mail_username);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
            logsService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_EMAIL_MSG_001, methodParam.getUserId(), "邮件发送失败");
        }
    }

    public void sendCCEmailMessage(MethodParam methodParam, String subject, String content, List<Map<String, Object>> receive_users, List<Map<String, Object>> cc_users) {
        try {
            //邮件地址
            //String mail_adress = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_adress");
            //邮箱服务
            String mail_host = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_host");
            //邮箱密码
            String mail_password = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_password");
            //邮箱端口
            String mail_port = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_port");
            //邮箱账号
            String mail_username = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "mail_username");
            logger.info("----------------mail_host:" + mail_host);
            logger.info("----------------mail_password:" + mail_password);
            logger.info("----------------mail_port:" + mail_port);
            logger.info("----------------mail_username:" + mail_username);
            Properties properties = System.getProperties();
            // Setup mail server
            properties.setProperty("mail.smtp.ssl.trust", mail_host);
            properties.setProperty("mail.smtp.host", mail_host);
            properties.setProperty("mail.smtp.starttls.enable", "true");
            properties.setProperty("mail.smtp.auth", "true");
            // Get the default Session object.
            Session session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mail_username, mail_password);
                }
            });
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(mail_username));
            // Set To: header field of the header.
            for (Map<String, Object> receiver : receive_users) {
                message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiver.get("email").toString()));
            }
            //抄送人
            if (RegexUtil.optNotNull(cc_users).isPresent()) {
                for (Map<String, Object> ccer : cc_users) {
                    message.addRecipient(MimeMessage.RecipientType.CC, new InternetAddress(ccer.get("email").toString()));
                }
            }
            // Set Subject: header field
            message.setSubject(subject, "GB2312");
            // Now set the actual message
            message.setText(content, "GB2312");
            // Send message
            Transport.send(message);
            logger.info("----------------Sent message successfully....发送者:" + mail_username);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
            logsService.newErrorLog(methodParam.getSchemaName(), ErrorConstant.EC_EMAIL_MSG_001, methodParam.getUserId(), "邮件发送失败");
        }
    }
}
