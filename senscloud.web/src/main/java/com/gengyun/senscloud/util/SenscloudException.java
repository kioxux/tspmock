package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.ResponseConstant;

import java.util.HashMap;
import java.util.Map;

/**
 * 本系统异常消息处理
 * User: sps
 * Date: 2019/07/15
 * Time: 上午11:42
 */
public class SenscloudException extends RuntimeException {
    /**
     * 消息参数等详细内容
     */
    private ThreadLocal<Map<String, Object>> msgContent = new ThreadLocal<>();

    /**
     * 异常消息
     *
     * @param msg 消息
     */
    public SenscloudException(String msg) {
        super(msg);
        msgContent = null;
    }

    /**
     * 带参数异常消息
     *
     * @param msg       消息
     * @param msgParams 消息参数
     */
    public SenscloudException(String msg, String[] msgParams) {
        super(msg);
        Map<String, Object> map = new HashMap<>();
        map.put(ResponseConstant.ERROR_MSG_LANG_PMS, msgParams);
        msgContent.set(map);
    }

    /**
     * 异常信息（仅含错误码）
     *
     * @param code 错误码
     */
    public SenscloudException(Integer code) {
        Map<String, Object> map = new HashMap<>();
        map.put(ResponseConstant.ERROR_MSG_CODE, code);
        msgContent.set(map);
    }

    /**
     * 带备注消息
     *
     * @param msg    消息
     * @param remark 备注
     */
    public SenscloudException(String msg, String remark) {
        super(msg);
        Map<String, Object> map = new HashMap<>();
        map.put(ResponseConstant.ERROR_MSG_REMARK, remark);
        msgContent.set(map);
    }

    public ThreadLocal<Map<String, Object>> getMsgContent() {
        return msgContent;
    }
}
