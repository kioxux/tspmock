package com.gengyun.senscloud.util;

import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Decoder;

import java.nio.charset.StandardCharsets;

/**
 * Base64处理
 */
public class ScdEncryptUtil {
    /**
     * 加密
     *
     * @param str 字符串
     * @return 加密字符串
     */
    static String getBase64(String str) {
        return RegexUtil.optNotBlankStrOpt(str).map(s -> s.getBytes(StandardCharsets.UTF_8)).map(Base64::encodeBase64String).orElse(null);
    }

    /**
     * 解密
     *
     * @param s 字符串
     * @return 解密字符串
     */
    public static String getFromBase64(String s) {
        byte[] b;
        String result = null;
        if (s != null) {
            BASE64Decoder decoder = new BASE64Decoder();
            try {
                b = decoder.decodeBuffer(s);
                result = new String(b, StandardCharsets.UTF_8);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param byteArray 字节数组
     * @return 十六进制字符串
     */
    public static String bytesToStr(byte[] byteArray) {
        StringBuilder sb = new StringBuilder();
        for (byte b : byteArray) {
            sb.append(byteToHexStr(b));
        }
        return sb.toString();
    }

    /**
     * 将字节转换为十六进制字符串
     *
     * @param mByte 字节
     * @return 十六进制字符串
     */
    private static String byteToHexStr(byte mByte) {
        char[] Digit = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] tempArr = new char[2];
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
        tempArr[1] = Digit[mByte & 0X0F];
        return new String(tempArr);
    }
}
