package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.Constants;

import java.time.Instant;

/**
 * session使用记录
 * User: sps
 * Date: 2019/07/15
 * Time: 上午11:42
 */
public class ScdSesRecordUtil {
//    /**
//     * 清空session
//     */
//    public static void sessionClear() {
//        RegexUtil.optNotNull(HttpRequestUtils.getSession()).ifPresent(HttpSession::invalidate);
//    }

    /**
     * 根据key获取指定系统信息
     *
     * @param token token
     * @param key   键值
     * @return 指定系统信息
     */
    public static Object getScdSesDataByKey(String token, String key) {
        return RegexUtil.optNotNull(HttpRequestUtils.getScdSesInfo(token)).map(s -> s.get(key)).orElse(null);
    }

    /**
     * 根据key设置系统信息
     *
     * @param token token
     * @param key   键值
     * @param data  数据
     */
    public static void setScdSesDataByKey(String token, String key, Object data) {
        RegexUtil.optNotNull(HttpRequestUtils.getScdSesInfo(token)).ifPresent(s -> s.put(key, data));
    }

    /**
     * 登录时登录信息记录到系统信息中
     *
     * @param token    token
     * @param account  账号
     * @param password 密码
     */
    public static void setLoginSesData(String token, String account, String password) {
        RegexUtil.optNotNull(HttpRequestUtils.getScdSesInfo(token)).ifPresent(s -> {
            s.put(Constants.SCD_SES_KEY_PASSWORD, password);
            s.put(Constants.SCD_SES_KEY_LOGIN_TOKEN, token);
            s.put(Constants.SCD_SES_KEY_ACCOUNT, account);
            s.put(Constants.SCD_SES_KEY_LOGIN_TIME, Instant.now());
        });
    }

    /**
     * 登录成功后系统信息中的登录信息处理
     *
     * @param token     token
     * @param companyId 企业
     */
    public static void setAfterLoginSesData(String token, Object companyId) {
        RegexUtil.optNotNull(HttpRequestUtils.getScdSesInfo(token)).ifPresent(s -> {
            s.remove(Constants.SCD_SES_KEY_PASSWORD);
            s.remove(Constants.SCD_SES_KEY_LOGIN_TOKEN);
            s.remove(Constants.SCD_SES_KEY_ACCOUNT);
            s.remove(Constants.SCD_SES_KEY_LOGIN_TIME);
            s.put(Constants.SCD_SES_KEY_COMPANY_ID, companyId);
        });
    }

    /**
     * 获取登录账号
     *
     * @param token token
     * @return 登录账号
     */
    public static String getLoginAccount(String token) {
        return RegexUtil.optStrOrNull(ScdSesRecordUtil.getScdSesDataByKey(token, Constants.SCD_SES_KEY_ACCOUNT));
    }

    /**
     * 获取登录密码
     *
     * @param token token
     * @return 登录账号
     */
    public static String getLoginPassword(String token) {
        return RegexUtil.optStrOrNull(ScdSesRecordUtil.getScdSesDataByKey(token, Constants.SCD_SES_KEY_PASSWORD));
    }
}
