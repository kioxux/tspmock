package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.Constants;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * 数据转换工具
 * User: sps
 * Date: 2020/12/07
 * Time: 上午11:42
 */
public class DataChangeUtil {
    /**
     * 集合转map
     *
     * @param list      源集合
     * @param keyName   对象key取值字段
     * @param valueName 对象value取值字段
     * @return 对象
     */
    public static Map<String, Object> scdListToMap(List<Map<String, Object>> list, String keyName, String valueName) {
        return RegexUtil.optNotNullList(list).map(l -> l.stream().collect(Collectors.toMap(e -> RegexUtil.optStrOrBlank(e.get(keyName)), e -> RegexUtil.optNotNull(e.get(valueName)).orElse("")))).orElse(null);
    }

//    /**
//     * 泛型转换为对象
//     *
//     * @param o 泛型
//     * @return 对象
//     */
//    @SuppressWarnings("unchecked")
//    public static Map<String, Object> scdToMapOrNew(Object o) {
//        return RegexUtil.optNotNull(o).map(m -> (Map<String, Object>) m).orElseGet(HashMap::new);
//    }

    /**
     * 泛型转换为对象
     *
     * @param o 泛型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> scdToMap(Object o) {
        return (Map<String, Object>) RegexUtil.optNotNull(o).orElse(null);
    }

    /**
     * 泛型转换为对象
     *
     * @param o 泛型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> scdToMapUseJson(Object o) {
        return RegexUtil.optNotBlankStrOpt(o).map(JSONObject::fromObject).orElse(null);
    }

    /**
     * 泛型转换为对象
     *
     * @param o 泛型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> scdObjToMapOrNew(Object o) {
        return JSONObject.fromObject(RegexUtil.optNotNull(o).orElseGet(HashMap::new));
    }

    /**
     * 泛型转换为对象
     *
     * @param o 泛型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> scdObjToMap(Object o) {
        return RegexUtil.optNotNull(o).map(JSONObject::fromObject).orElse(null);
    }

    /**
     * 泛型转换为Map
     *
     * @param o 泛型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> scdObjToMapStr(Object o) {
        return RegexUtil.optNotNull(o).map(JSONObject::fromObject).orElse(null);
    }

    /**
     * 字符串转换为对象（Map-JSONObject）
     *
     * @param str 字符串
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static Map<String, JSONObject> scdStringToMap(String str) {
        return RegexUtil.optNotBlankStrOpt(str).map(JSONObject::fromObject).orElse(null);
    }

//    /**
//     * 字符串转换为对象（Map-Map-String）
//     *
//     * @param str 字符串
//     * @return 对象
//     */
//    @SuppressWarnings("unchecked")
//    public static Map<String, Map<String, String>> scdStringToMapMs(String str) {
//        return RegexUtil.optNotBlankStrOpt(str).map(JSONObject::fromObject).orElse(null);
//    }

    /**
     * 字符串转换为集合（List-Map）
     *
     * @param str 字符串
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> scdStringToList(String str) {
        return RegexUtil.optNotBlankStrOpt(str).map(JSONArray::fromObject).orElse(null);
    }

    /**
     * 泛型转换为集合（Obj-ListStr）
     *
     * @param obj 泛型值
     * @return 集合
     */
    @SuppressWarnings("unchecked")
    public static List<String> scdObjToListStr(Object obj) {
        try {
            return (List<String>) RegexUtil.optNotNull(obj).orElse(null);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 泛型转换为集合（Obj-ListStr）
     *
     * @param obj 泛型值
     * @return 集合，默认新建集合
     */
    @SuppressWarnings("unchecked")
    public static List<String> scdObjToListStrOrNew(Object obj) {
        try {
            return (List<String>) RegexUtil.optNotNull(obj).orElseGet(ArrayList::new);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * 泛型转换为集合（Obj-List）
     *
     * @param obj 泛型值
     * @return 集合
     */
    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> scdObjToList(Object obj) {
        return (List<Map<String, Object>>) RegexUtil.optNotNull(obj).orElse(null);
    }

    /**
     * 数组转换为集合（字符串数组-ListStr）
     *
     * @param obj 泛型值
     * @return 集合
     */
    @SuppressWarnings("unchecked")
    public static List<String> scdArrayToListStr(String[] obj) {
        return JSONArray.fromObject(obj);
    }

    /**
     * 给定字符串，转成以逗号隔开的字符串
     *
     * @param strValue 字符串
     * @return 转换后字符串
     */
    public static String joinByStr(String strValue) {
        String[] strValues = strValue.split(",");
        StringBuilder sb = new StringBuilder();
        for (String str : strValues) {
            if (RegexUtil.optIsPresentStr(str)) {
                sb.append(",'").append(str).append("'");
            }
        }
        return sb.length() > 0 ? sb.substring(1) : sb.toString();
    }

    /**
     * 以逗号隔开的字符串删除元素
     *
     * @param old   原字符串
     * @param value 要删除字符串
     * @return 删除后字符串
     */
    public static String removeStrForStrings(String old, String value) {
        return Arrays.stream(old.split(",")).filter(RegexUtil::optIsPresentStr).filter(v -> !v.equals(value)).collect(Collectors.joining(","));
    }

    /**
     * 给定字符串数组，转成以逗号隔开的字符串（不做空指针处理）
     *
     * @param strValues 字符串数组
     * @return 转换后字符串
     */
    public static String joinByStrArrayWithoutCheck(String[] strValues) {
        StringBuilder sb = new StringBuilder();
        for (String str : strValues) {
            sb.append(",'").append(str).append("'");
        }
        return sb.substring(1);
    }

    /**
     * 元素添加到数组首位
     *
     * @param a   元素
     * @param ary 数组
     * @return 合并后数组
     */
    public static Object[] firstAddArray(Object a, Object[] ary) {
        return DataChangeUtil.mergeArray(new Object[]{a}, ary);
    }

    /**
     * 数组合并
     *
     * @param a 前排数组
     * @param b 后排数组
     * @return 合并后数组
     */
    private static Object[] mergeArray(Object[] a, Object[] b) {
        List<Object> list = RegexUtil.optNotNull(a).map(Arrays::asList).map(ArrayList::new).orElseGet(ArrayList::new);
        RegexUtil.optNotNull(b).map(Arrays::asList).ifPresent(list::addAll);
        return list.toArray();
    }

    /**
     * 字符串数组合并
     *
     * @param a 前排数组
     * @param b 后排数组
     * @return 合并后数组
     */
    public static String[] mergeStrArray(String[] a, String[] b) {
        List<String> list = RegexUtil.optNotNull(a).map(Arrays::asList).map(ArrayList::new).orElseGet(ArrayList::new);
        RegexUtil.optNotNull(b).map(Arrays::asList).ifPresent(list::addAll);
        return list.toArray(new String[0]);
    }

    /**
     * 替换模板中的占位符，输出填完值后的字符串
     *
     * @param params     参数
     * @param patternStr 值
     * @return 转换后值
     */
    public static String wxGzhMsgTemplateValueHandler(Object[] params, String patternStr) {
        try {
            Matcher m = Constants.SCD_PT_INT_D.matcher(patternStr);
            List<String> strList = new ArrayList<>();
            String format = patternStr;
            while (m.find()) {
                String tag = m.group();
                format = format.replace(tag, "%s");
                int index = Integer.valueOf(tag.replace("{", "").replace("}", ""));
                strList.add(params[index - 1] != null ? String.valueOf(params[index - 1]) : "");
            }
            return String.format(format, strList.toArray());
        } catch (NumberFormatException e) {
            return "";
        }
    }

    /**
     * 集合按字段分割
     *
     * @param list    源集合
     * @param keyName 分割字段
     */
    public static void scdListSplitByKey(List<Map<String, Object>> list, String keyName) {
        RegexUtil.optNotNullList(list).ifPresent(l ->
                l.forEach(info -> RegexUtil.optNotBlankStrOpt(info.get(keyName)).map(s -> s.split(Constants.SCD_SPLIT_TYPE)).filter(RegexUtil::optIsPresentArray).ifPresent(dl -> {
                    for (String str : dl) {
                        RegexUtil.optNotBlankStrOpt(str).ifPresent(s -> {
                            int index = s.indexOf(':');
                            info.put(s.substring(0, index), s.substring((index + 1)));
                        });
                    }
                }))
        );
    }
}
