package com.gengyun.senscloud.util.dynamiccommon.topage;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONNull;
//import net.sf.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.*;
//
///**
// * 数据解析方法（动态进页面前）
// * User: sps
// * Date: 2020/06/11
// * Time: 上午09:20
// */
public class DataUtil {
//    private static final Logger logger = LoggerFactory.getLogger(DataUtil.class);
//
//    /**
//     * 数据初始化
//     */
//    public static Map<String, Object> initData(Map<String, Object> allMap) {
//        // 页面信息
//        allMap.put("pageView", new LinkedHashMap<>());
//        allMap.put("tabInfo", new ArrayList<Map<String, Object>>());
//        // 模块信息
//        allMap.put("sectionList", new ArrayList<Map<String, Object>>());
//        allMap.put("sectionInfo", new HashMap<String, Map<String, Object>>());
//        allMap.put("sectionKeys", new LinkedHashSet<String>());
//        allMap.put("sectionContentList", new HashMap<String, List<String>>());
//        allMap.put("selectKeyList", new HashMap<String, List<String>>());
//        allMap.put("specLoadList", new ArrayList<Map<String, Object>>());
//        allMap.put("linkAgeInfo", new HashMap<String, Object>()); // 所有联动根字段
//        allMap.put("linkAgeDtlList", new HashMap<String, Object>()); // 所有待联动字段信息
//        allMap.put("pageRowDataInfo", new HashMap<String, Object>()); // 行编辑信息
//        allMap.put("pageAllValue", new HashMap<String, Object>()); // 所有字段信息
//        allMap.put("pageImgInfo", new HashMap<String, Object>()); // 图片信息
//        allMap.put("pageListColumn", new HashMap<String, Object>()); // 列表列信息
//        allMap.put("pageSelectDefaultOption", new HashMap<String, Object>());
//        allMap.put("fieldAttrByLinkCodeList", new ArrayList());
//        allMap.put("hideList", new ArrayList<Map<String, Object>>());
//        allMap.put("btnList", new ArrayList<Map<String, Object>>());
//        return allMap;
//    }
//
//    /**
//     * 原数据json（键值对）缓存
//     *
//     * @param allMap
//     * @param strInfo
//     * @return
//     */
//    public static void doCatchOldData(Map<String, Object> allMap, Map<String, String> strInfo) {
//        Map<String, Object> oldData = (Map<String, Object>) allMap.get("oldData");
//        allMap.put("isMain", 0);
//        allMap.put("status", 0);
//        allMap.put("subWorkCnt", 0);
//        if (null != oldData && oldData.size() > 0) {
//            strInfo.put("businessNo", (String) oldData.get("businessNo"));
//            allMap.put("feeRight", oldData.get("feeRight"));
//            allMap.put("isMain", null == oldData.get("is_main") ? 1 : oldData.get("is_main"));
//            allMap.put("subWorkCnt", null == oldData.get("subWorkCnt") ? 0 : oldData.get("subWorkCnt"));
//            try {
//                allMap.put("status", oldData.get("status"));
//            } catch (Exception stExp) {
//                allMap.put("status", ((Long) oldData.get("status")).intValue());
//            }
//            Boolean isAssetScan = (Boolean) allMap.get("isAssetScan");
//            if (!isAssetScan) {
//                Object oldDodyProperty = oldData.get("body_property"); // 原模板数据
//                if (null != oldDodyProperty && !"".equals(oldDodyProperty)) {
//                    Map<String, JSONObject> dataJsonInfo = new HashMap<String, JSONObject>();
//                    JSONArray arrays = JSONArray.fromObject(JSONObject.fromObject(oldDodyProperty).get("value"));
//                    JSONObject data = null;
//                    for (Object object : arrays) {
//                        data = JSONObject.fromObject(object);
//                        dataJsonInfo.put(data.get("fieldFormCode").toString(), data);
//                    }
//                    allMap.put("dataJsonInfo", dataJsonInfo);
//                }
//            }
//        }
//    }
//
//    /**
//     * 设置新模板默认值
//     *
//     * @param result
//     * @param data
//     * @param strInfo
//     * @param allMap
//     */
//    public static void doSetDefaultValue(JSONObject result, JSONObject data, Map<String, String> strInfo, Map<String, Object> allMap) {
//        Map<String, Object> oldData = (Map<String, Object>) allMap.get("oldData");
//        Map<String, JSONObject> dataJsonInfo = (Map<String, JSONObject>) allMap.get("dataJsonInfo");
//        data.put("fieldTextName", data.get("fieldName"));
//        strInfo.put("fieldFormCode", data.getString("fieldFormCode"));
//        strInfo.put("fieldCode", data.getString("fieldCode"));
//        strInfo.put("fieldViewType", data.getString("fieldViewType"));
//
//        String fieldFormCode = strInfo.get("fieldFormCode");
//        allMap.put("value", data.get("fieldValue"));
//        allMap.put("fieldTextValue", data.get("fieldTextValue"));
//        if (RegexUtil.isNull((String) allMap.get("value"))) {
//            // 获取原值
//            if (null != dataJsonInfo && dataJsonInfo.size() > 0) {
//                JSONObject oldJsonData = dataJsonInfo.get(fieldFormCode);
//                if (null != oldJsonData) {
//                    allMap.put("value", oldJsonData.get("fieldValue"));
//                    allMap.put("fieldTextValue", oldJsonData.get("fieldTextValue"));
//                    // 对象
//                    if ("8".equals(strInfo.get("fieldViewType")) && oldJsonData.containsKey("relationContent")) {
//                        try {
//                            data.put("relationContent", oldJsonData.get("relationContent").toString().replace(":null", ":\"\""));
//                            result.put("relationNames", oldJsonData.get("relationNames").toString().replace(":null", ":\"\""));
//                        } catch (Exception taskExp) {
//                            logger.warn("workInfoRelationWarn");
//                        }
//                    }
//                    // 任务
//                    if ("9".equals(strInfo.get("fieldViewType")) && oldJsonData.containsKey("taskContent")) {
//                        try {
//                            data.put("taskContent", oldJsonData.get("taskContent").toString().replace(":null", ":\"\""));
//                        } catch (Exception taskExp) {
//                            logger.warn("workInfoTaskWarn");
//                        }
//                    }
//                    // 列表
//                    if ("12".equals(strInfo.get("fieldViewType")) && oldJsonData.containsKey("bomContent")) {
//                        try {
//                            data.put("bomContent", oldJsonData.get("bomContent").toString().replace(":null", ":\"\""));
//                        } catch (Exception taskExp) {
//                            logger.warn("workInfoListWarn");
//                        }
//                    }
//                }
//            }
//
//            // 取原固定字段值
//            if (null != oldData && oldData.size() > 0) {
//                // 取原固定字段值
//                if (oldData.containsKey(strInfo.get("fieldCode"))) {
//                    allMap.put("value", oldData.get(strInfo.get("fieldCode"))); // 公共字段值
//                } else if ("9".equals(strInfo.get("fieldViewType"))) {
//                    String businessNo = strInfo.get("businessNo");
//                    if (null != businessNo && ("51".equals(businessNo) || "52".equals(businessNo))) {
//                        try {
//                            List<Map<String, Object>> taskList = (List<Map<String, Object>>) oldData.get("taskContent");
//                            for (Map<String, Object> task : taskList) {
//                                task.put("taskRight", "readonly");
//                                task.put("nowModalKey", fieldFormCode);
//                            }
//                            JSONObject taskListObject = new JSONObject();
//                            taskListObject.put("key", "WTT000049");
//                            taskListObject.put("data", taskList);
//                            data.put("taskContent", taskListObject); // 任务
//                        } catch (Exception taskExp) {
//                            logger.warn("任务数据解析异常！" + strInfo.get("subWorkCode"));
//                        }
//                    }
//                }
//            }
//            Object value = allMap.get("value");
//            if (null == value || "null".equals(value) || JSONNull.getInstance().equals(value)) {
//                allMap.put("value", "");
//            }
//            data.put("fieldValue", allMap.get("value")); // 数据回显
//        }
//
//        // 对象类型（页面特殊操作，单独列出）
//        if ("relation_type".equals(strInfo.get("fieldCode"))) {
//            if (null == allMap.get("value") || "".equals(allMap.get("value"))) {
//                allMap.put("value", allMap.get("relation_type"));
//                data.put("fieldValue", allMap.get("relation_type")); // 数据回显
//            }
//            result.put("relationType", allMap.get("relation_type"));
//        }
//    }
//
//    /**
//     * 尾部处理
//     *
//     * @param result
//     * @param allMap
//     */
//    public static void setTailInfo(JSONObject result, Map<String, Object> allMap) {
////            pageAllValue.put("pageListColumn", mapInfo.get("pageListColumn"));
////            pageAllValue.put("pageImgInfo", mapInfo.get("pageImgInfo"));
////            // 扫码按钮是否显示【处理中】
////            String showScan = (String) request.getAttribute("showScan");
////            if (RegexUtil.isNotNull(showScan)) {
////                pageAllValue.put("showScan", showScan);
////            }
////
////            // 库存消耗类型校验
////            if (pageAllValue.containsKey("checkBomConsume")) {
////                SystemConfigData scd = systemConfigService.getSystemConfigData(schemaName, SystemConfigConstant.BOM_CONSUME_TYPE);
////                String bomConsumeType = scd.getSettingValue();
////                if ("2".equals(bomConsumeType)) {
////                    pageAllValue.put("checkBomConsume", bomConsumeType);
////                } else {
////                    pageAllValue.remove("checkBomConsume");
////                }
////            }
//
//        RelationUtil.doSetFieldAttrByLinkCodeList(allMap);
//        Map<String, Object> pageView = (Map<String, Object>) allMap.get("pageView");
//        Map<String, Object> pageAllValue = (Map<String, Object>) allMap.get("pageAllValue");
//        if (pageAllValue.containsKey("showHistory") && RegexUtil.isNotNull((String) pageAllValue.get("showHistory"))) {
//            pageView.put("historyData", result.get("historyData"));
//        }
//        result.remove("historyData");
//        result.put("pageListColumn", allMap.get("pageListColumn"));
////            result.put("pageRowDataInfo", pageRowDataInfo);
//            result.put("linkAgeInfo", allMap.get("linkAgeInfo"));
//            result.put("linkAgeDtlList", allMap.get("linkAgeDtlList"));
//        List specLoadList = (List) allMap.get("specLoadList");
//        if (null != specLoadList && specLoadList.size() > 0) {
//            result.put("specLoadList", specLoadList);
//        }
////            result.put("pageSelectDefaultOption", pageSelectDefaultOption);
////            result.put("selectKeyList", selectKeyList);
//        List btnList = (List) allMap.get("btnList");
//        if (null != btnList && btnList.size() > 0) {
//            if (btnList.size() == 1) {
//                String btnCode = btnList.get(0).toString();
//                JSONObject btnInfo = JSONObject.fromObject(pageAllValue.get(btnCode));
//                btnInfo.put("mainBtn", "mainBtn");
//                pageAllValue.put(btnCode, btnInfo);
//            }
//            Map<String, Object> tabbarInfo = new HashMap<>();
//            tabbarInfo.put("code", "pageBottomTabbar");
//            tabbarInfo.put("content", btnList);
//            pageView.put("tabbarInfo", tabbarInfo);
//        }
//        result.put("dataDtl", pageAllValue);
//        List tabInfo = (List) allMap.get("tabInfo");
//        if (null != tabInfo && tabInfo.size() > 0) {
//            pageView.put("tabInfo", tabInfo);
//        }
//        Map<String, List<String>> selectKeyList = (Map<String, List<String>>) allMap.get("selectKeyList");
//        if (null != selectKeyList && selectKeyList.size() > 0) {
//            List<Map<String, Object>> selectList = new ArrayList<Map<String, Object>>();
//            Set<String> keys = selectKeyList.keySet();
//            for (String sk : keys) {
//                Map<String, Object> map = new HashMap<String, Object>();
//                map.put("selectKey", sk);
//                map.put("selectIds", selectKeyList.get(sk));
//                selectList.add(map);
//            }
//            result.put("selectList", selectList);
//        }
//        result.put("pageView", pageView);
//        //时区转换处理
//        SCTimeZoneUtil.responseJsonObjectDateHandler(result, new String[]{"createTime"});
////            result.put("hideList", hideList);
////            result.put("btnList", btnList);
//    }
}
