package com.gengyun.senscloud.util;

//import com.gengyun.senscloud.common.SensConstant;
//import org.apache.commons.lang.ArrayUtils;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.postgresql.util.PGobject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.servlet.http.HttpServletRequest;
//import java.lang.reflect.Field;
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * 日期的时区处理工具类
// *
// */
public final class SCTimeZoneUtil {
//    private static final Logger logger = LoggerFactory.getLogger(SCTimeZoneUtil.class);
//
//    public static final String YYYYMMDD_ZH = "yyyy年MM月dd日";
//    public static final String SUFFIX_MIN = "00:00:00";
//    public static final String SUFFIX_MAX = "23:59:59";
//
//
//    /*  业务常量 */
//    public static final String BOOTSTRAP_TABLE_DATE_LIST_KEY = "rows";
//    public static final String MAP_KEY_COMMENT_LIST = "comment_list";
//    public static final String MAP_KEY_COMMENT_TIME = "comment_time";
//    public static final String BODY_PROPERTY = "body_property";
//    public static final String BODY_PROPERTY_VALUE = "value";
//    public static final String TAMPLATE_DATA_INFO = "dataInfo";
//    public static final String TAMPLATE_DATATIME_TYPE = "dateTime@";
//    public static final String FIELD_VIEW_RELATION = "fieldViewRelation";
//    public static final String FIELD_VIEW_RELATION_DETAIL = "fieldViewRelationDetail";
//    public static final String FIELD_KEY_TIMEZONE = "timeZone";
//    public static final String FIELD_VIEW_TYPE = "fieldViewType";
//    public static final String FIELD_VIEW_TYPE_DATETIME = "4";
//    public static final String FIELD_VIEW_RELATION_TYPE_8 = "8";
//    public static final String FIELD_VALUE = "fieldValue";
//    public static final String FIELD_VIEW_RELATION_DETAIL_DATE_INFO = "dateInfo";
//
//    /*======================================================    【目标时区 =》服务器时区】  请求参数相关日期时间的时区处理   START ==================================================*/
//
//
//    /**
//     *  请求日期参数格式为：Constants.DATE_FMT_SS
//     *  将【指定时区】的日期时间时间戳根据转换为【服务器端】日期时间戳
//     * @param dateTimeKey  请求时间参数的key.
//     * @return 转化后的服务器端日期时间.
//     * @return
//     */
//    public static String requestParamHandler(String dateTimeKey) {
//        String dateTime = HttpRequestUtils.getRequest().getParameter(dateTimeKey);
//        if(StringUtils.isBlank(dateTime))
//            return null;
//
//        String srcTimeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        //没有时区ID，则不转换时间，直接返回原值
//        if(StringUtils.isBlank(srcTimeZoneId))
//            return dateTime;
//
//        return string2SystemTimezoneDefault(dateTime, srcTimeZoneId);
//    }
//
//    /**
//     *  获取时区转换后的时间（自动补上 00:00:00）
//     *  请求日期参数格式为：Constants.DATE_FMT
//     *  将【指定时区】的日期时间时间戳根据转换为【服务器端】日期时间戳
//     * @param dateTimeKey  请求时间参数的key.
//     * @return 转化后的服务器端日期时间.
//     * @return
//     */
//    public static String requestParamHandlerWithMinSuffix(String dateTimeKey) {
//        return requestParamHandler(dateTimeKey, SUFFIX_MIN);
//    }
//
//    /**
//     *  获取时区转换后的时间（自动补上 23:59:59）
//     *  请求日期参数格式为：Constants.DATE_FMT
//     *  将【指定时区】的日期时间时间戳根据转换为【服务器端】日期时间戳
//     * @param dateTimeKey  请求时间参数的key.
//     * @return 转化后的服务器端日期时间.
//     * @return
//     */
//    public static String requestParamHandlerWithMaxSuffix(String dateTimeKey) {
//        return requestParamHandler(dateTimeKey, SUFFIX_MAX);
//    }
//
//    /**
//     *  请求日期参数格式为：Constants.DATE_FMT
//     *  将【指定时区】的日期时间时间戳根据转换为【服务器端】日期时间戳
//     * @param dateTimeKey  请求时间参数的key.
//     * @param suffix    后缀 （格式：“00:00:00”、“23:59:59”）
//     * @return 转化后的服务器端日期时间.
//     * @return
//     */
//    public static String requestParamHandler(String dateTimeKey, String suffix) {
//        String dateTime = HttpRequestUtils.getRequest().getParameter(dateTimeKey);
//        if(StringUtils.isBlank(dateTime))
//            return null;
//
//        if(StringUtils.isNotBlank(suffix))
//            dateTime += " " + suffix.trim();
//        else
//            dateTime += " " + SUFFIX_MIN;
//
//        String srcTimeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        //没有时区ID，则不转换时间，直接返回原值
//        if(StringUtils.isBlank(srcTimeZoneId))
//            return dateTime;
//
//        return string2SystemTimezoneDefault(dateTime, srcTimeZoneId);
//    }
//
//    /**
//     *  模板页面的请求参数处理
//     *  将【指定时区】的日期时间时间戳根据转换为【服务器端】日期时间戳
//     * @param mapObject    动态解析后的模板数据
//     */
//    public static void requestMapParamHandler(Map<String, Object> mapObject){
//        String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        //没有时区ID，则不转换时间
//        if(StringUtils.isBlank(timeZoneId) || mapObject == null || !mapObject.containsKey(TAMPLATE_DATA_INFO))
//            return;
//
//        Map<String, Object> dataInfo = (Map<String, Object>)mapObject.get(TAMPLATE_DATA_INFO);
//        if(dataInfo == null)
//            return;
//
//        for(String key : dataInfo.keySet()){
//            Map<String, Object> obj = (Map<String, Object>)dataInfo.get(key);
//            if(obj != null) {
//                for (String k : obj.keySet()) {
//                    if(k.startsWith(TAMPLATE_DATATIME_TYPE)) {//如果是时间类型的字段，则要进行时区转换
//                        String fieldKey = (String)obj.get(k);
//                        obj.put(fieldKey, string2SystemTimezoneDefault((String)obj.get(fieldKey),timeZoneId));
//                    }else if(k.equals(BODY_PROPERTY)){//body_property字段需要特殊处理
//                        String bodyProperty = bodyProperty2SystemTimeZoneHandler((String)obj.get(k), timeZoneId);
//                        obj.put(k, bodyProperty);
//                    }
//                }
//            }
//        }
//    }
//
//    /*======================================================    【目标时区 =》服务器时区】  请求参数相关日期时间的时区处理   END  ==================================================*/
//
//    /*======================================================    【服务器时区 =》目标时区】  返回参数相关日期时间的时区处理   START  ==================================================*/
//
//    /**
//     * 响应返回时，BootstrapTable表格列表数据中日期时间的时区转化
//     * @param result
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static void responseTableDateListHandler(JSONObject result, String[] fields) {
//        responseTableDateListHandler(result, fields, null);
//    }
//
//    /**
//     * 响应返回时，BootstrapTable表格列表数据中日期时间的时区转化
//     * @param result
//     * @param fields    需要转换时区的日期字段key
//     * @param format    指定待处理时间字符串的日期格式
//     */
//    public static void responseTableDateListHandler(JSONObject result, String[] fields, String[] format) {
//        String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        //没有时区ID，则不转换时间
//        if(StringUtils.isBlank(timeZoneId) || result == null || !result.has(BOOTSTRAP_TABLE_DATE_LIST_KEY) || result.getJSONArray(BOOTSTRAP_TABLE_DATE_LIST_KEY) == null
//                || result.getJSONArray(BOOTSTRAP_TABLE_DATE_LIST_KEY).length() == 0)
//            return;
//
//        JSONArray jsonArray = result.getJSONArray(BOOTSTRAP_TABLE_DATE_LIST_KEY);
//        for(int i = 0;i<jsonArray.length();i++){
//            JSONObject jsonObject = jsonArray.getJSONObject(i);
//            if(jsonObject != null){
//                for(String key : jsonObject.keySet()){
//                    //body_property字段需要特殊处理
//                    if(key.equals(BODY_PROPERTY)){
//                        analysisBodyProperty(jsonObject, timeZoneId);
//                    }else{
//                        analysisJsonObject(jsonObject, key, jsonObject.get(key), timeZoneId, fields, format);
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 响应返回时，org.json.JSONArray类型数据中日期时间的时区转化
//     * @param jsonArray
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static JSONArray responseJsonArrayDateHandler(JSONArray jsonArray, String[] fields) {
//        if(jsonArray != null && jsonArray.length()>0){
//            JSONArray newJsonnArray = new JSONArray();
//            for(int i=0;i<jsonArray.length();i++){
//                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                newJsonnArray.put(responseJsonObjectDateHandler(jsonObject, fields));
//            }
//            return newJsonnArray;
//        }
//        return jsonArray;
//    }
//
//    /**
//     * 响应返回时，org.json.JSONObject类型数据中日期时间的时区转化
//     * @param jsonObject
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static JSONObject responseJsonObjectDateHandler(JSONObject jsonObject, String[] fields) {
//        String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        //没有时区ID，则不转换时间
//        if(StringUtils.isBlank(timeZoneId) || jsonObject == null)
//            return jsonObject;
//
//        for(String key : jsonObject.keySet()){
//            //body_property字段需要特殊处理
//            if(key.equals(BODY_PROPERTY)) {
//                analysisBodyProperty(jsonObject, timeZoneId);
//            }else {
//                analysisJsonObject(jsonObject, key, jsonObject.get(key), timeZoneId, fields, null);
//            }
//        }
//        return jsonObject;
//    }
//
//    /**
//     * 响应返回时，org.json.JSONObject类型数据中日期时间的时区转化
//     * @param jsonObject
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static net.sf.json.JSONObject responseJsonObjectDateHandler(net.sf.json.JSONObject jsonObject, String[] fields) {
//        String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        //没有时区ID，则不转换时间
//        if(StringUtils.isBlank(timeZoneId) || jsonObject == null)
//            return jsonObject;
//
//        for(Object key : jsonObject.keySet()){
//            //body_property字段需要特殊处理
//            if(key.equals(BODY_PROPERTY)) {
//                analysisBodyProperty(jsonObject, timeZoneId);
//            }else {
//                analysisJsonObject(jsonObject, (String)key, jsonObject.get(key), timeZoneId, fields, null);
//            }
//        }
//        return jsonObject;
//    }
//
//    /**
//     * 遍历json所有属性以及子级属性，并对指定的属性进行时区处理
//     * @param jsonObject
//     * @param field
//     * @param fieldObj
//     * @param timeZoneId
//     * @param fields        需要时区处理的属性字段
//     * @param format
//     */
//    private static void analysisJsonObject(JSONObject jsonObject, String field, Object fieldObj, String timeZoneId, String[] fields, String[] format) {
//        if(fieldObj == null)
//            return;
//
//        if(fieldObj instanceof String && fields != null && fields.length > 0) {
//            List<String> fieldsArray = Arrays.asList(fields);
//            if(fieldsArray.contains(field))
//                jsonObject.put(field, string2TargetTimezone(format == null?Constants.DATE_FMT_SS:format[fieldsArray.indexOf(field)], (String) fieldObj, Constants.DATE_FMT_SS, timeZoneId));//转换系统时间到对应时区的时间
//
//        }else{
//            analysisJson(fieldObj, timeZoneId, fields, format);
//        }
//    }
//
//    /**
//     * 遍历json所有属性以及子级属性，并对指定的属性进行时区处理
//     * @param jsonObject
//     * @param field
//     * @param fieldObj
//     * @param timeZoneId
//     * @param fields        需要时区处理的属性字段
//     * @param format
//     */
//    private static void analysisJsonObject(net.sf.json.JSONObject jsonObject, String field, Object fieldObj, String timeZoneId, String[] fields, String[] format) {
//        if(fieldObj == null)
//            return;
//
//        if(fieldObj instanceof String && fields != null && fields.length > 0) {
//            List<String> fieldsArray = Arrays.asList(fields);
//            if(fieldsArray.contains(field))
//                jsonObject.put(field, string2TargetTimezone(format == null?Constants.DATE_FMT_SS:format[fieldsArray.indexOf(field)], (String) fieldObj, Constants.DATE_FMT_SS, timeZoneId));//转换系统时间到对应时区的时间
//
//        }else{
//            analysisJson(fieldObj, timeZoneId, fields, format);
//        }
//    }
//
//    /**
//     * 遍历jsonArray所有属性以及子级属性，并对指定的属性进行时区处理
//     * @param jsonArray
//     * @param timeZoneId
//     * @param fields        需要时区处理的属性字段
//     * @param format
//     */
//    private static void analysisJsonArray(JSONArray jsonArray, String timeZoneId, String[] fields, String[] format){
//        if(jsonArray == null)
//            return;
//
//        for(int i=0;i<jsonArray.length();i++){
//            Object obj = jsonArray.get(i);
//            if(obj == null)
//                return;
//
//            analysisJson(obj, timeZoneId, fields, format);
//        }
//    }
//
//    /**
//     * 模板json数据-body_property字段特殊处理
//     * @param jsonObject
//     * @param timeZoneId
//     */
//    private static void analysisBodyProperty(JSONObject jsonObject, String timeZoneId) {
//        JSONObject bodyProperty = jsonObject.getJSONObject(BODY_PROPERTY);
//        bodyProperty.put(BODY_PROPERTY_VALUE, bodyProperty2TargetTimeZoneHandler(bodyProperty.optString(BODY_PROPERTY_VALUE), timeZoneId));
//    }
//
//    /**
//     * 模板json数据-body_property字段特殊处理
//     * @param jsonObject
//     * @param timeZoneId
//     */
//    private static void analysisBodyProperty(net.sf.json.JSONObject jsonObject, String timeZoneId) {
//        net.sf.json.JSONObject bodyProperty = jsonObject.getJSONObject(BODY_PROPERTY);
//        bodyProperty.put(BODY_PROPERTY_VALUE, bodyProperty2TargetTimeZoneHandler(bodyProperty.optString(BODY_PROPERTY_VALUE), timeZoneId));
//    }
//
//    /**
//     *  jsonObject、jsonArray类型数据解析
//     * @param obj
//     * @param timeZoneId
//     * @param fields
//     * @param format
//     */
//    private static void analysisJson(Object obj, String timeZoneId, String[] fields, String[] format) {
//        if(obj instanceof JSONObject){
//            JSONObject childJsonObject = (JSONObject)obj;
//            for(String key : childJsonObject.keySet()){
//                //body_property字段需要特殊处理
//                if(key.equals(BODY_PROPERTY)) {
//                    analysisBodyProperty(childJsonObject, timeZoneId);
//                }else{
//                    analysisJsonObject(childJsonObject, key, childJsonObject.get(key), timeZoneId, fields, format);
//                }
//            }
//        }else if(obj instanceof JSONArray){
//            analysisJsonArray((JSONArray)obj, timeZoneId, fields, format);
//        }
//    }
//
//    /**
//     * 响应返回时，对象列表数据中日期时间的时区转化
//     * @param objectList
//     */
//    public static void responseObjectListDataHandler(List<? extends Object> objectList) {
//        if(objectList != null && objectList.size() > 0){
//            String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//            for(Object obj : objectList){
//                responseObjectDataHandler(obj, null, timeZoneId);
//            }
//        }
//    }
//
//    /**
//     * 响应返回时，对象列表数据中日期时间的时区转化
//     * @param objectList
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static void responseObjectListDataHandler(List<? extends Object> objectList, String[] fields) {
//        if(objectList != null && objectList.size() > 0){
//            String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//            for(Object obj : objectList){
//                responseObjectDataHandler(obj, fields, timeZoneId);
//            }
//        }
//    }
//
//    /**
//     * 响应返回时，对象数据中日期时间的时区转化
//     * @param object
//     */
//    public static void responseObjectDataHandler(Object object) {
//        responseObjectDataHandler(object, null);
//    }
//
//    /**
//     * 响应返回时，对象数据中日期时间的时区转化
//     * @param object
//     * @param handleFields 需要转换时区的日期字段key
//     */
//    public static void responseObjectDataHandler(Object object, String[] handleFields) {
//        String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        responseObjectDataHandler(object, handleFields, timeZoneId);
//    }
//
//    /**
//     * 响应返回时，对象数据中日期时间的时区转化
//     * @param object
//     * @param handleFields  需要转换时区的日期字段key
//     * @param timeZoneId    客户端时区ID
//     */
//    private static void responseObjectDataHandler(Object object, String[] handleFields, String timeZoneId) {
//        //没有时区ID，则不转换时间
//        if(StringUtils.isBlank(timeZoneId) || object == null)
//            return;
//
//        List<String> handleFieldsArray = handleFields==null?new ArrayList<>():Arrays.asList(handleFields);
//        //反射机制遍历处理对象时间字段
//        Class cls = object.getClass();
//        Field[] fields = getBeanFields(cls, new Field[]{});
//        for(Field f : fields){
//            f.setAccessible(true);
//            if(isDateType(f.getType()) || handleFieldsArray.contains(f.getName())){
//                try {
//                    f.set(object, convertTimeData(f.get(object), timeZoneId));
//                } catch (IllegalAccessException e) {
//                    logger.error("", e);
//                }
//            }else if(f.getName().equals(BODY_PROPERTY)){
//                try {
//                    PGobject bodyProperty = (PGobject)f.get(object);
//                    bodyProperty.setValue(bodyProperty2TargetTimeZoneHandler(bodyProperty.getValue(), timeZoneId));
//                } catch (Exception e) {
//                    logger.error("", e);
//                }
//            }
//        }
//    }
//
//    /**
//     * 响应返回时，map列表数据中日期时间的时区转化
//     * @param mapList
//     */
//    public static void responseMapListDataHandler(List<Map<String, Object>> mapList) {
//        responseMapListDataHandler(mapList,null);
//    }
//
//    /**
//     * 响应返回时，map列表数据中日期时间的时区转化
//     * @param mapList
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static void responseMapListDataHandler(List<Map<String, Object>> mapList, String[] fields) {
//        if(mapList != null && mapList.size() > 0){
//            String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//            for(Map<String, Object> map : mapList){
//                responseMapDataHandler(map, fields, timeZoneId);
//            }
//        }
//    }
//
//    /**
//     * 响应返回时，map数据中日期时间的时区转化
//     * @param map
//     */
//    public static void responseMapDataHandler(Map<String, Object> map) {
//        responseMapDataHandler(map, null);
//    }
//
//    /**
//     * 响应返回时，map数据中日期时间的时区转化
//     * @param map
//     * @param fields 需要转换时区的日期字段key
//     */
//    public static void responseMapDataHandler(Map<String, Object> map, String[] fields) {
//        String timeZoneId = getTimeZoneIdFromRequest();//客户端时区ID
//        responseMapDataHandler(map, fields, timeZoneId);
//    }
//
//    /**
//     * 响应返回时，map数据中日期时间的时区转化
//     * @param map
//     * @param fields        需要转换时区的日期字段key
//     * @param timeZoneId    客户端时区ID
//     */
//    private static void responseMapDataHandler(Map<String, Object> map, String[] fields, String timeZoneId) {
//        //没有时区ID，则不转换时间
//        if(StringUtils.isBlank(timeZoneId) || map == null || map.size() == 0)
//            return;
//
//        if(map.containsKey(BODY_PROPERTY)){
//            PGobject bodyProperty = (PGobject)map.get(BODY_PROPERTY);
//            try {
//                bodyProperty.setValue(bodyProperty2TargetTimeZoneHandler(bodyProperty.getValue(), timeZoneId));
//            } catch (SQLException e) {
//                logger.error("", e);
//            }
//        }
//        if(map.containsKey(MAP_KEY_COMMENT_LIST)){//评论模块特殊处理
//            PGobject commentList = (PGobject)map.get(MAP_KEY_COMMENT_LIST);
//            try {
//                commentList.setValue(commentList2TargetTimeZoneHandler(commentList.getValue(), timeZoneId));
//            } catch (SQLException e) {
//                logger.error("", e);
//            }
//        }
//        Set<String> keySet = map.keySet();
//        for(String key : keySet){
//            Object obj = map.get(key);
//            if(isDateType(obj == null?null:obj.getClass()) || (fields != null && Arrays.asList(fields).contains(key) && !key.equals(BODY_PROPERTY))){
//                map.put(key, convertTimeData(map.get(key), timeZoneId));
//            }
//        }
//    }
//
//    /**
//     * 判断对象类型是否是时间格式
//     * @param c
//     * @return
//     */
//    private static boolean isDateType(Class<?> c) {
//        if(c == null)
//            return false;
//
//        if(c.equals(Date.class) || c.equals(java.sql.Date.class) || c.equals(Timestamp.class))
//            return true;
//
//        return false;
//    }
//
//    /**
//     * 数据字段时区处理
//     * @param obj
//     * @param timeZoneId
//     * @return
//     */
//    private static Object convertTimeData(Object obj, String timeZoneId) {
//        if(obj == null)
//            return null;
//
//        if(obj instanceof Timestamp){
//            String timestamp = Timestamp2TargetTimezone((Timestamp)obj, timeZoneId);
//            obj = Timestamp.valueOf(timestamp);
//        }else if(obj instanceof Date){
//            String date = date2TargetTimezone((Date)obj, timeZoneId);
//            SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            try {
//                obj = formatter.parse(date);
//            } catch (ParseException e) {
//                logger.error("", e);
//            }
//        }else if(obj instanceof String){
//            obj = string2TargetTimezoneDefault((String)obj, timeZoneId);
//        }
//        return obj;
//    }
//
//    /*======================================================    【服务器时区 =》目标时区】  返回参数相关日期时间的时区处理   END  ==================================================*/
//
//    /**
//     * 获取客户端登录地所在时区
//     * @return
//     */
//    public static String getTimeZoneIdFromRequest(){
//        try {
////            return "GMT+0700"; //设置指定时区(测试不同时区时打开)
//            HttpServletRequest request = HttpRequestUtils.getRequest();
//            String clientType = request.getParameter("clientType");
//            if (clientType == null)
//                return (String) request.getSession().getAttribute(Constants.SESSION_KEY_TIME_ZONE_ID);
//
//            switch (clientType){
//                case SensConstant.PC_CLIENT_NAME:
//                    return (String) request.getSession().getAttribute(Constants.SESSION_KEY_TIME_ZONE_ID);
//                case SensConstant.APP_CLIENT_NAME:
//                case SensConstant.IOS_CLIENT_NAME:
//                case SensConstant.WECHAT_CLIENT_NAME:
//                    return request.getParameter(Constants.SESSION_KEY_TIME_ZONE_ID);
//                default:
//                    return null;
//            }
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    /**
//     * 将【指定时区】的日期时间字符串根据转换为【服务器端】日期时间.
//     *
//     * @param srcDateTime   待转化的日期时间.
//     * @param srcTimeZoneId 待转化日期所在的时区编号.
//     * @return 转化后的服务器端日期时间.
//     * @see #string2SystemTimezone(String, String, String, String)
//     */
//    public static String string2SystemTimezoneDefault(String srcDateTime, String srcTimeZoneId) {
//        return string2SystemTimezone(Constants.DATE_FMT_SS, srcDateTime, Constants.DATE_FMT_SS, srcTimeZoneId);
//    }
//
//    /**
//     * 将【指定时区】的日期时间字符串转换为【服务器端】日期时间.
//     *
//     * @param srcFormater   待转化的日期时间的格式.
//     * @param srcDateTime   待转化的日期时间字符串.
//     * @param srcTimeZoneId 待转化日期所在的时区编号.
//     * @param dstFormater   服务器端的日期时间的格式.
//     * @return 转化后的服务器端日期时间.
//     */
//    public static String string2SystemTimezone(String srcFormater, String srcDateTime, String srcTimeZoneId, String dstFormater) {
//        if (StringUtils.isBlank(srcFormater) || StringUtils.isBlank(srcDateTime) || StringUtils.isBlank(srcTimeZoneId))
//            return null;
//
//        SimpleDateFormat sdf = new SimpleDateFormat(srcFormater);
//        try {
//            Date d = sdf.parse(srcDateTime);
//            return date2SystemTimezone(d, dstFormater, srcTimeZoneId);
//        } catch (ParseException e) {
//            logger.error("", e);
//            return srcDateTime;
//        } finally {
//            sdf = null;
//        }
//    }
//
//    /**
//     * 将【指定时区】的日期时间转换为【服务器端】日期时间.
//     * @param dateTime      待转化的日期时间.
//     * @param srcTimeZoneId 待转化日期所在的时区编号.
//     * @param dstFormater   目标的日期时间的格式.
//     * @return  转化后的服务器端日期时间.
//     */
//    public static String date2SystemTimezone(Date dateTime, String srcTimeZoneId, String dstFormater) {
//        if (StringUtils.isBlank(dstFormater) || StringUtils.isBlank(srcTimeZoneId))
//            return null;
//
//        int diffTime = getDiffTimeZoneRawOffset(srcTimeZoneId);
//        long nowTime = dateTime.getTime();
//        long systemNowTime = nowTime + diffTime;
//        dateTime = new Date(systemNowTime);
//        return date2String(dstFormater, dateTime);
//    }
//
//    /**
//     * 将【服务器端】日期时间字符串转换为【指定时区】的日期时间.
//     *
//     * @param srcDateTime   待转化的日期时间.
//     * @param dstTimeZoneId 目标的时区编号.
//     * @return 转化后的日期时间.
//     * @see #string2TargetTimezone(String, String, String, String)
//     */
//    public static String string2TargetTimezoneDefault(String srcDateTime, String dstTimeZoneId) {
//        return string2TargetTimezone(Constants.DATE_FMT_SS, srcDateTime, Constants.DATE_FMT_SS, dstTimeZoneId);
//    }
//
//    /**
//     * 将【服务器端】日期时间字符串转换为【指定时区】的日期时间.
//     *
//     * @param srcFormater   待转化的日期时间的格式.
//     * @param srcDateTime   待转化的日期时间字符串.
//     * @param dstFormater   目标的日期时间的格式.
//     * @param dstTimeZoneId 目标的时区编号.
//     * @return 转化后的日期时间.
//     */
//    public static String string2TargetTimezone(String srcFormater, String srcDateTime, String dstFormater, String dstTimeZoneId) {
//        if (StringUtils.isBlank(srcFormater) || StringUtils.isBlank(srcDateTime))
//            return null;
//
//        SimpleDateFormat sdf = new SimpleDateFormat(srcFormater);
//        try {
//            Date d = sdf.parse(srcDateTime);
//            return date2TargetTimezone(d, dstFormater, dstTimeZoneId);
//        } catch (ParseException e) {
//            logger.error("", e);
//            return srcDateTime;
//        } finally {
//            sdf = null;
//        }
//    }
//
//    /**
//     * 将【服务器端】日期时间戳转换为【指定时区】的日期时间.
//     * @param timestamp      待转化的日期时间.
//     * @param dstTimeZoneId 目标的时区编号.
//     * @return  转化后的日期时间.
//     */
//    public static String Timestamp2TargetTimezone(Timestamp timestamp, String dstTimeZoneId) {
//        return date2TargetTimezone(new Date(timestamp.getTime()), dstTimeZoneId);
//    }
//
//    /**
//     * 将【服务器端】日期时间转换为【指定时区】的日期时间.
//     * @param dateTime      待转化的日期时间.
//     * @param dstTimeZoneId 目标的时区编号.
//     * @return  转化后的日期时间.
//     */
//    public static String date2TargetTimezone(Date dateTime, String dstTimeZoneId) {
//        return date2TargetTimezone(dateTime, Constants.DATE_FMT_SS, dstTimeZoneId);
//    }
//
//    /**
//     * 将【服务器端】日期时间转换为【指定时区】的日期时间.
//     * @param dateTime      待转化的日期时间.
//     * @param dstFormater   目标的日期时间的格式.
//     * @param dstTimeZoneId 目标的时区编号.
//     * @return  转化后的日期时间.
//     */
//    public static String date2TargetTimezone(Date dateTime, String dstFormater, String dstTimeZoneId) {
//        if (StringUtils.isBlank(dstFormater) || StringUtils.isBlank(dstTimeZoneId))
//            return null;
//
//        int diffTime = getDiffTimeZoneRawOffset(dstTimeZoneId);
//        long nowTime = dateTime.getTime();
//        long targetNowTime = nowTime - diffTime;
//        dateTime = new Date(targetNowTime);
//        return date2String(dstFormater, dateTime);
//    }
//
//    /**
//     * 获取系统当前默认时区与UTC的时间差.(单位:毫秒)
//     *
//     * @return 系统当前默认时区与UTC的时间差.(单位 : 毫秒)
//     */
//    private static int getDefaultTimeZoneRawOffset() {
//        return TimeZone.getDefault().getRawOffset();
//    }
//
//    /**
//     * 获取指定时区与UTC的时间差.(单位:毫秒)
//     *
//     * @param timeZoneId 时区Id
//     * @return 指定时区与UTC的时间差.(单位 : 毫秒)
//     */
//    private static int getTimeZoneRawOffset(String timeZoneId) {
//        return TimeZone.getTimeZone(timeZoneId).getRawOffset();
//    }
//
//    /**
//     * 获取系统当前默认时区与指定时区的时间差.(单位:毫秒)
//     *
//     * @param timeZoneId 时区Id
//     * @return 系统当前默认时区与指定时区的时间差.(单位 : 毫秒)
//     */
//    private static int getDiffTimeZoneRawOffset(String timeZoneId) {
//        return TimeZone.getDefault().getRawOffset() - TimeZone.getTimeZone(timeZoneId).getRawOffset();
//    }
//
//    /**
//     * 日期(时间)转化为字符串.
//     *
//     * @param formater 日期或时间的格式.
//     * @param aDate    java.util.Date类的实例.
//     * @return 日期转化后的字符串.
//     */
//    private static String date2String(String formater, Date aDate) {
//        if (formater == null || "".equals(formater))
//            return null;
//        if (aDate == null)
//            return null;
//        return (new SimpleDateFormat(formater)).format(aDate);
//    }
//
//    /**
//     * 获取所有的时区编号.
//     * 排序规则:按照ASCII字符的正序进行排序.
//     * 排序时候忽略字符大小写.
//     *
//     * @return 所有的时区编号(时区编号已经按照字符[忽略大小写]排序).
//     */
//    public static String[] fecthAllTimeZoneIds() {
//        Vector v = new Vector();
//        String[] ids = TimeZone.getAvailableIDs();
//        for (int i = 0; i < ids.length; i++) {
//            v.add(ids[i]);
//        }
//        java.util.Collections.sort(v, String.CASE_INSENSITIVE_ORDER);
//        v.copyInto(ids);
//        v = null;
//        return ids;
//    }
//
//    /**
//     * bodyProperty字段时区处理(系统时间 =》目标时区)
//     * @param commentList
//     * @param timeZoneId
//     */
//    public static String commentList2TargetTimeZoneHandler(String commentList, String timeZoneId){
//        if(StringUtils.isBlank(commentList))
//            return commentList;
//
//        JSONArray commentListArray = new JSONArray(commentList);
//        if(commentListArray != null && commentListArray.length() > 0){
//            for(int i=0;i<commentListArray.length();i++){
//                JSONObject fieldObj = commentListArray.getJSONObject(i);
//                fieldObj.put(MAP_KEY_COMMENT_TIME,string2TargetTimezoneDefault(fieldObj.optString(MAP_KEY_COMMENT_TIME), timeZoneId));//转换时间并回填值
//            }
//        }
//        return commentListArray.toString();
//    }
//
//    /**
//     * bodyProperty字段时区处理(系统时间 =》目标时区)
//     * @param bodyProperty
//     * @param timeZoneId
//     */
//    public static String bodyProperty2TargetTimeZoneHandler(String bodyProperty, String timeZoneId){
//        if(StringUtils.isBlank(bodyProperty))
//            return bodyProperty;
//
//        JSONArray bodyPropertyArray = new JSONArray(bodyProperty);
//        if(bodyPropertyArray != null && bodyPropertyArray.length() > 0){
//            for(int i=0;i<bodyPropertyArray.length();i++){
//                JSONObject fieldObj = bodyPropertyArray.getJSONObject(i);
//                if(needTimeZoneConvert(fieldObj)){
//                    fieldObj.put(FIELD_VALUE,string2TargetTimezoneDefault(fieldObj.optString(FIELD_VALUE), timeZoneId));//转换时间并回填值
//                }
//            }
//        }
//        return bodyPropertyArray.toString();
//    }
//
//    /**
//     * bodyProperty字段时区处理(目标时区 =》系统时间)
//     * @param bodyProperty
//     * @param timeZoneId
//     */
//    public static String bodyProperty2SystemTimeZoneHandler(String bodyProperty, String timeZoneId){
//        if(StringUtils.isBlank(bodyProperty))
//            return bodyProperty;
//
//        JSONArray bodyPropertyArray = new JSONArray(bodyProperty);
//        if(bodyPropertyArray != null && bodyPropertyArray.length() > 0){
//            for(int i=0;i<bodyPropertyArray.length();i++){
//                JSONObject fieldObj = bodyPropertyArray.getJSONObject(i);
//                if(needTimeZoneConvert(fieldObj)){
//                    fieldObj.put(FIELD_VALUE,string2SystemTimezoneDefault(fieldObj.optString(FIELD_VALUE), timeZoneId));//转换时间并回填值
//                }
//            }
//        }
//        return bodyPropertyArray.toString();
//    }
//
//    /**
//     * 判断字段是否需要时区转换
//     * @param fieldObj
//     * @return
//     */
//    private static boolean needTimeZoneConvert(JSONObject fieldObj){
//        if(fieldObj != null){
//            if(fieldObj.has(FIELD_VIEW_TYPE) && FIELD_VIEW_TYPE_DATETIME.equals(fieldObj.optString(FIELD_VIEW_TYPE))){
//                return true;
//            }else if(fieldObj.has(FIELD_VIEW_RELATION) && FIELD_VIEW_RELATION_TYPE_8.equals(fieldObj.optString(FIELD_VIEW_RELATION))){//判断是否是关联自定义字段
//                Object frd = fieldObj.get(FIELD_VIEW_RELATION_DETAIL);
//                if(frd == null)
//                    return false;
//
//                JSONObject fieldViewRelationDetail = null;
//                if(frd instanceof String) {
//                    if(StringUtils.isNotBlank((String)frd))
//                        fieldViewRelationDetail = new JSONObject(fieldObj.optString(FIELD_VIEW_RELATION_DETAIL));
//                    else
//                        return false;
//                }else
//                    fieldViewRelationDetail = (JSONObject) frd;
//
//                if(fieldViewRelationDetail!=null && fieldViewRelationDetail.has(FIELD_VIEW_RELATION_DETAIL_DATE_INFO)){
//                    JSONObject dateInfo = fieldViewRelationDetail.optJSONObject(FIELD_VIEW_RELATION_DETAIL_DATE_INFO);
//                    if(dateInfo != null && dateInfo.has(FIELD_KEY_TIMEZONE) && FIELD_KEY_TIMEZONE.equals(dateInfo.optString(FIELD_KEY_TIMEZONE)))//判断是否需要时区转换
//                        return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 获取月初日期
//     * @param year
//     * @param month
//     * @return
//     */
//    public static String getFirstDayOfMonth(int year, int month){
//        Calendar cl = Calendar.getInstance();
//        cl.set(year, month, 1);
//        cl.set(Calendar.HOUR_OF_DAY, 0);
//        cl.set(Calendar.MINUTE, 0);
//        cl.set(Calendar.SECOND, 0);
//        return new SimpleDateFormat(Constants.DATE_FMT_SS).format(cl.getTime());
//    }
//
//    /**
//     * 获取月初日期
//     * @param year
//     * @param month
//     * @return
//     */
//    public static String getFirstDayOfMonth(int year, int month, int offset){
//        Calendar cl = Calendar.getInstance();
//        cl.set(year, month, 1);
//        cl.set(Calendar.HOUR_OF_DAY, 0);
//        cl.set(Calendar.MINUTE, 0);
//        cl.set(Calendar.SECOND, 0);
//        cl.add(Calendar.MONTH, offset);
//        return new SimpleDateFormat(Constants.DATE_FMT_SS).format(cl.getTime());
//    }
//
//    /**
//     *  换算GMT值
//     * @return
//     */
//    public static float getTimeZoneOffset(){
//        int dufaultOffset = TimeZone.getDefault().getRawOffset();
//        return new BigDecimal(String.valueOf(dufaultOffset)).divide(new BigDecimal(60).multiply(new BigDecimal(60)).multiply(new BigDecimal(1000)), BigDecimal.ROUND_HALF_UP, 2).floatValue();
//    }
//
//    /**
//     * 获取一个类及其所有父类的所有字段
//     * @param cls
//     * @param fs
//     * @return
//     */
//    private static Field[] getBeanFields(Class cls, Field[] fs){
//        fs = (Field[]) ArrayUtils.addAll(fs, cls.getDeclaredFields());
//        if(cls.getSuperclass()!=null){
//            Class clsSup = cls.getSuperclass();
//            fs = getBeanFields(clsSup, fs);
//        }
//        return fs;
//    }
//
//    /**
//     * 测试的main方法.
//     *
//     * @param argc
//     */
//    public static void main(String[] argc) {
//        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
//        System.setProperty("user.timezone", "Asia/Shanghai");
////        String[] ids = fecthAllTimeZoneIds();
////        //显示世界每个时区当前的实际时间
////        for (int i = 0; i < ids.length; i++) {
////            System.out.println(" * " + ids[i] + "=" + string2TimezoneDefault(nowDateTime, ids[i]));
////        }
//        //显示程序运行所在地的时区
//        System.out.println("显示程序运行所在地的时区 TimeZone.getDefault().getID()=" + TimeZone.getDefault().getID());
//
//        // 获取 “GMT+08:00”对应的时区
//        TimeZone china = TimeZone.getTimeZone("GMT+06:00");
//        // 获取 “中国/重庆”对应的时区
//        TimeZone chongqing = TimeZone.getTimeZone("Pacific/Auckland");
//
//        //模拟服务器时间转指定时区时间
//        System.out.println("模拟服务器时间转指定时区时间    GMT+06:00:"+string2TargetTimezoneDefault("2019-10-10 10:10:10", china.getID())+"      Pacific/Auckland:"+string2TargetTimezoneDefault("2019-10-10 10:10:10", chongqing.getID()));
//
//        //模拟客户端(其它时区)时间转服务器端时间
//        System.out.println("模拟客户端(其它时区)时间转服务器端时间    GMT+06:00:"+string2SystemTimezoneDefault("2019-10-08 10:10:10", "GMT+06:00")+"      Pacific/Auckland:"+string2SystemTimezoneDefault("2019-10-08 10:10:10", chongqing.getID()));
//    }
}
