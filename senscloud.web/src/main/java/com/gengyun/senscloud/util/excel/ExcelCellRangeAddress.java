package com.gengyun.senscloud.util.excel;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * @author senscloud
 * @project freemarker-excel
 * @description: 合并单元格信息
 */
public class ExcelCellRangeAddress {

    private CellRangeAddress cellRangeAddress;

    private CellStyle cellStyle;

    public ExcelCellRangeAddress(CellRangeAddress cellRangeAddress, CellStyle cellStyle) {
        this.cellRangeAddress = cellRangeAddress;
        this.cellStyle = cellStyle;
    }

    public CellRangeAddress getCellRangeAddress() {
        return cellRangeAddress;
    }

    public void setCellRangeAddress(CellRangeAddress cellRangeAddress) {
        this.cellRangeAddress = cellRangeAddress;
    }

    public CellStyle getCellStyle() {
        return cellStyle;
    }

    public void setCellStyle(CellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }
}
