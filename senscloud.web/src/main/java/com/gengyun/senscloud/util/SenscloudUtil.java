package com.gengyun.senscloud.util;

import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.interceptor.BodyReaderRequestWrapper;
import com.gengyun.senscloud.model.MethodParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 通用工具
 * User: sps
 * Date: 2020/12/07
 * Time: 上午11:42
 */
public class SenscloudUtil {
    private static final Logger logger = LoggerFactory.getLogger(SenscloudUtil.class);

    /**
     * 生成企业库名
     *
     * @param companyId 企业号
     * @return 企业库名
     */
    public static String getSchemaByCompId(Object companyId) {
        return SensConstant.SCHEMA_PREFIX + companyId.toString();
    }

    /**
     * 根据客户端类别和账号以及time拼接字符串使用base64进行加密，获得token字符串
     *
     * @param clientType 客户端类别
     * @param account    账号
     * @return token
     */
    public static String getToken(String clientType, String account) {
        return ScdEncryptUtil.getBase64(clientType + account + SenscloudUtil.generateUUID());
    }

    /**
     * 生成UUID
     *
     * @return 不带"-"的UUID
     */
    public static String generateUUIDStr() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 获取当前系统时间
     *
     * @return 当前系统时间
     */
    public static Timestamp getNowTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 生成UUID
     *
     * @return 带"-"的UUID
     */
    private static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 根据最大值生成范围内随机数
     *
     * @param max 最大值
     * @return 随机数
     */
    public static int gnrRandNumByNum(int max) {
        return (int) (new Random().nextFloat() * max);
    }

    /**
     * 生成范围内随机数
     *
     * @param min 最小值
     * @param max 最大值
     * @return 随机数
     */
    public static int gnrRandNumInRange(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    /**
     * 生成范围内随机数（字符串）
     *
     * @param min 最小值
     * @param max 最大值
     * @return 随机数
     */
    public static String gnrStrRandNumInRange(int min, int max) {
        return String.valueOf(SenscloudUtil.gnrRandNumInRange(min, max));
    }

    /**
     * 分页参数处理
     *
     * @param methodParam 系统参数
     * @return 分页参数条件
     */
    public static String changePagination(MethodParam methodParam) {
        if (RegexUtil.booleanCheck(methodParam.getNeedPagination())) {
            int pageSize = RegexUtil.optSelectOrExpParam(methodParam.getPageSize(), LangConstant.TITLE_FF); // 页数未选择
            int pageNumber = RegexUtil.optIntegerOrExpParam(methodParam.getPageNumber(), LangConstant.COM_PAGES_A) - 1; // 页不能为空
            return " limit " + pageSize + " offset " + pageSize * pageNumber;
        }
        return "";
    }

    /**
     * 分页参数处理
     *
     * @param methodParam 系统参数
     * @return 分页参数条件
     */
    public static String changePaginationNoErr(MethodParam methodParam) {
        try {
            return changePagination(methodParam);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 分页参数处理
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     */
    public static void changePagination(MethodParam methodParam, Map<String, Object> paramMap) {
        int pageSize = RegexUtil.optSelectOrExpParam(methodParam.getPageSize(), LangConstant.TITLE_FF); // 页数未选择
        int pageNumber = RegexUtil.optIntegerOrExpParam(methodParam.getPageNumber(), LangConstant.COM_PAGES_A) - 1; // 页不能为空
        paramMap.put("pageSize", pageSize);
        paramMap.put("pageNumber", pageSize * pageNumber);
    }

    /**
     * 数据流关闭
     *
     * @param is 数据流
     */
    public static void closeInputStream(InputStream is) {
        try {
            if (null != is) {
                is.close();
            }
        } catch (Exception ignored) {
        }
    }

    /**
     * 输出流关闭
     *
     * @param os 数据流
     */
    public static void closeOutputStream(OutputStream os) {
        try {
            if (null != os) {
                os.flush();
                os.close();
            }
        } catch (Exception ignored) {
        }
    }

    /**
     * 输出流关闭
     *
     * @param os 数据流
     */
    public static void closeByteArrayOutputStream(ByteArrayOutputStream os) {
        try {
            if (null != os) {
                os.close();
                os.flush();
            }
        } catch (Exception ignored) {
        }
    }

    /**
     * 判断request中是否含有注入攻击字符
     *
     * @param fcList 验证规则
     * @return 验证结果
     */
    public static String injectInput(List<String> fcList) {
        return RegexUtil.optNotNull(HttpRequestUtils.getRequest()).map(rq -> {
            Enumeration e = rq.getParameterNames();
            String attributeName;
            String[] attributeValues;
            String inj = "";
            String nameHasXss;
            while (e.hasMoreElements()) {
                attributeName = (String) e.nextElement();
                // 不对密码信息进行过滤，一般密码中可以包含特殊字符
                if ("p".equals(attributeName) || "newPassWord".equals(attributeName)) {
                    continue;
                }
                nameHasXss = cleanXSS(attributeName, fcList);
                if (!"".equals(nameHasXss)) {
                    return nameHasXss;
                } else {
                    attributeValues = rq.getParameterValues(attributeName);
                    for (String value : attributeValues) {
                        if (RegexUtil.optIsPresentStr(value)) {
                            inj = cleanXSS(value, fcList);
                            if (!"".equals(inj)) {
                                return inj;
                            }
                        }
                    }
                }
            }

            try {
                String dataInfo = new BodyReaderRequestWrapper(rq).getBodyString(rq);
                if (RegexUtil.optIsPresentStr(dataInfo)) {
                    JSONObject parameterMap = JSONObject.fromObject(dataInfo);
                    Collection dataList = parameterMap.values();
                    for (Object tmpVal : dataList) {
                        if (null != tmpVal) {
                            inj = cleanXSS(tmpVal.toString(), fcList);
                            if (!"".equals(inj)) {
                                return inj;
                            }
                        }
                    }
                }
            } catch (Exception jsonExp) {
                logger.warn(jsonExp.getMessage());
            }
            return inj;
        }).orElse("");
    }

    /**
     * 清除恶意的脚本
     *
     * @param value 值
     * @param list  验证规则
     * @return 验证结果
     */
    private static String cleanXSS(String value, List<String> list) {
        for (String key : list) {
            if (value.toLowerCase().contains(key)) {
                logger.error(ResponseConstant.ERROR_ILLEGAL_CODE + "-fc:【key】" + key + "【value】" + value);
                return value;
            }
        }
        return "";
    }

    /**
     * 遍历设备自定义字段用作缓存
     *
     * @param id         设备主键
     * @param properties 设备自定义字段
     * @param dataMap    缓存值存储对象
     * @param err1       错误信息1
     * @param err2       错误信息2
     * @param err3       错误信息3
     */
    public static void doCacheAssetFieldVal(String id, String properties, Map<String, Object> dataMap, String err1, String err2, String err3) {
        if (!"".equals(properties)) {
            try {
                JSONArray jsonArray = RegexUtil.stringToJaOrNew(properties);
                if (jsonArray.size() > 0) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String k = RegexUtil.optStrOrBlank(jsonObject.get("field_code"));
                            String v = RegexUtil.optStrOrBlank(jsonObject.get("field_value"));
                            if (!"".equals(k) && !"".equals(v)) {
                                dataMap.put(k, v);
                            }
                        } catch (Exception e) {
                            logger.error(err1, id + e);
                        }
                    }
                }
            } catch (Exception e) {
                try {
                    if (JSONObject.fromObject(properties).keySet().size() > -1) {
                        logger.error(err2, id + e);
                    }
                } catch (Exception e1) {
                    logger.error(err3, id + e);
                }
            }
        }
    }

    /**
     * 将英文括号替换成中文括号
     *
     * @param str 字符
     * @return 替换后的字符
     */
    public static String replaceBrackets(String str) {
        // 替换英文括号的时候因为正则表达式问题，英文前括号和后括号的前边都要加上\\
        return str.replaceAll("\\(", "（").replaceAll("\\)", "）");
    }

    /**
     * 判断两集合包含关系，输出日志
     *
     * @param left    左集合
     * @param right   右集合
     * @param logArr  日志集合
     * @param remark  日志备注
     * @param logKey  日志多语言key
     * @param logPrmA 日志参数
     * @param logPrmB 日志参数
     */
    public static void checkAbListForLog(List<String> left, List<String> right, JSONArray logArr, String remark, String logKey, String logPrmA, String logPrmB) {
        left.stream().filter(s -> !right.contains(s)).map(s -> "【" + s + "】").map(s -> LangUtil.doSetLogArray(remark, logKey, new String[]{logPrmA, logPrmB, s})).forEach(logArr::add);
    }

    /**
     * 根据参数列表拼接重组数据
     *
     * @param obj        参数值集合
     * @param fieldNames 参数数组
     * @return 参数值拼接集合
     */
    public static List<String> dateTypeChange(String obj, String[] fieldNames) {
        return RegexUtil.optNotNullList(DataChangeUtil.scdStringToList(obj)).map(l -> l.stream().map(e -> {
            StringBuilder strBuf = new StringBuilder();
            for (String fieldName : fieldNames) {
                strBuf.append(e.get(fieldName));
                strBuf.append('-');
            }
            strBuf.deleteCharAt(strBuf.length() - 1);
            return strBuf.toString();
        }).collect(Collectors.toList())).orElseGet(ArrayList::new);
    }

    /**
     * 通过Http请求public资源，并获取结果（POST）
     *
     * @param url        请求地址
     * @param jsonString 条件
     * @return 资源
     */
    public static String requestPublicJsonByPost(String url, String jsonString) {
        try {
            logger.info("jbp-in-public:" + url + " pm:" + jsonString);
            // 设置默认的连接超时时间 请求超时时间为30(s)
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(requestConfig);
            httpPost.addHeader("Content-type", "application/json; charset=utf-8");
            httpPost.setHeader("Accept", "application/json");
            httpPost.setEntity(new StringEntity(jsonString, Charset.forName("UTF-8")));
            try (CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
                 CloseableHttpResponse response = httpClient.execute(httpPost);
                 BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
                StringBuilder sb = new StringBuilder();
                String line;
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line).append(NL);
                }
                logger.info("jbp-out-public:" + url);
                if (sb.indexOf("\"exception\"") > 0) {
                    Map<String, Object> resultMap = DataChangeUtil.scdObjToMap(RegexUtil.optStrOrNull(sb.toString()));
                    RegexUtil.optNotNullMap(resultMap).ifPresent(r -> {
                        RegexUtil.optNotBlankStrOpt(r.get("error")).ifPresent(e -> {
                            throw new SenscloudException(RegexUtil.optStrOrBlank(r.get("message"))); // public返回异常信息
                        });
                    });
                }
                return sb.toString();
            } catch (SenscloudException se) {
                throw se;
            } catch (Exception e) {
                throw new SenscloudException(ErrorConstant.EC_HTTP_201, e.getMessage());
            }
        } catch (SenscloudException se) {
            throw se;
        } catch (Exception e) {
            throw new SenscloudException(ErrorConstant.EC_HTTP_202, e.getMessage());
        }
    }

    public static String askPublic(String path, Map<String, Object> param, String url) {
        JSONObject jsonObject = new JSONObject();
        Map<String, Object> map = RegexUtil.optMapOrNew(param);
        for (String key : map.keySet()) {
            jsonObject.put(key, map.get(key));
        }
        String jsonString = jsonObject.toString();
        String requestUrl = url.concat(path);
        return requestPublicJsonByPost(requestUrl, jsonString);
    }

    /**
     * 拼接2个字符串（A,B -> A-B）
     *
     * @param a 第一个字符串
     * @param b 第二个字符串
     * @return 拼接后字符串
     */
    public static String concatAbWithDefault(Object a, Object b) {
        String name = RegexUtil.optStrOrBlank(a);
        return RegexUtil.optNotBlankStrOpt(b).map(m -> "".equals(name) ? m : name.concat("-").concat(m)).orElse(name);
    }
}
