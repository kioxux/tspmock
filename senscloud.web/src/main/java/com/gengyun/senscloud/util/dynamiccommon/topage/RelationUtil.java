package com.gengyun.senscloud.util.dynamiccommon.topage;
//
//import com.fitit100.util.RegexUtil;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 字段关联属性解析方法（动态进页面前）
// * User: sps
// * Date: 2020/06/11
// * Time: 上午09:20
// */
public class RelationUtil {
//    /**
//     * 关联字段处理
//     *
//     * @param result
//     * @param data
//     * @param strInfo
//     * @param allMap
//     * @return
//     */
//    public static boolean doGetFieldViewRelationInfo(JSONObject result, JSONObject data, Map<String, String> strInfo, Map<String, Object> allMap) {
//        String fieldFormCode = strInfo.get("fieldFormCode");
//        Boolean feeRight = (Boolean) allMap.get("feeRight");
//        strInfo.put("fieldBaseViewType", null);
//        // 关联字段
//        if (data.containsKey("fieldViewRelation") && !"".equals(data.get("fieldViewRelation")) && !"0".equals(data.get("fieldViewRelation"))) {
//            String fvRelation = data.get("fieldViewRelation").toString(); // 页面特殊配置类型
//            String fvrDtl = data.get("fieldViewRelationDetail").toString(); // 页面特殊配置内容
//            if ("8".equals(fvRelation)) {
//                JSONObject jo = JSONObject.fromObject(fvrDtl);
//                Object pageViewType = jo.get("pageViewType"); // 平台特殊字段处理类型
//                if (null != pageViewType) {
//                    if ("PM".equals(pageViewType)) {
//                        return true;
//                    }
//                }
//                Map<String, Object> pageAllValue = (Map<String, Object>) allMap.get("pageAllValue");
//                // 手机端特殊显示方式
//                if (jo.containsKey("phoneShowType")) {
//                    strInfo.put("fieldBaseViewType", strInfo.get("fieldViewType"));
//                    strInfo.put("fieldViewType", (String) jo.get("phoneShowType")); // 平台特殊字段处理类型
//                }
//                if (jo.containsKey("dateInfo")) {
//                    JSONObject dateInfo = JSONObject.fromObject(jo.get("dateInfo"));
//                    String type = (String) dateInfo.get("type");
//                    if (RegexUtil.isNotNull(type) && "default".equals(type)) {
//                        allMap.put("value", strInfo.get("nowTimeStr"));
//                        data.put("fieldValue", strInfo.get("nowTimeStr"));
//                        Integer diff = (Integer) dateInfo.get("diff");
//                        if (RegexUtil.isNotNull(diff)) {
//                            long nowSysTime = (long) allMap.get("nowSysTime");
//                            String newTime = new SimpleDateFormat(Constants.DATE_FMT_SS);.format(new Timestamp(nowSysTime + diff));
//                            allMap.put("value", newTime);
//                            data.put("fieldValue", newTime);
//                        }
//                    }
//                }
//                // 设备列表过滤条件
//                if (jo.containsKey("assetSelectStatus")) {
//                    pageAllValue.put("assetSelectStatus", jo.get("assetSelectStatus"));
//                }
//                // 行编辑数据
//                if (jo.containsKey("rowDataInfo")) {
//                    JSONObject rowDataInfo = JSONObject.fromObject(jo.get("rowDataInfo"));
//                    // 自定义字段
//                    if ("0".equals(data.get("saveType"))) {
//                        rowDataInfo.put("jsonInfo", data);
//                    }
//                    Map<String, Object> pageRowDataInfo = (Map<String, Object>) allMap.get("pageRowDataInfo"); // 行编辑信息
//                    pageRowDataInfo.put(fieldFormCode, rowDataInfo);
//                }
//                // 图片
//                if (jo.containsKey("imgInfo")) {
//                    Map<String, Object> pageImgInfo = (Map<String, Object>) allMap.get("pageImgInfo");
//                    pageImgInfo.put(fieldFormCode, jo.get("imgInfo"));
//                }
//                // 设置检索条件【位置】取值字段
//                String modalFacilityCode = (String) jo.get("bomStockSearchValue");
//                if (RegexUtil.isNotNull(modalFacilityCode)) {
//                    data.put("modalFacilityCode", modalFacilityCode); // 页面弹出层检索条件控制
//                }
//                // 设置检索条件【位置】操作权限
//                String modalFacilityRight = (String) jo.get("bomStockSearchRight");
//                data.put("modalFacilityRight", true);
//                if (RegexUtil.isNotNull(modalFacilityRight) && "readonly".equals(modalFacilityRight)) {
//                    data.put("modalFacilityRight", false); // 页面弹出层检索条件操作权限控制
//                }
//                // 联动
//                if (jo.containsKey("linkage")) {
//                    Map<String, Object> linkAgeInfo = (Map<String, Object>) allMap.get("linkAgeInfo"); // 所有联动根字段
//                    Map<String, Object> linkAgeDtlList = (Map<String, Object>) allMap.get("linkAgeDtlList"); // 所有待联动字段信息
//                    getDynamicLinkageInfo(linkAgeInfo, linkAgeDtlList, jo.get("linkage").toString(), fieldFormCode, strInfo.get("fieldViewType"), (String) data.get("fieldDataBase")); // 联动
//                }
//                // 设置默认值
//                if (jo.containsKey("default")) {
//                    JSONObject defaultInfo = JSONObject.fromObject(jo.get("default"));
//                    if (defaultInfo.containsKey("phone")) {
//                        data.put("fieldValue", defaultInfo.get("phone"));
//                    }
//                }
//                // 移动端特殊选择图片方式，拍照或选择
//                if (jo.containsKey("imgType")) {
//                    data.put("imgType", jo.get("imgType"));
//                }
//                // 设置醒目
//                if (jo.containsKey("xingmu")) {
//                    data.put("xingmu", jo.get("xingmu"));
//                }
//                //下拉选默认选项
//                if (jo.containsKey("defaultOption")) {
//                    JSONObject defaultOption = JSONObject.fromObject(jo.get("defaultOption"));
//                    Map<String, Object> pageSelectDefaultOption = (Map<String, Object>) allMap.get("pageSelectDefaultOption");
//                    pageSelectDefaultOption.put(fieldFormCode, defaultOption);
//                }
//                // 首层属性
//                if (jo.containsKey("firstAttr")) {
//                    JSONArray firstAttrArray = JSONArray.fromObject(jo.get("firstAttr"));
//                    JSONObject tmpParam = null;
//                    String firstAttrType = null;
//                    List fieldAttrByLinkCodeList = (List) allMap.get("fieldAttrByLinkCodeList");
//                    for (Object fab : firstAttrArray) {
//                        tmpParam = JSONObject.fromObject(fab);
//                        firstAttrType = (String) tmpParam.get("type");
//                        String tmpKey = (String) tmpParam.get("key");
//                        // 字段属性
//                        if ("1".equals(firstAttrType) || "3".equals(firstAttrType)) {
//                            if (tmpParam.containsKey("info")) {
//                                data.put(tmpKey, tmpParam.get("info")); // 首层属性
//                            } else if (tmpParam.containsKey("value")) {
//                                data.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                            } else if (tmpParam.containsKey("linkInfo")) {
//                                fieldAttrByLinkCodeList.add(tmpParam.get("linkInfo"));
//                            } else if (tmpParam.containsKey("extInfo")) {
//                                data.put(tmpKey, allMap.get(tmpParam.get("extInfo")));
//                            } else if (tmpParam.containsKey("btnLink")) {
//                                JSONArray btnLinkArray = JSONArray.fromObject(tmpParam.get("btnLink"));
//                                for (Object btnLinkObj : btnLinkArray) {
//                                    fieldAttrByLinkCodeList.add(btnLinkObj);
//                                }
//                            } else if (tmpParam.containsKey("optInfo")) {
//                                Map<String, Object> assigneeInfo = (Map<String, Object>) allMap.get("assigneeInfo");
//                                data.put(tmpKey, assigneeInfo.get(tmpParam.get("optInfo")));
//                            }
//                            if (RegexUtil.isNotNull(tmpKey) && "fieldValue".equals(tmpKey)) {
//                                allMap.put("value", data.get("fieldValue"));
//                            }
//                            if (tmpParam.containsKey("rightType")) {
//                                String rightType = (String) tmpParam.get("rightType");
//                                if ("1".equals(rightType) && !feeRight) {
//                                    data.remove(tmpKey);
//                                }
//                            }
//                            // 节点属性
//                        } else if ("2".equals(firstAttrType)) {
//                            if (tmpParam.containsKey("info")) {
//                                pageAllValue.put(tmpKey, tmpParam.get("info")); // 首层属性
//                            } else if (tmpParam.containsKey("value")) {
//                                pageAllValue.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                            }
//                            if (tmpParam.containsKey("rightType")) {
//                                String rightType = (String) tmpParam.get("rightType");
//                                if ("1".equals(rightType) && !feeRight) {
//                                    pageAllValue.remove(tmpKey);
//                                }
//                            }
//                            // 归类属性
//                        } else if ("4".equals(firstAttrType)) {
//                            Map<String, Object> tmpInfo = (Map<String, Object>) pageAllValue.get(tmpKey);
//                            if (null == tmpInfo) {
//                                tmpInfo = new HashMap<String, Object>();
//                            }
//                            if (tmpParam.containsKey("info")) {
//                                tmpInfo.put(tmpKey, tmpParam.get("info")); // 首层属性
//                            } else if (tmpParam.containsKey("value")) {
//                                tmpInfo.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                            }
//                            if (tmpParam.containsKey("rightType")) {
//                                String rightType = (String) tmpParam.get("rightType");
//                                if ("1".equals(rightType) && !feeRight) {
//                                    tmpInfo.remove(tmpKey);
//                                }
//                            }
//                            pageAllValue.put(tmpKey, tmpInfo);
//                            // 页面信息
//                        } else if ("5".equals(firstAttrType)) {
//                            if (tmpParam.containsKey("info")) {
//                                result.put(tmpKey, tmpParam.get("info")); // 首层属性
//                            } else if (tmpParam.containsKey("value")) {
//                                result.put(tmpKey, tmpParam.get("value")); // 首层属性值
//                            } else if (tmpParam.containsKey("attr")) {
//                                result.put(tmpKey, data.get(tmpParam.get("attr"))); // 首层属性值
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        if (data.containsKey("rightType")) {
//            String rightType = (String) data.get("rightType");
//            if ("1".equals(rightType) && !feeRight) {
//                strInfo.put("fieldViewType", "10");
//            }
//        }
//        data.put("phoneShowType", strInfo.get("fieldViewType"));
//        data.put("phoneViewType", strInfo.get("fieldViewType"));
//        return false;
//    }
//
//    /**
//     * 关联字段属性设置
//     *
//     * @param allMap
//     */
//    public static void doSetFieldAttrByLinkCodeList(Map<String, Object> allMap) {
//        List fieldAttrByLinkCodeList = (List) allMap.get("fieldAttrByLinkCodeList");
//        if (null != fieldAttrByLinkCodeList && fieldAttrByLinkCodeList.size() > 0) {
//            String fieldFormCode = null;
//            String fromCode = null;
//            String fromAttr = null;
//            String attrName = null;
//            String type = null;
//            JSONObject jlcJo = null;
//            JSONObject data = null;
//            JSONObject fromJo = null;
//            Map<String, Object> pageAllValue = (Map<String, Object>) allMap.get("pageAllValue");
//            for (Object flcInfo : fieldAttrByLinkCodeList) {
//                jlcJo = JSONObject.fromObject(flcInfo);
//                fieldFormCode = jlcJo.getString("code");
//                attrName = (String) jlcJo.get("attr");
//                data = (JSONObject) pageAllValue.get(fieldFormCode);
//                if (null == data) {
//                    continue;
//                }
//                type = (String) jlcJo.get("type");
//                if (RegexUtil.isNotNull(type)) {
//                    if ("1".equals(type)) {
//                        fromCode = jlcJo.getString("fromCode");
//                        List<JSONObject> subList = new ArrayList<>();
//                        String[] fromCodes = fromCode.split(",");
//                        for (int i = 0; i < fromCodes.length; i++) {
//                            fromCode = fromCodes[i];
//                            fromJo = (JSONObject) pageAllValue.get(fromCode);
//                            subList.add(fromJo);
//                        }
//                        data.put(attrName, subList);
//                        continue;
//                    } else if ("2".equals(type)) {
//                        data.putAll(JSONObject.fromObject(jlcJo.get("fromInfo")));
//                        setDataUrl(data, data, allMap);
//                        continue;
//                    } else if ("3".equals(type)) {
//                        fromCode = jlcJo.getString("fromCode");
//                        fromJo = (JSONObject) pageAllValue.get(fromCode);
//                        String checkVal = jlcJo.getString("checkVal");
//                        String tmpVal = fromJo.getString(jlcJo.getString("fromAttr"));
//                        boolean checkSuccess = true;
//                        if ("noData".equals(checkVal)) {
//                            if (RegexUtil.isNotNull(tmpVal)) {
//                                List btnList = (List) allMap.get("btnList");
//                                btnList.remove(fieldFormCode);
//                                checkSuccess = false;
//                            }
//                        } else if ("hasData".equals(checkVal)) {
//                            if (RegexUtil.isNull(tmpVal)) {
//                                List btnList = (List) allMap.get("btnList");
//                                btnList.remove(fieldFormCode);
//                                checkSuccess = false;
//                            }
//                        } else if (!checkVal.equals(tmpVal)) {
//                            List btnList = (List) allMap.get("btnList");
//                            btnList.remove(fieldFormCode);
//                            checkSuccess = false;
//                        }
//                        if (!checkSuccess) {
//                            String closeLinkage = (String) jlcJo.get("closeLinkage");
//                            if (RegexUtil.isNotNull(closeLinkage)) {
//                                Map<String, Object> linkAgeInfo = (Map<String, Object>) allMap.get("linkAgeInfo");
//                                if (linkAgeInfo.containsKey(closeLinkage)) {
//                                    List<String> list = (List<String>) linkAgeInfo.get(closeLinkage);
//                                    list.remove(fieldFormCode);
//                                }
//                            }
//                        }
//                        continue;
//                    } else if ("4".equals(type)) {
//                        JSONObject linkParamInfo = JSONObject.fromObject(jlcJo.get("fromInfo"));
//                        setDataUrl(linkParamInfo, data, allMap);
//                        continue;
//                    }
//                }
//                fromCode = jlcJo.getString("fromCode");
//                if ("self".equals(fromCode)) {
//                    fromJo = data;
//                } else {
//                    fromJo = (JSONObject) pageAllValue.get(fromCode);
//                }
//                fromAttr = jlcJo.getString("fromAttr");
//                String fromSubAttr = (String) jlcJo.get("fromSubAttr");
//                if (RegexUtil.isNull(fromSubAttr)) {
//                    data.put(attrName, fromJo.get(fromAttr));
//                } else {
//                    JSONObject subFromJo = (JSONObject) fromJo.get(fromAttr);
//                    if (null != subFromJo) {
//                        data.put(attrName, subFromJo.get(fromSubAttr));
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 设置数据链接信息
//     *
//     * @param tmpMap
//     * @param dataMap
//     * @param allMap
//     */
//    public static void setDataUrl(Map<String, Object> tmpMap, Map<String, Object> dataMap, Map<String, Object> allMap) {
//        if (tmpMap.containsKey("linkInfoType")) {
//            String linkInfo = ""; // /pages/dypage/common?form_key=100000090&sub_work_code=WD20200603000006
//            String appLinkInfo = ""; // /pages/dypage/common?form_key=100000090&sub_work_code=WD20200603000006
//            if ("1".equals(tmpMap.get("linkInfoType"))) {
//                String workUrlInfo = "?task_id=" + allMap.get("task_id");
//                linkInfo = "common" + workUrlInfo;
//                appLinkInfo = "common" + workUrlInfo;
//            } else if ("2".equals(tmpMap.get("linkInfoType"))) {
//                // /pages/asset/detail?assetId=abc
//                linkInfo = "/pages/asset/detail";
//                appLinkInfo = "/pages/asset/detail";
//            }
//            if (tmpMap.containsKey("linkInfoParam")) {
//                JSONArray arrays = JSONArray.fromObject(tmpMap.get("linkInfoParam"));
//                if (null != arrays && arrays.size() > 0) {
//                    Map<String, Object> pageAllValue = null;
//                    if (tmpMap.containsKey("linkInfoParamType")) {
//                        if ("1".equals(tmpMap.get("linkInfoParamType"))) {
//                            pageAllValue = (Map<String, Object>) allMap.get("pageAllValue");
//                        }
//                    }
//                    StringBuffer params = new StringBuffer();
//                    for (Object object : arrays) {
//                        JSONObject data = JSONObject.fromObject(object);
//                        String type = data.getString("type");
//                        String attr = (String) data.get("attr");
//                        params.append("&");
//                        if ("1".equals(type)) {
//                            String fromCode = (String) data.get("fromCode");
//                            JSONObject fromJo = (JSONObject) pageAllValue.get(fromCode);
//                            if (RegexUtil.isNull(attr)) {
//                                params.append(fromJo.getString("fieldCode"));
//                            } else {
//                                params.append(attr);
//                            }
//                            params.append("=");
//                            params.append(fromJo.get(data.getString("fromAttr")));
//                        } else if ("2".equals(type)) {
//                            params.append(attr);
//                            params.append("=");
//                            params.append(data.getString("value"));
//                        } else if ("3".equals(type)) {
//                            if (RegexUtil.isNull(attr)) {
//                                params.append(tmpMap.get("fieldCode"));
//                            } else {
//                                params.append(attr);
//                            }
//                            params.append("=");
//                            params.append(tmpMap.get(data.getString("fromAttr")));
//                        }
//                    }
//                    String strPms = params.toString();
//                    if (!"1".equals(tmpMap.get("linkInfoType"))) {
//                        strPms = "?" + strPms.substring(1);
//                    }
//                    linkInfo += strPms;
//                    appLinkInfo += strPms;
//                }
//            }
//            String linkName = RegexUtil.optStrOrVal(tmpMap.get("linkName"), "linkInfo");
//            dataMap.put(linkName, linkInfo);
//            dataMap.put("app" + linkName, appLinkInfo);
//        }
//    }
//
//    /**
//     * 获取动态联动信息（手机端）
//     *
//     * @param linkAgeInfo
//     * @param linkAgeDtlList
//     * @param info
//     * @param id
//     * @param type
//     * @param selectDb
//     */
//    public static void getDynamicLinkageInfo(Map<String, Object> linkAgeInfo, Map<String, Object> linkAgeDtlList, String info, String id, String type, String selectDb) {
//        JSONObject jo = JSONObject.fromObject(info);
//        jo.put("type", type);
//        jo.put("selectDb", selectDb);
//        linkAgeDtlList.put(id, jo);
//        Object pageParam = jo.get("param");
//        if (null != pageParam) {
//            JSONArray paramList = JSONArray.fromObject(jo.get("param"));
//            JSONObject tmpParam = null;
//            String tmpId = "";
//            String linkAgeType = "";
//            for (Object object : paramList) {
//                tmpParam = JSONObject.fromObject(object);
//                tmpId = (String) tmpParam.get("fieldFormCode");
//                linkAgeType = (String) tmpParam.get("type");
//                if (RegexUtil.isNotNull(tmpId)) {
//                    List<String> list = null;
//                    // 记录联动信息
//                    if (linkAgeInfo.containsKey(tmpId)) {
//                        list = (List<String>) linkAgeInfo.get(tmpId);
//                        if (!list.contains(id)) {
//                            list.add(id);
//                        }
//                    } else {
//                        list = new ArrayList<String>();
//                        list.add(id);
//                    }
//                    linkAgeInfo.put(tmpId, list);
//                }
//
//                // 直接使用关联值
//                if (RegexUtil.isNotNull(linkAgeType) && "0".equals(linkAgeType)) {
//                    break;
//                }
//            }
//        }
//    }

}
