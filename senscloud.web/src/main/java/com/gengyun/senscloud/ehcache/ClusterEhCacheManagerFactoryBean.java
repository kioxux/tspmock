package com.gengyun.senscloud.ehcache;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.CacheConfiguration.CacheEventListenerFactoryConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import net.sf.ehcache.config.FactoryConfiguration;
import net.sf.ehcache.distribution.RMICacheManagerPeerListenerFactory;
import net.sf.ehcache.distribution.RMICacheManagerPeerProviderFactory;
import net.sf.ehcache.distribution.RMICacheReplicatorFactory;
import net.sf.ehcache.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.ehcache.EhCacheManagerUtils;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * 扩展了EhCacheManagerFactoryBean 增加通过配置文件rmi 集群配置
 * 
 * @author luanhy
 *
 */
@Component
public class ClusterEhCacheManagerFactoryBean implements FactoryBean<CacheManager>, InitializingBean, DisposableBean {
 
	private static final Logger LOG = LoggerFactory.getLogger(ClusterEhCacheManagerFactoryBean.class);
 
	private Resource configLocation;
 
	private String cacheManagerName;
 
	private boolean acceptExisting = false;
 
	private boolean shared = false;
 
	private CacheManager cacheManager;
 
	private boolean locallyManaged = true;
	
	@Autowired
	private ClusterEhCacheConfig ehClusterConfig;
 
	private String defaultRMICacheReplicatorFactoryPropertiesName = "replicatePuts=true,replicateUpdates=true,replicateRemovals=true,replicateAsynchronously=true,replicatePutsViaCopy=true,replicateUpdatesViaCopy=true,asynchronousReplicationIntervalMillis=100";
 
 
	/**
	 * Set the location of the EhCache config file. A typical value is
	 * "/WEB-INF/ehcache.xml".
	 * <p>
	 * Default is "ehcache.xml" in the root of the class path, or if not found,
	 * "ehcache-failsafe.xml" in the EhCache jar (default EhCache
	 * initialization).
	 * 
	 * @see net.sf.ehcache.CacheManager#create(java.io.InputStream)
	 * @see net.sf.ehcache.CacheManager#CacheManager(java.io.InputStream)
	 */
	public void setConfigLocation(Resource configLocation) {
		this.configLocation = configLocation;
	}

	/**
	 * Set the name of the EhCache CacheManager (if a specific name is desired).
	 *
	 * @see net.sf.ehcache.CacheManager#setName(String)
	 */
	public void setCacheManagerName(String cacheManagerName) {
		this.cacheManagerName = cacheManagerName;
	}

	/**
	 * Set whether an existing EhCache CacheManager of the same name will be
	 * accepted for this EhCacheManagerFactoryBean setup. Default is "false".
	 * <p>
	 * Typically used in combination with {@link #setCacheManagerName
	 * "cacheManagerName"} but will simply work with the default CacheManager
	 * name if none specified. All references to the same CacheManager name (or
	 * the same default) in the same ClassLoader space will share the specified
	 * CacheManager then.
	 *
	 * @see #setCacheManagerName #see #setShared
	 * @see net.sf.ehcache.CacheManager#getCacheManager(String)
	 * @see net.sf.ehcache.CacheManager#CacheManager()
	 */
	public void setAcceptExisting(boolean acceptExisting) {
		this.acceptExisting = acceptExisting;
	}

	/**
	 * Set whether the EhCache CacheManager should be shared (as a singleton at
	 * the ClassLoader level) or independent (typically local within the
	 * application). Default is "false", creating an independent local instance.
	 * <p>
	 * <b>NOTE:</b> This feature allows for sharing this
	 * EhCacheManagerFactoryBean's CacheManager with any code calling
	 * <code>CacheManager.create()</code> in the same ClassLoader space, with no
	 * need to agree on a specific CacheManager name. However, it only supports
	 * a single EhCacheManagerFactoryBean involved which will control the
	 * lifecycle of the underlying CacheManager (in particular, its shutdown).
	 * <p>
	 * This flag overrides {@link #setAcceptExisting "acceptExisting"} if both
	 * are set, since it indicates the 'stronger' mode of sharing.
	 *
	 * @see #setCacheManagerName
	 * @see #setAcceptExisting
	 * @see net.sf.ehcache.CacheManager#create()
	 * @see net.sf.ehcache.CacheManager#CacheManager()
	 */
	public void setShared(boolean shared) {
		this.shared = shared;
	}
 
	@Override
	public void afterPropertiesSet() throws CacheException {
		LOG.info("Initializing EhCache CacheManager");
		Configuration configuration = this.configLocation != null
				? EhCacheManagerUtils.parseConfiguration(this.configLocation)
				: ConfigurationFactory.parseConfiguration();
		if (this.cacheManagerName != null) {
			configuration.setName(this.cacheManagerName);
		}
		
		if(this.ehClusterConfig.getEnableCluster()){
			configCacheManagerPeerProviderFactory(configuration);
			configCacheManagerPeerListenerFactory(configuration);
			configCacheEventListenerFactory(configuration);
		}
	
		if (this.shared) {
			// Old-school EhCache singleton sharing...
			// No way to find out whether we actually created a new CacheManager
			// or just received an existing singleton reference.
			this.cacheManager = CacheManager.create(configuration);
		} else if (this.acceptExisting) {
			// EhCache 2.5+: Reusing an existing CacheManager of the same name.
			// Basically the same code as in CacheManager.getInstance(String),
			// just storing whether we're dealing with an existing instance.
			synchronized (CacheManager.class) {
				this.cacheManager = CacheManager.getCacheManager(this.cacheManagerName);
				if (this.cacheManager == null) {
					this.cacheManager = new CacheManager(configuration);
				} else {
					this.locallyManaged = false;
				}
			}
		} else {
			// Throwing an exception if a CacheManager of the same name exists
			// already...
			this.cacheManager = new CacheManager(configuration);
		}
 
	}
	
	/**
	 * 
	* @Title: configCacheManagerPeerProviderFactory
	* @Description:  配置缓存管理注册提供者工厂
	* @return void    返回类型
	* @param configuration
	 */
	public void configCacheManagerPeerProviderFactory(Configuration configuration) {
		LOG.debug("ehClusterConfig:" + ehClusterConfig.toString());
		String peerDiscovery = ehClusterConfig.getPeerDiscovery();
		LOG.debug("rmi方式：" + peerDiscovery);
		if (StringUtils.isEmpty(peerDiscovery)) {
			return;
		}
		if ("automatic".equals(peerDiscovery)) {
			configAutomaticCacheManagerPeerProviderFactory(configuration);
		} else if ("manual".equals(peerDiscovery)) {
			configManualCacheManagerPeerProviderFactory(configuration);
		} else {
			LOG.error("invalid peerDiscovery：" + peerDiscovery);
			return;
		}
	}
 
	/**
	 * 
	* @Title: configManualCacheManagerPeerProviderFactory
	* @Description: 配置缓存管理手工注册提供者工厂
	* @return void    返回类型
	* @param configuration
	 */
	private void configManualCacheManagerPeerProviderFactory(Configuration configuration) {
		String rmiUrls = ehClusterConfig.getRmiUrls().trim();
		StringTokenizer stringTokenizer = new StringTokenizer(rmiUrls, "|");
		Set<String> cacheConfigurationsKeySet = configuration.getCacheConfigurationsKeySet();
		StringBuilder rmiUrlsStr = new StringBuilder();
		while (stringTokenizer.hasMoreTokens()) {
			String rmiUrl = stringTokenizer.nextToken();
			rmiUrl = rmiUrl.trim();
			for (String key : cacheConfigurationsKeySet) {
				rmiUrlsStr.append("//").append(rmiUrl).append("/").append(key).append("|");
			}
		}
		rmiUrlsStr = rmiUrlsStr.deleteCharAt(rmiUrlsStr.length() - 1);
		LOG.debug("last rmiUrls：" + rmiUrlsStr.toString());
		configuration.cacheManagerPeerProviderFactory(new FactoryConfiguration<FactoryConfiguration<?>>()
				.className(RMICacheManagerPeerProviderFactory.class.getName())
				.properties("peerDiscovery=manual,rmiUrls=" + rmiUrlsStr));
	}
 
	/**
	 * 
	* @Title: configAutomaticCacheManagerPeerProviderFactory
	* @Description: 配置缓存管理自动注册提供者工厂
	* @return void    返回类型
	* @param configuration
	 */
	private void configAutomaticCacheManagerPeerProviderFactory(Configuration configuration) {
		configuration.cacheManagerPeerProviderFactory(new FactoryConfiguration<FactoryConfiguration<?>>()
				.className(RMICacheManagerPeerProviderFactory.class.getName())
				.properties("peerDiscovery=automatic,multicastGroupAddress="
						+ ehClusterConfig.getMulticastGroupAddress().trim() + ",multicastGroupPort="
						+ ehClusterConfig.getMulticastGroupPort() + ",timeToLive=32"));
	}
	
	/**
	 * 
	* @Title: configCacheManagerPeerListenerFactory
	* @Description:配置缓存同步管理监听工厂
	* @return void    返回类型
	* @param configuration
	 */
	private void configCacheManagerPeerListenerFactory(Configuration configuration) {
		configuration.cacheManagerPeerListenerFactory(new FactoryConfiguration<FactoryConfiguration<?>>()
				.className(RMICacheManagerPeerListenerFactory.class.getName())
				.properties("hostName=" + ehClusterConfig.getHostName().trim() + ",port="
						+ ehClusterConfig.getPort() + ",socketTimeoutMillis=2000"));
	}
	
	/**
	 * 
	* @Title: configCacheEventListenerFactory
	* @Description: 配置缓存监听工厂
	* @return void    返回类型
	* @param configuration
	 */
	private void configCacheEventListenerFactory(Configuration configuration) {
		Properties defaultRMICacheReplicatorFactoryProperties = PropertyUtil
				.parseProperties(defaultRMICacheReplicatorFactoryPropertiesName, ",");
		Map<String, CacheConfiguration> cacheConfigurations = configuration.getCacheConfigurations();
		
		Set<String> filterNoSyncCache = filterNoSyncCache(configuration.getCacheConfigurationsKeySet());
		
		for (String key : filterNoSyncCache) {
			
			CacheConfiguration cacheConfiguration = cacheConfigurations.get(key);
 
			boolean hasRMICacheReplicatorFactory = false;
			@SuppressWarnings("unchecked")
			List<CacheEventListenerFactoryConfiguration> cacheEventListenerConfigurations = cacheConfiguration
					.getCacheEventListenerConfigurations();
			//已经配置缓存监听情况
			if (cacheEventListenerConfigurations != null && !cacheEventListenerConfigurations.isEmpty()) {
				for (CacheEventListenerFactoryConfiguration c1 : cacheEventListenerConfigurations) {
					String className = c1.getFullyQualifiedClassPath();
					if (className.equals(RMICacheReplicatorFactory.class.getName())) {
						hasRMICacheReplicatorFactory = true;
						Properties parseProperties = PropertyUtil.parseProperties(c1.getProperties(),
								c1.getPropertySeparator());
						//属性为空 设置默认
						if (parseProperties == null) {
							c1.properties(defaultRMICacheReplicatorFactoryPropertiesName);
							continue;
						}
						//属性不为空 ，和默认属性合并
						Enumeration<?> propertyNames = parseProperties.propertyNames();
						while (propertyNames.hasMoreElements()) {
							String key1 = (String) propertyNames.nextElement();
							String property = parseProperties.getProperty(key1);
							if (StringUtils.hasText(property)) {
								defaultRMICacheReplicatorFactoryProperties.put(key1, property);
							}
						}
						//重新设置合并后的属性
						Enumeration<?> propertyNames1 = defaultRMICacheReplicatorFactoryProperties.propertyNames();
						StringBuilder sb = new StringBuilder();
						while (propertyNames1.hasMoreElements()) {
							String key1 = (String) propertyNames1.nextElement();
							String property = defaultRMICacheReplicatorFactoryProperties.getProperty(key1);
							sb.append(key1).append("=").append(property).append(",");
						}
						c1.setProperties(sb.substring(0, sb.length() - 1).toString());
					}
				}
			}
			//未配置缓存监听情况 设置默认RMICacheReplicatorFactory即属性
			if (!hasRMICacheReplicatorFactory) {
				CacheEventListenerFactoryConfiguration cacheEventListenerFactoryConfiguration = new CacheEventListenerFactoryConfiguration();
				cacheEventListenerFactoryConfiguration.className(RMICacheReplicatorFactory.class.getName());
				cacheEventListenerFactoryConfiguration.properties(defaultRMICacheReplicatorFactoryPropertiesName);
				cacheConfiguration.addCacheEventListenerFactory(cacheEventListenerFactoryConfiguration);
			}
 
		}
	}
	
	/**
	 * 
	* @Title: filterNoSyncCache
	* @Description: 过滤掉不同步的缓存key
	* @returnSet<String>    返回类型 过滤后的缓存配置key
	* @param cacheConfigurationsKeySet
	* @return
	 */
	private Set<String> filterNoSyncCache(Set<String> cacheConfigurationsKeySet){
		Set<String> filteredCacheConfigurationKeySet = new HashSet<String>();
		filteredCacheConfigurationKeySet.addAll(cacheConfigurationsKeySet);
		String unSyncCacheName = ehClusterConfig.getUnSyncCacheName();
		StringTokenizer unSyncCacheNameTokenizer = new StringTokenizer(unSyncCacheName, "|");
		while(unSyncCacheNameTokenizer.hasMoreTokens()){
			String nextToken = unSyncCacheNameTokenizer.nextToken();
			for (String key : cacheConfigurationsKeySet) {
				if(key.equals(nextToken)){
					filteredCacheConfigurationKeySet.remove(nextToken);
					continue;
				}
			}
		}
		return filteredCacheConfigurationKeySet;
	}
 
	@Override
	public CacheManager getObject() {
		return this.cacheManager;
	}
 
	@Override
	public Class<? extends CacheManager> getObjectType() {
		return this.cacheManager != null ? this.cacheManager.getClass() : CacheManager.class;
	}
 
	@Override
	public boolean isSingleton() {
		return true;
	}
 
	@Override
	public void destroy() {
		if (this.locallyManaged) {
			LOG.info("Shutting down EhCache CacheManager");
			this.cacheManager.shutdown();
		}
	}
	
	public ClusterEhCacheConfig getEhClusterConfig() {
		return ehClusterConfig;
	}
 
	public void setEhClusterConfig(ClusterEhCacheConfig ehClusterConfig) {
		this.ehClusterConfig = ehClusterConfig;
	}
}

