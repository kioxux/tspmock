package com.gengyun.senscloud.ehcache;
 
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ClusterEhCacheConfig {
	
	/**
	 * 是否启用集群
	 */
	@Value("${ehcache.enableCluster:true}")
	private boolean enableCluster;
	
	/**
	 * 无需同步的缓存名称 |分隔
	 */
	@Value("${ehcache.unSyncCacheName:noCache|printOrderCache}")
	private String unSyncCacheName;
	
	/**
	 * 缓存提供者rmi通信方式 自动和手动
	 */
	@Value("${ehcache.provider.peerDiscovery:automatic}")
	private String peerDiscovery;
	
	/**
	 * 缓存提供者广播组地址
	 */
	@Value("${ehcache.provider.automatic.multicastGroupAddress:224.1.1.1}")
	private String multicastGroupAddress;
	
	/**
	 * 缓存提供者广播组端口
	 */
	@Value("${ehcache.provider.automatic.multicastGroupPort:41000}")
	private int multicastGroupPort;
 
	/**
	 * 缓存提供者需要通知的rmi地址
	 */
	@Value("${ehcache.provider.manual.rmiUrls:127.0.0.1:40001}")
	private String rmiUrls;
	
	/**
	 * 缓存监听者端口是否随机 （自动配置时生效）
	 */
	@Value("${ehcache.listener.randomPort:true}")
	private boolean randomPort;
	
	/**
	 * 缓存监听者端口地址
	 */
	@Value("${ehcache.listener.hostName:127.0.0.1}")
	private String hostName;
	
	/**
	 * 缓存监听者默认端口
	 */
	@Value("${ehcache.listener.port:40001}")
	private int port;

	public String getPeerDiscovery() {
		return peerDiscovery;
	}
 
	public void setPeerDiscovery(String peerDiscovery) {
		this.peerDiscovery = peerDiscovery;
	}
 
	public String getMulticastGroupAddress() {
		return multicastGroupAddress;
	}
 
	public void setMulticastGroupAddress(String multicastGroupAddress) {
		this.multicastGroupAddress = multicastGroupAddress;
	}
 
	public int getMulticastGroupPort() {
		return multicastGroupPort;
	}
 
	public void setMulticastGroupPort(int multicastGroupPort) {
		this.multicastGroupPort = multicastGroupPort;
	}
 
	public String getRmiUrls() {
		return rmiUrls;
	}
 
	public void setRmiUrls(String rmiUrls) {
		this.rmiUrls = rmiUrls;
	}
 
	public String getHostName() {
		return hostName;
	}
 
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
 
	public int getPort() {
		return port;
	}
 
	public void setPort(int port) {
		this.port = port;
	}
	
	public boolean getRandomPort() {
		return randomPort;
	}
 
	public void setRandomPort(boolean randomPort) {
		this.randomPort = randomPort;
	}
	
	public boolean getEnableCluster() {
		return enableCluster;
	}
 
	public void setEnableCluster(boolean enableCluster) {
		this.enableCluster = enableCluster;
	}
	
	public String getUnSyncCacheName() {
		return unSyncCacheName;
	}
 
	public void setUnSyncCacheName(String unSyncCacheName) {
		this.unSyncCacheName = unSyncCacheName;
	}
 
	@Override
	public String toString() {
		return "EhClusterConfig [enableCluster=" + enableCluster + ", unSyncCacheName=" + unSyncCacheName + ",randomPort=" + randomPort + ", peerDiscovery="
				+ peerDiscovery + ", multicastGroupAddress=" + multicastGroupAddress + ", multicastGroupPort="
				+ multicastGroupPort + ", rmiUrls=" + rmiUrls + ", hostName=" + hostName + ", port=" + port + "]";
	}
 
	
}

