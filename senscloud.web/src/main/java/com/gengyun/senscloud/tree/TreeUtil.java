package com.gengyun.senscloud.tree;

import java.util.List;

public class TreeUtil {
    public final static String getOptionListForSelect(List<Node> roots) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<option value=\"0\"><t:resource key=\"msg_frist_parent_enum\"></t:resource></option>");
        try {
            for (int i = 0; i < roots.size(); i++) {
                Node node = roots.get(i);
                stringBuffer.append("<option value=\"" + node.getId() + "\">" + node.getName() + "</option>");
                if (node.getItems() != null && node.getItems().size() > 0) {
                    getOption(node, stringBuffer);
                }
            }
        } catch (Exception ex) {

        }
        return stringBuffer.toString();
    }

    private static void getOption(final Node node, final StringBuffer result) {
        List<Node> list = node.getItems();
        for (int i = 0; i < list.size(); i++) {
            Node tempNode = list.get(i);
            String fullName = "";
            int level = tempNode.getLevel();
            for (int j = 0; j < level; j++) {
                fullName = ("&nbsp;&nbsp;&nbsp;" + fullName);
            }
            fullName = fullName + "&nbsp;&nbsp;&nbsp;L" + tempNode.getName();
            result.append("<option value=\"" + tempNode.getId() + "\">" + fullName + "</option>");
            if (tempNode.getItems() != null && tempNode.getItems().size() > 0) {
                getOption(tempNode, result);
            }
        }
    }
}
