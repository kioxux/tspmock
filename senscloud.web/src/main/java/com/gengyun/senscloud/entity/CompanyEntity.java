package com.gengyun.senscloud.entity;

import java.io.Serializable;

/**
 * 企业实体类
 * User: sps
 * Date: 2020/12/08
 * Time: 上午11:42
 */
public class CompanyEntity implements Serializable {
    private static final long serialVersionUID = -1L;
    /**
     * 企业编码
     */
    private long _id;
    /**
     * 公司名字
     */
    private String company_name;

    /**
     * 电子邮件
     */
    private String email;

    /**
     * 管理员手机号
     */
    private String mobile;

    /**
     * 状态(1:启用 2:锁定 3:删除)
     */
    private int status;

    /**
     * 企业地址
     */
    private String address;

    /**
     * 公司系统名称
     */
    private String system_name;

    /**
     * 公司Logo地址
     */
    private String company_logo_id;

    /**
     * 是否允许公司LOGO
     */
    private boolean is_company_logo;

    /**
     * 客户公司的编码，用户备件对接时，判断传回来的备件属于哪一个schema
     */
    private String client_customer_code;

    /**
     * 是否开启短信
     */
    private Boolean is_open_sms;

    /**
     * 是否开启邮件
     */
    private Boolean is_open_email;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSystem_name() {
        return system_name;
    }

    public void setSystem_name(String system_name) {
        this.system_name = system_name;
    }

    public String getCompany_logo_id() {
        return company_logo_id;
    }

    public void setCompany_logo_id(String company_logo_id) {
        this.company_logo_id = company_logo_id;
    }

    public boolean isIs_company_logo() {
        return is_company_logo;
    }

    public void setIs_company_logo(boolean is_company_logo) {
        this.is_company_logo = is_company_logo;
    }

    public String getClient_customer_code() {
        return client_customer_code;
    }

    public void setClient_customer_code(String client_customer_code) {
        this.client_customer_code = client_customer_code;
    }

    public Boolean getIs_open_sms() {
        return is_open_sms;
    }

    public void setIs_open_sms(Boolean is_open_sms) {
        this.is_open_sms = is_open_sms;
    }

    public Boolean getIs_open_email() {
        return is_open_email;
    }

    public void setIs_open_email(Boolean is_open_email) {
        this.is_open_email = is_open_email;
    }
}
