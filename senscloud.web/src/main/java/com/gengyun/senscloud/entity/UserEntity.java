package com.gengyun.senscloud.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 用户实体类
 * User: sps
 * Date: 2020/12/08
 * Time: 上午11:42
 */
public class UserEntity implements Serializable {
    private static final long serialVersionUID = -1L;
    /**
     * 用户编号
     */
    private String id;
    /**
     * 账号
     */
    private String account;
    /**
     * 用户名
     */
    private String user_name;
    /**
     * 用户编码
     */
    private String user_code;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 邮件
     */
    private String email;
    /**
     * 状态
     */
    private int status;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 是否委外
     */
    private boolean is_out;
    /**
     * 委外服务商
     */
    private Integer customer_id;
    /**
     * 是否付费用户
     */
    private boolean is_charge;
    /**
     * 默认语言
     */
    private String language_tag;
    /**
     * 小程序公众号open_id
     */
    private String mini_program_open_id;
    /**
     * 微信公众号open_id
     */
    private String official_open_id;
    /**
     * 微信union_id
     */
    private String union_id;
    /**
     * 微信号
     */
    private String wei_xin_code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isIs_out() {
        return is_out;
    }

    public void setIs_out(boolean is_out) {
        this.is_out = is_out;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public boolean isIs_charge() {
        return is_charge;
    }

    public void setIs_charge(boolean is_charge) {
        this.is_charge = is_charge;
    }

    public String getLanguage_tag() {
        return language_tag;
    }

    public void setLanguage_tag(String language_tag) {
        this.language_tag = language_tag;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getMini_program_open_id() {
        return mini_program_open_id;
    }

    public void setMini_program_open_id(String mini_program_open_id) {
        this.mini_program_open_id = mini_program_open_id;
    }

    public String getOfficial_open_id() {
        return official_open_id;
    }

    public void setOfficial_open_id(String official_open_id) {
        this.official_open_id = official_open_id;
    }

    public String getUnion_id() {
        return union_id;
    }

    public void setUnion_id(String union_id) {
        this.union_id = union_id;
    }

    public String getWei_xin_code() {
        return wei_xin_code;
    }

    public void setWei_xin_code(String wei_xin_code) {
        this.wei_xin_code = wei_xin_code;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }
}
