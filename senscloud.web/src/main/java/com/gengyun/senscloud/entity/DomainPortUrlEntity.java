package com.gengyun.senscloud.entity;

import java.io.Serializable;

public class DomainPortUrlEntity implements Serializable {
    private static final long serialVersionUID = -6870895462329907540L;

    /**
     * 域名
     */
    private String domain_name;
    /**
     * 单点登录地址
     */
    private String port_url;
    /**
     * 单点登录异常地址
     */
    private String bak_url1;
    /**
     * 单点登录注销跳转地址
     */
    private String bak_url2;
    /**
     * 备用，是否需要NFC登录，1：需求；其他：不需要；
     */
    private String bak_url3;
    /**
     * 备用
     */
    private String bak_url4;
    /**
     * 是否需要单点登录
     */
    private Boolean is_need_passport;
    /**
     * 是否需要技术支持框
     */
    private Boolean is_need_tech_support;
    /**
     * 登录页文字
     */
    private String login_title;
    /**
     * 登录页logo
     */
    private String login_logo;
    /**
     * 移动端单点登录验证
     */
    private String bak_url5;
    /**
     * 备用
     */
    private String bak_url6;
    /**
     * 系统的背景图地址
     */
    private String bak_url7;
    /**
     * pc登录logo地址，如果没有，用login_logo（小程序的登录logo）的值
     */
    private String bak_url8;

    public String getDomain_name() {
        return domain_name;
    }

    public void setDomain_name(String domain_name) {
        this.domain_name = domain_name;
    }

    public String getPort_url() {
        return port_url;
    }

    public void setPort_url(String port_url) {
        this.port_url = port_url;
    }

    public String getBak_url1() {
        return bak_url1;
    }

    public void setBak_url1(String bak_url1) {
        this.bak_url1 = bak_url1;
    }

    public String getBak_url2() {
        return bak_url2;
    }

    public void setBak_url2(String bak_url2) {
        this.bak_url2 = bak_url2;
    }

    public String getBak_url3() {
        return bak_url3;
    }

    public void setBak_url3(String bak_url3) {
        this.bak_url3 = bak_url3;
    }

    public String getBak_url4() {
        return bak_url4;
    }

    public void setBak_url4(String bak_url4) {
        this.bak_url4 = bak_url4;
    }

    public Boolean getIs_need_passport() {
        return is_need_passport;
    }

    public void setIs_need_passport(Boolean is_need_passport) {
        this.is_need_passport = is_need_passport;
    }

    public Boolean getIs_need_tech_support() {
        return is_need_tech_support;
    }

    public void setIs_need_tech_support(Boolean is_need_tech_support) {
        this.is_need_tech_support = is_need_tech_support;
    }

    public String getLogin_title() {
        return login_title;
    }

    public void setLogin_title(String login_title) {
        this.login_title = login_title;
    }

    public String getLogin_logo() {
        return login_logo;
    }

    public void setLogin_logo(String login_logo) {
        this.login_logo = login_logo;
    }

    public String getBak_url5() {
        return bak_url5;
    }

    public void setBak_url5(String bak_url5) {
        this.bak_url5 = bak_url5;
    }

    public String getBak_url6() {
        return bak_url6;
    }

    public void setBak_url6(String bak_url6) {
        this.bak_url6 = bak_url6;
    }

    public String getBak_url7() {
        return bak_url7;
    }

    public void setBak_url7(String bak_url7) {
        this.bak_url7 = bak_url7;
    }

    public String getBak_url8() {
        return bak_url8;
    }

    public void setBak_url8(String bak_url8) {
        this.bak_url8 = bak_url8;
    }
}
