package com.gengyun.senscloud.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 登录记录实体类
 * User: sps
 * Date: 2020/12/08
 * Time: 上午11:42
 */
public class LoginLogEntity implements Serializable {
    private static final long serialVersionUID = -1L;
    /**
     * 登录账号
     */
    private String account;
    /**
     * 客户端名称
     */
    private String client_name;
    /**
     * 登录时间
     */
    private Timestamp login_time;
    /**
     * 客户端IP
     */
    private String client_ip;
    /**
     * 客户端操作系统
     */
    private String client_os;
    /**
     * 客户端浏览器
     */
    private String client_browser;
    /**
     * 登录token
     */
    private String token;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public Timestamp getLogin_time() {
        return login_time;
    }

    public void setLogin_time(Timestamp login_time) {
        this.login_time = login_time;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public String getClient_os() {
        return client_os;
    }

    public void setClient_os(String client_os) {
        this.client_os = client_os;
    }

    public String getClient_browser() {
        return client_browser;
    }

    public void setClient_browser(String client_browser) {
        this.client_browser = client_browser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
