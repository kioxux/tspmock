package com.gengyun.senscloud.exception;

public class DataTypeFormatException extends Exception {
    public DataTypeFormatException(String message) {
        super(message);
    }
}
