package com.gengyun.senscloud.response;

//位置供应商维修保养统计
public class AnalysFacilityRepairMaintainResult {
//
//    private String analysName;
//
//    public String getAnalysName() {
//        return analysName;
//    }
//
//    public void setAnalysName(String analysName) {
//        this.analysName = analysName;
//    }
//
//    private String analysCode;
//
//    public String getAnalysCode() {
//        return analysCode;
//    }
//
//    public void setAnalysCode(String analysCode) {
//        this.analysCode = analysCode;
//    }
//
//
//    private String asset_alias;
//
//    public String getAsset_alias() {
//        return asset_alias;
//    }
//
//    public void setAsset_alias(String asset_alias) {
//        this.asset_alias = asset_alias;
//    }
//
//    private String strname;
//
//    private int asset_count;
//
//    private int finishedMaintainTimes;
//
//    private double totalMaintainMinute;
//
//    private double maintainMinutePreTime;
//
//    private int noFinishedMaintainTimes;
//
//    private int planMaintainTimes;
//
//    private double maintainFinishedRate;
//
//    private int runTimeRepairTimes;
//
//    private double runTimeTotalRepairMinute;
//
//
//    private double runTimeRepairMinutePreTime;
//
//    private int repairTimes;
//
//    private double totalRepairMinute;
//
//    private double totalRepairMinutePreTime;
//
//    private double totalMaintainOfRepairRate;
//
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public int getAsset_count() {
//        return asset_count;
//    }
//
//    public void setAsset_count(int asset_count) {
//        this.asset_count = asset_count;
//    }
//
//    public int getFinishedMaintainTimes() {
//        return finishedMaintainTimes;
//    }
//
//    public void setFinishedMaintainTimes(int finishedMaintainTimes) {
//        this.finishedMaintainTimes = finishedMaintainTimes;
//    }
//
//    public double getTotalMaintainMinute() {
//        return totalMaintainMinute;
//    }
//
//    public void setTotalMaintainMinute(double totalMaintainMinute) {
//        this.totalMaintainMinute = totalMaintainMinute;
//    }
//
//    public int getNoFinishedMaintainTimes() {
//        return noFinishedMaintainTimes;
//    }
//
//    public void setNoFinishedMaintainTimes(int noFinishedMaintainTimes) {
//        this.noFinishedMaintainTimes = noFinishedMaintainTimes;
//    }
//
//    public int getRunTimeRepairTimes() {
//        return runTimeRepairTimes;
//    }
//
//    public void setRunTimeRepairTimes(int runTimeRepairTimes) {
//        this.runTimeRepairTimes = runTimeRepairTimes;
//    }
//
//    public double getRunTimeTotalRepairMinute() {
//        return runTimeTotalRepairMinute;
//    }
//
//    public void setRunTimeTotalRepairMinute(double runTimeTotalRepairMinute) {
//        this.runTimeTotalRepairMinute = runTimeTotalRepairMinute;
//    }
//
//    public int getRepairTimes() {
//        return repairTimes;
//    }
//
//    public void setRepairTimes(int repairTimes) {
//        this.repairTimes = repairTimes;
//    }
//
//    public double getTotalRepairMinute() {
//        return totalRepairMinute;
//    }
//
//    public void setTotalRepairMinute(double totalRepairMinute) {
//        this.totalRepairMinute = totalRepairMinute;
//    }
//
//    public double getMaintainMinutePreTime() {
//        return maintainMinutePreTime;
//    }
//
//    public void setMaintainMinutePreTime(double maintainMinutePreTime) {
//        this.maintainMinutePreTime = maintainMinutePreTime;
//    }
//
//    public double getRunTimeRepairMinutePreTime() {
//        return runTimeRepairMinutePreTime;
//    }
//
//    public void setRunTimeRepairMinutePreTime(double runTimeRepairMinutePreTime) {
//        this.runTimeRepairMinutePreTime = runTimeRepairMinutePreTime;
//    }
//
//    public double getTotalRepairMinutePreTime() {
//        return totalRepairMinutePreTime;
//    }
//
//    public void setTotalRepairMinutePreTime(double totalRepairMinutePreTime) {
//        this.totalRepairMinutePreTime = totalRepairMinutePreTime;
//    }
//
//    public double getTotalMaintainOfRepairRate() {
//        return totalMaintainOfRepairRate;
//    }
//
//    public void setTotalMaintainOfRepairRate(double totalMaintainOfRepairRate) {
//        this.totalMaintainOfRepairRate = totalMaintainOfRepairRate;
//    }
//
//    public int getPlanMaintainTimes() {
//        return planMaintainTimes;
//    }
//
//    public void setPlanMaintainTimes(int planMaintainTimes) {
//        this.planMaintainTimes = planMaintainTimes;
//    }
//
//    public double getMaintainFinishedRate() {
//        return maintainFinishedRate;
//    }
//
//    public void setMaintainFinishedRate(double maintainFinishedRate) {
//        this.maintainFinishedRate = maintainFinishedRate;
//    }
//
//    private int device_fault_times;
//
//    private double device_fault_minutes;
//
//    private int engine_fault_times;
//
//    private double engine_fault_minutes;
//
//    private int belt_fault_times;
//
//    private double belt_fault_minutes;
//
//    private int roll_fault_times;
//
//    private double roll_fault_minutes;
//
//    private int bearing_fault_times;
//
//    private double bearing_fault_minutes;
//
//    private int other_fault_times;
//
//    private double other_fault_minutes;
//
//    public int getDevice_fault_times() {
//        return device_fault_times;
//    }
//
//    public void setDevice_fault_times(int device_fault_times) {
//        this.device_fault_times = device_fault_times;
//    }
//
//    public double getDevice_fault_minutes() {
//        return device_fault_minutes;
//    }
//
//    public void setDevice_fault_minutes(double device_fault_minutes) {
//        this.device_fault_minutes = device_fault_minutes;
//    }
//
//    public int getEngine_fault_times() {
//        return engine_fault_times;
//    }
//
//    public void setEngine_fault_times(int engine_fault_times) {
//        this.engine_fault_times = engine_fault_times;
//    }
//
//    public double getEngine_fault_minutes() {
//        return engine_fault_minutes;
//    }
//
//    public void setEngine_fault_minutes(double engine_fault_minutes) {
//        this.engine_fault_minutes = engine_fault_minutes;
//    }
//
//    public int getBelt_fault_times() {
//        return belt_fault_times;
//    }
//
//    public void setBelt_fault_times(int belt_fault_times) {
//        this.belt_fault_times = belt_fault_times;
//    }
//
//    public double getBelt_fault_minutes() {
//        return belt_fault_minutes;
//    }
//
//    public void setBelt_fault_minutes(double belt_fault_minutes) {
//        this.belt_fault_minutes = belt_fault_minutes;
//    }
//
//    public int getRoll_fault_times() {
//        return roll_fault_times;
//    }
//
//    public void setRoll_fault_times(int roll_fault_times) {
//        this.roll_fault_times = roll_fault_times;
//    }
//
//    public double getRoll_fault_minutes() {
//        return roll_fault_minutes;
//    }
//
//    public void setRoll_fault_minutes(double roll_fault_minutes) {
//        this.roll_fault_minutes = roll_fault_minutes;
//    }
//
//    public int getBearing_fault_times() {
//        return bearing_fault_times;
//    }
//
//    public void setBearing_fault_times(int bearing_fault_times) {
//        this.bearing_fault_times = bearing_fault_times;
//    }
//
//    public double getBearing_fault_minutes() {
//        return bearing_fault_minutes;
//    }
//
//    public void setBearing_fault_minutes(double bearing_fault_minutes) {
//        this.bearing_fault_minutes = bearing_fault_minutes;
//    }
//
//    public int getOther_fault_times() {
//        return other_fault_times;
//    }
//
//    public void setOther_fault_times(int other_fault_times) {
//        this.other_fault_times = other_fault_times;
//    }
//
//    public double getOther_fault_minutes() {
//        return other_fault_minutes;
//    }
//
//    public void setOther_fault_minutes(double other_fault_minutes) {
//        this.other_fault_minutes = other_fault_minutes;
//    }
//
//
//    private String _id;
//    private String strcode;
//    private String title;
//    private String parent_title;
//    private String customer_name;
//
//    public String get_id() {
//        return _id;
//    }
//
//    public void set_id(String _id) {
//        this._id = _id;
//    }
//
//    public String getStrcode() {
//        return strcode;
//    }
//
//    public void setStrcode(String strcode) {
//        this.strcode = strcode;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getParent_title() {
//        return parent_title;
//    }
//
//    public void setParent_title(String parent_title) {
//        this.parent_title = parent_title;
//    }
//
//    public String getCustomer_name() {
//        return customer_name;
//    }
//
//    public void setCustomer_name(String customer_name) {
//        this.customer_name = customer_name;
//    }
//
//
//    private String account;
//    private String username;
//
//    public String getAccount() {
//        return account;
//    }
//
//    public void setAccount(String account) {
//        this.account = account;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    private double totalArriveMinute;
//    private double arriveMinutePreTime;
//    private int auditRepairTimes;
//    private double totalAuditRepairMinute;
//    private double auditMinutePreTime;
//
//    public double getTotalArriveMinute() {
//        return totalArriveMinute;
//    }
//
//    public void setTotalArriveMinute(double totalArriveMinute) {
//        this.totalArriveMinute = totalArriveMinute;
//    }
//
//    public double getArriveMinutePreTime() {
//        return arriveMinutePreTime;
//    }
//
//    public void setArriveMinutePreTime(double arriveMinutePreTime) {
//        this.arriveMinutePreTime = arriveMinutePreTime;
//    }
//
//    public int getAuditRepairTimes() {
//        return auditRepairTimes;
//    }
//
//    public void setAuditRepairTimes(int auditRepairTimes) {
//        this.auditRepairTimes = auditRepairTimes;
//    }
//
//    public double getTotalAuditRepairMinute() {
//        return totalAuditRepairMinute;
//    }
//
//    public void setTotalAuditRepairMinute(double totalAuditRepairMinute) {
//        this.totalAuditRepairMinute = totalAuditRepairMinute;
//    }
//
//    public double getAuditMinutePreTime() {
//        return auditMinutePreTime;
//    }
//
//    public void setAuditMinutePreTime(double auditMinutePreTime) {
//        this.auditMinutePreTime = auditMinutePreTime;
//    }
//
//
//    private double totalMinute;
//
//    public double getTotalMinute() {
//        return totalMinute;
//    }
//
//    public void setTotalMinute(double totalMinute) {
//        this.totalMinute = totalMinute;
//    }
//
//
//    private int inspectionTimes;
//    private double inspectionMinute;
//
//    public int getInspectionTimes() {
//        return inspectionTimes;
//    }
//
//    public void setInspectionTimes(int inspectionTimes) {
//        this.inspectionTimes = inspectionTimes;
//    }
//
//    public double getInspectionMinute() {
//        return inspectionMinute;
//    }
//
//    public void setInspectionMinute(double inspectionMinute) {
//        this.inspectionMinute = inspectionMinute;
//    }
}
