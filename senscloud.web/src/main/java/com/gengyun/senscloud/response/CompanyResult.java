package com.gengyun.senscloud.response;

import java.io.Serializable;

/**
 * 公司信息
 */
public class CompanyResult implements Serializable {
    private static final long serialVersionUID = -1;
    /**
     * 公司id
     */
    private long _id;
    /**
     * 公司名字
     */
    private String company_name;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }
}
