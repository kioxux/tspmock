package com.gengyun.senscloud.response;

import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@ApiModel(description = "返回页面信息")
public class ResponseModel {
    private static final Logger logger = LoggerFactory.getLogger(ResponseModel.class);
    @ApiModelProperty(value = ResponseConstant.RSP_CODES)
    private int code;
    @ApiModelProperty(value = ResponseConstant.RSP_DESC_MSG)
    private String msg;
    @ApiModelProperty(value = ResponseConstant.RSP_DESC_CONTENT)
    private Object content;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public ResponseModel() {
    }

    public ResponseModel(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseModel(int code, String msg, Object content) {
        this.code = code;
        this.msg = msg;
        this.content = content;
    }

    /**
     * 错误返回方法（错误消息提示）
     *
     * @param msgCode 错误编码
     * @param msgKey  错误消息的国际化主键
     * @return 错误信息
     */
    private static ResponseModel errorMsgInfo(Integer msgCode, String msgKey, Map<String, Object> errInfo) {
        errInfo = RegexUtil.optMapOrNew(errInfo);
        errInfo.put(ResponseConstant.ERROR_MSG_CODE, RegexUtil.optIntegerOrVal(msgCode, ResponseConstant.ERROR_CODE, LangConstant.TITLE_PD));
        RegexUtil.optNotNull(HttpRequestUtils.getResponse()).ifPresent(rp -> rp.setStatus(HttpStatus.SC_OK));
        return new ResponseModel(ResponseConstant.ERROR_CODE, msgKey, errInfo);
    }

    /**
     * 错误返回方法（错误消息文件）
     *
     * @param fileCode 错误文件编码
     * @return 错误信息
     */
    public static ResponseModel errorToFile(Object fileCode) {
        return errorMsgInfo(ResponseConstant.ERROR_CONTENT_CODE, LangConstant.MSG_BK, new HashMap<String, Object>() {
            {
                put(ResponseConstant.ERROR_MSG_FILE, fileCode);
            }
        }); // 未知错误！
    }

    public static ResponseModel errorQuickExp(SenscloudException e) {
        return RegexUtil.optNotNull(e.getMsgContent()).map(ThreadLocal::get).filter(info -> info.size() > 0)
                .map(m -> {
                    String msg = e.getMessage();
                    Integer msgCode = RegexUtil.optIntegerOrNull(m.get(ResponseConstant.ERROR_MSG_CODE), LangConstant.TITLE_PD);
                    if (null != msgCode) {
                        if (ResponseConstant.RE_LOGIN_CODE == msgCode) {
                            msg = LangConstant.MSG_CC;
                        } else if (ResponseConstant.ERROR_RIGHT_CODE == msgCode) {
                            msg = LangConstant.MSG_CA;
                        } else if (ResponseConstant.ERROR_ILLEGAL_CODE == msgCode) {
                            msg = LangConstant.MSG_CB;
                        } else {
                            msgCode = null;
                            msg = RegexUtil.optStrOrVal(e.getMessage(), LangConstant.MSG_BK); // 未知错误
                        }
                    }
                    RegexUtil.optNotBlankStrOpt(m.get(ResponseConstant.ERROR_MSG_REMARK)).ifPresent(logger::error);
                    return errorMsgInfo(msgCode, msg, m);
                })
                .orElseGet(() -> ResponseModel.errorMsgInfo(null, e.getMessage(), null));
    }

    public static ResponseModel expUnKnown(Exception e) {
        logger.error("scdErrTotal:", e);
        String msg = RegexUtil.optNotBlankStrOpt(e.getMessage()).filter(m -> m.contains("FileUploadBase")).filter(m -> m.contains("FileSizeLimitExceededException") || m.contains("SizeLimitExceededException")).map(m -> LangConstant.TITLE_FILE_H).orElse(LangConstant.MSG_BK);
        RegexUtil.optNotBlankStrOpt(e.getMessage()).filter(m -> m.contains("Request method")).filter(m -> m.contains("not supported")).ifPresent(m -> {
            RegexUtil.optNotNull(HttpRequestUtils.getRequest()).ifPresent(rq -> {
                try {
                    rq.getRequestDispatcher("/index.html").forward(rq, HttpRequestUtils.getResponse());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
        });
        boolean isToOk = RegexUtil.optNotNull(e.getStackTrace()).map(stl -> Arrays.stream(stl).anyMatch(st -> "SelectOptionsController.java".equals(RegexUtil.optStrOrBlank(st.getFileName())))).orElse(false);
        return isToOk ? ResponseModel.okOnly() : errorMsgInfo(null, msg, null); // 未知错误
    }

    public static ResponseModel reLogin() {
        return errorMsgInfo(ResponseConstant.RE_LOGIN_CODE, null, null); // LangConstant.MSG_CC
    }

    public static ResponseModel errorRight() {
        return errorMsgInfo(ResponseConstant.ERROR_RIGHT_CODE, LangConstant.MSG_CA, null);
    }

    public static ResponseModel errorIllegal() {
        return errorMsgInfo(ResponseConstant.ERROR_ILLEGAL_CODE, LangConstant.MSG_CB, null);
    }

    public static ResponseModel okMsgForOperate() {
        return new ResponseModel(ResponseConstant.SUCCESS_CODE, LangConstant.MSG_I); // 操作成功！
    }

    public static ResponseModel okMsg(String msg) {
        return new ResponseModel(ResponseConstant.SUCCESS_CODE, msg, null); // 操作成功！
    }

    public static ResponseModel okMsgContent(Object content) {
        return new ResponseModel(ResponseConstant.SUCCESS_CODE, LangConstant.MSG_I, content); // 操作成功！
    }

    public static ResponseModel ok(Object content) {
        return new ResponseModel(ResponseConstant.SUCCESS_CODE, null, content);
    }

    public static ResponseModel okExport(Object content) {
        return new ResponseModel(ResponseConstant.EXPORT_CODE, null, content);
    }

    public static ResponseModel okOnly() {
        return new ResponseModel(ResponseConstant.SUCCESS_CODE, null, null);
    }

    /**
     * 将Object对象里面的属性和值转化成Map对象
     *
     * @return
     * @throws IllegalAccessException
     */
    public Map<String, Object> objectToMap() {
        try {
            Map<String, Object> map = new HashMap<>();
            Field[] declaredFields = this.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                field.setAccessible(true);
                map.put(field.getName(), field.get(this));
            }
            return map;
        } catch (Exception e) {
            return null;
        }
    }
}
