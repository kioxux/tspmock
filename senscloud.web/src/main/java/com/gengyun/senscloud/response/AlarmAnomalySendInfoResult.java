package com.gengyun.senscloud.response;

/**
 * 设备监控报警异常记录表实例
 * Created by DwD on 2018/8/6.
 */
public class AlarmAnomalySendInfoResult {
//
//    private String alarm_code;//主键
//    private String asset_code;//设备编码
//    private String monitor_name;//监控项名称
//    private String monitor_name_new;//监控项名称
//    private int monitor_value_type;//监控值类型
//    private String monitor_value_unit;//单位
//    private String monitor_value;//监控值
//    private String gather_time;//收集时间
//    private String alarm_up;//报警上限
//    private String alarm_down;//报警下限
//
//    public String getMonitor_name_new() {
//        return monitor_name_new;
//    }
//
//    public void setMonitor_name_new(String monitor_name_new) {
//        this.monitor_name_new = monitor_name_new;
//    }
//
//    private String exception_up;//异常上限
//    private String exception_down;//异常下限
//    private String alarm_msg;//是否发送报警消息
//    private String exception_msg;//是否发送异常消息
//    private int is_send_task;//是否生成维修单
//    private int is_send_msg;//是否生成维修单
//    private String alarm_up_task_code;//报警上限任务代码
//    private String alarm_down_task_code;//报警下限任务代码
//    private int sms_times ;//短信次数：0；1；2；3；4；5。。。。
//    private int status;//1：未处理；2：处理中；3：已处理；4：取消；
//    private Timestamp create_time;//创建时间
//    private String strname;//设备名称
//    private  Timestamp send_sms_time;//上次发送时间
//    private int intstatus;
//    private String asset_status;
//    private String repair_code;
//    private String fault_note;
//
//    public String getAlarm_code() {
//        return alarm_code;
//    }
//
//    public void setAlarm_code(String alarm_code) {
//        this.alarm_code = alarm_code;
//    }
//
//    public String getAsset_code() {
//        return asset_code;
//    }
//
//    public void setAsset_code(String asset_code) {
//        this.asset_code = asset_code;
//    }
//
//    public String getMonitor_name() {
//        return monitor_name;
//    }
//
//    public void setMonitor_name(String monitor_name) {
//        this.monitor_name = monitor_name;
//    }
//
//    public int getMonitor_value_type() {
//        return monitor_value_type;
//    }
//
//    public void setMonitor_value_type(int monitor_value_type) {
//        this.monitor_value_type = monitor_value_type;
//    }
//
//    public String getMonitor_value_unit() {
//        return monitor_value_unit;
//    }
//
//    public void setMonitor_value_unit(String monitor_value_unit) {
//        this.monitor_value_unit = monitor_value_unit;
//    }
//
//    public String getMonitor_value() {
//        return monitor_value;
//    }
//
//    public void setMonitor_value(String monitor_value) {
//        this.monitor_value = monitor_value;
//    }
//
//    public String getGather_time() {
//        return gather_time;
//    }
//
//    public void setGather_time(String gather_time) {
//        this.gather_time = gather_time;
//    }
//
//    public String getAlarm_up() {
//        return alarm_up;
//    }
//
//    public void setAlarm_up(String alarm_up) {
//        this.alarm_up = alarm_up;
//    }
//
//    public String getAlarm_down() {
//        return alarm_down;
//    }
//
//    public void setAlarm_down(String alarm_down) {
//        this.alarm_down = alarm_down;
//    }
//
//    public String getException_up() {
//        return exception_up;
//    }
//
//    public void setException_up(String exception_up) {
//        this.exception_up = exception_up;
//    }
//
//    public String getException_down() {
//        return exception_down;
//    }
//
//    public void setException_down(String exception_down) {
//        this.exception_down = exception_down;
//    }
//
//    public int getIs_send_task() {
//        return is_send_task;
//    }
//
//    public void setIs_send_task(int is_send_task) {
//        this.is_send_task = is_send_task;
//    }
//
//    public int getIs_send_msg() {
//        return is_send_msg;
//    }
//
//    public void setIs_send_msg(int is_send_msg) {
//        this.is_send_msg = is_send_msg;
//    }
//
//    public String getAlarm_up_task_code() {
//        return alarm_up_task_code;
//    }
//
//    public void setAlarm_up_task_code(String alarm_up_task_code) {
//        this.alarm_up_task_code = alarm_up_task_code;
//    }
//
//    public String getAlarm_down_task_code() {
//        return alarm_down_task_code;
//    }
//
//    public void setAlarm_down_task_code(String alarm_down_task_code) {
//        this.alarm_down_task_code = alarm_down_task_code;
//    }
//
//    public int getSms_times() {
//        return sms_times;
//    }
//
//    public void setSms_times(int sms_times) {
//        this.sms_times = sms_times;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getStrname() {
//        return strname;
//    }
//
//    public void setStrname(String strname) {
//        this.strname = strname;
//    }
//
//    public Timestamp getSend_sms_time() {
//        return send_sms_time;
//    }
//
//    public void setSend_sms_time(Timestamp send_sms_time) {
//        this.send_sms_time = send_sms_time;
//    }
//
//    public int getIntstatus() {
//        return intstatus;
//    }
//
//    public void setIntstatus(int intstatus) {
//        this.intstatus = intstatus;
//    }
//
//    public String getAsset_status() {
//        return asset_status;
//    }
//
//    public void setAsset_status(String asset_status) {
//        this.asset_status = asset_status;
//    }
//
//    public String getRepair_code() {
//        return repair_code;
//    }
//
//    public void setRepair_code(String repair_code) {
//        this.repair_code = repair_code;
//    }
//
//    public String getFault_note() {
//        return fault_note;
//    }
//
//    public void setFault_note(String fault_note) {
//        this.fault_note = fault_note;
//    }
//
//    public void setAlarm_msg(String alarm_msg) {
//        this.alarm_msg = alarm_msg;
//    }
//
//    public void setException_msg(String exception_msg) {
//        this.exception_msg = exception_msg;
//    }
}
