package com.gengyun.senscloud.response;

import java.sql.Timestamp;

public class PositionResult {
    private Integer positionId;
    private Integer id;
    private String position_name;
    private Integer group_id;
    private String remark;
    private Timestamp create_time;
    private String create_user_account;

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public Integer getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Integer group_id) {
        this.group_id = group_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }
}
