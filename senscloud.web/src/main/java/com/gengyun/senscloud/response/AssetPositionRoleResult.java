package com.gengyun.senscloud.response;

import java.util.List;

public class AssetPositionRoleResult {
    private String position_code;//系统编码
    private String position_name;//名称
    private String parent_code;//所属位置，可空
    private int address_id;//地址
    private int org_id;//组织类型
    private int position_type_id;//组织类型
    private String location;//坐标
    private String file_url;
    private Integer file_width;
    private Integer file_height;
    private Integer child_count;
    private int isSelect;//该角色是否选中 1选中 2未选中
    private List<AssetPositionRoleResult> childrens; //设备位置子类集合

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getPosition_type_id() {
        return position_type_id;
    }

    public void setPosition_type_id(int position_type_id) {
        this.position_type_id = position_type_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public Integer getFile_width() {
        return file_width;
    }

    public void setFile_width(Integer file_width) {
        this.file_width = file_width;
    }

    public Integer getFile_height() {
        return file_height;
    }

    public void setFile_height(Integer file_height) {
        this.file_height = file_height;
    }

    public Integer getChild_count() {
        return child_count;
    }

    public void setChild_count(Integer child_count) {
        this.child_count = child_count;
    }

    public int getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(int isSelect) {
        this.isSelect = isSelect;
    }

    public List<AssetPositionRoleResult> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<AssetPositionRoleResult> childrens) {
        this.childrens = childrens;
    }
}
