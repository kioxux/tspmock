package com.gengyun.senscloud.response;

public class FacilitiesResult {
//    private String schema_name;
//    private Integer id;
//    private String title;
//    private int parentId;
//    private String parentName;
//    private Timestamp create_time;
//    private String remark;
//    private boolean isuse;
//    private double lng;//经度
//    private double lat;//维度
//    private String location;
//    private String maptype;
//    private int facilitytypeid;
//    private String facilitycode;
//    private String address;
//    private String areacode;
//    private String layerpath;
//    private String file_url;
//    private Integer file_width;
//    private Integer file_height;
//    private String facility_no;   //位置区分父子类型的编号：001，001001
////    private boolean hasAwaitingRepairAsset = false; //是否有待修复的设备
////    private int repairAssetCount = 0; //待维修设备数量
////    private int maintainAssetCount = 0; //保养设备数量
//    private int repairMaintainAssetCount = 0;//待维修、待保养设备数量
//    private int doingRepairMaintainAssetCount=0;//处理中的设备数量
//    private int untreatedRepairMaintainAssetCount=0;//待处理设备数量
//    private int iotAssetCount = 0;//物联设备数量
//    private int onlineIotAssetCount = 0;//在线物联设备数量
//    private int offlineIotAssetCount = 0;//离线物联设备数量
//    private int assetCount = 0;//设备数量
//
//    public String getSchema_name() {
//        return schema_name;
//    }
//
//    public void setSchema_name(String schema_name) {
//        this.schema_name = schema_name;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public int getParentId() {
//        return parentId;
//    }
//
//    public void setParentId(int parentId) {
//        this.parentId = parentId;
//    }
//
//    public String getParentName() {
//        return parentName;
//    }
//
//    public void setParentName(String parentName) {
//        this.parentName = parentName;
//    }
//
//    public Timestamp getCreate_time() {
//        return create_time;
//    }
//
//    public void setCreate_time(Timestamp create_time) {
//        this.create_time = create_time;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public boolean isIsuse() {
//        return isuse;
//    }
//
//    public void setIsuse(boolean isuse) {
//        this.isuse = isuse;
//    }
//
//    public double getLng() {
//        return lng;
//    }
//
//    public void setLng(double lng) {
//        this.lng = lng;
//    }
//
//    public double getLat() {
//        return lat;
//    }
//
//    public void setLat(double lat) {
//        this.lat = lat;
//    }
//
//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
//
//    public String getMaptype() {
//        return maptype;
//    }
//
//    public void setMaptype(String maptype) {
//        this.maptype = maptype;
//    }
//
//    public int getFacilitytypeid() {
//        return facilitytypeid;
//    }
//
//    public void setFacilitytypeid(int facilitytypeid) {
//        this.facilitytypeid = facilitytypeid;
//    }
//
//    public String getFacilitycode() {
//        return facilitycode;
//    }
//
//    public void setFacilitycode(String facilitycode) {
//        this.facilitycode = facilitycode;
//    }
//
//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    public String getAreacode() {
//        return areacode;
//    }
//
//    public void setAreacode(String areacode) {
//        this.areacode = areacode;
//    }
//
//    public String getLayerpath() {
//        return layerpath;
//    }
//
//    public void setLayerpath(String layerpath) {
//        this.layerpath = layerpath;
//    }
//
//    public String getFile_url() {
//        return file_url;
//    }
//
//    public void setFile_url(String file_url) {
//        this.file_url = file_url;
//    }
//
//    public Integer getFileWidth() {
//        return file_width;
//    }
//
//    public void setFileWidth(Integer _file_width) {
//        this.file_width = _file_width;
//    }
//
//    public Integer getFileHeight() {
//        return file_height;
//    }
//
//    public void setFileHeight(Integer _file_height) {
//        this.file_height = _file_height;
//    }
//
//    public String getFacilityNo() {
//        return facility_no;
//    }
//
//    public void setFacilityNo(String facility_no) {
//        this.facility_no = facility_no;
//    }
//
//    //位置的设备
//    private List<Asset> assetList;
//
//    public List<Asset> getAssetList() {
//        return assetList;
//    }
//
//    public void setAssetList(List<Asset> assetList) {
//        this.assetList = assetList;
//    }
//
//    //巡更点
//    private List<InspectionAreaData> inspectionAreaList;
//
//    public List<InspectionAreaData> getInspectionList() {
//        return inspectionAreaList;
//    }
//
//    public void setInspectionList(List<InspectionAreaData> inspectionAreaList) {
//        this.inspectionAreaList = inspectionAreaList;
//    }
//
//    public int getIotAssetCount() {
//        return iotAssetCount;
//    }
//
//    public void setIotAssetCount(int iotAssetCount) {
//        this.iotAssetCount = iotAssetCount;
//    }
//
//    public int getOnlineIotAssetCount() {
//        return onlineIotAssetCount;
//    }
//
//    public void setOnlineIotAssetCount(int onlineIotAssetCount) {
//        this.onlineIotAssetCount = onlineIotAssetCount;
//    }
//
//    public int getOfflineIotAssetCount() {
//        return offlineIotAssetCount;
//    }
//
//    public void setOfflineIotAssetCount(int offlineIotAssetCount) {
//        this.offlineIotAssetCount = offlineIotAssetCount;
//    }
//
//    public int getRepairMaintainAssetCount() {
//        return repairMaintainAssetCount;
//    }
//
//    public void setRepairMaintainAssetCount(int repairMaintainAssetCount) {
//        this.repairMaintainAssetCount = repairMaintainAssetCount;
//    }
//
//    public int getAssetCount() {
//        return assetCount;
//    }
//
//    public void setAssetCount(int assetCount) {
//        this.assetCount = assetCount;
//    }
//
//    public int getDoingRepairMaintainAssetCount() {
//        return doingRepairMaintainAssetCount;
//    }
//
//    public void setDoingRepairMaintainAssetCount(int doingRepairMaintainAssetCount) {
//        this.doingRepairMaintainAssetCount = doingRepairMaintainAssetCount;
//    }
//
//    public int getUntreatedRepairMaintainAssetCount() {
//        return untreatedRepairMaintainAssetCount;
//    }
//
//    public void setUntreatedRepairMaintainAssetCount(int untreatedRepairMaintainAssetCount) {
//        this.untreatedRepairMaintainAssetCount = untreatedRepairMaintainAssetCount;
//    }
}
