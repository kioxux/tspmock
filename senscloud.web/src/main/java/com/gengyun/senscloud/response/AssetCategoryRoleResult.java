package com.gengyun.senscloud.response;

import java.io.Serializable;
import java.util.List;

public class AssetCategoryRoleResult implements Serializable {
    private Integer id;
    private String category_name;
    private Integer parent_id;
    private int isSelect;//该角色是否选中 1选中 2未选中
    private List<AssetCategoryRoleResult> childrens; //设备类型子类集合

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public int getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(int isSelect) {
        this.isSelect = isSelect;
    }

    public List<AssetCategoryRoleResult> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<AssetCategoryRoleResult> childrens) {
        this.childrens = childrens;
    }
}
