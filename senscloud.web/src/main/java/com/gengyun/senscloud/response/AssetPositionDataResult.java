package com.gengyun.senscloud.response;

import java.util.List;

public class AssetPositionDataResult {

    //当前的位置
    private AssetPositionResult assetPosition;

    public AssetPositionResult getAssetPosition() {
        return assetPosition;
    }

    public void setAssetPosition(AssetPositionResult assetPosition) {
        this.assetPosition = assetPosition;
    }

    //所有的子集位置
    private List<AssetPositionResult> subAssetPositionData;

    public List<AssetPositionResult> getSubAssetPositionData() {
        return subAssetPositionData;
    }

    public void setSubAssetPositionData(List<AssetPositionResult> subAssetPositionData) {
        this.subAssetPositionData = subAssetPositionData;
    }

    //所有的待办设备，包含维修、保养、巡检，按时间倒序
    private List<ToDoDataListResult> todoData;

    public List<ToDoDataListResult> getToDoData() {
        return todoData;
    }

    public void setToDoData(List<ToDoDataListResult> todoData) {
        this.todoData = todoData;
    }


    //设备总数，按类型进行统计的总数,按待办类型的总数
    private List<AssetTypeTotalResult> assetTypeTotalResultData;

    public List<AssetTypeTotalResult> getAssetTypeTotalResult() {
        return assetTypeTotalResultData;
    }

    public void setAssetTypeTotalResult(List<AssetTypeTotalResult> assetTypeTotalResultData) {
        this.assetTypeTotalResultData = assetTypeTotalResultData;
    }

    //所有的设备
    private List<AssetDataResult> assetData;

    public List<AssetDataResult> getAssetData() {
        return assetData;
    }

    public void setAssetData(List<AssetDataResult> assetData) {
        this.assetData = assetData;
    }

}
