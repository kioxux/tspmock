package com.gengyun.senscloud.response;

import java.sql.Timestamp;
import java.util.List;

public class GroupResult {
    private Integer id;
    private String group_name;
    private String group_code;
    private Integer parent_id;
    private String remark;
    private Timestamp create_time;
    private String create_user_id;
    private String create_user_account;
    private Boolean is_out;
    private List<GroupResult> childrens;
    private Boolean is_group;
    private String title;
    private Integer org_id;
    private List<PositionResult> positions;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getOrg_id() {
        return org_id;
    }

    public void setOrg_id(Integer org_id) {
        this.org_id = org_id;
    }

    public Boolean getIs_group() {
        return is_group;
    }

    public void setIs_group(Boolean is_group) {
        this.is_group = is_group;
    }

    public List<PositionResult> getPositions() {
        return positions;
    }

    public void setPositions(List<PositionResult> positions) {
        this.positions = positions;
    }

    public List<GroupResult> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<GroupResult> childrens) {
        this.childrens = childrens;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }

    public Boolean getIs_out() {
        return is_out;
    }

    public void setIs_out(Boolean is_out) {
        this.is_out = is_out;
    }
}
