package com.gengyun.senscloud.response;

import java.sql.Timestamp;
import java.util.List;

/**
 * 权限角色关连列表
 */
public class PermissionRoleResult {
    private String id;//企业id
    private String menu_name;//名称
    private String key;//键
    private String url;//默认链接(分厂)
    private String root_url;//树结构根节点链接
    private String position_url;//树结构车间链接
    private String asset_url;//树结构设备连接
    private String parent_id;//父菜单
    private boolean available;//是否可用
    private Timestamp createtime;//
    private int index;//排序
    private String icon;//样式名
    private int type;//菜单还是功能:1:菜单2:功能3:操作
    private String typeName;//
    private int tree_type;//树类型（菜单对应的页面中的树类型）：1：设备组织树；2：设备树；3：备件树；其他：无树；
    private int isSelect;//该角色是否选中 1选中 2未选中
    private String lang_key;//多语言key
    private List<PermissionRoleResult> childrens; //权限子类集合

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRoot_url() {
        return root_url;
    }

    public void setRoot_url(String root_url) {
        this.root_url = root_url;
    }

    public String getPosition_url() {
        return position_url;
    }

    public void setPosition_url(String position_url) {
        this.position_url = position_url;
    }

    public String getAsset_url() {
        return asset_url;
    }

    public void setAsset_url(String asset_url) {
        this.asset_url = asset_url;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTree_type() {
        return tree_type;
    }

    public void setTree_type(int tree_type) {
        this.tree_type = tree_type;
    }

    public int getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(int isSelect) {
        this.isSelect = isSelect;
    }

    public List<PermissionRoleResult> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<PermissionRoleResult> childrens) {
        this.childrens = childrens;
    }

    public String getLang_key() {
        return lang_key;
    }

    public void setLang_key(String lang_key) {
        this.lang_key = lang_key;
    }
}
