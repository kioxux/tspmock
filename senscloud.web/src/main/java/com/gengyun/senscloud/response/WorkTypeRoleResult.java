package com.gengyun.senscloud.response;

import java.io.Serializable;
import java.sql.Timestamp;

public class WorkTypeRoleResult implements Serializable {
    private Integer id;//工单类型id
    private String type_name;//工单类型名称
    private String flow_template_code;//关联流程模板
    private String work_template_code;//工单模板
    private Integer business_type_id;//业务类型
    private Integer order;//排序
    private boolean isuse;//是否启用
    private Timestamp createdTime;//创建时间
    private String create_user_id;//创建人ID
    private String  create_user_account;//创建人
    private int isSelect;//该角色是否选中 1选中 2未选中

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getFlow_template_code() {
        return flow_template_code;
    }

    public void setFlow_template_code(String flow_template_code) {
        this.flow_template_code = flow_template_code;
    }

    public String getWork_template_code() {
        return work_template_code;
    }

    public void setWork_template_code(String work_template_code) {
        this.work_template_code = work_template_code;
    }

    public Integer getBusiness_type_id() {
        return business_type_id;
    }

    public void setBusiness_type_id(Integer business_type_id) {
        this.business_type_id = business_type_id;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public boolean isIsuse() {
        return isuse;
    }

    public void setIsuse(boolean isuse) {
        this.isuse = isuse;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreate_user_account() {
        return create_user_account;
    }

    public void setCreate_user_account(String create_user_account) {
        this.create_user_account = create_user_account;
    }

    public String getCreate_user_id() {
        return create_user_id;
    }

    public void setCreate_user_id(String create_user_id) {
        this.create_user_id = create_user_id;
    }

    public int getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(int isSelect) {
        this.isSelect = isSelect;
    }

}
