package com.gengyun.senscloud.response;

public class WorkflowStartResult {
    private boolean started;
    private String formKey;

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public WorkflowStartResult(boolean started, String formKey) {
        this.started = started;
        this.formKey = formKey;
    }
}
