package com.gengyun.senscloud.response;

import java.sql.Timestamp;

public class AssetPositionResult {
    private String schema_name;
    private String position_code;
    private String position_name;
    private Integer facility_type_id;
    private String parent_code;
    private String location;
    private String layer_path;
    private String remark;
    private String file_id;
    private Timestamp create_time;
    private Integer status;
    private String file_url;
    private Integer file_width;
    private Integer file_height;


    private Integer repairMaintainAssetCount = 0;//待维修、待保养设备数量
    private Integer assetCount = 0;//设备数量
    private Integer iotAssetCount = 0;//物联设备数量
    private Integer onlineIotAssetCount = 0;//在线物联设备数量
    private Integer offlineIotAssetCount = 0;//离线物联设备数量




//    private String facilitycode;
//    private String address;
//    private String areacode;
//    private String facility_no;   //位置区分父子类型的编号：001，001001
//    private boolean hasAwaitingRepairAsset = false; //是否有待修复的设备
//    private int repairAssetCount = 0; //待维修设备数量
//    private int maintainAssetCount = 0; //保养设备数量
//    private int doingRepairMaintainAssetCount=0;//处理中的设备数量
//    private int untreatedRepairMaintainAssetCount=0;//待处理设备数量
//    //位置的设备
//    private List<Asset> assetList;
//    //巡更点
//    private List<InspectionAreaData> inspectionAreaList;
//


    public String getSchema_name() {
        return schema_name;
    }

    public void setSchema_name(String schema_name) {
        this.schema_name = schema_name;
    }

    public String getPosition_code() {
        return position_code;
    }

    public void setPosition_code(String position_code) {
        this.position_code = position_code;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public Integer getFacility_type_id() {
        return facility_type_id;
    }

    public void setFacility_type_id(Integer facility_type_id) {
        this.facility_type_id = facility_type_id;
    }

    public String getParent_code() {
        return parent_code;
    }

    public void setParent_code(String parent_code) {
        this.parent_code = parent_code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLayer_path() {
        return layer_path;
    }

    public void setLayer_path(String layer_path) {
        this.layer_path = layer_path;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public Integer getFile_width() {
        return file_width;
    }

    public void setFile_width(Integer file_width) {
        this.file_width = file_width;
    }

    public Integer getFile_height() {
        return file_height;
    }

    public void setFile_height(Integer file_height) {
        this.file_height = file_height;
    }

    public Integer getRepairMaintainAssetCount() {
        return repairMaintainAssetCount;
    }

    public void setRepairMaintainAssetCount(Integer repairMaintainAssetCount) {
        this.repairMaintainAssetCount = repairMaintainAssetCount;
    }

    public Integer getAssetCount() {
        return assetCount;
    }

    public void setAssetCount(Integer assetCount) {
        this.assetCount = assetCount;
    }

    public Integer getIotAssetCount() {
        return iotAssetCount;
    }

    public void setIotAssetCount(Integer iotAssetCount) {
        this.iotAssetCount = iotAssetCount;
    }

    public Integer getOnlineIotAssetCount() {
        return onlineIotAssetCount;
    }

    public void setOnlineIotAssetCount(Integer onlineIotAssetCount) {
        this.onlineIotAssetCount = onlineIotAssetCount;
    }

    public Integer getOfflineIotAssetCount() {
        return offlineIotAssetCount;
    }

    public void setOfflineIotAssetCount(Integer offlineIotAssetCount) {
        this.offlineIotAssetCount = offlineIotAssetCount;
    }
}
