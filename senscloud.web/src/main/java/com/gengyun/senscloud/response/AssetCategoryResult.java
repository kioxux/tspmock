package com.gengyun.senscloud.response;

import java.util.List;

public class AssetCategoryResult {
    private Integer id;
    private String category_name;
    private Integer parent_id;
    private String fields;
    List<AssetCategoryResult> childrens;

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public List<AssetCategoryResult> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<AssetCategoryResult> childrens) {
        this.childrens = childrens;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }
}
