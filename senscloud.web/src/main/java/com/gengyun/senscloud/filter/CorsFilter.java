package com.gengyun.senscloud.filter;

import com.gengyun.senscloud.util.RegexUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Title: 跨域访问处理(跨域资源共享)
 * Description:解决前后端分离架构中的跨域问题
 *
 * @author 穆书伟
 * Date: 2017/10/10
 * Time: 上午16:55:36
 */
public class CorsFilter implements Filter {
    /**
     * Log4j日志处理(@author: 穆书伟)
     */
    private String allowOrigin;
    private String allowMethods;
    private String allowCredentials;
    private String allowHeaders;
    private String exposeHeaders;

    @Override
    public void init(FilterConfig filterConfig) {
        allowOrigin = filterConfig.getInitParameter("allowOrigin");
        allowMethods = filterConfig.getInitParameter("allowMethods");
        allowCredentials = filterConfig.getInitParameter("allowCredentials");
        allowHeaders = filterConfig.getInitParameter("allowHeaders");
        exposeHeaders = filterConfig.getInitParameter("exposeHeaders");

    }

    /**
     * 通过CORS技术实现AJAX跨域访问，只要将CORS响应头写入response对象中即可
     *
     * @param servletRequest  servletRequest
     * @param servletResponse servletResponse
     * @param filterChain     filterChain
     * @throws IOException      IOException
     * @throws ServletException ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String currentOrigin = request.getHeader("Origin");
        RegexUtil.optNotBlankStrOpt(allowOrigin).map(","::split).filter(RegexUtil::optIsPresentArray).map(Arrays::asList).filter(aa -> aa.contains(currentOrigin)).ifPresent(ao -> response.setHeader("Access-Control-Allow-Origin", currentOrigin));
        RegexUtil.optNotBlankStrOpt(allowMethods).ifPresent(am -> response.setHeader("Access-Control-Allow-Methods", am));
        RegexUtil.optNotBlankStrOpt(allowCredentials).ifPresent(am -> response.setHeader("Access-Control-Allow-Credentials", am));
        RegexUtil.optNotBlankStrOpt(allowHeaders).ifPresent(am -> response.setHeader("Access-Control-Allow-Headers", am));
        RegexUtil.optNotBlankStrOpt(exposeHeaders).ifPresent(am -> response.setHeader("Access-Control-Expose-Headers", am));
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
