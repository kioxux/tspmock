package com.gengyun.senscloud.filter;

import com.alibaba.druid.support.http.WebStatFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(filterName="druidWebStatFilter",urlPatterns="/*",
        initParams={
                @WebInitParam(name="exclusions",value="*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")// 忽略资源
        }
)
/**
 * @author 穆书伟
 * @date 2017年11月2日 10:10
 * @description Druid数据连接池过滤器
 */
public class DruidStatFilter extends WebStatFilter {

}
