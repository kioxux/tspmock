package com.gengyun.senscloud.common;

/**
 * 设备调拨审核节点
 */
public enum AssetTransferAuditNode {
    TRANSFER_OUT_AUDIT(LangConstant.TITLE_AAZ_Y), // 调出审核
    TRANSFER_IN_AUDIT(LangConstant.TITLE_AAY_B), // 调入审核
    TRANSFER_OUT_ASSET(LangConstant.TITLE_ASSET_AC_H), // 调出设备
    TRANSFER_IN_ASSET(LangConstant.TITLE_ASSET_AC_I); // 调入设备

    private String value;

    AssetTransferAuditNode(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
