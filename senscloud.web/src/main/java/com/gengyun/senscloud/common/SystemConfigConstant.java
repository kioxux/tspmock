package com.gengyun.senscloud.common;

/**
 * 系统配置表常量
 */
public interface SystemConfigConstant {

    String WX_CHOOSE_ALBUM = "wx_choose_album";//小程序图片上传是否支持选择手机相册
    String QR_CODE_PREFIX = "qr_code_prefix";//二维码前缀
    String CODE_TEMPLATE = "code_template";//二维码模版
    String BOM_CONSUME_TYPE = "bom_consume_type";//备件消耗类型
    String CLIENT_ENVIRONMENT = "client_environment";//系统应用环境
    String ADDRESS_TYPE = "address_type";//地址类型
    String ASSET_ORG_POSITION_TYPE = "asset_org_position_type";//组织位置主导类型
    String CURRENT_CURRENCY = "current_currency";//货币单位
    String PIC_ADD_WATERMARK = "pic_add_watermark";//图片是否加水印
    String QUICK_URL = "quick_url";//快捷跳转
    String WORK_INTERVAL_SHOW = "work_interval_show"; // 工单列表时效显示
    String SERVICE_MOBILE = "service_mobile"; // 客服电话
    String REPAIR_HELP = "repair_help"; // 报修助手
    String REPORT_LOGO = "report_logo";//报表logo
    String COMPRESSION_IMG_TYPE = "jpg";//压缩支持类型
    String PHONE_REPIAR_WORK_TYPE_ID = "phone_repiar_work_type_id";//手机端问题上报工单类型
    String PHONE_SETUP_WORK_TYPE_ID = "phone_setup_work_type_id";//手机端安装调试工单类型
    String PLAN_WORK_CALENDAR_GENERATE_YEAR = "plan_work_calendar_generate_year";//生成行事历的时间范围(单位：年)
    String PLAN_WORK_CALENDAR_GENERATE_WORK_ORDER_ADVANCE_DAY = "plan_work_calendar_generate_work_order_advance_day";//行事历生成工单的提前天数
    String TOP_PIC_ID = "top_pic_id"; // 顶层平面图信息
    String OUT_SERVER_AAA_AAA = "out_server_aaa_aaa"; // 中法长寿智能锁地址信息http://mail.ccipsf.com:24526/zf/coswinWorkOrder/insert,218.207.9.84 24526
    /**
     * 系统默认国际化语言配置
     */
    String DEFAULT_LANGUAGE = "default_language";
    /**
     * 检查密码是否为强验证密码
     */
    String IS_VALIDATE_STRONG_PASSWORD = "is_validate_strong_password";
    /**
     * 手机端图片上传是否支持选择手机相册
     */
    String PHONE_CHOOSE_ALBUM = "phone_choose_album";

    //工单类型
    String WORK_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CATEGORY_AB_F + "','name':'business_type_id','option':'select','selectKey':'business_type'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //维修类型
    String REPAIR_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //设备运行状态
    String ASSET_RUNNING_STATUS_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'running_status'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //设备级别
    String IMPORTMENT_LEVEL_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'level_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //备件类型
    String BOM_TYPE_FIELD = "{'isTree':true,'field':[{'langKey':'" + LangConstant.TITLE_CATEGORY_AI + "','name':'type_code'},{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'},{'langKey':'" + LangConstant.TITLE_F + "','name':'parent_id','option':'select','isTree':true,'selectKey':'bom_type_all'}]}";
    //位置类型
    String FACILITY_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //文档类型
    String FILE_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_CATEGORY_AB_A + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //单位管理
    String UNIT_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_BAAB_A + "','name':'unit_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //完修说明
    String WORK_FINISHED_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'finished_name'},{'langKey':'" + LangConstant.TITLE_AAU_T + "','name':'is_finished','option':'select','selectKey':'log_whether'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
    //货币种类 todo 货币符号多语言缺失
    String CURRENCY_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'currency_name'},{'langKey':'" + LangConstant.TITLE_AAU_T + "','name':'currency_sign'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'isuse','option':'radio','selectKey':'is_enable'}]}";
}
