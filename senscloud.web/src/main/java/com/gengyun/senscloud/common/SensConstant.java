package com.gengyun.senscloud.common;

import java.util.HashMap;
import java.util.Map;

public interface SensConstant {
    String SCHEMA_PREFIX = "sc_com_";
    Integer DOSAGE_SUPPLEMENT = 1;
    Integer DOSAGE_IOT = 3;
    Integer DOSAGE_QR_CODE = 2;
    String PC_CLIENT_NAME = "PC";//PC端
    String APP_CLIENT_NAME = "android";//Android端
    String IOS_CLIENT_NAME = "IOS";//IOS端
    String WECHAT_CLIENT_NAME = "WECHAT";//微信端
    String DINGTALK_CLIENT_NAME = "DINGTALK";//钉钉

    String DASHBOARD_REGEX = "\\[([^\\]]+):([^\\[]+)\\]";

    String PAGE_ERROR_SUBMIT_REPEAT = "数据已变更，请重新查询获取最新数据！";
    String PAGE_ERROR_NO_PC_SUBMIT_RIGHT = "没有PC端执行提交权限，请联系管理员！";
    String REQUIRED_MSG = "不能为空！";
    String WAE101 = "WAE101";
    String WAE102 = "WAE102";
    String WAE103 = "WAE103";
    String WAE104 = "WAE104";
    String WAE105 = "WAE105";

    /**
     * 菜单模块【0：设备-asset_data_management，1：设备类型-asset_category，2：角色-user_roles_config，3：组织架构-organization_management，
     * 4：用户-user_config，5：设备型号-asset_unit_type，6：维护配置-cloud_config，7：任务模板-task_template，8：任务项-work_task，9：设备位置-asset_address，
     * 10：工作流，11厂商管理-org_customer，12备件管理-bom，13工单模板-metadata_work，14工单列表-work_sheet,15工单调度-work_schedule,16代办列表-todo_list，
     * 17:维保计划-plan_schedule,18:设备地图-asset_map,19:维保行事历-schedule_calendar,20:统计配置列表-statistic_config,21:生产作业单-production_order,
     * 22:日常报告-daily_report,23:消息管理-message,24:库房管理-warehouse_manage,25:个人中心,26:修改密码,27:报表信息,28:平台多语言配置,29:企业多语言配置,30:设备出入库,31:备件出入库,32:知识库,33:工单模板导入,34:客户排水卡管理,35:缴费信息  】
     */
    String[] MENUS = new String[]{"asset_data_management", "asset_category", "user_roles_config", "organization_management",
            "user_config", "asset_unit_type", "cloud_config", "task_template", "work_task", "asset_address", "work_flow",
            "org_customer", "bom", "metadata_work", "work_sheet", "work_schedule", "todo_list", "plan_schedule",
            "asset_map", "schedule_calendar", "statistic_config", "production_order", "daily_report", "message", "warehouse_manage",
            "person_center", "change_password", "report_info", "language", "self_language", "asset_io", "bom_io", "knowledge_base",
            "import_work_template", "drainage_card_management", "cost_compute"};
    /**
     * 权限类型（必须要和PRM_TYPE_INFO保持一致）
     * 【0：新增-add，1：编辑-edit，2：详情-detail，3：导入-import，4：导出-export，5：导出二维码-BR_export，6：删除-delete，7：费用显示-fee_show，8：下载-download，9：分配-distribution，10：pdf】
     */
    String[] PRM_TYPES = new String[]{"add", "edit", "detail", "import", "export", "BR_export", "delete", "fee_show", "download", "distribution", "pdf"};
    /**
     * 菜单页签权限类型（）
     * 【0：设备详情故障履历-asset_fault_record，1：设备附件文档-asset_file，2：设备详情设备日志-asset_log，
     * 3：设备型号故障履历-asset_model_fault_record，4：设备型号附件文档-asset_model_file，5：厂商管理附件文档，6：厂商管理联系人，7：设备子页面 】
     */
    String[] MENU_TAG_TYPES = new String[]{"asset_data_management_falut_tag", "asset_data_management_attach_tag", "asset_data_management_log_tag", "asset_model_fault_record",
            "asset_model_file", "facilities_file", "asset_model_file", "asset_data_management_child_tag"};

    /**
     * 工单特殊权限类型（必须要和WORK_URL_PRM保持一致）
     * 【0：提交-/worksSubmit，1：作废-/cancelWorks】
     */
    String[] WORK_PRM_TYPES = new String[]{"/worksSubmit", "/cancelWorks"};

    /**
     * 工单业务特殊权限类型（必须要和WORK_URL_PRM保持一致）
     * 【0：提交-Asset，1：作废-Bom】
     */
    String[] WORK_BUSINESS_PRM_TYPES = new String[]{"Asset", "Bom"};

    /**
     * 工单特殊地址（必须要和WORK_URL_PRM保持一致）
     * 【0：提交-/worksSubmitAsset，1：提交-/worksSubmitBom，2：作废-/cancelWorksAsset，3：作废-/cancelWorksBom】
     */
    String[] WORK_URIS = new String[]{WORK_PRM_TYPES[0] + WORK_BUSINESS_PRM_TYPES[0], WORK_PRM_TYPES[0] + WORK_BUSINESS_PRM_TYPES[1], WORK_PRM_TYPES[1] + WORK_BUSINESS_PRM_TYPES[0], WORK_PRM_TYPES[1] + WORK_BUSINESS_PRM_TYPES[1]};

    /**
     * 工单特殊权限类型（必须要和WORK_URL_PRM保持一致）
     * 【0：提交-/worksSubmit，1：作废-/cancelWorks】
     */
    Map<String, String[]> WORK_URL_PRM = new HashMap<String, String[]>() {
        {
            put(WORK_PRM_TYPES[0], new String[]{WORK_PRM_TYPES[0], WORK_URIS[0], WORK_URIS[1]}); // 提交/worksSubmit
            put(WORK_PRM_TYPES[1], new String[]{WORK_PRM_TYPES[1], WORK_URIS[2], WORK_URIS[3]}); // 作废/cancelWorks
        }
    };


    /**
     * 页面权限类型（必须要和PRM_TYPES保持一致）
     * 【0：新增-add-isAdd，1：编辑-edit-isEdit，2：详情-detail-isDetail，3：导入-import-isImport，4：导出-export-isExport，
     * 5：导出二维码-BR_export-isBRExport，6：删除-delete-isDelete，7：费用显示-fee_show-isFeeShow，8：下载-download-isDownload，9：，10：】
     */
    Map<String, String> PRM_TYPE_INFO = new HashMap<String, String>() {
        {
            put(PRM_TYPES[0], "isAdd"); // 新增
            put(PRM_TYPES[1], "isEdit"); // 编辑
            put(PRM_TYPES[2], "isDetail"); // 详情
            put(PRM_TYPES[3], "isImport"); // 导入
            put(PRM_TYPES[4], "isExport"); // 导出
            put(PRM_TYPES[5], "isBRExport"); // 导出二维码
            put(PRM_TYPES[6], "isDelete"); // 删除
            put(PRM_TYPES[7], "isFeeShow"); // 费用显示
            put(PRM_TYPES[8], "isDownload"); // 下载
            put(PRM_TYPES[9], "isDistribution"); //分配
            put(PRM_TYPES[10], "isPdf"); //pdf
        }
    };

    Map<String, String> URL_PRM = new HashMap<String, String>() {
        {
            //设备
            put("/getAssetList", MENUS[0]);
            put("/deleteSelectAsset", MENUS[0] + "_" + PRM_TYPES[6]);
            put("/showAssetQRCode", MENUS[0] + "_" + PRM_TYPES[5]);
            put("/exportAssetQRCode", MENUS[0] + "_" + PRM_TYPES[5]);
            put("/exportAllAssetQRCode", MENUS[0] + "_" + PRM_TYPES[5]);
            put("/getImportAssetTemplateCode", MENUS[0] + "_" + PRM_TYPES[3]);
            put("/getExportAssetCode", MENUS[0] + "_" + PRM_TYPES[4]);
            put("/getExportAllAssetCode", MENUS[0] + "_" + PRM_TYPES[4]);
            put("/addAsset", MENUS[0] + "_" + PRM_TYPES[0]);
            put("/editAsset", MENUS[0] + "_" + PRM_TYPES[1]);
            put("/getAssetInfo", MENUS[0] + "_" + PRM_TYPES[2]);
            put("/getAssetInfoByCode", MENUS[0] + "_" + PRM_TYPES[2]);
            put("/searchAssetIdByCode", MENUS[0] + "_" + PRM_TYPES[2]);
            put("/getAssetWorkInfo", MENUS[0] + "_" + PRM_TYPES[2]);
            put("/getAssetHistoryWork", MENUS[0] + "_" + PRM_TYPES[2]);
            put("/getAssetFileList", MENU_TAG_TYPES[1]);
            put("/removeAssetFile", MENU_TAG_TYPES[1] + "_" + PRM_TYPES[6]);
            put("/addAssetFile", MENU_TAG_TYPES[1] + "_" + PRM_TYPES[0]);
            put("/getAssetParentList", MENU_TAG_TYPES[7]);
            put("/addAssetParent", MENU_TAG_TYPES[7] + "_" + PRM_TYPES[0]);
            put("/removeAssetParent", MENU_TAG_TYPES[7] + "_" + PRM_TYPES[6]);


            //设备类型
            put("/searchAssetCategoryList", MENUS[1]);
            put("/addAssetCategory", MENUS[1]);
            put("/editAssetCategory", MENUS[1]);
            put("/removeAssetCategory", MENUS[1]);
            put("/searchAssetCategoryInfo", MENUS[1]);
            put("/searchAssetCategoryTreeList", MENUS[1]);
            //角色
            put("/searchRoleList", MENUS[2]);
            put("/addRole", MENUS[2]);
            put("/removeRoleById", MENUS[2]);
            put("/editRoleById", MENUS[2]);
            put("/editPermissionRole", MENUS[2]);
            put("/searchRoleInfo", MENUS[2]);
            put("/searchRolePermission", MENUS[2]);
            put("/searchRoleAssetPositionPermission", MENUS[2]);
            put("/searchRoleAssetTypePermission", MENUS[2]);
            put("/searchRoleWorkTypePermission", MENUS[2]);
            //部门岗位
            put("/searchGroupList", MENUS[3]);
            put("/addGroup", MENUS[3]);
            put("/addPosition", MENUS[3]);
            put("/addGroupRole", MENUS[3]);
            put("/addPositionRole", MENUS[3]);
            put("/editGroup", MENUS[3]);
            put("/editPosition", MENUS[3]);
            put("/removeGroup", MENUS[3]);
            put("/removeGroupRole", MENUS[3]);
            put("/removePositionRole", MENUS[3]);
            put("/removePosition", MENUS[3]);
            put("/searchGroupInfo", MENUS[3]);
            put("/searchSelectGroupList", MENUS[3]);
            put("/searchPositionInfo", MENUS[3]);
            put("/searchGroupRoleList", MENUS[3]);
            put("/searchPositionRoleList", MENUS[3]);
            put("/searchAddGroupRoleList", MENUS[3]);
            put("/searchAddPositionRoleList", MENUS[3]);
            //用户管理
            put("/searchUserListPage", MENUS[4]);
            put("/addUser", MENUS[4]);
            put("/removeUser", MENUS[4]);
            put("/editUser", MENUS[4]);
            put("/changeUserPassword", MENUS[4]);

            //设备型号管理
            put("/searchAssetModelListPage", MENUS[5]);
            put("/addAssetModel", MENUS[5]);
            put("/editAssetModel", MENUS[5]);
            put("/removeAssetModel", MENUS[5]);
            put("/searchAssetModelInfo", MENUS[5]);
            put("/searchAssetModelDocPage", MENUS[5]);
            put("/removeAssetModelDoc", MENUS[5]);
            put("/addAssetModelDoc", MENUS[5]);

            //系统维护配置
            put("/searchSystemConfigList", MENUS[6]);
            put("/addSystemConfig", MENUS[6]);
            put("/editSystemConfig", MENUS[6]);
            put("/removeSystemConfig", MENUS[6]);

            //任务模板
            put("/searchTaskTempLatePage", MENUS[7]);
            put("/addTaskTempLate", MENUS[7]);
            put("/editTaskTempLate", MENUS[7]);
            put("/removeTaskTempLate", MENUS[7]);
            put("/searchTaskTempLateInfo", MENUS[7]);
            put("/addTaskTempLateItem", MENUS[7]);
            put("/removeTaskTempLateItem", MENUS[7]);
            put("/editTaskTempLateItem", MENUS[7]);
            put("/moveUpTaskTempLateItem", MENUS[7]);
            put("/moveDownTaskTempLateItem", MENUS[7]);
            put("/searchTaskTempLateItemList", MENUS[7]);

            //任务项
            put("/searchTaskItemPage", MENUS[8]);
            put("/addTaskItem", MENUS[8]);
            put("/editTaskItem", MENUS[8]);
            put("/removeTaskItem", MENUS[8]);
            put("/searchTaskItemInfo", MENUS[8]);
            put("/searchChoseTaskItemList", MENUS[8]);
            put("/removeTaskItemFile", MENUS[8]);
            put("/addTaskItemFile", MENUS[8]);
            put("/searchTaskItemFileList", MENUS[8]);

            // 工作流
            put("/searchWorkflowPermission", MENUS[10]);
            put("/searchWorkflowDeploymentList", MENUS[10]);
            put("/deleteWorkflowDeployment", MENUS[10]);
            put("/searchWorkflowDefinitionList", MENUS[10]);
            put("/showWorkflowProcessDiagram", MENUS[10]);
            put("/searchWorkflowInstanceList", MENUS[10]);
            put("/deleteWorkflowInstance", MENUS[10]);
            put("/deleteWorkflowInstanceFilterFlowName", MENUS[10]);
            put("/showWorkflowInstanceDiagram", MENUS[10]);

            //厂商管理
            put("/addFacilities", MENUS[11] + "_" + PRM_TYPES[0]);
            put("/editFacilities", MENUS[11] + "_" + PRM_TYPES[1]);
            put("/changeUseFacilities", MENUS[11] + "_" + PRM_TYPES[1]);
            put("/removeFacilities", MENUS[11] + "_" + PRM_TYPES[6]);
            put("/searchFacilitiesInfo", MENUS[11] + "_" + PRM_TYPES[2]);
            put("/searchFacilitiesList", MENUS[11]);

            //备件管理
            put("/searchBomList", MENUS[12]);
            put("/addBom", MENUS[12]);
            put("/searchBomInfo", MENUS[12]);
            put("/searchBomStockInfo", MENUS[12]);
            put("/searchBomSupplierInfo", MENUS[12]);
            put("/searchBomLogList", MENUS[12]);
            put("/addBomSupplier", MENUS[12]);
            put("/editBomSupplier", MENUS[12]);
            put("/removeBomSupplier", MENUS[12]);
            put("/editUseBomSupplier", MENUS[12]);
            put("/searchBomMaterialQRCode", MENUS[12]);
            put("/editBom", MENUS[12]);
            put("/editUseBom", MENUS[12]);
            put("/removeSelectBom", MENUS[12]);
            put("/showBomQRCode", MENUS[12] + "_" + PRM_TYPES[5]);
            put("/exportBomQRCode", MENUS[12] + "_" + PRM_TYPES[5]);
            put("/exportAllBomQRCode", MENUS[12] + "_" + PRM_TYPES[5]);


            //流程配置模板
            put("/searchWorkflowProcessDiagram", MENUS[13]);
            put("/searchWorkFlowTemplateInfo", MENUS[13]);
            put("/editWorkFlowTemplate", MENUS[13]);
            put("/searchWorkFlowTemplateColumnList", MENUS[13]);
            put("/searchWorkFlowColumnListForAdd", MENUS[13]);
            put("/addWorkFlowTemplateColumn", MENUS[13]);
            put("/editWorkFlowColumnUse", MENUS[13]);
            put("/editWorkFlowTemplateColumn", MENUS[13]);
            put("/editWorkFlowTemplateHandle", MENUS[13]);
            put("/removeSelectWorkFlowColumn", MENUS[13]);
            put("/searchWorkFlowColumnInfo", MENUS[13]);
            put("/searchWorkFlowNodeTemplateInfo", MENUS[13]);
            put("/searchWorkFlowStartNodeTemplateInfo", MENUS[13]);
            put("/searchWorkFlowTemplateListForClone", MENUS[13]);
            put("/addWorkFlowNodeClone", MENUS[13]);
            //工单
            put("/searchWorksList", MENUS[14]);

            put(WORK_PRM_TYPES[0], MENUS[14] + "_" + PRM_TYPES[1]); // /worksSubmit
            put(WORK_URIS[0], MENUS[30] + "_" + PRM_TYPES[1]); // /worksSubmitAsset
            put(WORK_URIS[1], MENUS[31] + "_" + PRM_TYPES[1]); // /worksSubmitBom

            put(WORK_PRM_TYPES[1], MENUS[14] + "_" + PRM_TYPES[6]); // /cancelWorks
            put(WORK_URIS[2], MENUS[30] + "_" + PRM_TYPES[6]); // /cancelWorksAsset
            put(WORK_URIS[3], MENUS[31] + "_" + PRM_TYPES[6]); // /cancelWorksBom

            put("/exportWorksPdf", MENUS[14] + "_" + PRM_TYPES[10]);


            //待办列表
            put("/tasks", SensConstant.MENUS[16]);
            //维保计划
//            put("/searchAssetPositionTree", MENUS[17]);
            put("/addPlanSchedule", MENUS[17] + "_" + PRM_TYPES[0]);
            put("/updatePlanSchedule", MENUS[17] + "_" + PRM_TYPES[1]);
            put("/searchPlanScheduleInfo", MENUS[17]);
            put("/searchPlanScheduleList", MENUS[17]);
            put("/editPlanScheduleUse", MENUS[17] + "_" + PRM_TYPES[1]);
            put("/removeSelectPlanSchedule", MENUS[17] + "_" + PRM_TYPES[6]);
            put("/searchPlanWorkAssetByPositionCodeAndAssetType", MENUS[16]);
            put("/searchPlanWorkTaskTemplateList", MENUS[17]);
            put("/searchPlanWorkTaskTemplateItemList", MENUS[17]);
            //put("/searchPlanWorkTaskItemList", MENUS[17]);

            //统计分析配置
            put("/searchStatisticConfigList", MENUS[20]);
            put("/addStatistic", MENUS[20] + "_" + PRM_TYPES[0]);
            put("/editStatistic", MENUS[20] + "_" + PRM_TYPES[1]);
            put("/searchStatisticInfo", MENUS[20]);
            put("/removeSelectStatistic", MENUS[20] + "_" + PRM_TYPES[6]);

            //生产作业单
            put("/searchProductTaskPage", MENUS[21]);
            put("/searchProductTaskInfo", MENUS[21]);
            put("/searchProductTaskWellInfo", MENUS[21]);
            put("/searchProductTaskWellItemInfo", MENUS[21]);
            put("/editProductTask", MENUS[21] + "_" + PRM_TYPES[1]);
            put("/editProductTaskWell", MENUS[21] + "_" + PRM_TYPES[1]);
            put("/editProductTaskWellItem", MENUS[20] + "_" + PRM_TYPES[1]);
            put("/removeProductTask", MENUS[21] + "_" + PRM_TYPES[6]);
            put("/removeProductTaskWell", MENUS[21] + "_" + PRM_TYPES[6]);
            put("/removeProductTaskWellItem", MENUS[21] + "_" + PRM_TYPES[6]);

            //报表信息
            put("/searchStatisticList", MENUS[27]);
            put("/searchStatisticInfo", MENUS[27]);
            put("/getStatisticsTableDataExport", MENUS[27] + "_" + PRM_TYPES[4]);

            //个人中心
            put("/searchUserInfo", MENUS[25]);

            //修改密码
            put("/changeUserPassword", MENUS[26]);

            //消息管理
            put("/searchMessagePageByReceiver", MENUS[23]);
            put("/searchNoReadCountByReceiver", MENUS[23]);
            put("/searchNoReadThreeCountByReceiver", MENUS[23]);
            put("/searchMessageInfoById", MENUS[23]);
            put("/editMessageReceiverRead", MENUS[23]);

        }
    };

    /**
     * 业务类型编号
     * 1***:工单类型
     * 2***:设备
     * 3***:备件
     * 4***:动态流程页面(以后可能会删除)
     * 5***:报表报告
     * 6***:服务资源
     * 7***:业务配置
     * 8***:系统管理
     * 9***:其它
     * 10***:工单管理
     */
    String BUSINESS_NO_1001 = "1001"; // 维修
    String BUSINESS_NO_1002 = "1002"; // 保养
    String BUSINESS_NO_1003 = "1003"; // 巡检
    String BUSINESS_NO_1004 = "1004"; // 点检
    String BUSINESS_NO_1005 = "1005"; // 安装
    String BUSINESS_NO_1006 = "1006"; // 资本性项目
    String BUSINESS_NO_1007 = "1007"; // 改进性维护
    String BUSINESS_NO_2000 = "2000"; // 设备
    String BUSINESS_NO_2001 = "2001"; // 设备验收
    String BUSINESS_NO_2002 = "2002"; // 设备调拨
    String BUSINESS_NO_2003 = "2003"; // 设备报废
    String BUSINESS_NO_2004 = "2004"; // 设备盘点
    String BUSINESS_NO_2005 = "2005"; // 设备出库
    String BUSINESS_NO_2006 = "2006"; // 设备入库
    String BUSINESS_NO_2007 = "2007"; // 设备导入
    String BUSINESS_NO_2008 = "2008"; // 设备备件导入
    String BUSINESS_NO_3000 = "3000"; // 备件
    String BUSINESS_NO_3001 = "3001"; // 备件入库
    String BUSINESS_NO_3002 = "3002"; // 备件调拨
    String BUSINESS_NO_3003 = "3003"; // 备件领用
    String BUSINESS_NO_3004 = "3004"; // 备件报废
    String BUSINESS_NO_3005 = "3005"; // 备件盘点
    String BUSINESS_NO_3006 = "3006"; // 备件清单
    String BUSINESS_NO_3007 = "3007"; // 备件销账
    String BUSINESS_NO_3008 = "3008"; // 备件采购需求
    String BUSINESS_NO_3009 = "3009"; // 备件导入
    String BUSINESS_NO_3100 = "3100"; // 库房
    String BUSINESS_NO_4001 = "4001"; // 工作任务
    String BUSINESS_NO_4002 = "4002"; // 我要巡检
    String BUSINESS_NO_4003 = "4003"; // 我要点检
    String BUSINESS_NO_4004 = "4004"; // 用户添加
    String BUSINESS_NO_4005 = "4005"; // 考核计划
    String BUSINESS_NO_4006 = "4006"; // 用户注册
    String BUSINESS_NO_4007 = "4007"; // 用户上下班1
    String BUSINESS_NO_5001 = "5001"; // 用户看板
    String BUSINESS_NO_5002 = "5002"; // 日常报告
    String BUSINESS_NO_6001 = "6001"; // 厂商管理
    String BUSINESS_NO_6002 = "6002"; // 客户管理
    String BUSINESS_NO_6003 = "6003"; // 抄表计量
    String BUSINESS_NO_6004 = "6004"; // 缴费信息
    String BUSINESS_NO_6005 = "6005"; // 缴费信息导入
    String BUSINESS_NO_7001 = "7001"; // 设备类型
    String BUSINESS_NO_7002 = "7002"; // 维护配置
    String BUSINESS_NO_7003 = "7003"; // 任务模板
    String BUSINESS_NO_7004 = "7004"; // 任务项
    String BUSINESS_NO_7005 = "7005"; // 设备位置
    String BUSINESS_NO_7006 = "7006"; // 角色管理
    String BUSINESS_NO_7007 = "7007"; // 部门
    String BUSINESS_NO_7008 = "7008"; // 用户管理
    String BUSINESS_NO_7009 = "7009"; // 设备型号
    String BUSINESS_NO_7010 = "7010"; // 数据字典配置
    String BUSINESS_NO_7011 = "7011"; // 消息管理
    String BUSINESS_NO_7012 = "7012"; // 用户导入
    String BUSINESS_NO_7013 = "7013"; // 职安健
    String BUSINESS_NO_7014 = "7014"; // 任务项导入
    String BUSINESS_NO_8001 = "8001"; // 系统配置
    String BUSINESS_NO_8002 = "8002"; // 工单模板
    String BUSINESS_NO_8003 = "8003"; // 工作流
    String BUSINESS_NO_8005 = "8005"; // 统计配置
    String BUSINESS_NO_8006 = "8006"; // 工单模板导入
    String BUSINESS_NO_9000 = "9000"; // 其他
    String BUSINESS_NO_10001 = "10001"; // 工单管理
    String BUSINESS_NO_10002 = "10002"; // 维保计划
    String BUSINESS_NO_10003 = "10003"; // 生产作业
    String BUSINESS_NO_10004 = "10004"; // 生产作业导入
    String BUSINESS_NO_10005 = "10005"; // 维保行事历导入
    String BUSINESS_NO_10006 = "10006"; // 计划行事历
    String BUSINESS_NO_11007 = "11007"; // 平台多语言配置
    String BUSINESS_NO_11008 = "11008"; // 企业多语言配置
    String BUSINESS_NO_PROCESS = "_process"; //工单流转记录拼接字符串用于log_type
    String BUSINESS_NO_1100 = "1100"; // 试验工单

    // 对象类型编号
    String RELATION_TYPE_ASSET = "2";
    String RELATION_TYPE_AREA = "3";

    //仅验证功能权限的业务类型和菜单对应关系 无关数据权限
    Map<String, String> BUSINESS_NO_AND_MENU_NO_DATA = new HashMap<String, String>() {
        {
            put(BUSINESS_NO_2000, MENUS[0]);
            put(BUSINESS_NO_2007, MENUS[0]);
            put(BUSINESS_NO_3000, MENUS[12]);
            put(BUSINESS_NO_3009, MENUS[12]);
            put(BUSINESS_NO_5001, MENUS[27]);
            put(BUSINESS_NO_5002, MENUS[22]);
            put(BUSINESS_NO_8003, MENUS[10]);
            put(BUSINESS_NO_6001, MENUS[11]);
            put(BUSINESS_NO_6002, MENUS[34]);
            put(BUSINESS_NO_6005, MENUS[35]);
            put(BUSINESS_NO_7003, MENUS[7]);
            put(BUSINESS_NO_7004, MENUS[8]);
            put(BUSINESS_NO_7009, MENUS[5]);
            put(BUSINESS_NO_7012, MENUS[4]);
            put(BUSINESS_NO_7014, MENUS[8]);
            put(BUSINESS_NO_8006, MENUS[13]);
            put(BUSINESS_NO_10001, MENUS[14]);
            put(BUSINESS_NO_10005, MENUS[19]);
        }
    };
    String LOG_SPLIT = "::";
    String LOG_SPLIT_LANG = "@@lg";
    // 日志标志
    Map<String, String> LOG_TYPE_INFO = new HashMap<String, String>() {
        {
            put(BUSINESS_NO_1001, "repair");
            put(BUSINESS_NO_1002, "maintain");
            put(BUSINESS_NO_1003, "work_Inspection");
            put(BUSINESS_NO_1004, "work_spot");
            put(BUSINESS_NO_1005, "install");
            put(BUSINESS_NO_2001, "assetInStock");
            put(BUSINESS_NO_2002, "assetAllot");
            put(BUSINESS_NO_2003, "assetDiscard");
            put(BUSINESS_NO_2004, "assetInventory");
            put(BUSINESS_NO_3000, "bomManage");
            put(BUSINESS_NO_3001, "bomInStock");
            put(BUSINESS_NO_3002, "bomAllot");
            put(BUSINESS_NO_3003, "bomRecipient");
            put(BUSINESS_NO_3004, "bomDiscard");
            put(BUSINESS_NO_3005, "bomInventory");
            put(BUSINESS_NO_3006, "bomStockList");
            put(BUSINESS_NO_4001, "workTask");
            put(BUSINESS_NO_4002, "work_Inspection");
            put(BUSINESS_NO_4003, "work_spot");
            put(BUSINESS_NO_4004, "userClient");
            put(BUSINESS_NO_9000, "other");
            put(BUSINESS_NO_4005, "performancePlan");
        }
    };
//    // 日志标志名称
//    public final static Map<String, String> LOG_TYPE_NAME = new HashMap<String, String>() {
//        {
//            put(BUSINESS_NO_1001, LangConstant.BTN_REPAIR_A);//维修
//            put(BUSINESS_NO_1002, LangConstant.BTN_CBM_A);//保养
//            put(BUSINESS_NO_1003, LangConstant.BTN_INSPECTION_A);//巡检
//            put(BUSINESS_NO_1004, LangConstant.BTN_POINT_A);// 点检
//            put(BUSINESS_NO_1005, LangConstant.TITLE_AAAAB_C);//安装
//            put(BUSINESS_NO_2001, LangConstant.TITLE_ASSET_AB_D);//设备入库
//            put(BUSINESS_NO_2002, LangConstant.TITLE_ASSET_AB_G);//设备调拨
//            put(BUSINESS_NO_2003, LangConstant.TITLE_ASSET_AD);//设备报废
//            put(BUSINESS_NO_2004, LangConstant.TITLE_ASSET_AB_A);//设备盘点
//            put(BUSINESS_NO_3000, LangConstant.TITLE_AAAAY);//备件
//            put(BUSINESS_NO_3001, LangConstant.TITLE_ABBA_I);//备件入库
//            put(BUSINESS_NO_3002, LangConstant.TITLE_ABBA_O);//备件调拨
//            put(BUSINESS_NO_3003, LangConstant.TITLE_ABBA_D);//备件领用
//            put(BUSINESS_NO_3004, LangConstant.TITLE_ABAA_U);//备件报废
//            put(BUSINESS_NO_3005, LangConstant.TITLE_ABBA_F);//备件盘点
//            put(BUSINESS_NO_3006, LangConstant.TITLE_ABBA_H);//备件清单
//            put(BUSINESS_NO_4001, LangConstant.TITLE_BAAAB_Z);//工作任务
//            put(BUSINESS_NO_4002, LangConstant.TITLE_AAAD_F);//我要巡检
//            put(BUSINESS_NO_4003, LangConstant.TITLE_AAAD_E);//我要点检
//            put(BUSINESS_NO_9000, LangConstant.TITLE_AAM_Y);//其他
//            put(BUSINESS_NO_4005, LangConstant.TITLE_AK_P);//考核计划
//        }
//    };

    // 消息查询表
    Map<String, String> TABLE_TYPE_INFO = new HashMap<String, String>() {
        {
            put(BUSINESS_NO_1001, "@._sc_works w ,@._sc_works_detail wd");
            put(BUSINESS_NO_1002, "@._sc_works w ,@._sc_works_detail wd");
            put(BUSINESS_NO_1003, "@._sc_works w ,@._sc_works_detail wd");
            put(BUSINESS_NO_1004, "@._sc_works w ,@._sc_works_detail wd");
            put(BUSINESS_NO_2002, "@._sc_asset_allot");
            put(BUSINESS_NO_2003, "@._sc_asset_discard");
            put(BUSINESS_NO_2004, "@._sc_asset_inventory");
            put(BUSINESS_NO_3001, "@._sc_bom_in_stock bs,@._sc_stock_group gp");
            put(BUSINESS_NO_3002, "@._sc_bom_allot bs,@._sc_stock_group gp");
            put(BUSINESS_NO_3003, "@._sc_bom_recipient bs,@._sc_stock_group gp");
            put(BUSINESS_NO_3004, "@._sc_bom_discard bs,@._sc_stock_group gp");
        }
    };
    // 消息查询字段表
    Map<String, String> TABLE_COLUMN_TYPE_INFO = new HashMap<String, String>() {
        {
            put(BUSINESS_NO_1001, "w.facility_id,w.position_code");
            put(BUSINESS_NO_1002, "w.facility_id,w.position_code");
            put(BUSINESS_NO_1003, "w.facility_id,w.position_code");
            put(BUSINESS_NO_1004, "w.facility_id,w.position_code");
            put(BUSINESS_NO_2002, "position_id as position_code");
            put(BUSINESS_NO_2003, "position_id as position_code");
            put(BUSINESS_NO_2004, "position_id as position_code");
            put(BUSINESS_NO_3001, "gp group_id ");
            put(BUSINESS_NO_3002, "gp group_id");
            put(BUSINESS_NO_3003, "gp group_id");
            put(BUSINESS_NO_3004, "gp group_id");
        }
    };
    // 消息查询字段表
    Map<String, String> TABLE_CONDITION_TYPE_INFO = new HashMap<String, String>() {
        {
            put(BUSINESS_NO_1001, "w.work_code=wd.work_code and wd.sub_work_code");
            put(BUSINESS_NO_1002, "w.work_code=wd.work_code and wd.sub_work_code");
            put(BUSINESS_NO_1003, "w.work_code=wd.work_code and wd.sub_work_code");
            put(BUSINESS_NO_1004, "w.work_code=wd.work_code and wd.sub_work_code");
            put(BUSINESS_NO_2002, "position_id");
            put(BUSINESS_NO_2003, "position_id");
            put(BUSINESS_NO_2004, "position_id");
            put(BUSINESS_NO_3001, "gp.stock_code=bs.stock_code and stock_code ");
            put(BUSINESS_NO_3002, "gp.stock_code=bs.stock_code and stock_code ");
            put(BUSINESS_NO_3003, "gp.stock_code=bs.stock_code and stock_code ");
            put(BUSINESS_NO_3004, "gp.stock_code=bs.stock_code and stock_code ");
        }
    };

    //int appid = 1400054314;
    //String appkey = "f6bb4cf8dc5fded1d4aa8ca66082fb8b";
    /**
     * 尊敬的用户，您好！您的{1}报修平台账户已开通（微信搜索小程序“{2}”），账户：{3}，密码：{4}，请您及时登录修改密码。
     */
    String SMS_10001001 = "10001001";
    /**
     * 您的验证码为{1}，请于{2}分钟内填写。为保障您的账号安全，请勿将验证码短信转发给他人。
     */
    String SMS_10001002 = "10001002";
    /**
     * 尊敬的用户，您好！您的{1}报修平台账户已开通（微信搜索小程序“{2}”），账户：{3}，欢迎使用。
     */
    String SMS_10001003 = "10001003";

    // 备件/工作任务/客户用户等申请单-start
    /**
     * 备件名称：{1}，备件编码：{2}，库房：{3}，当前库存小于安全库存，请及时安排补充。
     */
    String SMS_10002001 = "10002001";
    /**
     * {1}申请{2}，请您审核。
     */
    String SMS_10002002 = "10002002";
    /**
     * {1}请您尽快完成{2}。
     */
    String SMS_10002003 = "10002003";
    /**
     * {1}拒绝了{2}，请您知悉。
     */
    String SMS_10002004 = "10002004";
    /**
     * {1}的{2}申请单，审核已通过，请您知悉。
     */
    String SMS_10002005 = "10002005";
    /**
     * {1}的{2}申请单，审核已退回，请您知悉。
     */
    String SMS_10002006 = "10002006";
    /**
     * {1}请您尽快审核{2}。
     */
    String SMS_10002007 = "10002007";
    /**
     * 您的工作任务{1}，已被取消，请您知悉。
     */
    String SMS_10002009 = "10002009";
    // 备件/工作任务/客户用户等申请单-end

    // 服务请求/工单-start
    /**
     * {1}请您尽快确认服务请求：{2}。
     */
    String SMS_10003001 = "10003001";
    /**
     * {1}请您尽快分配人员确认服务请求：{2}。
     */
    String SMS_10003002 = "10003002";
    /**
     * 本次服务请求已完成：{1}，请您知悉。
     */
    String SMS_10003003 = "10003003";
    /**
     * {1}关闭了服务请求：{2}，请您知悉。
     */
    String SMS_10003004 = "10003004";
    /**
     * {1}请您尽快分配人员{2}：{3}。
     */
    String SMS_10003005 = "10003005";
    /**
     * {1}请您尽快分配人员确认{2}工单：{3}。
     */
    String SMS_10003006 = "10003006";
    /**
     * {1}请您尽快完成{2}工单：{3}。
     */
    String SMS_10003007 = "10003007";
    /**
     * {1}请您尽快确认{2}工单：{3}。
     */
    String SMS_10003008 = "10003008";
    /**
     * {1}拒绝了{2}工单：{3}，请您重新处理。
     */
    String SMS_10003009 = "10003009";
    /**
     * {1}完成了{2}工单：{3}，请您尽快确认。
     */
    String SMS_10003010 = "10003010";
    /**
     * {1}确认通过了{2}工单：{3}，请您知悉。
     */
    String SMS_10003011 = "10003011";
    /**
     * {1}退回了{2}工单：{3}，请您重新处理。
     */
    String SMS_10003012 = "10003012";
    /**
     * 您的{1}工单：{2}，还有{3}就会超时，请加紧完成。
     */
    String SMS_10003013 = "10003013";
    /**
     * 您的{1}工单：{2}，已超时{3}，请知悉。
     */
    String SMS_10003014 = "10003014";
    /**
     * {1}负责的{2}工单：{3}，已超时{4}，请您知悉。
     */
    String SMS_10003015 = "10003015";
    /**
     * {1}关闭了工单：{2}，请您知悉。
     */
    String SMS_10003016 = "10003016";
    /**
     * {1}提交了{2}申请，{3}正在处理，请您知晓。
     */
    String SMS_10004003 = "10004003";
    // 服务请求/工单-end

    // 设备监控-start
    /**
     * 设备名称：{1}，设备编码：{2}，位置：{3}，监控项：{4}，有{5}报警，请及时安排查看。
     */
    String SMS_10004001 = "10004001";
    /**
     * 设备名称：{1}，设备编码：{2}，位置：{3}，监控项：{4}，有{5}异常，请及时安排查看。
     */
    String SMS_10004002 = "10004002";
    /**
     * 排污口名称：{1}，排污口编码：{2}，片区：{3}，排污有异常，请及时安排查看。
     */
    String SMS_10004004 = "10004004";
    // 设备监控-end
}
