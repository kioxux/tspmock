package com.gengyun.senscloud.common;

/**
 * 启用状态枚举类
 */
public enum IsuseEnum {
    ENABLE("1","启用"),
    DISABLE("0","禁用");

    private String key;
    private String value;
    private IsuseEnum(String key,String value){
        this.key = key;
        this.value = value;
    }

    public static String getValue(String key) {
        IsuseEnum[] isuseEnums = values();
        for (IsuseEnum isuseEnum : isuseEnums) {
            if (isuseEnum.key().equals(key)) {
                return isuseEnum.value();
            }
        }
        return null;
    }

    private String key() {
        return this.key;
    }

    private String value() {
        return this.value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
