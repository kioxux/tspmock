package com.gengyun.senscloud.common;

/**
 * 功能：按钮状态关键词
 * Created by Dong wudang on 2018/12/14.
 */
public interface ButtonConstant {

    public final static int BTN_DEFAULT = 0;//-----------------------0：按钮未设定
    public final static int BTN_SAVE = 1;//-----------------------1：保存
    public final static int BTN_SUBMIT = 2;//---------------------2：提交
    public final static int BTN_DISTRIBUTION = 3;//---------------3：分配
    public final static int BTN_AGREE = 4;//----------------------4：同意
    public final static int BTN_RETURN = 5;//---------------------5：退回
    public final static int BTN_OTHER = 6;//---------------------6：其它
    public final static int BTN_CLOSE = 8;//----------------------8：关闭
    public final static int BTN_CANCEL = 9;//----------------------9：取消
    public final static int BTN_NEW = 10;//----------------------10: 新建

}
