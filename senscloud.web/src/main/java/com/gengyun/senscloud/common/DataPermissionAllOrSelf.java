package com.gengyun.senscloud.common;

public enum DataPermissionAllOrSelf {
    None,
    All_Facility,
    Self_Facility
}
