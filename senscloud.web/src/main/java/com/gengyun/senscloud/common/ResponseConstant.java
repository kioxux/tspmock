package com.gengyun.senscloud.common;

/**
 * 返回页面常量
 * User: sps
 * Date: 2020/11/03
 * Time: 上午11:20
 */
public interface ResponseConstant {
    // 返回信息-start
    int SUCCESS_CODE = 200; // 正确码
    int ERROR_CODE = -1; // 错误、通用错误码
    int EXPORT_CODE = 201; // 导出文件正确码
    int ERROR_CONTENT_CODE = -2; // 错误信息输出到文件
    int ERROR_RIGHT_CODE = 401; // 权限不足
    int RE_LOGIN_CODE = 402; // 重新登录、未登录
    int ERROR_ILLEGAL_CODE = 403; // 非法操作

    String ERROR_MSG_CODE = "type"; // 错误码标识字段
    String ERROR_MSG_FILE = "fileCode"; // -2明细字段
    String ERROR_MSG_LANG_PMS = "langs"; // 错误消息拼接参数、国际化参数
    String ERROR_MSG_REMARK = "remark"; // 备注信息
    // 返回信息-end

    String DESC_ID = "数据主键"; // id说明
    String DESC_IDS = "选中数据主键"; // ids说明

    // 返回信息描述-start
    String RSP_CODES = "返回码：" + ResponseConstant.SUCCESS_CODE + "/" + ResponseConstant.EXPORT_CODE + "/" + ResponseConstant.ERROR_CODE;
    String RSP_DESC_MSG = "返回结果描述，有参数时{d}代表数据参数，其他如{0}等代表描述参数，参数参考content中langs属性";
    String RSP_DESC_CONTENT = "返回内容，参考【接口描述-返回内容】";

    String RSP_DESC_COMMON_DESC = "<br/><br/>返回内容：<br/>200：正确内容，-1：错误，401：权限不足，402：重新登录、未登录，403：非法操作";
    String RSP_DESC_COMMON_EXPORT_DESC = "<br/><br/>返回内容：<br/>201：正确内容，-1：错误，401：权限不足，402：重新登录、未登录，403：非法操作";

    String RSP_DESC_COMMON_OTHER = "<br/><br/>返回内容其他参数描述：<br/>langs：消息拼接参数，用于msg";
    String RSP_DESC_COMMON_DEMO = "<br/><br/>返回内容示例：<br/>错误有参数：{'type':-1,'langs':['设备','AS001']}<br/>错误无参数：{'type':-1}<br/>正确：";
    String RSP_DESC_COMMON = ResponseConstant.RSP_DESC_COMMON_DESC + ResponseConstant.RSP_DESC_COMMON_OTHER + ResponseConstant.RSP_DESC_COMMON_DEMO;
    String RSP_DESC_COMMON_EXPORT = ResponseConstant.RSP_DESC_COMMON_EXPORT_DESC + ResponseConstant.RSP_DESC_COMMON_OTHER + ResponseConstant.RSP_DESC_COMMON_DEMO;

    String RSP_DESC_COMMON_LIST = ResponseConstant.RSP_DESC_COMMON + "{'total':'总行数【int】','rows':'列表数据【array】'}，列表数据参考：";

    String RSP_DESC_LOGIN_DESC = "<br/><br/>返回内容：<br/>200：正确内容，-1：错误，403：非法操作";
    String RSP_DESC_LOGIN = ResponseConstant.RSP_DESC_LOGIN_DESC + ResponseConstant.RSP_DESC_COMMON_OTHER + ResponseConstant.RSP_DESC_COMMON_DEMO;
    String RSP_DESC_RIGHT_BY_MENU = "(能看到菜单就拥有本模块所有权限)";
    // 返回信息描述-end

    // 请求信息描述-start
    String REQ_DESC_COMMON = "<br/><br/>请求特殊参数：<br/>";
    // 请求信息描述-end

    // 接口描述-start
    String RSP_DESC_SEARCH_LOGIN_DATA = "获取登录页信息" + ResponseConstant.RSP_DESC_COMMON + "{'loginTitle':'登录页文字','loginLogo':'登录页logo','loginBackground':'登录页背景图地址','isNeedTechSupport':'是否需要技术支持框【boolean】','loginRsvAa':'备用信息'}";
    String RSP_DESC_CHECK_LOGIN = "根据用户名、密码信息进行登录" + ResponseConstant.RSP_DESC_LOGIN + "token";
    String RSP_DESC_SEARCH_COMPANY_LIST = "根据用户信息查询所属企业信息" + ResponseConstant.RSP_DESC_COMMON + "[{'company_name':'公司名字','id':'公司主键【long】'}]";
    String RSP_DESC_DO_LOGIN = "根据用户信息进行登录" + ResponseConstant.RSP_DESC_COMMON + "{'unionId':'','openId':'','session_key':''}";
    String RSP_DESC_NFC_LOGIN = "根据用户工号和客户企业id进行登录" + ResponseConstant.RSP_DESC_COMMON + "{'session_key':'微信小程序session_key'}";
    String RSP_DESC_GET_MENU_LIST = "根据用户信息获取菜单" + ResponseConstant.RSP_DESC_COMMON + "content:[{icon:a-icon-home//样式名,id:1038343949712904192,items:null,key://键,cloud:,lang_key:title_aat_h//多语言key,name:首页//名称,parent_id://父菜单id,type:1//类型}]";
    String RSP_DESC_SEARCH_DEFAULT_LANG_INFO = "获取用户常用语言包/系统默认语言包" + ResponseConstant.RSP_DESC_COMMON + "{'国际化类型':{'键值':'中文/英文'}}";
    String RSP_DESC_EDIT_USER_LANG = "更新用户常用语言" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_LOGIN_USER_INFO = "获取登录用户信息" + ResponseConstant.RSP_DESC_COMMON + "{'userId':'登录用户主键','account':'登录用户账号','userName':'登录用户名称','timeZoneId':'系统时区','companyName':'登录用户企业名称','bdWxCode':'登录用户绑定微信号','hasWxGzh':'登录用户是否绑定公众号','userGpName':'登录用户首个部门岗位名称','userGpName':'登录用户部门岗位列表','userTmpPhone':'登录用户手机号简写','companySystemName':'公司系统名称','companyLogo':'公司Logo地址','phoneChooseAlbum':'手机端图片上传是否支持选择手机相册'}";
    String RSP_DESC_DO_LOGOUT = "退出登录返回登录页" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEND_VERIFICATION_CODE_BY_PHONE = "根据手机号发送验证码" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHECK_VERIFICATION_CODE = "验证短信验证码并返回企业列表" + ResponseConstant.RSP_DESC_COMMON + "[{'company_name':'公司名字','_id':'公司id【long】'}]";
    String RSP_DESC_EDIT_PSD_BY_PHONE = "根据手机验证码修改密码" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEND_VERIFICATION_CODE = "发送验证码" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_PASSWORD = "修改密码" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_FOLLOW_OA_STATUS = "查询用户公众号关注状态" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_BIND_APPLET = "小程序绑定用户账号" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_UNBIND_APPLET = "小程序解绑用户账号" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ALL_CACHE = "获取所有缓存" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CLEAR_CACHE = "一键清除所有缓存" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SHOW_USER = "展示设备二维码图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EXPORT_USER = "导出选中设备二维码图片信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "不返回任何信息";

    /**
     * 部门岗位接口描述
     */
    String RSP_DESC_SEARCH_GROUP_LIST_PERMISSION = "获取设备模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_ADD_GROUP = "新增部门" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_GROUP = "更新部门" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_GROUP = "删除部门" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_GROUP_INFO = "查询部门详情" + ResponseConstant.RSP_DESC_COMMON + "{'id':部门id,'group_name':'部门名称','group_code':'部门编码','parent_id':父部门id,'remark':'备注','create_time':创建时间,'create_user_account':'创建人','is_out':是否委外}";
    String RSP_DESC_SEARCH_GROUP_LIST = "查询部门列表" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_POSITION = "新增岗位" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_POSITION = "更新岗位" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_POSITION = "删除岗位" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_POSITION_INFO = "查询岗位详情" + ResponseConstant.RSP_DESC_COMMON + "{'id':岗位id,'position_name':'岗位名称','group_id':部门id,'remark':备注,'create_time':创建时间,'create_user_account':创建人}";
    String RSP_DESC_EDIT_SEARCH_POSITION_ROLE_LIST = "查询岗位角列表" + ResponseConstant.RSP_DESC_COMMON + "[{'name':'企业管理员','description':'系统默认角色，不能修改','id':'角色id','isDelete':'1为可删除 2为不可删除'}]";
    String RSP_DESC_EDIT_SEARCH_ADD_POSITION_ROLE_LIST = "新增岗位角色的可选角色列表" + ResponseConstant.RSP_DESC_COMMON + "[{'name':'企业管理员','description':'系统默认角色，不能修改','id':'角色id'}]";
    String RSP_DESC_EDIT_ADD_POSITION_ROLE = "新增岗位角色" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_REMOVE_POSITION_ROLE = "删除岗位角色" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";


    /**
     * 角色管理接口描述
     */
    String RSP_DESC_SEARCH_ROLE_LIST_PERMISSION = "获取角色模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_ADD_ROLE = "新增角色" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_ROLE = "更新角色" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ROLE = "删除角色" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ROLE_INFO = "查询角色详情" + ResponseConstant.RSP_DESC_COMMON + "{'create_time':创建时间,'creater_account':创建人,'is_free':是否免费角色,'name':'角色名称','is_use':是否可用,'description':'角色描述','id':'角色id','sys_role':是否系统角色}";
    String RSP_DESC_SEARCH_ROLE_LIST = "查询角色列表" + ResponseConstant.RSP_DESC_COMMON + "{'noSysRoleList':非系统默认角色[{'create_time':创建人,'is_free':是否免费角色,'name':'角色名称','is_use':是否可用,'description':'角色描述','id':'角色id','sys_role':是否系统角色}],'sysRoleList':系统默认角色[{'create_time':创建时间,'is_free':false,'name':'企业管理员','available':true,'description':'系统默认角色，不能修改','id':'2000','sys_role':true}]}";
    String RSP_DESC_SEARCH_ROLE_PERMISSION = "查询角色功能权限" + ResponseConstant.RSP_DESC_COMMON + "[{'id':'企业id','name':'名称','key':'cloud','url':'键','root_url':null,'position_url':null,'asset_url':null,'parent_id':'父菜单','is_use':是否可用,'create_time':null,'index':顺序,'icon':'样式名','type':菜单还是功能1:菜单2:功能3:操作,'typeName':null,'tree_type':0,'isSelect':是否选中1选中2未选中,'childrens':null}]";
    String RSP_DESC_SEARCH_ROLE_ASSET_POSITION_PERMISSION = "查询角色设备位置数据权限" + ResponseConstant.RSP_DESC_COMMON + "[{'position_code':'主键位置编码','position_name':'名称演示','parent_code':所属位置,'address_id':地址,'org_id':组织id,'position_type_id':1,'location':'坐标','isSelect':2,'childrens':[]}]";
    String RSP_DESC_SEARCH_ROLE_ASSET_TYPE_PERMISSION = "查询角色设备类型数据权限" + ResponseConstant.RSP_DESC_COMMON + "[{‘id‘:设备类型id,‘category_name‘:‘类型名称‘,‘parent_id‘:父类型id,‘data_order‘:排序,‘is_use‘:启用,‘category_code‘:‘自逻辑编码‘,‘fields‘:‘规格参数，自定义字段‘,‘schema_name‘:null,‘isSelect‘:2是否选中1选中2未选中,‘childrens‘:null}]";
    String RSP_DESC_SEARCH_ROLE_WORK_TYPE_PERMISSION = "查询角色工单类型数据权限" + ResponseConstant.RSP_DESC_COMMON + "[{‘id‘:工单类型id,‘type_name‘:‘工单类型名称‘,‘flow_template_code‘:‘关联流程模板‘,‘work_template_code‘:‘工单模板‘,‘business_type_id‘:业务类型,‘data_order‘:排序,‘is_use‘:是否启用,‘create_time‘:null,‘create_user_account‘:‘创建人‘,‘isSelect‘:是否选中1选中2未选中}]";
    String RSP_DESC_SEARCH_NEW_WORK_TYPE_PERMISSION = "获取创建申请单数据权限" + ResponseConstant.RSP_DESC_COMMON + "[{‘id‘:申请单类型id,‘type_name‘:‘申请单类型名称‘,‘isSelect‘:是否选中1选中2未选中}]";
    String RSP_DESC_EDIT_PERMISSION_ROLE = "更新角色功能权限" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_DATA_PERMISSION_ROLE = "更新角色数据权限" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ROLE_STOCK_PERMISSION = "查询角色库房数据权限" + ResponseConstant.RSP_DESC_COMMON + "[{‘stock_id‘:库房编码,‘stock_name‘:‘库房名称‘,‘isSelect‘:是否选中1选中2未选中}]";
    String RSP_DESC_SEARCH_ROLE_CLONE = "克隆角色" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    /**
     * 设备类型管理接口描述
     */
    String RSP_DESC_SEARCH_ASSET_CATEGORY_LIST_PERMISSION = "获取设备类型模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_ADD_ASSET_CATEGORY = "新增设备类型" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_ASSET_CATEGORY = "更新设备类型" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_CATEGORY = "删除设备类型" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ASSET_CATEGORY_INFO = "查询设备类型详情" + ResponseConstant.RSP_DESC_COMMON + "{'is_use':是否可用,'type_name':'设备种类','category_name':'类型名称','create_time':类型创建时间,'category_code':'类型编码','parent_category_name':父类型名称,'remark':'备注'l,'type':种类id,'create_user_account':'创建人','parent_id':父类型id,'id':设备类型id,'fields'自定义字段:[{'field_code':'字段id','field_name':'显示名称','field_remark':'备注','field_source':'数据来源','field_view_type':'显示控件'}],'data_order':排序}";
    String RSP_DESC_SEARCH_ASSET_CATEGORY_LIST = "查询设备类型列表" + ResponseConstant.RSP_DESC_COMMON + "[{'is_use':是否可用,'type_name':'设备种类名称','category_name':'类型名称','create_time':'创建时间','category_code':'类型编码','parent_id':父类型id,'remark':'备注','id':类型id}]";
    String RSP_DESC_CLONE_ASSET_CATEGORY_FIELDS = "克隆设备类型自定义字段" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    /**
     * 设备型号管理接口描述
     */
    String RSP_DESC_ADD_ASSET_MODEL = "新增设备型号" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_MODEL = "删除设备型号" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_ASSET_MODEL = "编辑设备型号" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ASSET_MODEL_INFO = "获取设备型号详情" + ResponseConstant.RSP_DESC_COMMON + "{'category_name':'类型名称','model_name':'型号名称','category_id':'类型id','create_time':'创建时间','properties':[{'field_code':'字段id','field_name':'显示名称','field_remark':'备注','field_source':'数据来源','field_view_type':'显示控件','field_value':'选中值'}]}";
    String RSP_DESC_SEARCH_ASSET_MODEL_LIST_PAGE = "分页获取设备型号" + ResponseConstant.RSP_DESC_COMMON + "{'total':4,'rows':[{'category_name':'类型名称','model_name':'型号名称','category_id':'类型id','create_time':'创建时间','title':'制造商'}]}";
    String RSP_DESC_SEARCH_ASSET_MODEL_DOC_PAGE = "分页获取设备型号附件文档列表" + ResponseConstant.RSP_DESC_COMMON + "{'total':1,'rows':[{'type_name':'类型名称','file_type':'类型id','file_name':'文件名称','file_id':'文件id','remark':'备注','model_id':'设备型号id','file_category_name':'文件类别','file_size':'文件大小'}]}";
    String RSP_DESC_REMOVE_ASSET_MODEL_DOC = "删除设备型号附件文档" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_ASSET_MODEL_DOC = "新增设备型号附件文档" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_FAULT_RECORD_LIST_PAGE = "获取设备型号故障履历信息" + ResponseConstant.RSP_DESC_COMMON + "[{'work_type_id':1：工单类型id,'type_name':'设备程序故障：故障类型名称','finished_time':'2020-04-1715:39:09：完成时间','work_code':'W20200417000008：工单号','fault_type':7：故障类型id,'relation_type':2,'repair_code':'FA000026：故障代码','code_name':'开关柜门无法关合：故障名称','relation_id':'375a5dfa-8251-4025-a9b8-d07047bbb4fa'}";

    /**
     * 设备管理接口描述
     */
    String RSP_DESC_SEARCH_ASSET_LIST_PERMISSION = "获取设备模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_ASSET_LIST = "获取设备列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'id':'设备主键','asset_code':'设备编码','asset_name':'设备名称','position_name':'设备位置','model_name':'设备型号','category_name':'设备类型','supplier_name':'供应商','running_status_name':'运行状态','status_name':'资产状态','guarantee_status_name':'保固状态','guarantee_status_id':'保固状态键值','enable_time':'启用日期','importment_level_name':'重要级别','startCreateDateSearch':'开始日期-创建时间','endCreateDateSearch':'结束日期-创建时间'}]";
    String RSP_DESC_REMOVE_ASSET_BY_ID = "删除设备信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_BY_SELECT = "删除选中设备信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SHOW_ASSET_QR_CODE = "展示设备二维码图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EXPORT_ASSET_QR_CODE = "导出选中设备二维码图片信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "不返回任何信息";
    String RSP_DESC_EXPORT_ALL_ASSET_QR_CODE = "导出全部设备二维码图片信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "不返回任何信息";
    String RSP_DESC_GET_IMPORT_ASSET_TEMPLATE_CODE = "设备导入模板文件码" + ResponseConstant.RSP_DESC_COMMON + "'设备导入模板文件码'";
    String RSP_DESC_GET_EXPORT_ASSET_CODE = "导出选中设备信息（导出码）" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'设备导出码'";
    String RSP_DESC_GET_EXPORT_ALL_ASSET_CODE = "导出全部设备信息（导出码）" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'设备导出码'";
    String RSP_DESC_ADD_ASSET = "新增设备信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_ASSET = "编辑设备信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    //            + "{'asset_code':'设备编号','asset_name':'设备名称','position_code':'所在位置','asset_model_id':'设备型号','category_id':'设备类型','importment_level_id':'重要级别','enable_time':'启用日期','use_year':'设备寿命','remark':'备注','buy_date':'购买日期','supplier_id':'供应商','manufacturer_id':'制造商','tax_rate':'税率','price':'税前价格','tax_price':'税费','install_date':'安装日期','install_price':'安装价格','buy_currency_id':'购买价格货币单位','install_currency_id':'安装价格货币单位','asset_icon':'设备图片','running_status_id':'运行状态','status':'资产状态','guarantee_status_id':'保固状态','running_status_name':'运行状态名称','status_name':'资产状态名称','guarantee_status_name':'保固状态名称','position_name':'所在位置名称','model_name':'设备型号名称','category_name':'设备类型名称','importment_level_name':'重要级别名称','supplier_name':'供应商名称','manufacturer_name':'制造商名称','buy_currency':'购买价格货币单位名称','install_currency':'安装价格货币单位名称'}";
    String RSP_DESC_EDIT_ASSET_MAP = "设备地图打点" + ResponseConstant.RSP_DESC_COMMON + "{'id':'设备id','location':'坐标'}";
    String RSP_DESC_GET_ASSET_INFO = "获取设备明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'asset_code':'设备编号','asset_name':'设备名称','position_code':'所在位置','asset_model_id':'设备型号','category_id':'设备类型','importment_level_id':'重要级别','enable_time':'启用日期','use_year':'设备寿命','remark':'备注','buy_date':'购买日期','supplier_id':'供应商','manufacturer_id':'制造商','tax_rate':'税率','price':'税前价格','tax_price':'税费','install_date':'安装日期','install_price':'安装价格','buy_currency_id':'购买价格货币单位','install_currency_id':'安装价格货币单位','asset_icon':'设备图片','running_status_id':'运行状态','status':'资产状态','guarantee_status_id':'保固状态','running_status_name':'运行状态名称','status_name':'资产状态名称','guarantee_status_name':'保固状态名称','position_name':'所在位置名称','model_name':'设备型号名称','category_name':'设备类型名称','importment_level_name':'重要级别名称','supplier_name':'供应商名称','manufacturer_name':'制造商名称','buy_currency':'购买价格货币单位名称','install_currency':'安装价格货币单位名称'}";
    String RSP_DESC_GET_ASSET_INFO_BY_CODE = "获取设备明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'asset_code':'设备编号','asset_name':'设备名称','position_code':'所在位置','asset_model_id':'设备型号','category_id':'设备类型','importment_level_id':'重要级别','enable_time':'启用日期','use_year':'设备寿命','remark':'备注','buy_date':'购买日期','supplier_id':'供应商','manufacturer_id':'制造商','tax_rate':'税率','price':'税前价格','tax_price':'税费','install_date':'安装日期','install_price':'安装价格','buy_currency_id':'购买价格货币单位','install_currency_id':'安装价格货币单位','asset_icon':'设备图片','running_status_id':'运行状态','status':'资产状态','guarantee_status_id':'保固状态','running_status_name':'运行状态名称','status_name':'资产状态名称','guarantee_status_name':'保固状态名称','position_name':'所在位置名称','model_name':'设备型号名称','category_name':'设备类型名称','importment_level_name':'重要级别名称','supplier_name':'供应商名称','manufacturer_name':'制造商名称','buy_currency':'购买价格货币单位名称','install_currency':'安装价格货币单位名称'}";
    String RSP_DESC_SEARCH_ASSET_ID_BY_CODE = "根据设备编码获取设备主键" + ResponseConstant.RSP_DESC_COMMON + "'设备编号'";
    String RSP_DESC_GET_ASSET_WORK_INFO = "获取设备下一步工作信息" + ResponseConstant.RSP_DESC_COMMON + "[{'work_type_id':'工单类型id,'work_type_name':'工单类型名称','work_calendar_code':'维保计划编号','title':'维保计划名称','relation_type':'适用对象','occur_time':'发生时间','deadline_time':'截止时间','relation_id':'设备id','user_name':'执行人名称'}";
    String RSP_DESC_GET_ASSET_HISTORY_WORK = "获取设备历史工作信息" + ResponseConstant.RSP_DESC_COMMON + "[{'work_type_id':2:工单类型id,'type_name':'设备程序故障：故障类型名称','work_code':'W20200422000006：工单号','receive_account':'hz：执行人账号','fault_type_id':7：故障；类型id,'begin_time':null,：工单开始时间'relation_type':2,'repair_code':'FA000026：故障代码','code_name':'开关柜门无法关合：故障名称','relation_id':'375a5dfa-8251-4025-a9b8-d07047bbb4fa：设备id','work_type_name':'保养：工单类型名称','user_name':'黄哲：执行人名称'}";
    String RSP_DESC_GET_ASSET_FILE_LIST = "获取设备附件文档信息" + ResponseConstant.RSP_DESC_COMMON + "{'code':200,'msg':null,'content':[{'id':7085：附件id,'remark':null：备注,'is_use':true：true正常false删除,'create_time_str':'2020-04-2410:25:16：创建时间','fileName':'ea3fdb82-272a-4243-a0c2-b25ab221a0d0.jpg：文件名称','create_time':1587695116272,'fileOriginalName':'tmp_b433cd29517adb425ae3f20c7069de66e5977404f5df793a.jpg：文件原始名称','createUserAccount':'fsm1：创建人账号','fileType':1文件类型1图片2文档,'fileUrl':'aaa.jpg：文件路径','fileSize':114.0：文件大小,'fileHeight':1920：高度（如果是图片）,'fileWidth':960：宽度（如果是图片）}]}";
    String RSP_DESC_SEARCH_ASSET_LOG_LIST = "获得设备所有的日志" + ResponseConstant.RSP_DESC_COMMON + "[{'remark':[{'remark':'日志描述','key':'日志内容','params':['日志参数']}],'create_user_account':'创建人账号','create_user_name':'创建人姓名','create_time':'创建时间'}]";
    String RSP_DESC_GET_ASSET_REPAIR_INFO = "获取设备故障履历信息" + ResponseConstant.RSP_DESC_COMMON + "[{'work_type_id':1：工单类型id,'type_name':'设备程序故障：故障类型名称','finished_time':'2020-04-1715:39:09：完成时间','work_code':'W20200417000008：工单号','fault_type':7：故障类型id,'relation_type':2,'repair_code':'FA000026：故障代码','code_name':'开关柜门无法关合：故障名称','relation_id':'375a5dfa-8251-4025-a9b8-d07047bbb4fa'}";
    String RSP_DESC_ADD_ASSET_FACILITY = "新增设备厂商信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_ASSET_FACILITY_LIST_INFO = "获取设备厂商信息" + ResponseConstant.RSP_DESC_COMMON + "[{'short_title':'厂商简称','address':'地址','inner_code':'编码','title':'厂商全程','org_name':'厂商类型'}]";
    String RSP_DESC_REMOVE_ASSET_FACILITY = "删除设备厂商" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_ASSET_DUTY_MAN_LIST_INFO = "获取设备关联人员信息" + ResponseConstant.RSP_DESC_COMMON + "[{'create_user_id':'创建人id','create_time':'创建时间','duty_name':'职责名称','duty_id':'职责id','user_id':'用户id','user_name':'负责人名称','mobile':'手机号','remark':'描述','id':'关联人员主键（删除时需要）','asset_id':'设备id','is_use':'是否启用','email':'邮箱'}]";
    String RSP_DESC_ADD_ASSET_DUTY_MAN = "新增设备关联人员信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_DUTY_MAN = "删除设备关联人员" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_ASSET_BOM = "新增设备关联备件信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_ASSET_PARENT = "新增子设备" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_PARENT = "删除子设备" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_ASSET_BOM = "更新设备关联备件" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_BOM = "删除设备关联备件" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_ASSET_BOM_LIST_INFO = "获取设备关联备件信息" + ResponseConstant.RSP_DESC_COMMON + "[{'bom_id':'备件id','unit_name':'单位','use_count':'所需数量','type_name':'备件类型','bom_model':'型号规格','type_id':'备件类型id','id':'设备备件主键','bom_code':'备件编码','supplier_name':'供应商','bom_name':'备件名称'}]";
    String RSP_DESC_GET_ASSET_PARENT_LIST = "获取子设备信息" + ResponseConstant.RSP_DESC_COMMON + "[{'asset_name':'设备名称','asset_code':'设备编码','position_name':'设备位置','category_name':'设备类型','model_name':'设备型号','id':'设备id'}]";
    String RSP_DESC_GET_BOM_USAGE_RECORD_LIST_INFO = "获取设备关联备件信息" + ResponseConstant.RSP_DESC_COMMON + "[{'unit_name':'单位','bom_id':'备件id','use_count':'s使用数量','work_code':'工单号','begin_time':'开工时间','show_price':'价格','sub_work_code':'子工单号','work_type_name':'工单类型'，'bom_code':'备件编码'}]";
    String RSP_DESC_GET_BOM_IMPORT_RECORD_LIST_INFO = "获得备件导入记录列表" + ResponseConstant.RSP_DESC_COMMON + "[{'bom_id':'备件id','stock_id':'库房id','bom_name':'备件名称','material_code':'物料编码','bom_type':'备件类型','bom_model':'规格型号','bom_unit':'单位','quantity':'数量'，'show_price':'价格', 'show_price':'价格', 'currency':'货币单位', 'supplier':'供应商', 'supply_cycle':'供货周期(天)', 'manufacturer':'制造商', 'service_life':'使用寿命(天)', 'bom_stock':'库房', 'security_quantity':'最小库存', 'max_security_quantity':'最大库存', 'store_position':'库位', 'create_time':'导入时间', 'create_user_id':'操作人', 'remark':'备注'}]";
    String RSP_DESC_SEARCH_CHOOSE_TASK_TEMPLATE_LIST_BY_POSITION_CODE = "获取该设备位置可选择的任务模板列表" + ResponseConstant.RSP_DESC_COMMON + "[{'task_template_name':'任务模板名称','task_template_code':'任务模板编码','remark':'月度保养'}]";
    String RSP_DESC_SEARCH_TASK_TEMPLATE_LIST_BY_POSITION_CODE = "获取该设备位置已选择的任务模板列表" + ResponseConstant.RSP_DESC_COMMON + "[{'work_type_id ':'工单类型','taskcount ':'任务书','task_template_name ':'任务模板名称 ','task_template_code ':'任务模板编码 ','position_code ':'设备位置编码 ','remark ':'备注 ','is_use':  '是否启用1启用-1未启用' }]";
    String RSP_DESC_SEARCH_TASK_TEMPLATE_BY_POSITION_CODE_AND_WORK_TYPE_ID = "根据设备位置和工单类型的任务模板列表" + ResponseConstant.RSP_DESC_COMMON + "[{'work_type_id ':'工单类型','taskcount ':'任务书','task_template_name ':'任务模板名称 ','task_template_code ':'任务模板编码 ','task_id':'任务模板任务项关联主键 ','position_code ':'设备位置编码 ','remark ':'备注 ','is_use':  '是否启用1启用-1未启用' }]";
    String RSP_DESC_ADD_ASSET_POSITION_TEMPLATE = "新增设备位置和任务模板关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_POSITION_TEMPLATE = "删除设备位置和任务模板关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ASSET_REPAIR_STATUS_BY_POSITION_CODE = "根据位置编码获取该位置内的设备的故障状态" + ResponseConstant.RSP_DESC_COMMON + "[{'isrepair':'是否有故障 1有 -1 没有','asset_name':'设备名称','id':'设备id'}]";
    String RSP_DESC_SEARCH_ALL_POSITION_CODE_ASSET_REPAIR_STATUS = "获取所有位置的设备故障数" + ResponseConstant.RSP_DESC_COMMON + "[{'repairCount':'设备故障数','position_name':'设备位置名称','position_code':'设备编码'}]";
    String RSP_DESC_GET_SPECIAL_ASSET_TYPE = "获取指定设备类型" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_GET_EXPORT_ALL_CHECK_METER = "导出全部抄表记录信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'抄表记录'";

    /**
     * 用户管理接口描述
     */
    String RSP_DESC_SEARCH_USER_LIST_PERMISSION = "获取用户模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_ADD_USER = "新增用户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_USER = "删除用户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_SELECT_USER = "删除选中用户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_USER = "编辑用户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_IS_USE_USER = "启用禁用用户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_USER_PASSWORD = "重置用户密码" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_USER_INFO = "获取用户详情" + ResponseConstant.RSP_DESC_COMMON + "{'is_on_duty':'是否在上班 1在 -1不在','create_time':‘创建时间’,'language_tag':'默认语言','join_date':入职时间,'mobile':'手机号','remark':备注,'user_code':'工号','gender_tag':性别1男2女,'is_charge':是否付费boolean,'id':'主键id','account':'账号','email':'邮箱','user_name':'用户名','is_use':'是否启用1启用0禁用string','is_on_duty':'是否在上班 1是 -1不是string'}";
    String RSP_DESC_SEARCH_USER_LIST_PAGE = "分页查询用户列表" + ResponseConstant.RSP_DESC_COMMON + "{'total':43,'rows':[{'create_time':'创建时间','groupPositionName':'部门岗位','user_code':'工号','mobile':'手机号','id':'主键','account':'账号','email':邮箱,'user_name':'姓名','is_use':'是否启用1启用0禁用string'}]}";
    String RSP_DESC_CHANGE_USER_IS_ON_DUTY = "更新用户上班状态" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    /**
     * 文件处理
     */
    String RSP_DESC_GET_IMG = "获取图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_LOGIN_IMAGE = "获取登录页图片" + "<br/><br/>id：1-背景图，2-logo" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EXPORT_FILE = "导出单个文件" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "不返回任何信息";
    String RSP_DESC_ADD_FILE = "文件上传" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ASSET_FILE_UPDATE = "设备附件更新" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CUSTOMER_FILE_UPDATE = "客户附件更新" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";

    /**
     * 数据导入
     */
    String RSP_DESC_IMPORT_DATA = "数据导入" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EXPORT_DATA_TEMPLATE = "数据导入模板" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_IMPORT_DATA_LOGS = "获取数据导入日志列表" + ResponseConstant.RSP_DESC_COMMON + "[{'log_type_name':'业务类型名称','create_time':'操作时间','user_name':'操作人','remark':'日志内容'}]";
    String RSP_DESC_SEARCH_IMPORT_TYPES = "获取数据导入类型列表" + ResponseConstant.RSP_DESC_COMMON + "{'importTypeId':'导入类型id','importTypeName':'导入类型名称'}";

    /**
     * 字典接口描述
     */
    String RSP_DESC_SEARCH_SELECT_OPTION_LIST = "字典数据信息" + ResponseConstant.RSP_DESC_COMMON + "[{'value':'编码','text':'描述','is_lang':'显示类型【非必填，默认直接显示，1：直接显示，2：国际化转换，3：json】'}]";
    String RSP_DESC_SEARCH_SELECT_OPTION_LIST_BY_KEY_AND_CODE = "根据key和code获取静态字典参数数据" + ResponseConstant.RSP_DESC_COMMON + "[{'value':'编码','text':'描述','is_lang':'显示类型【非必填，默认直接显示，1：直接显示，2：国际化转换，3：json】'}]";
    String RSP_DESC_SEARCH_SELECT_OPTION_BY_CODE = "字典选中项信息" + ResponseConstant.RSP_DESC_COMMON + "{'value':'编码','text':'描述','is_lang':'显示类型【非必填，默认直接显示，1：直接显示，2：国际化转换，3：json】'}";
    String RSP_DESC_SEARCH_SELECT_OPTION_ATTR_BY_CODE = "字典选中项属性" + ResponseConstant.RSP_DESC_COMMON + "'属性信息'";

    /**
     * 系统配置维护接口描述
     */
    String RSP_DESC_SEARCH_SYSTEM_CONFIG = "查询系统配置维护列表" + ResponseConstant.RSP_DESC_COMMON + "[{'config_name':'键值名称','group_title':'分组标题','source_data':'数据源值','config_title':'显示名称：','remark':'备注','setting_value':'配置值','data_order_index':'排序','control_type':'类型'}]";
    String RSP_DESC_SEARCH_SYSTEM_CONFIG_BY_NAME = "查询系统配置维护" + ResponseConstant.RSP_DESC_COMMON + "{'config_name':'键值名称','group_title':'分组标题','source_data':'数据源值','config_title':'显示名称：','remark':'备注','setting_value':'配置值','data_order_index':'排序','control_type':'类型'}";
    String RSP_DESC_SEARCH_MAP_TYPE_SYSTEM_CONFIG = "查询地图类型系统配置" + ResponseConstant.RSP_DESC_COMMON + "[{'config_name':'键值名称','group_title':'分组标题','source_data':'数据源值','config_title':'显示名称：','remark':'备注','setting_value':'配置值','data_order_index':'排序','control_type':'类型'}]";
    String RSP_DESC_EDIT_SYSTEM_CONFIG_LIST = "编辑系统配置维护列表" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_TOP_MAP_TYPE_SYSTEM_CONFIG = "查询顶层地图类型系统配置" + ResponseConstant.RSP_DESC_COMMON + "[{'config_name':'键值名称','group_title':'分组标题','source_data':'数据源值','config_title':'显示名称：','remark':'备注','setting_value':'配置值','data_order_index':'排序','control_type':'类型'}]";
    String RSP_DESC_SEARCH_GROUP_TITLE_LIST = "查询系统配置分组名称" + ResponseConstant.RSP_DESC_COMMON + "['分组标题']";

    /**
     * 业务配置维护
     */
    String RSP_DESC_SEARCH_SYSTEM_CONFIG_PERMISSION = "获取系统配置模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_ADD_SYSTEM_CONFIG = "新增系统配置维护" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_SYSTEM_CONFIG = "编辑系统配置维护" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_SYSTEM_CONFIG = "删除系统配置维护" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_SYSTEM_CONFIG_LIST = "查询系统配置维护" + ResponseConstant.RSP_DESC_COMMON + "{type_name:名称,data_order:排序,business_type_id:业务id,is_use:是否启用,running_status:运行状态名称,level_name:级别名称,type_code:类型编码,parent_id:父类id,unit_name:单位名称,finished_name:完修说明,is_finished:是否完修1是2否,currency_name:货币名称,currency_code:货币编号}";
    String RSP_DESC_SEARCH_SYSTEM_CONFIG_FIELD = "查询系统维护配置字段" + ResponseConstant.RSP_DESC_COMMON + "{'isTree':false,'field':[{'langKey':'title_name_ab_b','name':'currency_name'},{'langKey':'title_aau_t','name':'currency_code'},{'langKey':'title_cb','name':'data_order'},{'langKey':'title_aal_n','name':'is_use','option':'radio','selectKey':'is_enable'}]}";

    /**
     * 任务模板接口描述
     */
    String RSP_DESC_ADD_TASK_TEMPLATE = "新增任务模板" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_TASK_TEMPLATE = "编辑任务模板" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_TASK_TEMPLATE = "删除任务模板" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_TASK_TEMPLATE_INFO = "查询任务模板详情" + ResponseConstant.RSP_DESC_COMMON + "{is_use:是否启用,create_time:创建时间,task_template_name:任务模板名称,task_template_code:任务模板编号,remark:详细描述,create_user_account:创建人账号}";
    String RSP_DESC_SEARCH_TASK_TEMPLATE_PAGE = "分页查询任务模板列表" + ResponseConstant.RSP_DESC_COMMON + "{'total':43,'rows':[{itemNum:任务项数量,is_use:是否启用,create_time:创建时间,task_template_name:任务模板名称,task_template_code:任务模板编号,remark:详细描述,create_user_account:创建人账号}]}";
    String RSP_DESC_ADD_TASK_TEMPLATE_ITEM = "新增任务模板任务项关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_TASK_TEMPLATE_ITEM = "删除任务模板任务项关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_TASK_TEMPLATE_ITEM = "编辑任务模板任务项关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_MOVE_UP_TASK_TEMPLATE_ITEM = "向上任务模板任务项关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_MOVE_MOVE_TASK_TEMPLATE_ITEM = "向下任务模板任务项关联" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_TASK_TEMPLATE_ITEM_LIST = "向下任务模板任务项关联" + ResponseConstant.RSP_DESC_COMMON + "[{result_type:结果类型,result_verification:结果校验,group_name:任务组名,task_template_code:WTT000283,task_id:模板任务项关联主键,task_item_name:任务项名称,task_item_code:WTS000270,requirements:任务项描述,data_order:2}]";
    String RSP_DESC_SEARCH_TASK_TEMPLATE_LIST = "查询任务模板列表" + ResponseConstant.RSP_DESC_COMMON + "{[{itemNum:任务项数量,is_use:是否启用,create_time:创建时间,task_template_name:任务模板名称,task_template_code:任务模板编号,remark:详细描述,create_user_account:创建人账号}]";
    String RSP_DESC_GET_EXPORT_TASK_TEMP_CODE = "导出选中任务模板信息（导出码）" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'任务模板导出码'";
    String RSP_DESC_GET_EXPORT_ALL_TASK_TEMP_CODE = "导出全部任务模板信息（导出码）" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'任务模板导出码'";

    /**
     * 任务项接口描述
     */
    String RSP_DESC_ADD_TASK_ITEM = "新增任务项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_TASK_ITEM = "编辑任务项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_TASK_ITEM = "删除任务项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_TASK_ITEM_INFO = "查询任务项详情" + ResponseConstant.RSP_DESC_COMMON + "{result_type:结果类型,result_verification:结果校验,requirements:任务描述,create_time:创建时间,task_item_name:任务名称,task_item_code:任务编码,create_user_account:创建人}";
    String RSP_DESC_SEARCH_TASK_ITEM_PAGE = "分页查询任务项列表" + ResponseConstant.RSP_DESC_COMMON + "{'total':43,'rows':[{result_type:结果类型,result_verification:结果校验,requirements:任务描述,create_time:创建时间,task_item_name:任务名称,task_item_code:任务编码,create_user_account:创建人}]}";
    String RSP_DESC_SEARCH_CHOSE_TASK_ITEM_LIST = "分页查询任务项列表" + ResponseConstant.RSP_DESC_COMMON + "[{result_type:结果类型,task_item_name:任务项名称,task_item_code:任务项编码}]";
    String RSP_DESC_SEARCH_TASK_ITEM_LIST_BY_TEMPLATE_CODE = "根据任务模板编码查询任务项列表" + ResponseConstant.RSP_DESC_COMMON + "[{result_type:结果类型,task_item_name:任务项名称,task_item_code:任务项编码}]";
    String RSP_DESC_TASK_ITEM_FILE_UPDATE = "任务项附件更新" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_TASK_ITEM_FILE_LIST = "获取任务项附件文档信息" + ResponseConstant.RSP_DESC_COMMON + "{'code':200,'msg':null,'content':[{'id':7085：附件id,'remark':null：备注,'is_use':true：true正常false删除,'create_time_str':'2020-04-2410:25:16：创建时间','fileName':'ea3fdb82-272a-4243-a0c2-b25ab221a0d0.jpg：文件名称','create_time':1587695116272,'fileOriginalName':'tmp_b433cd29517adb425ae3f20c7069de66e5977404f5df793a.jpg：文件原始名称','createUserAccount':'fsm1：创建人账号','fileType':1文件类型1图片2文档,'fileUrl':'aaa.jpg：文件路径','fileSize':114.0：文件大小,'fileHeight':1920：高度（如果是图片）,'fileWidth':960：宽度（如果是图片）}]}";
    String RSP_DESC_GET_EXPORT_TASK_ITEM_CODE = "导出选中任务模板信息（导出码）" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'任务模板导出码'";
    String RSP_DESC_GET_EXPORT_ALL_TASK_ITEM_CODE = "导出全部任务模板信息（导出码）" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'任务模板导出码'";

    /**
     * 设备位置接口描述
     */
    String RSP_DESC_ADD_ASSET_POSITION = "新增设备位置" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_ASSET_POSITION = "编辑设备位置" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_ASSET_POSITION = "删除设备位置" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ASSET_POSITION_INFO = "查询设备位置详情" + ResponseConstant.RSP_DESC_COMMON + "{parentPosition:{父位置信息，字段一样},create_time:创建时间,parent_id:父位置编码,file_id:平面图id,layer_path:组织图层,position_name:位置名称,type_name:位置类型名称,location:{type:point,value:(1.0,1.0),x:1.0,y:1.0},remark:位置描述,id:位置编码,position_type_id:位置类型id,create_user_account:创建人,scada_config:scada配置}";
    String RSP_DESC_SEARCH_ASSET_POSITION_LIST = "分页查询设备位置列表" + ResponseConstant.RSP_DESC_COMMON + "[{create_time:创建时间,parent_id:父位置编码,file_id:平面图id,layer_path:组织图层,position_name:位置名称,type_name:位置类型名称,location:{type:point,value:(1.0,1.0),x:1.0,y:1.0},remark:位置描述,id:位置编码,position_type_id:位置类型id,create_user_account:创建人}]";
    String RSP_DESC_SHOW_ASSET_POSITION_QR_CODE = "展示设备位置二维码图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EXPORT_ASSET_POSITION_QR_CODE = "导出设备位置二维码图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_SUPERIOR_POSITION_PLAN = "根据设备位置获取上级的平面图" + ResponseConstant.RSP_DESC_COMMON + "文件主键【Integer】";


    /**
     * 厂商管理接口描述
     */
    String RSP_DESC_SEARCH_FACILITIES_LIST_PERMISSION = "获取厂商模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_ADD_FACILITIES = "新增厂商" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_FACILITIES = "编辑厂商" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_USE_FACILITIES = "启用禁用厂商" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_FACILITIES = "删除厂商" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_FACILITIES_INFO = "查询厂商详情" + ResponseConstant.RSP_DESC_COMMON + "{'is_use':'是否启用 1启用2不启用','layer_path':'组织图层','address':'地址','create_time':'创建时间','org_type':'组织类型','remark':'描述','title':'名称','parent_id':'父级厂商id','inner_code':'厂商编码','create_user_account':'创建人','short_title':'厂商简称','location':'坐标','currency_id':'货币单位'}";
    String RSP_DESC_SEARCH_FACILITIES_LIST = "查询厂商列表" + ResponseConstant.RSP_DESC_COMMON + "['is_use':'是否启用 1启用2不启用','layer_path':'组织图层','address':'地址','create_time':'创建时间','org_type':'组织类型','remark':'描述','title':'名称','parent_id':'父级厂商id','inner_code':'厂商编码','create_user_account':'创建人','short_title':'厂商简称','location':'坐标','currency_id':'货币单位'}]";
    String RSP_DESC_ADD_FACILITIES_DOC = "新增厂商附件文档" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_FACILITIES_DOC = "删除厂商附件文档" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_FACILITIES_DOC_LIST = "查询厂商附件文档列表" + ResponseConstant.RSP_DESC_COMMON + "[{'file_url':'附件路径','file_original_name':'文件名称','create_time':'创建时间','file_name':'29b99554-9493-4240-a72e-2742bf5ac566.jpg','org_id':'厂商id','file_type':'附件类型id','file_id':'附件id','remark':'1','file_size':'文件大小','create_user_name':'创建人'}]";
    String RSP_DESC_ADD_FACILITIES_CONTACT = "新增厂商联系人" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_FACILITIES_CONTACT = "编辑厂商联系人" + ResponseConstant.RSP_DESC_COMMON + "{'contact_name':'联系人名称','wei_xin':'微信号','org_id':'厂商id','contact_mobile':'手机号','id':713,'contact_email':'邮箱','id':厂商联系人主键id}";
    String RSP_DESC_REMOVE_FACILITIES_CONTACT = "删除厂商联系人" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_FACILITIES_CONTACT_LIST = "查询厂商联系人列表" + ResponseConstant.RSP_DESC_COMMON + "[{'contact_name':'联系人名称','wei_xin':'微信号','org_id':'厂商id','contact_mobile':'手机号','id':713,'contact_email':'邮箱'}]";

    /**
     * 备件管理接口描述
     */
    String RSP_DESC_SEARCH_BOM_LIST_PERMISSION = "获取备件权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_SEARCH_BOM_LIST = "获取备件列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'id':'备件主键','bom_name':'备件名称','material_code':'物料编码','bom_model':'规格型号','type_name':'备件类型','unit_name':'单位','code_classification':'1：一类一码；2：一物一码','title':'供应商','total':'总库存','is_use':'是否启用 1启用 0禁用'}]";
    String RSP_DESC_ADD_BOM = "新增备件信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_BOM = "编辑备件信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_BOM_INFO = "获取备件明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'id':'备件主键','bom_name':'备件名称','material_code':'物料编码','bom_model':'规格型号','remark':'备注','create_time':'创建时间','type_id':'备件类别','unit_id':'单位','service_life':'使用寿命（天）','show_price':'备件单价','currency_id':'货币单位','manufacturer_id':'制造商','icon':'头像id','quantity':'总库存'}";
    String RSP_DESC_GET_BOM_STOCK_INFO = "获取备件库存信息列表" + ResponseConstant.RSP_DESC_COMMON + "[{'total':'库存','stock_name':'库房名称','store_position':'库位','security_quantity':'最小库存','max_security_quantity':'最大库存'}]";
    String RSP_DESC_GET_BOM_SUPPLIER_INFO = "获取备件供应商信息" + ResponseConstant.RSP_DESC_COMMON + "{'is_use':'是否启用 1启用 0禁用','short_title':'供应商名称','price':'价格','id':'备件供应商主键','supply_period':'供货周期（天）','currency_id':'货币单位'}";
    String RSP_DESC_SEARCH_BOM_LOG_LIST = "获取备件历史记录信息" + ResponseConstant.RSP_DESC_COMMON + "[{'remark':[{'remark':'日志描述','key':'日志内容','params':['日志参数']}],'create_user_account':'创建人账号','create_user_name':'创建人姓名','create_time':'创建时间'}]";
    String RSP_DESC_ADD_BOM_SUPPLIER = "新增备件供应商信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_BOM_SUPPLIER = "编辑备件供应商信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_DELETE_BOM_SUPPLIER = "删除备件供应商信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_USE_BOM_SUPPLIER = "启用禁用备件供应商" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_USE_BOM = "启用禁用备件" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SHOW_BOM_QR_CODE = "展示备件物料二维码图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_BOM_BY_SELECT = "删除选中备件信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_BOM_IN_AND_OUT_LIST = "获取备件出入库信息列表" + ResponseConstant.RSP_DESC_COMMON + "[{'create_user_id':'操作人id','code':'工单号','quantity':'数量','create_time':'创建时间','stock_name':' 库房212','page_type':'出入库类型 1：调拨，2：领用，3：报废，4：入库','user_name':'操作人姓名','status_name':'工单状态名称','real_quantity':'最终数量（暂时不考虑）','stock_id':'库房id','bom_id':'备件id','store_position':'库位','bom_code':'备件编码','process_method':'操作方法','status':'工单状态'}]";
    String RSP_DESC_SHOW_BOM_NEW_QR_CODE = "展示备件二维码图片" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EXPORT_BOM_QR_CODE = "导出选中备件二维码图片信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "不返回任何信息";
    String RSP_DESC_EXPORT_ALL_BOM_QR_CODE = "导出全部备件二维码图片信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "不返回任何信息";

    /**
     * 工作流接口描述
     */
    String RSP_DESC_SEARCH_WORKFLOW_PERMISSION = "获取工作流模块权限" + ResponseConstant.RSP_DESC_RIGHT_BY_MENU + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{}}";
    String RSP_DESC_SEARCH_WORKFLOW_DEPLOYMENT_LIST = "获取工作流部署列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'id':'部署主键','name':'部署名称','category':'部署分类','key':'流程编号','deploy_time':'部署时间'}]";
    String RSP_DESC_REMOVE_WORKFLOW_DEPLOYMENT = "删除工作流部署对象" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_WORKFLOW_DEFINITION_LIST = "获取工作流定义列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'id':'流程主键','name':'流程名称','key':'流程编号','version':'流程版本号','suspension_state':'是否挂起【1激活、2挂起】'}]";
    String RSP_DESC_SHOW_WORKFLOW_PROCESS_DIAGRAM = "获取工作流定义流程图" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_WORKFLOW_INSTANCE_LIST = "获取工作流实例列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'id':'实例主键','inst_id':'主实例主键','create_time':'创建时间','instance_name':'实例名称','business_key':'实例编号','pref_name':'流程定义','pref_version':'流程版本号','start_user':'启动者信息','start_user_name':'启动者','instance_status':'实例状态【boolean】','pref_status':'流程状态【1激活、2挂起】','task_name':'任务名称','task_user':'当前操作人信息','task_user_name':'当前操作人','task_count':'任务数量','business_code':'业务编码'}]";
    String RSP_DESC_SHOW_WORKFLOW_INSTANCE_DIAGRAM = "获取工作流实例流程图" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORKFLOW_INSTANCE = "删除工作流实例" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORKFLOW_INSTANCE_FILTER_FLOW_NAME = "删除流程名称之外的工作流实例" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORKFLOW_INSTANCE_BY_SUB_WORK_CODE = "根据子工单编号删除工作流实例" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_WORKFLOW_INST_DSP_LIST = "获取工作流流程、节点信息列表" + ResponseConstant.RSP_DESC_COMMON + "[{'pageId':'页面主键','parentPageId':'所属页面主键','id':'主键','name':'名称','is_node':'是否是节点【boolean】','node_type':'节点类型【flow:工作流，start:启动节点，end:结束节点，node:节点，subNode:子流程节点】','parent':'所属流程主键','flow_code':'流程编号'}]";
    String RSP_DESC_SHOW_WORKFLOW_NODE_DIAGRAM = "获取工作流节点流程图" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_START_WORKFLOW = "启动流程" + ResponseConstant.RSP_DESC_COMMON + "{'started':'启动状态','formKey':'工单模板id'}";
    String RSP_DESC_CLAIM_TASK = "签收任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_PARENT_CLAIM_TASK = "签收所有子任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_UNCLAIM_TASK = "取消签收任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_PARENT_UNCLAIM_TASK = "取消签收所有子任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ASSIGN_TASK = "分配任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_PARENT_ASSIGN_TASK = "分配所有子任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_COMPLETE_TASK = "完成任务" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_TASK_VARIABLES = "获取流程变量" + ResponseConstant.RSP_DESC_COMMON;
    String RSP_DESC_SEARCH_TASK_LIST = "获取待办列表" + ResponseConstant.RSP_DESC_COMMON + "{'total':'查询结果总数','rows':[{'work_type_name':'申请单类型','task_id':'待办主键','create_time':'创建时间','deadline_time':'截止时间','process_create_date':'待办流转日期','receive_time':'执行人接收时间','begin_time':'执行人开始时间','finished_time':'执行人完成时间','distribute_time':'分配时间','permission':'操作权限(isEdit: 处理,isDelete: 清除,isDistribution: 分配,isDetail: 查看)','title':'申请单主题','assignee_name':'当前处理人','sub_work_code':'申请单主键','status_name':'申请单状态','distribute_user_name':'分配人','receive_user_name':'执行人','create_user_name':'申请人','duty_user_name':'申请单负责人','relation_name':'申请单对象','position_name':'所属位置','duty_user_id':'当前操作人id'}]}";
    String RSP_DESC_SEARCH_SINGLE_WORK_INFO = "根据工单编号，获取工单页面数据" + ResponseConstant.RSP_DESC_COMMON + "{'create_user_id':'创建人id','type_name':'工单类型','deal_role_id':'处理角色id','create_time':'创建时间','deal_type_id':'节点处理类型：1：单人处理；2：会签；3：抢单；4：处理链；','page_type':'页面类型-基本类型（处理）：base、详情：detail','node_name':'流程节点名称','work_template_code':'工单模板编号','flow_name':'流程名称','remark':'备注','relation_type':'适用对象key','relation_type_name':'适用对象名称','flow_code':'流程code','field_section_list':[{'field_section_type':'分栏（数据字典code）','work_flow_node_column_list':[{'is_table_key':'是否是表主键','create_user_id':'创建人id','field_is_show':'是否显示：hide 隐藏，show 显示','create_time':'创建时间','save_type':'存储方式 0:自定义字段','field_code':'字段编码','field_section_type':'所属模块','data_order':'排序','change_type':'字段类型','work_flow_node_column_linkage_list':[{'create_user_id':'创建人id','linkage_type':'联动类型：1：界面内容；2：接口；3：自定义；','create_time':'创建时间','work_flow_node_column_linkage_condition':{'interface_address':'接口地址：各业务方法','condition':'条件json格式字符串','link_field_form_code':'联动字段主键','self_defined':'自定义','linkage_id':'联动表id'},'field_form_code':'字段主键','id':'模板字段联动主键','node_id':'流程主键id，流程的sid(pref_id)，工单模板code'}],'field_data_base':'接口数据源','db_table_type':'表类型','field_name':'字段显示名称/key','field_view_type':'控件类型','word_flow_node_id':'节点id','work_flow_node_column_ext_list':[{'create_user_id':'创建人id','field_write_type':'属性类型/写入只读权限/日期类型/小数点位数/','create_time':'创建时间','field_form_code':'字段主键','field_value':'属性值','id':'流程配置字段联动主键','node_id':'节点id','field_name':'属性名称/taskRight'}],'is_required':'是否必填 1是 2 否','field_form_code':'字段主键','field_right':'写入只读权限 readonly 只读  edit 写入','field_remark':'备注','field_value':'默认值','id':'流程配置字段主键','field_validate_method':'校验','node_id':'节点id'}]}],'work_type_id':'工单类型id','word_flow_node_id':'工单流程节点id','node_type':'节点类型-流程：flow、开始节点：start、过程节点：node、结束节点：end、子节点：subNode','work_template_name':'工单模板名称','pref_id':'流程的sid','node_show_name':'流程节点显示名称','flow_show_name':'流程显示名称','id':'流程节点主键','node_id':'节点id'}";
    String RSP_DESC_SEARCH_METADATA_WORK_BY_SCAN = "扫码获取模板信息" + ResponseConstant.RSP_DESC_COMMON + "{'scanData':[{'field_code':'字段编码','field_value':'字段值'}],'create_user_id':'创建人id','type_name':'工单类型','deal_role_id':'处理角色id','create_time':'创建时间','deal_type_id':'节点处理类型：1：单人处理；2：会签；3：抢单；4：处理链；','page_type':'页面类型-基本类型（处理）：base、详情：detail','node_name':'流程节点名称','work_template_code':'工单模板编号','flow_name':'流程名称','remark':'备注','relation_type':'适用对象key','relation_type_name':'适用对象名称','flow_code':'流程code','field_section_list':[{'field_section_type':'分栏（数据字典code）','work_flow_node_column_list':[{'is_table_key':'是否是表主键','create_user_id':'创建人id','field_is_show':'是否显示：hide 隐藏，show 显示','create_time':'创建时间','save_type':'存储方式 0:自定义字段','field_code':'字段编码','field_section_type':'所属模块','data_order':'排序','change_type':'字段类型','work_flow_node_column_linkage_list':[{'create_user_id':'创建人id','linkage_type':'联动类型：1：界面内容；2：接口；3：自定义；','create_time':'创建时间','work_flow_node_column_linkage_condition':{'interface_address':'接口地址：各业务方法','condition':'条件json格式字符串','link_field_form_code':'联动字段主键','self_defined':'自定义','linkage_id':'联动表id'},'field_form_code':'字段主键','id':'模板字段联动主键','node_id':'流程主键id，流程的sid(pref_id)，工单模板code'}],'field_data_base':'接口数据源','db_table_type':'表类型','field_name':'字段显示名称/key','field_view_type':'控件类型','word_flow_node_id':'节点id','work_flow_node_column_ext_list':[{'create_user_id':'创建人id','field_write_type':'属性类型/写入只读权限/日期类型/小数点位数/','create_time':'创建时间','field_form_code':'字段主键','field_value':'属性值','id':'流程配置字段联动主键','node_id':'节点id','field_name':'属性名称/taskRight'}],'is_required':'是否必填 1是 2 否','field_form_code':'字段主键','field_right':'写入只读权限 readonly 只读  edit 写入','field_remark':'备注','field_value':'默认值','id':'流程配置字段主键','field_validate_method':'校验','node_id':'节点id'}]}],'work_type_id':'工单类型id','word_flow_node_id':'工单流程节点id','node_type':'节点类型-流程：flow、开始节点：start、过程节点：node、结束节点：end、子节点：subNode','work_template_name':'工单模板名称','pref_id':'流程的sid','node_show_name':'流程节点显示名称','flow_show_name':'流程显示名称','id':'流程节点主键','node_id':'节点id'}";
    String RSP_DESC_SEARCH_TASK_LIST_PERMISSION = "获取待办任务列表模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_CLEAR_WORKFLOW = "清空流程" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_EXPORT_WORK_FLOW = "导出流程配置信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'流程配置信息'";

    /**
     * 字段库接口描述
     */
    String RSP_DESC_SEARCH_WORK_COLUMN_LIST = "获取字段库列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'field_form_code':'字段主键','field_code':'字段编码','field_name':'字段显示名称/key','field_view_type':'控件类型','field_view_type_name':'控件类型名称','field_section_type':'所属模块','field_section_type_name':'所属模块名称','create_time':'创建时间','create_user_id':'创建人id','field_right':'写入只读权限 readonly 只读  edit 写入','is_required':'是否必填 1是 2 否','field_remark':'备注'}]";
    String RSP_DESC_GET_WORK_COLUMN_KEY = "获取字段唯一键信息" + ResponseConstant.RSP_DESC_COMMON + "{返回唯一键值}";
    String RSP_DESC_ADD_WORK_COLUMN = "新增字段信息" + ResponseConstant.REQ_DESC_COMMON + "extList:[{'field_name':'属性名称/taskRight','field_write_type':'属性类型 /写入只读权限/日期类型/小数点位数/','field_value':'属性值'}]" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_WORK_COLUMN = "编辑字段信息" + ResponseConstant.REQ_DESC_COMMON + "extList:[{'field_name':'属性名称/taskRight','field_write_type':'属性类型 /写入只读权限/日期类型/小数点位数/','field_value':'属性值'}]" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORK_COLUMN_BY_SELECT = "删除选中字段信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_WORK_COLUMN_INFO = "获取字段明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'create_user_id':'创建人id','is_table_key':'是否是表主键','create_time':'创建时间','save_type':'存储方式 0:自定义字段','field_code':'字段编码','field_section_type':'所属模块','change_type':'字段类型','field_section_type_name':'所属模块名称','field_data_base':'接口数据源','db_table_type':'表类型','field_name':'字段显示名称/key','field_view_type':'控件类型','is_required':'是否必填 1是 2 否','field_view_type_name':'控件类型名称','field_form_code':'字段主键','field_right':'写入只读权限 readonly 只读  edit 写入','field_remark':'备注','field_value':'默认值','field_validate_method':'校验','extList':[{'field_write_type':'属性类型/写入只读权限/日期类型/小数点位数/','field_value':'属性值','id':'特殊属性主键','field_name':'属性名称/taskRight'}]}";
    String RSP_DESC_GET_WORK_COLUMN_CLONE = "克隆" + ResponseConstant.RSP_DESC_COMMON + "模板主键";

    /**
     * 工单模板接口描述
     */
    String RSP_DESC_SEARCH_WORK_TEMPLATE_LIST = "获取工单模板列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'work_template_name':'工单名称','work_template_code':'工单模板唯一键、编码','field_name':'字段显示名称/key','create_time':'创建时间'}]";
    String RSP_DESC_ADD_WORK_TEMPLATE = "新增工单模板基础信息" + ResponseConstant.RSP_DESC_COMMON + "工单模板唯一键";
    String RSP_DESC_GET_WORK_TEMPLATE_INFO = "获取工单模板基本信息" + ResponseConstant.RSP_DESC_COMMON + "{'work_template_name':'工单名称','work_template_code':'工单模板唯一键、编码','field_name':'字段显示名称/key','create_time':'创建时间'}";
    String RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_LIST = "获取工单模板字段列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'source':'数据来源','create_user_id':'创建人id','field_view_type':'控件类型','field_view_type_name':'控件类型名称','field_form_code':'字段库字段主键','field_code':'字段编码','field_section_type':'所属模块','field_section_type_name':'所属模块名称','create_time':'创建时间','field_name':'字段显示名称/key','id':'工单模板字段主键'}]";
    String RSP_DESC_REMOVE_WORK_TEMPLATE_BY_SELECT = "删除选中工单模板信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORK_TEMPLATE_COLUMN_BY_SELECT = "删除工单模板选中字段信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_WORK_TEMPLATE_COLUMN = "编辑字段信息" + ResponseConstant.REQ_DESC_COMMON + "extList:[{'field_name':'属性名称/taskRight','field_write_type':'属性类型 /写入只读权限/日期类型/小数点位数/','field_value':'属性值'}]<br/> linkList:[{'linkage_type':'联动类型：1：界面内容；2：接口；3：自定义；4：子页面的值','link_field_form_code':'联动字段主键 ；当 linkage type 为1时传值'},'self_defined':'自定义 ；当 linkage type 为3时传值','interface_address':'接口地址：各业务方法 ；当 linkage type 为2时传值','sub_page_template_code':'子页面模板的值','condition':'条件字段，包含顶部联动控件和来源为接口时的参数设置，详情见下']<br/> condition:{'top':'顶部的联动空间，集合格式，暂取字段见下',interface:'来源为接口时的参数，集合格式，暂取字段见下'}<br/> top:[{'code':'控件名称编码','condition':'条件','data':'数据','data_type':'1、input框 2、select框'}] <br/> interface :[{'code':'条件参数名称或编码','data_type':'1、控件值 2、固定值','data':'数据'}] " + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_WORK_TEMPLATE = "编辑模板基本信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_WORK_TEMPLATE_COLUMN_INFO = "获取模板字段明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'id':'工单模板字段主键','node_id':'工单模板主键','is_table_key':'是否是表主键','create_user_id':'创建人id','field_is_show':'是否显示 ，流程处使用，模板处无用','create_time':'创建时间','save_type':'存储方式 0:自定义字段','field_code':'字段编码','field_section_type':'所属模块','change_type':'字段类型','field_section_type_name':'所属模块名称','field_data_base':'接口数据源','db_table_type':'表类型','field_name':'字段显示名称/key','field_view_type':'控件类型','is_required':'是否必填 1是 2 否','field_view_type_name':'控件类型名称','field_form_code':'字段主键','field_right':'写入只读权限 readonly 只读  edit 写入','field_remark':'备注','field_value':'默认值','field_validate_method':'校验','extList':[{'field_write_type':'属性类型/写入只读权限/日期类型/小数点位数/','field_value':'属性值','id':'特殊属性主键','field_name':'属性名称/taskRight'}],'linkList':[{'linkage_type':'联动类型：1：界面内容；2：接口；3：自定义；4:子页面的值','sub_page_template_code':'子页面的值','interface_address':'接口地址：各业务方法','link_field_form_code':'联动字段主键','self_defined':'自定义','id':'模板字段联动主键','node_id':'工单模板主键','condition':'条件json格式字符串'}]}";
    String RSP_DESC_GET_WORK_TEMPLATE_COLUMN_LINKAGE_CONDITION_INFO = "获取模板字段联动条件信息" + ResponseConstant.RSP_DESC_COMMON + "{'id':'模板字段联动条件主键','linkage_id':'模板字段联动主键','linkage_type':'联动类型：1：界面内容；2：接口；3：自定义；','interface_address':'接口地址：各业务方法','link_field_form_code':'联动字段主键','self_defined':'自定义','condition':'条件json格式字符串'}";
    String RSP_DESC_GET_WORK_TEMPLATE_COLUMN_CLONE = "克隆" + ResponseConstant.RSP_DESC_COMMON + "模板主键";
    String RSP_DESC_GET_WORK_TEMPLATE_COLUMN_SYNC = "一键同步" + ResponseConstant.RSP_DESC_COMMON + "模板主键";
    String RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_SPECIAL_PROPERTY = "根据字段主键获取属性值及字段控件类型" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'field_view_type':'控件类型','value':[{'text':'','value':''}]}";
    String RSP_DESC_SEARCH_COLUMN_PROPERTY = "根据字段id获取基本及特殊属性" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'text':'','value':''}";
    String RSP_DESC_SEARCH_COLUMN_SPECIAL_PROPERTY = "根据控件类型获取特殊属性key和value" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'text':'','value':'','is_lang':'','relation':'','keyValue':[{'text':'','value':'','is_lang':''}]}";
    String RSP_DESC_EDIT_WORK_TEMPLATE_COLUMN_USE = "编辑模板字段是否显示" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";

    /**
     * 流程配置接口描述
     */
    String RSP_DESC_GET_WORK_FLOW_TEMPLATE_INFO = "获取流程配置基本信息" + ResponseConstant.RSP_DESC_COMMON + "流程节点只存在base字段，节点存在base和handle，子节点存在base、sub、handle。<br/> {'base':{'page_type':'base 基本页面，detail 详细页面','create_user_id':'创建人id','type_name':'工单类型名称','create_time':'创建时间','work_template_code':'工单模板唯一键、编码','node_name':'节点名称','flow_name':'流程名称','relation_type':'适用对象key','flow_code':'流程code','relation_type_name':'适用对象名称','work_type_id':'工单类型id','node_type':'节点类型： 流程：flow,开始节点：start,过程节点：node,结束节点：end,子节点：subNode','work_template_name':'工单名称','pref_id':'流程id','node_show_name':'节点展示名称','flow_show_name':'流程展示名称','id':'流程表主键','node_id':'节点id','word_flow_node_id':'节点表主键id'},'sub':{'create_user_id':'创建人id','type_name':'工单类型名称','create_time':'创建时间','work_template_code':'工单模板唯一键、编码','node_name':'节点名称','flow_name':'流程名称','relation_type':'适用对象key','relation_type_name':'适用对象名称','work_type_id':'工单类型id','node_type':'流程：flow,开始节点：start,过程节点：node,结束节点：end,子节点：subNode','work_template_name':'工单名称','pref_id':'流程id','node_show_name':'节点展示名称','flow_show_name':'流程展示名称','id':'流程表主键','node_id':'节点id','word_flow_node_id':'节点表主键id'},'handle':{'deal_role_id':'处理人角色','deal_type_id':'处理类型'}}";
    String RSP_DESC_ADD_WORK_FLOW_TEMPLATE = "新增/编辑流程配置基础信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_LIST_FOR_ADD = "新增字段查询工单模板字段列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'create_time':'创建时间','create_user_id':'创建人id','field_code':'字段编码','field_section_type':'所属模块','work_template_code':'工单模板code','field_form_code':'字段主键','field_name':'字段显示名称/key','field_view_type':'控件类型','field_view_type_name':'控件类型名称','field_section_type_name':'所属模块名称','field_right':'写入只读权限 readonly 只读  edit 写入','is_required':'是否必填 1是 2 否','field_remark':'备注'}]";
    String RSP_DESC_ADD_WORK_FLOW_COLUMN = "新增字段信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_WORK_FLOW_TEMPLATE_COLUMN_LIST = "获取流程配置字段列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'create_user_id':'创建人id','create_time':'创建时间','field_code':'字段编码','field_section_type':'所属模块','work_template_code':'工单模板code','field_form_code':'字段主键','field_name':'字段显示名称/key','field_view_type':'控件类型','field_view_type_name':'控件类型名称','field_section_type_name':'所属模块名称','field_right':'写入只读权限 readonly 只读  edit 写入','is_required':'是否必填 1是 2 否','field_remark':'备注','id':'流程配置字段字段主键','field_is_show':'是否显示：hide 隐藏 show 显示','data_order':'排序','source':'数据来源'}]";
    String RSP_DESC_EDIT_WORK_FLOW_TEMPLATE_COLUMN_USE = "编辑流程字段区块、是否显示、操作权限、上移、下移信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_WORK_FLOW_TEMPLATE_HANDLE = "编辑操作人信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORK_FLOW_TEMPLATE_COLUMN_BY_SELECT = "删除流程配置中字段信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_WORK_FLOW_COLUMN_INFO = "获取模板字段明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'id':'流程配置字段主键','node_id':'节点id','is_table_key':'是否是表主键','create_user_id':'创建人id','field_is_show':'是否显示：hide 隐藏，show 显示','create_time':'创建时间','save_type':'存储方式 0:自定义字段','field_code':'字段编码','field_section_type':'所属模块','change_type':'字段类型','field_section_type_name':'所属模块名称','field_data_base':'接口数据源','db_table_type':'表类型','field_name':'字段显示名称/key','field_view_type':'控件类型','is_required':'是否必填 1是 2 否','data_order':'排序','field_view_type_name':'控件类型名称','field_form_code':'字段主键','field_right':'写入只读权限 readonly 只读  edit 写入','field_remark':'备注','field_value':'默认值','field_validate_method':'校验','extList':[{'field_write_type':'属性类型/写入只读权限/日期类型/小数点位数/','field_value':'属性值','id':'特殊属性主键','field_name':'属性名称/taskRight'}],'linkList':[{'linkage_type':'联动类型：1：界面内容；2：接口；3：自定义；','interface_address':'接口地址：各业务方法','link_field_form_code':'联动字段主键','self_defined':'自定义','id':'模板字段联动主键','node_id':'工单模板主键','condition':'条件json格式字符串'}]}";

    /**
     * 维保计划接口描述
     */
    String RSP_DESC_SEARCH_PLAN_WORK_LIST_PERMISSION = "获取维保计划模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_ASSET_POSITION_TREE = "获取设备位置树" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'value':'节点值','text':'展示文字','parent':'父节点值，为空字符表示父节点','is_scada':'是否有scada检测，1有  0无 '}]";
    String RSP_DESC_ADD_PLAN_WORK = "新增计划信息" + ResponseConstant.REQ_DESC_COMMON + "maintenanceObject:[{'position_codes':'设备位置树value字符串','asset_category_id':'设备类型','asset_model_id':'设备型号','is_all_asset':'勾选：true （全部选择）不勾选：false （手动选择设备）','enable_begin_date':'启用日期开始','enable_end_date':'启用日期截止','assets':'选中的设备信息，逗号拼接'}]<br/>  assistList:[{'duty_type':'职责类型 1：执行，2：协助','self_or_out':'团队类型 1：内部 2：外部','total_hour':'计划总工时','user_id':'执行人的id','group_id':'协助团队的id','team_size':'团队人数'}]<br/> triggleInfo:[{'period_type':'周期类型1：单次2：按天3：按周4：按月5：年','execute_day':'单次时的日期','period_value':'周期值（天数、月数等，为年时，表示第几月）','type_day_select':'4、5时的选择日期类型，天，工作日，具体星期几','which_type_day':'4、5时选的第几周工作日或具体某一天','type_week_select':'3时周几：可多选，用“,”隔开','execute_hour':'在几时','execute_minute':'在几分','duration_time':'持续时间','duration_type':'持续时间类型'},'column_key':'判断列名'},'column_condition':'判断条件 1:<= 2:==3:>=4:like'},'column_value':'判断值'}] <br/> planJson:{'task_list':'任务项处数据，详见下'} <br/> 'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]<br/> conditionTriggle:[{'column_key':'字段code','column_condition':'>=判断条件','column_value':'75%值（数字或百分数）'}]<br/> " + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_PLAN_WORK = "编辑计划信息" + ResponseConstant.REQ_DESC_COMMON + "maintenanceObject:[{'position_codes':'设备位置树value字符串','asset_category_id':'设备类型','asset_model_id':'设备型号','is_all_asset':'勾选：true （全部选择）不勾选：false （手动选择设备）','enable_begin_date':'启用日期开始','enable_end_date':'启用日期截止','assets':'选中的设备信息，逗号拼接'}]<br/> assistList:[{'duty_type':'职责类型 1：执行，2：协助','self_or_out':'团队类型 1：内部 2：外部','total_hour':'计划总工时','user_id':'执行人的id','group_id':'协助团队的id','team_size':'团队人数'}]<br/> triggleInfo:[{'period_type':'周期类型1：单次2：按天3：按周4：按月5：年','execute_day':'单次时的日期','period_value':'周期值（天数、月数等，为年时，表示第几月）','type_day_select':'4、5时的选择日期类型，天，工作日，具体星期几','which_type_day':'4、5时选的第几周工作日或具体某一天','type_week_select':'3时周几：可多选，用“,”隔开','execute_hour':'在几时','execute_minute':'在几分','duration_time':'持续时间','duration_type':'持续时间类型'}] <br/> planJson:{'task_list':'任务项处数据，详见下'} <br/> 'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]<br/> conditionTriggle:[{'column_key':'字段code','column_condition':'>=判断条件','column_value':'75%值（数字或百分数）'}]<br/> " + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    //    String RSP_DESC_ADD_PLAN_WORK = "新增计划信息" + ResponseConstant.REQ_DESC_COMMON + "maintenanceObject:[{'position_codes':'设备位置树value字符串','asset_category_id':'设备类型','asset_model_id':'设备型号','is_all_asset':'勾选：true （全部选择）不勾选：false （手动选择设备）','enable_begin_date':'启用日期开始','enable_end_date':'启用日期截止','assets':'选中的设备信息，详细见下'}]<br/> 'assetInfoNew':[{'asset_id':'设备id'}]<br/> assistList:[{'assist_type':'职责类型 1：执行，2：协助','self_or_out':'团队类型 1：委内 2：委外','total_hour':'计划总工时','user_id':'执行人的id','facility_id':'协助团队的id','team_size':'团队人数'}]<br/> planJson:{'task_list':'任务项处数据，详见下'} <br/> 'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]<br/> " + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
//    String RSP_DESC_EDIT_PLAN_WORK = "编辑计划信息" + ResponseConstant.REQ_DESC_COMMON + "maintenanceObject:[{'position_codes':'设备位置树value字符串','asset_category_id':'设备类型','asset_model_id':'设备型号','is_all_asset':'勾选：true （全部选择）不勾选：false （手动选择设备）','enable_begin_date':'启用日期开始','enable_end_date':'启用日期截止','assets':'选中的设备信息，详细见下'}]<br/> 'assetInfoNew':[{'asset_id':'设备id'}]<br/> assistList:[{'assist_type':'职责类型 1：执行，2：协助','self_or_out':'团队类型 1：委内 2：委外','total_hour':'计划总工时','user_id':'执行人的id','facility_id':'协助团队的id','team_size':'团队人数'}]<br/> planJson:{'task_list':'任务项处数据，详见下'} <br/> 'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]<br/> " + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_PLAN_WORK_INFO = "获取计划详细信息" + ResponseConstant.RSP_DESC_COMMON + "{'plan_code':'维保计划主键','relation_type':'适用对象','assistList':[{'total_hour':'总工时','user_id':'执行人的id','team_size':'团队人数','user_name':'执行人姓名','plan_code':'维保计划主键','group_id':'协助团队的id','group_name':'协助团队的名称','id':'主键','self_or_out':'团队类型','duty_type':'职责类型：1执行，2协助','duty_type_name':'职责类型名称','self_or_out_name':'团队类型名称'}],'relation_type_name':'适用对象名称','work_type_name':'工单类型名称','work_type_id':'工单类型','allocate_receiver_type':'分配人员配置：1：自动指定 2：不分配','maintenanceObject':[{'asset_model_id':'设备型号id','assets':'设备的id字符串，逗号拼接','asset_category_id':'设备类型id','plan_code':'维保计划主键','enable_begin_date':'启用开始日期','is_all_asset':'勾选：true （全部选择）不勾选：false （手动选择设备）','setting_id':'每个维保对象的id','enable_end_date':'启用截止日期','position_codes':'设备位置字符换，逗号拼接'}],'is_use':'是否启用 0否，1是','do_type':'触发条件 1：所有条件满足执行；2：任一条件满足即执行','create_time':'创建时间','end_time':'截止日期','begin_time':'开始日期','generate_bill_type':'生成工单配置： 1：生成请求；2：直接生成工单','priority_level':'紧急程度:0:无；1:低；2:中；3:高；','planJson':{'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]},'plan_name':'任务名称','plan_hour':'预计总工时','pre_day':'预计完成需多少天','status':'计划状态','triggleInfo':[{'period_type':'周期类型1：单次2：按天3：按周4：按月5：年','execute_day':'单次时的日期','period_value':'周期值（天数、月数等，为年时，表示第几月）','type_day_select':'4、5时的选择日期类型，天，工作日，具体星期几','which_type_day':'4、5时选的第几周工作日或具体某一天','type_week_select':'3时周几：可多选，用“,”隔开','execute_hour':'在几时','execute_minute':'在几分','duration_time':'持续时间','duration_type':'持续时间类型'}] <br/>}";
    String RSP_DESC_SEARCH_PLAN_WORK_LIST = "获取维保计划列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'plan_code':'维保计划主键','plan_name':'计划名称','position_name':'位置','asset_name':'维保对象','work_type_id':'工单类型id','work_type_name':'工单类型名称','begin_time':'开始时间','end_time':'截止时间','is_use':'是否启用 1启用 0禁用'}]";
    String RSP_DESC_CHANGE_USE_PLAN_WORK = "启用禁用维修计划" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_PLAN_WORK_BY_SELECT = "删除选中维保计划" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_PLAN_ASSET_LIST = "获取维保计划设备列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'id':'设备主键','asset_code':'设备编码','asset_name':'设备名称','position_name':'设备位置','model_name':'设备型号','category_name':'设备类型','supplier_name':'供应商','supplier_id':'供应商id','status_name':'资产状态名称','status':'资产状态','enable_time':'启用日期','create_time':'创建时间'}]";
    String RSP_DESC_SEARCH_PLAN_TASK_TEMPLATE_LIST = "获取维保计划任务模板列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'itemnum':'任务项数目','task_template_name':'任务模板名称','task_template_code':'任务模板编号'}]";
    String RSP_DESC_SEARCH_PLAN_TASK_TEMPLATE_ITEM_LIST = "获取维保计划任务模板的任务项列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'requirements':'任务项描述','group_name':'组名','task_item_code':'任务项编码','task_item_name':'任务项名称'}]";
    String RSP_DESC_SEARCH_PLAN_TASK_ITEM_LIST = "获取维保计划标准任务项列表信息" + ResponseConstant.RSP_DESC_COMMON + "[{result_type:结果类型,task_item_name:任务项名称,task_item_code:任务项编码}]";
    String RSP_DESC_EXPORT_PLAN_WORK_CALENDAR = "导出选中维保计划" + ResponseConstant.RSP_DESC_COMMON + "[{result_type:结果类型,task_item_name:任务项名称,task_item_code:任务项编码}]";
    String RSP_DESC_EXPORT_ALL_PLAN_WORK_CALENDAR = "导出全部维保计划" + ResponseConstant.RSP_DESC_COMMON + "[{result_type:结果类型,task_item_name:任务项名称,task_item_code:任务项编码}]";

    /**
     * 工单管理接口描述
     */
    String RSP_DESC_SEARCH_WORK_LIST_PERMISSION = "获取工单模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isDetail':'是否拥有查看详情权限【boolean】'}}";
    String RSP_DESC_SEARCH_WORKS_LIST = "分页查询工单列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "['business_type_name':'业务类型名称','business_type_id':'业务id','position_name':'所属位置名称',{'status_name':'状态名称','create_user_id':'1353992786801913856','work_code':'工单编号','position_code':'所属位置','user_name':'执行人名称','begin_time':'开工时间','relation_type':2,'title':'工单主题','relation_id':'1f284c84942e449f90f52a0903f873fc','work_type_id':'工单类型id','asset_name':'设备名称','category_id':'设备类型id','asset_code':'设备编码','receive_user_id':'维修工id','status':'状态','category_name':'设备类型名称','work_type_name':'工单类型名称','duty_user_id':'执行人id','duty_user_name':'执行人名称','startCreateDateSearch':'开始日期-创建时间','endCreateDateSearch':'结束日期-创建时间'}]";
    String RSP_DESC_SEARCH_ASSIGNMENT_USER_LIST = "查询工单可分配用户列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'user_name':'用户名','id':'用户id'}]";
    String RSP_DESC_SEARCH_CANCEL_WORKS = "工单作废" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_WORKS_ASSIGNMENT = "工单分配" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_CODE_SCANNING_SIGN = "扫码签到" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";


    /**
     * 设备工单管理接口描述
     */
    String RSP_DESC_SEARCH_ASSET_WORK_LIST_PERMISSION = "获取设备工单模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isDetail':'是否拥有查看详情权限【boolean】'}}";
    String RSP_DESC_SEARCH_ASSET_WORKS_LIST = "分页查询设备工单列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'status':'状态','status_name':'状态名称','create_user_id':'提交人','work_code':'工单编号','position_code':'所属位置','position_name':'所属位置名称','asset_model_id':'设备型号','model_name':'设备型号','manufacturer_id':'制造商id','manufacturer_title':'制造商名称','supplier_title':'供应商名称','supplier_id':'供应商id','model_name':'设备型号','asset_name':'设备名称','work_type_id':'工单类型id','asset_name':'设备名称','asset_category_id':'设备类型id','asset_code':'设备编码','category_name':'设备类型名称','work_type_name':'工单类型名称','duty_user_id':'执行人id','duty_user_name':'执行人名称'}]";
    /**
     * 备件工单管理接口描述
     */
    String RSP_DESC_SEARCH_BOM_WORK_LIST_PERMISSION = "获取备件工单模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isDetail':'是否拥有查看详情权限【boolean】'}}";
    String RSP_DESC_SEARCH_BOM_WORKS_LIST = "分页查询备件工单列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'status':'状态','create_user_id':'提交人','work_code':'工单编号','sub_work_code':'工单详情编号','in_type':'入库类型','bom_model':'备件型号','bom_name':'备件名称','bom_type_name':'备件类型名称','type_id':'备件类型id','bom_name':'备件名称','create_time':'创建时间','work_type_id':'工单类型id','duty_user_id':'执行人id','duty_user_name':'执行人名称','create_user_id':'创建人id','create_user_name':'创建人名称'}]";

    /**
     * 统计分析管理接口
     */
    String RSP_DESC_SEARCH_REPORT_PERMISSION = "获取统计报表模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_STATISTICS_REPORT_DATA = "根据id获取统计报表数据" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'key':'value'},{'key':'value'}]";
    String RSP_DESC_SEARCH_STATISTICS_TABLE_DATA = "根据id获取统计表格数据" + "[{'key':'value'},{'key':'value'}]";
    String RSP_DESC_GET_STATISTICS_TABLE_DATA_EXPORT = "根据id导出统计表格数据" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_STATISTIC_LIST = "获取统计标题列表信息【text为dashboard的为首页展示的报表，别的都是分组名称，点击组名称行，分别调查看具体报表信息接口获取各自报表数据】" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'text':'多语言key的分组名称','list':[{'group_method_name':'组名称','group_name':'组名称id','description':'描述','id':'报表id','title':'标题'}],'value':'组名称id'}]";
    String RSP_DESC_REMOVE_SELF_STATISTIC = "删除选中个人看板信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_SELF_STATISTIC = "添加个人看板信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";


    /**
     * 首页接口
     */
    String RSP_DESC_SEARCH_HOME_WORKS_COUNT = " 首页的待办任务，今日上报，今日完成，超时任务数" + ResponseConstant.RSP_DESC_COMMON + "{'tasksCount':'待办任务'},{vreportWorksCount':'今日上报'},{'finishedWorksCount':'今日完成'},{'overtimeWorksCount':'超时任务'}";
    String RSP_DESC_CALENDAR_WORKS_COUNT = "首页的日历待办任务数" + ResponseConstant.RSP_DESC_COMMON + "[{'create_time': '日历-日期','count': '待办任务数'}]";
    String RSP_DESC_SEARCH_STATISTIC_SHOWDASHBOARD_LIST = "首页显示的图表" + ResponseConstant.RSP_DESC_COMMON + "[{'id':'报表id','options':'展示的列名','query':'图表sql','query_conditions':'从以下列中进行查询：','table_query':'表格sql','table_columns':'表格列名','title':'标题','description':'描述','is_show_dashboard':'是否显示在首页 1显示','client_page':'定制页面地址','group_name':'组名称'}]";

    /**
     * 字典管理接口
     */
    String RSP_DESC_SEARCH_CLOUD_DATA_TYPE_LIST = "获取数据项类型列表" + "[{'create_user_id':'创建人id','create_time':'创建时间','data_type':'类型编码','data_order':'排序','data_type_name':'类型名称','remark':'备注','is_lang':'是否是多语言 1是2不是'}]";
    String RSP_DESC_ADD_CLOUD_DATA_TYPE = "新增数据项类型表" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_EDIT_CLOUD_DATA_TYPE = "新增数据项类型表" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_REMOVE_CLOUD_DATA_TYPE = "删除数据项类型表" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_SEARCH_CLOUD_DATA_LIST_BY_DATA_TYPE = "根据数据项类型获取数据项列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'create_user_id':'创建人id','code':'值','create_time':'创建时间','name':'显示名称','data_type':'类型','reserve1':null,'data_order':'排序','parent_code':'父编码','remark':'备注','is_lang':'是否是多语言 1多语言 2不是','type_remark':'类型备注'}]";
    String RSP_DESC_ADD_CLOUD_DATA = "新增数据项表" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_EDIT_CLOUD_DATA = "更新数据项表" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";
    String RSP_DESC_REMOVE_CLOUD_DATA = "删除数据项表" + ResponseConstant.RSP_DESC_COMMON_LIST + "不返回任何信息";

    /**
     * 工单调度接口描述
     */
    String RSP_DESC_SEARCH_WORKSHEET_DISPATCH_PERMISSION = "获取工单调度模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_WORKSHEET_DISPATCH_DISTRIBUTION_LIST = "获取已分配工单列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'receive_name':'处理人姓名','receive_user_id':'处理人id','workload':'任务数量','overtimeCount':'超时任务量','workListModel':[{'work_code':'工单编号','work_type_id':'工单类型id','work_template_code':'工单模板编号','position_code':'设备位置code','relation_type':'适用对象','relation_id':'设备id或设备位置code','status':'工单状态id','status_name':'工单状态名称','create_user_id':'创建人/联系人id','title':'工单主题','receive_user_id':'处理人id','duty_user_id':'当前责任人id','duty_user_name':'当前责任人名称','user_name':'联系人名称','mobile':'联系人电话','work_type_name':'工单类型名称','position_name':'设备位置名称','priority_level_name':'紧急度名称','priority_level':'紧急度','category_id':'设备类型id','category_name':'设备类型名称','str_sub_work_code':'子工单字符串','sub_work_code':'子工单id','asset_code':'设备code','asset_name':'设备名称','business_name':'工单业务类型名称','business_type_id':'工单业务类型id','occur_time':'发生时间','deadline_time':'计划截止时间','create_time':'创建时间','receive_time':'接收时间','distribute_time':'分配时间','begin_time':'实际开工时间','finished_time':'实际完工时间','plan_arrive_time':'预计到达时间','plan_begin_time':'计划开工时间'}]}]";
    String RSP_DESC_SEARCH_WORKSHEET_DISPATCH_UNDISTRIBUTED_LIST = "获取待分配工单列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'work_code':'工单编号','work_type_id':'工单类型id','work_template_code':'工单模板编号','position_code':'设备位置code','relation_type':'适用对象','relation_id':'设备id或设备位置code','status':'工单状态id','status_name':'工单状态名称','create_user_id':'创建人/联系人id','title':'工单主题','receive_user_id':'处理人id','duty_user_id':'当前责任人id','duty_user_name':'当前责任人名称','user_name':'联系人名称','mobile':'联系人电话','work_type_name':'工单类型名称','position_name':'设备位置名称','priority_level_name':'紧急度名称','priority_level':'紧急度','category_id':'设备类型id','category_name':'设备类型名称','str_sub_work_code':'子工单字符串','sub_work_code':'子工单id','asset_code':'设备code','asset_name':'设备名称','business_name':'工单业务类型名称','business_type_id':'工单业务类型id','occur_time':'发生时间','deadline_time':'计划截止时间','create_time':'创建时间','receive_time':'接收时间','distribute_time':'分配时间','begin_time':'实际开工时间','finished_time':'实际完工时间','plan_arrive_time':'预计到达时间','plan_begin_time':'计划开工时间'}]";
    String RSP_DESC_SEARCH_WORKSHEET_LIST_BY_SUB_WORK_CODE = "根据子工单字符串获取工单列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'work_code':'工单编号','work_type_id':'工单类型id','work_template_code':'工单模板编号','position_code':'设备位置code','relation_type':'适用对象','relation_id':'设备id或设备位置code','status':'工单状态id','status_name':'工单状态名称','create_user_id':'创建人/联系人id','title':'工单主题','receive_user_id':'处理人id','duty_user_id':'当前责任人id','duty_user_name':'当前责任人名称','user_name':'联系人名称','mobile':'联系人电话','work_type_name':'工单类型名称','position_name':'设备位置名称','priority_level_name':'紧急度名称','priority_level':'紧急度','category_id':'设备类型id','category_name':'设备类型名称','str_sub_work_code':'子工单字符串','sub_work_code':'子工单id','asset_code':'设备code','asset_name':'设备名称','business_name':'工单业务类型名称','business_type_id':'工单业务类型id','occur_time':'发生时间','deadline_time':'计划截止时间','create_time':'创建时间','receive_time':'接收时间','distribute_time':'分配时间','begin_time':'实际开工时间','finished_time':'实际完工时间','plan_arrive_time':'预计到达时间','plan_begin_time':'计划开工时间'}]";

    /**
     * 库房管理接口描述
     */
    String RSP_DESC_SEARCH_STOCK_PERMISSION = "获取库房模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_STOCK_LIST = "获取库房列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'currency_name':'货币单位名称','address':'地址','stock_name':'库房名称','user_name':'管理员名称','id':'库房主键','is_use':'是否启用 1是 0否','stock_code':'库房编码','currency_id':'货币单位id','manager_user_id':'管理员id'}]";
    String RSP_DESC_ADD_STOCK = "新增库房信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_STOCK = "编辑库房信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_STOCK_INFO = "获取库房明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'currency_name':'货币单位名称','address':'地址','stock_name':'库房名称','user_name':'管理员名称','id':'库房主键','is_use':'是否启用 1是 0否','stock_code':'库房编码','currency_id':'货币单位id','manager_user_id':'管理员id','location':'地址坐标点','remark':'详细描述'}";
    String RSP_DESC_CHANGE_USE_STOCK = "启用禁用库房" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_STOCK_BY_SELECT = "删除选中库房信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_STOCK_BOM_LIST = "查询库房库存清单列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'unit_name':'单位名称','type_name':'备件类型名称','quantity':'数量','bom_model':'备件型号','type_id':'备件类型id','store_position':'库位','security_quantity':'安全库存','id':'库存表主键','unit_id':'单位id','bom_name':'备件名称','material_code':'物料编码','max_security_quantity':'最大安全库存'}]";
    String RSP_DESC_UPDATE_SAFE_STOCK = "设置库房安全库存" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_STOCK_IN_AND_OUT_LIST = "查询库房出入库记录列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'create_user_id':'操作人id','type_name':'备件销账','code':'工单号','quantity':'数量','create_time':'创建时间','stock_name':'库房名称','user_name':'操作人姓名','status_name':'工单状态名称','real_quantity':'最终数量（暂时不考虑）','stock_id':'库房id','bom_id':'备件id','bom_code':'备件编码','store_position':'库位','process_method':'操作方法','status':'工单状态'}]";
    String RSP_DESC_SEARCH_STOCK_BOM_IMPORT_LIST = "查询库房备件导入记录列表" + ResponseConstant.RSP_DESC_COMMON + "[{'bom_id':'备件id','stock_id':'库房id','bom_name':'备件名称','material_code':'物料编码','bom_type':'备件类型','bom_model':'规格型号','bom_unit':'单位','quantity':'数量'，'show_price':'价格', 'show_price':'价格', 'currency':'货币单位', 'supplier':'供应商', 'supply_cycle':'供货周期(天)', 'manufacturer':'制造商', 'service_life':'使用寿命(天)', 'bom_stock':'库房', 'security_quantity':'最小库存', 'max_security_quantity':'最大库存', 'store_position':'库位', 'create_time':'导入时间', 'create_user_id':'操作人', 'remark':'备注'}]";

    /**
     * 统计配置接口描述
     */
    String RSP_DESC_SEARCH_STATISTIC_CONFIG_LIST_PERMISSION = "获取统计分析配置列表模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_SEARCH_STATISTIC_CONFIG_LIST = "获取统计配置列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'group_method_name':'分组名称','group_name':'分组id','description':'描述','is_show_dashboard':'是否看板显示： 0否，1是','id':'系统配置主键','title':'标题'}]";
    String RSP_DESC_ADD_STATISTIC_CONFIG = "新增统计配置信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_STATISTIC_CONFIG = "编辑统计配置信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_STATISTIC_CONFIG = "获取统计配置明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'client_page':'定制页面地址','group_method_name':'分组名称','export_url':'导出地址','create_user_id':'创建人id','create_time':'创建时间','group_name':'分组id','query':'表格加密后的sql','description':'描述','title':'标题','table_columns':'表格展示字段的jsonarray字符串','table_query':'表格查询加密后的sql','query_conditions':'查询条件的jsonarray字符串','options':'表格显示参数的json字符串','is_show_dashboard':'是否看板显示： 0否，1是','id':'主键'} <br/> 'query_conditions':[{'smul':true,'qname':'search_date_default','qtype':'search_date_default','sokey':'1','soval':'30','qtitle':'1133','svalue':'1'}]";
    String RSP_DESC_REMOVE_STATISTIC_CONFIG_BY_SELECT = "删除选中统计配置信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_USE_STATISTIC_CONFIG = "展示看板" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";

    /**
     * 设备地图接口描述
     */
    String RSP_DESC_SEARCH_ASSET_MAP_PERMISSION = "获取设备地图权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isDelete':'是否拥有删除权限【boolean】'}}";
    String RSP_DESC_GET_ASSET_MAP_POSITION = "获取全部设备位置" + ResponseConstant.RSP_DESC_COMMON + "[{'parent_position_name':'父位置名称','position_code':'设备位置编码','layer_path':'组织图层id','file_id':'位置附件id','position_name':'位置名称','parent_code':'父位置编码','location':'(1,1)--》图层上点位置坐标点字符串','remark':'位置描述','facility_type_id':'位置类型id','status':'1：正常 -1000：删除','high_light':'是否高亮显示， 0 否 1 是','is_fault':'是否存在故障， 0 否 1 是','work_count':'任务数量---任务模式下需要'}]";
    String RSP_DESC_GET_ASSET_MAP_ASSET_AND_WORK = "获取位置的设备及子位置数据" + ResponseConstant.RSP_DESC_COMMON + "{'file_width':'图层图片的宽度','file_url':'图片地址','parent_position_name':'父位置名称','position_code':'位置编码','position_name':'位置名称','remark':'位置描述','list':[{'parent':'父位置编码或父设备编码','high_light':'是否高亮显示， 0 否 1 是','is_fault':'是否存在故障， 0 否 1 是','work_count':'任务数量---任务模式下需要','name':'设备名称或位置名称','location':'(1,1)--》图层上点坐标点字符串','id':'设备id或位置编码','type':'该点类型： asset-设备点  position-位置点','status':'状态'}],'file_height':'图层图片的高度','layer_path':'图层图片的id','file_id':'位置附件id','parent_code':'父位置编码','location':'(1,1)---在父位置图层图片上的点位置','facility_type_id':'位置类型','status':'状态'}";
    String RSP_DESC_SEARCH_ASSET_MAP_DETAIL = "获得选择位置或设备的信息" + ResponseConstant.RSP_DESC_COMMON + "childPosition--类型为位置时会出现，子位置信息；position--位置的基本信息；asset-设备的基本信息；works--代办信息列表数据<br/> {'works':[{'work_code':'工单编号','work_type_id':'工单类型id','work_template_code':'工单模板编号','position_code':'设备位置code','relation_type':'适用对象','relation_id':'设备id或设备位置code','status':'工单状态id','status_name':'工单状态名称','create_user_id':'创建人/联系人id','title':'工单主题','receive_user_id':'处理人id','duty_user_id':'当前责任人id','duty_user_name':'当前责任人名称','user_name':'联系人名称','mobile':'联系人电话','work_type_name':'工单类型名称','position_name':'设备位置名称','priority_level_name':'紧急度名称','priority_level':'紧急度','category_id':'设备类型id','category_name':'设备类型名称','sub_work_code':'子工单id','asset_code':'设备code','asset_name':'设备名称','business_name':'工单业务类型名称','business_type_id':'工单业务类型id','occur_time':'发生时间','deadline_time':'计划截止时间','create_time':'创建时间','receive_time':'接收时间','distribute_time':'分配时间','begin_time':'实际开工时间','finished_time':'实际完工时间','plan_arrive_time':'预计到达时间','plan_begin_time':'计划开工时间'}],'childPositions':[{'type_name':'位置类型名称','position_type_id':'位置类型id','position_code':'位置编码','layer_path':'图层id','position_name':'位置名称','location':'(1,1)--在父位置图层点坐标','facility_type_id':'位置类型'}],'position':{'position_code':'位置编码','position_name':'位置名称','remark':'位置描述'},'asset':{'result_type_name':'保固状态名称','asset_name':'设备名称','asset_code':'设备编码','status_name':'资产状态名称','guarantee_status_id':'保固状态id','id':'设备id','running_status_name':'运行状态名称','running_status_id':'运行状态id','status':'状态'}}";

    /**
     * 维保行事历接口描述
     */
    String RSP_DESC_SEARCH_PLAN_WORK_CALENDAR_LIST_PERMISSION = "获取库房模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_PLAN_WORK_CALENDAR_LIST = "获取维保行事历列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'plan_code':'维保计划编号','work_type_id':'工单类型id','asset_name':'设备名称','occur_time':'发生时间','position_code':'位置编码','position_name':'位置名称','deadline_time':'截止时间','work_calendar_code':'行事历id','title':'计划工单名称','relation_id':'设备id','work_type_name':'工单类型名称','status':'行事历状态 1：未转工单；2：已转工单；3：已过期','auto_generate_bill':'是否自动生成工单 ： 1：生成 2：不生成','user_name':'负责人','begin_time':'日历格式中列表展示的开始时间（时分格式）'}]";
    String RSP_DESC_GENERATE_WORK = "计划行事历生成工单" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_UPDATE_WORK = "计划行事历编辑" + " planJson:{'task_list':'任务项处数据，详见下'} <br/> 'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]<br/> " + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_WORK = "计划行事历作废" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_PLAN_CALENDAR_BY_SELECT = "作废选中计划行事历" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_PLAN_WORK_CALENDAR_INFO = "获取计划行事历详细信息" + ResponseConstant.RSP_DESC_COMMON + "{'type_name':'工单类型名称','plan_deal_time':'预计工时（分）','position_code':'设备位置编码','work_template_code':'工单模板编码','plan_code':'计划编码','remark':'备注描述','work_calendar_code':'行事历编码主键','relation_type':'适用对象','title':'标题','problem_note':'描述（问题）','work_type_id':'工单类型id','asset_name':'设备名称','occur_time':'开始时间','deadline_time':'结束时间','logs':[{'create_user_id':'创建人id','create_time':'创建时间','create_user_name':'创建人','remark':'[信息]'}],'auto_generate_bill':'是否自动生成工单','create_user_id':'创建人id','create_time':'创建时间','position_name':'设备位置名称','generate_time_point':'自动生成工单时间点','priority_level':'紧急程度','planJson':{'task_list':[{'is_use':'是否启用 0否，1是','order':'排序','group_name':'组名','task_item_name':'任务项名称','task_item_code':'任务项编码','requirements':'任务描述','result_type':'结果类型','check_method':'检查方法'}]},'relation_id':'设备id','assist':{'duty_type':'职责类型','total_hour':'时间','receive_user_name':'人名','group_id':'组织id','group_name':'组织名称','team_size':'人数','duty_type_name':'职责名称','receive_user_id':'人id','self_or_out':'团队类型','self_or_out_name':'团队类型名称'},'status':'行事历状态'}";

    /**
     * 获取缓存列表
     */
    String RSP_DESC_SEARCH_CACHE_LIST = "获取缓存列表" + ResponseConstant.RSP_DESC_COMMON + "缓存列表";
    String RSP_DESC_CLEAR_CACHE_BY_NAME = "根据缓存名称清除缓存" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CLEAR_CACHE_BY_KEY = "根据缓存名称和键值清除缓存" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_ERROR_LOG_LIST = "获取错误日志列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'error_code':'错误码','description':'描述','create_time':'发生时间','user_name':'操作人','remark':'备注'}]";

    /**
     * 生产作业单接口描述
     */
    String RSP_DESC_SEARCH_PRODUCT_TASK_PAGE = "获取生产作业单列表" + ResponseConstant.RSP_DESC_COMMON + "{'total':1,'rows':[{'un_do_count':'未完成数量','create_user_id':'创建人id','product_task_code':'生产作业单编码','create_time':'创建时间','create_user_name':'创建人','remark':'备注'}]}";
    String RSP_DESC_SEARCH_PRODUCT_TASK_INFO = "获取生产作业单详情" + ResponseConstant.RSP_DESC_COMMON + "{'create_user_id':'创建人id','product_task_code':'生产作业单编码','create_time':'创建时间','create_user_name':'创建人','remark':'备注'}";
    String RSP_DESC_SEARCH_PRODUCT_TASK_WELL_INFO = "获取生产作业下井详情" + ResponseConstant.RSP_DESC_COMMON + "{'max_temperature':'最高井温（℃）','bottom_pressure':'井底压力（Mpc）','id':'1','asset_out_code':'出库单号'}";
    String RSP_DESC_SEARCH_PRODUCT_TASK_WELL_ITEM_INFO = "获取生产作业下井次数详情" + ResponseConstant.RSP_DESC_COMMON + "{'come_out_time':'出井时间','id':'1','go_down_time':'下井时间','product_task_well_item_asset_list':[{'category_name':'设备类型','asset_name':'设备名称','category_id':'设备类型id','asset_code':'设备编码','asset_id':'设备id','task_well_item_code':'生产作业次数编码'}]}";
    String RSP_DESC_SEARCH_PRODUCT_TASK_WELL_LIST = "获取生产作业单明细列表" + ResponseConstant.RSP_DESC_COMMON + "[{'actual_arrival_time':'实际到井时间 ','asset_out_code':'出库单号，多个逗号隔开','bit_size':'钻头尺寸','bottom_pressure':'井底压力（Mpc）','branch_office':'分公司','breakdown_time':'故障时间','bridge_plug_type':'桥塞类别','cable_joint_location':'电缆接头位置','casing_size':'套管尺寸','come_out_time':'出井时间','construction_technology':'施工工艺','core_harvested_number':'收获岩心颗数','demolition_time':'拆除时间','departure_time':'出发时间','depth_of_encounter':'遇卡深度','downhole_instrument_failures_times':'井下仪器故障次数','go_down_count':'下井次数','go_down_time':'下井时间','ground_instrument_series':'地面仪器系列','gun_type':'枪型','id':'编码','installation_time':'安装时间','is_contain_hydrogen_sulfide':'是否含硫化氢','is_use_source':'是否用源','max_temperature':'最高井温（℃）','maximum_slope':'最大斜度','measuring_meter':'测量米','net_tension_jamming':'遇卡净张力','one_way_kilometers':'单程公里数  ','operation_items':'作业项目','operation_purpose':'作业目的','parent_id':'父编码','party_a':'甲方单位','perforation_section':'射孔段','product_task_code':'产作业编码','project_department':'项目部 ','ranks':'队伍','return_time':'返回时间','several_cores_are_designed':'取心设计数颗','success_times':'成功次数','survey_section_from':'测量井段从','survey_section_to':'测量井段到','task_code':'任务单号','task_notes':'任务单备注 ','task_type':'任务单类型','total_shooting_thickness':'射开总厚度','total_tension_jamming':'遇卡总张力','total_time':'总时间','total_vehicle_kilometers':'车辆行驶总公里数','un_do_count':'未完成数量','well_depth':'井深','well_no':'井号','well_occupation_time':'占井时间 ','well_type':'井型 '}]";
    String RSP_DESC_EDIT_PRODUCT_TASK = "更新生产作业单信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_PRODUCT_TASK_WELL = "更新生产作业下井信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_PRODUCT_TASK_WELL_ITEM = "更新生产作业下井次数信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_PRODUCT_TASK = "删除生产作业信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_PRODUCT_TASK_WELL = "删除生产作业下井信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_PRODUCT_TASK_WELL_ITEM = "删除生产作业下井次数信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_PRODUCT_TASK_WELL_ITEM_ASSET_LIST = "查询出库单下设备列表列表" + ResponseConstant.RSP_DESC_COMMON + "[{'category_name':'设备类型','asset_name':'设备名称','asset_code':'设备编码（设备流水码）','asset_id':'设备id','status':'设备资产状态','running_status':'设备运行状态'}]";

    /**
     * 日常报告
     */
    String RSP_DESC_SEARCH_DAILY_REPORT_LIST = "获取日常报告列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'create_time':'生成时间','group_name':'','end_time':'截止时间','position_name':'组织名称','begin_time':'开始时间','id':'主键','report_name':'报告名称'}]";
    String RSP_DESC_ADD_DAILY_REPORT = "新增日常报告信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_DAILY_REPORT = "删除日常报告信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_DOWNLOAD_DAILY_REPORT = "下载日常报告信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_DAILY_REPORT_NT = "新增日常报告信息NT" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_DOWNLOAD_DAILY_REPORT_NT = "下载日常报告信息NT" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REPORT_EXIST = "报告是否已生成" + ResponseConstant.RSP_DESC_COMMON + "{'exist':'true/false'}";
    String RSP_DESC_SEARCH_DAILY_REPORT_LIST_NT = "获取日常报告列表信息NT" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'create_time':'生成时间','group_name':'','end_time':'截止时间','position_name':'组织名称','begin_time':'开始时间','id':'主键','report_name':'报告名称'}]";
    String RSP_DESC_REMOVE_DAILY_REPORT_NT = "删除日常报告信息NT" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";

    /**
     * 消息管理
     */
    String RSP_DESC_SEARCH_MESSAGE_PAGE_BY_RECEIVER = "分页查询个人消息" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'total':1,'rows':[{'send_user_name':'发送人姓名','receive_user_name':'接收人姓名','is_read':'是否已读 1已读 -1未读','send_time':'发送时间','send_user_id':'发送人','msg_content':'消息内容','receive_user_id':'接收人','id':'消息id','type':'消息类型 1：短信 2邮箱 3：消息通知'}]}";
    String RSP_DESC_SEARCH_NO_READ_COUNT_BY_RECEIVER = "查询接收人的未读消息数量" + ResponseConstant.RSP_DESC_COMMON_LIST + "{ content: 未读消息数量}";
    String RSP_DESC_SEARCH_NO_READ_THREE_COUNT_BY_RECEIVER = "分页查询最新三个未读消息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'send_user_name':'发送人姓名','receive_user_name':'接收人姓名','is_read':'是否已读 1已读 -1未读','send_time':'发送时间','send_user_id':'发送人','msg_content':'消息内容','receive_user_id':'接收人','id':'消息id','type':'消息类型 1：短信 2邮箱 3：消息通知'}]";
    String RSP_DESC_SEARCH_MESSAGE_INFO_BY_ID = "根据消息id获取消息详情" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'send_user_name':'发送人姓名','receive_user_name':'接收人姓名','receive_mobile':'接收号码','send_time':'发送时间','send_user_id':'发送人','msg_content':'消息内容','business_type':'业务类型','business_no':'业务编码','id':'消息id','is_success':'是否发送成功 1成功-1未成功','type':'消息类型 1：短信 2邮箱 3：消息通知'}";
    String RSP_DESC_EDIT_MESSAGE_RECEIVE_READ = "阅读消息，把消息改为已读" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";

    /**
     * 大屏看板
     */
    String RSP_DESC_SEARCH_REPAIR_TASK_LIST = "待办的维修工单列表" + ResponseConstant.RSP_DESC_COMMON_LIST + "{[{'work_type_id':'工单类型id','work_type':'工单类型','create_time':'发起时间','task_id':'待办流程id','deadline_time':'截止时间','title':'流程标题','assignee_name':'处理人','sub_work_code':'子工单编号'}]";
    String RSP_DESC_SEARCH_MAINTAIN_TASK_LIST = "待办的维护工单" + ResponseConstant.RSP_DESC_COMMON_LIST + "{[{'work_type_id':'工单类型id','work_type':'工单类型','create_time':'发起时间','task_id':'待办流程id','deadline_time':'截止时间','title':'流程标题','assignee_name':'处理人','sub_work_code':'子工单编号'}]";
    String RSP_DESC_SEARCH_REPAIR_FINISHED_COUNT = "故障任务总数" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'finishedCount':'完成数','noFinishedCount':'未完成数','total':'总数'}";
    String RSP_DESC_SEARCH_MAINTAIN_FINISHED_COUNT = "维护任务总数" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'finishedCount':'完成数','noFinishedCount':'未完成数','total':'总数'}";
    String RSP_DESC_SEARCH_ON_DUTY_USER_LIST = "当班人员名单" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'user_name':'姓名','groupPositionName':'岗位','mobile':'电话'}";
    String RSP_DESC_SEARCH_FINISHED_TASK_COUNT = "今日完成本周完成本月完成" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'monthCount':'本月完成','weekCount':'本周完成','dayCount':'今日完成'}";
    String RSP_DESC_SEARCH_ASSET_BY_ORGUTILIZATION_RATE = "（本月设备利用率）查询本月组织设备的利用率" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'repairtime':'故障时间','utilizationrate':'设备利用率','runningtime':'运行时间','maintime':'维保时间'}";
    String RSP_DESC_SEARCH_ASSET_RUNNINGTIME = "（本月设备利用率）本月运行时间故障时间维护时间" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'repairtime':'本月故障时间','runtime':'本月运行时间','maintime':'本月维护时间'}";
    String RSP_DESC_SEARCH_NOW_YEAR_RATE_LIST = "（本月设备利用率）设备利用率趋势" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'totaltime':'总时间','month':'月份','breakdowntime':'故障时间','rate':'设备利用率'}]";
    String RSP_DESC_SEARCH_REPAIR_COUNT = "（故障情况）今日故障数本周故障数本月故障数" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'nowmonthrepaircount':'本月故障数','todayrepaircount':'本日故障数','nowweekrepaircount':'本周故障数'}";
    String RSP_DESC_SEARCH_TODAY_REPAIR_SITUATION = "（故障情况）当天修复情况" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'inner_code':'组织编码','finishedCount':'故障完成数','noFinishedCount':'未完成数'}";
    String RSP_DESC_SEARCH_TODAY_INTACT_RATE = "（故障情况）当天完好率" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'repairassetcount':'故障数','goodassetrate':'完好数','goodassetcount':'完好率'}";
    String RSP_DESC_SEARCH_FAULT_DISTRI_BUTION = "（故障情况）当天故障分布" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'nowmonthrepaircount':'本月故障数','todayrepaircount':'本日故障数','nowweekrepaircount':'本周故障数'}";
    String RSP_DESC_SEARCH_FAULT_REND = "（故障情况）故障趋势" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'finishedcount':'完成数','finishedrate':'完成率','create_date':'日期','nofinishedcount':'未完成数'}]";
    String RSP_DESC_SEARCH_MAIN_COUNT = "（维护状况）今日维护数本周维护数本月维护数" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'nowmonthmaincount':'本月维护数','todaymaincount':'本日维护数','nowweekmaincount':'本周维护数'}";
    String RSP_DESC_SEARCH_TODAY_EQUIPMENT_SPOT_CHECK = "（维护状况）当天设备点检" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'inner_code':'组织编码','finishedRate':'完成率','nowweekmaincount':'本周维护数'}";
    String RSP_DESC_SEARCH_TODAY_ROUTINE_MAINTENANCE = "（维护状况）当天常规维护" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'finishedRate':'完成率'";
    String RSP_DESC_SEARCH_MONTH_ROUTINE_MAINTENANCE = "（维护状况）当月常规维护" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'finishedRate':'完成率'";
    String RSP_DESC_SEARCH_MAINT_REND = "（维护状况）维护趋势" + ResponseConstant.RSP_DESC_COMMON_LIST + "{'finishedRate':'完成率','finishedCount':'完成数','noFinishedCount':'未完成数量'}";
    String RSP_DESC_SEARCH_ASSET_REPAIR_STATUS_FOR_SCREEN = "获取所有位置的设备的故障状态" + ResponseConstant.RSP_DESC_COMMON + "[{'asset_status':'设备状态 -1：正常，1：维修，2：保养','asset_name':'设备名称','location':'设备坐标'}]";


    /**
     * 职安健
     */
    String RSP_DESC_SEARCH_SECURITY_MANAGE_INFO = "查询职安健" + ResponseConstant.RSP_DESC_COMMON + "{'security_item_code':'编码','security_item_name':'风险评估','rask_note':'预防措施','result_type':'职安健审批 1是2否','standard':'标准'}";
    String RSP_DESC_SEARCH_SECURITY_MANAGE_PAGE = "查询职安健分页" + ResponseConstant.RSP_DESC_COMMON_LIST + "{total:1,rows:[{'security_item_code':'编码','security_item_name':'风险评估','rask_note':'预防措施','result_type':'职安健审批 1是2否','standard':'标准'}]}";
    String RSP_DESC_SEARCH_ALL_SECURITY_MANAGE_LIST = "查询职安健分页" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'security_item_code':'编码','security_item_name':'风险评估','rask_note':'预防措施','result_type':'职安健审批 1是2否','standard':'标准'}]";
    String RSP_DESC_ADD_SECURITY_MANAGE = "新增职安健" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_MODIFY_SECURITY_MANAGE = "更新职安健" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_SECURITY_MANAGE = "删除职安健" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    /**
     * 星巴克poc
     */
    String RSP_DESC_SEARCH_IOT_ASSET_CATEGORY_LIST = "查询监控设备类型列表" + ResponseConstant.RSP_DESC_COMMON + "[{'category_name':'设备类型名称','category_code':'设备类型编码','id':'主键'}]";
    String RSP_DESC_SEARCH_IOT_ASSET_AND_POINTS_LIST = "查询设备以及设备监控信息列表" + ResponseConstant.RSP_DESC_COMMON + "[{'asset_name':'设备名称','monitor':[{'gather_time':'采集时间','monitor_value_unit':'单位','asset_name':'设备名称','monitor_value':'监测值','asset_code':'设备编号','id':'主键','monitor_name':'监测项名称'}],'asset_id':'设备id'}]";
    String RSP_DESC_SEARCH_ASSET_CURRENT_MONITOR_LIST_PAGE = "分页查询设备实时监测数据" + ResponseConstant.RSP_DESC_COMMON + "[{'gather_time':'采集时间','monitor_value_unit':'单位','monitor_value':'监测值','id':'主键','monitor_name':'监控项名称'}]";
    String RSP_DESC_SEARCH_ASSET_HISTORY_MONITOR_LIST_PAGE = "分页查询设备历史监测数据" + ResponseConstant.RSP_DESC_COMMON + "[{'gather_time':'采集时间','monitor_value_unit':'单位','monitor_value':'监测值','id':'主键','monitor_name':'监控项名称'}]";
    String RSP_DESC_UPDATE_ASSET_MONITOR_CURRENT_INFO = "更新设备实时信息" + ResponseConstant.RSP_DESC_COMMON + "[{'asset_id':'设备ID','monitor_name':'被更改属性的名称','monitor_value':'被更改属性的值'}]";
    /**
     * 多语言
     */
    String RSP_DESC_GET_LANG_LIST_PERMISSION = "获取多语言模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】'}}";
    String RSP_DESC_GET_LANGUAGES_LIST = "查询多语言列表" + ResponseConstant.RSP_DESC_COMMON + "[{'use_type':'用途描述','key':'键值','from_name':'企业名'}]";
    String RSP_DESC_GET_SELF_LANG_LIST = "获取企业多语言列表（含手机端）" + ResponseConstant.RSP_DESC_COMMON + "[{'use_type':'用途描述','key':'键值'}]";
    String RSP_DESC_ADD_LANGUAGES = "新增多语言" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_LANGUAGES = "编辑多语言" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_ADD_SELF_LANGUAGES = "新增多语言（企业）" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_SELF_LANGUAGES = "编辑多语言（企业）" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_BUILD_LANG = "发布多语言" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_BUILD_LANG_BY_COMPANY = "发布多语言（企业）" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";

    /**
     * 知识库接口描述
     */
    String RSP_DESC_SEARCH_KNOWLEDGE_BASE_LIST_PERMISSION = "获取知识库模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isImport':'是否拥有导入权限【boolean】'}}";
    String RSP_DESC_SEARCH_KNOWLEDGE_BASE_LIST = "获取知识库列表信息" + ResponseConstant.RSP_DESC_COMMON_LIST + "[{'file_original_name':'文件原名称','create_user_id':'创建人','create_time':'创建时间','work_type_id':'工单类型','asset_code':'设备编号','asset_name':'设备名称','work_code':'工单号','business_no':'所属模块','file_category_id':'文件类型'}]";
    String RSP_DESC_GET_KNOWLEDGE_BASE_INFO = "获取知识库明细信息" + ResponseConstant.RSP_DESC_COMMON + "{'file_original_name':'文件原名称','create_user_id':'创建人','create_time':'创建时间','work_type_id':'工单类型','asset_code':'设备编号','asset_name':'设备名称','work_code':'工单号','business_no':'所属模块','file_category_id':'文件类型'}";
    String RSP_DESC_GET_EXPORT_KNOWLEDGE_BASE_CODE = "导出选中知识库信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'知识库导出码'";
    String RSP_DESC_GET_EXPORT_ALL_KNOWLEDGE_BASE_CODE = "导出全部知识库信息" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'知识库导出码'";
    // 接口描述-end

    /**
     * 客户管理接口描述
     */
    String RSP_DESC_SEARCH_CUSTOMERS_LIST_PERMISSION = "获取客户模块权限" + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{'isAdd':'是否拥有新增权限【boolean】','isEdit':'是否拥有编辑权限【boolean】','isImport':'是否拥有导入权限【boolean】','isExport':'是否拥有导出权限【boolean】','isCode':'是否拥有导出二维码权限【boolean】','isDelete':'是否拥有删除权限【boolean】', 'isFee':'是否拥有费用查看权限【boolean】'}}";
    String RSP_DESC_SEARCH_CUSTOMERS_LIST = "查询客户列表" + ResponseConstant.RSP_DESC_COMMON + "['is_use':'是否启用 1启用2不启用','layer_path':'组织图层','address':'地址','create_time':'创建时间','org_type':'组织类型','remark':'描述','title':'名称','parent_id':'父级厂商id','inner_code':'厂商编码','create_user_account':'创建人','short_title':'厂商简称','location':'坐标','currency_id':'货币单位','paid_status':'缴费状态','unloading_group_code':'并管排污分组','unloading_time':'排污时间'}]";
    String RSP_DESC_ADD_CUSTOMERS = "新增客户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_CUSTOMERS = "编辑客户" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_CUSTOMERS_INFO = "查询客户详情" + ResponseConstant.RSP_DESC_COMMON + "{'is_use':'是否启用 1启用2不启用','layer_path':'组织图层','address':'地址','create_time':'创建时间','org_type':'组织类型','remark':'描述','title':'名称','parent_id':'父级厂商id','inner_code':'厂商编码','create_user_account':'创建人','short_title':'厂商简称','location':'坐标','currency_id':'货币单位','unloading_group_id':'并管组','position_code':'片区','pipeline_code':'管道编号'," +
            "'sewage_destination':'污水去向','main_products':'主要产品','province':'省','city':'市','zone':'区'," +
            "'important_level_id':'重要级别','customer_type_id':'客户类型','unloading_time':'排污时间'}";

    String RSP_DESC_SEARCH_RELATION_CUSTOMERS_LIST = "查询关联客户列表" + ResponseConstant.RSP_DESC_COMMON + "['title':'名称','inner_code':'厂商编码','customer_type_id':'客户类型'}]";
    String RSP_DESC_SEARCH_RELATION_ASSET_LIST = "查询关联设备列表" + ResponseConstant.RSP_DESC_COMMON + "['asset_code':'设备编码','asset_name':'设备名称','status':'状态','category_id':'设备类型','asset_model_id':'设备型号','position_code':'设备位置'}]";
    String RSP_DESC_EDIT_RELATION_ASSET = "关联设备编辑" + ResponseConstant.RSP_DESC_COMMON + "['asset_code':'设备编码','asset_name':'设备名称','status':'状态','category_id':'设备类型','asset_model_id':'设备型号','position_code':'设备位置'}]";
    String RSP_DESC_SEARCH_POLLUTE_FACTOR_LIST = "查询污染因子列表" + ResponseConstant.RSP_DESC_COMMON + "['facility_id':'客户编码','factor_code':'污染因子','limit_value':'限值','limit_unit':'单位','begin_date':'开始日期','end_date':'结束日期','status':'状态'}]";
    String RSP_DESC_ADD_POLLUTE_FACTOR = "新增污染因子" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_POLLUTE_FACTOR = "编辑污染因子" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_POLLUTE_FACTOR = "删除污染因子" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_UNLOADING_TIME_LIST = "查询排污时间列表" + ResponseConstant.RSP_DESC_COMMON + "['facility_id':'客户编码','begin_time':'开始时间','end_time':'结束时间','begin_date':'开始日期','end_date':'结束日期','status':'状态'}]";
    String RSP_DESC_EDIT_UNLOADING_TIME = "编辑排污时间" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_REMOVE_NLOADING_TIME = "删除排污时间" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_EXPORT_CUSTOMER = "导出选中客户" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'客户导出'";
    String RSP_DESC_GET_EXPORT_ALL_CUSTOMER = "导出全部客户" + ResponseConstant.RSP_DESC_COMMON_EXPORT + "'客户导出'";
    String RSP_DESC_SEARCH_CUSTOMER_LOG_LIST = "获得客户所有的日志" + ResponseConstant.RSP_DESC_COMMON + "[{'remark':[{'remark':'日志描述','key':'日志内容','params':['日志参数']}],'create_user_account':'创建人账号','create_user_name':'创建人姓名','create_time':'创建时间'}]";
    String RSP_DESC_SEARCH_ASSET_POSITION_FOR_MAP = "获取位置的设备及子位置数据" + ResponseConstant.RSP_DESC_COMMON + "{'file_width':'图层图片的宽度','file_url':'图片地址','parent_position_name':'父位置名称','position_code':'位置编码','position_name':'位置名称','remark':'位置描述','list':[{'parent':'父位置编码或父设备编码','high_light':'是否高亮显示， 0 否 1 是','work_count':'任务数量---任务模式下需要','name':'设备名称或位置名称','location':'(1,1)--》图层上点坐标点字符串','id':'设备id或位置编码','type':'该点类型： asset-设备点  position-位置点','status':'状态'}],'file_height':'图层图片的高度','layer_path':'图层图片的id','file_id':'位置附件id','parent_code':'父位置编码','location':'(1,1)---在父位置图层图片上的点位置','facility_type_id':'位置类型','status':'状态'}";
    String RSP_DESC_SEARCH_ALL_CUSTOMER_POSITION_FOR_MAP = "获取所有客户位置数据" + ResponseConstant.RSP_DESC_COMMON + "[{'is_use':'是否启用 1启用2不启用','layer_path':'组织图层','address':'地址','create_time':'创建时间','org_type':'组织类型','remark':'描述','title':'名称','parent_id':'父级厂商id','inner_code':'厂商编码','create_user_account':'创建人','short_title':'厂商简称','location':'坐标','currency_id':'货币单位','paid_status':'缴费状态','unloading_group_code':'并管排污分组','unloading_time':'排污时间','works':'服务请求数量'}]";
    String RSP_DESC_SEARCH_CUSTOMER_MAP_DETAIL = "获取客户地图明细" + ResponseConstant.RSP_DESC_COMMON + "[{'is_use':'是否启用 1启用2不启用','layer_path':'组织图层','address':'地址','create_time':'创建时间','org_type':'组织类型','remark':'描述','title':'名称','parent_id':'父级厂商id','inner_code':'厂商编码','create_user_account':'创建人','short_title':'厂商简称','location':'坐标','currency_id':'货币单位','paid_status':'缴费状态','unloading_group_code':'并管排污分组','unloading_time':'排污时间','works':'服务请求数量','water_quality':'水质'}]";
    String RSP_DESC_SEARCH_CUSTOMER_CHECK_METER_HISTORY = "获取客户排污历史" + ResponseConstant.RSP_DESC_COMMON + "[{'average':'日平均排污量','year_month':'年月','total':'排污量','last_month_average':'上月日均排污量','last_year_average':'上年日均排污量'}]";
    String RSP_DESC_SEARCH_CUSTOMER_WORKS_HISTORY = "获取客户互动记录" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_SEARCH_CUSTOMER_CHECK_METER_LIST = "获取客户排污量" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_SEARCH_CUSTOMER_POLLUTE_FEE_LIST = "获取客户缴费明细" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_SEARCH_ALL_CUSTOMER_ASSET_POSITION_FOR_MAP = "获取所有客户设备位置数据" + ResponseConstant.RSP_DESC_COMMON + "[{'location':'坐标','postion_code':'编号','position_name':'位置'}}]";
    String RSP_DESC_GET_CUSTOMER_FILE_LIST = "获取客户附件文档信息" + ResponseConstant.RSP_DESC_COMMON + "{'code':200,'msg':null,'content':[{'id':7085：附件id,'remark':null：备注,'is_use':true：true正常false删除,'create_time_str':'2020-04-2410:25:16：创建时间','fileName':'ea3fdb82-272a-4243-a0c2-b25ab221a0d0.jpg：文件名称','create_time':1587695116272,'fileOriginalName':'tmp_b433cd29517adb425ae3f20c7069de66e5977404f5df793a.jpg：文件原始名称','createUserAccount':'fsm1：创建人账号','fileType':1文件类型1图片2文档,'fileUrl':'aaa.jpg：文件路径','fileSize':114.0：文件大小,'fileHeight':1920：高度（如果是图片）,'fileWidth':960：宽度（如果是图片）}]}";
    String RSP_DESC_ADD_CUSTOMER_CONTACT = "新增客户联系人" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_CUSTOMER_CONTACT = "编辑客户联系人" + ResponseConstant.RSP_DESC_COMMON + "{'contact_name':'联系人名称','wei_xin':'微信号','org_id':'厂商id','contact_mobile':'手机号','id':713,'contact_email':'邮箱','id':厂商联系人主键id}";
    String RSP_DESC_REMOVE_CUSTOMER_CONTACT = "删除客户联系人" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_CUSTOMER_CONTACT_LIST = "查询客户联系人列表" + ResponseConstant.RSP_DESC_COMMON + "[{'contact_name':'联系人名称','wei_xin':'微信号','org_id':'厂商id','contact_mobile':'手机号','id':713,'contact_email':'邮箱'}]";
    String RSP_DESC_ADD_CUSTOMER_INTERACTIVE_RECORD = "新增客户互动记录信息" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_GET_CUSTOMER_INTERACTIVE_RECORD = "获取客户互动记录信息" + ResponseConstant.RSP_DESC_COMMON + "{'code':200,'msg':null,'content':[{'id':7085：附件id,'remark':null：内容}]}";

    /**
     * 抄表计量
     */
    String RSP_DESC_SEARCH_CHECK_METER_MONTH_LIST = "查询抄表月份统计列表" + ResponseConstant.RSP_DESC_COMMON + "[{'date':'日期','valid_asset':'有效流量计','has_exceeded_asset':'已抄流量计','un_exceeded_asset':'未抄流量计','odd_asset':'异常流量计','sup_pollute':'超级排污','success_rate':'成功率(%)'}]";
    String RSP_DESC_SEARCH_CHECK_METER_LIST = "查询客户排污列表" + ResponseConstant.RSP_DESC_COMMON + "[{'inner_code':'客户编码','title':'客户名称','asset_id':'设备id','asset_code':'设备编码','asset_name':'设备名称','last_indication':'上期行至','final_flow':'本期流量','check_type':'抄表类型','status':'抄表状态','check_time':'抄表时间'}]";
    String RSP_DESC_SEARCH_CHECK_METER_STATISTICS = "客户计量统计信息" + ResponseConstant.RSP_DESC_COMMON + "[{'normal':'正常','total':'全部客户','exceed':'超标','checked':'已抄','unusual':'异常'}]";
    String RSP_DESC_SEARCH_ASSET_CHECK_METER_LIST = "设备抄表记录" + ResponseConstant.RSP_DESC_COMMON + "[{'last_indication':'上期行至','indication':'当前读数','final_flow':'当前水量','water_quality':'水质','check_type':'抄表类型','unusual_type':'异常类型','check_time':'抄表时间'}]";
    String RSP_DESC_SEARCH_CHECK_METER_DETAIL = "排污详情" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_ADD_CHECK_METER = "手动抄表" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_CHECK_METER_CALENDAR = "抄表日历" + ResponseConstant.RSP_DESC_COMMON + "[{'unchecked':'未抄表数量','unusual':'异常数量','water_quality':'水质超标数量','days':'日期'}]";
    String RSP_DESC_EDIT_CHECK_METER = "抄表编辑" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_SEARCH_CHECK_METER_LOG_LIST = "获得抄表记录所有的日志" + ResponseConstant.RSP_DESC_COMMON + "[{'remark':[{'remark':'日志描述','key':'日志内容','params':['日志参数']}],'create_user_account':'创建人账号','create_user_name':'创建人姓名','create_time':'创建时间'}]";
    String RSP_DESC_AUTO_CHECK_METER_SETING = "自动缴费设置" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_AUTO_CHECK_METER_SEARCH = "自动缴费查询" + ResponseConstant.RSP_DESC_COMMON + "[{'check_meter_shedule_hour':'自动抄表时间','try_retry_times':'重试次数','check_meter_rule':'检定期计量规则'}]";
    String RSP_DESC_SEARCH_METER_LOG_LIST = "获得抄表记录所有的日志" + ResponseConstant.RSP_DESC_COMMON + "[{'remark':[{'remark':'日志描述','key':'日志内容','params':['日志参数']}],'create_user_account':'创建人账号','create_user_name':'创建人姓名','create_time':'创建时间'}]";
    String RSP_DESC_METER_CALENDAR = "抄表日历" + ResponseConstant.RSP_DESC_COMMON + "[{'normal_total':'正常数量','abnormal_total':'异常数量','exceed_total':'超标数量','days':'日期'}]";
    String RSP_DESC_SEARCH_METER_STATISTICS = "抄表计量统计数据" + ResponseConstant.RSP_DESC_COMMON + "[{'exceed':'超标','checked':'已抄','unchecked':'未抄','normal_total':'正常数量','abnormal_total':'异常数量','exceed_total':'超标数量','total':'总数'}]";
    String RSP_DESC_SEARCH_METER_LIST = "查询抄表记录" + ResponseConstant.RSP_DESC_COMMON + "[{'inner_code':'客户编号','title':'客户名称','last_flow_total':'(水量)上期行至','current_flow_total':'当前水量','this_flow_total':'本期水量','meter_status':'抄表状态','check_time':'抄表时间','status':'水质超标'}]";
    String RSP_DESC_SEARCH_METER_ASSERT_LIST = "根据客户查询排污口" + ResponseConstant.RSP_DESC_COMMON + "[{'assert_id':'流量计id','assert_name':'流量计名称'}]";
    String RSP_DESC_SEARCH_METER_ITEM_LIST = "根据客户和排污口查询水质水量信息" + ResponseConstant.RSP_DESC_COMMON + "[{'factor_id':'因子id','facility_id':'客户id','asset_id':'流量计id','check_item_name':'检查项名称','indication':'当前读数','limit_value':'纳管标准限制','current_flow':'当前排水量','last_indication':'上期读数','unit':'单位','status':'状态','meter_status':'抄表状态','meter_type':'抄表方式','update_time':'更新时间'}]";
    String RSP_DESC_SEARCH_METER_RECORD_LIST = "根据客户和排污口查询检测记录" + ResponseConstant.RSP_DESC_COMMON + "[{'factor_id':'因子id','facility_id':'客户id','asset_id':'流量计id','check_item_name':'检查项名称','indication':'当前读数','limit_value':'纳管标准限制','unit':'单位','item_status':'因子状态','meter_status':'抄表状态','meter_type':'抄表方式','update_time':'更新时间'}]";
    String RSP_DESC_EDIT_METER_ITEM = "编辑水质水量记录" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_DELETE_METER_RECORD = "删除检测记录" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_SEARCH_GUESS_METER = "触发估抄" + ResponseConstant.RSP_DESC_COMMON + "[{'id':'主键','factor_id':'因子id','facility_id':'客户id','asset_id':'流量计id','check_item':'检查项','check_item_name':'检查项名称','indication':'当前读数'}]";
    String RSP_DESC_GUESS_METER_ITEM = "估抄" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_SEARCH_METER_SEWAGE_RECORDS = "排污记录查询" + ResponseConstant.RSP_DESC_COMMON + "[{'id':'主键','facility_id':'客户id','asset_id':'流量计id','begin_date':'实际排污开始时间','end_date':'实际排污结束时间','plan_begin_date':'计划排污开始时间','plan_end_date':'计划排污结束时间'}]";
    String RSP_DESC_METER_SEWAGE_RECORDS_CHART = "排污记录图表" + ResponseConstant.RSP_DESC_COMMON + "[{'time':'时间','plan':'计划排污时间','actual':'实际排污时间','is_sewage':'是否排污'}]";

    /**
     * 缴费管理
     */
    String RSP_DESC_SEARCH_POLLUTE_FEE_PERMISSION = "获取缴费模块权限" + ResponseConstant.RSP_DESC_RIGHT_BY_MENU + ResponseConstant.RSP_DESC_COMMON + "{'模块key':{}}";
    String RSP_DESC_SEARCH_POLLUTE_FEE_LIST = "查询客户缴费列表" + ResponseConstant.RSP_DESC_COMMON + "[{'inner_code':'客户编码','title':'客户名称','belong_month':'所属月份','begin_date':'开始日期','end_date':'截止日期','pay_time':'缴费时间','amount':'应缴金额','pay_amount':'实缴金额','final_flow':'当期水量','exceed_times':'超标次数','normal_amount':'正常费','exceed_amount':'超标费'}]";
    String RSP_DESC_SEARCH_POLLUTE_FEE_STATISTICS = "查询客户缴费统计信息" + ResponseConstant.RSP_DESC_COMMON + "[{'total':'总数','should_pay':'应缴','payed':'已缴','audit':'审核中'}]";
    String RSP_DESC_EDIT_POLLUTE_FEE = "缴费编辑" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_AUDIT_POLLUTE_FEE = "缴费审核" + ResponseConstant.RSP_DESC_COMMON + "[{}]";
    String RSP_DESC_AUTO_POLLUTE_FEE_SETING = "自动计费周期设置" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_AUTO_POLLUTE_FEE_SEARCH = "自动计费周期查询" + ResponseConstant.RSP_DESC_COMMON + "[{'pollute_begin_month':'自动计费开始月份','pollute_begin_day':'自动计费开始第几天','pollute_end_month':'自动计费结束月份','pollute_end_day':'自动计费结束第几天','pollute_shedule_begin_day':'自动计费生成日期'}]";
    String RSP_DESC_SEARCH_POLLUTE_FEE_LOG_LIST = "获得缴费记录所有的日志" + ResponseConstant.RSP_DESC_COMMON + "[{'remark':[{'remark':'日志描述','key':'日志内容','params':['日志参数']}],'create_user_account':'创建人账号','create_user_name':'创建人姓名','create_time':'创建时间'}]";

    /**
     * 费用项管理
     */
    String RSP_DESC_FEE_ITEM_LIST = "查询费用项列表" + ResponseConstant.RSP_DESC_COMMON + "['is_use':'是否启用 1启用2不启用','fee_code':'编码','fee_name':'名称','begin_date':'开始日期','end_date':'结束日期','begin_over_times':'超标次数起','end_over_times':'超标次数止','price':'单价','ratio_cal_type':'超标计算类型','repeat_price':'重复测量单价']";
    String RSP_DESC_ADD_FEE_ITEM = "新增费用项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_EDIT_FEE_ITEM = "修改费用项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_SEARCH_FEE_ITEM_INFO = "查询费用项详情" + ResponseConstant.RSP_DESC_COMMON + "{'is_use':'是否启用 1启用2不启用','fee_code':'编码','fee_name':'名称','begin_date':'开始日期','end_date':'结束日期','begin_over_times':'超标次数起','end_over_times':'超标次数止','price':'单价','ratio_cal_type':'超标计算类型','repeat_price':'重复测量单价','status':'状态'}";
    String RSP_DESC_REMOVE_FEE_ITEM = "删除费用项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
    String RSP_DESC_CHANGE_FEE_ITEM = "启用/禁用费用项" + ResponseConstant.RSP_DESC_COMMON + "不返回任何信息";
}
