package com.gengyun.senscloud.common;

import java.util.HashMap;
import java.util.Map;

/**
 * sql常量
 * User: sps
 * Date: 2018/12/19
 * Time: 上午11:20
 */
public interface SqlConstant {
    String TEMPLATE_TASK = "template_task";//任务模板
    String WORK_TASK = "work_task";//任务项
    String SCHEMA_NAME = "@S@";
    String SQL_WHERE_NAME = "@whereString@";
    String SQL_SUB_WHERE_NAME = "@subWhereString@";
    String SQL_LANG_KEY = "@sqlLangKey@";
    String SQL_WHERE_LIKE = "@whereStringLike@";
    String SQL_WORK_AUTH_NAME = "@workAuth@";
    String SQL_ASSET_AUTH_NAME = "@assetAuth@";
    String SQL_BOM_AUTH_NAME = "@bomAuth@";
    String SQL_BOM_WORK_AUTH_NAME = "@bomWorkAuth@";
    String SQL_CUSTOMER_AUTH_NAME = "@customerAuth@";
    String NO_LANG_KEY = "no_lang_"; // 不需要国际化
    String FACILITY_ID_LEN = "99999"; // 数字转字符用长度
    String FACILITY_CUSTOMER = "1"; // 客户
    String FACILITY_INSIDE = "1,2"; // 内部组织
    String POSITION_STRUCTURE_TYPE = "1"; // 设备位置为主 ：作用于筛选设备，派案逻辑  --新结构 zys
    String FACILITY_STRUCTURE_TYPE = "2"; //设备组织为主 ：适用于组织位置组合树筛选，或组织主导 --老结构zys
    String PLAN_WORK_TEMPLATE_TYPE_RECYCLE = "1"; // 维保计划模板类型-循环计划（默认）
    String PLAN_WORK_TEMPLATE_TYPE_SINGLE = "2"; // 维保计划模板类型-一次性计划
    String FACILITY_OUTSIDE = "3,4,5,6"; // 外部组织
    String FACILITY_SERVICE_PROVIDER = "3"; //服务商
    String FACILITY_SUPPLIER = "4"; // 供应商
    String FACILITY_MANUFACTURER = "6"; // 制造商
    String DB_TYPE_NORMAL = ",float,int,jsonb,"; // 数据库普通类型
    String DB_TYPE_TIMESTAMP = ",dateTime,"; // 数据库时间戳常用格式
    /**
     * 数据库时间类型
     */
    String SQL_DATE_TIME_FMT = "yyyy-MM-dd HH24:mi:ss";
    /**
     * 数据库日期类型
     */
    String SQL_DATE_FMT = "yyyy-MM-dd";
    ;
    /**
     * 日期格式-年
     */
    String SQL_DATE_FMT_YEAR = "yyyy";
    /**
     * 日期格式-年月
     */
    String SQL_DATE_FMT_YEAR_MON = "yyyyMM";
    /**
     * 日期格式-年-月
     */
    String SQL_DATE_FMT_YEAR_MON_2 = "yyyy-MM";
    /**
     * 数据库时间类型
     */
    String SQL_TIME_FMT = "HH24:mi:ss";
    String OTHER_TABLE = "other"; // 通用表
    String[] WORK_DB_TABLES = {"_sc_works", "_sc_works_detail", "_sc_work_bom", "_sc_work_fee", "_sc_work_dutyman_hour",
            "_sc_work_request", "_sc_bom_in_stock", "_sc_bom_stock", "_sc_bom_allocation", "_sc_bom_recipient",
            "_sc_bom_discard", "_sc_client_user", "_sc_bom_inventory", "_sc_bom_inventory_stock", "", "_sc_work_task",
            "_sc_work_task_finished", "_sc_work_task_receiver", "_sc_asset_discard", "_sc_asset_discard_detail", "_sc_asset_transfer",
            "_sc_asset_transfer_detail", "_sc_asset_inventory", "_sc_asset_inventory_org", "_sc_asset_inventory_org_detail", "_sc_user_register"}; // 工单业务相关表名


    String[] WORK_DB_KEYS_BASE = {"work_sheet", "work_sheet_detail", "work_request", "bom_in_stock", "bom_allot",
            "bom_recipient", "bom_diacard", "asset_discard", "asset_transfer", "bom_inventory", "bom_inventory_detail",
            "asset_inventory", "asset_inventory_detail", "user_register", "plan_work", "asset_work_code", "asset_sub_work_code",
            "bom", "bom_work_code", "bom_sub_work_code", "asset_out_put_work_code", "asset_out_put_sub_work_code",
            "asset_in_put_work_code", "asset_in_put_sub_work_code", "asset_change_work_code", "asset_change_sub_work_code",
            "asset_accept_work_code", "asset_accept_sub_work_code", "bom_in_put_work_code", "bom_in_put_sub_work_code",
            "bom_out_put_work_code", "bom_out_put_sub_work_code", "bom_purchasing_work_code", "bom_purchasing_sub_work_code",
            "product_task_code"}; // 工单业务相关表主键值来源
    String IS_NEW_DATA = "@new"; // 是否是新增数据

    String _sc_work_bom_columns = "sub_work_code,bom_model,bom_code,bom_name,stock_code,facility_id,huojia," +
            "fetch_man,use_count,material_code,is_from_service_supplier,price,bom_source_type,currency_id"; // 工单备件表列信息
    String _sc_work_fee_columns = "sub_work_code,fee_type,fee_amount,currency"; // 工单费用表列信息
    String _sc_work_dutyman_hour_columns = "sub_work_code,operate_account,is_support,work_hour,work_request_code"; // 工单负责人工时表
    String _sc_asset_organization_columns = "asset_id,org_id"; // 设备组织表
    String _sc_work_request_columns = "work_request_code,work_type_id,work_template_code,parent_code,position_code," +
            "relation_type,relation_id,title,problem_note,problem_img,occur_time,deadline_time,status,remark," +
            "create_time,create_user_id,customer_id,plan_code,body_property"; // 工单请求表
    String _sc_role_asset_type_columns = "role_id,category_id"; // 角色设备类型表
    String _sc_role_permission_columns = "role_id,permission_id"; // 角色权限表
    String _sc_bom_in_stock_columns = "in_code,stock_code,file_ids,status,remark,create_time,create_user_id"; // 备件入库表
    String _sc_bom_columns = "bom_name,bom_model,material_code,is_use,create_time,create_user_id,type_id,manufacturer_id,unit_id,is_from_service_supplier,service_life,show_price,currency_id,code_classification,data_order,status";
    String _sc_bom_stock_columns = "quantity,security_quantity,max_security_quantity,show_price,currency_id,store_position,bom_id,stock_id"; // 备件清单表
    String _sc_bom_stock_detail_columns = "bom_code,store_position,quantity,stock_id,bom_id";
    String _sc_bom_supplies_columns = "bom_id,bom_code,supplier_id,supply_period,is_use";
    String _sc_bom_allocation_columns = "transfer_code,from_stock_code,to_stock_code,send_file_ids,receive_file_ids,status,remark,create_time,create_user_id,body_property"; // 备件调拨表
    String _sc_bom_allocation_detail_columns = "transfer_code,bom_code,material_code,send_file_ids,receive_file_ids,quantity,real_quantity,area_code_out,area_code_in,price,currency_id"; // 备件调拨备件详情表
    String _sc_bom_recipient_columns = "recipient_code,stock_code,file_ids,status,remark,create_time,create_user_id,client_org_id,body_property";//备件领用表
    String _sc_bom_recipient_detail_columns = "recipient_code,bom_code,material_code,quantity,file_ids,real_quantity,area_code,price,currency_id";//备件报废详情表
    String _sc_bom_discard_columns = "discard_code,stock_code,status,remark,create_time,create_user_id,from_bill_code,body_property";//备件报废表
    String _sc_bom_discard_detail_columns = "discard_code,bom_code,material_code,leave_cost,quantity,real_quantity,area_code,file_ids,currency_id";
    String _sc_asset_columns = "asset_name,asset_code,category_id,asset_model_id,position_code,status,importment_level_id,enable_time,use_year,buy_date,supplier_id,manufacturer_id,price,buy_currency_id,tax_rate,install_date,install_price,install_currency_id,id,create_time,create_user_id,data_order,tax_price,properties,guarantee_status_id,running_status_id";//设备表
    String _sc_asset_inventory_org_columns = "inventory_code,facility_id,record_count,inventory_count,balance_count,duty_man,finished_time,status,remark,create_time,create_user_id,body_property,sub_inventory_code";//设备盘点位置表
    String _sc_asset_inventory_org_detail_columns = "inventory_code,facility_id,asset_id,asset_code,asset_name,in_status,deal_result,status,remark,create_time,create_user_id,sub_inventory_code";//设备盘点明细表
    String _sc_asset_inventory_asset_category_columns = "inventory_code,asset_category_id";//设备盘点设备类型表
    String _sc_bom_in_stock_detail_columns = "in_code,bom_code,material_code,quantity,price,buy_time,batch_no,in_type,file_ids,currency_id";//备件入库详情表
    String _sc_bom_inventory_stock_columns = "inventory_code,stock_code,record_count,inventory_count,balance_count,duty_man,status,create_time,create_user_id,sub_inventory_code,body_property";//备件盘点库房表
    String _sc_bom_inventory_stock_detail_columns = "inventory_code,stock_code,bom_model,bom_code,material_code,in_status,deal_result,status,create_time,create_user_id,record_count,inventory_count,sub_inventory_code";//备件盘点库房详情表
    String _sc_bom_inventory_bom_type_columns = "inventory_code,bom_type_id";//备件盘点备件类型表
    String _sc_performance_management_template_options_equation_columns = "template_id,template_option_id,greater,less,greater_value,less_value,equation";//人员绩效考核模板-考核项-考核标准表
    String _sc_asset_vmi_alarm_color_columns = "asset_id,greater,less,greater_value,less_value,equation";//vmi设备颜色配置表
    String _sc_asset_allocation_detail_columns = "transfer_code,asset_id,send_file_ids,receive_file_ids,asset_code_new"; // 设备调拨设备详情表

    String _sc_work_type_columns = "type_name,flow_template_code,data_order,is_use,create_time,create_user_id,business_type_id,work_template_code";//工单类型表
    String _sc_repair_type_columns = "type_name,data_order,is_use";//维修类型表
    String _sc_asset_running_status_columns = "running_status,data_order,is_use";//设备运行状态表
    String _sc_importment_level_columns = "level_name,data_order,is_use";//设备重要级别表
    String _sc_bom_type_columns = "type_name,type_code,parent_id,data_order,is_use";//备件类型表
    String _sc_facility_type_columns = "type_name,data_order,create_user_id,create_time,is_use";//位置类型表
    String _sc_file_type_columns = "type_name,data_order,is_use";//文档类型表
    String _sc_unit_columns = "unit_name,data_order,is_use";//文档类型表
    String _sc_work_finished_type_columns = "finished_name,data_order,is_finished,operate_account,operate_time,is_use";//工单完修状态表
    String _sc_currency_columns = "currency_name,data_order,currency_code,is_use";//货币表


    String SQL_WLF_SELECT_NAME = "feeSelectSql";
    String SQL_WLF_SELECT = " round(sum(COALESCE(fee_amount,0)::NUMERIC),2) as fee_amount, " +
            " round(sum(COALESCE(case when fee_type = 1 then fee_amount end,0)::NUMERIC),2) as fee_1, " +
            " round(sum(COALESCE(case when fee_type = 2 then fee_amount end,0)::NUMERIC),2) as fee_2, " +
            " round(sum(COALESCE(case when fee_type = 9 then fee_amount end,0)::NUMERIC),2) as fee_9, " +
            " max(cur.currency_code) as fee_currency, "; // 工单列表费用查询sql
    String SQL_WLF_TABLE_NAME = "feeTableSql";
    String SQL_WLF_TABLE = " LEFT JOIN ${schema_name}._sc_work_fee fee on fee.sub_work_code = w2.sub_work_code " +
            "LEFT JOIN ${schema_name}._sc_currency cur ON fee.currency = cur.id "; // 工单列表费用查询表sql
    String SQL_WLF_CONDITION = " group by w.work_code,u.user_name,C.user_name,s.status,t.type_name,dv.asset_name," +
            "w2.title,w2.priority_level,w2.receive_time,w2.distribute_time,w2.receive_account,w2.sub_work_code, " +
            "dv.asset_code,ct.short_title,t.flow_template_code,t.business_type_id,ma.category_name,ff.type_name," +
            "tt.type_name,ars.running_status,ap.position_name, apt.position_name,w2.begin_time,w2.finished_time," +
            "w2.audit_time,w2.result_note,w2.status,fc.fault_code,fc.code_name,wt.work_template_code "; // 工单列表费用查询条件sql
    String SQL_WLF_GROUP_COLUMN_NAME = "feeSelectSqlGroupColumn";
    String SQL_WLF_GROUP_COLUMN = ",fee_amount,fee_1,fee_2,fee_9,fee_currency";//工单列表费用分组列名


    // 设备列表查询用表
    String ASSET_SEARCH_TBL_SQL = "LEFT JOIN ${schema_name}._sc_asset_model t9 ON t9.ID = t.asset_model_id " +
            "LEFT JOIN ${schema_name}._sc_asset_status t4 ON t.status = t4.ID " +
            "LEFT JOIN ${schema_name}._sc_asset_organization ao ON T.id = ao.asset_id " +
            "LEFT JOIN ${schema_name}._sc_facilities t5 ON ao.org_id = t5.ID and t5.is_use = '1' and t5.org_type_id = " + FACILITY_SUPPLIER +
            " LEFT JOIN ${schema_name}._sc_facilities t5_1 ON ao.org_id = t5_1.ID and t5_1.is_use = '1' and t5_1.org_type_id = " + FACILITY_MANUFACTURER +
            " LEFT JOIN ${schema_name}._sc_facilities sf_c ON ao.org_id = sf_c.ID and sf_c.org_type_id = 1 " +
            " LEFT JOIN ${schema_name}._sc_asset_category t6 ON t6.ID = t.category_id and t6.is_use='1' " +
            "LEFT JOIN ${schema_name}._sc_asset_position p1 ON t.position_code = p1.position_code " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON f.id = ao.org_id " +
//            "LEFT JOIN ${schema_name}._sc_facilities t2 ON ao.org_id = t2.ID AND t2.org_type in (" + SqlConstant.FACILITY_INSIDE + ") " +
//            "LEFT JOIN ${schema_name}._sc_facilities f ON t2.facility_no LIKE f.facility_no || '%' " +
            "LEFT JOIN ${schema_name}._sc_asset_iot_setting t10 ON t10.asset_id = t.id " +
            "LEFT JOIN ${schema_name}._sc_asset_running_status t11 ON t.running_status_id = t11.ID " +
            " LEFT JOIN ${schema_name}._sc_importment_level t13 on t.importment_level_id = t13.id ";

    String ASSET_SEARCH_LIST_SQL = " FROM ( " +
            "   SELECT t1.id,t1.asset_code,t1.asset_name,t1.position_code,t1.asset_model_id,t1.category_id,t1.supplier_id," +
            "          t1.running_status_id,t1.status,t1.guarantee_status_id,t1.enable_time,t1.importment_level_id," +
            "          t1.manufacturer_id,t1.parent_id,t1.create_time " +
            "   FROM ${schema_name}._sc_asset t1 " + SqlConstant.ASSET_DATA_PRM_SQL +
            "   ${whereString} " +
            ") t " + SqlConstant.ASSET_SEARCH_TBL_SQL;

    String ASSET_SEARCH_INFO_SQL = " FROM ( " +
            "   SELECT t1.id,t1.asset_name,t1.asset_code,t1.status,t1.asset_icon,t1.remark,t1.file_ids,t1.create_time,t1.maintain_begin_time,t1.enable_time,t1.buy_date,t1.install_date," +
            "   t1.supplier_id,t1.parent_id,t1.properties,t1.quantity,t1.unit_id,t1.create_user_id,t1.category_id,t1.importment_level_id,t1.price,t1.tax_rate,t1.tax_price,t1.currency_id,t1.position_code," +
            "   t1.manufacturer_id,t1.asset_model_id,t1.install_price,t1.use_year,t1.running_status_id,t1.guarantee_status_id,t1.buy_currency_id,t1.install_currency_id " +
            "   FROM ${schema_name}._sc_asset t1 " + SqlConstant.ASSET_DATA_PRM_SQL +
            "   ${whereString} " +
            ") t " + SqlConstant.ASSET_SEARCH_TBL_SQL +
            " LEFT JOIN ${schema_name}._sc_currency cur on t.currency_id = cur.id " +
            " LEFT JOIN ${schema_name}._sc_currency cur_buy on t.buy_currency_id = cur_buy.id " +
            " LEFT JOIN ${schema_name}._sc_currency cur_install on t.install_currency_id = cur_install.id " +
            " LEFT JOIN ${schema_name}._sc_unit unt on t.unit_id = unt.id ";

    String ASSET_SEARCH_BODY_SQL = " FROM ( " +
            "   SELECT t1.id,t1.asset_name,t1.asset_code,t1.status,t1.asset_icon,t1.remark,t1.file_ids,t1.create_time,t1.maintain_begin_time,t1.enable_time,t1.buy_date,t1.install_date," +
            "   t1.supplier_id,t1.parent_id,t1.properties,t1.quantity,t1.unit_id,t1.create_user_id,t1.category_id,t1.importment_level_id,t1.price,t1.tax_rate,t1.tax_price,t1.currency_id,t1.position_code," +
            "   t1.manufacturer_id,t1.asset_model_id,t1.install_price,t1.use_year,t1.running_status_id,t1.guarantee_status_id,t1.buy_currency_id,t1.install_currency_id " +
            "   FROM ${schema_name}._sc_asset t1 " + SqlConstant.ASSET_DATA_PRM_SQL +
            "   ${whereString} " +
            ") t " + SqlConstant.ASSET_SEARCH_TBL_SQL +
            " LEFT JOIN ${schema_name}._sc_currency cur on t.currency_id = cur.id " +
            " LEFT JOIN ${schema_name}._sc_currency cur_buy on t.buy_currency_id = cur_buy.id " +
            " LEFT JOIN ${schema_name}._sc_currency cur_install on t.install_currency_id = cur_install.id " +
            " LEFT JOIN ${schema_name}._sc_unit unt on t.unit_id = unt.id ";

    String ASSET_DATA_PRM_SQL = " INNER JOIN ( select DISTINCT rat.category_id, rap.asset_position_code " +
            " from ${schema_name}._sc_user_position up " +
            " INNER JOIN ${schema_name}._sc_position_role pr on up.position_id = pr.position_id " +
            " INNER JOIN ${schema_name}._sc_role ur on ur.id = pr.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_type rat on pr.role_id = rat.role_id " +
            " INNER JOIN ${schema_name}._sc_role_asset_position rap on pr.role_id = rap.role_id " +
            " where up.user_id = #{userId} ) prm " +
            " on t1.category_id = prm.category_id and t1.position_code = prm.asset_position_code ";

    String FLOW_TASK_SQL = " FROM ( SELECT t.*, t2.business_type_id,t2.type_name as work_type_name, T3.status AS status_name, t4.user_name as distribute_user_name, t5.user_name as receive_user_name, t6.user_name as create_user_name, " +
            "   case when t.relation_type = -1 then (coalesce((SELECT position_name FROM ${schema_name}._sc_asset_position " +
            "     WHERE position_code = t.relation_id), (SELECT asset_name FROM ${schema_name}._sc_asset WHERE id = t.relation_id))) " +
            "    when t.relation_type = -2 then (SELECT stock_name FROM ${schema_name}._sc_stock WHERE id = t.position_code::int) " +
            "    when t.relation_type = 1 then (SELECT position_name FROM ${schema_name}._sc_asset_position WHERE position_code = t.relation_id) " +
            "    when t.relation_type = 2 then (SELECT asset_name FROM ${schema_name}._sc_asset WHERE id = t.relation_id) " +
            "    ELSE (SELECT position_name FROM ${schema_name}._sc_asset_position WHERE position_code = t.relation_id) " +
            "   end as relation_name, " +
            "   case when t.relation_type = -2 then (SELECT stock_name FROM ${schema_name}._sc_stock WHERE id = t.position_code::int) " +
            "    ELSE (SELECT position_name FROM ${schema_name}._sc_asset_position WHERE position_code = t.position_code) " +
            "   end as position_name, " +
            "   ART.assignee_ as duty_user_id, t7.user_name as duty_user_name, t7.user_name as assignee_name, ART.id_ as task_id, task_def_key_ as task_definition_key, ART.PROC_INST_ID_ as process_instance_id, " +
            "   ART.proc_def_id_ as process_definition_id, to_char(ART.create_time_, '" + SqlConstant.SQL_DATE_FMT + "') as process_create_date, to_char(ART.create_time_, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as process_create_time, ART.task_def_key_ as node_id," +
            "   CASE WHEN position('签到' in ART.name_) > 0 THEN '执行人签到' WHEN position('执行' in ART.name_) > 0 THEN '执行人处理' ELSE ART.name_ END as flow_node_show_name " +
            " FROM ( " +
            "   select sub_work_code,d.work_code,coalesce(d.work_type_id,w.work_type_id) as work_type_id,coalesce(d.relation_type,w.relation_type) as relation_type,d.relation_id,w.relation_id as w_relation_id, " +
            "     coalesce(d.title,w.title) as title,coalesce(d.problem_note,w.problem_note) as problem_note,running_status_id,fault_type_id,repair_type_id,coalesce(priority_level,1) as priority_level,result_note,d.status,w.status as w_status, " +
            "     w.remark as w_remark,d.remark,from_code,waiting,bom_app_result,to_char(receive_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as receive_time, to_char(distribute_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as distribute_time," +
            "     to_char(begin_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as begin_time,to_char(finished_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as finished_time,to_char(audit_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as audit_time," +
            "     fault_number,is_main,to_char(plan_arrive_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as plan_arrive_time,service_score,repair_code, " +
            "     is_finished,finished_id,is_redo,to_char(total_deal_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as total_deal_time,distribute_user_id,receive_user_id,duty_user_id as duty_user_id_a,pref_id,d.position_code,w.position_code as w_position_code," +
            "     to_char(plan_deal_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as plan_deal_time, to_char(plan_begin_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as plan_begin_time," +
            "     to_char(d.deadline_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as deadline_time,to_char(w.deadline_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as w_deadline_time," +
            "     to_char(sign_in_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as sign_in_time,to_char(close_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as close_time,create_user_id," +
            "     to_char(create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time,bak_value1,bak_value2,bak_value3,bak_value4,bak_value5 " +
            "   from ${schema_name}._sc_works_detail d INNER JOIN ${schema_name}._sc_works w on d.work_code = w.work_code " +
            "   WHERE sub_work_code in <foreach collection='upCodeList' item='upCode' open='(' close=')' separator=','> #{upCode} </foreach> " +
            "   union " +
            "   select sub_work_code,d.work_code,work_type_id,-1 as relation_type,d.asset_id as relation_id,d.asset_id as w_relation_id,null as title,null as problem_note,null as running_status_id,null as fault_type_id," +
            "     null as repair_type_id,1 as priority_level,null as result_note,d.status,w.status as w_status,w.remark as w_remark,d.remark,null as from_code,null as waiting,null as bom_app_result,null::char as receive_time, " +
            "     null::char as distribute_time,null::char as begin_time,null::char as finished_time,null::char as audit_time,null as fault_number,is_main,null::char as plan_arrive_time,null as service_score,null as repair_code,null as is_finished, " +
            "     null as finished_id,null as is_redo,null::char as total_deal_time,null as distribute_user_id,null as receive_user_id,duty_user_id as duty_user_id_a,pref_id,position_code,position_code as w_position_code, " +
            "     null::char as plan_deal_time,null::char as plan_begin_time,null::char as deadline_time,null::char as w_deadline_time,null::char as sign_in_time,null::char as close_time,create_user_id," +
            "     to_char(create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time,bak_value1,bak_value2,bak_value3,bak_value4,bak_value5 " +
            "   from ${schema_name}._sc_asset_works_detail d INNER JOIN ${schema_name}._sc_asset_works w on d.work_code = w.work_code " +
            "   WHERE sub_work_code in <foreach collection='upCodeList' item='upCode' open='(' close=')' separator=','> #{upCode} </foreach> " +
            "   union " +
            "   select sub_work_code,d.work_code,work_type_id,-2 as relation_type,bom_id::VARCHAR as relation_id,bom_id::VARCHAR as w_relation_id,null as title,null as problem_note,null as running_status_id,null as fault_type_id," +
            "     null as repair_type_id,1 as priority_level,null as result_note,d.status,w.status as w_status,w.remark as w_remark,d.remark,null as from_code,null as waiting,null as bom_app_result,null::char as receive_time, " +
            "     null::char as distribute_time,null::char as begin_time,null::char as finished_time,null::char as audit_time,null as fault_number,null as is_main,null::char as plan_arrive_time,null as service_score,null as repair_code,null as is_finished, " +
            "     null as finished_id,null as is_redo,null::char as total_deal_time,null as distribute_user_id,null as receive_user_id,duty_user_id as duty_user_id_a,pref_id,stock_id::VARCHAR as position_code, " +
            "     stock_id::VARCHAR as w_position_code,null::char as plan_deal_time,null::char as plan_begin_time,null::char as deadline_time,null::char as w_deadline_time,null::char as sign_in_time,null::char as close_time,create_user_id," +
            "     to_char(create_time, '" + SqlConstant.SQL_DATE_TIME_FMT + "') as create_time,bak_value1, bak_value2,bak_value3,bak_value4,bak_value5 " +
            "   from ${schema_name}._sc_bom_works_detail d INNER JOIN ${schema_name}._sc_bom_works w on d.work_code = w.work_code " +
            "   WHERE sub_work_code in <foreach collection='upCodeList' item='upCode' open='(' close=')' separator=','> #{upCode} </foreach> " +
            " ) T  " +
            " INNER JOIN ${schema_name}.ACT_RU_VARIABLE arv ON arv.NAME_= 'sub_work_code' and arv.TYPE_ = 'string' and arv.TEXT_ = t.sub_work_code " +
            " INNER JOIN ${schema_name}.ACT_RU_TASK ART ON ART.ID_ = arv.TASK_ID_ or ART.PROC_INST_ID_ = arv.EXECUTION_ID_ " +
            " LEFT JOIN ${schema_name}._sc_work_type t2 on t.work_type_id = t2.id " +
            " LEFT JOIN ${schema_name}._sc_status t3 on t.status = t3.ID " +
            " LEFT JOIN ${schema_name}._sc_user t4 on t.distribute_user_id = t4.id " +
            " LEFT JOIN ${schema_name}._sc_user t5 on t.receive_user_id = t5.id " +
            " LEFT JOIN ${schema_name}._sc_user t6 on t.create_user_id = t6.id " +
            " LEFT JOIN ${schema_name}._sc_user t7 on ART.assignee_ = t7.id " +
            ") T " +
            "WHERE 1=1 " +
            "<if test = 'null != wfParam.keywordSearch'> " +
            " and (t.work_type_name like CONCAT('%', #{wfParam.keywordSearch}, '%') or t.relation_name like CONCAT('%', #{wfParam.keywordSearch}, '%') or t.status_name like CONCAT('%', #{wfParam.keywordSearch}, '%') " +
            " or t.distribute_user_name like CONCAT('%', #{wfParam.keywordSearch}, '%') or t.receive_user_name like CONCAT('%', #{wfParam.keywordSearch}, '%') or t.create_user_name like CONCAT('%', #{wfParam.keywordSearch}, '%') " +
            " or t.position_name like CONCAT('%', #{wfParam.keywordSearch}, '%') or t.duty_user_name like CONCAT('%', #{wfParam.keywordSearch}, '%')) " +
            "</if> " +
            "<if test = 'wfParam.mine_only'> " +
            " and t.duty_user_id = #{userId} " +
            "</if> " +
            "<if test = 'wfParam.work_type_id'> " +
            " and t.work_type_id in <foreach collection='wfParam.work_type_id' item='wtId' open='(' close=')' separator=','> #{wtId}::int </foreach> " +
            "</if> " +
            "<if test = 'wfParam.start_time'> " +
            " AND t.create_time &gt;=#{wfParam.start_time} " +
            "</if> " +
            "<if test = 'wfParam.end_time'> " +
            " AND t.create_time &lt;=#{wfParam.end_time}  " +
            "</if> " +
            "<if test = 'wfParam.process_start_time'> " +
            " AND t.process_create_time &gt;=#{wfParam.process_start_time} " +
            "</if> " +
            "<if test = 'wfParam.process_end_time'> " +
            " AND t.process_create_time &lt;=#{wfParam.process_end_time}  " +
            "</if> ";

    String WORK_AUTH_SQL = " inner join (select d.* from ( " +
            " select d.sub_work_code from ${schema_name}._sc_works_detail d, " +
            "  (SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id  " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.relation_type = 1 and d.relation_id = p.asset_position_code and d.work_type_id = p.work_type_id " +
            " UNION select d.sub_work_code from ${schema_name}._sc_works_detail d, ${schema_name}._sc_asset a " +
            " ,(SELECT distinct p.asset_position_code, at.category_id, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and p.asset_position_code is not null and at.category_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.relation_type = 2 and d.relation_id = a.id and a.position_code = p.asset_position_code and a.category_id = p.category_id and d.work_type_id = p.work_type_id " +
            " UNION select d.sub_work_code from ${schema_name}._sc_works_detail d, " +
            "  (SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.relation_type not in (1, 2) and d.work_type_id = p.work_type_id " +
            " UNION select d.sub_work_code from ${schema_name}._sc_works_detail d," +
            "  (SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "  INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "  INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "  LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "  WHERE up.user_id = #{user_id} and wt.work_type_id is not null " +
            "  ) p " +
            "  where is_main = 1 and (relation_id is null or position(',' in relation_id) &gt; 0) and d.work_type_id = p.work_type_id ) d) pd on pd.sub_work_code = wd.sub_work_code ";

    String ASSET_WORK_AUTH_SQL = " inner join (select d.* from (select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w, ${schema_name}._sc_asset a " +
            " ,(SELECT distinct p.asset_position_code, at.category_id, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and p.asset_position_code is not null and at.category_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = a.id and a.position_code = p.asset_position_code and a.category_id = p.category_id and w.work_type_id = p.work_type_id " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and d.asset_id = p.asset_position_code and w.work_type_id = p.work_type_id " +
            " union select d.sub_work_code from ${schema_name}._sc_asset_works_detail d, ${schema_name}._sc_asset_works w " +
            " ,(SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.is_main = 1 and (d.asset_id is null or position(',' in d.asset_id) &gt; 0) and d.work_code = w.work_code and w.work_type_id = p.work_type_id ) d) pd on pd.sub_work_code = awd.sub_work_code ";

    String PLAN_AUTH_SQL = " inner join (SELECT distinct p.asset_position_code, wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and p.asset_position_code is not null and wt.work_type_id is not null " +
            "  ) p " +
            " ON pwc.position_code = p.asset_position_code and pwc.work_type_id = p.work_type_id ";

    String ASSET_AUTH_SQL = " inner join (SELECT distinct p.asset_position_code, at.category_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_position p on ur.id = p.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_asset_type at on ur.id = at.role_id " +
            "   WHERE up.user_id = #{user_id} and p.asset_position_code is not null and at.category_id is not null " +
            "  ) p " +
            " ON a.position_code = p.asset_position_code and a.category_id = p.category_id ";

    String BOM_AUTH_SQL = " inner join (SELECT distinct s.stock_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   INNER JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "   WHERE up.user_id = #{user_id} " +
            "  ) p " +
            " ON bs.stock_id = p.stock_id ";

    String STOCK_AUTH_SQL = " inner join (SELECT distinct s.stock_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   INNER JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "   WHERE up.user_id = #{user_id} " +
            "  ) p " +
            " ON s.id = p.stock_id ";

    String BOM_WORK_AUTH_SQL = " inner join (select d.* from ( " +
            " select d.sub_work_code from ${schema_name}._sc_bom_works_detail d, ${schema_name}._sc_bom_works w " +
            " ,( SELECT distinct wt.work_type_id, s.stock_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_stock s on ur.id = s.role_id " +
            "   WHERE up.user_id = #{user_id} and s.stock_id is not null and wt.work_type_id is not null " +
            "  ) p " +
            " where d.work_code = w.work_code and (w.stock_id = p.stock_id or w.to_stock_id = p.stock_id) and w.work_type_id = p.work_type_id and d.is_main = 1 " +
            " union select d.sub_work_code from ${schema_name}._sc_bom_works_detail d, ${schema_name}._sc_bom_works w " +
            " ,( SELECT distinct wt.work_type_id FROM ${schema_name}._sc_user_position up " +
            "   INNER JOIN ${schema_name}._sc_position_role pr ON up.position_id = pr.position_id " +
            "   INNER JOIN ${schema_name}._sc_role ur ON ur.id = pr.role_id " +
            "   LEFT JOIN ${schema_name}._sc_role_work_type wt on ur.id = wt.role_id " +
            "   WHERE up.user_id = #{user_id} and wt.work_type_id is not null " +
            "  ) p " +
            " where d.is_main = 1 and (d.bom_id is null or position(',' in d.bom_id) &gt; 0) and d.work_code = w.work_code and w.work_type_id = p.work_type_id " +
            " ) d) pd on pd.sub_work_code = bwd.sub_work_code ";

    String WORK_USE_TEMPLATE = " (select count(tab.node_id) from (select distinct node_id from " +
            " ${schema_name}._sc_work_flow_node where node_id in (select distinct node_id from " +
            " ${schema_name}._sc_work_flow_node_column_linkage where id in (select distinct linkage_id from " +
            " ${schema_name}._sc_work_flow_node_column_linkage_condition where sub_page_template_code=t.work_template_code)) " +
            " union " +
            " select distinct node_id from ${schema_name}._sc_work_flow_node where node_id in (select distinct node_id from " +
            " ${schema_name}._sc_work_flow_node_column_linkage where id in (select distinct linkage_id from " +
            " ${schema_name}._sc_work_flow_node_column_linkage_condition where edit_sub_page_template_code=t.work_template_code)) " +
            " union " +
            " select distinct node_id from ${schema_name}._sc_work_flow_node where node_id in (select distinct node_id from " +
            " ${schema_name}._sc_work_flow_node_column_linkage where id in (select distinct linkage_id from " +
            " ${schema_name}._sc_work_flow_node_column_linkage_condition where scan_fail_return_template_code=t.work_template_code)) " +
            " union " +
            " select distinct pref_id as node_id from ${schema_name}._sc_work_flow_info where work_template_code=t.work_template_code) tab) as work_by_used ";

    String OTHER_KNOWLEDGE_BASE = "select f.id from schema_name._sc_files f join (" +
            " select id::varchar as pkey from schema_name._sc_asset union " +
            " select id::varchar as pkey from schema_name._sc_bom union " +
            " select id::varchar as pkey from schema_name._sc_facilities union " +
            " select id::varchar as pkey from schema_name._sc_asset_model union " +
            " select task_item_code::varchar as pkey from schema_name._sc_task_item union " +
            " select work_code::varchar as pkey from schema_name._sc_works union " +
            " select work_code::varchar as pkey from schema_name._sc_asset_works union " +
            " select work_code::varchar as pkey from schema_name._sc_bom_works_detail) tb on f.business_no_id=tb.pkey";

    String CUSTOMER_AUTH_SQL = "(select distinct sr.asset_position_code from " +
            " ${schema_name}._sc_role_asset_position sr " +
            " join ${schema_name}._sc_role f on sr.role_id = f.id " +
            " join ${schema_name}._sc_position_role g on g.role_id = f.id " +
            " join ${schema_name}._sc_position h on h.id = g.position_id " +
            " join ${schema_name}._sc_user_position i on i.position_id = g.position_id " +
            " join ${schema_name}._sc_user k on i.user_id = k.id" +
            " where k.id= #{user_id} )";

    // 下拉选配置
    Map<String, String> SELECTS_FOR_DETAIL = new HashMap<String, String>() {
        {
            //扫码获取设备信息
            put("asset_info_by_scan", "SELECT id as value, asset_name as text,asset_code,supplier_id,category_id,importment_level_id," +
                    " position_code ,manufacturer_id,asset_model_id,running_status_id " +
                    " FROM " + SCHEMA_NAME + "._sc_asset " +
                    "where id = " + "'" + SQL_WHERE_NAME + "'");
            //流转记录日志
            put("works_log", "SELECT business_type_id as value, business_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_business_type " +
                    "where business_type_id=" + SQL_WHERE_NAME);
            //工单业务类型
            put("business_type", "SELECT business_type_id as value, business_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_business_type " +
                    "where business_type_id=" + SQL_WHERE_NAME);
            //工单模版
            put("work_template", "SELECT relation_type as relation, work_template_code as value, work_template_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_template  " +
                    "where  is_use = '1' AND parent_code='0' AND work_template_code='" + SQL_WHERE_NAME + "'");
            //工单模版
            put("work_template_for_detail", "SELECT relation_type as relation, work_template_code as value, work_template_name as text, work_type_id " +
                    " FROM " + SCHEMA_NAME + "._sc_work_template  " +
                    "where  is_use = '1' " + SQL_WHERE_NAME);
            //工单模版-业务信息
            put("work_template_business", "SELECT b.business_type_id " +
                    " FROM " + SCHEMA_NAME + "._sc_work_template a, " + SCHEMA_NAME + "._sc_work_type b " +
                    "where a.work_type_id = b.id and a.is_use = '1' and b.is_use = '1' AND a.work_template_code='" + SQL_WHERE_NAME + "'");
            //根据工单类型id取工单类型信息
            put("work_type", "SELECT id::varchar as value, type_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type  " +
                    "where is_use = '1' AND id=" + SQL_WHERE_NAME);
            //根据工单类型id取business_type_id
            put("work_type_business_type_id", "SELECT business_type_id as value, type_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type  " +
                    "where is_use = '1' AND id=" + SQL_WHERE_NAME);
            //根据工单类型business_type_id取工单类型信息
            put("work_type_by_business_type_id", "SELECT id as value, type_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type  " +
                    "where is_use = '1' AND business_type_id=" + SQL_WHERE_NAME);
            put("work_type_by_business_info", "SELECT id as value, type_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type  " +
                    "where is_use = '1' " + SQL_WHERE_NAME);
            put("work_pool", "SELECT id as value, pool_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_pool " +
                    "where is_use = '1' AND id=" + SQL_WHERE_NAME);
            put("facilities", "SELECT ltrim(to_char(f.id, '" + FACILITY_ID_LEN + "')) as value, f.short_title as text, f.facility_no as relation, " +
                    "f.currency_id as currency_id, cur.currency_code as currency_name, f.location[0] as fac_longitude, f.location[1] as fac_latitude " +
                    " FROM " + SCHEMA_NAME + "._sc_facilities f" +
                    " LEFT JOIN " + SCHEMA_NAME + "._sc_currency cur ON f.currency_id = cur.id " +
                    "where ltrim(to_char(f.ID, '" + FACILITY_ID_LEN + "'))=" + SQL_WHERE_NAME + " UNION " +
                    "SELECT position_code as value, position_name as text, position_code as relation, " +
                    "0 as currency_id, '' as currency_name, location[0] as fac_longitude, location[1] as fac_latitude " +
                    "FROM " + SCHEMA_NAME + "._sc_asset_position " +
                    "where position_code=" + SQL_WHERE_NAME);

            put("facilities_dtl", "SELECT * FROM (SELECT f.location[0] as fac_longitude, f.location[1] as fac_latitude FROM " + SCHEMA_NAME + "._sc_facilities f " +
                    "where ltrim(to_char(f.ID, '" + FACILITY_ID_LEN + "'))=" + SQL_WHERE_NAME + " UNION " +
                    "SELECT af.location[0] as fac_longitude, af.location[1] as fac_latitude " +
                    "FROM " + SCHEMA_NAME + "._sc_asset_position_organization apo, " + SCHEMA_NAME + "._sc_facilities af " +
                    "where apo.org_id = af.id and position_code=" + SQL_WHERE_NAME + " ) t limit 1");

            put("userGroupsFacilities", "SELECT f.short_title as text, ltrim(to_char(f.id, '" + FACILITY_ID_LEN + "')) as value, f.facility_no as relation " +
                    "FROM " + SCHEMA_NAME + "._sc_facilities f " +
                    "LEFT JOIN " + SCHEMA_NAME + "_sc_group_org g on g.org_id=f.id " +
                    "where g.group_id in(" + SQL_WHERE_NAME + ") UNION " +
                    "SELECT position_name as text, position_code as value, " +
                    "position_code as relation " +
                    "FROM " + SCHEMA_NAME + "._sc_asset_position p" +
                    "LEFT JOIN " + SCHEMA_NAME + "_sc_asset_position_organization pf on p.position_code= pf.position_code " +
                    "LEFT JOIN " + SCHEMA_NAME + "_sc_group_org pg on pg.org_id=pf.org_id" +
                    "where  pg.group_id in(" + SQL_WHERE_NAME + ") ");
            //用户组
            put("userGroups", "SELECT a.parent_id as parent,a.id as value, a.group_name as text FROM "
                    + SCHEMA_NAME + "._sc_group a " + "LEFT JOIN " + SCHEMA_NAME
                    + "._sc_group b ON a.parent_id=b.id  " +
                    " where  a.id in(" + SQL_WHERE_NAME + ") ");
            // 组织
            put("onlyFacilities", "SELECT facility_no as relation, id as value, short_title as text FROM "
                    + SCHEMA_NAME + "._sc_facilities where id=" + SQL_WHERE_NAME);
            //设备
            put("asset", "select asset_code as relation, category_id,t1.position_code, " +
                    "t1.currency_id as currency_id, cur.currency_code as currency_name, t1.id as value, t1.asset_name || '(' || t1.asset_code || ')' as text, t1.asset_code, t1.asset_name " +
                    "from " + SCHEMA_NAME + "._sc_asset t1 " +
                    " LEFT JOIN " + SCHEMA_NAME + "._sc_currency cur ON t1.currency_id = cur.id " +
                    " where t1.id='" + SQL_WHERE_NAME + "'");
            //扫码取设备
            put("assetByCode", "select asset_code, asset_name, id as value, asset_name || '(' || asset_code || ')' as text from " + SCHEMA_NAME + "._sc_asset  " +
                    "where status > 0 and asset_code=" + SQL_WHERE_NAME);
            //设备运行状态
            put("asset_status", "SELECT id as value, running_status as text FROM " + SCHEMA_NAME + "._sc_asset_running_status " +
                    "where is_use = '1' AND id=" + SQL_WHERE_NAME);
            //状态
            put("status", "SELECT id::varchar as value, status as text " +
                    "from " + SCHEMA_NAME + "._sc_status " +
                    "where id=" + SQL_WHERE_NAME);
            //用户
            put("allUser", "SELECT mobile, account as value, user_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_user u " +
                    " left JOIN ( " +
                    " select ur.user_id, string_agg(name||'' , ',') roles from "
                    + SCHEMA_NAME + "._sc_user_role ur INNER JOIN "
                    + SCHEMA_NAME + "._sc_role r on ur.role_id = r.id " +
                    " group by ur.user_id ) r on u.id = r.user_id" +
                    " where u.id='" + SQL_WHERE_NAME + "'");
            //工单池用户
            put("user", "SELECT id as value, user_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_user " +
                    "where id='" + SQL_WHERE_NAME + "'");

            //用户明细
            put("user_detail", "SELECT u.id, u.account, u.user_name, u.mobile, uw.mini_program_open_id as wx_open_id, uw.official_open_id " +
                    "from " + SCHEMA_NAME + "._sc_user u " +
                    "left join " + SCHEMA_NAME + "._sc_user_wei_xin_login_info uw on uw.user_id = u.id " +
                    "where u.id='" + SQL_WHERE_NAME + "'");

            //点巡检点
            put("inspection_area", "SELECT position_code as relation, position_code as value, position_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_asset_position " +
                    "where  position_code=" + SQL_WHERE_NAME);//is_use = '1' AND 暂时没有禁用启用字段

            //故障类型表
            put("fault_type", "SELECT id::varchar as value, type_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_fault_type " +
                    "where is_use='1' AND id=" + SQL_WHERE_NAME);

            //维修类型表
            put("repair_type", "SELECT id as value, type_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_repair_type " +
                    "where is_use='1' AND id=" + SQL_WHERE_NAME);

            //故障代码
            put("fault_code", "SELECT code as value, concat(code_name,'【',fault_code,'】') as text  " +
                    "from " + SCHEMA_NAME + "._sc_fault_code " +
                    "where is_use='1' AND code=" + SQL_WHERE_NAME);

            //预防措施
            put("preventive_type", "SELECT id as value, type_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_preventive_type " +
                    "where is_use='1' AND id=" + SQL_WHERE_NAME);

            // 二维码
            put("code_template", "SELECT id as value, code_template_code as text FROM "
                    + SCHEMA_NAME + "._sc_code_template where id=" + SQL_WHERE_NAME);

            // 位置类型
            put("facility_type", "SELECT id::varchar as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_facility_type where id=" + SQL_WHERE_NAME);

            // 省市区
            put("area", "SELECT code as value, areaname as text FROM "
                    + SCHEMA_NAME + "._sc_area where code= " + SQL_WHERE_NAME);

            // 设备位置
            put("asset_position", "SELECT position_code as value, position_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_position where position_code= '" + SQL_WHERE_NAME + "'");

            // 设备位置描述
            put("asset_position_remark", "SELECT position_code as value, remark as text FROM "
                    + SCHEMA_NAME + "._sc_asset_position where position_code= '" + SQL_WHERE_NAME + "'");

            // 配件BOM表
            put("bom", "SELECT b.ID as value, bom_name as text FROM "
                    + SCHEMA_NAME + "._sc_bom b where is_use = '1' and status = 1 and b.id = '" + SQL_WHERE_NAME + "'");

            // 配件BOM表
            put("bom_list", "SELECT DISTINCT b.id as value, " +
                    "  b.bom_name || '(' || b.material_code || ')' as text, " +
                    " COALESCE( bs.show_price,b.show_price) AS price,b.material_code,b.type_id,b.bom_model,b.manufacturer_id,COALESCE(bs.quantity,0) AS now_quantity, bs.stock_id, COALESCE(bs.security_quantity, 0) security_quantity " +
                    " FROM " + SCHEMA_NAME + "._sc_bom b " +
                    " LEFT JOIN " + SCHEMA_NAME + "._sc_bom_stock bs on bs.bom_id=b.id " +
                    "WHERE b.is_use = '1' AND b.status = 1  and b.id = '" + SQL_WHERE_NAME + "'");

            // 配件BOM表（物料编码查询）
            put("bom_material", "SELECT b.*, bt.ID AS bom_model_id FROM "
                    + SCHEMA_NAME + "._sc_bom b LEFT JOIN " + SCHEMA_NAME + "._sc_bom_template bt ON b.bom_model = bt.bom_model " +
                    " where b.is_use = '1' AND b.status = 1 and b.material_code = " + SQL_WHERE_NAME);

            // 备件类别
            put("bom_type", "SELECT id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_bom_type where id= " + SQL_WHERE_NAME);

            // 备件类别（多选）
            put("bom_type_multiple", "SELECT string_agg(a.id, ',') as value, string_agg(a.type_name, ',') as text FROM "
                    + SCHEMA_NAME + "._sc_bom_type a where id in (" + SQL_WHERE_NAME + ")");

            // 备件型号
            put("bom_template", "SELECT bom_model as value, bom_model as text FROM "
                    + SCHEMA_NAME + "._sc_bom_template where bom_model= " + SQL_WHERE_NAME);

            // 库房信息
            put("stock_list", "SELECT id::varchar as value, stock_name as text FROM "
                    + SCHEMA_NAME + "._sc_stock where status=1 AND is_use ='1' AND id='" + SQL_WHERE_NAME + "'");

            // 设备类型
            put("asset_type", "SELECT a.parent_id as parent, a.id as value, a.category_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_category a where id= " + SQL_WHERE_NAME);

            // 设备类型种类
            put("asset_category_type", "SELECT id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_category_type where id= " + SQL_WHERE_NAME);

            // 设备类型（多选）
            put("asset_type_multiple", "SELECT string_agg(a.id, ',') as value, string_agg(a.category_name, ',') as text FROM "
                    + SCHEMA_NAME + "._sc_asset_category a where id in (" + SQL_WHERE_NAME + ")");

            // 重要级别
            put("level", "SELECT id as value, level_name as text FROM "
                    + SCHEMA_NAME + "._sc_importment_level where id= " + SQL_WHERE_NAME);

            // 单位
            put("unit", "SELECT id as value, unit_name as text FROM "
                    + SCHEMA_NAME + "._sc_unit where id= " + SQL_WHERE_NAME);

            // 角色列表
            put("role_list", "SELECT id::varchar as value, name as text FROM "
                    + SCHEMA_NAME + "._sc_role where sys_role <> '1' AND id='" + SQL_WHERE_NAME + "'");

            // 用途
            put("use_to", "SELECT id as value, use_to_name as text FROM "
                    + SCHEMA_NAME + "._sc_use_to where id= " + SQL_WHERE_NAME);

            // 客户负责人
            put("customer_contact", "SELECT contact_email as value, contact_name as text FROM "
                    + SCHEMA_NAME + "._sc_organization_contact where org_id= " + SQL_WHERE_NAME);

            //客户联系人
            put("customer_contact_all", "SELECT id as value, contact_name as text,contact_name,contact_mobile as mobile,contact_email,wei_xin FROM "
                    + SCHEMA_NAME + "._sc_customer_contact where id = '" + SQL_WHERE_NAME +"'::int");

            // 成本中心
            put("cost_center", "SELECT id as value, cost_name as text FROM "
                    + SCHEMA_NAME + "._sc_cost_center where id= " + SQL_WHERE_NAME);
            //
            put("monitor_area", "SELECT monitor_area_code as value, monitor_area_name as text FROM "
                    + SCHEMA_NAME + "._sc_monitor_area where id= " + SQL_WHERE_NAME);

            // 部位（设备）
            put("asset_part", "SELECT id as value, part_name as text FROM " + SCHEMA_NAME + "._sc_part where id= " + SQL_WHERE_NAME);

            // 完修说明
            put("finished_type", "SELECT id as value, finished_name as text FROM "
                    + SCHEMA_NAME + "._sc_work_finished_type WHERE is_use = '1' AND id='" + SQL_WHERE_NAME + "'");

            // 工单信息
            put("work_info", "SELECT d.sub_work_code as value, COALESCE(d.title, w.title) || '（' || d.work_code || '）' as text FROM "
                    + SCHEMA_NAME + "._sc_works w, " + SCHEMA_NAME + "._sc_works_detail d "
                    + "where sub_work_code = " + SQL_WHERE_NAME);

            // 产品
            put("product_name", "SELECT id as value, product_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_use_product where id=" + SQL_WHERE_NAME);

            // 资产状态
            put("asset_info_status", "SELECT id as value, status as text " +
                    "from " + SCHEMA_NAME + "._sc_asset_status where id=" + SQL_WHERE_NAME);

            //所有设备型号
            put("asset_model_all", "SELECT a.id as value, a.model_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_model a WHERE a.id=" + SQL_WHERE_NAME);

            //所有货币单位
            put("currency", "SELECT t.id as value, t.currency_code as text FROM "
                    + SCHEMA_NAME + "._sc_currency t WHERE t.id=" + SQL_WHERE_NAME);

            //流程
            put("work_flow", "SELECT pref_id as value, show_name as text FROM "
                    + SCHEMA_NAME + "._sc_work_flow_info t WHERE t.pref_id='" + SQL_WHERE_NAME + "'");

            //短信模板
            put("msg_content_config", "SELECT id as value, pattern_description as text FROM "
                    + SCHEMA_NAME + "._sc_msg_content_config t WHERE t.id='" + SQL_WHERE_NAME + "'");

            // 文档类别
            put("file_category", "SELECT id as value, file_category_name as text " +
                    "from " + SCHEMA_NAME + "._sc_file_category t where t.id='" + SQL_WHERE_NAME + "'");

            // 文档类型
            put("file_type", "SELECT id as value, type_name as text " +
                    "from " + SCHEMA_NAME + "._sc_file_type t where t.id='" + SQL_WHERE_NAME + "'");
        }
    };
    Map<String, String> SELECTS = new HashMap<String, String>() {
        {
            // 库房信息
            put("stock_list", "SELECT id::varchar as value, stock_name as text FROM "
                    + SCHEMA_NAME + "._sc_stock where status=1 AND is_use ='1' ");
            //流程
            put("work_flow", "SELECT pref_id as value, show_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_flow_info ");
            //短信模板
            put("msg_content_config", "SELECT id as value, pattern_description as text " +
                    " FROM " + SCHEMA_NAME + "._sc_msg_content_config ");
            //工单业务类型
            put("business_type", "SELECT business_type_id as value, business_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_business_type " +
                    "order by business_type_id ");
            //工单模版
            put("work_template", "SELECT work_template_code as value, work_template_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_template " +
                    "WHERE is_use = '1' ORDER BY work_template_code desc");
//            put("work_template", "SELECT relation_type as relation, work_template_code as value, work_template_name as text " +
//                    " FROM " + SCHEMA_NAME + "._sc_work_template " +
//                    "WHERE is_use = '1' AND parent_code='0' ORDER BY work_template_code desc");
            //流程模版
//            put("flow_template", "SELECT  flow_template_code as value, flow_name as text " +
//                    " FROM " + SCHEMA_NAME + "._sc_flow_template " +
//                    "WHERE is_use = '1'  ORDER BY flow_template_code desc");
            //工单模版
//            put("work_template_for_detail", "SELECT work_template_code as value, work_template_name as text, relation_type as relation, work_type_id " +
//                    " FROM " + SCHEMA_NAME + "._sc_work_template  " +
//                    "where  is_use = '1' " + SQL_WHERE_NAME);
            //工单类型
            put("work_type", "SELECT business_type_id as relation, id::varchar as value, type_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type WHERE is_use = '1' and business_type_id < " + SensConstant.BUSINESS_NO_2000 +
                    " order by data_order ");
            //维保计划查询关联了适用对象的工单类型
            put("work_type_with_relation_type", "SELECT wt.business_type_id as relation, wt.id::varchar as value, wt.type_name as text,wfi.relation_type " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type wt JOIN " + SCHEMA_NAME + "._sc_work_flow_info wfi ON wfi.work_type_id = wt.id WHERE wt.is_use = '1' and wt.business_type_id < " + SensConstant.BUSINESS_NO_2000 +
                    " order by data_order ");
            //工单类型（手机端待办列表显示）
//            put("work_type_phone_todo", "SELECT flow_template_code as relatiHon, business_type_id as relation2, id as value, type_name as text " +
//                    " FROM " + SCHEMA_NAME + "._sc_work_type WHERE is_use = '1' and business_type_id < 40 and flow_template_code is not null and flow_template_code <> '' " +
//                    " order by data_order ");
            //工单类型（所有流程类型）
            put("work_type_all", "SELECT business_type_id as relation,id as value, type_name as text " +
                    " FROM " + SCHEMA_NAME + "._sc_work_type WHERE is_use = '1' " +
                    " order by data_order ");
            //工单池
//            put("work_pool", "SELECT DISTINCT p.id as value, p.pool_name as text FROM " + SCHEMA_NAME + "._sc_work_pool p, "
//                    + SCHEMA_NAME + "._sc_work_pool_facility f left join "
//                    + SCHEMA_NAME + "._sc_asset_position_organization po on po.org_id = f.facility_id left join "
//                    + SCHEMA_NAME + "._sc_facilities fc on fc.id = f.facility_id and fc.is_use = '1' AND fc.status > " + StatusConstant.STATUS_DELETEED + " AND fc.org_type_id in (" + FACILITY_INSIDE + ") " +
//                    " where p.id = f.pool_id and p.is_use = '1' " + SQL_WHERE_NAME + " order by p.id desc ");
//            put("userGroupsFacilities", "SELECT f.facility_no as relation, ltrim(to_char(f.id, '" + FACILITY_ID_LEN + "')) as value, f.short_title as text FROM " + SCHEMA_NAME + "._sc_facilities f " +
//                    "LEFT JOIN " + SCHEMA_NAME + "._sc_group_org g on g.org_id=f.id " +
//                    "where g.group_id in(" + SQL_WHERE_NAME + ") UNION " +
//                    "SELECT p.position_code as relation, p.position_code as value, p.position_name as text FROM " + SCHEMA_NAME + "._sc_asset_position p " +
//                    "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position_organization pf on p.position_code= pf.position_code " +
//                    "LEFT JOIN " + SCHEMA_NAME + "._sc_group_org pg on pg.org_id=pf.org_id " +
//                    "where pg.group_id in(" + SQL_WHERE_NAME + ") ");
            //用户组
            put("allUserGroups", "SELECT a.parent_id as parent,a.group_code AS relation, a.id as value, a.group_name as text FROM "
                    + SCHEMA_NAME + "._sc_group a "
                    + "ORDER BY a.id,a.parent_id desc");
            //用户组
//            put("selfUserGroups", "SELECT a.parent_id as parent,a.id AS relation, a.id as value, a.group_name as text FROM "
//                    + SCHEMA_NAME + "._sc_group a " +
//                    "LEFT JOIN " + SCHEMA_NAME
//                    + "._sc_group b ON a.parent_id=b.id  "
//                    + "LEFT JOIN " + SCHEMA_NAME
//                    + "._sc_user_group c ON a.id=c.group_id where 1=1 " +
//                    SQL_WHERE_NAME
//                    + "ORDER BY a.id,a.parent_id desc");
            // 组织
            put("onlyFacilities", "SELECT parent_id as parent, id as value, short_title as text FROM " + SCHEMA_NAME + "._sc_facilities " +
                    "where is_use = '1' and status > " + StatusConstant.STATUS_DELETEED);
            // 外部组织
            put("customerFacilities", "SELECT a.parent_id as parent,a.id as value, a.short_title as text FROM "
                    + SCHEMA_NAME + "._sc_facilities a WHERE a.org_type_id in (" + FACILITY_OUTSIDE + ") "
                    + "ORDER BY a.id,a.parent_id desc");
            //内部
            put("insideFacilities", "SELECT a.parent_id as parent,a.facility_no as relation, a.id as value, a.short_title as text FROM "
                    + SCHEMA_NAME + "._sc_facilities a " + "LEFT JOIN " + SCHEMA_NAME
                    + "._sc_facilities b ON a.parent_id=b.id and b.is_use = '1' and b.status > " + StatusConstant.STATUS_DELETEED + " AND b.org_type_id in (" + FACILITY_INSIDE + ") "
                    + " WHERE a.is_use = '1' and a.status > " + StatusConstant.STATUS_DELETEED + " AND a.org_type_id in (" + FACILITY_INSIDE + ") "
                    + " ORDER BY a.id,a.parent_id desc");
            //设备（内部组织）
            put("asset", "select DISTINCT t1.asset_code, " +
                    " t1.category_id,t1.running_status_id,t1.guarantee_status_id,t1.position_code,ap.position_name,t1.currency_id as currency_id, cur.currency_code as currency_name, t1.asset_name, t1.id as value, t1.asset_name || '(' || asset_code || ')' as text " +
                    " from " + SCHEMA_NAME + "._sc_asset t1 LEFT JOIN " + SCHEMA_NAME + "._sc_asset_organization ao ON ao.asset_id = t1.id "
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities t2 ON t2. ID = ao.org_id AND t2.is_use = '1' AND t2.status > " + StatusConstant.STATUS_DELETEED + " AND t2.org_type_id in (" + FACILITY_INSIDE + ") " // 设备组织
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position ap ON ap.position_code = t1.position_code "
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position_organization apo ON ap. position_code = apo.position_code "
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities t3 ON t3. ID = apo.org_id AND t3.is_use = '1' AND t3.status > " + StatusConstant.STATUS_DELETEED + " AND t3.org_type_id in (" + FACILITY_INSIDE + ") " // 设备位置组织
                    + " LEFT JOIN " + SCHEMA_NAME + "._sc_asset_category ac on t1.category_id=ac.id "
                    + " LEFT JOIN " + SCHEMA_NAME + "._sc_currency cur ON t1.currency_id = cur.id "
//                    + SQL_WHERE_NAME
                    + " WHERE t1.status > " + StatusConstant.STATUS_DELETEED + " order by asset_code ");
            put("asset_with_position", "select DISTINCT asset_code, " +
                    " t1.position_code,ap.position_name,t1.currency_id as currency_id, cur.currency_code as currency_name, t1.id as value, t1.id as asset_id, t1.asset_name || '(' || asset_code || ')' as text,t1.asset_name as relation_name,t1.category_id,t1.status " +
                    " from " + SCHEMA_NAME + "._sc_asset t1 LEFT JOIN " + SCHEMA_NAME + "._sc_asset_organization ao ON ao.asset_id = t1.id "
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities t2 ON t2. ID = ao.org_id AND t2.is_use = '1' AND t2.status > " + StatusConstant.STATUS_DELETEED + " AND t2.org_type_id in (" + FACILITY_INSIDE + ") " // 设备组织
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position ap ON ap.position_code = t1.position_code "
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position_organization apo ON ap. position_code = apo.position_code "
                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities t3 ON t3. ID = apo.org_id AND t3.is_use = '1' AND t3.status > " + StatusConstant.STATUS_DELETEED + " AND t3.org_type_id in (" + FACILITY_INSIDE + ") " // 设备位置组织
                    + " LEFT JOIN " + SCHEMA_NAME + "._sc_asset_category ac on t1.category_id=ac.id "
                    + " LEFT JOIN " + SCHEMA_NAME + "._sc_currency cur ON t1.currency_id = cur.id "
                    + "where t1.position_code = '" + SQL_WHERE_NAME + "'"
                    + " AND t1.status > " + StatusConstant.STATUS_DELETEED + "AND t1.status != 3 " + SQL_WHERE_LIKE + " order by asset_code ");
            //设备（非工单模块）
//            put("asset_not_work_sheet", "select DISTINCT asset_code, t1.id as value, t1.asset_name || '(' || asset_code || ')' as text from " + SCHEMA_NAME
//                    + "._sc_asset t1 LEFT JOIN " + SCHEMA_NAME + "._sc_asset_organization ao ON ao.asset_id = t1.id "
//                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities t2 ON t2. ID = ao.org_id AND t2.is_use = '1' AND t2.status > " + StatusConstant.STATUS_DELETEED + " AND t2.org_type_id in (" + FACILITY_INSIDE + ") " // 设备组织
//                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position ap ON ap.position_code = t1.position_code "
//                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_asset_position_organization apo ON ap. position_code = apo.position_code "
//                    + "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities t3 ON t3. ID = apo.org_id AND t3.is_use = '1' AND t3.status > " + StatusConstant.STATUS_DELETEED + " AND t3.org_type_id in (" + FACILITY_INSIDE + ") " // 设备位置组织
//                    + SQL_WHERE_NAME + " AND t1.status > " + StatusConstant.STATUS_DELETEED + " order by asset_code ");
            //设备运行状态
            put("asset_status", "SELECT priority_level as relation, id as value, running_status as text FROM "
                    + SCHEMA_NAME + "._sc_asset_running_status WHERE is_use = '1'  order by data_order ");
            //状态
            put("status", "SELECT id::varchar as value, status as text " +
                    "from " + SCHEMA_NAME + "._sc_status where 1=1 order by id");

            // 有效用户
            put("validUser", "SELECT id::varchar as value, user_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_user where status = 1");

            //用户
            put("allUser", "SELECT mobile, id as value, user_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_user where 1=1");

            //用户明细
            put("user_detail", "SELECT u.account, u.user_name, u.mobile, uw.mini_program_open_id as wx_open_id, uw.official_open_id " +
                    "from " + SCHEMA_NAME + "._sc_user u " +
                    "left join " + SCHEMA_NAME + "._sc_user_wei_xin_login_info uw on uw.user_id = u.id " +
                    "where u.id= '" + SQL_WHERE_NAME + "'");

            //点巡检点
            put("inspection_area", "SELECT position_code::varchar as value, position_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_asset_position  ");

            //故障类型表
            put("fault_type", "SELECT id::varchar as value, type_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_fault_type where is_use='1' order by data_order ");

            //维修类型表
            put("repair_type", "SELECT id::varchar as value, type_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_repair_type where is_use='1' order by data_order ");

            //故障代码
            put("fault_code", "SELECT code::varchar as value, concat(code_name,'【',fault_code,'】') as text  " +
                    "from " + SCHEMA_NAME + "._sc_fault_code where is_use='1' order by data_order ");

            // 故障代码（类型）
            put("category_fault_code", "SELECT f.code::varchar as value, concat(f.code_name,'【',f.fault_code,'】') as text  " +
                    "from " + SCHEMA_NAME + "._sc_fault_code f " +
                    "left join " + SCHEMA_NAME + "._sc_asset_category_fault ac on f.code=ac.fault_code " +
                    "left join " + SCHEMA_NAME + "._sc_asset a on ac.category_id=a.category_id " +
                    "where is_use='1' AND a.id=" + SQL_WHERE_NAME + " order by f.order");

            //预防措施
            put("preventive_type", "SELECT id as value, type_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_preventive_type where is_use='1' order by data_order ");
            //设备类型小程序
            put("asset_type_app", "SELECT id as value, category_name as text  " +
                    "from " + SCHEMA_NAME + "._sc_asset_category where is_use='1' and parent_id=0 order by data_order ");
            //设备类型pc
            put("asset_type_all", "SELECT a.parent_id as parent,b.category_name as text_pr,a.category_code as relation, a.id as value, a.category_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_category a " + "LEFT JOIN " + SCHEMA_NAME
                    + "._sc_asset_category b ON a.parent_id=b.id AND b.is_use='1' WHERE a.is_use='1'  "
                    + "ORDER BY a.data_order asc ,a.id desc ,a.parent_id desc");

            //所有设备型号
            put("asset_model_all", "SELECT a.id::varchar as value, a.model_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_model a WHERE a.is_use='1' ORDER BY a.id desc");

            //监控配置项（不包含SCADA）
            put("metadata_monitors_list", "SELECT u.unit_name as relation, m.id as value, m.monitor_name as text " +
                    "FROM " + SCHEMA_NAME + "._sc_metadata_monitors m " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_unit u ON u.id = m.unit_id " +
                    "WHERE m.show_type <> 2 ORDER BY m.id desc");

            // 服务商
            put("service_provider", "SELECT a.parent_id as parent,a.id as value, a.short_title as text, a.facility_no as relation FROM "
                    + SCHEMA_NAME + "._sc_facilities a " + "LEFT JOIN " + SCHEMA_NAME
                    + "._sc_facilities b ON a.parent_id=b.id and b.is_use = '1' and b.status > " + StatusConstant.STATUS_DELETEED + " AND b.org_type_id in (" + FACILITY_SERVICE_PROVIDER + ") "
                    + " WHERE a.is_use = '1' and a.status > " + StatusConstant.STATUS_DELETEED + " and a.org_type_id in (" + FACILITY_SERVICE_PROVIDER + ") "
                    + "ORDER BY a.id,a.parent_id desc");

            // 供应商
            put("customer", "SELECT a.parent_id as parent, a.facility_no as relation, a.id::varchar as value, a.short_title as text FROM "
                    + SCHEMA_NAME + "._sc_facilities a " + "LEFT JOIN " + SCHEMA_NAME
                    + "._sc_facilities b ON a.parent_id=b.id and b.is_use='1' and b.status > " + StatusConstant.STATUS_DELETEED + " AND b.org_type_id in (" + FACILITY_SUPPLIER + ") "
                    + " WHERE a.is_use='1' and a.status > " + StatusConstant.STATUS_DELETEED + " and a.org_type_id in (" + FACILITY_SUPPLIER + ") "
                    + "ORDER BY a.id,a.parent_id desc");
            //所有厂商
            put("all_facilities", "SELECT a.parent_id as parent, a.id as value, a.short_title as text FROM "
                    + SCHEMA_NAME + "._sc_facilities a ORDER BY a.id,a.parent_id desc");
            // 制造商
            put("manufacturer", "SELECT a.parent_id as parent,a.facility_no as relation, a.id::varchar as value, a.short_title as text FROM "
                    + SCHEMA_NAME + "._sc_facilities a " + "LEFT JOIN " + SCHEMA_NAME
                    + "._sc_facilities b ON a.parent_id=b.id and b.is_use = '1' and b.status > " + StatusConstant.STATUS_DELETEED + " AND b.org_type_id in (" + FACILITY_MANUFACTURER + ") "
                    + " WHERE a.is_use = '1' and a.status > " + StatusConstant.STATUS_DELETEED + " and a.org_type_id in (" + FACILITY_MANUFACTURER + ") "
                    + "ORDER BY a.id,a.parent_id desc");

            // 二维码
//            put("code_template", "SELECT id as value, code_template_code as text FROM "
//                    + SCHEMA_NAME + "._sc_code_template order by id ");

            // 位置类型
            put("facility_type", "SELECT id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_facility_type order by id ");

            //组织类型
            put("org_type_id", "SELECT id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_org_type_id WHERE is_use = '1' order by id ");

            // 地理位置
//            put("address", "SELECT id as value, address_name as text FROM "
//                    + SCHEMA_NAME + "._sc_address order by id ");

            // 省市区
            put("area", "SELECT code as value, area_name as text FROM "
                    + SCHEMA_NAME + "._sc_area a where a.is_use = '1' " + SQL_WHERE_NAME + " order by code ");
            // 设备位置
            put("asset_position", "SELECT parent_code as parent, position_code as value, position_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_position order by position_code ");

            // 设备位置
            put("asset_position_by_parent_code", "SELECT parent_code as parent, position_code as value, position_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_position ap WHERE 1=1 " + SQL_WHERE_NAME + " order by position_code ");

            // 重要级别
            put("level", "SELECT id as value, level_name as text FROM "
                    + SCHEMA_NAME + "._sc_importment_level where is_use = '1' order by id ");

            // 单位
            put("unit", "SELECT id as value, unit_name as text FROM "
                    + SCHEMA_NAME + "._sc_unit where is_use='1' order by id ");

            // 配件BOM表
            put("bom", "SELECT material_code as value, bom_name as text FROM "
                    + SCHEMA_NAME + "._sc_bom where is_use = '1'  AND status = 1 order by id ");

            // 库房备件
            put("stock_bom", "SELECT b.id as value, bom_name as text,b.material_code,b.bom_model,b.type_id,b.manufacturer_id,COALESCE(s.show_price, b.show_price) price,b.unit_id,b.currency_id," +
                    "(SELECT sum(quantity) FROM " + SCHEMA_NAME + "._sc_bom_stock bs WHERE b.id = bs.bom_id and s.stock_id = bs.stock_id ) AS now_quantity , " +
                    " bsd.bom_code FROM " + SCHEMA_NAME + "._sc_bom b JOIN " + SCHEMA_NAME + "._sc_bom_stock s on b.id = s.bom_id " +
                    " JOIN " + SCHEMA_NAME + "._sc_bom_stock_detail bsd ON s.stock_id = bsd.stock_id AND bsd.bom_id = s.bom_id " +
                    "where b.is_use = '1' AND  b.status = 1 and s.stock_id = "
                    + SQL_WHERE_NAME + " order by b.id ");
            //备件列表
            put("bom_list", "SELECT b.id as value, " +
                    "  b.bom_name || '(' || b.material_code || ')' as text, " +
                    " b.show_price AS price,b.material_code,b.type_id,b.bom_model,b.manufacturer_id," +
                    " COALESCE((SELECT quantity FROM " + SCHEMA_NAME + "._sc_bom_stock bs WHERE b.id = bs.bom_id ORDER BY bs.id DESC LIMIT 1 ), 0) AS now_quantity , " +
                    " (SELECT stock_id::varchar FROM " + SCHEMA_NAME + "._sc_bom_stock bs WHERE b.id = bs.bom_id ORDER BY bs.id DESC LIMIT 1 ) AS stock_id," +
                    " COALESCE ((SELECT security_quantity FROM " + SCHEMA_NAME + "._sc_bom_stock bs WHERE b.id = bs.bom_id AND bs.stock_id = stock_id ORDER BY bs.id DESC LIMIT 1 ),0) AS security_quantity" +
                    " FROM " + SCHEMA_NAME + "._sc_bom b  " +
                    "WHERE b.is_use = '1' AND b.status = 1 " + SQL_WHERE_NAME + " order by b.id ");

            //备件出库列表
            put("bom_out_list", "SELECT b.id::varchar AS value,b.bom_name AS text,bsd.quantity,b.show_price,b.material_code,b.type_id,b.bom_model,bsd.bom_code," +
                    "(SELECT quantity FROM " + SCHEMA_NAME + "._sc_bom_stock_detail bs WHERE b.id = bs.bom_id AND bsd.bom_code = bs.bom_code ORDER BY bs.id DESC LIMIT 1 ) AS now_quantity , " +
                    " (SELECT stock_id::varchar FROM " + SCHEMA_NAME + "._sc_bom_stock bs WHERE b.id = bs.bom_id ORDER BY bs.id DESC LIMIT 1 ) AS stock_id," +
                    " COALESCE ((SELECT security_quantity FROM " + SCHEMA_NAME + "._sc_bom_stock bs WHERE b.id = bs.bom_id AND bs.stock_id = stock_id ORDER BY bs.id DESC LIMIT 1 ),0) AS security_quantity" +
                    " FROM " + SCHEMA_NAME + "._sc_bom_stock_detail bsd " +
                    " LEFT JOIN " + SCHEMA_NAME + "._sc_bom AS b ON b.id = bsd.bom_id " +
                    " WHERE b.is_use = '1' AND b.status = 1 order by b.id ");
            // 备件类型表
            put("bom_type", "SELECT parent_id as parent, id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_bom_type where is_use='1' order by id ");

            // 备件类型表(全部)
            put("bom_type_all", "SELECT parent_id as parent, id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_bom_type order by id ");

            // 备件明细信息（按备件型号查询）
//            put("bom_detail_info", "SELECT b.bom_name,t.type_name,b.bom_model,b.brand_name,b.material_code, " +
//                    "b.bom_code,b.bom_code as bom_code_show,f.short_title as supplier,u.unit_name " +
//                            "FROM " + SCHEMA_NAME + "._sc_bom b " +
//                            "LEFT JOIN " + SCHEMA_NAME + "._sc_bom_type t on b.type_id = t.id and t.is_use = '1' " +
//                            "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities f on f.is_use = '1' and f.status > " + StatusConstant.STATUS_DELETEED + " and f.org_type_id in ("
//                            + FACILITY_OUTSIDE + ") and f.id = b.supplier " +
//                            "LEFT JOIN " + SCHEMA_NAME + "._sc_unit u on u.is_use = '1' and b.unit_id = u.id " +
//                            "where b.is_use = '1' and b.bom_model = " + SQL_WHERE_NAME);
            put("bom_detail_info", "SELECT b.bom_name,T.type_name,b.bom_model,b.brand_name,b.material_code, " +
                    "bis.bom_code,bis.bom_code AS bom_code_show,f.short_title AS supplier,u.unit_name " +
                    "FROM " + SCHEMA_NAME + "._sc_bom b LEFT JOIN " + SCHEMA_NAME + "._sc_bom_type T ON b.type_id = T.ID AND T.is_use = '1' " +
                    "LEFT JOIN (SELECT bisd.bom_code,bisd.bom_id,bisd.supplier_id FROM " + SCHEMA_NAME + "._sc_bom_in_stock bis " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_bom_in_stock_detail bisd ON bis.in_code = bisd.in_code WHERE bis.status = 60) bis ON b.id = bis.bom_id " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities f ON f.is_use = '1' AND f.status > " + StatusConstant.STATUS_DELETEED + " AND f.org_type_id IN (" + FACILITY_OUTSIDE + ") AND f.ID = bis.supplier_id " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_unit u ON u.is_use = '1' AND b.unit_id = u.ID " +
                    "WHERE b.is_use = '1'  AND b.status = 1 AND b.bom_model = '" + SQL_WHERE_NAME + "'");


            // 备件明细信息（包含已禁用的备件）（按备件型号查询）
            put("bom_detail_all_info", "SELECT b.bom_name,t.type_name,b.bom_model,b.brand_name,b.material_code, " +
                    "b.bom_code,b.bom_code as bom_code_show,f.short_title as supplier,u.unit_name " +
                    "FROM " + SCHEMA_NAME + "._sc_bom b " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_bom_type t on b.type_id = t.id and t.is_use = '1' " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_facilities f on f.is_use = '1' and f.status > " + StatusConstant.STATUS_DELETEED + " and f.org_type_id in ("
                    + FACILITY_OUTSIDE + ") and f.id = b.supplier " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_unit u on u.is_use = '1' and b.unit_id = u.id " +
                    "where  b.status = 1 AND  b.bom_model = " + SQL_WHERE_NAME);

            // 角色列表
            put("role_list", "SELECT id as value, name as text FROM " + SCHEMA_NAME + "._sc_role where sys_role <> '1' order by id ");

            // 任务列表
            put("task_template_list", "SELECT task_template_code as value, task_template_name as text " +
                    "FROM " + SCHEMA_NAME + "._sc_task_template " +
                    "where is_use = '1' order by task_template_code");

            // 设备类型种类
            put("asset_category_type", "SELECT id as value, type_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset_category_type order by id ");

            // 库房下拉框列表信息
            put("onlyStock", "SELECT id as value, stock_name as text FROM "
                    + SCHEMA_NAME + "._sc_stock where status='1' order by stock_code ");

            // 用途
            put("use_to", "SELECT id as value, use_to_name as text FROM "
                    + SCHEMA_NAME + "._sc_use_to order by id ");
            // 客户负责人
            put("customer_contact", "SELECT contact_email as value, contact_name as text FROM "
                    + SCHEMA_NAME + "._sc_organization_contact " + SQL_WHERE_NAME);

            //客户联系人
            put("customer_contact_all", "SELECT id as value, contact_name as text,contact_name,contact_mobile as mobile,contact_email,wei_xin FROM "
                    + SCHEMA_NAME + "._sc_customer_contact where org_id = '" + SQL_WHERE_NAME +"'::int");
            // 成本中心
            put("cost_center", "SELECT id as value, cost_name as text FROM "
                    + SCHEMA_NAME + "._sc_cost_center order by id ");

            //
//            put("monitor_area", "SELECT asset_position_code as relation, monitor_area_code as value, monitor_area_name as text FROM "
//                    + SCHEMA_NAME + "._sc_monitor_area");

            //个人绩效模板列表
//            put("performanceTemplate", "SELECT period as relation, id as value, template_name as text " +
//                    "FROM " + SCHEMA_NAME + "._sc_performance_management_template " +
//                    "WHERE is_use = '1' ORDER BY id DESC");

            //看板配置列表
//            put("dashboard_list", "SELECT id as value, snippet as text FROM " + SCHEMA_NAME + "._sc_dashboard ");

            //部位列表
            put("part_list", "SELECT id as value, part_name as text FROM " + SCHEMA_NAME + "._sc_part ORDER BY id DESC");

            // 部位（设备）
            put("asset_part", "SELECT distinct p.id as value, p.part_name as text FROM " + SCHEMA_NAME + "._sc_asset a, "
                    + SCHEMA_NAME + "._sc_asset_bom b, " + SCHEMA_NAME + "._sc_part p " +
                    "where a.id = b.asset_id and b.part_id = p.id and a.id = " + SQL_WHERE_NAME +
                    " ORDER BY p.id DESC");

            //工具列表
            put("tool_list", "SELECT id as value, tool_name as text FROM " + SCHEMA_NAME + "._sc_tool ORDER BY id DESC");

            // 完修说明
            put("finished_type", "SELECT is_finished as relation, id::varchar as value, finished_name as text FROM "
                    + SCHEMA_NAME + "._sc_work_finished_type WHERE is_use = '1' ORDER BY id");

            // 工单信息
            put("work_info", "SELECT d.sub_work_code as value, COALESCE(d.title, w.title) || '（' || d.work_code || '）' as text " +
                    "FROM " + SCHEMA_NAME + "._sc_works w, " + SCHEMA_NAME + "._sc_works_detail d "
                    + "where w.work_code = d.work_code and w.position_code = " + SQL_WHERE_NAME + " ORDER BY d.work_code desc");

            // 产品
            put("product_name", "SELECT id as value, product_name as text " +
                    "FROM " + SCHEMA_NAME + "._sc_asset_use_product  ORDER BY id ");

            // 工作任务
            put("allWorkTask", "SELECT id as value, task_title as text " +
                    "FROM " + SCHEMA_NAME + "._sc_work_task ORDER BY id ");

            put("facilities_dtl", "SELECT * FROM (SELECT f.location[0] as fac_longitude, f.location[1] as fac_latitude FROM " + SCHEMA_NAME + "._sc_facilities f " +
                    "where ltrim(to_char(f.ID, '" + FACILITY_ID_LEN + "'))=" + SQL_WHERE_NAME + " UNION " +
                    "SELECT af.location[0] as fac_longitude, af.location[1] as fac_latitude " +
                    "FROM " + SCHEMA_NAME + "._sc_asset_position_organization apo, " + SCHEMA_NAME + "._sc_facilities af " +
                    "where apo.org_id = af.id and position_code=" + SQL_WHERE_NAME + " ) t limit 1");

            // 资产状态
            put("asset_info_status", "SELECT id as value, status as text " +
                    "from " + SCHEMA_NAME + "._sc_asset_status where is_use='1' order by data_order");
            // 文档类别
            put("file_category", "SELECT id as value, file_category_name as text " +
                    "from " + SCHEMA_NAME + "._sc_file_category where is_use='1' order by data_order");
            // 文档类型
            put("file_type", "SELECT id as value, type_name as text " +
                    "from " + SCHEMA_NAME + "._sc_file_type where is_use='1' order by data_order");

            //部门-内部、外部
            put("group_with_type", "SELECT id as value,group_name as text, case when parent_id is null then 0 else parent_id end as parent_id " +
                    "FROM " + SCHEMA_NAME + "._sc_group g " +
                    "WHERE case when " + Constants.PLAN_WORK_TEAM_IN + "= " + SQL_WHERE_NAME + " then false = g.is_out else true = g.is_out end  " +
                    "ORDER BY parent_id,id");

            //执行人员-部门下特定角色的人
            put("group_role_user", "SELECT distinct u.id as value,u.user_name as text " +
                    "FROM " + SCHEMA_NAME + "._sc_group g " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_position p on g.id = p.group_id " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_position_role pr ON p.id = pr.position_id " +
                    "LEFT JOIN (SELECT setting_value from " + SCHEMA_NAME + "._sc_system_config yc WHERE yc.config_name = 'executor_role') s ON pr.role_id = any (STRING_TO_ARRAY(s.setting_value,','))" +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_user_position up on p.id = up.position_id " +
                    "JOIN " + SCHEMA_NAME + "._sc_user u ON up.user_id = u.id " +
                    "WHERE g.id = " + SQL_WHERE_NAME + " AND u.status > " + StatusConstant.STATUS_DELETEED);

            //根据sub_work_code获取满足条件的角色人员
            put("user_with_role_sub_work_code", "SELECT DISTINCT u.id as value,u.user_name as text " +
                    "FROM " + SCHEMA_NAME + "._sc_group g " +
                    "JOIN " + SCHEMA_NAME + "._sc_position p on g.id = p.group_id " +
                    "JOIN " + SCHEMA_NAME + "._sc_position_role pr ON p.id = pr.position_id " +
                    "JOIN (SELECT setting_value from " + SCHEMA_NAME + "._sc_system_config yc WHERE yc.config_name = 'executor_role') s ON pr.role_id = any (STRING_TO_ARRAY(s.setting_value,','))" +
                    "JOIN " + SCHEMA_NAME + "._sc_user_position up on p.id = up.position_id " +
                    "JOIN " + SCHEMA_NAME + "._sc_user u ON up.user_id = u.id " +
                    "JOIN " + SCHEMA_NAME + "._sc_role_asset_position rap on rap.role_id = pr.role_id " +
                    "JOIN " + SCHEMA_NAME + "._sc_role_work_type rwt ON pr.role_id=rwt.role_id " +
                    "JOIN " + SCHEMA_NAME + "._sc_role_asset_type rat ON rat.role_id = pr.role_id " +
                    "JOIN (SELECT wd.work_type_id,w.position_code,wd.relation_id,wd.relation_type,a.category_id " +
                    "FROM " + SCHEMA_NAME + "._sc_works_detail wd JOIN " + SCHEMA_NAME + "._sc_works w ON w.work_code = wd.work_code " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_asset a on a.id = wd.relation_id  WHERE wd.sub_work_code = '" + SQL_WHERE_NAME + "' " +
                    ") aa ON aa.position_code = rap.asset_position_code AND rwt.work_type_id = aa.work_type_id " +
                    "AND case when aa.relation_type = '2' then aa.category_id = rat.category_id else 1=1 end " +
                    "WHERE u.status > " + StatusConstant.STATUS_DELETEED);

            //所有货币种类
            put("currency", "SELECT t.id as value, t.currency_code as text FROM "
                    + SCHEMA_NAME + "._sc_currency t ");

            // 根据设备id获取设备位置
            put("asset_position_with_asset", "SELECT ap.position_code as value, ap.position_name as text FROM "
                    + SCHEMA_NAME + "._sc_asset a JOIN " + SCHEMA_NAME + "._sc_asset_position ap ON a.position_code = ap.position_code WHERE a.id = '" + SQL_WHERE_NAME + "'");

            // 根据设备id获取设备运行状态
            put("asset_running_status_with_asset", "SELECT ars.id as value, ars.running_status as text FROM "
                    + SCHEMA_NAME + "._sc_asset a JOIN " + SCHEMA_NAME + "._sc_asset_running_status ars ON a.running_status_id = ars.id WHERE a.running_status_id = '" + SQL_WHERE_NAME + "'");

            //根据维保计划参与团队的-执行人员-部门下特定角色的人
            put("group_role_user_by_plan_code", "SELECT distinct u.id as value,u.user_name as text " +
                    "FROM " + SCHEMA_NAME + "._sc_group g " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_position p on g.id = p.group_id " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_position_role pr ON p.id = pr.position_id " +
                    "LEFT JOIN (SELECT setting_value from " + SCHEMA_NAME + "._sc_system_config yc WHERE yc.config_name = 'executor_role') s ON pr.role_id = any (STRING_TO_ARRAY(s.setting_value,','))" +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_user_position up on p.id = up.position_id " +
                    "JOIN " + SCHEMA_NAME + "._sc_user u ON up.user_id = u.id " +
                    "LEFT JOIN " + SCHEMA_NAME + "._sc_plan_work_assist_team pwat ON pwat.group_id = g.id  AND pwat.duty_type = '1' " +
                    "WHERE pwat.plan_code = '" + SQL_WHERE_NAME + "' AND u.status > " + StatusConstant.STATUS_DELETEED);

            //查询设备所属客户
            put("asset_customer", "select a.id as value,a.short_title as text" +
                    " from " + SCHEMA_NAME + "._sc_facilities a ," + SCHEMA_NAME + "._sc_asset_organization" +
                    " b where a.id = b.org_id  " + SQL_WHERE_NAME +
                    " and a.status>" + StatusConstant.STATUS_DELETEED
            );


//            //根据企业id查询多语言权限
//            put("company_all", "SELECT id as value, company_name as text FROM public._sc_company order by value ");

        }
    };

}
