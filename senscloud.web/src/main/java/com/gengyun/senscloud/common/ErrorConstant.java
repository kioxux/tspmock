package com.gengyun.senscloud.common;

/**
 * 错误信息常量
 */
public interface ErrorConstant {
    String EC_DATA_TYPE = "error_constant"; // 字典错误码标识
    String EC_START = "EC_LG_"; // 错误信息开头
    String EC_REMARK_KEY = "remark";
    String EC_EXP_KEY = "exception";
    String EC_ACCOUNT_KEY = "account";
    String EC_USER_ID_KEY = "userId";
    String EC_SCHEMA_KEY = "schema";
    /**
     * 设备详情：自定义字段解析错误
     */
    String CODE_ASSET_101 = "assetErr101";
    /**
     * 设备详情：数值类字段格式错误
     */
    String CODE_ASSET_102 = "assetErr102";
    /**
     * 设备详情：自定义字段格式为对象，应该为集合
     */
    String CODE_ASSET_103 = "assetErr103";
    /**
     * 设备详情：自定义字段解析处理错误
     */
    String CODE_ASSET_104 = "assetErr104";
    /**
     * 设备详情：自定义字段转换错误
     */
    String CODE_ASSET_105 = "assetErr105";
    /**
     * 设备编辑：基础字段组信息解析错误
     */
    String CODE_ASSET_201 = "assetErr201";
    /**
     * 设备编辑：基础字段值核对错误
     */
    String CODE_ASSET_202 = "assetErr202";
    /**
     * 设备编辑：自定义字段值核对错误
     */
    String CODE_ASSET_203 = "assetErr203";
    /**
     * 设备编辑：自定义字段解析错误
     */
    String CODE_ASSET_204 = "assetErr204";
    /**
     * 设备编辑：自定义字段格式为对象，应该为集合
     */
    String CODE_ASSET_205 = "assetErr205";
    /**
     * 设备编辑：自定义字段解析处理错误
     */
    String CODE_ASSET_206 = "assetErr206";
    /**
     * 设备新增：运行状态格式错误
     */
    String CODE_ASSET_301 = "assetErr301";
    /**
     * 设备新增：型号自定义字段解析错误
     */
    String CODE_ASSET_302 = "assetErr302";
    /**
     * 设备新增：型号自定义字段格式为对象，应该为集合
     */
    String CODE_ASSET_303 = "assetErr303";
    /**
     * 设备新增：型号自定义字段解析处理错误
     */
    String CODE_ASSET_304 = "assetErr304";
    /**
     * 日常报告：excel字段数据组装错误
     */
    String CODE_SUEZ_REPORT_101 = "daReportErr101";
    /**
     * 日常报告：excel任务项图片解析错误
     */
    String CODE_SUEZ_REPORT_102 = "daReportErr102";
    /**
     * 日常报告：excel任务项图片写入错误
     */
    String CODE_SUEZ_REPORT_103 = "daReportErr103";
    /**
     * 外部接口10：处理错误
     */
    String CODE_OUT_SERVER_1001 = "outServerErr1001";
    /**
     * 外部接口10：设备主键为空或有误
     */
    String CODE_OUT_SERVER_1002 = "outServerErr1002";
    /**
     * 数字类型格式错误
     */
    String EC_DATA_1 = ErrorConstant.EC_START + "D001";
    /**
     * 下拉框类型为空
     */
    String EC_SELECT_1 = ErrorConstant.EC_START + "1001";
    /**
     * 下拉框类型为空，明细未获取
     */
    String EC_SELECT_2 = ErrorConstant.EC_START + "1002";
    /**
     * 下拉框选中项值为空，明细未获取
     */
    String EC_SELECT_3 = ErrorConstant.EC_START + "1003";
    /**
     * 设备：编码为空，二维码未生成
     */
    String EC_ASSET_1 = ErrorConstant.EC_START + "2001";
    /**
     * 设备编辑：类型为空，无法编辑
     */
    String EC_ASSET_2 = ErrorConstant.EC_START + "2002";
    /**
     * 设备位置：编码为空，二维码未生成
     */
    String EC_ASSET_3 = ErrorConstant.EC_START + "2003";
    /**
     * 设备详情：设备类型数字类型格式错误
     */
    String EC_ASSET_4 = ErrorConstant.EC_START + "2004";
    /**
     * 设备详情：设备型号数字类型格式错误
     */
    String EC_ASSET_5 = ErrorConstant.EC_START + "2005";
    /**
     * 维护配置：类型为空或无效，数据未修改
     */
    String EC_BUSINESS_CONFIG_1 = ErrorConstant.EC_START + "3001";
    /**
     * 维护配置：类型为空或无效，数据未新增
     */
    String EC_BUSINESS_CONFIG_2 = ErrorConstant.EC_START + "3002";
    /**
     * 维护配置：类型为空或无效，数据未查询
     */
    String EC_BUSINESS_CONFIG_3 = ErrorConstant.EC_START + "3003";
    /**
     * 维护配置：类型为空或无效，数据未删除
     */
    String EC_BUSINESS_CONFIG_4 = ErrorConstant.EC_START + "3004";
    /**
     * 权限验证：主键无效
     */
    String EC_PRIMARY_INVALID_1 = ErrorConstant.EC_START + "4001";
    /**
     * 权限验证：配置错误
     */
    String EC_PRIMARY_INVALID_2 = ErrorConstant.EC_START + "4002";
    /**
     * 权限验证：logo无权限
     */
    String EC_PRIMARY_INVALID_3 = ErrorConstant.EC_START + "4003";
    /**
     * 短信发送：短信正文模板ID未配置或无效
     */
    String EC_PRIMARY_SMS_1 = ErrorConstant.EC_START + "5001";
    /**
     * 短信发送：短信内容为空或无效
     */
    String EC_PRIMARY_SMS_2 = ErrorConstant.EC_START + "5002";
    /**
     * 短信发送：短信AppID未配置或无效
     */
    String EC_PRIMARY_SMS_3 = ErrorConstant.EC_START + "5003";
    /**
     * 短信发送：短信AppKey未配置
     */
    String EC_PRIMARY_SMS_4 = ErrorConstant.EC_START + "5004";
    /**
     * 短信发送：短信端口未配置
     */
    String EC_PRIMARY_SMS_5 = ErrorConstant.EC_START + "5005";
    /**
     * 短信发送：短信类型未配置
     */
    String EC_PRIMARY_SMS_6 = ErrorConstant.EC_START + "5006";
    /**
     * 短信发送：短信接收人手机号为空或无效
     */
    String EC_PRIMARY_SMS_7 = ErrorConstant.EC_START + "5007";
    /**
     * 短信发送：短信配置信息不存在
     */
    String EC_PRIMARY_SMS_8 = ErrorConstant.EC_START + "5008";
    /**
     * 短信发送：短信模板未配置
     */
    String EC_PRIMARY_SMS_9 = ErrorConstant.EC_START + "5009";
    /**
     * 短信发送：短信模板内容未配置
     */
    String EC_PRIMARY_SMS_10 = ErrorConstant.EC_START + "5010";
    /**
     * 短信发送：短信模板编号未配置或无效
     */
    String EC_PRIMARY_SMS_11 = ErrorConstant.EC_START + "5011";
    /**
     * 短信发送：短信接口返回结果为空
     */
    String EC_PRIMARY_SMS_12 = ErrorConstant.EC_START + "5012";
    /**
     * 短信发送：短信接口返回结果解析无效或为空
     */
    String EC_PRIMARY_SMS_13 = ErrorConstant.EC_START + "5013";
    /**
     * 短信发送：短信模板内容分隔符未配置
     */
    String EC_PRIMARY_SMS_14 = ErrorConstant.EC_START + "5014";
    /**
     * 短信发送：接口无法解析
     */
    String EC_PRIMARY_SMS_15 = ErrorConstant.EC_START + "5015";
    /**
     * 短信发送：接口异常
     */
    String EC_PRIMARY_SMS_16 = ErrorConstant.EC_START + "5016";
    /**
     * 短信发送：结果存储异常
     */
    String EC_PRIMARY_SMS_17 = ErrorConstant.EC_START + "5017";
    /**
     * 短信发送：结果未存储
     */
    String EC_PRIMARY_SMS_18 = ErrorConstant.EC_START + "5018";
    /**
     * 短信发送：短信发送用户账户为空或无效
     */
    String EC_PRIMARY_SMS_19 = ErrorConstant.EC_START + "5019";
    /**
     * 短信发送：短信host为空或无效
     */
    String EC_PRIMARY_SMS_20 = ErrorConstant.EC_START + "5020";
    /**
     * 短信发送：短信接口号extno为空或无效
     */
    String EC_PRIMARY_SMS_21 = ErrorConstant.EC_START + "5021";
    /**
     * 短信发送：短信请求类型为空或无效
     */
    String EC_PRIMARY_SMS_22 = ErrorConstant.EC_START + "5022";

    /**
     * 编号生成：编号类型为空
     */
    String EC_SN_1 = ErrorConstant.EC_START + "6001";
    /**
     * 编号生成：编号重置方式未配置
     */
    String EC_SN_2 = ErrorConstant.EC_START + "6002";
    /**
     * 编号生成：编号最大值未配置
     */
    String EC_SN_3 = ErrorConstant.EC_START + "6003";
    /**
     * 编号生成：数据库编号更新失败
     */
    String EC_SN_4 = ErrorConstant.EC_START + "6004";
    /**
     * 编号生成：编号明细信息未配置
     */
    String EC_SN_5 = ErrorConstant.EC_START + "6005";
    /**
     * 编号生成：编号信息未配置
     */
    String EC_SN_6 = ErrorConstant.EC_START + "6006";
    /**
     * 编号生成：编号前缀未配置
     */
    String EC_SN_7 = ErrorConstant.EC_START + "6007";
    /**
     * 编号生成：编号位数未配置
     */
    String EC_SN_8 = ErrorConstant.EC_START + "6008";
    /**
     * 备件：物料编码为空，二维码未生成
     */
    String EC_BOM_1 = ErrorConstant.EC_START + "7001";
    /**
     * 备件：备件编码为空，二维码未生成
     */
    String EC_BOM_2 = ErrorConstant.EC_START + "7002";
    /**
     * 备件编辑：类型为空，无法编辑
     */
    String EC_BOM_3 = ErrorConstant.EC_START + "7003";
    /**
     * 工作流部署列表：计数异常
     */
    String EC_WORKFLOW_1 = ErrorConstant.EC_START + "8001";
    /**
     * 工作流部署列表：分页处理异常
     */
    String EC_WORKFLOW_2 = ErrorConstant.EC_START + "8002";
    /**
     * 工作流部署列表：分页查询异常
     */
    String EC_WORKFLOW_3 = ErrorConstant.EC_START + "8003";
    /**
     * 工作流部署列表：数据查询异常
     */
    String EC_WORKFLOW_4 = ErrorConstant.EC_START + "8004";
    /**
     * 工作流部署列表：查询异常
     */
    String EC_WORKFLOW_5 = ErrorConstant.EC_START + "8005";
    /**
     * 工作流部署列表：数据库类型异常
     */
    String EC_WORKFLOW_6 = ErrorConstant.EC_START + "8006";
    /**
     * 工作流部署列表：数据处理异常
     */
    String EC_WORKFLOW_7 = ErrorConstant.EC_START + "8007";
    /**
     * 工作流部署删除：主键为空
     */
    String EC_WORKFLOW_8 = ErrorConstant.EC_START + "8008";
    /**
     * 工作流部署删除：数据库类型异常
     */
    String EC_WORKFLOW_9 = ErrorConstant.EC_START + "8009";
    /**
     * 工作流部署删除：异常
     */
    String EC_WORKFLOW_10 = ErrorConstant.EC_START + "8010";
    /**
     * 工作流部署：数据库类型异常
     */
    String EC_WORKFLOW_11 = ErrorConstant.EC_START + "8011";
    /**
     * 工作流部署：文件处理异常
     */
    String EC_WORKFLOW_12 = ErrorConstant.EC_START + "8012";
    /**
     * 工作流部署：异常
     */
    String EC_WORKFLOW_13 = ErrorConstant.EC_START + "8013";
    /**
     * 工作流部署：流程配置流程信息更新异常
     */
    String EC_WORKFLOW_14 = ErrorConstant.EC_START + "8014";
    /**
     * 工作流定义列表：计数异常
     */
    String EC_WORKFLOW_101 = ErrorConstant.EC_START + "8101";
    /**
     * 工作流定义列表：分页处理异常
     */
    String EC_WORKFLOW_102 = ErrorConstant.EC_START + "8102";
    /**
     * 工作流定义列表：分页查询异常
     */
    String EC_WORKFLOW_103 = ErrorConstant.EC_START + "8103";
    /**
     * 工作流定义列表：数据查询异常
     */
    String EC_WORKFLOW_104 = ErrorConstant.EC_START + "8104";
    /**
     * 工作流定义列表：查询异常
     */
    String EC_WORKFLOW_105 = ErrorConstant.EC_START + "8105";
    /**
     * 工作流定义流程图获取：主键为空
     */
    String EC_WORKFLOW_106 = ErrorConstant.EC_START + "8106";
    /**
     * 工作流定义流程图获取：处理异常
     */
    String EC_WORKFLOW_107 = ErrorConstant.EC_START + "8107";
    /**
     * 工作流定义流程图获取：异常
     */
    String EC_WORKFLOW_108 = ErrorConstant.EC_START + "8108";
    /**
     * 工作流定义流程图获取：数据库类型异常
     */
    String EC_WORKFLOW_109 = ErrorConstant.EC_START + "8109";
    /**
     * 工作流定义流程图获取：图片回显异常
     */
    String EC_WORKFLOW_110 = ErrorConstant.EC_START + "8110";
    /**
     * 工作流实例流程图获取：主键为空
     */
    String EC_WORKFLOW_201 = ErrorConstant.EC_START + "8201";
    /**
     * 工作流实例流程图获取：处理异常
     */
    String EC_WORKFLOW_202 = ErrorConstant.EC_START + "8202";
    /**
     * 工作流实例流程图获取：异常
     */
    String EC_WORKFLOW_203 = ErrorConstant.EC_START + "8203";
    /**
     * 工作流实例流程图获取：数据库类型异常
     */
    String EC_WORKFLOW_204 = ErrorConstant.EC_START + "8204";
    /**
     * 工作流实例流程图获取：图片回显异常
     */
    String EC_WORKFLOW_205 = ErrorConstant.EC_START + "8205";
    /**
     * 工作流实例删除：主键为空
     */
    String EC_WORKFLOW_206 = ErrorConstant.EC_START + "8206";
    /**
     * 工作流实例删除：数据库类型异常
     */
    String EC_WORKFLOW_207 = ErrorConstant.EC_START + "8207";
    /**
     * 工作流实例删除：异常
     */
    String EC_WORKFLOW_208 = ErrorConstant.EC_START + "8208";
    /**
     * 按名称删除工作流实例：异常
     */
    String EC_WORKFLOW_209 = ErrorConstant.EC_START + "8209";
    /**
     * 工作流流程、节点信息列表获取：数据库类型异常
     */
    String EC_WORKFLOW_301 = ErrorConstant.EC_START + "8301";
    /**
     * 工作流流程、节点信息列表获取：流程定义信息获取初始化异常
     */
    String EC_WORKFLOW_302 = ErrorConstant.EC_START + "8302";
    /**
     * 工作流流程、节点信息列表获取：流程定义信息获取参数设置异常
     */
    String EC_WORKFLOW_303 = ErrorConstant.EC_START + "8303";
    /**
     * 工作流流程、节点信息列表获取：流程定义信息获取异常
     */
    String EC_WORKFLOW_304 = ErrorConstant.EC_START + "8304";
    /**
     * 工作流流程、节点信息列表获取：流程定义信息解析异常
     */
    String EC_WORKFLOW_305 = ErrorConstant.EC_START + "8305";
    /**
     * 工作流流程、节点信息列表获取：流程定义明细信息获取异常
     */
    String EC_WORKFLOW_306 = ErrorConstant.EC_START + "8306";
    /**
     * 工作流流程、节点信息列表获取：流程定义明细信息解析异常
     */
    String EC_WORKFLOW_307 = ErrorConstant.EC_START + "8307";
    /**
     * 工作流流程、节点信息列表获取：流程模型信息获取异常
     */
    String EC_WORKFLOW_308 = ErrorConstant.EC_START + "8308";
    /**
     * 工作流流程、节点信息列表获取：流程模型信息解析异常
     */
    String EC_WORKFLOW_309 = ErrorConstant.EC_START + "8309";
    /**
     * 工作流流程、节点信息列表获取：流程信息解析异常
     */
    String EC_WORKFLOW_310 = ErrorConstant.EC_START + "8310";
    /**
     * 工作流流程、节点信息列表获取：节点信息解析异常
     */
    String EC_WORKFLOW_311 = ErrorConstant.EC_START + "8311";
    /**
     * 工作流流程、节点信息列表获取：异常
     */
    String EC_WORKFLOW_312 = ErrorConstant.EC_START + "8312";
    /**
     * 工作流节点流程图获取：流程编号为空
     */
    String EC_WORKFLOW_401 = ErrorConstant.EC_START + "8401";
    /**
     * 工作流节点流程图获取：节点主键为空
     */
    String EC_WORKFLOW_402 = ErrorConstant.EC_START + "8402";
    /**
     * 工作流节点流程图获取：处理异常
     */
    String EC_WORKFLOW_403 = ErrorConstant.EC_START + "8403";
    /**
     * 工作流节点流程图获取：数据库类型异常
     */
    String EC_WORKFLOW_404 = ErrorConstant.EC_START + "8404";
    /**
     * 工作流节点流程图获取：图片回显异常
     */
    String EC_WORKFLOW_405 = ErrorConstant.EC_START + "8405";
    /**
     * 工作流节点流程图获取：异常
     */
    String EC_WORKFLOW_406 = ErrorConstant.EC_START + "8406";
    /**
     * 工单调用工作流：数据库类型异常
     */
    String EC_WORKFLOW_501 = ErrorConstant.EC_START + "8501";
    /**
     * 工单调用工作流：业务数据主键为空
     */
    String EC_WORKFLOW_502 = ErrorConstant.EC_START + "8502";
    /**
     * 工单调用工作流：实例查询异常
     */
    String EC_WORKFLOW_503 = ErrorConstant.EC_START + "8503";
    /**
     * 工单调用工作流：数据回显异常
     */
    String EC_WORKFLOW_504 = ErrorConstant.EC_START + "8504";
    /**
     * 工单调用工作流：异常
     */
    String EC_WORKFLOW_505 = ErrorConstant.EC_START + "8505";
    /**
     * 工单调用工作流：下一个节点状态为空或者类型错误
     */
    String EC_WORKFLOW_506 = ErrorConstant.EC_START + "8506";
    /**
     * 根据流程号获取节点下拉框：流程编号为空
     */
    String EC_WORKFLOW_601 = ErrorConstant.EC_START + "8601";
    /**
     * 根据流程号获取节点下拉框：数据库类型异常
     */
    String EC_WORKFLOW_602 = ErrorConstant.EC_START + "8602";
    /**
     * 根据流程号获取节点下拉框：流程定义明细信息获取异常
     */
    String EC_WORKFLOW_603 = ErrorConstant.EC_START + "8603";
    /**
     * 根据流程号获取节点下拉框：流程定义明细信息解析异常
     */
    String EC_WORKFLOW_604 = ErrorConstant.EC_START + "8604";
    /**
     * 根据流程号获取节点下拉框：流程模型信息获取异常
     */
    String EC_WORKFLOW_605 = ErrorConstant.EC_START + "8605";
    /**
     * 根据流程号获取节点下拉框：流程模型信息解析异常
     */
    String EC_WORKFLOW_606 = ErrorConstant.EC_START + "8606";
    /**
     * 根据流程号获取节点下拉框：节点信息解析异常
     */
    String EC_WORKFLOW_607 = ErrorConstant.EC_START + "8607";
    /**
     * 根据流程号获取节点下拉框：异常
     */
    String EC_WORKFLOW_608 = ErrorConstant.EC_START + "8608";
    /**
     * 参数错误：页数
     */
    String EC_PAGE_PARAM_1 = ErrorConstant.EC_START + "9001";
    /**
     * 参数错误：页码
     */
    String EC_PAGE_PARAM_2 = ErrorConstant.EC_START + "9002";
    /**
     * HTTP错误：get处理异常
     */
    String EC_HTTP_101 = ErrorConstant.EC_START + "10101";
    /**
     * HTTP错误：get异常
     */
    String EC_HTTP_102 = ErrorConstant.EC_START + "10102";
    /**
     * HTTP错误：post处理异常
     */
    String EC_HTTP_201 = ErrorConstant.EC_START + "10201";
    /**
     * HTTP错误：post异常
     */
    String EC_HTTP_202 = ErrorConstant.EC_START + "10202";
    /**
     * 微信公众号：配置信息不存在
     */
    String EC_WX_GZH_003 = ErrorConstant.EC_START + "11003";
    /**
     * 微信公众号：secret未配置
     */
    String EC_WX_GZH_004 = ErrorConstant.EC_START + "11004";
    /**
     * 微信公众号：类型未配置
     */
    String EC_WX_GZH_005 = ErrorConstant.EC_START + "11005";
    /**
     * 微信公众号：公共配置信息不存在
     */
    String EC_WX_GZH_006 = ErrorConstant.EC_START + "11006";
    /**
     * 微信公众号：appToken获取为空
     */
    String EC_WX_GZH_009 = ErrorConstant.EC_START + "11009";
    /**
     * 微信公众号：配置信息不存在-刷新token
     */
    String EC_WX_GZH_011 = ErrorConstant.EC_START + "11011";
    /**
     * 微信公众号：类型未配置-刷新token
     */
    String EC_WX_GZH_012 = ErrorConstant.EC_START + "11012";
    /**
     * 微信公众号：secret未配置-刷新token
     */
    String EC_WX_GZH_013 = ErrorConstant.EC_START + "11013";
    /**
     * 微信公众号：公共配置信息不存在-刷新token
     */
    String EC_WX_GZH_014 = ErrorConstant.EC_START + "11014";
    /**
     * 微信公众号：AppID未配置-刷新token
     */
    String EC_WX_GZH_015 = ErrorConstant.EC_START + "11015";
    /**
     * 微信公众号：外部接口无返回值-刷新token
     */
    String EC_WX_GZH_016 = ErrorConstant.EC_START + "11016";
    /**
     * 微信公众号：外部接口返回错误-刷新token
     */
    String EC_WX_GZH_017 = ErrorConstant.EC_START + "11017";
    /**
     * 微信公众号：数据库信息未更新-刷新token
     */
    String EC_WX_GZH_018 = ErrorConstant.EC_START + "11018";
    /**
     * 微信公众号：数据库公共信息未更新-刷新token
     */
    String EC_WX_GZH_019 = ErrorConstant.EC_START + "11019";
    /**
     * 微信公众号：数据库更新失败-刷新token
     */
    String EC_WX_GZH_020 = ErrorConstant.EC_START + "11020";
    /**
     * 微信公众号-消息发送：配置信息不存在
     */
    String EC_WX_GZH_021 = ErrorConstant.EC_START + "11021";
    /**
     * 微信公众号-消息发送：openId未配置
     */
    String EC_WX_GZH_007 = ErrorConstant.EC_START + "11007";
    /**
     * 微信公众号-消息发送：消息模板未配置
     */
    String EC_WX_GZH_022 = ErrorConstant.EC_START + "11022";
    /**
     * 微信公众号-消息发送：消息内容解析为空
     */
    String EC_WX_GZH_023 = ErrorConstant.EC_START + "11023";
    /**
     * 微信公众号-消息发送：消息模板主键未配置
     */
    String EC_WX_GZH_008 = ErrorConstant.EC_START + "11008";
    /**
     * 微信公众号-消息发送：消无返回值
     */
    String EC_WX_GZH_001 = ErrorConstant.EC_START + "11001";
    /**
     * 微信公众号-消息发送：返回错误
     */
    String EC_WX_GZH_002 = ErrorConstant.EC_START + "11002";
    /**
     * 微信公众号-消息发送：异常
     */
    String EC_WX_GZH_010 = ErrorConstant.EC_START + "11010";
    /**
     * 微信公众号-消息发送：结果未存储
     */
    String EC_WX_GZH_024 = ErrorConstant.EC_START + "11024";
    /**
     * 微信公众号-消息发送：结果存储异常
     */
    String EC_WX_GZH_025 = ErrorConstant.EC_START + "11025";
    /**
     * JOB任务：定时任务id为空
     */
    String EC_JOB_001 = ErrorConstant.EC_START + "12001";
    /**
     * JOB任务：定时任务schema_name为空
     */
    String EC_JOB_002 = ErrorConstant.EC_START + "12002";
    /**
     * JOB任务：定时任务业务数据处理异常
     */
    String EC_JOB_003 = ErrorConstant.EC_START + "12003";
    /**
     * 消息通知：更新读取状态时缺少主键
     */
    String EC_MSG_001 = ErrorConstant.EC_START + "13001";
    /**
     * 消息通知：权限不足
     */
    String EC_MSG_002 = ErrorConstant.EC_START + "13002";
    /**
     * 统计分析：表格sql错误
     */
    String EC_STATISTIC_001 = ErrorConstant.EC_START + "14001";
    /**
     * 统计分析：图表sql错误
     */
    String EC_STATISTIC_002 = ErrorConstant.EC_START + "14002";
    /**
     * 系统消息：发送失败
     */
    String EC_SYS_MSG_001 = ErrorConstant.EC_START + "15001";
    /**
     * 邮件消息：发送失败
     */
    String EC_EMAIL_MSG_001 = ErrorConstant.EC_START + "16001";
    /**
     * 邮件消息：主题为空
     */
    String EC_EMAIL_MSG_002 = ErrorConstant.EC_START + "16002";
    /**
     * 邮件消息：内容为空
     */
    String EC_EMAIL_MSG_003 = ErrorConstant.EC_START + "16003";
    /**
     * 邮件消息：收件人为空
     */
    String EC_EMAIL_MSG_004 = ErrorConstant.EC_START + "16004";
    /**
     * 工单回调错误：业务类型为空或格式不正确
     */
    String EC_WORK_001 = ErrorConstant.EC_START + "17001";
    /**
     * 工单回调错误：工单类型为空或格式不正确
     */
    String EC_WORK_002 = ErrorConstant.EC_START + "17002";
    /**
     * 工单回调错误：工作流编号为空
     */
    String EC_WORK_003 = ErrorConstant.EC_START + "17003";
    /**
     * 工单回调错误：操作按钮未配置或类型错误
     */
    String EC_WORK_004 = ErrorConstant.EC_START + "17004";
    /**
     * 工单派案逻辑错误：派案类型格式错误
     */
    String EC_WORK_005 = ErrorConstant.EC_START + "17005";
    /**
     * 工单回调错误：工单当前状态类型错误
     */
    String EC_WORK_006 = ErrorConstant.EC_START + "17006";
    /**
     * 工单回调错误：工单对象类型格式错误
     */
    String EC_WORK_007 = ErrorConstant.EC_START + "17007";
    /**
     * 工单回调错误：工单是否主子工单类型错误
     */
    String EC_WORK_008 = ErrorConstant.EC_START + "17008";
    /**
     * 工单回调错误：工单库房编码类型错误
     */
    String EC_WORK_009 = ErrorConstant.EC_START + "17009";
    /**
     * 工单回显错误：工单业务类型未配置或格式错误
     */
    String EC_WORK_010 = ErrorConstant.EC_START + "17010";
    /**
     * 工单/待办分配：工单业务类型未配置或格式错误
     */
    String EC_WORK_011 = ErrorConstant.EC_START + "17011";
    /**
     * 工单/待办分配：流程编号为空
     */
    String EC_WORK_012 = ErrorConstant.EC_START + "17012";
    /**
     * 工单/创建维修工单：流程页面类型为空
     */
    String EC_WORK_013 = ErrorConstant.EC_START + "17013";
    /**
     * 工单/创建维修工单：流程页面节点编号为空
     */
    String EC_WORK_014 = ErrorConstant.EC_START + "17014";
    /**
     * 工单提交错误：工作流编号为空
     */
    String EC_WORK_015 = ErrorConstant.EC_START + "17015";
    /**
     * 工单提交错误：业务类型为空或格式不正确
     */
    String EC_WORK_016 = ErrorConstant.EC_START + "17016";
    /**
     * 工单提交错误：工单类型为空
     */
    String EC_WORK_017 = ErrorConstant.EC_START + "17017";
    /**
     * 任务模板任务项关联：任务项顺序类型错误
     */
    String EC_TASK_001 = ErrorConstant.EC_START + "18001";
    /**
     * 设备位置：顶层平面图数据类型错误
     */
    String EC_ASSET_POSITION_001 = ErrorConstant.EC_START + "19001";
    /**
     * 文件处理：excel图片写入处理失败
     */
    String EC_FILE_001 = ErrorConstant.EC_START + "20001";
    /**
     * 文件处理：excel单张图片写入失败
     */
    String EC_FILE_002 = ErrorConstant.EC_START + "20002";
    /**
     * 文件处理：excel图片写入失败
     */
    String EC_FILE_003 = ErrorConstant.EC_START + "20003";
    /**
     * 文件处理：excel图片写入坐标处理失败
     */
    String EC_FILE_004 = ErrorConstant.EC_START + "20004";
    /**
     * 文件处理：文件回显时主键格式错误
     */
    String EC_FILE_005 = ErrorConstant.EC_START + "20005";
    /**
     * 列表错误：工单列表状态格式错误或为空
     */
    String EC_LIST_001 = ErrorConstant.EC_START + "21001";
    /**
     * 列表错误：设备流程列表状态格式错误或为空
     */
    String EC_LIST_002 = ErrorConstant.EC_START + "21002";
    /**
     * 列表错误：备件流程列表状态格式错误或为空
     */
    String EC_LIST_003 = ErrorConstant.EC_START + "21003";
    /**
     * 国际化错误：用途键值不足，请联系管理员补充
     */
    String EC_LANG_001 = ErrorConstant.EC_START + "22001";
    /**
     * 国际化错误：基础语种新增未成功
     */
    String EC_LANG_002 = ErrorConstant.EC_START + "22002";
    /**
     * 国际化错误：特殊语种新增未成功
     */
    String EC_LANG_003 = ErrorConstant.EC_START + "22003";
    /**
     * 国际化错误：企业特殊语种新增未成功
     */
    String EC_LANG_004 = ErrorConstant.EC_START + "22004";
    /**
     * 国际化错误：基础语种更新未成功
     */
    String EC_LANG_005 = ErrorConstant.EC_START + "22005";
    /**
     * 国际化错误：特殊语种更新未成功（初次设置）
     */
    String EC_LANG_006 = ErrorConstant.EC_START + "22006";
    /**
     * 国际化错误：特殊语种更新未成功
     */
    String EC_LANG_007 = ErrorConstant.EC_START + "22007";
    /**
     * 国际化错误：企业特殊语种更新未成功（初次设置）
     */
    String EC_LANG_008 = ErrorConstant.EC_START + "22008";
    /**
     * 国际化错误：企业特殊语种更新未成功
     */
    String EC_LANG_009 = ErrorConstant.EC_START + "22009";
}
