package com.gengyun.senscloud.common;

public enum CheckMeterTypeEnum {

    /**
     * 手工抄表
     */
    MANUAL("1"),
    /**
     * 自动抄表
     */
    AUTO("2"),
    /**
     * 估抄
     */
    CACULATE("3"),
    /**
     * 初始化
     */
    INIT("4");

    private String key;

    CheckMeterTypeEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


}
