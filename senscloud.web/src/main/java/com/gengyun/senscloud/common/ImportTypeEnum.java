package com.gengyun.senscloud.common;

import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.listener.excel.read.impl.*;


/**
 * 数据导入类型枚举类
 */
public enum ImportTypeEnum {
    /**
     * 设备导入
     */
    ASSET(1, AssetExcelListener.class, SensConstant.BUSINESS_NO_2007, LangConstant.TITLE_EJK, "assetImportTemplate.xlsx"),
    /**
     * 备件导入
     */
    BOM(2, BomExcelListener.class, SensConstant.BUSINESS_NO_3009, LangConstant.TITLE_EJL, "bomImportTemplate.xlsx"),
//    /**
//     * 设备-备件，关联关系导入
//     */
//    ASSET_BOM(3, AssetBomExcelListener.class, SensConstant.BUSINESS_NO_2008, LangConstant.TITLE_EJM, ""),
//    /**
//     * 生产作业导入
//     */
//    PRODUCT_TASK(4, ProductTaskExcelListenerFor40.class, SensConstant.BUSINESS_NO_10004, ""),
    /**
     * 维保行事历导入
     */
    PLAN_WORK_CALENDAR(5, PlanWorkCalendarExcelListener.class, SensConstant.BUSINESS_NO_10005, LangConstant.TITLE_EJN, "planWorkCalendarImportTemplate.xlsx"),
    /**
     * 用户
     */
    USER(6, UserExcelListener.class, SensConstant.BUSINESS_NO_7012, LangConstant.TITLE_AAR_J, "userImportTemplate.xlsx"),

    /**
     * 任务项
     */
    Task(7, TaskAndTemplateExcelListener.class, SensConstant.BUSINESS_NO_7014, LangConstant.TITLE_AAQ_Q, "taskImportTemplate.xlsx"),

    /**
     * 工单模板
     */
    WORK_FLOW(8, WorkFlowExcelListener.class, SensConstant.BUSINESS_NO_8006, LangConstant.TITLE_AC_V, "workFlowTemplate.xlsx"),

    /**
     * 客户
     */
    CUSTOMER(9, CustomerExcelListener.class, SensConstant.BUSINESS_NO_6002, LangConstant.TITLE_AJ_G, "customerImportTemplate.xlsx"),

    /**
     * 缴费信息
     */
    POLLUTE_FEE(10, PolluteFeeExcelListener.class, SensConstant.BUSINESS_NO_6005, LangConstant.TITLE_ADFG, "polluteFeeImportTemplate.xlsx");

    private int key;
    private Class listener;
    private String businessNo;
    private String name;
    private String templateFileName;//模板文件名

    ImportTypeEnum(int key, Class<? extends BaseExcelListener> listener, String businessNo, String name, String templateFileName) {
        this.key = key;
        this.listener = listener;
        this.businessNo = businessNo;
        this.name = name;
        this.templateFileName = templateFileName;
    }

    public static Class getValue(int key) {
        ImportTypeEnum[] importTypeEnums = values();
        for (ImportTypeEnum importTypeEnum : importTypeEnums) {
            if (importTypeEnum.getKey() == key) {
                return importTypeEnum.getListener();
            }
        }
        return null;
    }

    public static String getBusinessNo(int key) {
        ImportTypeEnum[] importTypeEnums = values();
        for (ImportTypeEnum importTypeEnum : importTypeEnums) {
            if (importTypeEnum.getKey() == key) {
                return importTypeEnum.getBusinessNo();
            }
        }
        return "";
    }

    public static String getTemplateFileName(int key) {
        ImportTypeEnum[] importTypeEnums = values();
        for (ImportTypeEnum importTypeEnum : importTypeEnums) {
            if (importTypeEnum.getKey() == key) {
                return importTypeEnum.getTemplateFileName();
            }
        }
        return "";
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Class getListener() {
        return listener;
    }

    public void setListener(Class listener) {
        this.listener = listener;
    }

    public String getBusinessNo() {
        return businessNo;
    }

    public void setBusinessNo(String businessNo) {
        this.businessNo = businessNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplateFileName() {
        return templateFileName;
    }

    public void setTemplateFileName(String templateFileName) {
        this.templateFileName = templateFileName;
    }
}
