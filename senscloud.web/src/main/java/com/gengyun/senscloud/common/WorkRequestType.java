package com.gengyun.senscloud.common;

/**
 * 模板公共字段-请求类型
 */
public enum WorkRequestType {
    CONFIRM("confirm"),
    WORK("work");

    private String type;
    private WorkRequestType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
}
