package com.gengyun.senscloud.common;

/**
 * 国际化常量（java用）
 */
public interface LangConstant {
    /**
     * 展开
     */
    String BTN_A = "btn_a";
    /**
     * 选择流程
     */
    String BTN_AB = "btn_ab";
    /**
     * 领用
     */
    String BTN_AC = "btn_ac";
    /**
     * 添加
     */
    String BTN_ADD_A = "btn_add_a";
    /**
     * 保存并返回
     */
    String BTN_AE = "btn_ae";
    /**
     * 删除部署
     */
    String BTN_AF = "btn_af";
    /**
     * 新增部署
     */
    String BTN_AG = "btn_ag";
    /**
     * 新增联系人
     */
    String BTN_AH = "btn_ah";
    /**
     * 编辑联系人
     */
    String BTN_AI = "btn_ai";
    /**
     * 代办查看
     */
    String BTN_AJ = "btn_aj";
    /**
     * 代办处理
     */
    String BTN_AK = "btn_ak";
    /**
     * 绑定
     */
    String BTN_AL = "btn_al";
    /**
     * 已绑定
     */
    String BTN_AM = "btn_am";
    /**
     * 设置组名称
     */
    String BTN_AN = "btn_an";
    /**
     * 收起
     */
    String BTN_B = "btn_b";
    /**
     * 维保计划
     */
    String BTN_BC = "btn_bc";
    /**
     * 下一步
     */
    String BTN_BD = "btn_bd";
    /**
     * 请点击此链接
     */
    String BTN_BE = "btn_be";
    /**
     * 帮助
     */
    String BTN_BF = "btn_bf";
    /**
     * 模版新增
     */
    String BTN_C = "btn_c";
    /**
     * 取消
     */
    String BTN_CANCEL_A = "btn_cancel_a";
    /**
     * 保养
     */
    String BTN_CBM_A = "btn_cbm_a";
    /**
     * 清空
     */
    String BTN_CLEAR_A = "btn_clear_a";
    /**
     * 创建计划
     */
    String BTN_CREATE_A = "btn_create_a";
    /**
     * 获取验证码
     */
    String BTN_D = "btn_d";
    /**
     * 删除
     */
    String BTN_DELETE_A = "btn_delete_a";
    /**
     * 删除选中
     */
    String BTN_DELETE_B = "btn_delete_b";
    /**
     * 下载
     */
    String BTN_DOWN_A = "btn_down_a";
    /**
     * 下移
     */
    String BTN_DOWN_B = "btn_down_b";
    /**
     * 添加字段
     */
    String BTN_E = "btn_e";
    /**
     * 编辑
     */
    String BTN_EDIT_A = "btn_edit_a";
    /**
     * 估抄
     */
    String BTN_CF = "btn_cf";
    /**
     * 导出选中
     */
    String BTN_EXPORT_A = "btn_export_a";
    /**
     * 导出全部
     */
    String BTN_EXPORT_B = "btn_export_b";
    /**
     * 导出
     */
    String BTN_EXPORT_C = "btn_export_c";
    /**
     * 新增联动
     */
    String BTN_F = "btn_f";
    /**
     * 选择模版
     */
    String BTN_G = "btn_g";
    /**
     * 查看流程图
     */
    String BTN_H = "btn_h";
    /**
     * 修改
     */
    String BTN_I = "btn_i";
    /**
     * 导入{0}
     */
    String BTN_IMPORT_A = "btn_import_a";
    /**
     * 导入
     */
    String BTN_IMPORT_B = "btn_import_b";
    /**
     * 巡检
     */
    String BTN_INSPECTION_A = "btn_inspection_a";
    /**
     * 修改密码
     */
    String BTN_J = "btn_j";
    /**
     * 入库
     */
    String BTN_K = "btn_k";
    /**
     * 分配
     */
    String BTN_L = "btn_l";
    /**
     * 下载模版
     */
    String BTN_M = "btn_m";
    /**
     * 更多操作
     */
    String BTN_MORE_A = "btn_more_a";
    /**
     * 处理
     */
    String BTN_N = "btn_n";
    /**
     * 新增
     */
    String BTN_NEW_A = "btn_new_a";
    /**
     * 查看文件
     */
    String BTN_O = "btn_o";
    /**
     * 查看配置
     */
    String BTN_P = "btn_p";
    /**
     * 点检
     */
    String BTN_POINT_A = "btn_point_a";
    /**
     * 作废
     */
    String BTN_Q = "btn_q";
    /**
     * 生成选中二维码
     */
    String BTN_QR_A = "btn_qr_a";
    /**
     * 生成全部二维码
     */
    String BTN_QR_B = "btn_qr_b";
    /**
     * 生成{0}二维码
     */
    String BTN_R = "btn_r";
    /**
     * 维修
     */
    String BTN_REPAIR_A = "btn_repair_a";
    /**
     * 重置
     */
    String BTN_RST_A = "btn_rst_a";
    /**
     * 保存
     */
    String BTN_SAVE_A = "btn_save_a";
    /**
     * 搜索
     */
    String BTN_SEARCH_A = "btn_search_a";
    /**
     * 确定
     */
    String BTN_SURE_A = "btn_sure_a";
    /**
     * 上移
     */
    String BTN_UP_A = "btn_up_a";
    /**
     * 上传
     */
    String BTN_UPLOAD_A = "btn_upload_a";
    /**
     * 查看
     */
    String BTN_VIEW_A = "btn_view_a";
    /**
     * 创建{0}
     */
    String BTN_WSQ_A = "btn_wsq_a";
    /**
     * 已选中
     */
    String COM_DEVELOP_A = "com_develop_a";
    /**
     * 每页
     */
    String COM_EVE_A = "com_eve_a";
    /**
     * 页
     */
    String COM_PAGES_A = "com_pages_a";
    /**
     * 条
     */
    String COM_STRIP_A = "com_strip_a";
    /**
     * 至
     */
    String COM_TO_A = "com_to_a";
    /**
     * 共
     */
    String COM_TOTAL_A = "com_total_a";
    /**
     * 今日完成
     */
    String DP_A = "dp_a";
    /**
     * 姓名
     */
    String DP_AB = "dp_ab";
    /**
     * 岗位
     */
    String DP_AC = "dp_ac";
    /**
     * 任务数量
     */
    String DP_AD = "dp_ad";
    /**
     * 联系电话
     */
    String DP_AE = "dp_ae";
    /**
     * 今日故障数
     */
    String DP_AF = "dp_af";
    /**
     * 本周故障数
     */
    String DP_AG = "dp_ag";
    /**
     * 本月故障数
     */
    String DP_AH = "dp_ah";
    /**
     * 已修复
     */
    String DP_AI = "dp_ai";
    /**
     * 未修复
     */
    String DP_AJ = "dp_aj";
    /**
     * 修复进度
     */
    String DP_AK = "dp_ak";
    /**
     * 故障设备数
     */
    String DP_AL = "dp_al";
    /**
     * 完好设备数
     */
    String DP_AM = "dp_am";
    /**
     * 设备完好率
     */
    String DP_AN = "dp_an";
    /**
     * 本周完成
     */
    String DP_B = "dp_b";
    /**
     * 硬件故障
     */
    String DP_BC = "dp_bc";
    /**
     * 软件故障
     */
    String DP_BD = "dp_bd";
    /**
     * 网络故障
     */
    String DP_BE = "dp_be";
    /**
     * 其他故障
     */
    String DP_BF = "dp_bf";
    /**
     * 目标值
     */
    String DP_BG = "dp_bg";
    /**
     * 完成率
     */
    String DP_BH = "dp_bh";
    /**
     * 已完成
     */
    String DP_BI = "dp_bi";
    /**
     * 未完成
     */
    String DP_BJ = "dp_bj";
    /**
     * 今日维护数量
     */
    String DP_BK = "dp_bk";
    /**
     * 本周维护数量
     */
    String DP_BL = "dp_bl";
    /**
     * 本月维护数量
     */
    String DP_BM = "dp_bm";
    /**
     * 计划完成率
     */
    String DP_BN = "dp_bn";
    /**
     * 本月完成
     */
    String DP_C = "dp_c";
    /**
     * 计划完成率
     */
    String DP_CD = "dp_cd";
    /**
     * 计划任务数量
     */
    String DP_CE = "dp_ce";
    /**
     * 完成任务数量
     */
    String DP_CF = "dp_cf";
    /**
     * 本月运行时间
     */
    String DP_CG = "dp_cg";
    /**
     * 本月故障时间
     */
    String DP_CH = "dp_ch";
    /**
     * 本月维护时间
     */
    String DP_CI = "dp_ci";
    /**
     * 运行时间
     */
    String DP_CJ = "dp_cj";
    /**
     * 故障时间
     */
    String DP_CK = "dp_ck";
    /**
     * 维护时间
     */
    String DP_CL = "dp_cl";
    /**
     * 绩效标准
     */
    String DP_CM = "dp_cm";
    /**
     * 设备利用率
     */
    String DP_CN = "dp_cn";
    /**
     * 未完成任务
     */
    String DP_D = "dp_d";
    /**
     * 已完成任务
     */
    String DP_E = "dp_e";
    /**
     * 故障任务总数
     */
    String DP_F = "dp_f";
    /**
     * 维护任务总数
     */
    String DP_G = "dp_g";
    /**
     * 故障任务
     */
    String DP_H = "dp_h";
    /**
     * 维护任务
     */
    String DP_I = "dp_i";
    /**
     * 设备
     */
    String DP_J = "dp_j";
    /**
     * 所属位置
     */
    String DP_K = "dp_k";
    /**
     * 执行人
     */
    String DP_L = "dp_l";
    /**
     * 截止时间
     */
    String DP_M = "dp_m";
    /**
     * 当前状态
     */
    String DP_N = "dp_n";
    /**
     * 更新{0}：{1}修改为{2}
     */
    String LOG_A = "log_a";
    /**
     * 更新{0}：{d}修改为{d}
     */
    String LOG_D = "log_d";
    /**
     * 更新{0}【{d}】- {1}：{d}修改为{d}
     */
    String LOG_AE = "log_ae";
    /**
     * 该工号已经被注册
     */
    String LOG_G = "log_g";
    /**
     * 该账号已经被注册
     */
    String LOG_H = "log_h";
    /**
     * 全部标记已读
     */
    String LOG_I = "log_i";
    /**
     * 任务已提交
     */
    String LOG_J = "log_j";
    /**
     * 已报废
     */
    String LOG_K = "log_k";
    /**
     * 已驳回
     */
    String LOG_L = "log_l";
    /**
     * 已处理
     */
    String LOG_M = "log_m";
    /**
     * 已到期
     */
    String LOG_N = "log_n";
    /**
     * 已接单
     */
    String LOG_O = "log_o";
    /**
     * 操作人
     */
    String LOG_OPERATION_A = "log_operation_a";
    /**
     * 已拒绝
     */
    String LOG_P = "log_p";
    /**
     * 已没有更多数据
     */
    String LOG_Q = "log_q";
    /**
     * 已入库
     */
    String LOG_R = "log_r";
    /**
     * 已调整
     */
    String LOG_S = "log_s";
    /**
     * 已通过
     */
    String LOG_T = "log_t";
    /**
     * 已退出登录
     */
    String LOG_U = "log_u";
    /**
     * 已完成
     */
    String LOG_V = "log_v";
    /**
     * 已完成数量
     */
    String LOG_W = "log_w";
    /**
     * 已选图片
     */
    String LOG_X = "log_x";
    /**
     * 已作废
     */
    String LOG_Y = "log_y";
    /**
     * {0}不能为空
     */
    String MSG_A = "msg_a";
    /**
     * 该企业账号不存在，请与管理员申请开通服务！
     */
    String MSG_AA = "msg_aa";
    /**
     * 该手机号未注册，请先注册！
     */
    String MSG_AB = "msg_ab";
    /**
     * 该手机号已经被注册，请用别的手机号
     */
    String MSG_AC = "msg_ac";
    /**
     * 该账号无关联企业，请与管理员申请开通服务！
     */
    String MSG_AD = "msg_ad";
    /**
     * 工单作废，服务单关闭
     */
    String MSG_AE = "msg_ae";
    /**
     * 将二维码放入框内，即可自动扫描
     */
    String MSG_AF = "msg_af";
    /**
     * 密码必须包括字母，数字和特殊符号
     */
    String MSG_AG = "msg_ag";
    /**
     * 你的账号已经被锁定，请联系管理员解锁！
     */
    String MSG_AH = "msg_ah";
    /**
     * 您的企业已经被锁定，请联系管理员！
     */
    String MSG_AI = "msg_ai";
    /**
     * 您的企业已经被注销，请联系管理员！
     */
    String MSG_AJ = "msg_aj";
    /**
     * 您的设备不支持该功能，请手动拨打
     */
    String MSG_AK = "msg_ak";
    /**
     * 配置此项后，工单将直接分配到执行人
     */
    String MSG_AL = "msg_al";
    /**
     * 让对话框跟着某个元素，一个元素同时只能出现一个对话框
     */
    String MSG_AM = "msg_am";
    /**
     * 如2个规则都已经设置，系统取距离当前最近时间点通知派单负责人
     */
    String MSG_AN = "msg_an";
    /**
     * 设备报警时，第1次和第2次发送短信间隔（秒）
     */
    String MSG_AO = "msg_ao";
    /**
     * 设备报警时，第2次和第3次发送短信间隔（秒）
     */
    String MSG_AP = "msg_ap";
    /**
     * 设备报警时，第3次和第4次发送短信间隔（秒）
     */
    String MSG_AQ = "msg_aq";
    /**
     * 设备报警时，第4次后，依次发送短信间隔（秒）
     */
    String MSG_AR = "msg_ar";
    /**
     * 设备异常时，发送短信间隔（秒）
     */
    String MSG_AS = "msg_as";
    /**
     * 时，执行
     */
    String MSG_AT = "msg_at";
    /**
     * 手机号码未验证，请先进行短信验证！
     */
    String MSG_AU = "msg_au";
    /**
     * 说明：logo格式为png，像素宽高：144px*48px
     */
    String MSG_AV = "msg_av";
    /**
     * 条任务，直到全部任务执行完毕
     */
    String MSG_AW = "msg_aw";
    /**
     * 无法修改为收费用户，超出企业开通收费用户数量
     */
    String MSG_AX = "msg_ax";
    /**
     * 系统根据任务数量和指定的执行时间，自动平均分配每天的任务，适合大批量同类型设备维保计划的制定
     */
    String MSG_AY = "msg_ay";
    /**
     * 系统暂时不支持该您使用的客户端，抱歉您将无法正常使用本系统
     */
    String MSG_AZ = "msg_az";
    /**
     * {0}未选择
     */
    String MSG_B = "msg_b";
    /**
     * 需进行备件申领，申领完才能提交
     */
    String MSG_BA = "msg_ba";
    /**
     * 由用户设定计划执行的时间规则，按规则分配任务，适合于较复杂维保时间计划的制定
     */
    String MSG_BB = "msg_bb";
    /**
     * 账号或密码错误，请重试！
     */
    String MSG_BC = "msg_bc";
    /**
     * 执行日期，不能超过27，有的月份可能没有这一天
     */
    String MSG_BD = "msg_bd";
    /**
     * 【使用中、报废、闲置】状态不能修改！
     */
    String MSG_BE = "msg_be";
    /**
     * 处理中不允许取消！
     */
    String MSG_BF = "msg_bf";
    /**
     * 登录成功！
     */
    String MSG_BG = "msg_bg";
    /**
     * 分公司
     */
    String MSG_BH = "msg_bh";
    /**
     * 失败：缺少必要条件！
     */
    String MSG_BI = "msg_bi";
    /**
     * 完成处理不允许取消！
     */
    String MSG_BJ = "msg_bj";
    /**
     * 未知错误！
     */
    String MSG_BK = "msg_bk";
    /**
     * 已经评分过的人员绩效不能再次评分！
     */
    String MSG_BL = "msg_bl";
    /**
     * 已经取消！
     */
    String MSG_BM = "msg_bm";
    /**
     * 注册成功！
     */
    String MSG_BN = "msg_bn";
    /**
     * 模板错误：列数错误
     */
    String MSG_BO = "msg_bo";
    /**
     * 模板错误：sheet数错误
     */
    String MSG_BP = "msg_bp";
    /**
     * 超出企业开通收费用户数量上限
     */
    String MSG_BQ = "msg_bq";
    /**
     * 系统处理中，不能重复操作
     */
    String MSG_BR = "msg_br";
    /**
     * 对诊断结果申请了远程协助
     */
    String MSG_BS = "msg_bs";
    /**
     * 请分配一个执行人
     */
    String MSG_BT = "msg_bt";
    /**
     * 请检查前面输入信息是否符合要求
     */
    String MSG_BU = "msg_bu";
    /**
     * 请设置计划触发条件
     */
    String MSG_BV = "msg_bv";
    /**
     * 请选中要评分的人员绩效
     */
    String MSG_BW = "msg_bw";
    /**
     * 是否删除关联数据？
     */
    String MSG_BX = "msg_bx";
    /**
     * 是否直接要求现场服务
     */
    String MSG_BY = "msg_by";
    /**
     * 隐藏字段不能为必填
     */
    String MSG_C = "msg_c";
    /**
     * 权限不足
     */
    String MSG_CA = "msg_ca";
    /**
     * 非法操作
     */
    String MSG_CB = "msg_cb";
    /**
     * 用户未登录！
     */
    String MSG_CC = "msg_cc";
    /**
     * 失败：请获取最新数据后重试！
     */
    String MSG_CD = "msg_cd";
    /**
     * {0}下还有{1}，不能删除该{2}
     */
    String MSG_CE = "msg_ce";
    /**
     * 不支持{0}功能
     */
    String MSG_CF = "msg_cf";
    /**
     * 验证失效
     */
    String MSG_CG = "msg_cg";
    /**
     * 验证码错误
     */
    String MSG_CH = "msg_ch";
    /**
     * {0}下还有{1}，不能编辑该{2}
     */
    String MSG_CI = "msg_ci";
    /**
     * 项目部
     */
    String MSG_CJ = "msg_cj";
    /**
     * 队伍
     */
    String MSG_CK = "msg_ck";
    /**
     * 甲方单位
     */
    String MSG_CL = "msg_cl";
    /**
     * 任务单类型
     */
    String MSG_CM = "msg_cm";
    /**
     * 作业目的
     */
    String MSG_CN = "msg_cn";
    /**
     * 维保计划正在生成行事历，请稍后修改
     */
    String MSG_D = "msg_d";
    /**
     * 井号
     */
    String MSG_DE = "msg_de";
    /**
     * 井型
     */
    String MSG_DF = "msg_df";
    /**
     * 施工工艺
     */
    String MSG_DG = "msg_dg";
    /**
     * 是否用源
     */
    String MSG_DH = "msg_dh";
    /**
     * 桥塞类别
     */
    String MSG_DI = "msg_di";
    /**
     * 钻头尺寸
     */
    String MSG_DJ = "msg_dj";
    /**
     * 最大斜度
     */
    String MSG_DK = "msg_dk";
    /**
     * 套管尺寸
     */
    String MSG_DL = "msg_dl";
    /**
     * 地面仪器系列
     */
    String MSG_DM = "msg_dm";
    /**
     * 作业项目
     */
    String MSG_DN = "msg_dn";
    /**
     * 本流程不再使用时，请确认已解绑【工单模板-流程配置】中的【工单类型】
     */
    String MSG_E = "msg_e";
    /**
     * 井深
     */
    String MSG_EF = "msg_ef";
    /**
     * 下井次数
     */
    String MSG_EG = "msg_eg";
    /**
     * 成功次数
     */
    String MSG_EH = "msg_eh";
    /**
     * 井下仪器故障次数
     */
    String MSG_EI = "msg_ei";
    /**
     * 测量井段从
     */
    String MSG_EJ = "msg_ej";
    /**
     * 测量井段到
     */
    String MSG_EK = "msg_ek";
    /**
     * 测量米
     */
    String MSG_EL = "msg_el";
    /**
     * 取心设计数颗
     */
    String MSG_EM = "msg_em";
    /**
     * 收获岩心颗数
     */
    String MSG_EN = "msg_en";
    /**
     * 公司不存在或暂不支持NFC登录
     */
    String MSG_F = "msg_f";
    /**
     * 射开总厚度
     */
    String MSG_FG = "msg_fg";
    /**
     * 射孔段
     */
    String MSG_FH = "msg_fh";
    /**
     * 枪型
     */
    String MSG_FI = "msg_fi";
    /**
     * 单程公里数
     */
    String MSG_FJ = "msg_fj";
    /**
     * 实际到井时间
     */
    String MSG_FK = "msg_fk";
    /**
     * 出发时间
     */
    String MSG_FL = "msg_fl";
    /**
     * 返回时间
     */
    String MSG_FM = "msg_fm";
    /**
     * 安装时间
     */
    String MSG_FN = "msg_fn";
    /**
     * 不能为空
     */
    String MSG_G = "msg_g";
    /**
     * 拆除时间
     */
    String MSG_GH = "msg_gh";
    /**
     * 占井时间
     */
    String MSG_GI = "msg_gi";
    /**
     * 总时间
     */
    String MSG_GJ = "msg_gj";
    /**
     * 故障时间
     */
    String MSG_GK = "msg_gk";
    /**
     * 任务单备注
     */
    String MSG_GL = "msg_gl";
    /**
     * {0}重复，请修改
     */
    String MSG_H = "msg_h";
    /**
     * 操作成功
     */
    String MSG_I = "msg_i";
    /**
     * 必须为数字
     */
    String MSG_K = "msg_k";
    /**
     * 加载失败
     */
    String MSG_L = "msg_l";
    /**
     * NFC登录失败
     */
    String MSG_M = "msg_m";
    /**
     * 出井时间单位不能小于入井时间单位
     */
    String MSG_N = "msg_n";
    /**
     * ，第一个月的
     */
    String MSG_Q = "msg_q";
    /**
     * ，执行一次
     */
    String MSG_R = "msg_r";
    /**
     * 备件申领中，或已申领通过，您不能删除备件
     */
    String MSG_S = "msg_s";
    /**
     * 地图点击，可自动定位地址
     */
    String MSG_T = "msg_t";
    /**
     * 点击实时监控列表行，查看不同设备监控项的监控数据
     */
    String MSG_U = "msg_u";
    /**
     * 点击搜索，可定位到地图点
     */
    String MSG_V = "msg_v";
    /**
     * 短信发送成功，请在1分钟内完成验证。
     */
    String MSG_W = "msg_w";
    /**
     * 额外的收件人，可手动输入，多个“，”号隔开
     */
    String MSG_X = "msg_x";
    /**
     * 分钟没人领取时，自动发送提醒通知到派单负责人
     */
    String MSG_Y = "msg_y";
    /**
     * 该公司已经被注册，您可以申请认领！
     */
    String MSG_Z = "msg_z";
    /**
     * 切换
     */
    String TAB_A = "tab_a";
    /**
     * 设备型号
     */
    String TAB_ASSET_A = "tab_asset_a";
    /**
     * 设备类型
     */
    String TAB_ASSET_B = "tab_asset_b";
    /**
     * 设备详情
     */
    String TAB_ASSET_C = "tab_asset_c";
    /**
     * 报表报告
     */
    String TAB_B = "tab_b";
    /**
     * 报表信息
     */
    String TAB_C = "tab_c";
    /**
     * 生产作业信息维护详情
     */
    String TAB_D = "tab_d";
    /**
     * 互动记录
     */
    String TAB_M = "tab_m";
    /**
     * {0}编辑
     */
    String TAB_DEVICE_A = "tab_device_a";
    /**
     * 新增{0}
     */
    String TAB_DEVICE_B = "tab_device_b";
    /**
     * {0}详情
     */
    String TAB_DEVICE_C = "tab_device_c";
    /**
     * 附件文档
     */
    String TAB_DOC_A = "tab_doc_a";
    /**
     * 库房新增
     */
    String TAB_E = "tab_e";
    /**
     * 库房详情
     */
    String TAB_F = "tab_f";
    /**
     * 故障履历
     */
    String TAB_FAULT_A = "tab_fault_a";
    /**
     * 基本信息
     */
    String TAB_INFO_A = "tab_info_a";
    /**
     * 设备日志
     */
    String TAB_LOG_A = "tab_log_a";
    /**
     * 部件信息
     */
    String TAB_PARTS_A = "tab_parts_a";
    /**
     * 定制计划
     */
    String TAB_PLAN_A = "tab_plan_a";
    /**
     * 下一步工作
     */
    String TAB_WORK_A = "tab_work_a";
    /**
     * 历史工作
     */
    String TAB_WORK_B = "tab_work_b";
    /**
     * 上传的文件
     */
    String TEXT_A = "text_a";
    /**
     * 请选择要导入的文件（Excel格式），导入的文件必须符合数据导入模板的要求.导入的只能修改用户的角色其他都不能修改,角色之间用逗号分割,用英文逗号
     */
    String TEXT_AA = "text_aa";
    /**
     * 数据库中存在相同ID的记录时，将更新覆盖数据库中的记录.
     */
    String TEXT_AB = "text_ab";
    /**
     * 说明：点击单元格空白处，可添加排班；双击单条排班可编辑和删除
     */
    String TEXT_AC = "text_ac";
    /**
     * 请在有效范围内添加巡更点
     */
    String TEXT_AD = "text_ad";
    /**
     * 添加{0}
     */
    String TEXT_ADD_A = "text_add_a";
    /**
     * 系统处理中...
     */
    String TEXT_AE = "text_ae";
    /**
     * {d}【{0}】格式错误，应该是{1}格式{d}
     */
    String TEXT_AF = "text_af";
    /**
     * 导入成功了{d}条
     */
    String TEXT_AG = "text_ag";
    /**
     * 确定要{0}{1}{d}吗?
     */
    String TEXT_AH = "text_ah";
    /**
     * 请先在系统中添加{0}{d}
     */
    String TEXT_AI = "text_ai";
    /**
     * {0}了{1}{d}
     */
    String TEXT_AJ = "text_aj";
    /**
     * {0}{d}正在{1}处理中，无法重复申请{2}
     */
    String TEXT_AK = "text_ak";
    /**
     * （按{0}）
     */
    String TEXT_AL = "text_al";
    /**
     * 您的账户（{d}）{0}，到期后服务将被停止，请您及时联系销售人员进行续费或备份好数据，到期未续费的账户数据我们将为您保留7天。
     */
    String TEXT_AM = "text_am";
    /**
     * {0}{1}还没有处理完，{0}不能标记为盘点完成
     */
    String TEXT_AN = "text_an";
    /**
     * 未知
     */
    String TEXT_AO = "text_ao";
    /**
     * 批量删除
     */
    String TEXT_AP = "text_ap";
    /**
     * {0}-{1}
     */
    String TEXT_AQ = "text_aq";
    /**
     * 问题审批
     */
    String TEXT_AR = "text_ar";
    /**
     * 到场执行
     */
    String TEXT_AS = "text_as";
    /**
     * {0}关键字
     */
    String TEXT_ASSET_A = "text_asset_a";
    /**
     * 结果确认
     */
    String TEXT_AT = "text_at";
    /**
     * 上级部门
     */
    String TEXT_AU = "text_au";
    /**
     * 内部
     */
    String TEXT_AV = "text_av";
    /**
     * 外部
     */
    String TEXT_AW = "text_aw";
    /**
     * 所属部门
     */
    String TEXT_AX = "text_ax";
    /**
     * 入职日期
     */
    String TEXT_AY = "text_ay";
    /**
     * 工时成本
     */
    String TEXT_AZ = "text_az";
    /**
     * 注：文件大小不超过30M，支持txt、doc、xls、pdf、jpg、jpeg、png、bmp、gif、mp4类型。
     */
    String TEXT_B = "text_b";
    /**
     * 性别
     */
    String TEXT_BA = "text_ba";
    /**
     * 数据分类
     */
    String TEXT_BB = "text_bb";
    /**
     * 外
     */
    String TEXT_BC = "text_bc";
    /**
     * 角色描述
     */
    String TEXT_BD = "text_bd";
    /**
     * 可见范围
     */
    String TEXT_BE = "text_be";
    /**
     * 单人处理
     */
    String TEXT_BF = "text_bf";
    /**
     * 多人会签（所有人可处理，全同意后到下一环节）
     */
    String TEXT_BG = "text_bg";
    /**
     * 多人抢单（谁先提交处理，到下一环节）
     */
    String TEXT_BH = "text_bh";
    /**
     * 处理链(一个通过，到下一环节）
     */
    String TEXT_BI = "text_bi";
    /**
     * 请输入{0}
     */
    String TEXT_BJ = "text_bj";
    /**
     * 固定时间间隔
     */
    String TEXT_BK = "text_bk";
    /**
     * 固定维护工作量
     */
    String TEXT_BL = "text_bl";
    /**
     * 设备状态参数
     */
    String TEXT_BM = "text_bm";
    /**
     * 设备运行参数
     */
    String TEXT_BN = "text_bn";
    /**
     * 更换图片
     */
    String TEXT_C = "text_c";
    /**
     * 一月
     */
    String TEXT_CD = "text_cd";
    /**
     * 二月
     */
    String TEXT_CE = "text_ce";
    /**
     * 三月
     */
    String TEXT_CF = "text_cf";
    /**
     * 四月
     */
    String TEXT_CG = "text_cg";
    /**
     * 五月
     */
    String TEXT_CH = "text_ch";
    /**
     * 请选择{0}
     */
    String TEXT_CHOOSE_A = "text_choose_a";
    /**
     * 六月
     */
    String TEXT_CI = "text_ci";
    /**
     * 七月
     */
    String TEXT_CJ = "text_cj";
    /**
     * 八月
     */
    String TEXT_CK = "text_ck";
    /**
     * 九月
     */
    String TEXT_CL = "text_cl";
    /**
     * 十月
     */
    String TEXT_CM = "text_cm";
    /**
     * 十一月
     */
    String TEXT_CN = "text_cn";
    /**
     * 请输入
     */
    String TEXT_D = "text_d";
    /**
     * 十二月
     */
    String TEXT_DE = "text_de";
    /**
     * 未转工单
     */
    String TEXT_DF = "text_df";
    /**
     * 已转工单
     */
    String TEXT_DG = "text_dg";
    /**
     * H
     */
    String TEXT_DH = "text_dh";
    /**
     * {0}下没有查询到任何{1}，无需盘点
     */
    String TEXT_E = "text_e";
    /**
     * {0}审核退回
     */
    String TEXT_F = "text_f";
    /**
     * {0}已存在：{d}
     */
    String TEXT_G = "text_g";
    /**
     * {d}、{d}不能相同
     */
    String TEXT_H = "text_h";
    /**
     * 不允许{0}
     */
    String TEXT_I = "text_i";
    /**
     * 不可以{0}
     */
    String TEXT_J = "text_j";
    /**
     * {0}信息不存在
     */
    String TEXT_K = "text_k";
    /**
     * 只能盘点{0}库房的设备
     */
    String TEXT_L = "text_l";
    /**
     * 备件编码：{d}，库存数量不足，当前库存数量：{d}
     */
    String TEXT_M = "text_m";
    /**
     * 尊敬的{0}用户
     */
    String TEXT_N = "text_n";
    /**
     * 属性{d}必须为{0}！
     */
    String TEXT_O = "text_o";
    /**
     * 将于{d}到期
     */
    String TEXT_P = "text_p";
    /**
     * 操作成功，是否需要创建对应{0}？
     */
    String TEXT_Q = "text_q";
    /**
     * 数量（{d}）
     */
    String TEXT_R = "text_r";
    /**
     * 最多{d}张
     */
    String TEXT_S = "text_s";
    /**
     * 最多上传{d}张图片
     */
    String TEXT_T = "text_t";
    /**
     * 父设备所在位置为{0}，与当前设备的位置不一致，请重新选择设备位置
     */
    String TEXT_U = "text_u";
    /**
     * 长度最少{d}位
     */
    String TEXT_V = "text_v";
    /**
     * 文件
     */
    String TEXT_W = "text_w";
    /**
     * 自动生成工单时间点
     */
    String LOG_AB = "log_ab";
    /**
     * 自动生成
     */
    String LOG_E = "log_e";
    /**
     * 手动生成
     */
    String LOG_F = "log_f";
    /**
     * 用户
     */
    String TEXT_X = "text_x";
    /**
     * 公共字段
     */
    String TEXT_Y = "text_y";
    /**
     * 所有字段
     */
    String TEXT_Z = "text_z";
    /**
     * 工单详情字段
     */
    String TITLE_A = "title_a";
    /**
     * 安全库存短信接受
     */
    String TITLE_AAAA = "title_aaaa";
    /**
     * 未分配
     */
    String TITLE_AAAA_A = "title_aaaa_a";
    /**
     * 备件领用申请_app
     */
    String TITLE_AAAAA = "title_aaaaa";
    /**
     * 未启用
     */
    String TITLE_AAAA_B = "title_aaaa_b";
    /**
     * 备件入库申请_app
     */
    String TITLE_AAAAB = "title_aaaab";
    /**
     * 安全生产
     */
    String TITLE_AAAAB_A = "title_aaaab_a";
    /**
     * 安全隐患
     */
    String TITLE_AAAAB_B = "title_aaaab_b";
    /**
     * 安装
     */
    String TITLE_AAAAB_C = "title_aaaab_c";
    /**
     * 安装货币
     */
    String TITLE_AAAAB_D = "title_aaaab_d";
    /**
     * 安装调试
     */
    String TITLE_AAAAB_E = "title_aaaab_e";
    /**
     * 安装信息
     */
    String TITLE_AAAAB_F = "title_aaaab_f";
    /**
     * 按读数
     */
    String TITLE_AAAAB_G = "title_aaaab_g";
    /**
     * 按供应商
     */
    String TITLE_AAAAB_H = "title_aaaab_h";
    /**
     * 按钮
     */
    String TITLE_AAAAB_I = "title_aaaab_i";
    /**
     * 按时间
     */
    String TITLE_AAAAB_J = "title_aaaab_j";
    /**
     * 按数量
     */
    String TITLE_AAAAB_K = "title_aaaab_k";
    /**
     * 按天
     */
    String TITLE_AAAAB_L = "title_aaaab_l";
    /**
     * 按条件
     */
    String TITLE_AAAAB_M = "title_aaaab_m";
    /**
     * 按小时
     */
    String TITLE_AAAAB_N = "title_aaaab_n";
    /**
     * 按员工
     */
    String TITLE_AAAAB_O = "title_aaaab_o";
    /**
     * 按月
     */
    String TITLE_AAAAB_P = "title_aaaab_p";
    /**
     * 按组织
     */
    String TITLE_AAAAB_Q = "title_aaaab_q";
    /**
     * 百度
     */
    String TITLE_AAAAB_R = "title_aaaab_r";
    /**
     * 半年度保养计划
     */
    String TITLE_AAAAB_S = "title_aaaab_s";
    /**
     * 包含
     */
    String TITLE_AAAAB_T = "title_aaaab_t";
    /**
     * 包括准备工作
     */
    String TITLE_AAAAB_U = "title_aaaab_u";
    /**
     * 保存点检任务结果
     */
    String TITLE_AAAAB_V = "title_aaaab_v";
    /**
     * 保底值
     */
    String TITLE_AAAAB_W = "title_aaaab_w";
    /**
     * 保内
     */
    String TITLE_AAAAB_X = "title_aaaab_x";
    /**
     * 保外
     */
    String TITLE_AAAAB_Y = "title_aaaab_y";
    /**
     * 保修
     */
    String TITLE_AAAAB_Z = "title_aaaab_z";
    /**
     * 未入库
     */
    String TITLE_AAAA_C = "title_aaaa_c";
    /**
     * 备件报废申请_app
     */
    String TITLE_AAAAC = "title_aaaac";
    /**
     * 未设置
     */
    String TITLE_AAAA_D = "title_aaaa_d";
    /**
     * 设备报废申请_app
     */
    String TITLE_AAAAD = "title_aaaad";
    /**
     * 未提交
     */
    String TITLE_AAAA_E = "title_aaaa_e";
    /**
     * 工作任务分析
     */
    String TITLE_AAAAE = "title_aaaae";
    /**
     * 未完成
     */
    String TITLE_AAAA_F = "title_aaaa_f";
    /**
     * 设备监控_app
     */
    String TITLE_AAAAF = "title_aaaaf";
    /**
     * 未完成数量
     */
    String TITLE_AAAA_G = "title_aaaa_g";
    /**
     * scada权限
     */
    String TITLE_AAAAG = "title_aaaag";
    /**
     * 服务请求费用显示
     */
    String TITLE_AAAAH = "title_aaaah";
    /**
     * 未知状态
     */
    String TITLE_AAAA_I = "title_aaaa_i";
    /**
     * 工单费用显示
     */
    String TITLE_AAAAI = "title_aaaai";
    /**
     * 位置
     */
    String TITLE_AAAA_J = "title_aaaa_j";
    /**
     * 备件启用
     */
    String TITLE_AAAAJ = "title_aaaaj";
    /**
     * 位置（TOP20）
     */
    String TITLE_AAAA_K = "title_aaaa_k";
    /**
     * 用户注册审核
     */
    String TITLE_AAAAK = "title_aaaak";
    /**
     * 位置编码
     */
    String TITLE_AAAA_L = "title_aaaa_l";
    /**
     * 看板筛选
     */
    String TITLE_AAAAL = "title_aaaal";
    /**
     * 位置代码
     */
    String TITLE_AAAA_M = "title_aaaa_m";
    /**
     * 设备费用显示
     */
    String TITLE_AAAAM = "title_aaaam";
    /**
     * 位置地图
     */
    String TITLE_AAAA_N = "title_aaaa_n";
    /**
     * 备件费用显示
     */
    String TITLE_AAAAN = "title_aaaan";
    /**
     * 温度（℃）
     */
    String TITLE_AAAA_O = "title_aaaa_o";
    /**
     * 入库费用显示
     */
    String TITLE_AAAAO = "title_aaaao";
    /**
     * 文本
     */
    String TITLE_AAAA_P = "title_aaaa_p";
    /**
     * 角色详情
     */
    String TITLE_AAAAP = "title_aaaap";
    /**
     * 文档
     */
    String TITLE_AAAA_Q = "title_aaaa_q";
    /**
     * 用户组筛选
     */
    String TITLE_AAAAQ = "title_aaaaq";
    /**
     * 文明礼貌
     */
    String TITLE_AAAA_R = "title_aaaa_r";
    /**
     * 设备类型详情
     */
    String TITLE_AAAAR = "title_aaaar";
    /**
     * 文章
     */
    String TITLE_AAAA_S = "title_aaaa_s";
    /**
     * 用户还原
     */
    String TITLE_AAAAS = "title_aaaas";
    /**
     * 文章管理
     */
    String TITLE_AAAA_T = "title_aaaa_t";
    /**
     * 详细报告
     */
    String TITLE_AAAAT = "title_aaaat";
    /**
     * 文章来源
     */
    String TITLE_AAAA_U = "title_aaaa_u";
    /**
     * 用户详情
     */
    String TITLE_AAAAU = "title_aaaau";
    /**
     * 文章模板
     */
    String TITLE_AAAA_V = "title_aaaa_v";
    /**
     * 部门岗位
     */
    String TITLE_AAAAV = "title_aaaav";
    /**
     * 文字描述
     */
    String TITLE_AAAA_W = "title_aaaa_w";
    /**
     * 部门岗位详情
     */
    String TITLE_AAAAW = "title_aaaaw";
    /**
     * 问题个数
     */
    String TITLE_AAAA_X = "title_aaaa_x";
    /**
     * 数据模板
     */
    String TITLE_AAAAX = "title_aaaax";
    /**
     * 问题级别
     */
    String TITLE_AAAA_Y = "title_aaaa_y";
    /**
     * 备件
     */
    String TITLE_AAAAY = "title_aaaay";
    /**
     * 问题来源
     */
    String TITLE_AAAA_Z = "title_aaaa_z";
    /**
     * 安全库存
     */
    String TITLE_AAAAZ = "title_aaaaz";
    /**
     * 设备型号详情
     */
    String TITLE_AAAB = "title_aaab";
    /**
     * A区产线年度大修
     */
    String TITLE_AAAB_A = "title_aaab_a";
    /**
     * B区4号车间安全检查
     */
    String TITLE_AAAB_B = "title_aaab_b";
    /**
     * B区生产车间安全检查
     */
    String TITLE_AAAB_C = "title_aaab_c";
    /**
     * checklist
     */
    String TITLE_AAAB_D = "title_aaab_d";
    /**
     * CNY/元
     */
    String TITLE_AAAB_E = "title_aaab_e";
    /**
     * Cron任务
     */
    String TITLE_AAAB_F = "title_aaab_f";
    /**
     * English
     */
    String TITLE_AAAB_G = "title_aaab_g";
    /**
     * id
     */
    String TITLE_AAAB_H = "title_aaab_h";
    /**
     * IT保养工单时效过长关闭总数
     */
    String TITLE_AAAB_I = "title_aaab_i";
    /**
     * json串
     */
    String TITLE_AAAB_J = "title_aaab_j";
    /**
     * LOGO图片
     */
    String TITLE_AAAB_K = "title_aaab_k";
    /**
     * MTBF（平均故障间隔时间）
     */
    String TITLE_AAAB_L = "title_aaab_l";
    /**
     * MTTR（平均维修时效）
     */
    String TITLE_AAAB_M = "title_aaab_m";
    /**
     * photo
     */
    String TITLE_AAAB_N = "title_aaab_n";
    /**
     * PM
     */
    String TITLE_AAAB_O = "title_aaab_o";
    /**
     * PMC（预防性维护合规性）
     */
    String TITLE_AAAB_P = "title_aaab_p";
    /**
     * PMP（计划性维护百分比）
     */
    String TITLE_AAAB_Q = "title_aaab_q";
    /**
     * SCADA
     */
    String TITLE_AAAB_R = "title_aaab_r";
    /**
     * SMCP（计划维护关键百分比）
     */
    String TITLE_AAAB_S = "title_aaab_s";
    /**
     * spray
     */
    String TITLE_AAAB_T = "title_aaab_t";
    /**
     * VMI报表
     */
    String TITLE_AAAB_U = "title_aaab_u";
    /**
     * VMI分析
     */
    String TITLE_AAAB_V = "title_aaab_v";
    /**
     * X坐标
     */
    String TITLE_AAAB_W = "title_aaab_w";
    /**
     * Y坐标
     */
    String TITLE_AAAB_X = "title_aaab_x";
    /**
     * 安全检查
     */
    String TITLE_AAAB_Y = "title_aaab_y";
    /**
     * 安全日历
     */
    String TITLE_AAAB_Z = "title_aaab_z";
    /**
     * 设备效率图表
     */
    String TITLE_AAAC = "title_aaac";
    /**
     * 维修次数
     */
    String TITLE_AAAC_A = "title_aaac_a";
    /**
     * 维修单号
     */
    String TITLE_AAAC_B = "title_aaac_b";
    /**
     * 维修技能
     */
    String TITLE_AAAC_C = "title_aaac_c";
    /**
     * 维修进度
     */
    String TITLE_AAAC_D = "title_aaac_d";
    /**
     * 维修内容
     */
    String TITLE_AAAC_E = "title_aaac_e";
    /**
     * 维修平均时效分析
     */
    String TITLE_AAAC_F = "title_aaac_f";
    /**
     * 维修其他时效
     */
    String TITLE_AAAC_G = "title_aaac_g";
    /**
     * 维修人
     */
    String TITLE_AAAC_H = "title_aaac_h";
    /**
     * 维修人员
     */
    String TITLE_AAAC_I = "title_aaac_i";
    /**
     * 维修时间
     */
    String TITLE_AAAC_J = "title_aaac_j";
    /**
     * 维修时间(h)
     */
    String TITLE_AAAC_K = "title_aaac_k";
    /**
     * 维修时效
     */
    String TITLE_AAAC_L = "title_aaac_l";
    /**
     * 维修时效分析
     */
    String TITLE_AAAC_M = "title_aaac_m";
    /**
     * 维修使用数量
     */
    String TITLE_AAAC_N = "title_aaac_n";
    /**
     * 维修图片
     */
    String TITLE_AAAC_O = "title_aaac_o";
    /**
     * 维修完成数
     */
    String TITLE_AAAC_P = "title_aaac_p";
    /**
     * 维修信息
     */
    String TITLE_AAAC_Q = "title_aaac_q";
    /**
     * 维修养护统计
     */
    String TITLE_AAAC_R = "title_aaac_r";
    /**
     * 维修症状描述
     */
    String TITLE_AAAC_S = "title_aaac_s";
    /**
     * 维修状态
     */
    String TITLE_AAAC_T = "title_aaac_t";
    /**
     * 维修总次数
     */
    String TITLE_AAAC_U = "title_aaac_u";
    /**
     * 维修总时间
     */
    String TITLE_AAAC_V = "title_aaac_v";
    /**
     * 维修总时间</br>（分钟）
     */
    String TITLE_AAAC_W = "title_aaac_w";
    /**
     * 维修总数
     */
    String TITLE_AAAC_X = "title_aaac_x";
    /**
     * 未处理
     */
    String TITLE_AAAC_Y = "title_aaac_y";
    /**
     * 未读
     */
    String TITLE_AAAC_Z = "title_aaac_z";
    /**
     * 供应商维修类型统计
     */
    String TITLE_AAAD = "title_aaad";
    /**
     * 问题描述
     */
    String TITLE_AAAD_A = "title_aaad_a";
    /**
     * 问题上报
     */
    String TITLE_AAAD_B = "title_aaad_b";
    /**
     * 问题图片
     */
    String TITLE_AAAD_C = "title_aaad_c";
    /**
     * 我的待办
     */
    String TITLE_AAAD_D = "title_aaad_d";
    /**
     * 我要点检
     */
    String TITLE_AAAD_E = "title_aaad_e";
    /**
     * 我要巡检
     */
    String TITLE_AAAD_F = "title_aaad_f";
    /**
     * 污染源
     */
    String TITLE_AAAD_G = "title_aaad_g";
    /**
     * 污染因子
     */
    String TAB_AD = "tab_ad";
    /**
     * 无
     */
    String TITLE_AAAD_H = "title_aaad_h";
    /**
     * 无法上移
     */
    String TITLE_AAAD_I = "title_aaad_i";
    /**
     * 无法下移
     */
    String TITLE_AAAD_J = "title_aaad_j";
    /**
     * 无物联
     */
    String TITLE_AAAD_K = "title_aaad_k";
    /**
     * 无物联（No IoT）
     */
    String TITLE_AAAD_L = "title_aaad_l";
    /**
     * 无效关闭
     */
    String TITLE_AAAD_M = "title_aaad_m";
    /**
     * 无效链接
     */
    String TITLE_AAAD_N = "title_aaad_n";
    /**
     * 物联
     */
    String TITLE_AAAD_O = "title_aaad_o";
    /**
     * 物联地址
     */
    String TITLE_AAAD_P = "title_aaad_p";
    /**
     * 物联对接平台ID
     */
    String TITLE_AAAD_Q = "title_aaad_q";
    /**
     * 物联对接平台方ID
     */
    String TITLE_AAAD_R = "title_aaad_r";
    /**
     * 物联二维码
     */
    String TITLE_AAAD_S = "title_aaad_s";
    /**
     * 物联离线
     */
    String TITLE_AAAD_T = "title_aaad_t";
    /**
     * 物联信息
     */
    String TITLE_AAAD_U = "title_aaad_u";
    /**
     * 物联在线
     */
    String TITLE_AAAD_V = "title_aaad_v";
    /**
     * 物联账号
     */
    String TITLE_AAAD_W = "title_aaad_w";
    /**
     * 物联账号密码
     */
    String TITLE_AAAD_X = "title_aaad_x";
    /**
     * 物联状态
     */
    String TITLE_AAAD_Y = "title_aaad_y";
    /**
     * 物料
     */
    String TITLE_AAAD_Z = "title_aaad_z";
    /**
     * 员工效率图表
     */
    String TITLE_AAAE = "title_aaae";
    /**
     * 备件管理详情
     */
    String TITLE_AAAF = "title_aaaf";
    /**
     * 备件/BOM
     */
    String TITLE_AAAG = "title_aaag";
    /**
     * 关联客户
     */
    String TITLE_AAAH = "title_aaah";
    /**
     * 位置信息
     */
    String TITLE_AAAI = "title_aaai";
    /**
     * PC盘点
     */
    String TITLE_AAAJ = "title_aaaj";
    /**
     * 用户审核
     */
    String TITLE_AAAK = "title_aaak";
    /**
     * 位置费用显示
     */
    String TITLE_AAAL = "title_aaal";
    /**
     * 待办分配
     */
    String TITLE_AAAM = "title_aaam";
    /**
     * webSCADA
     */
    String TITLE_AAAN = "title_aaan";
    /**
     * 用量补登
     */
    String TITLE_AAAO = "title_aaao";
    /**
     * brose设备监控
     */
    String TITLE_AAAP = "title_aaap";
    /**
     * 费用结算
     */
    String TITLE_AAAQ = "title_aaaq";
    /**
     * 统计分析费用显示
     */
    String TITLE_AAAR = "title_aaar";
    /**
     * 区域监控
     */
    String TITLE_AAAS = "title_aaas";
    /**
     * 统计分析
     */
    String TITLE_AAAT = "title_aaat";
    /**
     * 产品管理
     */
    String TITLE_AAAU = "title_aaau";
    /**
     * 系统管理
     */
    String TITLE_AAAV = "title_aaav";
    /**
     * 指标配置
     */
    String TITLE_AAAW = "title_aaaw";
    /**
     * 物流录入
     */
    String TITLE_AAAX = "title_aaax";
    /**
     * 备件修改
     */
    String TITLE_AAAY = "title_aaay";
    /**
     * 维护配置修改
     */
    String TITLE_AAAZ = "title_aaaz";
    /**
     * 1月
     */
    String TITLE_AAB_A = "title_aab_a";
    /**
     * 20号
     */
    String TITLE_AAB_B = "title_aab_b";
    /**
     * 21号
     */
    String TITLE_AAB_C = "title_aab_c";
    /**
     * 22号
     */
    String TITLE_AAB_D = "title_aab_d";
    /**
     * 23号
     */
    String TITLE_AAB_E = "title_aab_e";
    /**
     * 24号
     */
    String TITLE_AAB_F = "title_aab_f";
    /**
     * 25号
     */
    String TITLE_AAB_G = "title_aab_g";
    /**
     * 26号
     */
    String TITLE_AAB_H = "title_aab_h";
    /**
     * 27号
     */
    String TITLE_AAB_I = "title_aab_i";
    /**
     * 28号
     */
    String TITLE_AAB_J = "title_aab_j";
    /**
     * 29号
     */
    String TITLE_AAB_K = "title_aab_k";
    /**
     * 2级
     */
    String TITLE_AAB_L = "title_aab_l";
    /**
     * 2月
     */
    String TITLE_AAB_M = "title_aab_m";
    /**
     * 30号
     */
    String TITLE_AAB_N = "title_aab_n";
    /**
     * 31号
     */
    String TITLE_AAB_O = "title_aab_o";
    /**
     * 3级
     */
    String TITLE_AAB_P = "title_aab_p";
    /**
     * 3月
     */
    String TITLE_AAB_Q = "title_aab_q";
    /**
     * 4级
     */
    String TITLE_AAB_R = "title_aab_r";
    /**
     * 4月
     */
    String TITLE_AAB_S = "title_aab_s";
    /**
     * 5级
     */
    String TITLE_AAB_T = "title_aab_t";
    /**
     * 5月
     */
    String TITLE_AAB_U = "title_aab_u";
    /**
     * 6月
     */
    String TITLE_AAB_V = "title_aab_v";
    /**
     * 7月
     */
    String TITLE_AAB_W = "title_aab_w";
    /**
     * 8月
     */
    String TITLE_AAB_X = "title_aab_x";
    /**
     * 9月
     */
    String TITLE_AAB_Y = "title_aab_y";
    /**
     * at88
     */
    String TITLE_AAB_Z = "title_aab_z";
    /**
     * 每月
     */
    String TITLE_AAH_A = "title_aah_a";
    /**
     * 每周
     */
    String TITLE_AAH_B = "title_aah_b";
    /**
     * 每周提醒
     */
    String TITLE_AAH_C = "title_aah_c";
    /**
     * 门店代号
     */
    String TITLE_AAH_D = "title_aah_d";
    /**
     * 门店确认
     */
    String TITLE_AAH_E = "title_aah_e";
    /**
     * 密码
     */
    String TITLE_AAH_F = "title_aah_f";
    /**
     * 密码修改
     */
    String TITLE_AAH_G = "title_aah_g";
    /**
     * 描述
     */
    String TITLE_AAH_H = "title_aah_h";
    /**
     * 描述信息
     */
    String TITLE_AAH_I = "title_aah_i";
    /**
     * 秒
     */
    String TITLE_AAH_J = "title_aah_j";
    /**
     * 明细
     */
    String TITLE_AAH_K = "title_aah_k";
    /**
     * 模板
     */
    String TITLE_AAH_L = "title_aah_l";
    /**
     * 模板key
     */
    String TITLE_AAH_M = "title_aah_m";
    /**
     * 模板编号
     */
    String TITLE_AAH_N = "title_aah_n";
    /**
     * 模板内容
     */
    String TITLE_AAH_O = "title_aah_o";
    /**
     * 模板任务
     */
    String TITLE_AAH_P = "title_aah_p";
    /**
     * 模块
     */
    String TITLE_AAH_Q = "title_aah_q";
    /**
     * 默认工单池
     */
    String TITLE_AAH_R = "title_aah_r";
    /**
     * 默认密码
     */
    String TITLE_AAH_S = "title_aah_s";
    /**
     * 默认显示前10个
     */
    String TITLE_AAH_T = "title_aah_t";
    /**
     * 目标值
     */
    String TITLE_AAH_U = "title_aah_u";
    /**
     * 目测
     */
    String TITLE_AAH_V = "title_aah_v";
    /**
     * 目的
     */
    String TITLE_AAH_W = "title_aah_w";
    /**
     * 内部编码
     */
    String TITLE_AAH_X = "title_aah_x";
    /**
     * 内部用户
     */
    String TITLE_AAH_Y = "title_aah_y";
    /**
     * 内部组织
     */
    String TITLE_AAH_Z = "title_aah_z";
    /**
     * 临时值存储5
     */
    String TITLE_AAI_A = "title_aai_a";
    /**
     * 临时字段1
     */
    String TITLE_AAI_B = "title_aai_b";
    /**
     * 领用单号
     */
    String TITLE_AAI_C = "title_aai_c";
    /**
     * 领用人
     */
    String TITLE_AAI_D = "title_aai_d";
    /**
     * 领用申请
     */
    String TITLE_AAI_E = "title_aai_e";
    /**
     * 领用数量
     */
    String TITLE_AAI_F = "title_aai_f";
    /**
     * 领用消耗
     */
    String TITLE_AAI_G = "title_aai_g";
    /**
     * 领用总数
     */
    String TITLE_AAI_H = "title_aai_h";
    /**
     * 流程
     */
    String TITLE_AAI_I = "title_aai_i";
    /**
     * 流程ID
     */
    String TITLE_AAI_J = "title_aai_j";
    /**
     * 流程KEY
     */
    String TITLE_AAI_K = "title_aai_k";
    /**
     * 流程版本
     */
    String TITLE_AAI_L = "title_aai_l";
    /**
     * 流程定义
     */
    String TITLE_AAI_M = "title_aai_m";
    /**
     * 流程模板
     */
    String TITLE_AAI_N = "title_aai_n";
    /**
     * 流程实例
     */
    String TITLE_AAI_O = "title_aai_o";
    /**
     * 流程图片
     */
    String TITLE_AAI_P = "title_aai_p";
    /**
     * 流程状态
     */
    String TITLE_AAI_Q = "title_aai_q";
    /**
     * 满意度
     */
    String TITLE_AAI_R = "title_aai_r";
    /**
     * 满足全部条件触发
     */
    String TITLE_AAI_S = "title_aai_s";
    /**
     * 满足任意条件触发
     */
    String TITLE_AAI_T = "title_aai_t";
    /**
     * 每
     */
    String TITLE_AAI_U = "title_aai_u";
    /**
     * 每日
     */
    String TITLE_AAI_V = "title_aai_v";
    /**
     * 每天
     */
    String TITLE_AAI_W = "title_aai_w";
    /**
     * 每天提醒
     */
    String TITLE_AAI_X = "title_aai_x";
    /**
     * 每天执行
     */
    String TITLE_AAI_Y = "title_aai_y";
    /**
     * 每天执行时刻
     */
    String TITLE_AAI_Z = "title_aai_z";
    /**
     * 来源库房
     */
    String TITLE_AAJ_A = "title_aaj_a";
    /**
     * 累计
     */
    String TITLE_AAJ_B = "title_aaj_b";
    /**
     * 累计登录
     */
    String TITLE_AAJ_C = "title_aaj_c";
    /**
     * 累计签到
     */
    String TITLE_AAJ_D = "title_aaj_d";
    /**
     * 离线
     */
    String TITLE_AAJ_E = "title_aaj_e";
    /**
     * 离线（Offline）
     */
    String TITLE_AAJ_F = "title_aaj_f";
    /**
     * 历史
     */
    String TITLE_AAJ_G = "title_aaj_g";
    /**
     * 历史记录
     */
    String TITLE_AAJ_H = "title_aaj_h";
    /**
     * 历史曲线
     */
    String TITLE_AAJ_I = "title_aaj_i";
    /**
     * 历史数据
     */
    String TITLE_AAJ_J = "title_aaj_j";
    /**
     * 利旧
     */
    String TITLE_AAJ_K = "title_aaj_k";
    /**
     * 连续量
     */
    String TITLE_AAJ_L = "title_aaj_l";
    /**
     * 联动
     */
    String TITLE_AAJ_M = "title_aaj_m";
    /**
     * 联系电话
     */
    String TITLE_AAJ_N = "title_aaj_n";
    /**
     * 联系方式
     */
    String TITLE_AAJ_O = "title_aaj_o";
    /**
     * 联系人
     */
    String TITLE_AAJ_P = "title_aaj_p";
    /**
     * 联系人姓名
     */
    String TITLE_AAJ_Q = "title_aaj_q";
    /**
     * 两次输入的密码不一致
     */
    String TITLE_AAJ_R = "title_aaj_r";
    /**
     * 列表列
     */
    String TITLE_AAJ_S = "title_aaj_s";
    /**
     * 列表显示
     */
    String TITLE_AAJ_T = "title_aaj_t";
    /**
     * 列号
     */
    String TITLE_AAJ_U = "title_aaj_u";
    /**
     * 临时地址
     */
    String TITLE_AAJ_V = "title_aaj_v";
    /**
     * 临时任务
     */
    String TITLE_AAJ_W = "title_aaj_w";
    /**
     * 临时值存储
     */
    String TITLE_AAJ_X = "title_aaj_x";
    /**
     * 临时值存储3
     */
    String TITLE_AAJ_Y = "title_aaj_y";
    /**
     * 临时值存储4
     */
    String TITLE_AAJ_Z = "title_aaj_z";
    /**
     * 清理
     */
    String TITLE_AAK_A = "title_aak_a";
    /**
     * 请求
     */
    String TITLE_AAK_B = "title_aak_b";
    /**
     * 请求地址
     */
    String TITLE_AAK_C = "title_aak_c";
    /**
     * 请求服务时间
     */
    String TITLE_AAK_D = "title_aak_d";
    /**
     * 请求内容
     */
    String TITLE_AAK_E = "title_aak_e";
    /**
     * 区
     */
    String TITLE_AAK_F = "title_aak_f";
    /**
     * 区间
     */
    String TITLE_AAK_G = "title_aak_g";
    /**
     * 区域
     */
    String TITLE_AAK_H = "title_aak_h";
    /**
     * 区域信息
     */
    String TITLE_AAK_I = "title_aak_i";
    /**
     * 区域组件
     */
    String TITLE_AAK_J = "title_aak_j";
    /**
     * 曲线显示最近
     */
    String TITLE_AAK_K = "title_aak_k";
    /**
     * 取消
     */
    String TITLE_AAK_L = "title_aak_l";
    /**
     * 去完成
     */
    String TITLE_AAK_M = "title_aak_m";
    /**
     * 权限
     */
    String TITLE_AAK_N = "title_aak_n";
    /**
     * 权限设置
     */
    String TITLE_AAK_P = "title_aak_p";
    /**
     * 权限位置
     */
    String TITLE_AAK_Q = "title_aak_q";
    /**
     * 权限信息
     */
    String TITLE_AAK_R = "title_aak_r";
    /**
     * 权重
     */
    String TITLE_AAK_S = "title_aak_s";
    /**
     * 权重得分
     */
    String TITLE_AAK_T = "title_aak_t";
    /**
     * 全部请求
     */
    String TITLE_AAK_U = "title_aak_u";
    /**
     * 全厂生产技能大比武
     */
    String TITLE_AAK_V = "title_aak_v";
    /**
     * 全屏
     */
    String TITLE_AAK_W = "title_aak_w";
    /**
     * 全屏面板
     */
    String TITLE_AAK_X = "title_aak_x";
    /**
     * 全选
     */
    String TITLE_AAK_Y = "title_aak_y";
    /**
     * 确认
     */
    String TITLE_AAK_Z = "title_aak_z";
    /**
     * 其他故障次数
     */
    String TITLE_AAL_A = "title_aal_a";
    /**
     * 其他故障率
     */
    String TITLE_AAL_B = "title_aal_b";
    /**
     * 其他故障总时间（分钟）
     */
    String TITLE_AAL_C = "title_aal_c";
    /**
     * 其它附件
     */
    String TITLE_AAL_D = "title_aal_d";
    /**
     * 企业LOGO
     */
    String TITLE_AAL_E = "title_aal_e";
    /**
     * 企业地址
     */
    String TITLE_AAL_F = "title_aal_f";
    /**
     * 企业管理员
     */
    String TITLE_AAL_G = "title_aal_g";
    /**
     * 企业信息
     */
    String TITLE_AAL_H = "title_aal_h";
    /**
     * 企业账号
     */
    String TITLE_AAL_I = "title_aal_i";
    /**
     * 启动
     */
    String TITLE_AAL_J = "title_aal_j";
    /**
     * 启动者
     */
    String TITLE_AAL_K = "title_aal_k";
    /**
     * 启用
     */
    String TITLE_AAL_L = "title_aal_l";
    /**
     * 启用时间
     */
    String TITLE_AAL_M = "title_aal_m";
    /**
     * 启用状态
     */
    String TITLE_AAL_N = "title_aal_n";
    /**
     * 签到
     */
    String TITLE_AAL_O = "title_aal_o";
    /**
     * 签到时间
     */
    String TITLE_AAL_P = "title_aal_p";
    /**
     * 签名
     */
    String TITLE_AAL_Q = "title_aal_q";
    /**
     * 签收
     */
    String TITLE_AAL_R = "title_aal_r";
    /**
     * 签收人
     */
    String TITLE_AAL_S = "title_aal_s";
    /**
     * 签收时间
     */
    String TITLE_AAL_T = "title_aal_t";
    /**
     * 签退
     */
    String TITLE_AAL_U = "title_aal_u";
    /**
     * 签退时间
     */
    String TITLE_AAL_V = "title_aal_v";
    /**
     * 抢单
     */
    String TITLE_AAL_W = "title_aal_w";
    /**
     * 抢单模式
     */
    String TITLE_AAL_X = "title_aal_x";
    /**
     * 抢修服务
     */
    String TITLE_AAL_Y = "title_aal_y";
    /**
     * 清除
     */
    String TITLE_AAL_Z = "title_aal_z";
    /**
     * 设备管理详情
     */
    String TITLE_AAM = "title_aam";
    /**
     * 平均故障间隔和修复时间
     */
    String TITLE_AAM_A = "title_aam_a";
    /**
     * 平均故障间隔时间
     */
    String TITLE_AAM_B = "title_aam_b";
    /**
     * 平均故障间隔时间（分钟）
     */
    String TITLE_AAM_C = "title_aam_c";
    /**
     * 平均故障时长（分钟）
     */
    String TITLE_AAM_D = "title_aam_d";
    /**
     * 平均故障维修工时分析
     */
    String TITLE_AAM_E = "title_aam_e";
    /**
     * 平均故障修复时间
     */
    String TITLE_AAM_F = "title_aam_f";
    /**
     * 平均确认时效（分钟）
     */
    String TITLE_AAM_G = "title_aam_g";
    /**
     * 平均时效（分钟）
     */
    String TITLE_AAM_H = "title_aam_h";
    /**
     * 平均维修</br>时效（分钟）
     */
    String TITLE_AAM_I = "title_aam_i";
    /**
     * 平均维修到场时效
     */
    String TITLE_AAM_J = "title_aam_j";
    /**
     * 平均维修时效
     */
    String TITLE_AAM_K = "title_aam_k";
    /**
     * 平均维修时效（分钟）
     */
    String TITLE_AAM_L = "title_aam_l";
    /**
     * 平均维修时效</br>（分钟）
     */
    String TITLE_AAM_M = "title_aam_m";
    /**
     * 平均维修时长（分钟）
     */
    String TITLE_AAM_N = "title_aam_n";
    /**
     * 平均维修响应时间
     */
    String TITLE_AAM_O = "title_aam_o";
    /**
     * 平均巡检时效（分钟）
     */
    String TITLE_AAM_P = "title_aam_p";
    /**
     * 平面图
     */
    String TITLE_AAM_Q = "title_aam_q";
    /**
     * 平面线故障率
     */
    String TITLE_AAM_R = "title_aam_r";
    /**
     * 平面线故障率（%）
     */
    String TITLE_AAM_S = "title_aam_s";
    /**
     * 平台配置
     */
    String TITLE_AAM_T = "title_aam_t";
    /**
     * 评分
     */
    String TITLE_AAM_U = "title_aam_u";
    /**
     * 评价
     */
    String TITLE_AAM_V = "title_aam_v";
    /**
     * 评论
     */
    String TITLE_AAM_W = "title_aam_w";
    /**
     * 评论记录
     */
    String TITLE_AAM_X = "title_aam_x";
    /**
     * 其他
     */
    String TITLE_AAM_Y = "title_aam_y";
    /**
     * 其他故障
     */
    String TITLE_AAM_Z = "title_aam_z";
    /**
     * 供应商统计
     */
    String TITLE_AAN = "title_aan";
    /**
     * 盘点要求
     */
    String TITLE_AAN_A = "title_aan_a";
    /**
     * 盘点中
     */
    String TITLE_AAN_B = "title_aan_b";
    /**
     * 盘点子编码
     */
    String TITLE_AAN_C = "title_aan_c";
    /**
     * 盘点组织
     */
    String TITLE_AAN_D = "title_aan_d";
    /**
     * 判定值
     */
    String TITLE_AAN_E = "title_aan_e";
    /**
     * 配置
     */
    String TITLE_AAN_F = "title_aan_f";
    /**
     * 配置管理
     */
    String TITLE_AAN_G = "title_aan_g";
    /**
     * 喷吹气源压力
     */
    String TITLE_AAN_H = "title_aan_h";
    /**
     * 批报表
     */
    String TITLE_AAN_I = "title_aan_i";
    /**
     * 批报查询
     */
    String TITLE_AAN_J = "title_aan_j";
    /**
     * 批次
     */
    String TITLE_AAN_K = "title_aan_k";
    /**
     * 皮带故障
     */
    String TITLE_AAN_L = "title_aan_l";
    /**
     * 皮带故障次数
     */
    String TITLE_AAN_M = "title_aan_m";
    /**
     * 皮带故障率
     */
    String TITLE_AAN_N = "title_aan_n";
    /**
     * 皮带故障总时间（分钟）
     */
    String TITLE_AAN_O = "title_aan_o";
    /**
     * 品牌
     */
    String TITLE_AAN_P = "title_aan_p";
    /**
     * 平均
     */
    String TITLE_AAN_Q = "title_aan_q";
    /**
     * 平均保养</br>时效（分钟）
     */
    String TITLE_AAN_R = "title_aan_r";
    /**
     * 平均保养时效
     */
    String TITLE_AAN_S = "title_aan_s";
    /**
     * 平均保养时效（分钟）
     */
    String TITLE_AAN_T = "title_aan_t";
    /**
     * 平均保养时效</br>（分钟）
     */
    String TITLE_AAN_U = "title_aan_u";
    /**
     * 平均到场时效（分钟）
     */
    String TITLE_AAN_V = "title_aan_v";
    /**
     * 平均点检时效（分钟）
     */
    String TITLE_AAN_W = "title_aan_w";
    /**
     * 平均分
     */
    String TITLE_AAN_X = "title_aan_x";
    /**
     * 平均分配时效（分钟）
     */
    String TITLE_AAN_Y = "title_aan_y";
    /**
     * 平均故障间隔
     */
    String TITLE_AAN_Z = "title_aan_z";
    /**
     * 员工统计
     */
    String TITLE_AAO = "title_aao";
    /**
     * 上报来源
     */
    String TITLE_AAO_A = "title_aao_a";
    /**
     * 上报人员
     */
    String TITLE_AAO_B = "title_aao_b";
    /**
     * 上报一个问题
     */
    String TITLE_AAO_C = "title_aao_c";
    /**
     * 上传时间
     */
    String TITLE_AAO_D = "title_aao_d";
    /**
     * 上次采购价格
     */
    String TITLE_AAO_E = "title_aao_e";
    /**
     * 上级位置
     */
    String TITLE_AAO_F = "title_aao_f";
    /**
     * 上限值
     */
    String TITLE_AAO_G = "title_aao_g";
    /**
     * 设置成员角色
     */
    String TITLE_AAO_H = "title_aao_h";
    /**
     * 设置位置
     */
    String TITLE_AAO_I = "title_aao_i";
    /**
     * 申领
     */
    String TITLE_AAO_J = "title_aao_j";
    /**
     * 申领采购
     */
    String TITLE_AAO_K = "title_aao_k";
    /**
     * 申领中
     */
    String TITLE_AAO_L = "title_aao_l";
    /**
     * 申请
     */
    String TITLE_AAO_M = "title_aao_m";
    /**
     * 申请单号
     */
    String TITLE_AAO_N = "title_aao_n";
    /**
     * 申请人员
     */
    String TITLE_AAO_O = "title_aao_o";
    /**
     * 申请时间
     */
    String TITLE_AAO_P = "title_aao_p";
    /**
     * 伸缩机故障率
     */
    String TITLE_AAO_Q = "title_aao_q";
    /**
     * 伸缩机故障率（%）
     */
    String TITLE_AAO_R = "title_aao_r";
    /**
     * 审核
     */
    String TITLE_AAO_S = "title_aao_s";
    /**
     * 审核人
     */
    String TITLE_AAO_T = "title_aao_t";
    /**
     * 审核人员
     */
    String TITLE_AAO_U = "title_aao_u";
    /**
     * 审核时间
     */
    String TITLE_AAO_V = "title_aao_v";
    /**
     * 审核通过
     */
    String TITLE_AAO_W = "title_aao_w";
    /**
     * 审核退回
     */
    String TITLE_AAO_X = "title_aao_x";
    /**
     * 审核完成
     */
    String TITLE_AAO_Y = "title_aao_y";
    /**
     * 审核信息
     */
    String TITLE_AAO_Z = "title_aao_z";
    /**
     * 位置中心统计
     */
    String TITLE_AAP = "title_aap";
    /**
     * 日常任务
     */
    String TITLE_AAP_A = "title_aap_a";
    /**
     * 日度重要事项
     */
    String TITLE_AAP_B = "title_aap_b";
    /**
     * 日计划
     */
    String TITLE_AAP_C = "title_aap_c";
    /**
     * 日历记录
     */
    String TITLE_AAP_D = "title_aap_d";
    /**
     * 日执行
     */
    String TITLE_AAP_E = "title_aap_e";
    /**
     * 日志
     */
    String TITLE_AAP_F = "title_aap_f";
    /**
     * 容量
     */
    String TITLE_AAP_G = "title_aap_g";
    /**
     * 如A0001-003-3
     */
    String TITLE_AAP_H = "title_aap_h";
    /**
     * 如南孚
     */
    String TITLE_AAP_I = "title_aap_i";
    /**
     * 如智能机器人
     */
    String TITLE_AAP_J = "title_aap_j";
    /**
     * 入库
     */
    String TITLE_AAP_K = "title_aap_k";
    /**
     * 入库单号
     */
    String TITLE_AAP_L = "title_aap_l";
    /**
     * 入库人
     */
    String TITLE_AAP_M = "title_aap_m";
    /**
     * 入库时间
     */
    String TITLE_AAP_N = "title_aap_n";
    /**
     * 入库数量
     */
    String TITLE_AAP_O = "title_aap_o";
    /**
     * 入库图片
     */
    String TITLE_AAP_P = "title_aap_p";
    /**
     * 入库中
     */
    String TITLE_AAP_Q = "title_aap_q";
    /**
     * 入库总量
     */
    String TITLE_AAP_R = "title_aap_r";
    /**
     * 润滑
     */
    String TITLE_AAP_S = "title_aap_s";
    /**
     * 扫码开始
     */
    String TITLE_AAP_T = "title_aap_t";
    /**
     * 扫码签到
     */
    String TITLE_AAP_U = "title_aap_u";
    /**
     * 扫描
     */
    String TITLE_AAP_V = "title_aap_v";
    /**
     * 扫一扫
     */
    String TITLE_AAP_W = "title_aap_w";
    /**
     * 筛选
     */
    String TITLE_AAP_X = "title_aap_x";
    /**
     * 上
     */
    String TITLE_AAP_Y = "title_aap_y";
    /**
     * 上报
     */
    String TITLE_AAP_Z = "title_aap_z";
    /**
     * 场地维护统计
     */
    String TITLE_AAQ = "title_aaq";
    /**
     * 任务开始
     */
    String TITLE_AAQ_A = "title_aaq_a";
    /**
     * 任务量
     */
    String TITLE_AAQ_B = "title_aaq_b";
    /**
     * 任务列表
     */
    String TITLE_AAQ_C = "title_aaq_c";
    /**
     * 任务描述
     */
    String TITLE_AAQ_D = "title_aaq_d";
    /**
     * 任务模板
     */
    String TITLE_AAQ_E = "title_aaq_e";
    /**
     * 任务配置
     */
    String TITLE_AAQ_F = "title_aaq_f";
    /**
     * 任务频率
     */
    String TITLE_AAQ_G = "title_aaq_g";
    /**
     * 任务评分
     */
    String TITLE_AAQ_H = "title_aaq_h";
    /**
     * 任务评价
     */
    String TITLE_AAQ_I = "title_aaq_i";
    /**
     * 任务签收提交
     */
    String TITLE_AAQ_J = "title_aaq_j";
    /**
     * 任务实现
     */
    String TITLE_AAQ_K = "title_aaq_k";
    /**
     * 任务数分析
     */
    String TITLE_AAQ_L = "title_aaq_l";
    /**
     * 任务数量
     */
    String TITLE_AAQ_M = "title_aaq_m";
    /**
     * 任务图片
     */
    String TITLE_AAQ_N = "title_aaq_n";
    /**
     * 任务完成查询
     */
    String TITLE_AAQ_O = "title_aaq_o";
    /**
     * 任务完成情况分析
     */
    String TITLE_AAQ_P = "title_aaq_p";
    /**
     * 任务项
     */
    String TITLE_AAQ_Q = "title_aaq_q";
    /**
     * 任务项备件
     */
    String TITLE_AAQ_R = "title_aaq_r";
    /**
     * 任务项编号
     */
    String TITLE_AAQ_S = "title_aaq_s";
    /**
     * 任务项描述
     */
    String TITLE_AAQ_T = "title_aaq_t";
    /**
     * 任务项权限
     */
    String TITLE_AAQ_U = "title_aaq_u";
    /**
     * 任务信息
     */
    String TITLE_AAQ_V = "title_aaq_v";
    /**
     * 任务要求
     */
    String TITLE_AAQ_W = "title_aaq_w";
    /**
     * 任务周期
     */
    String TITLE_AAQ_X = "title_aaq_x";
    /**
     * 任务状态
     */
    String TITLE_AAQ_Y = "title_aaq_y";
    /**
     * 日报表
     */
    String TITLE_AAQ_Z = "title_aaq_z";
    /**
     * 场地维修类型统计
     */
    String TITLE_AAR = "title_aar";
    /**
     * 确认密码
     */
    String TITLE_AAR_A = "title_aar_a";
    /**
     * 确认时间
     */
    String TITLE_AAR_B = "title_aar_b";
    /**
     * 确认时效
     */
    String TITLE_AAR_C = "title_aar_c";
    /**
     * 确认事项
     */
    String TITLE_AAR_D = "title_aar_d";
    /**
     * 确认项
     */
    String TITLE_AAR_E = "title_aar_e";
    /**
     * 热线处理
     */
    String TITLE_AAR_F = "title_aar_f";
    /**
     * 人
     */
    String TITLE_AAR_G = "title_aar_g";
    /**
     * 人工计划
     */
    String TITLE_AAR_H = "title_aar_h";
    /**
     * 人为图片
     */
    String TITLE_AAR_I = "title_aar_i";
    /**
     * 人员
     */
    String TITLE_AAR_J = "title_aar_j";
    /**
     * 人员到岗
     */
    String TITLE_AAR_K = "title_aar_k";
    /**
     * 人员绩效
     */
    String TITLE_AAR_L = "title_aar_l";
    /**
     * 人员排班
     */
    String TITLE_AAR_M = "title_aar_m";
    /**
     * 人员数量
     */
    String TITLE_AAR_N = "title_aar_n";
    /**
     * 人员信息
     */
    String TITLE_AAR_O = "title_aar_o";
    /**
     * 任务
     */
    String TITLE_AAR_P = "title_aar_p";
    /**
     * 任务编号
     */
    String TITLE_AAR_Q = "title_aar_q";
    /**
     * 任务编码
     */
    String TITLE_AAR_R = "title_aar_r";
    /**
     * 任务等待接单
     */
    String TITLE_AAR_S = "title_aar_s";
    /**
     * 任务等级
     */
    String TITLE_AAR_T = "title_aar_t";
    /**
     * 任务发布
     */
    String TITLE_AAR_U = "title_aar_u";
    /**
     * 任务工时分析
     */
    String TITLE_AAR_V = "title_aar_v";
    /**
     * 任务建议
     */
    String TITLE_AAR_W = "title_aar_w";
    /**
     * 任务交付物
     */
    String TITLE_AAR_X = "title_aar_x";
    /**
     * 任务接收人
     */
    String TITLE_AAR_Y = "title_aar_y";
    /**
     * 任务结束
     */
    String TITLE_AAR_Z = "title_aar_z";
    /**
     * 位置效率统计
     */
    String TITLE_AAS = "title_aas";
    /**
     * 顺序
     */
    String TITLE_AAS_A = "title_aas_a";
    /**
     * 说明
     */
    String TITLE_AAS_B = "title_aas_b";
    /**
     * 缩略图
     */
    String TITLE_AAS_C = "title_aas_c";
    /**
     * 所属厂商
     */
    String TITLE_AAS_D = "title_aas_d";
    /**
     * 所属代码
     */
    String TITLE_AAS_E = "title_aas_e";
    /**
     * 所属服务商
     */
    String TITLE_AAS_F = "title_aas_f";
    /**
     * 所属工单池
     */
    String TITLE_AAS_G = "title_aas_g";
    /**
     * 所属工单号
     */
    String TITLE_AAS_H = "title_aas_h";
    /**
     * 所属角色
     */
    String TITLE_AAS_I = "title_aas_i";
    /**
     * 所属库房
     */
    String TITLE_AAS_J = "title_aas_j";
    /**
     * 所属模板
     */
    String TITLE_AAS_K = "title_aas_k";
    /**
     * 所属区域
     */
    String TITLE_AAS_L = "title_aas_l";
    /**
     * 所属位置
     */
    String TITLE_AAS_M = "title_aas_m";
    /**
     * 所属组
     */
    String TITLE_AAS_N = "title_aas_n";
    /**
     * 所属组织
     */
    String TITLE_AAS_O = "title_aas_o";
    /**
     * 所辖组织
     */
    String TITLE_AAS_P = "title_aas_p";
    /**
     * 所需资源
     */
    String TITLE_AAS_Q = "title_aas_q";
    /**
     * 所有
     */
    String TITLE_AAS_R = "title_aas_r";
    /**
     * 所有方法
     */
    String TITLE_AAS_S = "title_aas_s";
    /**
     * 所有任务每
     */
    String TITLE_AAS_T = "title_aas_t";
    /**
     * 所有状态
     */
    String TITLE_AAS_U = "title_aas_u";
    /**
     * 所有组织
     */
    String TITLE_AAS_V = "title_aas_v";
    /**
     * 锁定
     */
    String TITLE_AAS_W = "title_aas_w";
    /**
     * 台
     */
    String TITLE_AAS_X = "title_aas_x";
    /**
     * 特殊权限
     */
    String TITLE_AAS_Y = "title_aas_y";
    /**
     * 提交
     */
    String TITLE_AAS_Z = "title_aas_z";
    /**
     * 设备维护统计
     */
    String TITLE_AAT = "title_aat";
    /**
     * 适用对象
     */
    String TITLE_AAT_A = "title_aat_a";
    /**
     * 收货量
     */
    String TITLE_AAT_B = "title_aat_b";
    /**
     * 收货量（KG）
     */
    String TITLE_AAT_C = "title_aat_c";
    /**
     * 手工录入
     */
    String TITLE_AAT_D = "title_aat_d";
    /**
     * 手机端标题块
     */
    String TITLE_AAT_E = "title_aat_e";
    /**
     * 手机号码
     */
    String TITLE_AAT_F = "title_aat_f";
    /**
     * 手摸
     */
    String TITLE_AAT_G = "title_aat_g";
    /**
     * 首页
     */
    String TITLE_AAT_H = "title_aat_h";
    /**
     * 售后信息
     */
    String TITLE_AAT_I = "title_aat_i";
    /**
     * 输入
     */
    String TITLE_AAT_J = "title_aat_j";
    /**
     * 输入校验
     */
    String TITLE_AAT_K = "title_aat_k";
    /**
     * 输入值
     */
    String TITLE_AAT_L = "title_aat_l";
    /**
     * 属于
     */
    String TITLE_AAT_M = "title_aat_m";
    /**
     * 树形
     */
    String TITLE_AAT_N = "title_aat_n";
    /**
     * 树状显示
     */
    String TITLE_AAT_O = "title_aat_o";
    /**
     * 数据
     */
    String TITLE_AAT_P = "title_aat_p";
    /**
     * 数据Key
     */
    String TITLE_AAT_Q = "title_aat_q";
    /**
     * 数据来源
     */
    String TITLE_AAT_R = "title_aat_r";
    /**
     * 数据同步
     */
    String TITLE_AAT_S = "title_aat_s";
    /**
     * 数据校验
     */
    String TITLE_AAT_T = "title_aat_t";
    /**
     * 数据值
     */
    String TITLE_AAT_U = "title_aat_u";
    /**
     * 数量
     */
    String TITLE_AAT_V = "title_aat_v";
    /**
     * 数值
     */
    String TITLE_AAT_W = "title_aat_w";
    /**
     * 数字
     */
    String TITLE_AAT_X = "title_aat_x";
    /**
     * 刷新
     */
    String TITLE_AAT_Y = "title_aat_y";
    /**
     * 税前单价
     */
    String TITLE_AAT_Z = "title_aat_z";
    /**
     * 设备故障统计
     */
    String TITLE_AAU = "title_aau";
    /**
     * 使用年限格式不对
     */
    String TITLE_AAU_A = "title_aau_a";
    /**
     * 使用寿命
     */
    String TITLE_AAU_B = "title_aau_b";
    /**
     * 使用数量
     */
    String TITLE_AAU_C = "title_aau_c";
    /**
     * 使用位置
     */
    String TITLE_AAU_D = "title_aau_d";
    /**
     * 使用组织
     */
    String TITLE_AAU_E = "title_aau_e";
    /**
     * 市
     */
    String TITLE_AAU_F = "title_aau_f";
    /**
     * 事后照片
     */
    String TITLE_AAU_G = "title_aau_g";
    /**
     * 事前照片
     */
    String TITLE_AAU_H = "title_aau_h";
    /**
     * 是
     */
    String TITLE_AAU_I = "title_aau_i";
    /**
     * 是否
     */
    String TITLE_AAU_J = "title_aau_j";
    /**
     * 是否返修
     */
    String TITLE_AAU_K = "title_aau_k";
    /**
     * 是否分配
     */
    String TITLE_AAU_L = "title_aau_l";
    /**
     * 付费用户
     */
    String TITLE_AAU_M = "title_aau_m";
    /**
     * 是否合格
     */
    String TITLE_AAU_N = "title_aau_n";
    /**
     * 是否紧缺
     */
    String TITLE_AAU_O = "title_aau_o";
    /**
     * 是否可填写
     */
    String TITLE_AAU_P = "title_aau_p";
    /**
     * 是否判定变量
     */
    String TITLE_AAU_Q = "title_aau_q";
    /**
     * 是否启用
     */
    String TITLE_AAU_R = "title_aau_r";
    /**
     * 是否收费
     */
    String TITLE_AAU_S = "title_aau_s";
    /**
     * 是否完修
     */
    String TITLE_AAU_T = "title_aau_t";
    /**
     * 是否委外服务商
     */
    String TITLE_AAU_U = "title_aau_u";
    /**
     * 是否选择
     */
    String TITLE_AAU_V = "title_aau_v";
    /**
     * 是否有子工单
     */
    String TITLE_AAU_W = "title_aau_w";
    /**
     * 是否主供应商
     */
    String TITLE_AAU_X = "title_aau_x";
    /**
     * 是否主键
     */
    String TITLE_AAU_Y = "title_aau_y";
    /**
     * 是否主联系人
     */
    String TITLE_AAU_Z = "title_aau_z";
    /**
     * 供应商维修统计
     */
    String TITLE_AAV = "title_aav";
    /**
     * 审核意见
     */
    String TITLE_AAV_A = "title_aav_a";
    /**
     * 审核照片
     */
    String TITLE_AAV_B = "title_aav_b";
    /**
     * 审核状态
     */
    String TITLE_AAV_C = "title_aav_c";
    /**
     * 审批
     */
    String TITLE_AAV_D = "title_aav_d";
    /**
     * 升级CallOut
     */
    String TITLE_AAV_E = "title_aav_e";
    /**
     * 升降排序
     */
    String TITLE_AAV_F = "title_aav_f";
    /**
     * 生成二维码的区域
     */
    String TITLE_AAV_G = "title_aav_g";
    /**
     * 生成模式
     */
    String TITLE_AAV_H = "title_aav_h";
    /**
     * 生成时间
     */
    String TITLE_AAV_I = "title_aav_i";
    /**
     * 省
     */
    String TITLE_AAV_J = "title_aav_j";
    /**
     * 剩余库存
     */
    String TITLE_AAV_K = "title_aav_k";
    /**
     * 时间
     */
    String TITLE_AAV_L = "title_aav_l";
    /**
     * 时间（平月）
     */
    String TITLE_AAV_M = "title_aav_m";
    /**
     * 时间（周）
     */
    String TITLE_AAV_N = "title_aav_n";
    /**
     * 时效
     */
    String TITLE_AAV_O = "title_aav_o";
    /**
     * 时效（分钟）
     */
    String TITLE_AAV_P = "title_aav_p";
    /**
     * 时效比
     */
    String TITLE_AAV_Q = "title_aav_q";
    /**
     * 实际费用
     */
    String TITLE_AAV_R = "title_aav_r";
    /**
     * 实际工时（h）
     */
    String TITLE_AAV_S = "title_aav_s";
    /**
     * 实际在岗
     */
    String TITLE_AAV_T = "title_aav_t";
    /**
     * 实施措施
     */
    String TITLE_AAV_U = "title_aav_u";
    /**
     * 实施措施描述
     */
    String TITLE_AAV_V = "title_aav_v";
    /**
     * 实时
     */
    String TITLE_AAV_W = "title_aav_w";
    /**
     * 实时监测
     */
    String TITLE_AAV_X = "title_aav_x";
    /**
     * 实时监控
     */
    String TITLE_AAV_Y = "title_aav_y";
    /**
     * 实时数据
     */
    String TITLE_AAV_Z = "title_aav_z";
    /**
     * 供应商故障统计
     */
    String TITLE_AAW = "title_aaw";
    /**
     * 网络报修
     */
    String TITLE_AAW_A = "title_aaw_a";
    /**
     * 忘记密码
     */
    String TITLE_AAW_B = "title_aaw_b";
    /**
     * 微信
     */
    String TITLE_AAW_C = "title_aaw_c";
    /**
     * 职务
     */
    String TITLE_AEIK = "title_aeik";
    /**
     * {d}【{0}】为空或格式有误
     */
    String TITLE_AAW_D = "title_aaw_d";
    /**
     * 违规操作
     */
    String TITLE_AAW_E = "title_aaw_e";
    /**
     * 违规图片
     */
    String TITLE_AAW_F = "title_aaw_f";
    /**
     * 违规照片
     */
    String TITLE_AAW_G = "title_aaw_g";
    /**
     * 维保次数与平均维修时长（分钟）
     */
    String TITLE_AAW_H = "title_aaw_h";
    /**
     * 维保对象
     */
    String TITLE_AAW_I = "title_aaw_i";
    /**
     * 维保履历
     */
    String TITLE_AAW_J = "title_aaw_j";
    /**
     * 维保人
     */
    String TITLE_AAW_K = "title_aaw_k";
    /**
     * 维保人员账号
     */
    String TITLE_AAW_L = "title_aaw_l";
    /**
     * 维保统计
     */
    String TITLE_AAW_M = "title_aaw_m";
    /**
     * 维保项目
     */
    String TITLE_AAW_N = "title_aaw_n";
    /**
     * 维保效率（%）
     */
    String TITLE_AAW_O = "title_aaw_o";
    /**
     * 维护
     */
    String TITLE_AAW_P = "title_aaw_p";
    /**
     * 维护保修
     */
    String TITLE_AAW_Q = "title_aaw_q";
    /**
     * 维护保养
     */
    String TITLE_AAW_R = "title_aaw_r";
    /**
     * 维护管理报告
     */
    String TITLE_AAW_S = "title_aaw_s";
    /**
     * 维护配置
     */
    String TITLE_AAW_T = "title_aaw_t";
    /**
     * 维护频率
     */
    String TITLE_AAW_U = "title_aaw_u";
    /**
     * 维护人员执行
     */
    String TITLE_AAW_V = "title_aaw_v";
    /**
     * 维护优先级
     */
    String TITLE_AAW_W = "title_aaw_w";
    /**
     * 维修
     */
    String TITLE_AAW_X = "title_aaw_x";
    /**
     * 维修保养支持备件申领
     */
    String TITLE_AAW_Y = "title_aaw_y";
    /**
     * 维修备件
     */
    String TITLE_AAW_Z = "title_aaw_z";
    /**
     * 员工维护统计
     */
    String TITLE_AAX = "title_aax";
    /**
     * 统计指标
     */
    String TITLE_AAX_A = "title_aax_a";
    /**
     * 突发故障
     */
    String TITLE_AAX_B = "title_aax_b";
    /**
     * 图标
     */
    String TITLE_AAX_C = "title_aax_c";
    /**
     * 图标配置
     */
    String TITLE_AAX_D = "title_aax_d";
    /**
     * 图表
     */
    String TITLE_AAX_E = "title_aax_e";
    /**
     * 图表配置
     */
    String TITLE_AAX_F = "title_aax_f";
    /**
     * 图片
     */
    String TITLE_AAX_G = "title_aax_g";
    /**
     * 图片不能大于5M
     */
    String TITLE_AAX_H = "title_aax_h";
    /**
     * 图片浏览
     */
    String TITLE_AAX_I = "title_aax_i";
    /**
     * 图片信息
     */
    String TITLE_AAX_J = "title_aax_j";
    /**
     * 团队
     */
    String TITLE_AAX_K = "title_aax_k";
    /**
     * 推荐
     */
    String TITLE_AAX_L = "title_aax_l";
    /**
     * 推荐备件
     */
    String TITLE_AAX_M = "title_aax_m";
    /**
     * 推荐原因
     */
    String TITLE_AAX_N = "title_aax_n";
    /**
     * 退出
     */
    String TITLE_AAX_O = "title_aax_o";
    /**
     * 退回
     */
    String TITLE_AAX_P = "title_aax_p";
    /**
     * 退回原因
     */
    String TITLE_AAX_Q = "title_aax_q";
    /**
     * 外部用户
     */
    String TITLE_AAX_R = "title_aax_r";
    /**
     * 外派订单
     */
    String TITLE_AAX_S = "title_aax_s";
    /**
     * 完成
     */
    String TITLE_AAX_T = "title_aax_t";
    /**
     * 完成关闭
     */
    String TITLE_AAX_U = "title_aax_u";
    /**
     * 完成任务
     */
    String TITLE_AAX_V = "title_aax_v";
    /**
     * 完成时效
     */
    String TITLE_AAX_W = "title_aax_w";
    /**
     * 完成提交
     */
    String TITLE_AAX_X = "title_aax_x";
    /**
     * 完工报告
     */
    String TITLE_AAX_Y = "title_aax_y";
    /**
     * 完修说明
     */
    String TITLE_AAX_Z = "title_aax_z";
    /**
     * 员工维修统计
     */
    String TITLE_AAY = "title_aay";
    /**
     * 调度人
     */
    String TITLE_AAY_A = "title_aay_a";
    /**
     * 调入审核
     */
    String TITLE_AAY_B = "title_aay_b";
    /**
     * 调试
     */
    String TITLE_AAY_C = "title_aay_c";
    /**
     * 调往库房
     */
    String TITLE_AAY_D = "title_aay_d";
    /**
     * 调往位置
     */
    String TITLE_AAY_E = "title_aay_e";
    /**
     * 调整
     */
    String TITLE_AAY_F = "title_aay_f";
    /**
     * 调整位置
     */
    String TITLE_AAY_G = "title_aay_g";
    /**
     * 停机时间(h)
     */
    String TITLE_AAY_H = "title_aay_h";
    /**
     * 停用
     */
    String TITLE_AAY_I = "title_aay_i";
    /**
     * 停止
     */
    String TITLE_AAY_J = "title_aay_j";
    /**
     * 通过
     */
    String TITLE_AAY_K = "title_aay_k";
    /**
     * 通用地址
     */
    String TITLE_AAY_L = "title_aay_l";
    /**
     * 通用模版
     */
    String TITLE_AAY_M = "title_aay_m";
    /**
     * 通知成功
     */
    String TITLE_AAY_N = "title_aay_n";
    /**
     * 通知配置
     */
    String TITLE_AAY_O = "title_aay_o";
    /**
     * 通知人员
     */
    String TITLE_AAY_P = "title_aay_p";
    /**
     * 通知失败
     */
    String TITLE_AAY_Q = "title_aay_q";
    /**
     * 同步
     */
    String TITLE_AAY_R = "title_aay_r";
    /**
     * 同意
     */
    String TITLE_AAY_S = "title_aay_s";
    /**
     * 统计
     */
    String TITLE_AAY_T = "title_aay_t";
    /**
     * 统计报表
     */
    String TITLE_AAY_U = "title_aay_u";
    /**
     * 统计方式
     */
    String TITLE_AAY_V = "title_aay_v";
    /**
     * 统计分析配置
     */
    String TITLE_AAY_W = "title_aay_w";
    /**
     * 统计分析预览
     */
    String TITLE_AAY_X = "title_aay_x";
    /**
     * 统计配置
     */
    String TITLE_AAY_Y = "title_aay_y";
    /**
     * 统计图表
     */
    String TITLE_AAY_Z = "title_aay_z";
    /**
     * 接受报警短信
     */
    String TITLE_AAZ = "title_aaz";
    /**
     * 提交审批
     */
    String TITLE_AAZ_A = "title_aaz_a";
    /**
     * 提交时间
     */
    String TITLE_AAZ_B = "title_aaz_b";
    /**
     * 提前生成
     */
    String TITLE_AAZ_C = "title_aaz_c";
    /**
     * 提示
     */
    String TITLE_AAZ_D = "title_aaz_d";
    /**
     * 提醒
     */
    String TITLE_AAZ_E = "title_aaz_e";
    /**
     * 提醒规则
     */
    String TITLE_AAZ_F = "title_aaz_f";
    /**
     * 提醒一次
     */
    String TITLE_AAZ_G = "title_aaz_g";
    /**
     * 替代品
     */
    String TITLE_AAZ_H = "title_aaz_h";
    /**
     * 替换
     */
    String TITLE_AAZ_I = "title_aaz_i";
    /**
     * 天
     */
    String TITLE_AAZ_J = "title_aaz_j";
    /**
     * 天完成
     */
    String TITLE_AAZ_K = "title_aaz_k";
    /**
     * 天执行一遍
     */
    String TITLE_AAZ_L = "title_aaz_l";
    /**
     * 填充Key
     */
    String TITLE_AAZ_M = "title_aaz_m";
    /**
     * 填充Val
     */
    String TITLE_AAZ_N = "title_aaz_n";
    /**
     * 挑战值
     */
    String TITLE_AAZ_O = "title_aaz_o";
    /**
     * 条件参数
     */
    String TITLE_AAZ_P = "title_aaz_p";
    /**
     * 条码机模版
     */
    String TITLE_AAZ_Q = "title_aaz_q";
    /**
     * 调拨
     */
    String TITLE_AAZ_R = "title_aaz_r";
    /**
     * 调拨单号
     */
    String TITLE_AAZ_S = "title_aaz_s";
    /**
     * 调拨量
     */
    String TITLE_AAZ_T = "title_aaz_t";
    /**
     * 调拨人
     */
    String TITLE_AAZ_U = "title_aaz_u";
    /**
     * 调拨数量
     */
    String TITLE_AAZ_V = "title_aaz_v";
    /**
     * 调拨图片
     */
    String TITLE_AAZ_W = "title_aaz_w";
    /**
     * 调拨总数
     */
    String TITLE_AAZ_X = "title_aaz_x";
    /**
     * 调出审核
     */
    String TITLE_AAZ_Y = "title_aaz_y";
    /**
     * 调出图片
     */
    String TITLE_AAZ_Z = "title_aaz_z";
    /**
     * 工单详情字段联动
     */
    String TITLE_AB = "title_ab";
    /**
     * 组织编号
     */
    String TITLE_AB_A = "title_ab_a";
    /**
     * 保修期
     */
    String TITLE_ABA_A = "title_aba_a";
    /**
     * 保养总用时（分钟）
     */
    String TITLE_ABAA_A = "title_abaa_a";
    /**
     * 备件盈亏
     */
    String TITLE_ABAAA_A = "title_abaaa_a";
    /**
     * 备件用途
     */
    String TITLE_ABAAA_B = "title_abaaa_b";
    /**
     * 备件总数
     */
    String TITLE_ABAAA_C = "title_abaaa_c";
    /**
     * 本次服务内容
     */
    String TITLE_ABAAA_D = "title_abaaa_d";
    /**
     * 本次维护过程发现的问题
     */
    String TITLE_ABAAA_E = "title_abaaa_e";
    /**
     * 本次维护子系统
     */
    String TITLE_ABAAA_F = "title_abaaa_f";
    /**
     * 本月
     */
    String TITLE_ABAAA_G = "title_abaaa_g";
    /**
     * 本月保养完成率
     */
    String TITLE_ABAAA_H = "title_abaaa_h";
    /**
     * 本月点检完成率
     */
    String TITLE_ABAAA_I = "title_abaaa_i";
    /**
     * 本月技能之星
     */
    String TITLE_ABAAA_J = "title_abaaa_j";
    /**
     * 本月任务之星
     */
    String TITLE_ABAAA_K = "title_abaaa_k";
    /**
     * 本月维修进度
     */
    String TITLE_ABAAA_L = "title_abaaa_l";
    /**
     * 本月巡检完成率
     */
    String TITLE_ABAAA_M = "title_abaaa_m";
    /**
     * 本周
     */
    String TITLE_ABAAA_N = "title_abaaa_n";
    /**
     * 比较条件
     */
    String TITLE_ABAAA_O = "title_abaaa_o";
    /**
     * 必填
     */
    String TITLE_ABAAA_P = "title_abaaa_p";
    /**
     * 必填项
     */
    String TITLE_ABAAA_Q = "title_abaaa_q";
    /**
     * 必选字段
     */
    String TITLE_ABAAA_R = "title_abaaa_r";
    /**
     * 编号
     */
    String TITLE_ABAAA_S = "title_abaaa_s";
    /**
     * 编码
     */
    String TITLE_ABAAA_T = "title_abaaa_t";
    /**
     * 变更工单池
     */
    String TITLE_ABAAA_U = "title_abaaa_u";
    /**
     * 变量名
     */
    String TITLE_ABAAA_V = "title_abaaa_v";
    /**
     * 标题
     */
    String TITLE_ABAAA_W = "title_abaaa_w";
    /**
     * 标题简称
     */
    String TITLE_ABAAA_X = "title_abaaa_x";
    /**
     * 标准
     */
    String TITLE_ABAAA_Y = "title_abaaa_y";
    /**
     * 标准任务
     */
    String TITLE_ABAAA_Z = "title_abaaa_z";
    /**
     * 保养总用时</br>（分钟）
     */
    String TITLE_ABAA_B = "title_abaa_b";
    /**
     * 报表
     */
    String TITLE_ABAA_C = "title_abaa_c";
    /**
     * 报废
     */
    String TITLE_ABAA_D = "title_abaa_d";
    /**
     * 报废备件
     */
    String TITLE_ABAA_E = "title_abaa_e";
    /**
     * 报废申请
     */
    String TITLE_ABAA_F = "title_abaa_f";
    /**
     * 报废数量
     */
    String TITLE_ABAA_G = "title_abaa_g";
    /**
     * 报废图片
     */
    String TITLE_ABAA_H = "title_abaa_h";
    /**
     * 报告
     */
    String TITLE_ABAA_I = "title_abaa_i";
    /**
     * 报警
     */
    String TITLE_ABAA_J = "title_abaa_j";
    /**
     * 报警情况
     */
    String TITLE_ABAA_K = "title_abaa_k";
    /**
     * 报警上-下限
     */
    String TITLE_ABAA_L = "title_abaa_l";
    /**
     * 报警上限
     */
    String TITLE_ABAA_M = "title_abaa_m";
    /**
     * 报警时间
     */
    String TITLE_ABAA_N = "title_abaa_n";
    /**
     * 报警下限
     */
    String TITLE_ABAA_O = "title_abaa_o";
    /**
     * 报修方式
     */
    String TITLE_ABAA_P = "title_abaa_p";
    /**
     * 报修图片
     */
    String TITLE_ABAA_Q = "title_abaa_q";
    /**
     * 报修症状简述
     */
    String TITLE_ABAA_R = "title_abaa_r";
    /**
     * 报修助手
     */
    String TITLE_ABAA_S = "title_abaa_s";
    /**
     * 备件
     */
    String TITLE_ABAA_T = "title_abaa_t";
    /**
     * 备件报废
     */
    String TITLE_ABAA_U = "title_abaa_u";
    /**
     * 备件编码
     */
    String TITLE_ABAA_V = "title_abaa_v";
    /**
     * 备件管理
     */
    String TITLE_ABAA_W = "title_abaa_w";
    /**
     * 备件规格
     */
    String TITLE_ABAA_X = "title_abaa_x";
    /**
     * 备件价格
     */
    String TITLE_ABAA_Y = "title_abaa_y";
    /**
     * 备件库存数量小于安全库存数量
     */
    String TITLE_ABAA_Z = "title_abaa_z";
    /**
     * 保养
     */
    String TITLE_ABA_B = "title_aba_b";
    /**
     * 保养备件
     */
    String TITLE_ABA_C = "title_aba_c";
    /**
     * 保养编号
     */
    String TITLE_ABA_D = "title_aba_d";
    /**
     * 保养次数
     */
    String TITLE_ABA_E = "title_aba_e";
    /**
     * 保养单号
     */
    String TITLE_ABA_F = "title_aba_f";
    /**
     * 保养过期
     */
    String TITLE_ABA_G = "title_aba_g";
    /**
     * 保养结果
     */
    String TITLE_ABA_H = "title_aba_h";
    /**
     * 保养人
     */
    String TITLE_ABA_I = "title_aba_i";
    /**
     * 保养人工号
     */
    String TITLE_ABA_J = "title_aba_j";
    /**
     * 保养人员
     */
    String TITLE_ABA_K = "title_aba_k";
    /**
     * 保养时间
     */
    String TITLE_ABA_L = "title_aba_l";
    /**
     * 保养时间（小时）
     */
    String TITLE_ABA_M = "title_aba_m";
    /**
     * 保养时效
     */
    String TITLE_ABA_N = "title_aba_n";
    /**
     * 保养时效（分钟）
     */
    String TITLE_ABA_O = "title_aba_o";
    /**
     * 保养时效</br>占比（%）
     */
    String TITLE_ABA_P = "title_aba_p";
    /**
     * 保养时效占比（%）
     */
    String TITLE_ABA_Q = "title_aba_q";
    /**
     * 保养使用数量
     */
    String TITLE_ABA_R = "title_aba_r";
    /**
     * 保养完成次数
     */
    String TITLE_ABA_S = "title_aba_s";
    /**
     * 保养完成率
     */
    String TITLE_ABA_T = "title_aba_t";
    /**
     * 保养完成总数
     */
    String TITLE_ABA_U = "title_aba_u";
    /**
     * 保养项
     */
    String TITLE_ABA_V = "title_aba_v";
    /**
     * 保养效率
     */
    String TITLE_ABA_W = "title_aba_w";
    /**
     * 保养效率（%）
     */
    String TITLE_ABA_X = "title_aba_x";
    /**
     * 保养信息
     */
    String TITLE_ABA_Y = "title_aba_y";
    /**
     * 保养状态
     */
    String TITLE_ABA_Z = "title_aba_z";
    /**
     * 存在则更新记录
     */
    String TITLE_AB_B = "title_ab_b";
    /**
     * 备件库房
     */
    String TITLE_ABBA_A = "title_abba_a";
    /**
     * 备件来源
     */
    String TITLE_ABBA_B = "title_abba_b";
    /**
     * 备件列表
     */
    String TITLE_ABBA_C = "title_abba_c";
    /**
     * 备件领用
     */
    String TITLE_ABBA_D = "title_abba_d";
    /**
     * 备件领用出库
     */
    String TITLE_ABBA_E = "title_abba_e";
    /**
     * 备件盘点
     */
    String TITLE_ABBA_F = "title_abba_f";
    /**
     * 备件批次
     */
    String TITLE_ABBA_G = "title_abba_g";
    /**
     * 备件清单
     */
    String TITLE_ABBA_H = "title_abba_h";
    /**
     * 备件入库
     */
    String TITLE_ABBA_I = "title_abba_i";
    /**
     * 备件申领结果
     */
    String TITLE_ABBA_J = "title_abba_j";
    /**
     * 备件申领中
     */
    String TITLE_ABBA_K = "title_abba_k";
    /**
     * 备件审核入库
     */
    String TITLE_ABBA_L = "title_abba_l";
    /**
     * 备件使用
     */
    String TITLE_ABBA_M = "title_abba_m";
    /**
     * 备件数量
     */
    String TITLE_ABBA_N = "title_abba_n";
    /**
     * 备件调拨
     */
    String TITLE_ABBA_O = "title_abba_o";
    /**
     * 备件调出确认
     */
    String TITLE_ABBA_P = "title_abba_p";
    /**
     * 备件调出审核
     */
    String TITLE_ABBA_Q = "title_abba_q";
    /**
     * 备件调入确认
     */
    String TITLE_ABBA_R = "title_abba_r";
    /**
     * 备件调入审核
     */
    String TITLE_ABBA_S = "title_abba_s";
    /**
     * 备件图标
     */
    String TITLE_ABBA_T = "title_abba_t";
    /**
     * 备件消耗
     */
    String TITLE_ABBA_U = "title_abba_u";
    /**
     * 备件消耗供应商统计
     */
    String TITLE_ABBA_V = "title_abba_v";
    /**
     * 备件消耗明细与成本统计
     */
    String TITLE_ABBA_W = "title_abba_w";
    /**
     * 备件型号
     */
    String TITLE_ABBA_X = "title_abba_x";
    /**
     * 备件需求
     */
    String TITLE_ABBA_Y = "title_abba_y";
    /**
     * 备件验收入库
     */
    String TITLE_ABBA_Z = "title_abba_z";
    /**
     * 催办工单
     */
    String TITLE_ABBBA_A = "title_abbba_a";
    /**
     * 存储方式
     */
    String TITLE_ABBBA_B = "title_abbba_b";
    /**
     * 存量曲线
     */
    String TITLE_ABBBA_C = "title_abbba_c";
    /**
     * 打勾
     */
    String TITLE_ABBBA_D = "title_abbba_d";
    /**
     * 打卡次数
     */
    String TITLE_ABBBA_E = "title_abbba_e";
    /**
     * 代码
     */
    String TITLE_ABBBA_F = "title_abbba_f";
    /**
     * 待办
     */
    String TITLE_ABBBA_G = "title_abbba_g";
    /**
     * 待办列表
     */
    String TITLE_ABBBA_H = "title_abbba_h";
    /**
     * 待办任务
     */
    String TITLE_ABBBA_I = "title_abbba_i";
    /**
     * 待保养
     */
    String TITLE_ABBBA_J = "title_abbba_j";
    /**
     * 待保养总数
     */
    String TITLE_ABBBA_K = "title_abbba_k";
    /**
     * 待补录组织
     */
    String TITLE_ABBBA_L = "title_abbba_l";
    /**
     * 待处理
     */
    String TITLE_ABBBA_M = "title_abbba_m";
    /**
     * 待分配
     */
    String TITLE_ABBBA_N = "title_abbba_n";
    /**
     * 待归档
     */
    String TITLE_ABBBA_O = "title_abbba_o";
    /**
     * 待接单
     */
    String TITLE_ABBBA_P = "title_abbba_p";
    /**
     * 待盘点
     */
    String TITLE_ABBBA_Q = "title_abbba_q";
    /**
     * 待评价
     */
    String TITLE_ABBBA_R = "title_abbba_r";
    /**
     * 待确认
     */
    String TITLE_ABBBA_S = "title_abbba_s";
    /**
     * 待审核
     */
    String TITLE_ABBBA_T = "title_abbba_t";
    /**
     * 待维修
     */
    String TITLE_ABBBA_U = "title_abbba_u";
    /**
     * 待维修总数
     */
    String TITLE_ABBBA_V = "title_abbba_v";
    /**
     * 单次
     */
    String TITLE_ABBBA_W = "title_abbba_w";
    /**
     * 单次累计
     */
    String TITLE_ABBBA_X = "title_abbba_x";
    /**
     * 单号
     */
    String TITLE_ABBBA_Y = "title_abbba_y";
    /**
     * 单价
     */
    String TITLE_ABBBA_Z = "title_abbba_z";
    /**
     * 超文本
     */
    String TITLE_ABBBB_A = "title_abbbb_a";
    /**
     * 车载电源
     */
    String TITLE_ABBBB_B = "title_abbbb_b";
    /**
     * 撤单
     */
    String TITLE_ABBBB_C = "title_abbbb_c";
    /**
     * 成本中心
     */
    String TITLE_ABBBB_D = "title_abbbb_d";
    /**
     * 程度条
     */
    String TITLE_ABBBB_E = "title_abbbb_e";
    /**
     * 出库
     */
    String TITLE_ABBBB_F = "title_abbbb_f";
    /**
     * 出库量
     */
    String TITLE_ABBBB_G = "title_abbbb_g";
    /**
     * 出库时间
     */
    String TITLE_ABBBB_H = "title_abbbb_h";
    /**
     * 出库图片
     */
    String TITLE_ABBBB_I = "title_abbbb_i";
    /**
     * 初始化加载故障次数
     */
    String TITLE_ABBBB_J = "title_abbbb_j";
    /**
     * 储罐容量
     */
    String TITLE_ABBBB_K = "title_abbbb_k";
    /**
     * 处理
     */
    String TITLE_ABBBB_L = "title_abbbb_l";
    /**
     * 处理方式
     */
    String TITLE_ABBBB_M = "title_abbbb_m";
    /**
     * 处理结果
     */
    String TITLE_ABBBB_N = "title_abbbb_n";
    /**
     * 处理情况
     */
    String TITLE_ABBBB_O = "title_abbbb_o";
    /**
     * 处理人
     */
    String TITLE_ABBBB_P = "title_abbbb_p";
    /**
     * 处理人电话
     */
    String TITLE_ABBBB_Q = "title_abbbb_q";
    /**
     * 处理人角色信息
     */
    String TITLE_ABBBB_R = "title_abbbb_r";
    /**
     * 处理人员
     */
    String TITLE_ABBBB_S = "title_abbbb_s";
    /**
     * 处理图片
     */
    String TITLE_ABBBB_T = "title_abbbb_t";
    /**
     * 处理完成
     */
    String TITLE_ABBBB_U = "title_abbbb_u";
    /**
     * 处理状态
     */
    String TITLE_ABBBB_V = "title_abbbb_v";
    /**
     * 触发条件
     */
    String TITLE_ABBBB_W = "title_abbbb_w";
    /**
     * 创建人
     */
    String TITLE_ABBBB_X = "title_abbbb_x";
    /**
     * 此致
     */
    String TITLE_ABBBB_Y = "title_abbbb_y";
    /**
     * 次数
     */
    String TITLE_ABBBB_Z = "title_abbbb_z";
    /**
     * （数字格式）列号
     */
    String TITLE_AB_C = "title_ab_c";
    /**
     * 页面类型
     */
    String TITLE_ABC = "title_abc";
    /**
     * 01号
     */
    String TITLE_AB_D = "title_ab_d";
    /**
     * 数据来源
     */
    String TITLE_ABD = "title_abd";
    /**
     * 02号
     */
    String TITLE_AB_E = "title_ab_e";
    /**
     * 流程节点
     */
    String TITLE_ABE = "title_abe";
    /**
     * 03号
     */
    String TITLE_AB_F = "title_ab_f";
    /**
     * 委内
     */
    String TITLE_ABF = "title_abf";
    /**
     * 04号
     */
    String TITLE_AB_G = "title_ab_g";
    /**
     * 委外
     */
    String TITLE_ABG = "title_abg";
    /**
     * 05号
     */
    String TITLE_AB_H = "title_ab_h";
    /**
     * 字段属性
     */
    String TITLE_ABH = "title_abh";
    /**
     * 06号
     */
    String TITLE_AB_I = "title_ab_i";
    /**
     * 接口名称
     */
    String TITLE_ABI = "title_abi";
    /**
     * 07号
     */
    String TITLE_AB_J = "title_ab_j";
    /**
     * 详情信息
     */
    String TITLE_ABJ = "title_abj";
    /**
     * 08号
     */
    String TITLE_AB_K = "title_ab_k";
    /**
     * 节点处理人
     */
    String TITLE_ABK = "title_abk";
    /**
     * 09号
     */
    String TITLE_AB_L = "title_ab_l";
    /**
     * 处理角色
     */
    String TITLE_ABL = "title_abl";
    /**
     * 10号
     */
    String TITLE_AB_M = "title_ab_m";
    /**
     * 模版详情
     */
    String TITLE_ABM = "title_abm";
    /**
     * 10月
     */
    String TITLE_AB_N = "title_ab_n";
    /**
     * 工单详情
     */
    String TITLE_ABN = "title_abn";
    /**
     * 11号
     */
    String TITLE_AB_O = "title_ab_o";
    /**
     * 11月
     */
    String TITLE_AB_P = "title_ab_p";
    /**
     * 12号
     */
    String TITLE_AB_Q = "title_ab_q";
    /**
     * 12月
     */
    String TITLE_AB_R = "title_ab_r";
    /**
     * 13号
     */
    String TITLE_AB_S = "title_ab_s";
    /**
     * 14号
     */
    String TITLE_AB_T = "title_ab_t";
    /**
     * 15号
     */
    String TITLE_AB_U = "title_ab_u";
    /**
     * 16号
     */
    String TITLE_AB_V = "title_ab_v";
    /**
     * 17号
     */
    String TITLE_AB_W = "title_ab_w";
    /**
     * 18号
     */
    String TITLE_AB_X = "title_ab_x";
    /**
     * 19号
     */
    String TITLE_AB_Y = "title_ab_y";
    /**
     * 1级
     */
    String TITLE_AB_Z = "title_ab_z";
    /**
     * 工单详情字段联动条件
     */
    String TITLE_AC = "title_ac";
    /**
     * 个人配置
     */
    String TITLE_AC_A = "title_ac_a";
    /**
     * 个人信息
     */
    String TITLE_AC_B = "title_ac_b";
    /**
     * 个人中心
     */
    String TITLE_AC_C = "title_ac_c";
    /**
     * 个月执行一遍
     */
    String TITLE_AC_D = "title_ac_d";
    /**
     * 激活
     */
    String TITLE_ACD = "title_acd";
    /**
     * 跟进人
     */
    String TITLE_AC_E = "title_ac_e";
    /**
     * 周期类型
     */
    String TITLE_ACE = "title_ace";
    /**
     * 更多...
     */
    String TITLE_AC_F = "title_ac_f";
    /**
     * 持续时间类型
     */
    String TITLE_ACF = "title_acf";
    /**
     * 更改图片
     */
    String TITLE_AC_G = "title_ac_g";
    /**
     * 持续时间
     */
    String TITLE_ACG = "title_acg";
    /**
     * 报修
     */
    String TITLE_AC_H = "title_ac_h";
    /**
     * 满足任意条件触发
     */
    String TITLE_ACH = "title_ach";
    /**
     * 工单编号
     */
    String TITLE_AC_I = "title_ac_i";
    /**
     * 满足全部条件触发
     */
    String TITLE_ACI = "title_aci";
    /**
     * 工单变更通知
     */
    String TITLE_AC_J = "title_ac_j";
    /**
     * 主键
     */
    String TITLE_ACJ = "title_acj";
    /**
     * 工单池
     */
    String TITLE_AC_K = "title_ac_k";
    /**
     * 字典管理
     */
    String TITLE_ACK = "title_ack";
    /**
     * 工单池配置
     */
    String TITLE_AC_L = "title_ac_l";
    /**
     * 字典列表
     */
    String TITLE_ACL = "title_acl";
    /**
     * 工单池用户
     */
    String TITLE_AC_M = "title_ac_m";
    /**
     * 数据项类型
     */
    String TITLE_ACM = "title_acm";
    /**
     * 工单处理
     */
    String TITLE_AC_N = "title_ac_n";
    /**
     * 是否翻译
     */
    String TITLE_ACN = "title_acn";
    /**
     * 工单创建通知
     */
    String TITLE_AC_O = "title_ac_o";
    /**
     * 工单费用
     */
    String TITLE_AC_P = "title_ac_p";
    /**
     * 工单分配
     */
    String TITLE_AC_Q = "title_ac_q";
    /**
     * 工单分配模式
     */
    String TITLE_AC_R = "title_ac_r";
    /**
     * 工单管理
     */
    String TITLE_AC_S = "title_ac_s";
    /**
     * 工单过期关闭总数
     */
    String TITLE_AC_T = "title_ac_t";
    /**
     * 操作
     */
    String TITLE_ACTION_A = "title_action_a";
    /**
     * 工单列表
     */
    String TITLE_AC_U = "title_ac_u";
    /**
     * 工单模板
     */
    String TITLE_AC_V = "title_ac_v";
    /**
     * 工单模板编码
     */
    String TITLE_AC_W = "title_ac_w";
    /**
     * 工单请求状态
     */
    String TITLE_AC_X = "title_ac_x";
    /**
     * 工单日历
     */
    String TITLE_AC_Y = "title_ac_y";
    /**
     * 工单调度
     */
    String TITLE_AC_Z = "title_ac_z";
    /**
     * 子页面
     */
    String TITLE_AD = "title_ad";
    /**
     * 服务请求详情模版
     */
    String TITLE_AD_A = "title_ad_a";
    /**
     * 服务人数
     */
    String TITLE_AD_B = "title_ad_b";
    /**
     * 服务商
     */
    String TITLE_AD_C = "title_ad_c";
    /**
     * 服务时间
     */
    String TITLE_AD_D = "title_ad_d";
    /**
     * 服务态度
     */
    String TITLE_AD_E = "title_ad_e";
    /**
     * 流转记录
     */
    String TITLE_ADE = "title_ade";
    /**
     * 服务性质
     */
    String TITLE_AD_F = "title_ad_f";
    /**
     * 流程配置
     */
    String TITLE_ADF = "title_adf";
    /**
     * 服务资源
     */
    String TITLE_AD_G = "title_ad_g";
    /**
     * 工单模版
     */
    String TITLE_ADG = "title_adg";
    /**
     * 父位置
     */
    String TITLE_AD_H = "title_ad_h";
    /**
     * 字段库
     */
    String TITLE_ADH = "title_adh";
    /**
     * 父位置不可为自己
     */
    String TITLE_AD_I = "title_ad_i";
    /**
     * 特殊属性
     */
    String TITLE_ADI = "title_adi";
    /**
     * 负责角色
     */
    String TITLE_AD_J = "title_ad_j";
    /**
     * 数据联动
     */
    String TITLE_ADJ = "title_adj";
    /**
     * 负责人
     */
    String TITLE_AD_K = "title_ad_k";
    /**
     * 等于
     */
    String TITLE_ADK = "title_adk";
    /**
     * 负责员工
     */
    String TITLE_AD_L = "title_ad_l";
    /**
     * 控件名称
     */
    String TITLE_ADL = "title_adl";
    /**
     * 负责组织
     */
    String TITLE_AD_M = "title_ad_m";
    /**
     * 条件
     */
    String TITLE_ADM = "title_adm";
    /**
     * 附件
     */
    String TITLE_AD_N = "title_ad_n";
    /**
     * 显示区域
     */
    String TITLE_ADN = "title_adn";
    /**
     * 复制
     */
    String TITLE_AD_O = "title_ad_o";
    /**
     * 该模式仅支持派单负责人手动分配工单
     */
    String TITLE_AD_P = "title_ad_p";
    /**
     * 该模式同时支持手动派单和抢单
     */
    String TITLE_AD_Q = "title_ad_q";
    /**
     * 改变派工池
     */
    String TITLE_AD_R = "title_ad_r";
    /**
     * 岗位配置
     */
    String TITLE_AD_S = "title_ad_s";
    /**
     * 高
     */
    String TITLE_AD_T = "title_ad_t";
    /**
     * 高精度GPS点项目附件
     */
    String TITLE_AD_U = "title_ad_u";
    /**
     * 个点
     */
    String TITLE_AD_V = "title_ad_v";
    /**
     * 个监测值
     */
    String TITLE_AD_W = "title_ad_w";
    /**
     * 个人
     */
    String TITLE_AD_X = "title_ad_x";
    /**
     * 个人绩效
     */
    String TITLE_AD_Y = "title_ad_y";
    /**
     * 个人领用
     */
    String TITLE_AD_Z = "title_ad_z";
    /**
     * 显示
     */
    String TITLE_AE = "title_ae";
    /**
     * 费用明细
     */
    String TITLE_AE_A = "title_ae_a";
    /**
     * 费用属性
     */
    String TITLE_AE_B = "title_ae_b";
    /**
     * 费用信息
     */
    String TITLE_AE_C = "title_ae_c";
    /**
     * 分
     */
    String TITLE_AE_D = "title_ae_d";
    /**
     * 分拨
     */
    String TITLE_AE_E = "title_ae_e";
    /**
     * 分拨（大区、分拨）
     */
    String TITLE_AE_F = "title_ae_f";
    /**
     * 写入/只读
     */
    String TITLE_AEF = "title_aef";
    /**
     * 分派信息
     */
    String TITLE_AE_G = "title_ae_g";
    /**
     * 节点处理类型
     */
    String TITLE_AEG = "title_aeg";
    /**
     * 分配
     */
    String TITLE_AE_H = "title_ae_h";
    /**
     * 模版
     */
    String TITLE_AEH = "title_aeh";
    /**
     * 分配编号
     */
    String TITLE_AE_I = "title_ae_i";
    /**
     * 流程类型
     */
    String TITLE_AEI = "title_aei";
    /**
     * 分配短信提醒分钟
     */
    String TITLE_AE_J = "title_ae_j";
    /**
     * 流程标题
     */
    String TITLE_AEJ = "title_aej";
    /**
     * 分配人
     */
    String TITLE_AE_K = "title_ae_k";
    /**
     * 工作环节
     */
    String TITLE_AEK = "title_aek";
    /**
     * 分配人员
     */
    String TITLE_AE_L = "title_ae_l";
    /**
     * 当前处理人
     */
    String TITLE_AEL = "title_ael";
    /**
     * 分配任务提交
     */
    String TITLE_AE_M = "title_ae_m";
    /**
     * 发起时间
     */
    String TITLE_AEM = "title_aem";
    /**
     * 分配时间
     */
    String TITLE_AE_N = "title_ae_n";
    /**
     * 当前状态
     */
    String TITLE_AEN = "title_aen";
    /**
     * 分配时效
     */
    String TITLE_AE_O = "title_ae_o";
    /**
     * 分配维修任务
     */
    String TITLE_AE_P = "title_ae_p";
    /**
     * 分配信息
     */
    String TITLE_AE_Q = "title_ae_q";
    /**
     * 分配主管
     */
    String TITLE_AE_R = "title_ae_r";
    /**
     * 分钟
     */
    String TITLE_AE_S = "title_ae_s";
    /**
     * 风险评估
     */
    String TITLE_AE_T = "title_ae_t";
    /**
     * 否
     */
    String TITLE_AE_U = "title_ae_u";
    /**
     * 服务报告号
     */
    String TITLE_AE_V = "title_ae_v";
    /**
     * 服务单号
     */
    String TITLE_AE_W = "title_ae_w";
    /**
     * 服务内容
     */
    String TITLE_AE_X = "title_ae_x";
    /**
     * 服务评分
     */
    String TITLE_AE_Y = "title_ae_y";
    /**
     * 服务请求
     */
    String TITLE_AE_Z = "title_ae_z";
    /**
     * 执行人
     */
    String TITLE_AF = "title_af";
    /**
     * 计划保养总数
     */
    String TITLE_AF_A = "title_af_a";
    /**
     * 计划触发条件
     */
    String TITLE_AF_B = "title_af_b";
    /**
     * 计划点检总数
     */
    String TITLE_AF_C = "title_af_c";
    /**
     * 计划内容
     */
    String TITLE_AF_D = "title_af_d";
    /**
     * 计划任务
     */
    String TITLE_AF_E = "title_af_e";
    /**
     * 计划信息
     */
    String TITLE_AF_F = "title_af_f";
    /**
     * 计划性维修
     */
    String TITLE_AF_G = "title_af_g";
    /**
     * 开工时间
     */
    String TITLE_AFG = "title_afg";
    /**
     * 计划巡检总数
     */
    String TITLE_AF_H = "title_af_h";
    /**
     * 类型备注
     */
    String TITLE_AFH = "title_afh";
    /**
     * 计划有效期
     */
    String TITLE_AF_I = "title_af_i";
    /**
     * 字典类型
     */
    String TITLE_AFI = "title_afi";
    /**
     * 计划在岗
     */
    String TITLE_AF_J = "title_af_j";
    /**
     * 解决措施
     */
    String TITLE_AFJ = "title_afj";
    /**
     * 计划总工时（h）
     */
    String TITLE_AF_K = "title_af_k";
    /**
     * 维修工
     */
    String TITLE_AFK = "title_afk";
    /**
     * 计划总数
     */
    String TITLE_AF_L = "title_af_l";
    /**
     * 支持人员
     */
    String TITLE_AFL = "title_afl";
    /**
     * 计数
     */
    String TITLE_AF_M = "title_af_m";
    /**
     * 故障原因描述
     */
    String TITLE_AFM = "title_afm";
    /**
     * 记录间隔（s）
     */
    String TITLE_AF_N = "title_af_n";
    /**
     * 解决措施
     */
    String TITLE_AFN = "title_afn";
    /**
     * 记录时间
     */
    String TITLE_AF_O = "title_af_o";
    /**
     * 记录值
     */
    String TITLE_AF_P = "title_af_p";
    /**
     * 技能之星
     */
    String TITLE_AF_Q = "title_af_q";
    /**
     * 技术参数
     */
    String TITLE_AF_R = "title_af_r";
    /**
     * 技术支持:苏州耕耘无忧物联科技有限公司
     */
    String TITLE_AF_S = "title_af_s";
    /**
     * 绩效报告
     */
    String TITLE_AF_T = "title_af_t";
    /**
     * 绩效查询
     */
    String TITLE_AF_U = "title_af_u";
    /**
     * 绩效得分
     */
    String TITLE_AF_V = "title_af_v";
    /**
     * 绩效模板
     */
    String TITLE_AF_W = "title_af_w";
    /**
     * 绩效评分
     */
    String TITLE_AF_X = "title_af_x";
    /**
     * 加注
     */
    String TITLE_AF_Y = "title_af_y";
    /**
     * 价格
     */
    String TITLE_AF_Z = "title_af_z";
    /**
     * 来源方式
     */
    String TITLE_AG = "title_ag";
    /**
     * 滚筒故障次数
     */
    String TITLE_AG_A = "title_ag_a";
    /**
     * 滚筒故障率
     */
    String TITLE_AG_B = "title_ag_b";
    /**
     * 滚筒故障总时间（分钟）
     */
    String TITLE_AG_C = "title_ag_c";
    /**
     * 国家
     */
    String TITLE_AG_D = "title_ag_d";
    /**
     * 过期
     */
    String TITLE_AG_E = "title_ag_e";
    /**
     * 还原
     */
    String TITLE_AG_F = "title_ag_f";
    /**
     * 行号
     */
    String TITLE_AG_G = "title_ag_g";
    /**
     * 耗材
     */
    String TITLE_AG_H = "title_ag_h";
    /**
     * 解决措施描述
     */
    String TITLE_AGH = "title_agh";
    /**
     * 合格
     */
    String TITLE_AG_I = "title_ag_i";
    /**
     * 资源费用
     */
    String TITLE_AGI = "title_agi";
    /**
     * 合格率（%）
     */
    String TITLE_AG_J = "title_ag_j";
    /**
     * 需申请支持
     */
    String TITLE_AGJ = "title_agj";
    /**
     * 合计
     */
    String TITLE_AG_K = "title_ag_k";
    /**
     * 技术人员
     */
    String TITLE_AGK = "title_agk";
    /**
     * 合同
     */
    String TITLE_AG_L = "title_ag_l";
    /**
     * 产线班长
     */
    String TITLE_AGL = "title_agl";
    /**
     * 合同范围
     */
    String TITLE_AG_M = "title_ag_m";
    /**
     * 下一步操作人
     */
    String TITLE_AGM = "title_agm";
    /**
     * 忽略
     */
    String TITLE_AG_N = "title_ag_n";
    /**
     * 指定维修人员
     */
    String TITLE_AGN = "title_agn";
    /**
     * 欢迎
     */
    String TITLE_AG_O = "title_ag_o";
    /**
     * 欢迎登录
     */
    String TITLE_AG_P = "title_ag_p";
    /**
     * 回收
     */
    String TITLE_AG_Q = "title_ag_q";
    /**
     * 活动
     */
    String TITLE_AG_R = "title_ag_r";
    /**
     * 货币
     */
    String TITLE_AG_S = "title_ag_s";
    /**
     * 货币单位
     */
    String TITLE_AG_T = "title_ag_t";
    /**
     * 基于字段显示
     */
    String TITLE_AG_U = "title_ag_u";
    /**
     * 级别
     */
    String TITLE_AG_V = "title_ag_v";
    /**
     * 级联
     */
    String TITLE_AG_W = "title_ag_w";
    /**
     * 急停
     */
    String TITLE_AG_X = "title_ag_x";
    /**
     * 计划
     */
    String TITLE_AG_Y = "title_ag_y";
    /**
     * 计划保养
     */
    String TITLE_AG_Z = "title_ag_z";
    /**
     * 界面内容
     */
    String TITLE_AH = "title_ah";
    /**
     * 故障率
     */
    String TITLE_AH_A = "title_ah_a";
    /**
     * 故障率（%）
     */
    String TITLE_AH_B = "title_ah_b";
    /**
     * 故障明细
     */
    String TITLE_AH_C = "title_ah_c";
    /**
     * 故障停机
     */
    String TITLE_AH_D = "title_ah_d";
    /**
     * 故障图片
     */
    String TITLE_AH_E = "title_ah_e";
    /**
     * 故障信息
     */
    String TITLE_AH_F = "title_ah_f";
    /**
     * 故障原因
     */
    String TITLE_AH_G = "title_ah_g";
    /**
     * 故障照片
     */
    String TITLE_AH_H = "title_ah_h";
    /**
     * 故障综合分析
     */
    String TITLE_AH_I = "title_ah_i";
    /**
     * 费用项
     */
    String TITLE_AHI = "title_ahi";
    /**
     * 挂起
     */
    String TITLE_AH_J = "title_ah_j";
    /**
     * 工单流程分支字典
     */
    String TITLE_AHJ = "title_ahj";
    /**
     * 关闭
     */
    String TITLE_AH_K = "title_ah_k";
    /**
     * 不指定人员
     */
    String TITLE_AHK = "title_ahk";
    /**
     * 关单
     */
    String TITLE_AH_L = "title_ah_l";
    /**
     * 指定确认人员
     */
    String TITLE_AHL = "title_ahl";
    /**
     * 关键词
     */
    String TITLE_AH_M = "title_ah_m";
    /**
     * 指定分配人员
     */
    String TITLE_AHM = "title_ahm";
    /**
     * 关联备件
     */
    String TITLE_AH_N = "title_ah_n";
    /**
     * 指定处理人员
     */
    String TITLE_AHN = "title_ahn";
    /**
     * 关联厂商
     */
    String TITLE_AH_O = "title_ah_o";
    /**
     * 关联角色
     */
    String TITLE_AH_P = "title_ah_p";
    /**
     * 关联企业
     */
    String TITLE_AH_Q = "title_ah_q";
    /**
     * 关联人员
     */
    String TITLE_AH_R = "title_ah_r";
    /**
     * 关联位置
     */
    String TITLE_AH_S = "title_ah_s";
    /**
     * 关联字段
     */
    String TITLE_AH_T = "title_ah_t";
    /**
     * 关联组织
     */
    String TITLE_AH_U = "title_ah_u";
    /**
     * 关于我们
     */
    String TITLE_AH_V = "title_ah_v";
    /**
     * 管理员
     */
    String TITLE_AH_W = "title_ah_w";
    /**
     * 规格参数
     */
    String TITLE_AH_X = "title_ah_x";
    /**
     * 规格型号
     */
    String TITLE_AH_Y = "title_ah_y";
    /**
     * 滚筒故障
     */
    String TITLE_AH_Z = "title_ah_z";
    /**
     * 接口数据
     */
    String TITLE_AI = "title_ai";
    /**
     * 工作日
     */
    String TITLE_AI_A = "title_ai_a";
    /**
     * 工作时间
     */
    String TITLE_AI_B = "title_ai_b";
    /**
     * 工作时长
     */
    String TITLE_AI_C = "title_ai_c";
    /**
     * 工作台
     */
    String TITLE_AI_D = "title_ai_d";
    /**
     * 工作要求
     */
    String TITLE_AI_E = "title_ai_e";
    /**
     * 工作优先级
     */
    String TITLE_AI_F = "title_ai_f";
    /**
     * 工作助手
     */
    String TITLE_AI_G = "title_ai_g";
    /**
     * 工作准备
     */
    String TITLE_AI_H = "title_ai_h";
    /**
     * 公告
     */
    String TITLE_AI_I = "title_ai_i";
    /**
     * 公告管理
     */
    String TITLE_AI_J = "title_ai_j";
    /**
     * 指定审核人员
     */
    String TITLE_AIJ = "title_aij";
    /**
     * 公告时间
     */
    String TITLE_AI_K = "title_ai_k";
    /**
     * 指定支援人员
     */
    String TITLE_AIK = "title_aik";
    /**
     * 公共模板
     */
    String TITLE_AI_L = "title_ai_l";
    /**
     * 微信号
     */
    String TITLE_AIL = "title_ail";
    /**
     * 公式
     */
    String TITLE_AI_M = "title_ai_m";
    /**
     * 公众号
     */
    String TITLE_AIM = "title_aim";
    /**
     * 公司选择
     */
    String TITLE_AI_N = "title_ai_n";
    /**
     * 默认展示
     */
    String TITLE_AIN = "title_ain";
    /**
     * 功率
     */
    String TITLE_AI_O = "title_ai_o";
    /**
     * 功率（KW）
     */
    String TITLE_AI_P = "title_ai_p";
    /**
     * 功能
     */
    String TITLE_AI_Q = "title_ai_q";
    /**
     * 供应商ID号
     */
    String TITLE_AI_R = "title_ai_r";
    /**
     * 购买时间
     */
    String TITLE_AI_S = "title_ai_s";
    /**
     * 谷歌
     */
    String TITLE_AI_T = "title_ai_t";
    /**
     * 固资
     */
    String TITLE_AI_U = "title_ai_u";
    /**
     * 故障
     */
    String TITLE_AI_V = "title_ai_v";
    /**
     * 故障产线
     */
    String TITLE_AI_W = "title_ai_w";
    /**
     * 故障处理能力（分钟）
     */
    String TITLE_AI_X = "title_ai_x";
    /**
     * 故障次数
     */
    String TITLE_AI_Y = "title_ai_y";
    /**
     * 故障工时
     */
    String TITLE_AI_Z = "title_ai_z";
    /**
     * 自定义
     */
    String TITLE_AJ = "title_aj";
    /**
     * 可以
     */
    String TITLE_AJ_A = "title_aj_a";
    /**
     * 客服电话
     */
    String TITLE_AJ_B = "title_aj_b";
    /**
     * 客服工程师
     */
    String TITLE_AJ_C = "title_aj_c";
    /**
     * 客观平均分
     */
    String TITLE_AJ_D = "title_aj_d";
    /**
     * 客观评分
     */
    String TITLE_AJ_E = "title_aj_e";
    /**
     * 客观指标
     */
    String TITLE_AJ_F = "title_aj_f";
    /**
     * 客户
     */
    String TITLE_AJ_G = "title_aj_g";
    /**
     * 客户对本次维护保养意见与建议
     */
    String TITLE_AJ_H = "title_aj_h";
    /**
     * 客户联系人
     */
    String TITLE_AJ_I = "title_aj_i";
    /**
     * 客户用户
     */
    String TITLE_AJ_J = "title_aj_j";
    /**
     * 客户准备
     */
    String TITLE_AJ_K = "title_aj_k";
    /**
     * 重复
     */
    String TITLE_AJK = "title_ajk";
    /**
     * 库存
     */
    String TITLE_AJ_L = "title_aj_l";
    /**
     * 在
     */
    String TITLE_AJL = "title_ajl";
    /**
     * 库存编号
     */
    String TITLE_AJ_M = "title_aj_m";
    /**
     * 第一
     */
    String TITLE_AJM = "title_ajm";
    /**
     * 库存数量
     */
    String TITLE_AJ_N = "title_aj_n";
    /**
     * 第二
     */
    String TITLE_AJN = "title_ajn";
    /**
     * 库存消耗
     */
    String TITLE_AJ_O = "title_aj_o";
    /**
     * 库存预警短信提醒
     */
    String TITLE_AJ_P = "title_aj_p";
    /**
     * 库房
     */
    String TITLE_AJ_Q = "title_aj_q";
    /**
     * 库房备件
     */
    String TITLE_AJ_R = "title_aj_r";
    /**
     * 库房编码
     */
    String TITLE_AJ_S = "title_aj_s";
    /**
     * 库房管理
     */
    String TITLE_AJ_T = "title_aj_t";
    /**
     * 库房管理人
     */
    String TITLE_AJ_U = "title_aj_u";
    /**
     * 库房号
     */
    String TITLE_AJ_V = "title_aj_v";
    /**
     * 库房列表
     */
    String TITLE_AJ_W = "title_aj_w";
    /**
     * 库房维护
     */
    String TITLE_AJ_X = "title_aj_x";
    /**
     * 亏损
     */
    String TITLE_AJ_Y = "title_aj_y";
    /**
     * 来源单号
     */
    String TITLE_AJ_Z = "title_aj_z";
    /**
     * 节点ID
     */
    String TITLE_AK = "title_ak";
    /**
     * 旧密码
     */
    String TITLE_AK_A = "title_ak_a";
    /**
     * 拒绝
     */
    String TITLE_AK_B = "title_ak_b";
    /**
     * 均故障间隔（小时）
     */
    String TITLE_AK_C = "title_ak_c";
    /**
     * 开关量
     */
    String TITLE_AK_D = "title_ak_d";
    /**
     * 开关量为0或1
     */
    String TITLE_AK_E = "title_ak_e";
    /**
     * 开启
     */
    String TITLE_AK_F = "title_ak_f";
    /**
     * 开始保养时间
     */
    String TITLE_AK_G = "title_ak_g";
    /**
     * 开始时间
     */
    String TITLE_AK_H = "title_ak_h";
    /**
     * 开始时间必须小于结束时间
     */
    String TITLE_AK_I = "title_ak_i";
    /**
     * 开始维修
     */
    String TITLE_AK_J = "title_ak_j";
    /**
     * 看板
     */
    String TITLE_AK_K = "title_ak_k";
    /**
     * 看板指标
     */
    String TITLE_AK_L = "title_ak_l";
    /**
     * 第三
     */
    String TITLE_AKL = "title_akl";
    /**
     * 考核标准
     */
    String TITLE_AK_M = "title_ak_m";
    /**
     * 第四
     */
    String TITLE_AKM = "title_akm";
    /**
     * 考核对象
     */
    String TITLE_AK_N = "title_ak_n";
    /**
     * 最后一
     */
    String TITLE_AKN = "title_akn";
    /**
     * 考核分
     */
    String TITLE_AK_O = "title_ak_o";
    /**
     * 考核计划
     */
    String TITLE_AK_P = "title_ak_p";
    /**
     * 考核模板
     */
    String TITLE_AK_Q = "title_ak_q";
    /**
     * 考核人数
     */
    String TITLE_AK_R = "title_ak_r";
    /**
     * 考核维度
     */
    String TITLE_AK_S = "title_ak_s";
    /**
     * 考核项
     */
    String TITLE_AK_T = "title_ak_t";
    /**
     * 考核项配置
     */
    String TITLE_AK_U = "title_ak_u";
    /**
     * 考核月份
     */
    String TITLE_AK_V = "title_ak_v";
    /**
     * 考核周期
     */
    String TITLE_AK_W = "title_ak_w";
    /**
     * 考勤签到
     */
    String TITLE_AK_X = "title_ak_x";
    /**
     * 考勤日
     */
    String TITLE_AK_Y = "title_ak_y";
    /**
     * 可编辑列表
     */
    String TITLE_AK_Z = "title_ak_z";
    /**
     * 模版名称
     */
    String TITLE_AL = "title_al";
    /**
     * 角色权限
     */
    String TITLE_AL_A = "title_al_a";
    /**
     * 接单
     */
    String TITLE_AL_B = "title_al_b";
    /**
     * 接收人
     */
    String TITLE_AL_C = "title_al_c";
    /**
     * 接受短信提醒分钟
     */
    String TITLE_AL_D = "title_al_d";
    /**
     * 接受时间
     */
    String TITLE_AL_E = "title_al_e";
    /**
     * 接受图片
     */
    String TITLE_AL_F = "title_al_f";
    /**
     * 街道
     */
    String TITLE_AL_G = "title_al_g";
    /**
     * 结果
     */
    String TITLE_AL_H = "title_al_h";
    /**
     * 结果描述
     */
    String TITLE_AL_I = "title_al_i";
    /**
     * 结果照片
     */
    String TITLE_AL_J = "title_al_j";
    /**
     * 结束时间
     */
    String TITLE_AL_K = "title_al_k";
    /**
     * 结算
     */
    String TITLE_AL_L = "title_al_l";
    /**
     * 结算价格
     */
    String TITLE_AL_M = "title_al_m";
    /**
     * 自动生产工单
     */
    String TITLE_ALM = "title_alm";
    /**
     * 截止交付时间
     */
    String TITLE_AL_N = "title_al_n";
    /**
     * 生成
     */
    String TITLE_ALN = "title_aln";
    /**
     * 截止时间
     */
    String TITLE_AL_O = "title_al_o";
    /**
     * 解决方案
     */
    String TITLE_AL_P = "title_al_p";
    /**
     * 今日
     */
    String TITLE_AL_Q = "title_al_q";
    /**
     * 今日上报
     */
    String TITLE_AL_R = "title_al_r";
    /**
     * 今日完成
     */
    String TITLE_AL_S = "title_al_s";
    /**
     * 今日未签到
     */
    String TITLE_AL_T = "title_al_t";
    /**
     * 金额
     */
    String TITLE_AL_U = "title_al_u";
    /**
     * 紧急程度
     */
    String TITLE_AL_V = "title_al_v";
    /**
     * 进一步行动必要性
     */
    String TITLE_AL_W = "title_al_w";
    /**
     * 禁用
     */
    String TITLE_AL_X = "title_al_x";
    /**
     * 经销商
     */
    String TITLE_AL_Y = "title_al_y";
    /**
     * 警告
     */
    String TITLE_AL_Z = "title_al_z";
    /**
     * 模版描述
     */
    String TITLE_AM = "title_am";
    /**
     * 监测值
     */
    String TITLE_AM_A = "title_am_a";
    /**
     * 监控大屏切换时间（秒）
     */
    String TITLE_AM_B = "title_am_b";
    /**
     * 监控键后缀
     */
    String TITLE_AM_C = "title_am_c";
    /**
     * 监控列表
     */
    String TITLE_AM_D = "title_am_d";
    /**
     * 监控配置
     */
    String TITLE_AM_E = "title_am_e";
    /**
     * 监控区域
     */
    String TITLE_AM_F = "title_am_f";
    /**
     * 监控图
     */
    String TITLE_AM_G = "title_am_g";
    /**
     * 监控图标
     */
    String TITLE_AM_H = "title_am_h";
    /**
     * 监控项
     */
    String TITLE_AM_I = "title_am_i";
    /**
     * 监控项报警
     */
    String TITLE_AM_J = "title_am_j";
    /**
     * 监控项名
     */
    String TITLE_AM_K = "title_am_k";
    /**
     * 监控项异常
     */
    String TITLE_AM_L = "title_am_l";
    /**
     * 监控值
     */
    String TITLE_AM_M = "title_am_m";
    /**
     * 检查
     */
    String TITLE_AM_N = "title_am_n";
    /**
     * 不生成
     */
    String TITLE_AMN = "title_amn";
    /**
     * 检查方法
     */
    String TITLE_AM_O = "title_am_o";
    /**
     * 简称
     */
    String TITLE_AM_P = "title_am_p";
    /**
     * 简单任务
     */
    String TITLE_AM_Q = "title_am_q";
    /**
     * 键值
     */
    String TITLE_AM_R = "title_am_r";
    /**
     * 将就下列系统或其相应组成部件提供服务
     */
    String TITLE_AM_S = "title_am_s";
    /**
     * 交叉带故障率
     */
    String TITLE_AM_T = "title_am_t";
    /**
     * 交叉带故障率（%）
     */
    String TITLE_AM_U = "title_am_u";
    /**
     * 交付时间
     */
    String TITLE_AM_V = "title_am_v";
    /**
     * 交付物
     */
    String TITLE_AM_W = "title_am_w";
    /**
     * 交付物模板
     */
    String TITLE_AM_X = "title_am_x";
    /**
     * 角色
     */
    String TITLE_AM_Y = "title_am_y";
    /**
     * 角色管理
     */
    String TITLE_AM_Z = "title_am_z";
    /**
     * 节点类型
     */
    String TITLE_AN = "title_an";
    /**
     * 内嵌网页
     */
    String TITLE_AN_A = "title_an_a";
    /**
     * 内容
     */
    String TITLE_AN_B = "title_an_b";
    /**
     * 内容明细
     */
    String TITLE_AN_C = "title_an_c";
    /**
     * 年
     */
    String TITLE_AN_D = "title_an_d";
    /**
     * 年报表
     */
    String TITLE_AN_E = "title_an_e";
    /**
     * 年度
     */
    String TITLE_AN_F = "title_an_f";
    /**
     * 年计划
     */
    String TITLE_AN_G = "title_an_g";
    /**
     * 年累计
     */
    String TITLE_AN_H = "title_an_h";
    /**
     * 拍照
     */
    String TITLE_AN_I = "title_an_i";
    /**
     * 排班
     */
    String TITLE_AN_J = "title_an_j";
    /**
     * 排班维护
     */
    String TITLE_AN_K = "title_an_k";
    /**
     * 排序
     */
    String TITLE_AN_L = "title_an_l";
    /**
     * 派单
     */
    String TITLE_AN_M = "title_an_m";
    /**
     * 派单模式
     */
    String TITLE_AN_N = "title_an_n";
    /**
     * 派件信息
     */
    String TITLE_AN_O = "title_an_o";
    /**
     * 盘点
     */
    String TITLE_AN_P = "title_an_p";
    /**
     * 盘点编码
     */
    String TITLE_AN_Q = "title_an_q";
    /**
     * 盘点处理
     */
    String TITLE_AN_R = "title_an_r";
    /**
     * 盘点分拨
     */
    String TITLE_AN_S = "title_an_s";
    /**
     * 盘点分配
     */
    String TITLE_AN_T = "title_an_t";
    /**
     * 盘点库房
     */
    String TITLE_AN_U = "title_an_u";
    /**
     * 盘点人员
     */
    String TITLE_AN_V = "title_an_v";
    /**
     * 盘点时间
     */
    String TITLE_AN_W = "title_an_w";
    /**
     * 盘点数量
     */
    String TITLE_AN_X = "title_an_x";
    /**
     * 盘点完成
     */
    String TITLE_AN_Y = "title_an_y";
    /**
     * 盘点位置
     */
    String TITLE_AN_Z = "title_an_z";
    /**
     * 设备名称
     */
    String TITLE_ASSET_A = "title_asset_a";
    /**
     * 盘点设备总数
     */
    String TITLE_ASSET_AA = "title_asset_aa";
    /**
     * 扫码取设备
     */
    String TITLE_ASSET_AB = "title_asset_ab";
    /**
     * 设备盘点
     */
    String TITLE_ASSET_AB_A = "title_asset_ab_a";
    /**
     * 设备盘点时报废
     */
    String TITLE_ASSET_AB_B = "title_asset_ab_b";
    /**
     * 设备启用禁用通知
     */
    String TITLE_ASSET_AB_C = "title_asset_ab_c";
    /**
     * 设备入库
     */
    String TITLE_ASSET_AB_D = "title_asset_ab_d";
    /**
     * 设备使用时间
     */
    String TITLE_ASSET_AB_E = "title_asset_ab_e";
    /**
     * 设备数量
     */
    String TITLE_ASSET_AB_F = "title_asset_ab_f";
    /**
     * 设备调拨
     */
    String TITLE_ASSET_AB_G = "title_asset_ab_g";
    /**
     * 设备调度报废
     */
    String TITLE_ASSET_AB_H = "title_asset_ab_h";
    /**
     * 设备调整
     */
    String TITLE_ASSET_AB_I = "title_asset_ab_i";
    /**
     * 设备统计
     */
    String TITLE_ASSET_AB_J = "title_asset_ab_j";
    /**
     * 设备图标
     */
    String TITLE_ASSET_AB_K = "title_asset_ab_k";
    /**
     * 设备维护
     */
    String TITLE_ASSET_AB_L = "title_asset_ab_l";
    /**
     * 设备维护专用
     */
    String TITLE_ASSET_AB_N = "title_asset_ab_n";
    /**
     * 设备维修效率分析
     */
    String TITLE_ASSET_AB_O = "title_asset_ab_o";
    /**
     * 设备位置（带数据权限控制）
     */
    String TITLE_ASSET_AB_P = "title_asset_ab_p";
    /**
     * 设备位置（权限）
     */
    String TITLE_ASSET_AB_Q = "title_asset_ab_q";
    /**
     * 设备详情动态监控（小时）
     */
    String TITLE_ASSET_AB_R = "title_asset_ab_r";
    /**
     * 设备详细
     */
    String TITLE_ASSET_AB_S = "title_asset_ab_s";
    /**
     * 设备盈亏
     */
    String TITLE_ASSET_AB_T = "title_asset_ab_t";
    /**
     * 设备用量
     */
    String TITLE_ASSET_AB_U = "title_asset_ab_u";
    /**
     * 设备照片
     */
    String TITLE_ASSET_AB_V = "title_asset_ab_v";
    /**
     * 设备主管
     */
    String TITLE_ASSET_AB_W = "title_asset_ab_w";
    /**
     * 生产主管
     */
    String TITLE_ASSET_AB_X = "title_asset_ab_x";
    /**
     * 设备状况
     */
    String TITLE_ASSET_AB_Y = "title_asset_ab_y";
    /**
     * 设备状态
     */
    String TITLE_ASSET_AB_Z = "title_asset_ab_z";
    /**
     * 设备保修选择
     */
    String TITLE_ASSET_AC = "title_asset_ac";
    /**
     * 设备资产
     */
    String TITLE_ASSET_AC_A = "title_asset_ac_a";
    /**
     * 设备总数
     */
    String TITLE_ASSET_AC_B = "title_asset_ac_b";
    /**
     * 设备组织
     */
    String TITLE_ASSET_AC_C = "title_asset_ac_c";
    /**
     * 实验室设备利用率（月）
     */
    String TITLE_ASSET_AC_D = "title_asset_ac_d";
    /**
     * 所属设备
     */
    String TITLE_ASSET_AC_E = "title_asset_ac_e";
    /**
     * 所有设备
     */
    String TITLE_ASSET_AC_F = "title_asset_ac_f";
    /**
     * 调拨设备
     */
    String TITLE_ASSET_AC_G = "title_asset_ac_g";
    /**
     * 调出设备
     */
    String TITLE_ASSET_AC_H = "title_asset_ac_h";
    /**
     * 调入设备
     */
    String TITLE_ASSET_AC_I = "title_asset_ac_i";
    /**
     * 维保设备
     */
    String TITLE_ASSET_AC_J = "title_asset_ac_j";
    /**
     * 维护设备厂商
     */
    String TITLE_ASSET_AC_K = "title_asset_ac_k";
    /**
     * 维修提交支持扫码设备
     */
    String TITLE_ASSET_AC_L = "title_asset_ac_l";
    /**
     * 新自动线设备安装调试
     */
    String TITLE_ASSET_AC_M = "title_asset_ac_m";
    /**
     * 选择设备分布图
     */
    String TITLE_ASSET_AC_N = "title_asset_ac_n";
    /**
     * 选择适用设备对象
     */
    String TITLE_ASSET_AC_O = "title_asset_ac_o";
    /**
     * 选中设备
     */
    String TITLE_ASSET_AC_P = "title_asset_ac_p";
    /**
     * 巡检设备
     */
    String TITLE_ASSET_AC_Q = "title_asset_ac_q";
    /**
     * 一般设备数量
     */
    String TITLE_ASSET_AC_R = "title_asset_ac_r";
    /**
     * 移动端保养提交支持扫码设备
     */
    String TITLE_ASSET_AC_S = "title_asset_ac_s";
    /**
     * 移动端维修支持选择设备
     */
    String TITLE_ASSET_AC_T = "title_asset_ac_t";
    /**
     * 异常设备
     */
    String TITLE_ASSET_AC_U = "title_asset_ac_u";
    /**
     * 重点设备数量
     */
    String TITLE_ASSET_AC_V = "title_asset_ac_v";
    /**
     * 子设备
     */
    String TITLE_ASSET_AC_W = "title_asset_ac_w";
    /**
     * 设备报废
     */
    String TITLE_ASSET_AD = "title_asset_ad";
    /**
     * 设备编码
     */
    String TITLE_ASSET_AE = "title_asset_ae";
    /**
     * 设备编码（新）
     */
    String TITLE_ASSET_AF = "title_asset_af";
    /**
     * 设备别名
     */
    String TITLE_ASSET_AG = "title_asset_ag";
    /**
     * 设备不支持物联
     */
    String TITLE_ASSET_AH = "title_asset_ah";
    /**
     * 设备部件
     */
    String TITLE_ASSET_AI = "title_asset_ai";
    /**
     * 设备代码
     */
    String TITLE_ASSET_AJ = "title_asset_aj";
    /**
     * 设备地图
     */
    String TITLE_ASSET_AK = "title_asset_ak";
    /**
     * 设备告警
     */
    String TITLE_ASSET_AL = "title_asset_al";
    /**
     * 设备故障
     */
    String TITLE_ASSET_AM = "title_asset_am";
    /**
     * 设备故障类型
     */
    String TITLE_ASSET_AN = "title_asset_an";
    /**
     * 设备故障率
     */
    String TITLE_ASSET_AO = "title_asset_ao";
    /**
     * 设备故障台数
     */
    String TITLE_ASSET_AP = "title_asset_ap";
    /**
     * 设备管理
     */
    String TITLE_ASSET_AQ = "title_asset_aq";
    /**
     * 设备或其部件保养
     */
    String TITLE_ASSET_AR = "title_asset_ar";
    /**
     * 设备级别
     */
    String TITLE_ASSET_AS = "title_asset_as";
    /**
     * 设备监测
     */
    String TITLE_ASSET_AT = "title_asset_at";
    /**
     * 设备监控
     */
    String TITLE_ASSET_AU = "title_asset_au";
    /**
     * 设备健康监测
     */
    String TITLE_ASSET_AV = "title_asset_av";
    /**
     * 设备来源
     */
    String TITLE_ASSET_AW = "title_asset_aw";
    /**
     * 设备列表
     */
    String TITLE_ASSET_AX = "title_asset_ax";
    /**
     * 设备模板列表
     */
    String TITLE_ASSET_AY = "title_asset_ay";
    /**
     * 设备模版
     */
    String TITLE_ASSET_AZ = "title_asset_az";
    /**
     * 设备运行状态
     */
    String TITLE_ASSET_BA = "title_asset_ba";
    /**
     * 设备类型
     */
    String TITLE_ASSET_C = "title_asset_c";
    /**
     * 设备型号
     */
    String TITLE_ASSET_D = "title_asset_d";
    /**
     * {0}信息
     */
    String TITLE_ASSET_E = "title_asset_e";
    /**
     * VMI设备
     */
    String TITLE_ASSET_F = "title_asset_f";
    /**
     * 设备位置
     */
    String TITLE_ASSET_G = "title_asset_g";
    /**
     * 未缴费用
     * */
    String TITLE_ACFG = "title_acfg";
    /**
     * 设备图片
     */
    String TITLE_ASSET_H = "title_asset_h";
    /**
     * 设备
     */
    String TITLE_ASSET_I = "title_asset_i";
    /**
     * 百台设备维修次数TOP20
     */
    String TITLE_ASSET_J = "title_asset_j";
    /**
     * 保养设备
     */
    String TITLE_ASSET_K = "title_asset_k";
    /**
     * 报废设备
     */
    String TITLE_ASSET_L = "title_asset_l";
    /**
     * 本月设备故障类型
     */
    String TITLE_ASSET_M = "title_asset_m";
    /**
     * 本月设备故障台数
     */
    String TITLE_ASSET_N = "title_asset_n";
    /**
     * 不同设备故障分布
     */
    String TITLE_ASSET_O = "title_asset_o";
    /**
     * 超期未保养</br>设备数量
     */
    String TITLE_ASSET_P = "title_asset_p";
    /**
     * 超期未保养设备数量
     */
    String TITLE_ASSET_Q = "title_asset_q";
    /**
     * 待保养设备
     */
    String TITLE_ASSET_R = "title_asset_r";
    /**
     * 待维修设备
     */
    String TITLE_ASSET_S = "title_asset_s";
    /**
     * 点检设备
     */
    String TITLE_ASSET_T = "title_asset_t";
    /**
     * 耕云设备健康云
     */
    String TITLE_ASSET_U = "title_asset_u";
    /**
     * 计划设备总数
     */
    String TITLE_ASSET_V = "title_asset_v";
    /**
     * 离线设备
     */
    String TITLE_ASSET_W = "title_asset_w";
    /**
     * 盘点设备
     */
    String TITLE_ASSET_X = "title_asset_x";
    /**
     * 盘点设备列表
     */
    String TITLE_ASSET_Y = "title_asset_y";
    /**
     * 盘点设备数量
     */
    String TITLE_ASSET_Z = "title_asset_z";
    /**
     * 工单号
     */
    String TITLE_B = "title_b";
    /**
     * 残值
     */
    String TITLE_B_A = "title_b_a";
    /**
     * 物料编码
     */
    String TITLE_BA = "title_ba";
    /**
     * 表格查询列
     */
    String TITLE_BA_A = "title_ba_a";
    /**
     * 工单完成通知
     */
    String TITLE_BAAAB_A = "title_baaab_a";
    /**
     * 工单信息
     */
    String TITLE_BAAAB_B = "title_baaab_b";
    /**
     * 工单主题
     */
    String TITLE_BAAAB_C = "title_baaab_c";
    /**
     * 工单状态
     */
    String TITLE_BAAAB_D = "title_baaab_d";
    /**
     * 工单状态变更通知
     */
    String TITLE_BAAAB_E = "title_baaab_e";
    /**
     * 工单自动生成总数
     */
    String TITLE_BAAAB_F = "title_baaab_f";
    /**
     * 工单总数
     */
    String TITLE_BAAAB_G = "title_baaab_g";
    /**
     * 工号
     */
    String TITLE_BAAAB_H = "title_baaab_h";
    /**
     * 工具
     */
    String TITLE_BAAAB_I = "title_baaab_i";
    /**
     * 工具材料
     */
    String TITLE_BAAAB_J = "title_baaab_j";
    /**
     * 工具规格
     */
    String TITLE_BAAAB_K = "title_baaab_k";
    /**
     * 工商注册号
     */
    String TITLE_BAAAB_L = "title_baaab_l";
    /**
     * 工时
     */
    String TITLE_BAAAB_M = "title_baaab_m";
    /**
     * 工时单价
     */
    String TITLE_BAAAB_N = "title_baaab_n";
    /**
     * 工时费
     */
    String TITLE_BAAAB_O = "title_baaab_o";
    /**
     * 工艺单元
     */
    String TITLE_BAAAB_P = "title_baaab_p";
    /**
     * 工作安排
     */
    String TITLE_BAAAB_Q = "title_baaab_q";
    /**
     * 工作饱和</br>度（分钟）
     */
    String TITLE_BAAAB_R = "title_baaab_r";
    /**
     * 工作饱和度
     */
    String TITLE_BAAAB_S = "title_baaab_s";
    /**
     * 工作标准
     */
    String TITLE_BAAAB_T = "title_baaab_t";
    /**
     * 工作范围
     */
    String TITLE_BAAAB_U = "title_baaab_u";
    /**
     * 工作简述
     */
    String TITLE_BAAAB_V = "title_baaab_v";
    /**
     * 工作建议
     */
    String TITLE_BAAAB_W = "title_baaab_w";
    /**
     * 工作流
     */
    String TITLE_BAAAB_X = "title_baaab_x";
    /**
     * 工作内容
     */
    String TITLE_BAAAB_Y = "title_baaab_y";
    /**
     * 工作任务
     */
    String TITLE_BAAAB_Z = "title_baaab_z";
    /**
     * 单位
     */
    String TITLE_BAAB_A = "title_baab_a";
    /**
     * 单项得分
     */
    String TITLE_BAAB_B = "title_baab_b";
    /**
     * 单项权重
     */
    String TITLE_BAAB_C = "title_baab_c";
    /**
     * 单选按钮
     */
    String TITLE_BAAB_D = "title_baab_d";
    /**
     * 当工单被分配到工单池后
     */
    String TITLE_BAAB_E = "title_baab_e";
    /**
     * 当记录值
     */
    String TITLE_BAAB_F = "title_baab_f";
    /**
     * 当距离工单预约开始时间
     */
    String TITLE_BAAB_G = "title_baab_g";
    /**
     * 当年用量
     */
    String TITLE_BAAB_H = "title_baab_h";
    /**
     * 当前存量
     */
    String TITLE_BAAB_I = "title_baab_i";
    /**
     * 当前登录人
     */
    String TITLE_BAAB_J = "title_baab_j";
    /**
     * 当前容量
     */
    String TITLE_BAAB_K = "title_baab_k";
    /**
     * 当天实时
     */
    String TITLE_BAAB_L = "title_baab_l";
    /**
     * 到场处理结果确认
     */
    String TITLE_BAAB_M = "title_baab_m";
    /**
     * 到场单确认
     */
    String TITLE_BAAB_N = "title_baab_n";
    /**
     * 到场工作
     */
    String TITLE_BAAB_O = "title_baab_o";
    /**
     * 到场签到
     */
    String TITLE_BAAB_P = "title_baab_p";
    /**
     * 到场申请
     */
    String TITLE_BAAB_Q = "title_baab_q";
    /**
     * 到场时效
     */
    String TITLE_BAAB_R = "title_baab_r";
    /**
     * 到场时效</br>（分钟）
     */
    String TITLE_BAAB_S = "title_baab_s";
    /**
     * 到达时间
     */
    String TITLE_BAAB_T = "title_baab_t";
    /**
     * 到期的消息提醒
     */
    String TITLE_BAAB_U = "title_baab_u";
    /**
     * 到期时间
     */
    String TITLE_BAAB_V = "title_baab_v";
    /**
     * 登录
     */
    String TITLE_BAAB_W = "title_baab_w";
    /**
     * 登录名
     */
    String TITLE_BAAB_X = "title_baab_x";
    /**
     * {d}登录系统
     */
    String TITLE_BAAB_Y = "title_baab_y";
    /**
     * 登录账号
     */
    String TITLE_BAAB_Z = "title_baab_z";
    /**
     * 表格查询语句
     */
    String TITLE_BA_B = "title_ba_b";
    /**
     * 等待时间
     */
    String TITLE_BAB_A = "title_bab_a";
    /**
     * 等待重新调度
     */
    String TITLE_BAB_B = "title_bab_b";
    /**
     * 低
     */
    String TITLE_BAB_C = "title_bab_c";
    /**
     * 底部按钮
     */
    String TITLE_BAB_D = "title_bab_d";
    /**
     * 地理位置
     */
    String TITLE_BAB_E = "title_bab_e";
    /**
     * 地图
     */
    String TITLE_BAB_F = "title_bab_f";
    /**
     * 地址
     */
    String TITLE_BAB_G = "title_bab_g";
    /**
     * 地址管理
     */
    String TITLE_BAB_H = "title_bab_h";
    /**
     * 第
     */
    String TITLE_BAB_I = "title_bab_i";
    /**
     * 第%d行数据
     */
    String TITLE_BAB_J = "title_bab_j";
    /**
     * 第二负责人
     */
    String TITLE_BAB_K = "title_bab_k";
    /**
     * 第二审核
     */
    String TITLE_BAB_L = "title_bab_l";
    /**
     * 第三负责人
     */
    String TITLE_BAB_M = "title_bab_m";
    /**
     * 第四负责人
     */
    String TITLE_BAB_N = "title_bab_n";
    /**
     * 第一次维修内容
     */
    String TITLE_BAB_O = "title_bab_o";
    /**
     * 第一次响应时间
     */
    String TITLE_BAB_P = "title_bab_p";
    /**
     * 第一负责人
     */
    String TITLE_BAB_Q = "title_bab_q";
    /**
     * 第一审核
     */
    String TITLE_BAB_R = "title_bab_r";
    /**
     * 点检报修
     */
    String TITLE_BAB_S = "title_bab_s";
    /**
     * 点检次数
     */
    String TITLE_BAB_T = "title_bab_t";
    /**
     * 点检对象
     */
    String TITLE_BAB_U = "title_bab_u";
    /**
     * 点检结果
     */
    String TITLE_BAB_V = "title_bab_v";
    /**
     * 点检区域
     */
    String TITLE_BAB_W = "title_bab_w";
    /**
     * 点检人员
     */
    String TITLE_BAB_X = "title_bab_x";
    /**
     * 点检时效（分钟）
     */
    String TITLE_BAB_Y = "title_bab_y";
    /**
     * 点检完成率
     */
    String TITLE_BAB_Z = "title_bab_z";
    /**
     * 驳回
     */
    String TITLE_BA_C = "title_ba_c";
    /**
     * 博阿研发
     */
    String TITLE_BA_D = "title_ba_d";
    /**
     * 不合格
     */
    String TITLE_BA_E = "title_ba_e";
    /**
     * 不同供应商故障分布
     */
    String TITLE_BA_F = "title_ba_f";
    /**
     * 不做校验
     */
    String TITLE_BA_G = "title_ba_g";
    /**
     * 部件
     */
    String TITLE_BA_H = "title_ba_h";
    /**
     * 部件/BOM
     */
    String TITLE_BA_I = "title_ba_i";
    /**
     * 部件保养
     */
    String TITLE_BA_J = "title_ba_j";
    /**
     * 部件编码
     */
    String TITLE_BA_K = "title_ba_k";
    /**
     * 部件型号
     */
    String TITLE_BA_L = "title_ba_l";
    /**
     * 部门
     */
    String TITLE_BA_M = "title_ba_m";
    /**
     * 部门经理
     */
    String TITLE_BA_N = "title_ba_n";
    /**
     * 部署
     */
    String TITLE_BA_O = "title_ba_o";
    /**
     * 部署KEY
     */
    String TITLE_BA_P = "title_ba_p";
    /**
     * 部署对象
     */
    String TITLE_BA_Q = "title_ba_q";
    /**
     * 部署时间
     */
    String TITLE_BA_R = "title_ba_r";
    /**
     * 部位
     */
    String TITLE_BA_S = "title_ba_s";
    /**
     * 材料
     */
    String TITLE_BA_T = "title_ba_t";
    /**
     * 采购
     */
    String TITLE_BA_U = "title_ba_u";
    /**
     * 采购信息
     */
    String TITLE_BA_V = "title_ba_v";
    /**
     * 采集时间
     */
    String TITLE_BA_W = "title_ba_w";
    /**
     * 菜单
     */
    String TITLE_BA_X = "title_ba_x";
    /**
     * 参考附件
     */
    String TITLE_BA_Y = "title_ba_y";
    /**
     * 参与团队
     */
    String TITLE_BA_Z = "title_ba_z";
    /**
     * 操作内容
     */
    String TITLE_B_B = "title_b_b";
    /**
     * 物料费
     */
    String TITLE_BB = "title_bb";
    /**
     * 定时任务配置
     */
    String TITLE_BBA_A = "title_bba_a";
    /**
     * 定义
     */
    String TITLE_BBA_B = "title_bba_b";
    /**
     * 定制页面
     */
    String TITLE_BBA_C = "title_bba_c";
    /**
     * 读数
     */
    String TITLE_BBA_D = "title_bba_d";
    /**
     * 短信发送时间间隔
     */
    String TITLE_BBA_E = "title_bba_e";
    /**
     * 短信接收人
     */
    String TITLE_BBA_F = "title_bba_f";
    /**
     * 对或错
     */
    String TITLE_BBA_G = "title_bba_g";
    /**
     * 对象
     */
    String TITLE_BBA_H = "title_bba_h";
    /**
     * 对象列表
     */
    String TITLE_BBA_I = "title_bba_i";
    /**
     * 多关键词之间用英文逗号隔开
     */
    String TITLE_BBA_J = "title_bba_j";
    /**
     * 多选下拉框
     */
    String TITLE_BBA_K = "title_bba_k";
    /**
     * 耳听
     */
    String TITLE_BBA_L = "title_bba_l";
    /**
     * 发布
     */
    String TITLE_BBA_M = "title_bba_m";
    /**
     * 发布任务
     */
    String TITLE_BBA_N = "title_bba_n";
    /**
     * 发布时间
     */
    String TITLE_BBA_O = "title_bba_o";
    /**
     * 发货
     */
    String TITLE_BBA_P = "title_bba_p";
    /**
     * 发货量（KG）
     */
    String TITLE_BBA_Q = "title_bba_q";
    /**
     * 发货人
     */
    String TITLE_BBA_R = "title_bba_r";
    /**
     * 发货时间
     */
    String TITLE_BBA_S = "title_bba_s";
    /**
     * 发生时间
     */
    String TITLE_BBA_T = "title_bba_t";
    /**
     * 技术发展经理
     */
    String TITLE_BBA_U = "title_bba_u";
    /**
     * 返回
     */
    String TITLE_BBA_V = "title_bba_v";
    /**
     * 方法
     */
    String TITLE_BBA_W = "title_bba_w";
    /**
     * 费用
     */
    String TITLE_BBA_Y = "title_bba_y";
    /**
     * 费用报表
     */
    String TITLE_BBA_Z = "title_bba_z";
    /**
     * 点检完成总数
     */
    String TITLE_BBBA_A = "title_bbba_a";
    /**
     * 点检项
     */
    String TITLE_BBBA_B = "title_bbba_b";
    /**
     * 点检信息
     */
    String TITLE_BBBA_C = "title_bbba_c";
    /**
     * 点检总时长（分钟）
     */
    String TITLE_BBBA_D = "title_bbba_d";
    /**
     * 点巡检
     */
    String TITLE_BBBA_E = "title_bbba_e";
    /**
     * 点巡检报告
     */
    String TITLE_BBBA_F = "title_bbba_f";
    /**
     * 点巡检编号
     */
    String TITLE_BBBA_G = "title_bbba_g";
    /**
     * 点巡检点
     */
    String TITLE_BBBA_H = "title_bbba_h";
    /**
     * 点巡检点编号
     */
    String TITLE_BBBA_I = "title_bbba_i";
    /**
     * 点巡检情况分析
     */
    String TITLE_BBBA_J = "title_bbba_j";
    /**
     * 点巡检区域
     */
    String TITLE_BBBA_K = "title_bbba_k";
    /**
     * 电话报修
     */
    String TITLE_BBBA_L = "title_bbba_l";
    /**
     * 电话号码（手机或座机）
     */
    String TITLE_BBBA_M = "title_bbba_m";
    /**
     * 电机
     */
    String TITLE_BBBA_N = "title_bbba_n";
    /**
     * 电机故障
     */
    String TITLE_BBBA_O = "title_bbba_o";
    /**
     * 电机故障次数
     */
    String TITLE_BBBA_P = "title_bbba_p";
    /**
     * 电机故障率
     */
    String TITLE_BBBA_Q = "title_bbba_q";
    /**
     * 电机故障总时间（分钟）
     */
    String TITLE_BBBA_R = "title_bbba_r";
    /**
     * 电气故障率
     */
    String TITLE_BBBA_S = "title_bbba_s";
    /**
     * 电气故障率（%）
     */
    String TITLE_BBBA_T = "title_bbba_t";
    /**
     * 电器故障次数
     */
    String TITLE_BBBA_U = "title_bbba_u";
    /**
     * 电器故障率
     */
    String TITLE_BBBA_V = "title_bbba_v";
    /**
     * 电器故障总时间（分钟）
     */
    String TITLE_BBBA_W = "title_bbba_w";
    /**
     * 电子邮箱
     */
    String TITLE_BBBA_X = "title_bbba_x";
    /**
     * 顶部检索
     */
    String TITLE_BBBA_Y = "title_bbba_y";
    /**
     * 定时任务
     */
    String TITLE_BBBA_Z = "title_bbba_z";
    /**
     * 操作权限
     */
    String TITLE_B_C = "title_b_c";
    /**
     * 物流单号
     */
    String TITLE_BC = "title_bc";
    /**
     * 完工提交
     */
    String TITLE_BCD = "title_bcd";
    /**
     * 维修转派
     */
    String TITLE_BCE = "title_bce";
    /**
     * 申请支持
     */
    String TITLE_BCF = "title_bcf";
    /**
     * 父编码
     */
    String TITLE_BCG = "title_bcg";
    /**
     * 下一步操作
     */
    String TITLE_BCH = "title_bch";
    /**
     * 正式操作工
     */
    String TITLE_BCI = "title_bci";
    /**
     * 转单
     */
    String TITLE_BCJ = "title_bcj";
    /**
     * 入库价格
     */
    String TITLE_BCK = "title_bck";
    /**
     * 库位
     */
    String TITLE_BCL = "title_bcl";
    /**
     * 时
     */
    String TITLE_BCM = "title_bcm";
    /**
     * 备件型号重复
     */
    String TITLE_BCN = "title_bcn";
    /**
     * 操作人
     */
    String TITLE_B_D = "title_b_d";
    /**
     * 物流发货
     */
    String TITLE_BD = "title_bd";
    /**
     * 更新失败
     */
    String TITLE_BDE = "title_bde";
    /**
     * 导入成功
     */
    String TITLE_BDF = "title_bdf";
    /**
     * 来源工单
     */
    String TITLE_BDG = "title_bdg";
    /**
     * 最终数量
     */
    String TITLE_BDH = "title_bdh";
    /**
     * 存放位置
     */
    String TITLE_BDI = "title_bdi";
    /**
     * 调出位置
     */
    String TITLE_BDJ = "title_bdj";
    /**
     * 入库价格
     */
    String TITLE_BDK = "title_bdk";
    /**
     * 批次号
     */
    String TITLE_BDL = "title_bdl";
    /**
     * 入库类型
     */
    String TITLE_BDM = "title_bdm";
    /**
     * 任务管理
     */
    String TITLE_BDN = "title_bdn";
    /**
     * 操作时间
     */
    String TITLE_B_E = "title_b_e";
    /**
     * 物流签收
     */
    String TITLE_BE = "title_be";
    /**
     * 提交人
     */
    String TITLE_BEF = "title_bef";
    /**
     * 提交日期
     */
    String TITLE_BEG = "title_beg";
    /**
     * 单据类型
     */
    String TITLE_BEH = "title_beh";
    /**
     * 作废此条工单
     */
    String TITLE_BEI = "title_bei";
    /**
     * 作废工单
     */
    String TITLE_BEJ = "title_bej";
    /**
     * 备件编号
     */
    String TITLE_BEK = "title_bek";
    /**
     * 挂靠设备
     */
    String TITLE_BEL = "title_bel";
    /**
     * 采购理由
     */
    String TITLE_BEM = "title_bem";
    /**
     * 设备出入库
     */
    String TITLE_BEN = "title_ben";
    /**
     * 操作指引
     */
    String TITLE_B_F = "title_b_f";
    /**
     * 物流信息
     */
    String TITLE_BF = "title_bf";
    /**
     * 备件出入库
     */
    String TITLE_BFG = "title_bfg";
    /**
     * 为空或格式有误
     */
    String TITLE_BFH = "title_bfh";
    /**
     * 请先在系统中添加
     */
    String TITLE_BFI = "title_bfi";
    /**
     * 错误
     */
    String TITLE_BFJ = "title_bfj";
    /**
     * 类型错误，应该为：
     */
    String TITLE_BFK = "title_bfk";
    /**
     * 资产号
     */
    String TITLE_BFL = "title_bfl";
    /**
     * 任务项新增
     */
    String TITLE_BFM = "title_bfm";
    /**
     * 任务模板新增
     */
    String TITLE_BFN = "title_bfn";
    /**
     * 草稿
     */
    String TITLE_B_G = "title_b_g";
    /**
     * 系统
     */
    String TITLE_BG = "title_bg";
    /**
     * 任务项详情
     */
    String TITLE_BGH = "title_bgh";
    /**
     * 任务模板详情
     */
    String TITLE_BGI = "title_bgi";
    /**
     * 其他信息
     */
    String TITLE_BGJ = "title_bgj";
    /**
     * 结果校验
     */
    String TITLE_BGK = "title_bgk";
    /**
     * 备件新增
     */
    String TITLE_BGL = "title_bgl";
    /**
     * 备件详情
     */
    String TITLE_BGM = "title_bgm";
    /**
     * 备件编辑
     */
    String TITLE_BGN = "title_bgn";
    /**
     * 查看配置
     */
    String TITLE_B_H = "title_b_h";
    /**
     * 系统管理员
     */
    String TITLE_BH = "title_bh";
    /**
     * 出库类型
     */
    String TITLE_BHI = "title_bhi";
    /**
     * 申请人
     */
    String TITLE_BHJ = "title_bhj";
    /**
     * 销账数量
     */
    String TITLE_BHK = "title_bhk";
    /**
     * 当前库存量
     */
    String TITLE_BHL = "title_bhl";
    /**
     * 最小安全库存
     */
    String TITLE_BHM = "title_bhm";
    /**
     * 备件描述
     */
    String TITLE_BHN = "title_bhn";
    /**
     * 查询
     */
    String TITLE_B_I = "title_b_i";
    /**
     * 系统记录数量
     */
    String TITLE_BI = "title_bi";
    /**
     * 备件号
     */
    String TITLE_BIJ = "title_bij";
    /**
     * 消息通知
     */
    String TITLE_BIK = "title_bik";
    /**
     * 消息详情
     */
    String TITLE_BIL = "title_bil";
    /**
     * 超时任务
     */
    String TITLE_BIM = "title_bim";
    /**
     * 账号设置
     */
    String TITLE_BIN = "title_bin";
    /**
     * 查询条件
     */
    String TITLE_B_J = "title_b_j";
    /**
     * 系统可用时间
     */
    String TITLE_BJ = "title_bj";
    /**
     * 位置描述
     */
    String TITLE_BJK = "title_bjk";
    /**
     * 设备位置详情
     */
    String TITLE_BJL = "title_bjl";
    /**
     * 开始
     */
    String TITLE_BJM = "title_bjm";
    /**
     * 结束
     */
    String TITLE_BJN = "title_bjn";
    /**
     * 查询条件标题
     */
    String TITLE_B_K = "title_b_k";
    /**
     * 系统模板
     */
    String TITLE_BK = "title_bk";
    /**
     * 库房权限
     */
    String TITLE_BKL = "title_bkl";
    /**
     * 新增备件
     */
    String TITLE_BKM = "title_bkm";
    /**
     * 统计配置详情
     */
    String TITLE_BKN = "title_bkn";
    /**
     * 查询语句
     */
    String TITLE_B_L = "title_b_l";
    /**
     * 系统配置
     */
    String TITLE_BL = "title_bl";
    /**
     * 已生成
     */
    String TITLE_BLM = "title_blm";
    /**
     * 维保计划编辑
     */
    String TITLE_BLN = "title_bln";
    /**
     * 查找备件
     */
    String TITLE_B_M = "title_b_m";
    /**
     * 系统时间
     */
    String TITLE_BM = "title_bm";
    /**
     * 维保计划详情
     */
    String TITLE_BMN = "title_bmn";
    /**
     * 差旅时间
     */
    String TITLE_B_N = "title_b_n";
    /**
     * 系统首页
     */
    String TITLE_BN = "title_bn";
    /**
     * 厂商
     */
    String TITLE_B_O = "title_b_o";
    /**
     * 系统新建自动任务
     */
    String TITLE_BO = "title_bo";
    /**
     * 厂商管理
     */
    String TITLE_B_P = "title_b_p";
    /**
     * 系统语言
     */
    String TITLE_BP = "title_bp";
    /**
     * 场地
     */
    String TITLE_B_Q = "title_b_q";
    /**
     * 下
     */
    String TITLE_BQ = "title_bq";
    /**
     * 场地监控
     */
    String TITLE_B_R = "title_b_r";
    /**
     * 下次维护安排和推荐
     */
    String TITLE_BR = "title_br";
    /**
     * 场地清洁
     */
    String TITLE_B_S = "title_b_s";
    /**
     * 下次预防性维护预计相关工作内容及备件更换清单
     */
    String TITLE_BS = "title_bs";
    /**
     * 场地统计
     */
    String TITLE_B_T = "title_b_t";
    /**
     * 下拉框
     */
    String TITLE_BT = "title_bt";
    /**
     * 抄表
     */
    String TITLE_B_U = "title_b_u";
    /**
     * 水量水质信息
     */
    String TITLE_BCJN = "title_bcjn";
    /**
     * 检测记录
     */
    String TAB_AK = "tab_ak";
    /**
     * 缴费信息
     */
    String TITLE_ADFG = "title_adfg";
    /**
     * 下限值
     */
    String TITLE_BU = "title_bu";
    /**
     * 抄送人
     */
    String TITLE_B_V = "title_b_v";
    /**
     * 下一步操作人角色
     */
    String TITLE_BV = "title_bv";
    /**
     * 超期工单
     */
    String TITLE_B_W = "title_b_w";
    /**
     * 下一步状态
     */
    String TITLE_BW = "title_bw";
    /**
     * 超期未</br>保养次数
     */
    String TITLE_B_X = "title_b_x";
    /**
     * 下载模版
     */
    String TITLE_BX = "title_bx";
    /**
     * 超期未保</br>养次数
     */
    String TITLE_B_Y = "title_b_y";
    /**
     * 显示方式
     */
    String TITLE_BY = "title_by";
    /**
     * 超期未保养次数
     */
    String TITLE_B_Z = "title_b_z";
    /**
     * 显示看板
     */
    String TITLE_BZ = "title_bz";
    /**
     * 文件类别
     */
    String TITLE_C = "title_c";
    /**
     * 显示曲线
     */
    String TITLE_CA = "title_ca";
    /**
     * 种类
     */
    String TITLE_CATEGORY_A = "title_category_a";
    /**
     * 故障类型
     */
    String TITLE_CATEGORY_AA = "title_category_aa";
    /**
     * 关闭类型
     */
    String TITLE_CATEGORY_AB = "title_category_ab";
    /**
     * 文档类型
     */
    String TITLE_CATEGORY_AB_A = "title_category_ab_a";
    /**
     * 问题类型
     */
    String TITLE_CATEGORY_AB_B = "title_category_ab_b";
    /**
     * 显示类型
     */
    String TITLE_CATEGORY_AB_C = "title_category_ab_c";
    /**
     * 详情类型
     */
    String TITLE_CATEGORY_AB_D = "title_category_ab_d";
    /**
     * 协约类型
     */
    String TITLE_CATEGORY_AB_E = "title_category_ab_e";
    /**
     * 业务类型
     */
    String TITLE_CATEGORY_AB_F = "title_category_ab_f";
    /**
     * 值类型
     */
    String TITLE_CATEGORY_AB_G = "title_category_ab_g";
    /**
     * 指标类型
     */
    String TITLE_CATEGORY_AB_H = "title_category_ab_h";
    /**
     * 资产类型
     */
    String TITLE_CATEGORY_AB_I = "title_category_ab_i";
    /**
     * 字段类型
     */
    String TITLE_CATEGORY_AB_J = "title_category_ab_j";
    /**
     * 组织类型
     */
    String TITLE_CATEGORY_AB_K = "title_category_ab_k";
    /**
     * 关联类型
     */
    String TITLE_CATEGORY_AC = "title_category_ac";
    /**
     * 监控类型
     */
    String TITLE_CATEGORY_AD = "title_category_ad";
    /**
     * 结果类型
     */
    String TITLE_CATEGORY_AE = "title_category_ae";
    /**
     * 看板类型
     */
    String TITLE_CATEGORY_AF = "title_category_af";
    /**
     * 考核类型
     */
    String TITLE_CATEGORY_AG = "title_category_ag";
    /**
     * 类型
     */
    String TITLE_CATEGORY_AH = "title_category_ah";
    /**
     * 类型编号
     */
    String TITLE_CATEGORY_AI = "title_category_ai";
    /**
     * 模板类型
     */
    String TITLE_CATEGORY_AJ = "title_category_aj";
    /**
     * 模块类型
     */
    String TITLE_CATEGORY_AK = "title_category_ak";
    /**
     * 企业类型
     */
    String TITLE_CATEGORY_AL = "title_category_al";
    /**
     * 请求类型
     */
    String TITLE_CATEGORY_AM = "title_category_am";
    /**
     * 任务类型
     */
    String TITLE_CATEGORY_AN = "title_category_an";
    /**
     * 日类型
     */
    String TITLE_CATEGORY_AO = "title_category_ao";
    /**
     * 日志类型
     */
    String TITLE_CATEGORY_AP = "title_category_ap";
    /**
     * 入库类型
     */
    String TITLE_CATEGORY_AQ = "title_category_aq";
    /**
     * 数据库字段类型
     */
    String TITLE_CATEGORY_AR = "title_category_ar";
    /**
     * 所属类型
     */
    String TITLE_CATEGORY_AS = "title_category_as";
    /**
     * 所有类型
     */
    String TITLE_CATEGORY_AT = "title_category_at";
    /**
     * 投资类型
     */
    String TITLE_CATEGORY_AU = "title_category_au";
    /**
     * 团队类型
     */
    String TITLE_CATEGORY_AV = "title_category_av";
    /**
     * 维护类型
     */
    String TITLE_CATEGORY_AW = "title_category_aw";
    /**
     * 维修类型
     */
    String TITLE_CATEGORY_AX = "title_category_ax";
    /**
     * 维修类型统计
     */
    String TITLE_CATEGORY_AY = "title_category_ay";
    /**
     * 位置类型
     */
    String TITLE_CATEGORY_AZ = "title_category_az";
    /**
     * 保养类型
     */
    String TITLE_CATEGORY_B = "title_category_b";
    /**
     * 备件类别
     */
    String TITLE_CATEGORY_C = "title_category_c";
    /**
     * 备件请求类型
     */
    String TITLE_CATEGORY_D = "title_category_d";
    /**
     * 备件消耗类型
     */
    String TITLE_CATEGORY_E = "title_category_e";
    /**
     * 备件消耗类型统计
     */
    String TITLE_CATEGORY_F = "title_category_f";
    /**
     * 部件类型
     */
    String TITLE_CATEGORY_G = "title_category_g";
    /**
     * 部署分类
     */
    String TITLE_CATEGORY_H = "title_category_h";
    /**
     * 采集值类型
     */
    String TITLE_CATEGORY_I = "title_category_i";
    /**
     * 查询条件类型
     */
    String TITLE_CATEGORY_J = "title_category_j";
    /**
     * 车间类型
     */
    String TITLE_CATEGORY_K = "title_category_k";
    /**
     * 处理人分配类型
     */
    String TITLE_CATEGORY_L = "title_category_l";
    /**
     * 存储类型
     */
    String TITLE_CATEGORY_M = "title_category_m";
    /**
     * 措施类型
     */
    String TITLE_CATEGORY_N = "title_category_n";
    /**
     * 单位类型
     */
    String TITLE_CATEGORY_O = "title_category_o";
    /**
     * 地图类型
     */
    String TITLE_CATEGORY_P = "title_category_p";
    /**
     * 地址类型
     */
    String TITLE_CATEGORY_Q = "title_category_q";
    /**
     * 订单类型
     */
    String TITLE_CATEGORY_R = "title_category_r";
    /**
     * 费用分类
     */
    String TITLE_CATEGORY_S = "title_category_s";
    /**
     * 费用类型
     */
    String TITLE_CATEGORY_T = "title_category_t";
    /**
     * 分类
     */
    String TITLE_CATEGORY_U = "title_category_u";
    /**
     * 分派类型
     */
    String TITLE_CATEGORY_V = "title_category_v";
    /**
     * 服务请求类型
     */
    String TITLE_CATEGORY_W = "title_category_w";
    /**
     * 工单类型
     */
    String TITLE_CATEGORY_X = "title_category_x";
    /**
     * 工具类型
     */
    String TITLE_CATEGORY_Y = "title_category_y";
    /**
     * 故障代码（类型）
     */
    String TITLE_CATEGORY_Z = "title_category_z";
    /**
     * 显示顺序
     */
    String TITLE_CB = "title_cb";
    /**
     * 显示所有
     */
    String TITLE_CC = "title_cc";
    /**
     * 显示位置
     */
    String TITLE_CD = "title_cd";
    /**
     * 维保计划新增
     */
    String TITLE_CDE = "title_cde";
    /**
     * 通过手机号修改
     */
    String TITLE_CDF = "title_cdf";
    /**
     * 填写手机号
     */
    String TITLE_CDG = "title_cdg";
    /**
     * 设置新密码
     */
    String TITLE_CDH = "title_cdh";
    /**
     * 设置成功
     */
    String TITLE_CDI = "title_cdi";
    /**
     * 重置密码成功，3s后自动跳转到登陆页
     */
    String TITLE_CDJ = "title_cdj";
    /**
     * 如果您的浏览器没跳转
     */
    String TITLE_CDK = "title_cdk";
    /**
     * 已过期
     */
    String TITLE_CDL = "title_cdl";
    /**
     * 联动属性
     */
    String TITLE_CDM = "title_cdm";
    /**
     * 关联子页面
     */
    String TITLE_CDN = "title_cdn";
    /**
     * 显示语言
     */
    String TITLE_CE = "title_ce";
    /**
     * 子页面名称
     */
    String TITLE_CEF = "title_cef";
    /**
     * 子页面数据
     */
    String TITLE_CEG = "title_ceg";
    /**
     * 选择字段
     */
    String TITLE_CEH = "title_ceh";
    /**
     * 控件
     */
    String TITLE_CEI = "title_cei";
    /**
     * 是否显示
     */
    String TITLE_CEJ = "title_cej";
    /**
     * 详情
     */
    String TITLE_CEK = "title_cek";
    /**
     * 任务模版
     */
    String TITLE_CEL = "title_cel";
    /**
     * 上传图片
     */
    String TITLE_CEM = "title_cem";
    /**
     * 默认值
     */
    String TITLE_CEN = "title_cen";
    /**
     * 显示最新
     */
    String TITLE_CF = "title_cf";
    /**
     * 电话
     */
    String TITLE_CFG = "title_cfg";
    /**
     * 设备出入库详情
     */
    String TITLE_CFH = "title_cfh";
    /**
     * 设备出入库处理
     */
    String TITLE_CFI = "title_cfi";
    /**
     * 备件出入库详情
     */
    String TITLE_CFJ = "title_cfj";
    /**
     * 备件出入库处理
     */
    String TITLE_CFK = "title_cfk";
    /**
     * 选择最多不能超过5个
     */
    String TITLE_CFL = "title_cfl";
    /**
     * 报表展示
     */
    String TITLE_CFM = "title_cfm";
    /**
     * 生产作业编码
     */
    String TITLE_CFN = "title_cfn";
    /**
     * 现场照片
     */
    String TITLE_CG = "title_cg";
    /**
     * 生产作业下井编码
     */
    String TITLE_CGH = "title_cgh";
    /**
     * 任务单号
     */
    String TITLE_CGI = "title_cgi";
    /**
     * 井号
     */
    String TITLE_CGJ = "title_cgj";
    /**
     * 最高井温(℃)
     */
    String TITLE_CGK = "title_cgk";
    /**
     * 井底压力(Mpc)
     */
    String TITLE_CGL = "title_cgl";
    /**
     * 下井次数
     */
    String TITLE_CGM = "title_cgm";
    /**
     * 出库单号
     */
    String TITLE_CGN = "title_cgn";
    /**
     * 相册
     */
    String TITLE_CH = "title_ch";
    /**
     * 校验格式
     */
    String TITLE_CHECK_A = "title_check_a";
    /**
     * 生产作业下井次数编码
     */
    String TITLE_CHI = "title_chi";
    /**
     * 生产作业
     */
    String TITLE_CHJ = "title_chj";
    /**
     * 生产作业下井
     */
    String TITLE_CHK = "title_chk";
    /**
     * 生产作业下井次数
     */
    String TITLE_CHL = "title_chl";
    /**
     * 下井时间
     */
    String TITLE_CHM = "title_chm";
    /**
     * 出井时间
     */
    String TITLE_CHN = "title_chn";
    /**
     * 相机
     */
    String TITLE_CI = "title_ci";
    /**
     * 生产作业下井设备
     */
    String TITLE_CIJ = "title_cij";
    /**
     * 生产作业单
     */
    String TITLE_CIK = "title_cik";
    /**
     * 设备库存
     */
    String TITLE_CIL = "title_cil";
    /**
     * 设备台账
     */
    String TITLE_CIM = "title_cim";
    /**
     * 生产作业信息维护
     */
    String TITLE_CIN = "title_cin";
    /**
     * 详情模版
     */
    String TITLE_CJ = "title_cj";
    /**
     * 错误日志
     */
    String TITLE_CJK = "title_cjk";
    /**
     * 错误帮助
     */
    String TITLE_CJL = "title_cjl";
    /**
     * 编辑(主数据)
     */
    String TITLE_CJM = "title_cjm";
    /**
     * 编辑(子数据)
     */
    String TITLE_CJN = "title_cjn";
    /**
     * 详细描述
     */
    String TITLE_CK = "title_ck";
    /**
     * 所用设备
     */
    String TITLE_CKL = "title_ckl";
    /**
     * 设备流水号
     */
    String TITLE_CKM = "title_ckm";
    /**
     * 明细列表
     */
    String TITLE_CKN = "title_ckn";
    /**
     * 详细数据
     */
    String TITLE_CL = "title_cl";
    /**
     * 设备选择
     */
    String TITLE_CLM = "title_clm";
    /**
     * 功能选择
     */
    String TITLE_CLN = "title_cln";
    /**
     * 详细信息
     */
    String TITLE_CM = "title_cm";
    /**
     * 设备描述
     */
    String TITLE_CMN = "title_cmn";
    /**
     * 项目
     */
    String TITLE_CN = "title_cn";
    /**
     * 项目号
     */
    String TITLE_CO = "title_co";
    /**
     * 完成时间
     */
    String TITLE_COMP_A = "title_comp_a";
    /**
     * 消耗
     */
    String TITLE_CP = "title_cp";
    /**
     * 消息
     */
    String TITLE_CQ = "title_cq";
    /**
     * 消息配置
     */
    String TITLE_CR = "title_cr";
    /**
     * 创建时间
     */
    String TITLE_CREATED_A = "title_created_a";
    /**
     * 小车
     */
    String TITLE_CS = "title_cs";
    /**
     * 小时
     */
    String TITLE_CT = "title_ct";
    /**
     * 效率统计
     */
    String TITLE_CU = "title_cu";
    /**
     * 协议名
     */
    String TITLE_CV = "title_cv";
    /**
     * 协助
     */
    String TITLE_CW = "title_cw";
    /**
     * 写入
     */
    String TITLE_CX = "title_cx";
    /**
     * 新购
     */
    String TITLE_CY = "title_cy";
    /**
     * 新购货币
     */
    String TITLE_CZ = "title_cz";
    /**
     * 上传文件
     */
    String TITLE_D = "title_d";
    /**
     * 新密码
     */
    String TITLE_DA = "title_da";
    /**
     * 启用日期
     */
    String TITLE_DATE_A = "title_date_a";
    /**
     * 巡检日期
     */
    String TITLE_DATE_A_A = "title_date_a_a";
    /**
     * 购买日期
     */
    String TITLE_DATE_B = "title_date_b";
    /**
     * 开始日期
     */
    String TITLE_DATE_C = "title_date_c";
    /**
     * 结束日期
     */
    String TITLE_DATE_D = "title_date_d";
    /**
     * 保养日期
     */
    String TITLE_DATE_E = "title_date_e";
    /**
     * 报废日期
     */
    String TITLE_DATE_F = "title_date_f";
    /**
     * 操作日期
     */
    String TITLE_DATE_G = "title_date_g";
    /**
     * 创建日期
     */
    String TITLE_DATE_H = "title_date_h";
    /**
     * 点检日期
     */
    String TITLE_DATE_I = "title_date_i";
    /**
     * 发生日期
     */
    String TITLE_DATE_J = "title_date_j";
    /**
     * 分析日期
     */
    String TITLE_DATE_K = "title_date_k";
    /**
     * 更换日期
     */
    String TITLE_DATE_L = "title_date_l";
    /**
     * 工作日期
     */
    String TITLE_DATE_M = "title_date_m";
    /**
     * 汇总日期
     */
    String TITLE_DATE_N = "title_date_n";
    /**
     * 计划开工日期
     */
    String TITLE_DATE_O = "title_date_o";
    /**
     * 交付日期
     */
    String TITLE_DATE_P = "title_date_p";
    /**
     * 结算日期
     */
    String TITLE_DATE_Q = "title_date_q";
    /**
     * 领用日期
     */
    String TITLE_DATE_R = "title_date_r";
    /**
     * 盘点日期
     */
    String TITLE_DATE_S = "title_date_s";
    /**
     * 日期
     */
    String TITLE_DATE_T = "title_date_t";
    /**
     * 日期（默认）
     */
    String TITLE_DATE_U = "title_date_u";
    /**
     * 日期和时间
     */
    String TITLE_DATE_V = "title_date_v";
    /**
     * 调拨日期
     */
    String TITLE_DATE_W = "title_date_w";
    /**
     * 完成日期
     */
    String TITLE_DATE_X = "title_date_x";
    /**
     * 维护日期
     */
    String TITLE_DATE_Y = "title_date_y";
    /**
     * 维修日期
     */
    String TITLE_DATE_Z = "title_date_z";
    /**
     * 新密码和确认密码不一致
     */
    String TITLE_DB = "title_db";
    /**
     * 信息不存在
     */
    String TITLE_DC = "title_dc";
    /**
     * 型号
     */
    String TITLE_DD = "title_dd";
    /**
     * 型号规格
     */
    String TITLE_DE = "title_de";
    /**
     * 发起日期
     */
    String TITLE_DEF = "title_def";
    /**
     * 申请日期
     */
    String TITLE_DEG = "title_deg";
    /**
     * 新增设备型号
     */
    String TITLE_DEH = "title_deh";
    /**
     * 新增设备类型
     */
    String TITLE_DEI = "title_dei";
    /**
     * 新增设备位置
     */
    String TITLE_DEJ = "title_dej";
    /**
     * 缓存管理
     */
    String TITLE_DEK = "title_dek";
    /**
     * 缓存名称
     */
    String TITLE_DEL = "title_del";
    /**
     * 缓存的对象数量
     */
    String TITLE_DEM = "title_dem";
    /**
     * 缓存对象占用内存的数量
     */
    String TITLE_DEN = "title_den";
    /**
     * 性能指标
     */
    String TITLE_DF = "title_df";
    /**
     * 缓存对象占用磁盘的数量
     */
    String TITLE_DFG = "title_dfg";
    /**
     * 缓存读取的命中次数
     */
    String TITLE_DFH = "title_dfh";
    /**
     * 内存中缓存读取的命中次数
     */
    String TITLE_DFI = "title_dfi";
    /**
     * 磁盘中缓存读取的命中次数
     */
    String TITLE_DFJ = "title_dfj";
    /**
     * 缓存读取的丢失次数
     */
    String TITLE_DFK = "title_dfk";
    /**
     * 缓存读取的已经被销毁的对象丢失次数
     */
    String TITLE_DFL = "title_dfl";
    /**
     * 命中次数
     */
    String TITLE_DFM = "title_dfm";
    /**
     * 最后访问时间
     */
    String TITLE_DFN = "title_dfn";
    /**
     * 姓名
     */
    String TITLE_DG = "title_dg";
    /**
     * 过期时间
     */
    String TITLE_DGH = "title_dgh";
    /**
     * 最后更新时间
     */
    String TITLE_DGI = "title_dgi";
    /**
     * 存活时间
     */
    String TITLE_DGJ = "title_dgj";
    /**
     * 空闲时间
     */
    String TITLE_DGK = "title_dgk";
    /**
     * 出入库类型
     */
    String TITLE_DGL = "title_dgl";
    /**
     * 出入记录
     */
    String TITLE_DGM = "title_dgm";
    /**
     * 明细清单
     */
    String TITLE_DGN = "title_dgn";
    /**
     * 修改
     */
    String TITLE_DH = "title_dh";
    /**
     * 建卡
     */
    String TITLE_DHI = "title_dhi";
    /**
     * 本次服务请求已完成：{d}，请您知悉。
     */
    String TITLE_DHJ = "title_dhj";
    /**
     * {d}请您尽快分配人员确认服务请求：{d}。
     */
    String TITLE_DHK = "title_dhk";
    /**
     * {d}拒绝了{d}，请您知悉。
     */
    String TITLE_DHL = "title_dhl";
    /**
     * {d}请您尽快分配人员确认{2}工单：{3}。
     */
    String TITLE_DHM = "title_dhm";
    /**
     * 申请人{d}请您尽快审核提交的{d}申请。
     */
    String TITLE_DHN = "title_dhn";
    /**
     * 修改密码
     */
    String TITLE_DI = "title_di";
    /**
     * {d}请您尽快确认{2}工单：{d}。
     */
    String TITLE_DIJ = "title_dij";
    /**
     * {d}完成了{2}工单：{d}，请您尽快确认。
     */
    String TITLE_DIK = "title_dik";
    /**
     * {d}拒绝了{2}工单：{d}，请您重新处理。
     */
    String TITLE_DIL = "title_dil";
    /**
     * {d}关闭了服务请求：{d}，请您知悉。
     */
    String TITLE_DIM = "title_dim";
    /**
     * {d}请您尽快分配人员{d}：{d}。
     */
    String TITLE_DIN = "title_din";
    /**
     * 需求物料
     */
    String TITLE_DJ = "title_dj";
    /**
     * {d}退回了{2}工单：{d}，请您重新处理。
     */
    String TITLE_DJK = "title_djk";
    /**
     * {d}的{d}申请单，审核已通过，请您知悉。
     */
    String TITLE_DJL = "title_djl";
    /**
     * {d}请您尽快完成{d}。
     */
    String TITLE_DJM = "title_djm";
    /**
     * {d}申请{d}，请您审核。
     */
    String TITLE_DJN = "title_djn";
    /**
     * 许可证
     */
    String TITLE_DK = "title_dk";
    /**
     * {d}的{d}申请单，审核已退回，请您知悉。
     */
    String TITLE_DKL = "title_dkl";
    /**
     * 备件名称：{d}，备件编码：{d}，库房：{d}，当前库存小于安全库存，请及时安排补充。
     */
    String TITLE_DKM = "title_dkm";
    /**
     * 尊敬的用户，您好！您的{d}报修平台账户已开通（微信搜索小程序“{d}”），账户：{d}，欢迎使用。
     */
    String TITLE_DKN = "title_dkn";
    /**
     * 序号
     */
    String TITLE_DL = "title_dl";
    /**
     * 设备名称：{d}，设备编码：{d}，位置：{d}，监控项：{d}，有{1}报警，请及时安排查看。
     */
    String TITLE_DLM = "title_dlm";
    /**
     * {d}请您尽快确认服务请求：{d}。
     */
    String TITLE_DLN = "title_dln";
    /**
     * 选平面图
     */
    String TITLE_DM = "title_dm";
    /**
     * 您的验证码为{d}，请于{d}分钟内填写。为保障您的账号安全，请勿将验证码短信转发给他人。
     */
    String TITLE_DMN = "title_dmn";
    /**
     * 选择
     */
    String TITLE_DN = "title_dn";
    /**
     * 选择（本地）
     */
    String TITLE_DO = "title_do";
    /**
     * 选择（远程）
     */
    String TITLE_DP = "title_dp";
    /**
     * 选择URL或JSON
     */
    String TITLE_DQ = "title_dq";
    /**
     * 选择当前页
     */
    String TITLE_DR = "title_dr";
    /**
     * 选择分拨
     */
    String TITLE_DS = "title_ds";
    /**
     * 选择分配人员
     */
    String TITLE_DT = "title_dt";
    /**
     * 选择附件
     */
    String TITLE_DU = "title_du";
    /**
     * 选择库房
     */
    String TITLE_DV = "title_dv";
    /**
     * 选择企业
     */
    String TITLE_DW = "title_dw";
    /**
     * 选择填充数据（URL或JSON）
     */
    String TITLE_DX = "title_dx";
    /**
     * 选择图片
     */
    String TITLE_DY = "title_dy";
    /**
     * 选择维保对象
     */
    String TITLE_DZ = "title_dz";
    /**
     * 类型编码
     */
    String TITLE_E = "title_e";
    /**
     * 选择位置
     */
    String TITLE_EA = "title_ea";
    /**
     * 选择支援人员
     */
    String TITLE_EB = "title_eb";
    /**
     * 选择组织
     */
    String TITLE_EC = "title_ec";
    /**
     * 巡更点
     */
    String TITLE_ED = "title_ed";
    /**
     * 巡检报修
     */
    String TITLE_EE = "title_ee";
    /**
     * 巡检次数
     */
    String TITLE_EF = "title_ef";
    /**
     * 尊敬的用户，您好！您的{d}报修平台账户已开通（微信搜索小程序“{d}”），账户：{d}，密码：{d}，请您及时登录修改密码。
     */
    String TITLE_EFG = "title_efg";
    /**
     * 设备名称：{d}，设备编码：{d}，位置：{d}，监控项：{d}，有{5}异常，请及时安排查看。
     */
    String TITLE_EFH = "title_efh";
    /**
     * {d}关闭了工单：{2d}，请您知悉。
     */
    String TITLE_EFI = "title_efi";
    /**
     * 您的{1}工单：{d}，还有{d}就会超时，请加紧完成。
     */
    String TITLE_EFJ = "title_efj";
    /**
     * {d}确认通过了{2}工单：{d}，请您知悉。
     */
    String TITLE_EFK = "title_efk";
    /**
     * {1d}负责的{2}工单：{d}，已超时{d}，请您知悉。
     */
    String TITLE_EFL = "title_efl";
    /**
     * 您的{1}工单：{d}，已超时{d}，请知悉。
     */
    String TITLE_EFM = "title_efm";
    /**
     * {1d}请您尽快完成{2}工单：{d}。
     */
    String TITLE_EFN = "title_efn";
    /**
     * 巡检单号
     */
    String TITLE_EG = "title_eg";
    /**
     * 设备验收
     */
    String TITLE_EGH = "title_egh";
    /**
     * 设备出库
     */
    String TITLE_EGI = "title_egi";
    /**
     * 备件销账
     */
    String TITLE_EGJ = "title_egj";
    /**
     * 备件采购需求
     */
    String TITLE_EGK = "title_egk";
    /**
     * 关注
     */
    String TITLE_EGL = "title_egl";
    /**
     * 未关注
     */
    String TITLE_EGM = "title_egm";
    /**
     * 已读
     */
    String TITLE_EGN = "title_egn";
    /**
     * 巡检点
     */
    String TITLE_EH = "title_eh";
    /**
     * 库存清单
     */
    String TITLE_EHI = "title_ehi";
    /**
     * 所需数量
     */
    String TITLE_EHJ = "title_ehj";
    /**
     * 使用记录
     */
    String TITLE_EHK = "title_ehk";
    /**
     * 微信号绑定
     */
    String TITLE_EHL = "title_ehl";
    /**
     * 微信号解绑
     */
    String TITLE_EHM = "title_ehm";
    /**
     * 解绑当前微信号
     */
    String TITLE_EHN = "title_ehn";
    /**
     * 巡检对象
     */
    String TITLE_EI = "title_ei";
    /**
     * 成功
     */
    String TITLE_EIJ = "title_eij";
    /**
     * 失败
     */
    String TITLE_EIK = "title_eik";
    /**
     * 【{d}】：数量从{d}变成了{d}，单号：{d}
     */
    String TITLE_EIL = "title_eil";
    /**
     * 文件格式错误，目前只支持xls、xlsx文件！
     */
    String TITLE_EIM = "title_eim";
    /**
     * 第{d}行，{d}
     */
    String TITLE_EIN = "title_ein";
    /**
     * 巡检结果
     */
    String TITLE_EJ = "title_ej";
    /**
     * 设备导入
     */
    String TITLE_EJK = "title_ejk";
    /**
     * 备件导入
     */
    String TITLE_EJL = "title_ejl";
    /**
     * 设备备件导入
     */
    String TITLE_EJM = "title_ejm";
    /**
     * 维保行事历导入
     */
    String TITLE_EJN = "title_ejn";
    /**
     * 巡检区域
     */
    String TITLE_EK = "title_ek";
    /**
     * 运行时间
     */
    String TITLE_EKL = "title_ekl";
    /**
     * 排污时间
     */
    String TITLE_ACEL = "title_acel";
    /**
     * 维护时间
     */
    String TITLE_EKM = "title_ekm";
    /**
     * 绩效标准
     */
    String TITLE_EKN = "title_ekn";
    /**
     * 巡检人员
     */
    String TITLE_EL = "title_el";
    /**
     * 设备利用率
     */
    String TITLE_ELM = "title_elm";
    /**
     * 本月运行时间
     */
    String TITLE_ELN = "title_eln";
    /**
     * 巡检时效（分钟）
     */
    String TITLE_EM = "title_em";
    /**
     * 本月故障时间
     */
    String TITLE_EMN = "title_emn";
    /**
     * 巡检完成率
     */
    String TITLE_EN = "title_en";
    /**
     * 巡检完成总数
     */
    String TITLE_EO = "title_eo";
    /**
     * 巡检问题
     */
    String TITLE_EP = "title_ep";
    /**
     * 巡检项
     */
    String TITLE_EQ = "title_eq";
    /**
     * 巡检项明细
     */
    String TITLE_ER = "title_er";
    /**
     * 故障代码
     */
    String TITLE_ERROR_A = "title_error_a";
    /**
     * 故障描述
     */
    String TITLE_ERROR_B = "title_error_b";
    /**
     * 故障现象
     */
    String TITLE_ERROR_C = "title_error_c";
    /**
     * 巡检信息
     */
    String TITLE_ES = "title_es";
    /**
     * 巡检总时长（分钟）
     */
    String TITLE_ET = "title_et";
    /**
     * 压力（MPa）
     */
    String TITLE_EU = "title_eu";
    /**
     * 延迟提交
     */
    String TITLE_EV = "title_ev";
    /**
     * 颜色
     */
    String TITLE_EW = "title_ew";
    /**
     * 颜色配置
     */
    String TITLE_EX = "title_ex";
    /**
     * 颜色值
     */
    String TITLE_EY = "title_ey";
    /**
     * 验证方式
     */
    String TITLE_EZ = "title_ez";
    /**
     * 父类型
     */
    String TITLE_F = "title_f";
    /**
     * 业务编号
     */
    String TITLE_FA = "title_fa";
    /**
     * 业务配置
     */
    String TITLE_FB = "title_fb";
    /**
     * 页面id
     */
    String TITLE_FC = "title_fc";
    /**
     * 页面用字段名
     */
    String TITLE_FD = "title_fd";
    /**
     * 页签
     */
    String TITLE_FE = "title_fe";
    /**
     * 页数（默认）
     */
    String TITLE_FF = "title_ff";
    /**
     * 一级位置必须上传平面图
     */
    String TITLE_FG = "title_fg";
    /**
     * 本月维护时间
     */
    String TITLE_FGH = "title_fgh";
    /**
     * 本周完成
     */
    String TITLE_FGI = "title_fgi";
    /**
     * 本月完成
     */
    String TITLE_FGJ = "title_fgj";
    /**
     * 未完成任务
     */
    String TITLE_FGK = "title_fgk";
    /**
     * 已完成任务
     */
    String TITLE_FGL = "title_fgl";
    /**
     * 故障任务总数
     */
    String TITLE_FGM = "title_fgm";
    /**
     * 维护任务总数
     */
    String TITLE_FGN = "title_fgn";
    /**
     * 移除
     */
    String TITLE_FH = "title_fh";
    /**
     * 故障任务
     */
    String TITLE_FHI = "title_fhi";
    /**
     * 维护任务
     */
    String TITLE_FHJ = "title_fhj";
    /**
     * 提醒时间
     */
    String TITLE_FHK = "title_fhk";
    /**
     * 安全库存提醒时间(小时)
     */
    String TITLE_FHL = "title_fhl";
    /**
     * 全部关闭
     */
    String TITLE_FHM = "title_fhm";
    /**
     * 计划完成率
     */
    String TITLE_FHN = "title_fhn";
    /**
     * 移动
     */
    String TITLE_FI = "title_fi";
    /**
     * 计划任务数量
     */
    String TITLE_FIJ = "title_fij";
    /**
     * 完成任务数量
     */
    String TITLE_FIK = "title_fik";
    /**
     * 今日维护数量
     */
    String TITLE_FIL = "title_fil";
    /**
     * 文件名称
     */
    String TITLE_FILE_A = "title_file_a";
    /**
     * 文件类型
     */
    String TITLE_FILE_B = "title_file_b";
    /**
     * 文件大小(M)
     */
    String TITLE_FILE_C = "title_file_c";
    /**
     * 查看文件
     */
    String TITLE_FILE_D = "title_file_d";
    /**
     * 点击这里选择文件
     */
    String TITLE_FILE_E = "title_file_e";
    /**
     * 你也可以把要上传的文件拖入该窗口
     */
    String TITLE_FILE_F = "title_file_f";
    /**
     * 图片/文件
     */
    String TITLE_FILE_G = "title_file_g";
    /**
     * 文件不能大于5M
     */
    String TITLE_FILE_H = "title_file_h";
    /**
     * 文件说明
     */
    String TITLE_FILE_I = "title_file_i";
    /**
     * 职安健文件
     */
    String TITLE_FILE_J = "title_file_j";
    /**
     * 本周维护数量
     */
    String TITLE_FIM = "title_fim";
    /**
     * 本月维护数量
     */
    String TITLE_FIN = "title_fin";
    /**
     * 异常
     */
    String TITLE_FJ = "title_fj";
    /**
     * 未修复
     */
    String TITLE_FJK = "title_fjk";
    /**
     * 修复进度
     */
    String TITLE_FJL = "title_fjl";
    /**
     * 故障设备数
     */
    String TITLE_FJM = "title_fjm";
    /**
     * 完好设备数
     */
    String TITLE_FJN = "title_fjn";
    /**
     * 异常报警
     */
    String TITLE_FK = "title_fk";
    /**
     * 完成率
     */
    String TITLE_FKL = "title_fkl";
    /**
     * 完好设备率
     */
    String TITLE_FKM = "title_fkm";
    /**
     * 今日故障数
     */
    String TITLE_FKN = "title_fkn";
    /**
     * 异常上-下限
     */
    String TITLE_FL = "title_fl";
    /**
     * 本周故障数
     */
    String TITLE_FLM = "title_flm";
    /**
     * 本月故障数
     */
    String TITLE_FLN = "title_fln";
    /**
     * 异常上限
     */
    String TITLE_FM = "title_fm";
    /**
     * 已修复
     */
    String TITLE_FMN = "title_fmn";
    /**
     * 电子签名
     */
    String TITLE_FMO = "title_fmo";
    /**
     * 异常提交
     */
    String TITLE_FN = "title_fn";
    /**
     * 异常下限
     */
    String TITLE_FO = "title_fo";
    /**
     * 意见
     */
    String TITLE_FP = "title_fp";
    /**
     * 隐藏
     */
    String TITLE_FQ = "title_fq";
    /**
     * 盈余
     */
    String TITLE_FR = "title_fr";
    /**
     * 盈余数量
     */
    String TITLE_FS = "title_fs";
    /**
     * {0}应为大于0的数值
     */
    String TITLE_FT = "title_ft";
    /**
     * 用户
     */
    String TITLE_FU = "title_fu";
    /**
     * 用户标识
     */
    String TITLE_FV = "title_fv";
    /**
     * 用户登录
     */
    String TITLE_FW = "title_fw";
    /**
     * 用户电话
     */
    String TITLE_FX = "title_fx";
    /**
     * 用户管理
     */
    String TITLE_FY = "title_fy";
    /**
     * 用户角色
     */
    String TITLE_FZ = "title_fz";
    /**
     * 自定义字段
     */
    String TITLE_G = "title_g";
    /**
     * 用户列表
     */
    String TITLE_GA = "title_ga";
    /**
     * 用户名
     */
    String TITLE_GB = "title_gb";
    /**
     * 用户账号
     */
    String TITLE_GC = "title_gc";
    /**
     * 用户注册
     */
    String TITLE_GD = "title_gd";
    /**
     * 用户注册链接
     */
    String TITLE_GE = "title_ge";
    /**
     * 用户状态
     */
    String TITLE_GF = "title_gf";
    /**
     * 用户组
     */
    String TITLE_GG = "title_gg";
    /**
     * 用量
     */
    String TITLE_GH = "title_gh";
    /**
     * 创建工单
     */
    String TITLE_GHJ = "title_ghj";
    /**
     * 设备管理
     */
    String TITLE_GHK = "title_ghk";
    /**
     * 备件管理
     */
    String TITLE_GHL = "title_ghl";
    /**
     * 工单列表
     */
    String TITLE_GHM = "title_ghm";
    /**
     * 设备台账
     */
    String TITLE_GHN = "title_ghn";
    /**
     * 用量报告
     */
    String TITLE_GI = "title_gi";
    /**
     * 职安健管理
     */
    String TITLE_GIH = "title_gih";
    /**
     * 职安健新增
     */
    String TITLE_GII = "title_gii";
    /**
     * 备件台账
     */
    String TITLE_GIJ = "title_gij";
    /**
     * 职安健详情
     */
    String TITLE_GIM = "title_gim";
    /**
     * 批量作废
     */
    String TITLE_GIN = "title_gin";
    /**
     * 用量偏差
     */
    String TITLE_GJ = "title_gj";
    /**
     * 用途
     */
    String TITLE_GK = "title_gk";
    /**
     * 用油点
     */
    String TITLE_GL = "title_gl";
    /**
     * 优先级
     */
    String TITLE_GM = "title_gm";
    /**
     * 邮件报修
     */
    String TITLE_GN = "title_gn";
    /**
     * 邮件地址
     */
    String TITLE_GO = "title_go";
    /**
     * 邮件发送
     */
    String TITLE_GP = "title_gp";
    /**
     * 邮件发送人
     */
    String TITLE_GQ = "title_gq";
    /**
     * 邮件接收人配置
     */
    String TITLE_GR = "title_gr";
    /**
     * 邮件模板
     */
    String TITLE_GS = "title_gs";
    /**
     * 邮件模板配置
     */
    String TITLE_GT = "title_gt";
    /**
     * 邮件配置
     */
    String TITLE_GU = "title_gu";
    /**
     * 邮件主机
     */
    String TITLE_GV = "title_gv";
    /**
     * 邮箱
     */
    String TITLE_GW = "title_gw";
    /**
     * 邮箱端口
     */
    String TITLE_GX = "title_gx";
    /**
     * 邮箱密码
     */
    String TITLE_GY = "title_gy";
    /**
     * 邮箱名
     */
    String TITLE_GZ = "title_gz";
    /**
     * 字段ID
     */
    String TITLE_H = "title_h";
    /**
     * 油量（L）
     */
    String TITLE_HA = "title_ha";
    /**
     * 油品品牌
     */
    String TITLE_HB = "title_hb";
    /**
     * 油品型号
     */
    String TITLE_HC = "title_hc";
    /**
     * 油位是否达标
     */
    String TITLE_HD = "title_hd";
    /**
     * 有效性
     */
    String TITLE_HE = "title_he";
    /**
     * 有效用户
     */
    String TITLE_HF = "title_hf";
    /**
     * 右
     */
    String TITLE_HG = "title_hg";
    /**
     * 逾期
     */
    String TITLE_HH = "title_hh";
    /**
     * 逾期任务
     */
    String TITLE_HI = "title_hi";
    /**
     * 预测性故障
     */
    String TITLE_HJ = "title_hj";
    /**
     * 预测性维护
     */
    String TITLE_HK = "title_hk";
    /**
     * 预防措施
     */
    String TITLE_HL = "title_hl";
    /**
     * 预计
     */
    String TITLE_HM = "title_hm";
    /**
     * 预计（天）
     */
    String TITLE_HN = "title_hn";
    /**
     * 预计到达
     */
    String TITLE_HO = "title_ho";
    /**
     * 预计到达时间
     */
    String TITLE_HP = "title_hp";
    /**
     * 预计费用
     */
    String TITLE_HQ = "title_hq";
    /**
     * 预计工时
     */
    String TITLE_HR = "title_hr";
    /**
     * 预计工作内容
     */
    String TITLE_HS = "title_hs";
    /**
     * 预计时间
     */
    String TITLE_HT = "title_ht";
    /**
     * 预计维护时间
     */
    String TITLE_HU = "title_hu";
    /**
     * 预警
     */
    String TITLE_HV = "title_hv";
    /**
     * 预览
     */
    String TITLE_HW = "title_hw";
    /**
     * 员工
     */
    String TITLE_HX = "title_hx";
    /**
     * 员工保养效率
     */
    String TITLE_HY = "title_hy";
    /**
     * 员工编码
     */
    String TITLE_HZ = "title_hz";
    /**
     * 显示控件
     */
    String TITLE_I = "title_i";
    /**
     * 员工维保时效分析
     */
    String TITLE_IA = "title_ia";
    /**
     * 员工效率
     */
    String TITLE_IB = "title_ib";
    /**
     * 员工姓名
     */
    String TITLE_IC = "title_ic";
    /**
     * 原件
     */
    String TITLE_ID = "title_id";
    /**
     * 原密码
     */
    String TITLE_IE = "title_ie";
    /**
     * 原所属位置
     */
    String TITLE_IF = "title_if";
    /**
     * 远程协助
     */
    String TITLE_IG = "title_ig";
    /**
     * 月
     */
    String TITLE_IH = "title_ih";
    /**
     * 月报表
     */
    String TITLE_II = "title_ii";
    /**
     * 月度
     */
    String TITLE_IJ = "title_ij";
    /**
     * 月度重要事项
     */
    String TITLE_IK = "title_ik";
    /**
     * 月份
     */
    String TITLE_IL = "title_il";
    /**
     * 月份不能超过12
     */
    String TITLE_IM = "title_im";
    /**
     * 月计划
     */
    String TITLE_IN = "title_in";
    /**
     * 安装日期
     */
    String TITLE_INSTALL_B = "title_install_b";
    /**
     * 月累计
     */
    String TITLE_IO = "title_io";
    /**
     * 允许多选
     */
    String TITLE_IP = "title_ip";
    /**
     * 问题确认
     */
    String TITLE_IQ = "title_iq";
    /**
     * 运行人员
     */
    String TITLE_IR = "title_ir";
    /**
     * 运行时维修</br>时效（分钟）
     */
    String TITLE_IS = "title_is";
    /**
     * 运行时维修</br>总次数
     */
    String TITLE_IT = "title_it";
    /**
     * 运行时维修</br>总时间（分钟）
     */
    String TITLE_IU = "title_iu";
    /**
     * 运行时维修时效（分钟）
     */
    String TITLE_IV = "title_iv";
    /**
     * 运行时维修信息
     */
    String TITLE_IW = "title_iw";
    /**
     * 运行时维修总次数
     */
    String TITLE_IX = "title_ix";
    /**
     * 运行时维修总时间（分钟）
     */
    String TITLE_IY = "title_iy";
    /**
     * 运行中
     */
    String TITLE_IZ = "title_iz";
    /**
     * 可否为空
     */
    String TITLE_J = "title_j";
    /**
     * 运送量
     */
    String TITLE_JA = "title_ja";
    /**
     * 再次输入密码
     */
    String TITLE_JB = "title_jb";
    /**
     * 再次输入新密码
     */
    String TITLE_JC = "title_jc";
    /**
     * 再分配
     */
    String TITLE_JD = "title_jd";
    /**
     * 在计划发布后多少天完成
     */
    String TITLE_JE = "title_je";
    /**
     * 在库
     */
    String TITLE_JF = "title_jf";
    /**
     * 在库状态
     */
    String TITLE_JG = "title_jg";
    /**
     * 在库总数
     */
    String TITLE_JH = "title_jh";
    /**
     * 在线
     */
    String TITLE_JI = "title_ji";
    /**
     * 在线（Online）
     */
    String TITLE_JJ = "title_jj";
    /**
     * 责任
     */
    String TITLE_JK = "title_jk";
    /**
     * 责任人
     */
    String TITLE_JL = "title_jl";
    /**
     * 摘要
     */
    String TITLE_JM = "title_jm";
    /**
     * 展示控件
     */
    String TITLE_JN = "title_jn";
    /**
     * 张三
     */
    String TITLE_JO = "title_jo";
    /**
     * 长文本
     */
    String TITLE_JP = "title_jp";
    /**
     * 账号
     */
    String TITLE_JQ = "title_jq";
    /**
     * 照片
     */
    String TITLE_JR = "title_jr";
    /**
     * 这是初始地图
     */
    String TITLE_JS = "title_js";
    /**
     * 诊断图谱
     */
    String TITLE_JT = "title_jt";
    /**
     * 整改
     */
    String TITLE_JU = "title_ju";
    /**
     * 整洁评分
     */
    String TITLE_JV = "title_jv";
    /**
     * 整数
     */
    String TITLE_JW = "title_jw";
    /**
     * 整型
     */
    String TITLE_JX = "title_jx";
    /**
     * 正常
     */
    String TITLE_JY = "title_jy";
    /**
     * 正常运行
     */
    String TITLE_JZ = "title_jz";
    /**
     * 启用/禁用
     */
    String TITLE_K = "title_k";
    /**
     * 证书编号
     */
    String TITLE_KA = "title_ka";
    /**
     * 支援人员
     */
    String TITLE_KB = "title_kb";
    /**
     * 只读
     */
    String TITLE_KC = "title_kc";
    /**
     * 只看自己
     */
    String TITLE_KD = "title_kd";
    /**
     * 只能输入整数
     */
    String TITLE_KE = "title_ke";
    /**
     * 知识库
     */
    String TITLE_KF = "title_kf";
    /**
     * 执行
     */
    String TITLE_KG = "title_kg";
    /**
     * 执行岗位
     */
    String TITLE_KH = "title_kh";
    /**
     * 执行过程
     */
    String TITLE_KI = "title_ki";
    /**
     * 执行模版
     */
    String TITLE_KJ = "title_kj";
    /**
     * 直接单位
     */
    String TITLE_KK = "title_kk";
    /**
     * 直接上报
     */
    String TITLE_KL = "title_kl";
    /**
     * 值
     */
    String TITLE_KM = "title_km";
    /**
     * 值基于主键
     */
    String TITLE_KN = "title_kn";
    /**
     * 值基于字段
     */
    String TITLE_KO = "title_ko";
    /**
     * 值无效
     */
    String TITLE_KP = "title_kp";
    /**
     * 职安健
     */
    String TITLE_KQ = "title_kq";
    /**
     * 职安健审批
     */
    String TITLE_KR = "title_kr";
    /**
     * 职称
     */
    String TITLE_KS = "title_ks";
    /**
     * 职务角色
     */
    String TITLE_KT = "title_kt";
    /**
     * 职责
     */
    String TITLE_KU = "title_ku";
    /**
     * 职责id
     */
    String TITLE_KV = "title_kv";
    /**
     * 职责为
     */
    String TITLE_KW = "title_kw";
    /**
     * 指标
     */
    String TITLE_KX = "title_kx";
    /**
     * 至今两天
     */
    String TITLE_KY = "title_ky";
    /**
     * 至今三天
     */
    String TITLE_KZ = "title_kz";
    /**
     * 数据来源
     */
    String TITLE_L = "title_l";
    /**
     * 质保服务
     */
    String TITLE_LA = "title_la";
    /**
     * 质量缺陷
     */
    String TITLE_LB = "title_lb";
    /**
     * 置顶
     */
    String TITLE_LC = "title_lc";
    /**
     * 中
     */
    String TITLE_LD = "title_ld";
    /**
     * 中文
     */
    String TITLE_LE = "title_le";
    /**
     * 重要级别
     */
    String TITLE_LEVEL_A = "title_level_a";
    /**
     * 客户类型
     */
    String TITLE_ACEI = "title_acei";
    /**
     * 并管排污分组
     */
    String TITLE_ACFH = "title_acfh";
    /**
     * 中文名
     */
    String TITLE_LF = "title_lf";
    /**
     * 重新分配执行人
     */
    String TITLE_LG = "title_lg";
    /**
     * 重新提交
     */
    String TITLE_LH = "title_lh";
    /**
     * 重要任务
     */
    String TITLE_LI = "title_li";
    /**
     * 周
     */
    String TITLE_LJ = "title_lj";
    /**
     * 周二
     */
    String TITLE_LK = "title_lk";
    /**
     * 周计划
     */
    String TITLE_LL = "title_ll";
    /**
     * 周六
     */
    String TITLE_LM = "title_lm";
    /**
     * 周期
     */
    String TITLE_LN = "title_ln";
    /**
     * 周期模式
     */
    String TITLE_LO = "title_lo";
    /**
     * 所在位置
     */
    String TITLE_LOCA_A = "title_loca_a";
    /**
     * 周期频率
     */
    String TITLE_LP = "title_lp";
    /**
     * 周日
     */
    String TITLE_LQ = "title_lq";
    /**
     * 周三
     */
    String TITLE_LR = "title_lr";
    /**
     * 周四
     */
    String TITLE_LS = "title_ls";
    /**
     * 周五
     */
    String TITLE_LT = "title_lt";
    /**
     * 周一
     */
    String TITLE_LU = "title_lu";
    /**
     * 周重要事项
     */
    String TITLE_LV = "title_lv";
    /**
     * 轴承故障
     */
    String TITLE_LW = "title_lw";
    /**
     * 轴承故障次数
     */
    String TITLE_LX = "title_lx";
    /**
     * 轴承故障率
     */
    String TITLE_LY = "title_ly";
    /**
     * 轴承故障总时间（分钟）
     */
    String TITLE_LZ = "title_lz";
    /**
     * 全部
     */
    String TITLE_M = "title_m";
    /**
     * 主观平均分
     */
    String TITLE_MA = "title_ma";
    /**
     * 主观评分
     */
    String TITLE_MB = "title_mb";
    /**
     * 主观指标
     */
    String TITLE_MC = "title_mc";
    /**
     * 主题
     */
    String TITLE_MD = "title_md";
    /**
     * 主要维护内容总结
     */
    String TITLE_ME = "title_me";
    /**
     * 注册码
     */
    String TITLE_MF = "title_mf";
    /**
     * 制造商
     */
    String TITLE_MFR_A = "title_mfr_a";
    /**
     * 注册时间
     */
    String TITLE_MG = "title_mg";
    /**
     * 注销
     */
    String TITLE_MH = "title_mh";
    /**
     * 专业评分
     */
    String TITLE_MI = "title_mi";
    /**
     * 转交人
     */
    String TITLE_MJ = "title_mj";
    /**
     * 转派
     */
    String TITLE_MK = "title_mk";
    /**
     * 转派时执行人不能为自己
     */
    String TITLE_ML = "title_ml";
    /**
     * 转弯机故障率
     */
    String TITLE_MM = "title_mm";
    /**
     * 转弯机故障率（%）
     */
    String TITLE_MN = "title_mn";
    /**
     * 转专家处理
     */
    String TITLE_MO = "title_mo";
    /**
     * 状态
     */
    String TITLE_MP = "title_mp";
    /**
     * 追踪明细
     */
    String TITLE_MQ = "title_mq";
    /**
     * 资产
     */
    String TITLE_MR = "title_mr";
    /**
     * 资产维保人
     */
    String TITLE_MS = "title_ms";
    /**
     * 资产维保人员
     */
    String TITLE_MT = "title_mt";
    /**
     * 资产位置
     */
    String TITLE_MU = "title_mu";
    /**
     * 资料
     */
    String TITLE_MV = "title_mv";
    /**
     * 子工单
     */
    String TITLE_MW = "title_mw";
    /**
     * 子工单模版
     */
    String TITLE_MX = "title_mx";
    /**
     * 子工单信息
     */
    String TITLE_MY = "title_my";
    /**
     * 子页面信息
     */
    String TITLE_MZ = "title_mz";
    /**
     * 工单详情字段扩展
     */
    String TITLE_N = "title_n";
    /**
     * 自定义解析
     */
    String TITLE_NA = "title_na";
    /**
     * 计划名称
     */
    String TITLE_NAME_A = "title_name_a";
    /**
     * 门店名称
     */
    String TITLE_NAME_AB_A = "title_name_ab_a";
    /**
     * 名称
     */
    String TITLE_NAME_AB_B = "title_name_ab_b";
    /**
     * 模板名称
     */
    String TITLE_NAME_AB_C = "title_name_ab_c";
    /**
     * 盘点名称
     */
    String TITLE_NAME_AB_D = "title_name_ab_d";
    /**
     * 品牌名称
     */
    String TITLE_NAME_AB_E = "title_name_ab_e";
    /**
     * 企业名称
     */
    String TITLE_NAME_AB_F = "title_name_ab_f";
    /**
     * 区域名称
     */
    String TITLE_NAME_AB_G = "title_name_ab_g";
    /**
     * 权限名称
     */
    String TITLE_NAME_AB_H = "title_name_ab_h";
    /**
     * 任务名称
     */
    String TITLE_NAME_AB_I = "title_name_ab_i";
    /**
     * 任务项名称
     */
    String TITLE_NAME_AB_J = "title_name_ab_j";
    /**
     * 图片名称
     */
    String TITLE_NAME_AB_K = "title_name_ab_k";
    /**
     * 位置名称
     */
    String TITLE_NAME_AB_L = "title_name_ab_l";
    /**
     * 系统名称
     */
    String TITLE_NAME_AB_M = "title_name_ab_m";
    /**
     * 项目名称
     */
    String TITLE_NAME_AB_N = "title_name_ab_n";
    /**
     * 型号名称
     */
    String TITLE_NAME_AB_O = "title_name_ab_o";
    /**
     * 预计更换备件名称
     */
    String TITLE_NAME_AB_P = "title_name_ab_p";
    /**
     * 员工名称
     */
    String TITLE_NAME_AB_Q = "title_name_ab_q";
    /**
     * 字段名称
     */
    String TITLE_NAME_AB_R = "title_name_ab_r";
    /**
     * 组件名称
     */
    String TITLE_NAME_AB_S = "title_name_ab_s";
    /**
     * 组名称
     */
    String TITLE_ABKL = "title_abkl";
    /**
     * 组织名称
     */
    String TITLE_NAME_AB_U = "title_name_ab_u";
    /**
     * 类型名称
     */
    String TITLE_NAME_B = "title_name_b";
    /**
     * 显示名称
     */
    String TITLE_NAME_C = "title_name_c";
    /**
     * 名称类型
     */
    String TITLE_NAME_D = "title_name_d";
    /**
     * 报告名称
     */
    String TITLE_NAME_E = "title_name_e";
    /**
     * 备件名称
     */
    String TITLE_NAME_F = "title_name_f";
    /**
     * 部件名称
     */
    String TITLE_NAME_G = "title_name_g";
    /**
     * 部署名称
     */
    String TITLE_NAME_H = "title_name_h";
    /**
     * 部位名称
     */
    String TITLE_NAME_I = "title_name_i";
    /**
     * 仓库名称
     */
    String TITLE_NAME_J = "title_name_j";
    /**
     * 查询条件名称
     */
    String TITLE_NAME_K = "title_name_k";
    /**
     * 产品名称
     */
    String TITLE_NAME_L = "title_name_l";
    /**
     * 厂商名称
     */
    String TITLE_NAME_M = "title_name_m";
    /**
     * 代码名称
     */
    String TITLE_NAME_N = "title_name_n";
    /**
     * 地址名称
     */
    String TITLE_NAME_O = "title_name_o";
    /**
     * 点巡检点名称
     */
    String TITLE_NAME_P = "title_name_p";
    /**
     * 点巡检名称
     */
    String TITLE_NAME_Q = "title_name_q";
    /**
     * 分拨名称
     */
    String TITLE_NAME_R = "title_name_r";
    /**
     * 工单池名称
     */
    String TITLE_NAME_S = "title_name_s";
    /**
     * 工具名称
     */
    String TITLE_NAME_T = "title_name_t";
    /**
     * 功能名称
     */
    String TITLE_NAME_U = "title_name_u";
    /**
     * 交付物名称
     */
    String TITLE_NAME_V = "title_name_v";
    /**
     * 角色名称
     */
    String TITLE_NAME_W = "title_name_w";
    /**
     * 客户名称
     */
    String TITLE_NAME_X = "title_name_x";
    /**
     * 库房名称
     */
    String TITLE_NAME_Y = "title_name_y";
    /**
     * 流程名称
     */
    String TITLE_NAME_Z = "title_name_z";
    /**
     * 自定义模板
     */
    String TITLE_NB = "title_nb";
    /**
     * 自动计划
     */
    String TITLE_NC = "title_nc";
    /**
     * 自动生成的ID号
     */
    String TITLE_ND = "title_nd";
    /**
     * 自修
     */
    String TITLE_NE = "title_ne";
    /**
     * 自主保养
     */
    String TITLE_NF = "title_nf";
    /**
     * 字段
     */
    String TITLE_NG = "title_ng";
    /**
     * 字段标题
     */
    String TITLE_NH = "title_nh";
    /**
     * 字段配置
     */
    String TITLE_NI = "title_ni";
    /**
     * 字符串
     */
    String TITLE_NJ = "title_nj";
    /**
     * 总得分
     */
    String TITLE_NK = "title_nk";
    /**
     * 总的故障率（%）
     */
    String TITLE_NL = "title_nl";
    /**
     * 总费用
     */
    String TITLE_NM = "title_nm";
    /**
     * 总分
     */
    String TITLE_NN = "title_nn";
    /**
     * 总工时
     */
    String TITLE_NO = "title_no";
    /**
     * 备注
     */
    String TITLE_NOTE_A = "title_note_a";
    /**
     * 总工时/天
     */
    String TITLE_NP = "title_np";
    /**
     * 总故障</br>时间（分钟）
     */
    String TITLE_NQ = "title_nq";
    /**
     * 总故障次数
     */
    String TITLE_NR = "title_nr";
    /**
     * 总故障时间（分钟）
     */
    String TITLE_NS = "title_ns";
    /**
     * 总故障时间</br>（分钟）
     */
    String TITLE_NT = "title_nt";
    /**
     * 总故障信息
     */
    String TITLE_NU = "title_nu";
    /**
     * 总计
     */
    String TITLE_NV = "title_nv";
    /**
     * 总价
     */
    String TITLE_NW = "title_nw";
    /**
     * 总金额
     */
    String TITLE_NX = "title_nx";
    /**
     * 总库存
     */
    String TITLE_NY = "title_ny";
    /**
     * 总权重
     */
    String TITLE_NZ = "title_nz";
    /**
     * 总时间（分钟）
     */
    String TITLE_OA = "title_oa";
    /**
     * 总数
     */
    String TITLE_OB = "title_ob";
    /**
     * 总用量
     */
    String TITLE_OC = "title_oc";
    /**
     * 总长度
     */
    String TITLE_OD = "title_od";
    /**
     * 组
     */
    String TITLE_OE = "title_oe";
    /**
     * 组编码
     */
    String TITLE_OF = "title_of";
    /**
     * 组管理
     */
    String TITLE_OG = "title_og";
    /**
     * 组件个数
     */
    String TITLE_OH = "title_oh";
    /**
     * 组织
     */
    String TITLE_OI = "title_oi";
    /**
     * 组织编码
     */
    String TITLE_OJ = "title_oj";
    /**
     * 组织代码
     */
    String TITLE_OK = "title_ok";
    /**
     * 组织管理
     */
    String TITLE_OL = "title_ol";
    /**
     * 组织列表
     */
    String TITLE_OM = "title_om";
    /**
     * 组织群组
     */
    String TITLE_ON = "title_on";
    /**
     * 最大安全库存
     */
    String TITLE_OO = "title_oo";
    /**
     * 最大库存
     */
    String TITLE_OP = "title_op";
    /**
     * 最大值
     */
    String TITLE_OQ = "title_oq";
    /**
     * 最小库存
     */
    String TITLE_OR = "title_or";
    /**
     * 最小值
     */
    String TITLE_OS = "title_os";
    /**
     * 最终得分
     */
    String TITLE_OT = "title_ot";
    /**
     * 左
     */
    String TITLE_OU = "title_ou";
    /**
     * 作废
     */
    String TITLE_OV = "title_ov";
    /**
     * 作为一级菜单
     */
    String TITLE_OW = "title_ow";
    /**
     * 作者
     */
    String TITLE_OX = "title_ox";
    /**
     * 座机号
     */
    String TITLE_OY = "title_oy";
    /**
     * 赋予
     */
    String TITLE_PA = "title_pa";
    /**
     * 单位管理
     */
    String TITLE_PB = "title_pb";
    /**
     * 货币种类
     */
    String TITLE_PC = "title_pc";
    /**
     * 错误码
     */
    String TITLE_PD = "title_pd";
    /**
     * 克隆
     */
    String TITLE_PE = "title_pe";
    /**
     * 岗位
     */
    String TITLE_PF = "title_pf";
    /**
     * 子部门
     */
    String TITLE_PG = "title_pg";
    /**
     * 短信
     */
    String TITLE_PH = "title_ph";
    /**
     * 验证码
     */
    String TITLE_PI = "title_pi";
    /**
     * 货币符号
     */
    String TITLE_PJ = "title_pj";
    /**
     * 基础类型
     */
    String TITLE_PK = "title_pk";
    /**
     * 管理方式
     */
    String TITLE_PL = "title_pl";
    /**
     * 计划时间
     */
    String TITLE_PLAN_A = "title_plan_a";
    /**
     * 备件类型
     */
    String TITLE_PM = "title_pm";
    /**
     * 基础配置
     */
    String TITLE_PN = "title_pn";
    /**
     * 维保配置
     */
    String TITLE_PO = "title_po";
    /**
     * 其他配置
     */
    String TITLE_PP = "title_pp";
    /**
     * 供货周期(天)
     */
    String TITLE_PQ = "title_pq";
    /**
     * 厂商编码
     */
    String TITLE_PR = "title_pr";
    /**
     * 税前价格
     */
    String TITLE_PRICE_A = "title_price_a";
    /**
     * 安装价格
     */
    String TITLE_PRICE_B = "title_price_b";
    /**
     * 厂商新增
     */
    String TITLE_PS = "title_ps";
    /**
     * 厂商详情
     */
    String TITLE_PT = "title_pt";
    /**
     * 厂商编辑
     */
    String TITLE_PU = "title_pu";
    /**
     * 厂商简称
     */
    String TITLE_PV = "title_pv";
    /**
     * 上级厂商
     */
    String TITLE_PW = "title_pw";
    /**
     * 所在地址
     */
    String TITLE_PX = "title_px";
    /**
     * 所在片区
     */
    String TITLE_ACFI = "title_acfi";
    /**
     * 管道编号
     */
    String TITLE_ACFJ = "title_acfj";
    /**
     * 污水去向
     */
    String TITLE_ACFK = "title_acfk";
    /**
     * 主要产品
     */
    String TITLE_ACFL = "title_acfl";
    /**
     * 收集方式
     */
    String TITLE_AEHN = "title_aehn";
    /**
     * 排污阈值
     */
    String TITLE_BDFH = "title_bdfh";
    /**
     * 警告阈值
     */
    String TITLE_BDFI = "title_bdfi";
    /**
     * 异常阈值
     */
    String TITLE_BDFJ = "title_bdfj";
    /**
     * 数仓客户id
     */
    String TITLE_BDEM = "title_bdem";
    /**
     * 纳管标准限制
     */
    String TITLE_ACHI = "title_achi";
    /**
     * 依据
     */
    String TITLE_AEIL = "title_aeil";
    /**
     * 排污开始时间
     */
    String TITLE_ACHJ = "title_achj";
    /**
     * 排污结束时间
     */
    String TITLE_ACHK = "title_achk";
    /**
     * 交易币种
     */
    String TITLE_PY = "title_py";
    /**
     * 单选列表
     */
    String TITLE_PZ = "title_pz";
    /**
     * 多选列表
     */
    String TITLE_QA = "title_qa";
    /**
     * 单选
     */
    String TITLE_QB = "title_qb";
    /**
     * 复选
     */
    String TITLE_QC = "title_qc";
    /**
     * 唯一键
     */
    String TITLE_QD = "title_qd";
    /**
     * 所在区块
     */
    String TITLE_QE = "title_qe";
    /**
     * 控件类型
     */
    String TITLE_QF = "title_qf";
    /**
     * 设备信息
     */
    String TITLE_QG = "title_qg";
    /**
     * 报修信息
     */
    String TITLE_QH = "title_qh";
    /**
     * 备件信息
     */
    String TITLE_QI = "title_qi";
    /**
     * 上报信息
     */
    String TITLE_QJ = "title_qj";
    /**
     * 二维码
     */
    String TITLE_QR_A = "title_qr_a";
    /**
     * {0}二维码
     */
    String TITLE_QR_B = "title_qr_b";
    /**
     * 维修方案
     */
    String TITLE_REPAIR_A = "title_repair_a";
    /**
     * 上报时间
     */
    String TITLE_RPT_A = "title_rpt_a";
    /**
     * 运行状态
     */
    String TITLE_STATUS_A = "title_status_a";
    /**
     * 资产状态
     */
    String TITLE_STATUS_B = "title_status_b";
    /**
     * 保固状态
     */
    String TITLE_STATUS_C = "title_status_c";
    /**
     * 主题内容
     */
    String TITLE_SUBJECT_A = "title_subject_a";
    /**
     * 供应商
     */
    String TITLE_SUPPLIER_A = "title_supplier_a";
    /**
     * 税率
     */
    String TITLE_TAX_A = "title_tax_a";
    /**
     * 税费
     */
    String TITLE_TAX_B = "title_tax_b";
    /**
     * 设备寿命
     */
    String TITLE_USE_A = "title_use_a";
    /**
     * 设备位置权限
     */
    String TITLE_HIN = "title_hin";
    /**
     * 设备类型权限
     */
    String COM_B = "com_b";
    /**
     * 工单类型权限
     */
    String COM_C = "com_c";
    /**
     * 创建工单权限
     */
    String COM_D = "com_d";
    /**
     * 请联系管理员调整页面
     */
    String LOG_B = "log_b";
    /**
     * 系统忙，请稍后再试
     */
    String LOG_C = "log_c";
    /**
     * 数据过多请修改条件后重试！
     */
    String MSG_J = "msg_j";
    /**
     * 解绑
     */
    String BTN_BI = "btn_bi";
    /**
     * 上锁小组
     */
    String TITLE_NAME_AB_T = "title_name_ab_t";
    /**
     * 解绑失败，只能用户绑定的微信用户才能解绑
     */
    String LOG_AD = "log_ad";
    /**
     * 模板状态
     */
    String TITLE_BCHN = "title_bchn";
    /**
     * 模板描述
     */
    String TITLE_BCIJ = "title_bcij";
    /**
     * 设置
     */
    String BTN_CI = "btn_ci";
    /**
     * 更新
     */
    String BTN_CJ = "btn_cj";
    /**
     * 请联系管理员调整页面：{d}
     */
    String MSG_HJ = "msg_hj";
}
