package com.gengyun.senscloud.common;

public enum WaterQualityEnum {

    /**
     * 正常
     */
    NORMAL("1"),
    /**
     * 超标
     */
    EXCEED("2");

    private String key;

    WaterQualityEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
