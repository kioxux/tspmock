package com.gengyun.senscloud.common;

/**
 * DataPermissionForPosition interface 用户设备位置数据权限接口 后期可能会修改
 *
 * @author Zys
 * @date 2020/03/13
 */
public interface DataPermissionForPosition {

//    //权限标识符  所有位置权限
//    String ALL_FACILITY = "_all_facility";
//
//    //权限标识符  所在位置权限
//    String SELF_FACILITY = "_self_facility";
//
//    /**
//     * 获取用户权限内的设备位置编码
//     *
//     * @param schema_name
//     * @param currentUser
//     * @param positions
//     * @param businessKey
//     * @return
//     */
//    String findPositionCodesByBusinessForSingleUser(String schema_name, User currentUser, String[] positions, String businessKey);
//
//
//    /**
//     * 获取用户权限内的设备位置编码
//     *
//     * @param schema_name
//     * @param currentUser
//     * @param positions
//     * @param businessKey
//     * @param businessAllFunctionKey
//     * @param businessSelfFunctionKey
//     * @return
//     */
//    String findPositionCodesByBusinessFunctionKeyForSingleUser(String schema_name, User currentUser, String[] positions, String businessKey, String businessAllFunctionKey, String businessSelfFunctionKey);
//
//    /**
//     * 获取用户权限内的设备位置编码数组
//     *
//     * @param schema_name
//     * @param currentUser
//     * @param positions
//     * @param businessKey
//     * @return
//     */
//    String[] findPositionCodesByBusinessForUser(String schema_name, User currentUser, String[] positions, String businessKey);
//
//    /**
//     * 获取用户权限内的设备位置编码数组
//     *
//     * @param schema_name
//     * @param currentUser
//     * @param positions
//     * @param businessKey
//     * @param businessAllFunctionKey
//     * @param businessSelfFunctionKey
//     * @return
//     */
//    String[] findPositionCodesByBusinessFunctionKeyUser(String schema_name, User currentUser, String[] positions, String businessKey, String businessAllFunctionKey, String businessSelfFunctionKey);


}
