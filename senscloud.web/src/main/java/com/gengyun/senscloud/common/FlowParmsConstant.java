package com.gengyun.senscloud.common;

/**
 * 流程自定义参数key
 */
public class FlowParmsConstant {
    /**
     * 创建时间
     */
    public final static String CREATE_TIME = "create_time";
    /**
     * 创建人
     */
    public final static String CREATE_USER_ACCOUNT = "create_user_account";
    /**
     * 创建人id
     */
    public final static String CREATE_USER_ID = "create_user_id";
    /**
     * 工单类型id
     */
    public final static String WORK_TYPE_ID = "work_type_id";
    /**
     * 工单类型
     */
    public final static String WORK_TYPE = "work_type";
    /**
     * 接收人account
     */
    public final static String RECEIVE_ACCOUNT = "receive_account";
    /**
     * 接受人id
     */
    public final static String RECEIVE_USER_ID = "receive_user_id";
    /**
     * 下一步处理人
     */
    public final static String NEXT_USER_ID = "next_user_id";
    /**
     * 备件采购需求编码
     */
    public final static String BOM_NEED_WORK_CODE = "bom_need_work_code";
    /**
     * 处理人id
     */
    public final static String DUTY_USER_ID = "duty_user_id";
    /**
     * 工单截止时间
     */
    public final static String DEADLINE_TIME = "deadline_time";
    /**
     * 工单标题
     */
    public final static String TITLE_PAGE = "title_page";
    /**
     * 工单状态
     */
    public final static String STATUS = "status";
    /**
     * 工单编号
     */
    public final static String WORK_CODE = "work_code";
    /**
     * 子工单编号
     */
    public final static String SUB_WORK_CODE = "sub_work_code";
    /**
     * 主工单编号
     */
    public final static String MAIN_SUB_WORK_CODE = "main_sub_work_code";
    /**
     * 流程编码
     */
    public final static String PDEID = "pdeId";
    /**
     *
     */
    public final static String PREF_ID = "pref_id";
    /**
     * 业务类型id
     */
    public final static String BUSINESS_TYPE_ID = "business_type_id";
    /**
     * 流程节点显示名称
     */
    public final static String FLOW_NODE_SHOW_NAME = "flow_node_show_name";
    /**
     * 按钮
     */
    public final static String DO_FLOW_KEY = "do_flow_key";
    /**
     *
     */
    public final static String DO_BRANCH_KEY = "do_branch_key";
    /**
     * 紧急程度
     */
    public final static String PRIORITY_LEVEL = "priority_level";
    /**
     * 设备位置
     */
    public final static String POSITION_NAME = "position_name";
    /**
     * 设备位置编码
     */
    public final static String POSITION_CODE = "position_code";
    /**
     * 调往设备位置编码
     */
    public final static String NEW_POSITION_CODE = "new_position_code";

    /**
     * 请求参数
     */
    public final static String REQUEST_PARAM = "request_param";
    /**
     * 计划开始时间
     */
    public final static String PLAN_BEGIN_TIME = "plan_begin_time";

    /**
     * 开始时间
     */
    public final static String BEGIN_TIME = "begin_time";
    /**
     * 子工单信息
     */
    public final static String FLOW_DATA = "flow_data";
    /**
     * 附件
     */
    public final static String ATTACHFILE = "attachfile";
    /**
     * 操作 1 新增设备信息 2更新设备信息  3新增附件信息 4更新附件信息（设备验收用）
     */
    public final static String OPERATION_TYPE = "operation_type";
    /**
     * 维保行事历编码
     */
    public final static String WORK_CALENDAR_CODE = "work_calendar_code";
    /**
     * 设备列表
     */
    public final static String ASSET_LIST = "asset_list";
}
