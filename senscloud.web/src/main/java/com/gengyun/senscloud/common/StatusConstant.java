package com.gengyun.senscloud.common;

public interface StatusConstant {
    // -------------------------------------------------------------数据状态-start
    /**
     * 正常状态
     */
    public final static int STATUS_ENABLE = 1;
    /**
     * 删除（逻辑删除）
     */
    public final static int STATUS_DELETEED = -1000;
    /**
     * 注销的
     */
    public final static int STATUS_CANCELLED = -2000;
    // -------------------------------------------------------------数据状态-end
    // -------------------------------------------------------------订单状态-start
    /**
     * 草稿（维修保存未提交时）
     */
    public final static int DRAFT = 10;
    /**
     * 待确认（工单请求确认）
     */
    public final static int TO_BE_CONFIRM = 15;
    /**
     * 待分配（维修未找到维修人时）
     */
    public final static int UNDISTRIBUTED = 20;
    /**
     * 待接单（维修人未接单时）
     */
    public final static int PENDING_ORDER = 30;
    /**
     * 处理中（维修人接单时，保养点击计时后）
     */
    public final static int PENDING = 40;
    /**
     * 待审核（维修完提交审核后）
     */
    public final static int TO_BE_AUDITED = 50;
    /**
     * 完工报告（维修完提交审核后）
     */
    public final static int TO_REPORT = 55;
    /**
     * 已完成（维修完成，保养完成，巡检任务完成）
     */
    public final static int COMPLETED = 60;
    /**
     * 已入库
     */
    public final static int WAREHOUSED = 65;
    /**
     * 待保养（保养单生成后）
     */
    public final static int TO_BE_MAINTAINED = 70;
    /**
     * 待确认（保养完成，提交确认）
     */
    public final static int TO_BE_DETERMINED = 80;
    /**
     * 未提交（巡检任务分配未提交）
     */
    public final static int UNSUBMITTED = 90;
    /**
     * 保养过期（过期后不允许再提交）
     */
    public final static int MAINTENANCE_OVERDUE = 100;
    /**
     * 已关闭（维修完成，保养完成，巡检任务完成，服务单关闭）
     */
    public final static int COMPLETED_CLOSE = 110;
    /**
     * 线下关闭（线下完成，服务单关闭）
     */
    public final static int OFFLINE_COMPLETED_CLOSE = 120;
    /**
     * 无效关闭（无效关闭，服务单关闭）
     */
    public final static int INVALID_CLOSE = 130;
    /**
     * 已拒绝（审核拒绝）
     */
    public final static int REFUSE = 140;
    /**
     * 作废（维修和保养单废除后）
     */
    public final static int CANCEL = 900;
    /**
     * 空状态或未知状态
     */
    public final static int NULL_STATUS = 999;
    // -------------------------------------------------------------订单状态-end
    // -------------------------------------------------------------设备状态-start
    /**
     * 设备状态-在用
     */
    public final static int ASSET_INTSTATUS_USING = 1;
    /**
     * 设备状态-闲置
     */
    public final static int ASSET_INTSTATUS_IDLE = 2;
    /**
     * 设备状态-借出
     */
    public final static int ASSET_INTSTATUS_OUT = 3;
    /**
     * 设备状态-报废
     */
    public final static int ASSET_INTSTATUS_DISCARD = 9;
    // -------------------------------------------------------------设备状态-end
    // -------------------------------------------------------------维保计划-行事历生成任务状态-start
    /**
     * 维保计划-行事历生成任务状态-草稿
     */
    int PLAN_WORK_JOB_STATUS_DRAFT = 0;
    /**
     * 维保计划-行事历生成任务状态-未处理
     */
    int PLAN_WORK_JOB_STATUS_NEW = 10;
    /**
     * 维保计划-行事历生成任务状态-需重新生成
     */
    int PLAN_WORK_JOB_STATUS_MODIFIED = 20;
    /**
     * 维保计划-行事历生成任务状态-暂存
     */
    int PLAN_WORK_JOB_STATUS_DEPOSIT = 30;
    /**
     * 维保计划-行事历生成任务状态-处理中
     */
    int PLAN_WORK_JOB_STATUS_PROCESSING = 40;
    /**
     * 维保计划-行事历生成任务状态-已完成
     */
    int PLAN_WORK_JOB_STATUS_FINISHED = 60;
    // -------------------------------------------------------------维保计划-行事历生成任务状态-end
    /**
     * 是否启用-启用
     */
    int IS_USE_YES = 1;
    /**
     * 是否启用-禁用
     */
    int IS_USE_NO = 0;

    public final static String SHOW_TYPE_POSITION = "0"; //位置
    public final static String SHOW_TYPE_ASSET = "1";//设备

    int DASHBOARD_SHOW = 1;//显示看板
    int DASHBOARD_HIDE = 0;//不显示看板

    int DASHBOARD_SHOW_COUNT = 5;//看板显示最大数5

    public final static int CHECK_METER_UNCHECK = 0; //未抄
    public final static int CHECK_METER_CHECKED = 1; //已抄
    public final static int CHECK_METER_AUDIT = 2;   //部分抄
    public final static int CHECK_METER_UNSUAL = 3;  //异常

    public final static int CHECK_METER_STATUS_NORMAL = 1;// 正常
    public final static int CHECK_METER_TYPE_NORMAL = 0;// 正常

}