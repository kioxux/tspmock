package com.gengyun.senscloud.common;

/**
 * 业务维护配置表常量
 */
public interface BussinessConfigConstant {

    //工单类型
    String WORK_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CATEGORY_AB_F + "','name':'business_type_id','option':'select','selectKey':'business_type'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //维修类型
    String REPAIR_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //设备运行状态
    String ASSET_RUNNING_STATUS_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'running_status'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'},{'langKey':'" + LangConstant.TITLE_GM + "','name':'priority_level','option':'select','selectKey':'priority_level'}]}";
    //设备级别
    String IMPORTMENT_LEVEL_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'level_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //备件类型
    String BOM_TYPE_FIELD = "{'isTree':true,'field':[{'langKey':'" + LangConstant.TITLE_CATEGORY_AI + "','name':'type_code'},{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'},{'langKey':'" + LangConstant.TITLE_F + "','name':'parent_id','option':'select','isTree':true,'selectKey':'bom_type'}]}";
    //位置类型
    String FACILITY_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //文档类型
    String FILE_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_CATEGORY_AB_A + "','name':'type_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //单位管理
    String UNIT_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_BAAB_A + "','name':'unit_name'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //完修说明
    String WORK_FINISHED_TYPE_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'finished_name'},{'langKey':'" + LangConstant.TITLE_AAU_T + "','name':'is_finished','option':'select','selectKey':'log_whether'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
    //货币种类
    String CURRENCY_FIELD = "{'isTree':false,'field':[{'langKey':'" + LangConstant.TITLE_NAME_AB_B + "','name':'currency_name'},{'langKey':'" + LangConstant.TITLE_AG_T + "','name':'currency_code'},{'langKey':'" + LangConstant.TITLE_CB + "','name':'data_order'},{'langKey':'" + LangConstant.TITLE_AAL_N + "','name':'is_use','option':'radio','selectKey':'is_enable'}]}";
}
