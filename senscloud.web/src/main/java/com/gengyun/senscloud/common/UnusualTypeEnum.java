package com.gengyun.senscloud.common;

public enum UnusualTypeEnum {

    /**
     * 正常
     */
    NORMAL("0"),
    /**
     * 异常
     */
    UNUSUAL("1");

    private String key;

    UnusualTypeEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
