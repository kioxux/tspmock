package com.gengyun.senscloud.common;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 系统常量
 * User: sps
 * Date: 2019/07/15
 * Time: 上午11:42
 */
public interface Constants {

    String DEFAULT_PASSWORD = "cloud123";
    int MAX_FILE_UPLOAD_SIZE = 5242880;
    String MOBILE_NUMBER_SESSION_KEY = "sessionMobileNumber";
    String REGISTER_TOKEN_SESSION_KEY = "registerToken";
    String SENSCLOUD_SMS = "SMS";
    String SENSCLOUD_SMS_DEFAULT = "tencent";   //腾讯短信接口
    String SENSCLOUD_SMS_YUNDA = "yunda";       //韵达短信接口
    String SENSCLOUD_SMS_SIEMENS = "siemens";    //西门子短信接口


    String USUAL_CONFIG_ID_SMS = "sms";                //短信
    String USUAL_CONFIG_ID_WXAPPTOKEN = "wxAppToken";  //微信小程序token
    String USUAL_CONFIG_ID_WXMSG = "wxMsg";            //微信消息
    /**
     * 微信公众号accessToken
     */
    String USUAL_CONFIG_ID_WX_GZH_TOKEN = "wxoaAccessToken";

    int WORK_STATUS_EXPIRE = 900;   //工单状态-过期
    String WORK_STATUS_UNDISTRIBUTE = "20"; // 工单状态-待分配
    String PAGE_SIZE = "20";
    String PAGE_NUMBER = "1";

    int ASSET_IOT_SETTING_STATUS_NOTHING = 1; //设备物联表状态-1、无物联
    int ASSET_IOT_SETTING_STATUS_ONLINE = 2; //设备物联表状态-2、物联在线
    int ASSET_IOT_SETTING_STATUS_OFFLINE = 3; //设备物联表状态-3、物联离线

    int WORKSHEET_IS_MAIN = 1;         //判断是不是主子工单 1是
    int WORKSHEET_IS_NOT_MAIN = -1;    //判断是不是主子工单 -1不是
    int WORKSHEET_IS_MAIN_ASSET = -2;  //swiss-子工单，每个工单关联一个设备（relation_id）

    //business_type_id
    int BUSINESS_TYPE_REPAIR_TYPE = 1001;//        维修
    int BUSINESS_TYPE_MAINTAIN_TYPE = 1002;//      保养
    int BUSINESS_TYPE_INSPECTION_TYPE = 1003;//    巡检
    int BUSINESS_TYPE_SPOT_TYPE = 1004;//          点检


    String SCD_SES_KEY_NO_COMPANY = "no_company";
    String SCD_SES_KEY_UKEY = "ukey";
    String SCD_SES_KEY_USER = "user";
    String SCD_SES_KEY_LOGIN_TOKEN = "loginTmpToken";
    String SYSTEM_USER = "system";
    String SCD_SES_KEY_DPL = "dpl";
    String SCD_SES_KEY_COMPANY = "company";
    String SCD_SES_KEY_COMPANY_ID = "loginCompanyId";
    String SCD_SES_KEY_ACCOUNT = "loginAccount";
    String SCD_SES_KEY_PASSWORD = "loginPassword";
    String SCD_SES_KEY_LOGIN_TIME = "loginTime";
    String SCD_SES_KEY_SELECT_STATIC_INFO = "tmpCacheSelInfo";
    String SCD_SES_KEY_SELECT_DATA = "tmpCacheSelDt";
    String SCD_SES_KEY_ASSET_CATEGORY_FIELD = "tmpCacheAssetCategoryField";
    String SCD_SES_KEY_SELECT_FIRST_DATA = "tmpCacheSelFtDt";
    String SCD_SES_KEY_SELECT_ASSET_COM_FIELD = "tmpCacheAssetComFid";
    String SCD_SES_KEY_ASSET_FIELD_SECTION_INFO = "tmpCacheAssetFieldSectionInfo";
    String SELECT_CODE_NAME = "value";
    String SELECT_NAME_NAME = "text";
    String ASSET_DEFAULT_SECTION = "assetBase";
    String LOGIN_LANG = "loginLang";
    String SCD_SES_KEY_TIME_ZONE_ID = "timeZoneId";//客户端时区ID
    String EXCEPTION_SMS_INTERVAL = "exception_sms_interval";//设备异常时，发送短信间隔
    String ALARM_SMS1_INTERVAL = "alarm_sms1_interval";//设备报警时，第1次和第2次发送短信间隔（秒）
    String ALARM_SMS2_INTERVAL = "alarm_sms2_interval";//设备报警时，第2次和第3次发送短信间隔（秒）
    String ALARM_SMS3_INTERVAL = "alarm_sms3_interval";//设备报警时，第3次和第4次发送短信间隔（秒）
    String ALARM_SMS4_INTERVAL = "alarm_sms4_interval";//设备报警时，第4次后，依次发送短信间隔（秒）
    String APPLY_FOR_BOM = "need_bom_application";//维修保养申领备件
    String METADATA_WORK = "wt"; // 工单模板
    String METADATA_FORM = "ft"; // 表单模板

    String ASSET_RETIREMENT = "asset_retirement";//资产报废流程KEY
    String ASSET_ALLOCATION = "asset_allocation";//资产调度流程KEY
    int BOM_SOURCE_TYPE_DEFAULT = 1; // 备件来源默认值

    String ASSET_CATEGORY_NAME_BTS = "BTS";//设备类型名称-BTS
    String ASSET_CATEGORY_NAME_PPLA = "PPLA";//设备类型名称-PPLA
    String ASSET_CATEGORY_NAME_AT88MLA = "AT88MLA";//设备类型名称-AT88MLA

    //BTS监控变量名
    String ASSET_MONITOR_RECEIVE_START_TIME = "Receive_Start_Time";//收货开始时间
    String ASSET_MONITOR_RECEIVE_END_TIME = "Receive_End_Time";//收货结束时间
    String ASSET_MONITOR_RECEIVE_SHIPMENT = "Receive_Shipment";//安迪苏发货数量
    String ASSET_MONITOR_RECEIVE_ACTUAL = "Receive_Actual";//实际收货数量
    String ASSET_MONITOR_RECEIVE_DEVIATION = "Receive_Deviation";//收货偏差量
    String ASSET_MONITOR_RECEIVE_RUNNING_TIME = "Receive_Running_Time";//收货用时（min）
    String ASSET_MONITOR_LOADING_START_TIME = "Loading_Start_Time";//出库开始时间
    String ASSET_MONITOR_LOADING_END_TIME = "Loading_End_Time";//出库结束时间
    String ASSET_MONITOR_LOADING_TOTAL = "Loading_Total";//出库到生产线液体总量
    String ASSET_MONITOR_ALLOCATION_START_TIME = "Allocation_Start_Time";//调拨出库开始时间
    String ASSET_MONITOR_ALLOCATION_END_TIME = "Allocation_End_Time";//调拨出库结束时间
    String ASSET_MONITOR_ALLOCATION_TARGET = "Allocation_Target";//调拨出库液体目标量
    String ASSET_MONITOR_ALLOCATION_ACTUAL = "Allocation_Actual";//调拨出库液体实际量
    String ASSET_MONITOR_AMOUNT_TANK_VOLUME = "Amount_Tank_Volume";//当前大储罐总存量

    //MLA监控变量名
    String ASSET_MONITOR_MLA_BATCH_START_TIME = "Batch_Start_Time";//添加开始时间
    String ASSET_MONITOR_MLA_BATCH_END_TIME = "Batch_End_Time";//添加结束时间
    String ASSET_MONITOR_MLA_BATCH_TARGET = "Batch_Target";//目标添加量
    String ASSET_MONITOR_MLA_BATCH_ACTUAL = "Batch_Actual";//实际添加量
    String ASSET_MONITOR_MLA_BATCH_DOSING_TIME = "Batch_Dosing_Time";//添加用时
    String ASSET_MONITOR_MLA_BATCH_DEVIATION = "Batch_Deviation";//添加量偏差值
    String ASSET_MONITOR_MLA_BATCH_DEVIATION_RATE = "Batch_Deviation_Rate";//添加量偏差率
    String ASSET_MONITOR_MLA_BATCH_AVG_ADDITION_RATE = "Batch_Avg_Addition_Rate";//平均添加速率

    //PPLA监控变量名
    String ASSET_MONITOR_PPLA_BATCH_START_TIME = "Batch_Start_Time";//开始时间
    String ASSET_MONITOR_PPLA_BATCH_END_TIME = "Batch_End_Time";//结束时间
    String ASSET_MONITOR_PPLA_BATCH_LIQA_TARGET_TOTAL = "Batch_LIQA_Target_Total";//LIQ A目标添加量
    String ASSET_MONITOR_PPLA_BATCH_LIQA_ACTUAL_TOTAL = "Batch_LIQA_Actual_Total";//LIQ A实际添加量
    String ASSET_MONITOR_PPLA_BATCH_LIQA_DEVIATION_RATE = "Batch_LIQA_Deviation_Rate";//LIQ A添加偏差率
    String ASSET_MONITOR_PPLA_BATCH_LIQA_INCLUSION_RATE = "Batch_LIQA_Inclusion_Rate";//LIQ A添加比例
    String ASSET_MONITOR_PPLA_BATCH_LIQB_TARGET_TOTAL = "Batch_LIQB_Target_Total";//LIQ B目标添加量
    String ASSET_MONITOR_PPLA_BATCH_LIQB_ACTUAL_TOTAL = "Batch_LIQB_Actual_Total";//LIQ B实际添加量
    String ASSET_MONITOR_PPLA_BATCH_LIQB_DEVIATION_RATE = "Batch_LIQB_Deviation_Rate";//LIQ B添加偏差率
    String ASSET_MONITOR_PPLA_BATCH_LIQB_INCLUSION_RATE = "Batch_LIQB_Inclusion_Rate";//LIQ B添加比例
    String ASSET_MONITOR_PPLA_BATCH_WATER_TOTAL = "Batch_Water_Total";//Water用量
    String ASSET_MONITOR_PPLA_BATCH_WATER_INCLUSION_RATE = "Batch_Water_Inclusion_Rate";//Water添加比例
    String ASSET_MONITOR_PPLA_BATCH_RUNNING_TIME = "Batch_Running_Time";//批运行时长
    String ASSET_MONITOR_PPLA_BATCH_SOLIDFLOW_AVG_FLOWRATE = "Batch_Solidflow_Avg_Flowrate";//物料平均流量

    //监控类型
    int MONITOR_CATEGORY_DOS_AGE = 1;//用量
    int MONITOR_CATEGORY_ALARM_THRESHOLD = 2;//报警
    int MONITOR_CATEGORY_STATUS_A = 3;//状态
    int MONITOR_CATEGORY_STORAGE_A = 4;//入库
    int MONITOR_CATEGORY_OV_A = 5;//出库
    int MONITOR_CATEGORY_TRANSFER_A = 6;//调拨
    int MONITOR_CATEGORY_ITEM_OTHER = 9;//其他
    int MONITOR_CATEGORY_NONE_A = 0;//无

    String FP_PREFIX = "fp_";
    String VA_PREFIX = "va_";

    String ASSET_POSITION_CODE_PREFIX = "P";
    String ASSET_CATEGORY_CODE_PREFIX = "AC";
    String PROCESS_CREATE_TASK = "create"; // 工作流启动节点任务号
    String PROCESS_CANCEL_TASK = "cancel"; // 工作流作废节点任务号
    String PROCESS_SUB_FINISHED_TASK = "subFinished"; // 工作流子工单完成节点任务号
    String SYSTEM_NAME = "耕耘设备健康云"; // 系统名
    int BATCH_INSERT_NUM = 10;//批量insert时，每次插入条数
    String WORK_REQUEST_BUSINESS_TYPE = "90,work_request";
    String DY_MID_TBL = "midTblInfo"; // 动态页面中间存储表

    // 导入
    String EXCEL_IMORT_TYPE_USER = "user"; // 用户导入
    String EXCEL_IMORT_TYPE_ASSET = "asset"; // 设备导入
    String EXCEL_IMORT_TYPE_BIS = "bis"; // 备件入库导入
    int EXCEL_BATCH_COUNT = 500; // 导入每批数量

    //组织、位置树解构目录-节点类型
    int TREE_NODE_TYPE_FACILITY = 1;    //树节点类型-组织
    int TREE_NODE_TYPE_POSITION = 2;    //树节点类型-位置
    int TREE_NODE_TYPE_ASSET = 3;       //树节点类型-设备

//    //流程、节点类型
//    String WORK_FLOW_TYPE_FLOW = "1"; //流程：1（给详情用的）
//    String WORK_FLOW_TYPE_NODE = "2"; //节点：2（处理用的）
//    String WORK_FLOW_TYPE_SUB_NODE = "3"; //子节点：3（子节点详情用的）

    //    //流程、节点类型
    String WORK_FLOW_NODE_TYPE_FLOW = "flow"; //流程
    String WORK_FLOW_NODE_TYPE_START = "start"; //开始节点
    String WORK_FLOW_NODE_TYPE_NODE = "node"; //普通节点
    String WORK_FLOW_NODE_TYPE_SUB_NODE = "subNode"; //子节点
    String WORK_FLOW_NODE_TYPE_END = "end"; //结束节点

    //流程、节点页面类型
    String WORK_FLOW_PAGE_TYPE_BASE = "base"; //基本页面
    String WORK_FLOW_PAGE_TYPE_DETAIL = "detail"; //详情页面

    //适用对象
    Integer RELATION_TYPE_NONE = 0;//无
    Integer RELATION_TYPE_POSITION = 1;//位置
    Integer RELATION_TYPE_ASSET = 2;//设备
    Integer RELATION_TYPE_AREA = 3;//巡检区域
    Integer RELATION_TYPE_OTHER = 4;//其他

    //维保计划-职责类型
    String PLAN_WORK_DUTY_EXECUTOR = "1"; //执行
    String PLAN_WORK_DUTY_ASSIST = "2"; //协助

    //维保计划-团队类型
    String PLAN_WORK_TEAM_IN = "1"; //内部
    String PLAN_WORK_TEAM_OUT = "2"; //外部

    //计划触发类型
    Integer TRIGGER_PERIOD_TYPE_YEAR = 5;    //年
    Integer TRIGGER_PERIOD_TYPE_MONTH = 4;    //按月
    Integer TRIGGER_PERIOD_TYPE_WEEK = 3;    //按周
    Integer TRIGGER_PERIOD_TYPE_DAY = 2;    //按天
    Integer TRIGGER_PERIOD_TYPE_SINGLE = 1;    //单次

    //最高级设备位置显示类型
    String ASSET_MAP_SHOW_TYPE_PLAN = "1";//平面图
    String ASSET_MAP_SHOW_TYPE_MAP = "2";//地图

    //备件类型
    String BOM_CATEGORY_ONE_THING = "2";//一物一码
    String BOM_CATEGORY_ONE_CLASS = "1";//一类一码


    /**
     * 特殊属性名-默认值
     */
    String COLUMN_EXT_FIELD_VALUE = "field_value";
    /**
     * 特殊属性名-数据来源
     */
    String COLUMN_EXT_SOURCE = "source";


    String LANG_TYPE_KEY = "langs";
    /**
     * 后台转换
     */
    String LANG_CHANGE = "1";
    /**
     * 前端转换
     */
    String LANG_CHANGE_PAGE = "2";
    /**
     * 静态字典需要国际化标志
     */
    String STATIC_LANG = "2";
    /**
     * 静态字典JSON数据
     */
    String STATIC_JSON = "3";

    /**
     * 导出
     */
    String SEARCH_DTL_TYPE = "1";
    /**
     * 导入
     */
    String SEARCH_IMPORT_TYPE = "2";
    /**
     * 查询自定义信息
     */
    String SEARCH_BODY_TYPE = "3";
    String FILE_EXPORT_DIR = "tmp";     // 导出目录

    String ASSET_MAP_TYPE_ASSET = "asset";//设备
    String ASSET_MAP_TYPE_POSITION = "position";//位置


    /**
     * 计划持续时间单位-天
     */
    String TIME_UNIT_DAY = "day";
    /**
     * 计划持续时间单位-周
     */
    String TIME_UNIT_WEEK = "week";
    /**
     * 计划持续时间单位-月
     */
    String TIME_UNIT_MONTH = "month";
    /**
     * 计划持续时间单位-小时
     */
    String TIME_UNIT_HOUR = "hour";

    /**
     * 计划时间选择类型-天
     */
    String TRIGGER_TIME_TYPE_DAY = "day";

    /**
     * 计划时间选择类型-工作日
     */
    String TRIGGER_TIME_TYPE_WORK_DAY = "work_day";

    /**
     * 计划时间选择类型-周一
     */
    String TRIGGER_TIME_TYPE_MONDY = "mondy";

    /**
     * 计划时间选择类型-周二
     */
    String TRIGGER_TIME_TYPE_TUESDAY = "tuesday";

    /**
     * 计划时间选择类型-周三
     */
    String TRIGGER_TIME_TYPE_WEDNESDAY = "wednesday";

    /**
     * 计划时间选择类型-周四
     */
    String TRIGGER_TIME_TYPE_THURSDAY = "thursday";

    /**
     * 计划时间选择类型-周五
     */
    String TRIGGER_TIME_TYPE_FRIDAY = "friday";

    /**
     * 计划时间选择类型-周六
     */
    String TRIGGER_TIME_TYPE_SATURDAY = "saturday";

    /**
     * 计划时间选择类型-周日
     */
    String TRIGGER_TIME_TYPE_SUNDAY = "sunday";

    /**
     * 接口开发人员
     */
    String JAVA_AUTHOR = "耕云";
    /**
     * 日期格式
     */
    String DATE_FMT = "yyyy-MM-dd";
    /**
     * 日期格式-文件
     */
    String DATE_FMT_FILE = "yyyyMMddHHmmss";
    /**
     * 日期格式-年
     */
    String DATE_FMT_YEAR = "yyyy";
    /**
     * 日期格式-年月
     */
    String DATE_FMT_YEAR_MON = "yyyyMM";
    /**
     * 日期格式-年月
     */
    String DATE_FMT_YEAR_MON_2 = "yyyy-MM";
    /**
     * 日期格式-年月
     */
    String DATE_FMT_YEAR_MON_3 = "yyyy-MM-";
    /**
     * 日期格式-年月
     */
    String DATE_FMT_YEAR_MON_4 = "yyyy年MM月";
    /**
     * 日期格式-年月日
     */
    String DATE_FMT_YEAR_MON_5 = "yyyy年MM月dd日 HH:mm:ss";
    /**
     * 日期格式-年月日
     */
    String DATE_FMT_DATE = "yyyyMMdd";
    /**
     * 日期格式-年月日
     */
    String DATE_FMT_DATE_2 = "yyyy/MM/dd";
    /**
     * 日期格式-日月年
     */
    String DATE_FMT_DATE_3 = "dd MMM yyyy";
    /**
     * 日期格式-时分秒毫秒
     */
    String DATE_FMT_SSS = "yyyy-MM-dd HH:mm:ss.S";
    /**
     * 日期格式-时分秒毫秒
     */
    String DATE_FMT_SSS_2 = "yyyy/MM/dd HH:mm:ss:SSS";
    /**
     * 日期格式-时分秒
     */
    String DATE_FMT_SS = "yyyy-MM-dd HH:mm:ss";
    /**
     * 日期格式-时分秒2
     */
    String DATE_FMT_SS_2 = "yyyy/MM/dd HH:mm:ss";
    /**
     * 日期格式-时分秒3
     */
    String DATE_FMT_SS_3 = "yyyy-MM-dd'T'HH:mm:ssZ";
    /**
     * 日期格式-时分
     */
    String DATE_FMT_HM = "yyyy-MM-dd HH:mm";
    /**
     * 日期格式-时分
     */
    String DATE_FMT_HM_2 = "yyyy/MM/dd HH24:mi";
    /**
     * 日期格式-时
     */
    String DATE_FMT_H = "yyyy-MM-dd HH";
    /**
     * 日期格式-日
     */
    String DATE_FMT_D = "dd";
    /**
     * 时间格式-时分秒
     */
    String TIME_FMT = "HH:mm:ss";
    /**
     * 时间格式-时分
     */
    String TIME_FMT_HM = "HH:mm";
    /**
     * 时间格式-时
     */
    String TIME_FMT_H = "HH";

    // 特殊编号前缀
    Map<String, String> SPECIAL_ID_PREFIX = new HashMap<String, String>() {
        {
            put("bomStockList", "BSL"); // 备件清单
            put("taskReleasePublish", "TP");//任务发布
            put("bomRecipient", "BR");//任务发布
            put("bom", "BOM");
            put("performancePlan", "PP");//考核计划
        }
    };

    /**
     * 维保行事历状态-未转工单
     */
    int PLAN_WORK_CALENDAR_STATUS_NEW = 1;

    /**
     * 维保行事历状态-已转工单
     */
    int PLAN_WORK_CALENDAR_STATUS_GENERATED = 2;

    /**
     * 维保行事历状态-已过期
     */
    int PLAN_WORK_CALENDAR_STATUS_EXPIRED = 3;

    /**
     * 角色数据权限类型-新建申请单
     */
    String ROLE_DATA_PRM_TYPE = "newWork";

    /**
     * 默认任务组名称
     */
    String DEFAULT_TASK_GROUP_NAME = "/";

    /**
     * 流程选人类型
     */
    String FLOW_USER_TYPE = "flowUserType";

    /**
     * 自动生成工单-生成工单
     */
    int AUTO_GENERATE_BILL_YES = 1;
    /**
     * 自动生成工单-不生成工单
     */
    int AUTO_GENERATE_BILL_NO = 2;

    /**
     * 是否字段选人
     */
    String WORK_AUTO_USER = "auto_user";

    /**
     * 指定节点操作人所在节点
     */
    String WORK_NODE_USER = "node_user";

    /**
     * 维修工单模板
     */
    String REPORT_TEMPLATENAME_ASSETMAINTAIN = "sbwx.ftl"; //"Y8-07.xml";

    /**
     * 大中修模板
     */
    String REPORT_TEMPLATENAME_BIGREPAIR = "dzx.ftl"; //"Y8-05-05.xml";

    /**
     * 技术规格模板
     */
    String REPORT_TEMPLATENAME_TECHSTANDERD = "jsgg.ftl"; //"Y8-05-01.xml";

    /**
     * 设备台账模板
     */
    String REPORT_TEMPLATENAME_ASSETTOTAL = "sbtz.ftl"; //"Y8-04.xml";

    /**
     * 项修记录模板
     */
    String REPORT_TEMPLATENAME_MAITAINRECORD = "xxjl.ftl"; //"Y8-05-04.xml";

    /**
     * 二级保养模板
     */
    String REPORT_TEMPLATENAME_SECONDMAITAIN = "ejby.ftl"; //"Y8-05-06.xml";

    /**
     * 调拨报废记录模板
     */
    String REPORT_TEMPLATENAME_ASSETWORKS = "dbbf.ftl"; //"Y8-05-07.xml";

    /**
     * 备品备件库存月报记录模板
     */
    String REPORT_TEMPLATENAME_BOMMONTH = "bjyb.ftl"; //"T1-08.xml";

    /**
     * 备品备件库存年报记录模板
     */
    String REPORT_TEMPLATENAME_BOMYEAR = "bjnb.ftl"; //"T1-09.xml";

    /**
     * 备品备件记录模板
     */
    String REPORT_TEMPLATENAME_BOMRECORD = "bjjl.ftl"; //"Y8-13.xml";

    /**
     * 生成xls格式的报告类型
     */
    String XLS_REPORT_TYPES = "bjnb,bjyb,dbbf,ejby,xxjl,sbtz";

    /**
     * 统一的分隔符
     */
    String SCD_SPLIT_TYPE = "@@";

    /**
     * 需要方法自己获取当前节点
     */
    String GET_NOW_NODE = "@@nowNode";

    /**
     * 数字正则（0-9）
     */
    Pattern SCD_PT_INT = Pattern.compile("\\{d}|\\{[0-9]\\}");

    /**
     * 数字正则
     */
    Pattern SCD_PT_INT_D = Pattern.compile("\\{\\d+}");

    /**
     * 邮箱正则
     */
    Pattern SCD_PT_MAIL = Pattern.compile("\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}");

    /**
     * 默认企业
     */
    int DEFAULT_COMPANY = -9999;

    /**
     * 中法水务
     */
    int SUEZ_COMPANY = -2;

    /**
     * 语种-中文
     */
    String LANG_ZH_CN = "zh_CN";

    /**
     * 语种-英文
     */
    String LANG_EN_US = "en_US";
}
