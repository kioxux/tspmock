package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.service.customization.ManfuService;
//import com.gengyun.senscloud.util.Itextpdf.HtmlToPdfUtils;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.gengyun.senscloud.util.WxUtil;
//import net.sf.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 工单处理接口，供移动端程序使用
// */
//
//@RestController
//@RequestMapping("/service")
public class WorkHandlerServiceController {
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//    @Autowired
//    BomHandleService bomHandleService;
//    @Autowired
//    AssetHandleService assetHandleService;
//    @Autowired
//    WorkTaskService workTaskService;
//    @Autowired
//    WorkHandleUnMergeService workHandleUnMergeService;
//    @Autowired
//    QrcodeService qrcodeService;
//    @Autowired
//    ManfuService manfuService;
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    TaskTempLateService taskTempLateService;
//    @Autowired
//    UserRegisterService userRegisterService;
//
//    /**
//     * 根据key获取下拉框数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/get_options_by_key"})
//    public ResponseModel getOptionsByKey() {
//        try {
//            return ResponseModel.ok(selectOptionService.selectOptionForController());
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 根据key获取下拉框明细数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/get_option_by_key"})
//    public ResponseModel getOptionByKey(HttpServletRequest request) {
//        try {
//            String selectKey = request.getParameter("selectKey");
//            String selectKeyExt = request.getParameter("selectKeyExt");
//            Map<String, Object> info = selectOptionService.getOptionByCode(WxUtil.doGetSchemaName(), selectKey, selectKeyExt);
//            return ResponseModel.ok(info);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 根据主键查询模板信息
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_metadata_work")
//    public ResponseModel getMetadataWork(HttpServletRequest request) {
//        String account = request.getParameter("account");
//        try {
//            String workTemplateCode = RegexUtil.optNotBlankStrOrExp(request.getParameter("workTemplateCode"), selectOptionService.getLanguageInfo(LangConstant.TDN_E)); // 模板不存在。
//            String subWorkCode = request.getParameter("subWorkCode");
//            String schemaName = WxUtil.doGetSchemaName();
//            Map<String, Object> businessInfo = selectOptionService.getOptionByCode(schemaName, "work_template_business", workTemplateCode);
//            Integer businessNo = (Integer) businessInfo.get("business_type_id");
//            String pageTitle = SensConstant.LOG_TYPE_NAME.get(businessNo.toString()); // 标题名称
//            pageTitle = selectOptionService.getLanguageInfo(pageTitle);
//            JSONObject pageData = null;
//            // 工单信息
//            if (businessNo < 20 || Integer.valueOf(SensConstant.BUSINESS_NO_51) == businessNo || Integer.valueOf(SensConstant.BUSINESS_NO_52) == businessNo) {
//                request.setAttribute("showScan", "showScan");
//                pageData = workSheetHandleService.getWxWorkHandleInfo(schemaName, account, workTemplateCode, subWorkCode, request);
//                // 设备信息
//            } else if (businessNo < 30) {
//                request.setAttribute("pageTitle", pageTitle);
//                pageData = assetHandleService.getWxWorkHandleInfo(schemaName, account, workTemplateCode, subWorkCode, businessNo.toString(), request);
//                // 备件信息
//            } else if (businessNo < 40) {
//                request.setAttribute("pageTitle", pageTitle);
//                pageData = bomHandleService.getWxWorkHandleInfo(schemaName, account, workTemplateCode, subWorkCode, businessNo.toString(), request);
//            } else if (businessNo == 63) {
//                request.setAttribute("pageTitle", pageTitle);
//                pageData = userRegisterService.getUserRegisterHandleInfo(schemaName, account, workTemplateCode, subWorkCode, businessNo.toString(), request);
//            }
//            return ResponseModel.ok(pageData);
//        } catch (SenscloudException e) {
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 扫码取对象名称并更新创建时间
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/scan_work")
//    @Transactional //支持事务
//    public ResponseModel scanWork(HttpServletRequest request) {
//        try {
//            return ResponseModel.ok(workSheetHandleService.scanWork(WxUtil.doGetSchemaName(), request));
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            ex.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 扫码取设备信息
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/scan_repair_work")
//    @Transactional //支持事务
//    public ResponseModel scanRepairWork(HttpServletRequest request) {
//        try {
//            Map<String, Object> paramMap = new HashMap<String, Object>();
//            paramMap.put("wx_str_code", qrcodeService.dosageForCode(WxUtil.doGetSchemaName(), request.getParameter("scanCode")));
//            List<Map<String, Object>> assetList = workSheetHandleService.queryAssetByCondition(WxUtil.doGetSchemaName(), paramMap, request);
//            if (null != assetList && assetList.size() > 0) {
//                return ResponseModel.ok(assetList.get(0));
//            }
//            return ResponseModel.ok(null);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 扫码获取模板信息
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_metadata_work_by_scan")
//    @Transactional //支持事务
//    public ResponseModel getCollection(HttpServletRequest request) {
//        try {
//            String schemaName = WxUtil.doGetSchemaName();
//            String deviceCode = request.getParameter("deviceCode");
//            deviceCode = qrcodeService.dosageForCode(schemaName, deviceCode);
//            return workSheetHandleService.getWxWorkHandleInfoByAssetCode(schemaName, deviceCode, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 新增工单
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_to_add_work")
//    public ResponseModel goToAddWork(HttpServletRequest request) {
//        try {
//            String pageType = request.getParameter("pageType");
//            Map<String, Object> map = dynamicCommonService.goToAddWork(WxUtil.doGetSchemaName(), pageType);
//            return ResponseModel.ok(map);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 问题上报
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_repairWork_doSubmitRepair")
//    @Transactional //支持事务
//    public ResponseModel saveRepairProblem(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.save(WxUtil.doGetSchemaName(), processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL)); // 提交失败
//        }
//    }
//
//    /**
//     * 问题上报【单独流程】
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_worksheet_request_doSave")
//    @Transactional //支持事务
//    public ResponseModel saveWorksheetRequest(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workHandleUnMergeService.save(WxUtil.doGetSchemaName(), processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL)); // 提交失败
//        }
//    }
//
//    /**
//     * 维修确认操（审核）作
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_repairWork_confirm-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel confirmRepairProblem(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.confirmRepairProblem(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//
//    //维修确认操作（1级审核）
//    @RequestMapping("_repairWork_confirm-first-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel confirmFirstRepairProblem(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
//        try {
//            return workSheetHandleBusinessService.confirmFirstRepairProblem(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 维修执行事件（处理提交）
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_repairWork_doFinishedRepair")
//    @Transactional //支持事务
//    public ResponseModel doRepairProblem(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.doRepairProblem(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 子页面数据保存
//     *
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("_repairWork_doSaveSubInfo")
//    @Transactional //支持事务
//    public ResponseModel doSaveSubPage(@RequestParam Map<String, Object> paramMap) {
//        try {
//            return workSheetHandleBusinessService.doSaveSubPage(paramMap);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    //接单
//    @RequestMapping("_repairWork_receive-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel receiveRepairProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.receiveRepairProblemInfo(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_RECEIPT_ERROR));//接单出现异常
//        }
//    }
//
//    // 问题确认（重新提交、关闭）
//    @RequestMapping("_repairWork_confirmProblemInfo")
//    @Transactional //支持事务
//    public ResponseModel confirmProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.confirmProblemInfo(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERROR));
//        }
//    }
//
//    // 维修班长确认（提交）
//    @RequestMapping("_repairWork_confirmInfo")
//    @Transactional //支持事务
//    public ResponseModel confirmInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            paramMap.put("baseStatus", StatusConstant.TO_BE_CONFIRM);
//            return workSheetHandleBusinessService.reconfirmRepairProblemInfo(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERROR));//提交出现异常
//        }
//    }
//
//    // 店长确认（重新提交、关闭）
//    @RequestMapping("_repairWork_reconfirm-info-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel reconfirmRepairProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            paramMap.put("baseStatus", StatusConstant.DRAFT);
//            return workSheetHandleBusinessService.reconfirmRepairProblemInfo(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERROR));//提交出现异常
//        }
//    }
//
//    //维修问题确认（主管转派、接单、退回）
//    @RequestMapping("_repairWork_confirm-info-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel confirmRepairProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.confirmRepairProblemInfo(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PROBLEM_IDENT_ERROR));//问题确认出现异常
//        }
//    }
//
//    /**
//     * 巡检任务完成提交
//     *
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_workHandlerInspection_doFinishedInspection")
//    @Transactional //支持事务
//    public ResponseModel finishedInspectioncheck(@RequestParam(name = "task_id") String taskId, @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.finishedInspectioncheck(WxUtil.doGetSchemaName(), taskId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 巡检任务确认
//     *
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_workHandlerInspection_doAuditInspection")
//    @Transactional //支持事务
//    public ResponseModel auditInspectioncheck(@RequestParam(name = "task_id") String taskId, @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.auditInspectioncheck(WxUtil.doGetSchemaName(), taskId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 点检任务完成提交
//     *
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_workHandlerSpot_doFinishedSpot")
//    @Transactional //支持事务
//    public ResponseModel finishedSpotcheck(@RequestParam(name = "task_id") String taskId, @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.finishedSpotcheck(WxUtil.doGetSchemaName(), taskId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 点检任务确认
//     *
//     * @param taskId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_workHandlerSpot_doAuditSpot")
//    @Transactional //支持事务
//    public ResponseModel auditSpotcheck(@RequestParam(name = "task_id") String taskId, @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.auditSpotcheck(WxUtil.doGetSchemaName(), taskId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 重新分配人
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_workSheetHandle_doDistribution")
//    @Transactional
//    public ResponseModel distribution(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.distribution(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    // 问题上报（请求确认）
//    @RequestMapping("_worksheet_request_doConfirmRequest")
//    @Transactional //支持事务
//    public ResponseModel confirmWsq(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.confirmWsq(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    // 问题上报（请求确认）【单独流程】
//    @RequestMapping("_worksheet_request_doConfirmRequestWorkOnly")
//    @Transactional //支持事务
//    public ResponseModel confirmWsqWorkOnly(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workHandleUnMergeService.confirmWsq(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//
//    /**
//     * 更新字段值
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_workHandler_doUpdateByData")
//    @Transactional
//    public ResponseModel doUpdateByData(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return workSheetHandleBusinessService.doUpdateByData(WxUtil.doGetSchemaName(), paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 新增评论
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("_worksheet_comment")
//    @Transactional
//    public ResponseModel comment(HttpServletRequest request) {
//        try {
//            return workSheetHandleService.updateComment(request, WxUtil.doGetSchemaName());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_COMMENT_ERROR));//评论失败
//        }
//    }
//
//    /**
//     * 根据条件查询设备列表
//     *
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("_workHandler_queryAssetByCondition")
//    public ResponseModel queryAssetByCondition(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            if (null == paramMap || paramMap.size() == 0) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BI));
//            }
//            List<Map<String, Object>> list = workSheetHandleService.queryAssetByCondition(WxUtil.doGetSchemaName(), paramMap, request);
//            return ResponseModel.ok(list);
//        } catch (SenscloudException e) {
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 通过code查询任务项详情
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_task_item_detail")
//    public ResponseModel getTaskItemDetailByCode(HttpServletRequest request) {
//        try {
//            String taskItemCode = request.getParameter("taskItemCode");
//            WorkTask taskItem = workTaskService.getTaskItemDetailByCode(WxUtil.doGetSchemaName(), taskItemCode);
//            List<Map<String, Object>> taskItemToolList = workTaskService.queryTaskItemToolList(WxUtil.doGetSchemaName(), taskItemCode);
//            List<Map<String, Object>> taskItemBomList = workTaskService.queryTaskItemBomList(WxUtil.doGetSchemaName(), taskItemCode);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("taskInfo", taskItem);
//            map.put("toolInfo", taskItemToolList);
//            map.put("bomInfo", taskItemBomList);
//            return ResponseModel.ok(map);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.DATA_LOAD_WRONG));//数据加载失败
//        }
//    }
//
//    /**
//     * 跳转到报表填写页面-checklist
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/work_worksheet-detail-checklist-report")
//    public ModelAndView worksheetDetailChecklistReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            return manfuService.worksheetDetailChecklistReport(request, schema_name);
//        } catch (Exception ex) {
//            return new ModelAndView("manfu_report/check_list");
//        }
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-checklist
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailChecklistReport")
//    public ResponseModel doSaveWorksheetDetailCheckListReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            return manfuService.doSaveWorksheetDetailCheckListReport(request, schema_name);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到报表填写页面-spray
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/work_worksheet-detail-spray-report")
//    public ModelAndView worksheetDetailSprayReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            return manfuService.worksheetDetailSprayReport(request, schema_name);
//        } catch (Exception ex) {
//            return new ModelAndView("manfu_report/spray_accuracy_test_report");
//        }
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-spray
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailSprayReport")
//    public ResponseModel doSaveWorksheetDetailSprayReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            return manfuService.doSaveWorksheetDetailSprayReport(request, schema_name);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到报表填写页面-photo
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/work_worksheet-detail-photo-report")
//    public ModelAndView worksheetDetailPhotoReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            return manfuService.worksheetDetailPhotoReport(request, schema_name);
//        } catch (Exception ex) {
//            return new ModelAndView("manfu_report/field_site_photos");
//        }
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-photo
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailPhotoReport")
//    public ResponseModel doSaveWorksheetDetailPhotoReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            return manfuService.doSaveWorksheetDetailPhotoReport(request, schema_name);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到邮件发送页面
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/work_toSendEmailForWorksheetReport")
//    public ModelAndView toSendEmailForWorksheetReport(HttpServletRequest request) {
//        String sep = System.getProperty("file.separator");
//        String subWorkCode = request.getParameter("subWorkCode");
//        String schema_name = WxUtil.doGetSchemaName();
//        WorkListDetailModel workDetail = workSheetService.finddetilByID(schema_name, subWorkCode);
//        //checklist文件是否不存在
//        Boolean checklist = true;
//        //spray文件是否不存在
//        Boolean spray = true;
//        //photo文件是否不存在
//        Boolean photo = true;
//        String checklistFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "checklist" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//        String sprayFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "spray" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//        String photoFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "photo" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//        File file = new File(checklistFilePath);
//        if (file.exists()) {
//            checklist = false;
//        }
//        file = new File(sprayFilePath);
//        if (file.exists()) {
//            spray = false;
//        }
//        file = new File(photoFilePath);
//        if (file.exists()) {
//            photo = false;
//        }
//        String orgTitle;
//        try {
//            Facility facility = facilitiesService.FacilitiesById(schema_name, Integer.parseInt((String) workDetail.getFacility_id()));
//            orgTitle = facility.getShort_title();
//        } catch (Exception e) {
//            orgTitle = "";
//        }
//        return new ModelAndView("manfu_report/send_email")
//                .addObject("subWorkCode", subWorkCode)
//                .addObject("org_title", orgTitle)
//                .addObject("org_id", workDetail.getFacility_id())
//                .addObject("checklist", checklist)
//                .addObject("spray", spray)
//                .addObject("photo", photo)
//                .addObject("isMobile", true)
//                .addObject("account", WxUtil.doGetAccount())
//                .addObject("companyId", schema_name.replace(SensConstant.SCHEMA_PREFIX, ""))
//                .addObject("mail_send", selectOptionService.getLanguageInfo(LangConstant.MAIL_SEND))
//                .addObject("email", selectOptionService.getLanguageInfo(LangConstant.EMAIL))
//                .addObject("location_name", selectOptionService.getLanguageInfo(LangConstant.LOCATION_NAME))
//                .addObject("report_name", selectOptionService.getLanguageInfo(LangConstant.REPORT_NAME))
//                .addObject("mail_receiver", selectOptionService.getLanguageInfo(LangConstant.MAIL_RECEIVER))
//                .addObject("mail_content_notice", selectOptionService.getLanguageInfo(LangConstant.MAIL_CONTENT_NOTICE));
//    }
//
//    /**
//     * 发送邮件
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSendEmailForWorksheetReport")
//    public ResponseModel doSendEmailForWorksheetReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        String account = WxUtil.doGetAccount();
//        User user = userService.getUserByAccount(schema_name, account);
//        if (user == null)
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        try {
//            return manfuService.doSendEmailForWorksheetReport(request, schema_name, user);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 功能：查询任务模板信息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/find-plan-taskTemplate-list")
//    public ResponseModel findPlanTaskTemplateList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = WxUtil.doGetSchemaName();
//        try {
//            List<TaskTemplate> taskTemplatList = taskTempLateService.findPlanTaskTemplateList(schema_name);
//            if (taskTemplatList != null && taskTemplatList.size() > 0) {
//                return ResponseModel.ok(taskTemplatList);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//查询失败
//            }
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//查询失败
//        }
//    }
//
//    /**
//     * 功能：根据任务模板编号查询任务项内容
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/template_task_list_by_template_id")
//    public ResponseModel templateTaskListByTemplate(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = WxUtil.doGetSchemaName();
//            String taskTemplateCode = request.getParameter("taskTemplateCode");
//            List<WorkTask> taskdata = taskTempLateService.gettemplatecodeList(schema_name, taskTemplateCode);
//            if (taskdata != null && taskdata.size() > 0) {
//                return ResponseModel.ok(taskdata);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//未查询到任务项数据
//            }
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//未查询到任务项数据
//        }
//    }
//
//    /**
//     * 查询任务项
//     *
//     * @return
//     */
//    @RequestMapping("/search_work_task")
//    public ResponseModel queryWorkTaskList() {
//        try {
//            return ResponseModel.ok(workTaskService.queryWorkTaskList());
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//未查询到任务项数据
//        }
//    }
//
//    /**
//     * 子页面信息查询
//     *
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("_workSheetHandle_querySubPageListByCondition")
//    public ResponseModel getWorkSubPage(
//            @RequestParam Map<String, Object> paramMap) {
//        try {
//            return ResponseModel.ok(workSheetHandleService.getSubPageListByInfo(paramMap));
//        } catch (SenscloudException e) {
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 删除工单子页面相关表数据
//     *
//     * @param subWorkCode
//     * @return
//     */
//    @RequestMapping("deleteWorkSubPage")
//    @Transactional //支持事务
//    public ResponseModel deleteWorkSubPageData(
//            @RequestParam(name = "sub_work_code") String subWorkCode) {
//        try {
//            workSheetHandleService.deleteWorkSubPageData(subWorkCode);
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.OPERATE_SUCCESS)); // 操作成功
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
}
