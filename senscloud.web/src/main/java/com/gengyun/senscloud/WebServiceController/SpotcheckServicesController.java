package com.gengyun.senscloud.WebServiceController;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.SpotCheckGroupByDateResponse;
//import com.gengyun.senscloud.service.*;
//import org.apache.commons.lang.NullArgumentException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
///**
// * 微信登录服务接口，设备维修, 准备删除 yzj  2019-08-04
// */
//@RequestMapping("/service")
public class SpotcheckServicesController {
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    SpotCheckService spotCheckService;
//
//    @Autowired
//    InspectionService inspectionService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetDutyManService assetDutyManService;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //获取点检单列表
////    @RequestMapping("/get_spot_item_list")
////    public ResponseModel getSpotItemList(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        String account = request.getParameter("account");
////        String areaCode = request.getParameter("areacode");
////        try {
////            if (account == null || account.equals("null") || account.isEmpty()) {
////                result.setCode(2);
////                result.setMsg("当前登录人有错误。");
////                return result;
////            }
////            SpotcheckData spotcheckData = new SpotcheckData();
////            // 获取点检项列表
////            List<SpotcheckItemData> list = spotCheckService.SpotCheckItemList(schema_name);
////            spotcheckData.setSpotCheckItemList(list);
////            //获取当前时间,设置开始时间，用于点检的开始时间
////            Timestamp now = new Timestamp(System.currentTimeMillis());
////            spotcheckData.setBegin_time(now);
////            //查找巡检点实体
////            spotcheckData.setInspectionAreaData(inspectionService.InspectionAreaDatabyarea(schema_name, areaCode));
////
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(spotcheckData);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            //系统错误日志
////            // logService.AddLog(schema_name, "system", "maintain_list", "点检列表获取出现了问题：" + ex.getMessage(), account);
////            return result;
////        }
////    }
////
////    //点检结果提交
////    @RequestMapping("get_spotcheck_add")
////    @Transactional //支持事务
////    public ResponseModel getSpotCheckAdd(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        //当前登录人，也是点检的提交人员
////        String account = request.getParameter("account");
////        try {
////            String areaCode = request.getParameter("areaCode");
////            String itemList = request.getParameter("itemListWord");
////            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
////            String spotcheckCode = request.getParameter("spotcheckcode");
////            String beginTime = request.getParameter("begintime");
////            String assetList = request.getParameter("assetListLast");
////            //获取当前设备
////            InspectionAreaData areaData = inspectionService.InspectionAreaDatabyarea(schema_name, areaCode);
////            if (areaData == null || areaData.getArea_code().isEmpty()) {
////                result.setCode(2);
////                result.setMsg("巡检区域有误，在系统中不存在。");
////                return result;
////            }
////
////            SpotcheckData spotData = new SpotcheckData();
////            int resultOption = 0;
////            //订单号不存在，则是新增，生成新订单编号
////            if (spotcheckCode == null || spotcheckCode.isEmpty()) {
////                spotcheckCode = serialNumberService.generateMaxBusinessCode(schema_name, "spot");
////            }
////            spotData.setSpot_code(spotcheckCode);
////            spotData.setSpot_account(account);
////            spotData.setStatus(60);
////            spotData.setFault_number(faultNumber);
////            spotData.setFacility_id(areaData.getFacility_id());
////            spotData.setRemark("");
////            spotData.setAreaCode(areaCode);
////            spotData.setSpot_result(itemList);
////
////            //获取当前时间
////            Timestamp now = new Timestamp(System.currentTimeMillis());
////
////            //检查当前时间是否比发生时间早，是的话发生时间不合法
////            if (now.before(Timestamp.valueOf(beginTime))) {
////                spotData.setBegin_time(now);
////            } else {
////                spotData.setBegin_time(Timestamp.valueOf(beginTime));
////            }
////            spotData.setSpot_time(now);
////
////            resultOption = spotCheckService.insertSpotCheckData(schema_name, spotData);
////            if (resultOption > 0) {
////
////                //assetList
////                if (assetList != null && !assetList.equals("")) {
////                    net.sf.json.JSONArray jsonasset = net.sf.json.JSONArray.fromObject(assetList);
//////将检测类型不为2的且未选中的汇总作为上报问题
////                    net.sf.json.JSONArray itemListjson = net.sf.json.JSONArray.fromObject(itemList);
////                    String fault_note = "巡检发现问题，问题项：";
////                    for (int i = 0; i < itemListjson.size(); i++) {
////                        String isCheck = itemListjson.getJSONObject(i).getString("isCheck");
////                        String item_name = itemListjson.getJSONObject(i).getString("item_name");
////                        String resultType = itemListjson.getJSONObject(i).getString("resultType");
////                        //巡检类型不为2且没打勾的算有问题
////                        if (!resultType.equals("2") && isCheck.equals("false")) {
////                            fault_note = fault_note + item_name + ";";
////                        }
////                    }
////                    fault_note += "需维修。";
////
////                    //获取当前时间
////                    now = new Timestamp(System.currentTimeMillis());
////                    Calendar calendar = new GregorianCalendar();
////                    calendar.setTime(now);
////                    calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
////                    Date date = calendar.getTime();   //这个时间就是日期往后推一天的结果
////                    Timestamp nousedate = new Timestamp(date.getTime());
////                    List<User> repairusers = repairuser(schema_name, areaData.getFacility_id(), now, now);
////                    int assetInSiteNum = jsonasset.size();      // 该位置需要保养的设备数量
////                    int scheduleInSiteNum = repairusers.size();        // 该位置当天排班人数\
////                    // 每人平均负责保养的设备数量（向下取整）
////                    int avgNum = 1;
////                    int isBigYu = 0; //是否有余
////                    if (repairusers.size() != 0) {
////                        avgNum = assetInSiteNum / scheduleInSiteNum;
////                        isBigYu = assetInSiteNum % scheduleInSiteNum;
////                    }
////                    // 得到指定json key对象的value对象
////                    String groupId = "";
////                    for (int i = 0; i < jsonasset.size(); i++) {
////                        String assetcode = jsonasset.getJSONObject(i).getString("assetcode");
////                        //获取当前设备
////                        Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, assetcode);
////                        if (assetModel == null || assetModel.get_id().isEmpty()) {
////                            throw new NullArgumentException("设备不存在");
////                        }
////
////                        //看看一小时内，有没有上报未完成的，如果有，就不要再提交这个设备的问题
////                        int existCount = repairService.getRepairCountByAssetIdRecently(schema_name, assetModel.get_id(), now);
////                        if (existCount > 0) {
////                            continue;
////                        }
////
////                        if (scheduleInSiteNum > 0) {
////                            User user = new User();
////                            //首先看设备有没有专门的负责人员，有的话先给他
////                            List<User> dutyUserList = assetDutyManService.findDutyUserByAssetId(schema_name, 1, assetModel.get_id(), new Long(assetModel.getIntsiteid()).intValue());
////                            if (dutyUserList != null && dutyUserList.size() > 0) {
////                                int max = dutyUserList.size();
////                                int getInt = (int) (new Random().nextFloat() * max);
////                                user = dutyUserList.get(getInt);
////                            }
////                            if (user == null || user.getAccount() == null || user.getAccount().isEmpty()) {
////                                if (avgNum != 0 && i / avgNum < repairusers.size()) {
////                                    // 能整除的部分平均分配
////                                    user = repairusers.get(i / avgNum);
////                                } else {
////                                    // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
////                                    int max = repairusers.size();
////                                    int last = 0;
////                                    if (isBigYu > 0) {
//////                                        last = i % scheduleInSiteNum;
////                                        //随机取
////                                        last = (int) (new Random().nextFloat() * max);
////                                    }
////                                    user = repairusers.get(last);
////                                }
////                            }
////
////                            RepairData model = new RepairData();
////                            //生成新订单编号
////                            String repairCode = serialNumberService.generateMaxBusinessCode(schema_name, "repair");
////                            model.setRepairCode(repairCode);
////                            model.setAssetId(assetModel.get_id());
////                            model.setAssetType(assetModel.getAsset_type());
////                            model.setAssetStatus(2);
////                            model.setDeadlineTime(nousedate);
////                            model.setOccurtime(now);
////                            model.setFaultNote(fault_note);
////                            model.setDistributeAccount("");
////                            model.setDistributeTime(null);
////                            model.setReceiveAccount(user.getAccount());
////                            //判断状态
////                            int status = 40;   //待分配
////                            model.setReveiveTime(now);
////                            model.setDistributeTime(now);
////                            model.setRepairBeginTime(null);
////                            model.setStatus(status);
////                            model.setFacilityId((int) assetModel.getIntsiteid());
////                            model.setCreate_user_account(account);
////                            model.setCreatetime(now);
////                            model.setFromCode(spotcheckCode);
////                            model.setPriorityLevel(2);
////                            //保存上报问题
////                            int doCount = repairService.saveRepairInfo(schema_name, model);
////                        }
////                    }
////                }
////
////                result.setCode(1);
////                result.setMsg("点检保存成功！");
////                result.setContent(spotcheckCode);
////                //记录历史
////                logService.AddLog(schema_name, "spot", spotcheckCode, "点检结果提交", account);
////                return result;
////            } else {
////                result.setCode(0);
////                result.setMsg("点检保存失败！");
////                return result;
////            }
////        } catch (Exception ex) {
////            // 手动回滚
////            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
////            result.setCode(0);
////            result.setMsg("保存失败，请联系管理员！");
////            //系统错误日志
////            logService.AddLog(schema_name, "system", "spotcheck_add", "巡检结果保存出现了问题：" + ex.getMessage(), account);
////            return result;
////        }
////    }
////
////    //获取点检信息
////    @RequestMapping("/get_spot_info")
////    public ResponseModel getSpotList(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        //获取点检单号
////        String spotCode = request.getParameter("spotcode");
////        String account = request.getParameter("account");
////        try {
////            if (spotCode == null || spotCode.isEmpty()) {
////                result.setCode(2);
////                result.setMsg("当前点检单号有问题。");
////                return result;
////            }
////            //获取点检
////            SpotcheckData info = spotCheckService.spotCheckDataByNo(schema_name, spotCode);
////            //获取点检上报的问题
////            if (info != null && info.getSpot_code() != null && !info.getSpot_code().isEmpty()) {
////                info.setRepairDataList(repairService.getRepairListByFromCode(schema_name, spotCode));
////            }
////
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(info);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            //系统错误日志
////            logService.AddLog(schema_name, "system", "spot_info", "点检信息获取出现了问题：" + ex.getMessage(), account);
////            return result;
////        }
////    }
////
////    //获取点检单列表，按日期
////    @RequestMapping("/get_spot_list")
////    public ResponseModel getSpotCheckListData(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        //当前登录人，也是上报问题的人员
////        String account = request.getParameter("account");
////        try {
////            if (account == null || account.equals("null") || account.isEmpty()) {
////                result.setCode(2);
////                result.setMsg("当前登录人有错误。");
////                return result;
////            }
////            int page = Integer.parseInt(request.getParameter("page"));
////            int page_size = Integer.parseInt(request.getParameter("page_size"));
////
////            if (page < 0 || page_size < 0) {
////                page = 0;
////                page_size = 7;
////            }
////
////            //按用户权限，查看其获取巡检的条件
////            String condition = getSpotCodition(request);
////
////            //获取巡检列表，按日期
////            List<SpotCheckGroupByDateResponse> spotListAppData = spotCheckService.spotCheckListAppResult(schema_name, condition, page_size, page_size * page);
////            if (spotListAppData != null && spotListAppData.size() > 0) {
////                for (SpotCheckGroupByDateResponse listAppResult : spotListAppData) {
////                    List<SpotcheckData> dataList = spotCheckService.SpotCheckDataList(schema_name, condition, listAppResult.getInsdate(), page_size, page_size * page);
////                    listAppResult.setSpotCheckListAppDetailResultList(dataList);
////                }
////            }
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(spotListAppData);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            //系统错误日志
////            //logService.AddLog(schema_name, "system", "repair_list", "巡检列表获取出现了问题：" + ex.getMessage(), account);
////            return result;
////        }
////    }
////
////    //获取点检单列表，单日的点检列表
////    @RequestMapping("/get_spotcheck_detaillist")
////    public ResponseModel getSpotCheckDetailListData(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        //当前登录人，也是上报问题的人员
////        String account = request.getParameter("account");
////        try {
////            if (account == null || account.equals("null") || account.isEmpty()) {
////                result.setCode(2);
////                result.setMsg("当前登录人有错误。");
////                return result;
////            }
////            int page = Integer.parseInt(request.getParameter("page"));
////            int page_size = Integer.parseInt(request.getParameter("page_size"));
////            String insdate = request.getParameter("insdate");
////            if (page < 0 || page_size < 0) {
////                page = 0;
////                page_size = 10;
////            }
////
////            //按用户权限，查看其获取巡检的条件
////            String condition = getSpotCodition(request);
////
////            //获取巡检项
////            List<SpotcheckData> dataList = spotCheckService.SpotCheckDataList(schema_name, condition, insdate, page_size, page_size * page);
////
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(dataList);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            //系统错误日志
////            //logService.AddLog(schema_name, "system", "repair_list", "巡检列表获取出现了问题：" + ex.getMessage(), account);
////            return result;
////        }
////    }
////
////    //按当前用户的角色，获取其点检的查询条件
////    private String getSpotCodition(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        //获取当前用户
////        String account = request.getParameter("account");
////        User userInfo = userService.getUserByAccount(schema_name, account);
////
////        //查看用户权限
////        String condition;
////        Boolean isAllFacility = false;
////        Boolean isSelfFacility = false;
////        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, userInfo.getId(), "spotcheck");
////        if (userFunctionList != null || !userFunctionList.isEmpty()) {
////            for (UserFunctionData functionData : userFunctionList) {
////                //如果可以查看全部位置，则不限制
////                if (functionData.getFunctionName().equals("spot_all_facility")) {
////                    isAllFacility = true;
////                    break;
////                } else if (functionData.getFunctionName().equals("spot_self_facility")) {
////                    isSelfFacility = true;
////                }
////            }
////        }
////        if (isAllFacility) {
////            condition = " 1=1 ";
////        } else if (isSelfFacility) {
////            LinkedHashMap<String, IFacility> facilityList = userInfo.getFacilities();
////            String facilityIds = "";
////            if (facilityList != null && !facilityList.isEmpty()) {
////                for (String key : facilityList.keySet()) {
////                    facilityIds += facilityList.get(key).getId() + ",";
////                }
////            }
////            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
////                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
////                condition = " ins.facility_id in (" + facilityIds + ") ";
////            } else {
////                condition = " ins.spot_account='" + userInfo.getAccount() + "' ";    //没有用户所属位置，则按个人权限查询
////            }
////        } else {
////            condition = " ins.spot_account='" + userInfo.getAccount() + "' ";
////        }
////        return condition;
////    }
//
//
////    public List<User> repairuser(String schema, int facility, Timestamp now, Timestamp nowadd) {
////        //找到该位置所有的，能执行保养提交任务的人员
////        //修改为白天在上班的能保养的人员
////        List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////        if (scheduleList.size() == 0) {
////            //再找一级所属位置的，看有没有排班的人
////            List<Facility> facilities = facilitiesService.FacilitiesList(schema);
////            for (Facility facilityxx : facilities) {
////                if (facilityxx.getId() == facility)
////                    facility = facilityxx.getParentId();
////                break;
////            }
////            scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////            if (scheduleList.size() == 0) {
////                //还是没有人，则按角色找
//////                scheduleList = userService.findUserByFunctionKey(schema, "repair_do", facility);
////            }
////        }
////        //找到的人如果没有配置，则不分配保养人员
////        if (scheduleList == null || scheduleList.isEmpty()) {
////            scheduleList = new ArrayList<>();
////        }
////        return scheduleList;
////    }
}
