package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.gengyun.senscloud.util.WxUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Map;
//
///**
// * 备件工单处理接口，供移动端程序使用
// */
//@RestController
//@RequestMapping("/service")
public class BomHandlerServiceController {
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    private BomDiacardService bomDiscardService;
//    @Autowired
//    private BomRecipientService bomRecipientService;
//    @Autowired
//    private BomInStockService bomInStockService;
//    @Autowired
//    private BomAllotService bomAllotService;
//    @Autowired
//    private BomInventoryService bomInventoryService;
//    @Autowired
//    private UserService userService;
//
//    /**
//     * 提交数据（入库）
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_in_stock_doSubmitBomInStock")
//    @Transactional //支持事务
//    public ResponseModel doSubmitBomInStock(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return bomInStockService.save(WxUtil.doGetSchemaName(), processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31));
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件入库-验收入库
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_in_stock_inStockAcceptance")
//    @Transactional //支持事务
//    public ResponseModel inStockAcceptance(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomInStockService.inStockAudit(WxUtil.doGetSchemaName(), request, paramMap, user, false);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件入库-审核入库
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_in_stock_inStockAudit")
//    @Transactional //支持事务
//    public ResponseModel inStockAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomInStockService.inStockAudit(WxUtil.doGetSchemaName(), request, paramMap, user, true);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 提交数据（报废）
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_discard_doSubmitBomDiscard")
//    @Transactional //支持事务
//    public ResponseModel doSubmitBomDiscard(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return bomDiscardService.save(WxUtil.doGetSchemaName(), processDefinitionId, paramMap, request, null);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 报废审核提交
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_discard_bomDiscardAudit")
//    @Transactional //支持事务
//    public ResponseModel bomDiscardAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomDiscardService.bomDiscardAudit(WxUtil.doGetSchemaName(), request, paramMap, user, false);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 报废最后审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_discard_lastBomDiscardAudit")
//    @Transactional //支持事务
//    public ResponseModel lastBomDiscardAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomDiscardService.bomDiscardAudit(WxUtil.doGetSchemaName(), request, paramMap, user, true);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 提交数据（领用）
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_recipient_doSubmitBomRecipient")
//    @Transactional //支持事务
//    public ResponseModel doSubmitBomRecipient(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return bomRecipientService.save(WxUtil.doGetSchemaName(), processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件领用审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_recipient_bomRecipentAudit")
//    @Transactional //支持事务
//    public ResponseModel bomRecipentAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomRecipientService.inStockRecipent(WxUtil.doGetSchemaName(), request, paramMap, user, false);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件领用出库审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_recipient_bomRecipentPassAudit")
//    @Transactional //支持事务
//    public ResponseModel bomRecipentRecipentPassAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomRecipientService.inStockRecipent(WxUtil.doGetSchemaName(), request, paramMap, user, true);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件调拨出库确认
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_allot_bomAllotFromConfirm")
//    @Transactional //支持事务
//    public ResponseModel bomAllotAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomAllotService.bomAllotAudit(WxUtil.doGetSchemaName(), request, paramMap, user, "fromConfirm");
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件调拨入库确认
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_allot_bomAllotToConfirm")
//    @Transactional //支持事务
//    public ResponseModel bomAllotConfirm(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomAllotService.bomAllotAudit(WxUtil.doGetSchemaName(), request, paramMap, user, "toConfirm");
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 调出备件
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_allot_bomAllotFromAudit")
//    @Transactional //支持事务
//    public ResponseModel bomAllotFromAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomAllotService.bomAllotAudit(WxUtil.doGetSchemaName(), request, paramMap, user, "fromAudit");
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 调入备件
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_allot_bomAllotToAudit")
//    @Transactional //支持事务
//    public ResponseModel bomAllotToAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return bomAllotService.bomAllotAudit(WxUtil.doGetSchemaName(), request, paramMap, user, "toAudit");
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 备件盘点提交
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_bom_inventory_pcInventory")
//    @Transactional //支持事务
//    public ResponseModel bomPcInventory(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return bomInventoryService.pcInventory(WxUtil.doGetSchemaName(), request, paramMap);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
}
