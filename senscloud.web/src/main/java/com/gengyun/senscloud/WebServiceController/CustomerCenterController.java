package com.gengyun.senscloud.WebServiceController;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.enumResp.ResultStatus;
//import com.gengyun.senscloud.service.NoticeService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.SystemConfigService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.util.WxUtil;
//import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//
///**
// * 个人中心
// */
//@RestController
//@RequestMapping("/service")
public class CustomerCenterController {
//
//    private static final Logger logger = LoggerFactory.getLogger(CustomerCenterController.class);
//
//    @Autowired
//    private NoticeService noticeService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 查询系统公告列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("get_notice_list")
//    public ResponseModel getNoticeList(HttpServletRequest request) {
//        String account = request.getParameter("account");
//        try {
//
//            int page = Integer.parseInt(request.getParameter("page"));
//            int page_size = Integer.parseInt(request.getParameter("page_size"));
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                return new ResponseModel(ResultStatus.PARAMS_NO_USERNAME);
//            }
//            if (page < 0 || page_size < 0) {
//                page = 0;//默认第一页为0
//                page_size = 7;
//            }
//            JSONObject result = noticeService.queryForApp(WxUtil.doGetSchemaName(), "", page_size, page + 1, "a.create_time", "DESC");
//            return ResponseModel.ok(net.sf.json.JSONObject.fromObject(result.toString()));
//        } catch (Exception e) {
//            logger.error("CustomerCenterController.getNoticeList ~ ", e);
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 查询系统公告列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("get_notice_detail")
//    public ResponseModel getNoticeDetailById(HttpServletRequest request) {
//        String account = request.getParameter("account");
//        try {
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                return new ResponseModel(ResultStatus.PARAMS_NO_USERNAME);
//            }
//            return noticeService.getNoticeDetailById(WxUtil.doGetSchemaName(), request);
//        } catch (Exception e) {
//            logger.error("CustomerCenterController.getNoticeDetailById ~ ", e);
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    /**
//     * 查询系统公告列表
//     *
//     * @return
//     */
//    @RequestMapping("about_us")
//    public ResponseModel aboutUs() {
//        try {
//            SystemConfigData data = systemConfigService.getSystemConfigData(WxUtil.doGetSchemaName(), "about_us");
//            return ResponseModel.ok(data);
//        } catch (Exception e) {
//            logger.error("CustomerCenterController.aboutUs ~ ", e);
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
}
