package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.model.WorkListDetailModel;
//import com.gengyun.senscloud.model.WorkSheet;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DecimalFormat;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 工单处理接口，供移动端程序使用
// */
//
//@RestController
//@RequestMapping("/service")
public class WorkHandlerDetailServiceController {
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    MetadataWorkService metadataWorkService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    WorkListService workListService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    WorkSheetService workSheetService;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    WorkSheetRequestService workRequest;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//    //工单系统，上报问题
//
//    //新工单
//    @RequestMapping("/getFromKey")
//    public ResponseModel getFromKey(HttpServletRequest request, HttpServletResponse response) {
//        Boolean distributionFlag = false;//维修分配权限
//        int distributionType = 0;//分配类型
//        String typeFlag = null;//权限标识
//        String flagtype = null;//获取权限标识的标识
//        int workTypeId = 0; // 工单类型
//        String whereString = "";
//        int tamplate_type = 0;
//        int relation_type = 0;
//        int is_main = 0;
//        String subWorkCode = null; // 业务数据主键
//        String work_code = null; // 业务数据主键
//        String companyId = request.getParameter("companyId");
//        String finished_time_interval = "无";//完成时效
//        String arrive_time_interval = "无";//到场时效
//        Long tv;//时间转换方式
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        WorkListDetailModel workdetail = new WorkListDetailModel();
////        List<WorkListDetailModel> subList = new ArrayList<WorkListDetailModel>();
//        String account = request.getParameter("account");
//        User user = userService.getUserByAccount(schema_name, account);
//        DecimalFormat decimalFormat = new DecimalFormat("#.00");
//        try {
//            subWorkCode = request.getParameter("subWorkCode");
//            if (null == subWorkCode || "".equals(subWorkCode)) {
//                result.setCode(2);
//                result.setContent("");
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//            }
//            String handleType = request.getParameter("handleType");
//            Map<String, Object> resultMap = new HashMap<String, Object>();
//            if (null != handleType && "distribute".equals(handleType)) {
//                workdetail = workSheetService.finddetilByID(schema_name, subWorkCode);
//                distributionType = workdetail.getBusiness_type_id();
//                switch (distributionType) {
//                    case 1:
//                        typeFlag = "repair_distribute";
//                        flagtype = "repair";
//                        break;
//                    case 2:
//                        typeFlag = "maintain_distribute";
//                        flagtype = "maintain";
//                        break;
//                    case 3:
//                        typeFlag = "inspection_distribute";
//                        flagtype = "inspection";
//                        break;
//                    case 4:
//                        typeFlag = "spot_distribute";
//                        flagtype = "spotcheck";
//                        break;
//                }
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), flagtype);
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //查看有没有当前类型的工单分配权限，则不限制
//                        if (functionData.getFunctionName().equals(typeFlag)) {
//                            distributionFlag = true;
//                        }
//                    }
//                }
//                if (!distributionFlag) {
//                    handleType = null;
//                }
//            }
//            // 待分配
//            if (null != handleType && "distribute".equals(handleType)) {
//                // 明细数据
//                Map<String, Object> dtlInfo = workSheetHandleService.queryBusinessDetailById(schema_name, subWorkCode);
//                work_code = (String) dtlInfo.get("work_code");
//                // 主明细数据
//                Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, work_code);
//                subWorkCode = (String) mainDetailInfo.get("sub_work_code");
//                resultMap.put("subWorkCode", subWorkCode);
//            }
//            workdetail = workSheetService.finddetilByID(schema_name, subWorkCode);
//            //如果完成时间和开始时间存在则计算完成时效
//            if (workdetail.getFinished_time() != null && !"".equals(workdetail.getFinished_time()) && workdetail.getBegin_time() != null && !"".equals(workdetail.getBegin_time())) {
//                tv = (workdetail.getFinished_time().getTime() - workdetail.getBegin_time().getTime())
//                        / (1000 * 60);
//                finished_time_interval = String.valueOf(Double.parseDouble(decimalFormat.format(tv))) + "分钟";
//            }
//            //如果分配时间和开始时间存在则计算到场时效
//            if (workdetail.getBegin_time() != null && !"".equals(workdetail.getBegin_time()) && workdetail.getDistribute_time() != null && !"".equals(workdetail.getDistribute_time())) {
//                tv = (workdetail.getBegin_time().getTime() - workdetail.getDistribute_time().getTime())
//                        / (1000 * 60);
//                arrive_time_interval = String.valueOf(Double.parseDouble(decimalFormat.format(tv))) + "分钟";
//            }
//            resultMap.put("finished_time_interval", finished_time_interval);
//            resultMap.put("arrive_time_interval", arrive_time_interval);
//
////            // 待分配
////            if (null != handleType && "distribute".equals(handleType)) {
////                if (null != account && !"null".equals(account) && !account.isEmpty()) {
////                    user = userService.getUserByAccount(schema_name, account); // 获取当前用户
////                }
////                if (null == user) {
////                    return ResponseModel.errorMsg("当前登录人有错误。");
////                }
//////                List<Map<String, Object>> customerList = selectOptionService.SelectOptionList(schema_name, null, "customer", null, null);
//////                resultMap.put("customerList", customerList);
////
////            }
//
//            workTypeId = workdetail.getWork_type_id();
//            relation_type = workdetail.getRelation_type();
//            whereString += "and(work_type_id=" + workTypeId + ")";
//            whereString += "and(relation_type=" + relation_type + ")";
//            is_main = workdetail.getIs_main();
//            if (is_main == 1) {
////                subList = workSheetService.getSubWorkList(schema_name, work_code);
////                resultMap.put("subList", subList);
//                tamplate_type = 3;//主工单的详情
//            } else {
//                tamplate_type = 2;//子工单的详情
//            }
////            if (subList.isEmpty()) {
////                tamplate_type = 2;
////            } else {
////                tamplate_type = 3;
////            }
//            whereString += "and(template_type=" + tamplate_type + ")";
//            String workFormKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//            resultMap.put("workFormKey", workFormKey);
//            result.setCode(1);
//            result.setContent(resultMap);
//        } catch (Exception ex) {
//            result.setCode(2);
//            result.setContent("");
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//            ex.printStackTrace();
//        }
//        return result;
//    }
//
//    /**
//     * 获取详情FromKey
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/getDetailFromKey")
//    public ResponseModel getDetailFromKey(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        String subWorkCode=request.getParameter("sub_work_code");
//        String whereString="";
//        String workFormKey;
//        try {
//            if(RegexUtil.isNull(subWorkCode)){
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));
//            }
//            String msgNone = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
//            // 工单请求的详情
//            if(subWorkCode.startsWith("WR")){
//                Map<String, Object> wsqDetail = workRequest.queryWorkRequestById(schema_name, subWorkCode);
//                // 工单类型
//                int workTypeId = ((Long) wsqDetail.get("work_type_id")).intValue();
//                whereString += "and(template_type=4)and(work_type_id="+workTypeId+")";
//                workFormKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//                if (RegexUtil.isNull(workFormKey) || msgNone.equals(workFormKey)) {
//                    whereString = "and(template_type=4)"; // 工单请求的详情
//                    workFormKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//                }
//            }else{
//                int  templateType;
//                WorkListDetailModel workdetail= workSheetService.finddetilByID(schema_name, subWorkCode);
//                int is_main = workdetail.getIs_main();
//                if (is_main == 1) {
//                    //主工单的详情
//                    templateType = 3;
//                } else {
//                    //子工单的详情
//                    templateType = 2;
//                }
//                whereString += "and(template_type=" + templateType + ") and(relation_type="+ workdetail.getRelation_type() +
//                        ") and(work_type_id="+ workdetail.getWork_type_id() + ")";
//                workFormKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//            }
//            result.setCode(1);
//            result.setContent(workFormKey);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));
//        }
//        return result;
//    }
//
//    /**
//     * 新工单请求接口
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/getRequestFromKey")
//    public ResponseModel getRequestFromKey(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        String account = request.getParameter("account");
//        String handleType = request.getParameter("handleType");
//        try {
//            Map<String, Object> resultMap = new HashMap<String, Object>();
//            String workRequestCode = request.getParameter("subWorkCode");
//            Map<String, Object> wsqDetail = workRequest.queryWorkRequestById(schema_name, workRequestCode);
//            int workTypeId = ((Long) wsqDetail.get("work_type_id")).intValue(); // 工单类型
//            Map<String, String> permissionInfo = new HashMap<String, String>();
//            permissionInfo.put("todo_list_distribute", "isDistribute");
//            ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "work/worksheet_detail/worksheet_detail", "todo_list");
//
//            Integer status = (Integer) wsqDetail.get("status"); // 工单类型
//            if (StatusConstant.TO_BE_CONFIRM != status) {
//                mav.getModel().put("isDistribute", false);
//            }
//            if (mav.getModel().get("isDistribute")!=null && "distribute".equals(handleType)  &&  true == (boolean)mav.getModel().get("isDistribute") ) {//有分配权限
//                resultMap.put("subWorkCode", workRequestCode);
//            }
//            String msgNone = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
//            String whereString = "and(work_type_id=" + workTypeId + ")";
//            whereString += "and(template_type=4)"; // 工单请求的详情
//            String workFormKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//            if (RegexUtil.isNull(workFormKey) || msgNone.equals(workFormKey)) {
//                whereString = "and(template_type=4)"; // 工单请求的详情
//                workFormKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//            }
//            resultMap.put("workFormKey", workFormKey);
//            result.setCode(1);
//            result.setContent(resultMap);
//        } catch (Exception ex) {
//            result.setCode(2);
//            result.setContent("");
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//            ex.printStackTrace();
//        }
//        return result;
//    }
//

}
