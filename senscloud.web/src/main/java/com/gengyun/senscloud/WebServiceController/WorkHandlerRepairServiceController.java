package com.gengyun.senscloud.WebServiceController;
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.common.SystemConfigConstant;
//import com.gengyun.senscloud.model.MetadataWork;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import net.sf.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 工单处理接口，供移动端程序使用
// */
//
//@RestController
//@RequestMapping("/service")
public class
WorkHandlerRepairServiceController {
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    MetadataWorkService metadataWorkService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    WorkListService workListService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//    //工单系统，上报问题
//    //新工单
//    @RequestMapping("/get_from_key_collection")
//    public ResponseModel getFromKey(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//
//        String work_type_id = request.getParameter("work_type_id");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String flow_template_code = selectOptionService.getOptionNameByCode(schema_name, "work_type", work_type_id, "relation");
//            String formKey = workflowService.getStartFormKey(schema_name, flow_template_code);
//            MetadataWork matework = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//            Map map = new HashMap();
//            JSONObject pageData = workSheetHandleService.getWxWorkHandleInfo(schema_name, account, formKey, "", request);
//            map.put("pageData", pageData);
////            map.put("fieldList", matework.getBodyProperty());
//            map.put("workTemplateCode", matework.getWorkTemplateCode());
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(map);
//            return result;
//        } catch (SenscloudException e) {
//            result.setCode(0);
//            result.setMsg(e.getMessage());
//            return result;
//        } catch (Exception e) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
////
////    //上报问题
////    //account: "yaozhijun",asset_status:1,asset_type: "fzsb",deadline_time: "2018-03-23 12:20:29",device_code:"A29f80f5a-7a1e-4bde-bcad-9938821b8ec0",receive_account: "yaozhijun"    fault_img:"",fault_note:"",fault_type:1,help_account:"yaozhijun",occur_time: "2018-03-23 12:20:29",repair_type: 1
////    @RequestMapping("/get_work_repair_add")
////    @Transactional //支持事务
////    public ResponseModel getAddRepair(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        //当前登录人，也是上报问题的人员
////        String account = request.getParameter("account");
////        User user = userService.getUserByAccount(schema_name, account);
////        try {
////            String asset_status = request.getParameter("asset_status");//设备运行状态
////            String asset_type = request.getParameter("asset_type");//设备类型
////            String deadline_time = request.getParameter("deadline_time");//截止时间
////            String device_code = request.getParameter("device_code");//
////            String fault_note = request.getParameter("fault_note");//问题
////            String receive_account = request.getParameter("receive_account");//接受人
////            String help_account = request.getParameter("help_account");//支援人
////            String occur_time = request.getParameter("occur_time");//创建事件
////            String from_code = request.getParameter("from_code");//
////            String body_property = request.getParameter("field_listby");//自定义字段号集合
////            List<Object> list = JSON.parseArray(body_property);//转换城集合村数据库
////            String flow_id = request.getParameter("flow_id");//工单类型
////            String work_type_id = request.getParameter("work_type_id");//工单类型id
////            String work_template_code = request.getParameter("workTemplateCode");//模版编号
////            WorkflowStartResult workflowStartResult;//调用流程返回值
////            String content = null;
////            Map map = new HashMap();
////            if (account == null || account.isEmpty() || asset_type.isEmpty() || device_code.isEmpty() || fault_note == null || fault_note.isEmpty()) {
////                result.setCode(2);
////                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ACCOUNT_NO_EMPTY));//上报人、设备编号、故障描述不能为空。
////                return result;
////            }
////            selectOptionService.SelectOptionList(schema_name, null, "asset_status", "", null);
////            //处理图片
////            String fault_img = request.getParameter("fault_img");  //已转成id，“，”隔开的多个图片
////            //获取当前设备
////            Asset assetModel = assetService.getAssetByCode(schema_name, device_code);
////            if (assetModel == null || assetModel.get_id().isEmpty()) {
////                result.setCode(2);
////                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备编号有误，在系统中不存在。
////                return result;
////            }
////            //获取当前时间
////            Timestamp now = new Timestamp(System.currentTimeMillis());
////            //看看一小时内，有没有上报未完成的，如果有，就不要再提交
////            int existCount = workSheetService.getRepairCountByAssetIdRecently(schema_name, assetModel.get_id(), now);
////            if (existCount > 0) {
////                result.setCode(2);
////                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_PROBLEM_EXIST));//该设备目前已上报问题
////                return result;
////            }
////            //
////            WorkListModel workListModel = new WorkListModel();
////            WorkListDetailModel workListDetailModel = new WorkListDetailModel();
////            //生成新订单编号
////            String work_code = dynamicCommonService.getWorkCode(schema_name);//工单号
////            String sub_work_code = dynamicCommonService.getSubWorkCode(schema_name);//工单详情单号
////            workListModel.setWork_code(work_code);
////            workListDetailModel.setSub_work_code(sub_work_code);
////            workListDetailModel.setRelation_id(assetModel.get_id());
////            workListModel.setRelation_id(assetModel.get_id());
////            workListDetailModel.setAsset_running_status(Integer.parseInt(asset_status));
////            //查找所有的设备状态，按状态设置问题的优先级
////            List<AssetRunningData> assetRunningDataList = maintenanceSettingsService.getAssetRunningDataList(schema_name);
////            if (null != assetRunningDataList && assetRunningDataList.size() > 0) {
////                for (AssetRunningData data : assetRunningDataList) {
////                    if (data.getId() == Integer.parseInt(asset_status)) {
////                        workListDetailModel.setPriority_level(data.getPriorityLevel());
////                        break;
////                    }
////                }
////            } else {
////                workListDetailModel.setPriority_level(0);
////            }
////
////            workListDetailModel.setBody_property(list);
////            workListDetailModel.setWork_type_id(Integer.parseInt(work_type_id));
////            workListModel.setWork_type_id(Integer.parseInt(work_type_id));
////            workListModel.setWork_template_code(work_template_code);
////            workListDetailModel.setWork_template_code(work_template_code);
////            //检查当前时间是否比发生时间早，是的话发生时间不合法
////            if (now.before(Timestamp.valueOf(occur_time))) {
////                workListModel.setCreate_time(now);
////                workListModel.setOccur_time(now);
////                workListDetailModel.setOccur_time(now);
////            } else {
////                workListModel.setCreate_time(now);
////                workListModel.setOccur_time(now);
////                workListDetailModel.setOccur_time(now);
////            }
////            workListModel.setDeadline_time(Timestamp.valueOf(deadline_time));
////            workListDetailModel.setDeadline_time(Timestamp.valueOf(deadline_time));
////            workListDetailModel.setProblem_note(fault_note);
////            workListDetailModel.setProblem_img(fault_img);
////            workListDetailModel.setAccount_name(account);
////            workListDetailModel.setDistribute_time(null);
////            workListDetailModel.setReceive_account(receive_account);
////            //判断状态
////            int status = onstant.UNDISTRIBUTED;   //待分配
////
////            if (receive_account != null && !receive_account.isEmpty()) {
////                status = StatusConstant.PENDING;   //处理中
////                try {
////                    if (null != body_property && !"".equals(body_property)) {
////                        JSONArray arrays = JSONArray.fromObject(body_property);
////                        JSONObject data = null;
////                        for (Object object : arrays) {
////                            data = JSONObject.fromObject(object);
////                            if ("next_status".equals(data.get("fieldCode").toString())) {
////                                status = Integer.valueOf((String) data.get("fieldValue"));
////                                break;
////                            }
////                        }
////                    }
////                } catch (Exception e) {
////                    status = StatusConstant.PENDING;
////                }
////                workListDetailModel.setReceive_time(now);
////                workListDetailModel.setReceive_account(receive_account);
////                workListDetailModel.setDistribute_time(now);
//////                content = MessageFormat.format(SensConstant.UN_COMPLETE_INFO_WORK, user.getUsername(),assetModel.getStrname()+fault_note );
////
////                if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {//韵达短信发送平台
////                    content = MessageFormat.format(SensConstant.UN_COMPLETE_INFO_WORK, user.getUsername(), assetModel.getStrname() + fault_note);
////                } else {//腾讯短信发送平台
////                    //"{0}请您完成：{1}，请尽快完成。"
////                    String pattern = "{0,number,#}::{1}::{2}";
////                    content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_UN_COMPLETE_INFO_WORK, user.getUsername(), assetModel.getStrname() + fault_note);
////                }
////
////                map.put("receive_account", receive_account);
////            } else {
////                if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {//韵达短信发送平台
////                    content = MessageFormat.format(SensConstant.UN_DISTRIBUTION_INFO_REPAIR, user.getUsername(), assetModel.getStrname() + fault_note);
////                } else {//腾讯短信发送平台
////                    String pattern = "{0,number,#}::{1}::{2}";
////                    content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_UN_DISTRIBUTION_INFO_REPAIR, user.getUsername(), assetModel.getStrname() + fault_note);
////                }
////
////                workListDetailModel.setReceive_time(null);
//////                content = MessageFormat.format(SensConstant.UN_DISTRIBUTION_INFO_REPAIR, user.getUsername(), assetModel.getStrname()+fault_note);
////                map.put("receive_account", null);
////            }
////            workListDetailModel.setBegin_time(null);
////            workListDetailModel.setStatus(status);
////            workListDetailModel.setIs_main(1);
////            workListModel.setStatus(status);
////
////            workListModel.setFacility_id((int) assetModel.getIntsiteid());
////            workListModel.setCreate_user_account(account);
////            workListModel.setCreate_time(now);
////            workListDetailModel.setFrom_code(from_code);
////            workListModel.setWork_template_code(work_template_code);
////            workListModel.setParent_code("0");
////            workListModel.setRelation_type(2);
////
////            workListModel.setPool_id("-1");
////            workListModel.setWorks_title(assetModel.getStrname() + fault_note);
////            workListDetailModel.setTitle(assetModel.getStrname() + fault_note);
////            workListDetailModel.setWork_code(work_code);
////            workListDetailModel.setWork_template_code(work_template_code);
////            workListDetailModel.setRelation_type(2);
////            map.put("create_user_account", account);//发送短信人
////            map.put("title_page", assetModel.getStrname() + fault_note);
////            map.put("sub_work_code", sub_work_code);
////            map.put("content", content);//发送短信内容
////            map.put("businessType", String.valueOf(work_type_id));     //工单
////            map.put("deadline_time", deadline_time);
////            map.put("create_time", occur_time);
////            map.put("status", String.valueOf(status));
////            int i = workListService.addWorkList(schema_name, workListModel);
////            int s = workListService.addWorkDetailList(schema_name, workListDetailModel);
////
////            // 支援人员
////            if (null != help_account && !"".equals(help_account)) {
////                String[] helpAccounts = help_account.split(",");
////                workSheetHandleService.doSaveMultipleTableDataByList(schema_name, sub_work_code, "_sc_work_dutyman_hour", helpAccounts);
////            }
////
////            //保存上报问题
////            int doCount = i + s;
////            if (doCount >= 2) {
////                map.put("facility_id", String.valueOf(workListModel.getFacility_id())); // 所属位置
////                workflowStartResult = workflowService.startWithForm(schema_name, flow_id, account, map);
////                if (!workflowStartResult.isStarted()) {
////                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_PROCESS_ERROR));//维修流程启动失败
////                }
////                //更改设备的状态，去掉更新状态，yzj  2019-07-02
////                //assetService.setStatus(schema_name, assetModel.get_id(), Integer.parseInt(asset_status));
////
////                // 获取操作人
////                //如无分配人员，则给分配人员发送短信
////                result.setCode(1);
////                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_REPORT_REPAIR_SUCCESSFUL));//上报维修成功
////                result.setContent(sub_work_code);
////                //记录历史
////                logService.AddLog(schema_name, "repair", sub_work_code, selectOptionService.getLanguageInfo(LangConstant.SUCCESS_ASSET_MONITOR_B), account);
////                return result;
////            } else {
////                result.setCode(0);
////                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_REPORT_REPAIR_FAIL));//问题上报出现问题
////                return result;
////            }
////        } catch (Exception ex) {
////            // 手动回滚
////            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
////            result.setCode(0);
////            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL));//保存是吧
////            //系统错误日志
////            logService.AddLog(schema_name, "system", "repair_add", selectOptionService.getLanguageInfo(LangConstant.SUCCESS_ASSET_MONITOR_B) + ex.getMessage(), account);
////            return result;
////        }
////    }
//
//    //获取维修的工单类型
//    @RequestMapping("/get_worksheet_repair")
//    public ResponseModel getworksheetRepair(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //生成企业库名
//        try {
//
//            SystemConfigData scd = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.PHONE_REPIAR_WORK_TYPE_ID);
//            Map<String, Object> map = selectOptionService.getOptionByCode(schema_name, "work_type", scd.getSettingValue());
//            if (null != map && map.size() > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));//成功
//                result.setContent(map);
//            }
//            return result;
//        } catch (Exception e) {
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_ERROR));//当前维修单有问题
//            result.setContent("");
//            return result;
//        }
//    }
//
//    //获取维修的工单类型
//    @RequestMapping("/get_worksheet_setup")
//    public ResponseModel getworksheetSetup(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //生成企业库名
//        try {
//
//            SystemConfigData scd = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.PHONE_SETUP_WORK_TYPE_ID);
//            Map<String, Object> map = selectOptionService.getOptionByCode(schema_name, "work_type", scd.getSettingValue());
//            if (null != map && map.size() > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));//成功
//                result.setContent(map);
//            }
//            return result;
//        } catch (Exception e) {
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_REPAIR_ERROR));//当前维修单有问题
//            result.setContent("");
//            return result;
//        }
//    }
}
