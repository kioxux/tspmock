package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserRegisrerData;
//import com.gengyun.senscloud.model.UserClientModal;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.UserClientService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.util.AESUtils;
//import com.gengyun.senscloud.util.StringUtil;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @Author: Wudang Dong
// * @Description:
// * @Date: Create in 下午 2:01 2019/7/18 0018
// */
//@RestController
//@RequestMapping("/service")
public class UserClientServiceController {
//
//    @Autowired
//    UserClientService userClientService;
//    @Autowired
//    UserService userService;
//    //企业库前缀，移动端过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    //查询客户用户列表
//    @RequestMapping("/get_user_client_list_by_account")
//    public ResponseModel getUserClientListByAccount(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        int pageSize = Integer.parseInt(request.getParameter("page_size"));
//        int pageNumber = Integer.parseInt(request.getParameter("page"));
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            int begin = pageSize * pageNumber;
//            List<UserClientModal> userClientModalList = userClientService.getUserClientListByAccout(schema_name, account, pageSize, begin);
//            int count = userClientService.getUserClientListByAccoutCount(schema_name, account);
//            JSONObject pageResult = new JSONObject();
//            pageResult.put("total", count);
//            pageResult.put("rows", userClientModalList);
//            return ResponseModel.ok(net.sf.json.JSONObject.fromObject(pageResult.toString()));
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取成功
//            return result;
//        }
//    }
//
//    //查询客户用户列表数量 app已经不用
//    @RequestMapping("/get_user_client_list_by_account_count")
//    public ResponseModel getUserClientListByAccountCount(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            int count = userClientService.getUserClientListByAccoutCount(schema_name, account);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(count);
//            return result;
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //新增客户用户
//    @RequestMapping("/add_user_client")
//    @Transactional //支持事务
//    public ResponseModel addUserClient(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是上报问题的人员
//        try {
//            String user_name = request.getParameter("user_name");
//            String user_account = request.getParameter("user_account");
//            String user_mibole = request.getParameter("user_mobile");
//            String client_id = request.getParameter("client_id");
//            String remark = request.getParameter("remark");
//
//            UserClientModal model = new UserClientModal();
//
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            model.setUser_name(user_name);
//
//            model.setUser_account(user_account);
//            model.setUser_mobile(user_mibole);
//            model.setClient_id(Integer.parseInt(client_id));
//            model.setRemark(remark);
//            model.setCreate_time(now);
//            model.setCreate_user_account(account);
//
//            int doAdd = userClientService.addUserClient(request, schema_name, model);
//            if (doAdd == 1) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_USER_SUCCESS));
//            } else if (doAdd == 2) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_REPEAT_MOBILE));//用户账号或手机号重复
//            } else {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_USER_ERROR_CONTACT_MANAGE));//新添客户用户失败，请联系管理员。
//            }
//
//            return result;
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_USER_ERROR_CONTACT_MANAGE));//新添客户用户失败，请联系管理员。
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return result;
//        }
//    }
//
//    //新增用户位置坐标点
//    @RequestMapping("/add_user_position")
//    @Transactional //支持事务
//    public ResponseModel addUserPosition(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //当前登录人坐标点位
//        String location = request.getParameter("location");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            Map<String, Object> positionMap = new HashMap<String, Object>();
//            positionMap.put("schema_name", schema_name);
//            positionMap.put("user_account", account);
//            positionMap.put("location", location);
//            return userService.insertUserPosition(positionMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseModel.error(LangConstant.FAIL_A);
//        }
//    }
}
