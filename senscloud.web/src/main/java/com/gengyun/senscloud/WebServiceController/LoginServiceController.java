package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.common.SystemConfigConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.model.company_model.CompanyHireInfoModel;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.*;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.CacheControl;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.RequestContextUtils;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.concurrent.TimeUnit;
//
//@RestController
///**
// * 微信登录服务接口
// */
//@RequestMapping("/service")
public class LoginServiceController {
//
//    @Autowired
//    private CompanyService companyService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    SystemPermissionService systemPermissionService;
//
//    @Autowired
//    UserInfoEditService userInfoEditService;
//
//    @Autowired
//    IndexAppService indexAppService;
//
//    @Autowired
//    LoginLogService loginLogService;
//
//    @Autowired
//    ClientTokenService clientTokenService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    ResetPasswordService resetPasswordService;
//
//    @Autowired
//    WxMsgService wxMsgService;
//
//    @Autowired
//    MessageService messageService;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //根据域名，查询是否需要单点登录，以及是否显示企业自己的登录logo和系统名称，以及是否现实技术支持
//    @RequestMapping("/senscloudInfo")
//    public ResponseModel getSenscloudInitInfo(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String basePath = request.getParameter("baseurl");
//            //获取是否需要输入密码
//            if (basePath != null && !basePath.isEmpty()) {
//                basePath = basePath.toLowerCase().replaceAll("https://", "").replaceAll("http://", "");
//                if (basePath.endsWith("/") || basePath.endsWith("\\")) {
//                    basePath = basePath.substring(0, basePath.length() - 1);
//                }
//            }
//            List<DomainPortUrlData> domainPortUrlData = indexAppService.findAllDomain(basePath);
//            if (domainPortUrlData != null && !domainPortUrlData.isEmpty()) {
//                DomainPortUrlData item = domainPortUrlData.get(0);
//                result.setCode(2);
//                result.setContent(item);
//            } else {
//                result.setCode(1);
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取企业域名有问题！");
//            return result;
//        }
//    }
//
//    //检查用户属于哪个企业
//    @RequestMapping("/check_company")
//    public ResponseModel getUserCompany(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String timeId = SCTimeZoneUtil.getTimeZoneIdFromRequest();
//            String userCode = request.getParameter("usercode");
//            //根据用户，判断用户属于哪个企业
//            List<Company> companies = companyService.findCompanyByUser(userCode);
//            if (companies != null && !companies.isEmpty()) {
//                result.setCode(1);
//                result.setContent(companies);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg("用户不存在，或者企业被禁用");
//                return result;
//            }
//        } catch (Exception e) {
//            result.setCode(0);
//            result.setMsg("数据错误。");
//            return result;
//        }
//    }
//
//    //直接进行登录
//    @RequestMapping("/login")
//    public ResponseModel execute(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String companyId = request.getParameter("company_id");
//            String clientType = request.getParameter("clientType");
//            String lang = request.getParameter("lang");
//            String pc = SensConstant.PC_CLIENT_NAME;
//            String app = SensConstant.APP_CLIENT_NAME;
//            String ios = SensConstant.IOS_CLIENT_NAME;
//            String wechat = SensConstant.WECHAT_CLIENT_NAME;
//            String dingTalk = SensConstant.DINGTALK_CLIENT_NAME;
//            if (clientType == null || (!pc.equals(clientType) && app.equals(clientType) && ios.equals(clientType) && wechat.equals(clientType) && dingTalk.equals(clientType))) {
//                result.setCode(0);
//                result.setMsg("系统暂时不支持该您使用的客户端，抱歉您将无法正常使用本系统");
//                return result;
//            }
//            //region 获得用户所属的企业
//            if (RegexUtil.isNull(companyId)) {
//                return ResponseModel.error("用户所属的企业信息有误。");
//            }
//            Company company = companyService.findById(Integer.parseInt(companyId));
//            if (company == null) {
//                return ResponseModel.error("账号不存在,请与管理员申请开通服务！");
//            }
//
//            switch (company.getStatus()) {
//                case -1:
//                    return ResponseModel.error("您的企业已经被锁定，请联系管理员！");
//                case -2:
//                    return ResponseModel.error("您的企业已经被注销，请联系管理员！");
//                default:
//                    break;
//            }
//
////            authService.saveCompany(request, company);
////            //endregion
////            String schema_name = authService.getCompany(request).getSchema_name();
//            //生成企业库名
//            String schema_name = schema_name_pre + companyId;
//
//            //验证企业是否过期
//            CompanyHireInfoModel companyHireInfoModel = userService.selectServiceDateCompany(schema_name, company.getId());
//            if (companyHireInfoModel != null) {
//                Timestamp now = new Timestamp(System.currentTimeMillis());
//                if (companyHireInfoModel.getEnd_time().before(now)) {
//
//                    result.setCode(0);
//                    result.setMsg("您的账户已到期，到期后服务将被停止，请您及时联系销售人员进行续费或备份好数据，到期未续费的账户数据我们将为您保留7天。");
//                    return result;
//                }
//                //将到期
//                Date date = new Date();
//                Calendar rightNow = Calendar.getInstance();
//                rightNow.setTime(date);
//                rightNow.add(Calendar.MONTH, 1);//当前时间加一月
//                Date dt1 = rightNow.getTime();
//                if (companyHireInfoModel.getEnd_time().before(dt1)) {//一个月服务到期
//                    SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT);;
//                    result.setCode(0);
//                    result.setMsg("您的账户将于" + sdf.format(companyHireInfoModel.getEnd_time()) + "到期，到期后服务将被停止，请您及时联系销售人员进行续费或备份好数据，到期未续费的账户数据我们将为您保留7天。");
//                    return result;
//                }
//            } else {
//                result.setCode(JsonResult.CODE_ERROR);
//                result.setMsg("账号所属企业服务信息存在异常");
//                return result;
//            }
//
//            String userCode = request.getParameter("usercode");
//            String password = request.getParameter("password");
//            if (RegexUtil.isNull(userCode)) {
//                result.setCode(0);
//                result.setMsg("账号不能为空");
//                return result;
//            }
//            if (RegexUtil.isNull(password)) {
//                result.setCode(0);
//                result.setMsg("密码不能为空");
//                return result;
//            }
//            if (RegexUtil.isMobile(userCode)) {
//                int count = userService.checkUserByMobile(schema_name, userCode);
//                if (count < 1) {
//                    result.setCode(0);
//                    result.setMsg("该手机号未注册，请先注册！");
//                    return result;
//                }
//            }
//            //进行登录验证
//            password = User.encryptPassword(AESUtils.decryptAES(password));
//            User userInfo;
//            if (RegexUtil.isMobile(userCode)) {
//                userInfo = userService.loginByMobile(schema_name, userCode, password, request);
//            } else {
//                userInfo = userService.loginByAccount(schema_name, userCode, password, request);
//            }
//
//            if (userInfo == null) {
//                result.setCode(0);
//                result.setMsg("帐号或密码错误，请重试！");
//                return result;
//            } else {
//                //添加token验证
////                boolean updateStoken = userService.updateUserToken(schema_name ,token,userInfo.getId());
////                if(!updateStoken){
////                    result.setCode(0);
////                    result.setMsg("token信息生成有误！");
////                    return result;
////                }
//                //PC端登录接口方法，直接登录方式为PC
//                String account = userInfo.getAccount();
//                String token = TokenUtils.getToken(clientType, account);//生成token并保存到用户表中，并返回给前端
//                LoginLog loginLog = new LoginLog();
//                loginLog.setAccount(account);
//                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                loginLog.setLogin_time(timestamp);
//                loginLog.setLoginout_time(timestamp);
//                loginLog.setClient_name(clientType);
//                loginLog.setToken(token);
//                HttpHelper.clientInfo(loginLog);
//                loginLogService.insertLoginLog(schema_name, loginLog);
//                clientTokenService.refreshTokenCacheList(schema_name);
//                //移除对应的account 的 缓存并更新最新缓存值&& 退出账号清空缓存
//                userInfo.setToken(token);
//                result.setCode(1);
//                result.setMsg("登录成功！");
//                //获取当前用户所包含的菜单与操作
//                List<UserFunctionData> menuAndFunctionList = userService.getUserMenuAndFunction(schema_name, userInfo.getId());
//                userInfo.setMenusFunctions(menuAndFunctionList);
//                // 用户默认语言设置
//                if (null == lang || lang.isEmpty()) {
//                    lang = userInfo.getLanguage_tag();
//                }
//                //用户的资源
//                if (null == lang || lang.isEmpty()) {
//                    lang = "zh_CN";
//                }
//                Map<String, String> resource = resourceService.resourceByIdAndLang(company.getId(), lang);
//                String userWxCode = request.getParameter("userWxCode");
//                String session_key = request.getParameter("session_key");
//                String encryptedData = request.getParameter("encryptedData");
//                String iv = request.getParameter("iv");
//                String signature = request.getParameter("signature");
//                String rawData = request.getParameter("rawData");
//                String wxMsgTemplateList = wxMsgService.updateUserWxOpenId(schema_name, userInfo, userWxCode, session_key, encryptedData, iv, signature, rawData);
//                //系统配置信息-小程序图片上传是否支持选择手机相册
//                SystemConfigData wxChooseAlbum = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.WX_CHOOSE_ALBUM);
//                userInfo.setSystemConfigDatas(Arrays.asList(wxChooseAlbum));
//                userInfo.setResource(resource);
//                userInfo.setPassword("");
//                result.setContent(userInfo);
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("登录时出现错误！");
//            return result;
//        }
//    }
//
//    //跳转第三方，进行单点登录
//    @RequestMapping("/passport_login")
//    public ResponseModel executePassport(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            //是否需要单点登录，1：需要；2：不需要；如果需要，则登录时用单点登录接口验证
//            String isNeedPassport = request.getParameter("isNeedPassport");
//            if (isNeedPassport.equals("1")) {
//                String userCode = request.getParameter("usercode");
//                String password = request.getParameter("password");
//                if (RegexUtil.isNull(userCode)) {
//                    result.setCode(0);
//                    result.setMsg("账号不能为空");
//                    return result;
//                }
//                if (RegexUtil.isNull(password)) {
//                    result.setCode(0);
//                    result.setMsg("密码不能为空");
//                    return result;
//                }
//
//                //需要单点登录，调用第三方接口，验证用户名和密码
//                String basePath = request.getParameter("baseurl");
//                //获取是否需要输入密码
//                if (basePath != null && !basePath.isEmpty()) {
//                    basePath = basePath.replaceAll("http://", "").replaceAll("https://", "").toLowerCase();
//                    if (basePath.endsWith("/") || basePath.endsWith("\\")) {
//                        basePath = basePath.substring(0, basePath.length() - 1);
//                    }
//                }
//                List<DomainPortUrlData> domainPortUrlData = indexAppService.findAllDomain(basePath);
//                String passport = "";
//                if (domainPortUrlData != null && !domainPortUrlData.isEmpty()) {
//                    DomainPortUrlData item = domainPortUrlData.get(0);
//                    //移动端验证用户接口
//                    passport = item.getBak_url5();
//                }
//
//                String validUserResult = HttpHelper.httpPost(passport, "{'user_name':'" + userCode + "','password':'" + password + "','valid_key':'123'}");
//                //获取返回结果json串
//                Map resultObject = (Map) com.alibaba.fastjson.JSON.parse(validUserResult);
//                int loginResult = 0;
//                if (resultObject.size() > 0) {
//                    for (Object obj : resultObject.keySet()) {
//                        if (obj.equals("code")) {
//                            loginResult = (Integer) resultObject.get(obj);
//                            break;
//                        }
//                    }
//                }
//
//                if (loginResult == 1) {
//                    //登录成功，获取用户所属的企业
//                    result.setMsg("登录成功！");
//
//                    //根据用户，判断用户属于哪个企业
//                    List<Company> companies = companyService.findCompanyByUser(userCode);
//                    if (companies != null && !companies.isEmpty()) {
//                        result.setCode(1);
//                        result.setContent(companies);
//                        return result;
//                    } else {
//                        result.setCode(0);
//                        result.setMsg("用户的企业不存在，或者被禁用");
//                        return result;
//                    }
//                } else {
//                    result.setCode(0);
//                    result.setMsg("帐号或密码错误，请重试！");
//                    return result;
//                }
//            } else {
//                result.setCode(0);
//                result.setMsg("登录失败，请求错误！");
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("登录时出现错误！");
//            return result;
//        }
//    }
//
//    //获取当前登录用户分配的菜单
//    @RequestMapping("/get_user_menus")
//    public ResponseModel getUserMenus(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String userCode = request.getParameter("usercode");
//            String companyId = request.getParameter("company_id");
//            //生成企业库名
//            String schema_name = schema_name_pre + companyId;
//            if (RegexUtil.isNull(userCode)) {
//                result.setCode(0);
//                result.setMsg("账号不能为空");
//                return result;
//            }
//            User userInfo = userService.getUserByAccount(schema_name, userCode);
//            if (userInfo == null) {
//                result.setCode(0);
//                result.setMsg("帐号错误，请更换有效账号！");
//                return result;
//            } else {
//                result.setCode(1);
//                //获取当前用户所包含的菜单与操作
//                List<UserFunctionData> menuAndFunctionList = userService.getUserMenuAndFunction(schema_name, userInfo.getId());
//                userInfo.setMenusFunctions(menuAndFunctionList);
//                result.setContent(userInfo);
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("用户菜单获取有误！");
//            return result;
//        }
//    }
//
//
//    //修改密码
//    @RequestMapping("/modifyDoor")
//    public ResponseModel modifyDoor(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String companyId = request.getParameter("companyId");
//            //生成企业库名
//            String schema_name = schema_name_pre + companyId;
//
//            //region 获得用户所属的企业
//            if (RegexUtil.isNull(schema_name)) {
//                return ResponseModel.error("所在企业账号不允许登录！");
//            }
//
//            String userCode = request.getParameter("account");
//            String oldPassword = request.getParameter("olddoor");
//            String newPassword = request.getParameter("newdoor");
//            if (RegexUtil.isNull(userCode)) {
//                result.setCode(0);
//                result.setMsg("账号为空");
//                return result;
//            }
//            if (RegexUtil.isNull(oldPassword) || RegexUtil.isNull(newPassword)) {
//                result.setCode(0);
//                result.setMsg("密码不能为空");
//                return result;
//            }
//
//            // 检查密码是否为强验证密码
//            SystemConfigData dataPassword = systemConfigService.getSystemConfigData(schema_name, "is_validate_strong_password");
//            if ((null != dataPassword && dataPassword.getSettingValue().equals("1")) && RegexUtil.isStrongPwd(newPassword) == false) {
//                //密码非强密码，需修改
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.STRONG_PASSWORD));//强验证，必填字母数字及特殊字符，且以字母开头，8位以上。
//                return result;
//            }
//            //解密
//            oldPassword = AESUtils.decryptAES(oldPassword);
//            newPassword = AESUtils.decryptAES(newPassword);
//
//            //进行登录验证
//            oldPassword = User.encryptPassword(oldPassword);
//            newPassword = User.encryptPassword(newPassword);
//            int count = userInfoEditService.checkUserPassword(schema_name, oldPassword, userCode);
//            if (count > 0) {
//                //更新新密码
//                int updateStatus = userInfoEditService.updateUserPassword(schema_name, newPassword, userCode);
//                if (updateStatus > 0) {
//                    result.setCode(1);
//                    result.setMsg("密码更新成功。");
//                } else {
//                    result.setCode(-1);
//                    result.setMsg("密码更新失败。");
//                }
//            } else {
//                //旧密码输入错误
//                result.setCode(-1);
//                result.setMsg("原密码输入错误。");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("修改密码失败。");
//            return result;
//        }
//    }
//
//    @RequestMapping(value = {"/resource"})
//    public ResponseEntity<Map<String, String>> resource(@RequestParam(value = "companyId") Long id, @RequestParam(value = "lang", required = false) String lang,
//                                                        HttpServletRequest request) {
//        if (StringUtils.isEmpty(lang)) {
//            Locale locale = RequestContextUtils.getLocale(request);
//            lang = locale.toString();
//        }
//        Map<String, String> resource = resourceService.resourceByIdAndLang(id, lang);
//        if (resource == null || resource.size() == 0) {
//            resource = resourceService.resourceByIdAndLang(id, "zh_CN");
//        }
//        return ResponseEntity.ok().cacheControl(CacheControl.maxAge(1, TimeUnit.HOURS).cachePublic()).body(resource);
//    }
//
//    @RequestMapping("/send_verification_code")
//    public ResponseModel sendVerificationCode(@RequestParam(name = "companyId") Long company_id, @RequestParam(name = "phoneNumber") Long phoneNumber) {
//        Company company = companyService.findById(company_id);
//        String schema_name = company.getSchema_name();
//        return resetPasswordService.sendVerificationCode(schema_name, String.valueOf(phoneNumber));
//    }
//
//    @RequestMapping("/reset_password")
//    public ResponseModel resetPassword(@RequestParam(name = "companyId") Long company_id, @RequestParam(name = "phoneNumber") Long phoneNumber,
//                                       @RequestParam(name = "verificationCode") Long verificationCode,
//                                       @RequestParam(name = "newPassWord") String newPassWord, HttpServletRequest request) {
//        Company company = companyService.findById(company_id);
//        String schema_name = company.getSchema_name();
//
//        // 检查密码是否为强验证密码
//        SystemConfigData dataPassword = systemConfigService.getSystemConfigData(schema_name, "is_validate_strong_password");
//        if ((null != dataPassword && dataPassword.getSettingValue().equals("1")) && RegexUtil.isStrongPwd(newPassWord) == false) {
//            //密码非强密码，需修改
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.STRONG_PASSWORD));//强验证，必填字母数字及特殊字符，且以字母开头，8位以上。
//        }
//        newPassWord = AESUtils.decryptAES(newPassWord);
//        return resetPasswordService.resetPassword(schema_name, String.valueOf(phoneNumber), newPassWord, String.valueOf(verificationCode));
//    }
//
//    /**
//     * 获取用户公众号关注状态
//     *
//     * @return
//     */
//    @RequestMapping("/getFollowOAStatus")
//    public ResponseModel getFollowOAStatus() {
//        try {
//            return ResponseModel.ok(userService.getFollowOAStatus(WxUtil.doGetSchemaName(), WxUtil.doGetAccount()));
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
}
