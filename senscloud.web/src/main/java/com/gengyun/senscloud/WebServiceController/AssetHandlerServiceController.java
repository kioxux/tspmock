package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.AssetTransferAuditNode;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.gengyun.senscloud.util.WxUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Map;
//
///**
// * 设备工单处理接口，供移动端程序使用
// */
//@RestController
//@RequestMapping("/service")
public class AssetHandlerServiceController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    private AssetInventoryService assetInventoryService;
//    @Autowired
//    AssetTransferService assetTransferService;
//    @Autowired
//    AssetDiscardService assetDiscardService;
//    @Autowired
//    UserService userService;
//
//    /**
//     * 申请设备报废（无审核）
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetDiscard_addWithoutAudit")
//    @Transactional //支持事务
//    public ResponseModel doSubmitAssetDiscardWithoutAudit(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = WxUtil.doGetSchemaName();
//            User user = authService.getLoginUser(request);
//            user = commonUtilService.checkUser(schemaName, user, request);
//            return assetDiscardService.add(schemaName, request, user, processDefinitionId, paramMap, false);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 申请设备报废
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetDiscard_add")
//    @Transactional //支持事务
//    public ResponseModel doSubmitAssetDiscard(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = WxUtil.doGetSchemaName();
//            User user = authService.getLoginUser(request);
//            user = commonUtilService.checkUser(schemaName, user, request);
//            return assetDiscardService.add(schemaName, request, user, processDefinitionId, paramMap, true);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备报废审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetDiscard_disCardAudit")
//    @Transactional //支持事务
//    public ResponseModel assetDiscardAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return assetDiscardService.disCardAudit(WxUtil.doGetSchemaName(), request, paramMap, user, false);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备报废最后审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetDiscard_lastDisCardAudit")
//    @Transactional //支持事务
//    public ResponseModel lastAssetDiscardAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return assetDiscardService.disCardAudit(WxUtil.doGetSchemaName(), request, paramMap, user, true);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备调度-调出审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetTransfer_transferOutAudit")
//    @Transactional //支持事务
//    public ResponseModel transferOutAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return assetTransferService.transferAudit(WxUtil.doGetSchemaName(), request, paramMap, user, AssetTransferAuditNode.TRANSFER_OUT_AUDIT);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备调度-接收审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetTransfer_transferInAudit")
//    @Transactional //支持事务
//    public ResponseModel transferInAudit(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return assetTransferService.transferAudit(WxUtil.doGetSchemaName(), request, paramMap, user, AssetTransferAuditNode.TRANSFER_IN_AUDIT);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备调度-调出设备
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetTransfer_transferOutAsset")
//    @Transactional //支持事务
//    public ResponseModel transferOutAsset(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return assetTransferService.transferAudit(WxUtil.doGetSchemaName(), request, paramMap, user, AssetTransferAuditNode.TRANSFER_OUT_ASSET);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备调度-接收设备
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_assetTransfer_transferInAsset")
//    @Transactional //支持事务
//    public ResponseModel transferInAsset(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            return assetTransferService.transferAudit(WxUtil.doGetSchemaName(), request, paramMap, user, AssetTransferAuditNode.TRANSFER_IN_ASSET);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
//
//    /**
//     * 设备盘点提交
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_asset_inventory_pcInventory")
//    @Transactional //支持事务
//    public ResponseModel assetPcInventory(
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            return assetInventoryService.pcInventory(WxUtil.doGetSchemaName(), request, paramMap);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//获取失败
//        }
//    }
}
