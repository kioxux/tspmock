package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.SimpleCompany;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserRegisrerData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.AESUtils;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.gengyun.senscloud.util.StringUtil;
//import com.gengyun.senscloud.util.WxUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/service")
public class UserRegisterServiceController {
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    UserRegisterService userRegisterService;
//
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private CompanyService companyService;
//    @Autowired
//    VerificationCodeService verificationCodeService;
//
//    //企业库前缀，移动端过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    // 按id，查询企业信息
//    @RequestMapping("/get_simple_company_info")
//    public ResponseModel getSimplyCompanyInfo(@RequestParam(name = "companyId") Long company_id) {
//        Company company = companyService.findById(company_id);
//        SimpleCompany returnCompany = new SimpleCompany();
//        returnCompany.setId(company.getId());
//        returnCompany.setCompany_name(company.getCompany_name());
//        return ResponseModel.ok(returnCompany);
//    }
//
//    //用户注册
//    @RequestMapping("/add_user_register")
//    @Transactional //支持事务
//    public ResponseModel addUserRegister(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            // 验证码是否一致
//            String v_code = request.getParameter("vf_code");
//            if (StringUtil.isEmpty(v_code)) {
//                return ResponseModel.errorMsg("验证码不对");
//            }
//            // 用户注册手机
//            String user_mibole = request.getParameter("user_mobile");
//            //验证码是否包含
//            String systemVerificationCode = verificationCodeService.getVerificationCode(schema_name, user_mibole);
//            String verificationCode = request.getParameter("vf_code");
//            if (!systemVerificationCode.equals(verificationCode)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.VERITFY_PF));
//            }
//
//            // 用户注册信息
//            String user_name = request.getParameter("user_name");
//            String password = request.getParameter("password");
//            UserRegisrerData model = new UserRegisrerData();
//            model.setUser_name(user_name);
//            model.setUser_mobile(user_mibole);
//            password = User.encryptPassword(AESUtils.decryptAES(password));
//            model.setPassword(password);
//
//            int doAdd = userRegisterService.addUserRegister(request, schema_name, model);
//            if (200 == doAdd) {
//                return ResponseModel.ok("");
//            } else {
//                return ResponseModel.errorMsg(Integer.toString(doAdd));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg("-1");
//        }
//    }
//
//    /**
//     * 用户注册审核
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("_user_register_doAuditUserRegister")
//    @Transactional //支持事务
//    public ResponseModel userRegisterAcceptance(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = SensConstant.SCHEMA_PREFIX + companyId;
//            User user = userService.getUserByAccount(schema_name, account);
//            if (user.getId().isEmpty()) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.GET_DATA_WRONG));//获取数据失败
//            } else {
//                int doAuditResult = userRegisterService.auditUserRegister(request, schema_name, Long.parseLong(companyId), paramMap);
//                return ResponseModel.ok(String.valueOf(doAuditResult));
//            }
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));//操作异常
//        }
//    }
//
//    //获取用户注册验证码
//    @RequestMapping("/send_user_register_verification_code")
//    public ResponseModel sendVerificationCode(@RequestParam(name = "companyId") Long company_id, @RequestParam(name = "phoneNumber") String phoneNumber) {
//        Company company = companyService.findById(company_id);
//        String schema_name = company.getSchema_name();
//        return userRegisterService.sendUserRegisterVerificationCode(schema_name, phoneNumber);
//    }
}
