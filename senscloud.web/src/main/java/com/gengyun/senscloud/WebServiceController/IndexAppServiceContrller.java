package com.gengyun.senscloud.WebServiceController;

//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.flowable.CustomTaskEntityImpl;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.DefinitionKeyResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.ToDoDataListResult;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.WxUtil;
//import org.flowable.engine.repository.ProcessDefinition;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
///**
// * 微信登录服务接口，首页
// */
//
//@RequestMapping("/service")
public class IndexAppServiceContrller {
//    @Autowired
//    IndexAppService indexAppService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    MessageCenterService messageCenterService;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //待办事项的数量
////    @RequestMapping("/index_todo_count")
////    public ResponseModel getToDoCount(HttpServletRequest request) {
////        //当前登录人
////        String companyId = request.getParameter("companyId");
////        String account = request.getParameter("account");
////        boolean mineOnly = Boolean.parseBoolean(request.getParameter("mineOnly"));
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
//////        User currentUser = userService.getUserByAccount(schema_name, account);
////        ResponseModel result = new ResponseModel();
//////        int toDoCount = 0;
////        try {
////            if (account == null || account.equals("null") || account.isEmpty()) {
////                result.setCode(2);
////                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//当前登录人有错误
////                return result;
////            }
////            //获取待办数量
////
////            //获取用户所属的位置
////            List<String> facilityIds = new ArrayList<>();
////            User userInfo = userService.getUserByAccount(schema_name, account);
//////            List<String> roles = userService.getUserRolesByAccount(schema_name, account);
////            List<String> roles = null;
////            boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, userInfo.getId(), "todo_list");
////            if (isAllFacility) {
////                if (mineOnly) {
////                    account = userInfo.getAccount();
////                } else {
////                    account = null;
////                }
////            } else {
////                if (!mineOnly) {
////                    if (userInfo.getRoles() != null && userInfo.getRoles().size() > 0) {
////                        roles = new ArrayList<>(userInfo.getRoles().keySet());
////                    }
////                }
////                facilityIds = new ArrayList<>(userInfo.getFacilities().keySet());
////            }
////
////            //获取新工单的待办总数
////            long total = workflowService.getTasksCount(schema_name, "", facilityIds, null, account, roles, null, null, null);
////            int page_num = (int) total / 100;
////            int page_size = 100;
////            long page_decimal = total % 100;
////            if (page_decimal != 0) {
////                page_num += 1;
////            }
////            int total_num = 0;
////            for (int j = 0; j < page_num; j++) {
////                List<ToDoDataListResult> toDoDataListResultList = this.getToDoDataListFilter(schema_name, "", facilityIds, account, roles, j, page_size);
////                if (toDoDataListResultList != null) {
////                    int resultNum = toDoDataListResultList.size();
////                    total_num += resultNum;
////                }
////            }
////            result.setCode(1);
////            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
////            result.setContent(total_num);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
////            //系统错误日志
////            logService.AddLog(schema_name, "system", "index_todo_count", selectOptionService.getLanguageInfo(LangConstant.LOG_TODO_NUM_ERROR) + ex.getMessage(), account);//待办事项数量获取出现了问题
////            return result;
////        }
////    }
////
////    @RequestMapping(value = "/flow_type_list")
////    public ResponseModel getDefinitionKeysList(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
//////        ResponseModel result = new ResponseModel();
////        Set<DefinitionKeyResult> definitionKeyResultSet = new HashSet<>();
////        for (ProcessDefinition definition : workflowService.getDefinitions(schema_name, null, null, null, null, null, null, null)) {
////            DefinitionKeyResult definitionResult = new DefinitionKeyResult();
////            definitionResult.setKey(definition.getKey());
////            definitionResult.setName(definition.getName().split("_")[0]);
////            definitionKeyResultSet.add(definitionResult);
////        }
////        return ResponseModel.ok(new ArrayList<>(definitionKeyResultSet));
////    }
//
//    //待办事项的数量
//    @RequestMapping("/index_todo_count")
//    public ResponseModel getToDoCount(HttpServletRequest request) {
//        //当前登录人
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
////        boolean mineOnly = Boolean.parseBoolean(request.getParameter("mineOnly"));
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
////        User currentUser = userService.getUserByAccount(schema_name, account);
//        ResponseModel result = new ResponseModel();
////        int toDoCount = 0;
//        try {
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//当前登录人有错误
//                return result;
//            }
//            //获取待办数量
//            int total = 0;
//            List<CustomTaskEntityImpl> resultList = workflowService.getTasks(schema_name, null, null, null, account, null, true, null, null, null, null, null);
//            if (null != resultList && resultList.size() > 0) {
//                String subWorkCode = "";
//                for (CustomTaskEntityImpl task : resultList) {
//                    if (null == task.getProcessVariables() || null == task.getProcessVariables().get("title_page")) {
//                        if (null != task.getTaskLocalVariables() && null != task.getTaskLocalVariables().get("sub_work_code")) {
//                            subWorkCode = task.getTaskLocalVariables().get("sub_work_code").toString();
//                        } else {
//                            subWorkCode = "";
//                        }
//                    } else {
//                        if (null != task.getProcessVariables() && null != task.getProcessVariables().get("sub_work_code")) {
//                            subWorkCode = task.getProcessVariables().get("sub_work_code").toString();
//                        } else {
//                            subWorkCode = "";
//                        }
//                    }
//                    if (subWorkCode.startsWith("UC") || subWorkCode.startsWith("TRP") || subWorkCode.startsWith("TR")) {
//                        continue; // 手机端待办仅出现工单信息（维修、保养、巡检、点检）、设备、备件
//                    } else {
//                        total++;
//                    }
//                }
//            }
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(total);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "index_todo_count", selectOptionService.getLanguageInfo(LangConstant.LOG_TODO_NUM_ERROR) + ex.getMessage(), account);//待办事项数量获取出现了问题
//            return result;
//        }
//    }
//
//    @RequestMapping(value = "/flow_type_list")
//    public ResponseModel getDefinitionKeysList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
//        Set<DefinitionKeyResult> definitionKeyResultSet = new HashSet<>();
//        for (ProcessDefinition definition : workflowService.getDefinitions(schema_name, null, null, null, null, null, null, null)) {
//            DefinitionKeyResult definitionResult = new DefinitionKeyResult();
//            definitionResult.setKey(definition.getKey());
//            definitionResult.setName(definition.getName().split("_")[0]);
//            definitionKeyResultSet.add(definitionResult);
//        }
//        return ResponseModel.ok(new ArrayList<>(definitionKeyResultSet));
//    }
//
//
//    //待办事项list
//    @RequestMapping("/index_todo_list")
//    public ResponseModel getToDoDataList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        boolean mineOnly = Boolean.parseBoolean(request.getParameter("mineOnly"));
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
////        User currentUser = userService.getUserByAccount(schema_name, account);
//        ResponseModel result = new ResponseModel();
//        try {
//            int page = Integer.parseInt(request.getParameter("page"));
//            int page_size = Integer.parseInt(request.getParameter("page_size"));
//            String work_type = request.getParameter("work_type");
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//当前登录人有错误
//                return result;
//            }
//            //获取待办事项
//            int newPage = page + 1;
//
////            List<String> roles = userService.getUserRolesByAccount(schema_name, account);
//            List<String> roles = null;
//            //获取用户所属的位置
//            User user = userService.getUserByAccount(schema_name, account);
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "todo_list");
//            boolean isAllFacility = false;
//            boolean isDistribute = false;
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    if (functionData.getFunctionName().equals("todo_list_all_facility")) {
//                        isAllFacility = true;
//                    }
//                    if (functionData.getFunctionName().equals("todo_list_distribute")) {
//                        isDistribute = true;
//                    }
//                }
//            }
//            List<String> facilityIds = null;
//            if (isAllFacility) {
//                if (mineOnly) {
//                    account = user.getAccount();
//                } else {
//                    account = null;
//                }
//            } else {
//                if (!mineOnly) {
//                    if (user.getRoles() != null && user.getRoles().size() > 0) {
//                        roles = new ArrayList<>(user.getRoles().keySet());
//                    }
//                }
//                facilityIds = new ArrayList<>(user.getFacilities().keySet());
//            }
//            List<ToDoDataListResult> toDoDataListResult = this.getToDoDataListFilter(schema_name, work_type, facilityIds, account, roles, newPage, page_size);
//
//            // 子任务工单类型
//            if (work_type.startsWith("inspection")) {
//                work_type = "inspection_subflow";
//            } else if (work_type.startsWith("spotcheck")) {
//                work_type = "spotcheck_subflow";
//            } else if ("bom_count".equals(work_type)) {
//                work_type = "bom_subflow";
//            } else {
//                work_type = null;
//            }
//
//            if (null != work_type) {
//                List<ToDoDataListResult> toDoDataListResultList = this.getToDoDataListFilter(schema_name, work_type, facilityIds, account, roles, newPage, page_size);
//                toDoDataListResult.addAll(toDoDataListResultList);
//            }
//
//            Map<String, Object> info = new HashMap<String, Object>();
//            info.put("isDistribute", isDistribute);
//            info.put("list", toDoDataListResult);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(info);
//
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "index_todo_count", selectOptionService.getLanguageInfo(LangConstant.LOG_TODO_LIST_ERROR) + ex.getMessage(), account);//待办事项列表获取出现了问题
//            return result;
//        }
//    }
//
//    //是否支持单点登录，如果支持，则需禁用修改密码功能
//    @RequestMapping("/need_modify_p")
//    public ResponseModel getNeedModifyPassord(HttpServletRequest request) {
//        //当前登录人
//        String companyId = request.getParameter("companyId");
//        String domain = request.getParameter("domain");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
////        User currentUser = userService.getUserByAccount(schema_name, account);
//        ResponseModel result = new ResponseModel();
//        try {
//            int isNeedPassport = 2; //1:需要；2：不需要；
//            //获取是否需要输入密码
//            if (domain != null && !domain.isEmpty()) {
//                domain = domain.replaceAll("http://", "").toLowerCase();
//                if (domain.endsWith("/") || domain.endsWith("\\")) {
//                    domain = domain.substring(0, domain.length() - 1);
//                }
//            }
//            List<DomainPortUrlData> domainPortUrlData = indexAppService.findAllDomain(domain);
//            if (domainPortUrlData != null && !domainPortUrlData.isEmpty()) {
//                DomainPortUrlData item = domainPortUrlData.get(0);
//                //如果是单点登录，则不需要修改密码
//                if (item.getIs_need_passport()) {
//                    isNeedPassport = 1;
//                }
//            }
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(isNeedPassport);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "index_todo_count", selectOptionService.getLanguageInfo(LangConstant.LOG_TODO_LIST_ERROR) + ex.getMessage(), account);//待办事项列表获取出现了问题
//            return result;
//        }
//    }
//
//    /**
//     * @param schema_name
//     * @param work_type
//     * @param facilityIds
//     * @param account
//     * @param roles
//     * @param newPage
//     * @param page_size
//     * @return
//     * @Description:待办数量统计_过滤
//     */
//    private List<ToDoDataListResult> getToDoDataListFilter(String schema_name, String work_type, List<String> facilityIds, String account, List<String> roles, int newPage, int page_size) {
//        List<ToDoDataListResult> toDoDataListResult = new ArrayList<>();
//        List<CustomTaskEntityImpl> taskList = workflowService.getTasks(schema_name, work_type, facilityIds, null, account, roles, true, null, null, null, page_size, newPage);
//        if (null != taskList && !taskList.isEmpty()) {
//            //获取所有工单类型值
//            List<WorkTypeData> workTypeDataList = maintenanceSettingsService.getWorkTypeListInUse(schema_name);
//            //获取所有工单类型值
//            List<WorkStatus> workStatusList = maintenanceSettingsService.getWorkStatus(schema_name);
//            String statusName = selectOptionService.getLanguageInfo(LangConstant.PROCESSING_A);//处理中
//            String title = "", workTypeName = "", facility_id = "", subWorkCode = "", positionKey = "facilities",
//                    facility_name = "", create_time = "", deadline_time = "", priority_level = "0";
//            int status = 0, workTypeId = 0;
//            for (CustomTaskEntityImpl task : taskList) {
//                ToDoDataListResult todaResult = new ToDoDataListResult();
//                if (null == task.getProcessVariables() && null == task.getTaskLocalVariables()) {
//                    continue;
//                }
//                if (null == task.getProcessVariables()) {
//                    if (null != task.getTaskLocalVariables().get("title_page")) {
//                        title = task.getTaskLocalVariables().get("title_page").toString();
//                    }
//                    if (null != task.getTaskLocalVariables().get("status")) {
//                        status = Integer.parseInt(task.getTaskLocalVariables().get("status").toString());
//                    }
//                    if (null != task.getTaskLocalVariables().get("sub_work_code")) {
//                        subWorkCode = task.getTaskLocalVariables().get("sub_work_code").toString();
//                    }
//                    if (null != task.getTaskLocalVariables().get("businessType")) {
//                        workTypeId = Integer.parseInt(task.getTaskLocalVariables().get("businessType").toString());
//                    }
//                    if (null != task.getTaskLocalVariables().get("create_time")) {
//                        create_time = task.getTaskLocalVariables().get("create_time").toString();
//                    }
//                    if (null != task.getTaskLocalVariables().get("deadline_time")) {
//                        deadline_time = task.getTaskLocalVariables().get("deadline_time").toString();
//                    }
//                    if (null != task.getTaskLocalVariables().get("facility_id")) {
//                        facility_id = task.getTaskLocalVariables().get("facility_id").toString();
//                        if (RegexUtil.isNull(facility_id) && null != task.getTaskLocalVariables().get("position_code")) {
//                            facility_id = task.getTaskLocalVariables().get("position_code").toString();
//                        }
//                    }
//                    if (null != task.getTaskLocalVariables().get("priority_level")) {
//                        priority_level = task.getTaskLocalVariables().get("priority_level").toString();
//                    }
//                } else {
//                    if (null != task.getProcessVariables().get("title_page")) {
//                        title = task.getProcessVariables().get("title_page").toString();
//                    }
//                    if (null != task.getProcessVariables().get("status")) {
//                        status = Integer.parseInt(task.getProcessVariables().get("status").toString());
//                    }
//                    if (null != task.getProcessVariables().get("sub_work_code")) {
//                        subWorkCode = task.getProcessVariables().get("sub_work_code").toString();
//                    }
//                    if (null != task.getProcessVariables().get("businessType")) {
//                        workTypeId = Integer.parseInt(task.getProcessVariables().get("businessType").toString());
//                    }
//                    if (null != task.getProcessVariables().get("create_time")) {
//                        create_time = task.getProcessVariables().get("create_time").toString();
//                    }
//                    if (null != task.getProcessVariables().get("deadline_time")) {
//                        deadline_time = task.getProcessVariables().get("deadline_time").toString();
//                    }
//                    if (null != task.getProcessVariables().get("facility_id")) {
//                        facility_id = task.getProcessVariables().get("facility_id").toString();
//                        if (RegexUtil.isNull(facility_id) && null != task.getProcessVariables().get("position_code")) {
//                            facility_id = task.getProcessVariables().get("position_code").toString();
//                        }
//                    }
//                    if (null != task.getProcessVariables().get("priority_level")) {
//                        priority_level = task.getProcessVariables().get("priority_level").toString();
//                    }
//                }
//                // 手机端待办仅出现工单信息（维修、保养、巡检、点检）、设备、备件
//                if (subWorkCode.startsWith("UC") || subWorkCode.startsWith("TRP") || subWorkCode.startsWith("TR")) {
//                    continue;
//                }
//
//                if (status > 0) {
//                    if (null != workStatusList && !workStatusList.isEmpty()) {
//                        for (WorkStatus workStatus : workStatusList) {
//                            if (workStatus.getId() == status) {
//                                statusName = workStatus.getStatus();
//                                break;
//                            }
//                        }
//                    }
//                }
//                if (workTypeId > 0) {
//                    if (null != workTypeDataList && !workTypeDataList.isEmpty()) {
//                        for (WorkTypeData workTypeData : workTypeDataList) {
//                            if (workTypeData.getId() == workTypeId) {
//                                workTypeName = workTypeData.getType_name();
//                                break;
//                            }
//                        }
//                    }
//                }
//                if (RegexUtil.isNotNull(facility_id)) {
//                    if (facility_id.startsWith("p")) {
//                        positionKey = "asset_position";
//                    }
//                    facility_name = selectOptionService.getOptionNameByCode(schema_name, positionKey, facility_id, "desc");
//                }
//
//                //待办赋值
//                todaResult.setTitle(title);
//                todaResult.setPriority_level(priority_level);
//                todaResult.setFacility_name(facility_name);
//                todaResult.setStatus(statusName);
//                todaResult.setWorkStatus(status);
//                todaResult.setOrdertype(workTypeName);
//                todaResult.setWork_type_id(workTypeId);
//                todaResult.setOrderno(subWorkCode);
//                todaResult.setReceive_account(task.getAssignee());
//                todaResult.setReceive_account_name(task.getAssigneeName());
//                if (null != deadline_time && !deadline_time.isEmpty()) {
//                    todaResult.setDeadlineTime(Timestamp.valueOf(deadline_time));
//                }
//                if (null != create_time && !create_time.isEmpty()) {
//                    todaResult.setCreatetime(Timestamp.valueOf(create_time));
//                } else {
//                    //子任务可能出现没有时间，这时先给一个默认时间当前，需在生成子工单时，给传入截止时间和开始时间
//                    todaResult.setCreatetime(new Timestamp(System.currentTimeMillis()));
//                }
//                //流程键和值
//                todaResult.setFormKey(task.getFormKey());
//                todaResult.setTaskId(task.getId());
//                todaResult.setFlowId(task.getProcessDefinitionId());
//
//                toDoDataListResult.add(todaResult);
//                title = "";
//                status = 0;
//                workTypeId = 0;
//                workTypeName = "";
//                subWorkCode = "";
//                create_time = "";
//                deadline_time = "";
//                facility_name = "";
//                facility_id = "";
//                priority_level = "0";
//                positionKey = "facilities";
//            }
//        }
//        return toDoDataListResult;
//    }
//
//    /**
//     * 获取用户未读消息总数
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_unread_message")
//    public ResponseModel getUserUnreadMessageCount(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            int msgCount = messageCenterService.getMessageListCount(WxUtil.doGetSchemaName(), WxUtil.doGetAccount());
//            return ResponseModel.ok(msgCount);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.OPERATION_E));
//        }
//    }
//
//    /**
//     * 获取用户消息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_message")
//    public ResponseModel getUserMessage(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            JSONObject result = messageCenterService.getMessageList(WxUtil.doGetSchemaName(), request, WxUtil.doGetAccount());
//            return ResponseModel.ok(net.sf.json.JSONObject.fromObject(result.toString()));
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));
//        }
//    }
//
//    /**
//     * 修改用户消息状态
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/have_read_message")
//    public ResponseModel setUserMessageStatus(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            messageCenterService.UpdateReadStatus(WxUtil.doGetSchemaName(), request);
//            return ResponseModel.ok("");
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.OPERATION_E));
//        }
//    }

}
