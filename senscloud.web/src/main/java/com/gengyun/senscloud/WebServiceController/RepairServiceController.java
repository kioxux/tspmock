package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.config.qcloudsmsConfig;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.publicAPIController.APICodeStatus;
//import com.gengyun.senscloud.response.AssetRepairTypeResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.HttpHelper;
//import com.gengyun.senscloud.util.Message;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.*;
//import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.MessageFormat;
//import java.text.SimpleDateFormat;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
///**
// * 微信登录服务接口，设备维修, 准备删除 yzj  2019-08-04
// */
//@Deprecated
//@RequestMapping("/service")
public class RepairServiceController {
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    StockService stockService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    BomService bomService;
//
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    private CompanyService companyService;
//    @Autowired
//    SystemConfigService systemConfigService;
//
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
////    @RequestMapping("/getrepairCode")
////    public ResponseModel getRepair(HttpServletRequest request, HttpServletResponse response) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        try {
////            String assetCode = request.getParameter("assetcode");
////            List<RepairData> collection = repairService.getRepairListByCode(schema_name, assetCode);
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(collection);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            return result;
////        }
////    }
////
////    @RequestMapping("/getmaintainCode")
////    public ResponseModel getMaintain(HttpServletRequest request, HttpServletResponse response) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        try {
////            String assetCode = request.getParameter("assetcode");
////            List<MaintainData> collection = maintainService.getMaintainListByCode(schema_name, assetCode);
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(collection);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            return result;
////        }
////    }
////
////
////    @RequestMapping("/get_repair_type_collection")
////    public ResponseModel getCollection(HttpServletRequest request, HttpServletResponse response) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        try {
////            String deviceCode = request.getParameter("devicecode");
////            String userType = request.getParameter("userType");
////            AssetRepairTypeResult resultData = new AssetRepairTypeResult();
////            //找到设备，获取其所在位置
////            Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, deviceCode);
////            RepairTypeCollection collection = repairService.findAllRepairTypeCollectionList(schema_name, assetModel.getIntsiteid(),userType);
////
////            resultData.setAssetData(assetModel);
////            resultData.setRepairTypeCollection(collection);
////
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(resultData);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            return result;
////        }
////    }
//
//    //上报问题
//    //account: "yaozhijun",asset_status:1,asset_type: "fzsb",deadline_time: "2018-03-23 12:20:29",device_code:"A29f80f5a-7a1e-4bde-bcad-9938821b8ec0",receive_account: "yaozhijun"    fault_img:"",fault_note:"",fault_type:1,help_account:"yaozhijun",occur_time: "2018-03-23 12:20:29",repair_type: 1
//    @RequestMapping("/get_repair_add")
//    @Transactional //支持事务
//    public ResponseModel getAddRepair(HttpServletRequest request, HttpSession session) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是上报问题的人员
//        String account = request.getParameter("account");
//        try {
//            String asset_status = request.getParameter("asset_status");
//            String asset_type = request.getParameter("asset_type");
//            String deadline_time = request.getParameter("deadline_time");
//            String device_code = request.getParameter("device_code");
//            String fault_note = request.getParameter("fault_note");
//            String receive_account = request.getParameter("receive_account");
//            String help_account = request.getParameter("help_account");
//            String occur_time = request.getParameter("occur_time");
//            String from_code = request.getParameter("from_code");
//            //String repair_type = request.getParameter("repair_type");
//
//            if (account == null || account.isEmpty() || asset_type.isEmpty() || device_code.isEmpty() || fault_note == null || fault_note.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("上报人、设备编号、故障描述不能为空。");
//                return result;
//            }
//
//            //处理图片
//            String fault_img = request.getParameter("fault_img");  //已转成id，“，”隔开的多个图片
//            //获取当前设备
//            Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, device_code);
//            if (assetModel == null || assetModel.get_id().isEmpty()) {
//                result.setCode(2);
//                result.setMsg("设备编号有误，在系统中不存在。");
//                return result;
//            }
//
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//
//            //看看一小时内，有没有上报未完成的，如果有，就不要再提交
//            int existCount = repairService.getRepairCountByAssetIdRecently(schema_name, assetModel.get_id(), now);
//            if (existCount > 0) {
//                result.setCode(2);
//                result.setMsg("该设备目前已上报问题。");
//                return result;
//            }
//
//            RepairData model = new RepairData();
//            //生成新订单编号
//            String repairCode = serialNumberService.generateMaxBusinessCode(schema_name, "repair");
//            model.setRepairCode(repairCode);
//            model.setAssetId(assetModel.get_id());
//            model.setAssetType(assetModel.getAsset_type());
//            model.setAssetStatus(Integer.parseInt(asset_status));
//
//            //检查当前时间是否比发生时间早，是的话发生时间不合法
//            if (now.before(Timestamp.valueOf(occur_time))) {
//                model.setOccurtime(now);
//            } else {
//                model.setOccurtime(Timestamp.valueOf(occur_time));
//            }
//            model.setDeadlineTime(Timestamp.valueOf(deadline_time));
//
//            model.setFaultNote(fault_note);
//            model.setFaultImage(fault_img);
//            model.setDistributeAccount("");
//            model.setDistributeTime(null);
//            model.setReceiveAccount(receive_account);
//            //判断状态
//            int status = 20;   //待分配
//            if (receive_account != null && !receive_account.isEmpty()) {
//                status = 40;   //处理中
//                model.setReveiveTime(now);
//                model.setDistributeTime(now);
//            } else {
//                model.setReveiveTime(null);
//            }
//            model.setRepairBeginTime(null);
//            model.setStatus(status);
//            model.setFacilityId((int) assetModel.getIntsiteid());
//            model.setCreate_user_account(account);
//            model.setCreatetime(now);
//            model.setFromCode(from_code);
//
//            //model.setRepairType(Integer.parseInt(repair_type));
//
//            //查找所有的设备状态，按状态设置问题的优先级
//            List<AssetStatus> assetStatusList = maintenanceSettingsService.assetStatus(schema_name);
//            int priorityLevel = 1;
//            if (assetStatusList != null && asset_status != null) {
//                for (AssetStatus item : assetStatusList) {
//                    if (item.getId() == Integer.parseInt(asset_status)) {
//                        priorityLevel = item.getPriorityLevel();
//                        break;
//                    }
//                }
//            }
//            model.setPriorityLevel(priorityLevel);
//
//            //保存上报问题
//            int doCount = repairService.saveRepairInfo(schema_name, model);
//            if (doCount > 0) {
//                //更改设备的状态
//                assetService.setStatus(schema_name, assetModel.get_id(), Integer.parseInt(asset_status));
//
//                //保存支援人员
//                repairService.SaveRepairWorkMan(schema_name, receive_account, help_account, repairCode);
//
//                Company company = companyService.findById(Long.parseLong(companyId));
//                if (company != null && company.getIs_open_sms()) {
//                    // 获取操作人
//                    User sendInfo = userService.getUserByAccount(schema_name, account);
//                    //如无分配人员，则给分配人员发送短信
//                    if (receive_account == null || receive_account.isEmpty()) {
//                        //发送短信给维修分配人
//                        List<User> userList = null;//userService.findUserByFunctionStrKey(schema_name, "repair_distribute", assetModel.getIntsiteid());
//                        if (userList != null && !userList.isEmpty()) {
//                            String mobile = userList.get(0).getMobile();
//                            String receiveAccount = userList.get(0).getAccount();
//                            String content;
//                            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                                String pattern = "{0}上报了设备故障，设备名称：{1}，设备编码：{2}，请尽快分配人员维修。";
//                                content = MessageFormat.format(pattern, sendInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
//                            } else {
//                                String pattern = "{0,number,#}::{1}::{2}::{3}";
//                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_REPAIR_DIS, sendInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
//                            }
//                            Message.beginMsg(schema_name, content, mobile, receiveAccount, "repair", repairCode, account);
//                        }
//                    } else {
//                        //获给维修人员发短信
//                        User receiveInfo = userService.getUserByAccount(schema_name, receive_account);
//                        //发送短信给维修人
//                        String mobile = receiveInfo.getMobile();
//                        String content;
//                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                            String pattern = "{0}请您维修设备，设备名称：{1}，设备编码：{2}，请尽快完成 ";
//                            content = MessageFormat.format(pattern, sendInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
//                        } else {
//                            String pattern = "{0,number,#}::{1}::{2}::{3}";
//                            content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_REPAIR_REPORT, sendInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
//                        }
//                        Message.beginMsg(schema_name, content, mobile, receive_account, "repair", repairCode, account);
//                    }
//                }
//
//                result.setCode(1);
//                result.setMsg("上报维修成功！");
//                result.setContent(repairCode);
//                //记录历史
//                logService.AddLog(schema_name, "repair", repairCode, "上报了设备维修任务", account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg("上报维修失败！");
//                return result;
//            }
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(0);
//            result.setMsg("保存失败，请联系管理员！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_add", "维修上报出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    /**
//     * 从输入流中获取字节数组
//     *
//     * @param inputStream
//     * @return
//     * @throws IOException
//     */
//    public static byte[] readInputStream(InputStream inputStream) throws IOException {
//        byte[] buffer = new byte[1024];
//        int len = 0;
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        while ((len = inputStream.read(buffer)) != -1) {
//            bos.write(buffer, 0, len);
//        }
//        bos.close();
//        return bos.toByteArray();
//    }
//
//    //获取维修单列表
//    @RequestMapping("/get_repair_list")
//    public ResponseModel getRepairList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是上报问题的人员
//        String account = request.getParameter("account");
//        try {
//            int page = Integer.parseInt(request.getParameter("page"));
//            int page_size = Integer.parseInt(request.getParameter("page_size"));
//            int status = Integer.parseInt(request.getParameter("status"));
//
////            //license过期
////            Timestamp now = new Timestamp(System.currentTimeMillis());
////            Timestamp nationday = Timestamp.valueOf("2018-10-15 12:00:00");
////            if (now.after(nationday)) {
////                result.setCode(2);
////                result.setMsg("系统有误，请联系耕耘。");
////                return result;
////            }
//
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("当前登录人有错误。");
//                return result;
//            }
//            if (page < 0 || page_size < 0) {
//                page = 0;
//                page_size = 7;
//            }
//            //根据人员的角色，判断获取值的范围
//            String condition;
//            //获取当前用户
//            User userInfo = userService.getUserByAccount(schema_name, account);
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, userInfo.getId(), "repair");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("repair_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("repair_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            if (isAllFacility) {
//                condition = " and r.status=" + status + " ";
//            } else if (isSelfFacility) {
//                LinkedHashMap<String, IFacility> facilityList = userInfo.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        facilityIds += facilityList.get(key).getId() + ",";
//                    }
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                    condition = " and r.facility_id in (" + facilityIds + ") and r.status=" + status + " ";
//                } else {
//                    condition = " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.receive_account='" + account + "' or r.distribute_account='" + account + "' ) and r.status=" + status + " ";    //没有用户所属位置，则按个人权限查询
//                }
//            } else {
//                condition = " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.receive_account='" + account + "' or r.distribute_account='" + account + "' ) and r.status=" + status + " ";
//            }
//
//            //获取维修列表
//            List<RepairData> list = repairService.getRepairList(schema_name, condition, page_size, page_size * page);
//            result.setCode(1);
//            result.setMsg("获取成功！");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_list", "维修列表获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //获取维修单信息
////    @RequestMapping("/get_repair_info")
////    public ResponseModel getRepairInfo(HttpServletRequest request) {
////        String companyId = request.getParameter("companyId");
////        //生成企业库名
////        String schema_name = schema_name_pre + companyId;
////        ResponseModel result = new ResponseModel();
////        //获取维修工单
////        String repairCode = request.getParameter("repaircode");
////        String account = request.getParameter("account");
////        try {
////            if (repairCode == null || repairCode.isEmpty()) {
////                result.setCode(2);
////                result.setMsg("当前维修单有问题。");
////                return result;
////            }
////
////            //根据人员的角色，判断是否可以 获取维修单
////
////
////            //获取维修列表
////            RepairData info = repairService.getRepariInfo(schema_name, repairCode);
////
////            //如果是第一次进来，并进行维修，则需要更新维修时间
////            if (info != null && info.getRepairBeginTime() == null) {
////                String action = request.getParameter("doaction");
////                if (action != null && action.equals("result")) {
////                    info.setRepairBeginTime(new Timestamp(System.currentTimeMillis()));
////                    repairService.EditRepairBeginTime(schema_name, info);
////                }
////            }
////            result.setCode(1);
////            result.setMsg("获取成功！");
////            result.setContent(info);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(0);
////            result.setMsg("获取失败！");
////            //系统错误日志
////            logService.AddLog(schema_name, "system", "repair_info", "维修信息获取出现了问题：" + ex.getMessage(), account);
////            return result;
////        }
////    }
//
//    //分配人员
//    //account: "yaozhijun",repaircode:"",prioritylevel: "",receive_account: "",help_account:""
//    @RequestMapping("/get_repair_distribute")
//    @Transactional //支持事务
//    public ResponseModel getDistributeRepair(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是上报问题的人员
//        String account = request.getParameter("account");
//        try {
//            String repairCode = request.getParameter("repaircode");
//            String priorityLevel = request.getParameter("prioritylevel");
//            String receive_account = request.getParameter("receive_account");
//            String help_account = request.getParameter("help_account");
//
//            if (account == null || account.isEmpty() || repairCode == null || repairCode.isEmpty() || receive_account == null || receive_account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("维修人员不能为空。");
//                return result;
//            }
//
//            RepairData model = new RepairData();
//            //生成新订单编号
//            model.setRepairCode(repairCode);
//            model.setPriorityLevel(Integer.parseInt(priorityLevel));
//            model.setDistributeAccount(account);
//            model.setDistributeTime(new Timestamp(System.currentTimeMillis()));
//            model.setReceiveAccount(receive_account);
//            model.setReveiveTime(new Timestamp(System.currentTimeMillis()));
//            //判断状态:处理中
//            int status = 40;
//            model.setStatus(status);
//
//            //分配上报问题的分配人员，支援人员
//            int doCount = repairService.EditRepairReceiveMan(schema_name, model);
//            if (doCount > 0) {
//                //先删除支援人员
//                repairService.deleteRepairWorkHour(schema_name, repairCode);
//                //保存支援人员
//                repairService.SaveRepairWorkMan(schema_name, receive_account, help_account, repairCode);
//
//                //获取维修的设备
//                Asset assetInfo = repairService.getAssetCommonDataByRepairCode(schema_name, repairCode);
//
//                // 获取操作人，短信发送人
//                User accountInfo = userService.getUserByAccount(schema_name, receive_account);
//                //如无分配人员，则给分配人员发送短信
//                if (receive_account != null && !receive_account.isEmpty()) {
//                    Company company = companyService.findById(Long.parseLong(companyId));
//                    if (company != null && company.getIs_open_sms()) {
//                        String mobile = accountInfo.getMobile();
//                        String content;
//                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                            String pattern = "您有一个设备维修任务，设备名称：{0}，设备编码：{1}，请尽快完成。";
//                            content = MessageFormat.format(pattern, assetInfo.getStrname(), assetInfo.getStrcode());
//                        } else {
//                            String pattern = "{0,number,#}::{1}::{2}";
//                            content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_REPAIR_DISNOTIFY, assetInfo.getStrname(), assetInfo.getStrcode());
//                        }
//                        Message.beginMsg(schema_name, content, mobile, receive_account, "repair", repairCode, account);
//                    }
//                }
//
//                result.setCode(1);
//                result.setMsg("分配维修人员成功！");
//                result.setContent(repairCode);
//                //记录历史
//                logService.AddLog(schema_name, "repair", repairCode, "分配维护人员：" + accountInfo.getUsername(), account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg("分配维修人员失败！");
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("保存分配维修人员失败，请联系管理员！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_edit", "维修分配维修人员出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //获取维修单，根据设备编号和维修单，如果维修单为空，则根据设备编号获取，否则根据维修单获取，查看当前用户是否有该设备的维修单
//    @RequestMapping("/get_repair_info_by_repair_and_device")
//    public ResponseModel getRepairInfoByDeviceCode(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        String account = request.getParameter("account");
//        try {
//            //获取维修工单，按设备的型号
//            String assetCode = request.getParameter("devicecode");
//            String repairCode = request.getParameter("repaircode");
//            RepairData info = null;
//            if (repairCode != null && !repairCode.isEmpty()) {
//                //获取维修
//                info = repairService.getRepariInfo(schema_name, repairCode);
//            }
//            //根据维修单获取不到维修信息时，再通过设备id，找到最旧的一个维修信息
//            if (info == null || info.getRepairCode() == null || info.getRepairCode().isEmpty()) {
//                Asset assetInfo = repairService.getAssetCommonDataByCode(schema_name, assetCode);
//                if (assetInfo == null && assetInfo.getStrcode().isEmpty()) {
//                    result.setCode(3);
//                    result.setMsg("设备编号不存在。");
//                    return result;
//                }
//                //根据人员的角色，判断是否可以 获取维修单
//
//                //获取维修
//                info = repairService.getRepariInfoByAssetId(schema_name, assetInfo.get_id(), account);
//            }
//
//            if (info != null) {
//                //查询当前用户，可以使用的备件库，以通过备件库，查看其能领用的备件
//                info.setBomStock(stockService.getStockListByUserPermission(schema_name, "", account));
//                //是否需要申领配件
//                SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "need_bom_application");
//                String needBomApp = "0";
//                if (data != null && data.getSettingValue() != null && !data.getSettingValue().isEmpty()) {
//                    needBomApp = data.getSettingValue();
//                }
//                info.setNeed_bom_application(needBomApp);
//
//                result.setCode(1);
//                result.setMsg("获取成功！");
//                result.setContent(info);
//            } else {
//                result.setCode(2);
//                result.setMsg("此设备不需要您维修。");
//                result.setContent(info);
//            }
//
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_info", "维修信息获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //更新维修单的开始时间，微信小程序点击开始维修，执行扫码二维码时
//    @RequestMapping("/get_repair_info_begin")
//    public ResponseModel getRepairInfoBegin(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        String account = request.getParameter("account");
//        try {
//            //获取维修工单，按设备的型号
//            String assetCode = request.getParameter("devicecode");
//            String repairCode = request.getParameter("repaircode");
//            RepairData info = null;
//            if (repairCode != null && !repairCode.isEmpty()) {
//                //获取维修
//                info = repairService.getRepariInfo(schema_name, repairCode);
//            }
//            //如果是第一次进来，并进行维修，则需要更新维修时间
//            if (info != null) {
//                if (!assetCode.equals(info.getAssetCode())) {
//                    result.setCode(2);
//                    result.setMsg("扫码设备编号：" + assetCode + "不是此维修单中的设备！");
//                    result.setContent(info);
//                } else if (info.getRepairBeginTime() == null) {
//                    info.setRepairBeginTime(new Timestamp(System.currentTimeMillis()));
//                    repairService.EditRepairBeginTime(schema_name, info);
//                    result.setCode(1);
//                    result.setMsg("更新开始时间成功。");
//                    result.setContent(info);
//                } else {
//                    //再次记录扫码时间，
//                    result.setCode(1);
//                    result.setMsg("更新开始时间成功。");
//                    result.setContent(info);
//                }
//                //记录历史
//                logService.AddLog(schema_name, "repair", repairCode, "开始进行一次维修", account);
//            } else {
//                result.setCode(2);
//                result.setMsg("此设备维修信息有误。");
//                result.setContent(info);
//            }
//
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取设备编码设备！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_info", "维修信息获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //提交维修结果
//    //'auditnote': auditNote,'account': account,'repaircode': repaircode,'dotype': doType  审核维修
//    @RequestMapping("/get_repair_audit")
//    @Transactional //支持事务
//    public ResponseModel getRepairAudit(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是上报问题的人员
//        String account = request.getParameter("account");
//        try {
//            String repairCode = request.getParameter("repaircode");
//            String auditNote = request.getParameter("auditnote");
//            int dotype = Integer.parseInt(request.getParameter("dotype"));
//            if (account == null || account.isEmpty() || repairCode == null || repairCode.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("拒绝时，审核单据信息有误。");
//                return result;
//            }
//
//            //拒绝,状态改为处理中
//            String passOrReject = "退回";
//            int statue = 40;
//            if (dotype == 1) {
//                passOrReject = "通过";
//                statue = 60;
//            }
//
//            RepairData model = new RepairData();
//            // 填装实体，进行审核
//            model.setRepairCode(repairCode);
//            model.setStatus(statue);
//            model.setAuditAccount(account);
//            model.setAuditTime(new Timestamp(System.currentTimeMillis()));
//            model.setAuditWord(auditNote);
//
//            //对维修的问题进行审核
//            int doCount = repairService.AuditRepairResult(schema_name, model);
//            if (doCount > 0) {
//                result.setCode(1);
//                result.setMsg("执行成功！");
//                result.setContent(repairCode);
//
//                //获取维修的设备
//                Asset assetInfo = repairService.getAssetCommonDataByRepairCode(schema_name, repairCode);
//                RepairData repairData = repairService.getRepariInfo(schema_name, repairCode);
//                // 获取短信接受人
//                User accountInfo = userService.getUserByAccount(schema_name, repairData.getReceiveAccount());
//                Company company = companyService.findById(Long.parseLong(companyId));
//                if (company != null && company.getIs_open_sms()) {
//                    // 获取短信发送人
//                    User sendInfo = userService.getUserByAccount(schema_name, repairData.getCreate_user_account());
//                    if (accountInfo.getAccount() != null && !accountInfo.getAccount().isEmpty()) {
//
//                        //发送短信给维修审核人
//                        String mobile = accountInfo.getMobile();
//                        String content;
//                        if (dotype != 1) {
//                            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                                String pattern = "{0}退回了了设备：{1}，设备编码：{2}的维修任务，请重新维修。";
//                                content = MessageFormat.format(pattern, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                            } else {
//                                String pattern = "{0,number,#}::{1}::{2}::{3}";
//                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_REPAIR_REJECT, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                            }
//                        } else {
//                            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                                String pattern = "{0}确认了设备：{1}，设备编码：{2}的维修任务，请知晓。";
//                                content = MessageFormat.format(pattern, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                            } else {
//                                String pattern = "{0,number,#}::{1}::{2}::{3}";
//                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_REPAIR_CONFIRM, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                            }
//                        }
//                        Message.beginMsg(schema_name, content, mobile, accountInfo.getAccount(), "repair", repairCode, account);
//                    }
//                }
//                if (dotype == 1) {
//                    //更改设备的状态为正常,正常状态为1，写死
//                    assetService.setStatus(schema_name, assetInfo.get_id(), 1);
//
//                    //扣除备件，从备件库中减去使用的备件
//                    if (repairData != null && repairData.getBomList() != null) {
//                        for (RepairBomData data : repairData.getBomList()) {
//                            if (data != null && data.getFacilityId() > 0 && !data.getBomModel().isEmpty() && !data.getBomCode().isEmpty() && !data.getStockCode().isEmpty()) {
//                                bomService.updateBomCount(schema_name, data.getBomModel(), data.getBomCode(), data.getStockCode(), data.getFacilityId(), data.getUseCount());
//                            }
//                        }
//                    }
//                    //给设备记录一条日志
//                    DateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS_2);
//                    logService.AddLog(schema_name, "asset", assetInfo.get_id(), sdf.format(repairData.getOccurtime()) + "，设备进行了一次维修，故障原因：" + repairData.getFaultNote() + "，维修结果：" + repairData.getRepairNote() + "，维修单号:" + repairData.getRepairCode(), repairData.getReceiveAccount());
//                }
//
//                //记录历史
//                logService.AddLog(schema_name, "repair", repairCode, "维修确认【" + passOrReject + "】,意见：" + auditNote, account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg("执行失败！");
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("执行失败，请联系管理员！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_audit", "维修单审核出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //repairnote': note,'beforeimg': beforeimg, 'afterimg': afterimg, 'account': that.data.account, 'repaircode': that.data.repaircode, 'dotype': dotype 提交维修
//    @RequestMapping("/get_repair_result")
//    @Transactional //支持事务
//    public ResponseModel getRepairResult(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是上报问题的人员
//        String account = request.getParameter("account");
//        try {
//            String repairCode = request.getParameter("repaircode");
//            String repairNote = request.getParameter("repairnote");
//            String beforeImage = request.getParameter("beforeimg");
//            String afterImage = request.getParameter("afterimg");
//            Integer faultType = Integer.parseInt(request.getParameter("fault_type"));
//            Integer repairType = Integer.parseInt(request.getParameter("repair_type"));
//            int dotype = Integer.parseInt(request.getParameter("dotype"));
//            String bomListJson = request.getParameter("bomListWord");
//
//            if (account == null || account.isEmpty() || repairCode == null || repairCode.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("数据有误。");
//                return result;
//            }
//
//            RepairData model = new RepairData();
//            //保存,状态改为处理中
//            String logOperation = "保存";
//            int statue = 40;
//            if (dotype == 1) {
//                logOperation = "提交";
//                statue = 50;   //已经提交，待审核
//                model.setFinishedTime(new Timestamp(System.currentTimeMillis()));
//            }
//            if (repairNote == null || repairNote.isEmpty() || repairNote.equals("null")) {
//                repairNote = "";
//            }
//            //填装实体，进行结果提交
//            model.setRepairCode(repairCode);
//            model.setStatus(statue);
//            model.setRepairNote(repairNote);
//            model.setBeforeImage(beforeImage);
//            model.setAfterImage(afterImage);
//            model.setFaultType(faultType);
//            model.setRepairType(repairType);
//
//            //对维修的问题进行结果提交
//            int doCount = repairService.SaveRepairResult(schema_name, model);
//            if (doCount > 0) {
//                //保存备件
//                //使用备件信息[{"facilityId":63,"stockCode":"000006","stockName":"kk","bomName":"123","bomModel":"123","useCount":"3","index":0},{"facilityId":56,"stockCode":"000002","stockName":"we","bomName":"ds2","bomModel":"dsd","useCount":"2","index":1}]
//                if (bomListJson != null) {
//                    JSONArray bomJsonList = JSONArray.fromObject(bomListJson);
//                    if (bomJsonList != null && bomJsonList.size() > 0) {
//                        //先删除之前的
//                        repairService.deleteRepairBom(schema_name, repairCode);
//                        //再增加
//                        for (int i = 0; i < bomJsonList.size(); i++) {
//                            JSONObject bomJson = bomJsonList.getJSONObject(i);
//                            RepairBomData data = new RepairBomData();
//                            data.setRepairCode(repairCode);
//                            data.setFacilityId(Integer.parseInt(bomJson.get("facilityId").toString()));
//                            data.setBomModel(bomJson.get("bomModel").toString());
//                            data.setBomCode(bomJson.get("bomCode").toString());
//                            data.setStockCode(bomJson.get("stockCode").toString());
//                            data.setMaterial_code(bomJson.get("bomMaterialCode").toString());
//                            data.setIs_from_service_supplier(Integer.parseInt(bomJson.get("isFromService").toString()));
//                            data.setUseCount(Integer.parseInt(bomJson.get("useCount").toString()));
//                            data.setFetchMan(account);
//                            data.setHuojia("");
//
//                            repairService.insertRepairBom(schema_name, data);
//                        }
//                    }
//                }
//
//                result.setCode(1);
//                result.setMsg(logOperation + "执行成功！");
//                result.setContent(repairCode);
//                if (dotype == 1) {
//                    //获取维修的设备
//                    Asset assetInfo = repairService.getAssetCommonDataByRepairCode(schema_name, repairCode);
//                    RepairData repairData = repairService.getRepariInfo(schema_name, repairCode);
//                    Company company = companyService.findById(Long.parseLong(companyId));
//                    if (company != null && company.getIs_open_sms()) {
//                        // 获取短信接受人
//                        User accountInfo = userService.getUserByAccount(schema_name, repairData.getCreate_user_account());
//                        // 获取短信发送人
//                        User sendInfo = userService.getUserByAccount(schema_name, repairData.getReceiveAccount());
//                        // 给上报人员发短信，进行审核
//                        if (accountInfo.getAccount() != null && !accountInfo.getAccount().isEmpty()) {
//
//                            //发送短信给维修审核人
//                            String mobile = accountInfo.getMobile();
//                            String content;
//                            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                                String pattern = "{0}维修好设备：{1}，设备编码：{2}，请尽快确认。";
//                                content = MessageFormat.format(pattern, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                            } else {
//                                String pattern = "{0,number,#}::{1}::{2}::{3}";
//                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_REPAIR_ACCEPT, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                            }
//                            Message.beginMsg(schema_name, content, mobile, accountInfo.getAccount(), "repair", repairCode, account);
//                        }
//                    }
//                }
//                //记录历史
//                logService.AddLog(schema_name, "repair", repairCode, logOperation + "维修结果：" + repairNote, account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg(logOperation + "执行失败！");
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("执行失败，请联系管理员！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_finished", "维修单提交或保存完成出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //进行备件申领，返回是否成功结果:repair_bom_app
//    @RequestMapping("/repair_bom_app")
//    public ResponseModel getBomApp(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String repairCode = request.getParameter("repairCode");
//            String bomAppList = request.getParameter("bom_app_list");
//            String account = request.getParameter("account");
//
//            Company company = companyService.findById(Integer.parseInt(companyId));
//            RepairData data = repairService.getRepariInfo(schema_name, repairCode);
//
//            String jsonApp = "{" +
//                    "\"user_account\": \"" + account + "\"," +
//                    "\"valid_key\": \"valid123456789\"," +
//                    "\"token\": \"token123456789\"," +
//                    "\"bill_code\": \"" + repairCode + "\"," +
//                    "\"client_customer_code\": \"" + company.getClient_customer_code() + "\"," +
//                    "\"use_department\": \"" + data.getFacilityName() + "\"," +
//                    "\"cost_center\": \"" + data.getFacilityName() + "\"," +
//                    "\"applicant\": \"" + account + "\"," +
//                    "\"users\": \"" + account + "\"," +
//                    "\"remarks\": \"" + data.getRemark() + "\"," +
//                    "\"\":" + bomAppList +
//                    "}";
//
//            String doResult = HttpHelper.httpPost(APICodeStatus.JY_APPLAY_BOM, jsonApp);//获取返回结果json串
//            Map resultObject = (Map) com.alibaba.fastjson.JSON.parse(doResult);
//            if (resultObject.size() > 0) {
//                for (Object obj : resultObject.keySet()) {
//                    if (obj.equals("code")) {
//                        result.setCode((Integer) resultObject.get(obj));
//                    } else if (obj.equals("content")) {
//                        result.setContent(resultObject.get(obj));
//                    } else if (obj.equals("msg")) {
//                        result.setMsg(resultObject.get(obj) + "");
//                    }
//                }
//                if (result.getCode() == 1) {
//                    repairService.updateRepairApplyStatus(schema_name, repairCode);
//                }
//            } else {
//                result.setCode(0);
//                result.setMsg("申领失败！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("申领失败！");
//            return result;
//        }
//    }
}
