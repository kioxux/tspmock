package com.gengyun.senscloud.WebServiceController;
//
//import com.fitit100.util.ImageUtil;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.*;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.ImageUtils;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.WxUtil;
//import com.gengyun.senscloud.util.imgMarkUtil;
//import org.apache.commons.io.FileUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.multipart.MultipartHttpServletRequest;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.awt.*;
//import java.io.File;
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.List;
//
//@RestController
///**
// * 微信服务接口，通用，公共方法
// */
//@RequestMapping("/service")
public class UsualServiceController {
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    private FilesService filesService;
//
//    @Autowired
//    BomService bomService;
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    private FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetPositionService assetPositionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    DataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    DataPermissionForPosition dataPermissionForPosition;
//
//    @Autowired
//    UserInfoEditService userInfoEditService;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    @RequestMapping("/get_select_facility_list")
//    public ResponseModel getFacilityListByParentId(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//        try {
//            int facilityId = (null == request.getParameter("facilityId") || request.getParameter("facilityId").isEmpty()) ? 0 : Integer.parseInt(request.getParameter("facilityId"));
//            String condition = "and f.parentid=" + facilityId;
//            int page = Integer.parseInt(request.getParameter("page"));
//            int pageSize = Integer.parseInt(request.getParameter("page_size"));
//
//            //个人权限判断，能看哪一功能项的位置，先暂停
//            if (facilityId > 0) {
//                page = 0;
//                pageSize = 1000;
//            }
//
//            String permissionFacility = dataPermissionForFacility.findFacilityIdsByBusinessForSingalUser(schema_name, user, new String[]{}, "asset_data_management");
//            if (null != permissionFacility && !permissionFacility.isEmpty()) {
//                condition = "and f.parentid=" + facilityId + " and f.id in (" + permissionFacility + ")";
//            }
//            List<Facility> facilitiesList = facilitiesService.getSubFacilitiesList(schema_name, condition, pageSize, pageSize * page);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(facilitiesList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    @RequestMapping("/get_repair_select_asset_open")
//    public ResponseModel getRepairSelectAssetOpen(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String configName = request.getParameter("config_name");
//            SystemConfigData configData = systemConfigService.getSystemConfigData(schema_name, configName);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(configData);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //按库房，获取备件数据
//    @RequestMapping(value = "/get_bom_by_stock")
//    public ResponseModel getBomListByStock(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //当前设备编号
//        String stockCode = request.getParameter("stockcode");
//        try {
//            if (stockCode == null || stockCode.equals("null") || stockCode.isEmpty() || account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ERROR_REQUEST));//当前请求有错误
//                return result;
//            }
//
//            // 获取保养信息，以及保养项
//            List<BomData> traget = bomService.getBomListByStock(schema_name, stockCode);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(traget);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//
//    //上传附件,专供微信使用，验证方式改变
//    @RequestMapping(value = "/common_files_upload")
//    public ResponseModel getSingleFilesUpdate(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
//            List<MultipartFile> multipartFile = multipartRequest.getFiles("add_files");
//            String companyId = request.getParameter("companyId");
//            String account = request.getParameter("account");
//            //生成企业库名
//            String schema_name = schema_name_pre + companyId;
//            if (null != multipartFile && !multipartFile.isEmpty()) {
//                MultipartFile file = multipartFile.get(0);
//                String originalFilename = file.getOriginalFilename();
//                Long fileLength = file.getSize() / 1024;//转成k
//                String contentType = file.getContentType();
//                String sep = System.getProperty("file.separator");
//                SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FMT_DATE);
//                String date = format.format(new Date());
//                UUID id = UUID.randomUUID();
//                String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
//                String newFileName = id.toString() + "." + suffix;
//                //定义目录，并判断是否存在
//                String fileDir = file_upload_dir + sep + schema_name + sep + "attach" + sep + date;
//                File dir = new File(fileDir);
//                if (!dir.exists()) {
//                    dir.mkdirs();
//                }
//                //保存路径
//                String filePath = file_upload_dir + sep + schema_name + sep + "attach" + sep + date + sep + newFileName;
//                File desc = new File(filePath);
//                int imgOrDoc = ImageUtils.getIstance().isImage(desc) ? 1 : 2;
//                SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.PIC_ADD_WATERMARK);//从系统配置中读取是否需要添加水印
//                if (imgOrDoc == 1) {
//                    //转成m
//                    BigDecimal fileSize = new BigDecimal((file.getSize() / (1024 * 1024)));
//                    BigDecimal b = new BigDecimal(1.2);
//                    //大于0.5M压缩
//                    if (fileSize.compareTo(b) == 1) {
//                        //jar 只支持jpg压缩输出质量 其他图片png jpeg 无效需要做转换
//                        if (!SystemConfigConstant.COMPRESSION_IMG_TYPE.equals(suffix)) {
//                            suffix = SystemConfigConstant.COMPRESSION_IMG_TYPE;
//                            newFileName = id.toString() + "." + suffix;
//                            filePath = file_upload_dir + sep + schema_name + sep + "attach" + sep + date + sep + newFileName;
//                            desc = new File(filePath);
//                        }
//                        file = ImageUtils.compressMutipartFile(file, file.getName(), originalFilename, contentType, desc, fileSize);
//                    }
//                    if (null != dataList && "1".equals(dataList.getSettingValue())) {
//                        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
//                        file = imgMarkUtil.addWorkMarkToMutipartFile(file, dateFormat.format(new Timestamp(System.currentTimeMillis())), new Font("Arial", Font.BOLD, 20), Color.RED);
//                    }
//                }
//                file.transferTo(desc);
//                //保存附件实体
//                FilesData attachment = new FilesData();
//                attachment.setFileName(newFileName);
//                attachment.setFileOriginalName(originalFilename);
//                attachment.setFileUrl(filePath);
//                attachment.setFileType(imgOrDoc);
//                attachment.setFileSize((double) fileLength);
//                attachment.setFileWidth(0);
//                attachment.setFileHeight(0);
//                String extension = suffix.toLowerCase();
//                if (extension.equals("png") || extension.equals("jpg") || extension.equals("jpeg") || extension.equals("bmp")) {
//                    attachment.setFileWidth(ImageUtil.getImgWidth(desc));
//                    attachment.setFileHeight(ImageUtil.getImgHeight(desc));
//                }
//                attachment.setCreateUserAccount(account);
//                int newFileId = filesService.Add(schema_name, attachment);
//                if (newFileId >= 1) {
//                    return ResponseModel.ok(newFileId);
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPLOAD_FAIL)); // 上传附件失败
//                }
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DOC_EMPTY_ERROR)); // 附件未选择
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPLOAD_FAIL)); // 上传附件失败
//        }
//    }
//
//
//    /**
//     * 获取文件信息
//     *
//     * @param request
//     * @return
//     * @throws IOException
//     */
//    @RequestMapping("/get_files")
//    public ResponseModel getFiles(HttpServletRequest request) throws IOException {
//        try {
//            String companyId = request.getParameter("companyId");
//            String schema_name = schema_name_pre + companyId;
//            String files_id = request.getParameter("id");
//            List<FilesData> dataList = filesService.FindAllFiles(schema_name, files_id);
//            return ResponseModel.ok(dataList);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    //图片文件读取，返回流
//    @RequestMapping("/get_file")
//    public ResponseEntity<byte[]> execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        try {
//            String files_id = request.getParameter("id");
//            String companyId = request.getParameter("companyId");
//            //生成企业库名
//            String schema_name = schema_name_pre + companyId;
//            List<FilesData> fileList = filesService.FindAllFiles(schema_name, files_id);
//            String path = "";
//            String fileName = "";
//            if (fileList != null && !fileList.isEmpty()) {
//                path = fileList.get(0).getFileUrl();
//                File targetFile = new File(path);
//                if (targetFile.exists() == false) {
//                    targetFile = new File(path);
//                }
//                HttpHeaders headers = new HttpHeaders();
//                fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
//                headers.setContentDispositionFormData("attachment", fileName);
//                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//                return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(targetFile),
//                        headers, HttpStatus.OK);
//            } else {
//                return null;
//            }
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
////    //图片文件读取，返回流
////    @RequestMapping("/get_system_currency")
////    public ResponseModel getSystemCurrency(HttpServletRequest request, HttpServletResponse response) throws IOException {
////        try {
////            String companyId = request.getParameter("companyId");
////            String schema_name = schema_name_pre + companyId;
////            ResponseModel responseModel = new ResponseModel();
////            String currencyName = "￥";
////            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.CURRENT_CURRENCY);
////            if (data != null && !data.getSettingValue().isEmpty()) {
////                Map<String, Object> currencyData = selectOptionService.getOptionByCode(schema_name, "currency", data.getSettingValue());
////                if (currencyData != null) {
////                    currencyName = currencyData.get("desc").toString();
////                }
////            }
////            responseModel.setCode(1);
////            responseModel.setContent(currencyName);
////            return responseModel;
////        } catch (Exception e) {
////            return null;
////        }
////    }
//
//    //按位置名称，简称，code，id，查询设备位置,yzj 2019-11-08
//    @RequestMapping("/get_common_select_facility_list")
//    public ResponseModel getCommonFacilityList(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//        try {
//            String facility_word = request.getParameter("facility_word");
//            int facilityId = (null == request.getParameter("facility_id") || request.getParameter("facility_id").isEmpty()) ? 0 : Integer.parseInt(request.getParameter("facility_id"));
//            String permissionFacility = dataPermissionForFacility.findFacilityIdsByBusinessForSingalUser(schema_name, user, new String[]{}, "asset_data_management");
//
//            String condition = " and f.org_type<2 and f.parentid in (" + facilityId + ") ";
//            // 如果 facilityId > 0, 查该位置的子位置，如果有更下级位置，带子位置导航图标,且查询时,不按 facility_word　查；
//            // 如果 facilityId 为 0，如果有更下级位置，带子位置导航图标；
//            if (facilityId == 0) {
//                if (null != permissionFacility && !permissionFacility.isEmpty()) {
//                    condition = "and f.parentid=" + facilityId + " and f.id in(" + permissionFacility + ") ";
//                }
//
//                if (null != facility_word && !facility_word.isEmpty()) {
//                    condition = "and f.title like '%" + facility_word + "%' ";
//                }
//            }
//
//            int page = Integer.parseInt(request.getParameter("page"));
//            int pageSize = Integer.parseInt(request.getParameter("page_size"));
//
//            List<Facility> facilitiesList = facilitiesService.getSubFacilitiesList(schema_name, condition, pageSize, pageSize * page);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(facilitiesList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    /**
//     * 小程序获取设备位置  设备列表设备位置条件筛选 暂时先用 all_facility 判断是否有全部位置的权限--zys
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_common_select_position_list")
//    public ResponseModel getCommonPositionList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//        try {
//            String position_word = request.getParameter("position_word");
//            String position_code = (null == request.getParameter("position_code") || request.getParameter("position_code").isEmpty()) ? "0" : request.getParameter("position_code");
//            String permissionFacility = dataPermissionForPosition.findPositionCodesByBusinessForSingleUser(schema_name, user, new String[]{}, "asset_data_management");
//            String condition = " and p.parent_code in ('" + position_code + "') ";
//            if ("0".equals(position_code)) {
//                if (null != permissionFacility && !permissionFacility.isEmpty()) {
//                    condition = "and p.parent_code= '" + position_code + "' and p.position_code in(" + permissionFacility + ") ";
//                }
//                if (null != position_word && !position_word.isEmpty()) {
//                    condition = "and p.position_name like '%" + position_word + "%' ";
//                }
//            }
//            int page = Integer.parseInt(request.getParameter("page"));
//            int pageSize = Integer.parseInt(request.getParameter("page_size"));
//            List<AssetPosition> positionList = assetPositionService.getSubPositionList(schema_name, condition, pageSize, pageSize * page);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(positionList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    /**
//     * 个人中心信息配置
//     *
//     * @return
//     */
//    @RequestMapping("/getSelfCenterInfo")
//    public ResponseModel getSelfCenterInfo() {
//        List<String> result = new ArrayList<String>();
//        // 报修助手
//        SystemConfigData repairHelp = systemConfigService.getSystemConfigData(WxUtil.doGetSchemaName(), SystemConfigConstant.REPAIR_HELP);
//        if (null != repairHelp && "1".equals(repairHelp.getSettingValue())) {
//            result.add("repairHelp");
//        }
//        return ResponseModel.ok(result);
//    }
//
//    /**
//     * 获取用户个人信息
//     *
//     * @return
//     */
//    @RequestMapping("/get_user_info_for_edit")
//    public ResponseModel getUserInfo() {
//        return ResponseModel.ok(userInfoEditService.getUserInfoForEdit());
//    }
//
//    /**
//     * 保存用户个人配置信息
//     *
//     * @return
//     */
//    @RequestMapping("/saveUserConfig")
//    public ResponseModel saveUserConfig() {
//        try {
//            userInfoEditService.saveUserConfig();
//            return ResponseModel.ok("ok");
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL);
//            return ResponseModel.errorMsg(tmpMsg); // 保存失败
//        }
//    }
//
//    /**
//     * 联系方式页面信息获取
//     *
//     * @return
//     */
//    @RequestMapping("/getContactInfo")
//    public ResponseModel getContactInfo() {
//        Map<String, Object> result = new HashMap<String, Object>();
//        SystemConfigData sm = systemConfigService.getSystemConfigData(WxUtil.doGetSchemaName(), SystemConfigConstant.SERVICE_MOBILE);
//        if (null != sm && RegexUtil.isNotNull(sm.getSettingValue())) {
//            result.put("serviceMobile", sm.getSettingValue());
//        }
//        return ResponseModel.ok(result);
//    }
}
