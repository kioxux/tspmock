package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.common.DataPermissionForFacility;
//import com.gengyun.senscloud.common.DataPermissionForPosition;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.WxUtil;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.stream.Collectors;
//
//@RestController
///**
// * 设备管理
// */
//@RequestMapping("/service")
public class AssetServiceController {
//    @Autowired
//    AssetDataServiceV2 assetDataService;
//    @Autowired
//    AssetQueryService assetQueryService;
//    @Autowired
//    AssetInfoService assetInfoService;
//    @Autowired
//    AssetMonitorValueService assetMonitorValueService;
//    @Autowired
//    FilesService filesService;
//    @Autowired
//    DataPermissionForFacility dataPermissionForFacility;
//    @Autowired
//    DataPermissionForPosition dataPermissionForPosition;
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    AssetInventoryService assetInventoryService;
//    @Autowired
//    QrcodeService qrcodeService;
//    @Autowired
//    AssetGuaranteeService assetGuaranteeService;
//    //企业库前缀，移动端过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //按位置、设备，查询子设备
//    @RequestMapping("/get_select_asset_list")
//    public ResponseModel getDeviceBrowserFacilityList(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User currentUser = userService.getUserByAccount(schema_name, account);
//        try {
//            String facilityNO = request.getParameter("facility_no");//设备组织的id
//            String positionCode = request.getParameter("position_code");//设备组织的id
//            String keyword = request.getParameter("asset_code");
//            String orgType = request.getParameter("org_type");
//            String asset_category = request.getParameter("asset_type");
//            String asset_model = request.getParameter("asset_model");
//            int page = Integer.parseInt(request.getParameter("page"));
//            int pageSize = Integer.parseInt(request.getParameter("page_size"));
//            int isGetMonitorAssetOnly = 0;
//            String monitorOnly = request.getParameter("monitor_data");
//            if (null != monitorOnly && !monitorOnly.isEmpty()) {
//                isGetMonitorAssetOnly = Integer.parseInt(monitorOnly);
//            }
//            String[] positions = {};
//            if (positionCode != null && !positionCode.isEmpty()) {
//                positions = positionCode.split(",");
//            }
//            positions = dataPermissionForPosition.findPositionCodesByBusinessForUser(schema_name,currentUser,positions,"asset_data_management");
//            //获取有权限的组织数据，用于like进行比较
//            String[] facilities = {};
//            if (facilityNO != null && !facilityNO.isEmpty()) {
//                facilities = facilityNO.split(",");
//            }
//            //获取组织的根节点数据，用于like进行比较
//            facilities = dataPermissionForFacility.findFacilityNOsByBusinessForSingalUser(schema_name, currentUser, facilities, "asset_data_management");
//            //获取有权限的设备类型数据，用于in进行比较
//            String[] asset_types = {};
//            String[] asset_models = {};
//            if (asset_category != null && !asset_category.isEmpty()&&!"-1".equals(asset_category)) {
//                asset_types = asset_category.split(",");
//            }
//
//            if (asset_model != null && !asset_model.isEmpty()&& !asset_model.equals("")) {
//                asset_models = asset_model.split(",");
//            }
//            String[] status = {};
//            int begin = pageSize * (page - 1);
//
//            // TODO PC端改造处理
//            List<Asset> assetList = null;
////            List<Asset> assetList = assetDataService.searchAssetList(schema_name, currentUser, asset_types, asset_models, (StringUtils.isBlank(orgType)||"-1".equals(orgType))?null:Integer.valueOf(orgType), facilities, positions, status, keyword, false, false, pageSize, begin, "", isGetMonitorAssetOnly);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(assetList);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(assetList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //按位置、设备，查询子设备
//    @RequestMapping("/get_select_asset_list_count")
//    public ResponseModel getDeviceBrowserFacilityListCount(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String keyword = request.getParameter("asset_code");
//        String orgType = request.getParameter("orgType");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        User currentUser = userService.getUserByAccount(schema_name, account);
//        ResponseModel result = new ResponseModel();
//        try {
//            String positionStr = request.getParameter("positions");
//            String[] positions = StringUtils.isNotBlank(positionStr)?positionStr.split(","):null;
//            String facilityId = request.getParameter("facilityId");     //设备组织的id
//            String facilityNO = request.getParameter("facility_no");//设备组织的no
//            //获取有权限的组织数据，用于like进行比较
//            List<String> facilities = new ArrayList<>();
//            if(StringUtils.isNotBlank(facilityNO)) {
//                //获取有权限的组织数据，用于like进行比较
//                facilities.addAll(Arrays.asList(facilityNO.split(",")));
//            }
//            if(StringUtils.isNotBlank(facilityId)){
//                //设备组织的NO，用于like查询
//                Facility facility = facilitiesService.FacilitiesById(schema_name, Integer.parseInt(facilityId));
//                facilities.add(facility.getFacilityNo());
//            }
//            //获取有权限的设备类型数据，用于in进行比较
//            String[] asset_types = {};
//            String[] status = {};
//            int count = assetDataService.searchAssetListNum(schema_name, currentUser, asset_types, null, StringUtils.isBlank(orgType)?null:Integer.valueOf(orgType), facilities.toArray(new String[facilities.size()]), positions, status, keyword, false, false, "");
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(count);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //获取设备的详情
//    @RequestMapping("/get_device_info")
//    public ResponseModel getDeviceInfo(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String assetCode = request.getParameter("assetcode");
//            assetCode = qrcodeService.dosageForCode(schema_name, assetCode);
//            Asset collection = assetDataService.getAssetByCode(schema_name, assetCode);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(collection);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//
//    //获取设备的历史监测详情
//    @RequestMapping("/get_device_monitor_his_date")
//    public ResponseModel getDeviceMonitorHisDataByDate(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String assetCode = request.getParameter("assetcode");
//            Integer timeslot = 0;
//            //Integer.parseInt(request.getParameter("timeslot").toString());
//            String monitorname = request.getParameter("monitorname");
//            if (request.getParameter("assetcode") == null || request.getParameter("assetcode").equals("")) {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//                return result;
//            }
//            if (request.getParameter("timeslot") == null || request.getParameter("timeslot").equals("")) {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_TIME_ERROR));//时间段不可为空
//                return result;
//            } else {
//                timeslot = Integer.parseInt(request.getParameter("timeslot").toString());
//            }
//            if (request.getParameter("monitorname") == null || request.getParameter("monitorname").equals("")) {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_TEST_EMPTY));//检测项不可为空
//                return result;
//            }
//            SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FMT_SSS_2);
//            Date date = new Date();//当前日期
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(date);
//            String endtime = formatter.format(cal.getTime());
//            cal.add(Calendar.HOUR_OF_DAY, -timeslot);
//            String begtime = formatter.format(cal.getTime());
//
//            List<AssetMonitorData> collection = assetMonitorValueService.getAssetMonitorValuesByName(schema_name, assetCode, monitorname, begtime, endtime);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(collection);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //获取设备的监测项
//    @RequestMapping("/get_device_monitor_name")
//    public ResponseModel getDeviceMonitorName(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String assetCode = request.getParameter("assetcode");
////            Integer timeslot = 0;
//            //Integer.parseInt(request.getParameter("timeslot").toString());
//            if (request.getParameter("assetcode") == null || request.getParameter("assetcode").equals("")) {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//                return result;
//            }
//            List<AssetMonitorData> collection = assetMonitorValueService.getAssetMonitorName(schema_name, assetCode);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(collection);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //获取设备的详情
//    @RequestMapping("/get_device_info_by_id")
//    public ResponseModel getDeviceInfoById(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            Map<String, Object> pageData = assetDataService.getAssetDetail(request, schema_name);
//            assetDataService.getAssetDescriptionInfo(schema_name, pageData);
//            return ResponseModel.ok(pageData);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//
//    //获取设备的关联组织
//    @RequestMapping("/asset_org_list")
//    public ResponseModel getAssetOrganizationList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            String asset_id = request.getParameter("asset_id");
//            if (RegexUtil.isNull(asset_id)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//            }
//            List<Map<String, Object>> facilitiesObjectList = assetDataService.getAssetFacilitiesList(schema_name, asset_id);
//            if(facilitiesObjectList != null && facilitiesObjectList.size() > 0){
//                //查询厂商客户的联系人列表
//                String ids =facilitiesObjectList.stream().map(n -> String.valueOf(n.get("id"))).collect(Collectors.joining(","));
//                List<OrganizationContact> contacts = facilitiesService.ContactListByOrgIdArray(schema_name, ids);
//                Map<Integer, List<OrganizationContact>> contactMap = new HashMap<>();
//                if(contacts != null && contacts.size() > 0){
//                    for(OrganizationContact contact : contacts){
//                        int orgId = contact.getOrg_id();
//                        if(contactMap.containsKey(orgId)) {
//                            List<OrganizationContact> oc = contactMap.get(orgId);
//                            oc.add(contact);
//                        }else
//                            contactMap.put(orgId, new ArrayList<>(Arrays.asList(contact)));
//                    }
//                }
//                for(Map<String, Object> facility : facilitiesObjectList){
//                    facility.put("contacts", contactMap.get(Integer.valueOf(String.valueOf(facility.get("id")))));
//                }
//            }
//            return ResponseModel.ok(facilitiesObjectList);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//
//    //获取设备的知识库
//    @RequestMapping("/asset_knowledge_list")
//    public ResponseModel getAssetKnowledgeList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            String asset_id = request.getParameter("asset_id");
//            Map<String, Object> asset = assetDataService.findOneAsset(schema_name, asset_id);
//            String files_id = null;
//            if (null != asset && asset.size() > 0) {
//                files_id = (String) asset.get("file_ids");
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//            }
//            if (RegexUtil.isNull(files_id)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DOC_EMPTY));//知识库信息不存在
//            } else {
//                List<FilesData> dataList = filesService.FindAllFiles(schema_name, files_id);
//                return ResponseModel.ok(dataList);
//            }
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    //获取设备的维保费用数据
//    @RequestMapping("/asset_cost")
//    public ResponseModel getAssetFeeList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            String asset_id = request.getParameter("asset_id");
//            if (RegexUtil.isNull(asset_id)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//            }
//            Map<String, Object> result = assetInfoService.getAssetFeeTotalByAssetId(schema_name, asset_id);
//            if (null == result) {
//                return ResponseModel.ok(new HashMap<String, Object>() {{
//                    put("fee_amount_all", 0);
//                    put("list", new ArrayList<Map<String, Object>>());
//                }});
//            }
//            List<Map<String, Object>> feeList = assetInfoService.getAssetFeeListMoreInfoByAssetId(schema_name, asset_id);
//            result.put("list", feeList);
//            return ResponseModel.ok(result);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//
//    //获取设备的维保费用数据
//    @RequestMapping("/asset_bom")
//    public ResponseModel getAssetBomList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            String asset_id = request.getParameter("asset_id");
//            if (RegexUtil.isNull(asset_id)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASSET_NOTHINGNESS));//设备不存在
//            }
//            List<AssetBomData> dataList = assetInfoService.findAllAssetBomList(schema_name, asset_id);
//            return ResponseModel.ok(dataList);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 按条件查询盘点设备
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/assetInventoryList"})
//    public String assetInventoryList(HttpServletRequest request) {
//        try {
//            return assetInventoryService.findInventoryOrgDetailList(request, WxUtil.doGetSchemaName());
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 获取资产保修公司信息
//     *
//     * @return
//     */
//    @RequestMapping("_asset_data_management_queryAssetPeriodInfo")
//    public ResponseModel queryAssetPeriodInfo() {
//        try {
//            Integer result = assetGuaranteeService.findAssetPeriodInfo(WxUtil.doGetSchemaName());
//            return ResponseModel.ok(result);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_GET_RE_ERR);
//            return ResponseModel.error(tmpMsg);// 获取设备保修时出现错误
//        }
//    }
}
