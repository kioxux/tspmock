package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.InspectionListAppResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Message;
//import org.apache.commons.lang.NullArgumentException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
///**
// * 微信登录服务接口，设备巡检, 准备删除 yzj  2019-08-04
// */
//@Deprecated
//@RequestMapping("/service")
public class InspectionServiceController {
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    InspectionService inspectionService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    AssetDutyManService assetDutyManService;
//
//    @Autowired
//    CompanyService companyService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //获取点检信息
//    @RequestMapping("/get_inspection_info")
//    public ResponseModel getInspectionList(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        //获取巡检单号
//        String inspectionCode = request.getParameter("inspectioncode");
//        try {
//            if (inspectionCode == null || inspectionCode.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("当前巡检单号有问题。");
//                return result;
//            }
//            //获取巡检
//            InspectionData info = inspectionService.inspectionDataByNo(schema_name, inspectionCode);
//
//            //获取巡检上报的问题
//            if (info != null && info.getInspection_code() != null && !info.getInspection_code().isEmpty()) {
//                info.setRepairDataList(repairService.getRepairListByFromCode(schema_name, inspectionCode));
//            }
//
//            //如果是第一次进来，并进行保养，则需要更新保养时间
//            if (info != null && info.getBeginTime() == null) {
//                String action = request.getParameter("doaction");
//                if (action != null && action.equals("result")) {
//                    info.setBeginTime(new Timestamp(System.currentTimeMillis()));
//                    inspectionService.EditInspectionBeginTime(schema_name, info);
//                }
//            }
//
//            result.setCode(1);
//            result.setMsg("获取成功！");
//            result.setContent(info);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "inspectioninfo", "巡检信息获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //根据设备编号获取巡检单列表
//    @RequestMapping("/get_inspection_item")
//    public ResponseModel getInspectionByAssetId(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        String areacode = request.getParameter("areaCode");
//        String inspectioncode = request.getParameter("inspectioncode");
//
//        try {
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("当前登录人有错误。");
//                return result;
//            }
//            InspectionData inspectionData = new InspectionData();
//            //获取巡检项
//            List<InspectionItemData> inspectionItemData = inspectionService.inspectionItemData(schema_name);
//            //优先考虑订单是否存在，
//            if (inspectioncode != null && !inspectioncode.equals("null") && !inspectioncode.isEmpty()) {
//                InspectionData inspectionDataOne = inspectionService.inspectionDataByNo(schema_name, inspectioncode);
//                if (inspectionDataOne != null && !inspectionDataOne.getArea_code().equals(""))
//                    inspectionData = inspectionDataOne;
//            }
//            //订单不存在，若是从扫描设备进去，看当前人是否有最久未处理的数据
//            else {
//                InspectionData inspectionDataOne = inspectionService.inspectionData(schema_name, areacode, account);
//                if (inspectionDataOne != null && !inspectionDataOne.getArea_code().equals(""))
//                    inspectionData = inspectionDataOne;
//            }
//            inspectionData.setInspectionItemList(inspectionItemData);
//            //查找巡检点实体
//            inspectionData.setInspectionAreaData(inspectionService.InspectionAreaDatabyarea(schema_name, areacode));
//
//            //获取当前时间,设置开始时间，用于巡检的开始时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            inspectionData.setBeginTime(now);
//
//            result.setCode(1);
//            result.setMsg("获取成功！");
//            result.setContent(inspectionData);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "inspection_item_list", "巡检项列表获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //巡检结果提交
//    @RequestMapping("get_inspection_add")
//    @Transactional //支持事务
//    public ResponseModel getInspectionAdd(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String area_code = request.getParameter("areaCode");
//            String itemList = request.getParameter("itemListWord");
//            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
//            String inspectioncode = request.getParameter("inspectioncode");
//            String beginTime = request.getParameter("begintime");
//            String is_abnormal = request.getParameter("isweigui");
//            String abnormal_files = request.getParameter("fault_img");
//            String assetList = request.getParameter("assetListLast");
//
//            //获取当前设备
//            InspectionAreaData inspectionDataOn = inspectionService.InspectionAreaDatabyarea(schema_name, area_code);
//            if (inspectionDataOn == null || inspectionDataOn.getArea_code().isEmpty()) {
//                result.setCode(2);
//                result.setMsg("巡更点有误，在系统中不存在。");
//                return result;
//            }
//            InspectionData inspectionData = new InspectionData();
//            int resultOption = 0;
//            //订单号不存在，则是新增，生成新订单编号
//            if (inspectioncode == null || inspectioncode.isEmpty()) {
//                inspectioncode = serialNumberService.generateMaxBusinessCode(schema_name, "inspection");
//                inspectionData.setInspection_code(inspectioncode);
//                inspectionData.setArea_code(area_code);
//                inspectionData.setInspection_account(account);
//                inspectionData.setCreate_user_account(account);
//                inspectionData.setStatus(60);
//                inspectionData.setFault_number(faultNumber);
//                inspectionData.setFacility_id(inspectionDataOn.getFacility_id());
//                inspectionData.setBeginTime(Timestamp.valueOf(beginTime));
//                inspectionData.setInspection_time(new Timestamp(System.currentTimeMillis()));
//                inspectionData.setCreatetime(new Timestamp(System.currentTimeMillis()));
//                inspectionData.setInspection_result(itemList);
//                inspectionData.setSchema_name(schema_name);
//                inspectionData.setIs_abnormal(Boolean.valueOf(is_abnormal));
//                inspectionData.setAbnormal_files(abnormal_files);
//                resultOption = inspectionService.insertInspectionData(schema_name, inspectionData);
//            } else {
//                inspectionData.setInspection_code(inspectioncode);
//                inspectionData.setArea_code(area_code);
//                inspectionData.setInspection_account(account);
//                inspectionData.setStatus(60);
//                inspectionData.setFault_number(faultNumber);
//                inspectionData.setBeginTime(Timestamp.valueOf(beginTime));
//                inspectionData.setInspection_time(new Timestamp(System.currentTimeMillis()));
//                inspectionData.setCreatetime(new Timestamp(System.currentTimeMillis()));
//                inspectionData.setInspection_result(itemList);
//                inspectionData.setSchema_name(schema_name);
//                inspectionData.setIs_abnormal(Boolean.valueOf(is_abnormal));
//                inspectionData.setAbnormal_files(abnormal_files);
//                resultOption = inspectionService.updateInspectionData(inspectionData);
//            }
//            if (resultOption > 0) {
//                //assetList
//                if (assetList != null && !assetList.equals("")) {
//                    net.sf.json.JSONArray jsonasset = net.sf.json.JSONArray.fromObject(assetList);
////将检测类型不为2的且未选中的汇总作为上报问题
//                    net.sf.json.JSONArray itemListjson = net.sf.json.JSONArray.fromObject(itemList);
//                    String fault_note = "巡检发现问题，问题项：";
//                    for (int i = 0; i < itemListjson.size(); i++) {
//                        String isCheck = itemListjson.getJSONObject(i).getString("isCheck");
//                        String item_name = itemListjson.getJSONObject(i).getString("item_name");
//                        String resultType = itemListjson.getJSONObject(i).getString("resultType");
//                        //巡检类型不为2且没打勾的算有问题
//                        if (!resultType.equals("2") && isCheck.equals("false")) {
//                            fault_note = fault_note + item_name + ";";
//                        }
//                    }
//                    fault_note += "需维修。";
//
//                    //获取当前时间
//                    Timestamp now = new Timestamp(System.currentTimeMillis());
//                    Calendar calendar = new GregorianCalendar();
//                    calendar.setTime(now);
//                    calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
//                    Date date = calendar.getTime();   //这个时间就是日期往后推一天的结果
//                    Timestamp nousedate = new Timestamp(date.getTime());
//                    //维修人，找当前时间的人
//                    List<User> repairusers = null; //repairuser(schema_name, inspectionDataOn.getFacility_id(), now, now);
//                    int assetInSiteNum = jsonasset.size();      // 该位置需要保养的设备数量
//                    int scheduleInSiteNum = repairusers.size();        // 该位置当天排班人数\
//                    // 每人平均负责保养的设备数量（向下取整）
//                    int avgNum = 1;
//                    int isBigYu = 0; //是否有余
//                    if (repairusers.size() != 0) {
//                        avgNum = assetInSiteNum / scheduleInSiteNum;
//                        isBigYu = assetInSiteNum % scheduleInSiteNum;
//                    }
//                    // 得到指定json key对象的value对象
//                    String groupId = "";
//                    for (int i = 0; i < jsonasset.size(); i++) {
//                        String assetcode = jsonasset.getJSONObject(i).getString("assetcode");
//                        //获取当前设备
//                        Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, assetcode);
//                        if (assetModel == null || assetModel.get_id().isEmpty()) {
//                            throw new NullArgumentException("设备不存在");
//                        }
//
//                        //看看一小时内，有没有上报未完成的，如果有，就不要再提交这个设备的问题
//                        int existCount = repairService.getRepairCountByAssetIdRecently(schema_name, assetModel.get_id(), now);
//                        if (existCount > 0) {
//                            continue;
//                        }
//
//                        if (scheduleInSiteNum > 0) {
//                            User user = new User();
//                            //首先看设备有没有专门的负责人员，有的话先给他
//                            List<User> dutyUserList = assetDutyManService.findDutyUserByAssetId(schema_name, 1, assetModel.get_id(), new Long(assetModel.getIntsiteid()).intValue());
//                            if (dutyUserList != null && dutyUserList.size() > 0) {
//                                int max = dutyUserList.size();
//                                int getInt = (int) (new Random().nextFloat() * max);
//                                user = dutyUserList.get(getInt);
//                            }
//                            if (user == null || user.getAccount() == null || user.getAccount().isEmpty()) {
//                                if (avgNum != 0 && i / avgNum < repairusers.size()) {
//                                    // 能整除的部分平均分配
//                                    user = repairusers.get(i / avgNum);
//                                } else {
//                                    // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
//                                    int last = 0;
//                                    int max = repairusers.size();
//                                    if (isBigYu > 0) {
////                                        last = i % scheduleInSiteNum;
//                                        //随机取
//                                        last = (int) (new Random().nextFloat() * max);
//                                    }
//                                    user = repairusers.get(last);
//                                }
//                            }
//
//                            RepairData model = new RepairData();
//                            //生成新订单编号
//                            String repairCode = serialNumberService.generateMaxBusinessCode(schema_name, "repair");
//                            model.setRepairCode(repairCode);
//                            model.setAssetId(assetModel.get_id());
//                            model.setAssetType(assetModel.getAsset_type());
//                            model.setAssetStatus(2);
//                            model.setDeadlineTime(nousedate);
//                            model.setOccurtime(now);
//                            model.setFaultNote(fault_note);
//                            model.setDistributeAccount("");
//                            model.setDistributeTime(null);
//                            model.setReceiveAccount(user.getAccount());
//                            //判断状态
//                            int status = 40;   //待分配
//                            model.setReveiveTime(now);
//                            model.setDistributeTime(now);
//                            model.setRepairBeginTime(null);
//                            model.setStatus(status);
//                            model.setFacilityId((int) assetModel.getIntsiteid());
//                            model.setCreate_user_account(account);
//                            model.setCreatetime(now);
//                            model.setFromCode(inspectioncode);
//                            model.setPriorityLevel(2);
//                            //保存上报问题
//                            int doCount = repairService.saveRepairInfo(schema_name, model);
//                            int a = 0;
//                        }
//                    }
//                }
//                result.setCode(1);
//                result.setMsg("巡检保存成功！");
//                result.setContent(inspectioncode);
//                //记录历史
//                logService.AddLog(schema_name, "inspection", inspectioncode, "巡检结果提交", account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg("巡检保存失败！");
//                return result;
//            }
//
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(0);
//            result.setMsg("保存失败，请联系管理员！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "inspection_do", "巡检结果保存出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //根据设备编号获取巡检单列表
//    @RequestMapping("/get_inspection_list")
//    public ResponseModel getInspectionListData(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("当前登录人有错误。");
//                return result;
//            }
//            int page = Integer.parseInt(request.getParameter("page"));
//            int page_size = Integer.parseInt(request.getParameter("page_size"));
//
//            if (page < 0 || page_size < 0) {
//                page = 0;
//                page_size = 7;
//            }
//
//            //按用户权限，查看其获取巡检的条件
//            String condition = getInspectionCodition(request);
//
//            //获取巡检项
//            List<InspectionListAppResult> inspectionListAppData = inspectionService.inspectionListAppResult(schema_name, condition, page_size, page_size * page);
//            if (inspectionListAppData != null && inspectionListAppData.size() > 0) {
//                for (InspectionListAppResult listAppResult : inspectionListAppData) {
//                    if (listAppResult != null && listAppResult.getInsdate() != null) {
//                        List<InspectionData> inspectionData = inspectionService.InspectionDataByDate(schema_name, condition, listAppResult.getInsdate());
//                        if (inspectionData != null && inspectionData.size() > 0)
//                            listAppResult.setStatusName("未提交");
//                        else
//                            listAppResult.setStatusName("已完成");
//                        List<InspectionData> InspectionDataList = inspectionService.InspectionDataList(schema_name, condition, listAppResult.getInsdate(), page_size, page_size * page);
//                        listAppResult.setInspectionListAppDetailResultList(InspectionDataList);
//                    }
//                }
//            }
//            result.setCode(1);
//            result.setMsg("获取成功！");
//            result.setContent(inspectionListAppData);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            //系统错误日志
//            //logService.AddLog(schema_name, "system", "repair_list", "巡检列表获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //根据设备编号获取巡检单详细列表
//    @RequestMapping("/get_inspection_detaillist")
//    public ResponseModel getInspectionDetailListData(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            if (account == null || account.equals("null") || account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("当前登录人有错误。");
//                return result;
//            }
//            int page = Integer.parseInt(request.getParameter("page"));
//            int page_size = Integer.parseInt(request.getParameter("page_size"));
//            String insdate = request.getParameter("insdate");
//            if (page < 0 || page_size < 0) {
//                page = 0;
//                page_size = 10;
//            }
//
//            //按用户权限，查看其获取巡检的条件
//            String condition = getInspectionCodition(request);
//
//            //获取巡检项
//            List<InspectionData> InspectionDataList = inspectionService.InspectionDataList(schema_name, condition, insdate, page_size, page_size * page);
//
//            result.setCode(1);
//            result.setMsg("获取成功！");
//            result.setContent(InspectionDataList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            //系统错误日志
//            //logService.AddLog(schema_name, "system", "repair_list", "巡检列表获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //按当前用户的角色，获取其巡检的查询条件
//    private String getInspectionCodition(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//
//        String account = request.getParameter("account");
//        User user = userService.getUserByAccount(schema_name, account);
//
//        //查看用户权限
//        String condition;
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection");
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("inspection_all_facility")) {
//                    isAllFacility = true;
//                    break;
//                } else if (functionData.getFunctionName().equals("inspection_self_facility")) {
//                    isSelfFacility = true;
//                }
//            }
//        }
//        if (isAllFacility) {
//            condition = " 1=1 ";
//        } else if (isSelfFacility) {
//            LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//            String facilityIds = "";
//            if (facilityList != null && !facilityList.isEmpty()) {
//                for (String key : facilityList.keySet()) {
//                    facilityIds += facilityList.get(key).getId() + ",";
//                }
//            }
//            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                condition = " ins.facility_id in (" + facilityIds + ") ";
//            } else {
//                condition = " ins.inspection_account='" + user.getAccount() + "' ";    //没有用户所属位置，则按个人权限查询
//            }
//        } else {
//            condition = " ins.inspection_account='" + user.getAccount() + "' ";
//        }
//        return condition;
//    }
//
//    //获取巡检人，以进行从巡检任务的重新分配
//    @RequestMapping("/get_inspection_man_collection")
//    public ResponseModel getInspectionUserCollection(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String areaCode = request.getParameter("areacode");
//            //找到设备，获取其所在位置
////            InspectionAreaData areaModel = inspectionService.InspectionAreaDatabyarea(schema_name, areaCode);
//
//            List<User> inspectionUserList = null;//userService.findUserByFunctionStrKey(schema_name, "repair_distribute", assetModel.getIntsiteid());
//            result.setCode(1);
//            result.setMsg("获取成功！");
//            result.setContent(inspectionUserList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("获取失败！");
//            return result;
//        }
//    }
//
//    //获取重新分配巡检单的负责人
//    @RequestMapping("/get_inspection_distribute")
//    public ResponseModel getUpdateInspectionUser(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            String inspectionCode = request.getParameter("inspectioncode");
//            String receive_account = request.getParameter("receive_account");
//            String areaCode = request.getParameter("areacode");
//
//            if (account == null || account.isEmpty() || inspectionCode == null || inspectionCode.isEmpty() || receive_account == null || receive_account.isEmpty()) {
//                result.setCode(2);
//                result.setMsg("巡检人员不能为空。");
//                return result;
//            }
//
//            InspectionData model = new InspectionData();
//            model.setInspection_code(inspectionCode);
//            model.setInspection_account(receive_account);
//
//            //分配巡检人员
//            int doCount = inspectionService.EditInspectionMan(schema_name, model);
//            if (doCount > 0) {
//                //获取巡检的区域点
//                InspectionAreaData areaModel = inspectionService.InspectionAreaDatabyarea(schema_name, areaCode);
//
//                // 获取操作人，短信发送人
//                User accountInfo = userService.getUserByAccount(schema_name, receive_account);
//                //给巡检人员发送短信
//                if (receive_account != null && !receive_account.isEmpty()) {
//                    Company company = companyService.findById(Long.parseLong(companyId));
//                    if (company != null && company.getIs_open_sms()) {
//                        String mobile = accountInfo.getMobile();
//                        String content = " 您有区域：" + areaModel.getArea_name() + "的巡检任务，请尽快完成。 ";
//                        Message.beginMsg(schema_name, content, mobile, receive_account, "inspection", inspectionCode, account);
//                    }
//                }
//
//                result.setCode(1);
//                result.setMsg("分配巡检人员成功！");
//                result.setContent(inspectionCode);
//                //记录历史
//                logService.AddLog(schema_name, "inspection", inspectionCode, "分配巡检人员：" + accountInfo.getUsername(), account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg("分配巡检人员失败！");
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg("保存巡检人员失败，请联系管理员！");
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "inspection_edit", "分配巡检人员出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
////    public List<User> repairuser(String schema, int facility, Timestamp now, Timestamp nowadd) {
////        //找到该位置所有的，能执行保养提交任务的人员
////        //修改为白天在上班的能保养的人员
////        List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////        if (scheduleList.size() == 0) {
////            //再找一级所属位置的，看有没有排班的人
////            List<Facility> facilities = facilitiesService.FacilitiesList(schema);
////            for (Facility facilityxx : facilities) {
////                if (facilityxx.getId() == facility)
////                    facility = facilityxx.getParentId();
////                break;
////            }
////            scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////            if (scheduleList.size() == 0) {
////                //还是没有人，则按角色找
//////                scheduleList = userService.findUserByFunctionKey(schema, "repair_do", facility);
////            }
////        }
////        //找到的人如果没有配置，则不分配保养人员
////        if (scheduleList == null || scheduleList.isEmpty()) {
////            scheduleList = new ArrayList<>();
////        }
////        return scheduleList;
////    }
}
