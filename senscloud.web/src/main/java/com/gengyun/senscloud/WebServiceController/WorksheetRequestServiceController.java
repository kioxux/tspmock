package com.gengyun.senscloud.WebServiceController;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//import java.util.Map;
//
///**
// * 微信登录服务接口，工单列表和查询，供移动端程序使用
// */
//
//@RestController
//@RequestMapping("/service")
public class WorksheetRequestServiceController {
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    StockService stockService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    BomService bomService;
//
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    private CompanyService companyService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    WorkSheetRequestService workSheetRequestService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    /**
//     * 移动端工单请求列表接口
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_work_request_list")
//    public ResponseModel getRequestCollection(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user=userService.getUserByAccount(schema_name,account);
//        request.setAttribute("schema_name",schema_name);
//        request.setAttribute("user",user);
//        try {
//            List<Map<String, Object>> workRequest = workSheetRequestService.findWsqServiceList(request);
//            return  ResponseModel.ok(workRequest);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    @RequestMapping("/get_work_request_list_count")
//    public ResponseModel getWorkRequestListCount(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user=userService.getUserByAccount(schema_name,account);
//        request.setAttribute("schema_name",schema_name);
//        request.setAttribute("user",user);
//        try {
//            int total = workSheetRequestService.findWsqServiceListCount(request);
//            return  ResponseModel.ok(total);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
//
//    /**
//     * 工单请求列模板信息
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_worksheet_request_detail")
//    public ResponseModel getWorksheetRequestDetail(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user=userService.getUserByAccount(schema_name,account);
//        request.setAttribute("schema_name",schema_name);
//        request.setAttribute("user",user);
//        try{
//            Map<String, Object> detail = workSheetRequestService.getWorksheetRequestDetail(request);
//            return  ResponseModel.ok(detail);
//        }catch (Exception ex){
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
////    /**
////     * 根据工单请求id 查询工单详情信息
////     * @param request
////     * @param response
////     * @return
////     */
////    @RequestMapping("/findSingleWsqInfo")
////    public ResponseModel findSingleWorkInfo(HttpServletRequest request, HttpServletResponse response) {
////        String schemaName = authService.getCompany(request).getSchema_name();
////        try {
////            String subWorkCode = request.getParameter("subWorkCode");
////            Map<String, Object> map = workSheetRequestService.queryWorkRequestById(schemaName, subWorkCode);
////            SCTimeZoneUtil.responseMapDataHandler(map, new String[]{"occur_time", "deadline_time", "create_time"});
////            return ResponseModel.ok(map);
////        } catch (Exception ex) {
////            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
////            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
////        }
////    }


}
