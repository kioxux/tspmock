package com.gengyun.senscloud.WebServiceController;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.gengyun.senscloud.util.WxUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Map;
//
///**
// * Created by Administrator on 2018/5/4.
// */
//@RestController
///**
// * 备件管理
// */
//@RequestMapping("/service")
public class BomServiceController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    BomService bomService;
//    @Autowired
//    BomStockListService bomStockListService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    BomInventoryService bomInventoryService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    //获取备件列表
//    @RequestMapping("/getBomList")
//    public String searchList(HttpServletRequest request) {
//        try {
//            int pageNumber = 0, pageSize = 7;
//            try {
//                String strPage = request.getParameter("pageNumber");
//                if (RegexUtil.isNumeric(strPage)) {
//                    pageNumber = Integer.valueOf(strPage);
//                }
//            } catch (Exception ex) {
//                pageNumber = 0;
//            }
//            try {
//                pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            } catch (Exception ex) {
//                pageSize = 15;
//            }
//            //获取当前用户
//            User loginUser = authService.getLoginUser(request);
//            loginUser = commonUtilService.checkUser(WxUtil.doGetSchemaName(), loginUser, request);
//
//            return bomStockListService.findBslList(WxUtil.doGetSchemaName(), request, loginUser, pageNumber, pageSize).toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
////    /**
////     * 获取备件列表（与工单备件保持一致）【已废弃】
////     *
////     * @param request
////     * @return
////     */
////    @RequestMapping({"/getBomListByCondition"})
////    public String getBomListByCondition(HttpServletRequest request) {
////        try {
////            int pageNumber = 0, pageSize = 7;
////            try {
////                String strPage = request.getParameter("pageNumber");
////                if (RegexUtil.isNumeric(strPage)) {
////                    pageNumber = Integer.valueOf(strPage);
////                }
////            } catch (Exception ex) {
////                pageNumber = 0;
////            }
////            try {
////                pageSize = Integer.parseInt(request.getParameter("pageSize"));
////            } catch (Exception ex) {
////                pageSize = 15;
////            }
////            return bomService.getBomListByStockWithPage(WxUtil.doGetSchemaName(), request, pageNumber, pageSize).toString();
////        } catch (Exception ex) {
////            return "{}";
////        }
////    }
//
//    /**
//     * 按条件查询备件
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/bom_search_list"})
//    public String bomSearchList(HttpServletRequest request) {
//        try {
//            return bomService.searchList(request, WxUtil.doGetSchemaName(), false);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 按条件查询备件
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/bom_search_in_stock_list"})
//    public String bomInStockSearchList(HttpServletRequest request) {
//        try {
//            return bomService.searchList(request, WxUtil.doGetSchemaName(), true);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 按条件查询备件（支持按库房查询）
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/bom_searchBomList"})
//    public String searchBomList(HttpServletRequest request) {
//        try {
//            return bomService.searchBomList(request, WxUtil.doGetSchemaName());
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 按条件查询备件（支持按人员查询个人库存）
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/bom_searchReceiverBomList"})
//    public String searchReceiverBomList(HttpServletRequest request) {
//        try {
//            return bomService.getReceiverBomList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 按条件查询盘点备件
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/bomInventoryList"})
//    public String bomInventoryList(HttpServletRequest request) {
//        try {
//            return bomInventoryService.findInventoryStockDetailList(request, WxUtil.doGetSchemaName());
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    /**
//     * 按条件查询设备备件
//     *
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("_bom_assetBomSearch")
//    public ResponseModel getAssetBomList(
//            @RequestParam Map<String, Object> paramMap) {
//        try {
//            return ResponseModel.ok(bomService.getAssetBomList(paramMap));
//        } catch (SenscloudException e) {
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
}
