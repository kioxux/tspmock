package com.gengyun.senscloud.WebServiceController;

////巡检和点检保存方法，可针对设备，也可针对点巡检区域
//
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.MetadataWork;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/service")
public class WorkHandlerCheckServiceController {
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    MetadataWorkService metadataWorkService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    //数据模式 "${schema_name}";
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;
//
//    @Autowired
//    QrcodeService qrcodeService;
//
//    //按工单类型，查看工单的对象是设备、还是巡检区域
//    @RequestMapping("/get_work_relation_type")
//    public ResponseModel getWorkRelationType(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        int relationTypeId = 0;
//        try {
//            String businessId = request.getParameter("businessId");
//            if (businessId == null || businessId.isEmpty()) {
//                relationTypeId = 0;
//            }
//            Map<String, Object> workTypeMap = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_typeis_use = '1'", businessId);
//            if (null != workTypeMap && workTypeMap.size() > 0) {
//                String flowTemplateCode = String.valueOf(workTypeMap.get("relation"));
//                String formKey = workflowService.getStartFormKey(schema_name, flowTemplateCode);
//                MetadataWork workTemplateData = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//                if (null != workTemplateData) {
//                    relationTypeId = workTemplateData.getRelationType();
//                }
//            }
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(relationTypeId);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //扫码获取设备信息，以及设备的点巡检项
//    @RequestMapping("/get_work_check_target_task")
//    public ResponseModel getWorkCheckTargetAndTask(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel resultModel = new ResponseModel();
//        try {
//            String assetCode = request.getParameter("assetCode");
//            assetCode=qrcodeService.dosageForCode(schema_name, assetCode);
//            //找到设备，获取其所在位置
//            Map<String, Object> assetModel = workSheetHandleService.getWxAssetCommonDataByCode(schema_name, assetCode, request);
//            if (null != assetModel && assetModel.size() > 0) {
//                String businessId = request.getParameter("businessId");
//                //按设备类型，获取设备类型对应的任务项
//                Map<String, Object> workTypeMap = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_type_id", businessId);
//                if (null != workTypeMap && workTypeMap.size() > 0) {
//                    Integer workType = (Integer) workTypeMap.get("code");
//                    Integer assetModelId = ((Long) assetModel.get("asset_model_id")).intValue();
//                    List<Map<String, Object>> list = workSheetHandleService.getAssetTaskList(schema_name, assetModelId, workType);
//                    if (null != list && list.size() > 0) {
//                        String flowTemplateCode = String.valueOf(workTypeMap.get("relation"));
//                        String formKey = workflowService.getStartFormKey(schema_name, flowTemplateCode);
//                        MetadataWork workTemplateData = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//                        if (null != workTemplateData) {
//                            String fieldFormCode = "";
//                            //得到模板中定义的值
//                            String bodyProperty = workTemplateData.getBodyProperty();
//                            if (null != bodyProperty && !bodyProperty.isEmpty()) {
//                                //获取任务键的 fieldFormCode
//                                JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
//                                for (int i = 0; i < bodyArray.size(); i++) {
//                                    JSONObject rowJson = (JSONObject) bodyArray.get(i);
//                                    //给任务项进行赋值
//                                    if (rowJson.get("fieldViewType").equals("9")) {
//                                        fieldFormCode = String.valueOf(rowJson.get("fieldFormCode"));
//                                        break;
//                                    }
//                                }
//
//                                for (Map<String, Object> task : list) {
//                                    task.put("taskRight", "readonly");
//                                    task.put("nowModalKey", fieldFormCode);
//                                }
//
//                                assetModel.put("check_task", list);
//                                resultModel.setContent(assetModel);
//                            }
//                        }
//                    }
//                }
//            }
//
//            resultModel.setCode(1);
//            resultModel.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC));//获取成功
//            return resultModel;
//        } catch (Exception ex) {
//            resultModel.setCode(0);
//            resultModel.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取成功
//            return resultModel;
//        }
//    }
//
//
//    //扫码获取点巡检区信息，以及点巡检项
//    @RequestMapping("/get_work_check_target_task_position")
//    public ResponseModel getWorkCheckTargetAndTaskFromPosition(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel resultModel = new ResponseModel();
//        try {
//            String businessId = request.getParameter("businessId");
//            String positionCode = request.getParameter("assetCode");
//            positionCode=qrcodeService.dosageForCode(schema_name, positionCode);
//            //找到设备，获取其所在位置
//            Map<String, Object> assetPostionModel = workSheetHandleService.getWxPositionCommonDataByCode(schema_name, positionCode, request,businessId);
//            if (null != assetPostionModel && assetPostionModel.size() > 0) {
//                //按设备类型，获取设备类型对应的任务项
//                Map<String, Object> workTypeMap = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_type_id", businessId);
//                if (null != workTypeMap && workTypeMap.size() > 0) {
//                    Integer workType = (Integer) workTypeMap.get("code");
//                    String position_code=(String)assetPostionModel.get("relation_id");
//                    List<Map<String, Object>> list = workSheetHandleService.getAssetPositionTaskList(schema_name, position_code, workType);
//                    if (null != list && list.size() > 0) {
//                        String flowTemplateCode = String.valueOf(workTypeMap.get("relation"));
//                        String formKey = workflowService.getStartFormKey(schema_name, flowTemplateCode);
//                        MetadataWork workTemplateData = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//                        if (null != workTemplateData) {
//                            String fieldFormCode = "";
//                            //得到模板中定义的值
//                            String bodyProperty = workTemplateData.getBodyProperty();
//                            if (null != bodyProperty && !bodyProperty.isEmpty()) {
//                                //获取任务键的 fieldFormCode
//                                JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
//                                for (int i = 0; i < bodyArray.size(); i++) {
//                                    JSONObject rowJson = (JSONObject) bodyArray.get(i);
//                                    //给任务项进行赋值
//                                    if (rowJson.get("fieldViewType").equals("9")) {
//                                        fieldFormCode = String.valueOf(rowJson.get("fieldFormCode"));
//                                        break;
//                                    }
//                                }
//
//                                for (Map<String, Object> task : list) {
//                                    task.put("taskRight", "readonly");
//                                    task.put("nowModalKey", fieldFormCode);
//                                }
//
//                                assetPostionModel.put("check_task", list);
//                                resultModel.setContent(assetPostionModel);
//                            }
//                        }
//                    }
//                }
//            }
//
//            resultModel.setCode(1);
//            resultModel.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC));//获取成功
//            return resultModel;
//        } catch (Exception ex) {
//            resultModel.setCode(0);
//            resultModel.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取成功
//            return resultModel;
//        }
//    }
//
//
//
//    //提交点巡检工单
//    @RequestMapping("/get_work_inspection_and_spot_add")
//    @Transactional //支持事务
//    public ResponseModel getWorkOrderAdd(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        //TODO 异常操作设备-需要做处理 --zys
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel resultModel = new ResponseModel();
//        try {
//            String assetCode = request.getParameter("objectCode");
//            String businessId = request.getParameter("businessId");
//            if ("51".equals(businessId)) {
//                businessId = "3";
//            } else if ("52".equals(businessId)) {
//                businessId = "4";
//            }
//            //检查结果的任务项
//            String taskList = request.getParameter("itemListWord");
//            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
//            String beginTime = request.getParameter("begintime");
//            String abnormal_files = request.getParameter("fault_img");
//
//            String workTemplateCode = "";
//            String bodyProperty = "";
//            String businessName = "";
//            String insLanguageKey="";
//            String spotLanguageKey="";
//            int workTypeId = 0;
//            int relationTypeId = 0;
//            //找到设备，获取其所在位置
//            Map<String, Object> assetModel = new HashMap<String, Object>();
//            //获取工单模板
//            Map<String, Object> workTypeMap = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_type_id", businessId);
//            if (null != workTypeMap && workTypeMap.size() > 0) {
//                String flowTemplateCode = String.valueOf(workTypeMap.get("relation"));
//                workTypeId = Integer.parseInt(workTypeMap.get("code").toString());
//                String formKey = workflowService.getStartFormKey(schema_name, flowTemplateCode);
//                MetadataWork workTemplateData = (MetadataWork) metadataWorkService.queryById(schema_name, formKey).getContent();
//                if (null != workTemplateData) {
//                    workTemplateCode = workTemplateData.getWorkTemplateCode();
//                    relationTypeId = workTemplateData.getRelationType();
//                    if(SensConstant.RELATION_TYPE_ASSET.equals(String.valueOf(relationTypeId))){
//                        assetModel=workSheetHandleService.getWxAssetCommonDataByCode(schema_name, assetCode, request);
//                        insLanguageKey=LangConstant.INS_EQUIP;
//                        spotLanguageKey=LangConstant.POINT_INSE;
//                    }else{
//                        assetModel=workSheetHandleService.getWxPositionCommonDataByCode(schema_name, assetCode, request,businessId);
//                        insLanguageKey=LangConstant.INS_AREA;
//                        spotLanguageKey=LangConstant.PI_AREA;
//                    }
//                    //得到模板中定义的值，对这个模板中的任务项，进行拼接
//                    bodyProperty = workTemplateData.getBodyProperty();
//                    switch (businessId) {
//                        case "2":
//                            businessName = selectOptionService.getLanguageInfo( LangConstant.MAINTAIN_ASSET) + assetModel.get("strname");
//                            break;
//                        case "3":
//                            businessName = selectOptionService.getLanguageInfo( insLanguageKey)  + assetModel.get("strname");
//                            break;
//                        case "4":
//                            businessName = selectOptionService.getLanguageInfo( spotLanguageKey) + assetModel.get("strname");
//                            break;
//                    }
//                }
//            }
//
//            if (null != bodyProperty && !bodyProperty.isEmpty()) {
//                //body重新给值
//                JSONArray bodyArray = JSONArray.fromObject(bodyProperty);
//                for (int i = 0; i < bodyArray.size(); i++) {
//                    JSONObject rowJson = (JSONObject) bodyArray.get(i);
//                    //给任务项进行赋值
//                    if (rowJson.get("fieldViewType").equals("9")) {
//                        JSONObject taskListObject = new JSONObject();
//                        taskListObject.put("key", "WTT000049");
//                        taskListObject.put("data", taskList);
//                        rowJson.put("taskContent", taskListObject);
////                        ((JSONObject) bodyArray.get(i)).put("taskContent", taskListObject);
//                        break;
//                    }
//                }
//                bodyProperty = bodyArray.toString();
//            }
//
//            String work_code = dynamicCommonService.getWorkCode(schema_name);
//            String main_sub_work_code = dynamicCommonService.getSubWorkCode(schema_name);
//            String sub_work_code = dynamicCommonService.getSubWorkCode(schema_name);
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            Integer facilityId = null;
//            String positionCode = null;
//            try {
//                facilityId = Integer.valueOf(String.valueOf(assetModel.get("facility_id")) );
//            } catch (Exception fidExp) {
//                positionCode = (String) assetModel.get("facility_id");
//            }
//            String assetId = (String) assetModel.get("_id");
//
//            //先插入主表
//            int count = workSheetHandleService.insertWorks(schema_name, work_code, workTypeId, workTemplateCode,
//                    "-1", facilityId, positionCode, relationTypeId, assetId, now, now, 60, now,
//                    account, businessName, null, "");
//            if (count > 0) {
//                //概要详情的
//                workSheetHandleService.insertWorkDetail(schema_name, main_sub_work_code, work_code, workTypeId,
//                        workTemplateCode, relationTypeId, assetId, businessName, 60, bodyProperty,
//                        account, now, now, 1, Timestamp.valueOf(beginTime), faultNumber, abnormal_files, now);
//
//                workSheetHandleService.insertWorkAssetOrg(schema_name, main_sub_work_code, assetId); // 工单设备组织处理
//                workSheetHandleService.insertWorkReceiveGroup(schema_name, main_sub_work_code, account); // 工单负责人组处理
//
//                //执行结果
//                workSheetHandleService.insertWorkDetail(schema_name, sub_work_code, work_code, workTypeId,
//                        workTemplateCode, relationTypeId, assetId, businessName, 60, bodyProperty,
//                        account, now, now, -1, Timestamp.valueOf(beginTime), faultNumber, abnormal_files, now);
//                workSheetHandleService.insertWorkAssetOrg(schema_name, sub_work_code, assetId); // 工单设备组织处理
//                workSheetHandleService.insertWorkReceiveGroup(schema_name, sub_work_code, account); // 工单负责人组处理
//            }
//
//            resultModel.setCode(1);
//            resultModel.setMsg(selectOptionService.getLanguageInfo( LangConstant.SUBMIT_SUCESS));//提交成功
////            resultModel.setContent(count);
//            return resultModel;
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            resultModel.setCode(0);
//            resultModel.setMsg(selectOptionService.getLanguageInfo( LangConstant.SUB_FAIL));//提交失败
//            return resultModel;
//        }
//    }
//
//
//    /**
//     * 取工单模板信息
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_form_key_work_check")
//    public ResponseModel getFromKey(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            String businessId = request.getParameter("businessId");
//            if (null == businessId || "".equals(businessId)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_BUSINESS_NO_EMPTY));//业务编号不存在
//            }
//            String workTypeId = selectOptionService.getOptionNameByCode(schema_name, "work_type_by_business_type_id", businessId, "code");
//            String whereString = " and(work_type_id=" + workTypeId + ") ";
//            String formKey = selectOptionService.getOptionNameByCode(schema_name, "work_template_for_detail", whereString, "code");
//            if (null != formKey && !"".equals(formKey)) {
//                JSONObject pageData = workSheetHandleService.getWxWorkHandleInfo(schema_name, account, formKey, "", request);
//                return ResponseModel.ok(pageData);
//            }
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_OBTAIN_TAMPLATE_ERROR));//模板获取失败
//        } catch (SenscloudException e) {
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));//获取失败
//        }
//    }
}
