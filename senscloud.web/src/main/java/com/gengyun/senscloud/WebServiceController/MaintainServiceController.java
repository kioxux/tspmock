/*
package com.gengyun.senscloud.WebServiceController;

import com.gengyun.senscloud.auth.AuthService;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.config.qcloudsmsConfig;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.publicAPIController.APICodeStatus;
import com.gengyun.senscloud.response.FacilitiesResult;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.*;
import com.gengyun.senscloud.util.HttpHelper;
import com.gengyun.senscloud.util.Message;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
*/
/**
 * 微信登录服务接口，设备保养, 准备删除 yzj  2019-08-04
 *//*

@RequestMapping("/service")
public class MaintainServiceController extends WebServiceBaseController {
    @Autowired
    LogsService logService;

    @Autowired
    RepairService repairService;

    @Autowired
    SerialNumberService serialNumberService;

    @Autowired
    MaintainService maintainService;

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @Autowired
    StockService stockService;

    @Autowired
    BomService bomService;

    @Autowired
    WorkScheduleService workScheduleService;

    @Autowired
    FacilitiesService facilityService;

    @Autowired
    private CompanyService companyService;
    @Autowired
    SystemConfigService systemConfigService;

    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";

    //获取保养单列表
    @RequestMapping("/get_maintain_list")
    public ResponseModel getMaintainList(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        //当前登录人，分两种角色，即保养人和审核人，保养人时获取需要保养的设备的列表，审核人时获取保养列表
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        try {
            int status = Integer.parseInt(request.getParameter("status"));
            int page = Integer.parseInt(request.getParameter("page"));
            int page_size = Integer.parseInt(request.getParameter("page_size"));

            if (account == null || account.equals("null") || account.isEmpty()) {
                result.setCode(2);
                result.setMsg("当前登录人有错误。");
                return result;
            }
            if (page < 0 || page_size < 0) {
                page = 0;
                page_size = 7;
            }
            //根据人员的角色，判断获取值的范围
            // 获取保养列表
            List<MaintainData> list;
            String condition;

            //获取当前用户
            User userInfo = userService.getUserByAccount(schema_name, account);
            Boolean isAllFacility = false;
            Boolean isSelfFacility = false;
            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, userInfo.getId(), "maintain");
            if (userFunctionList != null || !userFunctionList.isEmpty()) {
                for (UserFunctionData functionData : userFunctionList) {
                    //如果可以查看全部位置，则不限制
                    if (functionData.getFunctionName().equals("maintain_all_facility")) {
                        isAllFacility = true;
                        break;
                    } else if (functionData.getFunctionName().equals("maintain_self_facility")) {
                        isSelfFacility = true;
                    }
                }
            }

            if (isAllFacility) {
                condition = " and (r.status=" + status + " ) ";
            } else if (isSelfFacility) {
                LinkedHashMap<String, IFacility> facilityList = userInfo.getFacilities();
                String facilityIds = "";
                if (facilityList != null && !facilityList.isEmpty()) {
                    for (String key : facilityList.keySet()) {
                        facilityIds += key + ",";
                    }
                }
                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
                    facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
                    condition = " and (r.facility_id in (" + facilityIds + ") and r.status=" + status + " ) ";
                } else {
                    //没有用户所属位置，则按个人权限查询
                    condition = " and (r.maintain_account='" + account + "' and r.create_user_account='" + account + "' and r.audit_account='" + account + "' and r.status=" + status + " ) ";
                }
            } else {
                condition = " and (r.maintain_account='" + account + "' and r.create_user_account='" + account + "' and r.audit_account='" + account + "' and r.status=" + status + " )";
            }

            //查待保养的列表
            list = maintainService.getMaintainList(schema_name, condition, page_size, page_size * page);

            result.setCode(1);
            result.setMsg("获取成功！");
            result.setContent(list);
            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            //系统错误日志
            logService.AddLog(schema_name, "system", "maintain_list", "保养列表获取出现了问题：" + ex.getMessage(), account);
            return result;
        }
    }

    //获取用户的待保养设备（可能是新的保养，也可能是别人提交的保养（待处理）），如果是新的保养，需要读取保养项列表
    @RequestMapping("/get_maintain_latest")
    public ResponseModel getMaintainInfoAndItemList(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        //当前设备编号
        String deviceCode = request.getParameter("devicecode");
        String maintainCode = request.getParameter("maintaincode");
        try {
            if (deviceCode == null || deviceCode.equals("null") || deviceCode.isEmpty() || account == null || account.equals("null") || account.isEmpty()) {
                result.setCode(2);
                result.setMsg("当前设备信息有错误。");
                return result;
            }

            Asset asset = repairService.getAssetCommonDataByCode(schema_name, deviceCode);
            // 获取保养信息，以及保养项
            MaintainData traget = maintainService.getMaintainInfoAndItemByAssetTypeId(schema_name, account, asset.get_id(), maintainCode);

            //查询当前用户，可以使用的备件库，以通过备件库，查看其能领用的备件
            traget.setBomStock(stockService.getStockListByUserPermission(schema_name, "", account));

            //是否需要申领配件
            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "need_bom_application");
            String needBomApp = "0";
            if (data != null && data.getSettingValue() != null && !data.getSettingValue().isEmpty()) {
                needBomApp = data.getSettingValue();
            }
            traget.setNeed_bom_application(needBomApp);

            result.setCode(1);
            result.setMsg("获取成功！");
            result.setContent(traget);
            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            return result;
        }
    }

    //获取保养确认项目列表
    @RequestMapping("/get_maintain_check_item")
    public ResponseModel getMaintainCheckItemList(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        //当前设备编号
        String deviceCode = request.getParameter("devicecode");
        try {
            if (deviceCode == null || deviceCode.equals("null") || deviceCode.isEmpty()) {
                result.setCode(2);
                result.setMsg("当前设备信息有错误。");
                return result;
            }
            Asset asset = repairService.getAssetCommonDataByCode(schema_name, deviceCode);

            // 获取保养确认项列表
            List<MaintainCheckItemData> list = maintainService.getMaintainCheckItem(schema_name, asset.getAsset_type());
            result.setCode(1);
            result.setMsg("获取成功！");
            result.setContent(list);
            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            return result;
        }
    }

    //保养结果提交
    @RequestMapping("get_maintain_result")
    @Transactional //支持事务
    public ResponseModel getMaintainAdd(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        try {
            String maintainCode = request.getParameter("maintainCode");
            String deviceCode = request.getParameter("deviceCode");
            String beforeImg = request.getParameter("beforeImg");
            String maintainNote = request.getParameter("maintainNote");
            String afterImg = request.getParameter("afterImg");
//            String deadlineTime = request.getParameter("deadlineTime");
            String itemList = request.getParameter("itemListWord");
            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
            String doType = request.getParameter("doType");
            String bomListJson = request.getParameter("bomListWord");
            String isWaiting = request.getParameter("isWaiting");
            boolean isExistMaintainCode = Boolean.parseBoolean(request.getParameter("isExistMaintainCode"));

            if (account == null || account.isEmpty() || deviceCode == null || deviceCode.isEmpty()) {
                result.setCode(2);
                result.setMsg("设备信息有误，如编号不能为空。");
                return result;
            }
//            //license过期
            Timestamp now = new Timestamp(System.currentTimeMillis());
//            Timestamp nationday = Timestamp.valueOf("2018-10-15 12:00:00");
//            if (now.after(nationday)) {
//                result.setCode(2);
//                result.setMsg("系统有误，请联系耕耘。");
//                return result;
//            }

            //获取当前设备
            Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, deviceCode);
            if (assetModel == null || assetModel.get_id().isEmpty()) {
                result.setCode(2);
                result.setMsg("设备编号有误，在系统中不存在。");
                return result;
            }
            MaintainData model = new MaintainData();
            //生成新订单编号
            if (maintainCode == null || maintainCode.isEmpty()) {
                maintainCode = serialNumberService.generateMaxBusinessCode(schema_name, "maintain");
                model.setReveiveTime(now);
                model.setCreatetime(now);
                model.setCreate_user_account(account);
            }
            model.setMaintainCode(maintainCode);
            model.setAssetId(assetModel.get_id());
            model.setAssetType(assetModel.getAsset_type());
            model.setFinishedTime(now);
//            Timestamp tempDeadLine = Timestamp.valueOf(deadlineTime);
//            model.setDeadlineTime(tempDeadLine);
            model.setBeforeImage(beforeImg);
            model.setAfterImage(afterImg);
            model.setFaultNumber(faultNumber);
            //状态
            int status = 80;   //默认待确认
            String logOperation = "提交";
            if (doType == null || doType.equals("save")) {
                status = 70;   //保存时，仍为待处理
                logOperation = "保存";
            }
            model.setMaintainNote(maintainNote);
            model.setStatus(status);
            model.setFacilityId((int) assetModel.getIntsiteid());
            model.setMaintainResult(itemList);
            model.setMaintainAccount(account);

            //保存保养结果
            int doCount;
            //if (isExistMaintainCode) {
            doCount = maintainService.SaveMaintainResult(schema_name, model);
            //} else {
            //新生成一个
            //doCount = maintainService.saveMaintain(schema_name, model);
            //}

            if (doCount > 0) {
                //保存支援人员
                //SaveRepairWorkMan(schema_name, receive_account, help_account, repairCode);

                //保存备件
                //使用备件信息[{"facilityId":63,"stockCode":"000006","stockName":"kk","bomName":"123","bomModel":"123","useCount":"3","index":0},{"facilityId":56,"stockCode":"000002","stockName":"we","bomName":"ds2","bomModel":"dsd","useCount":"2","index":1}]
                if (bomListJson != null) {
                    JSONArray bomJsonList = JSONArray.fromObject(bomListJson);
                    if (bomJsonList != null && bomJsonList.size() > 0) {
                        //先删除之前的
                        maintainService.deleteMaintainBom(schema_name, maintainCode);
                        //再增加
                        for (int i = 0; i < bomJsonList.size(); i++) {
                            JSONObject bomJson = bomJsonList.getJSONObject(i);
                            MaintainBomData data = new MaintainBomData();
                            data.setMaintainCode(maintainCode);
                            data.setFacilityId(Integer.parseInt(bomJson.get("facilityId").toString()));
                            data.setBomModel(bomJson.get("bomModel").toString());
                            data.setBomCode(bomJson.get("bomCode").toString());
                            data.setStockCode(bomJson.get("stockCode").toString());
                            data.setMaterial_code(bomJson.get("bomMaterialCode").toString());
                            data.setIs_from_service_supplier(Integer.parseInt(bomJson.get("isFromService").toString()));
                            data.setUseCount(Integer.parseInt(bomJson.get("useCount").toString()));
                            data.setFetchMan(account);
                            data.setHuojia("");

                            maintainService.insertMaintainBom(schema_name, data);
                        }
                    }
                }

                if (status == 80) {
                    Company company = companyService.findById(Long.parseLong(companyId));
                    if (company.getIs_open_sms()) {
                        //提交时，进行短信发送，保存时，不发送短信
                        // 获取操作人
                        User accountInfo = userService.getUserByAccount(schema_name, account);
                        //发送短信给保养确认人
                        List<User> userList = userService.findUserByFunctionKey(schema_name, "maintain_audit", (int) assetModel.getIntsiteid());
                        if (userList != null && !userList.isEmpty()) {
                            String mobile = userList.get(0).getMobile();
                            String receiveAccount = userList.get(0).getAccount();
                            String content;
                            if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
                                String pattern = "{0}提交了设备：{1}，设备编码：{2}的保养结果，请尽快确认。";
                                content = MessageFormat.format(pattern, accountInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
                            } else {
                                String pattern = "{0,number,#}::{1}::{2}::{3}";
                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_CONFIRM, accountInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
                            }
                            Message.beginMsg(schema_name, content, mobile, receiveAccount, "maintain", maintainCode, account);
                        }
                    }
                }

                result.setCode(1);
                result.setMsg("保养" + logOperation + "成功！");
                result.setContent(maintainCode);
                //记录历史
                logService.AddLog(schema_name, "maintain", maintainCode, logOperation + "保养结果", account);
                return result;
            } else {
                result.setCode(0);
                result.setMsg("保养" + logOperation + selectOptionService.getLanguageInfo( LangConstant.MSG_FAIL));
                return result;
            }
        } catch (Exception ex) {
            // 手动回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            result.setCode(0);
            result.setMsg("执行失败，请联系管理员！");
            //系统错误日志
            logService.AddLog(schema_name, "system", "maintain_add", "提交保存保养出现了问题：" + ex.getMessage(), account);
            return result;
        }
    }

    //更新保养单的开始时间，微信小程序点击开始维修，执行扫码二维码时
    @RequestMapping("/get_maintain_info_begin")
    public ResponseModel getMaintainInfoBegin(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        try {
            //获取保养工单，按设备的型号
            String maintainCode = request.getParameter("maintainCode");
            String assetCode = request.getParameter("devicecode");
            MaintainData info = null;
            if (maintainCode != null && !maintainCode.isEmpty()) {
                //获取保养
                info = maintainService.getMaintainInfo(schema_name, maintainCode);
            }
            //如果是第一次进来，并进行保养，则需要更新保养时间
            if (info != null) {
                if (!assetCode.equals(info.getAssetCode())) {
                    result.setCode(2);
                    result.setMsg("扫码设备：" + assetCode + "不是此保养单中的设备！");
                    result.setContent(info);
                } else if (info.getBeginTime() == null) {
                    info.setBeginTime(new Timestamp(System.currentTimeMillis()));
                    maintainService.EditMaintainBeginTime(schema_name, info);
                    result.setCode(1);
                    result.setMsg("更新开始时间成功。");
                    result.setContent(info);
                } else {
                    //再次记录扫码时间，
                    result.setCode(1);
                    result.setMsg("更新开始时间成功。");
                    result.setContent(info);
                }
                //记录历史
                logService.AddLog(schema_name, "maintain", maintainCode, "开始进行一次保养", account);
            } else {
                result.setCode(2);
                result.setMsg("此设备保养信息有误。");
                result.setContent(info);
            }

            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取设备编码设备！");
            //系统错误日志
            logService.AddLog(schema_name, "system", "maintain_info", "保养信息获取出现了问题：" + ex.getMessage(), account);
            return result;
        }
    }

    //获取保养单信息
    @RequestMapping("/get_maintain_info")
    public ResponseModel getmaintainInfo(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        //获取保养单号
        String maintainCode = request.getParameter("maintaincode");
        try {
            if (maintainCode == null || maintainCode.isEmpty()) {
                result.setCode(2);
                result.setMsg("当前保养单号有问题。");
                return result;
            }

            //根据人员的角色，判断是否可以 获取保养单

            //获取保养
            MaintainData info = maintainService.getMaintainInfo(schema_name, maintainCode);

            //查看小程序当前帐号是否为审核人
            List<User> userList = userService.findUserByFunctionKey(schema_name, "maintain_audit", info.getFacilityId());
            boolean isHaveAuditPermission = false;
            if (userList != null && !userList.isEmpty()) {
                for (User user : userList) {
                    if (user.getAccount().equals(account)) {
                        isHaveAuditPermission = true;
                        break;
                    }
                }
            }

            Asset asset = repairService.getAssetCommonDataByCode(schema_name, info.getAssetCode());

            //如果是审核人进行审核，则加载审核项
            if (info.getStatus() == 80 && isHaveAuditPermission) {
                info.setMaintainCheckItemList(maintainService.getMaintainCheckItem(schema_name, asset.getAsset_type()));
            }

            result.setCode(1);
            result.setMsg("获取成功！");
            result.setContent(info);
            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            //系统错误日志
            logService.AddLog(schema_name, "system", "maintain_info", "保养信息获取出现了问题：" + ex.getMessage(), account);
            return result;
        }
    }

    //保养信息审核确认
    @RequestMapping("/get_maintain_audit")
    @Transactional //支持事务
    public ResponseModel getmaintainAudit(HttpServletRequest request) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        //获取保养单号
        String maintainCode = request.getParameter("maintainCode");
        String doType = request.getParameter("doType");
        String auditWord = request.getParameter("auditNote");
        try {
            if (maintainCode == null || maintainCode.isEmpty()) {
                result.setCode(2);
                result.setMsg("当前保养单号有问题。");
                return result;
            }

            //确认拒绝
            MaintainData info = new MaintainData();
            info.setMaintainCode(maintainCode);
            String passOrReject = "通过";
            if ("back".equals(doType)) {
                //状态改为待处理
                info.setStatus(70);
                passOrReject = "退回";
            } else {
                //状态改为已完成
                info.setStatus(60);
                info.setAuditAccount(account);
                String checkItemList = request.getParameter("checkItemListWord");
                info.setCheckResult(checkItemList);
                info.setAuditTime(new Timestamp(System.currentTimeMillis()));
            }

            info.setAuditWord(auditWord);
            int count = maintainService.AuditMaintainResult(schema_name, info);
            if (count > 0) {
                result.setCode(1);
                result.setMsg("确认成功！");
                result.setContent(info);
                //获取维修的设备
                Asset assetInfo = maintainService.getAssetCommonDataByMaintainCode(schema_name, maintainCode);
                MaintainData maintainData = maintainService.getMaintainInfo(schema_name, maintainCode);
                Company company = companyService.findById(Long.parseLong(companyId));
                if (company != null && company.getIs_open_sms()) {
                    // 获取短信接受人
                    User accountInfo = userService.getUserByAccount(schema_name, maintainData.getMaintainAccount());
                    // 获取短信发送人
                    User sendInfo = userService.getUserByAccount(schema_name, account);
                    if (accountInfo.getAccount() != null && !accountInfo.getAccount().isEmpty()) {
                        //发送短信给维修审核人
                        String mobile = accountInfo.getMobile();
                        String content;
                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
                            if ("back".equals(doType)) {
                                String pattern = "{0}退回了设备：{1}，设备编码：{2}的保养任务，请重新保养。";
                                content = MessageFormat.format(pattern, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
                            } else {
                                String pattern = "{0}确认了设备：{1}，设备编码：{2}的保养任务，请知晓。";
                                content = MessageFormat.format(pattern, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
                            }
                        } else {
                            String pattern = "{0,number,#}::{1}::{2}::{3}";
                            if ("back".equals(doType)) {
                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_REJECT, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
                            } else {
                                content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_ACCEPT, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
                            }
                        }
                        Message.beginMsg(schema_name, content, mobile, accountInfo.getAccount(), "maintain", maintainCode, account);
                    }
                }
                if (!"back".equals(doType)) {
                    //扣除备件，从备件库中减去使用的备件
                    if (maintainData != null && maintainData.getBomList() != null) {
                        for (MaintainBomData data : maintainData.getBomList()) {
                            if (data != null && data.getFacilityId() > 0 && !data.getBomModel().isEmpty() && !data.getBomCode().isEmpty() && !data.getStockCode().isEmpty()) {
                                bomService.updateBomCount(schema_name, data.getBomModel(), data.getBomCode(), data.getStockCode(), data.getFacilityId(), data.getUseCount());
                            }
                        }
                    }
                    //给设备记录一条日志
                    DateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS_2);
                    String doTime = sdf.format(maintainData.getFinishedTime());
                    logService.AddLog(schema_name, "asset", assetInfo.get_id(), doTime + "，设备进行了一次保养：" + maintainData.getMaintainNote() + "，保养单号:" + maintainData.getMaintainCode(), account);

                }

                //保养确认日志
                logService.AddLog(schema_name, "maintain", maintainCode, "保养结果确认【" + passOrReject + "】，意见：" + auditWord, account);
                return result;
            } else {
                result.setCode(0);
                result.setMsg("确认失败！");
                return result;
            }
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            //系统错误日志
            logService.AddLog(schema_name, "system", "maintain_info", "保养信息获取出现了问题：" + ex.getMessage(), account);
            return result;
        }
    }

    //获取保养人，以进行从保养任务的重新分配
    @RequestMapping("/get_maintain_man_collection")
    public ResponseModel getMaintainUserCollection(HttpServletRequest request, HttpServletResponse response) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        try {
            String deviceCode = request.getParameter("devicecode");
            //找到设备，获取其所在位置
            Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, deviceCode);

            //获取当前日期
            Timestamp now = new Timestamp(System.currentTimeMillis());
            List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema_name, assetModel.getIntsiteid(), "maintain_result_submit", now, now);
            if (scheduleList.size() == 0) {
                //再找一级所属位置的，看有没有排班的人
                Number facilityIdNumber = assetModel.getIntsiteid();
                Integer facilityInt = facilityIdNumber.intValue();
                FacilitiesResult facilityData = facilityService.FacilitiesListById(schema_name, facilityInt);
                scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema_name, facilityData.getParentId(), "maintain_result_submit", now, now);
                if (scheduleList.size() == 0) {
                    //按角色权限找
                    scheduleList = userService.findUserByFunctionKey(schema_name, "maintain_result_submit", (int) assetModel.getIntsiteid());
                }
            }
            result.setCode(1);
            result.setMsg("获取成功！");
            result.setContent(scheduleList);
            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            return result;
        }
    }

    //获取重新分配保养单的负责人
    @RequestMapping("/get_maintain_distribute")
    @Transactional //支持事务
    public ResponseModel getUpdateMaintainUser(HttpServletRequest request, HttpServletResponse response) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        try {
            String maintainCode = request.getParameter("maintaincode");
            String receive_account = request.getParameter("receive_account");

            if (account == null || account.isEmpty() || maintainCode == null || maintainCode.isEmpty() || receive_account == null || receive_account.isEmpty()) {
                result.setCode(2);
                result.setMsg("保养人员不能为空。");
                return result;
            }

            MaintainData model = new MaintainData();
            model.setMaintainCode(maintainCode);
            model.setMaintainAccount(receive_account);

            //分配保养人员
            int doCount = maintainService.EditMaintainMan(schema_name, model);
            if (doCount > 0) {

                //获取保养的设备
                Asset assetInfo = maintainService.getAssetCommonDataByMaintainCode(schema_name, maintainCode);

                // 获取操作人，短信发送人
                User accountInfo = userService.getUserByAccount(schema_name, receive_account);
                //给保养人员发送短信
                if (receive_account != null && !receive_account.isEmpty()) {
                    Company company = companyService.findById(Long.parseLong(companyId));
                    if (company.getIs_open_sms()) {
                        String mobile = accountInfo.getMobile();
                        String content;
                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
                            String pattern = "您有设备：{0}，设备编码：{1}的保养任务，请尽快完成。";
                            content = MessageFormat.format(pattern, assetInfo.getStrname(), assetInfo.getStrcode());
                        } else {
                            String pattern = "{0,number,#}::{1}::{2}";
                            content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_REDIS, assetInfo.getStrname(), assetInfo.getStrcode());
                        }
                        Message.beginMsg(schema_name, content, mobile, receive_account, "maintain", maintainCode, account);
                    }
                }

                result.setCode(1);
                result.setMsg("分配保养人员成功！");
                result.setContent(maintainCode);
                //记录历史
                logService.AddLog(schema_name, "maintain", maintainCode, "分配保养人员：" + accountInfo.getUsername(), account);
                return result;
            } else {
                result.setCode(0);
                result.setMsg("分配保养人员失败！");
                return result;
            }
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("保存保养人员失败，请联系管理员！");
            //系统错误日志
            logService.AddLog(schema_name, "system", "maintain_edit", "分配保养人员出现了问题：" + ex.getMessage(), account);
            return result;
        }
    }

    //进行备件申领，返回是否成功结果:maintain_bom_app
    @RequestMapping("/maintain_bom_app")
    public ResponseModel getBomApp(HttpServletRequest request, HttpServletResponse response) {
        String companyId = request.getParameter("companyId");
        String account = request.getParameter("account");
        String token = request.getParameter("token");
        //生成企业库名
        String schema_name = schema_name_pre + companyId;
        ResponseModel result = new ResponseModel();

        //验证当前用户是否合法
        if (!this.validUserForAppService(schema_name, account, token)) {
            result.setCode(0);
            result.setMsg("用户信息有误！");
            return result;
        }
        try {
            String maintainCode = request.getParameter("maintainCode");
            String bomAppList = request.getParameter("bom_app_list");

            Company company = companyService.findById(Integer.parseInt(companyId));
            MaintainData data = maintainService.getMaintainInfo(schema_name, maintainCode);

            String jsonApp = "{" +
                    "\"user_account\": \"" + account + "\"," +
                    "\"valid_key\": \"valid123456789\"," +
                    "\"token\": \"token123456789\"," +
                    "\"bill_code\": \"" + maintainCode + "\"," +
                    "\"client_customer_code\": \"" + company.getClient_customer_code() + "\"," +
                    "\"use_department\": \"" + data.getFacilityName() + "\"," +
                    "\"cost_center\": \"" + data.getFacilityName() + "\"," +
                    "\"applicant\": \"" + account + "\"," +
                    "\"users\": \"" + account + "\"," +
                    "\"remarks\": \"" + data.getRemark() + "\"," +
                    "\"bom_list\":" + bomAppList +
                    "}";

            String doResult = HttpHelper.httpPost(APICodeStatus.JY_APPLAY_BOM, jsonApp);//获取返回结果json串
            Map resultObject = (Map) com.alibaba.fastjson.JSON.parse(doResult);
            if (resultObject.size() > 0) {
                for (Object obj : resultObject.keySet()) {
                    if (obj.equals("code")) {
                        result.setCode((Integer) resultObject.get(obj));
                    } else if (obj.equals("content")) {
                        result.setContent(resultObject.get(obj));
                    } else if (obj.equals("msg")) {
                        result.setMsg(resultObject.get(obj) + "");
                    }
                }
                if (result.getCode() == 1) {
                    maintainService.updateMaintainApplyStaus(schema_name, maintainCode);
                }
            } else {
                result.setCode(0);
                result.setMsg("申领失败！");
            }
            return result;
        } catch (Exception ex) {
            result.setCode(0);
            result.setMsg("获取失败！");
            return result;
        }
    }
}
*/
