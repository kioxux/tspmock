package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.*;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SqlConditionUtil;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 微信登录服务接口，工单列表和查询，供移动端程序使用
// */
//
//@RestController
//@RequestMapping("/service")
public class WorksheetServiceController {
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    StockService stockService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    BomService bomService;
//
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    private CompanyService companyService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    WorkSheetService workSheetService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    @RequestMapping("/get_work_type_list")
//    public ResponseModel getCollection(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            //找到工单类型，获取每个类型
//            String condition = "";
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                        isSelfFacility = true;
//                    }
//
//                }
//            }
//            condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, null, account, user.getFacilities());
//
//            String assetType = request.getParameter("assetType");
//            String[] assetTypes = null;
//            if (assetType != null && !assetType.isEmpty() && !assetType.equals("-1")) {
////                condition += " and ma.id in(" + asset_type + ") ";
//                assetTypes = assetType.split(",");
//            }
//            String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//            if (asset_type_word != null && !asset_type_word.isEmpty()) {
//                if (assetType != null && !assetType.isEmpty() && !assetType.equals("-1")) {
//                    condition += " and ma.id in (" + asset_type_word + ") ";
//                } else {
//                    condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//                }
//            }
//            List<WorkTypeData> dataList = workSheetService.getWorkTypeAndBillCountList(schema_name, condition);
//            if (null != dataList && !dataList.isEmpty()) {
//                result.setContent(dataList);
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            } else {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_DATADATA));//无数据
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    @RequestMapping("/get_worksheet_by_type")
//    public ResponseModel getWorksheetBytype(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String typeId = request.getParameter("typeId");
//        String status = request.getParameter("status");
//        String assetType = request.getParameter("assetType");
//        String asset_id = request.getParameter("assetcode");
//        String business_type_id = request.getParameter("business_type_id");
//        String pageSizeStr = request.getParameter("page_size");
//        String pageNumberStr = request.getParameter("page");
//        String beginTime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("beginTime");
//        String endTime = SCTimeZoneUtil.requestParamHandlerWithMaxSuffix("endTime");
//        String keyWord = request.getParameter("keyWord");
//        String facilities = request.getParameter("facilities");
//        String positions = request.getParameter("positions");
//        int pageSize = 10;
//        int pageNumber = 1;
//        if (pageSizeStr != null && !pageSizeStr.equals("")) {
//            pageSize = Integer.parseInt(pageSizeStr);
//        }
//        if (pageNumberStr != null && !pageNumberStr.equals("")) {
//            pageNumber = Integer.parseInt(pageNumberStr);
//        }
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//        try {
//            //找到工单类型，获取每个类型
//            String condition = "";
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            Boolean hasFeeFlag = false; // 工单费用显示权限
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                        isSelfFacility = true;
//                    }
//
//                }
//            }
//            Map<String, String> dySqlMap = new HashMap<String, String>(); // 按需动态sql
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//            boolean isPositionGuide = true;
//            if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//                isPositionGuide = false;
//            }
//            if (isPositionGuide) {
//                //组织权限
//                condition = SqlConditionUtil.getWorkOrgCondition(isAllFacility, isSelfFacility, "wdaof", condition, facilities, user.getFacilities(), account);
//                //位置权限
//                condition = SqlConditionUtil.getWorkPositionCondition(isAllFacility, isSelfFacility, "w", condition, positions, user.getAssetPositions(), account);
//                //没有权限只查看自己创建，或自己做的工单
//                if ((!isAllFacility && !isSelfFacility) || (!isAllFacility && isSelfFacility && "".equals(condition))) {
//                    condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + "')";
//                }
//            } else {
//                condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//            }
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(w.work_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%" + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or w2.receive_account like '%" + keyWord + "%' )";
//            }
//            if (typeId != null && !"".equals(typeId) && !"-1".equals(typeId)) {
//                condition += " and (w.work_type_id ='" + typeId + "' ) ";
//            }
//            if (status != null && !"".equals(status) && !"-1".equals(status)) {
//                condition += " and (w.status ='" + status + "' ) ";
//            }
//            if (StringUtils.isNotBlank(beginTime)) {
//                condition += " and w.occur_time >='" + beginTime + "'";
//            }
//            if (StringUtils.isNotBlank(endTime)) {
//                condition += " and w.occur_time <'" + endTime + "'";
//            }
//            if (asset_id != null && !"".equals(asset_id)) {
//                condition += " and (dv.strcode ='" + asset_id + "' ) ";
//            }
//            if (business_type_id != null && !"".equals(business_type_id) && !"-1".equals(business_type_id)) {
//                if(business_type_id.contains(",")){
//                    condition += String.format(" and (t.business_type_id in ('%s') ) ", String.join("','", business_type_id.split(",")));
//                }else{
//                    condition += " and (t.business_type_id ='" + business_type_id + "' ) ";
//                }
//            }
//            String[] assetTypes = null;
//            if (assetType != null && !assetType.isEmpty() && !assetType.equals("-1")) {
////                condition += " and ma.id in(" + asset_type + ") ";
//                assetTypes = assetType.split(",");
//            }
//            String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//            if (asset_type_word != null && !asset_type_word.isEmpty()) {
//                if (assetType != null && !assetType.isEmpty() && !assetType.equals("-1")) {
//                    condition += " and ma.id in (" + asset_type_word + ") ";
//                } else {
//                    condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//                }
//            }
//            // 费用sql
//            if (hasFeeFlag) {
//                dySqlMap.put(SqlConstant.SQL_WLF_SELECT_NAME, SqlConstant.SQL_WLF_SELECT);
//                dySqlMap.put(SqlConstant.SQL_WLF_TABLE_NAME, SqlConstant.SQL_WLF_TABLE.replace("${schema_name}", schema_name));
//                dySqlMap.put(SqlConstant.SQL_WLF_GROUP_COLUMN_NAME, SqlConstant.SQL_WLF_GROUP_COLUMN);
//                condition += SqlConstant.SQL_WLF_CONDITION;
//            }
//            condition += " order by w.occur_time desc,w.work_code desc ";
//            int begin = pageSize * (pageNumber - 1);
//            List<WorkSheet> worklistdata = workSheetService.getWorkSheetList(schema_name, condition, dySqlMap, pageSize, begin);
//            //时区转换处理
//            SCTimeZoneUtil.responseObjectListDataHandler(worklistdata);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(worklistdata);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    @RequestMapping("/get_worksheet_by_type_count")
//    public ResponseModel getWorksheetBytypeCount(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String typeId = request.getParameter("typeId");
//        String status = request.getParameter("status");
//        String assetType = request.getParameter("assetType");
//        String asset_id = request.getParameter("assetcode");
//        String business_type_id = request.getParameter("business_type_id");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        User user = userService.getUserByAccount(schema_name, account);
//        ResponseModel result = new ResponseModel();
//        try {
//            //找到工单类型，获取每个类型
//            String condition = "";
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            Boolean hasFeeFlag = false; // 工单费用显示权限
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                        isSelfFacility = true;
//                    } else if (functionData.getFunctionName().equals("work_fee")) {
//                        hasFeeFlag = true;
//                    }
//                }
//            }
//
//            Map<String, String> dySqlMap = new HashMap<String, String>(); // 按需动态sql
//            condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, null, account, user.getFacilities());
//            if (typeId != null && !"".equals(typeId)) {
//                condition += " and (w.work_type_id ='" + typeId + "' ) ";
//            }
//            if (status != null && !"".equals(status) && !"-1".equals(status)) {
//                condition += " and (w.status ='" + status + "' ) ";
//            }
//            if (asset_id != null && !"".equals(asset_id)) {
//                condition += " and (dv.strcode ='" + asset_id + "' ) ";
//            }
//            if (business_type_id != null && !"".equals(business_type_id) && !"-1".equals(business_type_id)) {
//                condition += " and (t.business_type_id ='" + business_type_id + "' ) ";
//            }
//            String[] assetTypes = null;
//            if (assetType != null && !assetType.isEmpty() && !assetType.equals("-1")) {
////                condition += " and ma.id in(" + asset_type + ") ";
//                assetTypes = assetType.split(",");
//            }
//            String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//            if (asset_type_word != null && !asset_type_word.isEmpty()) {
//                if (assetType != null && !assetType.isEmpty() && !assetType.equals("-1")) {
//                    condition += " and ma.id in (" + asset_type_word + ") ";
//                } else {
//                    condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//                }
//            }
//            // 费用sql
//            if (hasFeeFlag) {
//                dySqlMap.put(SqlConstant.SQL_WLF_SELECT_NAME, SqlConstant.SQL_WLF_SELECT);
//                dySqlMap.put(SqlConstant.SQL_WLF_TABLE_NAME, SqlConstant.SQL_WLF_TABLE.replace("${schema_name}", schema_name));
//                dySqlMap.put(SqlConstant.SQL_WLF_GROUP_COLUMN_NAME, SqlConstant.SQL_WLF_GROUP_COLUMN);
//                condition += SqlConstant.SQL_WLF_CONDITION;
//            }
//            int count = workSheetService.getWorkSheetListCount(schema_name, condition, dySqlMap);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//            result.setContent(count);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    @RequestMapping("/get_asset_type")
//    public ResponseModel getAsset_type(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        User user = userService.getUserByAccount(schema_name, account);
//        ResponseModel result = new ResponseModel();
//        try {
//            List<Map<String, Object>> dataList = selectOptionService.SelectOptionList(schema_name, null, "asset_type_app", "", null);
//            if (null == dataList || dataList.isEmpty()) {
//                result.setContent("");
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            } else {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_AC_SUC));//获取成功
//                result.setContent(dataList);
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));//获取失败
//            return result;
//        }
//    }
//
//    //作废工单
//    @RequestMapping("/cancel_work_sheet")
//    @Transactional
//    public ResponseModel WorkListcancel(HttpServletRequest request, HttpServletResponse response) {
//        String account = request.getParameter("account");
//        String companyId = request.getParameter("companyId");
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        try {
//            String workCode = request.getParameter("workCode");
//            if (StringUtils.isBlank(workCode))
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SYS_PARAM_ERROR));
//
//            return workSheetHandleService.cancelWorkSheetSelf(schema_name, workCode, account);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.OWOPQE_CA));
//        }
//    }
}
