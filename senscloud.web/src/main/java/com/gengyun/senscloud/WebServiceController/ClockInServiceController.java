package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.WxUtil;
//import jdk.nashorn.internal.runtime.regexp.joni.constants.RegexState;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * Created by Administrator on 2018/5/4.
// */
//@RestController
///**
// * 备件管理
// */
//@RequestMapping("/service")
public class ClockInServiceController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    ClockInService clockInService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    @RequestMapping("/clock_in")
//    @Transactional
//    public ResponseModel addClockIn(HttpServletRequest request) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            Timestamp create = new Timestamp(System.currentTimeMillis());
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.TIME_FMT);
//            SimpleDateFormat sdf2 = new SimpleDateFormat(Constants.DATE_FMT);;
//            String sign_in = sdf.format(new Date());
//            String  date_search = sdf2.format(new Date());
//            //逻辑判断如果已经存在则更新sign_out
//            List<Map<String,Object>> history = getTodayClockInHistory(schema_name, account, date_search,"history");
//            if(RegexUtil.isNotNull(history)){
//                for(Map<String,Object> his:history){
//                    if(his.get("sign_in")!=null && his.get("sign_out") == null){
//                        //更新
//                        SimpleDateFormat sdf_out = new SimpleDateFormat(Constants.TIME_FMT);
//                        String sign_out = sdf.format(new Date());
//                        clockInService.update(schema_name,sign_out,his.get("id").toString());
//                    }else{
//                        int do_add = clockInService.add(schema_name,account,sign_in,create);
//                    }
//                }
//            }else{
//                int do_add = clockInService.add(schema_name,account,sign_in,create);
//            }
//
//            //返回今天的数据
//            List<Map<String,Object>> todayClockInLog = getTodayClockInHistory(schema_name, account, null,null);
//            return ResponseModel.ok(todayClockInLog);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.SIGN_IN_FAIL_E));//签到失败
//            return result;
//        }
//    }
//
//    /**
//     * 查询某一天打卡记录信息
//     * @param request
//     * @return
//     */
//    @RequestMapping("/clock_in_log_by_date")
//    public ResponseModel getClockInLogByDate(HttpServletRequest request){
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String date_search = request.getParameter("date_search");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            List<Map<String,Object>> todayClockInLog = getTodayClockInHistory(schema_name, account, date_search,null);
//            return ResponseModel.ok(todayClockInLog);
//        }catch (Exception ex){
//            ex.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.SIGN_IN_RECORD_E));//签到记录查询失败
//            return result;
//        }
//    }
//
//    /**
//     * 统计打卡天数
//     * @param request
//     * @return
//     */
//    @RequestMapping("/clock_in_day")
//    public ResponseModel getClockInDay(HttpServletRequest request){
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            int day = clockInService.getClockInDay(schema_name,account);
//            return ResponseModel.ok(day);
//        }catch (Exception ex){
//            ex.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.SIGN_IN_RECORD_E));//签到记录查询失败
//            return result;
//        }
//    }
//
//    /**
//     * 查询过往所有记录
//     * @param request
//     * @return
//     */
//    @RequestMapping("/history_clock_in_day")
//    public ResponseModel historyClockInDay(HttpServletRequest request){
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        try {
//            List<Map<String,Object>> history = clockInService.historyClockInDay(schema_name,account);
//            List<Map<String,Object>> dayHistory = new ArrayList<Map<String,Object>>();
//            if(RegexUtil.isNotNull(history)){
//                for(Map<String,Object> his : history){
//                    Map<String,Object> map = new HashMap<String,Object>();
//                    String day = his.get("create_day").toString();
//                    map.put("id",day);
//                    map.put("style","background: green;  border-radius: 30px;");
//                    dayHistory.add(map);
//                }
//            }
//            return ResponseModel.ok(dayHistory);
//        }catch (Exception ex){
//            ex.printStackTrace();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.SIGN_IN_RECORD_E));//签到记录查询失败
//            return result;
//        }
//    }
//
//    /**
//     * 查询某个日期的打卡记录
//     * @param schema_name
//     * @param date
//     * @return
//     */
//    private List<Map<String,Object>> getTodayClockInHistory(String schema_name ,String account, String date, String limit){
//        String condation  = "";
//        String limitCondation = "";
//        if(RegexUtil.isNull(date)){
//            Timestamp create = new Timestamp(System.currentTimeMillis());
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT);;
//            date = sdf.format(new Date());
//        }
//        condation += "and create_time BETWEEN TIMESTAMP'"+date+" 00:00:00' AND TIMESTAMP'"+date+" 23:59:59' ";
//        if(RegexUtil.isNotNull(account)){
//            condation += "and create_user_account='"+account+"' ";
//        }
//        if(RegexUtil.isNotNull(limit)){
//            limitCondation += " order by create_time desc limit 1";
//        }else {
//            limitCondation += " order by create_time asc ";
//        }
//        List<Map<String,Object>> historyList =  clockInService.getTodayClockInHistory(schema_name, condation, limitCondation);
//        List<Map<String,Object>> historyDealList = new ArrayList<Map<String,Object>>();
//        if(RegexUtil.isNull(limit)){
//            if(RegexUtil.isNotNull(historyList)){
//                int times = 1;
//                for (Map<String,Object> history : historyList){
//
//                    String sign_in =  history.get("sign_in")!=null ? history.get("sign_in").toString():"" ;
//                    String sign_out =  history.get("sign_out")!=null ? history.get("sign_out").toString():"" ;
//                    if(!sign_in.equals("")){
//                        Map<String,Object> map = new HashMap<String,Object>();
//                        map.put("times",times);
//                        map.put("time",sign_in);
//                        map.put("clockInId",history.get("id").toString()+times);
//                        map.put("sign", selectOptionService.getLanguageInfo( LangConstant.CLOCK_IN));
//                        historyDealList.add(map);
//                        times +=1;
//                    }
//
//                    if(!sign_out.equals("")){
//                        String sign_info = "";
//                        sign_info = selectOptionService.getLanguageInfo( LangConstant.SIGN_OUT);
//                        if(RegexUtil.isNull(sign_info)){
//                            sign_info = "签退";
//                        }
//                        Map<String,Object> map = new HashMap<String,Object>();
//                        map.put("times",times);
//                        map.put("time",sign_out);
//                        map.put("clockInId",history.get("id").toString()+times);
//                        map.put("sign", sign_info);//SIGN_OUT
//                        historyDealList.add(map);
//                        times +=1;
//                    }
//                }
//            }
//            return historyDealList;//处理之后的数据
//        }else{
//            return historyList;//直接返回上一天历史数据
//        }
//    }


}
