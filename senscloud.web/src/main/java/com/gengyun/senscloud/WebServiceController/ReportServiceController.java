package com.gengyun.senscloud.WebServiceController;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.DataPermissionAllOrSelf;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.AnalysChartService;
//import com.gengyun.senscloud.service.system.LogsService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
///**
// * 报表服务，yzj 2019-10-30
// */
//@RequestMapping("/service")
public class ReportServiceController {
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AnalysChartService analysChartService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //按工单类型，查询完成的总数和进行中的总数
//    @RequestMapping("/get_work_type_list_total")
//    public ResponseModel getWorkTotal(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String type_id = request.getParameter("typeId");            //工单类型
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, type_id, beginDate, endDate);
//
//            Map<String, Object> dataTotal = analysChartService.getFinishedAndProcessingWorkTotal(schema_name, condition);
//            if (null != dataTotal && !dataTotal.isEmpty()) {
//                result.setContent(dataTotal);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//
//    //按工单类型，查询完成的总数和进行中的总数
//    @RequestMapping("/get_facility_work_type_list_total")
//    public ResponseModel getFacilityWorkTotal(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String type_id = request.getParameter("typeId");            //工单类型
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//            String pageSizeStr = request.getParameter("page_size");
//            String pageNumberStr = request.getParameter("page");
//            int pageSize = 0;
//            int pageNumber = 0;
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//            int begin = pageSize * (pageNumber - 1);
//
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, type_id, beginDate, endDate);
//
//            List<Map<String, Object>> dataList = analysChartService.getFacilityFinishedAndProcessingWorkTotal(schema_name, condition, pageSize, begin);
//            if (null != dataList && !dataList.isEmpty()) {
//                result.setContent(dataList);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//
//    //按工单类型，查询完成的总数和进行中的总数
//    @RequestMapping("/get_facility_work_type_list")
//    public ResponseModel getFacilityWorkList(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String type_id = request.getParameter("typeId");            //工单类型
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//            String pageSizeStr = request.getParameter("page_size");
//            String pageNumberStr = request.getParameter("page");
//            int pageSize = 0;
//            int pageNumber = 0;
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//            int begin = pageSize * (pageNumber - 1);
//
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, type_id, beginDate, endDate);
//
//            List<Map<String, Object>> dataList = analysChartService.getFacilityFinishedAndProcessingWorkListForAsset(schema_name, condition, pageSize, begin);
//            if (null != dataList && !dataList.isEmpty()) {
//                result.setContent(dataList);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//
//    //按日期，统计所有费用
//    @RequestMapping("/get_work_fee_list_total")
//    public ResponseModel getWorkFeeTotal(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, null, beginDate, endDate);
//
//            Map<String, Object> dataTotal = analysChartService.getWorkFeeTotalByWorkType(schema_name, condition);
//            if (null != dataTotal && !dataTotal.isEmpty()) {
//                result.setContent(dataTotal);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//
//    //按日期，统计个各位置的费用
//    @RequestMapping("/get_facility_work_fee_list_total")
//    public ResponseModel getFacilityWorkFeeTotal(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//            String pageSizeStr = request.getParameter("page_size");
//            String pageNumberStr = request.getParameter("page");
//            int pageSize = 0;
//            int pageNumber = 0;
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//            int begin = pageSize * (pageNumber - 1);
//
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, null, beginDate, endDate);
//
//            List<Map<String, Object>> dataList = analysChartService.getWorkFeeTotalListByFacility(schema_name, condition, pageSize, begin);
//            if (null != dataList && !dataList.isEmpty()) {
//                result.setContent(dataList);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//
//    //按位置，统计各工单费用
//    @RequestMapping("/get_facility_work_fee_list_for_asset")
//    public ResponseModel getFacilityWorkFeeListForAsset(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//            String pageSizeStr = request.getParameter("page_size");
//            String pageNumberStr = request.getParameter("page");
//            int pageSize = 0;
//            int pageNumber = 0;
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//            int begin = pageSize * (pageNumber - 1);
//
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, null, beginDate, endDate);
//
//            List<Map<String, Object>> dataList = analysChartService.getWorkFeeListForAsset(schema_name, condition, pageSize, begin);
//            if (null != dataList && !dataList.isEmpty()) {
//                result.setContent(dataList);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//    //按工单类型，查询各天的总数和进行中的总数
//    @RequestMapping("/get_work_type_finished_process_total_by_day")
//    public ResponseModel getWorkFinishedAndProcessTotal(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            String type_id = request.getParameter("typeId");            //工单类型
//            String facility_id = request.getParameter("facility_id");   //设备位置
//            String beginDate = request.getParameter("begin_date");      //开始日期
//            String endDate = request.getParameter("end_date");          //结束日期
//
//            //按条件，获取查询工单完成和未完成数量的sql
//            String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, type_id, beginDate, endDate);
//            String groupByColumn = type_id.equals("1") ? "occur_time" : "create_time";
//
//            List<Map<String, Object>> dataList = analysChartService.getFinishedAndProcessingWorkTotalByDay(schema_name, condition, groupByColumn);
//            if (null != dataList && !dataList.isEmpty()) {
//                result.setContent(dataList);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//    //按各天,查询工单的费用情况
//    @RequestMapping("/get_work_fee_total_by_day")
//    public ResponseModel getWorkFeeTotalByDay(HttpServletRequest request, HttpServletResponse response) {
//        String companyId = request.getParameter("companyId");
//        String account = request.getParameter("account");
//
//        //生成企业库名
//        String schema_name = schema_name_pre + companyId;
//        ResponseModel result = new ResponseModel();
//        User user = userService.getUserByAccount(schema_name, account);
//
//        try {
//            boolean feeStaticRight = pagePermissionService.getPermissionByKey(schema_name, user.getId(), "statistic", "statistic_fee");
//            Map<String, Object> resultInfo = new HashMap<String, Object>();
//            resultInfo.put("feeStaticRight", feeStaticRight);
//            if (feeStaticRight) {
//                String facility_id = request.getParameter("facility_id");   //设备位置
//                String beginDate = request.getParameter("begin_date");      //开始日期
//                String endDate = request.getParameter("end_date");          //结束日期
//
//                //按条件，获取查询工单完成和未完成数量的sql
//                String condition = getSearchSqlFinishedWorkCount(schema_name, user, account, facility_id, null, beginDate, endDate);
//
//                List<Map<String, Object>> dataList = analysChartService.getWorkFeeTotalListByDay(schema_name, condition);
//                if (null != dataList && !dataList.isEmpty()) {
//                    resultInfo.put("feeList", dataList);
//                }
//            }
//            if (null != resultInfo && !resultInfo.isEmpty()) {
//                result.setContent(resultInfo);
//                result.setCode(1);
//                result.setMsg("获取成功！");
//            } else {
//                result.setCode(0);
//                result.setMsg("无数据！");
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg("数据获取有误！");
//            return result;
//        }
//    }
//
//    //按条件，获取查询工单完成和未完成数量的sql
//    private String getSearchSqlFinishedWorkCount(String schema_name, User user, String account, String facility_id, String type_id, String beginDate, String endDate) {
//        String condition = "";
//        if (null == facility_id || facility_id.isEmpty() || facility_id.equals("0")) {
//            //没有场地的权限，则查自己做的工单
//            DataPermissionAllOrSelf facilityPermission = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusiness(schema_name, user, "worksheet");
//            if (DataPermissionAllOrSelf.None.equals(facilityPermission)) {
//                condition = " and (w.create_user_account='" + account + "' or wd.receive_account='" + account + "' )";
//            } else {
//                String facilityIds = dataPermissionForFacility.findFacilityIdsByBusinessForSingalUser(schema_name, user, null, "worksheet");
//                if (null != facilityIds && !facilityIds.isEmpty()) {
//                    condition = " and (org.facility_id in (" + facilityIds + ") or w.create_user_account='" + account + "')";
//                }
//            }
//        } else {
//            condition = " and (org.facility_id in (" + facility_id + ") )";
//        }
//
//        //看设备类型权限
//        String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, null);
//        if (asset_type_word != null && !asset_type_word.isEmpty()) {
//            condition += " and ( ac.id in (" + asset_type_word + ") or ac.ID is null ) ";
//        }
//
//        //查看工单类型
//        if (null != type_id && !type_id.isEmpty()) {
//            condition += " and wt.business_type_id =" + type_id;
//        }
//
//        //时间条件
//        if (beginDate != null && !beginDate.isEmpty() && !beginDate.equals("")) {
//            Timestamp beginTime = Timestamp.valueOf(beginDate + " 00:00:00");
//            if ("1".equals(type_id)) {
//                condition += " and w.occur_time >='" + beginTime + "'";
//            } else if ("2".equals(type_id)) {
//                condition += " and w.create_time >='" + beginTime + "'";
//            } else {
//                condition += " and wd.finished_time >='" + beginTime + "'";
//            }
//
//        }
//        if (endDate != null && !endDate.isEmpty() && !endDate.equals("")) {
//            Timestamp endTime = Timestamp.valueOf(endDate + " 23:59:59");
//            if ("1".equals(type_id)) {
//                condition += " and w.occur_time <'" + endTime + "'";
//            } else if ("2".equals(type_id)) {
//                condition += " and w.create_time <'" + endTime + "'";
//            } else {
//                condition += " and wd.finished_time <'" + endTime + "'";
//            }
//        }
//        return condition;
//    }

}
