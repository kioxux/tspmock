package com.gengyun.senscloud.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Dong_wudang on 2018/9/29.
 */
@Configuration
public class RestTemplateConfig {

    public RestTemplate restTemplate(ClientHttpRequestFactory factory){
        RestTemplate restTemplate = new RestTemplate(factory);
//        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
//        Iterator<HttpMessageConverter<?>> iterable =  messageConverters.iterator();
//        while (iterable.hasNext()){
//            HttpMessageConverter<?> converter = iterable.next();
//            if(converter instanceof StringHttpMessageConverter){
//
//            }
//        }
        return restTemplate;

    }


}
