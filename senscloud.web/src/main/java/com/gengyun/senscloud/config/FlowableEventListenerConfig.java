package com.gengyun.senscloud.config;

import com.gengyun.senscloud.flowable.listener.WorkHandleEventListener;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.api.delegate.event.FlowableEventDispatcher;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.annotation.Resource;

/**
 * 配置工作流事件监听器
 */
@Configuration
public class FlowableEventListenerConfig implements ApplicationListener<ContextRefreshedEvent> {

    @Resource
    SpringProcessEngineConfiguration configuration;

    @Resource
    WorkHandleEventListener workHandleEventListener;

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        FlowableEventDispatcher dispatcher = configuration.getEventDispatcher();
        // 任务创建全局监听
        dispatcher.addEventListener(workHandleEventListener,
                FlowableEngineEventType.TASK_CREATED,
                FlowableEngineEventType.TASK_COMPLETED,
                FlowableEngineEventType.TASK_ASSIGNED);
    }
}
