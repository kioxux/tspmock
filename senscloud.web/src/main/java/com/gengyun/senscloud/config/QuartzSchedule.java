package com.gengyun.senscloud.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
public class QuartzSchedule {
    @Autowired
    SensJobFactory sensJobFactory;
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setOverwriteExistingJobs(true);
        factory.setApplicationContextSchedulerContextKey("applicationContext");
        // 延时启动
//        factory.setStartupDelay(20);

        // 自定义Job Factory，用于Spring注入
        factory.setJobFactory(sensJobFactory);

        return factory;
    }
}