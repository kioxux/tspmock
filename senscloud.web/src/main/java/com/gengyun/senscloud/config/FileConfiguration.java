package com.gengyun.senscloud.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileConfiguration {

    @Value("${senscloud.file_upload}")
    public String file_upload_dir;
}
