package com.gengyun.senscloud.config;

import com.gengyun.senscloud.flowable.*;
import org.flowable.common.engine.impl.cfg.multitenant.TenantAwareDataSource;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.common.engine.impl.interceptor.Command;
import org.flowable.common.engine.impl.persistence.StrongUuidGenerator;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.impl.SchemaOperationProcessEngineClose;
import org.flowable.engine.impl.cfg.multitenant.ExecuteSchemaOperationCommand;
import org.flowable.engine.impl.util.CommandContextUtil;
import org.flowable.job.service.impl.asyncexecutor.multitenant.ExecutorPerTenantAsyncExecutor;
import org.flowable.job.service.impl.asyncexecutor.multitenant.TenantAwareAsyncExecutor;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.configurator.AutoDeploymentStrategy;
import org.flowable.task.service.TaskServiceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

public class CustomSpringProcessEngineConfiguration extends SpringProcessEngineConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomSpringProcessEngineConfiguration.class);

    protected TenantInfoHolder tenantInfoHolder;
    protected boolean booted;

    public CustomSpringProcessEngineConfiguration(TenantInfoHolder tenantInfoHolder, DataSource dataSource,
                                                  PlatformTransactionManager platformTransactionManager) {
        this.tenantInfoHolder = tenantInfoHolder;

        // Using the UUID generator, as otherwise the ids are pulled from a global pool of ids, backed by
        // a database table. Which is impossible with a multi-database-schema setup.

        // Also: it avoids the need for having a process definition cache for each tenant
        this.idGenerator = new StrongUuidGenerator();
        this.dataSource = dataSource;
        this.databaseType = DATABASE_TYPE_POSTGRES;
        this.transactionManager = platformTransactionManager;
        this.schemaManager = new CustomerProcessDbSchemaManager(tenantInfoHolder);
        this.taskService = new CustomTaskServiceImpl(this);
        deploymentStrategies.clear();
        deploymentStrategies.add(new DefaultAutoDeploymentStrategy(tenantInfoHolder));
        deploymentStrategies.add(new SingleResourceAutoDeploymentStrategy(tenantInfoHolder));
        deploymentStrategies.add(new ResourceParentFolderAutoDeploymentStrategy(tenantInfoHolder));
    }

    /**
     * Add a new {@link DataSource} for a tenant, identified by the provided tenantId, to the engine. This can be done after the engine has booted up.
     *
     * Note that the tenant identifier must have been added to the {@link TenantInfoHolder} *prior* to calling this method.
     */
    public void registerTenant(String tenantId, DataSource dataSource) {
        DataSource ds = super.getDataSource();
        if (ds instanceof TenantAwareDataSource) {
            ((TenantAwareDataSource) ds).addDataSource(tenantId, dataSource);
        } else {
            TenantAwareDataSource tds = (TenantAwareDataSource)(((TransactionAwareDataSourceProxy)ds).getTargetDataSource());
            if(tds != null) {
                tds.addDataSource(tenantId, dataSource);
            } else {
                return;
            }
        }

        if (booted) {
            createTenantSchema(tenantId);

            createTenantAsyncJobExecutor(tenantId);

            tenantInfoHolder.setCurrentTenantId(tenantId);
            super.postProcessEngineInitialisation();
            tenantInfoHolder.clearCurrentTenantId();
        }
    }

    @Override
    public void initAsyncExecutor() {

        if (asyncExecutor == null) {
            asyncExecutor = new ExecutorPerTenantAsyncExecutor(tenantInfoHolder);
        }

        super.initAsyncExecutor();

        if (asyncExecutor instanceof TenantAwareAsyncExecutor) {
            for (String tenantId : tenantInfoHolder.getAllTenants()) {
                ((TenantAwareAsyncExecutor) asyncExecutor).addTenantAsyncExecutor(tenantId, false); // false -> will be started later with all the other executors
            }
        }
    }

    @Override
    public ProcessEngine buildProcessEngine() {

        // Disable schema creation/validation by setting it to null.
        // We'll do it manually, see buildProcessEngine() method (hence why it's copied first)
        String originalDatabaseSchemaUpdate = this.databaseSchemaUpdate;
        this.databaseSchemaUpdate = null;

        // Also, we shouldn't start the async executor until *after* the schema's have been created
        boolean originalIsAutoActivateAsyncExecutor = this.asyncExecutorActivate;
        this.asyncExecutorActivate = false;

        ProcessEngine processEngine = super.buildProcessEngine();

        // Reset to original values
        this.databaseSchemaUpdate = originalDatabaseSchemaUpdate;
        this.asyncExecutorActivate = originalIsAutoActivateAsyncExecutor;

        // Create tenant schema
        for (String tenantId : tenantInfoHolder.getAllTenants()) {
            createTenantSchema(tenantId);
        }

        // Start async executor
        if (asyncExecutor != null && originalIsAutoActivateAsyncExecutor) {
            asyncExecutor.start();
        }

        booted = true;
        return processEngine;
    }

    protected void createTenantSchema(String tenantId) {
        LOGGER.info("creating/validating database schema for tenant {}", tenantId);
        tenantInfoHolder.setCurrentTenantId(tenantId);
        getCommandExecutor().execute(getSchemaCommandConfig(), new ExecuteSchemaOperationCommand(databaseSchemaUpdate));
        tenantInfoHolder.clearCurrentTenantId();
    }

    protected void createTenantAsyncJobExecutor(String tenantId) {
        ((TenantAwareAsyncExecutor) asyncExecutor).addTenantAsyncExecutor(tenantId, isAsyncExecutorActivate() && booted);
    }

    @Override
    protected TaskServiceConfiguration instantiateTaskServiceConfiguration() {
        TaskServiceConfiguration taskServiceConfiguration = new TaskServiceConfiguration();
        CustomMyBatisTaskDataManager dataManager = new CustomMyBatisTaskDataManager();
        taskServiceConfiguration.setTaskDataManager(dataManager);
        taskServiceConfiguration.setTaskEntityManager(new CustomTaskEntityManagerImpl(taskServiceConfiguration, dataManager));
        return taskServiceConfiguration;
    }

    @Override
    protected void postProcessEngineInitialisation() {
        // empty here. will be done in registerTenant
    }

    @Override
    public Runnable getProcessEngineCloseRunnable() {
        return () -> {
            for (String tenantId : tenantInfoHolder.getAllTenants()) {
                tenantInfoHolder.setCurrentTenantId(tenantId);
                commandExecutor.execute(getProcessEngineCloseCommand());
                tenantInfoHolder.clearCurrentTenantId();
            }
        };
    }

    public Command<Void> getProcessEngineCloseCommand() {
        return commandContext -> {
            CommandContextUtil.getProcessEngineConfiguration(commandContext).getCommandExecutor().execute(new SchemaOperationProcessEngineClose());
            return null;
        };
    }

    public TenantInfoHolder getTenantInfoHolder() {
        return tenantInfoHolder;
    }

    @Override
    protected void autoDeployResources(ProcessEngine processEngine) {
        if (deploymentResources != null && deploymentResources.length > 0) {
            final AutoDeploymentStrategy strategy = getAutoDeploymentStrategyWithTenantHolder(deploymentMode, tenantInfoHolder);
            strategy.deployResources(deploymentName, deploymentResources, processEngine.getRepositoryService());
        }
    }

    protected AutoDeploymentStrategy getAutoDeploymentStrategyWithTenantHolder(final String mode,
                                                                               TenantInfoHolder tenantInfoHolder) {
        AutoDeploymentStrategy result = new DefaultAutoDeploymentStrategy(tenantInfoHolder);
        for (final AutoDeploymentStrategy strategy : deploymentStrategies) {
            if (strategy.handlesMode(mode)) {
                result = strategy;
                break;
            }
        }
        return result;
    }

}
