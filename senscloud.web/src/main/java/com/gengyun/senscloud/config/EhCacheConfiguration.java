package com.gengyun.senscloud.config;
 
import com.gengyun.senscloud.ehcache.ClusterEhCacheManagerFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;


/**
 * EhCache配置文件，可以替代ehcache.xml 文件
 */
 
@Configuration
@EnableCaching
public class EhCacheConfiguration implements CachingConfigurer {

    @Resource
    ClusterEhCacheManagerFactoryBean bean;

 
    @Bean
    @Override
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(bean.getObject());
    }

    @Bean
    @Override
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }

    @Override
    public CacheResolver cacheResolver() { return null; }

    @Override
    public CacheErrorHandler errorHandler() {
        return null;
    }
 
}

