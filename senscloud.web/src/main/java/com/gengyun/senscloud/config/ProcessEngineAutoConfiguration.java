/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gengyun.senscloud.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.gengyun.senscloud.flowable.TenantInfoHolderImpl;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.HttpRequestUtils;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONObject;
import org.flowable.common.engine.impl.cfg.multitenant.TenantAwareDataSource;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.*;
import org.flowable.spring.boot.app.AppEngineAutoConfiguration;
import org.flowable.spring.boot.app.AppEngineServicesAutoConfiguration;
import org.flowable.spring.boot.app.FlowableAppProperties;
import org.flowable.spring.boot.condition.ConditionalOnProcessEngine;
import org.flowable.spring.boot.idm.FlowableIdmProperties;
import org.flowable.spring.boot.process.FlowableProcessProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Configuration
@ConditionalOnProcessEngine
@EnableConfigurationProperties({
        FlowableProperties.class,
        FlowableMailProperties.class,
        FlowableProcessProperties.class,
        FlowableAppProperties.class,
        FlowableIdmProperties.class
})
@AutoConfigureAfter({
        FlowableJpaAutoConfiguration.class,
        AppEngineAutoConfiguration.class,
})
@AutoConfigureBefore({
        AppEngineServicesAutoConfiguration.class,
})
@Import({
        FlowableJobConfiguration.class
})
public class ProcessEngineAutoConfiguration extends AbstractSpringEngineAutoConfiguration {

    private final FlowableProcessProperties processProperties;
    private final FlowableIdmProperties idmProperties;
    private final FlowableMailProperties mailProperties;
    @javax.annotation.Resource
    CompanyService companyService;
    @Value("${senscloud.com.license}")
    private String license;

    public ProcessEngineAutoConfiguration(FlowableProperties flowableProperties, FlowableProcessProperties processProperties,
                                           FlowableIdmProperties idmProperties, FlowableMailProperties mailProperties) {

        super(flowableProperties);
        this.processProperties = processProperties;
        this.idmProperties = idmProperties;
        this.mailProperties = mailProperties;
    }

    @Bean
    public TenantInfoHolder tenantInfoHolder() {
        return new TenantInfoHolderImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public SpringProcessEngineConfiguration springProcessEngineConfiguration(DataSource mainDataSource,
                                                                             TenantInfoHolder tenantInfoHolder) throws IOException {
        DataSource dataSource = new TenantAwareDataSource(tenantInfoHolder);
        PlatformTransactionManager platformTransactionManager = new DataSourceTransactionManager(dataSource);
        CustomSpringProcessEngineConfiguration conf = new CustomSpringProcessEngineConfiguration(tenantInfoHolder, dataSource, platformTransactionManager);

        List<Map<String, Object>> result = DataChangeUtil.scdStringToList(companyService.requestParamOne(license,"license","findAllCompanyByLicense"));
        List<Map<String, Object>> companyList = result;
        // List<Map<String, Object>> companyList = companyService.findAll();
        for (Map<String, Object> company : companyList) {
            DruidDataSource druidDataSource = ((DruidDataSource) mainDataSource).cloneDruidDataSource();
            String schemaName = SenscloudUtil.getSchemaByCompId(company.get("id"));
            druidDataSource.setUrl(druidDataSource.getUrl() + "?characterEncoding=UTF-8&serverTimezone=GMT%2b8&useUnicode=true&currentSchema=" + schemaName);
            conf.getTenantInfoHolder().setCurrentTenantId(schemaName);
            conf.registerTenant(schemaName, druidDataSource);
            conf.getTenantInfoHolder().clearCurrentTenantId();
        }
        conf.setDatabaseSchemaUpdate("true");

        List<Resource> resources = this.discoverDeploymentResources(
                flowableProperties.getProcessDefinitionLocationPrefix(),
                flowableProperties.getProcessDefinitionLocationSuffixes(),
                flowableProperties.isCheckProcessDefinitions()
        );

        if (resources != null && !resources.isEmpty()) {
            conf.setDeploymentResources(resources.toArray(new Resource[0]));
            conf.setDeploymentName(flowableProperties.getDeploymentName());
        }

        configureSpringEngine(conf, platformTransactionManager);
        configureEngine(conf, dataSource);

        conf.setDeploymentName(defaultText(flowableProperties.getDeploymentName(), conf.getDeploymentName()));

        conf.setDisableIdmEngine(!(idmProperties.isEnabled()));

        conf.setAsyncExecutorActivate(flowableProperties.isAsyncExecutorActivate());
        conf.setAsyncHistoryExecutorActivate(flowableProperties.isAsyncHistoryExecutorActivate());

        conf.setMailServerHost(mailProperties.getHost());
        conf.setMailServerPort(mailProperties.getPort());
        conf.setMailServerUsername(mailProperties.getUsername());
        conf.setMailServerPassword(mailProperties.getPassword());
        conf.setMailServerDefaultFrom(mailProperties.getDefaultFrom());
        conf.setMailServerForceTo(mailProperties.getForceTo());
        conf.setMailServerUseSSL(mailProperties.isUseSsl());
        conf.setMailServerUseTLS(mailProperties.isUseTls());

        conf.setEnableProcessDefinitionHistoryLevel(processProperties.isEnableProcessDefinitionHistoryLevel());
        conf.setProcessDefinitionCacheLimit(processProperties.getDefinitionCacheLimit());
        conf.setEnableSafeBpmnXml(processProperties.isEnableSafeXml());

        conf.setHistoryLevel(flowableProperties.getHistoryLevel());
        conf.setActivityFontName("宋体");
        conf.setLabelFontName("宋体");
        conf.setAnnotationFontName("宋体");

        Set<String> customMybatisXMLMappers = new HashSet<>();
        customMybatisXMLMappers.add("mappers/task.xml");
        conf.setCustomMybatisXMLMappers(customMybatisXMLMappers);
        return conf;
    }
}
