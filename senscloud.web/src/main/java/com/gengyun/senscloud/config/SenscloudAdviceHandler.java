package com.gengyun.senscloud.config;

import com.gengyun.senscloud.common.*;
import com.gengyun.senscloud.entity.CompanyEntity;
import com.gengyun.senscloud.entity.UserEntity;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.login.ClientTokenService;
import com.gengyun.senscloud.service.login.CompanyService;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 全局异常处理
 */
@ControllerAdvice
public class SenscloudAdviceHandler {
    @Resource
    LogsService logService;
    @Resource
    CompanyService companyService;
    @Resource
    ClientTokenService clientTokenService;
    @Resource
    UserService userService;
    @Resource
    SystemConfigService systemConfigService;
    @Resource
    PagePermissionService pagePermissionService;
    @Resource
    SelectOptionService selectOptionService;

    /**
     * 处理自定义的业务异常
     *
     * @param e 异常信息
     * @return 错误信息
     */
    @ExceptionHandler(value = SenscloudException.class)
    @ResponseBody
    public ResponseModel senscloudExceptionHandler(SenscloudException e) {
        return ResponseModel.errorQuickExp(e);
    }

    /**
     * 处理自定义的业务异常
     *
     * @param e 异常信息
     * @return 错误信息
     */
    @ExceptionHandler(value = SenscloudError.class)
    @ResponseBody
    public ResponseModel senscloudExceptionHandler(SenscloudError e) {
        return RegexUtil.optNotNull(e.getMsgContent()).map(ThreadLocal::get).filter(info -> info.size() > 0)
                .map(m -> {
                    String schemaName = m.get(ErrorConstant.EC_SCHEMA_KEY).toString();
                    String userId = m.get(ErrorConstant.EC_USER_ID_KEY).toString();
                    String remark = RegexUtil.optStrOrNull(m.get(ErrorConstant.EC_REMARK_KEY));
                    logService.newErrorLog(schemaName, e.getMessage(), userId, remark);
                    return RegexUtil.optStrOrBlank(m.get(ErrorConstant.EC_EXP_KEY));
                }).filter(err -> !"".equals(err)).map(err -> ResponseModel.errorQuickExp(new SenscloudException(err))).orElse(ResponseModel.okOnly());
    }

    /**
     * 处理其他异常
     *
     * @param e 异常信息
     * @return 错误信息
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseModel exceptionHandler(Exception e) {
        return ResponseModel.expUnKnown(e);
    }

    /**
     * 设置系统前置变量
     *
     * @return 系统前置变量
     */
    @ModelAttribute(name = "methodParam")
    public synchronized MethodParam senscloudData(@RequestParam Map<String, Object> paramMap) {
        return RegexUtil.optNotNull(HttpRequestUtils.getRequest()).map(rq -> {
            String uri = rq.getRequestURI();
            if (uri.startsWith("/swagger-resources") || uri.contains("api-docs")|| uri.contains("wxMsg")) {
                return new MethodParam();
            } else if ("/searchLoginData".equals(uri) || "/doNFCLogin".equals(uri) || "/searchLoginImage".equals(uri)) {
                MethodParam methodParam = new MethodParam();
                String port = RegexUtil.optNotBlankStrOpt(rq.getServerPort()).filter(p -> !"80".equals(p) && !"443".equals(p)).map(":"::concat).orElse("");
                String domainName = RegexUtil.optStrOrBlank(rq.getServerName()).concat(port);
                methodParam.setLoginPort(port);
                methodParam.setLoginDomain(domainName);
                methodParam.setClientName(RegexUtil.optStrOrVal(paramMap.get("clientType"), SensConstant.PC_CLIENT_NAME));
                return methodParam;
            } else if ("/error".equals(uri)) {
                throw new SenscloudException(RegexUtil.optIntegerOrNull(rq.getAttribute("javax.servlet.error.status_code"), LangConstant.TITLE_PD));
            } else {
                String token = RegexUtil.optStrOrNull(paramMap.get("token"));
                MethodParam aaa = RegexUtil.optNotBlankStrOpt(token).map(t -> ScdSesRecordUtil.getScdSesDataByKey(t, "loginBaseUserInfo")).filter(io -> RegexUtil.optNotNull(io).isPresent()).map(io -> (MethodParam) io).orElseGet(MethodParam::new);
                MethodParam methodParam = new MethodParam();
                methodParam.setToken(aaa.getToken());
                methodParam.setSchemaName(aaa.getSchemaName());
                methodParam.setCompanyId(aaa.getCompanyId());
                methodParam.setCompanyName(aaa.getCompanyName());
                methodParam.setOpenSms(aaa.getOpenSms());
                methodParam.setNeedCC(aaa.getNeedCC());
                methodParam.setUserId(aaa.getUserId());
                methodParam.setAccount(aaa.getAccount());
                methodParam.setUserName(aaa.getUserName());
                methodParam.setUserLang(aaa.getUserLang());
                methodParam.setMiniProgramOpenId(aaa.getMiniProgramOpenId());
                methodParam.setOfficialOpenId(aaa.getOfficialOpenId());
                methodParam.setUnionId(aaa.getUnionId());
                methodParam.setWeiXinCode(aaa.getWeiXinCode());
                methodParam.setUserPhone(aaa.getUserPhone());
                methodParam.setUserGpName(aaa.getUserGpName());
                methodParam.setUserGpList(aaa.getUserGpList());
                methodParam.setClientName(aaa.getClientName());
                methodParam.setFileTmpInfo(aaa.getFileTmpInfo());
                methodParam.setUserPermissionList(aaa.getUserPermissionList());
                methodParam.setUserModelPrmInfo(aaa.getUserModelPrmInfo());
                methodParam.setCompanySystemName(aaa.getCompanySystemName());
                methodParam.setCompanyLogo(aaa.getCompanyLogo());
                methodParam.setSystemLang(aaa.getSystemLang());
                if (!RegexUtil.optIsPresentStr(methodParam.getToken())) {
                    methodParam.setToken(token);
                    methodParam.setClientName(RegexUtil.optStrOrVal(paramMap.get("clientType"), SensConstant.PC_CLIENT_NAME));
                }
                methodParam.setUri(uri);
//                methodParam.setPageSize(RegexUtil.optIntegerOrNull(paramMap.get("pageSize"), methodParam, ErrorConstant.EC_PAGE_PARAM_1, LangConstant.MSG_BK));
//                methodParam.setPageNumber(RegexUtil.optIntegerOrNull(paramMap.get("pageNumber"), methodParam, ErrorConstant.EC_PAGE_PARAM_2, LangConstant.MSG_BK));
//                methodParam.setNeedPagination(RegexUtil.optBool(paramMap.get("isNeedPagination")));
//                methodParam.setSearchDtlType(RegexUtil.optStrOrNull(paramMap.get("searchDtlType")));
//                methodParam.setIds(RegexUtil.optStrOrNull(paramMap.get("ids")));
//                methodParam.setDataId(null);
//                methodParam.setDataIdArray(null);
                if (!"/doGoSns".equals(uri) && !"/checkCompanies".equals(uri) && !"/sendVerificationCodeByPhone".equals(uri) && !"/checkVerificationCode".equals(uri) && !"/editPsdByPhone".equals(uri)) {
                    if (!RegexUtil.optIsPresentStr(methodParam.getCompanyId())) {
                        RegexUtil.optNotBlankStrOpt(ScdSesRecordUtil.getScdSesDataByKey(token, Constants.SCD_SES_KEY_COMPANY_ID)).map(Long::parseLong).ifPresent(cio -> {
                            CompanyEntity company = companyService.getCompanyById(cio);
                            RegexUtil.optNotNull(company).ifPresent(cp -> {
                                methodParam.setCompanyId(cio);
                                methodParam.setCompanyName(company.getCompany_name());
                                methodParam.setOpenSms(company.getIs_open_sms());
                                if (RegexUtil.optBool(company.isIs_company_logo())) {
                                    methodParam.setCompanySystemName(company.getSystem_name());
                                    methodParam.setCompanyLogo(company.getCompany_logo_id());
                                } else {
                                    methodParam.setCompanySystemName(null); // 耕云设备健康云
                                    methodParam.setCompanyLogo(null); // img/logo.png
                                }
                            });
                            String schemaName = SenscloudUtil.getSchemaByCompId(cio);
                            methodParam.setSchemaName(schemaName);
                            String account;
                            if ("/doLogin".equals(uri)) {
                                account = RegexUtil.optNotBlankStrOrExp(clientTokenService.getLoginAccountByToken(methodParam), LangConstant.MSG_BC); // 帐号或密码错误，请重试！
                            } else {
                                account = RegexUtil.optNotBlankStrOrExp(clientTokenService.getLoginAccountByToken(methodParam), ResponseConstant.RE_LOGIN_CODE); // 用户未登录！
                            }
                            UserEntity user = userService.getUserForSystemLoginCache(schemaName, account);
                            RegexUtil.optNotNullOrExp(user, ResponseConstant.RE_LOGIN_CODE); // 用户未登录！
                            methodParam.setUserId(user.getId());
                            methodParam.setAccount(user.getAccount());
                            methodParam.setUserName(user.getUser_name());
                            methodParam.setSystemLang(systemConfigService.getSysLang(methodParam));
                            methodParam.setUserLang(RegexUtil.optStrAndValOrNull(user.getLanguage_tag(), methodParam.getSystemLang()));
                            methodParam.setMiniProgramOpenId(user.getMini_program_open_id());
                            methodParam.setOfficialOpenId(user.getOfficial_open_id());
                            methodParam.setUnionId(user.getUnion_id());
                            methodParam.setWeiXinCode(user.getWei_xin_code());
                            methodParam.setUserPhone(user.getMobile());
                            List<Map<String, Object>> ugpList = userService.getUserGpList(methodParam, methodParam.getUserId());
                            RegexUtil.optNotNullList(ugpList).ifPresent(l -> {
                                RegexUtil.optNotNullMap(l.get(0)).ifPresent(f -> methodParam.setUserGpName(RegexUtil.optStrOrBlank(f.get("gp_name"))));
                                Map<String, List<Map<String, Object>>> ml = l.stream().collect(Collectors.groupingBy(g -> g.get("group_id").toString()));
                                methodParam.setUserGpList(ml);
                            });
                            pagePermissionService.getUserPrmListByInfo(methodParam);
                            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), "loginBaseUserInfo", methodParam);
                        });
                    }
                    if (!"/doLogin".equals(uri)) {
                        pagePermissionService.getUserPrmListByInfo(methodParam);
                        RegexUtil.optNotBlankStrOrExp(methodParam.getSchemaName(), ResponseConstant.RE_LOGIN_CODE); // 用户未登录！
                    }
                }
                return this.doSetCommonInfo(methodParam, paramMap);
            }
        }).orElseGet(() -> {
            MethodParam methodParam = new MethodParam();
            methodParam.setClientName(SensConstant.PC_CLIENT_NAME);
            return this.doSetCommonInfo(methodParam, paramMap);
        });
    }

    /**
     * 设置系统前置变量公共处理
     *
     * @param methodParam 系统参数
     * @param paramMap    请求参数
     * @return 系统前置变量
     */
    private MethodParam doSetCommonInfo(MethodParam methodParam, Map<String, Object> paramMap) {
        String uri = RegexUtil.optStrOrBlank(methodParam.getUri());
        if (!"/doGoSns".equals(uri) && !"/checkCompanies".equals(uri) && !"/doLogin".equals(uri) && !"/sendVerificationCodeByPhone".equals(uri) && !"/checkVerificationCode".equals(uri) && !"/editPsdByPhone".equals(uri)) {
            doCheckIllegal(methodParam);
        }
        if ("/getOptionsByKey".equals(uri) || "/getOptionInfoByKey".equals(uri) || "/getOptionNameByCode".equals(uri)) {
            methodParam.setLangChangeType(Constants.LANG_CHANGE_PAGE);
        }
        pagePermissionService.applyCheckUserPrm(methodParam, paramMap);
        return methodParam;
    }

    /**
     * 非法字符校验（token和）
     *
     * @param methodParam 系统参数
     */
    private void doCheckIllegal(MethodParam methodParam) {
        String token = methodParam.getToken();
        List<String> fcList = RegexUtil.optNotNullListStr(RegexUtil.optNotNull(ScdSesRecordUtil.getScdSesDataByKey(token, "filterCheckList")).map(DataChangeUtil::scdObjToListStr).orElse(null))
                .orElseGet(() -> {
                    String filterCheck = selectOptionService.getSelectOptionTextByCode(methodParam, "system_check", "filter_check");
                    return RegexUtil.optNotBlankStrOpt(filterCheck).map(fc -> fc.split(",")).map(Arrays::asList).filter(l -> l.size() > 0).map(list -> {
                        ScdSesRecordUtil.setScdSesDataByKey(token, "filterCheckList", list);
                        return list;
                    }).orElse(null);
                });
        RegexUtil.optNotNullListStr(fcList).map(SenscloudUtil::injectInput).filter(RegexUtil::optIsPresentStr).map(i -> new SenscloudException(ResponseConstant.ERROR_ILLEGAL_CODE));
    }
}
