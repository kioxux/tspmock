package com.gengyun.senscloud.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * 初始化系统默认时区
 */
@Configuration
public class TimeZoneConfiguration {

    /**
     *  系统默认TimeZoneId (例如：中国-Asia/Shanghai)
     */
    @Value("${system.timeZone.id}")
    private String systemTimeZoneId;

    @PostConstruct
    void handleTimeZone() {
        if(StringUtils.isBlank(systemTimeZoneId)) {
            systemTimeZoneId = "Asia/Shanghai";//如果没有配置时区，默认设为东八区
        }

        TimeZone.setDefault(TimeZone.getTimeZone(systemTimeZoneId));
        System.setProperty("user.timezone", systemTimeZoneId);
    }

//    public static void main(String[] args) {
//        SpringApplication.run(TimeZoneConfiguration.class, args);
//    }
}
