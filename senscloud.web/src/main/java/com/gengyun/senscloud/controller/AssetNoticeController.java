package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.Asset;
//import com.gengyun.senscloud.model.AssetNotice;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.github.pagehelper.Page;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang3.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.List;
//
//import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
//import static org.springframework.web.bind.annotation.RequestMethod.POST;
//
//@RestController
public class AssetNoticeController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    AssetNoticeService assetNoticeService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 初始化资产维保人
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_notice_init")
//    public ModelAndView initAssetNotice(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            result.setContent(null);
//            result.setCode(1);
//            result.setMsg("");
//            String asset_id = request.getParameter("asset_id");
//
//            //TODO:找到设备，获取其所在位置:关系已变更该方法不能查出设备的相关组织 待修改：zys
//            Asset assetModel = assetService.findByID(schema_name, asset_id);
//
//            List<User> userList = userService.getUser(schema_name, (int) assetModel.getIntsiteid());
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(userList);
//            return new ModelAndView("AssetNoticeManage").addObject("result", result).addObject("userList", userList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CONF_PRO) + ex.getMessage());
//            return new ModelAndView("AssetNoticeManage").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得所有的资产维保人列表
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_notice_list")
//    public String findALLAssetNotice(@RequestParam("asset_id") String asset_id,
//                                     @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                     @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                     @RequestParam(name = "sortName", required = false) String sortName,
//                                     @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                     HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            if (pageNumber == null) {
//                pageNumber = 1;
//            }
//            if (pageSize == null) {
//                pageSize = 20;
//            }
//            String orderBy = null;
//            if (org.apache.commons.lang.StringUtils.isNotEmpty(sortName)) {
//                if (org.apache.commons.lang.StringUtils.isEmpty(sortOrder)) {
//                    sortOrder = "asc";
//                }
//                orderBy = sortName + " " + sortOrder;
//            }
//            Page page = PageHelper.startPage(pageNumber, pageSize, orderBy);
//            List<AssetNotice> dataList = assetNoticeService.findAllByAssetId(schema_name, asset_id);
//            JSONObject pageResult = new JSONObject();
//            pageResult.put("total", page.getTotal());
//            pageResult.put("rows", dataList);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping(value = "/asset_notice_save", method = POST, consumes = APPLICATION_JSON_VALUE)
//    public ResponseModel saveAssetNotice(@RequestBody AssetNotice assetNotice, HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        if (assetNotice == null || RegexUtil.isNull(assetNotice.getAsset_id()) || StringUtils.isEmpty(assetNotice.getUser_account())) {
//            result.setCode(-1);
//            if (assetNotice.getAsset_id() == null) {
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CONF_ID_NOT_NULL));
//            } else if (StringUtils.isEmpty(assetNotice.getUser_account())) {
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CONF_ACC_NOT_NULL));
//            } else {
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CONF_NOT_NULL));
//            }
//        } else {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            User user = authService.getLoginUser(request);
//            String checkResult = assetService.checkAssetList(request, user, schema_name, assetNotice.getAsset_id());
//            if (RegexUtil.isNull(checkResult)) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                result.setMsg(tmpMsg);
//                return result;
//            }
//
//            //看看是否有重复，如果有重复，则执行更新操作
//            AssetNotice notice = assetNoticeService.findOneByAssetIdAndUserAccount(schema_name, assetNotice.getAsset_id(), assetNotice.getUser_account());
//            //保存资产维保人员
//            int count = 0;
//            if (notice == null && assetNotice.getId() == null) {
//                assetNotice.setCreate_time(new Timestamp(System.currentTimeMillis()));
//                assetNotice.setCreate_user_account(user.getAccount());
//                count = assetNoticeService.insert(schema_name, assetNotice);
//            } else if (notice == null || notice.getId() == assetNotice.getId()) {
//                count = assetNoticeService.update(schema_name, assetNotice);
//            }
//            if (count > 0) {
//                //增加日志
//                logService.AddLog(schema_name, "asset", assetNotice.getAsset_id(), selectOptionService.getLanguageInfo(LangConstant.MSG_CONF_ACCOUNT) + assetNotice.getUser_account(), user.getAccount());
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUB_CON_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUB_CON_DE));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_notice_delete")
//    public ResponseModel deleteAssetNotice(@RequestParam("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        if (id == null) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CON_ID_NULL));
//        } else {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            User user = authService.getLoginUser(request);
//            AssetNotice notice = assetNoticeService.findOneById(schema_name, id);
//            if (null == notice || RegexUtil.isNull(notice.getAsset_id())) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASS_NOT));
//                return result;
//            }
//            String checkResult = assetService.checkAssetList(request, user, schema_name, notice.getAsset_id());
//            if (RegexUtil.isNull(checkResult)) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                result.setMsg(tmpMsg);
//                return result;
//            }
//
//            int count = assetNoticeService.delete(schema_name, id);
//            if (count > 0) {
//                //增加日志
//                logService.AddLog(schema_name, "asset", notice.getAsset_id(), selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_CONF_ACCOUNT) + notice.getUser_account(), user.getAccount());
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_CON_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_CON_DE));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @Deprecated
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/send_asset_notice")
//    public ResponseModel sendAssetNotice(@RequestParam("asset_id") String assetId, @RequestParam("type") Integer type, String content,
//                                         @RequestParam("business_type") String businessType, @RequestParam("business_no") String businessNo,
//                                         @RequestParam("send_account") String sendAccount, HttpServletRequest request) {
//        Company company = AuthService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            if (company.getIs_open_sms()) {
//                assetNoticeService.sendAssetNotice(schema_name, assetId, type, content, businessType, businessNo, sendAccount);
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_NOTIFICATION_SU));
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_NOTIFICATION_DE));
//        }
//    }
}
