package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.dynamic.impl.WorkSheetHandleServiceImpl;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 工单处理
 * User: sps
 * Date: 2018/11/20
 * Time: 上午11:20
 */
@Api(tags = "工单处理")
@RestController
public class WorkSheetHandleController {

    @Resource
    WorkSheetHandleServiceImpl workSheetHandleService;

    @ApiOperation(value = "获取工单页面数据-新增、处理、详情", notes = ResponseConstant.RSP_DESC_SEARCH_SINGLE_WORK_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "subWorkCode", "pageType", "workTypeId", "workTemplateCode"})
    @RequestMapping(value = "/searchSingleWorkInfo", method = RequestMethod.POST)
    public ResponseModel searchSingleWorkInfo(MethodParam methodParam,
                                              @ApiParam(value = "工单编号(处理、查看详情时，必填)") @RequestParam(required = false) String subWorkCode,
                                              @ApiParam(value = "页面类型(处理、查看详情时，必填) 基本类型（处理）：base  详情：detail") @RequestParam(required = false) String pageType,
                                              @ApiParam(value = "工单类型id(新增、查看详情时，必填)") @RequestParam(required = false) String workTypeId,
                                              @ApiParam(value = "工单类型id(新增时，必填)") @RequestParam(required = false) Map<String, Object> params,
                                              @ApiParam(value = "工单模板编码(查询子页面模板数据时，必填)") @RequestParam(required = false) String workTemplateCode) {
        return ResponseModel.ok(workSheetHandleService.getSingleWorkInfo(methodParam, params, subWorkCode, pageType, workTypeId, workTemplateCode));
    }

    @ApiOperation(value = "扫码获取模板信息", notes = ResponseConstant.RSP_DESC_SEARCH_METADATA_WORK_BY_SCAN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "scanCode", "needWorkInfo", "workTypeId"})
    @RequestMapping(value = "/searchMetadataWorkByScan", method = RequestMethod.POST)
    public ResponseModel getCollection(MethodParam methodParam,
                                       @ApiParam(value = "扫描的信息", required = true) @RequestParam String scanCode,
                                       @ApiParam(value = "工单类型id", required = true) @RequestParam String workTypeId,
                                       @ApiParam(value = "是否返回工单信息 true:是 false:否", required = true) @RequestParam boolean needWorkInfo) {
        return ResponseModel.ok(workSheetHandleService.getWxWorkHandleInfoByAssetCode(methodParam, scanCode, workTypeId, needWorkInfo));
    }


    /**
     * 从客服请求创建工单
     *
     * @param methodParam
     * @param subWorkCode
     * @param pageType
     * @param workTypeId
     * @param params
     * @param workTemplateCode
     * @return
     */
    @RequestMapping(value = "/searchSingleWorkInfoFromCustomerRequest", method = RequestMethod.POST)
    public ResponseModel searchSingleWorkInfoFromCustomerRequest(MethodParam methodParam,
                                                                 @RequestParam(required = false) String subWorkCode,
                                                                 @RequestParam(required = false) String pageType,
                                                                 @RequestParam(required = false) String workTypeId,
                                                                 @RequestParam(required = false) Map<String, Object> params,
                                                                 @RequestParam(required = false) String workTemplateCode) {
        return ResponseModel.ok(workSheetHandleService.getSingleWorkInfoFromCustomerRequest(methodParam, params, subWorkCode, pageType, workTypeId, workTemplateCode));
    }
}
