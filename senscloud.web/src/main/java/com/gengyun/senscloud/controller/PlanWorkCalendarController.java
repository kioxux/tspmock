package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkAdd;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.PlanWorkCalendarService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "维保行事历")
@RestController
public class PlanWorkCalendarController {

    @Resource
    PlanWorkCalendarService planWorkCalendarService;

    @ApiOperation(value = "获取维保行事历模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_WORK_CALENDAR_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchPlanWorkCalendarPermission", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkCalendarPermission(MethodParam methodParam) {
        return ResponseModel.ok(planWorkCalendarService.getPlanWorkCalendarPermission(methodParam));
    }

    @ApiOperation(value = "查询维保行事历列表", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_WORK_CALENDAR_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "status", "pageSize", "pageNumber", "keywordSearch", "workTypeIds", "startDateSearch", "endDateSearch", "positionCode"})
    @RequestMapping(value = "/searchPlanWorkCalendarList", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkCalendarList(MethodParam methodParam, SearchParam param) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(planWorkCalendarService.getPlanWorkCalendarList(methodParam, param));
    }

    @ApiOperation(value = "计划行事历生成工单", notes = ResponseConstant.RSP_DESC_GENERATE_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_calendar_code"})
    @Transactional //支持事务
    @RequestMapping(value = "/generateWork", method = RequestMethod.POST)
    public ResponseModel generateWork(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_calendar_code) {
        planWorkCalendarService.generateWork(methodParam, work_calendar_code);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑计划行事历", notes = ResponseConstant.RSP_DESC_UPDATE_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "generate_time_point", "work_calendar_code", "occur_time", "deadline_time", "receive_user_id", "work_type_id", "priority_level", "auto_generate_bill", "group_id", "planJson"})
    @Transactional //支持事务
    @RequestMapping(value = "/modifyWork", method = RequestMethod.POST)
    public ResponseModel modifyWork(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PlanWorkAdd planWorkAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_calendar_code) {
        planWorkCalendarService.modifyWork(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "作废计划行事历", notes = ResponseConstant.RSP_DESC_REMOVE_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_calendar_code"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeWork", method = RequestMethod.POST)
    public ResponseModel removeWork(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_calendar_code) {
        planWorkCalendarService.removeWork(methodParam, work_calendar_code);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "作废选中计划行事历", notes = ResponseConstant.RSP_DESC_REMOVE_PLAN_CALENDAR_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "calendar_codes"})
    @Transactional //支持事务
    @RequestMapping(value = "/removePlanCalendarBySelect", method = RequestMethod.POST)
    public ResponseModel removePlanCalendarBySelect(MethodParam methodParam, SearchParam param) {
        planWorkCalendarService.deletePlanCalendarByIds(methodParam, param);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "根据id查询计划行事历详情", notes = ResponseConstant.RSP_DESC_GET_PLAN_WORK_CALENDAR_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_calendar_code"})
    @RequestMapping(value = "/searchPlanWorkCalendarInfo", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkCalendarInfo(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String work_calendar_code) {
        return ResponseModel.ok(planWorkCalendarService.getPlanWorkCalendarInfo(methodParam, work_calendar_code));
    }

    @ApiOperation(value = "导出选中维保计划", notes = ResponseConstant.RSP_DESC_EXPORT_PLAN_WORK_CALENDAR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "calendar_codes"})
    @RequestMapping(value = "/exportPlanWorkCalendar", method = RequestMethod.POST)
    public ModelAndView exportPlanWorkCalendar(MethodParam methodParam, SearchParam param) {
        return planWorkCalendarService.exportPlanWorkCalendar(methodParam, param);
    }

    @ApiOperation(value = "导出全部维保计划", notes = ResponseConstant.RSP_DESC_EXPORT_ALL_PLAN_WORK_CALENDAR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "status", "keywordSearch", "workTypeIds", "startDateSearch", "endDateSearch", "positionCode"})
    @RequestMapping(value = "/exportAllPlanWorkCalendar", method = RequestMethod.POST)
    public ModelAndView exportAllPlanWorkCalendar(MethodParam methodParam, SearchParam param) {
        return planWorkCalendarService.exportAllPlanWorkCalendar(methodParam, param);
    }
}
