package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorksModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.dynamic.BomWorksService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 备件工单管理
 */

@Api(tags = "备件工单管理")
@RestController
public class BomWorksController {
    @Resource
    BomWorksService bomWorksService;

    @ApiOperation(value = "获取备件工单模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_BOM_WORK_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchBomWorkListPermission", method = RequestMethod.POST)
    public ResponseModel searchBomWorkListPermission(MethodParam methodParam) {
        return ResponseModel.ok(bomWorksService.getBwListPermission(methodParam));
    }

    @ApiOperation(value = "分页查询备件工单列表", notes = ResponseConstant.RSP_DESC_SEARCH_BOM_WORKS_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "bomTypeIdSearch", "workTypeIdSearch",
            "keywordSearch", "beginTime", "finishedTime"})
    @RequestMapping(value = "/searchBomWorksList", method = RequestMethod.POST)
    public ResponseModel searchWorksList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                         @SuppressWarnings("unused") WorksModel worksModel) {
        return ResponseModel.ok(bomWorksService.getBomWorksList(methodParam, paramMap));
    }
}
