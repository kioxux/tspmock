package com.gengyun.senscloud.controller;

///**
// * 功能：监控设置
// * Created by Dong wudang on 2018/12/27.
// */
//@RestController
//@RequestMapping("/set_monitor")
public class SetMonitorController {

//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    SetMonitorService setMonitorService;
//
//    @Autowired
//    ResolvingMetaServiceImpl resolvingMetaService;
//
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /*监控配置页面*/
//    @MenuPermission("set_monitor")
//    @RequestMapping("/index")
//    public ModelAndView index(HttpServletRequest request , HttpServletResponse response){
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("set_monitor/index");
//        return  mv;
//    }
//
//    /**
//     * 监控配置新增页面
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping({"/add_set_monitor"})
//    public ModelAndView addSetMonitor(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("set_monitor/editor_set_monitor");
//    }
//
//    /**
//     * 监控配置编辑页面
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping({"/update_set_monitor"})
//    public ModelAndView updateSetMonitor(HttpServletRequest request, HttpServletResponse response) {
//        try {
//
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            if (StringUtil.isEmpty(id)) {
//                return new ModelAndView("set_monitor/editor_set_monitor").addObject("error", selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_FA));
//            }
//            MetadataMonitorsModel metadataMonitorsModel = setMonitorService.findSetMonitorListById(schema_name,Integer.parseInt(id));
//            return new ModelAndView("set_monitor/editor_set_monitor").addObject("monitorsData", JSON.toJSON(metadataMonitorsModel));
//        } catch (NumberFormatException e) {
//            return new ModelAndView("set_monitor/editor_set_monitor").addObject("error", e.getMessage());
//        }
//    }
//
//    /**
//     * 功能：新增监控配置
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping(value = "/add-set-monitor-list",method = RequestMethod.POST)
//    @Transactional
//    public ResponseModel addSetMonitorList(@RequestBody MetadataMonitorsModel metadataMonitorsModel, HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Timestamp create_time = new Timestamp(System.currentTimeMillis());
//            String create_account = loginUser.getAccount();
//            metadataMonitorsModel.setCreate_time(create_time);
//            metadataMonitorsModel.setCreate_user_account(create_account);
//            int doAdd = setMonitorService.addSetMonitorList(schema_name,metadataMonitorsModel);
//
//            if(doAdd>0){
//                try{
//                    result.setCode(StatusConstant.SUCCES_RETURN);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_CON_SU));
//                    String mqStr  =schema_name;
//                    rabbitTemplate.setExchange("senscloud:resolving:info:mq:exchange");
//                    rabbitTemplate.setRoutingKey("senscloud:resolving:info:mq:routing:key");
//                    Message message= MessageBuilder.withBody(mqStr.getBytes("UTF-8")).setDeliveryMode(MessageDeliveryMode.PERSISTENT)
//                            .build();
//                    rabbitTemplate.send(message);
//                }catch (Exception e){
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SYNCHRON_FA));
//                }
//            }else {
//                result.setCode(StatusConstant.UPDATE_ERROR);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_CON_DE));
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(StatusConstant.ERROR_RETURN);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_CON_DE));
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_CON_DE));
//        }
//    }
//
//    /**
//     * 功能：编辑监控配置
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping(value ="/edit-set-monitor-list",method = RequestMethod.POST)
//    @Transactional
//    public ResponseModel editSetMonitorList(@RequestBody MetadataMonitorsModel metadataMonitorsModel, HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            int doUpdate = setMonitorService.editSetMonitorList(schema_name,metadataMonitorsModel);
//            if(doUpdate>0){
//                try{
//                    result.setCode(StatusConstant.SUCCES_RETURN);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_CONF_SU));
//                    resolvingMetaService.refreshMonitorMeta(schema_name);
//                    String mqStr  =schema_name;
//                    rabbitTemplate.setExchange("senscloud:resolving:info:mq:exchange");
//                    rabbitTemplate.setRoutingKey("senscloud:resolving:info:mq:routing:key");
//                    Message message= MessageBuilder.withBody(mqStr.getBytes("UTF-8")).setDeliveryMode(MessageDeliveryMode.PERSISTENT)
//                            .build();
//                    rabbitTemplate.send(message);
//                }catch (Exception e){
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SYNCHRON_FA));
//                }
//            }else{
//                result.setCode(StatusConstant.UPDATE_ERROR);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_CONF_FA));
//            }
//            return result;
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_CONF_EXEC));
//        }
//    }
//
//
//    /**
//     * 功能：查询监控配置列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping("/find-set-monitor-list")
//    public String findSetMonitorList(HttpServletRequest request, HttpServletResponse response) {
//
//        JSONObject result = new JSONObject();
//        String condition = "";
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String monitor_name = request.getParameter("monitor_name");
//            if (!StringUtil.isEmpty(monitor_name)) {
//                condition += " and m.monitor_name like '%"+monitor_name+"%'";
//            }
//            int pageNumber = 1;
//            int pageSize = 15;
//            String strPage = request.getParameter("pageNumber");
//            String strPageSize = request.getParameter("pageSize");
//            try {
//                if (RegexUtil.isNumeric(strPage)) {
//                    pageNumber = Integer.valueOf(strPage);
//                }
//            } catch (Exception ex) {
//                pageNumber = 1;
//            }
//            try {
//                if (RegexUtil.isNumeric(strPageSize)) {
//                    pageSize = Integer.valueOf(strPageSize);
//                }
//            } catch (Exception ex) {
//                pageSize = 15;
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<MetadataMonitorsModel> metadataMonitorsModelList =  setMonitorService.findSetMonitorList(schema_name , condition, pageSize, begin);
//            int total =  setMonitorService.findSetMonitorListNum(schema_name , condition);
//            result.put("rows", metadataMonitorsModelList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //拖动换行
//    @MenuPermission("set_monitor")
//    @RequestMapping("/changeorder")
//    public ResponseModel chanageorder(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel model = new ResponseModel();
//
//        String jsondata = request.getParameter("jsondata");
//        List<MetadataMonitorsModel> MetadataMonitorlist = JSON.parseArray(jsondata, MetadataMonitorsModel.class);
//        System.out.println(jsondata);
//        try {
//            MetadataMonitorsModel wot;
//            String condition="";
//            String monitor_name = request.getParameter("monitor_name");
//            if (!StringUtil.isEmpty(monitor_name)) {
//                condition += " and m.monitor_name like '%"+monitor_name+"%'";
//            }
//            int pageNumber = 1;
//            int pageSize = 15;
//            String strPage = request.getParameter("pageNumber");
//            String strPageSize = request.getParameter("pageSize");
//            try {
//                if (RegexUtil.isNumeric(strPage)) {
//                    pageNumber = Integer.valueOf(strPage);
//                }
//            } catch (Exception ex) {
//                pageNumber = 1;
//            }
//            try {
//                if (RegexUtil.isNumeric(strPageSize)) {
//                    pageSize = Integer.valueOf(strPageSize);
//                }
//            } catch (Exception ex) {
//                pageSize = 15;
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<MetadataMonitorsModel> metadataMonitorsModelListnew =  setMonitorService.findSetMonitorList(schema_name , condition, pageSize, begin);
//            for (int i = 0; i < MetadataMonitorlist.size(); i++) {
//                wot = MetadataMonitorlist.get(i);
//                setMonitorService.editSetMonitorOrder(schema_name, wot.getId(),metadataMonitorsModelListnew.get(i).getOrder());
//            }
//            model.setCode(1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//            return model;
//        } catch (Exception ex) {
//            model.setCode(-1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            return model;
//        }
//
//
//    }
//
//    /**
//     * 删除监控项
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping("/del-set-monitor-list")
//    public ResponseModel delSetMonitorList(HttpServletRequest request, HttpServletResponse response,HttpSession session) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            if (StringUtil.isEmpty(id)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_FA));
//            }
//            int doUpdate = setMonitorService.delSetMonitorList(schema_name,Integer.parseInt(id));
//            if(doUpdate>0){
//                result.setCode(StatusConstant.SUCCES_RETURN);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_MONITOR_DEL_SU));
//                resolvingMetaService.refreshMonitorMeta(schema_name);
//            }else {
//                result.setCode(StatusConstant.UPDATE_ERROR);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_MONITOR_DEL_FA));
//            }
//            return result;
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_EXEC));
//        }
//    }
//
//    /**
//     * 启动禁用监控内
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("set_monitor")
//    @RequestMapping("/isuse-set-monitor")
//    public ResponseModel isuseSetMonitor(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String tip_text_true = "";
//            String tip_text_fail = "";
//
//            String id = request.getParameter("id");
//            String isuse = request.getParameter("isuse");
//            if (StringUtil.isEmpty(id)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_MONITOR_FA));
//            }
//            int doUpdate = setMonitorService.isuseSetMonitor(schema_name,Integer.parseInt(id) , Integer.parseInt(isuse));
//            if(isuse!=null && !isuse.equals("") && isuse.equals("0")){
//                tip_text_true = selectOptionService.getLanguageInfo(LangConstant.MSG_DISABLE_M_CONF);
//                tip_text_fail = selectOptionService.getLanguageInfo(LangConstant.MSG_DISABLE_M_CONF_FA);
//            }else if(isuse!=null && !isuse.equals("") && isuse.equals("1")){
//                tip_text_true = selectOptionService.getLanguageInfo(LangConstant.MSG_USE_M_CONF);
//                tip_text_fail = selectOptionService.getLanguageInfo(LangConstant.MSG_USE_M_CONF_FA);
//            }
//            if(doUpdate>0){
//                result.setCode(StatusConstant.SUCCES_RETURN);
//                result.setMsg(tip_text_true);
//            }else {
//                result.setCode(StatusConstant.UPDATE_ERROR);
//                result.setMsg(tip_text_fail);
//            }
//            return result;
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.error(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
}
