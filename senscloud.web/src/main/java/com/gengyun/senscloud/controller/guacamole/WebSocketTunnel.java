package com.gengyun.senscloud.controller.guacamole;

import com.alibaba.fastjson.JSONObject;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.guacamole.GuacamoleWebService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.gengyun.senscloud.util.AESUtils;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.guacamole.GuacamoleException;
import org.apache.guacamole.net.GuacamoleSocket;
import org.apache.guacamole.net.GuacamoleTunnel;
import org.apache.guacamole.net.InetGuacamoleSocket;
import org.apache.guacamole.net.SimpleGuacamoleTunnel;
import org.apache.guacamole.protocol.ConfiguredGuacamoleSocket;
import org.apache.guacamole.protocol.GuacamoleClientInformation;
import org.apache.guacamole.protocol.GuacamoleConfiguration;
import org.apache.guacamole.websocket.GuacamoleWebSocketTunnelEndpoint;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.Map;

/**
 * Guacamole websocket连接
 */
@ServerEndpoint(value = "/webSocket", subprotocols = "guacamole")
@Component
public class WebSocketTunnel extends GuacamoleWebSocketTunnelEndpoint {

    @Resource
    private GuacamoleWebService guacamoleWebService;
    @Resource
    private SystemConfigService systemConfigService;
    @Resource
    private SelectOptionService selectOptionService;

//    public WebSocketTunnel() {
//        guacamoleWebService = SpringContextHolder.getBean("guacamoleWebServiceImpl");
//        systemConfigService = SpringContextHolder.getBean("systemConfigServiceImpl");
//        selectOptionService = SpringContextHolder.getBean("selectOptionServiceImpl");
//    }

    @Override
    protected GuacamoleTunnel createTunnel(Session session, EndpointConfig endpointConfig) throws GuacamoleException {
        String hostname = "192.168.31.83";
        int port = 4822;

        Map<String, List<String>> requestParameterMap = session.getRequestParameterMap();
        String protocol = "";
        String host_name = "";
        String ports = "";
        String username = "";
        String password = "";
        MethodParam methodParam = new MethodParam();
        if (requestParameterMap.containsKey("schema_name")) {
            List<String> schema_names = requestParameterMap.get("schema_name");
            String s = schema_names.get(0);
            String schema_name = s.split("\\?")[0];
            methodParam.setSchemaName(schema_name);
        } else {
            return null;
        }

        if (requestParameterMap.containsKey("position_code")) {
            List<String> position_codes = requestParameterMap.get("position_code");
            String s = position_codes.get(0);
            String position_code = s.split("\\?")[0];
            List<Map<String, String>> list = guacamoleWebService.getAssetPositionGuacamole(methodParam, position_code);
            if(RegexUtil.optIsPresentList(list)){
                for (Map<String, String> data : list) {
                    String name = RegexUtil.optStrOrNull(data.get("name"));
                    switch (name){
                        case "port":
                            ports = data.get("code");
                            break;
                        case "protocol":
                            protocol = data.get("code");
                            break;
                        case "password":
                            password = data.get("code");
                            break;
                        case "username":
                            username = data.get("code");
                            break;
                        case "hostname":
                            host_name = data.get("code");
                            break;
                        default: break;
                    }
                }
            }else{
                return null;
            }
            password = AESUtils.decryptAES(password);
        }
        String sysValue = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "guacamole_ip");
        if (RegexUtil.optIsPresentStr(sysValue)) {
            JSONObject jsonObject = JSONObject.parseObject(sysValue);
            hostname = jsonObject.getString("host");
            port = jsonObject.getInteger("port");
        }
        GuacamoleConfiguration configuration = new GuacamoleConfiguration();
        configuration.setProtocol(protocol);//
        configuration.setParameter("port", ports);//
        configuration.setParameter("hostname", host_name);//
        configuration.setParameter("username", username);//
        configuration.setParameter("password", password);//
        configuration.setParameter("ignore-cert", "true");//
        configuration.setParameter("security", "nla");//

        configuration.setParameter("read-only", "false");//鼠标禁用启用

//        configuration.setParameter("remote-app", "||Notepad");//指要在远程桌面上启动的RemoteApp
//        configuration.setParameter("remote-app-dir", "C:\\Windows\\System32\\notepad.exe");//远程应用程序的工作目录，若果未配置远程应用，就会忽略此参数
//        configuration.setParameter("remote-app-args", "");//远程应用程序的命令行参数，若果未配置远程应用，就会忽略此参数
        GuacamoleClientInformation information = new GuacamoleClientInformation();
        information.setOptimalScreenWidth(1920);
        information.setOptimalScreenHeight(1080);
        GuacamoleSocket socket = new ConfiguredGuacamoleSocket(new InetGuacamoleSocket(hostname, port), configuration,information);
        //GuacamoleSocket socket = new ConfiguredGuacamoleSocket(new InetGuacamoleSocket(hostname, port), configuration);
        GuacamoleTunnel tunnel = new SimpleGuacamoleTunnel(socket);
        return tunnel;
    }

}
