package com.gengyun.senscloud.controller.poc;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.asset.AssetCategoryService;
import com.gengyun.senscloud.service.asset.AssetMonitorValueService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "星巴克poc")
@RestController
public class StarbucksPocController {

    @Resource
    AssetCategoryService assetCategoryService;

    @Resource
    AssetMonitorValueService assetMonitorValueService;

    @ApiOperation(value = "获取带有监控项的设备类型列表", notes = ResponseConstant.RSP_DESC_SEARCH_IOT_ASSET_CATEGORY_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token"})
    @RequestMapping(value = "/searchIotAssetCategoryLists", method = RequestMethod.POST)
    public ResponseModel searchIotAssetCategoryLists(MethodParam methodParam) {
        return ResponseModel.ok(assetCategoryService.searchIotAssetCategoryLists(methodParam));
    }

    @ApiOperation(value = "通过设备位置、设备类型获取设备以及设备监控信息", notes = ResponseConstant.RSP_DESC_SEARCH_IOT_ASSET_AND_POINTS_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "position_code", "category_id"})
    @RequestMapping(value = "/searchIotAssetPoints", method = RequestMethod.POST)
    public ResponseModel searchIotAssetPoints(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(assetMonitorValueService.searchIotAssetPoints(methodParam, paramMap));
    }

    @ApiOperation(value = "分页查询设备实时监测数据", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_CURRENT_MONITOR_LIST_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "asset_code", "keywordSearch"})
    @RequestMapping(value = "/searchAssetCurrentMonitorListPage", method = RequestMethod.POST)
    public ResponseModel searchAssetCurrentMonitorListPage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(assetMonitorValueService.searchAssetCurrentMonitorListPage(methodParam, paramMap));
    }

    @ApiOperation(value = "分页查询设备历史监测数据", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_HISTORY_MONITOR_LIST_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "asset_code", "keywordSearch","beginDateSearch","endDateSearch"})
    @RequestMapping(value = "/searchAssetHistoryMonitorListPage", method = RequestMethod.POST)
    public ResponseModel searchAssetHistoryMonitorListPage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(assetMonitorValueService.searchAssetHistoryMonitorListPage(methodParam, paramMap));
    }

    @ApiOperation(value = "更新设备实时信息", notes = ResponseConstant.RSP_DESC_UPDATE_ASSET_MONITOR_CURRENT_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "asset_id", "monitor_name", "monitor_value"})
    @RequestMapping(value = "/updateAssetMonitorCurrentInfo", method = RequestMethod.POST)
    public ResponseModel updateAssetMonitorCurrentInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(assetMonitorValueService.updateAssetMonitorCurrentInfo(methodParam, paramMap));
    }
}
