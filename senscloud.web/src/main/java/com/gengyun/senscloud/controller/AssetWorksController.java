package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorksModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.dynamic.AssetWorksService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 设备工单管理
 */

@Api(tags = "设备工单管理")
@RestController
public class AssetWorksController {
    @Resource
    AssetWorksService assetWorksService;

    @ApiOperation(value = "获取设备工单模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_WORK_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetWorkListPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetWorkListPermission(MethodParam methodParam) {
        return ResponseModel.ok(assetWorksService.getAwListPermission(methodParam));
    }

    @ApiOperation(value = "分页查询设备工单列表", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_WORKS_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "workTypeIdSearch", "statusSearch",
            "keywordSearch", "beginTime", "finishedTime"})
    @RequestMapping(value = "/searchAssetWorksList", method = RequestMethod.POST)
    public ResponseModel searchWorksList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                         @SuppressWarnings("unused") WorksModel worksModel) {
        return ResponseModel.ok(assetWorksService.getAssetWorksList(methodParam, paramMap));
    }
}
