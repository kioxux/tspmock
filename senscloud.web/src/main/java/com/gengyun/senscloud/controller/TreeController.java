package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.DataPermissionAllOrSelf;
//import com.gengyun.senscloud.common.DataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Constants;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.*;
//
///**
// * Created by Administrator on 2018/8/23.
// */
//@RestController
//@RequestMapping("/tree")
public class TreeController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    AssetPositionService assetPositionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    DataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    PlanWorkService planWorkService;
//
//
//    /**
//     * 设备监控-组织、位置、监控设备树状数据
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/templetTree", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public String getBomTree(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();//获取配置数据库地址
//        try {
//            List<JsTreeModel> jsTreeModelList = new ArrayList<JsTreeModel>();
//            List<AssetPosition> assetPositions = null; //position列表
//            String id = request.getParameter("id");//获取设备编号
//            String has_root = request.getParameter("has_root");//是否添加根节点
//            boolean isRoot = true; //true:获取根目录 false:子级目录
//            if (id != null && !id.equals("#") && !id.equals("root")) {
//                isRoot = false;
//                id = id.substring(0, id.length() - 13);
//            }else if(id.equals("#") && "true".equals(has_root)){
//                JsTreeModel rootNode = new JsTreeModel();
//                rootNode.setId("root");
//                rootNode.setIcon("/image/tree/workshop.png");
//                rootNode.setSelected(false);
//                rootNode.setChildren(true);
//                rootNode.setText(selectOptionService.getLanguageInfo(LangConstant.ALL_CUSTOMER));
//                jsTreeModelList.add(rootNode);
//                return net.sf.json.JSONArray.fromObject(jsTreeModelList).toString();
//            }else{
//                id = "0";   //0表示根节点
//            }
//            //判断节点是组织还是position
//            if(!id.startsWith("P")){
//                User user = authService.getLoginUser(request);//用户身份
//                Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "asset_monitor");//查询用户的角色歌权限身份
//                //查询下级组织
//                List<Facility> facilityList = facilitiesService.getFcilityRootByUser(schema_name, canSeeAllFacility, user, Integer.valueOf(id), true);
//                if (facilityList != null && facilityList.size() > 0) {
//                    for (Facility facility : facilityList) {
//                        if (facility != null)
//                            //添加组织节点到jsTree
//                            handlerJsTreeModel(jsTreeModelList, String.valueOf(facility.getId()), facility.getTitle(), Constants.TREE_NODE_TYPE_FACILITY, 1);
//                    }
//                }
//                //查询组织下的position（一级目录不可能有位置，无需查询位置列表）
//                if (!isRoot)
//                    assetPositions = assetPositionService.findPositionByFacilityId(schema_name, Integer.valueOf(id), true);
//            }else{
//                //查询位置的子级位置
//                if (!isRoot)
//                    assetPositions = assetPositionService.findChildAssetPositionByPositionCode(schema_name, id, true);
//            }
//            if(assetPositions != null && assetPositions.size() > 0){
//                for(AssetPosition assetPosition : assetPositions){
//                    if(assetPosition != null)
//                        //添加位置节点到jsTree
//                        handlerJsTreeModel(jsTreeModelList, String.valueOf(assetPosition.getPosition_code()), assetPosition.getPosition_name(), Constants.TREE_NODE_TYPE_POSITION, 1);
//                }
//            }
//            if (!isRoot) {
//                //获取节点上的设备
//                List<Asset> assetList = assetDataServiceV2.searchAssetTree(schema_name, id);
//                if (assetList != null && assetList.size() > 0) {
//                    for (Asset asset : assetList) {
//                        if (asset != null) {
//                            int iotStatus = asset.getIot_status();
//                            String iotText = "";
//                            if(iotStatus == 1)
//                                iotText = "(无物联)";
//                            else if(iotStatus == 2)
//                                iotText = "(在线)";
//                            else if(iotStatus == 3)
//                                iotText = "(离线)";
//
//                            asset.setStrname(asset.getStrname() + iotText);
//                            //添加设备节点到jsTree
//                            handlerJsTreeModel(jsTreeModelList, asset.getStrcode(), asset.getStrname(), Constants.TREE_NODE_TYPE_ASSET, 0);
//                        }
//                    }
//                }
//            }
//            net.sf.json.JSONArray jsTreejson = net.sf.json.JSONArray.fromObject(jsTreeModelList);
//            return jsTreejson.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 树状-位置数据
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/templetFacilityTree", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public String getFacilityTree(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();//获取配置数据库地址
//        User user = authService.getLoginUser(request);//用户身份
//        List<AssetPosition> assetPositions = null; //position列表
//        List<JsTreeModel> jsTreeModelList = new ArrayList<JsTreeModel>();
//        try {
//            String id = request.getParameter("id");//获取设备编号
//            String menuKey = request.getParameter("menuKey");//查询用户的角色歌权限身份
//            String ignorePosition = request.getParameter("ignorePosition");//是否忽略position数据
//            String has_root = request.getParameter("has_root");//是否添加根节点
//            boolean isRoot = true; //true:获取根目录 false:子级目录
//            if (id != null && !id.equals("#") && !id.equals("root")) {
//                isRoot = false;
//                id = id.substring(0, id.length() - 13);
//            }else if(id.equals("#") && "true".equals(has_root)){
//                jsTreeModelList.add(new JsTreeModel("root", selectOptionService.getLanguageInfo(LangConstant.ALL_CUSTOMER), true, "/image/tree/workshop.png", false, null));
//                return net.sf.json.JSONArray.fromObject(jsTreeModelList).toString();
//            }else {
//                id = "0";   //0表示根节点
//            }
//            //判断节点是组织还是position
//            if(!id.startsWith("P")) {
//                Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), menuKey);//查询用户的角色歌权限身份
//                //查询下级组织
//                List<Facility> facilityList = facilitiesService.getFcilityRootByUser(schema_name, canSeeAllFacility, user, Integer.valueOf(id), false);
//                if (facilityList != null && facilityList.size() > 0) {
//                    for (Facility facility : facilityList) {
//                        if (facility != null)
//                            //添加组织节点到jsTree
//                            handlerJsTreeModel(jsTreeModelList, String.valueOf(facility.getId()), facility.getTitle(), Constants.TREE_NODE_TYPE_FACILITY, 1);
//                    }
//                }
//                //查询组织下的position（一级目录不可能有位置，无需查询位置列表）
//                if (!isRoot && !"true".equals(ignorePosition))
//                    assetPositions = assetPositionService.findPositionByFacilityId(schema_name, Integer.valueOf(id), false);
//            }else{
//                //查询位置的子级位置
//                if (!isRoot && !"true".equals(ignorePosition))
//                    assetPositions = assetPositionService.findChildAssetPositionByPositionCode(schema_name, id, false);
//            }
//            if(assetPositions != null && assetPositions.size() > 0){
//                for(AssetPosition assetPosition : assetPositions){
//                    if(assetPosition != null)
//                        //添加位置节点到jsTree
//                        handlerJsTreeModel(jsTreeModelList, String.valueOf(assetPosition.getPosition_code()), assetPosition.getPosition_name(), Constants.TREE_NODE_TYPE_POSITION, assetPosition.getChild_count());
//                }
//            }
//            net.sf.json.JSONArray jsTreejson = net.sf.json.JSONArray.fromObject(jsTreeModelList);
//            return jsTreejson.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 设备位置树（带数据权限控制）
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/templetPositionTree", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public String getPositionTree(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();//获取配置数据库地址
//        User user = authService.getLoginUser(request);//用户身份
//        List<AssetPosition> assetPositions = null; //position列表
//        List<JsTreeModel> jsTreeModelList = new ArrayList<JsTreeModel>();
//        try {
//            String id = request.getParameter("id");//获取设备编号
//            String rootPermission = request.getParameter("menuKey");//用户权限key
//            if (id != null && id.equals("root")) {
//                id = "0";   //0表示根节点
//            }else if(id.equals("#")){
//                jsTreeModelList.add(new JsTreeModel("root", selectOptionService.getLanguageInfo(LangConstant.ALL_POS), true, "/image/tree/workshop.png", false, Constants.TREE_NODE_TYPE_POSITION));
//                return net.sf.json.JSONArray.fromObject(jsTreeModelList).toString();
//            }else{
//                id = id.substring(0, id.length() - 13);
//            }
//            DataPermissionAllOrSelf dataPermissionAllOrSelf = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusiness(schema_name, user, rootPermission);
//            if (dataPermissionAllOrSelf == DataPermissionAllOrSelf.All_Facility) {
//                //查询所有子级位置
//                assetPositions = assetPositionService.findChildAssetPositionByPositionCode(schema_name, id, false);
//            } else {
//                //查询拥有权限的子级位置
//                assetPositions = assetPositionService.findChildAssetPositionByPositionCodeAndUserId(schema_name, id, user.getId());
//            }
//            if(assetPositions != null && assetPositions.size() > 0){
//                for(AssetPosition assetPosition : assetPositions){
//                    if(assetPosition != null)
//                        //添加位置节点到jsTree
//                        handlerJsTreeModel(jsTreeModelList, String.valueOf(assetPosition.getPosition_code()), assetPosition.getPosition_name(), Constants.TREE_NODE_TYPE_POSITION, assetPosition.getChild_count());
//                }
//            }
//            net.sf.json.JSONArray jsTreejson = net.sf.json.JSONArray.fromObject(jsTreeModelList);
//            return jsTreejson.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 设备位置树（带数据权限控制）
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/templetPositionTreeByKeyword", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public String getPositionTreeByKeyword(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();//获取配置数据库地址
//        User user = authService.getLoginUser(request);//用户身份
//        List<AssetPosition> assetPositions = null; //position列表
//        try {
//            String rootPermission = request.getParameter("menuKey");//用户权限key
//            String keyword = request.getParameter("keyword");//位置关键词模糊查询
//            DataPermissionAllOrSelf dataPermissionAllOrSelf = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusiness(schema_name, user, rootPermission);
//            keyword = "%" + keyword + "%";
//            if (dataPermissionAllOrSelf == DataPermissionAllOrSelf.All_Facility) {
//                //查询所有子级位置
//                assetPositions = assetPositionService.findChildAssetPositionByKeyword(schema_name, keyword);
//            } else {
//                //查询拥有权限的子级位置
//                assetPositions = assetPositionService.findChildAssetPositionByKeywordAndUserId(schema_name, keyword, user.getId());
//            }
//            Map<String, List<AssetPosition>> map = new HashMap<>();
//            JSONArray jsTreeArray = new JSONArray();
//            if(assetPositions != null){
//                for(AssetPosition assetPosition : assetPositions){
//                    if(map.containsKey(assetPosition.getParent_code())){
//                        map.get(assetPosition.getParent_code()).add(assetPosition);
//                    }else{
//                        map.put(assetPosition.getParent_code(), new ArrayList<>(Arrays.asList(assetPosition)));
//                    }
//                }
//                jsTreeArray = getTreeData(map, "0");
//            }
//            return jsTreeArray.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 设备组织树（带数据权限控制）-不关联查询上级，树只显示一层
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/templetFacilityTreeByKeyword", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public String getFacilityTreeByKeyword(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();//获取配置数据库地址
//        User user = authService.getLoginUser(request);//用户身份
//        JSONArray jsTreeArray = new JSONArray();
//        try {
//            String rootPermission = request.getParameter("menuKey");//用户权限key
//            String keyword = request.getParameter("keyword");//位置关键词模糊查询
//            String condition = "";
//            if(StringUtils.isNotBlank(keyword))
//                condition = String.format(" AND f.short_title like '%s' ", "%" + keyword + "%");
//
//            List<Facility> facilities = assetPositionService.getFacilityByCondition(schema_name, condition);
//            if (facilities != null && facilities.size() > 0){
//                DataPermissionAllOrSelf dataPermissionAllOrSelf = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusiness(schema_name, user, rootPermission);
//                for (Facility facility : facilities) {
//                    if (dataPermissionAllOrSelf != DataPermissionAllOrSelf.All_Facility) {
//                        //获取用户拥有权限的组织列表
//                        LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                        if (facilityList.size() > 0) {
//                            if(!facilityList.containsKey(String.valueOf(facility.getId())))
//                                break;
//                        }else{
//                            break;
//                        }
//                    }
//                    JSONObject json = new JSONObject();
//                    json.put("id", String.valueOf(facility.getId()) + System.currentTimeMillis());
//                    json.put("text", facility.getShort_title());
//                    json.put("state", "open");
//                    json.put("icon", "/image/tree/workshop.png");
//                    jsTreeArray.add(json);
//                }
//            }
//            return jsTreeArray.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 处理树状结构数据的父子级关系
//     * @param map
//     * @param parentCode
//     * @return
//     */
//    private JSONArray getTreeData(Map<String, List<AssetPosition>> map, String parentCode){
//        JSONArray jsTreeArray = new JSONArray();
//        if(map != null && map.containsKey(parentCode)){
//            for(AssetPosition assetPosition: map.get(parentCode)){
//                JSONObject json = new JSONObject();
//                json.put("id", assetPosition.getPosition_code() + System.currentTimeMillis());
//                json.put("text", assetPosition.getPosition_name());
//                json.put("state", "open");
//                json.put("icon", "/image/tree/workshop.png");
//                json.put("children", getTreeData(map, assetPosition.getPosition_code()));
//                jsTreeArray.add(json);
//            }
//        }
//        return jsTreeArray;
//    }
//
//    /**
//     * 封装jsree节点
//     * @param jsTreeModelList
//     * @param id
//     * @param text
//     * @param nodeType   1、组织 2、位置 3、设备
//     * @param childCount 子节点数量
//     */
//    private void handlerJsTreeModel(List<JsTreeModel> jsTreeModelList, String id, String text, int nodeType, Integer childCount){
//        JsTreeModel jsTreeModel = new JsTreeModel();
//        jsTreeModel.setId(id + System.currentTimeMillis());//位置、设备可能出现在多个组织下，加时间戳防止jsTree节点ID重复
//        jsTreeModel.setText(text);
//        jsTreeModel.setNodeType(nodeType);
//        if(nodeType == Constants.TREE_NODE_TYPE_ASSET){
//            jsTreeModel.setIcon("/image/tree/asset.png");
//            jsTreeModel.setChildren(false);
//        }else{
//            jsTreeModel.setIcon("/image/tree/workshop.png");
//            jsTreeModel.setChildren(childCount== null || childCount.intValue() == 0?false:true);
//        }
//        jsTreeModelList.add(jsTreeModel);
//    }
}
