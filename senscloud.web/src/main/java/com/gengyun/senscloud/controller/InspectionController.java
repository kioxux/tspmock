package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.view.InspectionDataExportView;
//import org.apache.commons.lang.NullArgumentException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
////巡检查询，含地图、列表，巡检点等功能// 准备删除 yzj  2019-08-04
//@RestController
//@RequestMapping("/inspection")
public class InspectionController {

//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    InspectionService inspectionService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AssetDutyManService assetDutyManService;
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @RequestMapping("/listmap")
//    public ModelAndView list(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("inspection/list");
//    }
//
//    /**
//     * 按日期，获取位置的所有巡检点（含巡检个数）
//     */
//    @RequestMapping("/find_inspection_by_facility")
//    public ResponseModel queryInspectionList(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            Integer facilityId = Integer.parseInt(request.getParameter("facility_id"));
//            String date = request.getParameter("inspection_date");
//            Timestamp beginTime = Timestamp.valueOf(date + " 00:00:00");
//            Timestamp endTime = Timestamp.valueOf(date + " 23:59:59");
//
//            List<InspectionAreaData> inspectionList = facilitiesService.GetInspectionListByFacilityId(schema, facilityId, beginTime, endTime);
//            return ResponseModel.ok(inspectionList);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//
//    /**
//     * 获得所有的能查看巡检的位置
//     */
//    @RequestMapping("/find_all_inspection_facilities")
//    public ResponseModel GetFacilitiesByUser(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//
//            Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "inspection");
//            List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//
//            if (isAllFacility) {
//                return ResponseModel.ok(facilitiesList);
//            } else {
//                //定义返回
//                List<Facility> result = new ArrayList<>();
//                //获取用户所在的位置，可能多个
//                User userInfo = userService.getUserByAccount(schema_name, user.getAccount());
//
//                LinkedHashMap<String, IFacility> userFacilityList = userInfo.getFacilities();
//                if (userFacilityList != null && !userFacilityList.isEmpty()) {
//                    for (String key : userFacilityList.keySet()) {
//                        for (Facility tempList : facilitiesList) {
//                            if (tempList != null && key.equals(String.valueOf(tempList.getId()))) {
//                                result.add(tempList);
//                            }
//                        }
//                    }
//                }
//                return ResponseModel.ok(result);
//            }
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    //查找巡检列表
//    @RequestMapping("/get_inspection_list_by_date_and_areacode")
//    public ResponseModel getInspectionDetailListData(HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            //根据日期和巡检点，查找巡检
//            String insdate = request.getParameter("inspection_date");
//            String areaCode = request.getParameter("area_code");
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("inspection_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("inspection_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            String condition;
//            if (isAllFacility) {
//                condition = " ins.area_code='" + areaCode + "' ";
//            } else if (isSelfFacility) {
//                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        facilityIds += facilityList.get(key).getId() + ",";
//                    }
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                    condition = " ins.area_code='" + areaCode + "' and ins.facility_id in (" + facilityIds + ") ";
//                } else {
//                    condition = " ins.area_code='" + areaCode + "' ins.inspection_account='" + user.getAccount() + "' ";    //没有用户所属位置，则按个人权限查询
//                }
//            } else {
//                condition = " ins.area_code='" + areaCode + "' ins.inspection_account='" + user.getAccount() + "' ";
//            }
//
//            //获取巡检项
//            List<InspectionData> InspectionDataList = inspectionService.InspectionDataList(schema_name, condition, insdate, 100, 0);
//
//            if (InspectionDataList != null && InspectionDataList.size() > 0) {
//                for (int i = 0; i < InspectionDataList.size(); i++) {
//                    InspectionDataList.get(i).setRepairDataList(repairService.getRepairListByFromCode(schema_name, InspectionDataList.get(i).getInspection_code()));
//                }
//            }
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_SUCC));
//            result.setContent(InspectionDataList);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));
//            //系统错误日志
//            //logService.AddLog(schema_name, "system", "repair_list", "巡检列表获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//
//    //list列表
//    @RequestMapping("/list")
//    public ModelAndView indexlist(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        Company company = authService.getCompany(request);
//        User loginUser = AuthService.getLoginUser(request);
//        Boolean canExportData = false;
//        String facilitiesSelect = "";
//        Boolean canInspection = false;//是否可以对巡检列表作废
//        Boolean inspectionAddPc = false;
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "inspection");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("inspection_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("inspection_export")) {
//                        canExportData = true;
//                    } else if (functionData.getFunctionName().equals("inspection_invalid")) {
//                        canInspection = true;
//                    } else if (functionData.getFunctionName().equals("inspection_do_pc")) {
//                        inspectionAddPc = true;
//                    }
//                    if (isAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            if (AuthService.isUserHasRoleId(request, UserRole.ROLE_ID_SYSTEM_ADMIN, UserRole.ROLE_ID_COMPANY_ADMIN)) {
//                isAllFacility = true;
//            }
//
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        return new ModelAndView("inspection/indexlist")
//                .addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("canExportData", canExportData)
//                .addObject("user_facility_id", company.getFacilityId())
//                .addObject("canInspection", canInspection)
//                .addObject("inspectionAddPc", inspectionAddPc);
//    }
//
//
//    //查找巡检列表
//    @RequestMapping("/get_inspection_indexlist_by_item")
//    public String getInspectionDetailindexListData(HttpServletRequest request) {
//
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            org.json.JSONObject result = new org.json.JSONObject();
//            //根据日期和巡检点，查找巡检
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String facilities = request.getParameter("facilities").trim();
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("inspection_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("inspection_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            String condition;
//            if (!facilities.isEmpty() && !facilities.isEmpty()) {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilities, facilityList);
//                condition = " ins.facility_id in (" + facilities + subFacility + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = " 1=1 ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = " ins.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition = " upper(ins.inspection_account)=upper('" + user.getAccount() + "') ";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    condition = " upper(ins.inspection_account)=upper('" + user.getAccount() + "') ";
//                }
//            }
//
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(ins.inspection_code) like upper('%" + keyWord + "%') or upper(ins.area_code) like upper('%" + keyWord + "%') or upper(lsx.area_name) like upper('%" + keyWord + "%') or u.username like '%" + keyWord + "%' or ins.inspection_account like '%" + keyWord + "%' )";
//            }
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and ins.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and ins.createtime <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //获取巡检项
//            List<InspectionData> InspectionDataList = inspectionService.InspectionDataListweb(schema_name, condition, pageSize, begin);
//
//            if (InspectionDataList != null && InspectionDataList.size() > 0) {
//                for (int i = 0; i < InspectionDataList.size(); i++) {
//                    InspectionDataList.get(i).setRepairDataList(repairService.getRepairListByFromCode(schema_name, InspectionDataList.get(i).getInspection_code()));
//                }
//            }
//
//            int total = inspectionService.InspectionDataListwebtotal(schema_name, condition);
//            result.put("rows", InspectionDataList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//
//            return "{}";
//        }
//    }
//
//
//    //巡检导出
//    @RequestMapping("/inspection-export")
//    public ModelAndView ExportInspection(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//
//            org.json.JSONObject result = new org.json.JSONObject();
//            //根据日期和巡检点，
//
//            int pageSize = 50000;//Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = 1;//Integer.parseInt(request.getParameter("pageNumber"));
//            String facilities = request.getParameter("facilities").trim();
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("inspection_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("inspection_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            String condition;
//            if (!facilities.isEmpty() && !facilities.equals("") && !facilities.equals("0")) {
//                condition = " ins.facility_id in (" + facilities + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = " 1=1 ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = "  ins.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition = "  upper(ins.inspection_account)=upper('" + user.getAccount() + "') ";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    condition = "  upper(ins.inspection_account)=upper('" + user.getAccount() + "') ";
//                }
//            }
//
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(ins.inspection_code) like upper('%" + keyWord + "%') or upper(ins.area_code) like upper('%" + keyWord + "%') or upper(lsx.area_name) like upper('%" + keyWord + "%') or u.username like '%" + keyWord + "%' or ins.inspection_account like '%" + keyWord + "%' )";
//            }
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and ins.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and ins.createtime <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //获取巡检项
//            List<InspectionData> InspectionDataList = inspectionService.InspectionDataListweb(schema_name, condition, pageSize, begin);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("InspectionList", InspectionDataList);
//            map.put("selectOptionService", selectOptionService);
//            InspectionDataExportView excelView = new InspectionDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//
//    @RequestMapping("/find-Inspection-detail")
//    public ResponseModel querySingleSpotcheck(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        User user = authService.getLoginUser(request);
//        String account = user.getAccount();
//        try {
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("inspection_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("inspection_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            String code = request.getParameter("code");
//            InspectionData data = inspectionService.InspectionDatabycode(schema_name, code);
//            if (data != null) {
//                data.setRepairDataList(repairService.getRepairListByFromCode(schema_name, code));
//            }
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(data);
//            if (isAllFacility) {
//                return result;
//            } else if (isSelfFacility) {
//                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        if (facilityList.get(key).getId() == data.getFacility_id()) {
//                            return result;
//                        }
//                    }
//                }
//            }
//            if (account.equalsIgnoreCase(data.getInspection_account())) {
//                return result;
//            }
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_AU_INS));
//
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 功能：废除巡检单据
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/cancle-Inspection")
//    public ResponseModel cancelInspection(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String code = request.getParameter("code");
//            int doCancle = inspectionService.cancelInspection(schema_name, code);
//            if (doCancle > 0) {
//                result.setCode(1);
//                result.setMsg("");
//                result.setContent(doCancle);
//            } else {
//                result.setCode(0);
//                result.setMsg("");
//                result.setContent(doCancle);
//            }
//            return result;
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/get-inspection-info")
//    public ResponseModel get_inspection_info(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            //获取巡检项
//            List<InspectionItemData> inspectionItemData = inspectionService.inspectionItemData(schema_name);
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(inspectionItemData);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    //巡检结果提交
//    @RequestMapping("get_inspection_add")
//    @Transactional //支持事务
//    public ResponseModel getInspectionAdd(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();//从配置文件读取数据库配置
//        String account = authService.getLoginUser(request).getAccount();
//        ResponseModel result = new ResponseModel();
//        try {
//            String area_code = request.getParameter("areaCode");//巡检区域编码
//            String itemList = request.getParameter("itemListWord");
//            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
//            String inspectioncode = request.getParameter("inspectioncode");
//            String assetList = request.getParameter("assetListLast");
//            System.out.println("@@@@@@@@@@@" + area_code);
//            //获取当前设备
//            InspectionAreaData inspectionDataOn = inspectionService.InspectionAreaDatabyarea(schema_name, area_code);
////            InspectionAreaData inspectionDataOn = inspectionService.InspectionAreaDatabyarea(schema_name, "000001");
//
//
//            if (inspectionDataOn == null || inspectionDataOn.getArea_code().isEmpty()) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_UPDATE_ERROR));
//                return result;
//            }
//            InspectionData inspectionData = new InspectionData();
//            int resultOption = 0;
//            //订单号不存在，则是新增，生成新订单编号
//            if (inspectioncode == null || inspectioncode.isEmpty()) {
//                inspectioncode = serialNumberService.generateMaxBusinessCode(schema_name, "inspection");
//                inspectionData.setInspection_code(inspectioncode);//
//                inspectionData.setArea_code(area_code);
//                inspectionData.setInspection_account(account);//----------------
//                inspectionData.setCreate_user_account(account);//---------------
//                inspectionData.setStatus(60);//-------------
//                inspectionData.setFault_number(faultNumber);
//                inspectionData.setFacility_id(inspectionDataOn.getFacility_id());//------------------------
//                inspectionData.setBeginTime(new Timestamp(System.currentTimeMillis()));
//                inspectionData.setInspection_time(new Timestamp(System.currentTimeMillis()));//-----------
//                inspectionData.setCreatetime(new Timestamp(System.currentTimeMillis()));//------
//                inspectionData.setInspection_result(itemList);
//                inspectionData.setSchema_name(schema_name);
//                resultOption = inspectionService.insertInspectionData(schema_name, inspectionData);
//            }
////            else {
////                inspectionData.setInspection_code(inspectioncode);
////                inspectionData.setArea_code(area_code);
////                inspectionData.setInspection_account(account);
////                inspectionData.setStatus(60);
////                inspectionData.setFault_number(faultNumber);
////                inspectionData.setBeginTime(new Timestamp(System.currentTimeMillis()));
////                inspectionData.setInspection_time(new Timestamp(System.currentTimeMillis()));
////                inspectionData.setCreatetime(new Timestamp(System.currentTimeMillis()));
////                inspectionData.setInspection_result(itemList);
////                inspectionData.setSchema_name(schema_name);
////                resultOption = inspectionService.updateInspectionData(inspectionData);
////            }
//            if (resultOption > 0) {
//                //assetList
//                if (assetList != null && !assetList.equals("")) {
//                    net.sf.json.JSONArray jsonasset = net.sf.json.JSONArray.fromObject(assetList);
////将检测类型不为2的且未选中的汇总作为上报问题
//                    net.sf.json.JSONArray itemListjson = net.sf.json.JSONArray.fromObject(itemList);
//                    String fault_note = selectOptionService.getLanguageInfo( LangConstant.MSG_INS_PRO_ITEM);
//                    for (int i = 0; i < itemListjson.size(); i++) {
//                        net.sf.json.JSONObject ss = itemListjson.getJSONObject(i);
//                        String isCheck = itemListjson.getJSONObject(i).getString("isCheck");
//                        String item_name = itemListjson.getJSONObject(i).getString("item_name");
//                        String resultType = itemListjson.getJSONObject(i).getString("resultType");
//                        //巡检类型不为2且没打勾的算有问题
//                        if (!resultType.equals("2") && isCheck.equals("false")) {
//                            fault_note = fault_note + item_name + ";";
//                        }
//                    }
//                    fault_note += selectOptionService.getLanguageInfo( LangConstant.MSG_NEED_REPAIR);
//
//                    //获取当前时间
//                    Timestamp now = new Timestamp(System.currentTimeMillis());
//                    Calendar calendar = new GregorianCalendar();
//                    calendar.setTime(now);
//                    calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
//                    Date date = calendar.getTime();   //这个时间就是日期往后推一天的结果
//                    Timestamp nousedate = new Timestamp(date.getTime());
//                    List<User> repairusers = null;//repairuser(schema_name, inspectionDataOn.getFacility_id(), now, now);
//                    int assetInSiteNum = jsonasset.size();      // 该位置需要保养的设备数量
//                    int scheduleInSiteNum = repairusers.size();        // 该位置当天排班人数\
//                    // 每人平均负责保养的设备数量（向下取整）
//                    int avgNum = 1;
//                    int isBigYu = 0; //是否有余
//                    if (repairusers.size() != 0) {
//                        avgNum = assetInSiteNum / scheduleInSiteNum;
//                        isBigYu = assetInSiteNum % scheduleInSiteNum;
//                    }
//                    // 得到指定json key对象的value对象
//                    String groupId = "";
//                    for (int i = 0; i < jsonasset.size(); i++) {
//                        String assetcode = jsonasset.getJSONObject(i).getString("assetcode");
//                        //获取当前设备
//                        Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, assetcode);
//                        if (assetModel == null || assetModel.get_id().isEmpty()) {
//                            throw new NullArgumentException(selectOptionService.getLanguageInfo( LangConstant.MSG_ASSET_NOTHINGNESS));
//                        }
//
//                        if (scheduleInSiteNum > 0) {
//                            User user = new User();
//                            //首先看设备有没有专门的负责人员，有的话先给他
//                            List<User> dutyUserList = assetDutyManService.findDutyUserByAssetId(schema_name, 1, assetModel.get_id(), new Long(assetModel.getIntsiteid()).intValue());
//                            if (dutyUserList != null && dutyUserList.size() > 0) {
//                                int max = dutyUserList.size();
//                                int getInt = (int) (new Random().nextFloat() * max);
//                                user = dutyUserList.get(getInt);
//                            }
//                            if (user == null || user.getAccount() == null || user.getAccount().isEmpty()) {
//                                if (avgNum != 0 && i / avgNum < repairusers.size()) {
//                                    // 能整除的部分平均分配
//                                    user = repairusers.get(i / avgNum);
//                                } else {
//                                    // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
//                                    int last = 0;
//                                    int max = repairusers.size();
//                                    if (isBigYu > 0) {
////                                        last = i % scheduleInSiteNum;
//                                        //随机取
//                                        last = (int) (new Random().nextFloat() * max);
//                                    }
//                                    user = repairusers.get(last);
//                                }
//                            }
//
//                            RepairData model = new RepairData();
//                            //生成新订单编号
//                            String repairCode = serialNumberService.generateMaxBusinessCode(schema_name, "repair");
//                            model.setRepairCode(repairCode);
//                            model.setAssetId(assetModel.get_id());
//                            model.setAssetType(assetModel.getAsset_type());
//                            model.setAssetStatus(2);
//                            model.setDeadlineTime(nousedate);
//                            model.setOccurtime(now);
//                            model.setFaultNote(fault_note);
//                            model.setDistributeAccount("");
//                            model.setDistributeTime(null);
//                            model.setReceiveAccount(user.getAccount());
//                            //判断状态
//                            int status = 40;   //待分配
//                            model.setReveiveTime(now);
//                            model.setDistributeTime(now);
//                            model.setRepairBeginTime(null);
//                            model.setStatus(status);
//                            model.setFacilityId((int) assetModel.getIntsiteid());
//                            model.setCreate_user_account(account);
//                            model.setCreatetime(now);
//                            model.setFromCode(inspectioncode);
//                            model.setPriorityLevel(2);
//                            //保存上报问题
//                            int doCount = repairService.saveRepairInfo(schema_name, model);
//                            int a = 0;
//                        }
//                    }
//                }
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_INS_SU));
//                result.setContent(inspectioncode);
//                //记录历史
//                logService.AddLog(schema_name, "inspection", inspectioncode, selectOptionService.getLanguageInfo( LangConstant.MSG_INS_DATA_SUBMIT), account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_INS_FA));
//                return result;
//            }
//
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_ERR_ADMIN));
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "inspection_do", selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_INS_ERROR) + ex.getMessage(), account);
//            return result;
//        }
//    }
//
////    public List<User> repairuser(String schema, int facility, Timestamp now, Timestamp nowadd) {
////        //找到该位置所有的，能执行保养提交任务的人员
////        //修改为白天在上班的能保养的人员
////        List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////        if (scheduleList.size() == 0) {
////            //再找一级所属位置的，看有没有排班的人
////            List<Facility> facilities = facilitiesService.FacilitiesList(schema);
////            for (Facility facilityxx : facilities) {
////                if (facilityxx.getId() == facility)
////                    facility = facilityxx.getParentId();
////                break;
////            }
////            scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////            if (scheduleList.size() == 0) {
////                //还是没有人，则按角色找
//////                scheduleList = userService.findUserByFunctionKey(schema, "repair_do", facility);
////            }
////        }
////        //找到的人如果没有配置，则不分配保养人员
////        if (scheduleList == null || scheduleList.isEmpty()) {
////            scheduleList = new ArrayList<>();
////        }
////        return scheduleList;
////    }
//

}
