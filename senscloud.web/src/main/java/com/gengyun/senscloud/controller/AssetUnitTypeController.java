package com.gengyun.senscloud.controller;

//import com.alibaba.fastjson.JSON;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.AssetUnitTypeTaskTemplateData;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import net.sf.json.JSONArray;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 设备型号
// * User: zys
// * Date: 2019/04/20
// * Time: 上午11:20
// */
//@RestController
//@RequestMapping("/asset_unit_type")
public class AssetUnitTypeController {
//    private static final Logger logger = LoggerFactory.getLogger(AssetUnitTypeController.class);
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AssetUnitTypeService assetUnitTypeService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    TaskTempLateService taskTempLateService;
//
//    @Autowired
//    private PlanWorkService planWorkService;
//
//    /**
//     * 进入主页
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        ModelAndView mav = new ModelAndView("asset_unit_type/index");
//
//        return mav;
//    }
//
//    /**
//     * 查询列表页列表数据（分页）
//     *
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_asset_unit_type_list")
//    public String queryAutList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                               @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                               @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                               @RequestParam(name = "sortName", required = false) String sortName,
//                               @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                               HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = assetUnitTypeService.query_modal(schemaName, searchKey, pageSize, pageNumber, sortName, sortOrder);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 进入新增页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/add_asset_unit_type_view")
//    public ModelAndView add(HttpServletRequest request) {
//        ModelAndView mav = new ModelAndView("asset_unit_type/add_asset_unit_type");
//        return mav;
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/add_asset_unit_type")
//    @Transactional //支持事务
//    public ResponseModel addAut(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return assetUnitTypeService.insert_modal(schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 进入新增页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/edit_asset_unit_type_view")
//    public ModelAndView edit(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        Map<String, Object> model_data = assetUnitTypeService.find_model_by_id(schemaName, Integer.parseInt(id));
//        ModelAndView mav = new ModelAndView("asset_unit_type/detail_asset_unit_type").addObject("model_data", JSON.toJSON(model_data));
//        return mav;
//    }
//
//    /**
//     * 进入新增页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/getAutById")
//    public ResponseModel getAutById(HttpServletRequest request) {
//        Map<String, Object> data = new HashMap<>();
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("autId");
//            if (RegexUtil.isNotNull(id)) {
//                data = assetUnitTypeService.find_model_by_id(schemaName, Integer.parseInt(id));
//                return ResponseModel.ok(data);
//            }
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//            e.printStackTrace();
//        }
//        return ResponseModel.ok(data);
//    }
//
//
//    /**
//     * 更新数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/edit_asset_unit_type")
//    @Transactional //支持事务
//    public ResponseModel editAut(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return assetUnitTypeService.update_model(schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/delete_asset_unit_type")
//    @Transactional //支持事务
//    public ResponseModel deleteAut(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return assetUnitTypeService.delete_model(schemaName, request);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 新增Bom数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/add_bom")
//    @Transactional //支持事务
//    public ResponseModel addBom(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            JSONArray json = JSONArray.fromObject(paramMap.get("bomList"));
//            String model_id = paramMap.get("model_id").toString();
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return assetUnitTypeService.insert_modal_bom(schemaName, (List) json, loginUser, model_id);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
////    /**
////     * 新增Bom数据
////     *
////     * @param paramMap
////     * @param request
////     * @return
////     */
////    @MenuPermission("asset_unit_type")
////    @RequestMapping("/add_bom_part")
////    @Transactional //支持事务
////    public ResponseModel addBomPart(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
////        String schemaName = authService.getCompany(request).getSchema_name();
////        try {
////
////            String bom_id = paramMap.get("bom_id").toString();
////            String part_id = paramMap.get("part_id").toString();
////            boolean permissionFlag = true;
////            if (permissionFlag) {
////                return assetUnitTypeService.update_model_bom_part(schemaName, Long.parseLong(bom_id), Long.parseLong(part_id));
////            } else {
////                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
////                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
////            }
////        } catch (Exception e) {
////            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
////            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
////            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
////        }
////    }
//
//    /**
//     * Bom列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_bom_list")
//    @Transactional //支持事务
//    public String findBomList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String model_id = request.getParameter("model_id");
//            boolean permissionFlag = true;
//            if (permissionFlag && null != model_id && !"".equals(model_id)) {
//                return assetUnitTypeService.query_modal_bom(schemaName, model_id).toString();
//            } else {
//                return "{}";
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return "{}";
//        }
//    }
//
//    /**
//     * Bom的Modal列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_bom_modal_list")
//    @Transactional //支持事务
//    public String findBomListforModal(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return assetUnitTypeService.findBomList(schemaName, request).toString();
//            } else {
//                return "{}";
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return "{}";
//        }
//    }
//
//    /**
//     * 删除Bom数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/delete_bom")
//    @Transactional //支持事务
//    public ResponseModel deleteBom(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//
//            String id = request.getParameter("id");
//            boolean permissionFlag = true;
//            if (permissionFlag && !RegexUtil.isNull(id)) {
//                return assetUnitTypeService.delete_model_bom(schemaName, Integer.parseInt(id));
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    //配置设备型号的备件周期
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/setting_asset_model_bom_cycle")
//    public ResponseModel settingAssetModelBomCycle(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String asset_model_bom_id = request.getParameter("asset_model_bom_id");
//            String use_days = request.getParameter("use_days");
//            float useDays = 0;
//            if (null != use_days && !use_days.isEmpty()) {
//                useDays = Float.parseFloat(use_days);
//            }
//            float maintenanceDays = 0;// 保养周期
//            String maintenance_days = request.getParameter("maintenance_days");
//            if (null != maintenance_days && !maintenance_days.isEmpty()) {
//                maintenanceDays = Float.parseFloat(maintenance_days);
//            }
//            int unitType = 2; // 1:小时；2：天
//            String unit_type = request.getParameter("unit_type");
//            if (null != unit_type && !unit_type.isEmpty()) {
//                unitType = Integer.parseInt(unit_type);
//            }
//            if(maintenanceDays<=0 && useDays<=0){
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_DE_DATA_NOT_NULL));// 数据为空
//            }
//            if (!RegexUtil.isNull(asset_model_bom_id)) {
//                int assetModelBomId = Integer.parseInt(asset_model_bom_id);
//                //给已有的设备型号的备件配置周期，且id有值
//                return assetUnitTypeService.updateAssetModelBomCycle(schemaName, assetModelBomId,useDays,maintenanceDays,unitType);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//                return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 新增组织数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/add_facility")
//    @Transactional //支持事务
//    public ResponseModel addFacility(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            JSONArray json = JSONArray.fromObject(paramMap.get("facilityList"));
//            String model_id = paramMap.get("model_id").toString();
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return assetUnitTypeService.insert_modal_customer(schemaName, (List) json, loginUser, model_id);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 关联组织列表
//     * * @param request
//     *
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_facility_list")
//    @Transactional //支持事务
//    public String findFacilityList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String model_id = request.getParameter("model_id");
//            boolean permissionFlag = true;
//            if (permissionFlag && null != model_id && !"".equals(model_id)) {
//                return assetUnitTypeService.query_modal_customer(schemaName, model_id).toString();
//            } else {
//                return "{}";
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return "{}";
//        }
//    }
//
//    /**
//     * 组织的Modal列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_facility_modal_list")
//    @Transactional //支持事务
//    public String findFacilityListforModal(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return assetUnitTypeService.findFacilitiesList(schemaName, request).toString();
//            } else {
//                return "{}";
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return "{}";
//        }
//    }
//
//    /**
//     * 删除组织数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/delete_facility")
//    @Transactional //支持事务
//    public ResponseModel deleteCustomer(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String id = request.getParameter("id");
//            boolean permissionFlag = true;
//            if (permissionFlag && !RegexUtil.isNull(id)) {
//                return assetUnitTypeService.delete_model_customer(schemaName, Integer.parseInt(id));
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 新增doc数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/add_doc")
//    @Transactional //支持事务
//    public ResponseModel addDoc(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            String model_id = paramMap.get("model_id").toString();
//
//            if (RegexUtil.isNull(model_id) || "".equals(model_id)) {
//                paramMap.put("model_id", 0);
//            }
//            paramMap.put("create_user_account", loginUser.getAccount());
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return assetUnitTypeService.insert_modal_doc(schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * doc列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_doc_list")
//    @Transactional //支持事务
//    public String findDocList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String model_id = request.getParameter("model_id");
//            boolean permissionFlag = true;
//            if (permissionFlag && null != model_id && !"".equals(model_id)) {
//                return assetUnitTypeService.query_modal_doc(schemaName, model_id).toString();
//            } else {
//                return "{}";
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return "{}";
//        }
//    }
//
//    /**
//     * 删除doc数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/delete_doc")
//    @Transactional //支持事务
//    public ResponseModel deleteDoc(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String file_id = request.getParameter("file_id");
//            file_id = file_id.replace(",", "").trim();
//            boolean permissionFlag = true;
//            if (permissionFlag && !RegexUtil.isNull(file_id)) {
//                return assetUnitTypeService.delete_model_doc(schemaName, Integer.parseInt(file_id));
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 获得所有的设备型号对应的工单类型任务模板
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/find_asset_model_task_template_list")
//    public String getAssetModelTaskTemplateList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject pageResult = new JSONObject();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String model_id_word = request.getParameter("ownerBusinessId");
//            Integer model_id = Integer.parseInt(model_id_word);
//            List<AssetUnitTypeTaskTemplateData> taskTemplateList = assetUnitTypeService.getAssetModelTaskTemplate(schema_name, model_id);
//            pageResult.put("rows", taskTemplateList);
//            pageResult.put("code", 1);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            pageResult.put("code", 0);
//            return pageResult.toString();
//        }
//    }
//
//    //新增设备型号的工单类型的任务模板
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/asset_model_task_template_save")
//    public ResponseModel getAddAssetPositionTaskTemplate(HttpServletRequest request, HttpServletResponse response) {
//        //保存任务模板
//        ResponseModel result = new ResponseModel();
//        try {
//            String model_id_word = request.getParameter("ownerBusinessId");
//            Integer work_type_id = Integer.parseInt(request.getParameter("work_type_id"));
//            String task_template_code = request.getParameter("task_template_code");
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            if (model_id_word == null || task_template_code == null || work_type_id <= 0) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CH_WORK_TEM));
//                return result;
//            } else {
//                User user = authService.getLoginUser(request);
//                Timestamp create_time = new Timestamp(System.currentTimeMillis());
//                Integer model_id = Integer.parseInt(model_id_word);
//
//                //看看是否有重复，如果有重复，则提示不需要保存
//                List<AssetUnitTypeTaskTemplateData> taskTemplateList = assetUnitTypeService.getAssetModelTaskTemplate(schema_name, model_id);
//                if (null != taskTemplateList && !taskTemplateList.isEmpty()) {
//                    boolean isHave = false;
//                    for (AssetUnitTypeTaskTemplateData item : taskTemplateList) {
//                        if (work_type_id == item.getWork_type_id() && item.getTask_template_code().equals(task_template_code)) {
//                            isHave = true;
//                            break;
//                        }
//                    }
//                    if (isHave) {
//                        result.setCode(-1);
//                        result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CONF_REPEAT));
//                        return result;
//                    }
//                }
//                //进行新增配置
//                AssetUnitTypeTaskTemplateData addData = new AssetUnitTypeTaskTemplateData();
//                addData.setCreate_time(create_time);
//                addData.setModel_id(model_id);
//                addData.setWork_type_id(work_type_id);
//                addData.setTask_template_code(task_template_code);
//                addData.setCreate_user_account(user.getAccount());
//
//                //保存资产关联的任务模板
//                int count = assetUnitTypeService.addAssetModelTaskTemplate(schema_name, addData);
//
//                if (count > 0) {
//                    result.setCode(1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_WORK_TEM_SU));
//                } else {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_WORK_TEM_DE));
//                }
//                //返回保存结果
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_WORK_TEM_DE));
//            return result;
//        }
//    }
//
//
//    //删除设备型号对应的工单类型任务模板
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/asset_model_task_template_delete")
//    public ResponseModel getDeleteAssetPositionTaskTemplate(HttpServletRequest request, HttpServletResponse response) {
//        //删除资产关联企业
//        ResponseModel result = new ResponseModel();
//        String model_id_word = request.getParameter("ownerBusinessId");
//        Integer work_type_id = Integer.parseInt(request.getParameter("work_type_id"));
//        String task_template_code = request.getParameter("task_template_code");
//        String schema_name = authService.getCompany(request).getSchema_name();
//        if (model_id_word == null || task_template_code == null || work_type_id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PETT_II));
//            return result;
//        } else {
//            Integer model_id = Integer.parseInt(model_id_word);
//            int count = assetUnitTypeService.deletaAssetModelTaskTemplate(schema_name, model_id, work_type_id, task_template_code);
//            if (count > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_TEM_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_TEM_DE));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    /**
//     * 查询设备型号下，维保计划列表信息
//     */
//    @MenuPermission("asset_unit_type")
//    @RequestMapping("/findAssetModelPlanWorkInfos")
//    public String getPlanWorkScheduleByAssetModel(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String assetModelId = request.getParameter("assetModelId");
//        org.json.JSONObject result = new org.json.JSONObject();
//        try {
//            if (StringUtils.isBlank(assetModelId)) {
//                return "{}";
//            }
//            List<Map<String, Object>> planWorkInfos = planWorkService.getPlanWorkScheduleByAssetModel(schema_name, Long.parseLong(assetModelId));
//            result.put("rows", planWorkInfos);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
}
