package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.DashboardConfigData;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.DashboardConfigService;
//import com.gengyun.senscloud.service.FacilitiesService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.List;
//

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    DashboardConfigService configService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//
//    @RequestMapping("/index")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//
//        Boolean isisDashboardSelect = false;
//        Boolean canSeeAllFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "dashboard");
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("dashboard-select")) {
//                    isisDashboardSelect = true;
//                }
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("dash_all_facility")) {
//                    canSeeAllFacility = true;
//                }
//            }
//        }
//
//        //获取登录用户所在位置
//       return new ModelAndView("dashboard/index").addObject("isCanSelectDashboard", isisDashboardSelect).addObject("canSeeAllFacility", canSeeAllFacility);
////       return new ModelAndView("cloud");
//    }
//
//
//    /**
//     * 获得所有的位置
//     */
//    @RequestMapping("find-all-facilities")
//    public ResponseModel queryAllFacilities(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "dashboard");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("dash_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    }
//                }
//            }
//
//            //按用户权限，获取用户所能看见的位置
//            List<Facility> facilitiesList = facilitiesService.GetFacilityListByUser(schema_name,isAllFacility,user);
//            return ResponseModel.ok(facilitiesList);
//
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    /**
//     * 获得所有需要显示的看板配置，并加载数据
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_dashboard_show_list")
//    public ResponseModel GetDashBoardShowList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //选择的位置，按位置查询工单
//            String facilityId = request.getParameter("facilityId");
//            List<DashboardConfigData> dataList = configService.findAllDashboardConfigList(schema_name);
//            if (dataList != null && !dataList.isEmpty()) {
//                List<DashboardConfigData> showList = new ArrayList();
//                for (DashboardConfigData data : dataList) {
//                    if (data.getIsshow()) {
//                        data.setKpivalue(configService.getDashboardConfigKPI(schema_name, data.getShowType(), data.getDatasource(), data.getDateType(), facilityId,null , null));
//                        showList.add(data);
//                    }
//                }
//                result.setContent(showList);
//            }
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_B_T_ERROR) + ex.getMessage());
//            return result;
//        }
//    }
//
//    /**
//     * 获得单条需要显示的看板配置，并加载数据，同时将其isshow设置为true
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_dashboard_show_single")
//    public ResponseModel GetDashBoardShowSingle(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Integer id = Integer.parseInt(request.getParameter("id"));
//            //选择的位置，按位置查询工单
//            String facilityId = request.getParameter("facilityId");
//
//            DashboardConfigData data = configService.findDashboardConfigData(schema_name, id);
//            if (data != null) {
//                data.setKpivalue(configService.getDashboardConfigKPI(schema_name, data.getShowType(), data.getDatasource(), data.getDateType(), facilityId, null, null));
//                result.setContent(data);
//                //将其更新为是需要显示的
//                configService.UpdateDashboardIsShow(schema_name, id, true);
//            }
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_B_T_ERROR) + ex.getMessage());
//            return result;
//        }
//    }
//
//    /**
//     * 设置看板的显示isshow为false
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_dashboard_notshow")
//    public ResponseModel GetDashBoardNoShow(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Integer id = Integer.parseInt(request.getParameter("id"));
//
//            if (id > 0) {
//                //将其更新为是需要显示的
//                configService.UpdateDashboardIsShow(schema_name, id, false);
//            }
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DISABLE_SU));
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DISABLE_ERROR) + ex.getMessage());
//            return result;
//        }
//    }
}
