package com.gengyun.senscloud.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.*;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.service.customization.ManfuService;
//import com.gengyun.senscloud.util.*;
//import com.gengyun.senscloud.util.Itextpdf.HtmlToPdfUtils;
//import com.gengyun.senscloud.view.WorkSheetDataExportView;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.sql.Timestamp;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@RestController
//@RequestMapping("/work")
public class WorkSheetController {
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//    @Autowired
//    AuthService authService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    WorkListService workListService;
//
//    @Autowired
//    MetadataWorkService metadataWorkService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    private WorkflowService workflowService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//    @Autowired
//    WorkScheduleService workScheduleService;
//    @Autowired
//    WorkSheetRequestService workSheetRequestService;
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//    @Autowired
//    EmailUtil emailUtil;
//    @Autowired
//    TaskTempLateService taskTempLateService;
//    @Autowired
//    ManfuService manfuService;
//
//    /**
//     * 报表所需任务模板名称
//     */
//    private String[] taskTemplateNames = new String[]{"Maintenances items", "Calibration and test items", "Recording"};
//
//    @MenuPermission("worksheet")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView selectStockData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String client_customer_code = authService.getCompany(request).getClient_customer_code();
//        User loginUser = AuthService.getLoginUser(request);
//        String status = request.getParameter("status");
//        Boolean exportFlag = false;
//        Boolean invalidFlag = false;
//        Boolean distributeFlag = false;
//        Boolean addFlag = false;
//        Boolean auditpcFlag = false;
//        Boolean dealpcFlag = false;
//        Boolean applyForBomFlag = false;//申领备件状态
//        String workIntervalShow = null; // 时效
//        Boolean isUserGroupFilter = false; // 是否开启用户组搜索
//        Boolean isWorkSheetFix = false; // 是否开启用户组搜索
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "worksheet");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_export")) {
//                        exportFlag = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_invalid")) {
//                        invalidFlag = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_distribute")) {
//                        distributeFlag = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_add")) {
//                        addFlag = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_audit_pc")) {
//                        auditpcFlag = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_do_pc")) {
//                        dealpcFlag = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_user_group")) {
//                        isUserGroupFilter = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_fix")) {
//                        isWorkSheetFix = true;
//                    }
//                }
//            }
//            //获取权限树
////            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, Constants.APPLY_FOR_BOM);
//            if (dataList != null) {
//                if (dataList.getSettingValue().equals("1")) {
//                    applyForBomFlag = true;
//                }
//            }
//            SystemConfigData wis = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.WORK_INTERVAL_SHOW);
//            if (null != wis) {
//                workIntervalShow = wis.getSettingValue();
//            }
//        } catch (Exception ex) {
////            facilitiesSelect = "";
//        }
//        return new ModelAndView("work/index")
////                .addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("exportFlag", exportFlag)
//                .addObject("rpAddFlag", addFlag)
//                .addObject("rpInvalidFlag", invalidFlag)
//                .addObject("isUserGroupFilter", isUserGroupFilter)
//                .addObject("isWorkSheetFix", isWorkSheetFix)
//                .addObject("rpDistributeFlag", distributeFlag)
//                .addObject("rpAuditpcFlag", auditpcFlag).addObject("rpDealpcFlag", dealpcFlag)
//                .addObject("currentUser", loginUser.getAccount())
//                .addObject("departmentname", loginUser.getDepartmentName())
//                .addObject("client_customer_code", client_customer_code)
//                .addObject("searchStatus", status)
//                .addObject("applyForBomFlag", applyForBomFlag)
//                .addObject("workIntervalShow", workIntervalShow);
//    }
//
//    @RequestMapping("/find-worksheet-list")
//    public String queryworksheet(HttpServletRequest request, HttpServletResponse response) {
//        return workSheetService.findWorkSheetList(request);
//    }
//
//    //取fromkey
//    @RequestMapping("/get-fromkey")
//    public ResponseModel findfromkey(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String flow_template_code = request.getParameter("id");
//            String formKey = workflowService.getStartFormKey(schemaName, flow_template_code);
//            result.setCode(1);
//            result.setContent(formKey);
//            // 获取系统模板信息
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            result.setCode(-1);
//            result.setContent("");
//        }
//
//        return result;
//    }
//
//    //获取子工单集合
//    @RequestMapping("/get-sublist")
//    public ResponseModel findsublist(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String work_code = request.getParameter("work_code");
//            List<Map<String, Object>> list = workSheetService.getSubWorkMapList(schemaName, work_code);
//            //时区转换处理
//            SCTimeZoneUtil.responseMapListDataHandler(list, new String[]{"begin_time", "finished_time", "finished_time_interval"});
//            result.setCode(1);
//            result.setContent(list);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            result.setCode(-1);
//            result.setContent("");
//        }
//
//        return result;
//    }
//
//    //工单详情
//    @RequestMapping("/detail-worksheet")
//    public ModelAndView findworkbycode(HttpServletRequest request, HttpServletResponse response) {
//        Boolean distributionFlag = false;//维修分配权限
//        int distributionType = 0;//分配类型
//        String typeFlag = null;//权限标识
//        String flagtype = null;//获取权限标识的标识
//        String subWorkCode = null; // 业务数据主键
//        String workFormKey = null; // 表单key
//        String do_flow_key = String.valueOf(ButtonConstant.BTN_DISTRIBUTION);//如果需要分配
//        String actionUrl = "/workSheetHandle/doDistribution";//分配提交的url
//        int workTypeId = 0; // 工单类型
//        Boolean isDistribute = false;//是否需要分配
//        String work_code = null;
//        String whereString = "";//查询条件用于筛选模版
//        int tamplate_type = 0;//模版类型
//        int relation_type = 0;//对象类型
//        int is_main = 0;//判断是不是主子工单 1是 -1不是
//        String finished_time_interval = selectOptionService.getLanguageInfo(LangConstant.NONE_A);//完成时效
//        String arrive_time_interval = selectOptionService.getLanguageInfo(LangConstant.NONE_A);//到场时效
//        Long tv;//时间转换方式
//        String schema_name = authService.getCompany(request).getSchema_name();
//        Boolean handleFlag = false;//判断子工单列表操作项是否可以处理
//        Boolean detailFlag = false;//判断子工单列表操作项是否可以查看详情
//        Boolean editFlag = false;//判断子工单列表操作向是否可以编辑
//        String handleType = null;//处理类型
//        WorkListDetailModel workdetail = new WorkListDetailModel();
//        List<Map<String, Object>> subList = new ArrayList<Map<String, Object>>();
//        DecimalFormat decimalFormat = new DecimalFormat("#.00");
//        ModelAndView mav = new ModelAndView("work/worksheet_detail/worksheet_detail");
//        try {
//
//            User user = authService.getLoginUser(request);
//            String schemaName = authService.getCompany(request).getSchema_name();
//            subWorkCode = request.getParameter("sub_work_code");
//            workdetail = workSheetService.finddetilByID(schemaName, subWorkCode);
//            handleType = request.getParameter("handleType");
//            String subPage = request.getParameter("subPage");
//            mav.addObject("subPage", subPage);
//            if ("handle".equals(handleType)) {//处理
//                handleFlag = true;
//            } else if ("detail".equals(handleType)) {//详情
//                detailFlag = true;
//            } else if ("edit".equals(handleType)) {//编辑
//                editFlag = true;
//            } else if ("distribute".equals(handleType)) {//分配
//                isDistribute = true;
//                detailFlag = true;
//            } else {
//                if (workdetail.getStatus() == 20 || workdetail.getStatus() == 30 || workdetail.getStatus() == 40) {
//                    detailFlag = true;
//                    isDistribute = true;
//                }
//
//            }
//            if (isDistribute) {
//                workdetail = workSheetService.finddetilByID(schemaName, subWorkCode);
//                distributionType = workdetail.getBusiness_type_id();
//                switch (distributionType) {
//                    case Constants.BUSINESS_TYPE_REPAIR_TYPE:
//                        typeFlag = "repair_distribute";
//                        flagtype = "repair";
//                        break;
//                    case Constants.BUSINESS_TYPE_MAINTAIN_TYPE:
//                        typeFlag = "maintain_distribute";
//                        flagtype = "maintain";
//                        break;
//                    case Constants.BUSINESS_TYPE_INSPECTION_TYPE:
//                        typeFlag = "inspection_distribute";
//                        flagtype = "inspection";
//                        break;
//                    case Constants.BUSINESS_TYPE_SPOT_TYPE:
//                        typeFlag = "spot_distribute";
//                        flagtype = "spotcheck";
//                        break;
//                }
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), flagtype);
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //查看有没有当前类型的工单分配权限，则不限制
//                        if (functionData.getFunctionName().equals(typeFlag)) {
//                            distributionFlag = true;
//                            break;
//                        }
//                    }
//                }
//                if (!distributionFlag) {
//                    isDistribute = false;
//                }
//            }
//            if (isDistribute && distributionFlag) {
//                // 明细数据
//                Map<String, Object> dtlInfo = workSheetHandleService.queryBusinessDetailById(schemaName, subWorkCode);
//                work_code = (String) dtlInfo.get("work_code");
//                request.setAttribute("work_code", work_code);
//                String result = workSheetService.findWorkSheetList(request); // 权限判断
//                if ("".equals(result)) {
//                    throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH));
//                }
//                // 主明细数据
//                Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schemaName, work_code);
//                subWorkCode = (String) mainDetailInfo.get("sub_work_code");
//
//            }
//            workdetail = workSheetService.finddetilByID(schemaName, subWorkCode);
//            //如果完成时间和开始时间存在则计算完成时效
//            if (workdetail.getFinished_time() != null && !"".equals(workdetail.getFinished_time()) && workdetail.getBegin_time() != null && !"".equals(workdetail.getBegin_time())) {
//                tv = (workdetail.getFinished_time().getTime() - workdetail.getBegin_time().getTime())
//                        / (1000 * 60);
//                finished_time_interval = String.valueOf(Double.parseDouble(decimalFormat.format(tv))) + selectOptionService.getLanguageInfo(LangConstant.MIN_A);
//            }
//            //如果分配时间和开始时间存在则计算到场时效
//            if (workdetail.getBegin_time() != null && !"".equals(workdetail.getBegin_time()) && workdetail.getDistribute_time() != null && !"".equals(workdetail.getDistribute_time())) {
//                tv = (workdetail.getBegin_time().getTime() - workdetail.getDistribute_time().getTime())
//                        / (1000 * 60);
//                arrive_time_interval = String.valueOf(Double.parseDouble(decimalFormat.format(tv))) + selectOptionService.getLanguageInfo(LangConstant.MIN_A);
//            }
//            work_code = workdetail.getWork_code();
//
//            request.setAttribute("work_code", work_code);
//            String result = workSheetService.findWorkSheetList(request); // 权限判断
//            if ("".equals(result)) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH));
//            }
//            workTypeId = workdetail.getWork_type_id();
//            relation_type = workdetail.getRelation_type();
//            whereString += "and(work_type_id=" + workTypeId + ")";
//            whereString += "and(relation_type=" + relation_type + ")";
//            is_main = workdetail.getIs_main();
//            if (is_main == 1) {
//                subList = workSheetService.getSubWorkMapList(schemaName, work_code);
//                tamplate_type = 3;//主工单的详情
//            } else {
//                tamplate_type = 2;//子工单的详情
//            }
//            whereString += "and(template_type=" + tamplate_type + ")";
//            if (RegexUtil.isNotNull(subPage)) {
//                workFormKey = request.getParameter("work_form_key");
//            } else {
//                workFormKey = selectOptionService.getOptionNameByCode(schemaName, "work_template_for_detail", whereString, "code");
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        if (null == subList || subList.size() == 0) {
//            mav.addObject("subList", "[]");
//        } else {
//            mav.addObject("subList", JSON.toJSONString(subList));
//        }
//        //时区转换处理
//        SCTimeZoneUtil.responseObjectDataHandler(workdetail);
//        mav.addObject("businessUrl", "/workSheetHandle/findSingleWorkInfo")
//                .addObject("fpPrefix", Constants.FP_PREFIX)
//                .addObject("workCode", subWorkCode)
//                .addObject("workFormKey", workFormKey)
//                .addObject("workTypeId", workTypeId)
//                .addObject("HandleFlag", handleFlag)
//                .addObject("DetailFlag", detailFlag)
//                .addObject("EditFlag", editFlag)
//                .addObject("WorkListDetailModel", workdetail)
//                .addObject("finished_time_interval", finished_time_interval)
//                .addObject("arrive_time_interval", arrive_time_interval)
//                .addObject("subWorkCode", subWorkCode)
//                .addObject("flow_id", workdetail.getFlow_code())
//                .addObject("do_flow_key", do_flow_key)
//                .addObject("actionUrl", actionUrl)
//                .addObject("is_main", is_main)
//                .addObject("isDistribute", isDistribute);
//        return mav;
//
//
//    }
//
//    //作废工单
//    @MenuPermission("worksheet_invalid")
//    @RequestMapping("/WorkListcancel")
//    @Transactional
//    public ResponseModel WorkListcancel(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            int deletesub = 0;
//            Boolean deleteflow = false;
//            String work_code = request.getParameter("work_code");//备注
//            String result = workSheetService.findWorkSheetList(request); // 权限判断
//            if ("".equals(result)) {
//                throw new Exception(selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH));
//            }
//            List<Map<String, Object>> workdetaillist = workSheetHandleService.querySubWorkCodeListByWorkCode(schema_name, work_code);
//            int s = workdetaillist.size();
//
//            // 保存工单主表，更新状态
//            Map<String, Object> dataInfo = new HashMap<String, Object>();
//            dataInfo.put("int@status", "status");
//            dataInfo.put("status", StatusConstant.CANCEL);//保存状态
//            dataInfo.put("work_code", work_code);
//            int i = workSheetHandleService.updateWorkByWorkCode(schema_name, dataInfo);
//            // 保存工单明细表，更新分配人、状态、时间等信息
//            deletesub = workSheetHandleService.updateDetailByWorkCode(schema_name, dataInfo, "work_code");
//            workSheetRequestService.cancelWsqByWork(schema_name, work_code, user.getAccount()); // 关闭服务单
//            if (i > 0 && s == deletesub) {
//                for (Map<String, Object> sub : workdetaillist) {
//                    deleteflow = workflowService.deleteInstancesBySubWorkCode(schema_name, (String) sub.get("sub_work_code"), "作废");
//                    if (!deleteflow) {
//                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//                        return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.PROCESS_DEL_ERROR));
//                    }
//                }
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_OBSOLETE_SUCCESS));
//            } else if (i > 0 && s <= 0) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CANCEL_ERROR));
//            } else if (i <= 0 && s != deletesub) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_WORK_CANCEL_ERROR));
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERR));
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUB_EXCEP));
//        }
//    }
//
////    //删除工单
////    @RequestMapping("/WorkListdelete")
////    @Transactional
////    public ResponseModel WorkListdelete(HttpServletRequest request, HttpServletResponse response) {
////        ResponseModel model = new ResponseModel();
////        String schema_name = authService.getCompany(request).getSchema_name();
////        User user = authService.getLoginUser(request);
////        try {
////            String result = workSheetService.findWorkSheetList(request); // 权限判断
////            if ("".equals(result)) {
////                throw new Exception(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH));
////            }
////            Boolean deletework = false;
////            Boolean deletesub = false;
////            String work_code = request.getParameter("work_code");//备注
////            WorkSheet worklist = workSheetService.findByID(schema_name, work_code);
////            List<Map<String, Object>> workdetaillist = workSheetService.getSubWorkMapList(schema_name, work_code);
////            int s = workdetaillist.size();
////            for (Map<String, Object> sub : workdetaillist) {
////                deletesub = workListService.delUndistributedWork(schema_name, (String) sub.get("sub_work_code"), work_code);
////            }
////
////            if (deletesub) {
////                model.setCode(200);
////                model.setContent("");
////                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_OBSOLETE_SUCCESS));
////            } else {
////                model.setCode(-1);
////                model.setContent("");
////                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_CANCEL_ERROR));
////            }
////            return model;
////        } catch (Exception e) {
////            model.setCode(-1);
////            model.setContent("");
////            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
////            return model;
////        }
////    }
//
//    //导出工单列表
//    @MenuPermission("worksheet_export")
//    @RequestMapping("/worksheet-export")
//    public ModelAndView ExportWorkSheet(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        String searchStatus = request.getParameter("status");
//        String facilities = request.getParameter("facilities");
//        String positions = request.getParameter("positions");//获取设备位置
//        String org_type = request.getParameter("org_type");//获取组织类型
//        String keyWord = request.getParameter("keyWord");
//        String beginTime = request.getParameter("beginTime");
//        String businessNo = request.getParameter("businessNo");
//        String endTime = request.getParameter("endTime");
//        String work_code = request.getParameter("work_code");
//        String asset_type = request.getParameter("asset_type");
//        String work_type_id = request.getParameter("type");
//        int pageSize = 50000;
//        int pageNumber = 1;
//        List<WorkSheet> workDataList = null;
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition = "";
//            String account = user.getAccount();
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            Boolean hasFeeFlag = false; // 工单费用显示权限
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                        isSelfFacility = true;
//                    } else if (functionData.getFunctionName().equals("work_fee")) {
//                        hasFeeFlag = true;
//                    }
//                }
//            }
//            Map<String, String> dySqlMap = new HashMap<String, String>(); // 按需动态sql
////            if (facilities != null && facilities != "") {
////                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
////                //加上位置的子位置
////                String subFacility = facilitiesService.getSubFacilityIds(facilities, facilityList);
////                condition = " and w.facility_id in (" + facilities + subFacility + ") ";
////            } else {
////                if (isAllFacility) {
////                    condition = "  ";
////                } else if (isSelfFacility) {
////                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
////                    String facilityIds = "";
////                    if (facilityList != null && !facilityList.isEmpty()) {
////                        for (String key : facilityList.keySet()) {
////                            facilityIds += facilityList.get(key).getId() + ",";
////                        }
////                    }
////                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
////                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
////                        condition = " and (w.facility_id in (" + facilityIds + ") or w.create_user_account='"+account+"')";
////                    } else {
////                        condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + " ')";    //没有用户所属位置，则按个人权限查询
////                    }
////                } else {
////                    condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + "' )";
////                }
////            }
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ASSET_ORG_POSITION_TYPE);
//            boolean isPositionGuide = true;
//            if (null == dataList || RegexUtil.isNull(dataList.getSettingValue()) || dataList.getSettingValue().equals(SqlConstant.FACILITY_STRUCTURE_TYPE)) {
//                isPositionGuide = false;
//            }
//            if (isPositionGuide) {
//                //组织权限
//                condition = SqlConditionUtil.getWorkOrgCondition(isAllFacility, isSelfFacility, "wdaof", condition, facilities, user.getFacilities(), account);
//                //位置权限
//                condition = SqlConditionUtil.getWorkPositionCondition(isAllFacility, isSelfFacility, "w", condition, positions, user.getAssetPositions(), account);
//                //没有权限只查看自己创建，或自己做的工单
//                if ((!isAllFacility && !isSelfFacility) || (!isAllFacility && isSelfFacility && "".equals(condition))) {
//                    condition = " and (w.create_user_account='" + account + "' or w2.receive_account='" + account + "')";
//                }
//            } else {
//                condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//            }
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(w.work_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%" + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or w2.receive_account like '%" + keyWord + "%' )";
//            }
//            if (org_type != null && !org_type.isEmpty() && !org_type.equals("-1")) {
//                condition += " and (wdaof.org_type =" + org_type + " ) ";
//            }
//            if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("-1")) {
//                condition += " and (w.status in(" + searchStatus + ") ) ";
//            }
//            String[] assetTypes = null;
//            if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
////                condition += " and ma.id in(" + asset_type + ") ";
//                assetTypes = asset_type.split(",");
//            }
//            String asset_type_word = dataPermissionForFacility.findAssetCategoryWordForSearchIn(schema_name, user, assetTypes);
//            if (asset_type_word != null && !asset_type_word.isEmpty()) {
//                if (asset_type != null && !asset_type.isEmpty() && !asset_type.equals("-1")) {
//                    condition += " and ma.id in (" + asset_type_word + ") ";
//                } else {
//                    condition += " and ( ma.id in (" + asset_type_word + ") or ma.ID is null ) ";
//                }
//            }
//            if (work_type_id != null && !work_type_id.isEmpty() && !work_type_id.equals("-1")) {
//                condition += " and (w.work_type_id = " + work_type_id + " ) ";
//            }
//            if (RegexUtil.isNotNull(businessNo) && !"-1".equals(businessNo)) {
//                condition += " and (t.business_type_id = " + businessNo + " ) ";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and w.occur_time <'" + end + "'";
//            }
//            if (null != work_code && !"".equals(work_code)) {
//                condition += " and w.work_code = '" + work_code + "' ";
//            }
//            // 费用sql
//            if (hasFeeFlag) {
//                dySqlMap.put(SqlConstant.SQL_WLF_SELECT_NAME, SqlConstant.SQL_WLF_SELECT);
//                dySqlMap.put(SqlConstant.SQL_WLF_TABLE_NAME, SqlConstant.SQL_WLF_TABLE.replace("${schema_name}", schema_name));
//                dySqlMap.put(SqlConstant.SQL_WLF_GROUP_COLUMN_NAME, SqlConstant.SQL_WLF_GROUP_COLUMN);
//                condition += SqlConstant.SQL_WLF_CONDITION;
//            }
//            //没有权限只查看自己创建，或自己做的工单
//            //condition+=" or w.create_user_account='" + account + "' or w2.receive_account='" + account + "' ";
//            condition += " order by w.occur_time desc,w.work_code desc ";
//            int begin = pageSize * (pageNumber - 1);
//            workDataList = workSheetService.getWorkSheetList(schema_name, condition, dySqlMap, pageSize, begin);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("WorkSheet", workDataList);
//            map.put("selectOptionService", selectOptionService);
//            map.put("hasFeeFlag", hasFeeFlag);
//            WorkSheetDataExportView excelView = new WorkSheetDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
////    /**
////     * 导出工单详情
////     * @param request
////     * @param response
////     * @return
////     */
////    @RequestMapping("/worksheet-detail-export")
////    public ModelAndView ExportWorkSheetDetail(HttpServletRequest request, HttpServletResponse response) {
////        String schema_name = authService.getCompany(request).getSchema_name();
////        String subWorkCode = request.getParameter("subWorkCode");
////        try {
////            Map<String, Object> map = new HashMap<String, Object>();
////            String taskContent = workSheetService.getWorkSheetTaskContentByWorkCode(schema_name, subWorkCode);
////            if(StringUtils.isNotBlank(taskContent)){
////                JSONObject item = new JSONObject(taskContent);
////                if(item != null && item.has("data")){
////                    JSONArray itemArray = item.getJSONArray("data");
////                    if(itemArray != null && itemArray.length() >0){
////                        StringBuffer nameStr = new StringBuffer();
////                        for(String n : taskTemplateNames){
////                            if(nameStr.length() == 0)
////                                nameStr.append("'"+n+"'");
////                            else
////                                nameStr.append(",'"+n+"'");
////                        }
////                        List<Map<String, Object>> taskTemplateAndTaskItemList = workSheetService.getTaskTemplateAndTaskItemByTemplateName(schema_name, nameStr.toString());
////                        if(taskTemplateAndTaskItemList != null && taskTemplateAndTaskItemList.size() > 0){
////                            List<Map<String, Object>> template1 = new ArrayList<>(),template2 = new ArrayList<>(),template3 = new ArrayList<>();
////                            Map<String, Object> taskItemCodeToTaskTemplateCodeMap = new HashMap<>();
////                            for(Map<String, Object> taskTemplateAndTaskItem : taskTemplateAndTaskItemList){
////                                taskItemCodeToTaskTemplateCodeMap.put((String)taskTemplateAndTaskItem.get("task_item_code"), taskTemplateAndTaskItem.get("task_template_name"));
////                            }
////                            for(int i=0;i<itemArray.length();i++){
////                                JSONObject json = itemArray.getJSONObject(i);
////                                String taskItemCode = json.getString("task_item_code");
////                                String templateName = (String)taskItemCodeToTaskTemplateCodeMap.get(taskItemCode);
////                                Map<String, Object> itemMap = new HashMap<>();
////                                itemMap.put("task_item_code", taskItemCode);
////                                itemMap.put("task_item_name", json.getString("task_item_name"));
////                                itemMap.put("result_type", json.getInt("result_type"));
////                                itemMap.put("lineValue", json.get("lineValue"));
////                                if(taskTemplateNames[0].equals(templateName))
////                                    template1.add(itemMap);
////
////                                if(taskTemplateNames[1].equals(templateName))
////                                    template2.add(itemMap);
////
////                                if(taskTemplateNames[2].equals(templateName))
////                                    template3.add(itemMap);
////                            }
////                            map.put("template1", template1);
////                            map.put("template2", template2);
////                            map.put("template3", template3);
////                        }
////                    }
////                }
////            }
////            ManfuReportExportView excelView = new ManfuReportExportView();
////            return new ModelAndView(excelView, map);
////        } catch (Exception ex) {
////            ex.printStackTrace();
////        }
////        return null;
////    }
//
//    /**
//     * 跳转到报表填写页面-checklist
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/worksheet-detail-checklist-report")
//    public ModelAndView worksheetDetailChecklistReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return manfuService.worksheetDetailChecklistReport(request, schema_name);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return new ModelAndView("manfu_report/check_list");
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-checklist
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailChecklistReport")
//    public ResponseModel doSaveWorksheetDetailCheckListReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return manfuService.doSaveWorksheetDetailCheckListReport(request, schema_name);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到报表填写页面-AT88
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/worksheet-detail-at88-report")
//    public ModelAndView worksheetDetailAt88Report(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String subWorkCode = request.getParameter("subWorkCode");
//        String sep = System.getProperty("file.separator");
//        ModelAndView modelAndView = new ModelAndView("manfu_report/at88")
//                .addObject("today", new SimpleDateFormat(Constants.DATE_FMT);.format(new Date()))
//                .addObject("subWorkCode", subWorkCode);
//        try {
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "manfu_logo");
//            if (data != null)
//                modelAndView.addObject("logo", ImageUtils.getBase64Extension(data.getSettingValue()) + ImageUtils.getImgStrToBase64(data.getSettingValue()));
//
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "at88" + sep + subWorkCode + ".json";
//            modelAndView.addObject("data", FileUtil.getMfReportData(filePath));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return modelAndView;
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-at88
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailAT88Report")
//    public ResponseModel doSaveWorksheetDetailAT88Report(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String sep = System.getProperty("file.separator");
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            String reportData = request.getParameter("reportData");
//            String filePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "at88" + sep + subWorkCode;
//            //生成json文件并保存
//            FileUtil.generateJsonFile(filePath, reportData);
//            String url = HttpRequestUtils.getBaseUrl() + "/work/worksheet-detail-at88-report?subWorkCode=" + subWorkCode;
//            //获取动态填写数据的html页面并生成文件
//            File html = HtmlToPdfUtils.getHtmlFile(request, url, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_HTML);
//            //html转pdf
//            HtmlToPdfUtils.htmlToPdf(html, filePath + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF, HttpRequestUtils.getBaseUrl(), true, true);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//        return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.SUBMIT_SUCESS));//提交成功
//    }
//
//    /**
//     * 跳转到报表填写页面-spray
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/worksheet-detail-spray-report")
//    public ModelAndView worksheetDetailSprayReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return manfuService.worksheetDetailSprayReport(request, schema_name);
//        } catch (Exception ex) {
//            return new ModelAndView("manfu_report/spray_accuracy_test_report");
//        }
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-spray
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailSprayReport")
//    public ResponseModel doSaveWorksheetDetailSprayReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return manfuService.doSaveWorksheetDetailSprayReport(request, schema_name);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到报表填写页面-photo
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/worksheet-detail-photo-report")
//    public ModelAndView worksheetDetailPhotoReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return manfuService.worksheetDetailPhotoReport(request, schema_name);
//        } catch (Exception ex) {
//            return new ModelAndView("manfu_report/field_site_photos");
//        }
//    }
//
//    /**
//     * 提交表单数据生成pdf并发送邮件-photo
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSaveWorksheetDetailPhotoReport")
//    public ResponseModel doSaveWorksheetDetailPhotoReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return manfuService.doSaveWorksheetDetailPhotoReport(request, schema_name);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 跳转到邮件发送页面
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/toSendEmailForWorksheetReport")
//    public ModelAndView toSendEmailForWorksheetReport(HttpServletRequest request) {
//        String sep = System.getProperty("file.separator");
//        String subWorkCode = request.getParameter("subWorkCode");
//        String schema_name = authService.getCompany(request).getSchema_name();
//        //checklist文件是否不存在
//        Boolean checklist = true;
//        //spray文件是否不存在
//        Boolean spray = true;
//        //photo文件是否不存在
//        Boolean photo = true;
//        WorkListDetailModel workDetail = workSheetService.finddetilByID(schema_name, subWorkCode);
//        String checklistFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "checklist" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//        String sprayFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "spray" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//        String photoFilePath = file_upload_dir + sep + schema_name + sep + "workSheetDetailReport" + sep + "photo" + sep + subWorkCode + "." + HtmlToPdfUtils.FILE_SUFFIXES_PDF;
//        File file = new File(checklistFilePath);
//        if (file.exists()) {
//            checklist = false;
//        }
//        file = new File(sprayFilePath);
//        if (file.exists()) {
//            spray = false;
//        }
//        file = new File(photoFilePath);
//        if (file.exists()) {
//            photo = false;
//        }
//        String orgTitle;
//        try {
//            Facility facility = facilitiesService.FacilitiesById(schema_name, Integer.parseInt((String) (workDetail.getFacility_id())));
//            orgTitle = facility.getShort_title();
//        } catch (ClassCastException e) {
//            orgTitle = "";
//        }
//        return new ModelAndView("manfu_report/send_email")
//                .addObject("subWorkCode", subWorkCode)
//                .addObject("org_title", orgTitle)
//                .addObject("checklist", checklist)
//                .addObject("spray", spray)
//                .addObject("photo", photo)
//                .addObject("org_id", workDetail.getFacility_id())
//                .addObject("mail_send", selectOptionService.getLanguageInfo(LangConstant.MAIL_SEND))
//                .addObject("email", selectOptionService.getLanguageInfo(LangConstant.EMAIL))
//                .addObject("location_name", selectOptionService.getLanguageInfo(LangConstant.LOCATION_NAME))
//                .addObject("report_name", selectOptionService.getLanguageInfo(LangConstant.REPORT_NAME))
//                .addObject("mail_receiver", selectOptionService.getLanguageInfo(LangConstant.MAIL_RECEIVER))
//                .addObject("mail_content_notice", selectOptionService.getLanguageInfo(LangConstant.MAIL_CONTENT_NOTICE));
//    }
//
//    /**
//     * 发送邮件
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/doSendEmailForWorksheetReport")
//    public ResponseModel doSendEmailForWorksheetReport(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            return manfuService.doSendEmailForWorksheetReport(request, schema_name, loginUser);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));//提交失败 SUB_FAIL
//        }
//    }
//
//    /**
//     * 工单修改费用
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("worksheet_fix")
//    @RequestMapping("/worksheet_fix")
//    public ResponseModel workSheetFix(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetService.workSheetFix(schema_name, paramMap, request);
//        } catch (Exception e) {
//            //修改费用失败
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.SUB_FAIL));
//        }
//    }
}

