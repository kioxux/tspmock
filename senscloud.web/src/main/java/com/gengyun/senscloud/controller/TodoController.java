package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.ToDoDataListResult;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
//@RestController
public class TodoController {
//    /**
//     * 获得资产所有的待办事项
//     *
//     * @return
//     */
//
//    @Autowired
//    IndexAppService indexAppService;
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/todo/index")
//    public ModelAndView FindALLTodoList() {
//        return new ModelAndView("todo/index");
//    }
//
//    //企业库前缀，小程序过来的方法中，每一个都带企业id，和此字段拼接成企业库
//    String schema_name_pre = SensConstant.SCHEMA_PREFIX;  //"${schema_name}";
//
//    //待办事项的数量
//    @RequestMapping("/index_todo_count_PC_All")
//    public ResponseModel getToDoCount(HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        //当前登录人
//
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//
//        try {
//
//            //获取待办数量
//            List<ToDoDataListResult> toDoDataListResult = indexAppService.toDoDataListForNumblitter(schema_name, loginUser.getAccount());
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_SUCC));
//            result.setContent(toDoDataListResult);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "index_todo_count", selectOptionService.getLanguageInfo( LangConstant.LOG_TODO_NUM_ERROR) + ex.getMessage(), loginUser.getAccount());
//            return result;
//        }
//    }
//
//
//    /**
//     * 获得所有的位置，供维修、保养、巡检、点检统计使用
//     */
//    @RequestMapping("find-all-facilities-for-analys_detial")
//    public ResponseModel queryAllFacilitiesForAnalys(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            String businessType = request.getParameter("businessType");
//            ResponseModel result = new ResponseModel();
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), businessType);
//            //3表示没有分配位置
//            result.setCode(3);
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals(businessType + IDataPermissionForFacility.ALL_FACILITY)) {//
//                        isAllFacility = true;
//                        result.setCode(1);//表示分配了全部位置
//                        break;
//                    }
//                    if (functionData.getFunctionName().equals(businessType + IDataPermissionForFacility.SELF_FACILITY)) {//_self_facility
//                        result.setCode(2);//表示分配了所在位置
//                        isSelfFacility = true;
//                    }
//                }
//                //点检需要独立判断，因为位置key组合问题
//                if (businessType.equals("spotcheck")) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("spot_all_facility")) {//
//                            isAllFacility = true;
//                            result.setCode(1);//表示分配了全部位置
//                            break;
//                        }
//                        if (functionData.getFunctionName().equals("spot_self_facility")) {//_self_facility
//                            result.setCode(2);//表示分配了所在位置
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//
//            }
//
//            if (isSelfFacility) {
//                //按用户权限，获取用户所能看见的位置
//                List<Facility> facilitiesList = facilitiesService.GetFacilityListByUser(schema_name, isAllFacility, user);
//                result.setContent(facilitiesList);
//            }
//            result.setMsg(user.getAccount());
//            return result;
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
}
