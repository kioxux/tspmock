package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.GroupModel;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.enumResp.ResultStatus;
//import com.gengyun.senscloud.service.system.CommonUtilService;
//import com.gengyun.senscloud.service.GroupService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * functino:用户组管理
// * author:Wudang Dong
// * time:2019-4-8
// */
//@RestController
//@RequestMapping("/userGroupManage")
public class UserGroupController {
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    GroupService groupService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    //用户组列表访问地址
//    @MenuPermission("user_group_manage")
//    @RequestMapping(value = "/index")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("user_group/index");
//        return mv;
//    }
//
//    //用户组新增页面地址
//    @MenuPermission("add_user_group")
//    @RequestMapping(value = "/add_user_group")
//    public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ModelAndView mv = new ModelAndView();
//        String idStr = request.getParameter("id");
//        String status = request.getParameter("status");
//        List<GroupModel> groupModelList = groupService.getGroup(schema_name);
//        TreeBuilder treeBuilder = new TreeBuilder();
//        List<Node> allNodes = new ArrayList<Node>();
//        for (GroupModel item : groupModelList) {
//            Node node = new Node();
//            node.setId(String.valueOf(item.getId()));
//            node.setParentId(String.valueOf(item.getGroup_code()));
//            node.setName(item.getGroup_name());
//            node.setDetail(item);
//            allNodes.add(node);
//        }
//        List<Node> roots = treeBuilder.buildListToTree(allNodes);
//        String groupSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//        List<User> userList = userService.getUserGroup(schema_name);
//        String duty_man = "";
//        StringBuffer stringBuffer = new StringBuffer();
//        if (userList != null && userList.size() > 0) {
//            for (int i = 0; i < userList.size(); i++) {
//                User user = userList.get(i);
//                stringBuffer.append("<option value=\"" + user.getAccount() + "\">" + user.getUsername() + "</option>");
//            }
//            duty_man = stringBuffer.toString();
//        }
//        if (idStr != null && !idStr.equals("")) {
//            int id = Integer.parseInt(idStr);
//            GroupModel groupModel = groupService.getGroupModelById(schema_name, id);
//            mv.setViewName("user_group/edit_user_group");
//            if (null != groupModel) {
//                List<Facility> facilityList = groupModel.getFacilities();
//                String title = "";
//                String code = "";
//                if (!facilityList.isEmpty()) {
//                    for (Facility facility : facilityList) {
//                        String title_val = facility.getTitle() + ",";
//                        title = title + title_val;
//                        String code_val = facility.getId() + ",";
//                        code = code + code_val;
//                    }
//                    title = title.substring(0, title.length() - 1);
//                    code = code.substring(0, code.length() - 1);
//                }
//                if (status != null && status.equals("edit")) {
//                    mv.addObject("group_id", groupModel.getId());
//                }
//                mv.addObject("duty_man", duty_man);
//                mv.addObject("positionCodes", StringUtils.join(groupModel.getPositionCodes(), ","));
//                mv.addObject("groupSelect", groupSelect);
//                mv.addObject("selectUser", groupModel.getDuty_man());
//                mv.addObject("belongGroup", groupModel.getParent_id());
//                mv.addObject("tipStatus", status);
//                mv.addObject("title", title);
//                mv.addObject("code", code);
//                mv.addObject("groupModel", groupModel);
//                mv.addObject("groupInfo", net.sf.json.JSONObject.fromObject(groupModel));
//            }
//        } else {
//            mv.addObject("groupSelect", groupSelect);
//            mv.addObject("duty_man", duty_man);
//            mv.setViewName("user_group/add_user_group");
//        }
//        return mv;
//    }
//
//    //用户组列表查询
//    @MenuPermission("user_group_manage")
//    @RequestMapping("/getGroupModelAll")
//    public String getGroupModelAll(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String condition = "";
//            String searchKey = request.getParameter("searchKey");
//
//            if (searchKey != null && !searchKey.equals("")) {
//                condition = "and g.group_name like  '%" + searchKey + "%'";
//            }
//            List<GroupModel> groupModels = groupService.getGroupModelAll(schema_name, condition);
//            int count = groupService.getGroupModelAllNum(schema_name, condition);
//            result.put("rows", groupModels);
//            result.put("total", count);
//            return result.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    //用户管理用户组列表查询
//    @MenuPermission("user_group_manage")
//    @RequestMapping("/getGroupUserModelAll")
//    public String getGroupUserModelAll(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String condition = "";
//            String searchKey = request.getParameter("searchKey");
//
//            if (searchKey != null && !searchKey.equals("")) {
//                condition = "and g.group_name like  '%" + searchKey + "%'";
//            }
//            List<GroupModel> groupModels = groupService.getGroupModelAll(schema_name, condition);
//            int count = groupService.getGroupModelAllNum(schema_name, condition);
//            result.put("rows", groupModels);
//            result.put("total", count);
//            result.put("code", 1);
//            return result.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    //编辑用户并查看用户信息
//    @MenuPermission("user_group_manage")
//    @RequestMapping("/getGroupModelById")
//    public ResponseModel getGroupModelById(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel model = new ResponseModel();
//        try {
//            String idStr = request.getParameter("id");
//            if (idStr != null && !idStr.equals("")) {
//                int id = Integer.parseInt(idStr);
//                GroupModel groupModel = groupService.getGroupModelById(schema_name, id);
//                if (groupModel != null) {
//                    model.setCode(StatusConstant.SUCCES_RETURN);
//                    model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_QUERY_GROUP_MODEL_SUCC));//对象数据查询成功
//                    model.setContent(groupModel);
//                } else {
//                    model.setCode(StatusConstant.NO_DATE_RETURN);
//                    model.setMsg(selectOptionService.getLanguageInfo(LangConstant.SRY_NDF));//没有查询到数据
//                }
//            } else {
//                model.setCode(StatusConstant.ERROR_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR));//没有获得系统参数
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return model;
//        }
//    }
//
//    //新建用户组
//    @MenuPermission("add_user_group")
//    @RequestMapping("/insertGroup")
//    public ResponseModel insertGroup(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        GroupModel group = new GroupModel();
//        try {
//            String groups = request.getParameter("groups");
//            int parentid = 0;
//            if (groups != null && groups != "") {
//                parentid = Integer.valueOf(groups);
//            }
//            String id_arr = request.getParameter("org_id");
//            if (StringUtils.isBlank(id_arr)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NOT_GET_ORG));//对不起没有获得负责组织
//            }
//            String position_code_arr = request.getParameter("position_code");
//            String[] position_code = null;
//            if (StringUtils.isBlank(position_code_arr)) {
////                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.CH_FACI));//请选择位置
//            } else {
//                position_code = position_code_arr.split(",");
//            }
//            String is_out = request.getParameter("is_out");
//            if (RegexUtil.isNotNull(is_out) && "true".equals(is_out)) {
//                group.setIs_out(true);//收费用户
//            } else {
//                group.setIs_out(false);//免费用户
//            }
//            String[] org_id = id_arr.split(",");
//            String group_name = request.getParameter("group_name");
//            String duty_man = request.getParameter("duty_man");
//            String remark = request.getParameter("remark");
//            group.setGroup_name(group_name);
//            group.setDuty_man(duty_man);
//            group.setRemark(remark);
//            group.setParent_id(parentid);
//            group.setCreate_user_account(loginUser.getAccount());
//            group.setGroup_code(commonUtilService.getMaxNoByParentId(schema_name, null, null, parentid,
//                    "_sc_group", "group_code", "parent_id",
//                    "id", null, null));
//            Timestamp create_time = new Timestamp(System.currentTimeMillis());
//            group.setCreate_time(create_time);
//            if (group != null) {
//                boolean add = groupService.insertGroup(schema_name, group, org_id, position_code);
//                if (add) {
//                    return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_USER_GROUP_SUCC));//新建用户组成功
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_USER_GROUP_FAIL));//新建用户组失败
//                }
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR));//没有获得系统参数
//            }
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_USER_GROUP_FAIL));//新建用户组失败
//        }
//    }
//
//    //编辑用户组信息
//    @MenuPermission("add_user_group")
//    @RequestMapping("/updateGroupOption")
//    public ResponseModel updateGroup(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        GroupModel group = new GroupModel();
//        try {
//            String id_arr = request.getParameter("org_id");
//            if (StringUtils.isBlank(id_arr)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NOT_GET_ORG));//对不起没有获得负责组织
//            }
//            String position_code_arr = request.getParameter("position_code");
//            String[] position_code = null;
//            if (StringUtils.isBlank(position_code_arr)) {
////                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.CH_FACI));//请选择位置
//            } else {
//                position_code = position_code_arr.split(",");
//            }
//            String is_out = request.getParameter("is_out");
//            if (RegexUtil.isNotNull(is_out) && "true".equals(is_out)) {
//                group.setIs_out(true);//收费用户
//            } else {
//                group.setIs_out(false);//免费用户
//            }
//            String[] org_id = id_arr.split(",");
//            String id_value = request.getParameter("group_id");
//            String group_name = request.getParameter("group_name");
//            String duty_man = request.getParameter("duty_man");
////            String status = request.getParameter("status");
//            String remark = request.getParameter("remark");
//            group.setParent_id(Integer.parseInt(request.getParameter("groups")));
//            group.setGroup_code(commonUtilService.getMaxNoByParentId(schema_name, null, null, Integer.parseInt(request.getParameter("groups")),
//                    "_sc_group", "group_code", "parent_id",
//                    "id", null, null));
//            group.setId(Integer.parseInt(id_value));
//            group.setGroup_name(group_name);
//            group.setDuty_man(duty_man);
//            group.setRemark(remark);
//
//            if (group != null) {
//                boolean add = groupService.updateGroup(schema_name, group, org_id, position_code);
//                if (add) {
//                    return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_GROUP_SUCC));//编辑用户组成功
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_GROUP_FAIL));//编辑用户组失败
//                }
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR));//没有获得系统参数
//            }
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_GROUP_FAIL));//编辑用户组失败
//        }
//    }
//
//    //禁用启用用户组状态
//    @MenuPermission("delGroupOpetion")
//    @RequestMapping("/updateGroupStatus")
//    public ResponseModel updateGroupStatus(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel model = new ResponseModel();
//        try {
//            String tipStr = selectOptionService.getLanguageInfo(LangConstant.DISABLE_A);//禁用
//            String idStr = request.getParameter("id");
//            String status = request.getParameter("status");
//            int status_int = 0;
//            if (status != null && status.equals("1")) {
//                status_int = 1;
//                tipStr = selectOptionService.getLanguageInfo(LangConstant.ENABLE_A);//启用
//            }
//            if (idStr != null && !idStr.equals("")) {
//                int id = Integer.parseInt(idStr);
//                int add = groupService.updateGroupStatus(schema_name, status_int, id);
//                if (add > 0) {
//                    model.setCode(StatusConstant.SUCCES_RETURN);
//                    model.setMsg(tipStr + selectOptionService.getLanguageInfo(LangConstant.MSG_USER_GROUP_SUCC));//用户组成功
//                } else {
//                    model.setCode(StatusConstant.NO_DATE_RETURN);
//                    model.setMsg(tipStr + selectOptionService.getLanguageInfo(LangConstant.MSG_USER_GROUP_FAIL));//用户组失败
//                }
//            } else {
//                model.setCode(StatusConstant.ERROR_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR));//没有获得系统参数
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return model;
//        }
//    }
//
//    @MenuPermission("delGroupOpetion")
//    @RequestMapping("/delGroupOpetion")
//    public ResponseModel delGroupOpetion(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String idStr = request.getParameter("id");
//            if (idStr != null && !idStr.equals("")) {
//                int id = Integer.parseInt(idStr);
//                boolean add = groupService.delGroup(schema_name, id);
//                if (add) {
//                    return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_USER_GROUP_SUCC));//用户组删除成功
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_USER_GROUP_FAIL));//用户组删除失败
//                }
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR));//没有获得系统参数
//            }
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_USER_GROUP_FAIL));//用户组删除失败
//        }
//    }
//
//    @MenuPermission("user_group_manage")
//    @RequestMapping({"/group_user_list"})
//    public ResponseModel findUser(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        List<User> userList = userService.getUserGroup(schema_name);
//        ResponseModel result = new ResponseModel();
//
//        result.setContent(userList);
//        result.setCode(ResultStatus.SUCCESS.getCode());
//        result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCCESS));//成功。
//
//        return result;
//    }
//
//    /**
//     * 获取用户组角色列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_group_manage")
//    @RequestMapping({"/getGroupRoleList"})
//    public ResponseModel getGroupRoleList(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String userGroupSearch = request.getParameter("userGroupSearch");
//            String roleNameSearch = request.getParameter("roleNameSearch");
//            List<Map<String, Object>> list = groupService.getGroupRoleList(schemaName, userGroupSearch, roleNameSearch);
//            return ResponseModel.ok(list);
//        } catch (Exception e) {
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
}
