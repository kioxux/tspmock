package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.auth.MenuPermission;
import com.gengyun.senscloud.auth.MenuPermissionRule;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.TaskItemService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 功能：计划工单controller
 * Created by Dong wudang on 2018/11/24.
 */
@Api(tags = "维保计划")
@RestController
public class PlanWorkController {

    @Resource
    PlanWorkService planWorkService;

    @ApiOperation(value = "获取维保计划模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_WORK_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchPlanWorkPermission", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkPermission(MethodParam methodParam) {
        return ResponseModel.ok(planWorkService.getPlanWorkPermission(methodParam));
    }

    @ApiOperation(value = "获取设备位置树列表-带用户权限查询", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_POSITION_TREE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchAssetPositionTree", method = RequestMethod.POST)
    public ResponseModel searchAssetPositionTree(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(planWorkService.getAssetPositionTree(methodParam, param));
    }

    @ApiOperation(value = "新增维保计划", notes = ResponseConstant.RSP_DESC_ADD_PLAN_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "plan_name","work_type_id","begin_time","end_time","priority_level","is_use","do_type","auto_generate_bill","isCreateSchedule","maintenanceObject","assistList","planJson","triggleInfo","conditionTriggle"})
    @RequestMapping(value = "/addPlanSchedule", method = RequestMethod.POST)
    @Transactional
    public ResponseModel addPlanSchedule(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PlanWorkAdd planWorkAdd) {
        planWorkService.newPlanSchedule(methodParam, paramMap, false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新维保计划", notes = ResponseConstant.RSP_DESC_EDIT_PLAN_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "plan_code","plan_name","work_type_id","begin_time","end_time","priority_level","do_type","auto_generate_bill","generate_time_point","isCreateSchedule","is_use","maintenanceObject","assistList","planJson","triggleInfo","conditionTriggle"})
    @RequestMapping(value = "/updatePlanSchedule", method = RequestMethod.POST)
    @Transactional
    public ResponseModel updatePlanSchedule(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PlanWorkAdd planWorkAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String plan_code) {
        planWorkService.newPlanSchedule(methodParam, paramMap, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "根据id查询维保计划详情", notes = ResponseConstant.RSP_DESC_GET_PLAN_WORK_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "plan_code"})
    @RequestMapping(value = "/searchPlanScheduleInfo", method = RequestMethod.POST)
    public ResponseModel searchPlanScheduleInfo(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String plan_code) {
        return ResponseModel.ok(planWorkService.getPlanScheduleInfo(methodParam, plan_code));
    }

    @ApiOperation(value = "查询维保计划列表", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_WORK_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber","keywordSearch","isUseSearch","workTypeIds","startDateSearch","endDateSearch","positionCode"})
    @RequestMapping(value = "/searchPlanScheduleList", method = RequestMethod.POST)
    public ResponseModel searchPlanScheduleList(MethodParam methodParam, SearchParam param) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(planWorkService.getPlanScheduleListForPage(methodParam, param));
    }

    @Transactional
    @ApiOperation(value = "启用禁用维保计划", notes = ResponseConstant.RSP_DESC_CHANGE_USE_PLAN_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "plan_code","is_use"})
    @RequestMapping(value = "/editPlanScheduleUse", method = RequestMethod.POST)
    public ResponseModel editPlanScheduleUse(MethodParam methodParam,@RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PlanWorkAdd planWorkAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String plan_code) {
        planWorkService.modifyPlanScheduleUse(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除选中维保计划（支持多选，逗号拼接）", notes = ResponseConstant.RSP_DESC_REMOVE_PLAN_WORK_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "planCodes"})
    @Transactional
    @RequestMapping(value="/removeSelectPlanSchedule", method = RequestMethod.POST)
    public ResponseModel removeSelectPlanSchedule(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PlanWorkAdd planWork ) {
        planWorkService.cutSelectPlanSchedule(methodParam,paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "维保计划查询设备对象根据设备位置、时间、类型、型号", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_ASSET_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch","asset_category_id","asset_model_id",
            "enable_begin_date","enable_end_date","positionCode","is_all_asset"})//"pageSize", "pageNumber",
    @RequestMapping(value = "/searchPlanWorkAssetByPositionCodeAndAssetType", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkAssetByPositionCodeAndAssetType(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(planWorkService.getPlanWorkAssetByPositionCodeAndAssetType(methodParam, param));
    }

    @ApiOperation(value = "维保计划查询任务模板列表", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_TASK_TEMPLATE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchPlanWorkTaskTemplateList", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkTaskTemplateList(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(planWorkService.getPlanWorkTaskTemplateList(methodParam, param));
    }

    @ApiOperation(value = "维保计划查询任务模板任务项列表", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_TASK_TEMPLATE_ITEM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "taskTemplateCodes","keywordSearch"})
    @RequestMapping(value = "/searchPlanWorkTaskTemplateItemList", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkTaskTemplateItemList(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(planWorkService.getPlanWorkTaskTemplateItemList(methodParam, param));
    }

    @ApiOperation(value = "标准任务项选择列表", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_TASK_ITEM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchPlanWorkTaskItemList", method = RequestMethod.POST)
    public ResponseModel searchPlanWorkTaskItemList(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(planWorkService.getPlanWorkTaskItemList(methodParam, param));
    }

}
