package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.AssetCategory;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.DashboardConfigData;
//import com.gengyun.senscloud.model.MetaDataAsset;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.asset.AssetCategoryService;
//import com.gengyun.senscloud.service.DashboardConfigService;
//import com.gengyun.senscloud.service.MetaDataAssetService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import net.sourceforge.jeval.EvaluationException;
//import net.sourceforge.jeval.Evaluator;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//

import org.springframework.web.bind.annotation.RestController;
@RestController
public class DashboardConfigController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    DashboardConfigService configService;
////    @Autowired
////    MetaDataAssetService assetService;
//@Autowired
//SelectOptionService selectOptionService;
//
//
//    @Autowired
//    private AssetCategoryService assetCategoryService;
//
//    @RequestMapping("/dashboard_config")
//    public ModelAndView DashboardConfig(HttpServletRequest request, HttpServletResponse response) {
//        //看板配置
//        return new ModelAndView("dashboard_config");
//    }
//
//    @RequestMapping("/dashboard_config_edit")
//    public ModelAndView DashboardConfigEdit(HttpServletRequest request, HttpServletResponse response) {
//        //看板配置维护
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Integer id = Integer.parseInt(request.getParameter("id"));
//            DashboardConfigData data = configService.findDashboardConfigData(schema_name, id);
//            result.setContent(data);
//            result.setCode(1);
//            result.setMsg("");
//
//            //找到资产信息
//            List<AssetCategory> assetList = assetCategoryService.findAllCategory(schema_name);
//
//            return new ModelAndView("dashboard_config_edit").addObject("result", result).addObject("assetList", assetList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_B_T_ERROR) + ex.getMessage());
//            return new ModelAndView("dashboard_config_edit").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得所有的看板配置
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/dashboard_config_list")
//    public ModelAndView DashboardConfigFindALL(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<DashboardConfigData> dataList = configService.findAllDashboardConfigList(schema_name);
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            return new ModelAndView("dashboard_config_list").addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_B_T_ERROR) + ex.getMessage());
//            return new ModelAndView("dashboard_config_list").addObject("result", result);
//        }
//    }
//
//    @RequestMapping("/dashboard_config_add")
//    public ResponseModel getAddDashboardConfig(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        String title = request.getParameter("title");
//        String snippet = request.getParameter("snippet");
//        String showtype = request.getParameter("showtype");
//        String date_type = request.getParameter("datetype");
//        String description = request.getParameter("description");
//        String boardtype = request.getParameter("boardtype");
//        if (title == null || title.isEmpty() || snippet == null || snippet.isEmpty()) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SHORT_TITLE_EMTRY));
//        } else if (StringUtils.isEmpty(showtype)) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_STATI_TI_EMPTY));
//        } else {
//            StringBuffer sb = new StringBuffer() ;
//            Pattern pattern = Pattern.compile(SensConstant.DASHBOARD_REGEX);
//            Matcher matcher = pattern.matcher(showtype);
//            while (matcher.find()) {
//                matcher.appendReplacement(sb, String.valueOf(Math.random() * 31 + 17));
//            }
//            matcher.appendTail(sb);
//            Evaluator evaluator = new Evaluator();
//            try {
//                evaluator.evaluate(sb.toString());
//            } catch (EvaluationException e) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_STATI_FORMULA_ERR));
//                return result;
//            }
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //保存看板配置
//            int count = configService.AddDashboardConfig(schema_name, title, snippet, showtype, date_type, description, "{}", boardtype);
//            if (count > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_CONF_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_CONF_FA));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @RequestMapping("/dashboard_config_edit_save")
//    public ResponseModel getEditDashboardConfig(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("id"));
//        String title = request.getParameter("title");
//        String snippet = request.getParameter("snippet");
//        String showtype = request.getParameter("showtype");
//        String date_type = request.getParameter("datetype");
//        String description = request.getParameter("description");
//        String boardtype = request.getParameter("boardtype");
//        if (StringUtils.isEmpty(title) || StringUtils.isEmpty(snippet)) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SHORT_TITLE_EMTRY));
//        } else if (StringUtils.isEmpty(showtype)) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_STATI_TI_EMPTY));
//        } else {
//            StringBuffer sb = new StringBuffer() ;
//            Pattern pattern = Pattern.compile(SensConstant.DASHBOARD_REGEX);
//            Matcher matcher = pattern.matcher(showtype);
//            while (matcher.find()) {
//                matcher.appendReplacement(sb, String.valueOf(Math.random() * 31 + 17));
//            }
//            matcher.appendTail(sb);
//            Evaluator evaluator = new Evaluator();
//            try {
//                evaluator.evaluate(sb.toString());
//            } catch (EvaluationException e) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_STATI_FORMULA_ERR));
//                return result;
//            }
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //保存看板配置
//            int count = configService.EditDashboardConfig(schema_name, id, title, snippet, showtype, date_type, description, boardtype);
//            if (count > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_BOARD_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_BOARD_FA));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @RequestMapping("/dashboard_config_delete")
//    public ResponseModel getDeleteDashboardConfig(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("id"));
//        if (id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_BOARD_NO_CHOICE));
//        } else {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //删除看板配置
//            int count = configService.DeleteDashboardConfigList(schema_name, id);
//            if (count > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_BOARD_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_BOARD_FA));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @RequestMapping("/dashboard_config_get_asset_column")
//    public ResponseModel getAssetColumn(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        String asset = request.getParameter("asset");
//        if (asset == null || asset.isEmpty()) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_ERR_FIELD));
//        } else {
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            String columnFields = "";
//            //找到资产信息
//            List<AssetCategory> assetList = assetCategoryService.findAllCategory(schema_name);
//            for (AssetCategory data : assetList) {
//                if (asset.equals(data.getCategory_name())) {
//                    columnFields = data.getFields();
//                    break;
//                }
//            }
//            if ("".equals(columnFields) || columnFields == null) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_ERR_FIELD));
//            } else {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NOT_GET_FIELD));
//                result.setContent(columnFields);
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @RequestMapping("/dashboard_config_datasource_save")
//    public ResponseModel getEditDashboardDatasource(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("id"));
//        String datasource = request.getParameter("datasource");
//        if (id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_DATA_CHECK));
//        } else {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //保存看板配置
//            int count = configService.EditDashboardCondition(schema_name, id, datasource);
//            if (count > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_CONF_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_CONF_FA));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    /**
//     * 获得所有的看板配置
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/get_dashboard_config_list")
//    public ResponseModel GetDashBoardConfigList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<DashboardConfigData> dataList = configService.findAllDashboardConfigList(schema_name);
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_B_T_ERROR) + ex.getMessage());
//            return result;
//        }
//    }
}
