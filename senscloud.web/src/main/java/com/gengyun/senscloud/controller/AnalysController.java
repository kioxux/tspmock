package com.gengyun.senscloud.controller;


//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.*;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.view.AnalysBomDataView;
//import com.gengyun.senscloud.view.AnalysMaintainDataExportView;
//import com.gengyun.senscloud.view.AnalysRepairDataExportView;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
//@RequestMapping("/analys")
public class AnalysController {
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    AnalysReportService analysReportService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    /*****统计部分****************************************************************************/
//    @MenuPermission("statistic")
//    @RequestMapping("/repair_analys")
//    public ModelAndView InitRepairAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "repair_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("repair_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("repair_anlys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("analys/repair_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_RE_STA);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("analys/repair_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    /**
//     * 获得所有的供应商
//     */
//    @MenuPermission("statistic")
//    @RequestMapping("find-all-supplier-for-analys")
//    public ResponseModel querySupplierForAnalys(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            // yzj 0830，清除customer表，导致bug，大家各自修改  --  修改好删除此注释
////            List<CustomersData> customersList = customerService.findAllCustomerList3(schema_name);
//            List<SupplierResult> supplierList = new ArrayList<>();
////            for (CustomersData data : customersList) {
////                SupplierResult result = new SupplierResult();
////                result.setSupplierId(data.getId());
////                result.setSupplierName(data.getCustomerName());
////                supplierList.add(result);
////            }
//            return ResponseModel.ok(supplierList);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_SUPP);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    //统计维修时效，按位置、设备和人三种情况进行统计
//    @MenuPermission("statistic")
//    @RequestMapping("/repair-analys-list")
//    public String analysRepairWork(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String condition = "";
//            String analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//            String groupColumn = "f.facilitycode,f.title";
//            switch (analysType) {
//                case "facility":
//                    analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//                    groupColumn = "f.facilitycode,f.title";
//                    condition += " and (f.title like '%" + keyWord + "%' or f.facilitycode like '%" + keyWord + "%') ";
//                    break;
//                case "device":
//                    analysColumn = "dv.strcode as analysCode,dv.strname as analysName";
//                    groupColumn = "dv.strcode,dv.strname";
//                    condition += " and (dv.strcode like '%" + keyWord + "%' or dv.strmodel like '%" + keyWord + "%' or dv.strname like '%" + keyWord + "%') ";
//                    break;
//                case "user":
//                    analysColumn = "r.receive_account as analysCode,u.username as analysName";
//                    groupColumn = "r.receive_account,u.username";
//                    condition += " and (r.receive_account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//                    break;
//                case "supplier":
//                    analysColumn = "dv.supplier as analysCode,c.customer_name as analysName";
//                    groupColumn = "dv.supplier,c.customer_name";
//                    condition += " and (c.customer_name like '%" + keyWord + "%') ";
//                    break;
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.finished_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.finished_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "repair_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("repair_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("repair_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and r.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.receive_account='" + account + "' or r.distribute_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.receive_account='" + account + "' or r.distribute_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and r.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysRepairWorkResult> repairReportList = analysReportService.GetRepairHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//            int total = analysReportService.GetRepairHoursReportCount(schema_name, condition, groupColumn);
//
//            result.put("rows", repairReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //维修统计导出
//    @MenuPermission("statistic")
//    @RequestMapping("/repair-analys-export")
//    public ModelAndView ExportRepairAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            int pageSize = 10000;
//            int pageNumber = 1;
//
//            String condition = "";
//            String analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//            String groupColumn = "f.facilitycode,f.title";
//            switch (analysType) {
//                case "device":
//                    analysColumn = "dv.strcode as analysCode,dv.strname as analysName";
//                    groupColumn = "dv.strcode,dv.strname";
//                    condition += " and (dv.strcode like '%" + keyWord + "%' or dv.strmodel like '%" + keyWord + "%' or dv.strname like '%" + keyWord + "%') ";
//                    break;
//                case "facility":
//                    analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//                    groupColumn = "f.facilitycode,f.title";
//                    condition += " and (f.title like '%" + keyWord + "%' or f.facilitycode like '%" + keyWord + "%') ";
//                    break;
//                case "user":
//                    analysColumn = "r.receive_account as analysCode,u.username as analysName";
//                    groupColumn = "r.receive_account,u.username";
//                    condition += " and (r.receive_account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//                    break;
//                case "supplier":
//                    analysColumn = "dv.supplier as analysCode,c.customer_name as analysName";
//                    groupColumn = "dv.supplier,c.customer_name";
//                    condition += " and (c.customer_name like '%" + keyWord + "%') ";
//                    break;
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.finished_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.finished_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "repair_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("repair_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("repair_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and r.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.receive_account='" + account + "' or r.distribute_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.receive_account='" + account + "' or r.distribute_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and r.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysRepairWorkResult> repairReportList = analysReportService.GetRepairHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairAnalys", repairReportList);
//            map.put("analysType", analysType);
//            map.put("selectOptionService", selectOptionService);
//            //map.put("name", "魅力城市");
//            AnalysRepairDataExportView excelView = new AnalysRepairDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //保养统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/maintain_analys")
//    public ModelAndView InitMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "maintain_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("maintain_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("maintain_anlys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("analys/maintain_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_RE_STA);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("analys/maintain_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    //统计保养时效，按位置、设备和人三种情况进行统计
//    @MenuPermission("statistic")
//    @RequestMapping("/maintain-analys-list")
//    public String analysMaintainWork(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String condition = "";
//            String analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//            String groupColumn = "f.facilitycode,f.title";
//            switch (analysType) {
//                case "facility":
//                    analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//                    groupColumn = "f.facilitycode,f.title";
//                    condition += " and (f.title like '%" + keyWord + "%' or f.facilitycode like '%" + keyWord + "%') ";
//                    break;
//                case "device":
//                    analysColumn = "dv.strcode as analysCode,dv.strname as analysName";
//                    groupColumn = "dv.strcode,dv.strname";
//                    condition += " and (dv.strcode like '%" + keyWord + "%' or dv.strmodel like '%" + keyWord + "%' or dv.strname like '%" + keyWord + "%') ";
//                    break;
//                case "user":
//                    analysColumn = "m.maintain_account as analysCode,u.username as analysName";
//                    groupColumn = "m.maintain_account,u.username";
//                    condition += " and (m.maintain_account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//                    break;
//                case "supplier":
//                    analysColumn = "dv.supplier as analysCode,c.customer_name as analysName";
//                    groupColumn = "dv.supplier,c.customer_name";
//                    condition += " and (c.customer_name like '%" + keyWord + "%') ";
//                    break;
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and m.finished_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and m.finished_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "maintain_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("maintain_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("maintain_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and m.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (m.maintain_account='" + account + "' or m.audit_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (m.maintain_account='" + account + "' or m.audit_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and m.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysRepairWorkResult> repairReportList = analysReportService.GetMaintainHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//            int total = analysReportService.GetMaintainHoursCount(schema_name, condition, groupColumn);
//
//            result.put("rows", repairReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //保养统计导出
//    @MenuPermission("statistic")
//    @RequestMapping("/maintain-analys-export")
//    public ModelAndView ExportMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String analysType = request.getParameter("analysType");
//            int pageSize = 10000;
//            int pageNumber = 1;
//
//            String condition = "";
//            String analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//            String groupColumn = "f.facilitycode,f.title";
//            switch (analysType) {
//                case "device":
//                    analysColumn = "dv.strcode as analysCode,dv.strname as analysName";
//                    groupColumn = "dv.strcode,dv.strname";
//                    condition += " and (dv.strcode like '%" + keyWord + "%' or dv.strmodel like '%" + keyWord + "%' or dv.strname like '%" + keyWord + "%') ";
//                    break;
//                case "facility":
//                    analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//                    groupColumn = "f.facilitycode,f.title";
//                    condition += " and (f.title like '%" + keyWord + "%' or f.facilitycode like '%" + keyWord + "%') ";
//                    break;
//                case "user":
//                    analysColumn = "m.maintain_account as analysCode,u.username as analysName";
//                    groupColumn = "m.maintain_account,u.username";
//                    condition += " and (m.maintain_account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//                    break;
//                case "supplier":
//                    analysColumn = "dv.supplier as analysCode,c.customer_name as analysName";
//                    groupColumn = "dv.supplier,c.customer_name";
//                    condition += " and (c.customer_name like '%" + keyWord + "%') ";
//                    break;
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and m.finished_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and m.finished_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "maintain_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("maintain_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("maintain_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and m.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (m.maintain_account='" + account + "' or m.audit_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (m.maintain_account='" + account + "' or m.audit_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and m.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysRepairWorkResult> maintainReportList = analysReportService.GetMaintainHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("maintainAnalys", maintainReportList);
//            map.put("analysType", analysType);
//            map.put("selectOptionService", selectOptionService);
//            //map.put("name", "魅力城市");
//            AnalysMaintainDataExportView excelView = new AnalysMaintainDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//
//    //巡检统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/inspection_analys")
//    public ModelAndView InitInspectionAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "inspection_analys");
//            return new ModelAndView("analys/inspection_analys").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_RE_STA);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("analys/inspection_analys").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //统计巡检时效，按位置、巡检区域和人三种情况进行统计
//    @MenuPermission("statistic")
//    @RequestMapping("/inspection-analys-list")
//    public String analysInspectionWork(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String condition = "";
//            String analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//            String groupColumn = "f.facilitycode,f.title";
//            switch (analysType) {
//                case "facility":
//                    analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//                    groupColumn = "f.facilitycode,f.title";
//                    condition += " and (f.title like '%" + keyWord + "%' or f.facilitycode like '%" + keyWord + "%') ";
//                    break;
//                case "area":
//                    analysColumn = "ae.area_code as analysCode,ae.area_name as analysName";
//                    groupColumn = "ae.area_code,ae.area_name";
//                    condition += " and (ae.area_code like '%" + keyWord + "%' or ae.area_name like '%" + keyWord + "%' ) ";
//                    break;
//                case "user":
//                    analysColumn = "s.inspection_account as analysCode,u.username as analysName";
//                    groupColumn = "s.inspection_account,u.username";
//                    condition += " and (s.inspection_account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//                    break;
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and s.inspection_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and s.inspection_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("inspection_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("inspection_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and s.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (s.inspection_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (s.inspection_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and s.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysRepairWorkResult> repairReportList = analysReportService.GetInspectionHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//            int total = analysReportService.GetInspectionHoursCount(schema_name, condition, groupColumn);
//
//            result.put("rows", repairReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //点检统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/spot_analys")
//    public ModelAndView InitSpotAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "spot_analys");
//            return new ModelAndView("analys/spot_analys").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_RE_STA);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("analys/spot_analys").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //统计巡检时效，按位置、点检区域和人三种情况进行统计
//    @MenuPermission("statistic")
//    @RequestMapping("/spot-analys-list")
//    public String analysSpotWork(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String condition = "";
//            String analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//            String groupColumn = "f.facilitycode,f.title";
//            switch (analysType) {
//                case "facility":
//                    analysColumn = "f.facilitycode as analysCode,f.title as analysName";
//                    groupColumn = "f.facilitycode,f.title";
//                    condition += " and (f.title like '%" + keyWord + "%' or f.facilitycode like '%" + keyWord + "%') ";
//                    break;
//                case "area":
//                    analysColumn = "ae.area_code as analysCode,ae.area_name as analysName";
//                    groupColumn = "ae.area_code,ae.area_name";
//                    condition += " and (ae.area_code like '%" + keyWord + "%' or ae.area_name like '%" + keyWord + "%' ) ";
//                    break;
//                case "user":
//                    analysColumn = "s.spot_account as analysCode,u.username as analysName";
//                    groupColumn = "s.spot_account,u.username";
//                    condition += " and (s.spot_account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//                    break;
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and s.spot_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and s.spot_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "spot_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("spot_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("spot_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and s.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (s.spot_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (s.spot_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and s.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysRepairWorkResult> repairReportList = analysReportService.GetSpotHoursReport(schema_name, condition, analysColumn, groupColumn, pageSize, begin);
//            int total = analysReportService.GetSpotHoursCount(schema_name, condition, groupColumn);
//
//            result.put("rows", repairReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @MenuPermission("statistic")
//    @RequestMapping("/bom_analys")
//    public ModelAndView InitBomAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "bom_analys");
//            return new ModelAndView("analys/bom_analys").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_BOM);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("analys/bom_analys").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    @MenuPermission("statistic")
//    @RequestMapping("/bom-analys-list")
//    public String analysBomWork(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String condition = "";
//            if (keyWord != null && !keyWord.equals("")) {
//                condition += "where (b.bom_model like '%" + keyWord + "%' or b.bom_name like '%" + keyWord + "%') ";
//            }
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "bom_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("bom_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("bom_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        if (keyWord != null && !keyWord.equals("")) {
//                            condition += " and b.facility_id in (" + facilityIds + ") ";
//                        } else {
//                            condition += " where b.facility_id in (" + facilityIds + ") ";
//                        }
//
//
//                    } else {
//                        //condition += " and (mb.maintain_account='" + account + "' or mb.audit_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    //condition += " and (mb.maintain_account='" + account + "' or mb.audit_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                if (keyWord != null && !keyWord.equals("")) {
//                    condition += " and b.facility_id in (" + facilityId + subFacility + ") ";
//                } else {
//                    condition += " where b.facility_id in (" + facilityId + subFacility + ") ";
//                }
//
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<BomData> bomDataList = analysReportService.getBom(schema_name, condition, pageSize, begin);
//            int total = analysReportService.getBomCount(schema_name, condition);
//            result.put("total", total);
//            result.put("rows", bomDataList);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //备件导出
//    //维修统计导出
//    @MenuPermission("statistic")
//    @RequestMapping("/bom-analys-export")
//    public ModelAndView ExportBomAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            int pageSize = 10000;
//            int pageNumber = 1;
//            String condition = "";
//            condition += " and (b.bom_model like '%" + keyWord + "%' or b.bom_name like '%" + keyWord + "%') ";
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("bom_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("bom_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and mb.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        //condition += " and (mb.maintain_account='" + account + "' or mb.audit_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    //condition += " and (mb.maintain_account='" + account + "' or mb.audit_account='" + account + "' )";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and mb.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<BomData> bomDataList = analysReportService.getBom(schema_name, condition, pageSize, begin);
//            //时效结果
//            //
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("bomAnalys", bomDataList);
//            //map.put("name", "魅力城市");
//            map.put("selectOptionService", selectOptionService);
//            AnalysBomDataView excelView = new AnalysBomDataView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 根据设备型号和库房编号查询保养明细
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("statistic")
//    @RequestMapping("/sc_maintain_bom_detail")
//    public String mainBomDetail(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 备件统计保养使用数量明细查询by bom_model and stock_code
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("statistic")
//    @RequestMapping("/find-maintain-bom-detail")
//    public ResponseModel maintainBomDetail(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String bom_model = request.getParameter("bom_model");
//            String stock_code = request.getParameter("stock_code");
//            String condition="and(wt.business_type_id=2)";
//            if(null!=bom_model&&!"".equals(bom_model)){
//                condition+="and(b.bom_model='"+bom_model+"')";
//            }
//            if(null!=stock_code&&!"".equals(stock_code)){
//                condition+="and(b.stock_code='"+stock_code+"')";
//            }
//            List<WorkSheet> list = repairService.getRepairDetailByBomModelAndStockCode(schema_name,condition);
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 备件统计维修使用数量明细查询by bom_model and stock_code
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("statistic")
//    @RequestMapping("/find-repair-bom-detail")
//    public ResponseModel repairBomDetail(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String bom_model = request.getParameter("bom_model");
//            String stock_code = request.getParameter("stock_code");
//            String condition="and(wt.business_type_id=1)";
//            if(null!=bom_model&&!"".equals(bom_model)){
//                condition+="and(b.bom_model='"+bom_model+"')";
//            }
//            if(null!=stock_code&&!"".equals(stock_code)){
//                condition+="and(b.stock_code='"+stock_code+"')";
//            }
//            List<WorkSheet> list = repairService.getRepairDetailByBomModelAndStockCode(schema_name,condition);
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @MenuPermission("statistic")
//    @RequestMapping("/bom_consume_analys")
//    public ModelAndView bomConsumeAnalys(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "bom_consume_analys");
//            return new ModelAndView("analys/bom_consume_analys").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            return new ModelAndView("analys/bom_consume_analys").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //人员绩效统计
//    @MenuPermission("statistic")
//    @RequestMapping("/user_performance_analys")
//    public ModelAndView userPerformanceAnalys(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canSeeSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "user_performance_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("user_performance_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("user_performance_analys_self_facility")) {
//                        canSeeSelfFacility = true;
//                    }
//                }
//            }
//            return new ModelAndView("analys/user_performance_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canSeeSelfFacility", canSeeSelfFacility);
//        } catch (Exception ex) {
//            return new ModelAndView("analys/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//    //brose看板
//    @RequestMapping("/brose")
//    public ModelAndView broseAnalys(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_analys");
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//    @RequestMapping("/brose2")
//    public ModelAndView broseAnalys2(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_analys2");
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//    @RequestMapping("/brose3")
//    public ModelAndView broseAnalys3(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_analys3");
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//    @RequestMapping("/brose_table")
//    public ModelAndView broseTable(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_table");
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//
//    @RequestMapping("/brose_map")
//    public ModelAndView broseMap(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_map");
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//    @RequestMapping("/brose_utilization")
//    public ModelAndView broseUtilization(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_utilization");
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//    @RequestMapping("/brose_real_time_data")
//    public ModelAndView broseRealtimeData(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String title=request.getParameter("code");
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            return new ModelAndView("brose/brose_real_time_data").addObject("title",title);
//        } catch (Exception ex) {
//            return new ModelAndView("brose/user_performance_analys").addObject("canSeeAllFacility", false).addObject("canSeeSelfFacility", false);
//        }
//    }
//
//    /**
//     * 获得用户任务完成率的排名，前50
//     */
//    @RequestMapping("user_finished_task_rate")
//    public ResponseModel queryUserFinishedTaskRate(HttpServletRequest request, HttpServletResponse response) {
//        User user = authService.getLoginUser(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//
//
//            String condition = "";
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and task.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and task.createtime <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("-1")) {
//
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "user_performance_analys");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("user_performance_analys_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("user_performance_analys_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and task.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (task.user_account='" + account + "')";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and (task.user_account='" + account + "')";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and task.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            List<AnalysRepariMaintainTaskCountResult> finishedTaskRateList = analysReportService.getUserTaskCount(schema_name, condition);
//            return ResponseModel.ok(finishedTaskRateList);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_SEACH);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    @MenuPermission("statistic")
//    @RequestMapping("/asset_fault")
//    public ModelAndView assetStatus(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "bom_consume_analys");
//            return new ModelAndView("analys/asset_status").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            return new ModelAndView("analys/asset_status").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //设备状态-故障率
//    @MenuPermission("statistic")
//    @RequestMapping("/asset_status_analy")
//    public ResponseModel equipmentFailureRates(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String assetType = request.getParameter("assetType");
//            String orderBy = "fault_rate";//故障率
//            String deviceFunctionType = "asset_fault_analys";
//            String type = request.getParameter("type");
//
//            int pageSize = 20;
//
//            if (orderBy == null || orderBy.isEmpty()) {
//                orderBy = "fault_rate";
//            }
//
//
//            String facilityCondition = " ";
//            String facility = "";
//            String repairCondition = " ";
//            String maintainCondition = " ";
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and r_run.occur_time >='" + beginDate + "'";
//                maintainCondition += " and mt.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and r_run.occur_time <'" + end + "'";
//                maintainCondition += " and mt.createtime <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0") || facilityId.equals("-1")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), deviceFunctionType);
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    facilityCondition += " ";
//                    facility += " ";
//
//                } else {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        facilityCondition += " and dv.intsiteid in (" + facilityIds + ") ";
//                        facility += " and f.id in (" + facilityIds + ") ";
//                    } else {
//                        facilityCondition += " and dv.intsiteid in () ";    //没有用户所属位置，则按个人权限查询
//                        facility += " and f.id in () ";
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                facilityCondition += " and dv.intsiteid in (" + facilityId + subFacility + ") ";
//                facility += " and f.id in (" + facilityId + subFacility + ") ";
//            }
//            AssetFault assetFault = new AssetFault();
//            int times = analysReportService.failurTimes(schema_name, facilityCondition, repairCondition, maintainCondition);//故障次数
//            int allTimes = analysReportService.allTime(schema_name, facilityCondition, repairCondition, maintainCondition);
////            long minMTBF = analysReportService.MTBF(schema_name, facilityCondition, repairCondition, maintainCondition);
//            long minRestore = analysReportService.meanTimeToRestore(schema_name, facilityCondition, repairCondition, maintainCondition);
//            double minMTTR = analysReportService.MTTR(schema_name, facility, beginTime, endTime);
//
//            assetFault.setTimes(times);
//            assetFault.setAllTimes(allTimes);
////            assetFault.setMinMTBF(minMTBF);
//            assetFault.setMinRestore(minRestore);
//            assetFault.setMinMTTR(minMTTR);
////            List<SupplierChartResult> supplierChartData1 = analysChartService.MTBF(schema_name, facilityCondition, repairCondition, maintainCondition, orderBy);break;//平均故障间隔时间
//
//
//            result.setCode(1);
//            result.setContent(assetFault);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//
//    //设备状态-故障率
//    @MenuPermission("statistic")
//    @RequestMapping("/asset_status_analy_mbtf")
//    public ResponseModel equipmentFailureMbtfRates(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String orderBy = "fault_rate";//故障率
//            String deviceFunctionType = "asset_fault_analys";
//            String type = request.getParameter("type");
//
//            int pageSize = 20;
//
//            if (orderBy == null || orderBy.isEmpty()) {
//                orderBy = "fault_rate";
//            }
//
//
//            String facilityCondition = " ";
//            String facility = "";
//            String repairCondition = " ";
//            String maintainCondition = " ";
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and r_run.occur_time >='" + beginDate + "'";
//                maintainCondition += " and mt.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and r_run.occur_time <'" + end + "'";
//                maintainCondition += " and mt.createtime <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0") || facilityId.equals("-1")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), deviceFunctionType);
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    facilityCondition += " ";
//                    facility += " ";
//
//                } else {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        facilityCondition += " and dv.intsiteid in (" + facilityIds + ") ";
//                        facility += " and f.id in (" + facilityIds + ") ";
//                    } else {
//                        facilityCondition += " and dv.intsiteid in () ";    //没有用户所属位置，则按个人权限查询
//                        facility += " and f.id in () ";
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                facilityCondition += " and dv.intsiteid in (" + facilityId + subFacility + ") ";
//                facility += " and f.id in (" + facilityId + subFacility + ") ";
//            }
//            AssetFault assetFault = new AssetFault();
//            long minMTBF = analysReportService.MTBF(schema_name, facilityCondition, repairCondition, maintainCondition);
//            assetFault.setMinMTBF(minMTBF);
//            result.setCode(1);
//            result.setContent(assetFault);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//    //设备状态-故障率
//
//    //设备状态-平均维修响应时间
//
//    //设备状态


}
