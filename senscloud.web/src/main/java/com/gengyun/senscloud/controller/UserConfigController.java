package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.model.company_model.CompanyHireInfoModel;
//import com.gengyun.senscloud.response.JsonObjectResult;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.AESUtils;
//import com.gengyun.senscloud.util.IdGenerator;
//import com.gengyun.senscloud.view.UserConfigDataExportView;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;

//@RestController
//@RequestMapping("/user_config")
public class UserConfigController {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    UserRoleService userRoleService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @MenuPermission("user_config")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean AllGroup = true;//权限判断不需要先不控制 2019-06-20 10：39
//            Boolean SelfGroup = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "user_config");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("user_config_all_group")) {
//                        AllGroup = true;
//                    } else if (functionData.getFunctionName().equals("user_config_self_group")) {
//                        SelfGroup = true;
//                    }
//                    if (AllGroup) {
//                        break;
//                    }
//                }
//            }
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("reset_user", "resetFlag");//用户还原
//                put("user_config_export", "canExportData");//导出
//            }};
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "user_config/index", "user_config");
//            return model.addObject("AllGroup", AllGroup).addObject("SelfGroup", SelfGroup).addObject("user_key", user.getId());
//        } catch (Exception ex) {
//            return new ModelAndView("user_config/index").addObject("AllGroup", false).addObject("SelfGroup", false).addObject("user_id", null)
//                    .addObject("canExportData", false);
//        }
//    }
//
//    @MenuPermission("user_config")
//    @RequestMapping({"/search"})
//    public String search(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            JSONObject result = new JSONObject();
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User loginUser = authService.getLoginUser(request);
////            boolean isAllGroup = true;//权限判断不需要先不控制 2019-06-20 10：39
////            boolean SelfGroup = false;
////            //查看员工是否能看所有的位置
////            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "user_config");
////            if (userFunctionList != null || !userFunctionList.isEmpty()) {
////                for (UserFunctionData functionData : userFunctionList) {
////                    //如果可以查看全部位置，则不限制
////                    if (functionData.getFunctionName().equals("user_config_all_group")) {
////                        isAllGroup = true;
////                        break;
////                    }else if(functionData.getFunctionName().equals("user_config_self_group")){
////                        SelfGroup=true;
////                    }
////                }
////            }
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String keyword = request.getParameter("keyword");
//            String user_groups = request.getParameter("user_groups");
//            String tempRoles = request.getParameter("roles");
//            String is_out = request.getParameter("is_out");
//            String groupCondition = " 1=1 ";
//            String roleCondition = " 1=1 ";
//            String condition = " 1=1 ";
//            if (is_out != null && is_out.equals("1")) {
//                condition += "and  t1.is_out=" + false;
//            } else if (is_out != null && is_out.equals("0")) {
//                condition += "and t1.is_out=" + true;
//            }
//            condition += " and ";
//            boolean isSearchAllGroup = false;
//            boolean isSearchAllRoles = false;
//            if (tempRoles != null && !tempRoles.isEmpty() && tempRoles.length() > 0) {
//                String[] tempRoleList = tempRoles.split(",");
//                String roleSearch = "'-2'";
//                for (String s : tempRoleList) {
//                    roleSearch += ",'" + s + "'";
//                    if (s.equals("-1")) {
//                        isSearchAllRoles = true;
//                        break;
//                    }
//                }
//                if (!isSearchAllRoles) {
//                    roleCondition = " r.role_id in (" + roleSearch + ")";
//                }
//            }
////            if (!isAllGroup) {
////                if (SelfGroup) {
////                    groupCondition += "and  t3.id in (select st.id from ${schema_name}._sc_group st LEFT JOIN ${schema_name}._sc_user_group ut on ut.group_id = st. ID " +
////                            "where ut.user_id='" + loginUser.getId() + "') ";
////                } else {
////                    groupCondition += " and t1.id ='" + loginUser.getId() + "'";
////                }
////            }
//            if (user_groups != null && !user_groups.isEmpty() && user_groups.length() > 0) {
//                String[] user_group = user_groups.split(",");
//                String groupSearch = "'-2'";
//                for (String s : user_group) {
//                    groupSearch += ",'" + s + "'";
//                    if (s.equals("-1")) {
//                        isSearchAllGroup = true;
//                        break;
//                    }
//                }
//                if (!isSearchAllGroup) {
//                    groupCondition += " and t3.id in (" + groupSearch + ")";
//                }
//            }
//            if (RegexUtil.isNull(keyword)) {
//                keyword = "";
//            }
//            keyword = "%" + keyword + "%";
//            int begin = pageSize * (pageNumber - 1);
//            boolean hasResetUserPermission = pagePermissionService.getPermissionByKey(schema_name, loginUser.getId(), "user_config", "reset_user");
//            if (hasResetUserPermission) {
//                condition += " t1.status > " + User.STATUS_CANCELLED + " and ";
//            } else {//没有还原权限的用户，只能查询非删除状态的用户
//                condition += " t1.status > " + User.STATUS_DELETED + " and ";
//            }
//
//            List<User> userListByPage = userService.searchUserByPage(schema_name, condition, groupCondition, roleCondition, keyword, pageSize, begin);
////            if (null == userListByPage || 0 == userListByPage.size()) {
////                return null;
////            }
//            int total = userService.searchUserByPageCount(schema_name, condition, groupCondition, roleCondition, keyword);
//            result.put("rows", userListByPage);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping({"/log_off_user"})
//    public ResponseModel logOffUser(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = authService.getLoginUser(request);
//        try {
//            userService.logOffUser(schema_name, loginUser.getId(), loginUser.getAccount());
//            return ResponseModel.ok("success");
//        } catch (Exception e) {
//            return ResponseModel.ok("error");
//        }
//    }
//
//    @MenuPermission("add_user")
//    @RequestMapping({"/add_user"})
//    public ModelAndView addUser(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = authService.getLoginUser(request);
//        List<UserRole> roleList = userRoleService.findAllCustomRole(schema_name);
//        String optionRole = userRoleService.optionRole(roleList, schema_name, loginUser, "add");
//        List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//        TreeBuilder treeBuilder = new TreeBuilder();
//        List<Node> allNodes = new ArrayList<Node>();
//        for (Facility item : facilitiesList) {
//            Node node = new Node();
//            node.setId(String.valueOf(item.getId()));
//            node.setParentId(String.valueOf(item.getParentId()));
//            node.setName(item.getTitle());
//            node.setDetail(item);
//            allNodes.add(node);
//        }
//        List<Node> roots = treeBuilder.buildListToTree(allNodes);
//        String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//        return new ModelAndView("user_config/add_user")
//                .addObject("roleList", optionRole)
//                .addObject("facilitiesSelect", facilitiesSelect);
//    }
//
//    @MenuPermission("update_user")
//    @RequestMapping({"/edit_user_detail"})
//    public ModelAndView editUserDetail(HttpServletRequest request, HttpServletResponse response) {
//        JsonObjectResult jsonObjectResult = new JsonObjectResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String user_id = request.getParameter("id");
//            User user = userService.findById(schema_name, user_id);
//            jsonObjectResult.setData(user);
//            jsonObjectResult.success("");
//            String roles = "";
//            String groups = "";
//
////            List<UserRole> roleLists = userRoleService.findAllCustomRole(schema_name);
//            LinkedHashMap<String, IUserRole> rolesList = user.getRoles();
//            LinkedHashMap<String, IUserGroup> groupList = user.getGroupLists();
//            for (String key : rolesList.keySet()) {
//                IUserRole role = rolesList.get(key);
//                String id_arr = role.getId() + ",";
//                roles = roles + id_arr;
//            }
//            for (String key : groupList.keySet()) {
//                IUserGroup role = groupList.get(key);
//                String group_arr = role.getGroup_id() + ",";
//                groups = groups + group_arr;
//            }
//
//            if (roles != null && !roles.equals("")) {
//                roles = roles.substring(0, roles.length() - 1);
//            }
//            if (groups != null && !groups.equals("")) {
//                groups = groups.substring(0, groups.length() - 1);
//            }
//
//
////            String roleList = userRoleService.optionRole(roleLists,schema_name,user,"update");
//
//            List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//            TreeBuilder treeBuilder = new TreeBuilder();
//            List<Node> allNodes = new ArrayList<Node>();
//            for (Facility item : facilitiesList) {
//                Node node = new Node();
//                node.setId(String.valueOf(item.getId()));
//                node.setParentId(String.valueOf(item.getParentId()));
//                node.setName(item.getTitle());
//                node.setDetail(item);
//                allNodes.add(node);
//            }
//            List<Node> roots = treeBuilder.buildListToTree(allNodes);
//            Set<String> selectSet = new HashSet<String>();
//            if (user.getFacilities() != null) {
//                selectSet.addAll(user.getFacilities().keySet());
//            }
//            String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots, selectSet);
//
//            return new ModelAndView("user_config/edit_user")
//                    .addObject("result", jsonObjectResult)
//                    .addObject("is_free", user.isIs_charge())
//                    .addObject("is_out", user.getIs_out())
//                    .addObject("status", user.getStatus() != user.STATUS_LOCKED)
//                    .addObject("customer_id", user.getCustomer_id())
//                    .addObject("roleList", roles)
//                    .addObject("groupsList", groups)
//                    .addObject("facilitiesSelect", facilitiesSelect);
//        } catch (Exception ex) {
//            jsonObjectResult.error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_INFO_ERROR_A) + ex.getMessage());
//            return new ModelAndView("user_config/edit_user")
//                    .addObject("result", jsonObjectResult);
//        }
//    }
//
//    @MenuPermission("add_user")
//    @RequestMapping({"/add_user2"})
//    @Transactional //支持事务
//    public JsonResult addUser2(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            boolean charge = false;
//            String is_free = request.getParameter("is_free");
//            String account = request.getParameter("account");
//            if (RegexUtil.isNull(account)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_REQUIRED));
//            }
//            String password = request.getParameter("password");
//            String password2 = request.getParameter("password2");
//            password = AESUtils.decryptAES(password);
//            password2 = AESUtils.decryptAES(password2);
//            String customer_id = request.getParameter("customer_id");
//            String username = request.getParameter("username");
//            String user_code = request.getParameter("user_code");
//            if (RegexUtil.isNull(username)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.USER_EMPTY_NOTICE));
//            }
//            if (RegexUtil.isNull(user_code)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_REQUIRED));
//            }
//            if (password.equals(password2) == false) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.PASSWORD_DIFF));
//            }
//
//            String mobile = request.getParameter("mobile");
//            if (RegexUtil.isNull(mobile)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MOBILE_REQUIRED));
//            }
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //是否需要验证手机号码
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "is_validate_mobile");
//            if (RegexUtil.isNotNull(mobile) && (null == data || data.getSettingValue().equals("2")) && RegexUtil.isMobile(mobile) == false) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MOBILE_ERROR));
//            }
//
//            // 检查密码是否为强验证密码
//            SystemConfigData dataPassword = systemConfigService.getSystemConfigData(schema_name, "is_validate_strong_password");
//            if ((null != dataPassword && dataPassword.getSettingValue().equals("1")) && RegexUtil.isStrongPwd(password) == false) {
//                //密码非强密码，需修改
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.STRONG_PASSWORD));//强验证，必填字母数字及特殊字符，且以字母开头，8位以上。
//            }
//
//            String email = request.getParameter("email");
//            if (RegexUtil.isNotNull(email) && RegexUtil.isEmail(email) == false) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.EMAIL_ERROR));
//            }
//
//            if (userService.checkUserByAccount(schema_name, account) > 0) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_REPEAT));
//            }
//
//            if (RegexUtil.isNotNull(mobile)) {
//                if (userService.checkUserByMobile(schema_name, mobile) > 0) {
//                    return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MOBILE_REPEAT));
//                }
//            }
//            User user = new User();
//            user.setSchema_name(schema_name);
//            user.setId(String.valueOf(IdGenerator.generateIntId()));
//            user.setUsername(username);
//            user.setUser_code(user_code);
//            user.setAccount(account);
//            user.setMobile(mobile);
//            user.setEmail(email);
//
//            if ("true".equalsIgnoreCase(request.getParameter("status"))) {
//                user.setStatus(User.STATUS_ACTIVE);
//            } else {
//                user.setStatus(User.STATUS_LOCKED);
//            }
//
//            if ("true".equalsIgnoreCase(request.getParameter("is_free"))) {
//                charge = true;
//                user.setIs_charge(true);//收费用户
//            } else {
//                user.setIs_charge(false);//免费用户
//            }
//            if ("true".equalsIgnoreCase(request.getParameter("is_out"))) {
//                charge = true;
//                user.setIs_out(true);//收费用户
//                if (RegexUtil.isNotNull(customer_id)) {
//                    user.setCustomer_id(Integer.parseInt(customer_id));
//                }
//            } else {
//                user.setIs_out(false);//免费用户
//            }
//            user.setCreatetime(new Timestamp(new Date().getTime()));
//            String[] roles = request.getParameterValues("roles");
//            String groups_str = request.getParameter("groups");
//            roles = roles[0].split(",");
//            if (roles != null && roles.length > 0) {
//                LinkedHashMap<String, IUserRole> roleSet = new LinkedHashMap<String, IUserRole>();
//                for (String role : roles) {
//                    IUserRole userRole = new UserRoleBase();
//                    userRole.setId(role);
//                    roleSet.put(userRole.getId(), userRole);
//                }
//                user.setRoles(roleSet);
//            }
//            user.setGroups(groups_str);
//
//            String[] facilities = request.getParameterValues("facilities");
//            if (facilities != null && facilities.length > 0) {
//                LinkedHashMap<String, IFacility> facilityLinkedHashMap = new LinkedHashMap<String, IFacility>();
//                for (String item : facilities) {
//                    if (RegexUtil.isNull(item)) {
//                        continue;
//                    }
//                    IFacility facility = new FacilityBase();
//                    facility.setId(Integer.valueOf(item));
//                    facilityLinkedHashMap.put(String.valueOf(facility.getId()), facility);
//                }
//                user.setFacilities(facilityLinkedHashMap);
//            }
//            //password = Base64.getBase64(MD5.encodeMD5Lowercase(MD5.encodeMD5Lowercase(password)));
//            user.setPassword(User.encryptPassword(password));
//
//            //查找所在企业id，将用户插入public表中，进行统一记录
//            user.setCompanyId(authService.getCompany(request).getId());
//
////
//            CompanyHireInfoModel companyHireInfoModel = userService.selectServiceDateCompany(schema_name, authService.getCompany(request).getId());
//            if (companyHireInfoModel != null) {
//                Timestamp now = new Timestamp(System.currentTimeMillis());
//                if (companyHireInfoModel.getEnd_time().before(now)) {
//                    return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_EXPIRED));
//                } else {
//                    if (charge) {
//                        int noFreeNum = userService.selectNoFreeNum(schema_name);
//                        int total = userService.selectCompanyTotalNum(schema_name, authService.getCompany(request).getId());
//                        if (noFreeNum < total) {
//                            userService.insertUser(user);
//                            userService.addUserSmsSend(user, password);
//                        } else {
//                            JsonResult result = new JsonResult();
//                            result.setCode(-2);
////                            result.setMsg("超出企业开通收费用户数："+noFreeNum+"个,无法添加收费用户"); // "超出企业开通收费用户数："+noFreeNum+"个,无法添加收费用户"
//                            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_ADD_MAX) + "：" + noFreeNum); // "无法添加收费用户，超出企业开通收费用户数量："+noFreeNum
//                            return result;
//                        }
//                        return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_ADD_SUCCESS));
//                    } else {
//                        userService.insertUser(user);
//                        userService.addUserSmsSend(user, password);
//                        return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_ADD_SUCCESS));
//                    }
//
//                }
//            } else {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_COMPANY_ERROR));
//            }
//
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_ADD_ERROR));
//        }
//    }
//
//    @MenuPermission("delete_user")
//    @RequestMapping({"/delete_user"})
//    @Transactional
//    public JsonResult deleteUser(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            String user_id = request.getParameter("id");
//            userService.deleteUser(schema_name, user_id, company.getId());
//            return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_SUCCESS));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_ERROR));
//        }
//    }
//
//    @RequestMapping({"/logout_user"})
//    @Transactional
//    public JsonResult logoutUser(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            String user_id = request.getParameter("id");
//            userService.logoutUser(schema_name, user_id, company.getId());
//            return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_SUCCESS));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_ERROR));
//        }
//    }
//
//    @MenuPermission("delete_user")
//    @RequestMapping({"/delete_select_user"})
//    public JsonResult deleteUserSelect(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String[] user_ids = request.getParameterValues("ids");
//            StringBuffer ids = null;
//            if (user_ids != null) {
//
//                for (String user_id : user_ids) {
//                    if (ids == null) {
//                        ids = new StringBuffer();
//                        ids.append("'" + user_id + "'");
//                    } else {
//                        ids.append(",'" + user_id + "'");
//                    }
//                    request.setAttribute("user_ids", user_id);
//                    String checkResult = this.search(request, response); // 权限判断
//                    if (RegexUtil.isNull(checkResult)) {
//                        return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MSG_CA));
//                    }
//                }
//            }
//            int i = userService.setSelectStatus(schema_name, ids.toString(), User.STATUS_DELETED);
//            return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_SUCCESS));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_ERROR));
//        }
//    }
//
//    @MenuPermission("reset_user")
//    @RequestMapping({"/completely_delete_user"})
//    public JsonResult completelyDeleteUser(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String user_id = request.getParameter("id");
//            userService.deleteUser(schema_name, user_id);
//            return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_SUCCESS));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_DELETE_ERROR));
//        }
//    }
//
//    @MenuPermission("reset_user")
//    @RequestMapping({"/restore_user"})
//    @Transactional
//    public JsonResult restoreUser(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            String user_id = request.getParameter("id");
//            userService.restoreUser(schema_name, user_id, company.getId());
//            return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_RESTORE_SUCCESS));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_RESTORE_ERROR));
//        }
//    }
//
//
//    @RequestMapping("/getRoleTableList")
//    public String getRoleTable(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            List<UserRole> roleList = userRoleService.findAllCustomRole(schema_name);
//            int count = roleList.size();
//            if (roleList != null && roleList.size() > 0) {
//                result.put("rows", roleList);
//                result.put("code", 1);
//            } else {
//                return "{}";
//            }
//            return result.toString();
//        } catch (Exception e) {
//            return "{}";
//        }
//
//    }
//
//    @MenuPermission("update_user")
//    @RequestMapping({"/user_detail"})
//    public ModelAndView userDetail(HttpServletRequest request, HttpServletResponse response) {
////        DomainPortUrlData domainPortUrl = AuthService.getDomainPortUrlData(request);
////        if (domainPortUrl != null && domainPortUrl.getIs_need_passport()) {
////            return new ModelAndView("redirect:"+domainPortUrl.getBak_url3());
////        } else {
//        JsonObjectResult jsonObjectResult = new JsonObjectResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String user_id = request.getParameter("id");
//            User user = userService.findById(schema_name, user_id);
//            jsonObjectResult.setData(user);
//            jsonObjectResult.success("");
//
//            List<UserRole> roleLists = userRoleService.findAllCustomRole(schema_name);
//            String roleList = userRoleService.optionRole(roleLists, schema_name, user, "update");
//
//            List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//            TreeBuilder treeBuilder = new TreeBuilder();
//            List<Node> allNodes = new ArrayList<Node>();
//            for (Facility item : facilitiesList) {
//                Node node = new Node();
//                node.setId(String.valueOf(item.getId()));
//                node.setParentId(String.valueOf(item.getParentId()));
//                node.setName(item.getTitle());
//                node.setDetail(item);
//                allNodes.add(node);
//            }
//            List<Node> roots = treeBuilder.buildListToTree(allNodes);
//            Set<String> selectSet = new HashSet<String>();
//            if (user.getFacilities() != null) {
//                selectSet.addAll(user.getFacilities().keySet());
//            }
//            String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots, selectSet);
//
//            return new ModelAndView("user_config/user_detail")
//                    .addObject("result", jsonObjectResult)
//                    .addObject("is_free", user.isIs_charge())
//
//                    .addObject("customer_id", user.getCustomer_id())
//                    .addObject("roleList", roleList)
//                    .addObject("facilitiesSelect", facilitiesSelect);
//        } catch (Exception ex) {
//            jsonObjectResult.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_DETAIL_ERR) + ex.getMessage());
//            return new ModelAndView("user_config/user_detail")
//                    .addObject("result", jsonObjectResult);
//        }
////        }
//    }
//
//    @MenuPermission("update_user")
//    @RequestMapping({"/update_user"})
//    public JsonResult updateUser(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String account = request.getParameter("account");
//            Boolean charge = false;
//            if (RegexUtil.isNull(account)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_REQUIRED));
//            }
//            String password = request.getParameter("password");
//            String password2 = request.getParameter("password2");
//            password = AESUtils.decryptAES(password);
//            password2 = AESUtils.decryptAES(password2);
//            String customer_id = request.getParameter("customer_id");
//            String username = request.getParameter("username");
//            String user_code = request.getParameter("user_code");
//            if (RegexUtil.isNull(username)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.USER_EMPTY_NOTICE));
//            }
//            if (RegexUtil.isNull(user_code)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_REQUIRED));
//            }
//            if (password.equals(password2) == false) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.PASSWORD_DIFF));
//            }
//            String mobile = request.getParameter("mobile");
//            if (RegexUtil.isNull(mobile)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MOBILE_REQUIRED));
//            }
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //是否需要验证手机号码
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "is_validate_mobile");
//            if ((null == data || data.getSettingValue().equals("2")) && RegexUtil.isMobile(mobile) == false) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MOBILE_ERROR));
//            }
//            // 检查密码是否为强验证密码
//            SystemConfigData dataPassword = systemConfigService.getSystemConfigData(schema_name, "is_validate_strong_password");
//            if ((null != dataPassword && dataPassword.getSettingValue().equals("1")) && !"########".equals(password) && !RegexUtil.isStrongPwd(password)) {
//                //密码非强密码，需修改
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.STRONG_PASSWORD));//强验证，必填字母数字及特殊字符，且以字母开头，8位以上。
//            }
//
//            String email = request.getParameter("email");
//            if (RegexUtil.isNotNull(email) && RegexUtil.isEmail(email) == false) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.EMAIL_ERROR));
//            }
//
//            User user = new User();
//            user.setSchema_name(schema_name);
//            user.setId(request.getParameter("id"));
//            user.setUsername(request.getParameter("username"));
//            user.setUser_code(request.getParameter("user_code"));
//            user.setAccount(account);
//            user.setMobile(mobile);
//            user.setEmail(email);
//
//            if ("true".equalsIgnoreCase(request.getParameter("status"))) {
//                user.setStatus(User.STATUS_ACTIVE);
//            } else {
//                user.setStatus(User.STATUS_LOCKED);
//            }
//            if ("true".equalsIgnoreCase(request.getParameter("is_free"))) {
//                charge = true;
//                user.setIs_charge(true);
//            } else {
//                user.setIs_charge(false);
//            }
//            if ("true".equalsIgnoreCase(request.getParameter("is_out"))) {
//                charge = true;
//                user.setIs_out(true);//收费用户
//                if (RegexUtil.isNotNull(customer_id)) {
//                    user.setCustomer_id(Integer.parseInt(customer_id));
//                }
//            } else {
//                user.setIs_out(false);//免费用户
//            }
//            user.setCreatetime(new Timestamp(new Date().getTime()));
//            String role_arr = request.getParameter("roles");
//            String groups_arr = request.getParameter("groups");
//            String[] roles = role_arr.split(",");
//            if (roles != null && roles.length > 0) {
//                LinkedHashMap<String, IUserRole> roleSet = new LinkedHashMap<String, IUserRole>();
//                for (String role : roles) {
//                    IUserRole userRole = new UserRoleBase();
//                    userRole.setId(role);
//                    roleSet.put(userRole.getId(), userRole);
//                }
//                user.setRoles(roleSet);
//            }
//            user.setGroups(groups_arr);
//
//            String[] facilities = request.getParameterValues("facilities");
//            if (facilities != null && facilities.length > 0) {
//                LinkedHashMap<String, IFacility> facilityLinkedHashMap = new LinkedHashMap<String, IFacility>();
//                for (String item : facilities) {
//                    if (RegexUtil.isNull(item)) {
//                        continue;
//                    }
//                    IFacility facility = new FacilityBase();
//                    facility.setId(Integer.valueOf(item));
//                    facilityLinkedHashMap.put(String.valueOf(facility.getId()), facility);
//                }
//                user.setFacilities(facilityLinkedHashMap);
//            }
//
//            //查找所在企业id，将用户插入public表中，进行统一记录
//            user.setCompanyId(authService.getCompany(request).getId());
//
//            CompanyHireInfoModel companyHireInfoModel = userService.selectServiceDateCompany(schema_name, authService.getCompany(request).getId());
//            if (companyHireInfoModel != null) {
//                Timestamp now = new Timestamp(System.currentTimeMillis());
//                if (companyHireInfoModel.getEnd_time().before(now)) {
//                    return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_EXPIRED));
//                } else {
//                    if (charge) {
//                        int noFreeNum = userService.selectNoFreeNum(schema_name);
//                        int total = userService.selectCompanyTotalNum(schema_name, authService.getCompany(request).getId());
//                        if (noFreeNum <= total) { //注意：修改时包含当前用户在内，不能超过总数total
//                            if ("########".equals(password) == false) {
//                                //password = Base64.getBase64(MD5.encodeMD5Lowercase(MD5.encodeMD5Lowercase(password)));
//                                user.setPassword(User.encryptPassword(password));
//                                userService.updateUser(user);
//                            } else {
//                                userService.updateUserWithoutPassword(user);
//                            }
//                            return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_UPDATE_SUCCESS));
//                        } else {
////                            return new JsonResult().error("超出企业开通收费用户数："+noFreeNum+"个,无法修改为收费用户");
//                            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_UPDATE_MAX) + "：" + noFreeNum); // 无法修改为收费用户，超出企业开通收费用户数量："+noFreeNum
//                        }
//                    } else {
//                        if ("########".equals(password) == false) {
//                            //password = Base64.getBase64(MD5.encodeMD5Lowercase(MD5.encodeMD5Lowercase(password)));
//                            user.setPassword(User.encryptPassword(password));
//                            userService.updateUser(user);
//                        } else {
//                            userService.updateUserWithoutPassword(user);
//                        }
//                        return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_UPDATE_SUCCESS));
//                    }
//                }
//            } else {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_COMPANY_ERROR));
//            }
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.ACCOUNT_UPDATE_ERROR));
//        }
//    }
//
//
//    //导出用户信息
//    @MenuPermission("user_config")
//    @RequestMapping("/user_list_export")
//    public ModelAndView exportUserList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            //根据日期和巡检点，查找巡检
//            int pageSize = 50000;//Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = 1;//Integer.parseInt(request.getParameter("pageNumber"));
//            User loginUser = authService.getLoginUser(request);
//            String keyword = request.getParameter("keyword");
//            String tempRoles = request.getParameter("roles");
//            String is_out = request.getParameter("is_out");
//            String user_groups = request.getParameter("user_groups");
//
//            String groupCondition = " 1=1 ";
//            String roleCondition = " 1=1 ";
//            String condition = " 1=1 ";
//            if (is_out != null && is_out.equals("1")) {
//                condition += "and  t1.is_out=" + false;
//            } else if (is_out != null && is_out.equals("0")) {
//                condition += "and t1.is_out=" + true;
//            }
//            condition += " and ";
//            boolean isSearchAllGroup = false;
//            boolean isSearchAllRoles = false;
//            if (tempRoles != null && !tempRoles.isEmpty() && tempRoles.length() > 0) {
//                String[] tempRoleList = tempRoles.split(",");
//                String roleSearch = "'-2'";
//                for (String s : tempRoleList) {
//                    roleSearch += ",'" + s + "'";
//                    if (s.equals("-1")) {
//                        isSearchAllRoles = true;
//                        break;
//                    }
//                }
//                if (!isSearchAllRoles) {
//                    roleCondition = " r.role_id in (" + roleSearch + ")";
//                }
//            }
//
//            if (user_groups != null && !user_groups.isEmpty() && user_groups.length() > 0) {
//                String[] user_group = user_groups.split(",");
//                String groupSearch = "'-2'";
//                for (String s : user_group) {
//                    groupSearch += ",'" + s + "'";
//                    if (s.equals("-1")) {
//                        isSearchAllGroup = true;
//                        break;
//                    }
//                }
//                if (!isSearchAllGroup) {
//                    groupCondition += " and t3.id in (" + groupSearch + ")";
//                }
//            }
//            if (RegexUtil.isNull(keyword)) {
//                keyword = "";
//            }
//            keyword = "%" + keyword + "%";
//            int begin = pageSize * (pageNumber - 1);
//            boolean hasResetUserPermission = pagePermissionService.getPermissionByKey(schema_name, loginUser.getId(), "user_config", "reset_user");
//            if (hasResetUserPermission) {
//                condition += " t1.status > " + User.STATUS_CANCELLED + " and ";
//            } else {//没有还原权限的用户，只能查询非删除状态的用户
//                condition += " t1.status > " + User.STATUS_DELETED + " and ";
//            }
//            List<User> userListByPage = userService.searchUserByPage(schema_name, condition, groupCondition, roleCondition, keyword, pageSize, begin);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("userConfigDataList", userListByPage);
//            map.put("selectOptionService", selectOptionService);
//            UserConfigDataExportView excelView = new UserConfigDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
}
