package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.response.FacilitiesDataResult;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.asset.AssetMapService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

///**
// * 设备地图
// */
@Api(tags = "设备地图")
@RestController
public class AssetMapController {

    @Resource
    private AssetMapService assetMapService;

    @ApiOperation(value = "获取设备地图模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_MAP_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetMapPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetMapPermission(MethodParam methodParam) {
        return ResponseModel.ok(assetMapService.getAssetMapPermission(methodParam));
    }

    @ApiOperation(value = "获得所有的位置", notes = ResponseConstant.RSP_DESC_GET_ASSET_MAP_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAllAssetMapPosition", method = RequestMethod.POST)
    public ResponseModel searchAllAssetMapPosition(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(assetMapService.getAllAssetMapPosition(methodParam, param));
    }

    @ApiOperation(value = "获取位置的设备及子位置数据", notes = ResponseConstant.RSP_DESC_GET_ASSET_MAP_ASSET_AND_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","showType","assetType","positionCode","workType","keywordSearch"})
    @RequestMapping(value = "/searchAssetAndAssetPositionForMap", method = RequestMethod.POST)
    public ResponseModel searchAssetAndAssetPositionForMap(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(assetMapService.getAssetAndAssetPositionForMap(methodParam, param));
    }

    @ApiOperation(value = "获得选择位置或设备的信息", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_MAP_DETAIL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","id","type"})
    @RequestMapping(value = "/searchAssetMapDetail", method = RequestMethod.POST)
    public ResponseModel searchAssetMapDetail(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(assetMapService.getAssetMapDetail(methodParam, param));
    }

}
