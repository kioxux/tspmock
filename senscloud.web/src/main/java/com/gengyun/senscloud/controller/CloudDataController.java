package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.CloudDataModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.CloudDataService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "字典管理")
@RestController
public class CloudDataController {
    @Resource
    CloudDataService cloudDataService;

    @ApiOperation(value = "新增数据项类型表", notes = ResponseConstant.RSP_DESC_ADD_CLOUD_DATA_TYPE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "data_type", "data_type_nam", "is_lang", "data_order", "remark"})
    @Transactional
    @RequestMapping(value = "/addCloudDataType", method = RequestMethod.POST)
    public ResponseModel addCloudDataType(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                          @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        cloudDataService.newCloudDataType(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取数据项类型列表", notes = ResponseConstant.RSP_DESC_SEARCH_CLOUD_DATA_TYPE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @Transactional
    @RequestMapping(value = "/searchCloudDataTypeList", method = RequestMethod.POST)
    public ResponseModel searchCloudDataTypeList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                                 @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        return ResponseModel.ok(cloudDataService.getCloudDataTypeList(methodParam, paramMap));
    }

    @ApiOperation(value = "更新数据项类型表", notes = ResponseConstant.RSP_DESC_EDIT_CLOUD_DATA_TYPE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "data_type", "data_type_nam", "is_lang", "data_order", "remark"})
    @Transactional
    @RequestMapping(value = "/editCloudDataType", method = RequestMethod.POST)
    public ResponseModel editCloudDataType(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                           @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        cloudDataService.modifyCloudDataType(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除数据项类型表", notes = ResponseConstant.RSP_DESC_REMOVE_CLOUD_DATA_TYPE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "data_type"})
    @Transactional
    @RequestMapping(value = "/removeCloudDataType", method = RequestMethod.POST)
    public ResponseModel removeCloudDataType(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                             @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        cloudDataService.cutCloudDataType(methodParam, cloudDataModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "根据数据项类型获取数据项列表", notes = ResponseConstant.RSP_DESC_SEARCH_CLOUD_DATA_LIST_BY_DATA_TYPE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "data_type"})
    @Transactional
    @RequestMapping(value = "/searchCloudDataListByDataType", method = RequestMethod.POST)
    public ResponseModel searchCloudDataListByDataType(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                                       @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        return ResponseModel.ok(cloudDataService.getCloudDataListByDataType(methodParam, paramMap));
    }

    @ApiOperation(value = "新增数据项表", notes = ResponseConstant.RSP_DESC_ADD_CLOUD_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "data_type", "type_remark", "is_lang", "remark", "parent_code", "data_order",
            "reserve1", "name", "code"})
    @Transactional
    @RequestMapping(value = "/addCloudData", method = RequestMethod.POST)
    public ResponseModel addCloudData(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                      @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        cloudDataService.newCloudData(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新数据项表", notes = ResponseConstant.RSP_DESC_EDIT_CLOUD_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "data_type", "type_remark", "is_lang", "remark", "parent_code", "data_order",
            "reserve1", "name", "code"})
    @Transactional
    @RequestMapping(value = "/editCloudData", method = RequestMethod.POST)
    public ResponseModel editCloudData(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                       @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        cloudDataService.modifyCloudData(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除数据项表", notes = ResponseConstant.RSP_DESC_REMOVE_CLOUD_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "data_type", "code"})
    @Transactional
    @RequestMapping(value = "/removeCloudData", method = RequestMethod.POST)
    public ResponseModel removeCloudData(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                         @SuppressWarnings("unused") CloudDataModel cloudDataModel) {
        cloudDataService.cutCloudData(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }
}
