package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.model.StatisticModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.StatisticService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 统计分析
 */

@Api(tags = "统计分析管理")
@RestController
public class StatisticController {
    @Resource
    StatisticService statisticService;

    @ApiOperation(value = "获取统计报表模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_REPORT_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchReportPermission", method = RequestMethod.POST)
    public ResponseModel searchReportPermission(MethodParam methodParam) {
        return ResponseModel.ok(statisticService.getReportPermission(methodParam));
    }

    @ApiOperation(value = "根据id获取统计报表数据", notes = ResponseConstant.RSP_DESC_SEARCH_STATISTICS_REPORT_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchStatisticsReportData", method = RequestMethod.POST)
    public ResponseModel searchStatisticsReportData(MethodParam methodParam, @RequestParam Map<String, Object> params,
                                                    @SuppressWarnings("unused") StatisticModel statisticModel) {
        return ResponseModel.ok(statisticService.getStatisticsTableData(methodParam, params, true));
    }

    @ApiOperation(value = "根据id获取统计表格数据", notes = ResponseConstant.RSP_DESC_SEARCH_STATISTICS_TABLE_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchStatisticsTableData", method = RequestMethod.POST)
    public ResponseModel searchStatisticsTableData(MethodParam methodParam, @RequestParam Map<String, Object> params,
                                                   @SuppressWarnings("unused") StatisticModel statisticModel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(statisticService.getStatisticsTableData(methodParam, params, false));
    }

    @ApiOperation(value = "查询统计分析列表", notes = ResponseConstant.RSP_DESC_SEARCH_STATISTIC_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchStatisticList", method = RequestMethod.POST)
    public ResponseModel searchStatisticList(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(statisticService.getStatisticList(methodParam, param));
    }

    @ApiOperation(value = "删除选中用户看板报表", notes = ResponseConstant.RSP_DESC_REMOVE_SELF_STATISTIC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/removeUserStatisticDash", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeUserStatisticDash(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String id) {
        statisticService.cutUserStatisticDash(methodParam, id);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "添加用户看板报表", notes = ResponseConstant.RSP_DESC_ADD_SELF_STATISTIC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/addUserStatisticDash", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel addUserStatisticDash(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String id) {
        statisticService.newUserStatisticDash(methodParam, id);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "根据id导出统计表格数据", notes = ResponseConstant.RSP_DESC_GET_STATISTICS_TABLE_DATA_EXPORT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getStatisticsTableDataExport", method = RequestMethod.POST)
    public ResponseModel getStatisticsTableDataExport(MethodParam methodParam, @RequestParam Map<String, Object> params) {
        return ResponseModel.okExport(statisticService.getStatisticsTableDataExport(methodParam, params));
    }
}
