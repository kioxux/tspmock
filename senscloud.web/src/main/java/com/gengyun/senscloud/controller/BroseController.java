package com.gengyun.senscloud.controller;

import org.springframework.web.bind.annotation.RestController;

//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.AssetDataServiceV2;
//import com.gengyun.senscloud.service.BroseService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.SystemConfigService;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
@RestController
//@RequestMapping("/brose")
public class BroseController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    BroseService broseService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/brose_all")
//    public ModelAndView borseMonitor(HttpServletRequest request, HttpServletResponse response){
//        String schema_name = "sc_com_62";
//        ModelAndView mv = new ModelAndView();
//        SystemConfigData systemConfigData = systemConfigService.getSystemConfigData(schema_name,"carousel_time");
//        SystemConfigData systemConfigDataFacility = systemConfigService.getSystemConfigData(schema_name,"facility_utilization_select");
//        mv.addObject("facility_utilization_select",systemConfigDataFacility.getSettingValue());
//        mv.addObject("carousel_time",systemConfigData.getSettingValue());
//        mv.setViewName("brose/brose_all");
//        return mv;
//    }
//
//    @RequestMapping("/brose_real_time_data")
//    public ModelAndView broseRealTimeData(HttpServletRequest request, HttpServletResponse response){
//        String schema_name = "sc_com_62";
//        ModelAndView mv = new ModelAndView();
//        String equipment_no = request.getParameter("equipment_no");
//        SystemConfigData systemConfigData = systemConfigService.getSystemConfigData(schema_name,"asset_detail_brose");
//        mv.addObject("asset_detail_brose",systemConfigData.getSettingValue());
//        mv.addObject("equipment_no",equipment_no);
//        mv.setViewName("brose/brose_real_time_data");
//        return mv;
//    }
//
//    //设备列表 Maintenance 按钮操作开启或者关闭controller
//    @RequestMapping("dealBroseAssetMonitorStatus")
//    public ResponseModel dealBroseAssetMonitorStatus(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = "sc_com_62";
//        try {
//            String id = request.getParameter("id");
//            String strcode = request.getParameter("strcode");
//            String dealStatus = request.getParameter("dealStatus");
//                boolean update = broseService.dealBroseAssetMonitorStatus(schema_name , id ,dealStatus,strcode);//并且修改设备状态
//                if(update){
//                    model.setCode(StatusConstant.SUCCES_RETURN);
//                    String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//                    model.setMsg(tmpMsg );//成功 MSG_SUCC_A
//                }else{
//                    model.setCode(StatusConstant.NO_DATE_RETURN);
//                    String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//                    model.setMsg(tmpMsg);//失败 MSG_FAIL
//                }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            model.setMsg(tmpMsg);//操作出现异常  MSG_OPERATION_E
//            return model;
//        }
//    }
//
//    //博泽设备列表查询（table展示）
//    @RequestMapping("/getAssetMonitorTable")
//    public ResponseModel getAssetMonitorTable(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = "sc_com_62";//正式博泽schema_name
//        try {
//            List<BroseAssetMonitorModel> broseAssetMonitorModelsLists = broseService.getAssetMonitorTable(schema_name);
//            if(broseAssetMonitorModelsLists!=null && broseAssetMonitorModelsLists.size()>0){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setContent(broseAssetMonitorModelsLists);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//                model.setMsg(tmpMsg );//成功 MSG_SUCC_A
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setContent(broseAssetMonitorModelsLists);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_DATA));
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            model.setMsg(tmpMsg);//操作出现异常  MSG_OPERATION_E
//            return model;
//        }
//    }
//
//    @RequestMapping("/getAssetMonitorTableById")
//    public ResponseModel getAssetMonitorTableById(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = "sc_com_62";//正式博泽schema_name
//        try {
//
//            String id_val = request.getParameter("id_val");
//            BroseAssetMonitorModel broseAssetMonitorModel = broseService.getAssetMonitorTableById(schema_name,id_val);
//            if(broseAssetMonitorModel!=null){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setContent(broseAssetMonitorModel);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//                model.setMsg(tmpMsg );//成功 MSG_SUCC_A
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setContent(broseAssetMonitorModel);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_DATA));
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            model.setMsg(tmpMsg);//操作出现异常  MSG_OPERATION_E
//            return model;
//        }
//    }
//
//    //设备监控地图设备详情查看温度湿度曲线图
//    @RequestMapping("/getTemAndHumByAssetCode")
//    public ResponseModel getTemAndHumByAssetCode(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = "sc_com_62";//正式博泽schema_name
//        try {
//            int i = 1;
//            String asset_code = request.getParameter("asset_code");
//            String time = request.getParameter("time");
//            if(time!=null && !time.equals("")){
//                i = Integer.parseInt(time);
//            }
//            BroseTodayCurveModel broseTodayCurveModel = broseService.getTemAndHumByAssetCode(schema_name , asset_code, i );
//            if(broseTodayCurveModel!=null){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setContent(broseTodayCurveModel);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//                model.setMsg(tmpMsg );//成功 MSG_SUCC_A
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setContent(broseTodayCurveModel);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_DATA));
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            model.setMsg(tmpMsg);//操作出现异常  MSG_OPERATION_E
//            return model;
//        }
//    }
//
//    //13种设备柱状图分析
//    @RequestMapping("/getAllAssetThreeMonth")
//    public ResponseModel getAllAssetThreeMonth(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = "sc_com_62";//正式博泽schema_name
//        String month_str = "";
//        try {
//            String month = request.getParameter("month");
//            //发生月出现变更，对旧数据进行重新查询，当处于持续月，记性当前月统计，减少压力
//            List<BroseThreeDataModel> broseStatusRateMonthModel = broseService.getAllAssetThreeMonth(schema_name , month);
//            Calendar c = Calendar.getInstance();
//            Calendar c_two = Calendar.getInstance();
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON_2);
//            String now =  sdf.format(new Date());
//            c.add(Calendar.MONTH, -1);
//            c_two.add(Calendar.MONTH, -2);
//            c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH),c.getActualMaximum(Calendar.DAY_OF_MONTH));
//            c_two.set(c_two.get(Calendar.YEAR), c_two.get(Calendar.MONTH),c_two.getActualMaximum(Calendar.DAY_OF_MONTH));
//            String lastMonth = sdf.format(c.getTime());
//            String twoLastMonth = sdf.format(c_two.getTime());
//            month_str = twoLastMonth+","+lastMonth+","+now;
//
//
//            if(broseStatusRateMonthModel!=null && broseStatusRateMonthModel.size()>0){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setContent(broseStatusRateMonthModel);
//                model.setMsg(month_str);
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setContent(broseStatusRateMonthModel);
//                model.setMsg(month_str);
//            }
//            return model;
//        }catch (Exception e){
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(month_str);
//            return model;
//        }
//    }
//
//    //设备监控地图展示6月占比和统计结果数据查询
//    @RequestMapping("/getAllAssetSixMonth")
//    public ResponseModel getAllAssetSixMonth(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = "sc_com_62";//正式博泽schema_name
//        try {
//            String month = request.getParameter("month");
//            //发生月出现变更，对旧数据进行重新查询，当处于持续月，记性当前月统计，减少压力
//            Map<String,String> broseStatusRateMonthModel = broseService.getAllAssetSixMonth(schema_name,month);
//            model.setCode(StatusConstant.SUCCES_RETURN);
//            model.setContent(broseStatusRateMonthModel);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A);
//            model.setMsg(tmpMsg );//成功 MSG_SUCC_A
//            return model;
//        }catch (Exception e){
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            model.setMsg(tmpMsg);//操作出现异常  MSG_OPERATION_E
//            return model;
//        }
//    }
}
