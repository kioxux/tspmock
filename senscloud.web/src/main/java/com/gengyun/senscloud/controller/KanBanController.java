package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.AssetPositionModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.KanBanService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 大屏看板
 */
@Api(tags = "大屏看板")
@RestController
public class KanBanController {
    @Resource
    KanBanService kanBanService;

    /**
     * 待办的维修工单
     */

    @ApiOperation(value = "待办的维修工单", notes = ResponseConstant.RSP_DESC_SEARCH_REPAIR_TASK_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchRepairTaskList", method = RequestMethod.POST)
    public ResponseModel searchRepairTaskList(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getRepairTaskList(methodParam));
    }

    /**
     * 待办的维护工单
     */

    @ApiOperation(value = "待办的维护工单", notes = ResponseConstant.RSP_DESC_SEARCH_MAINTAIN_TASK_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchMaintainTaskList", method = RequestMethod.POST)
    public ResponseModel searchMaintainTaskList(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getMaintainTaskList(methodParam));
    }

    /**
     * 故障任务总数
     * 当前的故障任务数   今日新增故障任务数   总数
     */
    @ApiOperation(value = "待办的维护工单", notes = ResponseConstant.RSP_DESC_SEARCH_REPAIR_FINISHED_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchRepairFinishedCount", method = RequestMethod.POST)
    public ResponseModel searchRepairFinishedCount(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getRepairFinishedCount(methodParam));
    }

    /**
     * 维护任务总数
     * 当前的维护任务数   今日新增维护任务数   总数
     */
    @ApiOperation(value = "待办的维护工单", notes = ResponseConstant.RSP_DESC_SEARCH_MAINTAIN_FINISHED_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchMaintainFinishedCount", method = RequestMethod.POST)
    public ResponseModel searchMaintainFinishedCount(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getMaintainFinishedCount(methodParam));
    }

    /**
     * 今日完成  本周完成  本月完成
     */
    @ApiOperation(value = "今日完成  本周完成  本月完成", notes = ResponseConstant.RSP_DESC_SEARCH_FINISHED_TASK_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchFinishedTaskCount", method = RequestMethod.POST)
    public ResponseModel searchFinishedTaskCount(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getFinishedTaskCount(methodParam));
    }

    /**
     * 当班人员名单
     */
    @ApiOperation(value = "当班人员名单", notes = ResponseConstant.RSP_DESC_SEARCH_ON_DUTY_USER_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchOnDutyUserList", method = RequestMethod.POST)
    public ResponseModel searchOnDutyUserList(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getOnDutyUserList(methodParam));
    }

    /**
     * 查询组织的利用率
     */
    @ApiOperation(value = "（本月设备利用率）查询组织的利用率", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_BY_ORGUTILIZATION_RATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchAssetByOrgUtilizationRate", method = RequestMethod.POST)
    public ResponseModel searchAssetByOrgUtilizationRate(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getAssetByOrgUtilizationRate(methodParam));
    }

    /**
     * SNC DMMS本月运行时间故障时间维护时间
     */
    @ApiOperation(value = "（本月设备利用率）本月运行时间故障时间维护时间", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_RUNNINGTIME)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchAssetRunningTime", method = RequestMethod.POST)
    public ResponseModel searchAssetRunningTime(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getAssetRunningTime(methodParam));
    }

    /**
     * 获取本年设备利用率列表
     */
    @ApiOperation(value = "（本月设备利用率）获取本年设备利用率列表", notes = ResponseConstant.RSP_DESC_SEARCH_NOW_YEAR_RATE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchNowYearRateList", method = RequestMethod.POST)
    public ResponseModel searchNowYearRateList(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getNowYearRateList(methodParam));
    }
    /**-----------------------故障情况-------------------------------------*/
    /**
     * 今日故障数本周故障数本月故障数
     */
    @ApiOperation(value = "（故障情况）今日故障数本周故障数本月故障数", notes = ResponseConstant.RSP_DESC_SEARCH_REPAIR_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchRepairCount", method = RequestMethod.POST)
    public ResponseModel searchRepairCount(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getRepairCount(methodParam));
    }

    /**
     * 当天修复情况
     */
    @ApiOperation(value = "（故障情况）当天修复情况", notes = ResponseConstant.RSP_DESC_SEARCH_TODAY_REPAIR_SITUATION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchTodayRepairSituation", method = RequestMethod.POST)
    public ResponseModel searchTodayRepairSituation(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getTodayRepairSituation(methodParam));
    }

    /**
     * 当天完好率
     */
    @ApiOperation(value = "（故障情况）当天完好率", notes = ResponseConstant.RSP_DESC_SEARCH_TODAY_INTACT_RATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchTodayIntactRate", method = RequestMethod.POST)
    public ResponseModel searchTodayIntactRate(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getTodayIntactRate(methodParam));
    }

    /**
     * 当天故障分布
     */
    @ApiOperation(value = "（故障情况）当天故障分布", notes = ResponseConstant.RSP_DESC_SEARCH_FAULT_DISTRI_BUTION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchFaultDistribution", method = RequestMethod.POST)
    public ResponseModel searchFaultDistribution(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getFaultDistribution(methodParam));
    }

    /**
     * 故障趋势
     */
    @ApiOperation(value = "（故障情况）故障趋势", notes = ResponseConstant.RSP_DESC_SEARCH_FAULT_REND)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchFaultTrend", method = RequestMethod.POST)
    public ResponseModel searchFaultTrend(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getFaultTrend(methodParam));
    }
    /**-----------------------维护情况-------------------------------------*/
    /**
     * 今日维护数本周维护数本月维护数
     */
    @ApiOperation(value = "（维护状况）今日维护数本周维护数本月维护数", notes = ResponseConstant.RSP_DESC_SEARCH_MAIN_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchMainCount", method = RequestMethod.POST)
    public ResponseModel searchMainCount(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getMainCount(methodParam));
    }

    /**
     * 当天设备点检
     */
    @ApiOperation(value = "（维护状况）当天设备点检", notes = ResponseConstant.RSP_DESC_SEARCH_TODAY_EQUIPMENT_SPOT_CHECK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchTodayEquipmentSpotCheck", method = RequestMethod.POST)
    public ResponseModel searchTodayEquipmentSpotCheck(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getTodayEquipmentSpotCheck(methodParam));
    }

    /**
     * 当天常规维护
     */
    @ApiOperation(value = "（维护状况）当天常规维护", notes = ResponseConstant.RSP_DESC_SEARCH_TODAY_ROUTINE_MAINTENANCE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchTodayRoutineMaintenance", method = RequestMethod.POST)
    public ResponseModel searchTodayRoutineMaintenance(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getTodayRoutineMaintenance(methodParam));
    }

    /**
     * 当月常规维护
     */
    @ApiOperation(value = "（维护状况）当月常规维护", notes = ResponseConstant.RSP_DESC_SEARCH_MONTH_ROUTINE_MAINTENANCE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchMonthRoutineMaintenance", method = RequestMethod.POST)
    public ResponseModel searchMonthRoutineMaintenance(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getMonthRoutineMaintenance(methodParam));
    }

    /**
     * 维护趋势
     */
    @ApiOperation(value = "（维护状况）维护趋势", notes = ResponseConstant.RSP_DESC_SEARCH_MAINT_REND)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchMainTrend", method = RequestMethod.POST)
    public ResponseModel searchMainTrend(MethodParam methodParam) {
        return ResponseModel.ok(kanBanService.getMainTrend(methodParam));
    }

    /**
     * -----------------------点位图-------------------------------------
     */
    @ApiOperation(value = "获取所有位置的设备的故障状态", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_REPAIR_STATUS_FOR_SCREEN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetRepairStatusForScreen", method = RequestMethod.GET)
    public ResponseModel searchAssetRepairStatusForScreen(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        return ResponseModel.ok(kanBanService.getAssetRepairStatusForScreen(methodParam));
    }
}
