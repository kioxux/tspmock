package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.mapper.AssetDiscardMapper;
//import com.gengyun.senscloud.mapper.AssetMapper;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @Author: Wudang Dong
// * @Description:设备报废
// * @Date: Create in 上午 10:18 2019/4/30 0030
// */
//
//@RestController
//@RequestMapping("/assetDiscard")
public class AssetDiscardController {

//    @Autowired
//    AssetDiscardService assetDiscardService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetStatusService assetStatusService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    AssetDiscardMapper assetDiscardMapper;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @MenuPermission("asset_discard")
//    @RequestMapping("/index")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        Map<String, Object> workType = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            workType = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_23);
//
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("asset_discard_invalid", "rpDeleteFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "asset_discard/index", "asset_discard")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("asset_discard/index");
//        }
//    }
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description:申请设备报废
//     */
//    @MenuPermission("asset_discard")
//    @RequestMapping("/add")
//    @Transactional
//    public ResponseModel add(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "flow_id") String processDefinitionId, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            return assetDiscardService.add(schemaName, request, loginUser, processDefinitionId, paramMap, true);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DE_IN_DE));
//        }
//
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备报废审核
//     */
//    @RequestMapping("/addWithoutAudit")
//    @Transactional
//    public ResponseModel addWithoutAudit(@RequestParam(name = "flow_id") String processDefinitionId, HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            return assetDiscardService.add(schemaName, request, loginUser, processDefinitionId, paramMap, false);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ASSET_DISCARD_DATA_WRONG));
//        }
//    }
//
//    /**
//     * 根据code查询盘点位置详情数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/findSingleAdInfo")
//    public ResponseModel findSingleAtInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = assetDiscardService.queryAssetDiscardByCode(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 报废详情
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_discard")
//    @RequestMapping("/detail-assetDiscard")
//    public ModelAndView bomInventoryStockDetail(HttpServletRequest request) {
//        try {
//            return assetDiscardService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description：查看列表
//     */
//    @MenuPermission("asset_discard")
//    @RequestMapping("/list")
//    public String selectAllDiscard(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String condition = "";
//            //设备类型
//            String status = request.getParameter("status");
//            String pageSize = request.getParameter("pageSize");
//            String facilities = request.getParameter("facilities");
//            String pageNumber = request.getParameter("pageNumber");
//            if (!RegexUtil.isNull(status) && !status.equals("-1")) {
//                condition += " and t.status='" + status + "'";
//            }
//            //获取组织的根节点数据，用于like进行比较
//            String[] facilityNos = dataPermissionForFacility.findFacilityNOsByBusinessForSingalUser(schemaName, loginUser, facilities.split(","), "asset_transfer");
//            if (facilityNos != null && facilityNos.length > 0) {
//                condition += " and (";
//                for (String facilityNo : facilityNos) {
//                    condition += " (p1.position_code like '"+facilityNo+"%' " +
//                            "or f1.facility_no like '"+facilityNo+"%' " +
//                            "or apo1.position_code like '"+facilityNo+"%' ) or";
//                }
//                condition = condition.substring(0, condition.length() - 2);
//                condition += ") ";
//            }
//            if (null != pageSize && null != pageNumber) {
//                PageHelper.startPage(Integer.parseInt(pageNumber), Integer.parseInt(pageSize));
//            }
//            List<Map<String, Object>> assetDiscardModel = assetDiscardService.selectAllDiscard(schemaName, condition, loginUser);
//            int num = assetDiscardService.selectAllDiscardNum(schemaName, condition);
//            //多时区处理
//            SCTimeZoneUtil.responseMapListDataHandler(assetDiscardModel);
//            result.put("rows", assetDiscardModel);
//            result.put("total", num);
//            return result.toString();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return "{}";
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备报废报废审核
//     */
//    @RequestMapping("/disCardAudit")
//    @Transactional
//    public ResponseModel transferOutAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetDiscardService.disCardAudit(schemaName, request, paramMap, loginUser,false);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备报废报废审核
//     */
//    @RequestMapping("/lastDisCardAudit")
//    @Transactional
//    public ResponseModel transferOutLastAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetDiscardService.disCardAudit(schemaName, request, paramMap, loginUser,true);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description:设备报废处理
//     */
//    @RequestMapping("/deal")
//    public ResponseModel deal(HttpServletRequest request, HttpServletResponse response) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String remark = request.getParameter("remark");
//            String discardCode = request.getParameter("id");
//            String asset_id = request.getParameter("asset_id");
//            String taskId = request.getParameter("taskId");
//            String val_deal = request.getParameter("val_deal");
//            String asset_name = request.getParameter("asset_name");
//            if (RegexUtil.isNull(discardCode) || RegexUtil.isNull(val_deal)) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_SYS_EXEC));
//            }
//
//            //go_back
//            boolean result = assetDiscardService.deal(schemaName, remark, discardCode, val_deal, loginUser.getAccount(), asset_id, taskId, asset_name);
//            if (val_deal != null && val_deal.equals("ok")) {
//                logService.AddLog(schemaName, "discard", discardCode, "设备报废确定", loginUser.getAccount());
//            } else if (val_deal != null && val_deal.equals("go_back")) {
//                logService.AddLog(schemaName, "discard", discardCode, "设备报废退回", loginUser.getAccount());
//            }
//            if (result) {
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC));
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATE_ERROR));
//            }
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATION_E));//操作出现异常  MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description:报废单据状态处理
//     */
//    @MenuPermission("asset_discard")
//    @RequestMapping("/cancel")
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String discard_code = request.getParameter("discard_code");
//            if (StringUtils.isBlank(discard_code)){
//                String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.errorMsg(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//
//            }
//
//            return assetDiscardService.cancel(schemaName, loginUser, discard_code);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
////    /**
////     * @param schemaName
////     * @param loginUser
////     * @param menuKey
////     * @param allFacilityName
////     * @param selfFacilityName
////     * @return
////     * @Description:根据菜单权限判断是否有全部场地查询权限，并返回拼接sql语句
////     */
////    private String checkFunctionPermission(String schemaName, String[] facility_id, User loginUser, String menuKey, String allFacilityName, String selfFacilityName, String trip) {
////        Boolean isAllFacility = false;
////        Boolean isSelfFacility = false;
////        String condition = " ";
////        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schemaName, loginUser.getId(), menuKey);
////        if (userFunctionList != null || !userFunctionList.isEmpty()) {
////            for (UserFunctionData functionData : userFunctionList) {
////                //如果可以查看全部位置，则不限制
////                if (functionData.getFunctionName().equals(allFacilityName)) {
////                    isAllFacility = true;
////                    break;
////                } else if (functionData.getFunctionName().equals(selfFacilityName)) {
////                    isSelfFacility = true;
////                }
////            }
////        }
////        if (facility_id != null && facility_id.length > 0 && !facility_id[0].equals("")) {
////            List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schemaName);
////            //加上位置的子位置
////            String facilitiesAll = StringUtil.join(facility_id);
////            String subFacility = facilitiesService.getSubFacilityIds(facilitiesAll, facilityList);
////            if (subFacility != null && !subFacility.isEmpty()) {
////                facilitiesAll += subFacility;
////                //包含子的位置数组
////                condition = " and " + trip + ".intsiteid in (" + facilitiesAll + ") ";
////            }
////        } else if (isAllFacility) {
////            condition = " ";
////        } else if (isSelfFacility) {
////            LinkedHashMap<String, IFacility> facilityList = loginUser.getFacilities();
////            String facilityIds = "";
////            if (facilityList != null && !facilityList.isEmpty()) {
////                for (String key : facilityList.keySet()) {
////                    facilityIds += facilityList.get(key).getId() + ",";
////                }
////            }
////            if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
////                facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
////                condition = " and " + trip + ".intsiteid in (" + facilityIds + ") ";
////            }
////        }
////        return condition;
////    }
//
//    @RequestMapping("/asset")
//    public ResponseModel asset(HttpServletResponse response, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String assetId = request.getParameter("assetId");
//            Map<String, Object> asset = assetMapper.getAssetDetailById(schemaName, assetId);
//            if (asset != null) {
//                return ResponseModel.ok(asset);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NULL_ASS_DETAIL));
//            }
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATION_E));//操作出现异常  MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * 查询所属位置的组织编码，如果是“P”开头，则直接返回
//     *
//     * @param response
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_facility_by_id")
//    @ResponseBody
//    public ResponseModel getFacilityById(HttpServletResponse response, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            if (!facilityId.startsWith("P")) {
//                Facility facility = facilitiesService.FacilitiesById(schemaName, Integer.valueOf(facilityId));
//                if (facility != null) {
//                    return ResponseModel.ok(facility.getFacilityNo());
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NULL_ASS_DETAIL));
//                }
//            } else {
//                return ResponseModel.ok(facilityId);
//            }
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATION_E));//操作出现异常  MSG_OPERATION_E
//        }
//    }
}
