package com.gengyun.senscloud.controller.suez;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.model.SuezReportAdd;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.suez.SuezReportService;
import com.gengyun.senscloud.util.SenscloudException;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * suez - 日常报告
 */
@Api(tags = "中法-日常报告")
@RestController
public class SuezReportController {
    private static final Lock lock = new ReentrantLock();
    @Resource
    SuezReportService suezReportService;

    /**
     * 获取日常报告列表
     *
     * @param methodParam
     * @param searchParam
     * @return
     */
    @ApiOperation(value = "获取日常报告列表", notes = ResponseConstant.RSP_DESC_SEARCH_DAILY_REPORT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageNumber", "pageSize", "startDateSearch", "endDateSearch", "keywordSearch"})
    @RequestMapping(value = "/findSuezReportList", method = RequestMethod.POST)
    public ResponseModel findSuezReportList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SearchParam searchParam) {
        return ResponseModel.ok(suezReportService.getSuezReportList(methodParam, paramMap));
    }


    @ApiOperation(value = "新增日常报告", notes = ResponseConstant.RSP_DESC_ADD_DAILY_REPORT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "report_name", "begin_time", "end_time", "position_code", "work_type_ids", "searchAsset"})
    @Transactional //支持事务
    @RequestMapping(value = "/addSuezReport", method = RequestMethod.POST)
    public ResponseModel addSuezReport(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SuezReportAdd reportAdd) {
        suezReportService.newSuezReport(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();

    }

    @ApiOperation(value = "删除日常报告", notes = ResponseConstant.RSP_DESC_REMOVE_DAILY_REPORT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/removeSuezReport", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSuezReport(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        suezReportService.cutSuezReport(methodParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "生成日常报告", notes = ResponseConstant.RSP_DESC_DOWNLOAD_DAILY_REPORT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getImportSuezReport", method = RequestMethod.GET)
    public void getImportSuezReport(MethodParam methodParam, SearchParam param) {
        suezReportService.getImportSuezReport(methodParam, param);
    }

    @ApiOperation(value = "新增日常报告-NT", notes = ResponseConstant.RSP_DESC_ADD_DAILY_REPORT_NT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "report_name", "begin_time", "end_time", "position_code", "work_type_ids", "bom_type_ids", "stock_ids", "asset_position_codes", "asset_type_ids"})
    @Transactional //支持事务
    @RequestMapping(value = "/addSuezReportNT", method = RequestMethod.POST)
    public ResponseModel addSuezReportNT(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SuezReportAdd reportAdd) {
        try {
            //避免报告功能并发执行
            if (!lock.tryLock()) {
                throw new SenscloudException(LangConstant.LOG_C);
            }
            Long id = suezReportService.newSuezReportNT(methodParam, paramMap);
            if (id != null) {
                //paramMap.put("id",String.valueOf(id));
                SearchParam param = new SearchParam();
                param.setId(String.valueOf(id));
                suezReportService.getImportSuezReportNew(methodParam, param);
            }
            return ResponseModel.okMsgForOperate();
        } catch (SenscloudException se) {
            throw se;
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }


    @ApiOperation(value = "下载日常报告-NT", notes = ResponseConstant.RSP_DESC_DOWNLOAD_DAILY_REPORT_NT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getImportSuezReportNT", method = RequestMethod.GET)
    public ModelAndView getImportSuezReportNT(MethodParam methodParam, SearchParam param) {
        return suezReportService.downLoadSuezReport(methodParam, param);
    }

    @ApiOperation(value = "报告是否已生成-NT", notes = ResponseConstant.RSP_DESC_REPORT_EXIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/SuezReportExistNT", method = RequestMethod.GET)
    public ResponseModel SuezReportExistNT(MethodParam methodParam, SearchParam param) {
        String message = "";
        boolean exist = suezReportService.SuezReportExistNT(methodParam, param);
        if (!exist) {
            message = "报告尚未生成";
        }
        Map<String, Object> obj = new HashMap<>();
        obj.put("exist", exist);
        return new ResponseModel(ResponseConstant.SUCCESS_CODE, message, obj);
    }

    /**
     * 获取日常报告列表
     *
     * @param methodParam
     * @param searchParam
     * @return
     */
    @ApiOperation(value = "获取日常报告列表-NT", notes = ResponseConstant.RSP_DESC_SEARCH_DAILY_REPORT_LIST_NT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageNumber", "pageSize", "startDateSearch", "endDateSearch", "keywordSearch"})
    @RequestMapping(value = "/findSuezReportListNT", method = RequestMethod.POST)
    public ResponseModel findSuezReportListNT(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SearchParam searchParam) {
        return ResponseModel.ok(suezReportService.getSuezReportListNT(methodParam, paramMap));
    }

    @ApiOperation(value = "删除日常报告-NT", notes = ResponseConstant.RSP_DESC_REMOVE_DAILY_REPORT_NT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/removeSuezReportNT", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSuezReportNT(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        suezReportService.cutSuezReportNT(methodParam);
        return ResponseModel.okMsgForOperate();
    }


}
