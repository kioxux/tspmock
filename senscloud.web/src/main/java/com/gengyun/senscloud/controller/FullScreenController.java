package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.LinkedHashMap;
//import java.util.List;
//
///**
// * 功能：全面屏面板
// * Created by Dong wudang on 2018/10/15.
// */

import org.springframework.web.bind.annotation.RestController;
@RestController
public class FullScreenController {
//
//    @Autowired
//    AuthService authService;
//    @Autowired
//    RepairService repairService;
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    InspectionService inspectionService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/full-screen")
//    public ModelAndView InitRepairAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            return new ModelAndView("fullScreen/full-screen-board");
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_BOARD_ERR) + ex.getMessage());
//            return new ModelAndView("fullScreen/full-screen-board");
//        }
//    }
//
//    /**
//     * 功能：大屏tip面板数据查询方法
//     * @param response
//     * @param request
//     * @return
//     */
//    @RequestMapping("/getTipBoard")
//    public ResponseModel getTipBoard(HttpServletResponse response, HttpServletRequest request){
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        FullScreenData fullScreenData = new FullScreenData();
//        try{
//            int failureRepair = 0;
//            int completeFailureRepair = 0;
//            int plannedMaintain = 0;
//            int completePlannedMaintain = 0;
//            int plannedPointInspection = 0;
//            int completePlannedPointInspection = 0;
//            int plannedTodayWork = 0;
//            int completePlannedTodayWork = 0;
//            fullScreenData = repairService.getPlannedFailureRepair(schema_name);//维修计划任务
//            FullScreenData maintainData = maintainService.getPlannedAndCompleteMaintain(schema_name);//保养数据任务
//            FullScreenData inspectionData = inspectionService.getPlannedAndCompleteInspection(schema_name);
//            FullScreenData spotInspectionData = inspectionService.getPlannedAndCompleteSpotInspection(schema_name);
//            completePlannedPointInspection = inspectionData.getInspection()+spotInspectionData.getSpot();
//            if(maintainData==null){
//                fullScreenData.setPlannedMaintain(plannedMaintain);
//                fullScreenData.setCompletePlannedMaintain(completePlannedMaintain);
//            }
//            fullScreenData.setPlannedMaintain(maintainData.getPlannedMaintain());
//            fullScreenData.setCompletePlannedMaintain(maintainData.getCompletePlannedMaintain());
//            fullScreenData.setCompletePlannedPointInspection(completePlannedPointInspection);
//
//            result.setMsg("");
//            result.setCode(1);
//            result.setContent(fullScreenData);
//            return  result;
//        }catch (Exception e){
//            result.setCode(-1);
//            result.setContent("");
//            result.setMsg("");
//            return  result;
//        }
//    }
//
//    @RequestMapping("/find-asset-failure-list")
//    public String queryAssetFailureList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//
//        try {
//            int begin = pageSize * (pageNumber - 1);
//            List<RepairData>  assetList =  repairService.queryAssetFailureList(schema_name,pageSize,begin);
//            int total = repairService.queryAssetFailureListCount(schema_name);
//
//            result.put("rows", assetList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
}
