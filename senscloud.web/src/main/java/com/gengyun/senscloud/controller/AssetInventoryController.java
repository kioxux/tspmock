package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.flowable.CustomTaskEntityImpl;
//import com.gengyun.senscloud.mapper.AssetInventoryMapper;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.asset.AssetInventoryService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.WorkflowService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 设备盘点
// */
//@RestController
//@RequestMapping("/asset_inventory")
public class AssetInventoryController {

//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private AssetInventoryMapper assetInventoryMapper;
//
//    @Autowired
//    private AssetInventoryService assetInventoryService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            Map<String, Object> workType = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_24);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("asset_inventory_add", "rpAddFlag");
//                put("asset_inventory_delete", "rpDeleteFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "asset_inventory/index", "asset_inventory")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("asset_inventory/index");
//        }
//    }
//
//    /**
//     * 设备盘点位置页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("/inventory_org")
//    public ModelAndView inventoryOrgList(HttpServletRequest request) {
//        try {
//            User user = AuthService.getLoginUser(request);
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String inventoryCode = request.getParameter("inventoryCode");
//            Map<String, Object> map = assetInventoryService.queryAssetInventoryByCode(schemaName, inventoryCode);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("asset_inventory_edit", "rpEditFlag");
//                put("asset_pc_inventory", "pcInventoryFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "asset_inventory/inventory_org", "asset_inventory")
//                    .addObject("inventoryCode", inventoryCode)
//                    .addObject("inventory_name", (String) map.get("inventory_name"))
//                    .addObject("userAccount", user.getAccount());
//        } catch (Exception e) {
//            return new ModelAndView("asset_inventory/inventory_org");
//        }
//    }
//
//    /**
//     * 盘点设备列表页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("inventory_org_detail")
//    public ModelAndView inventoryOrgDetailList(HttpServletRequest request) {
//        try {
//            User user = AuthService.getLoginUser(request);
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String inventoryCode = request.getParameter("inventoryCode");
//            String subInventoryCode = request.getParameter("subInventoryCode");
//            String facilityId = request.getParameter("facilityId");
//            //获取流程信息
//            List<CustomTaskEntityImpl> taskList = workflowService.getTasks(schemaName, "", null, Arrays.asList(subInventoryCode), null, null, false, null, null, null, null, null);
//            String taskId = "";
//            String assignee = "";
//            if (taskList != null && taskList.size() > 0) {
//                taskId = taskList.get(0).getId();
//                assignee = taskList.get(0).getAssignee();
//            }
//            ModelAndView mav = new ModelAndView("asset_inventory/inventory_org_detail");
//            Map<String, Object> map = assetInventoryService.queryAssetInventoryByCode(schemaName, inventoryCode);
//            Map<String, Object> org = assetInventoryMapper.queryAssetInventoryOrgByCode(schemaName, subInventoryCode);
//            if (map != null) {
//                mav.addObject("inventoryName", (String) map.get("inventory_name"));
//            }
//            return mav.addObject("inventoryCode", inventoryCode)
//                    .addObject("subInventoryCode", subInventoryCode)
//                    .addObject("facilityId", facilityId)
//                    .addObject("taskId", taskId)
//                    .addObject("isShowBtn", user.getAccount().equals(assignee))
//                    .addObject("orgStatus", org.get("status"))
//                    .addObject("orgName", org.get("orgname"));
//        } catch (Exception e) {
//            return new ModelAndView("asset_inventory/inventory_org_detail");
//        }
//    }
//
//    /**
//     * 查询设备盘点列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("findInventoryList")
//    public String findInventoryList(HttpServletRequest request) {
//        try {
//            return assetInventoryService.findAssetInventoryList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询盘点位置列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("findInventoryOrgList")
//    public String findInventoryOrgList(HttpServletRequest request) {
//        try {
//            return assetInventoryService.findInventoryOrgList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询盘点设备列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("findInventoryOrgDetailList")
//    public String findInventoryOrgDetailList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return assetInventoryService.findInventoryOrgDetailList(request, schemaName);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 设备盘点作废
//     *
//     * @param request
//     * @return
//     */
//    @Transactional
//    @RequestMapping("invalidInventory")
//    public ResponseModel invalidInventory(HttpServletRequest request) {
//        try {
//            return assetInventoryService.invalidInventory(request);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.errorMsg(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 根据code查询盘点位置详情数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/findSingleAidInfo")
//    public ResponseModel findSingleAidInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = assetInventoryService.queryAssetInventoryStockByCode(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 盘点分配页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("/detail-assetInventoryOrg")
//    public ModelAndView assetInventoryOrgDetail(HttpServletRequest request) {
//        try {
//            return assetInventoryService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 启动盘点提交
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_inventory")
//    @RequestMapping("/addInventory")
//    @Transactional
//    public ResponseModel addInventory(@RequestParam(name = "flow_id") String processDefinitionId, HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return assetInventoryService.addInventory(schemaName, request, processDefinitionId, paramMap);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_START_INV_FAIL));
//        }
//    }
//
//    /**
//     * PC盘点提交
//     *
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("pcInventory")
//    @Transactional
//    public ResponseModel pcInventory(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return assetInventoryService.pcInventory(schemaName, request, paramMap);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PC_INVENTORY_FAIL));
//        }
//    }
//
//    /**
//     * 盘点分配提交
//     *
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("inventoryAllot")
//    @Transactional
//    public ResponseModel inventoryAllot(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return assetInventoryService.inventoryAllot(schemaName, request, paramMap);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_INVENTORY_ALLOT_FAIL));
//        }
//    }
//
//    /**
//     * 盘点盈余设备的调整（后台走调拨流程）
//     *
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("assetTransfer")
//    @Transactional
//    public ResponseModel assetTransfer(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetInventoryService.assetTransfer(schemaName, request, paramMap);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 盘点盈余设备的报废
//     *
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("assetDiscard")
//    @Transactional
//    public ResponseModel assetDiscard(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetInventoryService.assetDiscard(schemaName, request, paramMap);
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 盘点完成
//     *
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("finishInventory")
//    @Transactional
//    public ResponseModel finishInventory(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetInventoryService.finishInventory(schemaName, request, paramMap);
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 查询分拨中心下，需要盘点的设备总数(模板页面联动获取分拨中心设备数量)
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("getAssetInventoryQuantity")
//    public ResponseModel getAssetInventoryQuantity(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetInventoryService.getAssetInventoryQuantity(schemaName, request);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 查询设备详情
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("getAssetFacilitiesById")
//    public ResponseModel getAssetFacilitiesById(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetInventoryService.getAssetFacilitiesById(schemaName, request);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
}
