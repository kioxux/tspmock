package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.TaskItemModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.TaskItemService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "任务项管理")
@RestController
public class TaskItemController {
    @Resource
    TaskItemService taskItemService;

    @ApiOperation(value = "获取任务项模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_SYSTEM_CONFIG_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchTaskItemPermission", method = RequestMethod.POST)
    public ResponseModel searchTaskItemPermission(MethodParam methodParam) {
        return ResponseModel.ok(taskItemService.getTaskItemPermission(methodParam));
    }

    @ApiOperation(value = "新增任务项", notes = ResponseConstant.RSP_DESC_ADD_TASK_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_name", "result_type", "requirements", "result_verification"})
    @Transactional
    @RequestMapping(value = "/addTaskItem", method = RequestMethod.POST)
    public ResponseModel addTaskItem(MethodParam methodParam, TaskItemModel taskItemModel) {
        taskItemService.newTaskItem(methodParam, taskItemModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑任务项", notes = ResponseConstant.RSP_DESC_EDIT_TASK_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_code", "task_item_name", "result_type", "requirements", "result_verification"})
    @Transactional
    @RequestMapping(value = "/editTaskItem", method = RequestMethod.POST)
    public ResponseModel editTaskItem(MethodParam methodParam, TaskItemModel taskItemModel) {
        taskItemService.modifyTaskItem(methodParam, taskItemModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除任务项", notes = ResponseConstant.RSP_DESC_REMOVE_TASK_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_code"})
    @Transactional
    @RequestMapping(value = "/removeTaskItem", method = RequestMethod.POST)
    public ResponseModel removeTaskItem(MethodParam methodParam, TaskItemModel taskItemModel) {
        taskItemService.cutTaskItem(methodParam, taskItemModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "任务项详情", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_ITEM_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_code"})
    @RequestMapping(value = "/searchTaskItemInfo", method = RequestMethod.POST)
    public ResponseModel searchTaskItemList(MethodParam methodParam, TaskItemModel taskItemModel) {
        return ResponseModel.ok(taskItemService.getTaskItemInfo(methodParam, taskItemModel));
    }

    @ApiOperation(value = "分页查询任务项列表", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_ITEM_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch",})
    @RequestMapping(value = "/searchTaskItemPage", method = RequestMethod.POST)
    public ResponseModel searchTaskItemPage(MethodParam methodParam, TaskItemModel taskItemModel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(taskItemService.getTaskItemList(methodParam, taskItemModel));
    }

    @ApiOperation(value = "任务项选择列表", notes = ResponseConstant.RSP_DESC_SEARCH_CHOSE_TASK_ITEM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_code", "keywordSearch",})
    @RequestMapping(value = "/searchChoseTaskItemList", method = RequestMethod.POST)
    public ResponseModel searchChoseTaskItemList(MethodParam methodParam, TaskItemModel taskItemModel) {
        return ResponseModel.ok(taskItemService.getChoseTaskItemList(methodParam, taskItemModel));
    }

    @ApiOperation(value = "根据任务模板编码查询任务项列表", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_ITEM_LIST_BY_TEMPLATE_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_code", "keywordSearch",})
    @RequestMapping(value = "/searchTaskItemListByTempLateCode", method = RequestMethod.POST)
    public ResponseModel searchTaskItemListByTempLateCode(MethodParam methodParam, TaskItemModel taskItemModel) {
        return ResponseModel.ok(taskItemService.getTaskItemListByTempLateCode(methodParam, taskItemModel));
    }

    @ApiOperation(value = "任务项附件删除", notes = ResponseConstant.RSP_DESC_ASSET_FILE_UPDATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "file_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeTaskItemFile", method = RequestMethod.POST)
    public ResponseModel removeTaskItemFile(MethodParam methodParam, TaskItemModel taskItemModel) {
        taskItemService.doModifyTaskItemFile(methodParam, taskItemModel, false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "任务项附件新增", notes = ResponseConstant.RSP_DESC_TASK_ITEM_FILE_UPDATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "file_id", "task_item_code"})
    @Transactional //支持事务
    @RequestMapping(value = "/addTaskItemFile", method = RequestMethod.POST)
    public ResponseModel addTaskItemFile(MethodParam methodParam, TaskItemModel taskItemModel) {
        taskItemService.doModifyTaskItemFile(methodParam, taskItemModel, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取任务项附件文档信息", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_ITEM_FILE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_code", "keywordSearch"})
    @RequestMapping(value = "/searchTaskItemFileList", method = RequestMethod.POST)
    public ResponseModel searchTaskItemFileList(MethodParam methodParam, TaskItemModel taskItemModel) {
        return ResponseModel.ok(taskItemService.getTaskItemFileList(methodParam, taskItemModel));
    }

    @ApiOperation(value = "获取选中任务项导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_TASK_ITEM_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_codes"})
    @RequestMapping(value = "/getExportTaskItemCode", method = RequestMethod.POST)
    public ResponseModel getExportTaskItemCode(MethodParam methodParam, TaskItemModel taskItemModel) {
        return ResponseModel.okExport(taskItemService.getExportTaskItem(methodParam, taskItemModel, false));
    }

    @ApiOperation(value = "获取全部任务项导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_TASK_ITEM_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/getExportAllTaskItemCode", method = RequestMethod.POST)
    public ResponseModel getExportAllTaskItemCodes(MethodParam methodParam, TaskItemModel taskItemModel) {
        return ResponseModel.okExport(taskItemService.getExportTaskItem(methodParam, taskItemModel, true));
    }
}
