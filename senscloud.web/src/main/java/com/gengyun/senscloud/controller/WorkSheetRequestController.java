package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Constants;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 工单请求
// */
//@RestController
//@RequestMapping("/worksheet_request")
public class WorkSheetRequestController {
//    @Autowired
//    WorkSheetRequestService workSheetRequestService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//    @Autowired
//    WorkHandleUnMergeService workHandleUnMergeService;
//
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("worksheet_request")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("worksheet_request_add", "rpAddFlag");
//        }};
//        ModelAndView mv = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "work/worksheet_request/index", "worksheet_request");
//        permissionInfo.put("worksheet_invalid", "rpInvalidFlag");
//        ModelAndView mvWork = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, null, "worksheet");
//        mv.getModel().putAll(mvWork.getModel());
//
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String workRequestFlow = selectOptionService.getOptionNameByCode(schemaName, "work_type_by_business_info", Constants.WORK_REQUEST_BUSINESS_TYPE, "relation");
//        String msgNone = selectOptionService.getLanguageInfo(LangConstant.NONE_A);
//        if (RegexUtil.isNull(workRequestFlow) || msgNone.equals(workRequestFlow)) {
//            workRequestFlow = "";
//            List<Map<String, Object>> workFormKeyList = selectOptionService.SelectOptionList(schemaName, request, "work_template_for_detail", "and(template_type=4)", null);
//            if (null != workFormKeyList && workFormKeyList.size() == 1) {
//                workRequestFlow = selectOptionService.getOptionNameByCode(schemaName, "work_type", String.valueOf((Integer) workFormKeyList.get(0).get("work_type_id")), "relation");
//            }
//        }
//        mv.getModel().put("workRequestFlow", workRequestFlow);
//        return mv;
//    }
//
//    /**
//     * 获取工单请求列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/findWsqList")
//    public String queryWsqList(HttpServletRequest request) {
//        try {
//            return workSheetRequestService.findWsqList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/findSingleWsqInfo")
//    public ResponseModel findSingleWorkInfo(HttpServletRequest request, HttpServletResponse response) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = workSheetRequestService.queryWorkRequestById(schemaName, subWorkCode);
//            //时区转换处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 作废工单
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("worksheet_invalid")
//    @RequestMapping("/cancelWsq")
//    @Transactional
//    public ResponseModel cancelWsq(HttpServletRequest request) {
//        try {
//            return workSheetRequestService.cancelWsq(request);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUB_EXCEP));
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/detail-worksheetRequest")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return workSheetRequestService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 问题上报【单独流程】
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/doSave")
//    @Transactional //支持事务
//    public ResponseModel saveWorksheetRequest(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return workHandleUnMergeService.save(schemaName, processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    // 问题上报（请求确认）
//    @RequestMapping("/doConfirmRequest")
//    @Transactional //支持事务
//    public ResponseModel confirmWsq(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.confirmWsq(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    // 问题上报（请求确认）【单独流程】
//    @RequestMapping("/doConfirmRequestWorkOnly")
//    @Transactional //支持事务
//    public ResponseModel confirmWsqWorkOnly(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workHandleUnMergeService.confirmWsq(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 取工单请求业务号
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/findWqBusinessInfo")
//    public ResponseModel findWqBusinessInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String info = workSheetRequestService.selectWqBusinessInfo(schemaName);
//            return ResponseModel.ok(info);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
}
