package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SecurityModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.SecurityManageService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "职安健管理")
@RestController
public class SecurityManageController {
    @Resource
    SecurityManageService securityManageService;

    @ApiOperation(value = "查询职安健", notes = ResponseConstant.RSP_DESC_SEARCH_SECURITY_MANAGE_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "security_item_code"})
    @RequestMapping(value = "/searchSecurityManageInfo", method = RequestMethod.POST)
    public ResponseModel searchSecurityManageInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SecurityModel securityModel) {
        return ResponseModel.ok(securityManageService.getSecurityDetailByCode(methodParam, paramMap));
    }

    @ApiOperation(value = "分页查询职安健", notes = ResponseConstant.RSP_DESC_SEARCH_SECURITY_MANAGE_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchSecurityManagePage", method = RequestMethod.POST)
    public ResponseModel searchAssetModelListPage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SecurityModel securityModel) {
        return ResponseModel.ok(securityManageService.getSecurityManage(methodParam, paramMap));
    }

    @ApiOperation(value = "查询所有职安健", notes = ResponseConstant.RSP_DESC_SEARCH_ALL_SECURITY_MANAGE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchAllSecurityManageList", method = RequestMethod.POST)
    public ResponseModel searchAllSecurityManageList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SecurityModel securityModel) {
        return ResponseModel.ok(securityManageService.getSecurityManageList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增职安健", notes = ResponseConstant.RSP_DESC_ADD_SECURITY_MANAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "security_item_name", "rask_note", "result_type", "standard"})
    @Transactional //支持事务
    @RequestMapping(value = "/addSecurityManage", method = RequestMethod.POST)
    public ResponseModel addSecurityManage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SecurityModel securityModel) {
        securityManageService.newSecurityManage(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除职安健", notes = ResponseConstant.RSP_DESC_REMOVE_SECURITY_MANAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "security_item_code"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeSecurityManage", method = RequestMethod.POST)
    public ResponseModel removeSecurityManage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SecurityModel securityModel) {
        securityManageService.cutSecurityManage(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }


    @ApiOperation(value = "更新职安健", notes = ResponseConstant.RSP_DESC_MODIFY_SECURITY_MANAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "security_item_code", "security_item_name", "rask_note", "result_type", "standard"})
    @Transactional //支持事务
    @RequestMapping(value = "/modifySecurityManage", method = RequestMethod.POST)
    public ResponseModel modifySecurityManage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SecurityModel securityModel) {
        securityManageService.modifySecurityManage(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }


}
