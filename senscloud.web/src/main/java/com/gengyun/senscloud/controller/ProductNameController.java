package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.ProductNameService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.xml.ws.RequestWrapper;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @Author: Wudang Dong
// * @Description:
// * @Date: Create in 下午 1:35 2019/10/25 0025
// */
//@RestController
//@RequestMapping("/product_name")
public class ProductNameController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    ProductNameService productNameService;
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//
//
//
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("product_add", "addFlag");//是否有添加权限
//                put("product_edit", "editFlag");//是否有编辑权限
//                put("product_del", "delFlag");//是否有删除权限
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "product_name/index", "product_name");
//        } catch (Exception e) {
//            return new ModelAndView("product_name/index");
//        }
//    }
//
//    @RequestMapping("/findProductNameList")
//    public String findBomInStockList(HttpServletRequest request) {
//        try {
//            return productNameService.findProductNameList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * @Description：保存或者编辑
//     * @param request
//     * @return
//     */
//    @RequestMapping("/addOrEdit")
//    public ResponseModel addProductName(HttpServletRequest request){
//        try {
//            return productNameService.add(request);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            ex.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
//
//    @RequestMapping("/delProductName")
//    public ResponseModel delProductName(HttpServletRequest request){
//        try {
//            return productNameService.dele(request);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            ex.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }



}
