package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.*;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//@RestController
//@RequestMapping("/charts")
public class AnalysChartController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    AnalysChartService analysChartService;
//    @Autowired
//    AnalysReportService analysReportService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//
//    /*****通用部分***********************************************************************/
//
//    //获取设备类型
//    @RequestMapping("/get_asset_type_data")
//    public ResponseModel getAssetTypeList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            //获取设备类型
//            List<MetaDataAsset> metaAssetList = maintenanceSettingsService.findAllMetaDataAsset(schema_name);
//            return ResponseModel.ok(metaAssetList);
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    /*****分析部分****************************************************************************/
//
//    //维修时效分析视图
//    @MenuPermission("statistic")
//    @RequestMapping("/repair_hour_radar")
//    public ModelAndView InitRepairTimesPolar(HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "repair_hour_radar");
//            return new ModelAndView("charts/repair_hour_radar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_RE_STA);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("charts/repair_hour_radar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //维修时效分析
//    @MenuPermission("statistic")
//    @RequestMapping("/get_repair_hour_radar_data")
//    public ResponseModel getRepairTimesPolarData(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String condition = "";
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.finished_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.finished_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "worksheet");
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and w.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and (r.create_user_account='" + account +"' or r.receive_account='" + account +"' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and w.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            //时效结果
//            AnalysRepairWorkResult repairReportList = analysReportService.GetRepairHoursByFacilityForChart(schema_name, condition);
//            result.setCode(1);
//            result.setContent(repairReportList);
//            String tmpMsg = selectOptionService.getLanguageInfo(  LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//    //百台设备维修次数视图
//    @MenuPermission("statistic")
//    @RequestMapping("/repair_times_bar")
//    public ModelAndView InitRepairTimesBarPercentDevice(HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "repair_hour_radar");
//            return new ModelAndView("charts/repair_times_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_RE_TIMES);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("charts/repair_times_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //百台设备维修次数分析，按次数，取前20个，
//    @MenuPermission("statistic")
//    @RequestMapping("/get_repair_times_bar_data")
//    public ResponseModel getRepairTimesBarData(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String condition = "";
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and finished_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and finished_time <'" + end + "'";
//            }
//
//            //时效结果
//            List<AnalysRepairWorkResult> repairTimesList = analysReportService.GetRepairTimesByPercentForChart(schema_name, condition);
//            result.setCode(1);
//            result.setContent(repairTimesList);
//            String tmpMsg = selectOptionService.getLanguageInfo(  LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//    //位置维修时效、故障率、保养完成率、保养效率图表分析视图
//    @MenuPermission("statistic")
//    @RequestMapping("/facility_repair_bar")
//    public ModelAndView InitFacilityChart(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "facility_chart");
//            return new ModelAndView("charts/facility_repair_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_MAP);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("charts/facility_repair_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //位置维修时效、故障率、保养完成率、保养效率的图表分析
//    @MenuPermission("statistic")
//    @RequestMapping("/facility_chart_data")
//    public ResponseModel facilityChartData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String orderBy = request.getParameter("orderBy");
//
//            if (orderBy == null || orderBy.isEmpty()) {
//                orderBy = "repairTimesPerPercent";
//            }
//
//            String facilityCondition = " ";
//            String repairCondition = " ";
//            String maintainCondition = " ";
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and r.begin_time >='" + beginDate + "'";
//                maintainCondition += " and mt.begin_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and r.begin_time <'" + end + "'";
//                maintainCondition += " and mt.begin_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "facility_chart");
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    facilityCondition += " ";
//                } else {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        facilityCondition += " and f.id in (" + facilityIds + ") ";
//                    } else {
//                        facilityCondition += " and f.id in () ";    //没有用户所属位置，则按个人权限查询
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                facilityCondition += " and f.id in (" + facilityId + subFacility + ") ";
//            }
//
//            //时效结果
//            List<RepairAndMaintainChartAnalysIndicator> facilityChartData = analysChartService.GetFacilityAnalysChart(schema_name, facilityCondition, repairCondition, maintainCondition, orderBy);
//
//            result.setCode(1);
//            result.setContent(facilityChartData);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//    //供应商设备故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、平均维修时长统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/supplier_repair_bar")
//    public ModelAndView InitSupplierChart(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "supplier_chart");
//            return new ModelAndView("charts/supplier_repair_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_FAI_RA_CHART);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("charts/supplier_repair_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //获取供应商设备故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、平均维修时长统计数据
//    @MenuPermission("statistic")
//    @RequestMapping("/supplier_chart_data")
//    public ResponseModel supplierChartData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
////            String assetType = request.getParameter("assetType");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String pageNumber = request.getParameter("pageNumber");
//            String orderBy = request.getParameter("orderBy");
//            int pageSize = 20;
//
//            if (orderBy == null || orderBy.isEmpty()) {
//                orderBy = "fault_rate";
//            }
//            if (pageNumber != null && !pageNumber.isEmpty()) {
//                pageSize = Integer.parseInt(pageNumber);
//            }
//
//            String facilityCondition = " ";
//            String repairCondition = " ";
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and wk.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and wk.occur_time <'" + end + "'";
//            }
//
////            if (assetType != null && !assetType.isEmpty() && !assetType.equals("") && !assetType.equals("0")) {
////                facilityCondition += " and dv.asset_type ='" + assetType + "' ";
////            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "supplier_chart");
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    facilityCondition += " ";
//                } else {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        facilityCondition += " and dv.intsiteid in (" + facilityIds + ") ";
//                    } else {
//                        facilityCondition += " and dv.intsiteid in () ";    //没有用户所属位置，则按个人权限查询
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                facilityCondition += " and dv.intsiteid in (" + facilityId + subFacility + ") ";
//            }
//
//            //结果
//            List<SupplierChartResult> supplierChartData = analysChartService.GetSupplierAnalysChart(schema_name, facilityCondition, repairCondition, orderBy, pageSize);
//
//            result.setCode(1);
//            result.setContent(supplierChartData);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//
//    //设备故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/device_repair_bar")
//    public ModelAndView initDeviceChart(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "device_chart");
//            return new ModelAndView("charts/device_repair_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_EQ);
//		    result.setMsg(tmpMsg  + ex.getMessage());
//            return new ModelAndView("charts/device_repair_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //设备维修时效、保养时效、平均维修时长、维修次数、保养次数统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/device_performance_bar")
//    public ModelAndView initDevicePerformanceChart(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "device_performance_bar");
//            return new ModelAndView("charts/device_performance_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_EQ);
//		    result.setMsg(tmpMsg  + ex.getMessage());
//            return new ModelAndView("charts/device_performance_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//
//    //设备维修时效、保养时效、故障率、电器故障率、电机故障率、皮带故障率、滚筒故障率、轴承故障率、其他故障率、平均维修时长、维修次数、保养次数数据
//    @MenuPermission("statistic")
//    @RequestMapping("/device_chart_data")
//    public ResponseModel deviceChartData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String pageNumber = request.getParameter("pageNumber");
//            String orderBy = request.getParameter("orderBy");
//            String deviceFunctionType = request.getParameter("deviceFunctionType");
//            String searchAssetType = request.getParameter("searchAssetType");
//            int pageSize = 20;
//
//            if (orderBy == null || orderBy.isEmpty()) {
//                orderBy = "fault_rate";
//            }
//            if (pageNumber != null && !pageNumber.isEmpty()) {
//                pageSize = Integer.parseInt(pageNumber);
//            }
//
//            String facilityCondition = " ";
//            String repairCondition = " ";
//            String maintainCondition = " ";
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and occur_time >='" + beginDate + "'";
//                maintainCondition += " and occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and occur_time <'" + end + "'";
//                maintainCondition += " and occur_time <'" + end + "'";
//            }
//
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), deviceFunctionType);
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    facilityCondition += " ";
//                } else {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        facilityCondition += " and dv.intsiteid in (" + facilityIds + ") ";
//                    } else {
//                        facilityCondition += " and dv.intsiteid in () ";    //没有用户所属位置，则按个人权限查询
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                facilityCondition += " and dv.intsiteid in (" + facilityId + subFacility + ") ";
//            }
//            if (searchAssetType != null && !searchAssetType.isEmpty() && !searchAssetType.equals("-1")) {
//
//                facilityCondition += " and (dv.category_id= " + searchAssetType + ")";
//            }
//
//            //结果
//            List<SupplierChartResult> supplierChartData = analysChartService.GetDeviceAnalysChart(schema_name, facilityCondition, repairCondition, maintainCondition, orderBy, pageSize);
//
//            result.setCode(1);
//            result.setContent(supplierChartData);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//
//    //员工保养时效占比、保养完成率统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/user_repair_bar")
//    public ModelAndView InitUserChart(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "user_repair_chart");
//            return new ModelAndView("charts/user_repair_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_EM);
//			result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("charts/user_repair_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//
//    //员工维修时效、保养时效、工作饱和度、维修次数、保养次数统计视图
//    @MenuPermission("statistic")
//    @RequestMapping("/user_performance_bar")
//    public ModelAndView initUserPerformanceChart(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean canSeeAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "user_performance_bar");
//            return new ModelAndView("charts/user_performance_bar").addObject("canSeeAllFacility", canSeeAllFacility);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_ERR_EM);
//			result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("charts/user_performance_bar").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //获取员工维修时效、保养时效、工作饱和度、保养时效占比、保养完成率、维修次数、保养次数数据
//    @MenuPermission("statistic")
//    @RequestMapping("/user_chart_data")
//    public ResponseModel userChartData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String pageNumber = request.getParameter("pageNumber");
//            String orderBy = request.getParameter("orderBy");
//            String userFunctionType = request.getParameter("userFunctionType");
//            int pageSize = 20;
//
//            if (orderBy == null || orderBy.isEmpty()) {
//                orderBy = "totalMinute";
//            }
//            if (pageNumber != null && !pageNumber.isEmpty()) {
//                pageSize = Integer.parseInt(pageNumber);
//            }
//
//            String repairCondition = " ";
//            String maintainCondition = " ";
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and occur_time >='" + beginDate + "'";
//                maintainCondition += " and occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and occur_time <'" + end + "'";
//                maintainCondition += " and occur_time <'" + end + "'";
//            }
//
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), userFunctionType);
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals(userFunctionType + IDataPermissionForFacility.ALL_FACILITY)) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals(userFunctionType + IDataPermissionForFacility.SELF_FACILITY)) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (!isAllFacility) {
//                    if (isSelfFacility) {
//                        LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                        String facilityIds = "";
//                        if (facilityList != null && !facilityList.isEmpty()) {
//                            for (String key : facilityList.keySet()) {
//                                facilityIds += facilityList.get(key).getId() + ",";
//                            }
//                        }
//                        if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                            facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                            repairCondition += " and wr.facility_id in (" + facilityIds + ") ";
//                            maintainCondition += " and wmt.facility_id in (" + facilityIds + ") ";
//                        } else {
//                            repairCondition += " and r.receive_account = '" + user.getAccount() + "' ";    //没有用户所属位置，则按个人权限查询
//                            maintainCondition += " and mt.receive_account = '" + user.getAccount() + "' ";
//                        }
//                    } else  // 查自己的
//                    {
//                        repairCondition += " and r.receive_account = '" + user.getAccount() + "' ";    //没有用户所属位置，则按个人权限查询
//                        maintainCondition += " and mt.receive_account = '" + user.getAccount() + "' ";
//                    }
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                repairCondition += " and wr.facility_id in (" + facilityId + subFacility + ") ";
//                maintainCondition += " and wmt.facility_id in (" + facilityId + subFacility + ") ";
//            }
//
//            //结果
//            List<UserRepairChartResult> supplierChartData = analysChartService.GetUserAnalysChart(schema_name, repairCondition, maintainCondition, orderBy, pageSize);
//
//            result.setCode(1);
//            result.setContent(supplierChartData);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC);
//            result.setMsg(tmpMsg);//获取数据成功 MSG_DATA_AC_SUC
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_ERR);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return result;
//        }
//    }
}
