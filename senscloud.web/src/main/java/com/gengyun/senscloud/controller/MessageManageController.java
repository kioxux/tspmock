package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MessageManageModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.MessageManageService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 消息管理中心
 */
@Api(tags = "消息管理中心")
@RestController
public class MessageManageController {
    @Resource
    MessageManageService messageManageService;

    @ApiOperation(value = "获取消息管理模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_PLAN_WORK_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchMessageManagePermission", method = RequestMethod.POST)
    public ResponseModel searchMessageManagePermission(MethodParam methodParam) {
        return ResponseModel.ok(messageManageService.getMessageManagePermission(methodParam));
    }

    @ApiOperation(value = "分页查询个人消息", notes = ResponseConstant.RSP_DESC_SEARCH_MESSAGE_PAGE_BY_RECEIVER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "startTime", "endTime"})
    @RequestMapping(value = "/searchMessagePageByReceiver", method = RequestMethod.POST)
    public ResponseModel searchMessagePageByReceiver(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                                     @SuppressWarnings("unused") MessageManageModel messageManageModel) {
        return ResponseModel.ok(messageManageService.findMessagePageByReceiver(methodParam, paramMap));
    }

    @ApiOperation(value = "阅读消息，把消息改为已读", notes = ResponseConstant.RSP_DESC_EDIT_MESSAGE_RECEIVE_READ)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "message_id"})
    @Transactional
    @RequestMapping(value = "/editMessageReceiverRead", method = RequestMethod.POST)
    public ResponseModel editMessageReceiverRead(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                                 @SuppressWarnings("unused") MessageManageModel messageManageModel) {
        messageManageService.modifyMessageReceiverRead(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询接收人的未读消息数量", notes = ResponseConstant.RSP_DESC_SEARCH_NO_READ_COUNT_BY_RECEIVER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchNoReadCountByReceiver", method = RequestMethod.GET)
    public ResponseModel searchNoReadCountByReceiver(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                                     @SuppressWarnings("unused") MessageManageModel messageManageModel) {
        return ResponseModel.ok(messageManageService.getNoReadCountByReceiver(methodParam, paramMap));
    }

    @ApiOperation(value = "查询接收人的未读消息最新3条", notes = ResponseConstant.RSP_DESC_SEARCH_NO_READ_THREE_COUNT_BY_RECEIVER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"GET"})
    @RequestMapping(value = "/searchNoReadThreeCountByReceiver", method = RequestMethod.POST)
    public ResponseModel searchNoReadThreeCountByReceiver(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                                          @SuppressWarnings("unused") MessageManageModel messageManageModel) {
        return ResponseModel.ok(messageManageService.getNoReadThreeCountByReceiver(methodParam, paramMap));
    }

    @ApiOperation(value = "根据消息id获取消息详情", notes = ResponseConstant.RSP_DESC_SEARCH_MESSAGE_INFO_BY_ID)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "message_id"})
    @RequestMapping(value = "/searchMessageInfoById", method = RequestMethod.GET)
    public ResponseModel searchMessageInfoById(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                               @SuppressWarnings("unused") MessageManageModel messageManageModel) {
        return ResponseModel.ok(messageManageService.getMessageInfoById(methodParam, paramMap));
    }
}
