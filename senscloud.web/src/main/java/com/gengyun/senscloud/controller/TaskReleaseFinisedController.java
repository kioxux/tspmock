package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.apache.ibatis.annotations.Param;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @Author: Wudang Dong
// * @Description:
// * @Date: Create in 下午 2:33 2019/9/17 0017
// */
//@RestController
//@RequestMapping("/task_release_finised")
public class TaskReleaseFinisedController {

//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    TaskReleaseFinishedService taskReleaseFinishedService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @MenuPermission("task_release_finised")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("task_release_finised_all_facility", "allGroup");
//            put("task_release_finised_self_facility", "selfGroup");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "task_release/task_release_finised/index", "task_release_finised");
//    }
//
//    @RequestMapping("/find-task-release-finished-list")
//    public String queryArrangementList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                       @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                       @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                       @RequestParam(name = "sortName", required = false) String sortName,
//                                       @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                       @Param("groups") String groups,
//                                       HttpServletRequest request) {
//        try {
//            User loginUser = AuthService.getLoginUser(request);
//            String searchKeyTrim = searchKey.trim();
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = taskReleaseFinishedService.query(schemaName, searchKeyTrim, pageSize, pageNumber, sortName, sortOrder,groups,loginUser);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * @Description: 查询任务详情信息
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_release_finised")
//    @RequestMapping("/findSingleWtInfo")
//    public ResponseModel findSingleAtInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = taskReleaseFinishedService.findSingleWtInfo(schemaName, subWorkCode);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * @Description:任务详情信息
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_release_finised")
//    @RequestMapping("/detail-workTask")
//    public ModelAndView detailWorkTask(HttpServletRequest request) {
//        try {
//            return taskReleaseFinishedService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * @Description:任务提交
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_release_finised")
//    @RequestMapping("/task_submit")
//    public ResponseModel doSubmitTaskRelease(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return taskReleaseFinishedService.save(schema_name, processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_41));
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * @Description: 任务评价
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_release_finised")
//    @RequestMapping("/task_evaluate")
//    public ResponseModel doEvaluateTaskRelease(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return taskReleaseFinishedService.doEvaluateTaskRelease(schema_name, processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_41));
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
}
