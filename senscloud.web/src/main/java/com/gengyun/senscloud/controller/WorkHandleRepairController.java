package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Map;
//
///**
// * 功能：维修-新工单controller
// * 作者：Dong wudang
// * 时间：2018-12-14
// */
//@RestController
//@RequestMapping("/repairWork")
public class WorkHandleRepairController {
//    private static final Logger logger = LoggerFactory.getLogger(WorkHandleRepairController.class);
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    //维修确认操作（最终审核）
//    @RequestMapping("/confirm-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel confirmRepairProblem(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.confirmRepairProblem(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));
//        }
//    }
//
//    //维修确认操作（1级审核）
//    @RequestMapping("/confirm-first-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel confirmFirstRepairProblem(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.confirmFirstRepairProblem(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));
//        }
//    }
//
//    //维修执行事件（处理提交）
//    @RequestMapping("/doFinishedRepair")
//    @Transactional //支持事务
//    public ResponseModel doRepairProblem(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            paramMap.put("pc_submit_right", "pc_submit_right");
//            return workSheetHandleBusinessService.doRepairProblem(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));
//        }
//    }
//
//    /**
//     * 子页面数据保存
//     *
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("/doSaveSubInfo")
//    @Transactional //支持事务
//    public ResponseModel doSaveSubPage(@RequestParam Map<String, Object> paramMap) {
//        try {
//            return workSheetHandleBusinessService.doSaveSubPage(paramMap);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));//失败
//        }
//    }
//
//    //接单
//    @RequestMapping("/receive-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel receiveRepairProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.receiveRepairProblemInfo(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));
//        }
//    }
//
//    // 问题确认（重新提交、关闭）
//    @RequestMapping("/confirmProblemInfo")
//    @Transactional //支持事务
//    public ResponseModel confirmProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.confirmProblemInfo(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERROR));
//        }
//    }
//
//    // 维修班长确认（提交）
//    @RequestMapping("/confirmInfo")
//    @Transactional //支持事务
//    public ResponseModel confirmInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            paramMap.put("baseStatus", StatusConstant.TO_BE_CONFIRM);
//            return workSheetHandleBusinessService.reconfirmRepairProblemInfo(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERROR));
//        }
//    }
//
//    // 店长确认（重新提交、关闭）
//    @RequestMapping("/reconfirm-info-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel reconfirmRepairProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            paramMap.put("baseStatus", StatusConstant.DRAFT);
//            return workSheetHandleBusinessService.reconfirmRepairProblemInfo(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_ERROR));
//        }
//    }
//
//    //维修问题确认（主管转派、接单、退回）
//    @RequestMapping("/confirm-info-repair-work-flowable")
//    @Transactional //支持事务
//    public ResponseModel confirmRepairProblemInfo(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.confirmRepairProblemInfo(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E));
//        }
//    }
//
//    // 问题上报
//    @RequestMapping("/doSubmitRepair")
//    @Transactional //支持事务
//    public ResponseModel saveRepairProblem(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.save(schema_name, processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
}
