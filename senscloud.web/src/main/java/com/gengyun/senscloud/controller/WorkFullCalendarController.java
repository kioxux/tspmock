package com.gengyun.senscloud.controller;
//
//import com.alibaba.fastjson.JSONArray;
//import com.fitit100.util.RegexUtil;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.JsonObjectResult;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SqlConditionUtil;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * Created by Administrator on 2018/4/27.
// */
//@RestController
//@RequestMapping("/WorkFullCalenda")
public class WorkFullCalendarController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    WorkScheduleService workScheduleService;
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    WorkListService workListService;
//    @Autowired
//    WorkSheetPoolConfigService workSheetPoolConfigService;
//    @Autowired
//    UserRoleService userRoleService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @RequestMapping({"/", "/index"})
//    public ModelAndView selectStockData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
////        Boolean isAllFacility = false;
//        List<WorkSheetPoolConfigModel> workSheetPoolConfigModelsList=workSheetPoolConfigService.selectWorkSheetPool(schema_name);
////        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
////        if (userFunctionList != null || !userFunctionList.isEmpty()) {
////            for (UserFunctionData functionData : userFunctionList) {
//////                //如果可以查看全部分拨，则不限制
//////                if (functionData.getFunctionName().equals("workplan_all_facility")) {
//////                    isAllFacility = true;
//////                    break;
//////                }
////                //如果可以查看全部分拨，则不限制
////                if (functionData.getFunctionName().equals("worksheet_all_facility")) {
////                    isAllFacility = true;
////                    break;
////                }
////            }
////        }
////        //按用户权限，获取用户所能看见的场地
////        List<Facility> facilitiesList = facilitiesService.GetFacilityListByUser(schema_name, isAllFacility, user);
////        TreeBuilder treeBuilder = new TreeBuilder();
////        List<Node> allNodes = new ArrayList<Node>();
////        for (Facility item : facilitiesList) {
////            Node node = new Node();
////            node.setId(String.valueOf(item.getId()));
////            node.setParentId(String.valueOf(item.getParentId()));
////            node.setName(item.getTitle());
////            node.setDetail(item);
////            allNodes.add(node);
////        }
////        List<Node> roots = treeBuilder.buildListToTree(allNodes);
////        String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
////        String facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, user);
//        String  workpool = JSONArray.toJSONString(workSheetPoolConfigModelsList);
//        return new ModelAndView("workfullcalendar/index")
////                .addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("workpool", workpool);
//    }
//
//    //获取当前日历下的所有工单排班计划
//    @RequestMapping("/find-worksheet-list")
//    public ResponseModel queryworksheet(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        int pageSize = 50;
//        int pageNumber = 1;
//        String searchStatus = request.getParameter("status");
//        String facilities = request.getParameter("facilities");
//        String keyWord = request.getParameter("keyWord");
//        String beginTime = request.getParameter("start");
//        String endTime = request.getParameter("end");
//        String work_type_id = request.getParameter("type");
//        List<WorkSheet> workDataList = null;
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition = "";
//            String account = user.getAccount();
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "worksheet");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部分拨，则不限制
//                    if (functionData.getFunctionName().equals("worksheet_all_facility")) {
//                        isAllFacility = true;
//                    }
//                    else if (functionData.getFunctionName().equals("worksheet_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            condition += SqlConditionUtil.getWorkFacilityCondition(isAllFacility, isSelfFacility, condition, facilities, account, user.getFacilities());
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(w.work_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%" + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or w2.receive_account like '%" + keyWord + "%' )";
//            }
//            if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("-1")) {
//                condition += " and (w.status ='" + searchStatus + "' ) ";
//            }
//            if (work_type_id != null && !searchStatus.isEmpty() && !work_type_id.equals("-1")) {
//                condition += " and (w.work_type_id = " + work_type_id + " ) ";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and w.occur_time <'" + end + "'";
//            }
//            workDataList = workSheetService.getWorkSheetListforcalendar(schema_name, condition);
//            result.setContent(workDataList);
//            result.setCode(1);
//            result.setMsg("ok");
//            return result;
//        } catch (Exception e) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_FA));
//            return result;
//        }
//    }
//
////    @RequestMapping({"/get_workInfo_by_id"})
////    public ResponseModel getWorkScheduleListByFacilityById(HttpServletRequest request, HttpServletResponse response) {
////        ResponseModel result = new ResponseModel();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            int id = Integer.valueOf(request.getParameter("id"));
////
////
////            WorkScheduleData workScheduleDataList = workScheduleService.getWorkScheduleListByFacilityById(schema_name, id);
////            result.setContent(workScheduleDataList);
////            result.setCode(1);
////            result.setMsg("成功");
////            return result;
////        } catch (Exception e) {
////            result.setCode(-1);
////            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_FA));
////            return result;
////        }
////    }
//
////    @RequestMapping({"/update_work"})
////    public JsonResult updateBom(HttpServletRequest request, HttpServletResponse response) {
////        String schema_name = authService.getCompany(request).getSchema_name();
////        try {
////            String id = request.getParameter("id");
////            String user_account = request.getParameter("user_account");
////            if (RegexUtil.isNull(user_account)) {
////                return new JsonResult().error("员工姓名不能为空");
////            }
////            String facility_id = request.getParameter("title2");
////            if (RegexUtil.isNull(facility_id)) {
////                return new JsonResult().error("库房名称不能为空");
////            }
////            String begin_time = request.getParameter("begin_time");
////            if (RegexUtil.isNull(begin_time)) {
////                return new JsonResult().error("工作日期不能为空");
////            }
////            String remark = request.getParameter("remark");
////            String begin_time4 = request.getParameter("begin_time4");
////            String end_time4 = request.getParameter("end_time4");
////            WorkScheduleData workScheduleData = new WorkScheduleData();
////            workScheduleData.setId(Integer.valueOf(id));
////            workScheduleData.setUser_account(user_account);
////            String newBegintime = begin_time + " " + begin_time4 + ":00";
////            String newEndtime = begin_time + " " + end_time4 + ":00";
////            workScheduleData.setGroup_id(Integer.valueOf(facility_id));
////            workScheduleData.setBegin_time(Timestamp.valueOf(newBegintime));
////            workScheduleData.setEnd_time(Timestamp.valueOf(newEndtime));
////            if (remark!=null) {
////                workScheduleData.setRemark(remark);
////            }
////            int a = workScheduleService.updateWorlSchedule(schema_name, workScheduleData);
////            return new JsonResult().success("成功更新");
////
////        } catch (Exception ex) {
////            return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_ERROR));
////        }
////    }
////
////    @RequestMapping({"/updateDate"})
////    public JsonResult updateDate(HttpServletRequest request, HttpServletResponse response) {
////        String schema_name = authService.getCompany(request).getSchema_name();
////        try {
////            String dayDelta = request.getParameter("dayDelta");
////            int day = Integer.valueOf(dayDelta);
////            String id = request.getParameter("id");
////            WorkListDetailModel work=workSheetService.finddetilByID(schema_name,id);
////            Date receive_time = work.getReceive_time();
////            Calendar calendar = new GregorianCalendar();
////            calendar.setTime(receive_time);
////            calendar.add(calendar.DATE, day);
////            // calendar的time转成java.util.Date格式日期
////            Date utilDate = (Date) calendar.getTime();
////            //java.util.Date日期转换成转成java.sql.Date格式
////            String receive_time2 = new SimpleDateFormat(Constants.DATE_FMT_SS).format(utilDate);
////            Timestamp receive_time3 = Timestamp.valueOf(receive_time2);
////            work.setReceive_time(receive_time3);
////            int a = workSheetService.updateWorkDetailList(schema_name,work);
////            if(a>0) {
////                return new JsonResult().success("成功更新");
////            }else{
////                return new JsonResult().success("失敗");
////            }
////        } catch (Exception ex) {
////            return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_ERROR));
////        }
////    }
////
////    @RequestMapping({"/delete_work"})
////    public JsonResult deleteWork(HttpServletRequest request, HttpServletResponse response) {
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            String work_id = request.getParameter("id");
////            workScheduleService.deleteWorkSchedule(schema_name, Integer.valueOf(work_id));
////            return new JsonResult().success("成功删除");
////        } catch (Exception ex) {
////            return new JsonResult().error("删除失败");
////        }
////    }
////
////    @RequestMapping({"/add_work"})
////    public JsonResult addWork(HttpServletRequest request, HttpServletResponse response) {
////        User user = authService.getLoginUser(request);
////        String schema_name = authService.getCompany(request).getSchema_name();
////        WorkScheduleData workScheduleData = new WorkScheduleData();
////
////        try {
////            String user_account = request.getParameter("username");
////            if (RegexUtil.isNull(user_account)) {
////                return new JsonResult().error("员工姓名不能为空");
////            }
////            String facility_id = request.getParameter("title");
////            if (RegexUtil.isNull(facility_id)) {
////                return new JsonResult().error("库房名称不能为空");
////            }
////            String begin_time = request.getParameter("begin_time2");
////            if (RegexUtil.isNull(begin_time)) {
////                return new JsonResult().error("开始时间不能为空");
////            }
////            String end_tiem = request.getParameter("end_time2");
////            if (RegexUtil.isNull(end_tiem)) {
////                return new JsonResult().error("结束时间不能为空");
////            }
////            String remark = request.getParameter("remark2");
////
////            String begin_time3 = request.getParameter("begin_time3");
////
////            String end_time3 = request.getParameter("end_time3");
////            String create_user_account = user.getAccount();
////            DateFormat df = new SimpleDateFormat(Constants.DATE_FMT);;
////            Date begintime = df.parse(begin_time);//将string转换成固定date
////            Date endtime = df.parse(end_tiem);
////            Calendar aCalendar = Calendar.getInstance();
////            Calendar aCalendar2 = Calendar.getInstance();
////            aCalendar.setTime(begintime);
////            int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);
////            aCalendar2.setTime(endtime);
////            int day2 = aCalendar2.get(Calendar.DAY_OF_YEAR);
////            int days = day2 - day1;
////            for (int i = 0; i <= days; i++) {
////                if (i == 0) {
////                    aCalendar.add(aCalendar.DATE, i);
////                    int b = days;
////                    aCalendar2.add(aCalendar2.DATE, -b);
////                } else {
////                    aCalendar.add(aCalendar.DATE, 1);
////                    aCalendar2.add(aCalendar2.DATE, 1);
////                }
////                Date begin = df.parse(df.format(aCalendar.getTime()));
////                Date end = df.parse(df.format(aCalendar2.getTime()));
////                begin_time = df.format(begin);
////                end_tiem = df.format(end);
////                String newBegintime = begin_time + " " + begin_time3 + ":00";
////                String newEndtime = end_tiem + " " + end_time3 + ":00";
////                workScheduleData.setUser_account(user_account);
////                workScheduleData.setFacility_id(Integer.valueOf(facility_id));
////                workScheduleData.setBegin_time(Timestamp.valueOf(newBegintime));
////                workScheduleData.setEnd_time(Timestamp.valueOf(newEndtime));
////                workScheduleData.setCreate_user_account(create_user_account);
////                if (remark!=null) {
////                    workScheduleData.setRemark(remark);
////                }
////                //当前创建人
////                workScheduleData.setCreate_time(new Timestamp(new Date().getTime()));
////                workScheduleService.insertWorkSchedule(schema_name, workScheduleData);
////            }
////            return new JsonResult().success("成功添加");
////        } catch (Exception ex) {
////            return new JsonResult().error("添加失败");
////        }
////    }
//
////    @RequestMapping({"/find_user"})
////    public ResponseModel findfac(HttpServletRequest request, HttpServletResponse response) {
////        String schema_name = authService.getCompany(request).getSchema_name();
////        String facilityID = request.getParameter("facid");
////        List<User> userList = userService.getUser(schema_name, Integer.valueOf(facilityID));
////        ResponseModel result = new ResponseModel();
////
////        result.setContent(userList);
////        result.setCode(1);
////        result.setMsg("成功。");
////
////        return result;
////    }
}