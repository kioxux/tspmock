package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.LoginModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.UserInfoEditService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.login.LoginService;
import com.gengyun.senscloud.service.system.WxMsgService;
import com.gengyun.senscloud.util.RegexUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Api(tags = "登录")
@RestController
public class LoginController {
    @Resource
    LoginService loginService;
    @Resource
    UserInfoEditService userInfoEditService;
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    UserService userService;
    @Resource
    WxMsgService wxMsgService;

    @ApiOperation(value = "获取登录页信息（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_LOGIN_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, ignoreParameters = {"token", "pageNumber", "pageSize", "batchDeal", "needPagination", "openSms"})
    @RequestMapping(value = "/searchLoginData", method = RequestMethod.GET)
    public ResponseModel queryLoginData(MethodParam methodParam) {
        return ResponseModel.ok(loginService.getLoginData(methodParam));
    }

    @ApiOperation(value = "登录验证（含手机端）", notes = ResponseConstant.RSP_DESC_CHECK_LOGIN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"u", "p"})
    @RequestMapping(value = "/doGoSns", method = RequestMethod.POST)
    public ResponseModel checkLogin(MethodParam methodParam, LoginModel loginModel) {
        return ResponseModel.ok(loginService.checkLogin(methodParam, loginModel));
    }

    @ApiOperation(value = "所属企业选择（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_COMPANY_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/checkCompanies", method = RequestMethod.POST)
    public ResponseModel getCompanyList(MethodParam methodParam) {
        return ResponseModel.ok(loginService.getCompanyList(methodParam));
    }

    @ApiOperation(value = "用户登录（含手机端）", notes = ResponseConstant.RSP_DESC_DO_LOGIN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "c_id"})
    @Transactional // 支持事务
    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public void executeLogin(MethodParam methodParam, LoginModel loginModel) {
        loginService.applyLogin(methodParam, loginModel.getC_id());
    }

    @ApiOperation(value = "用户NFC登录（手机端）", notes = ResponseConstant.RSP_DESC_NFC_LOGIN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "c_id", "nfc_code"})
    @Transactional // 支持事务
    @RequestMapping(value = "/doNFCLogin", method = RequestMethod.POST)
    public ResponseModel executeNFCLogin(MethodParam methodParam, LoginModel loginModel) {
        return ResponseModel.ok(loginService.applyNFCLogin(methodParam, loginModel.getC_id(), loginModel.getNfc_code()));
    }

    @ApiOperation(value = "获取菜单信息（含手机端）", notes = ResponseConstant.RSP_DESC_GET_MENU_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getMenuList", method = RequestMethod.POST)
    public ResponseModel getMenuList(MethodParam methodParam) {
        String clientName = RegexUtil.optStrOrBlank(methodParam.getClientName());
        if ("WECHAT".equals(clientName) || "APP".equals(clientName)) {
            Map<String, Object> menuInfo = new HashMap<>();
            menuInfo.put("menuNameList", methodParam.getUserPermissionList());
            menuInfo.put("menuList", loginService.getMenuList(methodParam));
            return ResponseModel.ok(menuInfo);
        }
        return ResponseModel.ok(loginService.getMenuList(methodParam));
    }

    @ApiOperation(value = "获取默认语言包（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_DEFAULT_LANG_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getDefaultLangInfo", method = RequestMethod.POST)
    public ResponseModel searchDefaultLangInfo(MethodParam methodParam) {
        return ResponseModel.ok(loginService.getDefaultLangInfo(methodParam));
    }

    @ApiOperation(value = "切换语言（含手机端）", notes = ResponseConstant.RSP_DESC_EDIT_USER_LANG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "lang"})
    @Transactional // 支持事务
    @RequestMapping(value = "/changeLangInfo", method = RequestMethod.POST)
    public void editUserLang(MethodParam methodParam, LoginModel loginModel) {
        userInfoEditService.modifyUserConfig(methodParam, loginModel.getLang());
    }

    @ApiOperation(value = "获取登录用户信息（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_LOGIN_USER_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getLoginUserInfo", method = RequestMethod.POST)
    public ResponseModel searchLoginUserInfo(MethodParam methodParam) {
        return ResponseModel.ok(loginService.getLoginUserInfo(methodParam));
    }

    @ApiOperation(value = "退出登录", notes = ResponseConstant.RSP_DESC_DO_LOGOUT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @Transactional // 支持事务
    @RequestMapping(value = "/leaveCloud", method = RequestMethod.POST)
    public void doLogOut(MethodParam methodParam) {
        cacheUtilService.applyLogOut(methodParam);
    }

    @ApiOperation(value = "根据手机号发送验证码（含手机端）", notes = ResponseConstant.RSP_DESC_SEND_VERIFICATION_CODE_BY_PHONE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"phone"})
    @RequestMapping(value = "/sendVerificationCodeByPhone", method = RequestMethod.POST)
    public ResponseModel sendVerificationCodeByPhone(LoginModel loginModel) {
        loginService.sendVfCodeByPhone(loginModel.getPhone());
        return ResponseModel.okMsg(LangConstant.MSG_W);
    }

    @ApiOperation(value = "验证短信验证码并返回企业列表（含手机端）", notes = ResponseConstant.RSP_DESC_CHECK_VERIFICATION_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"phone", "vfCode"})
    @RequestMapping(value = "/checkVerificationCode", method = RequestMethod.POST)
    public ResponseModel checkVerificationCode(LoginModel loginModel) {
        return ResponseModel.ok(loginService.getCompanyListByPhone(loginModel));
    }

    @ApiOperation(value = "根据手机验证码修改密码（含手机端）", notes = ResponseConstant.RSP_DESC_EDIT_PSD_BY_PHONE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"phone", "vfCode", "c_id", "newPsd", "cfmPsw"})
    @RequestMapping(value = "/editPsdByPhone", method = RequestMethod.POST)
    public ResponseModel editPsdByPhone(LoginModel loginModel) {
        loginService.modifyPsdByPhone(loginModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "发送验证码（含手机端）", notes = ResponseConstant.RSP_DESC_SEND_VERIFICATION_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/sendVerificationCode", method = RequestMethod.POST)
    public ResponseModel sendVerificationCode(MethodParam methodParam) {
        loginService.sendVfCode(methodParam);
        return ResponseModel.okMsg(LangConstant.MSG_W);
    }

    @ApiOperation(value = "修改密码（含手机端）", notes = ResponseConstant.RSP_DESC_EDIT_PASSWORD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "vfCode", "newPsd", "cfmPsw"})
    @RequestMapping(value = "/editPassword", method = RequestMethod.POST)
    public ResponseModel editPsd(MethodParam methodParam, LoginModel loginModel) {
        loginService.modifyPwd(methodParam, loginModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "通过新旧密码修改密码（含手机端）", notes = ResponseConstant.RSP_DESC_EDIT_PASSWORD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "newPsd", "cfmPsw", "p"})
    @RequestMapping(value = "/editPasswordWithoutVfCode", method = RequestMethod.POST)
    public ResponseModel editPsdWithoutVfCode(MethodParam methodParam, LoginModel loginModel) {
        loginService.modifyPwdWithoutVfCode(methodParam, loginModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取用户公众号关注状态（仅小程序）", notes = ResponseConstant.RSP_DESC_FOLLOW_OA_STATUS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getFollowOAStatus", method = RequestMethod.GET)
    public ResponseModel getFollowOAStatus(MethodParam methodParam) {
        return ResponseModel.ok(userService.getFollowOAStatus(methodParam.getSchemaName(), methodParam.getAccount()));
    }

    @ApiOperation(value = "小程序绑定用户账号", notes = ResponseConstant.RSP_DESC_BIND_APPLET)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "nickName", "openId", "unionId"})
    @RequestMapping(value = "/bindApplet", method = RequestMethod.POST)
    public ResponseModel bindApplet(MethodParam methodParam,
                                    @ApiParam(value = "昵称") @RequestParam(required = false) String nickName,
                                    @ApiParam(value = "openId") @RequestParam(required = false) String openId,
                                    @ApiParam(value = "unionId") @RequestParam(required = false) String unionId) {
        wxMsgService.updateUserWxOpenId(methodParam.getSchemaName(), methodParam.getUserId(), nickName, openId, unionId);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "小程序解绑用户账号", notes = ResponseConstant.RSP_DESC_UNBIND_APPLET)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "openId"})
    @RequestMapping(value = "/unbindApplet", method = RequestMethod.POST)
    public ResponseModel unbindApplet(MethodParam methodParam,
                                      @ApiParam(value = "openId") @RequestParam(required = false) String openId) {
        wxMsgService.removeUserWxOpenId(methodParam.getSchemaName(), methodParam.getUserId(), openId);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取微信信息", notes = ResponseConstant.RSP_DESC_UNBIND_APPLET)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "openId"})
    @RequestMapping(value = "/getWeiXinInfo", method = RequestMethod.POST)
    public ResponseModel getWeiXinInfo(MethodParam methodParam, LoginModel loginModel) {
        return ResponseModel.ok(loginService.getWeiXinInfo(methodParam, loginModel));
    }

    @ApiOperation(value = "获取所有缓存列表信息", notes = ResponseConstant.RSP_DESC_ALL_CACHE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getAllCache", method = RequestMethod.POST)
    public ResponseModel getAllCache(MethodParam methodParam) {
        return ResponseModel.ok(loginService.getAllCache(methodParam));
    }

    @ApiOperation(value = "一键清除所有缓存", notes = ResponseConstant.RSP_DESC_CLEAR_CACHE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "type"})
    @RequestMapping(value = "/clearAllCache", method = RequestMethod.POST)
    public ResponseModel clearAllCache(MethodParam methodParam, @RequestParam Map<String, Object> param) {
        loginService.clearAllCache(methodParam, param);
        return ResponseModel.okMsgForOperate();
    }
}
