package com.gengyun.senscloud.controller;


import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.CustomerModel;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.CustomersService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "客户管理")
@RestController
public class CustomerController {

    @Resource
    CustomersService customersService;
    @Resource
    LogsService logsService;

    @ApiOperation(value = "获取客户模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMERS_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getCustomerListPermission", method = RequestMethod.POST)
    public ResponseModel searchCustomerListPermission(MethodParam methodParam) {
        return ResponseModel.ok(customersService.getCustomerListPermission(methodParam));
    }

    @ApiOperation(value = "查询客户列表", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMERS_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "isUseSearch", "orgTypeSearch", "importantLevel", "customerType", "unloadingGroup", "isPaid"})
    @RequestMapping(value = "/searchCustomerList", method = RequestMethod.POST)
    public ResponseModel searchCustomerList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomersList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增客户", notes = ResponseConstant.RSP_DESC_ADD_CUSTOMERS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "title", "address", "parent_id", "remark", "location",
                    "inner_code", "layer_path", "org_type_id", "short_title", "currency_id",
                    "unloading_group_id", "position_code", "pipeline_code", "sewage_destination",
                    "main_products", "province", "city", "zone", "important_level_id", "customer_type_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addCustomer", method = RequestMethod.POST)
    public ResponseModel addCustomer(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.newCustomer(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询客户详情", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMERS_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchCustomerInfo", method = RequestMethod.POST)
    public ResponseModel searchCustomerInfo(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        return ResponseModel.ok(customersService.findById(methodParam, facilitiesModel));
    }

    @ApiOperation(value = "更新客户", notes = ResponseConstant.RSP_DESC_EDIT_CUSTOMERS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "title", "id", "address", "parent_id", "remark", "location",
                    "inner_code", "layer_path", "org_type_id", "short_title", "currency_id",
                    "unloading_group_id", "position_code", "pipeline_code", "sewage_destination",
                    "main_products", "province", "city", "zone", "important_level_id", "customer_type_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/editCustomers", method = RequestMethod.POST)
    public ResponseModel editCustomers(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.modifyCustomers(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询关联客户列表", notes = ResponseConstant.RSP_DESC_SEARCH_RELATION_CUSTOMERS_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRelationCustomerList", method = RequestMethod.POST)
    public ResponseModel searchRelationCustomerList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findRelationCustomersList(methodParam, paramMap));
    }

    @ApiOperation(value = "查询设备列表", notes = ResponseConstant.RSP_DESC_SEARCH_RELATION_ASSET_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRelationAssetList", method = RequestMethod.POST)
    public ResponseModel searchRelationAssetList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findRelationAssetList(methodParam, paramMap));
    }

    @ApiOperation(value = "关联设备编辑", notes = ResponseConstant.RSP_DESC_EDIT_RELATION_ASSET)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/editRelationAssetList", method = RequestMethod.POST)
    public ResponseModel editRelationAssetList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        customersService.editRelationAssetList(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    //污染因子
    @ApiOperation(value = "客户污染因子列表", notes = ResponseConstant.RSP_DESC_SEARCH_POLLUTE_FACTOR_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchPolluteFactorList", method = RequestMethod.POST)
    public ResponseModel searchPolluteFactorList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findPolluteFactorList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增污染因子", notes = ResponseConstant.RSP_DESC_ADD_POLLUTE_FACTOR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "facility_id", "factor_id", "limit_value",
                    "limit_unit", "begin_date", "end_date"})
    @Transactional //支持事务
    @RequestMapping(value = "/addPolluteFactor", method = RequestMethod.POST)
    public ResponseModel addPolluteFactor(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.newPolluteFactor(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新污染因子", notes = ResponseConstant.RSP_DESC_EDIT_POLLUTE_FACTOR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "facility_id", "factor_id", "limit_value",
                    "limit_unit", "begin_date", "end_date"})
    @Transactional //支持事务
    @RequestMapping(value = "/editPolluteFactor", method = RequestMethod.POST)
    public ResponseModel editPolluteFactor(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.modifyPolluteFactor(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }


    @ApiOperation(value = "删除污染因子", notes = ResponseConstant.RSP_DESC_REMOVE_POLLUTE_FACTOR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removePolluteFactor", method = RequestMethod.POST)
    public ResponseModel removePolluteFactor(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.deletePolluteFactor(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    //排污时间
    @ApiOperation(value = "客户排污时间列表", notes = ResponseConstant.RSP_DESC_SEARCH_UNLOADING_TIME_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchUnloadingTimeList", method = RequestMethod.POST)
    public ResponseModel searchUnloadingTimeList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findUnloadingTimeList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增排污时间", notes = ResponseConstant.RSP_DESC_ADD_POLLUTE_FACTOR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "facility_id", "begin_time", "end_time",
                    "begin_date", "end_date"})
    @Transactional //支持事务
    @RequestMapping(value = "/addUnloadingTime", method = RequestMethod.POST)
    public ResponseModel addUnloadingTime(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.newUnloadingTime(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新排污时间", notes = ResponseConstant.RSP_DESC_EDIT_UNLOADING_TIME)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "facility_id", "begin_time", "end_time",
                    "begin_date", "end_date"})
    @Transactional //支持事务
    @RequestMapping(value = "/editUnloadingTime", method = RequestMethod.POST)
    public ResponseModel editUnloadingTime(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.modifyUnloadingTime(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除排污时间", notes = ResponseConstant.RSP_DESC_REMOVE_NLOADING_TIME)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "facility_id", "begin_time", "end_time",
                    "begin_date", "end_date"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeUnloadingTime", method = RequestMethod.POST)
    public ResponseModel removeUnloadingTime(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        customersService.deleteUnloadingTime(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }


    //批导
    @ApiOperation(value = "获取选中客户导出", notes = ResponseConstant.RSP_DESC_GET_EXPORT_CUSTOMER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/getExportCustomer", method = RequestMethod.POST)
    public ResponseModel getExportCustomer(MethodParam methodParam, CustomerModel asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        return ResponseModel.okExport(customersService.getExportCustomer(methodParam, asParam));
    }

    @ApiOperation(value = "获取全部客户导出", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_CUSTOMER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "isUseSearch", "orgTypeSearch", "importantLevel", "customerType", "unloadingGroup", "isPaid"})
    @RequestMapping(value = "/getExportAllCustomer", method = RequestMethod.POST)
    public ResponseModel getExportAllCustomer(MethodParam methodParam, CustomerModel asParam) {
        return ResponseModel.okExport(customersService.getExportAllCustomer(methodParam, asParam));
    }

    @ApiOperation(value = "获得客户所有的日志", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getCustomerLogList", method = RequestMethod.POST)
    public ResponseModel getCustomerLogList(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(logsService.getLog(methodParam, SensConstant.BUSINESS_NO_6002, methodParam.getDataId()));
    }

    @ApiOperation(value = "查询客户设备位置地图", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_POSITION_FOR_MAP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRelationAssetPositionForMap", method = RequestMethod.POST)
    public ResponseModel searchRelationAssetPositionForMap(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findRelationAssetPositionForMap(methodParam, paramMap));
    }

    @ApiOperation(value = "查询所有客户位置地图", notes = ResponseConstant.RSP_DESC_SEARCH_ALL_CUSTOMER_POSITION_FOR_MAP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token"})
    @RequestMapping(value = "/searchAllCustomerPositionForMap", method = RequestMethod.POST)
    public ResponseModel searchAllCustomerPositionForMap(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findAllCustomerPositionForMap(methodParam, paramMap));
    }

    @ApiOperation(value = "查询所有客户设备位置地图", notes = ResponseConstant.RSP_DESC_SEARCH_ALL_CUSTOMER_ASSET_POSITION_FOR_MAP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token"})
    @RequestMapping(value = "/searchAllCustomerAssetPositionForMap", method = RequestMethod.POST)
    public ResponseModel searchAllCustomerAssetPositionForMap(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findAllCustomerAssetPositionForMap(methodParam,paramMap));
    }


    @ApiOperation(value = "查询客户地图明细", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_MAP_DETAIL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchCustomerMapDetail", method = RequestMethod.POST)
    public ResponseModel searchCustomerMapDetail(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomerMapDetail(methodParam, paramMap));
    }


    @ApiOperation(value = "查询客户历史排污量", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_CHECK_METER_HISTORY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "begin_time", "end_time"})
    @RequestMapping(value = "/searchCustomerCheckMeterHistory", method = RequestMethod.POST)
    public ResponseModel searchCustomerCheckMeterHistory(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomerCheckMeterHistory(methodParam, paramMap));
    }

    @ApiOperation(value = "查询客户历史工单", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_WORKS_HISTORY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchCustomerWorksHistory", method = RequestMethod.POST)
    public ResponseModel searchCustomerWorksHistory(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomerWorksHistory(methodParam, paramMap));
    }

    @ApiOperation(value = "客户排污量柱状图", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_WORKS_HISTORY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "begin_time", "end_time", "date_type"})
    @RequestMapping(value = "/searchCustomerCheckMeterPic", method = RequestMethod.POST)
    public ResponseModel searchCustomerCheckMeterPic(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomerCheckMeterPic(methodParam, paramMap));
    }

    @ApiOperation(value = "查询客户排污量", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_CHECK_METER_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "begin_time", "end_time"})
    @RequestMapping(value = "/searchCustomerCheckMeterList", method = RequestMethod.POST)
    public ResponseModel searchCustomerCheckMeterList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomerCheckMeterList(methodParam, paramMap));
    }

    @ApiOperation(value = "查询客户缴费列表", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_POLLUTE_FEE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "begin_time", "end_time"})
    @RequestMapping(value = "/searchCustomerPolluteFeeList", method = RequestMethod.POST)
    public ResponseModel searchCustomerPolluteFeeList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CustomerModel facilitiesModel) {
        return ResponseModel.ok(customersService.findCustomerPolluteFeeList(methodParam, paramMap));
    }

    @ApiOperation(value = "删除客户", notes = ResponseConstant.RSP_DESC_REMOVE_FACILITIES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeCustomer", method = RequestMethod.POST)
    public ResponseModel removeCustomer(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        customersService.removeCustomer(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "客户附件新增", notes = ResponseConstant.RSP_DESC_CUSTOMER_FILE_UPDATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "file_id", "file_type_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addCustomerFile", method = RequestMethod.POST)
    public ResponseModel addCustomerFile(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        customersService.doModifyCustomerFile(methodParam, paramMap, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "客户附件删除", notes = ResponseConstant.RSP_DESC_CUSTOMER_FILE_UPDATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "file_id", "file_type_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeCustomerFile", method = RequestMethod.POST)
    public ResponseModel removeCustomerFile(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        customersService.doModifyCustomerFile(methodParam, paramMap, false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取客户附件文档信息", notes = ResponseConstant.RSP_DESC_GET_CUSTOMER_FILE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getCustomerFileList", method = RequestMethod.POST)
    public ResponseModel getCustomerFileList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(customersService.getCustomerFileList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增客户联系人", notes = ResponseConstant.RSP_DESC_ADD_CUSTOMER_CONTACT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "contact_name", "contact_mobile", "contact_email", "wei_xin"})
    @Transactional //支持事务
    @RequestMapping(value = "/addCustomerContact", method = RequestMethod.POST)
    public ResponseModel addCustomerContact(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        customersService.newCustomerContact(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑客户联系人", notes = ResponseConstant.RSP_DESC_EDIT_CUSTOMER_CONTACT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "contact_name", "contact_mobile", "contact_email", "wei_xin"})
    @Transactional //支持事务
    @RequestMapping(value = "/editCustomerContact", method = RequestMethod.POST)
    public ResponseModel editCustomerContact(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        customersService.modifyCustomerContact(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除客户联系人", notes = ResponseConstant.RSP_DESC_REMOVE_CUSTOMER_CONTACT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeCustomerContact", method = RequestMethod.POST)
    public ResponseModel removeCustomerContact(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        customersService.cutCustomerContact(methodParam, facilitiesModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "客户联系人列表", notes = ResponseConstant.RSP_DESC_SEARCH_CUSTOMER_CONTACT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "keywordSearch"})
    @RequestMapping(value = "/searchCustomerContactList", method = RequestMethod.POST)
    public ResponseModel searchCustomerContactList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        return ResponseModel.ok(customersService.getCustomerContactList(methodParam, paramMap));
    }

    @ApiOperation(value = "获取手动同步结果")
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/searchExecSyncResult", method = RequestMethod.POST)
    public ResponseModel searchExecSyncResult(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(customersService.searchExecSyncResult(methodParam, paramMap));
    }

    @ApiOperation(value = "手动执行同步")
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR)
    @RequestMapping(value = "/handExecSync", method = RequestMethod.POST)
    public ResponseModel handExecSync(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        customersService.handExecSync(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "客户互动记录信息", notes = ResponseConstant.RSP_DESC_ADD_CUSTOMER_INTERACTIVE_RECORD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "type_id", "isDo", "id", "record_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/doCustomerRecord", method = RequestMethod.POST)
    public ResponseModel doCustomerRecord(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        customersService.doModifyCustomerRecord(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取客户互动记录信息", notes = ResponseConstant.RSP_DESC_GET_CUSTOMER_INTERACTIVE_RECORD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getCustomerRecord", method = RequestMethod.POST)
    public ResponseModel getCustomerRecordList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(customersService.getCustomerRecordList(methodParam, paramMap));
    }
}
