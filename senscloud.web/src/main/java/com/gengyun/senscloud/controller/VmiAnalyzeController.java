package com.gengyun.senscloud.controller;

//import com.alibaba.fastjson.JSON;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.VmiAssetModel;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.service.VmiAnalyzeService;
//import com.gengyun.senscloud.util.HttpRequestUtils;
//import com.gengyun.senscloud.util.StringUtil;
//import com.gengyun.senscloud.view.VmiAssetListDataExportView;
//import com.gengyun.senscloud.view.VmiReportDataExportView;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * VMI分析模块
// */
//@RestController
//@RequestMapping("/vmi_analyze")
public class VmiAnalyzeController {

//    @Autowired
//    private VmiAnalyzeService vmiAnalyzeService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    UserService userService;
//
//    /*VMI分析页面*/
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/vmi_asset")
//    public ModelAndView vmiAsset(HttpServletRequest request , HttpServletResponse response){
//        return  new ModelAndView("vmi_analyze/vmi_asset");
//    }
//
//    /*VMI报表页面*/
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/vmi_report")
//    public ModelAndView vmiReport(HttpServletRequest request , HttpServletResponse response){
//        return  new ModelAndView("vmi_analyze/vmi_report");
//    }
//
//    /**
//     * 功能：查询VMI设备列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/find-vmi-asset-list")
//    public String findVmiAssetList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        String condition = "";
//        try {
//            String schema_name = AuthService.getCompany(request).getSchema_name();
//            String facilities = request.getParameter("facilities");
//            String keyWord = request.getParameter("keyWord");
//            if (null != keyWord && !keyWord.isEmpty()) {
//                condition += " and ( asset.strname like '%" + keyWord + "%' or upper(asset.strcode) like upper('%" + keyWord + "%') ) ";
//            }
//            condition += splitJointCondition(facilities);
//            User user=AuthService.getLoginUser(request);
//            //全部位置 所在位置权限判断来源于 设备的权限
//            String rootPermission="asset_data_management";
//            boolean isAll = userService.isAllUserFunctionPermission(schema_name, user.getId(), rootPermission);
//            //若没有所有位置的权限且没有筛选的组织根据用户的权限 筛选所能看见得设备
//            if(!isAll&& RegexUtil.isNull(facilities)){
//                condition+="AND f.id in(select f.id from "+schema_name+"._sc_facilities f " +
//                          "LEFT JOIN "+schema_name+"._sc_group_org gr on gr.org_id=f.id " +
//                          "LEFT JOIN "+schema_name+"._sc_user_group up on up.group_id=gr.group_id " +
//                          "WHERE up.user_id='"+user.getId()+"')";
//            }
//            List<VmiAssetModel> vmiAssetList =  vmiAnalyzeService.findVmiAssetList(schema_name, condition, HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            int total =  vmiAnalyzeService.findVmiAssetListNum(schema_name , condition);
//            result.put("rows", vmiAssetList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 功能：VMI报表列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_report")
//    @RequestMapping("/find-vmi-report-list")
//    public String findVmiReportList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        String condition = "";
//        String dateCondition = "";
//        try {
//            String schemaName = AuthService.getCompany(request).getSchema_name();
//            String facilities = request.getParameter("facilities");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            condition += splitJointCondition(facilities);
//            if (beginTime != null && !beginTime.isEmpty() && !"".equals(beginTime)) {
//                dateCondition += " and m.gather_time >= '" + new SimpleDateFormat(Constants.DATE_FMT_DATE_2).format(new SimpleDateFormat(Constants.DATE_FMT);.parse(beginTime)) + " 00:00:00' ";
//            }
//            if (endTime != null && !endTime.isEmpty() && !"".equals(endTime)) {
//                Calendar cl = Calendar.getInstance();
//                cl.setTime(new SimpleDateFormat(Constants.DATE_FMT);.parse(endTime));
//                cl.add(Calendar.DAY_OF_MONTH, 1);
//                dateCondition += " and m.gather_time <'" + new SimpleDateFormat(Constants.DATE_FMT_DATE_2).format(cl.getTime()) + " 00:00:00' ";
//            }
//            User user=AuthService.getLoginUser(request);
//            //全部位置 所在位置权限判断来源于 设备的权限
//            String rootPermission="asset_data_management";
//            boolean isAll = userService.isAllUserFunctionPermission(schemaName, user.getId(), rootPermission);
//            //若没有所有位置的权限且没有筛选的组织根据用户的权限 筛选所能看见得设备
//            if(!isAll&& RegexUtil.isNull(facilities)){
//                condition+="AND f.id in(select f.id from "+schemaName+"._sc_facilities f " +
//                        "LEFT JOIN "+schemaName+"._sc_group_org gr on gr.org_id=f.id " +
//                        "LEFT JOIN "+schemaName+"._sc_user_group up on up.group_id=gr.group_id " +
//                        "WHERE up.user_id='"+user.getId()+"')";
//            }
//            List<VmiAssetModel> vmiAssetList =  vmiAnalyzeService.findVmiReportList(schemaName, condition, dateCondition, HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            int total =  vmiAnalyzeService.findVmiReportListNum(schemaName , condition);
//            result.put("rows", vmiAssetList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/vmi_asset_detail")
//    public ModelAndView findVmiAsset(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schemaName = AuthService.getCompany(request).getSchema_name();
//            Long facilitiesId = Long.valueOf(request.getParameter("facilitiesId"));
//            String assetId = request.getParameter("assetId");
//            if (StringUtil.isEmpty(assetId) || null == facilitiesId)
//                return new ModelAndView("vmi_analyze/vmi_asset_detail").addObject("error", "VMI设备详情获取失败");
//
//            VmiAssetModel vmiAssetModel = vmiAnalyzeService.findVmiAssetByFacilitiesIdAndAssetId(schemaName, facilitiesId, assetId);
//            if(vmiAssetModel == null)
//                return new ModelAndView("vmi_analyze/vmi_asset_detail").addObject("error", "VMI设备详情获取失败");
//
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("vmi_logistics_add", "vlAddFlag");//物流录入
//                put("vmi_logistics_send", "sendAddFlag");//物流返货
//                put("vmi_logistics_receive", "receiveAddFlag");//物流签收
//            }};
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "vmi_analyze/vmi_asset_detail", "vmi_asset");
//            return model.addObject("vmiAssetDetailData", JSON.toJSON(vmiAssetModel));
//        } catch (Exception e) {
//            return new ModelAndView("vmi_analyze/vmi_asset_detail").addObject("error", e.getMessage());
//        }
//    }
//
//    private String splitJointCondition(String facilities){
//        String condition = "";
//        if (null != facilities && !facilities.isEmpty()) {
//                String[] fs = facilities.split(",");
//                StringBuffer fsb = new StringBuffer();
//                for (String value : fs) {
//                    fsb.append(" or f.facility_no like '" + value+"%'");
//                }
//                String nfs = fsb.toString();
//                if (!nfs.isEmpty() && !"".equals(nfs)) {
//                    nfs = nfs.substring(3);
//                    condition += " and ( " + nfs + ") ";
//                }
//        }
//        return condition;
//    }
//
//    /**
//     * 导出VMI设备列表信息
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/vmi_asset_list_export")
//    public ModelAndView exportVmiAssetList(HttpServletRequest request, HttpServletResponse response) {
//        String schemaName = AuthService.getCompany(request).getSchema_name();
//        try {
//            int pageSize = 50000;//Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = 1;//Integer.parseInt(request.getParameter("pageNumber"));
//            String condition = "";
//
//            String facilities = request.getParameter("facilities");
//            String keyWord = request.getParameter("keyWord");
//            if (null != keyWord && !keyWord.isEmpty()) {
//                condition += " and ( asset.strname like '%" + keyWord + "%' or upper(asset.strcode) like upper('%" + keyWord + "%') ) ";
//            }
//            condition += splitJointCondition(facilities);
//
//            int begin = pageSize * (pageNumber - 1);
//            List<VmiAssetModel> vmiAssetList =  vmiAnalyzeService.findVmiAssetList(schemaName, condition, pageSize, begin);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("vmiAssetList", vmiAssetList);
//            map.put("selectOptionService",selectOptionService);
//            VmiAssetListDataExportView excelView = new VmiAssetListDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 导出VMI报表信息
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_report")
//    @RequestMapping("/vmi_report_export")
//    public ModelAndView exportVmiReport(HttpServletRequest request, HttpServletResponse response) {
//        String schemaName = AuthService.getCompany(request).getSchema_name();
//        try {
//            int pageSize = 50000;//Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = 1;//Integer.parseInt(request.getParameter("pageNumber"));
//            String condition = "";
//            String dateCondition = "";
//            String facilities = request.getParameter("facilities");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            condition += splitJointCondition(facilities);
//            if (beginTime != null && !beginTime.isEmpty() && !"".equals(beginTime)) {
//                dateCondition += " and gather_time >= '" + new SimpleDateFormat(Constants.DATE_FMT_DATE_2).format(new SimpleDateFormat(Constants.DATE_FMT);.parse(beginTime)) + " 00:00:00' ";
//            }
//            if (endTime != null && !endTime.isEmpty() && !"".equals(endTime)) {
//                Calendar cl = Calendar.getInstance();
//                cl.setTime(new SimpleDateFormat(Constants.DATE_FMT);.parse(endTime));
//                cl.add(Calendar.DAY_OF_MONTH, 1);
//                dateCondition += " and gather_time <'" + new SimpleDateFormat(Constants.DATE_FMT_DATE_2).format(cl.getTime()) + " 00:00:00' ";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<VmiAssetModel> vmiAssetList =  vmiAnalyzeService.findVmiReportList(schemaName, condition, dateCondition, pageSize, begin);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("vmiReportList", vmiAssetList);
//            map.put("selectOptionService",selectOptionService);
//            VmiReportDataExportView excelView = new VmiReportDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 功能：设备物流信息列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/getAssetVmiTransferList")
//    public String getAssetVmiTransferList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        try {
//            String schemaName = AuthService.getCompany(request).getSchema_name();
//            String strcode = request.getParameter("strcode");
//            List<Map<String, Object>> vmiAssetList =  vmiAnalyzeService.getAssetVmiTransferList(schemaName, strcode, HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            int total =  vmiAnalyzeService.countAssetVmiTransferList(schemaName, strcode);
//            result.put("rows", vmiAssetList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 功能：设备入库信息列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/getAssetStockInList")
//    public String getAssetStockInList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        try {
//            String schemaName = AuthService.getCompany(request).getSchema_name();
//            String strcode = request.getParameter("strcode");
//            List<Map<String, Object>> vmiAssetList =  vmiAnalyzeService.getAssetStockInList(schemaName, strcode, HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            int total =  vmiAnalyzeService.countAssetStockInList(schemaName, strcode);
//            result.put("rows", vmiAssetList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 功能：设备出库信息列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/getAssetStockOutList")
//    public String getAssetStockOutList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        try {
//            String schemaName = AuthService.getCompany(request).getSchema_name();
//            String strcode = request.getParameter("strcode");
//            List<Map<String, Object>> vmiAssetList =  vmiAnalyzeService.getAssetStockOutList(schemaName, strcode, HttpRequestUtils.getPageSize(), HttpRequestUtils.getPageBegin());
//            int total =  vmiAnalyzeService.countAssetStockOutList(schemaName, strcode);
//            result.put("rows", vmiAssetList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 功能：获取存量曲线数据
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/get_charts_data")
//    public ResponseModel getChartsList(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            List<Map<String,Object>> resultList=vmiAnalyzeService.getVmiList(request);
//            return ResponseModel.ok(resultList);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_SEACH));
//        }
//    }
//
//    /**
//     * vmi物流信息录入
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/doAddVmiLogistics")
//    public ResponseModel doAddVmiLogistics(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            return vmiAnalyzeService.doAddVmiLogistics(request);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));
//        }
//    }
//
//    /**
//     * vmi物流发货
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/doSendVmiLogistics")
//    public ResponseModel doSendVmiLogistics(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            return vmiAnalyzeService.doSendVmiLogistics(request);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));
//        }
//    }
//
//    /**
//     * vmi物流签收
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/doReceiveVmiLogistics")
//    public ResponseModel doReceiveVmiLogistics(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            return vmiAnalyzeService.doReceiveVmiLogistics(request);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_BK));
//        }
//    }
//
//    /**
//     * 查询vmi设备颜色列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("vmi_asset")
//    @RequestMapping("/getVmiAssetColor")
//    public ResponseModel getVmiAssetColor(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            List<Map<String, Object>> assetColorList = vmiAnalyzeService.getVmiAssetColor(request);
//            return ResponseModel.ok(assetColorList);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_SEACH));
//        }
//    }
//
//    /**
//     * 保存vmi设备颜色配置
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("vmi_asset")
//    @RequestMapping("doSaveVmiAssetColor")
//    public ResponseModel doSaveVmiAssetColor(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            return vmiAnalyzeService.doSaveVmiAssetColor(request);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.ERROR_SAVE));//保存失败
//        }
//    }
}
