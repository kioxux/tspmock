package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.system.KnowledgeBaseService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Api(tags = "知识库")
@RestController
public class KnowledgeBaseController {
    @Resource
    KnowledgeBaseService knowledgeBaseService;

    @ApiOperation(value = "获取知识库模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_KNOWLEDGE_BASE_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getKnowledgeBaseListPermission", method = RequestMethod.POST)
    public ResponseModel searchKnowledgeBaseListPermission(MethodParam methodParam) {
        return ResponseModel.ok(knowledgeBaseService.getKnowledgeBaseListPermission(methodParam));
    }

    @ApiOperation(value = "获取知识库列表", notes = ResponseConstant.RSP_DESC_SEARCH_KNOWLEDGE_BASE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "business_no", "keywordSearch"})
    @RequestMapping(value = "/searchKnowledgeBaseList", method = RequestMethod.POST)
    public ResponseModel searchKnowledgeBaseList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(knowledgeBaseService.getKnowledgeBaseListForPage(methodParam, paramMap));
    }

    @ApiOperation(value = "获取知识库明细信息", notes = ResponseConstant.RSP_DESC_GET_KNOWLEDGE_BASE_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getKnowledgeBaseInfo", method = RequestMethod.POST)
    public ResponseModel getKnowledgeBaseInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(knowledgeBaseService.getKnowledgeBaseDetailById(methodParam, paramMap));
    }

    @ApiOperation(value = "获取选中知识库导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_KNOWLEDGE_BASE_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids", "business_no"})
    @RequestMapping(value = "/getExportKnowledgeBaseCode", method = RequestMethod.GET)
    public void getExportKnowledgeBaseCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, HttpServletResponse response) {
        knowledgeBaseService.getExportKnowledgeBaseCode(methodParam, paramMap, response);
    }

    @ApiOperation(value = "获取全部知识库导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_KNOWLEDGE_BASE_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "business_no"})
    @RequestMapping(value = "/getExportAllKnowledgeBaseCode", method = RequestMethod.GET)
    public void getExportAllKnowledgeBase(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, HttpServletResponse response) {
        knowledgeBaseService.getExportKnowledgeBaseCode(methodParam, paramMap, response);
    }
}
