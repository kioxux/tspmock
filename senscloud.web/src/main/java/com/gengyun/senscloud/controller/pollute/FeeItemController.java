package com.gengyun.senscloud.controller.pollute;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.FeeItemModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.pollute.FeeItemService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "费用项管理")
@RestController
public class FeeItemController {

    @Resource
    FeeItemService feeItemService;


    @ApiOperation(value = "查询费用项列表", notes = ResponseConstant.RSP_DESC_FEE_ITEM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "begin_date", "end_date", "is_use","keywordSearch"})
    @RequestMapping(value = "/searchPolluteFeeItemList", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeItemList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FeeItemModel feeItemModel) {
        return ResponseModel.ok(feeItemService.findPolluteFeeItemList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增费用项", notes = ResponseConstant.RSP_DESC_ADD_FEE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "fee_code","fee_name","begin_date","end_date","begin_over_times","end_over_times","price","ratio_cal_type","repeat_price","is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/addPolluteFeeItem", method = RequestMethod.POST)
    public ResponseModel addPolluteFeeItem(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FeeItemModel feeItemModel) {
        feeItemService.newPolluteFeeItem(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "修改费用项", notes = ResponseConstant.RSP_DESC_EDIT_FEE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token","id", "fee_code","fee_name","begin_date","end_date","begin_over_times","end_over_times","price","ratio_cal_type","repeat_price","is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/editPolluteFeeItem", method = RequestMethod.POST)
    public ResponseModel editPolluteFeeItem(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FeeItemModel feeItemModel) {
        feeItemService.modifyPolluteFeeItem(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询费用项详情", notes = ResponseConstant.RSP_DESC_SEARCH_FEE_ITEM_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchPolluteFeeItemInfo", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeItemInfo(MethodParam methodParam, FeeItemModel feeItemModel) {
        return ResponseModel.ok(feeItemService.findById(methodParam, feeItemModel));
    }

    @ApiOperation(value = "删除费用项", notes = ResponseConstant.RSP_DESC_REMOVE_FEE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removePolluteFeeItem", method = RequestMethod.POST)
    public ResponseModel removePolluteFeeItem(MethodParam methodParam, FeeItemModel feeItemModel) {
        feeItemService.deletePolluteFeeItem(methodParam, feeItemModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "启用/禁用费用项", notes = ResponseConstant.RSP_DESC_CHANGE_FEE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id","is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/changeUsePolluteFeeItem", method = RequestMethod.POST)
    public ResponseModel changeUsePolluteFeeItem(MethodParam methodParam, FeeItemModel feeItemModel) {
        feeItemService.modifyUsePolluteFeeItem(methodParam, feeItemModel);
        return ResponseModel.okMsgForOperate();
    }


}
