package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.AssetCategoryModel;
import com.gengyun.senscloud.model.AssetModelModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.asset.AssetModelService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 设备型号模块
 * User: sps
 * Date: 2019/04/15
 * Time: 上午11:20
 */
@Api(tags = "设备型号管理")
@RestController
public class AssetModelController {
    @Resource
    AssetModelService assetModelService;

    @ApiOperation(value = "获取设备型号模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetModelListPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetModelListPermission(MethodParam methodParam) {
        return ResponseModel.ok(assetModelService.getAssetModelListPermission(methodParam));
    }

    @ApiOperation(value = "新增设备型号", notes = ResponseConstant.RSP_DESC_ADD_ASSET_MODEL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "model_name", "category_id", "remark", "properties", "org_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetModel", method = RequestMethod.POST)
    public ResponseModel addAssetModel(MethodParam methodParam, AssetModelModel assetModelModel) {
        assetModelService.newAssetModel(methodParam, assetModelModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑设备型号", notes = ResponseConstant.RSP_DESC_EDIT_ASSET_MODEL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "model_name", "category_id", "remark", "properties", "org_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/editAssetModel", method = RequestMethod.POST)
    public ResponseModel editAssetModel(MethodParam methodParam, AssetModelModel assetModelModel) {
        assetModelService.modifyAssetModel(methodParam, assetModelModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除设备型号", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_MODEL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetModel", method = RequestMethod.POST)
    public ResponseModel removeAssetModel(MethodParam methodParam, AssetModelModel assetModelModel) {
        assetModelService.cutAssetModel(methodParam, assetModelModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询设备型号详情", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_MODEL_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchAssetModelInfo", method = RequestMethod.POST)
    public ResponseModel searchAssetModelInfo(MethodParam methodParam, AssetModelModel assetModelModel) {
        return ResponseModel.ok(assetModelService.getAssetModelInfo(methodParam, assetModelModel));
    }

    @ApiOperation(value = "分页查询设备型号", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_MODEL_LIST_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchAssetModelListPage", method = RequestMethod.POST)
    public ResponseModel searchAssetModelListPage(MethodParam methodParam, AssetModelModel assetModelModel) {
        return ResponseModel.ok(assetModelService.getAssetModelListPage(methodParam, assetModelModel));
    }

    @ApiOperation(value = "分页查询设备型号附件文档", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_MODEL_DOC_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchAssetModelDocPage", method = RequestMethod.POST)
    public ResponseModel searchAssetModelDocPage(MethodParam methodParam, AssetModelModel assetModelModel) {
        return ResponseModel.ok(assetModelService.getAssetModelDocPage(methodParam, assetModelModel));
    }

    @ApiOperation(value = "删除设备型号附件文档", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_MODEL_DOC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "icon"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetModelDoc", method = RequestMethod.POST)
    public ResponseModel removeAssetModelDoc(MethodParam methodParam, AssetModelModel assetModelModel) {
        assetModelService.cutAssetModelDocs(methodParam, assetModelModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "新增设备型号附件文档", notes = ResponseConstant.RSP_DESC_ADD_ASSET_MODEL_DOC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "icon", "file_type", "file_name", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetModelDoc", method = RequestMethod.POST)
    public ResponseModel addAssetModelDoc(MethodParam methodParam, AssetModelModel assetModelModel) {
        assetModelService.newAssetModelDocs(methodParam, assetModelModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "分页查询设备型号故障履历", notes = ResponseConstant.RSP_DESC_SEARCH_FAULT_RECORD_LIST_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchFaultRecordListPage", method = RequestMethod.POST)
    public ResponseModel searchFaultRecordListPage(MethodParam methodParam, AssetModelModel assetModelModel) {
        return ResponseModel.ok(assetModelService.getFaultRecordListPage(methodParam, assetModelModel));
    }

    @ApiOperation(value = "查询设备类型自定义字段", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_MODEL_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "asset_category_id"})
    @RequestMapping(value = "/searchAssetCategoryCustomFields", method = RequestMethod.POST)
    public ResponseModel searchAssetCategoryCustomFields(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        return ResponseModel.ok(assetModelService.getAssetCategoryCustomFields(methodParam, assetCategoryModel));
    }
}
