package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.UserClientService;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 客户用户
// */
//@RestController
//@RequestMapping("/user_client")
public class UserClientController {
//    @Autowired
//    UserClientService userClientService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_client")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("user_client_audit", "userClientAuditFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "user_client/index", "user_client");
//    }
//
//
//    /**
//     * 获取用户列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_client")
//    @RequestMapping("/findUserClientList")
//    public String findUserClientList(HttpServletRequest request) {
//        try {
//            return userClientService.findUserClientList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询用户数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_client")
//    @RequestMapping("/findSingleUserClientInfo")
//    public ResponseModel findSingleUserClientInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = userClientService.queryUserClientById(schemaName, subWorkCode);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_client")
//    @RequestMapping("/detail-userClientList")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return userClientService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 审核
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/doAuditUserClient")
//    @Transactional //支持事务
//    public ResponseModel doAuditUserClient(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return userClientService.doAuditUserClient(schema_name, paramMap, request);
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
}
