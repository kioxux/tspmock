package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.Position;
//import com.gengyun.senscloud.response.FindAllPositionResult;
//import com.gengyun.senscloud.response.FindPositionResult;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.service.MetaDataAssetService;
//import com.gengyun.senscloud.service.PositionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Date;
//import java.util.List;
//
//@RestController
public class PositionController {
//
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    private MetaDataAssetService metaDataAssetService;
//    @Autowired
//    private PositionService positionService;
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @RequestMapping("/position_config")
//    public ModelAndView Config(HttpServletRequest request) {
//
//        ModelMap model = new ModelMap();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//        } catch (Exception ex) {
//
//        }
//        return new ModelAndView("position_config", model);
//    }
//
//    /**
//     * 添加新的监听配置
//     *
//     * @param request
//     * @param position
//     * @return
//     */
//    @RequestMapping("/position_config/add")
//    public JsonResult Add(HttpServletRequest request, Position position) {
//        JsonResult result = new JsonResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            position.setSchema_name(schema_name);
//            position.setCreateTime(new Date());
//            positionService.insertPosition(position);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_POST));
//            return result;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_POST_EXEC));
//            return result;
//        }
//    }
//
//    /**
//     * 删除岗位
//     * @param request
//     * @param position
//     * @return
//     */
//    @RequestMapping("/position_config/update")
//    public JsonResult Update(HttpServletRequest request, Position position) {
//        JsonResult result = new JsonResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            position.setSchema_name(schema_name);
//            positionService.updatePosition(position);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_POST_INFO)+ position.getPositionName());
//            return result;
//        } catch (Exception ex) {
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_POST_INFO_EXEC) + ex.getMessage());
//            return result;
//        }
//    }
//
//    /**
//     * 删除岗位
//     *
//     * @param request
//     * @param id
//     * @return
//     */
//    @RequestMapping("/position_config/delete")
//    public JsonResult Delete(HttpServletRequest request, Long id) {
//        JsonResult result = new JsonResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            positionService.deletePosition(schema_name, id);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_POST)  + id);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_POST_EXEC) + ex.getMessage());
//            return result;
//        }
//    }
//
//    /**
//     * 获得所有的岗位信息列表
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/position_config/list")
//    public ModelAndView list(HttpServletRequest request, HttpServletResponse response) {
//        FindAllPositionResult result = new FindAllPositionResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<Position> dataList = positionService.findAllPosition(schema_name);
//            result.setData(dataList);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg("");
//            return new ModelAndView("position_config_list").addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_POST_LIST_EXEC) + ex.getMessage());
//            return new ModelAndView("position_config_list").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得岗位信息列表
//     *
//     * @param request
//     * @param id
//     * @return
//     */
//    @RequestMapping("/position_config_edit")
//    public ModelAndView get(HttpServletRequest request, Long id) {
//        FindPositionResult result = new FindPositionResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Position position = positionService.findPosition(schema_name, id);
//            result.setData(position);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg("");
//            return new ModelAndView("position_config_edit").addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_POST_EXEC) + ex.getMessage());
//            return new ModelAndView("position_config_edit").addObject("result", result);
//        }
//    }
}
