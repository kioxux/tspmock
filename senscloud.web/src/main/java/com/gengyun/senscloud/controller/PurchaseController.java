package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.FacilitiesResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//
///**
// * 申领采购
// */
//
//@RestController
//@RequestMapping("/purchase")
public class PurchaseController {
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    PurchaseDataService purchaseDataService;
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//
//    @RequestMapping("/index")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String facilitiesSelect = "";
//        String booms = "";
//        try {
//            if (loginUser == null || loginUser.getAccount().isEmpty()) {
//                return new ModelAndView("purchase/index");
//            }
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "purchase");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("buy_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    }
//                }
//            }
//            if (AuthService.isUserHasRoleId(request, UserRole.ROLE_ID_SYSTEM_ADMIN, UserRole.ROLE_ID_COMPANY_ADMIN)) {
//                isAllFacility = true;
//            }
//
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//
//            //加载备件
//            String condition;
//            if (isAllFacility) {
//                condition = "  ";
//            } else {
//                LinkedHashMap<String, IFacility> facilityList = loginUser.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        facilityIds += facilityList.get(key).getId() + ",";
//                    }
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                    condition = " and r.facility_id in (" + facilityIds + ") ";
//                } else {
//                    //没有用户所属位置，则按个人权限查询
//                    condition = " and r.facility_id in (0) ";
//                    ;
//                }
//            }
//            List<BomData> boomlist = purchaseDataService.findAllForPage(schema_name, condition);
//
//            if (boomlist != null && boomlist.size() > 0) {
//                TreeBuilder treeBuilder = new TreeBuilder();
//                List<Node> allNodes = new ArrayList<Node>();
//                for (BomData boomdata : boomlist) {
//                    Node node = new Node();
//                    node.setId(String.valueOf(boomdata.getId()));
//                    // node.setParentId(String.valueOf(facilityList.get(key).getParentId()));
//                    node.setName(boomdata.getBom_name());
//                    // node.setDetail();
//                    allNodes.add(node);
//                }
//                List<Node> roots = treeBuilder.buildListToTree(allNodes);
//                booms = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//            }
//
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        return new ModelAndView("purchase/index")
//                .addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("boomlist", booms);
//    }
//
//
//    @RequestMapping("/find-purchase-list")
//    public String queryPurchase(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        org.json.JSONObject result = new org.json.JSONObject();
//        try {
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String facilities = request.getParameter("facilities");
//            // 创建人
//            User user = authService.getLoginUser(request);
//            String account = user.getAccount();
//            String condition;
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "purchase");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("buy_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else //if (functionData.getFunctionName().equals("purchase_self_facility")) {
//                        isSelfFacility = true;
//                    //}
//                }
//            }
//
//
//            if (facilities != null && facilities != "") {
//
//                condition = " and r.needfacilityid in (" + facilities + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = "  ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = " and r.needfacilityid in (" + facilityIds + ") ";
//                    } else {
//                        //没有用户所属位置，则按个人权限查询
//                        condition = " and (r.create_user_account='" + account + "' or r.Appaccount='" + account + "'   )";
//                    }
//                } else {
//                    condition = " and (r.create_user_account='" + account + "' or r.Appaccount='" + account + "'  )";
//                }
//            }
//
//
//            //if (searchAsset != null && !s  private String needfacilityid;earchAsset.isEmpty()) {
//            //  condition += " and (upper(dv.strname) like upper('%" + searchAsset + "%') or upper(dv.strcode) like upper('%" + searchAsset + "%')) ";
//            // }
//
//
//            int begin = pageSize * (pageNumber - 1);
//            List<PurchaseData> getPurchaseList = purchaseDataService.getPurchaseList(schema_name, condition, pageSize, begin);
//            int total = purchaseDataService.getPurchaseListCount(schema_name, condition);
//
//            result.put("rows", getPurchaseList);
//            result.put("total", total);
//            return result.toString();
//
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping("/add-purchase-boom")
//    public ResponseModel addPurchaseBoom(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            User user = authService.getLoginUser(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//            PurchaseData purchaseData = new PurchaseData();
//            String maptype = request.getParameter("maptype");
//            String booms = "0";
//            String needcount;
//            String beizhu;
//            if (maptype.equals("bj")) {
//                booms = request.getParameter("booms");
//                needcount = request.getParameter("needcount1");
//                beizhu = request.getParameter("beizhu1");
//                if (RegexUtil.isNull(booms) || RegexUtil.isNull(needcount)) {
//                    return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_BOM_NUM_NULL));
//                }
//                purchaseData.setBuytypeid(1);
//            } else {
//                needcount = request.getParameter("needcount2");
//                beizhu = request.getParameter("beizhu2");
//                if (RegexUtil.isNull(beizhu) || RegexUtil.isNull(needcount)) {
//                    return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_NEED_BOM_NUM_NULL));
//                }
//                purchaseData.setBuytypeid(2);
//            }
//
//            String needfacilityid = request.getParameter("needfacility");
//            String date = request.getParameter("expireddate");
//            if (RegexUtil.isNull(needfacilityid) || RegexUtil.isNull(date)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_NEED_AD_TIME_NULL));
//            }
//            Timestamp expireddate = Timestamp.valueOf(date);
//            Timestamp needdate = new Timestamp((new Date()).getTime());
//            Timestamp createtime = new Timestamp((new Date()).getTime());
//            FacilitiesResult facilitiesDataById = facilitiesService.FacilitiesListById(schema_name, Integer.valueOf(needfacilityid));
//            BomData bomDataById = purchaseDataService.findBomDataById(schema_name, Integer.valueOf(booms));
//            if (maptype.equals("bj") && bomDataById != null && bomDataById.getId() > 0) {
//                purchaseData.setSchema_name(schema_name);
//                purchaseData.setAssetcode(bomDataById.getBom_code());
//                purchaseData.setAssetname(bomDataById.getBom_name());
//                purchaseData.setAssetnote(bomDataById.getBom_model());
//                purchaseData.setAppaccount(user.getAccount());
//                purchaseData.setNeedfacilityid(Integer.valueOf(needfacilityid));
//                purchaseData.setNeedcount(Integer.valueOf(needcount));
//                purchaseData.setNeeddate(needdate);
//                purchaseData.setExpireddate(expireddate);
//                purchaseData.setRemark(beizhu);
//                purchaseData.setIsuse(true);
//                purchaseData.setCreate_user_account(user.getAccount());
//                purchaseData.setCreatetime(createtime);
//                purchaseData.setCreate_user_name(user.getUsername());
//                purchaseData.setAppname(user.getUsername());
//                purchaseData.setNeedfacilityname(facilitiesDataById.getTitle());
//            } else if (maptype.equals("nbj")) {
//                purchaseData.setSchema_name(schema_name);
//                purchaseData.setAssetcode("");
//                purchaseData.setAssetname(beizhu);
//                purchaseData.setAssetnote(beizhu);
//                purchaseData.setAppaccount(user.getAccount());
//                purchaseData.setNeedfacilityid(Integer.valueOf(needfacilityid));
//                purchaseData.setNeedcount(Integer.valueOf(needcount));
//                purchaseData.setNeeddate(needdate);
//                purchaseData.setExpireddate(expireddate);
//                purchaseData.setRemark(beizhu);
//                purchaseData.setIsuse(true);
//                purchaseData.setCreate_user_account(user.getAccount());
//                purchaseData.setCreatetime(createtime);
//                purchaseData.setCreate_user_name(user.getUsername());
//                purchaseData.setAppname(user.getUsername());
//                purchaseData.setNeedfacilityname(facilitiesDataById.getTitle());
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_BOM));
//            }
//
//            int result = purchaseDataService.InsertPurchaseData(purchaseData);
//            if (result > 0) {
//                //增加日志
//                logService.AddLog(schema_name, beizhu, needcount, selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_BOM_DE) + beizhu, user.getAccount());
//            }
//
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_SUCC));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FAIL));
//        }
//    }
//
//
//    @RequestMapping("/delete-purchase-boom")
//    public ResponseModel delPurchaseBoom(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            User user = authService.getLoginUser(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//            PurchaseData purchaseData = new PurchaseData();
//            String id = request.getParameter("id");
//            Timestamp createtime = new Timestamp((new Date()).getTime());
//            int result = purchaseDataService.UpdatePurchaseData(schema_name, Integer.valueOf(id), user.getAccount(), createtime);
//            if (result > 0) {
//                //增加日志
//                logService.AddLog(schema_name, id, id, selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_NEED_BOM_ID) + id, user.getAccount());
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));
//        }
//    }
}




