package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.system.CopyCallbackService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.service.system.impl.CopyCallbackServiceImpl;
import com.gengyun.senscloud.util.RegexUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "流程配置")
@RestController
public class WorkFlowTemplateController {

    @Resource
    WorkFlowTemplateService workFlowTemplateService;
    @Resource
    CopyCallbackService copyCallbackService;
    @Resource
    CopyCallbackServiceImpl callbackService;

    @ApiOperation(value = "获取流程配置模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_BOM_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchWorkFlowTemplateListPermission", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowTemplateListPermission(MethodParam methodParam) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowTemplateListPermission(methodParam));
    }

    @ApiOperation(value = "获取流程模板基本信息", notes = ResponseConstant.RSP_DESC_GET_WORK_FLOW_TEMPLATE_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id","node_id"})
    @RequestMapping(value = "/searchWorkFlowTemplateInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowTemplateInfo(MethodParam methodParam, WorkFlowTemplateSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String pref_id) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowTemplateInfo(methodParam, bParam));
    }

    @ApiOperation(value = "新增/编辑流程模板基础信息", notes = ResponseConstant.RSP_DESC_ADD_WORK_FLOW_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token","page_type", "node_id", "pref_id","node_type","flow_name","work_type_id","work_template_code","relation_type","flow_code","show_name","node_name","remark","flow_show_name","node_show_name"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkFlowTemplate", method = RequestMethod.POST)
    public ResponseModel editWorkFlowTemplate(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkFlowTemplateAdd workTemplateAdd) {
        workFlowTemplateService.modifyWorkFlowTemplate(methodParam, paramMap, copyCallbackService);
        Map<String, Object> callbackResult = callbackService.getResult();
        if (callbackResult.isEmpty()) {
            return ResponseModel.okMsgForOperate();
        } else {
            return ResponseModel.ok(callbackResult);
        }
    }

    @ApiOperation(value = "查询流程、节点字段信息列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_FLOW_TEMPLATE_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id","node_id","page_type","keywordSearch","field_view_types"})
    @RequestMapping(value = "/searchWorkFlowTemplateColumnList", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowTemplateColumnList(MethodParam methodParam, WorkFlowTemplateSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = "流程、节点列表数据里的id字段", required = true) @RequestParam String node_id
            , @SuppressWarnings("unused") @ApiParam(value = "基本页面 base ，详细页面 detail", required = true) @RequestParam String page_type, @SuppressWarnings("unused") @ApiParam(value = "流程、节点列表数据里的parent字段", required = true) @RequestParam String pref_id) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowTemplateColumnListByNodeIdAndNodeType(methodParam, bParam));
    }

    @ApiOperation(value = "查询新增字段查询工单模板字段列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_LIST_FOR_ADD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch","node_id","pref_id","page_type" })
    @RequestMapping(value = "/searchWorkFlowColumnListForAdd", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowColumnListForAdd(MethodParam methodParam, WorkFlowTemplateSearchParam param, @SuppressWarnings("unused") @ApiParam(value = "流程、节点列表数据里的id字段", required = true) @RequestParam String node_id
            , @SuppressWarnings("unused") @ApiParam(value = "基本页面 base ，详细页面 detail", required = true) @RequestParam String page_type, @SuppressWarnings("unused") @ApiParam(value = "流程、节点列表数据里的parent字段", required = true) @RequestParam String pref_id) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowColumnListForAdd(methodParam, param));
    }

    @ApiOperation(value = "新增流程、节点模板字段", notes = ResponseConstant.RSP_DESC_ADD_WORK_FLOW_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"add"},includeParameters = {"token", "pref_id","node_id","field_form_codes","page_type"})
    @Transactional //支持事务
    @RequestMapping(value = "/addWorkFlowTemplateColumn", method = RequestMethod.POST)
    public ResponseModel addWorkTemplateColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkFlowTemplateColumnAdd add ) {
        workFlowTemplateService.newWorkFlowTemplateColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑流程字段区块、是否显示、操作权限、上移、下移信息", notes = ResponseConstant.RSP_DESC_EDIT_WORK_FLOW_TEMPLATE_COLUMN_USE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token", "id", "field_is_show","field_section_type","field_right","type"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkFlowColumnUse", method = RequestMethod.POST)
    public ResponseModel editWorkFlowColumnUse(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateColumnAdd workTemplateAdd,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam Integer id
    ) {
        if(RegexUtil.optIsPresentStr(workTemplateAdd.getType()) && ("4".equals(workTemplateAdd.getType())||"5".equals(workTemplateAdd.getType()))){
            workFlowTemplateService.modifyWorkFlowOrder(methodParam, paramMap);
        }else {
            workFlowTemplateService.modifyWorkFlowColumnUse(methodParam, paramMap);
        }
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑流程配置字段", notes = ResponseConstant.RSP_DESC_EDIT_WORK_TEMPLATE_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnInfo"},includeParameters = {"token", "id","field_value","data_key","field_form_code", "field_code", "field_name", "field_right", "is_required", "field_remark", "field_view_type", "field_section_type", "extList","linkList"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkFlowTemplateColumn", method = RequestMethod.POST)
    public ResponseModel editWorkFlowTemplateColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateColumnAdd workColumnInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam Integer id) {
        workFlowTemplateService.modifyWorkFlowTemplateColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑流程模板操作人信息", notes = ResponseConstant.RSP_DESC_EDIT_WORK_FLOW_TEMPLATE_HANDLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token", "pref_id","node_id","deal_type_id","deal_role_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkFlowTemplateHandle", method = RequestMethod.POST)
    public ResponseModel editWorkFlowTemplateHandle(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkFlowTemplateColumnAdd workTemplateAdd ) {
        workFlowTemplateService.modifyWorkFlowTemplateHandle(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除流程模板字段", notes = ResponseConstant.RSP_DESC_REMOVE_WORK_FLOW_TEMPLATE_COLUMN_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/removeSelectWorkFlowColumn", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectWorkFlowColumn(MethodParam methodParam,WorkFlowTemplateColumnAdd param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam Integer id) {
        workFlowTemplateService.cutWorkFlowColumnById(methodParam,param);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "批量删除流程模板字段", notes = ResponseConstant.RSP_DESC_REMOVE_WORK_FLOW_TEMPLATE_COLUMN_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/removeSelectWorkFlowColumnBatch", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectWorkFlowColumnBatch(MethodParam methodParam,WorkFlowTemplateColumnAdd param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        workFlowTemplateService.cutWorkFlowColumnByIds(methodParam,param);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取流程配置字段详细信息", notes = ResponseConstant.RSP_DESC_GET_WORK_FLOW_COLUMN_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchWorkFlowColumnInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowColumnInfo(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String id) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowColumnInfoById(methodParam, bParam));
    }

    @ApiOperation(value = "获取流程、节点的全部信息", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id","node_id","page_type" })
    @RequestMapping(value = "/searchWorkFlowNodeTemplateInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowNodeTemplateInfo(MethodParam methodParam, WorkFlowTemplateColumnAdd param) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowNodeTemplateInfo(methodParam, param.getPref_id(),param.getNode_id(),param.getPage_type()));
    }

    @ApiOperation(value = "通过pref_id获取流程start节点的全部信息", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id" })
    @RequestMapping(value = "/searchWorkFlowStartNodeTemplateInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowStartNodeTemplateInfo(MethodParam methodParam, WorkFlowTemplateColumnAdd param) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowStartNodeTemplateInfo(methodParam, param.getPref_id()));
    }

    @ApiOperation(value = "通过pref_id获取流程end节点的全部信息", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id" })
    @RequestMapping(value = "/searchWorkFlowEndNodeTemplateInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowEndNodeTemplateInfo(MethodParam methodParam, WorkFlowTemplateColumnAdd param) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowEndNodeTemplateInfo(methodParam, param.getPref_id()));
    }


    @ApiOperation(value = "根据流程、节点类型查询对应克隆模板", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_LIST_FOR_ADD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","node_type" })
    @RequestMapping(value = "/searchWorkFlowTemplateListForClone", method = RequestMethod.POST)
    public ResponseModel searchWorkFlowTemplateListForClone(MethodParam methodParam, WorkFlowTemplateSearchParam param, @SuppressWarnings("unused") @ApiParam(value = "节点类型", required = true) @RequestParam String node_type) {
        return ResponseModel.ok(workFlowTemplateService.getWorkFlowTemplateListForClone(methodParam, param));
    }

    @ApiOperation(value = "克隆流程、节点", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_COLUMN_CLONE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id","node_id","node_type","word_flow_node_id","flow_code","flow_name","node_name"})
    @RequestMapping(value = "/addWorkFlowNodeClone", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel addWorkFlowNodeClone(MethodParam methodParam, WorkFlowTemplateAdd bParam,
                                              @SuppressWarnings("unused") @ApiParam(value = "流程、节点列表数据里的id字段", required = true) @RequestParam String node_id,
                                              @SuppressWarnings("unused") @ApiParam(value = "流程：flow,开始节点：start,过程节点：node,结束节点：end,子节点：subNode", required = true) @RequestParam String node_type,
                                              @SuppressWarnings("unused") @ApiParam(value = "流程、节点列表数据里的parent字段", required = true) @RequestParam String pref_id ,
                                              @SuppressWarnings("unused") @ApiParam(value = "节点表的主键", required = true) @RequestParam String word_flow_node_id) {
        workFlowTemplateService.newWorkFlowNodeClone(methodParam, bParam, copyCallbackService);
        Map<String, Object> callbackResult = callbackService.getResult();
        if (callbackResult.isEmpty()) {
            return ResponseModel.okMsgForOperate();
        } else {
            return ResponseModel.ok(callbackResult);
        }
    }

    @ApiOperation(value = "一键同步流程、节点", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_COLUMN_SYNC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token","page_type", "node_id", "pref_id","node_type","flow_name","work_type_id","work_template_code","relation_type","flow_code","show_name","node_name","remark","flow_show_name","node_show_name"})
    @RequestMapping(value = "/modifyWorkFlowNodeSync", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel modifyWorkFlowNodeSync(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        workFlowTemplateService.modifyWorkFlowNodeSync(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

//    @ApiOperation(value = "删除流程配置", notes = ResponseConstant.RSP_DESC_REMOVE_WORK_TEMPLATE_BY_SELECT)
//    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id"})
//    @RequestMapping(value = "/removeWorkFlowTemplate", method = RequestMethod.POST)
//    @Transactional //支持事务
//    public ResponseModel removeWorkFlowTemplate(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String pref_id) {
//        workFlowTemplateService.cutWorkFlowTemplate(methodParam,pref_id);
//        return ResponseModel.okMsgForOperate();
//    }

//    @ApiOperation(value = "获取工单模板字段关联条件明细信息", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_COLUMN_LINKAGE_CONDITION_INFO)
//    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
//    @RequestMapping(value = "/searchWorkTemplateColumnLinkageInfo", method = RequestMethod.POST)
//    public ResponseModel searchWorkTemplateColumnLinkageInfo(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam Integer id) {
//        return ResponseModel.ok(workTemplateService.getWorkTemplateColumnLinkageInfo(methodParam, bParam));
//    }
}
