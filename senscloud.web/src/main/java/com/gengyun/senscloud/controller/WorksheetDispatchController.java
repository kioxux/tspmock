package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SearchParam;
import com.gengyun.senscloud.model.WorkFlowTemplateSearchParam;
import com.gengyun.senscloud.model.WorkListModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.WorksheetDispatchService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(tags = "工单调度")
@RestController
public class WorksheetDispatchController {

    @Resource
    WorksheetDispatchService worksheetDispatchService;

    @ApiOperation(value = "获取工单调度模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_WORKSHEET_DISPATCH_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getWorksheetDispatchPermission", method = RequestMethod.POST)
    public ResponseModel getWorksheetDispatchPermission(MethodParam methodParam) {
        return ResponseModel.ok(worksheetDispatchService.getWorksheetDispatchPermission(methodParam));
    }

    @ApiOperation(value = "查询已分配工单列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKSHEET_DISPATCH_DISTRIBUTION_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","beginDateSearch","endDateSearch","keywordSearch"})
    @RequestMapping(value = "/searchWorksheetDistributionList", method = RequestMethod.POST)
    public ResponseModel searchWorksheetDistributionList(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(worksheetDispatchService.getWorksheetDistributionList(methodParam, param));
    }

    @ApiOperation(value = "查询待分配工单信息列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKSHEET_DISPATCH_UNDISTRIBUTED_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "workTypeIds","beginDateSearch","endDateSearch","keywordSearch","descStatus", "pageSize", "pageNumber"})
    @RequestMapping(value = "/searchWorksheetUndistributedList", method = RequestMethod.POST)
    public ResponseModel searchWorksheetUndistributedList(MethodParam methodParam, SearchParam param) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(worksheetDispatchService.getWorksheetUndistributedList(methodParam, param));
    }

    @ApiOperation(value = "根据子工单编号查询工单信息列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKSHEET_LIST_BY_SUB_WORK_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "subWorkCodes"})
    @RequestMapping(value = "/searchWorkListDetailBySwc", method = RequestMethod.POST)
    public ResponseModel selectWorkListsDetailByWorkCode(MethodParam methodParam, SearchParam param) {
        return ResponseModel.ok(worksheetDispatchService.getWorkListDetailBySwc(methodParam, param));
    }


//    /**
//     * 功能：查询待分配工单信息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/selectUndistributed")
//    public String selectWorkListsDetailUndistributed(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            result = workListService.selectWorkListsDetailUndistributed(schemaName);
//            return result.toString();
//        } catch (Exception e) {
//            result.put("msg", selectOptionService.getLanguageInfo(LangConstant.MSG_DIS_LIST_ERROR));//待分配订单查询异常
//            result.put("code", StatusConstant.ERROR_RETURN);
//            return result.toString();
//        }
//    }
//
//    /**
//     * 功能：更新工单详情状态
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/updateWorkListStatus")
//    public ResponseModel updateWorkListStatus(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String statusStr = request.getParameter("status");
//            String subWorkCode = request.getParameter("sub_work_code");
//            if (StringUtils.isEmpty(statusStr) || StringUtils.isEmpty(statusStr) || StringUtils.isEmpty(statusStr)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg("");
//                return model;
//            }
//            Integer status = Integer.parseInt(statusStr);
//            int doUpdate = workListService.updateWorkListStatus(schema_name, status, subWorkCode);
//            if (doUpdate > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_SUCCESS));
//            } else {
//                model.setCode(StatusConstant.UPDATE_ERROR);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_ERROR));
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_STATUS_ERROR));//状态更新出现异常
//            return model;
//        }
//    }
//
//    /**
//     * 功能：删除工单
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/delUndistributedWork")
//    public ResponseModel delUndistributedWork(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("work_sub_code");
//            String workCode = request.getParameter("work_code");
//            if (StringUtils.isEmpty(subWorkCode) || StringUtils.isEmpty(subWorkCode) || StringUtils.isEmpty(subWorkCode)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WK_CODE));
//                return model;
//            }
//            if (StringUtils.isEmpty(workCode) || StringUtils.isEmpty(workCode) || StringUtils.isEmpty(workCode)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UN_GET_WORK_CODE));
//                return model;
//            }
//            Boolean doUpdate = workListService.delUndistributedWork(schema_name, subWorkCode, workCode);
//            if (doUpdate) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_OK));
//            } else {
//                model.setCode(StatusConstant.UPDATE_ERROR);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.FAIL_DELE));
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_WK_DEL_FAIL));//工单删除出现异常
//            return model;
//        }
//    }
//
//    /**
//     * 功能：作废工单
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/cancelUndistributedWork")
//    public ResponseModel cancelUndistributedWork(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("work_sub_code");
//            String workCode = request.getParameter("work_code");
//            if (StringUtils.isEmpty(subWorkCode) || StringUtils.isEmpty(subWorkCode) || StringUtils.isEmpty(subWorkCode)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WK_CODE));//未获得工单详情编号
//                return model;
//            }
//            if (StringUtils.isEmpty(workCode) || StringUtils.isEmpty(workCode) || StringUtils.isEmpty(workCode)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WK_CODE));//未获得工单详情编号
//                return model;
//            }
//            int status = StatusConstant.CANCEL;//作废
//
//            Boolean doUpdate = workListService.cancelUndistributedWork(schema_name, subWorkCode, workCode, status);
//            if (doUpdate) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OBSOLETE_SUCCESS));//作废成功
//            } else {
//                model.setCode(StatusConstant.UPDATE_ERROR);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CANCEL_FAIL));//作废失败
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CANCEL_WORK_EXEC));
//            return model;
//        }
//    }
//
//    /**
//     * 功能：工单池任务分配
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @Deprecated
//    @RequestMapping("/linkRepairManToWorkOrder")
//    @Transactional
//    public ResponseModel linkRepairManToWorkOrder(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            WorkListDetailModel workListDetailModel = new WorkListDetailModel();
//            String receive_account = request.getParameter("repairManCode");//维修人编号
//            String time = request.getParameter("time");
//            String status = request.getParameter("status");
//            Timestamp createTimeStamp = new Timestamp(System.currentTimeMillis());
//            String creatTimeStr = createTimeStamp + "";
//            String hourMin = creatTimeStr.substring(10, creatTimeStr.length());
//            Timestamp revice_time = Timestamp.valueOf(time + hourMin);
//
//            String subWorkCode = request.getParameter("work_type_id");//工单编号
//            String workCode = request.getParameter("work_code");//工单编号
//            String[] arr_sub_work_code = subWorkCode.split(",");
//            workListDetailModel.setReceive_time(revice_time);
//            if (StringUtils.isEmpty(receive_account) || StringUtils.isEmpty(receive_account) || StringUtils.isEmpty(receive_account)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_P_CODE));//未获得负责人编号
//                return model;
//            }
//            if (subWorkCode == null || arr_sub_work_code.length == 0) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WK_CODE));//未获得工单详情编号
//                return model;
//            }
//            int doUpdate = 0;
////            for(int i =0;i<arr_sub_work_code.length;i++){
//            if (status != null && !status.equals("") && status.equals("update")) {
//                int doDee = workListService.linkRepairManToWorkOrder(schema_name, "", workCode);//删除原分配工单
//            }
//            doUpdate = workListService.linkRepairManToWorkOrder(schema_name, receive_account, workCode);//工单池任务分配
////            }
//
//            if (doUpdate > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ALLOT_TASK_SUCC));//分配成功
////                for(int j=0;j<arr_sub_work_code.length;j++){
//                int doUpdateStatus = workListService.updateWorkListStatusAndTime(schema_name, StatusConstant.PENDING, workCode, workListDetailModel);
//
////                }
//                int Status = workListService.updateWorkStatusAndTime(schema_name, StatusConstant.PENDING, workCode);
//
//                User user = AuthService.getLoginUser(request);
//                user = commonUtilService.checkUser(schema_name, user, request);
//                if (null == user) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//                }
//                // 主数据
//                Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, workCode);
//                String workTypeId = String.valueOf(mainDetailInfo.get("work_type_id"));
//                String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//                String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//                String receiveAccountName = selectOptionService.getOptionNameByCode(schema_name, "user", receive_account, null);
//                String doType = SensConstant.LOG_TYPE_NAME.get(businessTypeId); // 日志标志名称
//                doType = selectOptionService.getLanguageInfo(doType);
//                String log = selectOptionService.getLanguageInfo(LangConstant.DISTRIBUTION_A) + doType + selectOptionService.getLanguageInfo(LangConstant.TASK_TA) + selectOptionService.getLanguageInfo(LangConstant.LOG_GIVE) + receiveAccountName;
//                logService.AddLog(schema_name, pageType, (String) mainDetailInfo.get("sub_work_code"), log, user.getAccount());//记录历史
//            } else {
//                model.setCode(StatusConstant.UPDATE_ERROR);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ALLOT_TASK_FAIL));//分配成功
//            }
//            return model;
//
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ALLOT_TASK_FAIL));//分配成功
//            return model;
//        }
//    }
//
//    /**
//     * 功能：撤回工单池任务分配
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/delLinkRepairManToWorkOrder")
//    public ResponseModel delLinkRepairManToWorkOrder(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            WorkListDetailModel workListDetailModel = new WorkListDetailModel();
//            String subWorkCode = request.getParameter("work_type_id");//工单编号
//            if (StringUtils.isEmpty(subWorkCode) || StringUtils.isEmpty(subWorkCode) || StringUtils.isEmpty(subWorkCode)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WK_CODE));//未获得工单详情编号
//                return model;
//            }
//
//            int undealNum = workListService.selectWorkListsDetailStatus(schema_name, subWorkCode);
//            if (undealNum == 0) {
//                model.setCode(StatusConstant.ERROR_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_RECOVERY));//"不允许回收"
//                return model;
//            }
//            int doUpdate = workListService.linkRepairManToWorkOrder(schema_name, "", subWorkCode);//删除分配人员
////            int doUpdate = workListService.linkRepairManToWorkOrder(schema_name,"",work_code);//删除分配人员
//
//            if (doUpdate > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_RECOVERY_SUC));//回收工单成功
//                int doUpdateStatus = workListService.updateBackWorkListStatus(schema_name, StatusConstant.UNDISTRIBUTED, subWorkCode);//修改分配状态
//                int Status = workListService.updateWorkStatusAndTime(schema_name, StatusConstant.UNDISTRIBUTED, subWorkCode);
//
//                User user = AuthService.getLoginUser(request);
//                user = commonUtilService.checkUser(schema_name, user, request);
//                if (null == user) {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_LOGIN_USER_WRONG));//当前登录人有错误。
//                }
//                // 主数据
//                Map<String, Object> mainDetailInfo = workSheetHandleService.queryMainDetailByWorkCode(schema_name, subWorkCode);
//                String workTypeId = String.valueOf(mainDetailInfo.get("work_type_id"));
//                String businessTypeId = selectOptionService.queryWorkTypeById(schema_name, workTypeId).get("business_type_id").toString(); // 业务类型
//                String pageType = SensConstant.LOG_TYPE_INFO.get(businessTypeId); // 日志标志
//                String log = selectOptionService.getLanguageInfo(LangConstant.MSG_RECOVERY_A);
//                logService.AddLog(schema_name, pageType, (String) mainDetailInfo.get("sub_work_code"), log, user.getAccount());//记录历史
//
//            } else {
//                model.setCode(StatusConstant.UPDATE_ERROR);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_RECOVERY_FAIL));//回收工单失败
//            }
//            return model;
//
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_RECOVERY_ERROR));//回收工单出现异常
//            return model;
//        }
//    }
//

//
//    /**
//     * 功能：更新工单池
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/updateWorkPool")
//    public ResponseModel updateWorkPool(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String work_code = request.getParameter("work_code");
//            String pool_id = request.getParameter("pool_id");
//
//            if (StringUtils.isEmpty(work_code) || StringUtils.isEmpty(work_code) || StringUtils.isEmpty(work_code)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WK_CODE_ERROR));//工单编号不能为空
//                return model;
//            }
//            if (StringUtils.isEmpty(pool_id) || StringUtils.isEmpty(pool_id) || StringUtils.isEmpty(pool_id)) {
//                model.setCode(StatusConstant.VALIDATE);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NONE_WKP_CODE_ERROR));//工单池编号不允许为空
//                return model;
//            }
//
//            int poolId = Integer.parseInt(pool_id);
//            int doUpdate = workListService.updateWorkPool(schema_name, work_code, poolId);
//            if (doUpdate > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_WKP_SUC));//更改工单池成功
//            } else {
//                model.setCode(StatusConstant.UPDATE_ERROR);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_WKP_FAIL));//更改工单池失败
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_WKP_ERROR));//更改工单池出现异常
//            return model;
//        }
//    }
//
//    @RequestMapping("/selectWorkListsDetailByWorkCode")
//    public ResponseModel selectWorkListsDetailByWorkCode(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String sub_work_code = request.getParameter("sub_work_code");
//            String condition = "";
//            String[] code_split = sub_work_code.split(",");
//            for (int i = 0; i < code_split.length; i++) {
//                String code = code_split[i];
//                code = "\'" + code + "\'";
//                if (i < (code_split.length - 1)) {
//                    code = code + ",";
//                }
//                condition += code;
//
//            }
//            condition = "w.work_code in (" + condition + ")";
//
//            List<WorkListModel> workListDetailModels = workListService.selectWorkListsByWorkCode(schema_name, condition);
//            if (workListDetailModels != null && workListDetailModels.size() > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_SU));
//                model.setContent(workListDetailModels);
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo(LangConstant.CHECK_ND));//暂无查询数据
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo(LangConstant.AEO_WQ));//详情查询出现异常
//            return model;
//        }
//    }
//
//    /*public ResponseModel getAllUser(HttpServletResponse response, HttpServletRequest request){
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            List<User> userList = userService.findAll(schema_name);
//            if(userList!=null && userList.size()>0){
//                model.setCode(1);
//                model.setContent(userList);
//                model.setMsg("所有的维修人员");
//            }
//
//        }catch (Exception e){
//
//        }
//    }*/
}
