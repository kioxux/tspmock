package com.gengyun.senscloud.controller;

//import com.alibaba.fastjson.JSONArray;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.config.qcloudsmsConfig;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.enumResp.ResultStatus;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Constants;
//import com.gengyun.senscloud.util.Message;
//import com.gengyun.senscloud.util.StringUtil;
//import com.gengyun.senscloud.view.MaintainDataExportView;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.MessageFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * Created by hisou on 2018/3/21.// 准备删除 yzj  2019-08-04
// */
//@Deprecated
//@RestController
public class MaintainController {
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    BomService bomService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @RequestMapping("/maintain-management")
//    public ModelAndView maintainManagement(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String status = request.getParameter("status");
//        String facilitiesSelect = "";
//        Boolean exportFlag = false;
//        Boolean invalidFlag = false;
//        Boolean auditFlag = false;
//        Boolean auditpcFlag = false;
//        Boolean dealpcFlag = false;
//        Boolean deadlineFlag = false;
//        Boolean applyForBomFlag = false;
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "maintain");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("maintain_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("maintain_audit")) {
//                        auditFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_audit_pc")) {
//                        auditpcFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_result_submit_pc")) {
//                        dealpcFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_export")) {
//                        exportFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_invalid")) {
//                        invalidFlag = true;
//                    } else if (functionData.getFunctionName().equals("maintain_deadline")) {
//                        deadlineFlag = true;
//                    }
//                }
//            }
//
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, Constants.APPLY_FOR_BOM);
//            if (dataList != null) {
//                if (dataList.getSettingValue().equals("1")) {
//                    applyForBomFlag = true;
//                }
//            }
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        return new ModelAndView("maintain/maintain_list").addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("exportFlag", exportFlag).addObject("mtInvalidFlag", invalidFlag)
//                .addObject("mtAuditFlag", auditFlag).addObject("currentUser", loginUser.getAccount())
//                .addObject("mtAuditpcFlag", auditpcFlag).addObject("mtDealpcFlag", dealpcFlag)
//                .addObject("mtDeadlineFlag", deadlineFlag)
//                .addObject("searchStatus", status)
//                .addObject("applyForBomFlag", applyForBomFlag);
//    }
//
//    @RequestMapping("/find-asset-by-code")
//    public ResponseModel findAssetByCode(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            //设备编号
//            String code = request.getParameter("code");
//            Asset assetData = repairService.getAssetCommonDataByCode(schema_name, code);
//
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_SUCC));
//            result.setContent(assetData);
//
//            return result;
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_ASSET_FA));
//        }
//    }
//
//    @RequestMapping("/find-maintain-list")
//    public String queryMaintain(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        org.json.JSONObject result = new org.json.JSONObject();
//        List<MaintainData> maintainDataList;
//        try {
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String searchStatus = request.getParameter("status");
//            String facilities = request.getParameter("facilities");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            // 当前登录人
//            User user = authService.getLoginUser(request);
//            String account = user.getAccount();
//            String condition;
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "maintain");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("maintain_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("maintain_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//
//
//            if (facilities != null && facilities != "") {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilities, facilityList);
//                condition = " and r.facility_id in (" + facilities + subFacility + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = "  ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = " and r.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        //没有用户所属位置，则按个人权限查询
//                        condition = " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.maintain_account='" + account + "' )";
//                    }
//                } else {
//                    condition = " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.maintain_account='" + account + "' )";
//                }
//            }
//
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(r.maintain_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%" + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or r.maintain_account like '%" + keyWord + "%' )";
//            }
//
//            if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("0")) {
//                condition += " and (r.status = " + searchStatus + " ) ";
//            }
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.createtime <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            maintainDataList = maintainService.getMaintainList(schema_name, condition, pageSize, begin);
//            int total = maintainService.getMaintainListCount(schema_name, condition);
//
//            result.put("rows", maintainDataList);
//            result.put("total", total);
//            return result.toString();
//
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping("/find-single-maintain")
//    public ResponseModel querySingleMaintain(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        User user = authService.getLoginUser(request);
//        String account = user.getAccount();
//        try {
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "maintain");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("maintain_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("maintain_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            String code = request.getParameter("code");
//            MaintainData data = maintainService.getMaintainInfo(schema_name, code);
//            if (data != null && data.getAssetCode() != null) {
//                Asset assetData = assetService.findByID(schema_name, data.getAssetId());
//                if (assetData != null && assetData.getProperties() != null) {
//                    data.setAssetProperties(assetData.getProperties());
//                }
//            }
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(data);
//            if (isAllFacility) {
//                return result;
//            } else if (isSelfFacility) {
//                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        if (facilityList.get(key).getId() == data.getFacilityId()) {
//                            return result;
//                        }
//                    }
//                }
//            }
//            if (account.equalsIgnoreCase(data.getCreate_user_account()) || account.equalsIgnoreCase(data.getMaintainAccount())
//                    || account.equalsIgnoreCase(data.getAuditAccount())) {
//                return result;
//            }
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_AU_MAINTAIN));
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/find-maintain-item")
//    public ResponseModel queryMaintainItem(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        ResponseModel result = new ResponseModel();
//        try {
//            //设备编号
//            String code = request.getParameter("deviceCode");
//            Asset assetData = repairService.getAssetCommonDataByCode(schema_name, code);
//
//            List<MaintainItemData> list = maintainService.getMaintainItemByAssetTypeId(schema_name, assetData.getAsset_type());
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/find-maintain-work-hour")
//    public ResponseModel queryMaintainWorkHour(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        ResponseModel result = new ResponseModel();
//        try {
//            String code = request.getParameter("code");
//            List<MaintainWorkHourData> list = maintainService.getMaintainWorkHourList(schema_name, code);
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/find-maintain-bom")
//    public ResponseModel queryMaintainBom(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        ResponseModel result = new ResponseModel();
//        try {
//            String code = request.getParameter("code");
//            List<MaintainBomData> list = maintainService.getMaintainBomList(schema_name, code);
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/find-maintain-check")
//    public ResponseModel queryMaintainCheckItem(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        ResponseModel result = new ResponseModel();
//        try {
//            String code = request.getParameter("code");
//            Asset data = maintainService.getAssetCommonDataByMaintainCode(schema_name, code);
//            List<MaintainCheckItemData> list = maintainService.getMaintainCheckItem(schema_name, data.getAsset_type());
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/save-maintain-data")
//    @Transactional
//    public ResponseModel saveMaintain(HttpServletRequest request, HttpServletResponse response) {
//        Company company = AuthService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            MaintainData data = new MaintainData();
//            //找到设备
//            String deviceCode = request.getParameter("strcode");
//            Asset assetData = repairService.getAssetCommonDataByCode(schema_name, deviceCode);
//            //生成最大的保养单号
//            String maintainCode = serialNumberService.generateMaxBusinessCode(schema_name, "maintain");
//            data.setMaintainCode(maintainCode);
//            data.setFacilityId(Integer.parseInt(request.getParameter("facilityId")));
//
//            data.setAssetId(assetData.get_id());
//            data.setAssetType(assetData.getAsset_type());
//            data.setStatus(70);
//            data.setDeadlineTime(new Timestamp(Long.parseLong(request.getParameter("deadlineTime"))));
//            if (StringUtil.isNotEmpty(request.getParameter("receiveAccount"))) {
//                data.setMaintainAccount(request.getParameter("receiveAccount"));
//                data.setMaintainName(request.getParameter("receiveName"));
//                data.setReveiveTime(new Timestamp(new Date().getTime()));
//            } else {
//                data.setMaintainAccount("");
//                data.setReveiveTime(null);
//            }
//            User user = authService.getLoginUser(request);
//            data.setCreate_user_account(user.getAccount());
//            data.setCreate_user_name(user.getUsername());
//
//            if (StringUtil.isNotEmpty(request.getParameter("itemData"))) {
//                List<MaintainItemData> maintainItemDataList = JSONArray.parseArray(request.getParameter("itemData"), MaintainItemData.class);
////                for (MaintainItemData itemData : maintainItemDataList) {
////
////                }
//                data.setMaintainResult(request.getParameter("itemData"));
//            }
//
//            //JSONObject maintainResult = new JSONObject();
//
//
//            maintainService.saveMaintain(schema_name, data);
//
//            String opAccount = request.getParameter("operateAccount");
//            maintainService.deleteMaintainWorkHour(schema_name, data.getMaintainCode());
//            if (StringUtil.isNotEmpty(opAccount)) {
//                List<String> opAccountList = Arrays.asList(opAccount.split(","));
//                for (String account : opAccountList) {
//                    MaintainWorkHourData workHourData = new MaintainWorkHourData();
//                    workHourData.setOperateAccount(account);
//                    workHourData.setMaintainCode(data.getMaintainCode());
//                    workHourData.setWorkHour(0f);
//                    maintainService.insertMaintainWorkHour(schema_name, workHourData);
//                }
//            }
//            if (company.getIs_open_sms()) {
//                // 获取操作人
//                User accountInfo = userService.getUserByAccount(schema_name, user.getAccount());
//                //发送短信给保养确认人
//                List<User> userList = null;//userService.findUserByFunctionKey(schema_name, "maintain_audit", (int) assetData.getIntsiteid());
//                if (userList != null && !userList.isEmpty()) {
//                    String mobile = userList.get(0).getMobile();
//                    String receiveAccount = userList.get(0).getAccount();
//                    String content;
//                    if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                        String pattern = "{0}提交了设备：{1}，设备编码：{2}的保养结果，请尽快确认。";
//                        content = MessageFormat.format(pattern, accountInfo.getUsername(), assetData.getStrname(), assetData.getStrcode());
//                    } else {
//                        String pattern = "{0,number,#}::{1}::{2}::{3}";
//                        content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_CONFIRM, accountInfo.getUsername(), assetData.getStrname(), assetData.getStrcode());
//                    }
//                    Message.beginMsg(schema_name, content, mobile, receiveAccount, "maintain", maintainCode, user.getAccount());
//                }
//            }
//
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//        }
//    }
//
//    @RequestMapping("audit-maintain")
//    @Transactional
//    public ResponseModel auditMaintain(HttpServletRequest request, HttpServletResponse response) {
//        Company company = AuthService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            String code = request.getParameter("code");
//
//            User currentUser = authService.getLoginUser(request);
//
//            MaintainData maintainData = maintainService.getMaintainInfo(schema_name, code);
//            if (maintainData != null) {
//                String checkResult = request.getParameter("checkResult");
//                maintainData.setCheckResult(checkResult);
//                maintainData.setAuditAccount(currentUser.getAccount());
//                maintainData.setAuditTime(new Timestamp(new Date().getTime()));
//                maintainData.setAuditWord(request.getParameter("words"));
//                maintainData.setStatus(60);   //？如果确认是退回呢？姚志军  2018-05-02？？？？？？？？？？？？？？？？？？？？
////                String auditStatus = request.getParameter("auditStatus");
//                String logWord = selectOptionService.getLanguageInfo( LangConstant.MSG_SURE_MAIN_DATA);
////                if (auditStatus.equals("1")) {
//                maintainData.setStatus(60);
////                } else {
////                    maintainData.setStatus(70);
////                    logWord += "【退回】了保养结果";
////                }
//
//                maintainService.AuditMaintainResult(schema_name, maintainData);
//
//                //获取维修的设备
//                Asset assetInfo = maintainService.getAssetCommonDataByMaintainCode(schema_name, code);
//                if (company.getIs_open_sms()) {
//                    // 获取短信接受人
//                    User accountInfo = userService.getUserByAccount(schema_name, maintainData.getMaintainAccount());
//                    // 获取短信发送人
//                    User sendInfo = userService.getUserByAccount(schema_name, currentUser.getAccount());
//                    if (accountInfo.getAccount() != null && !accountInfo.getAccount().isEmpty() && !accountInfo.getAccount().isEmpty()) {
//                        String mobile = accountInfo.getMobile();
//                        String content;
//                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                            String pattern = "{0}确认了设备：{1}，设备编码：{2}的保养任务，请知晓。";
//                            content = MessageFormat.format(pattern, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                        } else {
//                            String pattern = "{0,number,#}::{1}::{2}::{3}";
//                            content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_ACCEPT, sendInfo.getUsername(), assetInfo.getStrname(), assetInfo.getStrcode());
//                        }
//                        Message.beginMsg(schema_name, content, mobile, accountInfo.getAccount(), "maintain", code, currentUser.getAccount());
//                    }
//                }
//
//                //扣除备件，从备件库中减去使用的备件
//                if (maintainData != null && maintainData.getBomList() != null) {
//                    for (MaintainBomData data : maintainData.getBomList()) {
//                        if (data != null && data.getFacilityId() > 0 && !data.getBomModel().isEmpty() && !data.getBomCode().isEmpty() && !data.getStockCode().isEmpty()) {
//                            bomService.updateBomCount(schema_name, data.getBomModel(), data.getBomCode(), data.getStockCode(), data.getFacilityId(), data.getUseCount());
//                        }
//                    }
//                }
//
//                //给设备记录一条日志
//                DateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS_2);
//                String doTime = sdf.format(maintainData.getFinishedTime());
//                logService.AddLog(schema_name, "asset", assetInfo.get_id(), doTime + "，设备进行了一次保养：" + maintainData.getMaintainNote() + "，保养单号:" + maintainData.getMaintainCode(), maintainData.getMaintainAccount());
//
//                //保养确认日志
//                logService.AddLog(schema_name, "maintain", code, logWord + "，意见：" + request.getParameter("words"), currentUser.getAccount());
//
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_AUDIT_COMPLETED));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_MAINTAIN_NO));
//            }
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_AUDIT_FA));
//        }
//    }
//
//    @RequestMapping("return-maintain")
//    @Transactional
//    public ResponseModel returnMaintain(HttpServletRequest request, HttpServletResponse response) {
//        Company company = AuthService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            String code = request.getParameter("code");
//            User currentUser = authService.getLoginUser(request);
//            MaintainData maintainData = maintainService.getMaintainInfo(schema_name, code);
//            if (maintainData != null) {
//                String checkResult = request.getParameter("checkResult");
//                maintainData.setCheckResult(checkResult);
//                maintainData.setAuditAccount(currentUser.getAccount());
//                maintainData.setReturnTime(new Timestamp(new Date().getTime()));
//                maintainData.setAuditWord(request.getParameter("words"));
//                maintainData.setStatus(70);
//                String logWord = selectOptionService.getLanguageInfo( LangConstant.MSG_SURE_MAIN_DATA_BACK);
//                maintainService.AuditMaintainResult(schema_name, maintainData);
//                if (company.getIs_open_sms()) {
//                    User accountInfo = userService.getUserByAccount(schema_name, maintainData.getMaintainAccount());
//                    // 获取短信发送人
//                    User sendInfo = userService.getUserByAccount(schema_name, currentUser.getAccount());
//                    if (accountInfo.getAccount() != null && !accountInfo.getAccount().isEmpty()) {
//                        //发送短信给维修审核人
//                        String mobile = accountInfo.getMobile();
//                        String content;
//                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                            String pattern = "{0}退回了设备：{1}，设备编码：{2}的保养任务，请重新保养。";
//                            content = MessageFormat.format(pattern, sendInfo.getUsername(), maintainData.getAssetName(), maintainData.getAssetName());
//                        } else {
//                            String pattern = "{0,number,#}::{1}::{2}::{3}";
//                            content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_REJECT, sendInfo.getUsername(), maintainData.getAssetName(), maintainData.getAssetName());
//                        }
//                        Message.beginMsg(schema_name, content, mobile, accountInfo.getAccount(), "maintain", code, currentUser.getAccount());
//                    }
//                }
//                //保养确认日志
//                logService.AddLog(schema_name, "maintain", code, logWord + "，意见：" + request.getParameter("words"), currentUser.getAccount());
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_BACK_COMPLETED));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_MAINTAIN_NO));
//            }
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_BACK_FA));
//        }
//    }
//
//    @RequestMapping("/maintain-check")
//    public ResponseModel loginCheck(HttpServletRequest request, HttpServletResponse response) {
//        String schema = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        boolean flag = false;
//        try {
//            String functionKey = request.getParameter("func");
//            String code = request.getParameter("code");
//            User user = authService.getLoginUser(request);
//            LinkedHashMap<String, ISystemPermission> permissions = user.getPermissions();
//            MaintainData maintainData = maintainService.getMaintainInfo(schema, code);
//
//            for (Map.Entry permission : permissions.entrySet()) {
//                SystemPermission sysPermission = (SystemPermission) permission.getValue();
//                // 如果是审核功能，需要判断创建人和登录用户是否为同一人
//                if ("maintain_audit".equals(functionKey)) {
//                    flag = maintainData.getCreate_user_account().equals(user.getAccount()) ? true : false;
//                }
//                // 其他功能
//                if (functionKey.equals(sysPermission.getKey())) {
//                    flag = true;
//                }
//            }
//            com.alibaba.fastjson.JSONObject jsonObject = new com.alibaba.fastjson.JSONObject();
//            jsonObject.put("functionFlag", flag);
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(jsonObject);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NOW_ACC_FA));
//            return result;
//        }
//    }
//
//
//    @RequestMapping("/get_menu_facilities")
//    public ResponseModel FindMenuFacilities(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        ResponseModel result = new ResponseModel();
//        String menucode = request.getParameter("menucode");
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), menucode);
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("asset_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    }
//                }
//            }
//            if (AuthService.isUserHasRoleId(request, UserRole.ROLE_ID_SYSTEM_ADMIN, UserRole.ROLE_ID_COMPANY_ADMIN)) {
//                isAllFacility = true;
//            }
//            String facilitiesSelect = "";
//            if (isAllFacility) {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                TreeBuilder treeBuilder = new TreeBuilder();
//                List<Node> allNodes = new ArrayList<Node>();
//                for (Facility facility : facilityList) {
//                    Node node = new Node();
//                    node.setId(String.valueOf(facility.getId()));
//                    node.setParentId(String.valueOf(facility.getParentId()));
//                    node.setName(facility.getTitle());
//                    node.setDetail(facility);
//                    allNodes.add(node);
//                }
//                List<Node> roots = treeBuilder.buildListToTree(allNodes);
//                facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//            } else
//
//
//                result.setCode(1);
//            result.setMsg("");
//            result.setContent(facilitiesSelect);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_LO_DE));
//            return result;
//        }
//    }
//
//    @RequestMapping("/maintain-detail")
//    public ModelAndView maintainDetail(HttpServletRequest request, HttpServletResponse response) {
//        String code;
//        try {
//            code = request.getParameter("code");
//        } catch (Exception ex) {
//            code = "";
//        }
//        return new ModelAndView("maintain/maintain_detail").addObject("code", code);
//    }
//
//    @RequestMapping("/maintain-export")
//    public ModelAndView ExportMaintain(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        List<MaintainData> maintainDataList;
//        try {
//            String searchStatus = request.getParameter("status");
//            String facilities = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            int pageSize = 50000;
//            int pageNumber = 1;
//            // 创建人
//            User user = authService.getLoginUser(request);
//            String account = user.getAccount();
//            String condition;
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "maintain");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("maintain_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("maintain_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//
//
//            if (facilities != null && facilities != "") {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilities, facilityList);
//                condition = " and r.facility_id in (" + facilities + subFacility + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = "  ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = " and r.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        //没有用户所属位置，则按个人权限查询
//                        condition = " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.maintain_account='" + account + "' )";
//                    }
//                } else {
//                    condition = " and (r.create_user_account='" + account + "' or r.audit_account='" + account + "' or r.maintain_account='" + account + "' )";
//                }
//            }
//
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(r.maintain_code) like upper('%" + keyWord + "%') or upper(dv.strname) like upper('%" + keyWord + "%') or upper(dv.strcode) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or r.maintain_account like '%" + keyWord + "%' )";
//            }
//
//            if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("0")) {
//                condition += " and (r.status = " + searchStatus + " ) ";
//            }
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.createtime >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.createtime <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            maintainDataList = maintainService.getMaintainList(schema_name, condition, pageSize, begin);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("maintains", maintainDataList);
//            map.put("selectOptionService", selectOptionService);
//            MaintainDataExportView excelView = new MaintainDataExportView();
//            return new ModelAndView(excelView, map);
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    @RequestMapping("/invalid-maintain")
//    public ResponseModel invalidRepair(HttpServletRequest request, HttpServletResponse response) {
//        String schema = authService.getCompany(request).getSchema_name();
//        String code = request.getParameter("code");
//        User user = AuthService.getLoginUser(request);
//        boolean invalidFlag = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema, user.getId(), "maintain");
//        if (userFunctionList != null) {
//            for (UserFunctionData functionData : userFunctionList) {
//                if (functionData.getFunctionName().equals("maintain_invalid")) {
//                    invalidFlag = true;
//                    break;
//                }
//            }
//        }
//        if (invalidFlag) {
//            if (maintainService.invalidMaintain(schema, code) > 0) {
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_CAN_MAIN_SU));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_CAN_MAIN_DE));
//            }
//        } else {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_AUTH));
//        }
//    }
//
//    @RequestMapping("/deal-maintain")
//    @Transactional //支持事务
//    public ResponseModel dealMaintain(HttpServletRequest request) {
//        Company company = AuthService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        ResponseModel result = new ResponseModel();
//        User user = AuthService.getLoginUser(request);
//        try {
//            String maintainCode = request.getParameter("maintainCode");
//            String deviceCode = request.getParameter("deviceCode");
//            String beforeImg = request.getParameter("beforeImg");
//            String maintainNote = request.getParameter("maintainNote");
//            String afterImg = request.getParameter("afterImg");
//            String itemList = request.getParameter("itemListWord");
//            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
//            String bomListJson = request.getParameter("bomListWord");
//            String beginTime = request.getParameter("beginTime");
//            String finishedTime = request.getParameter("finishedTime");
//
//            if (deviceCode == null || deviceCode.isEmpty()) {
//                result.setCode(ResultStatus.ERROR.getCode());
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_ASSET));
//                return result;
//            }
////            //license过期
//            Timestamp now = new Timestamp(System.currentTimeMillis());
////            Timestamp nationday = Timestamp.valueOf("2018-10-15 12:00:00");
////            if (now.after(nationday)) {
////                result.setCode(ResultStatus.ERROR.getCode());
////                result.setMsg("系统有误，请联系耕耘。");
////                return result;
////            }
//
//            //获取当前设备
//            Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, deviceCode);
//            if (assetModel == null || assetModel.get_id().isEmpty()) {
//                result.setCode(ResultStatus.ERROR.getCode());
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_ASSET_CODE));
//                return result;
//            }
//            MaintainData model = new MaintainData();
//            //生成新订单编号
//            if (maintainCode == null || maintainCode.isEmpty()) {
//                maintainCode = serialNumberService.generateMaxBusinessCode(schema_name, "maintain");
//                model.setReveiveTime(now);
//                model.setCreatetime(now);
//                model.setCreate_user_account(user.getAccount());
//            }
//            model.setMaintainCode(maintainCode);
//            model.setAssetId(assetModel.get_id());
//            model.setAssetType(assetModel.getAsset_type());
//            model.setFinishedTime(now);
//            if (StringUtils.isNotEmpty(beginTime)) {
//                model.setBeginTime(Timestamp.valueOf(beginTime));
//            }
//            if (StringUtils.isNotEmpty(finishedTime)) {
//                model.setFinishedTime(Timestamp.valueOf(finishedTime));
//            }
////            Timestamp tempDeadLine = Timestamp.valueOf(deadlineTime);
////            model.setDeadlineTime(tempDeadLine);
//            model.setBeforeImage(beforeImg);
//            model.setAfterImage(afterImg);
//            model.setFaultNumber(faultNumber);
//            //状态
//            int status = 80;   //默认待确认
//            String logOperation = selectOptionService.getLanguageInfo( LangConstant.SUBMIT);
//
//            model.setMaintainNote(maintainNote);
//            model.setStatus(status);
//            model.setFacilityId((int) assetModel.getIntsiteid());
//            model.setMaintainResult(itemList);
//            model.setMaintainAccount(user.getAccount());
//
//            //保存保养结果
//            int doCount = maintainService.SaveMaintainResult(schema_name, model);
//            if (doCount > 0) {
//                //保存支援人员
//                //SaveRepairWorkMan(schema_name, receive_account, help_account, repairCode);
//
//                //保存备件
//                //使用备件信息[{"facilityId":63,"stockCode":"000006","stockName":"kk","bomName":"123","bomModel":"123","useCount":"3","index":0},{"facilityId":56,"stockCode":"000002","stockName":"we","bomName":"ds2","bomModel":"dsd","useCount":"2","index":1}]
//                //先删除之前的
//                maintainService.deleteMaintainBom(schema_name, maintainCode);
//                if (bomListJson != null) {
//                    net.sf.json.JSONArray bomJsonList = net.sf.json.JSONArray.fromObject(bomListJson);
//                    if (bomJsonList != null && bomJsonList.size() > 0) {
//                        //再增加
//                        for (int i = 0; i < bomJsonList.size(); i++) {
//                            net.sf.json.JSONObject bomJson = bomJsonList.getJSONObject(i);
//                            MaintainBomData data = new MaintainBomData();
//                            data.setMaintainCode(maintainCode);
//                            data.setFacilityId(Integer.parseInt(bomJson.get("facilityId").toString()));
//                            data.setBomModel(bomJson.get("bomModel").toString());
//                            data.setBomCode(bomJson.get("bomCode").toString());
//                            data.setStockCode(bomJson.get("stockCode").toString());
//                            data.setUseCount(Integer.parseInt(bomJson.get("useCount").toString()));
//                            data.setFetchMan(user.getAccount());
//                            data.setHuojia("");
//
//                            maintainService.insertMaintainBom(schema_name, data);
//                        }
//                    }
//                }
//
//                if (company.getIs_open_sms()) {
//                    //提交时，进行短信发送，保存时，不发送短信
//                    // 获取操作人
//                    User accountInfo = userService.getUserByAccount(schema_name, user.getAccount());
//                    //发送短信给保养确认人
//                    List<User> userList = null;//userService.findUserByFunctionKey(schema_name, "maintain_audit", (int) assetModel.getIntsiteid());
//                    if (userList != null && !userList.isEmpty()) {
//                        String mobile = userList.get(0).getMobile();
//                        String receiveAccount = userList.get(0).getAccount();
//                        String content;
//                        if (Message.getProvider(schema_name).equalsIgnoreCase("yunda")) {
//                            String pattern = "{0}提交了设备：{1}，设备编码：{2}的保养结果，请尽快确认。";
//                            content = MessageFormat.format(pattern, accountInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
//                        } else {
//                            String pattern = "{0,number,#}::{1}::{2}::{3}";
//                            content = MessageFormat.format(pattern, qcloudsmsConfig.TEMPLATE_MAINTAIN_CONFIRM, accountInfo.getUsername(), assetModel.getStrname(), assetModel.getStrcode());
//                        }
//                        Message.beginMsg(schema_name, content, mobile, receiveAccount, "maintain", maintainCode, user.getAccount());
//                    }
//                }
//
//                result.setCode(ResultStatus.SUCCESS.getCode());
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MAINTENANCE_A)+ logOperation + selectOptionService.getLanguageInfo( LangConstant.MSG_SUCC_A));
//                result.setContent(maintainCode);
//                //记录历史
//                logService.AddLog(schema_name, "maintain", maintainCode, logOperation + selectOptionService.getLanguageInfo( LangConstant.MSG_MAINTAIN_DATA), user.getAccount());
//                return result;
//            } else {
//                result.setCode(ResultStatus.ERROR.getCode());
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MAINTENANCE_A) + logOperation + selectOptionService.getLanguageInfo( LangConstant.MSG_FAIL));
//                return result;
//            }
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(ResultStatus.ERROR.getCode());
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DO_ERR_ADMIN));
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "maintain_add", selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_MAIN_PRO) + ex.getMessage(), user.getAccount());
//            return result;
//        }
//    }
//
//
//    @RequestMapping("/domodify-maintain-deadline")
//    public ResponseModel updateMaintainDeadlineTime(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema = authService.getCompany(request).getSchema_name();
//            String maintainCode = request.getParameter("maintainCode");
//            String deadlineTime = request.getParameter("deadlineTime");
//            User user = AuthService.getLoginUser(request);
//            boolean successFlag = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema, user.getId(), "maintain");
//            if (userFunctionList != null) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    if (functionData.getFunctionName().equals("maintain_deadline")) {
//                        successFlag = true;
//                        break;
//                    }
//                }
//            }
//            if (successFlag) {
//                if (maintainService.updateMaintainDeadlineTime(schema, maintainCode, Timestamp.valueOf(deadlineTime + " 10:00:00")) > 0) {
//                    return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_SUCCESS));
//                } else {
//                    return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_ERROR));
//                }
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_MAIN_NO_AUTH));
//            }
//        } catch (Exception e) {
//            return ResponseModel.error(e.getMessage());
//        }
//    }
}
