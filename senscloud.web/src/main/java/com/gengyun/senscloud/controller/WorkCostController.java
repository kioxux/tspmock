package com.gengyun.senscloud.controller;

//import com.alibaba.fastjson.JSON;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
//@RequestMapping("/work_cost")
public class WorkCostController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    WorkCostService workCostService;
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @RequestMapping({"/", "/index"})
//    public ModelAndView selectStockData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        return new ModelAndView("work_cost/index");
//
//    }
//
//    //任务列表
//    @RequestMapping("/find_work_cost_employee_list")
//    public String queryWorkCostforCategory(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String keyWord = request.getParameter("keyWord");
//
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(a.username) like upper('%" + keyWord + "%') or upper(a.account) like upper('%" + keyWord + "%') )";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and b.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and b.occur_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = workCostService.getWorkCoustListforEmployee(schema_name, condition, pageSize, begin);
//            Map<String, Object>  totaldata = workCostService.getWorkCoustListforEmployeeCount(schema_name, condition);
//            result.put("rows", dataList);
//            result.put("total", totaldata.get("total"));
//            result.put("cost", totaldata.get("cost"));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //任务列表
//    @RequestMapping("/find_work_cost_customer_list")
//    public String queryWorkCostforCustomer(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String keyWord = request.getParameter("keyWord");
//
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(a.customer_code) like upper('%" + keyWord + "%') or upper(a.customer_name) like upper('%" + keyWord + "%') )";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and b.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and b.occur_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = workCostService.getWorkCoustListfoCustomer(schema_name, condition, pageSize, begin);
//            Map<String, Object>  totaldata = workCostService.getWorkCoustListforCustomerCount(schema_name, condition);
//            result.put("rows", dataList);
//            result.put("total", totaldata.get("total"));
//            result.put("cost", totaldata.get("cost"));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //任务列表
//    @RequestMapping("/find_work_cost_organization_list")
//    public String queryWorkCostforOrganization(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String keyWord = request.getParameter("keyWord");
//
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(fa.title) like upper('%" + keyWord + "%') or upper(fa.facilitycode) like upper('%" + keyWord + "%') )";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and b.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and b.occur_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = workCostService.getWorkCoustListforOrganization(schema_name, condition, pageSize, begin);
//            Map<String, Object>  totaldata = workCostService.getWorkCoustListforOrganizationCount(schema_name, condition);
//            result.put("rows", dataList);
//            result.put("total", totaldata.get("total"));
//            result.put("cost", totaldata.get("cost"));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    //任务列表
//    @RequestMapping("/find_work_cost_detail_list")
//    public String queryWorkCostforDetail(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String category_id = request.getParameter("category_id");
//        String customer_id = request.getParameter("customer_id");
//
//
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (category_id != null && !category_id.isEmpty()) {
//                condition += " and c.id ='" + category_id + "'";
//            }
//            if (customer_id != null && !customer_id.isEmpty()) {
//                condition += " and f.id ='" + customer_id + "'";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and w.occur_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = workCostService.getWorkCoustListByDetailforCustomer(schema_name, condition, pageSize, begin);
//            Map<String, Object>  totaldata = workCostService.getWorkCoustListDetailCountforCustomer(schema_name, condition);
//            result.put("rows", dataList);
//            result.put("total", totaldata.get("total"));
//            result.put("cost", totaldata.get("cost"));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//    @RequestMapping("/find_work_cost_detail_list_employee")
//    public String queryWorkCostforDetailWithEmployee(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String category_id = request.getParameter("category_id");
//        String account = request.getParameter("account");
//
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (category_id != null && !category_id.isEmpty()) {
//                condition += " and G.id ='" + category_id + "'";
//            }
//            if (account != null && !account.isEmpty()) {
//                condition += " and a.account ='" + account + "'";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and b.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and b.occur_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = workCostService.getWorkCoustListByDetailforEmployee(schema_name, condition, pageSize, begin);
//            Map<String, Object>  totaldata = workCostService.getWorkCoustListDetailCountforEmployee(schema_name, condition);
//            result.put("rows", dataList);
//            result.put("total", totaldata.get("total"));
//            result.put("cost", totaldata.get("cost"));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//    @RequestMapping("/find_work_cost_detail_list_organization")
//    public String queryWorkCostforDetailWithOrganization(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//
//        String facility_id = request.getParameter("facility_id");
//
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (facility_id != null && !facility_id.isEmpty()) {
//                condition += " and fa.id ='" + facility_id + "'";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and b.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and b.occur_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = workCostService.getWorkCoustListByDetailforOrganization(schema_name, condition, pageSize, begin);
//            Map<String, Object>  totaldata = workCostService.getWorkCoustListDetailCountforOrganization(schema_name, condition);
//            result.put("rows", dataList);
//            result.put("total", totaldata.get("total"));
//            result.put("cost", totaldata.get("cost"));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }

}