package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.AssetPositionModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.asset.AssetPositionService;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 设备位置
 */
@Api(tags = "设备位置")
@RestController
public class AssetPositionController {

    @Resource
    AssetPositionService assetPositionService;
    @Resource
    SystemConfigService systemConfigService;


    @ApiOperation(value = "获取设备位置模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetPositionListPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetPositionListPermission(MethodParam methodParam) {
        return ResponseModel.ok(assetPositionService.getAssetPositionListPermission(methodParam));
    }

    @ApiOperation(value = "新增设备位置", notes = ResponseConstant.RSP_DESC_ADD_ASSET_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_name", "parent_id", "parent_id", "layer_path", "layer_path", "position_type_id", "file_id"})
    @Transactional
    @RequestMapping(value = "/addAssetPosition", method = RequestMethod.POST)
    public ResponseModel addAssetPosition(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel AssetPositionModel) {
        assetPositionService.newAssetPosition(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑设备位置", notes = ResponseConstant.RSP_DESC_EDIT_ASSET_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_code", "position_name", "parent_id", "parent_id", "scada_config", "parent_id", "layer_path", "file_id"})
    @Transactional
    @RequestMapping(value = "/editAssetPosition", method = RequestMethod.POST)
    public ResponseModel editAssetPosition(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel AssetPositionModel) {
        assetPositionService.modifyAssetPosition(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除设备位置", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_code"})
    @Transactional
    @RequestMapping(value = "/removeAssetPosition", method = RequestMethod.POST)
    public ResponseModel removeAssetPosition(MethodParam methodParam, AssetPositionModel AssetPositionModel) {
        assetPositionService.cutAssetPosition(methodParam, AssetPositionModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "设备位置详情", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_POSITION_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_code"})
    @RequestMapping(value = "/searchAssetPositionInfo", method = RequestMethod.POST)
    public ResponseModel searchAssetPositionList(MethodParam methodParam, AssetPositionModel AssetPositionModel) {
        return ResponseModel.ok(assetPositionService.getAssetPositionInfo(methodParam, AssetPositionModel));
    }

    @ApiOperation(value = "分页查询设备位置列表", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_POSITION_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "positionTypeIdSearch", "keywordSearch"})
    @RequestMapping(value = "/searchAssetPositionList", method = RequestMethod.POST)
    public ResponseModel searchAssetPositionList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel AssetPositionModel) {
        return ResponseModel.ok(assetPositionService.getAssetPositionList(methodParam, paramMap));
    }

    @ApiOperation(value = "根据设备位置获取上级的平面图", notes = ResponseConstant.RSP_DESC_SEARCH_SUPERIOR_POSITION_PLAN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_code"})
    @RequestMapping(value = "/searchSuperiorPositionPlan", method = RequestMethod.POST)
    public ResponseModel searchSuperiorPositionPlan(MethodParam methodParam, AssetPositionModel AssetPositionModel) {
        return ResponseModel.ok(assetPositionService.getSuperiorPositionPlan(methodParam, AssetPositionModel));
    }

    @ApiOperation(value = "展示选择设备位置二维码图片", notes = ResponseConstant.RSP_DESC_SHOW_ASSET_POSITION_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/showAssetPositionQRCode", method = RequestMethod.GET)
    public void showAssetPositionQRCode(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        assetPositionService.doShowAssetPositionQRCode(methodParam);
    }

    @ApiOperation(value = "导出选中设备二维码图片", notes = ResponseConstant.RSP_DESC_EXPORT_ASSET_POSITION_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/exportAssetPositionQRCode", method = RequestMethod.POST)
    public void exportAssetPositionQRCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        assetPositionService.doExportAssetPositionQRCode(methodParam, asParam, paramMap);
    }

    @ApiOperation(value = "获取全部设备导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_ASSET_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "positionTypeIdSearch"})
    @RequestMapping(value = "/exportAllAssetPositionQRCode", method = RequestMethod.POST)
    public void exportAllAssetPositionQRCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        assetPositionService.doExportAssetPositionQRCode(methodParam, asParam, paramMap);
    }

    @ApiOperation(value = "查询是否需要scada配置列", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_POSITION_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchIsNeedScadaConfig", method = RequestMethod.POST)
    public ResponseModel searchIsNeedScadaConfig(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel AssetPositionModel) {
        String guacamole_ip = systemConfigService.getSysCfgValueByCfgName(methodParam.getSchemaName(), "is_need_scada");
        return ResponseModel.ok(guacamole_ip);
    }

    @ApiOperation(value = "获取该设备位置可选择的任务模板列表", notes = ResponseConstant.RSP_DESC_SEARCH_CHOOSE_TASK_TEMPLATE_LIST_BY_POSITION_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "position_code", "work_type_id"})
    @RequestMapping(value = "/searchChooseTaskTemplateListByPositionCode", method = RequestMethod.POST)
    public ResponseModel searchChooseTaskTemplateListByPositionCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        return ResponseModel.ok(assetPositionService.getChooseTaskTemplateListByPositionCode(methodParam, paramMap));
    }

    @ApiOperation(value = "获取该设备位置已选择的任务模板列表", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_TEMPLATE_LIST_BY_POSITION_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "position_code"})
    @RequestMapping(value = "/searchTaskTemplateListByPositionCode", method = RequestMethod.POST)
    public ResponseModel searchTaskTemplateListByPositionCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        return ResponseModel.ok(assetPositionService.getTaskTemplateListByPositionCode(methodParam, paramMap));
    }

    @ApiOperation(value = "新增设备位置和任务模板关联", notes = ResponseConstant.RSP_DESC_ADD_ASSET_POSITION_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_type_id", "position_code", "template_codes"})
    @RequestMapping(value = "/addAssetPositionTemplate", method = RequestMethod.POST)
    public ResponseModel addAssetPositionTemplate(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        assetPositionService.newPositionCodeTemplate(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除设备位置和任务模板关联", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_POSITION_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_type_id", "template_codes", "position_code"})
    @RequestMapping(value = "/removeAssetPositionTemplate", method = RequestMethod.POST)
    public ResponseModel removeAssetPositionTemplate(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        assetPositionService.cutPositionCodeTemplate(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "根据位置编码和工单类型获取任务模板列表", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_TEMPLATE_BY_POSITION_CODE_AND_WORK_TYPE_ID)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_type_id", "position_code"})
    @RequestMapping(value = "/searchTaskTemplateByPositionCodeAndWorkTypeId", method = RequestMethod.POST)
    public ResponseModel searchTaskTemplateByPositionCodeAndWorkTypeId(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        return ResponseModel.ok(assetPositionService.getTaskTemplateByPositionCodeAndWorkTypeId(methodParam, paramMap));
    }

    @ApiOperation(value = "根据位置编码获取该位置内的设备的故障状态", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_REPAIR_STATUS_BY_POSITION_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_code"})
    @RequestMapping(value = "/searchAssetRepairStatusByPositionCode", method = RequestMethod.POST)
    public ResponseModel searchAssetRepairStatusByPositionCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        return ResponseModel.ok(assetPositionService.getAssetRepairStatusByPositionCode(methodParam, paramMap));
    }

    @ApiOperation(value = "获取所有位置的设备故障数", notes = ResponseConstant.RSP_DESC_SEARCH_ALL_POSITION_CODE_ASSET_REPAIR_STATUS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAllPositionCodeAssetRepairStatusBy", method = RequestMethod.POST)
    public ResponseModel searchAllPositionCodeAssetRepairStatusBy(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetPositionModel asParam) {
        return ResponseModel.ok(assetPositionService.getAllPositionCodeAssetRepairStatusBy(methodParam, paramMap));
    }
}
