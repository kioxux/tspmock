package com.gengyun.senscloud.controller;


//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.FacilitiesResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.view.InspectionPdfView;
//import org.apache.commons.collections.map.HashedMap;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
public class InspectionConfigController {

//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    DashboardConfigService configService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    InspectionService inspectionService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//
//    @RequestMapping("/inspection_config")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//
//        //按当前登录用户，设备管理的权限，判断可看那些位置
//        Boolean isAllFacility = false;
//        List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "inspection_config");
//        if (userFunctionList != null || !userFunctionList.isEmpty()) {
//            for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
//                if (functionData.getFunctionName().equals("inspectionarea_all_facility")) {
//                    isAllFacility = true;
//                    break;
//                }
//            }
//        }
//
//        //获取权限树
//        String facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//
//        return new ModelAndView("inspection_config").
//                addObject("user_facility_id", authService.getCompany(request).getFacilityId()).
//                addObject("facilitiesSelect", facilitiesSelect);
//    }
//
//    /**
//     * 获得所有的位置
//     */
//    @RequestMapping("/ins_find-all-facilities")
//    public ResponseModel queryAllFacilities(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "inspection_config");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("inspectionarea_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    }
//                }
//            }
//
//            if (isAllFacility) {
//                List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//                return ResponseModel.ok(facilitiesList);
//            } else {
//                //定义返回
//                List<Facility> result = new ArrayList<>();
//                //获取全部的位置
//                List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//                //获取用户所在的位置，可能多个
//                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        Facility facilityd = new Facility();
//                        facilityd.setId(facilityList.get(key).getId());
//                        facilityd.setTitle(facilityList.get(key).getTitle());
//                        facilityd.setParentId(facilityList.get(key).getParentId());
//                        result.add(facilityd);
//                    }
//                }
//                return ResponseModel.ok(result);
//            }
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    /*//递归生成位置列表
//    private List<Facility> GenerateSubFacilityList(List<Facility> list, long facilityId) {
//        List<Facility> result = new ArrayList<>();
//        if (list != null && !list.isEmpty()) {
//            for (Facility item : list) {
//                if (item.getId() == facilityId) {
//                    result.add(item);
//                }
//                if (item.getparentId() == facilityId) {
//                    result.addAll(GenerateSubFacilityList(list, item.getId()));
//                }
//            }
//        }
//        return result;
//    }*/
//
//    /**
//     * 获得选择位置的信息，以及位置所有的设备
//     */
//    @RequestMapping("/ins_find_facility_and_asset")
//    public ResponseModel queryFacilityAndAssetList(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            Integer id = Integer.parseInt(request.getParameter("site_id"));
//            FacilitiesResult facilities = facilitiesService.FacilitiesListById(schema, id);
//            return ResponseModel.ok(facilities);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    /**
//     * 插入巡更点
//     */
//    @RequestMapping("/add_insp_area")
//    public ResponseModel addInspectionArea(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        User loginUser = authService.getLoginUser(request);
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            String areaname = request.getParameter("areaname");
//            String areacode = serialNumberService.generateMaxBusinessCode(schema, "inspection_area");
//            ;
//            String remark = request.getParameter("remark");
//            Boolean isuse = Boolean.parseBoolean(request.getParameter("isuse"));
//            String Location = request.getParameter("Location");
//            Integer facilityid = Integer.parseInt(request.getParameter("facilityid"));
//            Timestamp createtime = new Timestamp((new Date()).getTime());
//            double x = 0;
//            double y = 0;
//            if (Location != null && Location != "") {
//                x = Double.parseDouble(Location.split(",")[0].replace("(", "").toString());
//                y = Double.parseDouble(Location.split(",")[1].replace(")", "").toString());
//            }
//            //判断xy或areacode是否存在
//            InspectionAreaData inspectionAreaDatabyxy = inspectionService.InspectionAreaDatabyxy(schema, facilityid, x, y);
//            InspectionAreaData inspectionAreaDatabyarea = inspectionService.InspectionAreaDatabyarea(schema, areacode);
//
//            if (inspectionAreaDatabyxy != null && inspectionAreaDatabyxy.getArea_code() != null) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_HAVE_INS_SITE));
//            }
//            if (inspectionAreaDatabyarea != null && inspectionAreaDatabyarea.getArea_code() != null) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_HAVE_INS_SITE_CODE));
//            }
//
//
//            InspectionAreaData inspectionAreaData = new InspectionAreaData();
//            inspectionAreaData.setSchema_name(schema);
//            inspectionAreaData.setArea_code(areacode);
//            inspectionAreaData.setArea_name(areaname);
//            inspectionAreaData.setRemark(remark);
//            inspectionAreaData.setIs_use(isuse);
//            inspectionAreaData.setLocation_xy(Location);
//            inspectionAreaData.setFacility_id(facilityid);
//            inspectionAreaData.setCreatetime(createtime);
//            inspectionAreaData.setCreate_user_account(loginUser.getAccount());
//            int inserresult = inspectionService.insertInspectionAreaData(inspectionAreaData);
//            if (inserresult > 0) {
//                //增加日志
//                logService.AddLog(schema, areacode, areaname, selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_INS_CODE) + areacode, loginUser.getAccount());
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_OK));
//            } else
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FA));
//
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    /**
//     * 根据设备地图查询所有巡更点
//     */
//    @RequestMapping("/get_insparea_list")
//    public ResponseModel queryInspecctionAreaList(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            Integer facilityid = Integer.parseInt(request.getParameter("facilityid"));
//            List<InspectionAreaData> inspectionAreaData = inspectionService.InspectionAreaDatabyfac(schema, facilityid);
//            return ResponseModel.ok(inspectionAreaData);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_SEACH);
//            return ResponseModel.error(tmpMsg);
//        }
//    }
//
//
//    /**
//     * 根据设备地图查询所有巡更点
//     */
//    @RequestMapping("/get_insparea_xy")
//    public ResponseModel queryInspecctionAreaxy(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            String Location = request.getParameter("Location");
//            Integer facilityid = Integer.parseInt(request.getParameter("facilityid"));
//            double x = 0;
//            double y = 0;
//            if (Location != null && Location != "") {
//                x = Double.parseDouble(Location.split(",")[0].replace("(", "").toString());
//                y = Double.parseDouble(Location.split(",")[1].replace(")", "").toString());
//            }
//            //判断xy或areacode是否存在
//            InspectionAreaData inspectionAreaDatabyxy = inspectionService.InspectionAreaDatabyxy(schema, facilityid, x, y);
//            return ResponseModel.ok(inspectionAreaDatabyxy);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_READ_ERROR));
//        }
//    }
//
//    /**
//     * 根据设备地图查询所有巡更点
//     */
//    @RequestMapping("/update_inspection_area")
//    public ResponseModel updateInspecctionArea(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        User loginUser = authService.getLoginUser(request);
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//
//            String areaname = request.getParameter("areaname");
//            String areacode = request.getParameter("areacode");
//            if (RegexUtil.isNull(areacode)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.TEXT_K));
//            }
//            inspectionService.checkUserPermission(schema, request, areacode); // 权限控制
//
//            String remark = request.getParameter("remark");
//            Boolean isuse = Boolean.parseBoolean(request.getParameter("isuse"));
//            InspectionAreaData inspectionAreaData = new InspectionAreaData();
//            inspectionAreaData.setSchema_name(schema);
//            inspectionAreaData.setArea_code(areacode);
//            inspectionAreaData.setArea_name(areaname);
//            inspectionAreaData.setRemark(remark);
//            inspectionAreaData.setIs_use(isuse);
//            int inserresult = inspectionService.updateInspectionAreaData(inspectionAreaData);
//            if (inserresult > 0) {
//                //增加日志
//                logService.AddLog(schema, areacode, areaname, selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_INS_CODE) + areacode, loginUser.getAccount());
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_OK));
//            } else
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FA));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_READ_ERROR));
//        }
//    }
//
//
//    /**
//     * 删除巡更点
//     */
//    @RequestMapping("/delete_insparea_xy")
//    public ResponseModel deleteInspecctionAreaxy(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        User loginUser = authService.getLoginUser(request);
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            String areacode = request.getParameter("areacode");
//            if (RegexUtil.isNull(areacode)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.TEXT_K));
//            }
//            inspectionService.checkUserPermission(schema, request, areacode); // 权限控制
//            Integer facilityid = Integer.parseInt(request.getParameter("facilityid"));
//
//            int inserresult = inspectionService.DeleteionAreaDatabyfac(schema, facilityid, areacode);
//            if (inserresult > 0) {
//                //增加日志
//                logService.AddLog(schema, areacode, areacode, selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_INS_CODE) + areacode, loginUser.getAccount());
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));
//            } else
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_ERR));
//        }
//    }
//
//    /**
//     * 移动巡更点
//     */
//    @RequestMapping("/move_insparea_xy")
//    public ResponseModel updateInspecctionAreaxy(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//        User loginUser  = authService.getLoginUser(request);
//        try {
//            if (company == null) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CC);
//                return ResponseModel.error(tmpMsg);
//            }
//            String Locationxy = request.getParameter("Locationxy");
//            Integer facilityid = Integer.parseInt(request.getParameter("facilityid"));
//            String Location = request.getParameter("Location");
//            double x = 0;
//            double y = 0;
//            if (Location != null && Location != "") {
//                x = Double.parseDouble(Location.split(",")[0].replace("(", "").toString());
//                y = Double.parseDouble(Location.split(",")[1].replace(")", "").toString());
//            }
//            int inserresult = inspectionService.updateInspectionAreaxy(schema, facilityid, x, y, Locationxy);
//            if (inserresult > 0) {
//
//                //增加日志
//                logService.AddLog(schema, Location, Locationxy, selectOptionService.getLanguageInfo( LangConstant.MSG_REMOVE_OLD_XY) + Location, loginUser.getAccount());
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_REMOVE_OK));
//            } else
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_REMOVE_FA));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_REMOVE_ERR));
//        }
//    }
//
//
//    //批量生成巡更点的二维码
//    @RequestMapping("/Inspection/batchInspecctionQRCode")
//    public ModelAndView batchInspecctionQRCode(HttpServletRequest request, HttpServletResponse response) {
//        ServletOutputStream stream = null;
//        try {
//            Integer facility_id = Integer.parseInt(request.getParameter("ids"));
//            String schema_name = authService.getCompany(request).getSchema_name();
//            if (facility_id != null && facility_id > 0) {
//                List<InspectionAreaData> inspectionAreaData = inspectionService.InspectionAreaDatabyfac(schema_name, facility_id);
//                Map<String, Object> map = new HashedMap();
//                map.put("inspectionAreaData", inspectionAreaData);
//                map.put("selectOptionService", selectOptionService);
//                // map.put("name", "魅力城市");
//                InspectionPdfView excelView = new InspectionPdfView();
//                return new ModelAndView(excelView, map);
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            if (stream != null) {
//                try {
//                    stream.flush();
//                    stream.close();
//                } catch (IOException ex) {
//                }
//            }
//        }
//        return null;
//    }
//
//    /**
//     * 功能：查询维修计划的巡检点对象查询
//     */
//    @RequestMapping("/get_insparea_list_plan")
//    public String queryByPlanInspecctionAreaList(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            Company company = authService.getCompany(request);
//            if (company != null) {
//                String schema = SensConstant.SCHEMA_PREFIX + company.getId();
//                String facilityid = request.getParameter("facilityid");
//                int pageSize = 1000;
//                int pageNumber = 1;
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                String condition = "";
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "plan_schedule");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("plan_schedule_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("plan_schedule_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }if (facilityid != null && facilityid != "") {
//                    List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                    //加上位置的子位置
//                    String subFacility = facilitiesService.getSubFacilityIds(facilityid, facilityList);
//                    condition = " and i.facility_id in (" + facilityid + subFacility + ") ";
//                }else {
//                    if (isAllFacility) {
//                        condition = "  ";
//                    } else if (isSelfFacility) {
//                        LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                        String facilityIds = "";
//                        if (facilityList != null && !facilityList.isEmpty()) {
//                            for (String key : facilityList.keySet()) {
//                                facilityIds += facilityList.get(key).getId() + ",";
//                            }
//                        }
//                        if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                            facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                            condition = " and i.facility_id in (" + facilityIds + ") ";
//                        }
//                    }
//                }
//                int begin = pageSize * (pageNumber - 1);
//                List<InspectionAreaData> inspectionAreaData = inspectionService.queryByPlanInspecctionAreaList(schema, condition, pageSize, begin);
//                result.put("rows", inspectionAreaData);
//                result.put("total", inspectionAreaData.size());
//            }
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
}
