package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.service.system.CommonUtilService;
//import com.gengyun.senscloud.service.MessageService;
//import com.gengyun.senscloud.service.system.WxMsgService;
//import com.gengyun.senscloud.service.system.impl.WxMsgServiceImpl;
//import com.gengyun.senscloud.util.Constants;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.Map;
//
///**
// * 接口测试用
// * User: sps
// * Date: 2018/11/20
// * Time: 上午11:20
// */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/check_method")
public class CheckMethodController {
//    private static final Logger logger = LoggerFactory.getLogger(CheckMethodController.class);
//    @Autowired
//    CommonUtilService commonUtilService;
//    @Autowired
//    MessageService messageService;
//    @Autowired
//    WxMsgService wxMsgService;
//
//    /**
//     * 获取微信Token
//     *
//     * @return
//     */
//    @RequestMapping("/checkWxAccessToken")
//    public String checkWxAccessToken() {
//        try {
//            Map<String, Object> dbInfo = commonUtilService.doGetDbBaseInfo();
//            String schemaName = (String) dbInfo.get("schemaName");
//            String token = wxMsgService.checkWxAccessToken(schemaName);
//            return token;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "";
//        }
//    }
//
//    @RequestMapping(value="/routerToMyPage.html",method= RequestMethod.GET)
//    public void redirectToMyPage(HttpServletRequest request, HttpServletResponse response){
//        StringBuffer sb = new StringBuffer();
//        StringBuffer encodeUrl = new StringBuffer(300);
//        //公众号中配置的回调域名（网页授权回调域名）
//        String doname = "";
//        String root = request.getContextPath();
//        String appId = "";
//
//        sb.append("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
//        sb.append(appId);
//        String url = "";
//        try {
//            //对重定向url进行编码，官方文档要求
//            encodeUrl.append(doname).append(root).append("/my/myPage.html");
////          url = URLEncoder.encode(encodeUrl.toString(), "utf-8");
//            url = URLEncoder.encode("https://domain/xbdWXBG(项目名称)/my/myPage.html", "utf-8");
//            sb.append("&redirect_uri=").append(url);
//            //网页授权的静默授权snsapi_base
//            sb.append("&response_type=code&scope=snsapi_base&state=123#wechat_redirect");
//            response.sendRedirect(sb.toString());
//        } catch (UnsupportedEncodingException e) {
//            logger.error("重定向url编码失败：>>" + e.getMessage());
//            e.printStackTrace();
//        } catch (Exception e) {
//            logger.error("response重定向失败：>>" + e.getMessage());
//            e.printStackTrace();
//        }
//    }
}
