package com.gengyun.senscloud.controller.pollute;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.CheckMeterModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.pollute.CheckMeterService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "抄表管理")
@RestController
public class CheckMeterController {

    @Resource
    CheckMeterService checkMeterService;

    @Resource
    LogsService logsService;

    @ApiOperation(value = "查询抄表月份统计列表", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_MONTH_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "begin_date", "end_date"})
    @RequestMapping(value = "/searchCheckMeterMonthList", method = RequestMethod.POST)
    public ResponseModel searchCheckMeterMonthList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findCheckMeterMonthList(methodParam, paramMap));
    }

    @ApiOperation(value = "查询排污记录列表", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "title", "inner_code", "status", "begin_date", "end_date", "exceed", "unusual", "exceed"})
    @RequestMapping(value = "/searchCheckMeterList", method = RequestMethod.POST)
    public ResponseModel searchCheckMeterList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findCheckMeterList(methodParam, paramMap));
    }

    @ApiOperation(value = "客户排污统计信息", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_STATISTICS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "begin_date", "end_date"})
    @RequestMapping(value = "/searchCheckMeterStatistics", method = RequestMethod.POST)
    public ResponseModel searchCheckMeterStatistics(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findCheckMeterStatistics(methodParam, paramMap));
    }


    @ApiOperation(value = "查询设备排污记录列表", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_CHECK_METER_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "asset_id", "begin_date", "end_date"})
    @RequestMapping(value = "/searchAssetCheckMeterList", method = RequestMethod.POST)
    public ResponseModel searchAssetCheckMeterList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findAssetCheckMeterList(methodParam, paramMap));
    }

    @ApiOperation(value = "查询排污明细", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_DETAIL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchCheckMeterDetailInfo", method = RequestMethod.POST)
    public ResponseModel searchCheckMeterDetailInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findCheckMeterDetailInfo(methodParam, paramMap));
    }


    @ApiOperation(value = "设备最近的抄表记录", notes = ResponseConstant.RSP_DESC_ADD_CHECK_METER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "asset_id"})
    @RequestMapping(value = "/searchLastCheckMeter", method = RequestMethod.POST)
    public ResponseModel searchLastCheckMeter(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findLastCheckMeter(methodParam, paramMap));

    }

    @ApiOperation(value = "手动抄表", notes = ResponseConstant.RSP_DESC_ADD_CHECK_METER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "facility_id", "check_time", "asset_id", "indication", "last_indication"})
    @Transactional //支持事务
    @RequestMapping(value = "/addCheckMeter", method = RequestMethod.POST)
    public ResponseModel addCheckMeter(MethodParam methodParam, CheckMeterModel model) {
        checkMeterService.newCheckMeter(methodParam, model);
        return ResponseModel.okMsgForOperate();
    }


    @ApiOperation(value = "抄表日历", notes = ResponseConstant.RSP_DESC_CHECK_METER_CALENDAR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "begin_date", "end_date"})
    @RequestMapping(value = "/searchCheckMeterCalendar", method = RequestMethod.POST)
    public ResponseModel searchCheckMeterCalendar(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") CheckMeterModel model) {
        return ResponseModel.ok(checkMeterService.findCheckMeterCalendar(methodParam, paramMap));
    }


    @ApiOperation(value = "编辑抄表记录", notes = ResponseConstant.RSP_DESC_EDIT_CHECK_METER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "final_flow", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/editCheckMeter", method = RequestMethod.POST)
    public ResponseModel editCheckMeter(MethodParam methodParam, CheckMeterModel model) {
        checkMeterService.modifyCheckMeter(methodParam, model);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取全部抄表记录导出", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_CHECK_METER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "statusSearch", "keywordSearch", "positionsSearch",
            "assetTypesSearch", "assetModelSearch", "runStatusSearch", "customerSearch", "levelSearch", "startDateSearch", "endDateSearch",
            "warrantiesTypeSearch", "facilitiesSearch", "orgTypeSearch", "duplicateAssetSearch"})
    @RequestMapping(value = "/getExportAllCheckMeter", method = RequestMethod.POST)
    public ResponseModel getExportAllAssetCode(MethodParam methodParam, CheckMeterModel model) {
        return ResponseModel.okExport(checkMeterService.getExportCheckMeter(methodParam, model));
    }

    @ApiOperation(value = "获得抄表记录所有的日志", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getCheckMeterLogList", method = RequestMethod.POST)
    public ResponseModel getCheckMeterLogList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(logsService.getLogByLogType(methodParam, SensConstant.BUSINESS_NO_6003, paramMap));
    }

    @ApiOperation(value = "自动抄表设置", notes = ResponseConstant.RSP_DESC_AUTO_POLLUTE_FEE_SETING)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional
    @RequestMapping(value = "/autoCheckMeterSeting", method = RequestMethod.POST)
    public ResponseModel autoCheckMeterSeting(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        checkMeterService.autoCheckMeterSeting(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "自动抄表查询", notes = ResponseConstant.RSP_DESC_AUTO_POLLUTE_FEE_SEARCH)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "group_title"})
    @RequestMapping(value = "/autoCheckMeterSearch", method = RequestMethod.POST)
    public ResponseModel searchCheckMeterSetingInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(checkMeterService.findCheckMeterInfo(methodParam, paramMap));
    }
}
