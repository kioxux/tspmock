package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.AssetCategoryModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.asset.AssetCategoryService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 设备类型模块
 * User: sps
 * Date: 2019/04/15
 * Time: 上午11:20
 */
@Api(tags = "设备类型管理")
@RestController
public class AssetCategoryController {
    @Resource
    AssetCategoryService assetCategoryService;

    @ApiOperation(value = "获取设备类型模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_CATEGORY_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetCategoryListPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetCategoryListPermission(MethodParam methodParam) {
        return ResponseModel.ok(assetCategoryService.getAssetCategoryListPermission(methodParam));
    }

    @ApiOperation(value = "获取设备类型列表", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_CATEGORY_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "keywordSearch", "pageSize", "pageNumber", "isUseSearch"})
    @RequestMapping(value = "/searchAssetCategoryList", method = RequestMethod.POST)
    public ResponseModel searchAssetCategoryList(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(assetCategoryService.getAssetCategoryList(methodParam, assetCategoryModel));
    }

    @ApiOperation(value = "获取出设备类型列表", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_CATEGORY_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token"})
    @RequestMapping(value = "/searchAssetCategoryLists", method = RequestMethod.POST)
    public ResponseModel searchAssetCategoryLists(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        return ResponseModel.ok(assetCategoryService.searchAssetCategoryLists(methodParam, assetCategoryModel));
    }

    @ApiOperation(value = "设备类型自定义字段克隆", notes = ResponseConstant.RSP_DESC_CLONE_ASSET_CATEGORY_FIELDS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "asset_category_id", "clone_asset_category_id"})
    @RequestMapping(value = "/cloneAssetCategoryFields", method = RequestMethod.POST)
    public ResponseModel cloneAssetCategoryFields(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        assetCategoryService.cloneAssetCategoryFields(methodParam, assetCategoryModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "新增设备类型", notes = ResponseConstant.RSP_DESC_ADD_ASSET_CATEGORY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "category_name", "data_order",
                    "is_use", "parent_id", "category_code", "fields", "type"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetCategory", method = RequestMethod.POST)
    public ResponseModel addAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        assetCategoryService.newAssetCategory(methodParam, assetCategoryModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑设备类型", notes = ResponseConstant.RSP_DESC_EDIT_ASSET_CATEGORY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "category_name", "data_order",
                    "is_use", "category_code", "fields", "type"})
    @Transactional //支持事务
    @RequestMapping(value = "/editAssetCategory", method = RequestMethod.POST)
    public ResponseModel editAssetCategory(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        assetCategoryService.modifyAssetCategory(methodParam, assetCategoryModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除设备类型", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_CATEGORY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "asset_category_id"})
    @RequestMapping(value = "/removeAssetCategory", method = RequestMethod.POST)
    @Transactional
    public ResponseModel removeAssetCategoryById(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        assetCategoryService.cutAssetCategory(methodParam, assetCategoryModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取设备类型详情", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_CATEGORY_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "asset_category_id"})
    @RequestMapping(value = "/searchAssetCategoryInfo", method = RequestMethod.POST)
    public ResponseModel searchAssetCategoryInfo(MethodParam methodParam, AssetCategoryModel assetCategoryModel) {
        return ResponseModel.ok(assetCategoryService.getAssetCategoryInfo(methodParam, assetCategoryModel));
    }

}
