package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.LogsData;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.system.LogsService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
//@RestController
public class UnionLogController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 获得资产所有的日志
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/union_log_list")
//    public ModelAndView FindALLAssetLogs(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_id = request.getParameter("asset_id");
//
//            List<LogsData> dataList = logService.findAllLogsList(schema_name, "asset", asset_id);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//
//            List<User> userList = userService.findAll(schema_name);
//            return new ModelAndView("UnionLogList").addObject("result", result).addObject("userList", userList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_ERR_MAN) + ex.getMessage());//获取资产维保人员时出现错误：
//            return new ModelAndView("UnionLogList").addObject("result", result);
//        }
//    }

}
