package com.gengyun.senscloud.controller.pollute;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.pollute.MeterService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "抄表计量")
@RestController
public class MeterController {
    @Resource
    MeterService meterService;
    @Resource
    LogsService logsService;

    @ApiOperation(value = "获得抄表模块所有的日志", notes = ResponseConstant.RSP_DESC_SEARCH_METER_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getMeterLogList", method = RequestMethod.POST)
    public ResponseModel getMeterLogList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(logsService.getLogByLogType(methodParam, SensConstant.BUSINESS_NO_6003, paramMap));
    }

    @ApiOperation(value = "抄表日历", notes = ResponseConstant.RSP_DESC_METER_CALENDAR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "begin_date", "end_date"})
    @RequestMapping(value = "/searchMeterCalendar", method = RequestMethod.POST)
    public ResponseModel searchMeterCalendar(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.findMeterCalendar(methodParam, paramMap));
    }

    @ApiOperation(value = "抄表计量统计数据", notes = ResponseConstant.RSP_DESC_SEARCH_METER_STATISTICS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "begin_date", "end_date"})
    @RequestMapping(value = "/searchMeterStatistics", method = RequestMethod.POST)
    public ResponseModel searchMeterStatistics(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.findMeterStatistics(methodParam, paramMap));
    }

    @ApiOperation(value = "查询抄表记录", notes = ResponseConstant.RSP_DESC_SEARCH_METER_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "title", "inner_code", "meter_status", "begin_date", "end_date"})
    @RequestMapping(value = "/searchMeterList", method = RequestMethod.POST)
    public ResponseModel searchMeterList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(meterService.findMeterList(methodParam, paramMap));
    }

    @ApiOperation(value = "根据客户查询排污口", notes = ResponseConstant.RSP_DESC_SEARCH_METER_ASSERT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token","facility_id"})
    @RequestMapping(value = "/searchMeterAssertList", method = RequestMethod.POST)
    public ResponseModel searchMeterAssertList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.findMeterAssertList(methodParam, paramMap));
    }

    @ApiOperation(value = "根据客户和排污口查询水质水量信息", notes = ResponseConstant.RSP_DESC_SEARCH_METER_ITEM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token","facility_id","asset_id"})
    @RequestMapping(value = "/searchMeterItemList", method = RequestMethod.POST)
    public ResponseModel searchMeterItemList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.findMeterItemList(methodParam, paramMap));
    }

    @ApiOperation(value = "根据客户和排污口查询检测记录", notes = ResponseConstant.RSP_DESC_SEARCH_METER_RECORD_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token","facility_id","asset_id"})
    @RequestMapping(value = "/searchMeterRecordList", method = RequestMethod.POST)
    public ResponseModel searchMeterRecordList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.findMeterRecordList(methodParam, paramMap));
    }

    @ApiOperation(value = "编辑水质水量记录", notes = ResponseConstant.RSP_DESC_EDIT_METER_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "indication", "isReSet"})
    @Transactional //支持事务
    @RequestMapping(value = "/editMeterItem", method = RequestMethod.POST)
    public ResponseModel editMeterItem(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        meterService.modifyMeterItem(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除检测记录", notes = ResponseConstant.RSP_DESC_DELETE_METER_RECORD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/deleteMeterRecord", method = RequestMethod.POST)
    public ResponseModel deleteMeterRecord(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        meterService.removeMeterRecord(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "触发估抄", notes = ResponseConstant.RSP_DESC_SEARCH_GUESS_METER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/guessMeter", method = RequestMethod.POST)
    public ResponseModel guessMeter(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.findGuessMeter(methodParam, paramMap));
    }

    @ApiOperation(value = "估抄", notes = ResponseConstant.RSP_DESC_GUESS_METER_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "indication"})
    @Transactional //支持事务
    @RequestMapping(value = "/editGuessMeterItem", method = RequestMethod.POST)
    public ResponseModel editGuessMeterItem(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        meterService.modifyGuessMeterItem(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "排污记录查询", notes = ResponseConstant.RSP_DESC_SEARCH_METER_SEWAGE_RECORDS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "facility_id", "asset_id"})
    @RequestMapping(value = "/searchMeterSewageRecords", method = RequestMethod.POST)
    public ResponseModel searchMeterSewageRecords(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(meterService.findMeterSewageRecords(methodParam, paramMap));
    }

    @ApiOperation(value = "排污记录图表", notes = ResponseConstant.RSP_DESC_METER_SEWAGE_RECORDS_CHART)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "facility_id", "asset_id"})
    @RequestMapping(value = "/meterSewageRecordsChart", method = RequestMethod.POST)
    public ResponseModel meterSewageRecordsChart(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(meterService.meterSewageRecordsChart(methodParam, paramMap));
    }
}
