package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.MaintainSettingsResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Message;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.Date;
//import java.util.List;
//
//@RestController
public class MaintenanceSettingsConfigController {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    LogsService logService;
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    private AssetCategoryService assetCategoryService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @RequestMapping("/maintenancesettings")
//    public ModelAndView FacilitiesConfig(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("maintenance_settings_config");
//    }
//
//    @RequestMapping("/maintenancesettings_config_list")
//    //获取所有维护设置
//    public ResponseModel MaintenanceSettingsList(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            MaintainSettingsResult maintainSettingsResult = new MaintainSettingsResult();
//            List<RepairType> RepairData = maintenanceSettingsService.repairTypeData(schema_name);
//            List<FaultType> faultTypeData = maintenanceSettingsService.faultType(schema_name);
//            List<AssetStatus> assetStatusData = maintenanceSettingsService.assetStatus(schema_name);
//            List<MetaDataAsset> MetaDataAssetDat = maintenanceSettingsService.findAllMetaDataAsset(schema_name);
//            List<InspectionItemData> inspectionItemData = maintenanceSettingsService.inspectionItemData(schema_name);
//            List<SpotcheckItemData> spotcheckItemData = maintenanceSettingsService.spotcheckItemData(schema_name);
//            maintainSettingsResult.assetStatusData = assetStatusData;
//            maintainSettingsResult.repairData = RepairData;
//            maintainSettingsResult.faultTypeData = faultTypeData;
//            maintainSettingsResult.metaDataAssetData = MetaDataAssetDat;
//            maintainSettingsResult.inspectionItemData = inspectionItemData;
//            maintainSettingsResult.spotcheckItemData = spotcheckItemData;
//
//            return ResponseModel.ok(maintainSettingsResult);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAILED_TO_GET_MAINTENANCE_SETTINGS_LIST));
//        }
//    }
//
//    @RequestMapping("/change_maintenance_setting")
//    //获取添加或修改维护设置
//    public ResponseModel AddMaintenanceSettings(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            Company company = authService.getCompany(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            //操作表
//            String type = request.getParameter("type");
//            //设备类型
//            String assettypeid = request.getParameter("assettypeid");
//            //维护故障等类型
//            String typename = request.getParameter("typename");
//            //检查项
//            String itemname = request.getParameter("itemname");
//            //排序
//            String orderby = request.getParameter("orderby");
//            //检查方法
//            String checknote = request.getParameter("checknote");
//            //周期天数
//            String cycle = request.getParameter("cycle");
//            //设备状态对应的维修优先级
//            String priorityLevel = request.getParameter("level");
//            //点巡检项，对应的结果类型
//            String resultType = request.getParameter("resultType");
//
//            if (RegexUtil.isNull(typename) && RegexUtil.isNull(itemname) && RegexUtil.isNull(checknote) && RegexUtil.isNull(cycle)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_EDIT_CON));
//            }
//            if (RegexUtil.isNull(orderby)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ORDER_EM));
//            }
//            if (id != null && id != "") {
//                //维修类型
//                if (type.equals("1")) {
//                    maintenanceSettingsService.UpdaterepairTypeData(schema_name, typename, Integer.parseInt(orderby), Integer.parseInt(id));
//                }
//                //故障类型
//                if (type.equals("2")) {
//                    maintenanceSettingsService.UpdateFaultType(schema_name, typename, Integer.parseInt(orderby), Integer.parseInt(id));
//                }
//                //设备状态
//                if (type.equals("3")) {
//                    maintenanceSettingsService.UpdateAssetStatus(schema_name, typename, Integer.parseInt(orderby), Integer.parseInt(id));
//                }
//                //保养项
//                if (type.equals("4")) {
//                    maintenanceSettingsService.UpdateMaintainItem(schema_name, itemname, Integer.parseInt(id), checknote, Integer.parseInt(orderby));
//                }
//                //保养确认项
//                if (type.equals("5")) {
//                    maintenanceSettingsService.UpdateMaintainCheckItem(schema_name, itemname, Integer.parseInt(id), Integer.parseInt(orderby));
//                }
//                //巡检
//                if (type.equals("6")) {
//                    maintenanceSettingsService.UpdateInspection(schema_name, itemname, Integer.parseInt(id), checknote, Integer.parseInt(orderby), Integer.parseInt(resultType));
//
//                }
//                //点检
//                if (type.equals("7")) {
//                    maintenanceSettingsService.UpdateSpotcheck(schema_name, itemname, Integer.parseInt(id), checknote, Integer.parseInt(orderby), Integer.parseInt(resultType));
//                }
//                //周期
//                if (type.equals("8")) {
//                    maintenanceSettingsService.UpdateMaintainCycleData(schema_name, Integer.parseInt(cycle), Integer.parseInt(id), Integer.parseInt(orderby));
//                }
//
//            } else {
//                User loginUser = authService.getLoginUser(request);
//                //维修类型
//                if (type.equals("1")) {
//                    RepairType repairType = new RepairType();
//                    repairType.setSchema_name(schema_name);
//                    repairType.setTypeName(typename);
//                    repairType.setOrder(Integer.parseInt(orderby));
//                    maintenanceSettingsService.InsertrepairTypeData(repairType);
//                }
//                //故障类型
//                if (type.equals("2")) {
//                    FaultType faultType = new FaultType();
//                    faultType.setSchema_name(schema_name);
//                    faultType.setTypeName(typename);
//                    faultType.setOrder(Integer.parseInt(orderby));
//                    maintenanceSettingsService.InsertFaultType(faultType);
//                }
//                //设备状态
//                if (type.equals("3")) {
//                    AssetStatus assetStatus = new AssetStatus();
//                    assetStatus.setSchema_name(schema_name);
//                    assetStatus.setStatus(typename);
//                    assetStatus.setOrder(Integer.parseInt(orderby));
//                    assetStatus.setPriorityLevel(Integer.parseInt(priorityLevel));
//                    maintenanceSettingsService.InsertAssetStatus(assetStatus);
//                }
//                //保养项
//                if (type.equals("4")) {
//                    String itemCode = serialNumberService.generateMaxBusinessCode(schema_name, "setting_item");
//                    MaintainItemData maintainItemData = new MaintainItemData();
//                    maintainItemData.setItem_code(itemCode);
//                    maintainItemData.setSchema_name(schema_name);
//                    maintainItemData.setItem_name(itemname);
//                    maintainItemData.setCreate_user_account(loginUser.getAccount());
//                    maintainItemData.setAsset_type_id(assettypeid);
//                    maintainItemData.setCheck_note(checknote);
//                    Timestamp createtime = new Timestamp((new Date()).getTime());
//                    maintainItemData.setCreatetime(createtime);
//                    maintainItemData.setOrder(Integer.parseInt(orderby));
//                    maintenanceSettingsService.InsertMaintainItem(maintainItemData);
//                }
//                //保养确认项
//                if (type.equals("5")) {
//                    MaintainCheckItemData maintainCheckItemData = new MaintainCheckItemData();
//                    maintainCheckItemData.setSchema_name(schema_name);
//                    maintainCheckItemData.setCheck_item_name(itemname);
//                    maintainCheckItemData.setCreate_user_account(loginUser.getAccount());
//                    Timestamp createtime = new Timestamp((new Date()).getTime());
//                    maintainCheckItemData.setCreatetime(createtime);
//                    maintainCheckItemData.setAsset_type_id(assettypeid);
//                    maintainCheckItemData.setOrder(Integer.parseInt(orderby));
//                    maintenanceSettingsService.InsertMaintainCheckItem(maintainCheckItemData);
//                }
//                //巡检
//                if (type.equals("6")) {
//                    String itemCode = serialNumberService.generateMaxBusinessCode(schema_name, "setting_item");
//                    InspectionItemData inspectionItemData = new InspectionItemData();
//                    inspectionItemData.setItem_code(itemCode);
//                    inspectionItemData.setSchema_name(schema_name);
//                    inspectionItemData.setItem_name(itemname);
//                    inspectionItemData.setCreate_user_account(loginUser.getAccount());
//                    Timestamp createtime = new Timestamp((new Date()).getTime());
//                    inspectionItemData.setCreatetime(createtime);
//                    inspectionItemData.setCheck_note(checknote);
//                    //inspectionItemData.setAsset_type_id(assettypeid);
//                    inspectionItemData.setOrder(Integer.parseInt(orderby));
//                    inspectionItemData.setResultType(Integer.parseInt(resultType));
//                    maintenanceSettingsService.InsertInspection(inspectionItemData);
//
//                }
//                //点检
//                if (type.equals("7")) {
//                    String itemCode = serialNumberService.generateMaxBusinessCode(schema_name, "setting_item");
//                    SpotcheckItemData spotcheckItemData = new SpotcheckItemData();
//                    spotcheckItemData.setItem_code(itemCode);
//                    spotcheckItemData.setSchema_name(schema_name);
//                    spotcheckItemData.setItem_name(itemname);
//                    spotcheckItemData.setCreate_user_account(loginUser.getAccount());
//                    Timestamp createtime = new Timestamp((new Date()).getTime());
//                    spotcheckItemData.setCreatetime(createtime);
//                    spotcheckItemData.setCheck_note(checknote);
//                    spotcheckItemData.setAsset_type_id(assettypeid);
//                    spotcheckItemData.setOrder(Integer.parseInt(orderby));
//                    spotcheckItemData.setResultType(Integer.parseInt(resultType));
//                    maintenanceSettingsService.InsertSpotcheck(spotcheckItemData);
//                }
//                //周期
//                if (type.equals("8")) {
//                    MaintainCycleData maintainCycleData = new MaintainCycleData();
//                    maintainCycleData.setSchema_name(schema_name);
//                    maintainCycleData.setCycle_count(Integer.parseInt(cycle));
//                    maintainCycleData.setOrder(Integer.parseInt(orderby));
//                    maintainCycleData.setAsset_type_id(assettypeid);
//                    maintainCycleData.setCreate_user_account(loginUser.getAccount());
//                    Timestamp createtime = new Timestamp((new Date()).getTime());
//                    maintainCycleData.setCreatetime(createtime);
//                    maintenanceSettingsService.InsertMaintainCycleData(maintainCycleData);
//                }
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.ADD_MODIFY_SUCCESS));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.ADD_MODIFY_FAILURE));
//        }
//    }
//
//    @RequestMapping("/maintenancesettings_assetbyid")
//    //根据设备类型查询维护项
//    public ResponseModel MaintenanceSettingsById(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //操作表
//            String type = request.getParameter("type");
//            //设备类型
//            String assettypeid = request.getParameter("typeid");
//
//            if (type.equals("4")) {
//                List<MaintainItemData> maintainItemData = maintenanceSettingsService.maintainItemData(schema_name, assettypeid);
//                return ResponseModel.ok(maintainItemData);
//            }
//            if (type.equals("5")) {
//                List<MaintainCheckItemData> maintainCheckItemData = maintenanceSettingsService.maintainCheckItemData(schema_name, assettypeid);
//                return ResponseModel.ok(maintainCheckItemData);
//            }
//
//            if (type.equals("7")) {
//                List<SpotcheckItemData> spotcheckItemData = maintenanceSettingsService.spotcheckItemData(schema_name);
//                return ResponseModel.ok(spotcheckItemData);
//            }
//
//            if (type.equals("8")) {
//                List<MaintainCycleData> maintainCycleData = maintenanceSettingsService.maintainCycleData(schema_name, assettypeid);
//                return ResponseModel.ok(maintainCycleData);
//            }
//
//            return ResponseModel.ok("");
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_QUERY_LIST_FAIL));
//        }
//    }
//
//    @RequestMapping("/delete_manintenance")
//    //删除维护设置项
//    public ResponseModel DeleteMaintenanceSettingsById(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            MaintainSettingsResult maintainSettingsResult = new MaintainSettingsResult();
//            //操作表
//            String type = request.getParameter("type");
//            //设备类型
//            String id = request.getParameter("id");
//            int resultdelete = 0;
//            if (type.equals("1")) {
//                resultdelete = maintenanceSettingsService.deleteRepairData(schema_name, Integer.parseInt(id), "0");
//            }
//            if (type.equals("2")) {
//                resultdelete = maintenanceSettingsService.deleteFaultData(schema_name, Integer.parseInt(id), "0");
//            }
//            if (type.equals("3")) {
//                resultdelete = maintenanceSettingsService.deleteAssetStatusData(schema_name, Integer.parseInt(id), "0");
//            }
//            if (type.equals("4")) {
//                resultdelete = maintenanceSettingsService.deleteMaintainData(schema_name, Integer.parseInt(id));
//            }
//            if (type.equals("5")) {
//                resultdelete = maintenanceSettingsService.deleteMaintainCheckData(schema_name, Integer.parseInt(id));
//            }
//            if (type.equals("6")) {
//                resultdelete = maintenanceSettingsService.deleteInspectionData(schema_name, Integer.parseInt(id));
//            }
//            if (type.equals("7")) {
//                resultdelete = maintenanceSettingsService.deleteSpotcheckData(schema_name, Integer.parseInt(id));
//            }
//            if (type.equals("8")) {
//                resultdelete = maintenanceSettingsService.deleteMaintainCycleData(schema_name, Integer.parseInt(id));
//            }
//
//            return ResponseModel.ok(resultdelete);
//        } catch (Exception ex) {
//            return ResponseModel.error("selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_DATA_ERR)");
//        }
//    }
//
//    @RequestMapping("/save_asset_category")
//    //获取添加或修改维护设置
//    public ResponseModel saveAssetCategory(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        boolean insert = false;
//        //设备类型id
//        String sid = request.getParameter("id");
//        Integer id = null;
//        if (RegexUtil.isNull(sid)) {
//            insert = true;
//        } else {
//            id = Integer.parseInt(sid);
//        }
//        //父设备类型id
//        String sparent_id = request.getParameter("parent_id");
//        Integer parent_id = 0;
//        if (RegexUtil.isNotNull(sparent_id)) {
//            parent_id = Integer.parseInt(sparent_id);
//        }
//        //设备类型名称
//        String category_name = request.getParameter("category_name");
//        //排序
//        Integer order = Integer.parseInt(request.getParameter("order"));
//        //是否启用
//        int isuse = Boolean.parseBoolean(request.getParameter("isuse")) ? 1 : 0;
//        if (RegexUtil.isNull(category_name)) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.PLEASE_ENTER_TYPE_NAMES));
//        }
//        String icon = StringUtils.isEmpty(request.getParameter("icon")) ? null : request.getParameter("icon");
//        AssetCategory assetCategory = new AssetCategory(id, category_name, parent_id, order, isuse, icon);
//        try {
//            if (insert) {
//                assetCategory.setCategoryCode(assetCategoryService.findAssetCategoryCodeByParentId(schema_name, parent_id));
//                assetCategoryService.insert(schema_name, assetCategory);
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_SUCC));
//            } else {
//                //找到修改前的实体，进行比较
//                AssetCategory categoryData = assetCategoryService.findById(schema_name, Integer.valueOf(id));
//                if (categoryData.getParent_id() == parent_id) {
//                    String oldCategoryCode = categoryData.getCategoryCode();
//                    if (oldCategoryCode == null || oldCategoryCode.isEmpty()) {
//                        oldCategoryCode = assetCategoryService.findAssetCategoryCodeByParentId(schema_name, parent_id);
//                    }
//                    //位置编号不变
//                    assetCategory.setCategoryCode(oldCategoryCode);
//                } else {
//                    //位置编号改变
//                    assetCategory.setCategoryCode(assetCategoryService.findAssetCategoryCodeByParentId(schema_name, parent_id));
//                }
//
//                assetCategoryService.update(schema_name, assetCategory);
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MODIFY_SUCCESS));
//            }
//        } catch (Exception ex) {
//            if (insert) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FAIL));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_FAIL));
//            }
//        }
//
//    }
//
//    @RequestMapping("/asset_category_list")
//    //获取所有设备类型
//    public ResponseModel getAssetCategoryList(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<AssetCategory> result = assetCategoryService.findAllCategory(schema_name);
//            return ResponseModel.ok(result);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_LIST_FA));
//        }
//    }
//
//    @RequestMapping("/delete_asset_category")
//    //删除制定设备类型
//    public ResponseModel deleteAssetCategory(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //设备类型id
//            String sid = request.getParameter("id");
//            Integer id = null;
//            if (RegexUtil.isNotNull(sid)) {
//                id = Integer.parseInt(sid);
//            }
//            assetCategoryService.delete(schema_name, id, "1");
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));
//        } catch (Exception ex) {
//            return ResponseModel.error("selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_DATA_ERR)");
//        }
//    }

}
