package com.gengyun.senscloud.controller;

import org.springframework.web.bind.annotation.RestController;

//
//import com.alibaba.fastjson.JSON;
//import com.fitit100.util.RegexUtil;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.AssetCategory;
//import com.gengyun.senscloud.model.Charge;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.enumResp.ResultStatus;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.StringUtil;
//import com.github.pagehelper.Page;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.List;
//
@RestController()
public class ChargeConfigController {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    ChargeService chargeService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AssetCategoryService assetCategoryService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/charge_config")
//    public ModelAndView execute(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        List<Facility> facilitiesList = facilitiesService.FacilitiesListInUse(schema_name);
//        TreeBuilder treeBuilder = new TreeBuilder();
//        List<Node> allNodes = new ArrayList<Node>();
//        for (Facility item : facilitiesList) {
//            Node node = new Node();
//            node.setId(String.valueOf(item.getId()));
//            node.setParentId(String.valueOf(item.getParentId()));
//            node.setName(item.getTitle());
//            node.setDetail(item);
//            allNodes.add(node);
//        }
//        List<Node> roots = treeBuilder.buildListToTree(allNodes);
//        String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//        List<AssetCategory> categoryList = assetCategoryService.findAllCategory(schema_name);
//        allNodes.clear();
//        for (AssetCategory assetCategory : categoryList) {
//            Node node = new Node();
//            node.setId(String.valueOf(assetCategory.getId()));
//            node.setParentId(String.valueOf(assetCategory.getParent_id()));
//            node.setName(assetCategory.getCategory_name());
//            allNodes.add(node);
//        }
//        roots = treeBuilder.buildListToTree(allNodes);
//        String categorySelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//        return new ModelAndView("charge_config").addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("categorySelect", categorySelect);
//    }
//
//    @RequestMapping("/charge_config_list")
//    public String getChargeList(@RequestParam(name = "facilities", required = false) String facilities,
//                                @RequestParam(name = "categories", required = false) String categories,
//                                @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                @RequestParam(name = "sortName", required = false) String sortName,
//                                @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            if (pageNumber == null) {
//                pageNumber = 1;
//            }
//            if (pageSize == null) {
//                pageSize = 20;
//            }
//            String orderBy = null;
//            if (StringUtils.isNotEmpty(sortName)) {
//                if (StringUtils.isEmpty(sortOrder)) {
//                    sortOrder = "asc";
//                }
//                orderBy = sortName + " " + sortOrder;
//            }
//            Page page = PageHelper.startPage(pageNumber, pageSize, orderBy);
//            List<Charge> result = chargeService.find(schema_name, facilities, categories,
//                    null, null);
//            JSONObject pageResult = new JSONObject();
//            pageResult.put("total", page.getTotal());
//            pageResult.put("rows", result);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping("/save_charge")
//    @Transactional
//    public ResponseModel saveCharge(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String user = null;
//        if (loginUser != null) {
//            user = loginUser.getAccount();
//        }
//        String result = selectOptionService.getLanguageInfo( LangConstant.MSG_DE_DATA_NOT_NULL);
//        String dids = (String) request.getParameter("dids");
//        if (StringUtils.isNotEmpty(dids)) {
//            try {
//                chargeService.delete(schema_name, dids);
//            } catch (Exception e) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_FAIL));
//            }
//        }
//        String updates = (String)request.getParameter("updates");
//        if (StringUtils.isEmpty(updates)) {
//            return ResponseModel.ok("");
//        }
//        int total = 0;
//        List<Charge> chargeList = JSON.parseArray(updates, Charge.class);
//        if (chargeList != null && chargeList.size() > 0) {
//            for (Charge charge : chargeList) {
//                boolean insert = false;
//                String action = selectOptionService.getLanguageInfo( LangConstant.MOD_A);
//                if (charge.getId() == null) {
//                    insert = true;
//                    action = selectOptionService.getLanguageInfo( LangConstant.ADD);
//                }
//                if (StringUtils.isEmpty(user)) {
//                    result = action + selectOptionService.getLanguageInfo( LangConstant.MSG_DE_USER_NOT_NULL);
//                    break;
//                }
//                if (charge.getFacility_id() == null) {
//                    result = action + selectOptionService.getLanguageInfo( LangConstant.MSG_DE_LO_NOT_NULL);
//                    break;
//                }
//                if (charge.getAsset_category_id() == null) {
//                    result = action + selectOptionService.getLanguageInfo( LangConstant.MSG_DE_TYPE_NOT_NULL);
//                    break;
//                }
//                if (StringUtils.isEmpty(charge.getCharge_account())) {
//                    result = action + selectOptionService.getLanguageInfo( LangConstant.MSG_DE_MESS_NOT_NULL);
//                    break;
//                }
//                if (charge.getCharge_level() == null) {
//                    result = action + selectOptionService.getLanguageInfo( LangConstant.MSG_MESS_MAN_NOT_NULL);
//                    break;
//                }
//                charge.setCreatetime(new Timestamp(System.currentTimeMillis()));
//                charge.setCreate_user_account(user);
//                try {
//                    if (insert) {
//                        chargeService.insert(schema_name, charge);
//                        total++;
//                    } else {
//                        chargeService.update(schema_name, charge);
//                        total++;
//                    }
//                } catch (Exception ex) {
//                    if (insert) {
//                        result = selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FAIL);
//                    } else {
//                        result = selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_FAIL);
//                    }
//                }
//            }
//        }
//        if (total == chargeList.size()) {
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_SUCC_A));
//        } else {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(result);
//        }
//    }
//
//    @RequestMapping("/delete_charge")
//    public ResponseModel deleteCharge(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String ids = request.getParameter("ids");
//            if (StringUtils.isNotEmpty(ids)) {
//                chargeService.delete(schema_name, ids);
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));
//        } catch (Exception ex) {
//            return ResponseModel.error("selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_DATA_ERR)");
//        }
//    }
//
//    @RequestMapping({"/charge_user_list"})
//    public ResponseModel findUser(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String facilityID = request.getParameter("facility_id");
//        List<User> userList = userService.getUser(schema_name, Integer.valueOf(facilityID));
//        ResponseModel result = new ResponseModel();
//
//        result.setContent(userList);
//        result.setCode(ResultStatus.SUCCESS.getCode());
//        result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUCC_A));
//
//        return result;
//    }
}
