package com.gengyun.senscloud.controller;

//import com.alibaba.fastjson.JSON;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.SqlConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.SystemPermission;
//import com.gengyun.senscloud.model.UserRole;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.asset.AssetCategoryService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.SystemPermissionService;
//import com.gengyun.senscloud.service.business.UserRoleService;
//import com.gengyun.senscloud.util.IdGenerator;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//@RestController
public class UserRolesConfigController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    UserRoleService userRoleService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    SystemPermissionService systemPermissionService;
//    @Autowired
//    AssetCategoryService assetCategoryService;
//
//    /**
//     * 进入角色列表页
//     *
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping({"/user_roles_config/index"})
//    public ModelAndView userRolesConfig() {
//        return new ModelAndView("user_roles_config/index");
//    }
//
//    /**
//     * 角色管理列表查询
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping({"/user_roles_config/findRoleList"})
//    public String findRoleList(HttpServletRequest request) {
//        try {
//            return userRoleService.findRoleList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 添加角色界面
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping("/user_roles_config/addRole")
//    public ModelAndView goToAdd() {
//        return new ModelAndView("user_roles_config/add");
//    }
//
//    /**
//     * 获取权限列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping("/user_roles_config/getPermissionSelectList")
//    public String getPermissionSelect(HttpServletRequest request) {
//        try {
//            Company company = AuthService.getCompany(request);
//            List<SystemPermission> systemPermissionList = systemPermissionService.findAllByCompanyId(company.getId(), null);
//            JSONObject result = new JSONObject();
//            result.put("rows", systemPermissionList);
//            result.put("code", 200);
//            // 编辑页使用
//            String roleId = request.getParameter("roleId");
//            if (!RegexUtil.isNull(roleId)) {
//                String schemaName = company.getSchema_name();
//                List<String> spList = userRoleService.getRolePermissionList(schemaName, company.getId(), roleId);
//                result.put("permissionList", spList);
//            }
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 获取有效设备类型列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping("/user_roles_config/getAcList")
//    public String getUseAcList(HttpServletRequest request) {
//        try {
//            Company company = AuthService.getCompany(request);
//            String schemaName = company.getSchema_name();
//            List<Map<String, Object>> acList = assetCategoryService.getUseAcList(schemaName);
//            JSONObject result = new JSONObject();
//            result.put("rows", acList);
//            result.put("code", 200);
//            // 编辑页使用
//            String roleId = request.getParameter("roleId");
//            if (!RegexUtil.isNull(roleId)) {
//                List<String> roleAcList = userRoleService.getRoleAcList(schemaName, roleId);
//                result.put("assetCategoryList", roleAcList);
//            }
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增角色
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping({"/user_roles_config/add_role"})
//    @Transactional //支持事务
//    public JsonResult addRole(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            UserRole userRole = new UserRole();
//            String name = request.getParameter("name");
//            if (RegexUtil.isNull(name)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo("user_role_name_not_null"));
//            }
//            String roleId = String.valueOf(IdGenerator.generateIntId());
//            userRole.setName(name);
//            userRole.setDescription(request.getParameter("description"));
//            userRole.setId(roleId);
////            if ("on".equals(request.getParameter("available"))) {
//            userRole.setAvailable(true);
////            } else {
////                userRole.setAvailable(false);
////            }
//            userRole.setCreatetime(new Timestamp(new Date().getTime()));
//            userRoleService.add(schema_name, userRole);
//
//            // 角色权限
//            String permissionIds = request.getParameter("permissionIds");
//            selectOptionService.doSetRelationTableList(schema_name, permissionIds, roleId,
//                    "role_id", "permission_id", "_sc_role_permission",
//                    SqlConstant._sc_role_permission_columns);
//
//            // 设备类型
//            String acpIds = request.getParameter("acpIds");
//            selectOptionService.doSetRelationTableList(schema_name, acpIds, roleId,
//                    "role_id", "category_id", "_sc_role_asset_type",
//                    SqlConstant._sc_role_asset_type_columns);
//
//            return new JsonResult().success(selectOptionService.getLanguageInfo("user_role_add_success"));
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return new JsonResult().error(selectOptionService.getLanguageInfo("user_role_add_wrong"));
//        }
//    }
//
//    /**
//     * 编辑角色界面
//     *
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping({"/user_roles_config/editRole"})
//    public ModelAndView goToEdit(HttpServletRequest request) {
//        try {
//            Company company = authService.getCompany(request);
//            String schemaName = company.getSchema_name();
//            String roleId = request.getParameter("id");
//            List<UserRole> roleData = userRoleService.findById(schemaName, "'" + roleId + "'");
//            UserRole targetRole = new UserRole();
//            if (null != roleData && !roleData.isEmpty() && roleData.size() > 0) {
//                targetRole = roleData.get(0);
//            }
//            return new ModelAndView("user_roles_config/edit").addObject("roleData", JSON.toJSON(targetRole));
//        } catch (Exception e) {
//            return new ModelAndView("user_roles_config/edit").addObject("error", e.getMessage());
//        }
//    }
//
//    /**
//     * 更新角色
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping({"/user_roles_config/save_role"})
//    @Transactional //支持事务
//    public JsonResult saveRole(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            UserRole userRole = new UserRole();
//            String roleId = request.getParameter("id");
//            String name = request.getParameter("name");
//            if (RegexUtil.isNull(name)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo("user_role_name_not_null"));
//            }
//
//            userRole.setName(name);
//            userRole.setDescription(request.getParameter("description"));
////            if ("on".equals(request.getParameter("available"))) {
////                userRole.setAvailable(true);
////            } else {
////                userRole.setAvailable(false);
////            }
//            userRole.setCreatetime(new Timestamp(new Date().getTime()));
//            userRole.setId(roleId);
//            userRoleService.save(schema_name, userRole);
//
//            // 角色权限
//            String permissionIds = request.getParameter("permissionIds");
//            selectOptionService.doSetRelationTableList(schema_name, permissionIds, roleId,
//                    "role_id", "permission_id", "_sc_role_permission",
//                    SqlConstant._sc_role_permission_columns);
//
//            // 设备类型
//            String acpIds = request.getParameter("acpIds");
//            selectOptionService.doSetRelationTableList(schema_name, acpIds, roleId,
//                    "role_id", "category_id", "_sc_role_asset_type",
//                    SqlConstant._sc_role_asset_type_columns);
//            return new JsonResult().success(selectOptionService.getLanguageInfo("user_role_save_success"));
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return new JsonResult().error(selectOptionService.getLanguageInfo("user_role_save_wrong"));
//        }
//    }
//
//    /**
//     * 删除角色
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("user_roles_config")
//    @RequestMapping({"/user_roles_config/remove_role"})
//    @Transactional //支持事务
//    public JsonResult removeRole(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            String roleId = request.getParameter("id");
//            userRoleService.remove(schema_name, roleId);
//            return new JsonResult().success(selectOptionService.getLanguageInfo("user_role_delete_success"));
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return new JsonResult().error(selectOptionService.getLanguageInfo("user_role_delete_wrong"));
//        }
//    }
//
//    /// <summary>
//    /// 通用下拉框菜单 TODO
//    /// 获取角色
//    /// </summary>
//    /// <returns></returns>
//    @RequestMapping("/get_role_list_select")
//    public ResponseModel getRoleList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        List<UserRole> userRoleList = userRoleService.findAll(schema_name);
//        // JSONObject json =JSONObject.fromObject(listTree);
//        return ResponseModel.ok(userRoleList);
//    }
//
//    @RequestMapping({"/get_role_list_select_work_sheet_pool/all"})
//    public ResponseModel getRoleListWorkSheetPool(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        List<UserRole> userRoleList = userRoleService.findAll(schema_name);
//        // JSONObject json =JSONObject.fromObject(listTree);
//        result.setCode(StatusConstant.SUCCES_RETURN);
//        result.setContent(userRoleList);
//        return result;
//    }
//
//    @RequestMapping({"/get_role_list_select_work_sheet_pool/role_name_by_no_user"})
//    public ResponseModel checkUserByRole(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        try {
//            List<Map<String, Object>> roleNoUser = userRoleService.checkUserByRole(schema_name);
//            return ResponseModel.ok(roleNoUser);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseModel.error(selectOptionService.getLanguageInfo("get_data_wrong"));
//        }
//    }
}
