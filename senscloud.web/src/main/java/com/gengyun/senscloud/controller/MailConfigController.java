package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.Charge;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.MailConfigModel;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.MailConfigService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.github.pagehelper.Page;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//import java.util.UUID;
//
///**
// * @Author: Wudang Dong
// * @Description:邮件发送配置
// * @Date: Create in 下午 1:53 2019/5/23 0023
// */
//@RestController
public class MailConfigController {

//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    MailConfigService mailConfigService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/mailConfig")
//    public ModelAndView mailConfig(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("mail_config/mail_config");
//    }
//
//    @RequestMapping("/mail_list")
//    public String getChargeList(@RequestParam(name = "searchKey", required = false) String searchKey,
//                                @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                @RequestParam(name = "sortName", required = false) String sortName,
//                                @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String condition = "";
//            if (!StringUtils.isEmpty(searchKey) && !StringUtils.isEmpty(searchKey) && !StringUtils.isEmpty(searchKey)) {
//                //title  位置(名称/code) 创建者
//                condition += " and (mail_key like '%" + searchKey + "%' or mail_template like '%" + searchKey + "%')";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<MailConfigModel> mailConfigModels = mailConfigService.all(schema_name, condition, pageSize, begin);
//            int mailCount = mailConfigService.allCount(schema_name, condition);
//
//            result.put("rows", mailConfigModels);
//            result.put("total", mailCount);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    @RequestMapping("/save_mail")
//    public ResponseModel add(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        MailConfigModel mailConfigModel = new MailConfigModel();
//        ResponseModel model = new ResponseModel();
//        try {
//            String mail_name = request.getParameter("template_name");
//            String mail_template = request.getParameter("template");
//            String key = request.getParameter("key");
//
//            mailConfigModel.setMail_key(key);
//            mailConfigModel.setMail_name(mail_name);
//            mailConfigModel.setMail_template(mail_template);
//            int id = mailConfigService.add(schema_name, mailConfigModel);
//            if (id > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_template_add_wrong"));
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg("");
//            return model;
//        }
//
//    }
//
//    @RequestMapping("/edit_mail")
//    public ResponseModel edit(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        MailConfigModel mailConfigModel = new MailConfigModel();
//        ResponseModel model = new ResponseModel();
//        try {
//            String mail_name = request.getParameter("template_name");
//            String mail_template = request.getParameter("template");
//            String key = request.getParameter("key");
//            String id = request.getParameter("id");
//            if (RegexUtil.isNull("id")) {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_template_save_wrong"));
//                return model;
//            }
//
//            mailConfigModel.setMail_name(mail_name);
//            mailConfigModel.setId(Integer.parseInt(id));
//            mailConfigModel.setMail_template(mail_template);
//            mailConfigModel.setMail_key(key);
//            int doUpdate = mailConfigService.edit(schema_name, mailConfigModel);
//            if (doUpdate > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_template_save_wrong"));
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg("");
//            return model;
//        }
//
//    }
//
//    @RequestMapping("/del_mail")
//    public ResponseModel del(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        MailConfigModel mailConfigModel = new MailConfigModel();
//        ResponseModel model = new ResponseModel();
//        try {
//            String id = request.getParameter("id");
//            if (!RegexUtil.isNull("id")) {
//                int doDele = mailConfigService.delete(schema_name, Integer.parseInt(id));
//                if (doDele > 0) {
//                    model.setCode(StatusConstant.SUCCES_RETURN);
//                    model.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//                } else {
//                    model.setCode(StatusConstant.NO_DATE_RETURN);
//                    model.setMsg(selectOptionService.getLanguageInfo("mail_template_delete_wrong"));
//                }
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_template_delete_wrong"));
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo("mail_template_delete_wrong"));
//            return model;
//        }
//
//    }
//
//
////    @RequestMapping("/updateMailConfig")
////    public ResponseModel updateMailConfig(HttpServletRequest request, HttpServletResponse response){
////        ResponseModel model = new ResponseModel();
////        String schema_name = authService.getCompany(request).getSchema_name();
////        try{
////            String status = request.getParameter("status");
////            String id_str = request.getParameter("id");
////
////            if (StringUtils.isEmpty(id_str) || StringUtils.isEmpty(id_str) || StringUtils.isEmpty(id_str)) {
////                model.setCode(StatusConstant.VALIDATE);
////                model.setMsg("系统参数获取失败 ");
////                return model;
////            }
////            if (StringUtils.isEmpty(status) || StringUtils.isEmpty(status) || StringUtils.isEmpty(status)) {
////                model.setCode(StatusConstant.VALIDATE);
////                model.setMsg("未获得状态参数");
////                return model;
////            }
////
////            int id = Integer.parseInt(id_str);
////            boolean status_b  = false;
////            if(status!=null && status.equals("open")){
////                status_b = true;
////            }
////            int doUpdate = mailConfigService.updateMailConfig(schema_name,status_b, id);
////            if(doUpdate>0){
////                model.setCode(StatusConstant.SUCCES_RETURN);
////                model.setMsg("更改工单池成功");
////            }else{
////                model.setCode(StatusConstant.UPDATE_ERROR);
////                model.setMsg("更改工单池失败");
////            }
////            return model;
////        }catch (Exception e){
////            model.setCode(StatusConstant.ERROR_RETURN);
////            model.setMsg("更改工单池出现异常");
////            return model;
////        }
////    }
}
