package com.gengyun.senscloud.controller.system;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.OtherCfgModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "其它配置")
@RestController
public class OtherCfgController {
    @Resource
    CacheUtilService cacheUtilService;
    @Resource
    LogsService logsService;

    @ApiOperation(value = "获取缓存列表", notes = ResponseConstant.RSP_DESC_SEARCH_CACHE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchCacheList", method = RequestMethod.GET)
    public ResponseModel queryCacheList(@SuppressWarnings("unused") MethodParam methodParam, OtherCfgModel ocModel) {
        return ResponseModel.ok(cacheUtilService.getCacheListForPage(ocModel));
    }

    @ApiOperation(value = "根据缓存名称清除缓存", notes = ResponseConstant.RSP_DESC_CLEAR_CACHE_BY_NAME)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "name"})
    @RequestMapping(value = "/clearCacheByName", method = RequestMethod.GET)
    public void clearCacheByName(@SuppressWarnings("unused") MethodParam methodParam, OtherCfgModel ocModel) {
        cacheUtilService.evictCacheByName(ocModel.getName());
    }

    @ApiOperation(value = "根据缓存名称和键值清除缓存", notes = ResponseConstant.RSP_DESC_CLEAR_CACHE_BY_KEY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "name", "key"})
    @RequestMapping(value = "/clearCacheByKey", method = RequestMethod.GET)
    public void clearCacheByKey(@SuppressWarnings("unused") MethodParam methodParam, OtherCfgModel ocModel) {
        cacheUtilService.evictCacheByKey(ocModel.getName(), ocModel.getKey());
    }

    @ApiOperation(value = "获取错误日志列表", notes = ResponseConstant.RSP_DESC_SEARCH_ERROR_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "startDateSearch", "endDateSearch"})
    @RequestMapping(value = "/searchErrorLogList", method = RequestMethod.POST)
    public ResponseModel queryErrorLogList(MethodParam methodParam, OtherCfgModel ocModel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(logsService.getErrorLogList(methodParam, ocModel));
    }
}
