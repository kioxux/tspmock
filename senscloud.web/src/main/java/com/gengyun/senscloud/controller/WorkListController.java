package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.WorkListDetailModel;
//import com.gengyun.senscloud.model.WorkListModel;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;

///**
// * 功能：工单列表
// * Created by Administrator on 2018/11/2.
// */
//@RestController
//@RequestMapping("/work")
public class WorkListController {

//    @Autowired
//    WorkListService workListService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @RequestMapping("/addWork")
//    public ModelAndView add(HttpServletResponse response, HttpServletRequest request){
//        ModelAndView mv  = new ModelAndView();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String facilitiesSelect = "";
//        Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, loginUser.getId(), "repair");
//        //获取权限树
//        facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//        mv.addObject("facilitiesSelect", facilitiesSelect);
//        mv.setViewName("/work_lists/add_work");
//        return  mv;
//    }
//
//    @RequestMapping("/addWorkList")
//    @Transactional
//    public ResponseModel addWorkList(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        WorkListModel workListModel = new WorkListModel();
//        WorkListDetailModel workListDetailModel = new WorkListDetailModel();
//        try{
//            Integer work_type_id = 0;
//            Integer facility_id = 0;
//            String order_type = request.getParameter("order_type");//工单类型
//            if(order_type!=null&&!order_type.equals("")){
//                work_type_id = Integer.parseInt(order_type);
//            }else{
//                work_type_id = 0;
//            }
//            String sel_facility_id = request.getParameter("sel_facility_id");//所属位置
//            if(sel_facility_id!=null&&!work_type_id.equals("")){
//                facility_id = Integer.parseInt(sel_facility_id);
//            }else{
//                facility_id = 0;
//            }
//            String sel_asset_strcode = request.getParameter("sel_asset_strcode");//设备资产
//            String completeAccount = request.getParameter("completeAccount");//完成人
//            String beginTime = request.getParameter("beginTime");//开始时间
//            String deadlineTime = request.getParameter("deadlineTime");//截止时间
//            String remarks = request.getParameter("remarks");//备注
//            String work_code = serialNumberService.generateMaxBusinessCode(schema_name, "work_sheet");//工单号
//            String sub_work_code = serialNumberService.generateMaxBusinessCode(schema_name, "work_sheet_detail");//工单详情单号
//            String title = request.getParameter("title");//截止时间
//            String pic_id = request.getParameter("pic_id");//图片id
//
//
//            String work_template_code = "10";
//            Integer relation_type = 2;
//            String problem_img = "";
//            Timestamp createTimeStamp = new Timestamp(System.currentTimeMillis());
//            Timestamp beginTimeTimeStamp = Timestamp.valueOf(beginTime);
//            Timestamp deadlineTimeStamp = Timestamp.valueOf(deadlineTime);
//
//            //工单概要表-start
//            workListModel.setWork_code(work_code);
//            workListModel.setWork_type_id(work_type_id);
//            workListModel.setWork_template_code(work_template_code);
//            workListModel.setParent_code("0");
//            workListModel.setPool_id("0");
//            workListModel.setFacility_id(facility_id);
//            workListModel.setRelation_type(relation_type);//关联对象： 1：位置  2：设备  3：点巡检点  9：其他；
//            workListModel.setRelation_id(sel_asset_strcode);
//            workListModel.setOccur_time(beginTimeTimeStamp);
//            workListModel.setDeadline_time(deadlineTimeStamp);
//            workListModel.setStatus(20);
//            workListModel.setRemark(remarks);
//            workListModel.setCreate_time(createTimeStamp);
//            workListModel.setCreate_user_account(user.getAccount());
//            //工单概要表-end
//
//            //工单详情表-start
//            workListDetailModel.setSub_work_code(sub_work_code);
//            workListDetailModel.setWork_code(work_code);
//            workListDetailModel.setWork_type_id(work_type_id);
//            workListDetailModel.setWork_template_code(work_template_code);
//            workListDetailModel.setRelation_type(relation_type);
//            workListDetailModel.setRelation_id(sel_asset_strcode);
//            workListDetailModel.setTitle(title);
//            workListDetailModel.setProblem_note("");
//            workListDetailModel.setProblem_img(pic_id);
//            workListDetailModel.setAsset_running_status(1);
//            workListDetailModel.setFault_type(0);
//            workListDetailModel.setRepair_type(10);
//            workListDetailModel.setPriority_level(1);
//            workListDetailModel.setStatus(StatusConstant.UNDISTRIBUTED);//待分配
//            workListDetailModel.setRemark(remarks);
//            workListDetailModel.setBody_property(null);
//            workListDetailModel.setFrom_code("");
//            workListDetailModel.setWaiting(0);
//            workListDetailModel.setBom_app_result("");
//            workListDetailModel.setReceive_account("");
//            //工单详情表-end
//
//            int i = workListService.addWorkList(schema_name,workListModel);
//            int s = workListService.addWorkDetailList(schema_name,workListDetailModel);
//            if(i>0&&s>0){
//                model.setCode(1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.SUCC_SAVE));
//            }else if(i>0&&s<=0){
//                model.setCode(-1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_WORK_ERROR));
//            }else if(i<=0&&s>0){
//                model.setCode(1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_DETAIL));
//            }else {
//                model.setCode(1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_ERR));
//            }
//            return  model;
//        }catch (Exception e){
//            model.setCode(-1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            return model;
//        }
//    }

}
