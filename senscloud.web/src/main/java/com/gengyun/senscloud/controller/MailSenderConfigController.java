package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.enumResp.ResultStatus;
//import com.gengyun.senscloud.service.AssetDataServiceV2;
//import com.gengyun.senscloud.service.MailSenderConfigService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//import java.util.UUID;
//
///**
// * @Author: Wudang Dong
// * @Description:邮件发送人
// * @Date: Create in 下午 1:53 2019/5/23 0023
// */
//@RestController
public class MailSenderConfigController {
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    MailSenderConfigService mailSenderConfigService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description:邮件发送人功能
//     */
//    @RequestMapping("/mailSenserConfig")
//    public ModelAndView mailSender(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("mail_sender_config/mail_sender_config");
//    }
//
//
//    /**
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @param response
//     * @return
//     * @Description 邮件发送人配置
//     */
//    @RequestMapping("/mail_sender_list")
//    public String getChargeList(@RequestParam(name = "searchKey", required = false) String searchKey,
//                                @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                @RequestParam(name = "sortName", required = false) String sortName,
//                                @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String condition = "";
//            if (!StringUtils.isEmpty(searchKey) && !StringUtils.isEmpty(searchKey) && !StringUtils.isEmpty(searchKey)) {
//                //title  位置(名称/code) 创建者
//                condition += " and sender.obj_name like '%" + searchKey + "%' ";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<MailSenderConfigModel> mailConfigModels = mailSenderConfigService.all(schema_name, condition, pageSize, begin);
//            int mailCount = mailSenderConfigService.allCount(schema_name, condition);
//
//            result.put("rows", mailConfigModels);
//            result.put("total", mailCount);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping("/save_mail_sender")
//    public ResponseModel add(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        MailSenderConfigModel mailSenderConfigModel = new MailSenderConfigModel();
//        ResponseModel model = new ResponseModel();
//        try {
//            String obj_name = request.getParameter("obj_name");
//            String obj_type = request.getParameter("obj_type");
//            String device_id = request.getParameter("device_id");
//            String receiver_account = request.getParameter("receiver_account");
//            String cc_account = request.getParameter("cc_account");
//            String running_to_offline = request.getParameter("running_to_offline");
//            String running_to_error = request.getParameter("running_to_error");
//
//            mailSenderConfigModel.setCc_account(cc_account);
//            mailSenderConfigModel.setReceiver_account(receiver_account);
//            mailSenderConfigModel.setObj_name(obj_name);
//            mailSenderConfigModel.setObj_type(obj_type);
//            mailSenderConfigModel.setDevice_id(device_id);
//            mailSenderConfigModel.setStatus(true);
//
//            boolean status = false;
//            boolean error_status = false;
//            if(running_to_offline!=null && !running_to_offline.equals("") && running_to_offline.equals("1")){
//                status = true;
//            }
//            if(running_to_error!=null && !running_to_error.equals("") && running_to_error.equals("1")){
//                error_status = true;
//            }
//            mailSenderConfigModel.setStatus(status);
//            mailSenderConfigModel.setError_status(error_status);
//
//            int id = mailSenderConfigService.add(schema_name, mailSenderConfigModel);
//            if (id > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_receiver_save_wrong"));
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg("");
//            return model;
//        }
//
//    }
//
//    @RequestMapping("/edit_mail_sender")
//    public ResponseModel edit(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        MailSenderConfigModel mailSenderConfigModel = new MailSenderConfigModel();
//        ResponseModel model = new ResponseModel();
//        try {
//
//            String id = request.getParameter("id");
//            String obj_name = request.getParameter("obj_name");
//            String obj_type = request.getParameter("obj_type");
//            String device_id = request.getParameter("device_id");
//            String receiver_account = request.getParameter("receiver_account");
//            String cc_account = request.getParameter("cc_account");
//            String running_to_offline = request.getParameter("running_to_offline");
//            String running_to_error = request.getParameter("running_to_error");
//            if (RegexUtil.isNull("id")) {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_receiver_save_wrong"));
//                return model;
//            }
//
//            boolean status = false;
//            boolean error_status = false;
//            if(running_to_offline!=null && !running_to_offline.equals("") && running_to_offline.equals("1")){
//                status = true;
//            }
//            if(running_to_error!=null && !running_to_error.equals("") && running_to_error.equals("1")){
//                error_status = true;
//            }
//            mailSenderConfigModel.setCc_account(cc_account);
//            mailSenderConfigModel.setReceiver_account(receiver_account);
//            mailSenderConfigModel.setObj_name(obj_name);
//            mailSenderConfigModel.setObj_type(obj_type);
//            mailSenderConfigModel.setDevice_id(device_id);
//            mailSenderConfigModel.setId(Integer.parseInt(id));
//            mailSenderConfigModel.setStatus(status);
//            mailSenderConfigModel.setError_status(error_status);
//            int doUpdate = mailSenderConfigService.updateSenderMailList(schema_name, mailSenderConfigModel);
//            if (doUpdate > 0) {
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("mail_receiver_save_wrong"));
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg("");
//            return model;
//        }
//
//    }
//
//
//    @RequestMapping("/update_mail_sender")
//    public ResponseModel del(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        MailConfigModel mailConfigModel = new MailConfigModel();
//        ResponseModel model = new ResponseModel();
//        try {
//            String id = request.getParameter("id");
//            boolean status = true;
//            String text_content = request.getParameter("text_content");
//            if (text_content != null && (text_content.equals("禁用") || text_content.equals("disable"))) {
//                status = false;
//            } else if (text_content != null && (text_content.equals("启用") || text_content.equals("enable"))) {
//                status = true;
//            }
//            if (!RegexUtil.isNull("id")) {
//                int doDele = mailSenderConfigService.updateSenderMailConfig(schema_name, status, Integer.parseInt(id));
//                if (doDele > 0) {
//                    model.setCode(StatusConstant.SUCCES_RETURN);
//                    model.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//                } else {
//                    model.setCode(StatusConstant.NO_DATE_RETURN);
//                    model.setMsg(selectOptionService.getLanguageInfo("operate_failure"));
//                }
//            } else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo("operate_failure"));
//            }
//            return model;
//        } catch (Exception e) {
//            e.printStackTrace();
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg("");
//            return model;
//        }
//
//    }
//
//
//    @RequestMapping({"/mail_sender_user_list"})
//    public ResponseModel findUser(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        String facilityID = request.getParameter("facility_id");
//        List<User> userList = userService.findAll(schema_name);
//        ResponseModel result = new ResponseModel();
//
//        result.setContent(userList);
//        result.setCode(ResultStatus.SUCCESS.getCode());
//        result.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//
//        return result;
//    }
//
//    @RequestMapping({"/mail_sender_asset_list"})
//    public ResponseModel fintAssetObj(HttpServletRequest request, HttpServletResponse response) {
//        Company company = authService.getCompany(request);
//        String schema_name = company.getSchema_name();
//        String facilityID = request.getParameter("facility_id");
//        List<Asset> assetList = assetDataServiceV2.findAllWithNumber(schema_name, 20);
//        ResponseModel result = new ResponseModel();
//
//        result.setContent(assetList);
//        result.setCode(ResultStatus.SUCCESS.getCode());
//        result.setMsg(selectOptionService.getLanguageInfo("operate_success_a"));
//        return result;
//    }
}
