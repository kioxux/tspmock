package com.gengyun.senscloud.controller.MonitorArea;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 备件报废
 */
@RestController
@RequestMapping("/monitor_area_component")
public class MonitorAreaComponentController {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    MonitorAreaComponentService monitorAreaComponentService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//
//        try {
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("add_monitor_area_component", "brAddFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "monitor_area_component/index", "");
//        }catch (Exception e){
//            return new ModelAndView("monitor_area_component/index");
//        }
//
//
//    }
//
//    /**
//     * 获取区域组件列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find_monitor_area_component_list")
//    public String findMonitorAreaComponentList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                      @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                      @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                      @RequestParam(name = "area", required = false) String area,
//                                      @RequestParam(name = "facilityId", required = false) String facilityId,
//                                      HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User user = AuthService.getLoginUser(request);
//            JSONObject pageResult=monitorAreaComponentService.query(schemaName,searchKey,pageSize,pageNumber,area,facilityId,user);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//    /**
//     * 添加区域配置界面
//     */
//
//    @RequestMapping("/add_monitorAreaComponent_view")
//    public ModelAndView addMonitorAreaComponentView(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        return new ModelAndView("monitor_area_component/monitor_area_component_add");
//    }
//
//    /**
//     * 新增区域组件，处理业务
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/add_monitorAreaComponent_do")
//    public ResponseModel addMonitorAreaComponent(@RequestParam Map<String, Object> paramMap,HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "monitor_area_component", "add_monitor_area_component");
//            String assetIds=request.getParameter("assetIds");
//            String[]assetId=assetIds.split(",");
//            List<String> asset_id= Arrays.asList(assetId);
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                paramMap.put("schema_name", schemaName);
//                return monitorAreaComponentService.addMonitorAreaComponent(paramMap,asset_id);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/detail_monitorAreaComponent_view")
//    public ModelAndView detailMonitorAreaComponentView(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try{
//            Map<String,Object> monitorAreaComponent=monitorAreaComponentService.findMonitorAreaComponentById(request);
//            return new ModelAndView("monitor_area_component/monitor_area_component_detail").addObject("monitorAreaComponent", JSON.toJSON(monitorAreaComponent));
//        }catch (Exception e){
//            return new ModelAndView("monitor_area_component/monitor_area_component_detail");
//        }
//    }
//
//    /**
//     * 修改区域组件，处理业务
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/update_monitorAreaComponent")
//    public ResponseModel updateMonitorAreaComponent(@RequestParam Map<String, Object> paramMap,HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String assetIds=request.getParameter("assetIds");
//            String[]assetId=assetIds.split(",");
//            List<String> asset_id= Arrays.asList(assetId);
//            if (true) {
//                paramMap.put("schema_name", schemaName);
//                return monitorAreaComponentService.updateMonitorAreaComponent(paramMap,asset_id);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//    /**
//     * 删除区域组件
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response){
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try{
//            String id = request.getParameter("id");
//            if(StringUtils.isBlank(id)){
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.error(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//
//            }
//            return monitorAreaComponentService.deleteById(schemaName,request);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.error(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * 删除区域组件
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find_iot_asset")
//    @Transactional
//    public ResponseModel findIotAsset(HttpServletRequest request, HttpServletResponse response){
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try{
//            return monitorAreaComponentService.findIotAsset(schemaName,request);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.error(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//    /**
//     * 删除区域组件
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find_asset_by_component_id")
//    @Transactional
//    public ResponseModel findIotAssetByComponentId(HttpServletRequest request, HttpServletResponse response){
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try{
//            return monitorAreaComponentService.findIotAssetByComponentId(schemaName,request);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.error(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }


}

