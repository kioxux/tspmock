package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.StockService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "库房管理")
@RestController
public class StockController {
    @Resource
    StockService stockService;


    @ApiOperation(value = "获取库房管理模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_STOCK_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getStockListPermission", method = RequestMethod.POST)
    public ResponseModel searchStockListPermission(MethodParam methodParam) {
        return ResponseModel.ok(stockService.getStockListPermission(methodParam));
    }

    @ApiOperation(value = "查询库房列表", notes = ResponseConstant.RSP_DESC_SEARCH_STOCK_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","pageSize", "pageNumber","beginDateSearch","endDateSearch","keywordSearch"})
    @RequestMapping(value = "/searchStockList", method = RequestMethod.POST)
    public ResponseModel searchStockList(MethodParam methodParam, SearchParam param) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(stockService.getStockList(methodParam, param));
    }

    @ApiOperation(value = "新增库房", notes = ResponseConstant.RSP_DESC_ADD_STOCK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "stock_name", "manager_user_id", "address", "currency_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addStock", method = RequestMethod.POST)
    public ResponseModel addStock(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") StockAdd stockAdd) {
        stockService.newStock(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑库房", notes = ResponseConstant.RSP_DESC_EDIT_STOCK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,includeParameters = {"token","id", "stock_name", "manager_user_id", "address", "currency_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/editStock", method = RequestMethod.POST)
    public ResponseModel editStock(MethodParam methodParam,  @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") StockAdd stockAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        stockService.modifyStock(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取库房明细信息", notes = ResponseConstant.RSP_DESC_GET_STOCK_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchStockInfo", method = RequestMethod.POST)
    public ResponseModel searchStockInfo(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(stockService.getStockDetailById(methodParam, id));
    }

    @ApiOperation(value = "启用禁用库房", notes = ResponseConstant.RSP_DESC_CHANGE_USE_STOCK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"bomInfo"}, includeParameters = {"token", "id", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/editStockUse", method = RequestMethod.POST)
    public ResponseModel editStockUse(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") StockAdd stockAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        stockService.modifyStockUse(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除选中库房", notes = ResponseConstant.RSP_DESC_REMOVE_STOCK_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/removeSelectStock", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectStock(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        stockService.cutStockByIds(methodParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询库房库存清单列表", notes = ResponseConstant.RSP_DESC_SEARCH_STOCK_BOM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","pageSize", "pageNumber","keywordSearch","id"})
    @RequestMapping(value = "/searchStockBomList", method = RequestMethod.POST)
    public ResponseModel searchStockBomList(MethodParam methodParam, SearchParam param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String id) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(stockService.getStockBomList(methodParam, param));
    }

    @ApiOperation(value = "设置库房安全库存", notes = ResponseConstant.RSP_DESC_UPDATE_SAFE_STOCK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"bomInfo"}, includeParameters = {"token", "id", "security_quantity","max_security_quantity"})
    @Transactional //支持事务
    @RequestMapping(value = "/editSafeStock", method = RequestMethod.POST)
    public ResponseModel editSafeStock(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") BomAdd bomAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        stockService.modifySafeStock(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询库房出入库记录列表", notes = ResponseConstant.RSP_DESC_SEARCH_STOCK_IN_AND_OUT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","pageSize", "pageNumber","keywordSearch","id"})
    @RequestMapping(value = "/searchStockInAndOutList", method = RequestMethod.POST)
    public ResponseModel searchStockInAndOutList(MethodParam methodParam, SearchParam param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String id) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(stockService.getStockInAndOutList(methodParam, param));
    }

    @ApiOperation(value = "查询库房备件导入记录列表", notes = ResponseConstant.RSP_DESC_SEARCH_STOCK_BOM_IMPORT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","pageSize", "pageNumber","keywordSearch","id"})
    @RequestMapping(value = "/searchStockBomImportList", method = RequestMethod.POST)
    public ResponseModel searchStockBomImportList(MethodParam methodParam, SearchParam param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String id) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(stockService.getStockBomImportList(methodParam, param));
    }

//    //禁用库房
//    @MenuPermission("delete_stock")
//    @RequestMapping({"/change_stock"})
//    public JsonResult deleteStock(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String stock_code = request.getParameter("stock_code");
//            String statusWord = request.getParameter("status");
//            if (null == statusWord || statusWord.isEmpty()) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.TEXT_K));
//            }
//            int status = Integer.parseInt(statusWord) == 1 ? 0 : 1;
//
//            if (RegexUtil.isNull(stock_code)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.TEXT_K));
//            }
////            this.search(request, response); // 权限判断
//            if (stockService.deleteStockData(schema_name, stock_code, status) > 0) {
//                return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_FACI_SU));
//            } else {
//                return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_FACI_DE));
//            }
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_FACI_DE));
//        }
//    }
//
//    //维护库房
//    @MenuPermission("update_stock")
//    @RequestMapping({"/add_stock"})
//    public ModelAndView initManageStock(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String stockCode = request.getParameter("stock_code");
//        String stockName = "";
//        String address = "";
//        String manageAccount = "";
//        int currencyId = 0;
//        if (null != stockCode && !stockCode.isEmpty()) {
//            StockData stockData = stockService.findByStockCode(schema_name, stockCode);
//            if (stockData != null && null != stockData.getStock_code() && !stockData.getStock_code().isEmpty()) {
//                stockName = stockData.getStock_name();
//                address = stockData.getAddress();
//                manageAccount = stockData.getManager_account();
//                currencyId = stockData.getCurrency_id();
//            }
//        }
//
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        //备件费用查看权限获取
//        boolean feeBomRight = pagePermissionService.getPermissionByKey(schemaName, user.getId(), "bom", "bom_fee");
//        return new ModelAndView("stock/manage")
//                .addObject("stockCode", stockCode)
//                .addObject("stockName", stockName)
//                .addObject("address", address)
//                .addObject("manageAccount", manageAccount)
//                .addObject("currencyId", currencyId)
//                .addObject("feeBomRight",feeBomRight);
//    }
//
//    /**
//     * 查询库房关联用户组列表
//     */
//    @MenuPermission("update_stock")
//    @RequestMapping(value = "/stock_group_list")
//    public String getStockGroupList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        List<String> groupList = new ArrayList<String>();
//        try {
//            String stock_code = request.getParameter("stock_code");
//            if (!RegexUtil.isNull(stock_code)) {
//                StockData stockData = stockService.findByStockCode(schema_name, stock_code);
//                if (stockData != null && stockData.getUserGroupDataList() != null && stockData.getUserGroupDataList().size() > 0) {
//                    for (UserGroupData data : stockData.getUserGroupDataList()) {
//                        groupList.add(data.getId().toString());
//                    }
//                }
//            }
//            List<GroupModel> GroupList = groupService.getGroup(schema_name);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(GroupList);
//            SCTimeZoneUtil.responseObjectListDataHandler(groupList);
//            result.put("code", 1);
//            result.put("rows", GroupList);
//            result.put("groupList", groupList);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //获取组织下的用户
//    @MenuPermission("update_stock")
//    @RequestMapping(value = "/get_group_user_list")
//    public String getUserListByGroup(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String[] groupIds = request.getParameterValues("groupList[]");
//            String groups_ids = "";
//            if (groupIds != null) {
//                for (String id : groupIds) {
//                    if (groups_ids == null || groups_ids.isEmpty()) {
//                        groups_ids += id;
//                    } else {
//                        groups_ids += "," + id;
//                    }
//                }
//            }
//
//            List<User> groupUserList = groupService.getGroupUserList(schema_name, groups_ids);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(groupUserList);
//            result.put("code", 1);
//            result.put("userList", groupUserList);
//            return result.toString();
//        } catch (Exception ex) {
//            result.put("code", 0);
//            return result.toString();
//        }
//    }
//
//
//    //获取组织下的用户
//    @MenuPermission("update_stock")
//    @RequestMapping(value = "/stock_facility_list")
//    public ResponseModel getStockFacilityList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String stock_code = request.getParameter("stock_code");
//            if (!RegexUtil.isNull(stock_code)) {
//                StockData stockData = stockService.findByStockCode(schema_name, stock_code);
//                if (stockData != null && stockData.getFacilitiesList() != null && stockData.getFacilitiesList().size() > 0) {
//                    List<Facility> facilityDataList = stockData.getFacilitiesList();
//                    return ResponseModel.ok(facilityDataList);
//                }
//            }
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_DATA_FA));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_DATA_FA));
//        }
//    }
//
//    @MenuPermission("update_stock")
//    @RequestMapping({"/update_stock"})
//    public JsonResult updateStock(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            String stock_code = request.getParameter("stock_code");
//            String stock_name = request.getParameter("stock_name");
//            String currency_id = request.getParameter("currency_id");
//            boolean isAdd = false;
//            if (RegexUtil.isNull(stock_code)) {
//                stock_code = serialNumberService.generateMaxBusinessCode(schema_name, "stock_code");
//                isAdd = true;
//            } else {
//                StockData isExistData = stockService.findByStockCode(schema_name, stock_code);
//                if (null == isExistData || null == isExistData.getStock_code() || isExistData.getStock_code().isEmpty()) {
//                    return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_FACI_ERR));
//                }
//            }
//
//            if (RegexUtil.isNull(stock_name)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MSG_FACI_IS_NULL));
//            }
//            String manager_account = request.getParameter("manager_account");
//            String address = request.getParameter("address");
////            this.search(request, response);                           // 权限判断
//            StockData stockData = new StockData();
//            stockData.setStock_code(stock_code);
//            stockData.setStock_name(stock_name);
//            stockData.setManager_account(manager_account);
//            Timestamp timestamp = new Timestamp((new Date()).getTime());
//            stockData.setCreatetime(timestamp);
//            stockData.setCreate_user_account(user.getAccount());
//            stockData.setAddress(address);
//            if (RegexUtil.isNotNull(currency_id)) {
//                stockData.setCurrency_id(Integer.parseInt(currency_id));
//            }
//
//
//            //配置库房所属组织，以及使用位置
//            String userGroupIds = request.getParameter("user_group_ids");
//            if (null != userGroupIds && !userGroupIds.isEmpty()) {
//                List<UserGroupData> userGroupData = new ArrayList<>();
//                String[] userGroupIdList = userGroupIds.split(",");
//                for (String item : userGroupIdList) {
//                    if (null != item && !item.isEmpty()) {
//                        UserGroupData subItem = new UserGroupData();
//                        subItem.setId(Integer.parseInt(item));
//                        userGroupData.add(subItem);
//                    }
//                }
//                stockData.setUserGroupDataList(userGroupData);
//            }
//            String facilityIds = request.getParameter("facility_ids");
//            if (null != facilityIds && !facilityIds.isEmpty()) {
//                List<Facility> facilityList = new ArrayList<>();
//                String[] facilityIdList = facilityIds.split(",");
//                for (String item : facilityIdList) {
//                    if (null != item && !item.isEmpty()) {
//                        Facility subItem = new Facility();
//                        subItem.setId(Integer.parseInt(item));
//                        facilityList.add(subItem);
//                    }
//                }
//                stockData.setFacilitiesList(facilityList);
//            }
//
//            //执行新增，或者更新
//            int doResult;
//            if (isAdd) {
//                doResult = stockService.insertStockData(schema_name, stockData);
//            } else {
//                doResult = stockService.updateStockData(schema_name, stockData);
//            }
//
//            if (doResult > 0) {
//                return new JsonResult().success(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_FACI));
//            } else {
//                return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_FACI_FA));
//            }
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo(LangConstant.MSG_SUBMIT_FACI_FA));
//        }
//    }
//
//
//    @MenuPermission("update_stock")
//    @RequestMapping({"/", "/index_old"})
//    public ModelAndView selectStockData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), "stock");
//        //按用户权限，获取用户所能看见的位置
//        List<Facility> facilitiesList = facilitiesService.GetFacilityListByUser(schema_name, isAllFacility, user);
//        TreeBuilder treeBuilder = new TreeBuilder();
//        List<Node> allNodes = new ArrayList<Node>();
//        for (Facility item : facilitiesList) {
//            Node node = new Node();
//            node.setId(String.valueOf(item.getId()));
//            node.setParentId(String.valueOf(item.getParentId()));
//            node.setName(item.getTitle());
//            node.setDetail(item);
//            allNodes.add(node);
//        }
//        List<Node> roots = treeBuilder.buildListToTree(allNodes);
//        String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//        return new ModelAndView("stock/index")
//                .addObject("facilitiesSelect", facilitiesSelect);
//    }
//
////    @RequestMapping({"/search"})
////    public ModelAndView search(HttpServletRequest request, HttpServletResponse response) throws Exception {
////        String schema_name = authService.getCompany(request).getSchema_name();
////        PageInfo<StockData> result = null;
////        int page = 1, pageSize = 15;
////        try {
////            String strPage = request.getParameter("page");
////            if (RegexUtil.isNumeric(strPage)) {
////                page = Integer.valueOf(strPage);
////            }
////        } catch (Exception ex) {
////            page = 1;
////        }
////
////        String[] facilities = request.getParameterValues("facilities");
////        // 权限控制
////        String stockCode = (String) request.getParameter("stock_code");
////        if (null != stockCode && !"".equals(stockCode)) {
////            facilities = null;
////        }
////        if (facilities != null && facilities.length > 0) {
////            StringBuffer facilities_string = null;
////            for (String item : facilities) {
////                if (facilities_string == null) {
////                    facilities_string = new StringBuffer();
////                    facilities_string.append(item);
////                } else {
////                    facilities_string.append("," + item);
////                }
////            }
////            PageHelper.startPage(page, pageSize);
////            List<StockData> stockDataList = stockService.findByFacilities(schema_name, facilities_string.toString());
////            result = new PageInfo<StockData>(stockDataList);
////        } else {
////            List<StockData> stockDataList = null;
////            String condition = "";
////            User user = authService.getLoginUser(request);
////            //根据人员的角色，判断获取值的范围
////            Boolean isAllFacility = false;
////            Boolean isSelfFacility = true;
////            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "stock");
////            if (userFunctionList != null && !userFunctionList.isEmpty()) {
////                for (UserFunctionData functionData : userFunctionList) {
////                    //如果可以查看全部位置，则不限制
////                    if (functionData.getFunctionName().equals("stock_all_facility")) {
////                        isAllFacility = true;
////                        break;
////                    }
////                }
////            }
////            if (isAllFacility) {
////                condition = "  ";
////            } else if (isSelfFacility) {
////                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
////                String facilityIds = "";
////                if (facilityList != null && !facilityList.isEmpty()) {
////                    for (String key : facilityList.keySet()) {
////                        facilityIds += facilityList.get(key).getId() + ",";
////                    }
////                }
////                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
////                    facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
////                    condition = " and b.facility_id in (" + facilityIds + ") ";
////                } else {
////                    throw new Exception("权限不足！");
////                }
////            }
////            if (null != stockCode && !"".equals(stockCode)) {
////                condition += " and stock_code = '" + stockCode + "' ";
////            }
////            PageHelper.startPage(page, pageSize);
//////            stockDataList = stockService.findStockList(schema_name, condition);
////            if (null != stockCode && !"".equals(stockCode) && (null == stockDataList || 0 == stockDataList.size())) {
////                throw new Exception("权限不足！");
////            }
////            result = new PageInfo<StockData>(stockDataList);
////        }
////
////        return new ModelAndView("stock/search")
////                .addObject("result", result);
////    }
//
//    @RequestMapping("/stock_list_by_user_permission")
//    public ResponseModel getStockListByUserPermission(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            ResponseModel result = new ResponseModel();
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            List<StockData> stockDataList = stockService.getStockListByUserPermission(schema_name, "", user.getAccount());
//            result.setCode(ResultStatus.SUCCESS.getCode());
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_SUCC));
//            result.setContent(stockDataList);
//            return result;
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_FACILITY_LIST_FA));
//        }
//    }
}