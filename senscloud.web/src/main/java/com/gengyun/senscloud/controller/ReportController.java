package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.ReportMonthModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.SystemConfigConstant;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
import com.gengyun.senscloud.service.StatisticService;

//import com.gengyun.senscloud.service.system.SystemConfigService;
//import com.gengyun.senscloud.util.HttpRequestUtils;
//import com.gengyun.senscloud.util.ImageUtils;
//import org.json.JSONArray;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 统计报表
// */
//
//@RestController
//@RequestMapping("/report")
@Api(tags = "报告详情")
@RestController
public class ReportController {
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//
//    @Autowired
//    AuthService authService;
//
    @Resource
    StatisticService statisticService;

    //
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
    @ApiOperation(value = "查询报告列表", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "beginDate", "endDate", "pageSize", "pageNumber"})
    @RequestMapping(value = "/searchReportMonthList", method = RequestMethod.POST)
    public ResponseModel searchReportMonthList(MethodParam methodParam, ReportMonthModel reportMonthMdel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(statisticService.getReportDetailList(methodParam, reportMonthMdel));
    }
//    //前端显示可见到统计报表
//    @RequestMapping("/statistic_dash")
//    public ModelAndView statistic_dash() {
//        return new ModelAndView("report/statistic_dash");
//    }
//
//    //前端显示可见到统计报表,如果用户没有自定义的，则给其显示默认的4个
//    @RequestMapping("/get_self_statistic_dash_list")
//    public ResponseModel getUserDashBoard(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            ResponseModel result = new ResponseModel();
//            List<Map<String, Object>> userStatisticList = statisticService.getUserStatisticList(schema_name, loginUser.getAccount());
//            if (userStatisticList == null || userStatisticList.size() <= 0) {
//                userStatisticList = statisticService.getDefaultStatisticList(schema_name, 3);
//            }
//            if (userStatisticList != null && userStatisticList.size() > 0) {
//                result.setCode(1);
//                result.setContent(userStatisticList);
//            } else {
//                result.setCode(0);
//            }
//            return result;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    //添加用户的定制看板
//    @RequestMapping("/get_add_user_statistic_dash")
//    public ResponseModel getAddUserDashBoard(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            int dash_id = Integer.parseInt(request.getParameter("id"));
//            ResponseModel result = new ResponseModel();
//            if (statisticService.insertUserStatistic(schema_name, loginUser.getAccount(), dash_id) > 0) {
//                result.setCode(1);
//            } else {
//                result.setCode(0);
//            }
//            return result;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    //delete用户的定制看板
//    @RequestMapping("/get_delete_user_statistic_dash")
//    public ResponseModel getDeleteUserDashBoard(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            int dash_id = Integer.parseInt(request.getParameter("id"));
//            ResponseModel result = new ResponseModel();
//            if (statisticService.deleteUserStatistic(schema_name, loginUser.getAccount(), dash_id) > 0) {
//                result.setCode(1);
//            } else {
//                result.setCode(0);
//            }
//            return result;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//
//    //前端显示可见到统计报表
//    @RequestMapping("/statistic_list")
//    public ModelAndView statistic_list() {
//        return new ModelAndView("report/statistic_list");
//    }
//
//    //查询统计报表列表
//    @RequestMapping("/report_list")
//    public ModelAndView report_list(HttpServletRequest request) {
//        String id = request.getParameter("id");
//        if (null == id || id.isEmpty()) {
//            id = "";
//        }
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("statistic_all_facility", "allGroup");
//            put("statistic_self_facility", "selfGroup");
//            put("statistic_fee", "feeStaticRight");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "report/report_list", "statistic").addObject("id", id);
//    }
//
//    //查询统计报表图表
//    @RequestMapping("/report_bar")
//    public ModelAndView report_bar(HttpServletRequest request) {
//        String id = request.getParameter("id");
//        if (null == id || id.isEmpty()) {
//            id = "";
//        }
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("statistic_all_facility", "allGroup");
//            put("statistic_self_facility", "selfGroup");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "report/report_bar", "statistic").addObject("id", id);
//    }
//
//    /**
//     * 报告详情页面
//     * @param request
//     * @return
//     */
//    @RequestMapping("/analysisReportList")
//    public ModelAndView analysisReportList(HttpServletRequest request) {
//        try {
//            return new ModelAndView("report/analysis_report_list");
//        } catch (Exception e) {
//            return new ModelAndView("report/analysis_report_list");
//        }
//    }
//
//    /**
//     * 报告详情页面
//     * @param request
//     * @return
//     */
//    @RequestMapping("/analysisReportDetail")
//    public ModelAndView analysisReportDetail(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String year = request.getParameter("year");
//            String month = request.getParameter("month");
//            String reportLogo = "";
//            SystemConfigData systemConfigData = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.REPORT_LOGO);
//            if (null != systemConfigData) {
//                reportLogo = ImageUtils.getBase64Extension(systemConfigData.getSettingValue()) + ImageUtils.getImgStrToBase64(systemConfigData.getSettingValue());
//            }
//            JSONArray reportData = statisticService.getReportDetailDatas(schema_name, year, month);
//            return new ModelAndView("report/analysis_report_detail")
//                    .addObject("baseUrl", HttpRequestUtils.getBaseUrl())
//                    .addObject("reportLogo", reportLogo)
//                    .addObject("year", year)
//                    .addObject("month", month)
//                    .addObject("reportData", reportData.toString());
//        } catch (Exception e) {
//            return new ModelAndView("report/analysis_report_detail");
//        }
//    }
//
//    @RequestMapping("/analysisReportExport")
//    public void analysisReportExport(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            statisticService.analysisReportExport(schema_name, request, file_upload_dir, response);
//        } catch (Exception ex) {
//        }
//    }
}
