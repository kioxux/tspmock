package com.gengyun.senscloud.controller.snc;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorksModel;
import com.gengyun.senscloud.service.snc.SncDataImportService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "工单管理")
@RestController
public class SncDataImportController {

    @Resource
    SncDataImportService sncDataImportService;

    @ApiOperation(value = "维修工单打印pdf", notes = ResponseConstant.RSP_DESC_WORKS_ASSIGNMENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "sub_work_code"})
    @Transactional
    @RequestMapping(value = "/exportWorksPdf", method = RequestMethod.POST)
    public void exportWorksPdf(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                               @SuppressWarnings("unused") WorksModel worksModel) {
        sncDataImportService.exportWorksPdf(methodParam, paramMap);
    }
}
