package com.gengyun.senscloud.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.auth.MenuPermissionRule;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SqlConstant;
//import com.gengyun.senscloud.common.SystemConfigConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.FacilitiesResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//

import org.springframework.web.bind.annotation.RestController;

@RestController
public class FacilitiesConfigController {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/facilities")
//    public ModelAndView FacilitiesConfig(HttpServletRequest request, HttpServletResponse response) {
//        Boolean isCustomer = false;   //表示当前组织代表的是客户
//        Boolean organizationFlag = false; //是否从地址管理中获取地址信息
//        User loginUser = AuthService.getLoginUser(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ADDRESS_TYPE);//从系统配置中读取地址类型
//        if (null != dataList && "2".equals(dataList.getSettingValue())) {
//            organizationFlag = true;
//        }
//        boolean isAllFacility = pagePermissionService.getPermissionByKey(schema_name, loginUser.getId(), "facilities", "facilities_all_facility");
//        return new ModelAndView("organize_facilities/facilities_config").
//                addObject("isCustomer", isCustomer).
//                addObject("organizationFlag", organizationFlag).
//                addObject("isAllFacility", isAllFacility);
//
//    }
//
//    /**
//     * 按条件查询组织列表
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/find-facilities-list")
//    public ResponseModel queryworksheet(HttpServletRequest request, HttpServletResponse response) {
//
//        String condition = "and a.org_type in(" + SqlConstant.FACILITY_INSIDE + ") ";
//        return facilitiesService.FacilitiesListforTable(request, response, condition);
//    }
//
//    //组织详情
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/facilities_config_detail")
//    public ResponseModel FacilitiesDetail(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//
//            List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//
//            return ResponseModel.ok(facilitiesList);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC_LI);
//            return ResponseModel.error(tmpMsg);
//        }
//    }
//
//    /// <summary>
//    /// 获取所有位置,用于父位置
//    /// </summary>
//    /// <returns></returns>
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/facilities_parent_list")
//    public ResponseModel GacilitiesParentList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        List<Facility> facilitiesList = facilitiesService.FacilitiesList(schema_name);
//        return ResponseModel.ok(facilitiesList);
//    }
//
//    /**
//     * 添加组织界面
//     */
//    @MenuPermission("add_facilities")
//    @RequestMapping("/add_facilities_view")
//    public ModelAndView AddFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        Boolean isCustomer = false;//是否是厂商
//        Boolean organizationFlag = false;//是否从地址管理中获取地址信息
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ADDRESS_TYPE);//从系统配置中读取地址类型
//        if (null != dataList && "2".equals(dataList.getSettingValue())) {
//            organizationFlag = true;
//        }
//        return new ModelAndView("organize_facilities/add_facilities_config").addObject("isCustomer", isCustomer).addObject("organizationFlag", organizationFlag);
//    }
//
//    /**
//     * 组织详情
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/detail_facilities_view")
//    public ModelAndView DetailFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        Boolean isCustomer = false;         //是否是厂商
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        Boolean organizationFlag = false;//是否从地址管理中获取地址信息
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ADDRESS_TYPE);//从系统配置中读取地址类型
//        if (null != dataList && "2".equals(dataList.getSettingValue())) {
//            organizationFlag = true;
//        }
//
//        Facility facility_data = facilitiesService.FacilitiesById(schema_name, Integer.parseInt(id));
//
//        User loginUser = AuthService.getLoginUser(request);
//        boolean feeFacilityRight = pagePermissionService.getPermissionByKey(schema_name, loginUser.getId(), "facilities", "facilities_fee");
//        Map<String, Boolean> pagePermissionInfo = new HashMap<String, Boolean>();
//        pagePermissionInfo.put("feeFcyRight", feeFacilityRight);
//        pagePermissionInfo.put("orgCostTag", feeFacilityRight);
//        return new ModelAndView("organize_facilities/detail_facilities_config")
//                .addObject("pagePermissionInfo", net.sf.json.JSONObject.fromObject(pagePermissionInfo))
//                .addObject("facility_data", JSON.toJSON(facility_data))
//                .addObject("isCustomer", isCustomer)
//                .addObject("organizationFlag", organizationFlag);
////                .addObject("currency_code_name", systemConfigService.getCurrencyName(schema_name))
//    }
//
//    /**
//     * 添加组织
//     */
//    @MenuPermission("add_facilities")
//    @RequestMapping(value = "/add_facilities")
//    public ResponseModel InsertFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        return facilitiesService.InsertFacilitiesData(request, response);
//    }
//
//    /**
//     * 添加或修改组织联系人
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_contact")
//    public ResponseModel InsertOrganizationContact(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            User user = AuthService.getLoginUser(request);
//            Company company = authService.getCompany(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//            OrganizationContact ocdata = new OrganizationContact();
//            String idval = request.getParameter("id");
//            boolean isneedReturn = false;
//            if (!RegexUtil.isNull(idval)) {
//                int id = Integer.parseInt(idval);
//                ocdata.setId(id);
//
//            } else {
//                ocdata.setCreate_time(new Timestamp(System.currentTimeMillis()));
//            }
//            String org_idval = request.getParameter("org_id");
//            if (!RegexUtil.isNull(org_idval)) {
//                int org_id = Integer.parseInt(org_idval);
//                ocdata.setOrg_id(org_id);
//            } else {
//                ocdata.setOrg_id(0);
//                isneedReturn = true;
//            }
//            String contact_name = request.getParameter("contact_name");
//            ocdata.setContact_name(contact_name);
//            ocdata.setSchema_name(schema_name);
//            String contact_mobile = request.getParameter("contact_mobile");
//            ocdata.setContact_mobile(contact_mobile);
//            String contact_department = request.getParameter("contact_department");
//            ocdata.setContact_department(contact_department);
//            String contact_position = request.getParameter("contact_position");
//            ocdata.setContact_position(contact_position);
//            String contact_email = request.getParameter("contact_email");
//            ocdata.setContact_email(contact_email);
//            String weixin = request.getParameter("weixin");
//            ocdata.setWeixin(weixin);
//            String facebook = request.getParameter("facebook");
//            ocdata.setFacebook(facebook);
//            String whatsapp = request.getParameter("whatsapp");
//            ocdata.setWhatsapp(whatsapp);
//            String is_main = request.getParameter("is_main");
//            ocdata.setIs_main(Integer.valueOf(is_main));
//            String remark = request.getParameter("remark");
//            ocdata.setRemark(remark);
//            ocdata.setCreate_user_account(user.getAccount());
//            int chageid = 0;
//            if (RegexUtil.isNull(idval)) {
//                facilitiesService.InsertOrganizationContact(ocdata);
//                chageid = ocdata.getId();
//            } else {
//                facilitiesService.UpdateOrganizationContact(ocdata);
//            }
//            if (!isneedReturn) {
//                logService.AddLog(schema_name, "facilities", org_idval, selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_LINK_MAN) + contact_name, user.getAccount());
//            }
//            result.setCode(5);
//            if (RegexUtil.isNull(idval)) {
//                result.setContent(chageid);
//            } else {
//                result.setContent(Integer.parseInt(idval));
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_LINK_ERR));
//            return result;
//        }
//    }
//
//    /**
//     * 添加组织文件
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_doc")
//    public ResponseModel InsertOrganizationDoc(HttpServletRequest request, HttpServletResponse response) {
//        User user = AuthService.getLoginUser(request);
//        Company company = authService.getCompany(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//        OrganizationDoc docdata = new OrganizationDoc();
//        ResponseModel result = new ResponseModel();
//        try {
//            String idval = request.getParameter("file_id").trim();
//            if (RegexUtil.isNull(idval)) {
//                result.setCode(-1);
//                result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_FILE_ERR));
//                return result;
//            }
//            String org_idval = request.getParameter("org_id");
//            if (!RegexUtil.isNull(org_idval)) {
//                int org_id = Integer.parseInt(org_idval);
//                docdata.setOrg_id(org_id);
//            } else {
//                docdata.setOrg_id(0);
//
//            }
//            String remark = request.getParameter("remark");
//            docdata.setRemark(remark);
//            docdata.setSchema_name(schema_name);
//            docdata.setCreate_user_account(user.getAccount());
//            docdata.setCreate_time(new Timestamp(System.currentTimeMillis()));
//            if (idval.indexOf(',') > -1) {
//                String[] ids = idval.split(",");
//                for (String id : ids) {
//                    docdata.setFile_id(Integer.parseInt(id));
//                    facilitiesService.InsertOrganizationDoc(docdata);
//                }
//
//            } else {
//                docdata.setFile_id(Integer.parseInt(idval));
//                facilitiesService.InsertOrganizationDoc(docdata);
//            }
//            if (!RegexUtil.isNull(org_idval)) {
//                logService.AddLog(schema_name, "facilities", org_idval, selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_FILE_DES) + remark, user.getAccount());
//            }
//            result.setCode(6);
//            result.setContent(idval);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_LINK_ERR));
//            return result;
//        }
//    }
//
//    /**
//     * 查询组织文件列表
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_doc_list")
//    public String FindOrganizationDoc(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String org_id = request.getParameter("org_id");
//            if (RegexUtil.isNull(org_id)) {
//                return "{}";
//            }
//            List<OrganizationDoc> docsList = facilitiesService.DocListByOrgid(schema_name, Integer.parseInt(org_id));
//
//            result.put("rows", docsList);
//            result.put("code", 1);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询组织联系人列表
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_contact_list")
//    public String FindOrganizationContact(HttpServletRequest request, HttpServletResponse response) {
//        User user = AuthService.getLoginUser(request);
//        Company company = authService.getCompany(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String org_idval = request.getParameter("org_id");
//            if (RegexUtil.isNull(org_idval)) {
//                return "{}";
//            }
//            List<OrganizationContact> contactList = facilitiesService.ContactListByOrgid(schema_name, Integer.parseInt(org_idval));
//            result.put("rows", contactList);
//            result.put("total", 10);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询组织关联用户组列表
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_group_list")
//    public String FindOrganizationGroup(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        List<Long> groupList = new ArrayList<>();
//        try {
//            String org_idval = request.getParameter("org_id");
//            if (!RegexUtil.isNull(org_idval)) {
//                groupList = facilitiesService.findGroupidsforOrganization(schema_name, Integer.parseInt(org_idval));
//            }
//            List<GroupModel> GroupList = facilitiesService.findGroupforOrganization(schema_name);
//            result.put("rows", GroupList);
//            result.put("code", 1);
//            result.put("groupList", groupList);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 删除组织文件
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_doc_delete")
//    public ResponseModel DeleteOrganizationDoc(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String id = request.getParameter("id").replace(",", "").trim();
//            if (RegexUtil.isNull(id)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//            }
//            int i = facilitiesService.DeleteDoc(schema_name, Integer.parseInt(id));
//            if (i > 0) {
//                return ResponseModel.ok("success！");
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//            }
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//        }
//    }
//
//    /**
//     * 删除组织联系人
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_contact_delete")
//    public ResponseModel DeleteOrganizationContact(HttpServletRequest request, HttpServletResponse response) {
//        User user = AuthService.getLoginUser(request);
//        Company company = authService.getCompany(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//
//        JSONObject result = new JSONObject();
//        try {
//            String id = request.getParameter("id");
//            if (RegexUtil.isNull(id)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//            }
//            int i = facilitiesService.DeleteContact(schema_name, Integer.parseInt(id));
//            if (i > 0) {
//                return ResponseModel.ok("success！");
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//            }
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//        }
//    }
//
//    /**
//     * 删除组织关联的用户组
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/organization_group_update")
//    public ResponseModel DeleteOrganizationGroup(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String id = request.getParameter("id");
//            if (RegexUtil.isNull(id)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//            }
//            facilitiesService.DeleteGroup(schema_name, Integer.parseInt(id));
//            String groupList = request.getParameter("group_list");
//
//            if (RegexUtil.isNull(groupList)) {
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_SUCCESS));
//            }
//            String[] group_ids = groupList.split(",");
//            for (String group_id : group_ids) {
//                facilitiesService.AddOrganizationGroup(schema_name, Integer.parseInt(group_id), Integer.parseInt(id));
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_SUCCESS));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_ID_NOT_NULL));
//        }
//    }
//
//    /**
//     * 查询组织的设备列表
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/facilities_asset_list")
//    public String getOrganizationAssetList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String org_id = request.getParameter("org_id");
//            if (RegexUtil.isNull(org_id)) {
//                return "{}";
//            }
//            String pageSizeStr = request.getParameter("pageSize");
//            String pageNumberStr = request.getParameter("pageNumber");
//            int pageSize = 0;
//            int pageNumber = 0;
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//            int begin = pageSize * (pageNumber - 1);
//
//            List<Asset> assetList = facilitiesService.getAssetListByOrgid(schema_name, Integer.parseInt(org_id), pageSize, begin);
//            int total = facilitiesService.getAssetListCountByOrgid(schema_name, Integer.parseInt(org_id));
//            result.put("rows", assetList);
//            result.put("total", total);
//            result.put("code", 1);
//
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询组织的工单列表
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/facilities_work_list")
//    public String getOrganizationWorkList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String org_id = request.getParameter("org_id");
//            if (RegexUtil.isNull(org_id)) {
//                return "{}";
//            }
//            String pageSizeStr = request.getParameter("pageSize");
//            String pageNumberStr = request.getParameter("pageNumber");
//            int pageSize = 0;
//            int pageNumber = 0;
//            if (!RegexUtil.isNull(pageSizeStr)) {
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
//            if (!RegexUtil.isNull(pageNumberStr)) {
//                pageNumber = Integer.parseInt(pageNumberStr);
//            }
//            int begin = pageSize * (pageNumber - 1);
//
//            List<WorkSheet> workList = facilitiesService.getWorkListByOrgid(schema_name, Integer.parseInt(org_id), pageSize, begin);
//            Map<String,Object> polymericMap=facilitiesService.getWorkListCountByOrgid(schema_name, Integer.parseInt(org_id));
//            WorkSheet polymeric=new WorkSheet();
//            polymeric.setWork_code(selectOptionService.getLanguageInfo( LangConstant.TOTAL_T));
//            if(RegexUtil.isNotNull(polymericMap)&&polymericMap.containsKey("fee")){
//                polymeric.setFee_amount_currency(String.valueOf(polymericMap.get("fee")));
//            }
//            workList.add(polymeric);
//            int total=0;
//            if(RegexUtil.isNotNull(polymericMap)&&polymericMap.containsKey("total")){
//                total = Integer.parseInt(String.valueOf(polymericMap.get("total")));
//            }
//            //时区转换处理
//            SCTimeZoneUtil.responseObjectListDataHandler(workList);
//            result.put("rows", workList);
//            result.put("total", total);
//            result.put("code", 1);
//
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询组织的费用
//     */
//    @MenuPermission(value = {"client_manage", "facilities", "org_customer"}, rule = MenuPermissionRule.OR)
//    @RequestMapping(value = "/facilities_fee_list")
//    public String getOrganizationFeeList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String org_id = request.getParameter("org_id");
//            if (RegexUtil.isNull(org_id)) {
//                return "{}";
//            }
//            Facility facility = facilitiesService.FacilitiesById(schema_name, Integer.parseInt(org_id));
//            List<Map<String, Object>> feeList = facilitiesService.getFeeListByOrgNO(schema_name, facility.getFacilityNo() + "%");
//            result.put("rows", feeList);
//            result.put("code", 1);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 更新位置
//     */
//    @MenuPermission("update_facilities")
//    @RequestMapping(value = "/update_facilities")
//    public ResponseModel UpdateFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        return facilitiesService.UpdateFacilitiesData(request, response);
//    }
//
//    /**
//     * 更新位置的地图
//     */
//    @MenuPermission("update_facilities")
//    @RequestMapping(value = "/update_facility_laypathid")
//    public ResponseModel UpdateFacilityLaypath(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            Company company = authService.getCompany(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Integer id = Integer.parseInt(request.getParameter("id"));
//            String file_id = request.getParameter("files_id");
//            if (id <= 0 || RegexUtil.isNull(file_id)) {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.INCORRECT_DATA_K));
//            }
//            int count = facilitiesService.UpdateFacilityLaypath(schema_name, id, file_id);
//            if (count > 0) {
//                FacilitiesResult facilitiesDataById = facilitiesService.FacilitiesListById(schema_name, Integer.valueOf(id));
//                User loginUser = authService.getLoginUser(request);
//                //增加日志
//                logService.AddLog(schema_name, "facilities", facilitiesDataById.getTitle(), selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_MAO_LO) + facilitiesDataById.getTitle(), loginUser.getAccount());
//                return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_LO_SU));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_LO_FA));
//            }
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_LO_FA));
//        }
//    }
//
//    /**
//     * 删除位置
//     */
//    @MenuPermission("delete_facilities")
//    @RequestMapping(value = "/delete_facilities")
//    public ResponseModel DeleteFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            User user = AuthService.getLoginUser(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            int id = Integer.valueOf(request.getParameter("id"));
//            boolean isuse = Boolean.parseBoolean(request.getParameter("isuse"));
//            facilitiesService.DeleteFacilitiesData(schema_name, id, !isuse);
//            //增加日志
//            logService.AddLog(schema_name, "facilities", String.valueOf(id), selectOptionService.getLanguageInfo(LangConstant.MSG_DELE_LOCA_CODE) + String.valueOf(id), user.getAccount());
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_DELE_LOCA));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_DELE_LOCA_FA));
//        }
//    }
//
//    /**
//     * 启用禁用位置
//     */
//    @MenuPermission("delete_facilities")
//    @RequestMapping(value = "/restore_facility")
//    public ResponseModel restoreFacility(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            User user = AuthService.getLoginUser(request);
//            String schema_name = authService.getCompany(request).getSchema_name();
//            int id = Integer.valueOf(request.getParameter("id"));
//            facilitiesService.restoreFacility(schema_name, id);
//            //增加日志
//            logService.AddLog(schema_name, "facilities", String.valueOf(id), selectOptionService.getLanguageInfo(LangConstant.MSG_REP_LOCA_CODE) + String.valueOf(id), user.getAccount());
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_REP_LOCA_SU));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_REP_LOCA_FA));
//        }
//    }
//
//    /**
//     * 获取位置类型
//     */
//    @RequestMapping(value = "/get_facilitytype")
//    public ResponseModel GetFacilitiesTypeData(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Facility facilitiesData = new Facility();
//            List<FacilityType> FacilityTypeList = facilitiesService.FacilityTypeList(schema_name);
//            return ResponseModel.ok(FacilityTypeList);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_LOCA_FA));
//        }
//    }
//
//    /**
//     * 获取省市区
//     */
//    @RequestMapping(value = "/get_facilityarea")
//    public ResponseModel GetFacilitiesAreaData(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Facility facilitiesData = new Facility();
//
//            String parentcode = request.getParameter("parentcode");
//            List<AreaData> FacilityAreaCode = facilitiesService.FacilityAreaCode(schema_name, parentcode);
//            return ResponseModel.ok(FacilityAreaCode);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_CITY_FA));
//        }
//    }
//
//    /**
//     * 详细定义
//     */
//    @MenuPermission("update_facilities")
//    @RequestMapping(value = "/facilities_detail_id")
//    public ResponseModel GetFacilitiesDetailById(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            FacilitiesResult facilitiesDataById = facilitiesService.FacilitiesListById(schema_name, Integer.valueOf(id));
//            return ResponseModel.ok(facilitiesDataById);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_CITY_FA));
//        }
//    }
//
//    /**
//     * 获得选择位置的信息
//     */
//    @RequestMapping("find_facility_by_id")
//    public ResponseModel findFacilityById(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = AuthService.getLoginUser(request);
//        try {
//            Integer id = Integer.parseInt(request.getParameter("id"));
//            FacilitiesResult facilities = facilitiesService.FacilitiesListById(schema_name, id);
//            return ResponseModel.ok(facilities);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
//
//    /**
//     * 获得选择位置的信息
//     */
//    @MenuPermission("update_facilities")
//    @RequestMapping("/facilities_log_list")
//    public String FindALLAssetLogs(HttpServletRequest request, HttpServletResponse response) {
//
//        JSONObject result = new JSONObject();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("org_id");
//            List<LogsData> dataList = logService.findAllLogsList(schema_name, "facilities", id);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList, new String[]{"create_time"});
//            result.put("rows", dataList);
//            result.put("code", 1);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//
//        }
//    }
//
//    /**
//     * 获取所有有效的位置
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get-all-use-facilities-with-type")
//    public ResponseModel getFacilitiesInUse(HttpServletRequest request, HttpServletResponse response) {
//        String condition = " and a.isuse=true ";
//        String keyWord = request.getParameter("keyWord");
//        String orgType = request.getParameter("org_type_inner");
//        if (null != keyWord && !"".equals(keyWord)) {
//            condition += " and (upper(a.title) like upper('%" + keyWord + "%') or upper(a.areacode) like upper('%" + keyWord + "%') or upper(c.address_name) like upper('%" + keyWord + "%'))";
//        }
//        if (null != orgType && !"".equals(orgType) && "inner".equals(orgType)) {
//            condition += " and a.org_type in (" + SqlConstant.FACILITY_INSIDE + ") ";
//        }
//        return facilitiesService.FacilitiesListforTable(request, response, condition);
//
//    }
}