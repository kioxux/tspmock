package com.gengyun.senscloud.controller.bulletinBoard;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 科华公告
 */
@RestController
@RequestMapping("/arrangement")
public class ArrangementController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    ArrangementService arrangementService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("arrangement_add", "agAddFlag");
//            put("arrangement_edit", "agEditFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bulletin_board/arrangement/index", "arrangement");
//    }
//
//    /**
//     * 查询列表页列表数据（分页）
//     *
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-arrangement-list")
//    public String queryArrangementList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                       @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                       @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                       @RequestParam(name = "sortName", required = false) String sortName,
//                                       @RequestParam(name = "sortOrder", required = false) String sortOrder, @RequestParam("beginTime") String beginTime, @RequestParam("endTime") String endTime,
//                                       @Param("deliverable_type") String deliverable_type,
//                                   HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = arrangementService.query(schemaName, searchKey, pageSize, pageNumber, sortName, sortOrder,beginTime,endTime,deliverable_type);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/addArrangement")
//    @Transactional //支持事务
//    public ResponseModel addArrangement(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "arrangement", "arrangement_add");
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return arrangementService.addArrangement(request, loginUser, schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//无权限 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//    /**
//     * 进入编辑页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/editView")
//    public ModelAndView editView(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        if(null!=id&&!"".equals(id)){
//            return new ModelAndView("bulletin_board/arrangement/edit_arrangement").addObject("arrangement", JSON.toJSON(arrangementService.queryArrangementById(schema_name,Integer.parseInt(id))));
//        }else{
//            return new ModelAndView("bulletin_board/arrangement/edit_arrangement");
//        }
//    }
//
//    /**
//     * 更新数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/editArrangement")
//    @Transactional //支持事务
//    public ResponseModel editArrangement(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return arrangementService.updateArrangement(request, loginUser, schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/deleteArrangement")
//    @Transactional //支持事务
//    public ResponseModel deleteArrangement(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return arrangementService.deleteById(schemaName, request, loginUser);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//                return ResponseModel.error(tmpMsg);//失败
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
}
