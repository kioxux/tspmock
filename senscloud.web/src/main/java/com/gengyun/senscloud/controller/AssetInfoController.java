package com.gengyun.senscloud.controller;

///*
//* 集中处理设备信息控制器：设备的BOM、维修记录、性能指标等
//*/
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.mapper.AssetInfoMapper;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.AnalysRepairWorkResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/asset_info")
public class AssetInfoController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    AssetInfoService assetInfoService;
//    @Autowired
//    BomService bomService;
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    MaintainService maintainService;
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//    @Autowired
//    RepairService repairService;
//    @Autowired
//    AnalysReportService analysReportService;
//    @Autowired
//    WorkSheetService workSheetService;
//    @Autowired
//    PlanWorkService planWorkService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    PartService partService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    AssetInfoMapper mapper;
//
//    /**
//     * 初始化设备备件
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_bom_init")
//    public ModelAndView InitAssetBomManage(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            result.setContent(null);
//            result.setCode(1);
//            result.setMsg("");
//            List<BomData> bomList = bomService.getBomDataDistinctDistinct(schema_name);
//            //多时区
//            SCTimeZoneUtil.responseObjectListDataHandler(bomList);
//            return new ModelAndView("asset_info/asset_bom_manage").addObject("result", result)
//                    .addObject("bomList", bomList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_BOM_ERR) + ex.getMessage());
//            return new ModelAndView("asset_info/asset_bom_manage").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得所有的设备备件
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_bom_list")
//    public ModelAndView FindALLAssetBomList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_id = request.getParameter("asset_id");
//
//            List<AssetBomData> dataList = assetInfoService.findAllAssetBomList(schema_name, asset_id);
//            List<AssetBomData> repairdata = assetInfoService.findAllAssetRepairBomList(schema_name, asset_id);
//            Map<String, Object> polymericMap = mapper.findAllAssetRepairBomPolymeric(schema_name, asset_id);
//            AssetBomData polymeric = new AssetBomData();
//            polymeric.setBom_name(selectOptionService.getLanguageInfo(LangConstant.TOTAL_T));
//            if (RegexUtil.isNotNull(polymericMap) && polymericMap.containsKey("fee")) {
//                polymeric.setConsume_fee(String.valueOf(polymericMap.get("fee")));
//            }
//            repairdata.add(polymeric);
//            User user = AuthService.getLoginUser(request);
//            boolean amFeeShow = pagePermissionService.getPermissionByKey(schema_name, user.getId(), "asset_data_management", "asset_data_management_fee_show");
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));
//            result.setCode(1);
//            result.setMsg("");
//            //时区转换处理
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList);
//            return new ModelAndView("asset_info/asset_bom_list")
//                    .addObject("amFeeShow", amFeeShow)
//                    .addObject("result", result)
//                    .addObject("dataList", dataList)
//                    .addObject("selectOptionService", selectOptionService)
//                    .addObject("repairdata", repairdata);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_BOM_ERR) + ex.getMessage());
//            return new ModelAndView("asset_info/asset_bom_list").addObject("result", result);
//        }
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_bom_edit")
//    public ResponseModel getEditAssetBom(HttpServletRequest request, HttpServletResponse response) {
//        //获取设备备件信息
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("edit_id"));
//        if (id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_DATA_BOM));
//        } else {
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            //获取设备备件
//            AssetBomData data = assetInfoService.findAssetBom(schema_name, id);
//            result.setContent(data);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_BOM_SUCC));
//        }
//        //返回结果
//        return result;
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_bom_save")
//    public ResponseModel getAddAssetBom(HttpServletRequest request, HttpServletResponse response) {
//        //保存设备备件
//        ResponseModel result = new ResponseModel();
//        try {
//            String asset_id = request.getParameter("asset_id");
//            Integer id = Integer.parseInt(request.getParameter("edit_id"));
//            String bomModel = request.getParameter("bom_mode");
//            String bomCode = request.getParameter("bom_code");
//            String materialCode = request.getParameter("material_code");
////            String part_id = request.getParameter("part_id");
//            String use_days = request.getParameter("use_days");
//            float useDays = 0;
//            if (null != use_days && !use_days.isEmpty()) {
//                useDays = Float.parseFloat(use_days);
//            }
//            float maintenanceDays = 0;// 保养周期
//            String maintenance_days = request.getParameter("maintenance_days");
//            if (null != maintenance_days && !maintenance_days.isEmpty()) {
//                maintenanceDays = Float.parseFloat(maintenance_days);
//            }
//            int unitType = 2; // 1:小时；2：天
//            String unit_type = request.getParameter("unit_type");
//            if (null != unit_type && !unit_type.isEmpty()) {
//                unitType = Integer.parseInt(unit_type);
//            }
//
//            if (RegexUtil.isNull(asset_id)) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//获取参数失败
//            } else if (StringUtils.isBlank(bomCode)) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.PLEASE_SELECT_BOM));//请选择备件
//            } else if (StringUtils.isNotBlank(bomCode) && (RegexUtil.isNull(bomModel) || RegexUtil.isNull(materialCode))) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_BOM_DATA_WRONG));// 备件数据错误
//            } else {
//                Company company = authService.getCompany(request);
//                String schema_name = company.getSchema_name();
//                User user = authService.getLoginUser(request);
//                String checkResult = assetDataServiceV2.checkAssetList(request, user, schema_name, asset_id);
//                if (RegexUtil.isNull(checkResult)) {
//                    result.setCode(-1);
//                    String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                    result.setMsg(tmpMsg);
//                    return result;
//                }
//
//                //看看是否有重复，如果有重复，则执行更新操作,
//                AssetBomData list = assetInfoService.findAssetBom(schema_name, id);
////                Long partId = StringUtils.isBlank(part_id) ? null : Long.parseLong(part_id);
//                //保存资产备件
//                int count = 0;
//                if (list == null || list.getBom_code() == null) {
//                    Asset asset = assetService.findByID(schema_name, asset_id);
//                    count = assetInfoService.AddAssetBom(schema_name, asset.getStrcode(), asset_id, bomModel, bomCode, materialCode,
//                            useDays, maintenanceDays, unitType, user.getAccount());
//                } else {
//                    count = assetInfoService.UpdateAssetBom(schema_name, bomModel, bomCode, materialCode, useDays, maintenanceDays, unitType, id);
//                }
//                if (count > 0) {
//                    //增加日志
//                    logService.AddLog(schema_name, "asset", asset_id, selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_BOM) + bomModel, user.getAccount());
//                    result.setCode(1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_AS_BOM_S));
//                } else {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_AS_BOM_D));
//                }
//            }
//            //返回保存结果
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_AS_BOM_D));
//            return result;
//        }
//
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_bom_delete")
//    public ResponseModel getDeleteAssetBom(HttpServletRequest request, HttpServletResponse response) {
//        //删除资产备件
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("id"));
//        if (id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASS_BOM_CH));
//        } else {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            //删除资产备件
//            AssetBomData data = assetInfoService.findAssetBom(schema_name, id);
//            if (null == data || RegexUtil.isNull(data.getAsset_id())) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASS_NOT));
//                return result;
//            }
//            String checkResult = assetDataServiceV2.checkAssetList(request, user, schema_name, data.getAsset_id());
//            if (RegexUtil.isNull(checkResult)) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                result.setMsg(tmpMsg);
//                return result;
//            }
//
//            int count = assetInfoService.DeleteAssetBom(schema_name, id);
//            if (count > 0) {
//                //增加日志
//                logService.AddLog(schema_name, "asset", data.getAsset_id(), selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_BOM_MODEL) + data.getBom_model(), user.getAccount());
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_BOM_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_BOM_DE));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    //获取设备维修和保养记录
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_repair_maintain_list")
//    public ModelAndView getAssetRepairAndMaintainList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_id = request.getParameter("asset_id");
////            Asset asset = assetService.findByID(schema_name, asset_id);
//            String repairconfig = "and (t.business_type_id=1) and (dv._id='" + asset_id + "')";
//            String maintainconfig = "and (t.business_type_id=2) and (dv._id='" + asset_id + "')";
//
//            List<WorkSheet> repairList = workSheetService.getWorkSheetListforcalendar(schema_name, repairconfig);
//            List<WorkSheet> maintainList = workSheetService.getWorkSheetListforcalendar(schema_name, maintainconfig);
//            //时区转换处理
//            SCTimeZoneUtil.responseObjectListDataHandler(repairList);
//            //时区转换处理
//            SCTimeZoneUtil.responseObjectListDataHandler(maintainList);
//
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));
//            result.setCode(1);
//            result.setMsg("");
//
//            return new ModelAndView("asset_info/asset_repair_maintain_list")
//                    .addObject("result", result)
//                    .addObject("repairList", repairList)
//                    .addObject("maintainList", maintainList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.ERROR_ASSET_MAINTAIN_A) + ex.getMessage());
//            return new ModelAndView("asset_info/asset_repair_maintain_list").addObject("result", result);
//        }
//    }
//
//    //获取设备的性能统计
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_perfamance")
//    public ModelAndView getAssetAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_id = request.getParameter("asset_id");
//
//            //统计设备的平均维修时间，故障间隔时间
//            //时效结果
//            AnalysRepairWorkResult repairHour = analysReportService.GetRepairHourAndTimesByAssetId(schema_name, asset_id);
//            //故障间隔
//            AnalysRepairWorkResult repairIntervalHour = analysReportService.GetRepairHourIntervalByAssetId(schema_name, asset_id);
//
//            result.setContent("success");
//            result.setCode(1);
//            result.setMsg("");
//
//            return new ModelAndView("asset_info/asset_perfamance").addObject("result", result).addObject("repairHour", repairHour).addObject("repairIntervalHour", repairIntervalHour);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.ERROR_ASSET_MAINTAIN_A) + ex.getMessage());
//            return new ModelAndView("asset_info/asset_perfamance").addObject("result", result);
//        }
//    }
//
//    //设备监控视图
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_monitor_chart")
//    public ModelAndView getAssetMonitor(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            result.setContent("success");
//            result.setCode(1);
//            result.setMsg("");
//            return new ModelAndView("asset_info/asset_monitor_chart").addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.ERROR_ASSET_MAINTAIN_A) + ex.getMessage());
//            return new ModelAndView("asset_info/asset_monitor_chart").addObject("result", result);
//        }
//    }
//
//    //获取设备的监控数据
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_monitor_chart_data")
//    public ResponseModel getAssetMonitorDataList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String caption = "";
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String timeInterval = request.getParameter("timeInterval");
//            String asset_code = request.getParameter("asset_code");
//            Date time = new Date();
//            SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FMT_DATE_2);
//            Calendar curr = Calendar.getInstance();
//
//            if (timeInterval == null) {
//                String day = df.format(time);
//                caption = " and T.gather_time > '" + day + " 00:00:00'";
//            } else if (timeInterval.equals("day")) {
//                String day = df.format(time);
//                caption = " and T.gather_time > '" + day + " 00:00:00'";
//            } else if (timeInterval.equals("twoDay")) {
//                curr.set(Calendar.DAY_OF_MONTH, curr.get(Calendar.DAY_OF_MONTH) - 1);
//                Date week = curr.getTime();
//                String weekTime = df.format(week);
//                caption = " and T.gather_time >'" + weekTime + " 00:00:00'";
//            } else if (timeInterval.equals("threeDay")) {
////                curr.set(Calendar.MONTH,curr.get(Calendar.MONTH)-1);
//                curr.set(Calendar.DAY_OF_MONTH, curr.get(Calendar.DAY_OF_MONTH) - 2);
//                Date month = curr.getTime();
//                String monthTime = df.format(month);
//                caption = " and T.gather_time >'" + monthTime + " 00:00:00'";
//            }
//            List<AssetMonitorData> dataList = assetInfoService.findAllAssetMonitorList(schema_name, asset_code, caption);//默认执行
//
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.ERROR_ASSET_MAINTAIN_A) + ex.getMessage());
//            return result;
//        }
//    }
//
//
//    //获取设备的监控数据
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_monitor_chart_data_History")
//    public ResponseModel getAssetMonitorHistoryDataList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_code = request.getParameter("asset_code");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            beginTime = beginTime.replace("-", "/") + " 00:00:00";
//            endTime = endTime.replace("-", "/") + " 23:59:59";
//            List<AssetMonitorData> dataList = assetInfoService.getAssetMonitorHistoryDataList(schema_name, asset_code, beginTime, endTime);
//
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.ERROR_ASSET_MAINTAIN_A) + ex.getMessage());
//            return result;
//        }
//    }
//
//    @RequestMapping("/findByCode")
//    public ResponseModel findByCode(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//
//            String assetCode = request.getParameter("assetCode");
//            List<Asset> assetList = assetDataServiceV2.findByCode(schema_name, assetCode);
//            Asset asset = assetList.get(0);
//            result.setContent(asset);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEA_ASS_SU));
//            return result;
//        } catch (Exception e) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEA_ASS_DE));
//            return result;
//        }
//    }
//
//    //获取设备的维保费用数据
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_cost")
//    public String getAssetFeeList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        try {
//            String asset_id = request.getParameter("asset_id");
//            if (RegexUtil.isNull(asset_id)) {
//                result.put("code", 0);
//                return result.toString();
//            }
//            List<Map<String, Object>> feeList = assetInfoService.getAssetFeeListByAssetid(schema_name, asset_id);
//            //多时区
//            SCTimeZoneUtil.responseMapListDataHandler(feeList);
//            result.put("rows", feeList);
//            result.put("code", 1);
//            return result.toString();
//        } catch (Exception ex) {
//            result.put("code", 0);
//            return result.toString();
//        }
//    }
//
//    /**
//     * 查询设备类型下，维保计划列表信息
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/findAssetPlanWorkInfos")
//    public String getPlanWorkScheduleByAsset(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String assetId = request.getParameter("assetId");
//        org.json.JSONObject result = new org.json.JSONObject();
//        try {
//            if (StringUtils.isBlank(assetId)) {
//                return "{}";
//            }
//            List<Map<String, Object>> planWorkInfos = planWorkService.getPlanWorkScheduleByAsset(schema_name, assetId);
//            //多时区处理
//            SCTimeZoneUtil.responseMapListDataHandler(planWorkInfos);
//            result.put("rows", planWorkInfos);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增部位
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @Transactional
//    @RequestMapping("/asset_part_save")
//    public ResponseModel doAddAssetPart(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return partService.addPart(schemaName, request);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
}
