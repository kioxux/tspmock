package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.MetadataWork;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.system.CommonUtilService;
//import com.gengyun.senscloud.service.MetadataWorkService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 工单模板
// * User: sps
// * Date: 2018/11/16
// * Time: 下午14:00
// */
//@RestController
//@RequestMapping("/metadata_work")
public class MetadataWorkController {
//    private static final Logger logger = LoggerFactory.getLogger(MetadataWorkController.class);
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    MetadataWorkService metadataWorkService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    CommonUtilService commonUtilService;
//
//    /**
//     * 进入列表页，获取按钮权限
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        Boolean addFlag = false;
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            //按当前登录用户，获取权限
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schemaName, loginUser.getId(), "setting");
//          /*  if (null != userFunctionList || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    if ("settlement_add".equals(functionData.getFunctionName())) {
//                        addFlag = true;
//                        break;
//                    }
//                }
//            }*/
//            addFlag = true;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//        }
//        return new ModelAndView("metadata_work/metadata_work_index")
//                .addObject("rpAddFlag", addFlag);
//    }
//
//    /**
//     * 进入明细页
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping({"/goToDetail"})
//    public ModelAndView goToDetail(HttpServletRequest request, HttpServletResponse response) {
//        String workTemplateCode = null;
//        String linkType = null;
//        try {
//          /*  User loginUser = AuthService.getLoginUser(request);
//            //按当前登录用户，获取权限
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schemaName, loginUser.getId(), "setting");
//            if (null != userFunctionList || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    if ("settlement_add".equals(functionData.getFunctionName())) {
//                        break;
//                    }
//                }
//            }*/
//            workTemplateCode = request.getParameter("workTemplateCode");
//            linkType = request.getParameter("linkType");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//        }
//        return new ModelAndView("metadata_work/metadata_work_detail")
//                .addObject("workTemplateCode", workTemplateCode)
//                .addObject("linkType", linkType);
//    }
//
////    /**
////     * 查询列表页列表数据（分页）
////     *
////     * @param workTemplateName
////     * @param pageSize
////     * @param pageNumber
////     * @param sortName
////     * @param sortOrder
////     * @param request
////     * @param response
////     * @return
////     */
////    @RequestMapping("/queryMetadataWorkList")
////    public String queryMetadataWorkList(@RequestParam(name = "workTemplateName", required = false) String workTemplateName,
////                                        @RequestParam(name = "pageSize", required = false) Integer pageSize,
////                                        @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
////                                        @RequestParam(name = "sortName", required = false) String sortName,
////                                        @RequestParam(name = "sortOrder", required = false) String sortOrder,
////                                        HttpServletRequest request, HttpServletResponse response) {
////        try {
////            String schemaName = authService.getCompany(request).getSchema_name();
////            JSONObject pageResult = metadataWorkService.query(schemaName, workTemplateName, pageSize, pageNumber,
////                    sortName, sortOrder, Constants.METADATA_WORK);
////            return pageResult.toString();
////        } catch (Exception ex) {
////            return "{}";
////        }
////    }
//
//    /**
//     * 查询列表页列表数据（树形）
//     *
//     * @param request
//     * @param response
//     * @param workTemplateName
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping("/queryMetadataWorkListByCondition")
//    public ResponseModel queryMetadataWorkList(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "workTemplateName", required = false) String workTemplateName) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            List<MetadataWork> metadataWorkList = metadataWorkService.queryAllRootList(schemaName, workTemplateName);
//            return ResponseModel.ok(metadataWorkList);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL));
//            result.setContent("");
//            return result;
//        }
//    }
//
//
//    /**
//     * 查询所有可用的模板名称和编号
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping("/queryMetadataWorkCodeAndDesc")
//    public ResponseModel queryMetadataWorkCodeAndDesc(HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            List<Map<String, Object>> list = metadataWorkService.queryCodeAndDesc(schemaName, null, "0");
//            List<Map<String, Object>> commonList = metadataWorkService.queryCodeAndDesc(schemaName, null, "1");
//            Map<String, Object> resultInfo = new HashMap<String, Object>();
//            resultInfo.put("commonList", commonList);
//            resultInfo.put("list", list);
//            List<Map<String, Object>> stJaInfo = selectOptionService.getStaticSelectTypeList();
//            resultInfo.put("selectTypes", stJaInfo);
//            Map<String, String> stdInfo = selectOptionService.getStaticStlForInfo();
//            resultInfo.put("selectTypesDesc", stdInfo);
//            result.setContent(resultInfo);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL));
//        }
//    }
//
//    /**
//     * 保存数据
//     *
//     * @param metadataWork
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping("/save-metadataWork")
//    public ResponseModel saveMetadataWork(@RequestBody MetadataWork metadataWork, HttpServletRequest request, HttpServletResponse response) {
//        try {
//            if (metadataWork == null || null == metadataWork.getWorkTemplateName() || "".equals(metadataWork.getWorkTemplateName())) {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DE_DATA_NOT_NULL));
//            }
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String contentViewType = metadataWork.getContentViewType();
//            User loginUser = AuthService.getLoginUser(request);
//            metadataWorkService.saveMetadataWork(schemaName, loginUser, metadataWork, contentViewType);
//            return ResponseModel.ok(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_EDIT_FA));
//        }
//    }
//
//    /**
//     * 根据主键查询数据
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping("/find-single-metadataWork")
//    public ResponseModel querySingleMetadataWork(HttpServletRequest request, HttpServletResponse response) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String workTemplateCode = request.getParameter("workTemplateCode");
//            return metadataWorkService.queryById(schemaName, workTemplateCode);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 根据主键查询数据
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @MenuPermission(value = {"metadata_work", "worksheet", "asset_data_management", "bom"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/findMetadataWorkInfo")
//    public ResponseModel findMetadataWorkInfo(HttpServletRequest request, HttpServletResponse response) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String workTemplateCode = request.getParameter("workTemplateCode");
//            return metadataWorkService.queryMwInfoById(schemaName, request, workTemplateCode);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 根据主键更新使用状态
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("metadata_work")
//    @RequestMapping("/update-metadataWork")
//    public ResponseModel updateStatus(HttpServletRequest request, HttpServletResponse response) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return metadataWorkService.updateStatus(schemaName, request);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 查询所有可用的公共模板信息
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/commonTemplateList")
//    public ResponseModel commonTemplateList(HttpServletRequest request) {
//        ResponseModel result = new ResponseModel();
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            List<Map<String, Object>> commonList = metadataWorkService.commonTemplateList(schemaName);
//            result.setContent(commonList);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            logger.error(ex.getMessage());
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
}