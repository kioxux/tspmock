package com.gengyun.senscloud.controller.MonitorArea;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 备件报废
 */
@RestController
@RequestMapping("/monitor_area")
public class MonitorAreaController {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    MonitorAreaService monitorAreaService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("add_monitor_area", "brAddFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "monitor_area/index", "");
//        }catch (Exception e){
//            return new ModelAndView("monitor_area/index");
//        }
//    }
//
//    /**
//     * 获取监控区域列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find_monitor_area_list")
//    public String findMonitorAreaList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                     @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                     @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                     @RequestParam(name = "facilityId", required = false) String facilityId,
//                                     HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User user = AuthService.getLoginUser(request);
//            JSONObject pageResult=monitorAreaService.query(schemaName,searchKey,pageSize,pageNumber,facilityId,user);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//    /**
//     * 添加区域配置界面
//     */
//
//    @RequestMapping("/add_monitorArea_view")
//    public ModelAndView addMonitorAreaView(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        return new ModelAndView("monitor_area/monitor_area_add");
//    }
//
//    /**
//     * 新增监控区域，处理业务
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/add_monitorArea_do")
//    public ResponseModel addMonitorArea(@RequestParam Map<String, Object> paramMap,HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "monitor_area", "add_monitor_area");
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                paramMap.put("schema_name", schemaName);
//                paramMap.put("monitor_area_code", serialNumberService.generateMaxBusinessCode(schemaName, "monitor_area"));
//                return monitorAreaService.addMonitorArea(paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/detail_monitorArea_view")
//    public ModelAndView detailMonitorAreaView(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try{
//            Map<String,Object> monitorArea=monitorAreaService.findMonitorAreaById(request);
//            return new ModelAndView("monitor_area/monitor_area_detail").addObject("monitorArea", JSON.toJSON(monitorArea));
//        }catch (Exception e){
//            return new ModelAndView("monitor_area/monitor_area_detail");
//        }
//    }
//
//    /**
//     * 修改监控区域，处理业务
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/update_monitorArea")
//    public ResponseModel updateMonitorArea(@RequestParam Map<String, Object> paramMap,HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "monitor_area", "add_monitor_area");
//            if (permissionFlag) {
//                paramMap.put("schema_name", schemaName);
//                return monitorAreaService.updateMonitorArea(paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//    /**
//     * 删除监控区域
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response){
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try{
//            String id = request.getParameter("id");
//            if(StringUtils.isBlank(id)){
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.error(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//            }
//            return monitorAreaService.deleteById(schemaName,request);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.error(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
}

