package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorksModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.util.RegexUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 工单管理
 */

@Api(tags = "工单管理")
@RestController
public class WorksController {
    @Resource
    WorksService worksService;
    @Resource
    UserService userService;


    @ApiOperation(value = "获取工单模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchWorksListPermission", method = RequestMethod.POST)
    public ResponseModel searchWorksListPermission(MethodParam methodParam) {
        return ResponseModel.ok(worksService.getWorksListPermission(methodParam));
    }

    @ApiOperation(value = "获取工单号", notes = ResponseConstant.RSP_DESC_GET_ASSET_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ResponseModel start(MethodParam methodParam, WorksModel worksModel) {
        return ResponseModel.ok(worksService.start(methodParam, worksModel));
    }

    @ApiOperation(value = "提交", notes = ResponseConstant.RSP_DESC_GET_ASSET_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @Transactional
    @RequestMapping(value = "/worksSubmit", method = RequestMethod.POST)
    public ResponseModel submit(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                @SuppressWarnings("unused") WorksModel worksModel) {
        worksService.worksSubmit(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "分页查询工单列表（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_WORKS_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "positionCodeSearch",
            "assetCategoryIdSearch", "workTypeIdSearch", "statusSearch", "keywordSearch", "beginTime", "finishedTime", "overtimeSearch", "startCreateDateSearch", "endCreateDateSearch"})
    @RequestMapping(value = "/searchWorksList", method = RequestMethod.POST)
    public ResponseModel searchWorksList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                         @SuppressWarnings("unused") WorksModel worksModel) {
        return ResponseModel.ok(worksService.getWorksList(methodParam, paramMap));
    }

    @ApiOperation(value = "工单作废", notes = ResponseConstant.RSP_DESC_SEARCH_CANCEL_WORKS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_code", "work_type_id"})
    @Transactional
    @RequestMapping(value = "/cancelWorks", method = RequestMethod.POST)
    public ResponseModel cancelWorks(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                     @SuppressWarnings("unused") WorksModel worksModel) {
        worksService.cancelWorks(methodParam, worksModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "工单分配", notes = ResponseConstant.RSP_DESC_WORKS_ASSIGNMENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "sub_work_code",
            "receive_user_id", "pageType", "begin_time", "work_type_id"})
    @Transactional
    @RequestMapping(value = "/worksAssignment", method = RequestMethod.POST)
    public ResponseModel worksAssignment(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                         @SuppressWarnings("unused") WorksModel worksModel) {
        worksService.worksAssignment(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "工单分配列表", notes = ResponseConstant.RSP_DESC_SEARCH_ASSIGNMENT_USER_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "sub_work_code", "work_type_id"})
    @Transactional
    @RequestMapping(value = "/searchAssignmentUserList", method = RequestMethod.POST)
    public ResponseModel worksAssignmentList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                             @SuppressWarnings("unused") WorksModel worksModel) {
        return ResponseModel.ok(worksService.worksAssignmentList(methodParam, paramMap));
    }

    @ApiOperation(value = "工单回收", notes = ResponseConstant.RSP_DESC_WORKS_ASSIGNMENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "sub_work_code"})
    @Transactional
    @RequestMapping(value = "/worksRecovery", method = RequestMethod.POST)
    public ResponseModel worksRecovery(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                       @SuppressWarnings("unused") WorksModel worksModel) {
        worksService.worksRecovery(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "有主子工单时候的处理接口", notes = ResponseConstant.RSP_DESC_WORKS_ASSIGNMENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_type_id", "sub_work_code"})
    @Transactional
    @RequestMapping(value = "/haveSubWorksDeal", method = RequestMethod.POST)
    public ResponseModel haveSubWorksDeal(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                          @SuppressWarnings("unused") WorksModel worksModel) {
        return ResponseModel.ok(worksService.haveSubWorksDeal(methodParam, paramMap));
    }

    @ApiOperation(value = "扫码签到", notes = ResponseConstant.RSP_DESC_CODE_SCANNING_SIGN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_type_id", "sub_work_code", "id"})
    @Transactional
    @RequestMapping(value = "/codeScanningSign", method = RequestMethod.POST)
    public ResponseModel codeScanningSign(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,
                                          @SuppressWarnings("unused") WorksModel worksModel) {
        return ResponseModel.ok(worksService.codeScanningSign(methodParam, paramMap));
    }
//    public static void main(String[] args) throws FileNotFoundException, DocumentException {
////        Document document = new Document();
//////Step 2—Get a PdfWriter instance.
////        PdfWriter.getInstance(document, new FileOutputStream("C:/createSamplePDF.pdf"));
//////Step 3—Open the Document.
////        document.open();
//////Step 4—Add content.
////        document.add(new Paragraph("Hello World"));
//////Step 5—Close the Document.
////        document.close();
//        Rectangle rect = new Rectangle(PageSize.B5.rotate());
////页面背景色
//// //不需要背景色       rect.setBackgroundColor(BaseColor.ORANGE);
//
//        Document doc = new Document(rect);
//
//        PdfWriter writer = PdfWriter.getInstance(doc,  new FileOutputStream("C:/createSamplePDF.pdf"));
//
////PDF版本(默认1.4)
//        writer.setPdfVersion(PdfWriter.PDF_VERSION_1_2);
//
////文档属性
//        doc.addTitle("Title@sample");
//        doc.addAuthor("Author@rensanning");
//        doc.addSubject("Subject@iText sample");
//        doc.addKeywords("Keywords@iText");
//        doc.addCreator("Creator@iText");
//
////页边空白
//        doc.setMargins(10, 20, 30, 40);
//
//        //Step 3—Open the Document.
//        doc.open();
////Step 4—Add content.
////        doc.add(new Paragraph("Hello World"));
////Step 5—Close the Document.
////        doc.close();
//
//
////Paragraph对象: a Phrase with extra properties and a newline
//        doc.newPage();
//        doc.add(new Paragraph("Paragraph page"));
//
//        Paragraph info = new Paragraph();
//        info.add(new Chunk("China "));
//        info.add(new Chunk("chinese"));
//        info.add(Chunk.NEWLINE);
//        info.add(new Phrase("Japan "));
//        info.add(new Phrase("japanese"));
//        doc.add(info);
//
////List对象: a sequence of Paragraphs called ListItem
//        doc.newPage();
//        List list = new List(List.ORDERED);
//        for (int i = 0; i < 10; i++) {
//            ListItem item = new ListItem(String.format("%s: %d movies",
//                    "country" + (i + 1), (i + 1) * 100), new Font(
//                    Font.FontFamily.HELVETICA, 6, Font.BOLD, BaseColor.WHITE));
//            List movielist = new List(List.ORDERED, List.ALPHABETICAL);
//            movielist.setLowercase(List.LOWERCASE);
//            for (int j = 0; j < 5; j++) {
//                ListItem movieitem = new ListItem("Title" + (j + 1));
//                List directorlist = new List(List.UNORDERED);
//                for (int k = 0; k < 3; k++) {
//                    directorlist.add(String.format("%s, %s", "Name1" + (k + 1),
//                            "Name2" + (k + 1)));
//                }
//                movieitem.add(directorlist);
//                movielist.add(movieitem);
//            }
//            item.add(movielist);
//            list.add(item);
//        }
//        doc.add(list);
//
//
//
//        doc.close();
//    }


    public static void main(String[] args) {
        //文字类
        Map<String, String> dataMap = new HashMap<String, String>();
        dataMap.put("title", "title");

        //图片
        String knowImgPath = "C:/setting.png";
        Map<String, String> imgMap = new HashMap<String, String>();
        imgMap.put("knowImg", knowImgPath);


        //表格 一行数据是一个list
        java.util.List<String> list = new ArrayList<String>();
        list.add("日期");
        list.add("金额");

        java.util.List<String> list2 = new ArrayList<String>();
        list2.add("2018-01-01");
        list2.add("100");

        java.util.List<java.util.List<String>> List = new ArrayList<java.util.List<String>>();
        List.add(list);
        List.add(list2);

        Map<String, java.util.List<java.util.List<String>>> listMap = new HashMap<String, java.util.List<java.util.List<String>>>();
        listMap.put("fee_list", List);

        Map<String, Object> o = new HashMap<String, Object>();
        o.put("datemap", dataMap);
        o.put("imgmap", imgMap);
        o.put("list", listMap);

        String filePath = "C:/createSamplePDF.pdf";
        creatPdf(o, filePath);
    }


    public static void creatPdf(Map<String, Object> map, String filePath) {
        HashMap<String, String> text = new HashMap<>();
        text.put("asset_code", "asset_code");
        text.put("asset_name", "asset_name");
        text.put("asset_position", "asset_position");
        text.put("category_name", "category_name");
        text.put("asset_running_name", "asset_running_name");
        text.put("guarantee_status", "guarantee_status");
        map.put("problem_img", "C:/setting.png");
        text.put("problem_note", "problem_note");
        text.put("create_user_name", "create_user_name");
        text.put("fault_type", "fault_type");
        text.put("deal_result", "deal_result");
        text.put("fault_reason", "fault_reason");
        text.put("fault_reason_note", "fault_reason_note");
        text.put("solutions", "solutions");
        text.put("solutions_note", "solutions_note");
        text.put("receive_user", "receive_user");
        map.put("bom_list", "bom_list");
        map.put("fee_list", "fee_list");
        text.put("audit_opinion", "audit_opinion");
        text.put("approve_remark", "approve_remark");
        map.put("text", text);

        try {
            BaseFont bf = BaseFont.createFont("c://windows//fonts//simsun.ttc,1", BaseFont.IDENTITY_H,
                    BaseFont.EMBEDDED);
            FileOutputStream out = new FileOutputStream(filePath);// 输出流
            PdfReader reader = new PdfReader("C:/wxOrder.pdf");// 读取pdf模板
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PdfStamper stamper = new PdfStamper(reader, bos);
            stamper.setFormFlattening(true);
            AcroFields form = stamper.getAcroFields();
            // 文字类的内容处理
            Map<String, String> datemap = (Map<String, String>) map.get("text");
            form.addSubstitutionFont(bf);
            for (String key : datemap.keySet()) {
                String value = datemap.get(key);
                form.setField(key, value);
            }
            // 图片类的内容处理
//            Map<String, String> imgmap = (Map<String, String>) map.get("problem_img");
            String problem_img = (String) map.get("problem_img");
//            for (String key : imgmap.keySet()) {
//                String value = imgmap.get(key);
//                String imgpath = value;
            if (RegexUtil.optIsPresentStr(problem_img)) {
                String imgpath = problem_img;
                int pageNo = form.getFieldPositions("problem_img").get(0).page;
                Rectangle signRect = form.getFieldPositions("problem_img").get(0).position;
                float x = signRect.getLeft();
                float y = signRect.getBottom();
                // 根据路径读取图片
                Image image = Image.getInstance(imgpath);
                // 获取图片页面
                PdfContentByte under = stamper.getOverContent(pageNo);
                // 图片大小自适应
                image.scaleToFit(signRect.getWidth(), signRect.getHeight());
                // 添加图片
                image.setAbsolutePosition(x, y);
                under.addImage(image);
            }
//            }
            // 表格类
            Map<String, ArrayList<ArrayList<String>>> listMap = (Map<String, ArrayList<ArrayList<String>>>) map.get("list");
            for (String key : listMap.keySet()) {
                ArrayList<ArrayList<String>> lists = listMap.get(key);
                int pageNo = form.getFieldPositions(key).get(0).page;
                PdfContentByte pcb = stamper.getOverContent(pageNo);
                Rectangle signRect = form.getFieldPositions(key).get(0).position;
                //表格位置
                int column = lists.get(0).size();
                int row = lists.size();
                PdfPTable table = new PdfPTable(column);
                float tatalWidth = signRect.getRight() - signRect.getLeft() - 1;
                int size = lists.get(0).size();
                float[] width = new float[size];
                for (int i = 0; i < size; i++) {
                    if (i == 0) {
                        width[i] = 60f;
                    } else {
                        width[i] = (tatalWidth - 60) / (size - 1);
                    }
                }
                table.setTotalWidth(width);
                table.setLockedWidth(true);
                table.setKeepTogether(true);
                table.setSplitLate(false);
                table.setSplitRows(true);
                Font FontProve = new Font(bf, 10, 0);
                //表格数据填写
                for (int i = 0; i < row; i++) {
                    ArrayList<String> list = lists.get(i);
                    for (int j = 0; j < column; j++) {
                        Paragraph paragraph = new Paragraph(String.valueOf(list.get(j)), FontProve);
                        PdfPCell cell = new PdfPCell(paragraph);
                        cell.setBorderWidth(1);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setLeading(0, (float) 1.4);
                        table.addCell(cell);
                    }
                }
                table.writeSelectedRows(0, -1, signRect.getLeft(), signRect.getTop(), pcb);
            }
            stamper.setFormFlattening(true);// 如果为false，生成的PDF文件可以编辑，如果为true，生成的PDF文件不可以编辑
            stamper.close();
            Document doc = new Document();
            PdfCopy copy = new PdfCopy(doc, out);
            doc.open();
            int pageNum = reader.getNumberOfPages();
            for (int i = 1; i <= pageNum; i++) {
                PdfImportedPage importPage = copy.getImportedPage(new PdfReader(bos.toByteArray()), i);
                copy.addPage(importPage);
            }
            doc.close();
        } catch (IOException e) {
            System.out.println(e);
        } catch (DocumentException e) {
            System.out.println(e);
        }

    }
}

