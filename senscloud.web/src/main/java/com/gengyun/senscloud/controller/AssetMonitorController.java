package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.AlarmAnomalySendInfoResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.StringUtil;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@RestController
//@RequestMapping("/asset_monitor")
public class AssetMonitorController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    AssetInfoService assetInfoService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AnalysReportService analysReportService;
//
//    @Autowired
//    AssetMonitorValueService assetMonitorVauleService;
//
//    @Autowired
//    AlarmAnomalySendInfoService alarmAnomalySendInfoService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    AssetAnalysByMapService assetAnalysService;
//
//    //设备监控视图
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/list")
//    public ModelAndView InitAssetMonitor(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String errMsg = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            errMsg = selectOptionService.getLanguageInfo("error_asset_monitor_list");
//            Boolean canSeeAllFacility = false;
//            String facilitiesSelect = "";
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "asset_monitor");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("asset_monitor_all_facility")) {
//                        canSeeAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("repair_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    }
//                }
//            }
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("scada_flag", "scadaFalg");
//
//            }};
//            ModelAndView mv = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "asset_monitor/list-bak", "asset_monitor");
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, user);
//            return mv.addObject("canSeeAllFacility", canSeeAllFacility).addObject("facilitiesSelect", facilitiesSelect);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(errMsg + ex.getMessage());
//            return new ModelAndView("asset_monitor/list-bak").addObject("canSeeAllFacility", false);
//        }
//    }
//
//    //设备监控数据获取
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/get-asset-monitor-list")
//    public String getAssetMonitorList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String facilityId = request.getParameter("facilityId");
//            String keyWord = request.getParameter("keyWord");
//            String condition = "";
//            if (keyWord != null && !keyWord.equals("") && !keyWord.isEmpty()) {
//                condition += " and  m.asset_code like '%" + keyWord + "%' or dv.strname like '%" + keyWord + "%'  ";
//            }
//            if (facilityId == null || facilityId.isEmpty() || facilityId.equals("0")) {
//                User user = authService.getLoginUser(request);
//                //根据人员的角色，判断获取值的范围
//                String account = user.getAccount();
//                Boolean isAllFacility = false;
//                Boolean isSelfFacility = false;
//                List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "asset_monitor");
//                if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                    for (UserFunctionData functionData : userFunctionList) {
//                        //如果可以查看全部位置，则不限制
//                        if (functionData.getFunctionName().equals("asset_monitor_all_facility")) {
//                            isAllFacility = true;
//                            break;
//                        } else if (functionData.getFunctionName().equals("asset_monitor_self_facility")) {
//                            isSelfFacility = true;
//                        }
//                    }
//                }
//                //如果只能查自己所属位置，则按自己所在位置拼接查询条件，否则按是否全位置，或自己查询
//                if (isAllFacility) {
//                    condition += " ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String key : facilityList.keySet()) {
//                            facilityIds += facilityList.get(key).getId() + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition += " and dv.intsiteid in (" + facilityIds + ") ";
//                    } else {
//                        condition += " and 1<>1 ";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    //非全位置，则只能查询自己的，否则，不按位置条件进行判断
//                    condition += " and 1<>1 ";
//                }
//            } else {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilityId, facilityList);
//                condition += " and m.intsiteid in (" + facilityId + subFacility + ") ";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AssetMonitorData> assetMonitorList = analysReportService.findAllAssetMonitorList(schema_name, condition, pageSize, begin);
//            System.out.println(schema_name);
//            System.out.println(condition);
//            System.out.println(pageSize);
//            System.out.println(begin);
//
//            int total = analysReportService.findAllAssetMonitorListCount(schema_name, condition);
//
//            result.put("rows", assetMonitorList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/get-asset-monitor-list-by-code")
//    public String getAssetMonitorListByCode(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            String assetCode = "";
//            int total = 0;
//            List<AssetMonitorData> assetMonitorList = new ArrayList<AssetMonitorData>();
//            assetCode = request.getParameter("assetCode");
//            if (assetCode != null && !assetCode.equals("0") && assetCode.matches(".*[a-zA-z].*")) {
//                //时效结果
//                assetMonitorList = analysReportService.findAllAssetMonitorListByCode(schema_name, assetCode);
////                total = analysReportService.findAllAssetMonitorListCountByCode(schema_name, assetCode);
//            }
//            result.put("rows", assetMonitorList);
////            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 设备监控：异常报警信息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/getAlarmExceptionDataInfo")
//    public String getAlarmExceptionDataInfo(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            String selectId = request.getParameter("selectId");
//            String alarm_code = request.getParameter("alarm_code");
//            String condition = "";
//            if (selectId != null && !selectId.equals("0")) {
//                condition = " and s.asset_code ='" + selectId + "' "; //位置编号和设备编号
//                if (!selectId.matches(".*[a-zA-z].*")) {//如果不是设备
//                    List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                    //加上位置的子位置
//                    String subFacility = facilitiesService.getSubFacilityIds(selectId, facilityList);
//                    if (subFacility != null) {
//                        condition += " or m.intsiteid in (" + selectId + subFacility + ") ";
//                    }
//                }
//
//            } else {
//
//            }
//            if (null != alarm_code && !"".equals(alarm_code)) {
//                condition += "and s.alarm_code=" + alarm_code;
//            }
//
//            List<AlarmAnomalySendInfoResult> alarmAnomaly = alarmAnomalySendInfoService.getAlarmAnomalySendInfoDataList(schema_name, condition);
//            if (null != alarm_code && !"".equals(alarm_code) && (null == alarmAnomaly || 0 == alarmAnomaly.size())) {
//                return "";
//            }
//            int total = alarmAnomalySendInfoService.getAlarmAnomalySendInfoDataListCount(schema_name, condition);
//            result.put("rows", alarmAnomaly);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 功能：实时监控报警记录查询单据根据设备id
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/getAlarmHistoryData")
//    public String getAlarmHistoryData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            String assetCode = request.getParameter("assetCode");
//            String monitor_name = request.getParameter("monitor_name");
//            List<AlarmAnomalySendInfoResult> alarmAnomaly = alarmAnomalySendInfoService.getAlarmHistoryData(schema_name, assetCode, monitor_name);
//            int total = alarmAnomalySendInfoService.getAlarmHistoryDataCount(schema_name, assetCode, monitor_name);
//            result.put("rows", alarmAnomaly);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 异常报警生成任务单（维修单）
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/addRepairAlarmData1")
//    public ResponseModel addRepairAlarmData1(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        String errMsg = null;
//        try {
//            User user = authService.getLoginUser(request);
//            errMsg = selectOptionService.getLanguageInfo("error_asset_monitor_edit");
//            Boolean addRepairAlarmData = false;
//            String alarm_code = request.getParameter("alarm_code");
//            List<AlarmAnomalySendInfoResult> alarmAnomaly = alarmAnomalySendInfoService.getAlarmAnomalySendInfoDataListByAlarmCode(schema_name, alarm_code);
//            if (alarmAnomaly.size() > 0) {
//                addRepairAlarmData = addRepairAlarmData(schema_name, alarmAnomaly.get(0), alarm_code, request);
//            }
//            if (addRepairAlarmData) {
//                int doUpdate = alarmAnomalySendInfoService.updateStatusByAlarm_code(schema_name, alarm_code, 2);//修改状态为处理中2：处理中
//                result.setContent(addRepairAlarmData);
//                result.setCode(1);
//                result.setMsg("");
//            } else {
//                result.setCode(-1);
//                result.setContent("");
//                result.setMsg(selectOptionService.getLanguageInfo("errot_work_report"));
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(errMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//    //新增一个维修单
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/addRepairAlarmData")
//    @Transactional
//    public ResponseModel addRepairAlarmData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        User user = authService.getLoginUser(request);
//        String errMsg = null;
//        try {
//            String assetCode = request.getParameter("strcode");
//            String occurtime = request.getParameter("occurtime");
//            String faultNote = request.getParameter("faultNote");
//            errMsg = selectOptionService.getLanguageInfo("error_save");
//            if (StringUtils.isEmpty(assetCode) || StringUtils.isEmpty(occurtime) || StringUtils.isEmpty(faultNote)) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo("error_asset_monitor_a"));
//                return result;
//            }
//
//            //获取当前设备
//            Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, assetCode);
//            if (assetModel == null || assetModel.get_id().isEmpty()) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo("error_asset_monitor_b"));
//                return result;
//            }
//            String assetId = assetModel.get_id();
//
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//
//            //看看一小时内，有没有上报未完成的，如果有，就不要再提交
//            int existCount = repairService.getRepairCountByAssetIdRecently(schema_name, assetModel.get_id(), now);
//            if (existCount > 0) {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo("error_asset_monitor_c"));
//                return result;
//            }
//
//            String alarm_code = request.getParameter("alarm_code");
//            String assetStatus = request.getParameter("assetStatus");
//            String faultType = request.getParameter("faultType");
//            String priorityLevel = request.getParameter("priorityLevel");
//            String repairType = request.getParameter("repairType");
//            String receiveAccount = request.getParameter("receiveAccount");
//            String operateAccount = request.getParameter("operateAccount");
//            String deadlineTime = request.getParameter("deadlineTime");
//            String faultImg = request.getParameter("faultImg");
//
//            RepairData repairData = new RepairData();
//            String repairCode = serialNumberService.generateMaxBusinessCode(schema_name, "repair");
//            repairData.setRepairCode(repairCode);
//            repairData.setFacilityId((int) assetModel.getIntsiteid());
//            repairData.setAssetId(assetId);
//            repairData.setAssetStatus(Integer.parseInt(assetStatus));
//            repairData.setFaultType(Integer.parseInt(faultType));
//            repairData.setRepairType(Integer.parseInt(repairType));
//            repairData.setPriorityLevel(Integer.parseInt(priorityLevel));
//            repairData.setAssetType(assetModel.getAsset_type());
//
//            //检查当前时间是否比发生时间早，是的话发生时间不合法
//            if (now.before(Timestamp.valueOf(occurtime))) {
//                repairData.setOccurtime(now);
//            } else {
//                repairData.setOccurtime(Timestamp.valueOf(occurtime));
//            }
//
//            repairData.setDeadlineTime(Timestamp.valueOf(deadlineTime));
//
//            //接受人和分配人
//            if (StringUtil.isNotEmpty(receiveAccount)) {
//                repairData.setReceiveAccount(receiveAccount);
//                repairData.setReveiveTime(now);
//                //上报人也是分配人
//                repairData.setDistributeAccount(user.getAccount());
//                repairData.setDistributeTime(now);
//                repairData.setStatus(40);  //待维修
//            } else {
//                repairData.setReceiveAccount("");
//                repairData.setReveiveTime(null);
//                repairData.setDistributeAccount("");
//                repairData.setDistributeTime(null);
//                repairData.setStatus(20);  //待分配
//            }
//
//            repairData.setFaultNote(faultNote);
//            repairData.setFaultImage(faultImg);
//            repairData.setCreate_user_account(user.getAccount());
//            repairData.setCreatetime(now);
//            repairData.setFromCode(alarm_code);
//
//            //保存上报问题
//            int doCount = repairService.saveRepairInfo(schema_name, repairData);
//            if (doCount > 0) {
//                int doUpdate = alarmAnomalySendInfoService.updateStatusByAlarm_code(schema_name, alarm_code, 2);//修改状态为处理中2：处理中
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo("success_asset_monitor_a"));
//                result.setContent(repairCode);
//                //记录历史
//                logService.AddLog(schema_name, "repair", repairCode, selectOptionService.getLanguageInfo("success_asset_monitor_b"), user.getAccount());
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo("error_work_report"));
//                return result;
//            }
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(0);
//            result.setMsg(errMsg);
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "repair_add", selectOptionService.getLanguageInfo("error_asset_monitor_d") + ex.getMessage(), user.getAccount());
//            return result;
//        }
//    }
//
//    /**
//     * 取消异常报警信息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/cancleAlarmException")
//    public ResponseModel setCancleAlarmException(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        String errMsg = null;
//        try {
//            User user = authService.getLoginUser(request);
//            errMsg = selectOptionService.getLanguageInfo("error_asset_monitor_edit");
//            String res = this.getAlarmExceptionDataInfo(request, response);
//            if ("".equals(res)) {
//                throw new Exception(selectOptionService.getLanguageInfo("error_permission_a"));
//            }
//            String alarm_code = request.getParameter("alarm_code");
//            int doUpdate = alarmAnomalySendInfoService.updateStatusByAlarm_code(schema_name, alarm_code, 4);
//            result.setContent(doUpdate);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(errMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//    /**
//     * 异常报警查询设备的状态
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/getAssetStatusName")
//    public String getAssetStatusName(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            List<AssetStatus> assetStatus = maintenanceSettingsService.assetStatus(schema_name);
//            result.put("assetStatus", assetStatus);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    //设备监控视图
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/monitor_value_input")
//    public ModelAndView InitAssetMonitorValue(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("asset_monitor/monitor_value_input");
//    }
//
//    //设备监控视图
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/monitor_value_list")
//    public ModelAndView InitAssetMonitorValueList(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("asset_monitor/monitor_value_list");
//    }
//
//    /**
//     * 新增监控录入信息
//     */
//    @MenuPermission("asset_monitor")
//    @RequestMapping(value = "/add_monitor")
//    public ResponseModel AddMonitor(HttpServletRequest request, HttpServletResponse response) {
//        String errMsg = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_code = request.getParameter("asset_code");
//            String m_vaule1 = request.getParameter("m_vaule1");
//            String m_vaule2 = request.getParameter("m_vaule2");
//            String m_vaule3 = request.getParameter("m_vaule3");
//            String m_vaule4 = request.getParameter("m_vaule4");
//            String m_vaule5 = request.getParameter("m_vaule5");
//            String m_vaule6 = request.getParameter("m_vaule6");
//
//            AssetMonitorHistoryData monitorValueData = new AssetMonitorHistoryData();
//            monitorValueData.setAsset_code(asset_code);
//            monitorValueData.setMonitor_value1(m_vaule1);
//            monitorValueData.setMonitor_value2(m_vaule2);
//            monitorValueData.setMonitor_value3(m_vaule3);
//            monitorValueData.setMonitor_value4(m_vaule4);
//            monitorValueData.setMonitor_value5(m_vaule5);
//            monitorValueData.setMonitor_value6(m_vaule6);
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FMT_SS);
//            String gatherTime = sdf.format(new Date());
//            monitorValueData.setGather_time(gatherTime);
//
//            assetMonitorVauleService.insertAssetMonitorValue(schema_name, monitorValueData);
//            User user = authService.getLoginUser(request);
//            errMsg = selectOptionService.getLanguageInfo("error_insert_a");
//            return ResponseModel.ok(selectOptionService.getLanguageInfo("success_insert_a"));
//        } catch (Exception ex) {
//            return ResponseModel.error(errMsg);
//        }
//    }
//
//    /**
//     * 监控录入信息查询
//     */
//    @MenuPermission("asset_monitor")
//    @RequestMapping(value = "/monitor_list")
//    public String GetMonitor(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<AssetMonitorHistoryData> list = assetMonitorVauleService.getAssetMonitorValues(schema_name);
//            result.put("rows", list);
//            result.put("total", list.size());
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//
//    }
//
//    @MenuPermission("asset_monitor")
//    @RequestMapping("/asset-monitor-analys-list-by-id")
//    public String analysAssetMonitorCurrentDataById(String[] idList, HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            List<AssetMonitorData> monitorDataList = new ArrayList<AssetMonitorData>();
//            for (int i = 0; i < idList.length; i++) {
//                String id_str = idList[i];
//                int id = Integer.valueOf(id_str);
//                AssetMonitorData monitorData = analysReportService.analysAssetMonitorDataById(schema_name, id);
//                monitorDataList.add(monitorData);
//            }
//
//            if (monitorDataList.size() > 0) {
//                result.put("rows", monitorDataList);
//            }
//            result.put("rows", monitorDataList);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    @RequestMapping("/assetByAssetCode")
//    public ResponseModel assetByAssetCode(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        String errMsg = null;
//        try {
//            User user = authService.getLoginUser(request);
//            errMsg = selectOptionService.getLanguageInfo("error_bom_search_a");
//            String assetCode = request.getParameter("assetCode");
//            Asset asset = assetDataServiceV2.getAssetByCode(schema_name, assetCode);
//            result.setContent(asset);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(errMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//
//    /*统计设备的历史信息记录*/
//    @RequestMapping("/search_history_bom-analys-list-by-assetCode")
//    public ResponseModel searchHistoryBomAnalysListByAssetCode(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String errMsg = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            errMsg = selectOptionService.getLanguageInfo("error_asset_maintain_a");
//            String asset_code = request.getParameter("asset_code");
//            String monitorName = request.getParameter("monitorName");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            beginTime = beginTime + " 00:00:00";
//            endTime = endTime + " 23:59:59";
//
//            List<AssetMonitorData> dataList = assetInfoService.getAssetMonitorHistoryDataListByMonitor(schema_name, asset_code, beginTime, endTime, monitorName);
//
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(errMsg + ex.getMessage());
//            return result;
//        }
//    }
//
//    /**
//     * 自动生成任务单据，这个现在不需要了，功能注释掉
//     *
//     * @param schema
//     * @param data
//     * @param alarm_code
//     * @param request
//     * @return
//     */
//    private Boolean addRepairAlarmData(String schema, AlarmAnomalySendInfoResult data, String alarm_code, HttpServletRequest request) {
//        boolean result = false;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            String repairContent = "";
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//            Calendar calendar = new GregorianCalendar();
//            calendar.setTime(now);
//            calendar.add(calendar.DATE, 1);     //把日期往后增加一天.整数往后推,负数往前移动
//            Date date = calendar.getTime();             //这个时间就是日期往后推一天的结果
//            Timestamp nousedate = new Timestamp(date.getTime());
//            if (data.getAlarm_up() != null && !data.getAlarm_up().isEmpty() && data.getMonitor_value() != null && !data.getMonitor_value().isEmpty()) {
//                if (Double.parseDouble(data.getAlarm_up()) < Double.parseDouble(data.getMonitor_value())) {
//                    repairContent = data.getMonitor_name() + selectOptionService.getLanguageInfo("error_asset_monitor_e") + data.getAlarm_up_task_code();
//                }
//            } else if (data.getAlarm_down() != null && !data.getAlarm_down().isEmpty() && data.getMonitor_value() != null && !data.getMonitor_value().isEmpty()) {
//                if (Double.parseDouble(data.getAlarm_down()) > Double.parseDouble(data.getMonitor_value())) {
//                    repairContent = data.getMonitor_name() + selectOptionService.getLanguageInfo("error_asset_monitor_f") + data.getAlarm_down_task_code();
//                }
//            }
//            RepairData model = new RepairData();
//            //生成新订单编号
//            String repairCode = serialNumberService.generateMaxBusinessCode(schema, "repair");
//            model.setRepairCode(repairCode);
//            model.setAssetId("");
//            model.setAssetType("");
//            model.setAssetStatus(2);
//            model.setDeadlineTime(nousedate);
//            model.setOccurtime(now);
//            model.setFaultNote(repairContent);
//            model.setDistributeAccount("");
//            model.setDistributeTime(null);
//            model.setReceiveAccount("");
//            //判断状态
//            int status = 40;   //待分配
//            model.setReveiveTime(now);
//            model.setDistributeTime(now);
//            model.setRepairBeginTime(null);
//            model.setStatus(status);
//            model.setFacilityId(0);
//            model.setCreate_user_account("system");
//            model.setCreatetime(now);
//            model.setPriorityLevel(2);
//            model.setFromCode(alarm_code);
//            //保存上报问题
//            int doAdd = repairService.saveRepairInfo(schema, model);
//            if (doAdd > 0) {
//                result = true;
//            }
//            return result;
//        } catch (Exception e) {
//            return result;
//        }
//    }
//
//    /**
//     * 获取子组织（含有监控设备的组织）坐标信息以及监控设备数量
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_child_facility_with_monitor")
//    public ResponseModel getChildFacilityWithMonitor(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String facilityId = request.getParameter("facilityId");
//            if (StringUtils.isBlank(facilityId))
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//            return ResponseModel.ok(assetAnalysService.getChildFacilityWithMonitor(schemaName, Integer.parseInt(facilityId)));
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
}
