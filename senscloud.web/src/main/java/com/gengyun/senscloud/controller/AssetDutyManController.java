package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//import java.util.Map;
//
//@RestController
public class AssetDutyManController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    AssetDutyManService dutyManService;
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 初始化资产维保人
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_duty_man_init")
//    public ModelAndView InitAssetDutyMan(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            result.setContent(null);
//            result.setCode(1);
//            result.setMsg("");
//            String asset_id = request.getParameter("asset_id");
//
//            //找到设备，获取其所在位置
//            List<User> userList = userService.getUserOrgGroup(schema_name, asset_id);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(userList);
//            return new ModelAndView("AssetDutyManManage").addObject("result", result).addObject("userList", userList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_ERR_MAN) + ex.getMessage());
//            return new ModelAndView("AssetDutyManManage").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得所有的资产维保人列表
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_duty_man_list")
//    public ModelAndView FindALLAssetDutyMan(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_id = request.getParameter("asset_id");
//
//            List<AssetDutyManData> dataList = dutyManService.findAllAssetDutyManList(schema_name, asset_id);
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            //多时区
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList);
//            return new ModelAndView("AssetDutyManList").addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_ERR_MAN) + ex.getMessage());
//            return new ModelAndView("AssetDutyManList").addObject("result", result);
//        }
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_duty_man_save")
//    @Transactional
//    public ResponseModel getEditAssetDutyMan(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        try {
//            String asset_id = request.getParameter("asset_id");
//
//            String user_account = request.getParameter("user_account");
//            Integer duty_id = Integer.parseInt(request.getParameter("duty_id"));
//            String remark = request.getParameter("remark");
//            if (user_account == null || user_account.isEmpty() || duty_id <= 0) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ID_IS_NOT_NULL));
//            } else {
//                Company company = authService.getCompany(request);
//                String schema_name = company.getSchema_name();
//                User user = authService.getLoginUser(request);
//                if (RegexUtil.isNull(asset_id)) {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ASS_NOT));
//                    return result;
//                }
//                User dutyMan = userService.getUserByAccount(schema_name, user_account);
//                String checkResult = assetService.checkAssetList(request, user, schema_name, asset_id);
//                if (RegexUtil.isNull(checkResult)) {
//                    result.setCode(-1);
//                    String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH);
//			        result.setMsg(tmpMsg);
//                    return result;
//                }
//                //看看是否有重复，如果有重复，则执行更新操作
//                List<AssetDutyManData> list = dutyManService.findAssetDutyMan(schema_name, asset_id, user_account, duty_id);
//                //保存资产维保人员
//                int count = 0;
//                if (list == null || list.isEmpty()) {
//                    count = dutyManService.AddAssetDutyMan(schema_name, asset_id, user_account, duty_id, remark, user.getAccount());
//                } else {
//                    count = dutyManService.UpdateAssetDutyMan(schema_name, asset_id, user_account, duty_id, remark);
//                }
//                if (count > 0) {
//                    //增加日志
//                    String dutyName = "";
//                    List<Map<String, Object>> tempList = maintenanceSettingsService.findAllBusinessType(schema_name);
//                    if (null != tempList && tempList.size() > 0) {
//                        for (int i = 0; i < tempList.size(); i++) {
//                            int businessId = Integer.parseInt(tempList.get(i).get("business_type_id").toString());
//                            if (businessId == duty_id) {
//                                dutyName = tempList.get(i).get("business_name").toString();
//                                break;
//                            }
//                        }
//                    }
//                    logService.AddLog(schema_name, "asset", asset_id.toString(), selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_ACC)+ dutyMan.getUsername() + "，"+selectOptionService.getLanguageInfo( LangConstant.MSG_ORDER)+ dutyName, user.getAccount());
//                    result.setCode(1);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_REMAN_SU));
//                } else {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_REMAN_DE));
//                }
//            }
//        } catch (Exception e) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_duty_man_delete")
//    @Transactional
//    public ResponseModel getDeleteAssetDutyMan(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        try {
//            Integer id = Integer.parseInt(request.getParameter("id"));
//            if (id <= 0) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_UN_CH));//
//            } else {
//                Company company = authService.getCompany(request);
//                String schema_name = company.getSchema_name();
//                User user = authService.getLoginUser(request);
//                AssetDutyManData data = dutyManService.findAssetDutyManById(schema_name, id);
//                if (null == data || RegexUtil.isNull(data.getAssetId())) {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ASS_NOT));
//                    return result;
//                }
//                String checkResult = assetService.checkAssetList(request, user, schema_name, data.getAssetId());
//                if (RegexUtil.isNull(checkResult)) {
//                    result.setCode(-1);
//                    String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH);
//			result.setMsg(tmpMsg);
//                    return result;
//                }
//
//                //删除资产维保人员
//                int count = dutyManService.DeleteAssetDutyMan(schema_name, id, false);
//                if (count > 0) {
//                    String dutyName = "";
//                    List<Map<String, Object>> tempList = maintenanceSettingsService.findAllBusinessType(schema_name);
//                    if (null != tempList && tempList.size() > 0) {
//                        for (int i = 0; i < tempList.size(); i++) {
//                            int businessId = Integer.parseInt(tempList.get(i).get("business_type_id").toString());
//                            if (businessId == data.getId()) {
//                                dutyName = tempList.get(i).get("business_name").toString();
//                                break;
//                            }
//                        }
//                    }
//                    //增加日志
//                    logService.AddLog(schema_name, "asset", data.getAssetId().toString(), selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_ACC) + data.getUserAccount() + "，"+selectOptionService.getLanguageInfo( LangConstant.MSG_ORDER) + dutyName, user.getAccount());
//                    result.setCode(1);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DE_R_SU));
//                } else {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DE_R_DE));
//                }
//            }
//        } catch (Exception e) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));
//        }
//        //返回保存结果
//        return result;
//    }
}
