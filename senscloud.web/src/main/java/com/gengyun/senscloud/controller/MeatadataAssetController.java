package com.gengyun.senscloud.controller;

//import com.fitit100.util.FileUtil;
//import com.fitit100.util.ImageUtil;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.MetaDataAddAssetResult;
//import com.gengyun.senscloud.response.MetadataExportResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.ImageUtils;
//import com.gengyun.senscloud.view.MetadataExcelTemplate;
//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import org.apache.commons.collections.map.HashedMap;
//import org.apache.commons.io.FileUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/metadata_asset")
public class MeatadataAssetController {
//    @Value("${senscloud.file_upload}")
//    private String file_upload_dir;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    MetaDataAssetService metaDataAssetService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetStatusService assetStatusService;
//
//    @Autowired
//    MaintenanceSettingsService maintenanceSettingsService;
//
//    @Autowired
//    TaskTempLateService taskTempLateService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    private AssetCategoryService assetCategoryService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
////    @RequestMapping({"/", "/index"})
////    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
////        String schema_name = authService.getCompany(request).getSchema_name();
////
////        List<MetaDataAsset> metaDataAssetList = metaDataAssetService.findAllMetaDataAsset(schema_name);
////        return new ModelAndView("metadata_asset/metadata_asset_index")
////                .addObject("metaDataAssetList", metaDataAssetList);
////    }
//
//    /*@RequestMapping("/asset_config")
//    public ModelAndView assetConfig(HttpServletRequest request, HttpServletResponse response) {
//        List<String> allMetadataAsset = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            allMetadataAsset = metaDataAssetService.findAllMetaDataAssetName(schema_name);
//        } catch (Exception ex) {
//            allMetadataAsset = null;
//        }
//        if (allMetadataAsset != null) {
//            return new ModelAndView("metadata_asset_config")
//                    .addObject("parent_assets", allMetadataAsset);
//        }
//        return new ModelAndView("metadata_asset_config");
//    }*/
//
//    /**
//     * 定义一个新资产
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/add")
////    @ResponseBody
////    public MetaDataAddAssetResult assetAdd(HttpServletRequest request, HttpServletResponse response) {
////        MetaDataAddAssetResult result = new MetaDataAddAssetResult();
////        try {
////            String asset_name = request.getParameter("asset_name");
////            if (RegexUtil.isJavaVar(asset_name) == false) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("资产名称不符合规范，必须以英文大小写字母或'_'开头，后面跟英文大小写字母或数字！");
////                return result;
////            }
////            String schema_name = authService.getCompany(request).getSchema_name();
////            MetaDataAsset metaDataAsset = new MetaDataAsset();
////            metaDataAsset.setSchema_name(schema_name);
////            metaDataAsset.setAsset_name(asset_name);
////            metaDataAsset.setAsset_alias(request.getParameter("asset_alias"));
////            metaDataAsset.setDescription(request.getParameter("description"));
////            metaDataAsset.setFields("[]");
////
////            metaDataAssetService.insertMetaDataAsset(metaDataAsset);
////            result.setAsset_name(asset_name);
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("添加资产成功！");
////            return result;
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("添加资产时出现错误！");
////            return result;
////        }
////    }
//
//    /**
//     * 获得所有的资产列表
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    /*@RequestMapping("/asset_config_findall")
//    public ModelAndView assetConfigFindALL(HttpServletRequest request, HttpServletResponse response) {
//        FindAllMetaDataAssetResult result = new FindAllMetaDataAssetResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<MetaDataAsset> dataList = metaDataAssetService.findAllMetaDataAsset(schema_name);
//            result.setData(dataList);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg("");
//            return new ModelAndView("metadata_asset_config_findall").addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg("获取资产列表时出现错误：" + ex.getMessage());
//            return new ModelAndView("metadata_asset_config_findall").addObject("result", result);
//        }
//    }*/
//
//    /**
//     * 删除资产定义。删除该资产定义，意味着存贮该资产的表也同时被删除。
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/delete")
////    public JsonResult deleteMetaDataAsset(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            String asset_name = request.getParameter("asset_name");
////            metaDataAssetService.deleteMetaDataAsset(schema_name, asset_name);
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("成功删除资源：" + asset_name);
////            return result;
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("获取资产列表时出现错误：" + ex.getMessage());
////            return result;
////        }
////    }
////
////
////    @RequestMapping("/detail")
////    public ModelAndView metadataAssetDetail(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            String asset_name = request.getParameter("asset_name");
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////            List<String> parent_assets = metaDataAssetService.findAllMetaDataAssetName(schema_name);
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("");
////            return new ModelAndView("metadata_asset/metadata_asset_detail")
////                    .addObject("result", result)
////                    .addObject("metadata", metaDataAsset)
////                    .addObject("parent_assets", parent_assets);
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("获取资产详细信息时出现错误：" + ex.getMessage());
////            return new ModelAndView("metadata_asset/metadata_asset_detail")
////                    .addObject("result", result);
////        }
////    }
//
//    /**
//     * 给资产添加字段
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/add_field")
////    @ResponseBody
////    public JsonResult addField(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            String asset_name = request.getParameter("asset_name");
////            if (RegexUtil.isNull(asset_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("资产表名不能为空！");
////                return result;
////            }
////            String field_name = request.getParameter("field_name").trim();
////            if (RegexUtil.isJavaVar(field_name) == false) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("字段名称不符合规范，必须以英文大小写字母或'_'开头，后面跟英文大小写字母或数字！");
////                return result;
////            }
////
////            if (MetaDataAsset.isSystemField(field_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("非法的字段名，请使用别的名称！");
////                return result;
////            }
////            field_name = field_name.toLowerCase();
////
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////            String fields = metaDataAsset.getFields();
////            JSONArray fieldArray = null;
////            try {
////                fieldArray = JSONArray.fromObject(fields);
////            } catch (Exception ex) {
////                fieldArray = new JSONArray();
////            }
////            MetaDataField metaDataField = new MetaDataField();
////            metaDataField.setName(field_name);
////            metaDataField.setAlias(request.getParameter("field_alias"));
////            metaDataField.setValidation(request.getParameter("field_validation"));
////            metaDataField.setNullable(Boolean.valueOf(request.getParameter("field_nullable")));
////            metaDataField.setDescription(request.getParameter("field_description"));
////
////            for (int i = 0; i < fieldArray.size(); i++) {
////                String name = fieldArray.getJSONObject(i).getString("name");
////                if (name.equalsIgnoreCase(metaDataField.getName())) {
////                    result.setCode(JsonResult.CODE_ERROR);
////                    result.setMsg("该字段已经存在，请用别的名称！");
////                    return result;
////                }
////            }
////            fieldArray.add(JSONObject.fromObject(metaDataField));
////            metaDataAssetService.metadataUpdateFields(schema_name, asset_name, fieldArray.toString());
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("添加字段成功！");
////            return result;
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("添加字段时出现错误！");
////            return result;
////        }
////    }
//
//    /**
//     * 删除字段
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//   /* @RequestMapping("/delete_field")
//    @ResponseBody
//    public JsonResult deleteField(HttpServletRequest request, HttpServletResponse response) {
//        JsonResult result = new JsonResult();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            String asset_name = request.getParameter("asset_name");
//            if (RegexUtil.isNull(asset_name)) {
//                result.setCode(JsonResult.CODE_ERROR);
//                result.setMsg("资产表名不能为空！");
//                return result;
//            }
//
//            String field_name = request.getParameter("field_name");
//            if (RegexUtil.isNull(asset_name)) {
//                result.setCode(JsonResult.CODE_ERROR);
//                result.setMsg("要删除的字段名不能为空！");
//                return result;
//            }
//
//            metaDataAssetService.metadataDeleteField(schema_name, asset_name, field_name);
//            result.setCode(JsonResult.CODE_OK);
//            result.setMsg("删除字段成功！");
//            return result;
//        } catch (Exception ex) {
//            result.setCode(JsonResult.CODE_ERROR);
//            result.setMsg("删除字段时出现错误！");
//            return result;
//        }
//    }*/
//
//    /**
//     * 更新资源基础信息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/update_base_info")
////    @ResponseBody
////    public JsonResult updateBaseInfo(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            String asset_name = request.getParameter("asset_name");
////            if (RegexUtil.isJavaVar(asset_name) == false) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("资产名称名称不符合规范，必须以英文大小写字母或'_'开头，后面跟英文大小写字母或数字！");
////                return result;
////            }
////
////            MetaDataAsset metaDataAsset = new MetaDataAsset();
////            metaDataAsset.setSchema_name(schema_name);
////            metaDataAsset.setAsset_name(asset_name);
////
////            metaDataAsset.setAsset_alias(request.getParameter("asset_alias"));
////            metaDataAsset.setDescription(request.getParameter("description"));
////            /*metaDataAsset.setParent_asset(request.getParameter("parent_asset"));*/
////
////            metaDataAssetService.updateMetaDataAssetBaseInfo(metaDataAsset);
////
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("更新成功！");
////            return result;
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("更新时出现错误！");
////            return result;
////        }
////    }
//
//    /**
//     * 上传设备图标
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/upload_asset_icon")
////    public ModelAndView uploadAssetIcon(HttpServletRequest request, HttpServletResponse response) {
////        return new ModelAndView("metadata_asset/upload_asset_icon")
////                .addObject("asset_name", request.getParameter("asset_name"));
////    }
//
//    /**
//     * 上传设备图标
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/upload_asset_icon2")
////    public JsonResult uploadAssetIcon2(@RequestParam(value = "file", required = false) MultipartFile file,
////                                       HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        File targetFile = null;
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            String asset_name = request.getParameter("asset_name");
////            String path = file_upload_dir + File.separatorChar + "icon";
////            File dir = new File(path);
////            if (!dir.exists()) {
////                dir.mkdirs();
////            }
////            String fileName = file.getOriginalFilename();
////            targetFile = new File(path, fileName);
////            /*if (!targetFile.exists()) {
////                targetFile.mkdirs();
////            }*/
////            // 保存
////            try {
////                file.transferTo(targetFile);
////                String fileType = fileName.substring(fileName.lastIndexOf("."));
////                String iconFileName = asset_name + fileType;
////                String iconFile = path + File.separatorChar + iconFileName;
////                if (ImageUtil.createThumbnail(targetFile.getAbsolutePath(), iconFile, 68, 68, false) == true) {
////                    MetaDataAsset metaDataAsset = new MetaDataAsset();
////                    metaDataAsset.setSchema_name(schema_name);
////                    metaDataAsset.setAsset_name(asset_name);
////                    metaDataAsset.setIcon(iconFileName);
////                    metaDataAssetService.updateAssetIcon(metaDataAsset);
////                    result.success("上传成功");
////                    return result;
////                } else {
////                    result.setCode(JsonResult.CODE_ERROR);
////                    result.setMsg("创建图标失败！");
////                    return result;
////                }
////            } catch (Exception e) {
////                e.printStackTrace();
////                result.error("文件读写失败！");
////                return result;
////            }
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("上传时出现错误！");
////            return result;
////        } finally {
////            if (targetFile != null) {
////                try {
////                    targetFile.delete();
////                } catch (Exception ex) {
////
////                }
////            }
////        }
////    }
//
//    /**
//     * 获得字段详细信息
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/field_detail")
////    public ModelAndView fieldDetail(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            String asset_name = request.getParameter("asset_name");
////            if (RegexUtil.isNull(asset_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("资产表名不能为空！");
////                return new ModelAndView("metadata_asset/metadata_asset_field_detail")
////                        .addObject("result", result);
////            }
////
////
////            String field_name = request.getParameter("field_name");
////            if (RegexUtil.isNull(field_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("字段名不能为空！");
////                return new ModelAndView("metadata_asset/metadata_asset_field_detail")
////                        .addObject("result", result);
////            }
////
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////            String fields = metaDataAsset.getFields();
////            JSONArray fieldArray = JSONArray.fromObject(fields);
////            for (int i = 0; i < fieldArray.size(); i++) {
////                JSONObject item = fieldArray.optJSONObject(i);
////                if (field_name.equals(item.optString("name"))) {
////                    result.setCode(JsonResult.CODE_OK);
////                    return new ModelAndView("metadata_asset/metadata_asset_field_detail")
////                            .addObject("result", result)
////                            .addObject("field", item)
////                            .addObject("asset_name", asset_name);
////                }
////            }
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("获得的字段详细信息时出现错误！");
////        }
////        result.setCode(JsonResult.CODE_ERROR);
////        result.setMsg("找不到该字段");
////        return new ModelAndView("metadata_asset/metadata_asset_field_detail")
////                .addObject("result", result);
////    }
//
//    /**
//     * 更新字段
//     *
//     * @param request
//     * @param response
//     * @return
//     */
////    @RequestMapping("/update_field")
////    @ResponseBody
////    public JsonResult updateField(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            String asset_name = request.getParameter("asset_name");
////            if (RegexUtil.isNull(asset_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("资产表名不能为空！");
////                return result;
////            }
////
////
////            String old_field_name = request.getParameter("old_field_name");
////            if (RegexUtil.isNull(old_field_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("字段名不能为空！");
////                return result;
////            }
////
////            String field_name = request.getParameter("field_name").trim();
////            if (RegexUtil.isJavaVar(field_name) == false) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("字段名称不符合规范，必须以英文大小写字母或'_'开头，后面跟英文大小写字母或数字！");
////                return result;
////            }
////            field_name = field_name.toLowerCase();
////
////            if (MetaDataAsset.isSystemField(field_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("非法的字段名，请使用别的名称！");
////                return result;
////            }
////
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////            String fields = metaDataAsset.getFields();
////            JSONArray fieldArray = null;
////            try {
////                fieldArray = JSONArray.fromObject(fields);
////            } catch (Exception ex) {
////                fieldArray = new JSONArray();
////            }
////            MetaDataField metaDataField = new MetaDataField();
////            metaDataField.setName(field_name);
////            metaDataField.setAlias(request.getParameter("field_alias"));
////            metaDataField.setValidation(request.getParameter("field_validation"));
////            metaDataField.setNullable(Boolean.valueOf(request.getParameter("field_nullable")));
////            metaDataField.setDescription(request.getParameter("field_description"));
////
////            JSONArray new_field_array = new JSONArray();
////            for (int i = 0; i < fieldArray.size(); i++) {
////                String name = fieldArray.getJSONObject(i).getString("name");
////                if (name.equalsIgnoreCase(old_field_name)) {
////                    new_field_array.add(JSONObject.fromObject(metaDataField));
////                } else {
////                    new_field_array.add(fieldArray.getJSONObject(i));
////                }
////            }
////            metaDataAssetService.metadataUpdateFields(schema_name, asset_name, new_field_array.toString());
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("更新字段成功！");
////            return result;
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("更新字段时出现错误！");
////            return result;
////        }
////    }
//
////    /**
////     * 更新字段
////     *
////     * @param request
////     * @param response
////     * @return
////     */
////    @RequestMapping("/move_field_position")
////    @ResponseBody
////    public JsonResult moveFieldPosition(HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            String asset_name = request.getParameter("asset_name");
////            if (RegexUtil.isNull(asset_name)) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("资产表名不能为空！");
////                return result;
////            }
////
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////            String field_name = request.getParameter("field_name");
////            String direction = request.getParameter("direction");
////
////            org.json.JSONArray jsonArray = new org.json.JSONArray(metaDataAsset.getFields());
////            org.json.JSONArray newFieldArray = new org.json.JSONArray();
////            int field_position = -1;
////            for (int i = 0; i < jsonArray.length(); i++) {
////                org.json.JSONObject item = jsonArray.getJSONObject(i);
////                String field_name2 = item.optString("name", "");
////                if (field_name2.equalsIgnoreCase(field_name)) {
////                    field_position = i;
////                    break;
////                }
////            }
////            if (field_position == -1) {
////                result.setCode(JsonResult.CODE_ERROR);
////                result.setMsg("找不到指定字段！");
////                return result;
////            }
////            org.json.JSONObject move_field = jsonArray.getJSONObject(field_position);
////
////            switch (direction) {
////                case "0"://上移
////                    if (field_position == 0) {
////                        //已经在第一个，直接返回
////                        result.setCode(JsonResult.CODE_OK);
////                        return result;
////                    }
////                    org.json.JSONObject pre_field = jsonArray.getJSONObject(field_position - 1);
////
////                    for (int i = 0; i < jsonArray.length(); i++) {
////                        if (i == (field_position - 1)) {
////                            newFieldArray.put(move_field);
////                        } else if (i == field_position) {
////                            newFieldArray.put(pre_field);
////                        } else {
////                            newFieldArray.put(jsonArray.get(i));
////                        }
////                    }
////                    break;
////                case "1"://下移
////                    if (field_position == jsonArray.length() - 1) {
////                        //已经在最后一个，直接返回
////                        result.setCode(JsonResult.CODE_OK);
////                        return result;
////                    }
////                    org.json.JSONObject next_field = jsonArray.getJSONObject(field_position + 1);
////                    for (int i = 0; i < jsonArray.length(); i++) {
////                        if (i == (field_position + 1)) {
////                            newFieldArray.put(move_field);
////                        } else if (i == field_position) {
////                            newFieldArray.put(next_field);
////                        } else {
////                            newFieldArray.put(jsonArray.get(i));
////                        }
////                    }
////                    break;
////                case "2"://置顶
////                    newFieldArray.put(move_field);
////                    for (int i = 0; i < jsonArray.length(); i++) {
////                        if (i == field_position) {
////                            continue;
////                        } else {
////                            newFieldArray.put(jsonArray.get(i));
////                        }
////                    }
////                    break;
////                case "3"://置底
////                    for (int i = 0; i < jsonArray.length(); i++) {
////                        if (i == field_position) {
////                            continue;
////                        } else {
////                            newFieldArray.put(jsonArray.get(i));
////                        }
////                    }
////                    newFieldArray.put(move_field);
////                    break;
////                default:
////            }
////            metaDataAssetService.metadataUpdateFields(schema_name, asset_name, newFieldArray.toString());
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("更新字段成功！");
////            return result;
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("更新字段时出现错误！");
////            return result;
////        }
////    }
////
////    /**
////     * 导出资源定义成模板
////     *
////     * @param request
////     * @param response
////     * @return
////     */
////    @RequestMapping("/export")
////    @ResponseBody
////    public ModelAndView export(HttpServletRequest request, HttpServletResponse response) {
////        MetadataExportResult metadataExportResult = new MetadataExportResult();
////        try {
////            String asset_name = request.getParameter("asset_name");
////            String schema_name = authService.getCompany(request).getSchema_name();
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////            metadataExportResult.setCode(JsonResult.CODE_OK);
////            metadataExportResult.setMsg("导出模板成功！");
////            org.json.JSONObject data = new org.json.JSONObject(metaDataAsset);
////            data.remove("schema_name");//去掉schema_name字段
////            org.json.JSONArray fieldArray = new org.json.JSONArray(metaDataAsset.getFields());
////            data.put("fields", fieldArray);
////            data.put("version", "1.0");
////            metadataExportResult.setData(data.toString());
////        } catch (Exception ex) {
////            metadataExportResult.setCode(JsonResult.CODE_ERROR);
////            metadataExportResult.setMsg("导出模板失败！");
////        }
////        return new ModelAndView("metadata_export")
////                .addObject("result", metadataExportResult);
////    }
////
////    @RequestMapping("/import")
////    public ModelAndView importMetadata(@RequestParam(value = "file_import", required = false) MultipartFile file,
////                                       HttpServletRequest request, HttpServletResponse response) {
////        JsonResult result = new JsonResult();
////        try {
////            String path = request.getSession().getServletContext().getRealPath("upload");
////            String fileName = file.getOriginalFilename();
////            File targetFile = new File(path, fileName);
////            File targetPath = new File(path);
////            if (!targetPath.exists()) {
////                targetPath.mkdirs();
////            }
////            // 保存
////            try {
////                file.transferTo(targetFile);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////            //region 导入json
////            StringBuffer jsonString = new StringBuffer();
////            FileUtil.readToBuffer(jsonString, new FileInputStream(targetFile));
////            net.sf.json.JSONObject metaObject = net.sf.json.JSONObject.fromObject(jsonString.toString().trim());
////            net.sf.json.JSONArray fieldArray = metaObject.optJSONArray("fields");
////            metaObject.remove("fields");
////
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            MetaDataAsset templateAsset = (MetaDataAsset) net.sf.json.JSONObject.toBean(metaObject, MetaDataAsset.class);
////            String newAssetName = request.getParameter("asset_name");
////            String newAssetAlias = request.getParameter("asset_alias");
////            String newAssetDescription = request.getParameter("description");
////            templateAsset.setAsset_name(newAssetName);
////            templateAsset.setAsset_alias(newAssetAlias);
////            templateAsset.setDescription(newAssetDescription);
////            templateAsset.setSchema_name(schema_name);
////            templateAsset.setFields(fieldArray.toString());
////
////            metaDataAssetService.insertMetaDataAsset(templateAsset);
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("导入成功！");
////            //endregion
////        } catch (Exception e) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("导入失败！");
////        }
////        return new ModelAndView("metadata_import")
////                .addObject("result", result);
////    }
////
////    @RequestMapping("/clone")
////    public ModelAndView clone(HttpServletRequest request, HttpServletResponse response) {
////        String origin_asset_name = request.getParameter("asset_name");
////        String schema_name = authService.getCompany(request).getSchema_name();
////
////        MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(origin_asset_name));
////
////        return new ModelAndView("metadata_asset/metadata_asset_clone")
////                .addObject("asset_name", request.getParameter("asset_name"))
////                .addObject("metadata_origin", metaDataAsset);
////    }
////
////    @RequestMapping("/clone2")
////    public JsonResult doClone(HttpServletRequest request, HttpServletResponse response) {
////        String origin_asset_name = request.getParameter("origin_asset_name");
////        JsonResult result = new JsonResult();
////        try {
////            String asset_name = request.getParameter("asset_name");
////            String asset_alias = request.getParameter("asset_alias");
////            String description = request.getParameter("description");
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name,Integer.parseInt(origin_asset_name));
////            metaDataAsset.setAsset_name(asset_name);
////            metaDataAsset.setAsset_alias(asset_alias);
////            metaDataAsset.setDescription(description);
////            metaDataAsset.setSchema_name(schema_name);
////            metaDataAssetService.insertMetaDataAsset(metaDataAsset);
////            result.setCode(JsonResult.CODE_OK);
////            result.setMsg("克隆成功！");
////        } catch (Exception ex) {
////            result.setCode(JsonResult.CODE_ERROR);
////            result.setMsg("克隆失败！");
////        }
////        return result;
////    }
//
////    @RequestMapping("/export_excel_template")
////    public ModelAndView exportExcelTemplate(HttpServletRequest request, HttpServletResponse response) {
////        String asset_name = request.getParameter("asset_name");
////        String schema_name = authService.getCompany(request).getSchema_name();
////
////        MetaDataAsset metaDataAsset = metaDataAssetService.findMetaDataAsset(schema_name, Integer.parseInt(asset_name));
////
////        MetadataExcelTemplate excelView = new MetadataExcelTemplate();
////        Map<String, Object> map = new HashedMap();
////        map.put("asset_name", asset_name);
////        map.put("asset_alias", metaDataAsset.getAsset_alias());
////
////        try {
////            JSONArray systemFileds = MetaDataAsset.getSystemFields();
////            JSONArray customFields = JSONArray.fromObject(metaDataAsset.getFields());
////            for (int i = 0; i < customFields.size(); i++) {
////                systemFileds.add(customFields.getJSONObject(i));
////            }
////
////            //region 去掉_ID字段
////            for (int i = 0; i < systemFileds.size(); i++) {
////                JSONObject jsonObject = systemFileds.getJSONObject(i);
////                String name = jsonObject.optString("name", "");
////                if (MetaDataAsset.SYS_ID.equalsIgnoreCase(name)) {
////                    systemFileds.remove(i);
////                    break;
////                }
////            }
////            //endregion
////
////            //region 去掉设备类型字段
////            for (int i = 0; i < systemFileds.size(); i++) {
////                JSONObject jsonObject = systemFileds.getJSONObject(i);
////                String name = jsonObject.optString("name", "");
////                if (MetaDataAsset.ASSET_TYPE.equalsIgnoreCase(name)) {
////                    systemFileds.remove(i);
////                    break;
////                }
////            }
////            //endregion
////            map.put("fields", systemFileds);
////        } catch (Exception ex) {
////
////        }
////        //region 获得所有位置
////        List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
////        map.put("facilityList", facilityList);
////        //endregion
////
////        //region 获得所有设备状态
////        List<AssetStatus> assetStatusList = assetStatusService.findAllInUse(schema_name);
////        map.put("assetStatusList", assetStatusList);
////        //endregion
////
////        //region 获得关联企业列表
////        List<CustomersData> customerList = customerService.findAllCustomerList3(schema_name);
////        map.put("customerList", customerList);
////        //endregion
////        List<Unit> UnitList = assetDataServiceV2.findUnitList(schema_name);
////        map.put("unitList", UnitList);
////
////        List<Map<String,Object>>  currencyList= selectOptionService.SelectOptionList(schema_name,"currency","");
////        map.put("currencyList", currencyList);
////        List<Map<String,Object>>  asset_subject= new ArrayList<Map<String,Object>>();
////        Map asset_subjectmap=new HashMap();
////        Map asset_subjectmap2=new HashMap();
////        asset_subjectmap.put("1","资产");
////        asset_subject.add(asset_subjectmap);
////        asset_subjectmap2.put("2","费用");
////        asset_subject.add(asset_subjectmap2);
////        map.put("asset_subject", asset_subject);
////
////        //等级
////        List<ImportmentLevel> importmentLevelList = assetDataServiceV2.getImportmentLevels(schema_name);
////        map.put("importmentLevelList", importmentLevelList);
////
////        //设备类型
////        List<AssetCategory> categoryList = assetCategoryService.findAll(schema_name);
////        map.put("categoryList", categoryList);
////
////        return new ModelAndView(excelView, map);
////    }
//
//
////    //初始化设备模板的工单类型任务模板
////    @RequestMapping("/asset_template_task_template_init")
////    public ModelAndView initAssetTemplateTaskTemplate(HttpServletRequest request, HttpServletResponse response) {
////        ResponseModel result = new ResponseModel();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            result.setContent(null);
////            result.setCode(1);
////            result.setMsg("");
////
////            List<WorkTypeData> workTypeDataList = maintenanceSettingsService.getWorkTypeDataList(schema_name);
////            List<TaskTemplate> taskTemplateDataList = taskTempLateService.findPlanTaskTemplateList(schema_name);
////
////            return new ModelAndView("metadata_asset/task_template/task_template_manage").addObject("result", result)
////                    .addObject("workTypeList", workTypeDataList)
////                    .addObject("taskTemplateList", taskTemplateDataList);
////        } catch (Exception ex) {
////            result.setCode(-1);
////            result.setMsg("获取任务模板时出现错误：" + ex.getMessage());
////            return new ModelAndView("metadata_asset/task_template/task_template_manage").addObject("result", result);
////        }
////    }
////
////    /**
////     * 获得所有的设备模板对应的工单类型任务模板
////     *
////     * @param request
////     * @param response
////     * @return
////     */
////    @RequestMapping("/asset_template_task_template_list")
////    public ModelAndView getAssetTemplateTaskTemplateList(HttpServletRequest request, HttpServletResponse response) {
////        ResponseModel result = new ResponseModel();
////        try {
////            String schema_name = authService.getCompany(request).getSchema_name();
////            String asset_template_code = request.getParameter("asset_template_code");
////
////            List<AssetTemplateTaskTemplateData> taskTemplateList = assetCategoryService.getAssetTemplateTaskTemplate(schema_name, asset_template_code);
////            result.setContent(taskTemplateList);
////            result.setCode(1);
////            result.setMsg("");
////            return new ModelAndView("metadata_asset/task_template/task_template_list").addObject("result", result);
////        } catch (Exception ex) {
////            result.setCode(-1);
////            result.setMsg("获取任务模板时出现错误：" + ex.getMessage());
////            return new ModelAndView("metadata_asset/task_template/task_template_list").addObject("result", result);
////        }
////    }
////
////    //新增设备模板的工单类型的任务模板
////    @RequestMapping("/asset_template_task_template_save")
////    public ResponseModel getAddAssetTemplateTaskTemplate(HttpServletRequest request, HttpServletResponse response) {
////        //保存任务模板
////        ResponseModel result = new ResponseModel();
////        try {
////            String asset_template_code = request.getParameter("asset_template_code");
////            Integer work_type_id = Integer.parseInt(request.getParameter("work_type_id"));
////            String task_template_code = request.getParameter("task_template_code");
////            String schema_name = authService.getCompany(request).getSchema_name();
////
////            if (asset_template_code == null || task_template_code == null || work_type_id <= 0) {
////                result.setCode(-1);
////                result.setMsg("没有选择工单类型或任务模板。");
////                return result;
////            } else {
////                User user = authService.getLoginUser(request);
////                Timestamp create_time = new Timestamp(System.currentTimeMillis());
////
////                //看看是否有重复，如果有重复，则提示不需要保存
////                List<AssetTemplateTaskTemplateData> taskTemplateList = assetCategoryService.getAssetTemplateTaskTemplate(schema_name, asset_template_code);
////                if (null != taskTemplateList && !taskTemplateList.isEmpty()) {
////                    boolean isHave = false;
////                    for (AssetTemplateTaskTemplateData item : taskTemplateList) {
////                        if (work_type_id == item.getWork_type_id() && item.getTask_template_code().equals(task_template_code)) {
////                            isHave = true;
////                            break;
////                        }
////                    }
////                    if (isHave) {
////                        result.setCode(-1);
////                        result.setMsg("任务配置重复。");
////                        return result;
////                    }
////                }
////                //进行新增配置
////                AssetTemplateTaskTemplateData addData = new AssetTemplateTaskTemplateData();
////                addData.setCreate_time(create_time);
////                addData.setAsset_template_code(asset_template_code);
////                addData.setWork_type_id(work_type_id);
////                addData.setTask_template_code(task_template_code);
////                addData.setCreate_user_account(user.getAccount());
////
////                //保存资产关联的任务模板
////                int count = assetCategoryService.addAssetTemplateTaskTemplate(schema_name, addData);
////
////                if (count > 0) {
////                    result.setCode(1);
////                    result.setMsg("保存任务模板成功。");
////                } else {
////                    result.setCode(-1);
////                    result.setMsg("保存任务模板失败。");
////                }
////                //返回保存结果
////                return result;
////            }
////        } catch (Exception ex) {
////            result.setCode(-1);
////            result.setMsg("保存任务模板失败。");
////            return result;
////        }
////    }
////
////
////    //删除设备模板对应的工单类型任务模板
////    @RequestMapping("/asset_template_task_template_delete")
////    public ResponseModel getDeleteAssetTemplateTaskTemplate(HttpServletRequest request, HttpServletResponse response) {
////        //删除资产关联企业
////        ResponseModel result = new ResponseModel();
////        String asset_template_code = request.getParameter("asset_template_code");
////        Integer work_type_id = Integer.parseInt(request.getParameter("work_type_id"));
////        String task_template_code = request.getParameter("task_template_code");
////        String schema_name = authService.getCompany(request).getSchema_name();
////
////        if (asset_template_code == null || task_template_code == null || work_type_id <= 0) {
////            result.setCode(-1);
////            result.setMsg("数据有误。");
////            return result;
////        } else {
////            int count = assetCategoryService.deletaAssetTemplateTaskTemplate(schema_name, asset_template_code, work_type_id, task_template_code);
////            if (count > 0) {
////                result.setCode(1);
////                result.setMsg("删除任务模板成功。");
////            } else {
////                result.setCode(-1);
////                result.setMsg("删除任务模板失败。");
////            }
////        }
////        //返回保存结果
////        return result;
////    }

}
