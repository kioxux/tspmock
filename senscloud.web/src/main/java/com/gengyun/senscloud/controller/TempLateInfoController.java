package com.gengyun.senscloud.controller;

///*
// * 处理模板详情，及模板下的任务项等操作
// */
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.TaskTempLateItem;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.WorkTask;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//@RestController
//@RequestMapping("/template_info")
public class TempLateInfoController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    AssetInfoService assetInfoService;
//    @Autowired
//    WorkTaskService workTaskService;
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    MaintainService maintainService;
//    @Autowired
//    TaskTempLateService taskTempLateService;
//    @Autowired
//    RepairService repairService;
//    @Autowired
//    AnalysReportService analysReportService;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 初始化模板任务项
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("task_template")
//    @RequestMapping("/template_task_init")
//    public ResponseModel InitAssetBomManage(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            result.setCode(1);
//            String condtion = "";
//            List<WorkTask> taskList = workTaskService.getAllWorkTaskList(schema_name, condtion);
//            result.setContent(taskList);
//            return result;
//
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_TEMP_TASK) + ex.getMessage());//获取模板任务项时出现错误
//            return result;
//        }
//    }
//
//    /**
//     * 获得所有的任务项
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("task_template")
//    @RequestMapping("/template_task_list")
//    public ModelAndView FindALLAssetBomList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            String template_id = request.getParameter("template_id");
//            String condtion;
//            List<WorkTask> taskdata = taskTempLateService.gettemplatecodeList(schema_name, template_id);
//            result.setContent(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));
//            result.setCode(1);
//            result.setMsg("");
//            return new ModelAndView("template_info/template_task_list").addObject("result", result).addObject("taskdata", taskdata)
//                    .addObject("schema_name", schema_name)
//                    .addObject("user", user)
//                    .addObject("selectOptionService", selectOptionService);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_BOM_ERR) + ex.getMessage());
//            return new ModelAndView("template_info/template_task_list").addObject("result", result);
//        }
//    }
//
//    /**
//     * 保存模板任务项
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_template")
//    @RequestMapping("/template_task_save")
//    public ResponseModel getAddTemplateTask(@RequestParam("listtask[]") List<String> listtask, HttpServletRequest request, HttpServletResponse response) {
//        //保存模板任务项
//        ResponseModel result = new ResponseModel();
//        try {
//            String task_template_code = request.getParameter("template_id");//模板id
//            listtask.size();//任务项id
//            if (listtask.size() == 0 || listtask.isEmpty()) {
//                result.setCode(-1);
//                result.setMsg("任务项没有选择");
//                return result;
//            } else {
//                Company company = authService.getCompany(request);
//                String schema_name = company.getSchema_name();
//                User user = authService.getLoginUser(request);
//                String maxorder = "";
//                int neworder = 0;
//                //看看是否有重复，如果有重复，则执行更新操作,
//                for (int i = 0; i < listtask.size(); i++) {
//                    TaskTempLateItem list = taskTempLateService.findtemplatetask(schema_name, task_template_code, listtask.get(i));
//                    maxorder = taskTempLateService.findMaxtemplatetask(schema_name, task_template_code);
//                    if (maxorder != null && !"".equals(maxorder)) {
//                        neworder = Integer.parseInt(maxorder);
//                    }
//                    //保存模板任务项
//                    int count = 0;
//                    if (list == null) {//如果没有，则新增
//                        TaskTempLateItem ttl = new TaskTempLateItem();
//                        ttl.setTask_item_code(listtask.get(i));
//                        ttl.setOrder(neworder + 1);
//                        ttl.setTask_template_code(task_template_code);
//                        count = taskTempLateService.addTasktemplateitemList(schema_name, ttl);
//                    } else {//如果有更新原来的数据
//                        count = taskTempLateService.updateTemplateTask(schema_name, task_template_code, listtask.get(i), neworder + 1);
//                    }
//                    if (count > 0) {
//                        //增加日志
//                        logService.AddLog(schema_name, "template", task_template_code, selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_BOM_TASK) + task_template_code, user.getAccount());
//                        result.setCode(1);
//                        result.setMsg(selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//                    } else {
//                        result.setCode(-1);
//                        result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//                    }
//                }
//            }
//            //返回保存结果
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            return result;
//        }
//    }
//
//    /**
//     * 删除模板任务项
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_template")
//    @RequestMapping("/template_task_delete")
//    public ResponseModel getDeleteAssetBom(HttpServletRequest request, HttpServletResponse response) {
//        //删除模板任务项
//        ResponseModel result = new ResponseModel();
//        String task_template_code = request.getParameter("template_id");//模板id
//        String task_item_code = request.getParameter("task_code");//任务项id
//        if (task_item_code == null || task_template_code == null) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_UN_CHOICE_TASK));
//        } else {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            User user = authService.getLoginUser(request);
//            //删除模板任务项
//            int count = taskTempLateService.deleteTemplateTask(schema_name, task_template_code, task_item_code);
//            if (count > 0) {
//                //增加日志
//                logService.AddLog(schema_name, "template", task_template_code, selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_BOM_TASK) + task_item_code, user.getAccount());
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));//删除设备备件成功
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));//删除设备备件失败
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    /**
//     * 上移下移模板任务项
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("task_template")
//    @RequestMapping("/template_task_move")
//    public ResponseModel getMoveTamplateTask(HttpServletRequest request, HttpServletResponse response) {
//        //删除模板任务项
//        ResponseModel model = new ResponseModel();
//        try {
//            String task_template_code = request.getParameter("template_id");//模板id
//            String task_item_code = request.getParameter("task_code");//任务项id
//            String direction = request.getParameter("direction");//移动方向
//            if (task_item_code == null || task_template_code == null) {
//                model.setCode(-1);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_DATADATA));//未选择任务
//                return model;
//            }
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            User user = authService.getLoginUser(request);
//            //删除模板任务项
//            TaskTempLateItem ttnow = taskTempLateService.findtemplatetask(schema_name, task_template_code, task_item_code);
//            TaskTempLateItem ttnew;
//            String condtion = " and t.task_template_code='" + task_template_code + "' ";
//            List<TaskTempLateItem> list = new ArrayList<TaskTempLateItem>();
//            if ("upper".equals(direction)) {
//                condtion += "and t.order<" + ttnow.getOrder();
//                list = taskTempLateService.findtemplatetaskorder(schema_name, condtion);
//                if (list.isEmpty() || list.size() == 0) {
//                    model.setCode(-1);
//                    model.setContent("");
//                    model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_MOVE_UP));//无法上移
//                    return model;
//                }
//                Collections.sort(list, new Comparator<TaskTempLateItem>() {
//                    @Override
//                    public int compare(TaskTempLateItem o1, TaskTempLateItem o2) {
//                        //升序
//                        return o1.getOrder() - o2.getOrder();
//                    }
//                });
//                ttnew = list.get(list.size() - 1);
//            } else {
//                condtion += "and t.order>" + ttnow.getOrder();
//                list = taskTempLateService.findtemplatetaskorder(schema_name, condtion);
//                if (list.isEmpty() || list.size() == 0) {
//                    model.setCode(-1);
//                    model.setContent("");
//                    model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_MOVE_DOWN));//无法下移
//                    return model;
//                }
//                Collections.sort(list, new Comparator<TaskTempLateItem>() {
//                    @Override
//                    public int compare(TaskTempLateItem o1, TaskTempLateItem o2) {
//                        //升序
//                        return o1.getOrder() - o2.getOrder();
//                    }
//                });
//                ttnew = list.get(0);
//            }
//            int order = ttnow.getOrder();
//            ttnow.setOrder(ttnew.getOrder());
//
//            ttnew.setOrder(order);
//            int i = taskTempLateService.updateTemplateTask(schema_name, ttnow.getTask_template_code(), ttnow.getTask_item_code(), ttnow.getOrder());
//
//            int s = taskTempLateService.updateTemplateTask(schema_name, ttnew.getTask_template_code(), ttnew.getTask_item_code(), ttnew.getOrder());
//            if (i > 0 && s > 0) {
//                model.setCode(1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//            } else if (i > 0 && s <= 0) {
//                model.setCode(-1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            } else if (i <= 0 && s > 0) {
//                model.setCode(1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            } else {
//                model.setCode(1);
//                model.setContent("");
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(-1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            return model;
//        }
//    }
//
//    /**
//     * 功能：根据任务模板编号查询任务项内容
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/template_task_list_by_template_id")
//    public ResponseModel templateTaskListByTemplate(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String template_id = request.getParameter("template_id");
//            String condtion;
//            List<WorkTask> taskdata = taskTempLateService.gettemplatecodeList(schema_name, template_id);
//            if (taskdata != null && taskdata.size() > 0) {
//                result.setContent(taskdata);
//                result.setCode(StatusConstant.SUCCES_RETURN);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_SU));//任务项查询成功
//            } else {
//                result.setCode(StatusConstant.NO_DATE_RETURN);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_DE));//未查询到任务项数据
//            }
//            return result;
//        } catch (Exception ex) {
//            result.setCode(StatusConstant.ERROR_RETURN);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_DE));//任务项查询出现异常
//            return result;
//        }
//    }
}
