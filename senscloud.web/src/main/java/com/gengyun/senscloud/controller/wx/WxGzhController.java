package com.gengyun.senscloud.controller.wx;

import com.gengyun.senscloud.service.system.WxGzhMsgService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.wx.gzh.MessageUtil;
import com.gengyun.senscloud.util.wx.gzh.SignUtil;
import com.gengyun.senscloud.util.wx.gzh.WxGzhUtil;
import io.swagger.annotations.Api;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@Api(tags = "微信公众号服务接口")
@RestController
public class WxGzhController {
    private static final Logger logger = LoggerFactory.getLogger(WxGzhController.class);
    @Resource
    private WxGzhMsgService wxGzhMsgService;

    @RequestMapping({"/wxMsg"})
    public String searchList(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("WxGzhController wxMsg ************************  ");
        try {
            boolean isGet = "get".equals(request.getMethod().toLowerCase());
            PrintWriter print;
            if (isGet) {//get请求：公众号接入认证（验证token）
                // 微信加密签名
                String signature = request.getParameter("signature");
                // 时间戳
                String timestamp = request.getParameter("timestamp");
                // 随机数
                String nonce = request.getParameter("nonce");
                // 随机字符串
                String echostr = request.getParameter("echostr");
                String serverName = request.getServerName();
                logger.debug("WxGzhController wxMsg params   ~   signature:"+signature+"    timestamp："+timestamp+"   nonce:"+nonce+"    echostr:"+echostr);
                //获取公众号配置信息
                List<Map<String, Object>> golbalInfoList = wxGzhMsgService.queryGlobalInfoListByWeixinCode(serverName);
                if(golbalInfoList != null && golbalInfoList.size() > 0){
                    String token = (String)golbalInfoList.get(0).get("reserve4");
                    // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
                    if (StringUtils.isNotBlank(token) && signature != null && SignUtil.checkSignature(token, signature, timestamp, nonce)) {
                        try {
                            print = response.getWriter();
                            print.write(echostr);
                            print.flush();
                        } catch (IOException e) {
                            logger.error("", e);
                        }
                    }
                }
                return null;
            }else{//post请求：微信消息、事件推送等
                Map<String, String> map = MessageUtil.parseXml(request);//把微信返回的xml信息转义成map
                logger.debug("WxGzhController request params:" + map.toString());
                //ToUserName	开发者微信号原始ID
                //FromUserName	发送方帐号（一个OpenID）
                //CreateTime	消息创建时间 （整型）
                //MsgType	消息类型，event
                //Event	事件类型，subscribe
                //EventKey	事件KEY值，qrscene_为前缀，后面为二维码的参数值
                //Ticket 二维码的ticket，可用来换取二维码图片
                String fromUserName = map.get("FromUserName");
                String toUserName = map.get("ToUserName");
                String msgType = map.get("MsgType");
                String content = map.get("Content");
                String eventType = map.get("Event");
                String message = "";
                if(MessageUtil.REQ_MESSAGE_TYPE_EVENT.equals(msgType)){//如果为事件类型
                    if(MessageUtil.EVENT_TYPE_SUBSCRIBE.equals(eventType) && StringUtils.isNotBlank(toUserName)){//处理订阅事件
                        /**************************************************************
                         *
                         * 处理业务   begin
                         *
                         * ************************************************************/
                        //获取公众号配置信息
                        List<Map<String, Object>> golbalInfoList = wxGzhMsgService.queryGlobalInfoListByWeixinCode(toUserName);
                        if(golbalInfoList != null && golbalInfoList.size() >0) {
                            Map<String, Object> globalInfo = golbalInfoList.get(0);
                            String schemaName = (String) globalInfo.get("reserve2");
                            Object reserve6 = globalInfo.get("reserve6");//使用本公众号的客户schema，可能是多个客户
                            //wxGzhMsgService.checkWxAccessToken(schemaName);//如果accesstoken过期，重新获取一次（防止token更新job异常时，遗漏关注用户）
                            String accessToken = wxGzhMsgService.getAccessTokenByDb(schemaName);
                            String userInfo = WxGzhUtil.getUserClientInfo(accessToken, fromUserName);
                            if (RegexUtil.optIsPresentStr(userInfo)){
                                JSONObject user = JSONObject.fromObject(userInfo);
                                int errcode = user.optInt("errcode");
                                if(WxGzhUtil.ERRCODE_ACCESS_TOKEN_EXPIRED == errcode){
                                    logger.error("wx official account access_token ia expired or invalid!");
                                    wxGzhMsgService.checkWxAccessToken(schemaName);//如果accesstoken过期，重新获取一次（防止token更新job异常时，遗漏关注用户）
                                    accessToken = wxGzhMsgService.getAccessTokenByDb(schemaName);
                                    userInfo = WxGzhUtil.getUserClientInfo(accessToken, fromUserName);
                                    if(RegexUtil.optIsPresentStr(userInfo)) {
                                        user = JSONObject.fromObject(userInfo);
                                    }
                                }
                                String openId = user.optString("openid");
                                String unionId = user.optString("unionid");
                                if(StringUtils.isNotBlank(unionId) && StringUtils.isNotBlank(openId)) {
                                    //记录微信公众号用户
                                    wxGzhMsgService.newWxGzhUser(schemaName, toUserName, openId, unionId);
                                    //给关注了小程序的客户的用户绑定公众号openid
                                    if(reserve6 != null) {
                                        String[] schemaList = ((String) reserve6).split(",");
                                        if(schemaList.length >0){
                                            for(String schema : schemaList){
                                                wxGzhMsgService.modifyUserWxGzhOpenId(schema, unionId, openId);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        /**************************************************************
                         *
                         * 处理业务   end
                         *
                         * ************************************************************/
                        message = MessageUtil.subscribeForText(toUserName, fromUserName);
                    }else if(MessageUtil.EVENT_TYPE_UNSUBSCRIBE.equals(eventType)){//处理取消订阅事件
                        /**************************************************************
                         *
                         * 处理业务   begin
                         *
                         * ************************************************************/
                        //删除微信公众号用户
                        wxGzhMsgService.cutWxGzhUser(toUserName, fromUserName);
                        //获取公众号配置信息
                        List<Map<String, Object>> golbalInfoList = wxGzhMsgService.queryGlobalInfoListByWeixinCode(toUserName);
                        if(golbalInfoList != null && golbalInfoList.size() >0) {
                            Map<String, Object> globalInfo = golbalInfoList.get(0);
                            Object reserve6 = globalInfo.get("reserve6");//使用本公众号的客户schema，可能是多个客户
                            //清空关注了小程序的客户的用户绑定的公众号openid
                            if(reserve6 != null) {
                                String[] schemaList = ((String) reserve6).split(",");
                                if(schemaList.length >0){
                                    for(String schema : schemaList){
                                        wxGzhMsgService.cutUserWxGzhOpenId(schema, fromUserName);
                                    }
                                }
                            }
                        }
                        /**************************************************************
                         *
                         * 处理业务   end
                         *
                         * ************************************************************/
                        message = MessageUtil.unsubscribe(toUserName, fromUserName);
                    }
                }else{
                    //微信消息分为事件推送消息和普通消息,非事件即为普通消息类型
                    switch (msgType) {
                        case MessageUtil.REQ_MESSAGE_TYPE_TEXT:
                            break;
                        case MessageUtil.REQ_MESSAGE_TYPE_IMAGE:
                            break;
                        case MessageUtil.REQ_MESSAGE_TYPE_LINK:
                            break;
                        default:{//其他类型的普通消息
                            break;
                        }
                    }
                }
                return message;
            }
        } catch (Exception e) {
            logger.error("", e);
            return "{}";
        }
    }

}
