package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.UserModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.business.UserService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * 用户管理
 */
@Api(tags = "用户管理")
@RestController
public class UserController {
    @Resource
    UserService userService;

    @ApiOperation(value = "获取用户模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_USER_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchUserListPermission", method = RequestMethod.POST)
    public ResponseModel searchUserListPermission(MethodParam methodParam) {
        return ResponseModel.ok(userService.getUserListPermission(methodParam));
    }

    @ApiOperation(value = "新增用户", notes = ResponseConstant.RSP_DESC_ADD_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "user_name", "account", "password", "gender_tag", "user_code",
                    "mobile", "join_date", "email", "hour_fee", "positionIds", "is_use", "is_charge", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseModel addUser(MethodParam methodParam, UserModel userModel) {
        userService.newUser(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除用户", notes = ResponseConstant.RSP_DESC_REMOVE_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeUser", method = RequestMethod.POST)
    public ResponseModel removeUser(MethodParam methodParam, UserModel userModel) {
        userService.cutUser(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除选中用户", notes = ResponseConstant.RSP_DESC_REMOVE_SELECT_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "ids"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeSelectUser", method = RequestMethod.POST)
    public ResponseModel removeSelectUser(MethodParam methodParam, UserModel userModel) {
        userService.cutSelectUser(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑用户", notes = ResponseConstant.RSP_DESC_EDIT_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "sys_user", "user_name", "account", "password", "gender_tag", "user_code",
                    "mobile", "join_date", "email", "hour_fee", "positionIds", "status", "is_charge", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/editUser", method = RequestMethod.POST)
    public ResponseModel editUser(MethodParam methodParam, UserModel userModel) {
        userService.modifyUser(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "启用禁用用户", notes = ResponseConstant.RSP_DESC_CHANGE_IS_USE_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "sys_user", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/changeIsUseUser", method = RequestMethod.POST)
    public ResponseModel changeIsUseUser(MethodParam methodParam, UserModel userModel) {
        userService.changeIsUse(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "重置用户密码", notes = ResponseConstant.RSP_DESC_CHANGE_USER_PASSWORD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "sys_user", "password"})
    @Transactional //支持事务
    @RequestMapping(value = "/changeUserPassword", method = RequestMethod.POST)
    public ResponseModel changeUserPassword(MethodParam methodParam, UserModel userModel) {
        userService.changeUserPassword(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "用户详情", notes = ResponseConstant.RSP_DESC_SEARCH_USER_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchUserInfo", method = RequestMethod.POST)
    public ResponseModel searchUserInfo(MethodParam methodParam, UserModel userModel) {
        return ResponseModel.ok(userService.getUserInfo(methodParam, userModel));
    }

    @ApiOperation(value = "分页查询用户列表", notes = ResponseConstant.RSP_DESC_SEARCH_USER_LIST_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "statusSearch", "positionSearch"})
    @RequestMapping(value = "/searchUserListPage", method = RequestMethod.POST)
    public ResponseModel searchUserListPage(MethodParam methodParam, UserModel userModel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(userService.getUserListForPage(methodParam, userModel));
    }

    @RequestMapping(value = "/searchNowUserId", method = RequestMethod.POST)
    public String searchNowUserId(MethodParam methodParam) {
        return methodParam.getUserId();
    }

    @ApiOperation(value = "更新员工上下班状态", notes = ResponseConstant.RSP_DESC_CHANGE_USER_IS_ON_DUTY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "is_on_duty"})
    @Transactional //支持事务
    @RequestMapping(value = "/changeUserIsOnDuty", method = RequestMethod.POST)
    public ResponseModel changeUserIsOnDuty(MethodParam methodParam, UserModel userModel) {
        userService.changeUserIsOnDuty(methodParam, userModel);
        return ResponseModel.okMsgForOperate();
    }


    @ApiOperation(value = "导出选中用户", notes = ResponseConstant.RSP_DESC_SHOW_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/exportUser", method = RequestMethod.POST)
    public ModelAndView exportUser(MethodParam methodParam, UserModel model, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        return userService.doExportUser(methodParam, model);
    }

    @ApiOperation(value = "导出全部用户", notes = ResponseConstant.RSP_DESC_EXPORT_USER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "isUseSearch", "positionSearch"})
    @RequestMapping(value = "/exportAllUser", method = RequestMethod.POST)
    public ModelAndView exportAllUser(MethodParam methodParam, UserModel model) {
        return userService.doExportAllUser(methodParam, model);
    }

}
