package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.LangSearchParam;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.language.MultipleLanguagesService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;


@Api(tags = "多语言")
@RestController
public class MultipleLanguagesController {
    @Resource
    MultipleLanguagesService multipleLanguagesService;

    @ApiOperation(value = "获取用户权限", notes = ResponseConstant.RSP_DESC_GET_LANG_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getLangListPermission", method = RequestMethod.POST)
    public ResponseModel searchLangListPermission(MethodParam methodParam) {
        return ResponseModel.ok(multipleLanguagesService.getAssetListPermission(methodParam));
    }

    @ApiOperation(value = "获取多语言列表（含手机端）", notes = ResponseConstant.RSP_DESC_GET_LANGUAGES_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "companyIdSearch", "keywordSearch"})
    @RequestMapping(value = "/getLanguagesList", method = RequestMethod.POST)
    public ResponseModel searchLanguagesList(MethodParam methodParam, LangSearchParam lgParam) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(multipleLanguagesService.getLanguagesListForPage(methodParam, lgParam));
    }

    @ApiOperation(value = "获取企业多语言列表（含手机端）", notes = ResponseConstant.RSP_DESC_GET_SELF_LANG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/getSelfLangList", method = RequestMethod.POST)
    public ResponseModel searchLangListForCompany(MethodParam methodParam, LangSearchParam lgParam) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(multipleLanguagesService.getLangListForCompanyForPage(methodParam, lgParam));
    }

    @ApiOperation(value = "新增多语言", notes = ResponseConstant.RSP_DESC_ADD_LANGUAGES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "use_type"})
    @RequestMapping(value = "/addLanguages", method = RequestMethod.POST)
    public ResponseModel addLanguages(MethodParam methodParam, LangSearchParam lgParam, @RequestParam Map<String, Object> paramMap) {
        multipleLanguagesService.newLanguage(methodParam, lgParam, paramMap, false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑多语言", notes = ResponseConstant.RSP_DESC_EDIT_LANGUAGES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectCompanyIds", "key"})
    @RequestMapping(value = "/editLanguages", method = RequestMethod.POST)
    public ResponseModel editLanguages(MethodParam methodParam, LangSearchParam lgParam, @RequestParam Map<String, Object> paramMap) {
        multipleLanguagesService.editLanguages(methodParam, lgParam, paramMap, false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "新增多语言（企业）", notes = ResponseConstant.RSP_DESC_ADD_SELF_LANGUAGES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "use_type"})
    @RequestMapping(value = "/addSelfLanguages", method = RequestMethod.POST)
    public ResponseModel addSelfLanguages(MethodParam methodParam, LangSearchParam lgParam, @RequestParam Map<String, Object> paramMap) {
        multipleLanguagesService.newLanguage(methodParam, lgParam, paramMap, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑多语言（企业）", notes = ResponseConstant.RSP_DESC_EDIT_SELF_LANGUAGES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "key"})
    @RequestMapping(value = "/editSelfLanguages", method = RequestMethod.POST)
    public ResponseModel editSelfLanguages(MethodParam methodParam, LangSearchParam lgParam, @RequestParam Map<String, Object> paramMap) {
        multipleLanguagesService.editLanguages(methodParam, lgParam, paramMap, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "发布多语言", notes = ResponseConstant.RSP_DESC_BUILD_LANG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectCompanyIds"})
    @RequestMapping(value = "/buildLang", method = RequestMethod.POST)
    public ResponseModel buildLang(MethodParam methodParam, LangSearchParam lgParam) {
        multipleLanguagesService.releaseLang(methodParam, lgParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "发布多语言（企业）", notes = ResponseConstant.RSP_DESC_BUILD_LANG_BY_COMPANY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/buildLangByCompany", method = RequestMethod.POST)
    public ResponseModel buildLangByCompany(MethodParam methodParam) {
        multipleLanguagesService.releaseLangByCompany(methodParam);
        return ResponseModel.okMsgForOperate();
    }

}
