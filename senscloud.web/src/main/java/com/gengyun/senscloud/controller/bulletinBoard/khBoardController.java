package com.gengyun.senscloud.controller.bulletinBoard;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 科华公告
 */
@RestController
@RequestMapping("/khBoard")
public class khBoardController {
//    @Autowired
//    private KhBoardService khBoardService;
//
//    @Autowired
//    AssetAnalysByMapService assetAnalysService;
//
//    /**
//     * 设备数量与类型
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/AssetTypeAndNumList")
//    public ResponseModel getCategoryAndBillCountList(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getCategoryAndBillCountList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 设备状态与类型
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/AssetStatusList")
//    public ResponseModel getStatusAndBillCountList(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getStatusAndBillCountList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 产线类型和设备状态
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/LineTypeAndNumList")
//    public ResponseModel getLineAndBillCountList(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getLineAndBillCountList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 产线状态集合
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/LineStatusList")
//    public ResponseModel getLineStatusAndBillCountList(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getLineStatusAndBillCountList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 异常产线
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-fault-list")
//    public ResponseModel findFaultList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getLineList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 查询保养集合
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-maintain-list")
//    public ResponseModel findMaintainList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getWorkSheetStatusAndBillCountList(schemaName, request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 保养完成率
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/MaintainRate")
//
//    public ResponseModel findMaintainRate(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getMaintainRate(schemaName, request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 查询维修列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-repair-list")
//    public ResponseModel findRepairList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getLineImportantList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-dutyMan-list")
//    public ResponseModel findDutyManList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getArrangementList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 故障次数
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/FailureNum")
//    public ResponseModel findFailureNum(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getFailureNum(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 故障率
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/FailureRate")
//    public ResponseModel findFailureRate(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getFailureRate(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 平均故障时间
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/FailureInterval")
//    public ResponseModel findFailureInterval(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getMtbf(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 维修时效
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/RepairInterval")
//    public ResponseModel findRepairInterval(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getMttr(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 按条件查询工作安排列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-workTask-list")
//    public ResponseModel findWorkTask(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getArrangement(schemaName, request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 产线人员列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/LineManList")
//    public ResponseModel findLineManList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getLineManNum(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 产线挡板人员列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/OnDutyManList")
//    public ResponseModel findOnDutyManList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getLineOndutyNum(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 安全日历集合
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/CalendarList")
//    public ResponseModel findCalendarList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getCalendarList(schemaName, request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 当天实例看板
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/CalendarNow")
//    public ResponseModel findCalendarNow(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getSafetyInProduction(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 查找公告列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-notice")
//    public ResponseModel findNotice(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_47";
//            return ResponseModel.ok(khBoardService.getNotice(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//    /**
//     * 查找组织的地理位置
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_facility_and_asset_for_map")
//    public ResponseModel getFacilityAndAssetAnalysForMap(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_69";
//            List<FacilitiesDataResult> dataResult = new ArrayList<>();
//            List<FacilitiesResult> lists = null;
//            lists = khBoardService.GetFacilitiesCustomResultById(schemaName);
//            if (lists != null && lists.size() > 0) {
//                for (FacilitiesResult item : lists) {
//                    FacilitiesDataResult singleRsult = new FacilitiesDataResult();
//                    //加载位置
//                    singleRsult.setFacilityResult(item);
//                    dataResult.add(singleRsult);
//                }
//            }
//            return ResponseModel.ok(dataResult);
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 异常产线
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-fault-facility")
//    public ResponseModel findFaultFacilityList(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getLineList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//    /**
//     * 按紧急度查询维修工单数量
//     *
//     * @param request
//     * @return ResponseModel
//     */
//    @RequestMapping("/find-level-repair")
//    public ResponseModel getRepairNumByLevelCondition(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getRepairNumByLevelCondition(schemaName,request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 按设备类型统计维修工单数
//     *
//     * @param request
//     * @return ResponseModel
//     */
//    @RequestMapping("/find-category-repair")
//    public ResponseModel getCategoryAndRepairCountList(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getCategoryAndRepairCountList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 按客户统计维修工单数量
//     *
//     * @param request
//     * @return ResponseModel
//     */
//    @RequestMapping("/find-repair-facility")
//    public ResponseModel getRepairNumByFacilityCondition(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getRepairNumByFacilityCondition(schemaName,request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 用户列表
//     *
//     * @param request
//     * @return ResponseModel
//     */
//    @RequestMapping("/find-user-list")
//    public ResponseModel getUserAndRepairCountList(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getUserAndRepairCountList(schemaName));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//
//    /**
//     * 获取最新上报的工单
//     *
//     * @param request
//     * @return ResponseModel
//     */
//    @RequestMapping("/find-news")
//    public ResponseModel getPendingDisposalCondition(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getPendingDisposalCondition(schemaName,request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//    /**
//     * 获取维系信息
//     *
//     * @param request
//     * @return ResponseModel
//     */
//    @RequestMapping("/find-point")
//    public ResponseModel getPoint(HttpServletRequest request) {
//
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getPoint(schemaName,request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
//    /**
//     * 查询维修集合
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-maintain-repair-list")
//    public ResponseModel findMaintainRepairList(HttpServletRequest request) {
//        try {
//            String schemaName = "sc_com_69";
//            return ResponseModel.ok(khBoardService.getWorkSheetStatusAndBillCountList(schemaName, request));
//        } catch (Exception e) {
//            return ResponseModel.error("数据获取失败");
//        }
//    }
}
