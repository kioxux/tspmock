package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomTampLateService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 备件型号
// * User: zys
// * Date: 2019/07/08
// */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bom_tamplate")
public class BomTampLateController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    BomTampLateService bomTampLateService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("add_bom_tamplate", "bomTampLateAddFlag");
//            put("edit_bom_tamplate", "bomTampLateEditFlag");
//            put("delete_bom_tamplate", "bomTampLateDeleteFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom_tamplate/index", "bom_tamplate");
//    }
//
//    /**
//     * 查询列表页列表数据（分页）
//     *
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/find_bom_tamplate_list")
//    public String queryBomTampLateList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                       @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                       @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                       @RequestParam(name = "sortName", required = false) String sortName,
//                                       @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                       HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = bomTampLateService.query(schemaName, searchKey, pageSize, pageNumber, sortName, sortOrder);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{ }";
//        }
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/add_bom_tamplate")
//    @Transactional //支持事务
//    public ResponseModel addBomTampLate(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "bom_tamplate", "add_bom_tamplate");
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return bomTampLateService.addBomTampLate(schemaName, paramMap);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH));
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 更新数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/edit_bom_tamplate")
//    @Transactional //支持事务
//    public ResponseModel editBomTampLate(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "bom_tamplate", "edit_bom_tamplate");
//            if (permissionFlag) {
//                return bomTampLateService.editBomTampLate(schemaName, paramMap);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH));
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/delete_bom_tamplate")
//    @Transactional //支持事务
//    public ResponseModel deleteBomTampLate(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "bom_tamplate", "delete_bom_tamplate");
//            String id = request.getParameter("id");
//            if (permissionFlag && null != id && !"".equals(id)) {
//                return bomTampLateService.deleteBomTampLate(schemaName, Integer.parseInt(id));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH));
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 文档列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/find_doc_list")
//    @Transactional //支持事务
//    public String findDocList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String model_id = request.getParameter("model_id");
//            boolean permissionFlag = true;
//            if (permissionFlag && null != model_id && !"".equals(model_id)) {
//                return bomTampLateService.query_doc(schemaName, model_id).toString();
//            } else {
//                return "{}";
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增文档
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/add_doc")
//    @Transactional //支持事务
//    public ResponseModel addDoc(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            String model_id = paramMap.get("model_id").toString();
//
//            if (RegexUtil.isNull(model_id) || "".equals(model_id)) {
//                paramMap.put("model_id", 0);
//            }
//            paramMap.put("create_user_account", loginUser.getAccount());
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return bomTampLateService.insert_modal_doc(schemaName, paramMap);
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH));
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 删除文档
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_tamplate")
//    @RequestMapping("/delete_doc")
//    @Transactional //支持事务
//    public ResponseModel deleteDoc(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String file_id = request.getParameter("file_id");
//            file_id = file_id.replace(",", "").trim();
//            boolean permissionFlag = true;
//            if (permissionFlag && !RegexUtil.isNull(file_id)) {
//                return bomTampLateService.delete_model_doc(schemaName, Integer.parseInt(file_id));
//            } else {
//                return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH));
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
}
