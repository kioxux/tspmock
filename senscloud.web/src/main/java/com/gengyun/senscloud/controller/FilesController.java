package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.common.ResponseConstant;
//import com.gengyun.senscloud.model.MethodParam;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.ExcelImportService;
//import com.gengyun.senscloud.service.FilesService;
//import io.swagger.annotations.*;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//import springfox.documentation.annotations.ApiIgnore;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.Map;
//

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ErrorConstant;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.FileModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.FilesService;
import com.gengyun.senscloud.util.RegexUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "文件处理")
@RestController
public class FilesController {
    @Resource
    private FilesService filesService;

    @ApiOperation(value = "导出单个文件", notes = ResponseConstant.RSP_DESC_EXPORT_FILE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/exportFile", method = RequestMethod.GET)
    public void exportFile(MethodParam methodParam, FileModel fileModel) {
        filesService.doDownLoad(methodParam, fileModel);
    }

    @ApiOperation(value = "获取图片", notes = ResponseConstant.RSP_DESC_GET_IMG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "is_logo"})
    @RequestMapping(value = "/getImg", method = RequestMethod.GET)
    public void getImg(MethodParam methodParam, FileModel fileModel) {
        filesService.getImg(methodParam, fileModel);
    }

    @ApiOperation(value = "获取登录页图片（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_LOGIN_IMAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"id"})
    @RequestMapping(value = "/searchLoginImage", method = RequestMethod.GET)
    public void queryLoginImage(MethodParam methodParam, FileModel fileModel) {
        filesService.getLoginImage(methodParam, fileModel);
    }

    @ApiOperation(value = "文件上传", notes = ResponseConstant.RSP_DESC_ADD_FILE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "files", "business_no", "data_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addFile", method = RequestMethod.POST)
    public ResponseModel addFile(MethodParam methodParam, FileModel fileModel) {
        String rs = filesService.checkAddFile(methodParam, fileModel);
        if ("ok".equals(rs)) {
            return ResponseModel.okMsgForOperate();
        } else if ("".equals(rs)) {
            return ResponseModel.ok(null);
        } else {
            return ResponseModel.ok(Integer.valueOf(rs));
        }
    }

    @ApiOperation(value = "获取文件信息（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_LOGIN_IMAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchFileInfo", method = RequestMethod.GET)
    public ResponseModel queryFileInfo(MethodParam methodParam, FileModel fileModel) {
        return ResponseModel.ok(filesService.findById(methodParam.getSchemaName(), RegexUtil.optIntegerOrErr(fileModel.getId(), methodParam, ErrorConstant.EC_FILE_005)));
    }
}