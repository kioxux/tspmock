package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.Company;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
//@RestController
public class AssetCustomerController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    AssetCustomerService assetCustomerService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    AssetDataServiceV2 assetService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    /**
//     * 初始化设备关联厂商
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_customer_init")
//    public ModelAndView InitAssetCustomerManage(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            result.setContent(null);
//            result.setCode(1);
//            result.setMsg("");
//
//            List<Facility> suppliesList = facilitiesService.FacilitiesSuppliesListInUse(schema_name);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(suppliesList);
//            return new ModelAndView("AssetCustomerManage")
//                    .addObject("result", result)
//                    .addObject("suppliesList", suppliesList);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_AS_SU);
//            result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("AssetCustomerManage").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得所有的设备关联厂商
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_customer_list")
//    public ModelAndView FindALLAssetCustomerList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String asset_id = request.getParameter("asset_id");
//            List<Facility> dataList = assetCustomerService.getAssetFacilitiesSupplierList(schema_name, asset_id);
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//
//            return new ModelAndView("AssetCustomerList")
//                    .addObject("result", result);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_AS_SU);
//result.setMsg(tmpMsg + ex.getMessage());
//            return new ModelAndView("AssetCustomerList").addObject("result", result);
//        }
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_customer_save")
//    public ResponseModel getAddAssetCustomer(HttpServletRequest request, HttpServletResponse response) {
//        //保存关联厂商
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        String asset_id = request.getParameter("asset_id");
//        Integer org_id = Integer.parseInt(request.getParameter("org_id"));
//        if (asset_id == null || asset_id.isEmpty() || org_id <= 0) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_CH_SUPP);
//            result.setMsg(tmpMsg);
//        } else {
//            User user = authService.getLoginUser(request);
//
//            String checkResult = assetService.checkAssetList(request, user, schema_name, asset_id);
//            if (RegexUtil.isNull(checkResult)) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//			result.setMsg(tmpMsg);
//                return result;
//            }
//
//            //看看是否有重复，如果有重复，则执行更新操作
//            List<Facility> list = assetCustomerService.findAssetSupplier(schema_name, asset_id, org_id);
//            //保存设备关联厂商
//            int count = 0;
//            if (list == null || list.isEmpty()) {
//                count = assetCustomerService.AddAssetSupplier(schema_name, asset_id, org_id);
//            }
//            if (count > 0) {
//                //增加日志
//                Facility customer = facilitiesService.FacilitiesById(schema_name, org_id);
//
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_S);
//                logService.AddLog(schema_name, "asset", asset_id.toString(), tmpMsg + customer.getTitle(), user.getAccount());
//                result.setCode(1);
//                String tmpMsg2 = selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_S_OK);
//                result.setMsg(tmpMsg2);
//            } else {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_S_DE);
//                result.setMsg(tmpMsg);
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_customer_delete")
//    public ResponseModel getDeleteAssetCustomer(HttpServletRequest request, HttpServletResponse response) {
//        //删除设备关联厂商
//        ResponseModel result = new ResponseModel();
//        Integer org_id = Integer.parseInt(request.getParameter("id"));
//        String asset_id = request.getParameter("asset_id");
//        User user = authService.getLoginUser(request);
//        if (org_id <= 0) {
//            result.setCode(-1);
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_UN_S);
//            result.setMsg(tmpMsg);
//        } else {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//
//
//            //删除资产关联厂商
//            Facility data = facilitiesService.FacilitiesById(schema_name, org_id);
//            if (null == data || RegexUtil.isNull(data.getTitle())) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_S);
//                result.setMsg(tmpMsg);
//                return result;
//            }
//            String checkResult = assetService.checkAssetList(request, user, schema_name, asset_id);
//            if (RegexUtil.isNull(checkResult)) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_INS_AUTH);
//			result.setMsg(tmpMsg);
//                return result;
//            }
//
//            int count = assetCustomerService.DeleteAssetSupplier(schema_name, org_id, asset_id);
//            if (count > 0) {
//                //增加日志
//                logService.AddLog(schema_name, "asset", asset_id, selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_S_OR)+ data.getTitle(), user.getAccount());
//                result.setCode(1);
//                String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_S_SU);
//                result.setMsg(tmpMsg);
//            } else {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_S_DE);
//                result.setMsg(tmpMsg);
//            }
//        }
//        //返回保存结果
//        return result;
//    }
}
