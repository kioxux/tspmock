package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomAllotService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 备件调拨
// */
//@RestController
//@RequestMapping("/bom_allot")
public class BomAllotController {
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    private BomAllotService bomAllotService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_allot")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            Map<String, Object> workType = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_32);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("bom_allot_add", "bomAllotAddFlag");
//                put("bom_allot_invalid", "rpDeleteFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_allot/index", "bom_allot")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_allot/index");
//        }
//    }
//
//    /**
//     * 获取备件调拨列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_allot")
//    @RequestMapping("/findBomAllotList")
//    public String findBomInStockList(HttpServletRequest request) {
//        try {
//            return bomAllotService.findBomAllotList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/findSingleBisInfo")
//    public ResponseModel findSingleBisInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = bomAllotService.queryBomAllotById(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/detail-bomAllot")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return bomAllotService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//
//    /**
//     * 提交数据
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_allot_add")
//    @RequestMapping("/doSubmitBomAllot")
//    @Transactional //支持事务
//    public ResponseModel saveRepairProblem(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return bomAllotService.save(schema_name, processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * @Description: 备件调拨出库确认
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("/bomAllotFromConfirm")
//    @Transactional
//    public ResponseModel bomAllotConfirm(HttpServletRequest request, @RequestParam Map<String, Object> paramMap){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String node = "fromConfirm";
//        try{
//            return bomAllotService.bomAllotAudit(schemaName, request, paramMap,loginUser,node);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//    /**
//     * @Description: 备件调拨入库确认
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("/bomAllotToConfirm")
//    @Transactional
//    public ResponseModel bomAllotToConfirm(HttpServletRequest request, @RequestParam Map<String, Object> paramMap){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String node = "toConfirm";
//        try{
//            return bomAllotService.bomAllotAudit(schemaName, request, paramMap,loginUser,node);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @Description: 调出备件
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("/bomAllotFromAudit")
//    @Transactional
//    public ResponseModel bomAllotFromAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String node = "fromAudit";
//        try{
//            return bomAllotService.bomAllotAudit(schemaName, request, paramMap,loginUser,node);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//    /**
//     * @Description: 调入备件
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @RequestMapping("/bomAllotToAudit")
//    @Transactional
//    public ResponseModel bomAllotToAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        String node = "toAudit";
//        try{
//            return bomAllotService.bomAllotAudit(schemaName, request, paramMap,loginUser,node);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @Description:报废入库调拨
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("bom_allot_invalid")
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try{
//            String transfer_code = request.getParameter("transfer_code");
//            if(StringUtils.isBlank(transfer_code)){
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.errorMsg(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//            }
//            return bomAllotService.cancel(schemaName, loginUser, transfer_code);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }

}
