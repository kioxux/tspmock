package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.AssetTransferAuditNode;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.mapper.AssetMapper;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @Author: Wudang Dong
// * @Description:设备调拨
// * @Date: Create in 上午 10:17 2019/4/30 0030
// */
//
//@RestController
//@RequestMapping("/assetTransfer")
public class AssetTransferController {
//
//    @Autowired
//    AssetTransferService assetTransferService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetStatusService assetStatusService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    @Autowired
//    AssetMapper assetMapper;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    DynamicCommonService dynamicCommonService;
//
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @MenuPermission("asset_transfer")
//    @RequestMapping("/index")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        Map<String, Object> workType = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            workType = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_22);
//
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("asset_transfer_invalid", "rpDeleteFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "asset_transfer/index", "asset_transfer")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("asset_transfer/index");
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:申请设备调拨
//     */
//    @MenuPermission("asset_transfer")
//    @RequestMapping("/add")
//    @Transactional
//    public ResponseModel add(@RequestParam(name = "flow_id") String processDefinitionId, HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            return assetTransferService.add(schemaName, request, loginUser, processDefinitionId, paramMap);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_TRAN_ERR));
//        }
//    }
//
//    /**
//     * 根据code查询盘点位置详情数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/findSingleAtInfo")
//    public ResponseModel findSingleAtInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = assetTransferService.queryAssetTransferByCode(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 盘点分配页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_transfer")
//    @RequestMapping("/detail-assetTransfer")
//    public ModelAndView bomInventoryStockDetail(HttpServletRequest request) {
//        try {
//            return assetTransferService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description：查看列表
//     */
//    @MenuPermission("asset_transfer")
//    @RequestMapping("/list")
//    public String selectAllTransfer(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String condition = "";
//            //设备类型
//            String status = request.getParameter("status");//申请调度单据状态
//            String facilities = request.getParameter("facilities");
//            String pageSize = request.getParameter("pageSize");
//            String pageNumber = request.getParameter("pageNumber");
//            if (!RegexUtil.isNull(status) && !status.equals("-1")) {
//                condition += " and t.status='" + status + "'";
//            }
//            //获取组织的根节点数据，用于like进行比较
//            String[] facilityNos = dataPermissionForFacility.findFacilityNOsByBusinessForSingalUser(schemaName, loginUser, facilities.split(","), "asset_transfer");
//            if (facilityNos != null && facilityNos.length > 0) {
//                condition += " and (";
//                for (String facilityNo : facilityNos) {
//                    condition += " (p1.position_code like '" + facilityNo + "%' or p2.position_code like '" + facilityNo + "%' " +
//                            "or f1.facility_no like '" + facilityNo + "%' or f2.facility_no like '" + facilityNo + "%' " +
//                            "or apo1.position_code like '" + facilityNo + "%' or apo2.position_code like '" + facilityNo + "%' ) or";
//                }
//                condition = condition.substring(0, condition.length() - 2);
//                condition += ") ";
//            }
//            if (null != pageSize && null != pageNumber) {
//                PageHelper.startPage(Integer.parseInt(pageNumber), Integer.parseInt(pageSize));
//            }
//            List<Map<String, Object>> assetTransferModels = assetTransferService.selectAllDiscard(schemaName, condition, loginUser);
//            int num = assetTransferService.selectAllDiscardNum(schemaName, condition);
//            //多时区处理
//            SCTimeZoneUtil.responseMapListDataHandler(assetTransferModels);
//            result.put("rows", assetTransferModels);
//            result.put("total", num);
//            return result.toString();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return "{}";
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备调度-调出审核
//     */
//    @RequestMapping("/transferOutAudit")
//    @Transactional
//    public ResponseModel transferOutAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetTransferService.transferAudit(schemaName, request, paramMap, loginUser, AssetTransferAuditNode.TRANSFER_OUT_AUDIT);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备调度-接收审核
//     */
//    @RequestMapping("/transferInAudit")
//    @Transactional
//    public ResponseModel transferInAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetTransferService.transferAudit(schemaName, request, paramMap, loginUser, AssetTransferAuditNode.TRANSFER_IN_AUDIT);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备调度-调出设备
//     */
//    @RequestMapping("/transferOutAsset")
//    @Transactional
//    public ResponseModel transferOutAsset(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetTransferService.transferAudit(schemaName, request, paramMap, loginUser, AssetTransferAuditNode.TRANSFER_OUT_ASSET);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:设备调度-接收设备
//     */
//    @RequestMapping("/transferInAsset")
//    @Transactional
//    public ResponseModel transferInAsset(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return assetTransferService.transferAudit(schemaName, request, paramMap, loginUser, AssetTransferAuditNode.TRANSFER_IN_ASSET);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description:报废调度申请
//     */
//    @MenuPermission("asset_transfer")
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String transfer_code = request.getParameter("transfer_code");
//            if (StringUtils.isBlank(transfer_code)) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.errorMsg(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//            }
//            return assetTransferService.cancel(schemaName, loginUser, transfer_code);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    @RequestMapping("/asset")
//    public ResponseModel asset(HttpServletResponse response, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String assetId = request.getParameter("assetId");
//            Map<String, Object> asset = assetMapper.getAssetDetailById(schemaName, assetId);
//            if (asset != null) {
//                return ResponseModel.ok(asset);
//            } else {
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NULL_ASS_DETAIL));
//            }
//        } catch (Exception e) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * 查询所属位置的组织编码，如果是“P”开头，则直接返回
//     *
//     * @param response
//     * @param request
//     * @return
//     */
//    @RequestMapping("/get_facility_by_id")
//    @ResponseBody
//    public ResponseModel getFacilityById(HttpServletResponse response, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            if (!facilityId.startsWith("P")) {
//                Facility facility = facilitiesService.FacilitiesById(schemaName, Integer.valueOf(facilityId));
//                if (facility != null) {
//                    return ResponseModel.ok(facility.getFacilityNo());
//                } else {
//                    return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NULL_ASS_DETAIL));
//                }
//            } else {
//                return ResponseModel.ok(facilityId);
//            }
//        } catch (Exception e) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }

}
