package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.bom.BomService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by Administrator on 2018/4/27.
 */
@Api(tags = "备件管理")
@RestController
public class BomController {
    @Resource
    BomService bomService;
    @Resource
    LogsService logService;

    @ApiOperation(value = "获取备件模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_BOM_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchBomListPermission", method = RequestMethod.POST)
    public ResponseModel searchBomListPermission(MethodParam methodParam) {
        return ResponseModel.ok(bomService.getBomListPermission(methodParam));
    }

    @ApiOperation(value = "获取备件列表（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_BOM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber",
            "keywordSearch", "typeIdsSearch", "suppliersSearch", "isUseSearch", "startCountSearch", "endCountSearch"})
    @RequestMapping(value = "/searchBomList", method = RequestMethod.POST)
    public ResponseModel searchBomList(MethodParam methodParam, BomSearchParam bParam) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(bomService.getBomListForPage(methodParam, bParam));
    }

    @ApiOperation(value = "新增备件", notes = ResponseConstant.RSP_DESC_ADD_BOM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,includeParameters = {"token", "code_classification", "bom_name", "material_code", "bom_model", "type_id", "unit_id", "show_price", "supplier_id", "supply_period", "manufacturer_id", "service_life", "remark","currency_id"})//,"data_order"
    @Transactional //支持事务
    @RequestMapping(value = "/addBom", method = RequestMethod.POST)
    public ResponseModel addBom(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,@SuppressWarnings("unused") BomAdd bomAdd) {
        bomService.newBom(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取备件明细信息", notes = ResponseConstant.RSP_DESC_GET_BOM_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchBomInfo", method = RequestMethod.POST)
    public ResponseModel searchBomInfo(MethodParam methodParam, BomSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(bomService.getBomDetailById(methodParam, bParam));
    }

    @ApiOperation(value = "获取备件库存信息", notes = ResponseConstant.RSP_DESC_GET_BOM_STOCK_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id","pageSize", "pageNumber"})
    @RequestMapping(value = "/searchBomStockInfo", method = RequestMethod.POST)
    public ResponseModel searchBomStockInfo(MethodParam methodParam, BomSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(bomService.getBomStockByBomId(methodParam, bParam));
    }

    @ApiOperation(value = "获取备件出入库信息", notes = ResponseConstant.RSP_DESC_GET_BOM_IN_AND_OUT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id","pageSize", "pageNumber"})
    @RequestMapping(value = "/searchBomInAndOutList", method = RequestMethod.POST)
    public ResponseModel searchBomInAndOutList(MethodParam methodParam, BomSearchParam param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(bomService.getBomInAndOutList(methodParam, param));
    }

    @ApiOperation(value = "获取备件供应商信息", notes = ResponseConstant.RSP_DESC_GET_BOM_SUPPLIER_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id","keywordSearch","is_use","pageSize", "pageNumber"})
    @RequestMapping(value = "/searchBomSupplierInfo", method = RequestMethod.POST)
    public ResponseModel searchBomSupplierInfo(MethodParam methodParam, BomSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(bomService.getBomSupplierInfoByBomId(methodParam, bParam));
    }

    @ApiOperation(value = "获得备件系统日志", notes = ResponseConstant.RSP_DESC_SEARCH_BOM_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchBomLogList", method = RequestMethod.POST)
    public ResponseModel searchBomLogList(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(logService.getLog(methodParam, SensConstant.BUSINESS_NO_3000, id));
    }

    @ApiOperation(value = "新增备件供应商", notes = ResponseConstant.RSP_DESC_ADD_BOM_SUPPLIER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"bomAdd"},includeParameters = {"token", "bom_id", "supplier_id","supply_period", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/addBomSupplier", method = RequestMethod.POST)
    public ResponseModel addBomSupplier(MethodParam methodParam, @RequestParam Map<String, Object> paramMap,@SuppressWarnings("unused") BomAdd bomAdd) {
        bomService.newBomSupplier(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑备件供应商", notes = ResponseConstant.RSP_DESC_EDIT_BOM_SUPPLIER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, ignoreParameters = {"supplier"}, includeParameters = {"token", "id", "supplier_id","supply_period", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/editBomSupplier", method = RequestMethod.POST)
    public ResponseModel editBomSupplier(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") BomSupplierInfo supplier, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        bomService.modifyBomSupplier(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除备件供应商", notes = ResponseConstant.RSP_DESC_DELETE_BOM_SUPPLIER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeBomSupplier", method = RequestMethod.POST)
    public ResponseModel removeBomSupplier(MethodParam methodParam,BomSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        bomService.cutBomSupplier(methodParam,bParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "启用禁用备件供应商", notes = ResponseConstant.RSP_DESC_CHANGE_USE_BOM_SUPPLIER)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/editUseBomSupplier", method = RequestMethod.POST)
    public ResponseModel editUseBomSupplier(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") BomSupplierInfo supplierInfo) {
        bomService.modifyUseBomSupplier(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "展示选择备件物料编码二维码图片", notes = ResponseConstant.RSP_DESC_SHOW_BOM_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchBomMaterialQRCode", method = RequestMethod.GET)
    public void searchBomMaterialQRCode(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        bomService.getBomMaterialQRCode(methodParam,id);
    }

    @ApiOperation(value = "编辑备件", notes = ResponseConstant.RSP_DESC_EDIT_BOM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"bomInfo"},includeParameters = {"token","id", "code_classification", "bom_name", "material_code", "bom_model", "type_id", "unit_id", "show_price", "manufacturer_id", "service_life", "remark","currency_id","sectionType","icon"})
    @Transactional //支持事务
    @RequestMapping(value = "/editBom", method = RequestMethod.POST)
    public ResponseModel editBom(MethodParam methodParam, BomSearchParam bParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") BomInfo bomInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        bomService.modifyBom(methodParam, bParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "启用禁用备件", notes = ResponseConstant.RSP_DESC_CHANGE_USE_BOM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"bomInfo"}, includeParameters = {"token", "id", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/editUseBom", method = RequestMethod.POST)
    public ResponseModel editUseBom(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") BomInfo bomInfo) {
        bomService.modifyUseBom(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除选中备件", notes = ResponseConstant.RSP_DESC_REMOVE_BOM_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/removeSelectBom", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectBom(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        bomService.cutBomByIds(methodParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取备件明细清单信息", notes = ResponseConstant.RSP_DESC_GET_BOM_SUPPLIER_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id","keywordSearch","pageSize", "pageNumber"})
    @RequestMapping(value = "/searchBomDetailedList", method = RequestMethod.POST)
    public ResponseModel searchBomDetailedList(MethodParam methodParam, BomSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(bomService.getBomDetailedList(methodParam, bParam));
    }

    @ApiOperation(value = "展示选择备件二维码图片", notes = ResponseConstant.RSP_DESC_SHOW_BOM_NEW_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/showBomQRCode", method = RequestMethod.GET)
    public void showBomQRCode(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        bomService.doShowBomQRCode(methodParam);
    }

    @ApiOperation(value = "导出选中备件二维码图片", notes = ResponseConstant.RSP_DESC_EXPORT_BOM_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/exportBomQRCode", method = RequestMethod.POST)
    public void exportBomQRCode(MethodParam methodParam, BomSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        bomService.doExportBomQRCode(methodParam, bParam);
    }

    @ApiOperation(value = "导出全部备件二维码图片", notes = ResponseConstant.RSP_DESC_EXPORT_ALL_BOM_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "statusSearch", "keywordSearch",
            "positionsSearch", "assetTypesSearch", "assetModelSearch", "runStatusSearch", "customerSearch", "levelSearch",
            "startDateSearch", "endDateSearch", "warrantiesTypeSearch", "facilitiesSearch", "orgTypeSearch", "duplicateAssetSearch"})
    @RequestMapping(value = "/exportAllBomQRCode", method = RequestMethod.POST)
    public void exportAllBomQRCode(MethodParam methodParam, BomSearchParam bParam) {
        bomService.doExportBomQRCode(methodParam, bParam);
    }
}