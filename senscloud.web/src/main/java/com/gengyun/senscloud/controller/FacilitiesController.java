package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.FacilitiesModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.FacilitiesService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "厂商管理")
@RestController
public class FacilitiesController {
    @Resource
    FacilitiesService facilitiesService;

    @ApiOperation(value = "获取设备模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_FACILITIES_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchFacilitiesPermission", method = RequestMethod.POST)
    public ResponseModel searchFacilitiesPermission(MethodParam methodParam) {
        return ResponseModel.ok(facilitiesService.getFacilitiesPermission(methodParam));
    }

    @ApiOperation(value = "新增厂商", notes = ResponseConstant.RSP_DESC_ADD_FACILITIES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "title", "address", "parent_id", "remark", "location",
                    "inner_code", "layer_path", "org_type_id", "short_title", "currency_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addFacilities", method = RequestMethod.POST)
    public ResponseModel addFacilities(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.newFacilities(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新厂商", notes = ResponseConstant.RSP_DESC_EDIT_FACILITIES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "title", "id", "address", "parent_id", "remark", "location",
                    "inner_code", "layer_path", "org_type_id", "short_title", "currency_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/editFacilities", method = RequestMethod.POST)
    public ResponseModel editFacilities(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.modifyFacilities(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "启用禁用厂商", notes = ResponseConstant.RSP_DESC_CHANGE_USE_FACILITIES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "is_use"})
    @Transactional //支持事务
    @RequestMapping(value = "/changeUseFacilities", method = RequestMethod.POST)
    public ResponseModel changeUseFacilities(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.changeUseFacilities(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除厂商", notes = ResponseConstant.RSP_DESC_REMOVE_FACILITIES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeFacilities", method = RequestMethod.POST)
    public ResponseModel removeFacilities(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        facilitiesService.cutFacilities(methodParam, facilitiesModel);
        return ResponseModel.okMsgForOperate();
    }
    @ApiOperation(value = "批量删除厂商", notes = ResponseConstant.RSP_DESC_REMOVE_FACILITIES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeFacilitiesList", method = RequestMethod.POST)
    public ResponseModel removeFacilitiesList(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        facilitiesService.cutFacilitiesList(methodParam, facilitiesModel);
        return ResponseModel.okMsgForOperate();
    }
    @ApiOperation(value = "查询厂商详情", notes = ResponseConstant.RSP_DESC_SEARCH_FACILITIES_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchFacilitiesInfo", method = RequestMethod.POST)
    public ResponseModel searchFacilitiesInfo(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        return ResponseModel.ok(facilitiesService.findById(methodParam, facilitiesModel));
    }

    @ApiOperation(value = "查询厂商列表", notes = ResponseConstant.RSP_DESC_SEARCH_FACILITIES_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "isUseSearch", "orgTypeSearch"})
    @RequestMapping(value = "/searchFacilitiesList", method = RequestMethod.POST)
    public ResponseModel searchFacilitiesList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        return ResponseModel.ok(facilitiesService.findFacilitiesList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增厂商附件", notes = ResponseConstant.RSP_DESC_ADD_FACILITIES_DOC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "file_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addFacilitiesDoc", method = RequestMethod.POST)
    public ResponseModel addFacilitiesDoc(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.newOrganizationDoc(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除厂商附件", notes = ResponseConstant.RSP_DESC_REMOVE_FACILITIES_DOC)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "file_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeFacilitiesDoc", method = RequestMethod.POST)
    public ResponseModel removeFacilitiesDoc(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.cutOrganizationDoc(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "厂商附件列表", notes = ResponseConstant.RSP_DESC_SEARCH_FACILITIES_DOC_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "keywordSearch"})
    @RequestMapping(value = "/searchFacilitieDocsList", method = RequestMethod.POST)
    public ResponseModel searchFacilitieDocsList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        return ResponseModel.ok(facilitiesService.getOrganizationDocList(methodParam, paramMap));
    }

    @ApiOperation(value = "新增厂商联系人", notes = ResponseConstant.RSP_DESC_ADD_FACILITIES_CONTACT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "contact_name", "contact_mobile", "contact_email", "wei_xin"})
    @Transactional //支持事务
    @RequestMapping(value = "/addFacilitiesContact", method = RequestMethod.POST)
    public ResponseModel addFacilitiesContact(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.newOrganizationContact(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑厂商联系人", notes = ResponseConstant.RSP_DESC_EDIT_FACILITIES_CONTACT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "contact_name", "contact_mobile", "contact_email", "wei_xin"})
    @Transactional //支持事务
    @RequestMapping(value = "/editFacilitiesContact", method = RequestMethod.POST)
    public ResponseModel editFacilitiesContact(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        facilitiesService.modifyOrganizationContact(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除厂商联系人", notes = ResponseConstant.RSP_DESC_REMOVE_FACILITIES_CONTACT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeFacilitiesContact", method = RequestMethod.POST)
    public ResponseModel removeFacilitiesContact(MethodParam methodParam, FacilitiesModel facilitiesModel) {
        facilitiesService.cutOrganizationContact(methodParam, facilitiesModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "厂商联系人列表", notes = ResponseConstant.RSP_DESC_SEARCH_FACILITIES_CONTACT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "org_id", "keywordSearch"})
    @RequestMapping(value = "/searchFacilitiesContactList", method = RequestMethod.POST)
    public ResponseModel searchFacilitiesContactList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") FacilitiesModel facilitiesModel) {
        return ResponseModel.ok(facilitiesService.getOrganizationContactList(methodParam, paramMap));
    }

}
