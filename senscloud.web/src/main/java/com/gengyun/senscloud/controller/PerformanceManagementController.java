package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.PerformanceManagementService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 个人绩效管理
// */
//@RestController
//@RequestMapping("/performance_management")
public class PerformanceManagementController {

//    @Autowired
//    private PerformanceManagementService performanceManagementService;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    UserService userService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_index")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("performance_management_score", "pmAddFlag");//评分权限
//            }};
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "performance_management/index", "performance_management_index");
//            return model;
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/index");
//        }
//    }
//
//    /**
//     * 查询个人绩效列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_index")
//    @RequestMapping("/findPerformanceList")
//    public String findPerformanceList(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询个人绩效列表-获取考核项字段（bootstraptable动态列columns）
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_index")
//    @RequestMapping("/findPerformanceOptionList")
//    public ResponseModel findPerformanceOptionList(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceOptionList(request);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 评分页面-通过ID，查询个人绩效列表-获取考核项字段（bootstraptable动态列columns）
//     * 注意：这里的分数是没有经过公式折算的原始评分分数
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_index")
//    @RequestMapping("/findPerformanceOptionListById")
//    public ResponseModel findPerformanceOptionListById(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceOptionListById(request);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 个人业绩详情
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_index")
//    @RequestMapping("/detail")
//    public ModelAndView detail(HttpServletRequest request) {
//        try {
//            ModelAndView model = new ModelAndView("performance_management/detail");
//            Map<String, Object> performance = performanceManagementService.findPerformanceDetail(request);
//            return model.addObject("performance", performance);
//        } catch (Exception ex) {
//            return new ModelAndView("performance_management/detail");
//        }
//    }
//
//    /**
//     * 查询个人评分项列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_index")
//    @RequestMapping("findPerformanceItemsById")
//    public String findPerformanceItemsById(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceItemsById(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 跳转到评分页面
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/toScore")
//    public ModelAndView toScore(HttpServletRequest request) {
//        try {
//            String ids = request.getParameter("ids");
//            ModelAndView model = new ModelAndView("performance_management/score");
//            return model.addObject("ids", ids);
//        } catch (Exception ex) {
//            return new ModelAndView("performance_management/score");
//        }
//    }
//
//    /**
//     * 进入绩效模板配置页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_template")
//    @RequestMapping({"/template"})
//    public ModelAndView template(HttpServletRequest request) {
//        try {
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, null,
//                    "performance_management/template_list", "performance_management_template");
//            return model;
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/template_list");
//        }
//    }
//
//    /**
//     * 查询绩效模板列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_template")
//    @RequestMapping("/findPerformanceTemplateList")
//    public String findPerformanceTemplateList(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceTemplateList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 绩效模板启用、禁用
//     *
//     * @param request
//     * @return
//     */
//    @Transactional
//    @MenuPermission("performance_management_template")
//    @RequestMapping("/templateEnableControl")
//    public ResponseModel templateEnableControl(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            String status = request.getParameter("status");
//            if (StringUtils.isBlank(id) || StringUtils.isBlank(status))
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//            return performanceManagementService.enableOrDisablePerformanceTemplateById(schemaName, Long.parseLong(id), status);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//    /**
//     * 跳转到绩效模板新增页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_template")
//    @RequestMapping({"/toAddPerformanceTemplate"})
//    public ModelAndView toAddPerformanceTemplate(HttpServletRequest request) {
//        try {
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, null,
//                    "performance_management/template_add", "performance_management_template");
//            return model;
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/template_add");
//        }
//    }
//
//    /**
//     * 新增、编辑绩效考核模板
//     *
//     * @param request
//     * @return
//     */
//    @Transactional
//    @MenuPermission("performance_management_template")
//    @RequestMapping({"/doAddPerformanceTemplate"})
//    public ResponseModel doAddPerformanceTemplate(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return performanceManagementService.doAddPerformanceTemplate(schemaName, request);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//    /**
//     * 跳转到模板详情页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_template")
//    @RequestMapping({"/toTemplateDetail"})
//    public ModelAndView toTemplateDetail(HttpServletRequest request) {
//        try {
//            String id = request.getParameter("id");
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, null, "performance_management/template_add",
//                    "performance_management_template").addObject("id", id);
//            return model;
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/template_add");
//        }
//    }
//
//    /**
//     * 查询绩效模板详情
//     *
//     * @param request
//     * @return
//     */
//    @Transactional
//    @MenuPermission("performance_management_template")
//    @RequestMapping("/getTemplateDetailById")
//    public ResponseModel getTemplateDetailById(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            if (StringUtils.isBlank(id))
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//            return performanceManagementService.getTemplateDetailById(schemaName, Long.parseLong(id));
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//    /**
//     * 绩效评分
//     *
//     * @param request
//     * @return
//     */
//    @Transactional
//    @MenuPermission("performance_management_template")
//    @RequestMapping("/doAddPerformanceScore")
//    public ResponseModel doAddPerformanceScore(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return performanceManagementService.doAddPerformanceScore(schemaName, request);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//    /**
//     * 人员考核计划进入页
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_plan")
//    @RequestMapping({"/plan"})
//    public ModelAndView plan(HttpServletRequest request) {
//        try {
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, null, "performance_management/plan", "performance_management_plan");
//            return model;
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/plan");
//        }
//    }
//
//    /**
//     * 跳转到绩效模板新增页面
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_plan")
//    @RequestMapping({"/toAddPerformancePlan"})
//    public ModelAndView toAddPerformancePlan(HttpServletRequest request) {
//        try {
//            ModelAndView model = pagePermissionService.getUserFunctionPermissionForPage(request, null, "performance_management/plan_add", "performance_management_plan");
//            return model;
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/plan_add");
//        }
//    }
//
//    /**
//     * 查询考核计划列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_management_plan")
//    @RequestMapping("/findPerformancePlanList")
//    public String findPerformancePlanList(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformancePlanList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 考核计划启用、禁用
//     *
//     * @param request
//     * @return
//     */
//    @Transactional
//    @MenuPermission("performance_management_plan")
//    @RequestMapping("/plan_enable_control")
//    public ResponseModel planEnableControl(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String id = request.getParameter("id");
//            String status = request.getParameter("status");
//            if (StringUtils.isBlank(id) || StringUtils.isBlank(status))
//                return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_PARAM_ERROR));//参数获取失败
//
//            return performanceManagementService.enableOrDisablePerformancePlanById(schemaName, id, status);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//    @MenuPermission("performance_management_plan")
//    @RequestMapping("/findSinglePpInfo")
//    public ResponseModel queryArrangementList(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = performanceManagementService.queryPlanById(schemaName, subWorkCode);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @MenuPermission("performance_management_template")
//    @RequestMapping("/doSubmitPerformancePlan")
//    @Transactional //支持事务
//    public ResponseModel doSubmitTaskRelease(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return performanceManagementService.save(schema_name, processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_62));
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
//
//
//    @RequestMapping("/testFunction")
//    @Transactional
//    public void test(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            performanceManagementService.performancePlanDeal(schema_name);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//        }
//
//    }
//
//    /**
//     * 使用公式计算总分
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/equationExecute")
//    public ResponseModel equationExecute(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return performanceManagementService.equationExecute(schemaName, request);
//        } catch (Exception e) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//
//    /**
//     * 绩效报告
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_report")
//    @RequestMapping({"/performance_report"})
//    public ModelAndView performanceReport(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "performance_report");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("performance_report_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("performance_report_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("performance_management/performance_report")
//                    .addObject("canSeeAllFacility", canSeeAllFacility)
//                    .addObject("canExportData", canExportData);
//        } catch (Exception e) {
//            return new ModelAndView("performance_management/performance_report");
//        }
//    }
//
//    /**
//     * 查询绩效报告列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_report")
//    @RequestMapping("/findPerformanceReportList")
//    public String findPerformanceReportList(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceReportList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询绩效报告用户列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_report")
//    @RequestMapping("/findPerformanceReportUserList")
//    public String findPerformanceReportUserList(HttpServletRequest request) {
//        try {
//            return performanceManagementService.findPerformanceReportUserList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 绩效报告导出
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("performance_report")
//    @RequestMapping("/performanceReportExport")
//    public ModelAndView performanceReportExport(HttpServletRequest request) {
//        try {
//            return performanceManagementService.performanceReportExport(request);
//        } catch (Exception ex) {
//            return null;
//        }
//    }
}


