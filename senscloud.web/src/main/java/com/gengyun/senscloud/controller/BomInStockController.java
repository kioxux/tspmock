package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomInStockService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 备件入库
// */
//@RestController
//@RequestMapping("/bom_in_stock")
public class BomInStockController {
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    private BomInStockService bomInStockService;
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_in_stock")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            Map<String, Object> workType = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_31);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("bom_in_stock_add", "bisAddFlag");
//                put("bom_in_stock_invalid", "rpDeleteFlag");
//                put("bom_in_stock_import", "bisImportFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_in_stock/index", "bom_in_stock")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_in_stock/index");
//        }
//    }
//
//    /**
//     * 获取备件入库列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_in_stock")
//    @RequestMapping("/findBomInStockList")
//    public String findBomInStockList(HttpServletRequest request) {
//        try {
//            return bomInStockService.findBomInStockList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_in_stock")
//    @RequestMapping("/findSingleBisInfo")
//    public ResponseModel findSingleBisInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = bomInStockService.queryBomInStockById(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_in_stock")
//    @RequestMapping("/detail-bomInStock")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return bomInStockService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//
//    /**
//     * 提交数据
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_in_stock_add")
//    @RequestMapping("/doSubmitBomInStock")
//    @Transactional //支持事务
//    public ResponseModel saveBomInStock(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return bomInStockService.save(schema_name, processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_31));
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * @param request
//     * @param response
//     * @return
//     * @Description:报废入库申请
//     */
//    @MenuPermission("bom_in_stock_invalid")
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String in_code = request.getParameter("in_code");
//            if (StringUtils.isBlank(in_code)) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.errorMsg(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//            }
//            return bomInStockService.cancel(schemaName, loginUser, in_code);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:备件入库-验收入库
//     */
//    @MenuPermission("bom_in_stock")
//    @RequestMapping("/inStockAcceptance")
//    @Transactional
//    public ResponseModel inStockAcceptance(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomInStockService.inStockAudit(schemaName, request, paramMap, loginUser, false);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @param request
//     * @param paramMap
//     * @return
//     * @Description:备件入库-审核入库
//     */
//    @MenuPermission("bom_in_stock")
//    @RequestMapping("/inStockAudit")
//    @Transactional
//    public ResponseModel inStockAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomInStockService.inStockAudit(schemaName, request, paramMap, loginUser, true);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }

}


