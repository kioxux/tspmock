package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SelectModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "字典管理")
@RestController
public class SelectOptionsController {
    @Resource
    SelectOptionService selectOptionService;

    @ApiOperation(value = "根据key获取字典数据（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_SELECT_OPTION_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectKey", "selectExtKey"}, ignoreParameters = {"SearchSelectOptionList"})
    @RequestMapping(value = "/getOptionsByKey", method = RequestMethod.POST)
    public ResponseModel searchSelectOptionList(MethodParam methodParam, SelectModel selectModel, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(selectOptionService.getSelectOptionList(methodParam, selectModel.getSelectKey(), paramMap));
    }

    @ApiOperation(value = "根据key和code获取字典选中项数据", notes = ResponseConstant.RSP_DESC_SEARCH_SELECT_OPTION_BY_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectKey", "selectValue"})
    @RequestMapping(value = "/getOptionInfoByKey", method = RequestMethod.POST)
    public ResponseModel searchSelectOptionByCode(MethodParam methodParam, SelectModel selectModel, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(selectOptionService.getSelectOptionByCode(methodParam, selectModel.getSelectKey(), selectModel.getSelectValue(), paramMap));
    }

    @ApiOperation(value = "根据key和code获取字典选中项属性（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_SELECT_OPTION_ATTR_BY_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectKey", "selectValue", "querySelectName"})
    @RequestMapping(value = "/getOptionNameByCode", method = RequestMethod.POST)
    public ResponseModel searchSelectOptionAttrByCode(MethodParam methodParam, SelectModel selectModel) {
        return ResponseModel.ok(selectOptionService.getSelectOptionAttrByCode(methodParam, selectModel.getSelectKey(), selectModel.getSelectValue(), selectModel.getQuerySelectName()));
    }

    @ApiOperation(value = "根据key和extKey获取下拉框数据", notes = ResponseConstant.RSP_DESC_SEARCH_SELECT_OPTION_BY_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectKey", "selectExtKey"})
    @RequestMapping(value = "/getOptionInfoByKeyAndExtKey", method = RequestMethod.POST)
    public ResponseModel searchSelectOptionByKeyAndExtKey(MethodParam methodParam, SelectModel selectModel) {
        return ResponseModel.ok(selectOptionService.getSelectOptionByKeyAndExtKey(methodParam, selectModel.getSelectKey(), selectModel.getSelectExtKey()));
    }

}
