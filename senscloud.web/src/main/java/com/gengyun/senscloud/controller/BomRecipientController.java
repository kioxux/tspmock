package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomRecipientService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.TaskReleaseFinishedService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 备件领用
// */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bom_recipient")
public class BomRecipientController {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private BomRecipientService bomRecipientService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    TaskReleaseFinishedService taskReleaseFinishedService;
//
//
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_recipient")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        Map<String, Object> workType = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_33);
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("bom_recipient_add", "brAddFlag");
//            put("bom_recipient_invalid", "rpDeleteFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_recipient/index", "bom_recipient")
//                .addObject("workTypeId", workType.get("code"))
//                .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_recipient/index");
//        }
//    }
//
//    /**
//     * 获取备件领用列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_recipient")
//    @RequestMapping("/find_bom_recipient_list")
//    public String findBomRecipientList(HttpServletRequest request) {
//        try {
//            return bomRecipientService.findBomRecipientList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_recipient")
//    @RequestMapping("/findSingleBrInfo")
//    public ResponseModel findSingleBrInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = bomRecipientService.queryBomRecipientById(schemaName, subWorkCode);
//            //多时区转换
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 提交数据
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_recipient_add")
//    @RequestMapping("/doSubmitBomRecipient")
//    @Transactional //支持事务
//    public ResponseModel doSubmitBomRecipient(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return bomRecipientService.save(schema_name, processDefinitionId, paramMap, request);
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_recipient")
//    @RequestMapping("/detail-bomRecipient")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return bomRecipientService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * @Description:备件领用审核
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("bom_recipient")
//    @RequestMapping("/bomRecipentAudit")
//    @Transactional
//    public ResponseModel bomRecipentAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        Boolean pass = false;
//        try{
//            return bomRecipientService.inStockRecipent(schemaName, request, paramMap,loginUser,pass);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @Description: 备件领用出库审核
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("bom_recipient")
//    @RequestMapping("/bomRecipentPassAudit")
//    @Transactional
//    public ResponseModel bomRecipentRecipentPassAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        Boolean pass = true;
//        try{
//            return bomRecipientService.inStockRecipent(schemaName, request, paramMap,loginUser,pass);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * @Description:报废入库領用
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("bom_recipient_invalid")
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response){
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try{
//            String recipient_code = request.getParameter("recipient_code");
//            if(StringUtils.isBlank(recipient_code)){
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.errorMsg(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//            }
//            return bomRecipientService.cancel(schemaName, loginUser, recipient_code);
//        }catch (Exception ex){
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
}
