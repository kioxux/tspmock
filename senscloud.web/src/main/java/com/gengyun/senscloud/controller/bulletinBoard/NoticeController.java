package com.gengyun.senscloud.controller.bulletinBoard;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 科华公告
 */
@RestController
@RequestMapping("/notice")
public class NoticeController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    NoticeService noticeService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("notice_add", "noAddFlag");
//            put("notice_edit", "noEditFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bulletin_board/notice/index", "notice");
//    }
//
//    /**
//     * 查询列表页列表数据（分页）
//     *
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-notice-list")
//    public String queryAddressList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                   @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                   @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                   @RequestParam(name = "sortName", required = false) String sortName,
//                                   @RequestParam(name = "sortOrder", required = false) String sortOrder,@RequestParam("beginTime") String beginTime, @RequestParam("endTime") String endTime,
//                                   HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = noticeService.query(schemaName, searchKey, pageSize, pageNumber, sortName, sortOrder,beginTime,endTime);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/addNotice")
//    @Transactional //支持事务
//    public ResponseModel addAddress(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "notice", "notice_add");
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return noticeService.addNotice(schemaName, paramMap);
//            } else {
//                return ResponseModel.error("无权限");
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error("操作失败");
//        }
//    }
//    /**
//     * 进入编辑页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/editView")
//    public ModelAndView editView(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        if(null!=id&&!"".equals(id)){
//            return new ModelAndView("bulletin_board/notice/edit_notice").addObject("notice", JSON.toJSON(noticeService.queryNoticeById(schema_name,Integer.parseInt(id))));
//        }else{
//            return new ModelAndView("bulletin_board/notice/edit_notice");
//        }
//    }
//    /**
//     * 更新数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/editNotice")
//    @Transactional //支持事务
//    public ResponseModel editAddress(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "notice", "notice_edit");
//            if (permissionFlag) {
//                return noticeService.updateNotice(schemaName, paramMap);
//            } else {
//                return ResponseModel.error("权限不足");
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error("操作失败");
//        }
//    }
//    /**
//     * 修改启用
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/changeUse")
//    public ResponseModel changeUse (HttpServletRequest request) {
//       try{
//           String schema_name = authService.getCompany(request).getSchema_name();
//           String id = request.getParameter("id");
//           boolean is_use=Boolean.parseBoolean(request.getParameter("is_use"));
//           return noticeService.updateNotice(schema_name,Integer.parseInt(id),is_use);
//       }catch (Exception e){
//           TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚\
//           return ResponseModel.error("操作失败");
//       }
//
//    }
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/deleteNotice")
//    @Transactional //支持事务
//    public ResponseModel deleteAddress(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User loginUser = AuthService.getLoginUser(request);
//            boolean permissionFlag = true;
//            if (permissionFlag) {
//                return noticeService.deleteById(schemaName, request);
//            } else {
//                return ResponseModel.error("无权限");
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.error("失败");
//        }
//    }
}
