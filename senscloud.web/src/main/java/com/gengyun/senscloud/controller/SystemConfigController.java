package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.SystemConfigModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.system.SystemConfigService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "系统配置维护")
@RestController
public class SystemConfigController {
    @Resource
    SystemConfigService systemConfigService;

    @ApiOperation(value = "查询系统配置列表", notes = ResponseConstant.RSP_DESC_SEARCH_SYSTEM_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "group_title"})
    @RequestMapping(value = "/searchSysConfig", method = RequestMethod.POST)
    public ResponseModel searchSysConfigList(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        return ResponseModel.ok(systemConfigService.getSysConfigList(methodParam, systemConfigModel));
    }

    @ApiOperation(value = "查询系统配置", notes = ResponseConstant.RSP_DESC_SEARCH_SYSTEM_CONFIG_BY_NAME)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "config_name"})
    @RequestMapping(value = "/searchSysConfigByName", method = RequestMethod.POST)
    public ResponseModel searchSysConfigByName(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        return ResponseModel.ok(systemConfigService.getSystemConfigData(methodParam.getSchemaName(), systemConfigModel.getConfig_name()));
    }

    @ApiOperation(value = "查询地图类型系统配置", notes = ResponseConstant.RSP_DESC_SEARCH_MAP_TYPE_SYSTEM_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchMapTypeSysConfig", method = RequestMethod.POST)
    public ResponseModel searchMapTypeSysConfig(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        return ResponseModel.ok(systemConfigService.getMapTypeSysConfigList(methodParam, systemConfigModel));
    }

    @ApiOperation(value = "查询顶层地图类型系统配置", notes = ResponseConstant.RSP_DESC_SEARCH_TOP_MAP_TYPE_SYSTEM_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchTopMapTypeSysConfig", method = RequestMethod.POST)
    public ResponseModel searchTopMapTypeSysConfig(MethodParam methodParam, SystemConfigModel systemConfigModel) {
        return ResponseModel.ok(systemConfigService.getTopMapTypeSysConfigList(methodParam, systemConfigModel));
    }

    @ApiOperation(value = "更新系统配置", notes = ResponseConstant.RSP_DESC_EDIT_SYSTEM_CONFIG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "config_name", "remark",
            "config_title", "group_title", "source_data", "order_index"})
    @Transactional
    @RequestMapping(value = "/editSysConfigList", method = RequestMethod.POST)
    public ResponseModel editSysConfigList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") SystemConfigModel systemConfigModel) {
        systemConfigService.modifySysConfigList(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询系统配置分组名称", notes = ResponseConstant.RSP_DESC_SEARCH_GROUP_TITLE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchGroupTitleList", method = RequestMethod.GET)
    public ResponseModel searchGroupTitleList(MethodParam methodParam) {
        return ResponseModel.ok(systemConfigService.getGroupTitleList(methodParam.getSchemaName()));
    }
}
