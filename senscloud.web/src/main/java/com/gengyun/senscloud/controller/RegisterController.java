package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.MD5;
//import com.gengyun.senscloud.auth.UserRoles;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.RegisterResult;
//import com.gengyun.senscloud.service.login.CompanyService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.Base64;
//import com.gengyun.senscloud.util.Constants;
//import com.gengyun.senscloud.util.IdWorker;
//import com.gengyun.senscloud.util.StringUtil;
//import net.sf.json.JSONArray;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//@RestController
public class RegisterController {
//    @Autowired
//    private CompanyService companyService;
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    //@RequestMapping("/register")
//    public ModelAndView register(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("register");
//    }
//
//    //@RequestMapping("/register2")
//    public ModelAndView register2(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("register2")
//                .addObject("token", request.getParameter("token"));
//    }
//
//
//    //@RequestMapping("/do-register")
//    public JsonResult execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        JsonResult registerResult = new JsonResult();
//        try {
//            String password = request.getParameter("password");
//            String password2 = request.getParameter("password2");
//            if (password.equals(password2) == false) {
//                registerResult.setCode(RegisterResult.CODE_ERROR);
//                registerResult.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_PW_NOT_EQ));
//                return registerResult;
//            }
//
//            try {
//                password = Base64.getBase64(MD5.encodeMD5Lowercase(password));
//            } catch (Exception ex) {
//                registerResult.setCode(RegisterResult.CODE_ERROR);
//                registerResult.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_REG_FA));
//                return registerResult;
//            }
//            String companyName = request.getParameter("companyName");
//            String username = request.getParameter("username");
//            String token = request.getParameter("token");
//            String token2 = (String) request.getSession().getAttribute(Constants.REGISTER_TOKEN_SESSION_KEY);
//
//
//            if (token.equals(token2) == false) {
//                registerResult.setCode(RegisterResult.CODE_ERROR_MOBILE_NOT_CHECKED);
//                registerResult.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_PHONE_UN_VER));
//                return registerResult;
//            }
//
//
//            if (companyService.getCountByCompanyName(companyName) != 0) {
//                registerResult.setCode(RegisterResult.CODE_ERROR_COMPANY_EXIST);
//                registerResult.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_COM_REG));
//                return registerResult;
//            }
//            if (companyService.getCountByAccount(username) != 0) {
//                registerResult.setCode(RegisterResult.CODE_ERROR_ACCOUNT_EXIST);
//                registerResult.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ACC_REG));
//                return registerResult;
//            }
//            IdWorker idWorker = new IdWorker();
//            String mobile = (String) request.getSession().getAttribute(Constants.MOBILE_NUMBER_SESSION_KEY);
//            Company company = new Company();
//            company.setId(idWorker.nextId());
//            company.setCompany_name(companyName);
//            company.setCompany_account(username);
//            company.setPassword(password);
//            company.setMobile(mobile);
//            company.setStatus(1);
//            List<String> roles = new ArrayList<String>();
//            roles.add(UserRoles.ROLE_COMPANY);
//            company.setRoles(JSONArray.fromObject(roles).toString());
//            company.setCreatetime(StringUtil.getTime());
//
//            User companyAdmin = new User();
//            companyAdmin.setId(String.valueOf(idWorker.nextId()));
//            companyAdmin.setAccount(username);
//            companyAdmin.setUsername(username);
//            companyAdmin.setPassword(password);
//            companyAdmin.setCreatetime(new Timestamp(new Date().getTime()));
//            companyAdmin.setStatus(1);
//            companyAdmin.setMobile(mobile);
//            /*Map<String, UserRole> roleMap = new HashMap<String, UserRole>();
//            UserRole userRole = new UserRole();
//            userRole.setId(UserRole.ROLE_ID_COMPANY_ADMIN);
//            userRole.setName(UserRole.ROLE_NAME_COMPANY_ADMIN);*/
//            LinkedHashMap<String, IUserRole> roleSet = new LinkedHashMap<String, IUserRole>();
//            IUserRole userRole = new UserRoleBase();
//            userRole.setId(UserRole.ROLE_ID_COMPANY_ADMIN);
//            userRole.setName(selectOptionService.getLanguageInfo(UserRole.ROLE_NAME_COMPANY_ADMIN));
//            roleSet.put(userRole.getId(), userRole);
//            companyAdmin.setRoles(roleSet);
//
//            companyAdmin.setSchema_name(company.getSchema_name());
//
//            companyService.register(company, companyAdmin);
//            registerResult.setCode(RegisterResult.CODE_OK);
//            registerResult.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_REG_SU));
//            return registerResult;
//        } catch (Exception ex) {
//            registerResult.error(selectOptionService.getLanguageInfo( LangConstant.MSG_REG_SU_ERR));
//            return registerResult;
//        }
//    }
}
