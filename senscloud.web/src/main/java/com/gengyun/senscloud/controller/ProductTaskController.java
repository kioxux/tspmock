package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.ProductTaskModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.dynamic.ProductTaskService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 生产作业单
 */
@RestController
public class ProductTaskController {
    @Resource
    ProductTaskService productTaskService;

    @ApiOperation(value = "获取生产作业单模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_FACILITIES_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchProductTaskPermission", method = RequestMethod.POST)
    public ResponseModel searchFacilitiesPermission(MethodParam methodParam) {
        return ResponseModel.ok(productTaskService.getProductTaskPermission(methodParam));
    }

    @ApiOperation(value = "分页查询生产作业单列表", notes = ResponseConstant.RSP_DESC_SEARCH_PRODUCT_TASK_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchProductTaskPage", method = RequestMethod.POST)
    public ResponseModel searchProductTaskPage(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        return ResponseModel.ok(productTaskService.findProductTaskPage(methodParam, paramMap));
    }

    @ApiOperation(value = "查询生产作业单详情", notes = ResponseConstant.RSP_DESC_SEARCH_PRODUCT_TASK_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "product_task_code"})
    @RequestMapping(value = "/searchProductTaskInfo", method = RequestMethod.POST)
    public ResponseModel searchProductTaskInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        return ResponseModel.ok(productTaskService.findProductTaskInfo(methodParam, paramMap));
    }

    @ApiOperation(value = "查询生产作业单明细列表", notes = ResponseConstant.RSP_DESC_SEARCH_PRODUCT_TASK_WELL_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "product_task_code"})
    @RequestMapping(value = "/searchProductTaskWellList", method = RequestMethod.POST)
    public ResponseModel searchProductTaskWellList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        return ResponseModel.ok(productTaskService.findProductTaskWellList(methodParam, paramMap));
    }

    @ApiOperation(value = "更新生产作业单信息", notes = ResponseConstant.RSP_DESC_EDIT_PRODUCT_TASK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "product_task_code", "create_user_id", "create_time", "remark", "is_contain_hydrogen_sulfide", "cable_joint_location", "total_vehicle_kilometers"})
    @Transactional
    @RequestMapping(value = "/editProductTask", method = RequestMethod.POST)
    public ResponseModel editProductTask(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        productTaskService.modifyProductTask(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新生产作业下井信息", notes = ResponseConstant.RSP_DESC_EDIT_PRODUCT_TASK_WELL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "asset_out_code", "max_temperature", "bottom_pressure", "total_tension_jamming", "net_tension_jamming", "depth_of_encounter"})
    @Transactional
    @RequestMapping(value = "/editProductTaskWell", method = RequestMethod.POST)
    public ResponseModel editProductTaskWell(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        productTaskService.modifyProductTaskWell(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新生产作业下井次数信息", notes = ResponseConstant.RSP_DESC_EDIT_PRODUCT_TASK_WELL_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "go_down_time", "come_out_time", "product_task_well_item_asset_list","total_tension_jamming","net_tension_jamming","depth_of_encounter"})
    @Transactional
    @RequestMapping(value = "/editProductTaskWellItem", method = RequestMethod.POST)
    public ResponseModel editProductTaskWellItem(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        productTaskService.modifyProductTaskWellItem(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取生产作业下井详情", notes = ResponseConstant.RSP_DESC_SEARCH_PRODUCT_TASK_WELL_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchProductTaskWellInfo", method = RequestMethod.POST)
    public ResponseModel searchProductTaskWellInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        return ResponseModel.ok(productTaskService.findProductTaskWellInfo(methodParam, paramMap));
    }

    @ApiOperation(value = "获取生产作业下井次数详情", notes = ResponseConstant.RSP_DESC_SEARCH_PRODUCT_TASK_WELL_ITEM_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchProductTaskWellItemInfo", method = RequestMethod.POST)
    public ResponseModel searchProductTaskWellItemInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        return ResponseModel.ok(productTaskService.findProductTaskWellItemInfo(methodParam, paramMap));
    }

    @ApiOperation(value = "删除生产作业单", notes = ResponseConstant.RSP_DESC_REMOVE_PRODUCT_TASK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "product_task_code"})
    @Transactional
    @RequestMapping(value = "/removeProductTask", method = RequestMethod.POST)
    public ResponseModel removeProductTask(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        productTaskService.cutProductTask(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除生产作业单下井信息", notes = ResponseConstant.RSP_DESC_REMOVE_PRODUCT_TASK_WELL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional
    @RequestMapping(value = "/removeProductTaskWell", method = RequestMethod.POST)
    public ResponseModel removeProductTaskWell(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        productTaskService.cutProductTaskWell(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除生产作业单下井次数信息", notes = ResponseConstant.RSP_DESC_REMOVE_PRODUCT_TASK_WELL_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional
    @RequestMapping(value = "/removeProductTaskWellItem", method = RequestMethod.POST)
    public ResponseModel removeProductTaskWellItem(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        productTaskService.cutProductTaskWellItem(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询出库单下设备列表列表", notes = ResponseConstant.RSP_DESC_SEARCH_PRODUCT_TASK_WELL_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "asset_out_code","keywordSearch"})
    @RequestMapping(value = "/searchAssetListByAssetOutCode", method = RequestMethod.POST)
    public ResponseModel searchAssetListByAssetOutCode(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") ProductTaskModel productTaskModel) {
        return ResponseModel.ok(productTaskService.findAssetListByAssetOutCode(methodParam, paramMap));
    }
}
