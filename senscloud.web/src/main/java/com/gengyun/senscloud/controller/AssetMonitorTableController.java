package com.gengyun.senscloud.controller;

///*
//* 集中处理设备信息控制器：设备的BOM、维修记录、性能指标等
//*/
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.view.AssetMonitorDataExportView;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/asset_monitor_table")
public class AssetMonitorTableController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    AssetInfoService assetInfoService;
//    @Autowired
//    BomService bomService;
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    AssetDataServiceV2 assetDataServiceV2;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    AnalysReportService analysReportService;
//    @Autowired
//    WorkSheetService workSheetService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    /**
//     * 获得所有的设备备件
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_monitor_table")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView FindALLAssetBomList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//
//            result.setMsg("");
//            return new ModelAndView("asset_data_management/asset_monitor_table");
//        } catch (Exception ex) {
//            result.setCode(-1);
////            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_BOM_ERR)+ ex.getMessage());
//            return new ModelAndView("asset_data_management/asset_monitor_table");
//        }
//    }
//
//
//    //获取设备的监控数据
//    @MenuPermission("asset_monitor_table")
//    @RequestMapping("/asset_monitor_table_data")
//    public String  getAssetMonitorHistoryDataListforTable(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject pageResult = new JSONObject();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String condition="";
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(a.asset_code) like upper('%" + keyWord + "%') or upper(a.strname) like upper('%" + keyWord + "%' ))";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime );
//                condition += " and a.gather_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime );
//                condition += " and a.gather_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String,Object>> dataList = assetInfoService.getAssetMonitorDataListt(schema_name,condition,pageSize,begin);
//            int total=assetInfoService.getAssetMonitorDataListCount(schema_name,condition);
//            pageResult.put("rows",dataList);
//            pageResult.put("total",total);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return "{｝";
//        }
//    }
//
//    //导出设备的监控数据
//    @MenuPermission("asset_monitor_table")
//    @RequestMapping("/asset_monitor_table_data_exports")
//    public ModelAndView  getAssetMonitorHistoryDataListforExports(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            Map<String, String[]> parmMap=request.getParameterMap();
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String[] showFields=parmMap.get("showFields");
//            String[] showColumns=parmMap.get("showColumns");
//            String endTime = request.getParameter("endTime");
//            String condition="";
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(a.asset_code) like upper('%" + keyWord + "%') or upper(a.strname) like upper('%" + keyWord + "%' ))";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime );
//                condition += " and a.gather_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime );
//                condition += " and a.gather_time <'" + end + "'";
//            }
//            List<Map<String,Object>> dataList = assetInfoService.getAssetMonitorDataListforExports(schema_name, condition );
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("dataList", dataList);
//            map.put("showFields", showFields);
//            map.put("showColumns", showColumns);
//            map.put("selectOptionService", selectOptionService);
//            AssetMonitorDataExportView monitorview=new  AssetMonitorDataExportView();
//            //map.put("name", "魅力城市");
//            return new ModelAndView(monitorview, map);
//        } catch (Exception ex) {
//            return new ModelAndView("asset_data_management/asset_monitor_table").addObject("error","Data too large to export");
//        }
//    }


}
