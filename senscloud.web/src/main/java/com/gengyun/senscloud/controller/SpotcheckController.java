package com.gengyun.senscloud.controller;

//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.view.SpotDataExportView;
//import org.apache.commons.lang.NullArgumentException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
///**
// * Created by hisou on 2018/3/21.   // 准备删除 yzj  2019-08-04
// */
//@RestController
public class SpotcheckController {
//
//    @Autowired
//    WorkScheduleService workScheduleService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    InspectionService inspectionService;
//
//    @Autowired
//    BomService bomService;
//
//    @Autowired
//    SpotService spotService;
//
//    @Autowired
//    SpotCheckService spotCheckService;
//
//    @Autowired
//    MaintainService maintainService;
//
//    @Autowired
//    AssetDataService assetDataService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    AssetDutyManService assetDutyManService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//
//    @RequestMapping("/spotcheck-management")
//    public ModelAndView spotcheckManagement(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        Company company = authService.getCompany(request);
//        User loginUser = AuthService.getLoginUser(request);
//        String facilitiesSelect = "";
//        Boolean canExportData = false;
//        Boolean canSpot = false;//判断是否拥有作废权限
//        Boolean spotAddPc = false;//企业端新增点检权限
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "spotcheck");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("spot_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("spot_export")) {
//                        canExportData = true;
//                    } else if (functionData.getFunctionName().equals("spot_invalid")) {
//                        canSpot = true;
//                    } else if (functionData.getFunctionName().equals("spot_do_pc")) {
//                        spotAddPc = true;
//                    }
//                    if (isAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        return new ModelAndView("spotcheck/spotcheck_list")
//                .addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("canExportData", canExportData)
//                .addObject("user_facility_id", company.getFacilityId())
//                .addObject("canSpot", canSpot)
//                .addObject("spotAddPc", spotAddPc);
//    }
//
//    @RequestMapping("/find-spotcheck-list")
//    public String querySpotcheck(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        org.json.JSONObject result = new org.json.JSONObject();
//        List<SpotcheckData> spotcheckDataList = null;
//        try {
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String facilities = request.getParameter("facilities");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            User user = AuthService.getLoginUser(request);
//            String account = user.getAccount();
//            String condition;
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "spotcheck");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("spot_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("spot_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            if (facilities != null && !facilities.isEmpty()) {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilities, facilityList);
//                condition = " and r.facility_id in (" + facilities + subFacility + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = "  ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String data : facilityList.keySet()) {
//                            facilityIds += data + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = " and r.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition = " and ( r.spot_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    condition = " and ( r.spot_account='" + account + "' )";
//                }
//            }
//
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(r.spot_code) like upper('%" + keyWord + "%') or upper(r.area_code) like upper('%" + keyWord + "%') or upper(a.area_name) like upper('%" + keyWord + "%') or u.username like '%" + keyWord + "%' or r.spot_account like '%" + keyWord + "%' )";
//            }
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.spot_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.spot_time <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            spotcheckDataList = spotService.getToAuditSpotList(schema_name, condition, pageSize, begin);
//            int total = spotService.getToAuditSpotListCount(schema_name, condition);
//
//            result.put("rows", spotcheckDataList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping("/find-spotcheck-detail")
//    public ResponseModel querySingleSpotcheck(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        User user = AuthService.getLoginUser(request);
//        String account = user.getAccount();
//        try {
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "spotcheck");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("spot_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("spot_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            String code = request.getParameter("code");
//            SpotcheckData data = spotService.getSpotInfo(schema_name, code);
//            if (data != null) {
//                data.setRepairDataList(repairService.getRepairListByFromCode(schema_name, code));
//            }
//            result.setCode(1);
//            result.setMsg("");
//            result.setContent(data);
//            if (isAllFacility) {
//                return result;
//            } else if (isSelfFacility) {
//                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        if (facilityList.get(key).getId() == data.getFacility_id()) {
//                            return result;
//                        }
//                    }
//                }
//            }
//            if (account.equalsIgnoreCase(data.getSpot_account())) {
//                return result;
//            }
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_AUTH_SPOT));
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    @RequestMapping("/cancleSpotCheck")
//    public ResponseModel cancleSpotCheck(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String code = request.getParameter("code");
//            int doCancel = spotService.cancleSpotListCount(schema_name, code);
//            if (doCancel > 0) {
//                result.setCode(1);
//                result.setMsg("");
//                result.setContent(doCancel);
//            } else {
//                result.setCode(0);
//                result.setMsg("");
//                result.setContent(doCancel);
//            }
//            return result;
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
////    @RequestMapping("/find-spotcheck-item")
////    public ResponseModel querySpotcheckItem(HttpServletRequest request, HttpServletResponse response) {
////        String schema_name = authService.getCompany(request).getSchema_name();
////        ResponseModel result = new ResponseModel();
////        try {
////            String type = "lsxsb";
////            List<MaintainCheckItemData> list = maintainService.getMaintainCheckItem(schema_name, "lsxsb");
////            result.setCode(1);
////            result.setMsg("");
////            result.setContent(list);
////            return result;
////        } catch (Exception ex) {
////
////            return ResponseModel.error("获取失败");
////        }
////    }
//
//    @RequestMapping("/save-spotcheck-data")
//    @Transactional
//    public ResponseModel saveSpotcheck(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            SpotcheckData data = new SpotcheckData();
//
//            String spotCode = serialNumberService.generateMaxBusinessCode(schema_name, "spotcheck");
//            data.setSpot_code(spotCode);
//            data.setFacility_id(Integer.parseInt(request.getParameter("facilityId")));
//            data.setAreaCode(request.getParameter("areacode"));
//            data.setStatus(60);
//            data.setSpot_account(request.getParameter("spotAccount"));
//            data.setSpot_time(new Timestamp(new Date().getTime()));
//
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//        }
//    }
//
//
//    @RequestMapping("/spotcheck-export")
//    public ModelAndView Exportspot(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        org.json.JSONObject result = new org.json.JSONObject();
//        List<SpotcheckData> spotcheckDataList = null;
//        try {
//            User user = authService.getLoginUser(request);
//            //根据日期和巡检点，查找巡检
//            int pageSize = 50000;//Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = 1;//Integer.parseInt(request.getParameter("pageNumber"));
//            String facilities = request.getParameter("facilities");
//            String keyWord = request.getParameter("keyWord");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String account = user.getAccount();
//            String condition;
//
//            Boolean isAllFacility = false;
//            Boolean isSelfFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "spotcheck");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("spot_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("spot_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            if (facilities != null && facilities != "") {
//                List<Facility> facilityList = facilitiesService.FacilitiesListInUse(schema_name);
//                //加上位置的子位置
//                String subFacility = facilitiesService.getSubFacilityIds(facilities, facilityList);
//                condition = " and r.facility_id in (" + facilities + subFacility + ") ";
//            } else {
//                if (isAllFacility) {
//                    condition = "  ";
//                } else if (isSelfFacility) {
//                    LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                    String facilityIds = "";
//                    if (facilityList != null && !facilityList.isEmpty()) {
//                        for (String data : facilityList.keySet()) {
//                            facilityIds += data + ",";
//                        }
//                    }
//                    if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                        facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                        condition = " and r.facility_id in (" + facilityIds + ") ";
//                    } else {
//                        condition = " and ( r.spot_account='" + account + "' )";    //没有用户所属位置，则按个人权限查询
//                    }
//                } else {
//                    condition = " and ( r.spot_account='" + account + "' )";
//                }
//            }
//
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(r.inspection_code) like upper('%" + keyWord + "%') or upper(r.area_code) like upper('%" + keyWord + "%') or upper(a.area_name) like upper('%" + keyWord + "%') or c.username like '%" + keyWord + "%' or r.spot_account like '%" + keyWord + "%' )";
//            }
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and r.spot_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and r.spot_time <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            spotcheckDataList = spotService.getToAuditSpotList(schema_name, condition, pageSize, begin);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("spotcheckDataList", spotcheckDataList);
//
//            SpotDataExportView excelView = new SpotDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//
//    //获取点检单列表
//    @RequestMapping("/get_spot_item_list")
//    public ResponseModel getSpotItemList(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            List<SpotcheckItemData> list = spotCheckService.SpotCheckItemList(schema_name);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_SUCC));
//            result.setContent(list);
//            return result;
//        } catch (Exception ex) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_FAIL));
//            //系统错误日志
//            // logService.AddLog(schema_name, "system", "maintain_list", "点检列表获取出现了问题：" + ex.getMessage(), account);
//            return result;
//        }
//    }
//
//    //点检结果提交
//    @RequestMapping("get_spotcheck_add")
//    @Transactional //支持事务
//    public ResponseModel getSpotCheckAdd(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();//从配置文件读取数据库配置
//        String account = authService.getLoginUser(request).getAccount();
//        ResponseModel result = new ResponseModel();
//        //当前登录人，也是点检的提交人员
//        try {
//            String areaCode = request.getParameter("areaCode");
//            String itemList = request.getParameter("itemListWord");
//            int faultNumber = Integer.parseInt(request.getParameter("faultNumber"));
//            String spotcheckCode = request.getParameter("spotcheckcode");
//            String beginTime = request.getParameter("begintime");
//            String assetList = request.getParameter("assetListLast");
//            //获取当前设备
//            InspectionAreaData areaData = inspectionService.InspectionAreaDatabyarea(schema_name, areaCode);
//
////            InspectionAreaData areaData = inspectionService.InspectionAreaDatabyarea(schema_name, "000001");
//            if (areaData == null || areaData.getArea_code().isEmpty()) {
//                result.setCode(2);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_AUTH_SPOT_ERROR));
//                return result;
//            }
//
//            SpotcheckData spotData = new SpotcheckData();
//            int resultOption = 0;
//            //订单号不存在，则是新增，生成新订单编号
//            if (spotcheckCode == null || spotcheckCode.isEmpty()) {
//                spotcheckCode = serialNumberService.generateMaxBusinessCode(schema_name, "spot");
//            }
//            spotData.setSpot_code(spotcheckCode);
//            spotData.setSpot_account(account);
//            spotData.setStatus(60);
//            spotData.setFault_number(faultNumber);
//            spotData.setFacility_id(areaData.getFacility_id());
//            spotData.setRemark("");
//            spotData.setAreaCode(areaCode);
//            spotData.setSpot_result(itemList);
//
//            //获取当前时间
//            Timestamp now = new Timestamp(System.currentTimeMillis());
//
//            //检查当前时间是否比发生时间早，是的话发生时间不合法
//            if (now.before(Timestamp.valueOf(beginTime))) {
//                spotData.setBegin_time(now);
//            } else {
//                spotData.setBegin_time(Timestamp.valueOf(beginTime));
//            }
//            spotData.setSpot_time(now);
//
//            resultOption = spotCheckService.insertSpotCheckData(schema_name, spotData);
//            if (resultOption > 0) {
//
//                //assetList
//                if (assetList != null && !assetList.equals("")) {
//                    net.sf.json.JSONArray jsonasset = net.sf.json.JSONArray.fromObject(assetList);
////将检测类型不为2的且未选中的汇总作为上报问题
//                    net.sf.json.JSONArray itemListjson = net.sf.json.JSONArray.fromObject(itemList);
//                    String fault_note = selectOptionService.getLanguageInfo( LangConstant.MSG_SPOT_ITEM);
//                    for (int i = 0; i < itemListjson.size(); i++) {
//                        String isCheck = itemListjson.getJSONObject(i).getString("isCheck");
//                        String item_name = itemListjson.getJSONObject(i).getString("item_name");
//                        String resultType = itemListjson.getJSONObject(i).getString("resultType");
//                        //巡检类型不为2且没打勾的算有问题
//                        if (!resultType.equals("2") && isCheck.equals("false")) {
//                            fault_note = fault_note + item_name + ";";
//                        }
//                    }
//                    fault_note += selectOptionService.getLanguageInfo( LangConstant.MSG_NEED_REPAIR);
//
//                    //获取当前时间
//                    now = new Timestamp(System.currentTimeMillis());
//                    Calendar calendar = new GregorianCalendar();
//                    calendar.setTime(now);
//                    calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
//                    Date date = calendar.getTime();   //这个时间就是日期往后推一天的结果
//                    Timestamp nousedate = new Timestamp(date.getTime());
//                    List<User> repairusers = null; //repairuser(schema_name, areaData.getFacility_id(), now, now);
//                    int assetInSiteNum = jsonasset.size();      // 该位置需要保养的设备数量
//                    int scheduleInSiteNum = repairusers.size();        // 该位置当天排班人数\
//                    // 每人平均负责保养的设备数量（向下取整）
//                    int avgNum = 1;
//                    int isBigYu = 0; //是否有余
//                    if (repairusers.size() != 0) {
//                        avgNum = assetInSiteNum / scheduleInSiteNum;
//                        isBigYu = assetInSiteNum % scheduleInSiteNum;
//                    }
//                    // 得到指定json key对象的value对象
//                    String groupId = "";
//                    for (int i = 0; i < jsonasset.size(); i++) {
//                        String assetcode = jsonasset.getJSONObject(i).getString("assetcode");
//                        //获取当前设备
//                        Asset assetModel = repairService.getAssetCommonDataByCode(schema_name, assetcode);
//                        if (assetModel == null || assetModel.get_id().isEmpty()) {
//                            throw new NullArgumentException(selectOptionService.getLanguageInfo( LangConstant.MSG_ASSET_NOTHINGNESS));
//                        }
//
//                        if (scheduleInSiteNum > 0) {
//                            User user = new User();
//                            //首先看设备有没有专门的负责人员，有的话先给他
//                            List<User> dutyUserList = assetDutyManService.findDutyUserByAssetId(schema_name, 1, assetModel.get_id(), new Long(assetModel.getIntsiteid()).intValue());
//                            if (dutyUserList != null && dutyUserList.size() > 0) {
//                                int max = dutyUserList.size();
//                                int getInt = (int) (new Random().nextFloat() * max);
//                                user = dutyUserList.get(getInt);
//                            }
//                            if (user == null || user.getAccount() == null || user.getAccount().isEmpty()) {
//                                if (avgNum != 0 && i / avgNum < repairusers.size()) {
//                                    // 能整除的部分平均分配
//                                    user = repairusers.get(i / avgNum);
//                                } else {
//                                    // 余下的重新再给分配一遍；或者没人只分得0个的，即人比设备多
//                                    int max = repairusers.size();
//                                    int last = 0;
//                                    if (isBigYu > 0) {
////                                        last = i % scheduleInSiteNum;
//                                        //随机取
//                                        last = (int) (new Random().nextFloat() * max);
//                                    }
//                                    user = repairusers.get(last);
//                                }
//                            }
//
//                            RepairData model = new RepairData();
//                            //生成新订单编号
//                            String repairCode = serialNumberService.generateMaxBusinessCode(schema_name, "repair");
//                            model.setRepairCode(repairCode);
//                            model.setAssetId(assetModel.get_id());
//                            model.setAssetType(assetModel.getAsset_type());
//                            model.setAssetStatus(2);
//                            model.setDeadlineTime(nousedate);
//                            model.setOccurtime(now);
//                            model.setFaultNote(fault_note);
//                            model.setDistributeAccount("");
//                            model.setDistributeTime(null);
//                            model.setReceiveAccount(user.getAccount());
//                            //判断状态
//                            int status = 40;   //待分配
//                            model.setReveiveTime(now);
//                            model.setDistributeTime(now);
//                            model.setRepairBeginTime(null);
//                            model.setStatus(status);
//                            model.setFacilityId((int) assetModel.getIntsiteid());
//                            model.setCreate_user_account(account);
//                            model.setCreatetime(now);
//                            model.setFromCode(spotcheckCode);
//                            model.setPriorityLevel(2);
//                            //保存上报问题
//                            int doCount = repairService.saveRepairInfo(schema_name, model);
//                        }
//                    }
//                }
//
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SBUMIT_SPOT_SU));
//                result.setContent(spotcheckCode);
//                //记录历史
//                logService.AddLog(schema_name, "spot", spotcheckCode, selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_SPOT), account);
//                return result;
//            } else {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SBUMIT_SPOT_FA));
//                return result;
//            }
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_ERR_ADMIN));
//            //系统错误日志
//            logService.AddLog(schema_name, "system", "spotcheck_add", selectOptionService.getLanguageInfo( LangConstant.MSG_INS_ERROR) + ex.getMessage(), account);
//            return result;
//        }
//    }
//
////    public List<User> repairuser(String schema, int facility, Timestamp now, Timestamp nowadd) {
////        //找到该位置所有的，能执行保养提交任务的人员
////        //修改为白天在上班的能保养的人员
////        List<User> scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////        if (scheduleList.size() == 0) {
////            //再找一级所属位置的，看有没有排班的人
////            List<Facility> facilities = facilitiesService.FacilitiesList(schema);
////            for (Facility facilityxx : facilities) {
////                if (facilityxx.getId() == facility)
////                    facility = facilityxx.getParentId();
////                break;
////            }
////            scheduleList = workScheduleService.getWorkScheduleBySiteAndTimeOnDuty(schema, facility, "repair_do", now, nowadd);
////            if (scheduleList.size() == 0) {
////                //还是没有人，则按角色找
//////                scheduleList = userService.findUserByFunctionKey(schema, "repair_do", facility);
////            }
////        }
////        //找到的人如果没有配置，则不分配保养人员
////        if (scheduleList == null || scheduleList.isEmpty()) {
////            scheduleList = new ArrayList<>();
////        }
////        return scheduleList;
////    }
}
