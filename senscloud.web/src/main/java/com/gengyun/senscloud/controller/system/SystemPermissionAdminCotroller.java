package com.gengyun.senscloud.controller.system;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sys")
public class SystemPermissionAdminCotroller {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    SystemPermissionService systemPermissionService;
//
//    @RequestMapping("/permission_index")
//    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            SystemUser systemUser = authService.getSystemLoginUser(request);
//            List<SystemPermission> systemPermissionList = systemPermissionService.findAll();
//            TreeBuilder treeBuilder = new TreeBuilder();
//            List<Node> allNodes = new ArrayList<Node>();
//            for (SystemPermission item : systemPermissionList) {
//                Node node = new Node();
//                node.setId(item.getId());
//                node.setParentId(item.getParent_id());
//                node.setName(item.getName());
//                node.setDetail(item);
//                allNodes.add(node);
//            }
//            List<Node> roots = treeBuilder.buildListToTree(allNodes);
//            String permissionSelect = TreeUtil.getOptionListForSelect(roots);
//
//            return new ModelAndView("sys/permission")
//                    .addObject("systemUser", systemUser.getUsername())
//                    .addObject("permissionList", roots)
//                    .addObject("permissionSelect", permissionSelect);
//
//        } catch (Exception ex) {
//            return new ModelAndView("sys/permission")
//                    .addObject("error", ex.getMessage());
//        }
//    }
//
//    @RequestMapping("/add_permission")
//    public JsonResult addPermission(HttpServletRequest request, HttpServletResponse response) {
//        JsonResult jsonResult = new JsonResult();
//        try {
//            SystemPermission systemPermission = new SystemPermission();
//            String name = request.getParameter("name");
//            String key = request.getParameter("key");
//            if (RegexUtil.isNull(name)) {
//                return jsonResult.error("权限名称不能为空");
//            }
//            if (RegexUtil.isNull(key)) {
//                return jsonResult.error("权限标识不能为空");
//            }
//            if (systemPermissionService.getCountByKey(key) > 0) {
//                return jsonResult.error("权限标识已经存在");
//            }
//            String id = String.valueOf(new IdWorker().nextId());
//            systemPermission.setId(id);
//            if ("on".equalsIgnoreCase(request.getParameter("available"))) {
//                systemPermission.setAvailable(true);
//            } else {
//                systemPermission.setAvailable(false);
//            }
//
//            systemPermission.setName(name);
//            systemPermission.setKey(key);
//            systemPermission.setCreatetime(new Timestamp(new Date().getTime()));
//            systemPermission.setIcon(request.getParameter("icon"));
//            systemPermission.setIndex(0);
//            systemPermission.setParent_id(request.getParameter("parent_id"));
//            systemPermission.setType(Integer.valueOf(request.getParameter("type")));
//            systemPermission.setUrl(request.getParameter("url"));
//
//            systemPermissionService.add(systemPermission);
//
//            return jsonResult.success("添加权限成功");
//        } catch (Exception ex) {
//            return jsonResult.error("添加权限失败");
//        }
//    }
//
//    @RequestMapping("/remove_permission")
//    public JsonResult removePermission(HttpServletRequest request, HttpServletResponse response) {
//        JsonResult jsonResult = new JsonResult();
//        try {
//            String id = request.getParameter("id");
//            systemPermissionService.remove(id);
//            return jsonResult.success("删除权限成功");
//        } catch (Exception ex) {
//            return jsonResult.error("删除权限失败");
//        }
//    }
//
//    @RequestMapping("/permission_detail")
//    public ModelAndView permissionDetail(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String id = request.getParameter("id");
//            SystemPermission systemPermission = systemPermissionService.findById(id);
//
//            List<SystemPermission> systemPermissionList = systemPermissionService.findAll();
//            TreeBuilder treeBuilder = new TreeBuilder();
//            List<Node> allNodes = new ArrayList<Node>();
//            for (SystemPermission item : systemPermissionList) {
//                Node node = new Node();
//                node.setId(item.getId());
//                node.setParentId(item.getParent_id());
//                node.setName(item.getName());
//                node.setDetail(item);
//                allNodes.add(node);
//            }
//            List<Node> roots = treeBuilder.buildListToTree(allNodes);
//            String permissionSelect = TreeUtil.getOptionListForSelect(roots);
//
//            return new ModelAndView("sys/permission_detail")
//                    .addObject("data", systemPermission)
//                    .addObject("permissionSelect", permissionSelect);
//        } catch (Exception ex) {
//            return new ModelAndView("sys/permission_detail")
//                    .addObject("error", "读取权限项详细信息失败");
//        }
//    }
//
//    @RequestMapping("/update_permission")
//    public JsonResult updatePermission(HttpServletRequest request, HttpServletResponse response) {
//        JsonResult jsonResult = new JsonResult();
//        try {
//            SystemPermission systemPermission = new SystemPermission();
//            String name = request.getParameter("name");
//            String key = request.getParameter("key");
//            if (RegexUtil.isNull(name)) {
//                return jsonResult.error("权限名称不能为空");
//            }
//            if (RegexUtil.isNull(key)) {
//                return jsonResult.error("权限标识不能为空");
//            }
//            String id = request.getParameter("id");
//            systemPermission.setId(id);
//            if ("on".equalsIgnoreCase(request.getParameter("available"))) {
//                systemPermission.setAvailable(true);
//            } else {
//                systemPermission.setAvailable(false);
//            }
//
//            systemPermission.setName(name);
//            systemPermission.setKey(key);
//            systemPermission.setCreatetime(new Timestamp(new Date().getTime()));
//            systemPermission.setIcon(request.getParameter("icon"));
//            int index = 0;
//            try {
//                index = Integer.valueOf(request.getParameter("index"));
//            } catch (Exception ex) {
//                index = 0;
//            }
//            systemPermission.setIndex(index);
//            systemPermission.setParent_id(request.getParameter("parent_id"));
//            systemPermission.setType(Integer.valueOf(request.getParameter("type")));
//            systemPermission.setUrl(request.getParameter("url"));
//
//            systemPermissionService.update(systemPermission);
//
//            return jsonResult.success("更新权限成功");
//        } catch (Exception ex) {
//            return jsonResult.error("更新权限失败");
//        }
//    }
}
