package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.SystemConfigService;
//import com.gengyun.senscloud.service.UserRegisterService;
//import com.gengyun.senscloud.util.SenscloudException;
//import com.gengyun.senscloud.util.StringUtil;
//import com.google.zxing.qrcode.QRCodeWriter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//import com.google.zxing.BarcodeFormat;
//import com.google.zxing.WriterException;
//import com.google.zxing.client.j2se.MatrixToImageWriter;
//import com.google.zxing.common.BitMatrix;
//
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/user_register")
public class UserRegisterController {
//    @Autowired
//    UserRegisterService userRegisterService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_register")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("user_register_audit", "userRegisterAuditFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "user_register/index", "user_register");
//    }
//
//
//    /**
//     * 获取用户注册列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_register")
//    @RequestMapping("/findUserList")
//    public String findUserClientList(HttpServletRequest request) {
//        try {
//            return userRegisterService.findUserRegisterList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //用户注册二维码提供页面
//    @MenuPermission("user_register")
//    @RequestMapping({"/", "/register_link"})
//    public ModelAndView registerLink(HttpServletRequest request) {
//        String companyName = authService.getCompany(request).getCompany_name();
//        return pagePermissionService.getUserFunctionPermissionForPage(request, null, "user_register/register_link", "user_register").addObject("companyName", companyName);
//    }
//
//    // 生成二维码
//    @MenuPermission("user_register")
//    @RequestMapping("/register_qrcode")
//    public void userRegisterQRcode(HttpServletRequest request, HttpServletResponse response) {
//        ServletOutputStream stream = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "mini_register_url");
//            if (null != data && StringUtil.isNotEmpty(data.getSettingValue())) {
//                String miniUserRegisterUrl = systemConfigService.getSystemConfigData(schema_name, "mini_register_url").getSettingValue();
//                String companyId = schema_name.substring(7);
//                miniUserRegisterUrl += companyId;
//                stream = response.getOutputStream();
//                QRCodeWriter writer = new QRCodeWriter();
//                BitMatrix m = writer.encode(miniUserRegisterUrl, BarcodeFormat.QR_CODE, 180, 180);
//                MatrixToImageWriter.writeToStream(m, "png", stream);
//            }
//        } catch (WriterException e) {
//            e.printStackTrace();
//        } catch (IOException ex) {
//
//        } finally {
//            if (stream != null) {
//                try {
//                    stream.flush();
//                    stream.close();
//                } catch (IOException ex) {
//                }
//            }
//        }
//    }
//
//
//    /**
//     * 根据主键查询用户注册数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_register")
//    @RequestMapping("/findSingleUserRegisterInfo")
//    public ResponseModel findSingleUserRegisterInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = userRegisterService.queryUserRegisterByCode(schemaName, subWorkCode);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_register")
//    @RequestMapping("/detail_userRegister")
//    public ModelAndView goToUserRegisterDetail(HttpServletRequest request) {
//        try {
//            return userRegisterService.getUserRegisterDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("user_register_audit")
//    @RequestMapping("/doAuditUserRegister")
//    public ResponseModel doAuditUserRegister(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            long companyId = authService.getCompany(request).getId();
//            String account = AuthService.getLoginUser(request).getAccount();
//            paramMap.put("account", account);
//            int doAuditResult = userRegisterService.auditUserRegister(request, schema_name, companyId, paramMap);
//            if (1 == doAuditResult) {
//                return ResponseModel.ok("1");
//            } else {
//                return ResponseModel.errorMsg(String.valueOf(doAuditResult));
//            }
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
}
