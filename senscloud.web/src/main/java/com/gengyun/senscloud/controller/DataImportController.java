package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.DataImportService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Api(tags = "数据导入")
@RestController
public class DataImportController {

    @Resource
    private DataImportService dataImportService;

    @ApiOperation(value = "数据导入", notes = ResponseConstant.RSP_DESC_IMPORT_DATA)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "importType", "file"})
    @Transactional
    @RequestMapping(value = "/dataImport", method = RequestMethod.POST)
    public ResponseModel addFile(MethodParam methodParam,
                                 @ApiParam(value = "数据导入类型:1、设备导入 2、备件导入 4、生产作业导入 5、维保行事历导入 6、用户") @RequestParam("importType") int importType,
                                 @ApiParam(value = "数据导入excel文件") @RequestParam(name = "file") MultipartFile fileModel) {
        return dataImportService.doImport(methodParam, importType, fileModel);
    }

    @ApiOperation(value = "获取数据导入模板", notes = ResponseConstant.RSP_DESC_EXPORT_DATA_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "importType"})
    @RequestMapping(value = "/exportDataTemplate", method = RequestMethod.POST)
    public void exportDataTemplate(MethodParam methodParam, @ApiParam(value = "数据导入模板类型:1、设备导入模板 2、备件导入模板") @RequestParam("importType") int importType) {
        dataImportService.getDataTemplate(methodParam, importType);
    }

    @ApiOperation(value = "获取数据导入日志列表", notes = ResponseConstant.RSP_DESC_SEARCH_IMPORT_DATA_LOGS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "importType", "pageSize", "pageNumber"})
    @RequestMapping(value = "/searchImportDataLogs", method = RequestMethod.POST)
    public ResponseModel searchImportDataLogs(MethodParam methodParam, @ApiParam(value = "数据导入模板类型:1、设备导入模板 2、备件导入模板") @RequestParam(name = "importType", required = false) Integer importType) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(dataImportService.getImportDataLogs(methodParam, importType));
    }

    @ApiOperation(value = "获取数据导入类型列表", notes = ResponseConstant.RSP_DESC_SEARCH_IMPORT_TYPES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token"})
    @RequestMapping(value = "/searchImportTypes", method = RequestMethod.POST)
    public ResponseModel searchImportTypes(MethodParam methodParam) {
        return ResponseModel.ok(dataImportService.searchImportTypes(methodParam));
    }

}
