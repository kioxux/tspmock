package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomDiacardService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 备件报废
// */
//@RestController
//@RequestMapping("/bom_discard")
public class BomDiacardController {

//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private BomDiacardService bomDiacardService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_discard")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, Object> workType = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            workType = selectOptionService.getOptionByCode(schema_name, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_34);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("bom_diacard_add", "brAddFlag");
//                put("bom_diacard_invalid", "rpDeleteFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_discard/index", "bom_discard")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_discard/index");
//        }
//
//
//    }
//
//    /**
//     * 获取备件报废列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_discard")
//    @RequestMapping("/find_bom_discard_list")
//    public String findBomDiscardList(HttpServletRequest request) {
//        try {
//            return bomDiacardService.findBomDiacardList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_discard")
//    @RequestMapping("/findSingleBdInfo")
//    public ResponseModel findSingleBdInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = bomDiacardService.queryBomDiacardById(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 提交数据
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_diacard_add")
//    @RequestMapping("/doSubmitBomDiscard")
//    @Transactional //支持事务
//    public ResponseModel doSubmitBomDiacard(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return bomDiacardService.save(schema_name, processDefinitionId, paramMap, request, null);
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_discard")
//    @RequestMapping("/detail-bomDiscard")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return bomDiacardService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 报废单作废
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_diacard_invalid")
//    @RequestMapping("/cancel")
//    @Transactional
//    public ResponseModel cancel(HttpServletRequest request, HttpServletResponse response) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String discard_code = request.getParameter("discard_code");
//            if (StringUtils.isBlank(discard_code)) {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_NO_SYS_PAR);
//                return ResponseModel.errorMsg(tmpMsg);//没有获得系统参数 MSG_NO_SYS_PAR
//            }
//            return bomDiacardService.cancel(schemaName, loginUser, discard_code);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//
//    /**
//     * 报废审核提交
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_discard")
//    @RequestMapping("/bomDiscardAudit")
//    @Transactional
//    public ResponseModel inStockAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomDiacardService.bomDiscardAudit(schemaName, request, paramMap, loginUser, false);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
//
//    /**
//     * 报废最后审核
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_discard")
//    @RequestMapping("/lastBomDiscardAudit")
//    @Transactional
//    public ResponseModel lastBomAudit(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        User loginUser = authService.getLoginUser(request);
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomDiacardService.bomDiscardAudit(schemaName, request, paramMap, loginUser, true);
//        } catch (Exception ex) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_OPERATION_E);
//            return ResponseModel.errorMsg(tmpMsg);//操作出现异常 MSG_OPERATION_E
//        }
//    }
}

