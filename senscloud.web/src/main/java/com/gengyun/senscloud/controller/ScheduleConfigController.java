package com.gengyun.senscloud.controller;

//@RestController
public class ScheduleConfigController {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    ScheduleService scheduleService;
//
//    @Autowired
//    InitialJob initialJob;
//
//    @Autowired
//    DataSource mainDataSource;
//
//    @Autowired
//    SpringProcessEngineConfiguration springProcessEngineConfiguration;
//
//    @Autowired
//    TenantInfoHolder tenantInfoHolder;
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @MenuPermission("schedule_config")
//    @RequestMapping("/schedule_config")
//    public ModelAndView schedule(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("schedule_config");
//    }
//
//    @MenuPermission("schedule_config")
//    @RequestMapping("/save_schedule")
//    public ResponseModel saveSchedule(ScheduleData scheduleData, HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String user = null;
//        if (loginUser != null) {
//            user = loginUser.getAccount();
//        }
//        if (scheduleData == null || StringUtils.isEmpty(scheduleData.getShedule_name()) || StringUtils.isEmpty(scheduleData.getTask_class()) || StringUtils.isEmpty(scheduleData.getExpression())
//                || scheduleData.getTask_type() == null) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_TASK_CONTENT_NULL));
//        }
//        scheduleData.setCreate_user_account(user);
//        scheduleData.setCreatetime(new Timestamp(System.currentTimeMillis()));
//        scheduleData.setReschedule(scheduleData.getEnabled());
//        try {
//            if (scheduleData.getId() == null) {
//                scheduleData.setIs_running(false);
//                scheduleService.insert(schema_name, scheduleData);
//            } else {
//                scheduleService.update(schema_name, scheduleData);
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_TASK_EDIT_SU));
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_TASK_EDIT_FA));
//        }
//    }
//
//    @MenuPermission("schedule_config")
//    @RequestMapping("/delete_schedule")
//    public ResponseModel deleteSchedule(@RequestParam(name = "id", required = true) Integer id,
//                                        HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            scheduleService.delete(schema_name, id);
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_TASK_DEL_SU));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_TASK_DEL_FA));
//        }
//    }
//
//    @MenuPermission("schedule_config")
//    @RequestMapping("/schedule_config_list")
//    public ResponseModel getScheduleDataList(@RequestParam(name = "shedule_name", required = false) String shedule_name,
//                                             HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            List<ScheduleData> result = scheduleService.find(schema_name, null, shedule_name, null, null, null,null);
//            return ResponseModel.ok(result);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_TASK_DATA_FA));
//        }
//    }
//
//    @MenuPermission("schedule_config")
//    @RequestMapping("sync_schedule")
//    public ResponseModel syncSchedule(HttpServletRequest request, HttpServletResponse response) {
//        Company company = AuthService.getCompany(request);
//        try {
//            if (company != null && company.getStatus() == 1) {
//                String schema_name = company.getSchema_name();
//                initialJob.scheduleCompany(company);
//                CustomSpringProcessEngineConfiguration conf = (CustomSpringProcessEngineConfiguration) springProcessEngineConfiguration;
//                if (conf != null) {
//                    DataSource dataSource = conf.getDataSource();
//                    TenantAwareDataSource tenantAwareDataSource;
//                    if (dataSource instanceof TenantAwareDataSource) {
//                        tenantAwareDataSource = (TenantAwareDataSource) dataSource;
//                    } else {
//                        tenantAwareDataSource = (TenantAwareDataSource) (((TransactionAwareDataSourceProxy) dataSource).getTargetDataSource());
//                    }
//                    Set keys = tenantAwareDataSource.getDataSources().keySet();
//                    if (!keys.contains(schema_name)) {
//                        DruidDataSource druidDataSource = ((DruidDataSource) mainDataSource).cloneDruidDataSource();
//                        druidDataSource.setUrl(druidDataSource.getUrl() + "?characterEncoding=UTF-8&serverTimezone=GMT%2b8&useUnicode=true&currentSchema=" + schema_name);
//                        tenantInfoHolder.setCurrentTenantId(schema_name);
//                        conf.registerTenant(company.getSchema_name(), druidDataSource);
//                        tenantInfoHolder.clearCurrentTenantId();
//                    }
//                }
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_SYNCHRON_SUC));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_SYNCHRON_FA));
//       }
//    }
}
