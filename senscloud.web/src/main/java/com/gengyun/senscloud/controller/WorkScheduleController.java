package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.fitit100.util.tree.Node;
//import com.fitit100.util.tree.TreeBuilder;
//import com.fitit100.util.tree.TreeUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.DataPermissionAllOrSelf;
//import com.gengyun.senscloud.common.DataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.GroupModel;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.WorkScheduleData;
//import com.gengyun.senscloud.response.JsonResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.GroupService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.service.dynamic.WorkScheduleService;
//import net.sf.json.JSONObject;
//import org.apache.commons.lang3.time.DateFormatUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * Created by Administrator on 2018/4/27.
// */
//@RestController
//@RequestMapping("/workschedule")
public class WorkScheduleController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    WorkScheduleService workScheduleService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    GroupService groupService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    DataPermissionForFacility dataPermissionForFacility;
//
//    @MenuPermission("workschedule")
//    @RequestMapping({"/", "/workschedule"})
//    public ModelAndView selectStockData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        Boolean isAllFacility = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusinessFunctionKey(schema_name, user, "workschedule", "workplan_all_facility", "").equals(DataPermissionAllOrSelf.All_Facility);
//
//        //按用户权限，获取用户所能看见的用户组,此处配置的全部组织，指的是用户组织，不是设备组织
//        List<GroupModel> groupModelList = isAllFacility ? groupService.getGroup(schema_name) : groupService.getGroupListByUserAccount(schema_name, user.getAccount());
//
//        TreeBuilder treeBuilder = new TreeBuilder();
//        List<Node> allNodes = groupService.getGroupNodeList(groupModelList);
//        List<Node> roots = treeBuilder.buildListToTree(allNodes);
//
//        String groupSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots);
//
//        return new ModelAndView("workschedule/index")
//                .addObject("groupSelect", groupSelect);
//    }
//
//    //获取当前日历下的所有排班计划
//    @MenuPermission("workschedule")
//    @RequestMapping({"/get_work_schedule_list"})
//    public ResponseModel search(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        ResponseModel result = new ResponseModel();
//        List<WorkScheduleData> workScheduleDataList = null;
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String groupId = request.getParameter("group_id");
//            String beginTime = request.getParameter("start");
//            String endTime = request.getParameter("end");
//            String condition = " and b.begin_time >= '" + beginTime + "' and b.begin_time <= '" + endTime + "' ";
//
//            //不仅查自身，还查选择用户组织的下级位置
//            if (groupId != null && !groupId.isEmpty()) {
//                List<GroupModel> groupList = groupService.getGroup(schema_name);
//                String subGroupIds = groupService.getSubGroupIds(groupId, groupList);
//                condition += " and b.group_id in (" + groupId + subGroupIds + ")";
//                workScheduleDataList = workScheduleService.getWorkScheduleListByGroup(schema_name, condition);
//            }
//
////            User user = authService.getLoginUser(request);
////            Boolean isAllFacility = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusinessFunctionKey(schema_name, user, "workschedule", "workplan_all_facility", "").equals(DataPermissionAllOrSelf.All_Facility);
////            if (!isAllFacility) {
////                //按用户权限，获取用户所能看见的用户组,此处配置的全部组织，指的是用户组织，不是设备组织
////                List<GroupModel> groupModelList = groupService.getGroupListByUserAccount(schema_name, user.getAccount());
////                String groupIds = "";
////                if (groupModelList != null && !groupModelList.isEmpty()) {
////                    for (GroupModel groupItem : groupModelList) {
////                        groupIds += groupItem.getId() + ",";
////                    }
////                }
////
////                if (!groupIds.isEmpty() && !groupIds.equals("")) {
////                    groupIds = groupIds.substring(0, groupIds.length() - 1);
////                }
////
////                List<GroupModel> groupList = groupService.getGroup(schema_name);
////                String subGroupIds = groupService.getSubGroupIds(groupIds, groupList);
////                condition += " and b.group_id in (" + groupIds + subGroupIds + ")";
////            }
//
//        } catch (Exception e) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_FA));
//            return result;
//        }
//
//        result.setContent(workScheduleDataList);
//        result.setCode(1);
//        result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));
//        return result;
//    }
//
//    @MenuPermission("workschedule")
//    @RequestMapping({"/get_work_schedule_info_by_id"})
//    public ResponseModel userDetail(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            JSONObject result = new JSONObject();
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean isAllFacility = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusinessFunctionKey(schema_name, user, "workschedule", "workplan_all_facility", "").equals(DataPermissionAllOrSelf.All_Facility);
//
//            String scheduleId = request.getParameter("id");
//            WorkScheduleData workScheduleData = workScheduleService.findById(schema_name, Integer.valueOf(scheduleId));
//            if(workScheduleData == null)
//                return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//查询失败
//
//            //按用户权限，获取用户所能看见的用户组,此处配置的全部组织，指的是用户组织，不是设备组织
//            List<GroupModel> groupModelList = isAllFacility ? groupService.getGroup(schema_name) : groupService.getGroupListByUserAccount(schema_name, user.getAccount());
//
//            TreeBuilder treeBuilder = new TreeBuilder();
//            List<Node> allNodes = groupService.getGroupNodeList(groupModelList);
//            List<Node> roots = treeBuilder.buildListToTree(allNodes);
//
//            //默认查找
//            String facilityID = roots.get(0).getId();
//            List<User> userList = groupService.getGroupUserList(schema_name, facilityID);
//            Set<String> selectSet = new HashSet<String>();
//            if (workScheduleData != null) {
//                selectSet.add(String.valueOf(workScheduleData.getGroup_id()));
//            }
//            String facilitiesSelect = TreeUtil.getOptionListForSelectWithoutRoot(roots, selectSet);
//            TreeBuilder treeBuilder2 = new TreeBuilder();
//            List<Node> allNodes2 = new ArrayList<Node>();
//            for (User item : userList) {
//                Node node2 = new Node();
//                node2.setId(String.valueOf(item.getId()));
//                node2.setParentId(String.valueOf(""));
//                node2.setName(item.getUsername());
//                node2.setDetail(item);
//                allNodes2.add(node2);
//            }
//            List<Node> roots2 = treeBuilder2.buildListToTree(allNodes2);
//            String userList2 = TreeUtil.getOptionListForSelectWithoutRoot(roots2);
//            result.put("id", workScheduleData.getId());
//            result.put("user_account", workScheduleData.getUser_account());
//            result.put("group_id", workScheduleData.getGroup_id());
//            result.put("begin_time", DateFormatUtils.format(workScheduleData.getBegin_time(), Constants.DATE_FMT_SS));
//            result.put("end_time", DateFormatUtils.format(workScheduleData.getEnd_time(), Constants.DATE_FMT_SS));
//            result.put("remark", workScheduleData.getRemark());
//            result.put("create_user_account", workScheduleData.getCreate_user_account());
//            result.put("create_time", workScheduleData.getCreate_time());
//            result.put("username", workScheduleData.getUsername());
//            result.put("title", workScheduleData.getTitle());
//            result.put("facilitiesSelect", facilitiesSelect);
//            result.put("userList2", userList2);
//            return ResponseModel.ok(result);
//        } catch (Exception ex) {
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SEACH_DE));//查询失败
//        }
//    }
//
//    //获取单个日历控件中的信息
//    @MenuPermission("workschedule")
//    @RequestMapping({"/get_work_schedule_event_by_id"})
//    public ResponseModel getWorkScheduleListByFacilityById(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            int id = Integer.valueOf(request.getParameter("id"));
//
//            WorkScheduleData workScheduleDataList = workScheduleService.getWorkScheduleListByFacilityById(schema_name, id);
//            result.setContent(workScheduleDataList);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SUCC_A));
//            return result;
//        } catch (Exception e) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_FA));
//            return result;
//        }
//    }
//
//    @MenuPermission("workschedule")
//    @RequestMapping({"/update_work_schedule"})
//    public JsonResult updateWork(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String id = request.getParameter("id");
//            String user_account = request.getParameter("user_select2");
//            if (RegexUtil.isNull(user_account)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_EMP_NAME_NOT_NULL));
//            }
//            String group_id = request.getParameter("user_group_select2");
//            if (RegexUtil.isNull(group_id)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_USER_ORG_NULL));
//            }
//            String begin_day_time = request.getParameter("begin_day2");
//            if (RegexUtil.isNull(begin_day_time)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_WORK_TIME_NULL));
//            }
//            String remark = request.getParameter("remark2");
//            String begin_time = request.getParameter("begin_time2");
//            String end_time = request.getParameter("end_time2");
//            WorkScheduleData workScheduleData = new WorkScheduleData();
//            workScheduleData.setId(Integer.valueOf(id));
//            workScheduleData.setUser_account(user_account);
//            String newBegintime = begin_day_time + " " + begin_time + ":00";
//            String newEndtime = begin_day_time + " " + end_time + ":00";
//            workScheduleData.setGroup_id(Integer.valueOf(group_id));
//            workScheduleData.setBegin_time(Timestamp.valueOf(newBegintime));
//            workScheduleData.setEnd_time(Timestamp.valueOf(newEndtime));
//            if (remark != null) {
//                workScheduleData.setRemark(remark);
//            }
//            this.search(request, response);
//            int a = workScheduleService.updateWorlSchedule(schema_name, workScheduleData);
//            return new JsonResult().success(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_SUCC));
//
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_ERROR));
//        }
//    }
//
//    @MenuPermission("workschedule")
//    @RequestMapping({"/update_work_schedudle_date"})
//    public JsonResult updateDate(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String dayDelta = request.getParameter("dayDelta");
//            int day = Integer.valueOf(dayDelta);
//            String id = request.getParameter("id");
//            WorkScheduleData workScheduleData = new WorkScheduleData();
//            workScheduleData = workScheduleService.findById(schema_name, Integer.valueOf(id));
//            Date begin_time = workScheduleData.getBegin_time();
//            Date ent_time = workScheduleData.getEnd_time();
//            Calendar calendar = new GregorianCalendar();
//            Calendar calendar2 = new GregorianCalendar();
//            calendar.setTime(begin_time);
//            calendar.add(calendar.DATE, day);
//            calendar2.setTime(ent_time);
//            calendar2.add(calendar.DATE, day);
//            // calendar的time转成java.util.Date格式日期
//            java.util.Date utilDate = (java.util.Date) calendar.getTime();
//            java.util.Date utilDate2 = (java.util.Date) calendar.getTime();
//            utilDate = (java.util.Date) calendar.getTime();
//            utilDate2 = (java.util.Date) calendar2.getTime();
//            //java.util.Date日期转换成转成java.sql.Date格式
//            String begin_time2 = new SimpleDateFormat(Constants.DATE_FMT_SS).format(utilDate);
//            String end_time2 = new SimpleDateFormat(Constants.DATE_FMT_SS).format(utilDate2);
//            Timestamp begin_time3 = Timestamp.valueOf(begin_time2);
//            Timestamp end_time3 = Timestamp.valueOf(end_time2);
//            int id2 = Integer.valueOf(id);
//            this.search(request, response);
//            int a = workScheduleService.updateDate(schema_name, id2, begin_time3, end_time3);
//
//            return new JsonResult().success(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_SUCC));
//
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_ERROR));
//        }
//    }
//
//    @MenuPermission("workschedule")
//    @RequestMapping({"/delete_work_schedule"})
//    public JsonResult deleteWork(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String work_id = request.getParameter("id");
//            this.search(request, response);
//            workScheduleService.deleteWorkSchedule(schema_name, Integer.valueOf(work_id));
//            return new JsonResult().success(selectOptionService.getLanguageInfo( LangConstant.DELETED_SUCCESSFULLY));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));
//        }
//    }
//
//    @MenuPermission("workschedule")
//    @RequestMapping({"/add_work_schedule"})
//    public JsonResult addWork(HttpServletRequest request, HttpServletResponse response) {
//        User user = authService.getLoginUser(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//        WorkScheduleData workScheduleData = new WorkScheduleData();
//
//        try {
//            String user_account = request.getParameter("user_select");
//            if (RegexUtil.isNull(user_account)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_SUBMIT_DETAIL));
//            }
//            String group_id = request.getParameter("user_group_select");
//            if (RegexUtil.isNull(group_id)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_USER_ORG_NULL));
//            }
//            String begin_day = request.getParameter("begin_day");
//            if (RegexUtil.isNull(begin_day)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_START_TIME_NULL));
//            }
//            String end_day = request.getParameter("end_day");
//            if (RegexUtil.isNull(end_day)) {
//                return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_END_TIME_NULL));
//            }
//            String remark = request.getParameter("remark");
//            String begin_time = request.getParameter("begin_time");
//            String end_time = request.getParameter("end_time");
//            String create_user_account = user.getAccount();
//
//            DateFormat df = new SimpleDateFormat(Constants.DATE_FMT);;
//            Date beginDayTime = df.parse(begin_day);//将string转换成固定date
//            Date endDayTime = df.parse(end_day);
//
//            Calendar beginCalendar = Calendar.getInstance();
//            Calendar endCalendar = Calendar.getInstance();
//            beginCalendar.setTime(beginDayTime);
//            int day1 = beginCalendar.get(Calendar.DAY_OF_YEAR);
//
//            endCalendar.setTime(endDayTime);
//            int day2 = endCalendar.get(Calendar.DAY_OF_YEAR);
//            int days = day2 - day1;
//            for (int i = 0; i <= days; i++) {
//                if (i == 0) {
//                    beginCalendar.add(beginCalendar.DATE, i);
//                    int b = days;
//                    endCalendar.add(endCalendar.DATE, -b);
//                } else {
//                    beginCalendar.add(beginCalendar.DATE, 1);
//                    endCalendar.add(endCalendar.DATE, 1);
//                }
//                Date begin = df.parse(df.format(beginCalendar.getTime()));
//                Date end = df.parse(df.format(endCalendar.getTime()));
//                begin_day = df.format(begin);
//                end_day = df.format(end);
//                String newBegintime = begin_day + " " + begin_time + ":00";
//                String newEndtime = end_day + " " + end_time + ":00";
//                workScheduleData.setUser_account(user_account);
//                workScheduleData.setGroup_id(Integer.valueOf(group_id));
//                workScheduleData.setBegin_time(Timestamp.valueOf(newBegintime));
//                workScheduleData.setEnd_time(Timestamp.valueOf(newEndtime));
//                workScheduleData.setCreate_user_account(create_user_account);
//                if (remark != null) {
//                    workScheduleData.setRemark(remark);
//                }
//                //当前创建人
//                workScheduleData.setCreate_time(new Timestamp(new Date().getTime()));
//                workScheduleService.insertWorkSchedule(schema_name, workScheduleData);
//            }
//            return new JsonResult().success(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_SUCCESS));
//        } catch (Exception ex) {
//            return new JsonResult().error(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FAIL));
//        }
//    }
//
//    //通过用户组，获取用户
//    @MenuPermission("workschedule")
//    @RequestMapping({"/find_schedule_user"})
//    public ResponseModel findfac(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        String groupID = request.getParameter("group_id");
//        if (RegexUtil.isNull(groupID)) {
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_GET_DATA_FA));
//            return result;
//        }
//        List<User> userList = groupService.getGroupUserList(schema_name, groupID);
//        result.setContent(userList);
//        result.setCode(1);
//        result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DATA_AC_SUC));
//
//        return result;
//    }
}
