package com.gengyun.senscloud.controller;


import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.HomePageService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "首页接口管理")
@RestController
public class IndexController {
    @Resource
    HomePageService homePageService;

    /**
     * 首页的待办任务，今日上报，今日完成，超时任务数
     */
    @ApiOperation(value = " 首页的待办任务，今日上报，今日完成，超时任务数", notes = ResponseConstant.RSP_DESC_SEARCH_HOME_WORKS_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @Transactional
    @RequestMapping(value = "/searchHomeWorksCount", method = RequestMethod.POST)
    public ResponseModel searchHome(MethodParam methodParam) {
        return ResponseModel.ok(homePageService.getHomeWorksCount(methodParam));
    }

    @ApiOperation(value = "首页的日历待办任务数", notes = ResponseConstant.RSP_DESC_CALENDAR_WORKS_COUNT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "month"})
    @RequestMapping(value = "/searchCalendarWorksCount", method = RequestMethod.POST)
    public ResponseModel searchCalendarWorksCount(MethodParam methodParam, @ApiParam(value = "月份：yyyy-MM", required = true) @RequestParam String month) {
        return ResponseModel.ok(homePageService.getCalendarWorksCount(methodParam, month));
    }

    @ApiOperation(value = "首页显示的图表", notes = ResponseConstant.RSP_DESC_SEARCH_STATISTIC_SHOWDASHBOARD_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "month"})
    @RequestMapping(value = "/searchStatisticShowDashboardList", method = RequestMethod.POST)
    public ResponseModel searchStatisticList(MethodParam methodParam) {
        return ResponseModel.ok(homePageService.getStatisticList(methodParam));
    }

}
