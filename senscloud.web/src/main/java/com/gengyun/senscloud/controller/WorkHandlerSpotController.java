package com.gengyun.senscloud.controller;

//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SenscloudException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Map;
//
///**
// * 点检工单处理
// * yzj
// * 2018-12-12
// */
//
//
//@RestController
//@RequestMapping("/workHandlerSpot")
public class WorkHandlerSpotController {
//
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    WorkSheetHandleService workSheetHandleService;
//
//    @Autowired
//    WorkProcessService workProcessService;
//
//    @Autowired
//    WorkflowService workflowService;
//
//    @Autowired
//    WorkSheetHandleBusinessService workSheetHandleBusinessService;
//
//
//    //点检任务完成提交
//    @RequestMapping("/doFinishedSpot")
//    @Transactional
//    public ResponseModel finishedSpotcheck(@RequestParam(name = "task_id") String taskId,
//                                           @RequestParam Map<String, Object> paramMap,
//                                           HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            paramMap.put("pc_submit_right", "pc_submit_right");
//            return workSheetHandleBusinessService.finishedSpotcheck(schema_name, taskId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg("");
//        }
//    }
//
//
//    //点检任务确认
//    @RequestMapping("/doAuditSpot")
//    @Transactional
//    public ResponseModel auditSpotcheck(@RequestParam(name = "task_id") String taskId,
//                                        @RequestParam Map<String, Object> paramMap,
//                                        HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workSheetHandleBusinessService.auditSpotcheck(schema_name, taskId, paramMap, request);
//        } catch (SenscloudException e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg("");
//        }
//    }
}
