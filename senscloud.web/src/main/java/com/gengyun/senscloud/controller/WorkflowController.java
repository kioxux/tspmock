package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.WorkflowSearchParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Api(tags = "工作流")
@RestController
public class WorkflowController {
    @Resource
    WorkflowService workflowService;

    @ApiOperation(value = "获取工作流模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_WORKFLOW_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchWorkflowPermission", method = RequestMethod.POST)
    public ResponseModel searchWorkflowPermission(MethodParam methodParam) {
        return ResponseModel.ok(workflowService.getWorkflowPermission(methodParam));
    }

    @ApiOperation(value = "获取工作流部署列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKFLOW_DEPLOYMENT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchWorkflowDeploymentList", method = RequestMethod.POST)
    public ResponseModel searchWorkflowDeploymentList(MethodParam methodParam, @SuppressWarnings("unused") WorkflowSearchParam wfParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(workflowService.getWorkflowDeploymentList(methodParam, paramMap));
    }

    @ApiOperation(value = "删除工作流部署对象", notes = ResponseConstant.RSP_DESC_REMOVE_WORKFLOW_DEPLOYMENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/deleteWorkflowDeployment", method = RequestMethod.POST)
    @Transactional // 支持事务
    public ResponseModel removeWorkflowDeployment(MethodParam methodParam, @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        workflowService.cutWorkflowDeployment(methodParam, id);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取工作流定义列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKFLOW_DEFINITION_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "showLatest"})
    @RequestMapping(value = "/searchWorkflowDefinitionList", method = RequestMethod.POST)
    public ResponseModel searchWorkflowDefinitionList(MethodParam methodParam, @SuppressWarnings("unused") WorkflowSearchParam wfParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(workflowService.getWorkflowDefinitionList(methodParam, paramMap));
    }

    @ApiOperation(value = "获取工作流定义流程图", notes = ResponseConstant.RSP_DESC_SHOW_WORKFLOW_PROCESS_DIAGRAM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = {"/showWorkflowProcessDiagram", "searchWorkflowProcessDiagram"}, method = RequestMethod.GET)
    public void showWorkflowProcessDiagram(MethodParam methodParam, @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        workflowService.getWorkflowProcessDiagram(methodParam, id);
    }

    @ApiOperation(value = "获取工作流实例列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKFLOW_INSTANCE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch"})
    @RequestMapping(value = "/searchWorkflowInstanceList", method = RequestMethod.POST)
    public ResponseModel searchWorkflowInstanceList(MethodParam methodParam, @SuppressWarnings("unused") WorkflowSearchParam wfParam, @RequestParam Map<String, Object> paramMap) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(workflowService.getWorkflowInstanceList(methodParam, paramMap));
    }

    @ApiOperation(value = "删除工作流实例", notes = ResponseConstant.RSP_DESC_REMOVE_WORKFLOW_INSTANCE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/deleteWorkflowInstance", method = RequestMethod.POST)
    @Transactional // 支持事务
    public ResponseModel removeWorkflowInstance(MethodParam methodParam, @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        workflowService.cutWorkflowInstance(methodParam, id);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除流程名称之外的工作流实例", notes = ResponseConstant.RSP_DESC_REMOVE_WORKFLOW_INSTANCE_FILTER_FLOW_NAME)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/deleteWorkflowInstanceFilterFlowName", method = RequestMethod.POST)
    @Transactional // 支持事务
    public ResponseModel removeWorkflowInstanceFilterFlowName(MethodParam methodParam, @SuppressWarnings("unused") WorkflowSearchParam wfParam, @RequestParam Map<String, Object> paramMap) {
        workflowService.cutWorkflowInstanceFilterFlowName(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "根据子工单编号删除工作流实例", notes = ResponseConstant.RSP_DESC_REMOVE_WORKFLOW_INSTANCE_BY_SUB_WORK_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "subWorkCode"})
    @RequestMapping(value = "/deleteWorkflowInstanceBySubWorkCode", method = RequestMethod.POST)
    @Transactional // 支持事务
    public ResponseModel removeWorkflowInstanceBySubWorkCode(MethodParam methodParam,
                                                             @ApiParam(value = "子工单编号", required = true) @RequestParam String subWorkCode,
                                                             @ApiParam(value = "理由", required = false) @RequestParam(required = false) String reason) {
        workflowService.cutWorkflowInstanceBySubWorkCode(methodParam, subWorkCode, reason);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取工作流实例流程图", notes = ResponseConstant.RSP_DESC_SHOW_WORKFLOW_INSTANCE_DIAGRAM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = {"/showWorkflowInstanceDiagram"}, method = RequestMethod.GET)
    public void showWorkflowInstanceDiagram(MethodParam methodParam, @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        workflowService.getWorkflowInstanceDiagram(methodParam, id);
    }

    @ApiOperation(value = "获取工作流节点流程图", notes = ResponseConstant.RSP_DESC_SHOW_WORKFLOW_NODE_DIAGRAM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "flow_code", "id"})
    @RequestMapping(value = "/showWorkflowNodeDiagram", method = RequestMethod.GET)
    public void showWorkflowNodeDiagram(MethodParam methodParam, WorkflowSearchParam wfParam, @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        workflowService.getWorkflowNodeDiagram(methodParam, wfParam, id);
    }

    @ApiOperation(value = "获取工作流流程、节点信息列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORKFLOW_INST_DSP_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchWorkflowInstDspList", method = RequestMethod.POST)
    public ResponseModel searchWorkflowInstDspList(MethodParam methodParam) {
        return ResponseModel.ok(workflowService.getWorkflowInstDspList(methodParam));
    }

    @ApiOperation(value = "分配任务（根据流程实例主键taskId）", notes = ResponseConstant.RSP_DESC_ASSIGN_TASK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "taskId"})
    @RequestMapping(value = "/assign", method = RequestMethod.POST)
    public ResponseModel assign(MethodParam methodParam, @ApiParam(value = "流程实例ID", required = true) @RequestParam("taskId") String taskId, @RequestParam Map<String, String> paramMap) {
        return workflowService.assign(methodParam, taskId, paramMap);
    }

    @ApiOperation(value = "分配任务（根据工单编号subworkcode）", notes = ResponseConstant.RSP_DESC_ASSIGN_TASK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "subWorkCode"})
    @RequestMapping(value = "/assignSwc", method = RequestMethod.POST)
    public ResponseModel assignWithSWC(MethodParam methodParam, @ApiParam(value = "工单详情编号", required = true) @RequestParam("subWorkCode") String subWorkCode, @ApiParam(value = "用户id", required = true) @RequestParam("userId") String userId, @RequestParam Map<String, String> paramMap) {
        return workflowService.assignWithSWC(methodParam, subWorkCode, userId, paramMap);
    }

    @ApiOperation(value = "分配所有子任务", notes = ResponseConstant.RSP_DESC_PARENT_ASSIGN_TASK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "subWorkCodes[]"})
    @RequestMapping(value = "/parentAssign", method = RequestMethod.POST)
    public ResponseModel parentAssign(MethodParam methodParam, @ApiParam(value = "工单详情编号", required = true) @RequestParam("subWorkCodes[]") List<String> subWorkCodes, @ApiParam(value = "用户id", required = true) @RequestParam("userId") String userId, @RequestParam Map<String, String> paramMap) {
        return workflowService.parentAssign(methodParam, subWorkCodes, userId, paramMap);
    }

    @ApiOperation(value = "完成任务", notes = ResponseConstant.RSP_DESC_COMPLETE_TASK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "taskId"})
    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    public ResponseModel completeTask(MethodParam methodParam, @ApiParam(value = "流程实例ID", required = true) @RequestParam("taskId") String taskId, @RequestParam Map<String, String> paramMap) {
        return workflowService.complete(methodParam, taskId, paramMap);
    }

    @ApiOperation(value = "获取流程变量", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_VARIABLES)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "taskId", "name"})
    @RequestMapping(value = "/searchTaskVariables", method = RequestMethod.POST)
    public ResponseModel searchTaskVariables(MethodParam methodParam, @ApiParam(value = "流程实例ID", required = true) @RequestParam("taskId") String taskId, @ApiParam(value = "变量名称") @RequestParam(value = "name", required = false) String name) {
        if (StringUtils.isEmpty(name)) {
            return ResponseModel.ok(workflowService.getTaskVariables(methodParam, taskId));
        } else {
            return ResponseModel.ok(workflowService.getTaskVariableByName(methodParam, taskId, name));
        }
    }

    @ApiOperation(value = "获取待办任务列表模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getTasksPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetListPermission(MethodParam methodParam) {
        return ResponseModel.ok(workflowService.getTaskListPermission(methodParam));
    }

    @ApiOperation(value = "获取待办任务列表（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "mine_only", "count_only", "start_time", "end_time", "work_type_id", "pageSize", "pageNumber", "process_start_time", "process_end_time"})
    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public ResponseModel searchTaskList(MethodParam methodParam, WorkflowSearchParam wfParam) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(workflowService.searchTaskList(methodParam, wfParam));
    }

    @ApiOperation(value = "清空流程", notes = ResponseConstant.RSP_DESC_CLEAR_WORKFLOW)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id"})
    @RequestMapping(value = "/clearWorkflow", method = RequestMethod.POST)
    @Transactional // 支持事务
    public ResponseModel clearWorkflow(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        workflowService.clearWorkflow(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "导出流程配置", notes = ResponseConstant.RSP_DESC_GET_EXPORT_WORK_FLOW)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pref_id"})
    @RequestMapping(value = "/exportWorkFlow", method = RequestMethod.POST)
    public ModelAndView exportWorkFlow(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return workflowService.exportWorkFlow(methodParam, paramMap);
    }
}
