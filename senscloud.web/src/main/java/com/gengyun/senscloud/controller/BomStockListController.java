package com.gengyun.senscloud.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.auth.MenuPermissionRule;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomStockListService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import com.gengyun.senscloud.view.BomInventoryDataExportView;
//import net.sf.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 备件清单
// */
@RestController
@RequestMapping("/bom_stock_list")
public class BomStockListController {
//    @Autowired
//    PagePermissionService pagePermissionService;
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    private BomStockListService bomStockListService;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_stock_list")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("bom_stock_list_edit", "bslEditFlag");
//        }};
//        ModelAndView mav = pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_stock_list/index", "bom_stock_list");
//        String bomCode = request.getParameter("bomCode");
//        String stock_code = request.getParameter("stock_code");   // 应该根据库房来查看备件，yzj 2019-12-29 待实现
//        mav.addObject("searchKey", bomCode);
//        mav.addObject("stock_code", stock_code);
//        return mav;
//    }
//
//    /**
//     * 获取备件入库列表
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission(value = {"bom", "stock"}, rule = MenuPermissionRule.OR)
//    @RequestMapping("/findBslList")
//    public String findBslList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            User user = AuthService.getLoginUser(request);
//            int pageNumber = 1;
//            int pageSize = 15;
//            try {
//                String strPage = request.getParameter("pageNumber");
//                if (RegexUtil.isNumeric(strPage)) {
//                    pageNumber = Integer.valueOf(strPage);
//                }
//            } catch (Exception ex) {
//                pageNumber = 1;
//            }
//
//            try {
//                String strPageSize = request.getParameter("pageSize");
//                if (RegexUtil.isNumeric(strPageSize)) {
//                    pageSize = Integer.valueOf(strPageSize);
//                }
//            } catch (Exception ex) {
//                pageSize = 15;
//            }
//            return bomStockListService.findBslList(schemaName, request, user, pageNumber - 1, pageSize).toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 根据主键查询工单数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_stock_list")
//    @RequestMapping("/findSingleBslInfo")
//    public ResponseModel findSingleBslInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = bomStockListService.queryBslById(schemaName, subWorkCode);
//            //多时区处理
//            SCTimeZoneUtil.responseMapDataHandler(map);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 进入明细页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_stock_list")
//    @RequestMapping("/detail-bomStockList")
//    public ModelAndView goToDetail(HttpServletRequest request) {
//        try {
//            return bomStockListService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//    /**
//     * 导出备件
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_export")
//    @RequestMapping("/export-bomStockList")
//    public ModelAndView exportBomList(HttpServletRequest request) {
//        try {
//            String schemaName = AuthService.getCompany(request).getSchema_name();
//            User user = AuthService.getLoginUser(request);
//            JSONObject result= bomStockListService.findBslList(schemaName, request, user, 0, 0);
//            List<Map<String, Object>> dataList=(List<Map<String, Object>>)result.get("rows");
//            Map<String, Object> map = new HashMap<>();
//            map.put("bomList", dataList);
//            map.put("selectOptionService", selectOptionService);
//            BomInventoryDataExportView excelView = new BomInventoryDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//
//    /**
//     * 提交数据
//     *
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_stock_list_edit")
//    @RequestMapping("/doSubmitBomStockList")
//    @Transactional //支持事务
//    public ResponseModel saveBomStockList(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return bomStockListService.save(schema_name, processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_36));
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
}