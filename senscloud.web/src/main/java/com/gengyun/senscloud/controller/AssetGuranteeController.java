package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.SCTimeZoneUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.List;
//
//@RestController
public class AssetGuranteeController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    LogsService logService;
//    @Autowired
//    AssetGuaranteeService assetGuaranteeService;
//    @Autowired
//    AssetDataServiceV2 assetService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    /**
//     * 初始化设备保修
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_guarantee_init")
//    public ModelAndView InitAssetGuaranteeManage(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            result.setContent(null);
//            result.setCode(1);
//            result.setMsg("");
//            return new ModelAndView("AssetGuaranteeManage");
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_RE_ERR) + ex.getMessage());
//            return new ModelAndView("AssetGuaranteeManage").addObject("result", result);
//        }
//    }
//
//    /**
//     * 获得所有的设备保修
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_guarantee_list")
//    public ModelAndView FindALLAssetGuaranteeList(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String asset_id = request.getParameter("asset_id");
//
//            List<AssetGuaranteeData> dataList = assetGuaranteeService.findAllAssetGuaranteeList(schema_name, asset_id);
//            //多时区处理
//            SCTimeZoneUtil.responseObjectListDataHandler(dataList);
//            result.setContent(dataList);
//            result.setCode(1);
//            result.setMsg("");
//            return new ModelAndView("AssetGuaranteeList")
//                    .addObject("result", result)
//                    .addObject("guarantee_lang", selectOptionService.getLanguageInfo(LangConstant.GUARANTEE))
//                    .addObject("contract_lang", selectOptionService.getLanguageInfo(LangConstant.CONTRACT));
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_RE_ERR) + ex.getMessage());
//            return new ModelAndView("AssetGuaranteeList").addObject("result", result);
//        }
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_guarantee_edit")
//    public ResponseModel getEditAssetGuarantee(HttpServletRequest request, HttpServletResponse response) {
//        //获取设备保修信息
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("edit_id"));
//        if (id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_NO_R_DATA));
//        } else {
//            String schema_name = authService.getCompany(request).getSchema_name();
//
//            //获取设备保修
//            AssetGuaranteeData data = assetGuaranteeService.findAssetGuarantee(schema_name, id);
//            result.setContent(data);
//            result.setCode(1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_GET_MAIN_SU));
//        }
//        //返回结果
//        return result;
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_guarantee_save")
//    public ResponseModel getAddAssetGuarantee(HttpServletRequest request, HttpServletResponse response) {
//        //保存设备保修
//        ResponseModel result = new ResponseModel();
//        try {
//            String asset_id = request.getParameter("asset_id");
//            Integer id = Integer.parseInt(request.getParameter("edit_id"));
//            String serial_no = request.getParameter("serial_no");
//            String buytime = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("buy_date");
//            String expireddate = SCTimeZoneUtil.requestParamHandlerWithMinSuffix("expired_date");
//            String contract_type = request.getParameter("contract_type");
//            String company_id = request.getParameter("company_id");
//            Integer ccontract_type_int = 1;
//            Integer company_id_int = 0;
//            if (null != contract_type && !contract_type.isEmpty()) {
//                ccontract_type_int = Integer.parseInt(contract_type);
//            }
//            if (null != company_id && !company_id.isEmpty()) {
//                company_id_int = Integer.parseInt(company_id);
//            }
//
//            if (RegexUtil.isNull(asset_id) || RegexUtil.isNull(serial_no) || RegexUtil.isNull(buytime) || RegexUtil.isNull(expireddate)) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CERT_DATA));
//            } else {
//                DateFormat format1 = new SimpleDateFormat(Constants.DATE_FMT);;
//                java.util.Date buy_time = format1.parse(buytime);
//                java.util.Date expired_date = format1.parse(expireddate);
//                Company company = authService.getCompany(request);
//                String schema_name = company.getSchema_name();
//                User user = authService.getLoginUser(request);
//                String checkResult = assetService.checkAssetList(request, user, schema_name, asset_id);
//                if (RegexUtil.isNull(checkResult)) {
//                    result.setCode(-1);
//                    String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                    result.setMsg(tmpMsg);
//                    return result;
//                }
//
//                //看看是否有重复，如果有重复，则执行更新操作,
//                AssetGuaranteeData list = assetGuaranteeService.findAssetGuarantee(schema_name, id);
//                //保存保修信息
//                int count = 0;
//                if (list == null || list.getSerialNO() == null) {
//                    count = assetGuaranteeService.AddAssetGuarantee(schema_name, asset_id, buy_time, expired_date, serial_no, ccontract_type_int, company_id_int, user.getAccount());
//                } else {
//                    count = assetGuaranteeService.UpdateAssetGuarantee(schema_name, buy_time, expired_date, serial_no, ccontract_type_int, company_id_int, id);
//                }
//                if (count > 0) {
//                    //增加日志
//                    logService.AddLog(schema_name, "asset", asset_id.toString(), selectOptionService.getLanguageInfo(LangConstant.MSG_ADD_RM_CODE) + serial_no, user.getAccount());
//                    result.setCode(1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SU_RM_SU));
//                } else {
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SU_RM_DE));
//                }
//            }
//            //返回保存结果
//            return result;
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_SU_RM_DE));
//            return result;
//        }
//    }
//
//    @MenuPermission("asset_data_management")
//    @RequestMapping("/asset_guarantee_delete")
//    public ResponseModel getDeleteAssetGuarantee(HttpServletRequest request, HttpServletResponse response) {
//        //删除资产关联企业
//        ResponseModel result = new ResponseModel();
//        Integer id = Integer.parseInt(request.getParameter("id"));
//        if (id <= 0) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_RM_CH));
//        } else {
//            Company company = authService.getCompany(request);
//            String schema_name = company.getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            //删除资产维保人员
//            AssetGuaranteeData data = assetGuaranteeService.findAssetGuarantee(schema_name, id);
//
//            if (null == data || RegexUtil.isNull(data.getAssetId())) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ASS_NOT));
//                return result;
//            }
//            String checkResult = assetService.checkAssetList(request, user, schema_name, data.getAssetId());
//            if (RegexUtil.isNull(checkResult)) {
//                result.setCode(-1);
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                result.setMsg(tmpMsg);
//                return result;
//            }
//
//            int count = assetGuaranteeService.DeleteAssetGuarantee(schema_name, id, false);
//            if (count > 0) {
//                //增加日志
//                logService.AddLog(schema_name, "asset", data.getAssetId().toString(), selectOptionService.getLanguageInfo(LangConstant.MSG_RE_RM_CODE) + data.getSerialNO(), user.getAccount());
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_RM_SU));
//            } else {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_DEL_RM_DE));
//            }
//        }
//        //返回保存结果
//        return result;
//    }
}
