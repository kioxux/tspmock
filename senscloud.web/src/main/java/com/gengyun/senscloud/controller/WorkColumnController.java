package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.WorkColumnService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "字段库")
@RestController
public class WorkColumnController {

    @Resource
    WorkColumnService workColumnService;

    @ApiOperation(value = "获取字段库列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber",
            "keywordSearch","work_template_code","field_view_types"//, "typeIdsSearch", "suppliersSearch", "isUseSearch", "startCountSearch", "endCountSearch"
    })
    @RequestMapping(value = "/searchWorkColumnList", method = RequestMethod.POST)
    public ResponseModel searchWorkColumnList(MethodParam methodParam, WorkColumnSearchParam param) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(workColumnService.getWorkColumnListForPage(methodParam, param));
    }

    @ApiOperation(value = "获取字段主键", notes = ResponseConstant.RSP_DESC_GET_WORK_COLUMN_KEY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchWorkColumnKey", method = RequestMethod.POST)
    public ResponseModel searchWorkColumnKey(MethodParam methodParam ) {
        return ResponseModel.ok(workColumnService.getWorkColumnKey());
    }

    @ApiOperation(value = "新增字段", notes = ResponseConstant.RSP_DESC_ADD_WORK_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token","data_key","field_value", "field_form_code", "field_code", "field_name", "field_right", "is_required", "field_remark", "field_view_type", "field_section_type", "extList", "field_name", "field_write_type", "field_value"})
    @Transactional //支持事务
    @RequestMapping(value = "/addWorkColumn", method = RequestMethod.POST)
    public ResponseModel addWorkColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkColumnAdd workColumnAdd ) {
        workColumnService.newWorkColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取字段明细信息", notes = ResponseConstant.RSP_DESC_GET_WORK_COLUMN_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "field_form_code"})
    @RequestMapping(value = "/searchWorkColumnInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkColumnInfo(MethodParam methodParam, WorkColumnSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String field_form_code) {
        return ResponseModel.ok(workColumnService.getWorkColumnInfoByFieldFormCode(methodParam, bParam));
    }

    @ApiOperation(value = "编辑字段", notes = ResponseConstant.RSP_DESC_EDIT_WORK_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnInfo"},includeParameters = {"token","data_key","field_value", "field_form_code", "field_code", "field_name", "field_right", "is_required", "field_remark", "field_view_type", "field_section_type", "extList", "field_name", "field_write_type", "field_value"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkColumn", method = RequestMethod.POST)
    public ResponseModel editWorkColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkColumnAdd workColumnInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String field_form_code) {
        workColumnService.modifyWorkColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除选中字段", notes = ResponseConstant.RSP_DESC_REMOVE_WORK_COLUMN_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "field_form_code"})
    @RequestMapping(value = "/removeSelectWorkColumn", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectWorkColumn(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String field_form_code) {
        workColumnService.cutWorkColumnByFieldFormCode(methodParam,field_form_code);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "克隆字段", notes = ResponseConstant.RSP_DESC_GET_WORK_COLUMN_CLONE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "field_form_code"})
    @RequestMapping(value = "/searchWorkColumnClone", method = RequestMethod.POST)
    public ResponseModel searchWorkColumnClone(MethodParam methodParam, WorkColumnSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String field_form_code) {
        return ResponseModel.ok(workColumnService.getWorkColumnCloneByFieldFormCode(methodParam, bParam));
    }

//    @ApiOperation(value = "根据控件类型获取特殊属性列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_COLUMN_LIST)
//    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "column_type"})
//    @RequestMapping(value = "/searchSpecialPropertyList", method = RequestMethod.POST)
//    public ResponseModel searchSpecialPropertyList(MethodParam methodParam, @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String column_type) {
//        return ResponseModel.ok(workColumnService.getSpecialPropertyList(methodParam, column_type));
//    }
}
