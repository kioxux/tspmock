package com.gengyun.senscloud.controller.guacamole;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/guacamole")
public class GuacamoleController {

    @RequestMapping({"/", "/index"})
    public ModelAndView InitRepairTimesPolar(HttpServletRequest request) {
        return new ModelAndView("guacamole/test");
    }
}
