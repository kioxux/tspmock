package com.gengyun.senscloud.controller;

//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.TaskReleaseService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.util.Constants;
//import com.gengyun.senscloud.util.SenscloudException;
//import net.sf.json.JSONArray;
//import org.apache.ibatis.annotations.Param;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @Author: Wudang Dong
// * @Description:
// * @Date: Create in 下午 2:33 2019/9/17 0017
// */
//@RestController
//@RequestMapping("/task_release")
public class TaskReleaseController {

//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    TaskReleaseService taskReleaseService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @MenuPermission("task_release")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("task_release_all_facility", "allGroup");
//            put("task_release_self_facility", "selfGroup");
//            put("task_release_add", "agAddFlag");
//            put("task_release_edit", "agEditFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "task_release/index", "task_release");
//    }
//
//
//    /**
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     * @Description:任务发布列表接口方法
//     */
//    @MenuPermission("task_release")
//    @RequestMapping("/find-task-release-list")
//    public String queryArrangementList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                       @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                       @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                       @RequestParam(name = "sortName", required = false) String sortName,
//                                       @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                       @Param("groups") String groups,
//                                       HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String searchKeyTrim = searchKey.trim();
//            JSONObject pageResult = taskReleaseService.query(schemaName, searchKeyTrim, pageSize, pageNumber, sortName, sortOrder, groups);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @MenuPermission("task_release")
//    @RequestMapping("/taskReleaseDetailInfo")
//    public ResponseModel queryArrangementList(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = taskReleaseService.queryTaskReleaseById(schemaName, subWorkCode);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 获取用户组角色列表
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/getGroupRoleListByTask"})
//    public ResponseModel getGroupRoleListByTask(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String id = request.getParameter("subWorkCode");
//            if (RegexUtil.isNull(id)) {
//                return ResponseModel.ok(null);
//            } else {
//                String idPrefix = Constants.SPECIAL_ID_PREFIX.get("taskReleasePublish");
//                id = id.substring(idPrefix.length());
//                List<Map<String, Object>> list = taskReleaseService.getGroupRoleListByTask(schemaName, Integer.valueOf(id));
//                return ResponseModel.ok(JSONArray.fromObject(list).toString());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * @param processDefinitionId
//     * @param paramMap
//     * @param request
//     * @return
//     * @Description:任务发布
//     */
//    @MenuPermission("task_release")
//    @RequestMapping("/doSubmitTaskRelease")
//    @Transactional //支持事务
//    public ResponseModel doSubmitTaskRelease(
//            @RequestParam(name = "flow_id") String processDefinitionId,
//            @RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return taskReleaseService.save(schema_name, processDefinitionId, paramMap, request, SensConstant.LOG_TYPE_INFO.get(SensConstant.BUSINESS_NO_41));
//        } catch (SenscloudException e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return ResponseModel.error(e.getMessage());
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.error(tmpMsg);//失败
//        }
//    }
//
//    //启动、禁用
//    @MenuPermission("task_release")
//    @RequestMapping("/openOrCloseTaskRelease")
//    @Transactional //支持事务
//    public ResponseModel doSubmitEditTaskRelease(
//            @RequestParam(name = "id") String id,
//            @RequestParam(name = "create_user_account") String create_user_account,
//            @RequestParam(name = "title") String title,
//            @RequestParam(name = "status") boolean status,
//            HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            if (RegexUtil.isNull(id)) {
//                return ResponseModel.ok(null);
//            } else {
//                boolean submit = taskReleaseService.doSubmitTaskRelease(schema_name, id, !status,create_user_account,title);
//                String mes = status == true ? selectOptionService.getLanguageInfo(LangConstant.MSG_DISABLE_SU) : selectOptionService.getLanguageInfo(LangConstant.MSG_DIS_DE);//禁用成功 禁用失败
//                return ResponseModel.ok(mes);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    @RequestMapping("/testFunction")
//    @Transactional
//    public void test(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            taskReleaseService.productTaskRelease(schema_name);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @MenuPermission("task_release_analys")
//    @RequestMapping("/analys")
//    public ModelAndView analys_index(HttpServletRequest request) {
//        try {
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("task_release_analys_export", "rpExportFlag");
//                put("task_release_analys_all_facility", "allGroup");
//                put("task_release_analys_self_facility", "selfGroup");
//            }};
//
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "task_release/analys", "task_release_analys");
//        }catch (Exception e){
//            return new ModelAndView("task_release/analys");
//        }
//    }
//
//    @MenuPermission("task_release_analys")
//    @RequestMapping("/find_work_task_analys")
//    public String findWorkTaskAnalys(HttpServletRequest request) {
//        try {
//            return taskReleaseService.findWorkTaskAnalys(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @MenuPermission("task_release_analys")
//    @RequestMapping("/work_task_analys_export")
//    public ModelAndView workTaskAnalysExport(HttpServletRequest request) {
//        try {
//            return taskReleaseService.workTaskAnalysExport(request);
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    /**
//     * 查看工作任务详情信息
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find_work_task_analys_detail")
//    public String findWorkTaskAnalysDetail(HttpServletRequest request) {
//        try {
//            return taskReleaseService.findWorkTaskAnalysDetail(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }

}
