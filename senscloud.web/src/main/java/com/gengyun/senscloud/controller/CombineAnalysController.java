package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.DataPermissionAllOrSelf;
//import com.gengyun.senscloud.common.IDataPermissionForFacility;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.IFacility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.response.AnalysFacilityRepairMaintainResult;
//import com.gengyun.senscloud.response.AnalysFacilityResult;
//import com.gengyun.senscloud.response.AnalysSupplierResult;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.response.enumResp.EnumListAndCountResult;
//import com.gengyun.senscloud.service.AnalysReportService;
//import com.gengyun.senscloud.service.FacilitiesService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import com.gengyun.senscloud.view.*;
//import com.github.pagehelper.Page;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/combine_analys")
public class CombineAnalysController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    AnalysReportService analysReportService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    IDataPermissionForFacility dataPermissionForFacility;
//
//    //位置统计
//    @RequestMapping("/facility_analys")
//    public ModelAndView initFacilityAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "facility_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("facility_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("facility_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/facility_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/facility_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    //按位置统计数据进行统计
//    @RequestMapping("/facility_analys_list")
//    public String doFacilityAnalysList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String repairCondition = "";
//            String maintainCondition = "";
//            String cidCondition = "";
//            String categoryCondition = "";
//
//            if (StringUtils.isNotEmpty(beginTime)) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and w.occur_time >='" + beginDate + "'";
//                maintainCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (StringUtils.isNotEmpty(endTime)) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and w.occur_time <'" + end + "'";
//                maintainCondition += " and w.occur_time <'" + end + "'";
//            }
//            User user = authService.getLoginUser(request);
//            // 获取设备组织的NO
//            cidCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//            int begin = pageSize * (pageNumber - 1);
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//            //时效结果
//            List<AnalysFacilityResult> facilityReportList = analysReportService.getFacilityReport(schema_name, repairCondition, maintainCondition, cidCondition, categoryCondition, pageSize, begin);
//            int total = analysReportService.getFacilityReportCount(schema_name, repairCondition, maintainCondition, cidCondition, categoryCondition);
//
//            result.put("rows", facilityReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //按位置统计数据进行数据统计导出
//    @RequestMapping("/facility_analys_export")
//    public ModelAndView ExportFacilitAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String facilityId = request.getParameter("facilityId");
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//
//            String repairCondition = "";
//            String maintainCondition = "";
//            String cidCondition = "";
//            String categoryCondition = "";
//
//            if (StringUtils.isNotEmpty(beginTime)) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and w.occur_time >='" + beginDate + "'";
//                maintainCondition += " and w.create_time >='" + beginDate + "'";
//            }
//            if (StringUtils.isNotEmpty(endTime)) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and w.occur_time <'" + end + "'";
//                maintainCondition += " and w.create_time <'" + end + "'";
//            }
//            User user = authService.getLoginUser(request);
//            // 获取设备组织的NO
//            cidCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//            List<AnalysFacilityResult> facilityReportList = analysReportService.getFacilityReport(schema_name, repairCondition, maintainCondition, cidCondition, categoryCondition, 10000, 0);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("facilityAnalys", facilityReportList);
//            map.put("selectOptionService", selectOptionService);
//            FacilityCenterDataExportView excelView = new FacilityCenterDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //按位置，获取维修、保养统计数据视图
//    @RequestMapping("/facility_repair_maintain_analys")
//    public ModelAndView initFacilityRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "facility_repair_maintain_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("facility_repair_maintain_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("facility_repair_maintain_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/facility_repair_maintain_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/facility_repair_maintain_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    //按位置、供应商，获取维修、保养统计数据进行统计
//    @RequestMapping("/facility_repair_maintain_analys_list")
//    public String doAnalysRepairMaintainList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            User user = authService.getLoginUser(request);
//
//            //时效结果
//            EnumMap<EnumListAndCountResult, Object> findResult = getFacilityRepairMaintainList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//            result.put("rows", findResult.get(EnumListAndCountResult.RESULTLIST));
//            result.put("total", findResult.get(EnumListAndCountResult.RESULTTOTALCOUNT));
//
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //按位置、供应商，获取维修、保养统计数据统计导出
//    @RequestMapping("/facility_repair_maintain_analys_export")
//    public ModelAndView exportFacilityRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            int pageSize = 50000;
//            int pageNumber = 1;
//            User user = authService.getLoginUser(request);
//
//            EnumMap<EnumListAndCountResult, Object> result = getFacilityRepairMaintainList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairAnalys", result.get(EnumListAndCountResult.RESULTLIST));
//            map.put("analysType", analysType);
//            map.put("selectOptionService", selectOptionService);
//            FacilitySupplierRepairMaintianDataExportView excelView = new FacilitySupplierRepairMaintianDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //获取维修、保养统计数据
//    private EnumMap<EnumListAndCountResult, Object> getFacilityRepairMaintainList(String schema_name, String analysType, String facilityId, String categoryIds, String beginTime, String endTime, int pageSize, int pageNumber, User user) {
//        String dateCondition = "";
//        String facilityTypeCondition = " and f.org_type<3 ";
//        String facilityCondition = "";
//        String categoryCondition = "";
//
////        switch (analysType) {
////            case "facility":
////                facilityTypeCondition = " and f.org_type<3 ";
////                break;
////            case "supplier":
////                facilityTypeCondition = " and f.org_type=4 ";
////                break;
////        }
//
//        if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//            Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//            dateCondition += " and w.occur_time >='" + beginDate + "'";
//        }
//        if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//            Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//            dateCondition += " and w.occur_time <'" + end + "'";
//        }
//
//        // 获取设备组织的NO
//        facilityCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//        int begin = pageSize * (pageNumber - 1);
//
//        //查询设备类型，如果包括全部，则不判断
//        categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//        EnumMap<EnumListAndCountResult, Object> result = new EnumMap<EnumListAndCountResult, Object>(EnumListAndCountResult.class);
//        //时效结果
//        List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetFacilityRepairMaintainReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//
//        result.put(EnumListAndCountResult.RESULTLIST, repairMaintainReportList);
//        if (pageSize < 50000) {
//            int total = analysReportService.GetFacilityRepairMaintainReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//            result.put(EnumListAndCountResult.RESULTTOTALCOUNT, total);
//        }
//
//        return result;
//    }
//
//
//    //按位置，获取维修类型统计数据视图
//    @RequestMapping("/facility_repair_type_analys")
//    public ModelAndView initFacilityRepairTypeAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "facility_repair_type_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("facility_repair_type_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("facility_repair_type_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/facility_repair_type_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/facility_repair_type_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    //统计维修类型列表，按位置或供应商
//    @RequestMapping("/facility_repair_type_analys_list")
//    public String analysRepairType(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String endTime = request.getParameter("endTime");
//            String beginTime = request.getParameter("beginTime");
//            User user = authService.getLoginUser(request);
//
//            //时效结果
//            EnumMap<EnumListAndCountResult, Object> findResult = getFacilityRepairTypeList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//            //int total = analysReportService.GetFacilityAndSupplierRepairTypeReportCount(schema_name, deviceCondition, repairCondition, groupColumn);
//            result.put("rows", findResult.get(EnumListAndCountResult.RESULTLIST));
//            result.put("total", findResult.get(EnumListAndCountResult.RESULTTOTALCOUNT));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //维修类型统计导出
//    @RequestMapping("/facility_repair_type_analys_export")
//    public ModelAndView ExportRepairTypeAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            int pageSize = 50000;
//            int pageNumber = 1;
//            User user = authService.getLoginUser(request);
//            EnumMap<EnumListAndCountResult, Object> result = getFacilityRepairTypeList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairAnalys", result.get(EnumListAndCountResult.RESULTLIST));
//            map.put("analysType", analysType);
//            map.put("selectOptionService", selectOptionService);
//            //map.put("name", "魅力城市");
//            FacilitySupplierRepairTypeDataExportView excelView = new FacilitySupplierRepairTypeDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //按分析类型，位置，日期，获取位置、或供应商的维修和保养数据
//    private EnumMap<EnumListAndCountResult, Object> getFacilityRepairTypeList(String schema_name, String analysType, String facilityId, String categoryIds, String beginTime, String endTime, int pageSize, int pageNumber, User user) {
//        String facilityTypeCondition = " and f.org_type<3 ";
//        String facilityCondition = "";
//        String categoryCondition = "";
//        String dateCondition = "";
//
////        switch (analysType) {
////            case "facility":
////                facilityTypeCondition = " and f.org_type<3 ";
////                break;
////            case "supplier":
////                facilityTypeCondition = " and f.org_type=4 ";
////                break;
////        }
//
//        if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//            Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//            dateCondition += " and w.occur_time >='" + beginDate + "'";
//        }
//        if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//            Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//            dateCondition += " and w.occur_time <'" + end + "'";
//        }
//
//        // 获取设备组织的NO
//        facilityCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//        int begin = pageSize * (pageNumber - 1);
//
//        //查询设备类型，如果包括全部，则不判断
//        categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//        EnumMap<EnumListAndCountResult, Object> result = new EnumMap<EnumListAndCountResult, Object>(EnumListAndCountResult.class);
//        //时效结果
//        List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetFacilityRepairTypeReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//        result.put(EnumListAndCountResult.RESULTLIST, repairMaintainReportList);
//        if (pageSize < 50000) {
//            int total = analysReportService.GetFacilityRepairTypeReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//            result.put(EnumListAndCountResult.RESULTTOTALCOUNT, total);
//        }
//
//        return result;
//    }
//
//
//    //按位置、供应商，获取维修类型统计数据视图
//    @RequestMapping("/device_repair_maintain_analys")
//    public ModelAndView InitDeviceRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "device_repair_maintain_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("device_repair_maintain_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("device_repair_maintain_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/device_repair_maintain_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/device_repair_maintain_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    //统计维修类型列表，按位置或供应商
//    @RequestMapping("/device_repair_maintain_analys_list")
//    public String analysDeviceRepairMaintain(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String keyWord = request.getParameter("key");
//            String facilityId = request.getParameter("facilityId");
//            String endTime = request.getParameter("endTime");
//            String beginTime = request.getParameter("beginTime");
//            String asset_category_ids = request.getParameter("asset_category_ids"); // 设备类型ids
//
//            User user = authService.getLoginUser(request);
//
//            String facilityCondition = "";
//            String deviceCondition = " and (dv.strname like '%" + keyWord + "%' or act.category_name like '%" + keyWord + "%') ";
//            String dateCondition = "";
//            String categoryCondition = "";
//            // 获取设备组织的NO
//            facilityCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//            int begin = pageSize * (pageNumber - 1);
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(asset_category_ids);
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                dateCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                dateCondition += " and w.occur_time <'" + end + "'";
//            }
//
//            //时效结果
//            List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetDeviceRepairMaintainReport(schema_name, deviceCondition, dateCondition, facilityCondition, categoryCondition, pageSize, begin);
//            int total = analysReportService.GetDeviceRepairMaintainReportCount(schema_name, deviceCondition, dateCondition, facilityCondition, categoryCondition);
//
//            result.put("rows", repairMaintainReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    //导出
//    @RequestMapping("/device_repair_maintain_analys_export")
//    public ModelAndView ExportDeviceRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String keyWord = request.getParameter("key");
//            String facilityId = request.getParameter("facilityId");
//            String endTime = request.getParameter("endTime");
//            String beginTime = request.getParameter("beginTime");
//            String searchAssetType = request.getParameter("asset_category_ids"); // 设备类型ids
//
//            User user = authService.getLoginUser(request);
//
//            String facilityCondition = "";
//            String deviceCondition = " and (dv.strname like '%" + keyWord + "%' or act.category_name like '%" + keyWord + "%') ";
//            String dateCondition = "";
//            String categoryCondition = "";
//            // 获取设备组织的NO
//            facilityCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(searchAssetType);
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                dateCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                dateCondition += " and w.occur_time <'" + end + "'";
//            }
//            int pageSize = 50000;
//            int pageNumber = 1;
//
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetDeviceRepairMaintainReport(schema_name, deviceCondition, dateCondition, facilityCondition, categoryCondition, pageSize, begin);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairMaintainAnalys", repairMaintainReportList);
//            map.put("selectOptionService", selectOptionService);
//            //map.put("name", "魅力城市");
//            DeviceRepairMaintainDataExportView excelView = new DeviceRepairMaintainDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//
//    //按个人，统计设备维护、保养时效和总数和时间视图
//    @RequestMapping("/operation_repair_maintain_analys")
//    public ModelAndView InitOperationRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "operation_repair_maintain_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("operation_repair_maintain_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("operation_repair_maintain_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/operation_repair_maintain_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/operation_repair_maintain_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//    //按个人，统计设备维护、保养时效和总数和时间
//    @RequestMapping("/operation_repair_maintain_analys_list")
//    public String analysOperationRepairMaintain(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            User user = authService.getLoginUser(request);
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String keyWord = request.getParameter("key");
//            String groupIds = request.getParameter("groupIds");
//            String endTime = request.getParameter("endTime");
//            String beginTime = request.getParameter("beginTime");
//            String asset_category_ids = request.getParameter("asset_category_ids"); // 设备类型ids
//
//            String condition = " and (u.account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//            String groupCondition = "";
//            String dateCondition = "";
//            String categoryCondition = "";
//
//            // 获取用户组的NO
//            groupCondition = generateGroupCodeCondition(groupIds, schema_name, user, "operation_repair_maintain_analys", "operation_repair_maintain_analys_all_facility", "operation_repair_maintain_analys_self_facility");
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(asset_category_ids);
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                dateCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                dateCondition += " and w.occur_time <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetUserRepairMaintainReport(schema_name, condition, groupCondition, dateCondition, categoryCondition, pageSize, begin);
//            int total = analysReportService.GetUserRepairMaintainReportCount(schema_name, condition, groupCondition, dateCondition, categoryCondition);
//
//            result.put("rows", repairMaintainReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//
//    //导出
//    @RequestMapping("/operation_repair_maintain_analys_export")
//    public ModelAndView ExportOperationRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            User user = authService.getLoginUser(request);
//            String keyWord = request.getParameter("keyWord");
//            String groupIds = request.getParameter("groupIds");
//            String endTime = request.getParameter("endTime");
//            String beginTime = request.getParameter("beginTime");
//            String searchAssetType = request.getParameter("asset_category_ids"); // 设备类型ids
//            int pageSize = 50000;
//            int pageNumber = 1;
//
//            String condition = " and (u.account like '%" + keyWord + "%' or u.username like '%" + keyWord + "%' or u.user_code like '%" + keyWord + "%') ";
//            String groupCondition = "";
//            String dateCondition = "";
//            String categoryCondition = "";
//
//            // 获取用户组的NO
//            groupCondition = generateGroupCodeCondition(groupIds, schema_name, user, "operation_repair_maintain_analys", "operation_repair_maintain_analys_all_facility", "operation_repair_maintain_analys_self_facility");
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(searchAssetType);
//
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                dateCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                dateCondition += " and w.occur_time <'" + end + "'";
//            }
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetUserRepairMaintainReport(schema_name, condition, groupCondition, dateCondition, categoryCondition, pageSize, begin);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairMaintainAnalys", repairMaintainReportList);
//            map.put("selectOptionService", selectOptionService);
//            //map.put("name", "魅力城市");
//            OperationRepairMaintainDataExportView excelView = new OperationRepairMaintainDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //供应商统计
//    @RequestMapping("/supplier_analys")
//    public ModelAndView initSupplierAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllSupplier = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "supplier_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("supplier_analys_all_facility")) {
//                        canSeeAllSupplier = true;
//                    } else if (functionData.getFunctionName().equals("supplier_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllSupplier && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/supplier_analys").addObject("canSeeAllSupplier", canSeeAllSupplier).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/supplier_analys").addObject("canSeeAllSupplier", false).addObject("canExportData", false);
//        }
//    }
//
//    //按位置统计数据进行统计
//    @RequestMapping("/supplier_analys_list")
//    public String doSupplierAnalysList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String supplierId = request.getParameter("supplierId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String repairCondition = "";
//            String categoryCondition = "";
//            String sidCondition = "";
//
//            if (StringUtils.isNotEmpty(beginTime)) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (StringUtils.isNotEmpty(endTime)) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and w.occur_time <'" + end + "'";
//            }
//
//            if (!(supplierId == null || supplierId.isEmpty() || supplierId.equals("0"))) {
//                sidCondition += " and f.id in (" + supplierId + ") ";
//            }
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//            int begin = pageSize * (pageNumber - 1);
//            //时效结果
//            List<AnalysSupplierResult> facilityReportList = analysReportService.GetSupplierReport(schema_name, repairCondition, sidCondition, categoryCondition, pageSize, begin);
//            int total = analysReportService.GetSupplierReportCount(schema_name, repairCondition, sidCondition, categoryCondition);
//
//            result.put("rows", facilityReportList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //按供应商统计数据进行数据统计导出
//    @RequestMapping("/supplier_analys_export")
//    public ModelAndView ExportSupplierAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String supplierId = request.getParameter("supplierId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//
//            String repairCondition = "";
//            String categoryCondition = "";
//            String sidCondition = "";
//
//            if (StringUtils.isNotEmpty(beginTime)) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                repairCondition += " and w.occur_time >='" + beginDate + "'";
//            }
//            if (StringUtils.isNotEmpty(endTime)) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                repairCondition += " and w.occur_time <'" + end + "'";
//            }
//
//            if (!(supplierId == null || supplierId.isEmpty() || supplierId.equals("0"))) {
//                sidCondition += " and f.id in (" + supplierId + ") ";
//            }
//
//            //查询设备类型，如果包括全部，则不判断
//            categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//            List<AnalysSupplierResult> supplierReportList = analysReportService.GetSupplierReport(schema_name, repairCondition, sidCondition, categoryCondition, null, null);
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("supplierAnalys", supplierReportList);
//            map.put("selectOptionService", selectOptionService);
//            SupplierDataExportView excelView = new SupplierDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //按供应商，获取维修、保养统计数据视图
//    @RequestMapping("/supplier_repair_maintain_analys")
//    public ModelAndView initSupplierRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "supplier_repair_maintain_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("supplier_repair_maintain_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("supplier_repair_maintain_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/supplier_repair_maintain_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/supplier_repair_maintain_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//
//    //按位置、供应商，获取维修、保养统计数据进行统计
//    @RequestMapping("/supplier_repair_maintain_analys_list")
//    public String doAnalysSupplierRepairMaintainList(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            User user = authService.getLoginUser(request);
//
//            //时效结果
//            EnumMap<EnumListAndCountResult, Object> findResult = getSupplierRepairMaintainList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//            result.put("rows", findResult.get(EnumListAndCountResult.RESULTLIST));
//            result.put("total", findResult.get(EnumListAndCountResult.RESULTTOTALCOUNT));
//
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //按位置、供应商，获取维修、保养统计数据统计导出
//    @RequestMapping("/supplier_repair_maintain_analys_export")
//    public ModelAndView exportSupplierRepairMaintainAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            int pageSize = 50000;
//            int pageNumber = 1;
//            User user = authService.getLoginUser(request);
//
//            EnumMap<EnumListAndCountResult, Object> result = getSupplierRepairMaintainList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairAnalys", result.get(EnumListAndCountResult.RESULTLIST));
//            map.put("analysType", analysType);
//            map.put("selectOptionService", selectOptionService);
//            FacilitySupplierRepairMaintianDataExportView excelView = new FacilitySupplierRepairMaintianDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //获取维修、保养统计数据
//    private EnumMap<EnumListAndCountResult, Object> getSupplierRepairMaintainList(String schema_name, String analysType, String facilityId, String categoryIds, String beginTime, String endTime, int pageSize, int pageNumber, User user) {
//        String dateCondition = "";
//        String facilityTypeCondition = " and f.org_type=4 ";
//        String facilityCondition = "";
//        String categoryCondition = "";
//
////        switch (analysType) {
////            case "facility":
////                facilityTypeCondition = " and f.org_type<3 ";
////                break;
////            case "supplier":
////                facilityTypeCondition = " and f.org_type=4 ";
////                break;
////        }
//
//        if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//            Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//            dateCondition += " and w.occur_time >='" + beginDate + "'";
//        }
//        if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//            Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//            dateCondition += " and w.occur_time <'" + end + "'";
//        }
//
//        // 获取设备组织的NO
//        facilityCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//        int begin = pageSize * (pageNumber - 1);
//
//        //查询设备类型，如果包括全部，则不判断
//        categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//        EnumMap<EnumListAndCountResult, Object> result = new EnumMap<EnumListAndCountResult, Object>(EnumListAndCountResult.class);
//        //时效结果
//        List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetSupplierRepairMaintainReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//
//        result.put(EnumListAndCountResult.RESULTLIST, repairMaintainReportList);
//        if (pageSize < 50000) {
//            int total = analysReportService.GetSupplierRepairMaintainReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//            result.put(EnumListAndCountResult.RESULTTOTALCOUNT, total);
//        }
//
//        return result;
//    }
//
//
//    //按供应商，获取维修类型统计数据视图
//    @RequestMapping("/supplier_repair_type_analys")
//    public ModelAndView initSupplierRepairTypeAnalys(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//            Boolean canSeeAllFacility = false;
//            Boolean canExportData = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "supplier_repair_type_analys");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("supplier_repair_type_analys_all_facility")) {
//                        canSeeAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("supplier_repair_type_analys_export")) {
//                        canExportData = true;
//                    }
//                    if (canSeeAllFacility && canExportData) {
//                        break;
//                    }
//                }
//            }
//            return new ModelAndView("combine_analys/supplier_repair_type_analys").addObject("canSeeAllFacility", canSeeAllFacility).addObject("canExportData", canExportData);
//        } catch (Exception ex) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_STATI_ERR) + ex.getMessage());
//            return new ModelAndView("combine_analys/supplier_repair_type_analys").addObject("canSeeAllFacility", false).addObject("canExportData", false);
//        }
//    }
//
//
//    //统计维修类型列表，按位置或供应商
//    @RequestMapping("/supplier_repair_type_analys_list")
//    public String analysSupplierRepairType(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            JSONObject result = new JSONObject();
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String endTime = request.getParameter("endTime");
//            String beginTime = request.getParameter("beginTime");
//            User user = authService.getLoginUser(request);
//
//            //时效结果
//            EnumMap<EnumListAndCountResult, Object> findResult = getSupplierRepairTypeList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//            //int total = analysReportService.GetFacilityAndSupplierRepairTypeReportCount(schema_name, deviceCondition, repairCondition, groupColumn);
//            result.put("rows", findResult.get(EnumListAndCountResult.RESULTLIST));
//            result.put("total", findResult.get(EnumListAndCountResult.RESULTTOTALCOUNT));
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //维修类型统计导出
//    @RequestMapping("/supplier_repair_type_analys_export")
//    public ModelAndView ExportSupplierRepairTypeAnalys(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String analysType = request.getParameter("analysType");
//            String facilityId = request.getParameter("facilityId");
//            String categoryIds = request.getParameter("asset_category_ids"); // 设备类型ids
//            String beginTime = request.getParameter("beginTime");
//            String endTime = request.getParameter("endTime");
//            int pageSize = 50000;
//            int pageNumber = 1;
//            User user = authService.getLoginUser(request);
//            EnumMap<EnumListAndCountResult, Object> result = getSupplierRepairTypeList(schema_name, analysType, facilityId, categoryIds, beginTime, endTime, pageSize, pageNumber, user);
//
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("repairAnalys", result.get(EnumListAndCountResult.RESULTLIST));
//            map.put("analysType", analysType);
//            map.put("selectOptionService", selectOptionService);
//            //map.put("name", "魅力城市");
//            FacilitySupplierRepairTypeDataExportView excelView = new FacilitySupplierRepairTypeDataExportView();
//            return new ModelAndView(excelView, map);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    //按分析类型，位置，日期，获取位置、或供应商的维修和保养数据
//    private EnumMap<EnumListAndCountResult, Object> getSupplierRepairTypeList(String schema_name, String analysType, String facilityId, String categoryIds, String beginTime, String endTime, int pageSize, int pageNumber, User user) {
//        String facilityTypeCondition = " and f.org_type=4 ";
//        String facilityCondition = "";
//        String categoryCondition = "";
//        String dateCondition = "";
//
////        switch (analysType) {
////            case "facility":
////                facilityTypeCondition = " and f.org_type<3 ";
////                break;
////            case "supplier":
////                facilityTypeCondition = " and f.org_type=4 ";
////                break;
////        }
//
//        if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//            Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//            dateCondition += " and w.occur_time >='" + beginDate + "'";
//        }
//        if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//            Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//            dateCondition += " and w.occur_time <'" + end + "'";
//        }
//
//        // 获取设备组织的NO
//        facilityCondition = generateFacilityNoCondition(schema_name, user, facilityId);
//        int begin = pageSize * (pageNumber - 1);
//
//        //查询设备类型，如果包括全部，则不判断
//        categoryCondition = generateAssetCategoryCodeCondition(categoryIds);
//
//        EnumMap<EnumListAndCountResult, Object> result = new EnumMap<EnumListAndCountResult, Object>(EnumListAndCountResult.class);
//        //时效结果
//        List<AnalysFacilityRepairMaintainResult> repairMaintainReportList = analysReportService.GetSupplierRepairTypeReport(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition, pageSize, begin);
//        result.put(EnumListAndCountResult.RESULTLIST, repairMaintainReportList);
//        if (pageSize < 50000) {
//            int total = analysReportService.GetSupplierRepairTypeReportCount(schema_name, dateCondition, facilityTypeCondition, facilityCondition, categoryCondition);
//            result.put(EnumListAndCountResult.RESULTTOTALCOUNT, total);
//        }
//
//        return result;
//    }
//
//
//    //位置统计视图
//    @RequestMapping("/analys_facility")
//    public ModelAndView InitAnalysFacility(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            return new ModelAndView("combine_analys/analys_facility");
//        } catch (Exception ex) {
//            return new ModelAndView("combine_analys/analys_facility");
//        }
//    }
//
//
//    //位置统计视图
//    @RequestMapping("/analys_device")
//    public ModelAndView InitAnalysDevice(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            return new ModelAndView("combine_analys/analys_device");
//        } catch (Exception ex) {
//            return new ModelAndView("combine_analys/analys_device");
//        }
//    }
//
//
//    //位置统计视图
//    @RequestMapping("/analys_supplier")
//    public ModelAndView InitAnalysSupplier(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            return new ModelAndView("combine_analys/analys_supplier");
//        } catch (Exception ex) {
//            return new ModelAndView("combine_analys/analys_supplier");
//        }
//    }
//
//
//    //位置统计视图
//    @RequestMapping("/analys_user")
//    public ModelAndView InitAnalysUser(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        try {
//            return new ModelAndView("combine_analys/analys_user");
//        } catch (Exception ex) {
//            return new ModelAndView("combine_analys/analys_user");
//        }
//    }
//
//    // 返回按场地查询的组织
//    private String generateFacilityNoCondition(String schema_name, User loginUser, String facilityId) {
//        String facilityNoCondition = "";
//        List<String> facilities = new ArrayList<>();
//        if (null != facilityId && !facilityId.isEmpty()) {
//            Facility facility = facilitiesService.FacilitiesById(schema_name, Integer.parseInt(facilityId));
//            if (null != facility) {
//                facilities.add(facility.getFacilityNo());
//            }
//        }
//
//        // 查找可以看到的所有分拨中心
//        //获取组织的根节点数据，用于like进行比较
//        String[] facilityNos = dataPermissionForFacility.findFacilityNOsByBusinessForSingalUser(schema_name, loginUser, facilities.toArray(new String[0]), "asset_monitor");
//        if (facilityNos != null && facilityNos.length > 0) {
//            facilityNoCondition += " and (";
//            for (String facilityNo : facilityNos) {
//                facilityNoCondition += " f.facility_no like '" + facilityNo + "%' or";
//            }
//            facilityNoCondition = facilityNoCondition.substring(0, facilityNoCondition.length() - 2);
//            facilityNoCondition += ") ";
//        }
//        return facilityNoCondition;
//    }
//
//    // 返回按设备类型查询的code
//    private String generateAssetCategoryCodeCondition(String categoryIds) {
//        String categoryCondition = "";
//        //查询设备类型，如果包括全部，则不判断
//        if (StringUtils.isNotEmpty(categoryIds)) {
//            String[] categoryCodeList = categoryIds.split(",");
//            if (null != categoryCodeList && categoryCodeList.length > 0) {
//                categoryCondition += " and (";
//                for (String categoryCode : categoryCodeList) {
//                    categoryCondition += " act.category_code like '" + categoryCode + "%' or";
//                }
//                categoryCondition = categoryCondition.substring(0, categoryCondition.length() - 2);
//                categoryCondition += ") ";
//            }
//        }
//        return categoryCondition;
//    }
//
//    // 返回按用户组查询的code
//    private String generateGroupCodeCondition(String groupIds, String schema, User currentUser, String businessKey, String businessAllFunctionKey, String businessSelfFunctionKey) {
//        String groupCondition = "";
//        //查询用户组，如果包括全部，则不判断
//        if (StringUtils.isNotEmpty(groupIds) && !"0".equals(groupIds)) {
//            String[] groupCodeList = groupIds.split(",");
//            if (null != groupCodeList && groupCodeList.length > 0) {
//                groupCondition += " and (";
//                for (String groupCode : groupCodeList) {
//                    groupCondition += " g.id = " + groupCode + " or";
//                }
//                groupCondition = groupCondition.substring(0, groupCondition.length() - 2);
//                groupCondition += ") ";
//            }
//        } else {
//            DataPermissionAllOrSelf dataPermissionAllOrSelf = dataPermissionForFacility.isHaveAllOrSelfFacilityByBusinessFunctionKey(schema, currentUser, businessKey, businessAllFunctionKey, businessSelfFunctionKey);
//            if (dataPermissionAllOrSelf != DataPermissionAllOrSelf.All_Facility) {
//                if (StringUtils.isNotEmpty(currentUser.getGroupIdsString())) {
//                    groupCondition = " and g.id in (" + currentUser.getGroupIdsString() + ") ";
//                }
//            }
//        }
//        return groupCondition;
//    }
//
}
