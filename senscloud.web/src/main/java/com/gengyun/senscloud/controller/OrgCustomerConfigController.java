package com.gengyun.senscloud.controller;

//import com.alibaba.fastjson.JSON;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SqlConstant;
//import com.gengyun.senscloud.common.SystemConfigConstant;
//import com.gengyun.senscloud.model.Facility;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
public class OrgCustomerConfigController {
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    LogsService logService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @RequestMapping("/org_customer")
//    public ModelAndView FacilitiesConfig(HttpServletRequest request, HttpServletResponse response) {
//        Boolean isCustomer = true;//是否厂商
//        Boolean organizationFlag = false;//是否从地址管理中获取位置信息
//        String schema_name = authService.getCompany(request).getSchema_name();
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ADDRESS_TYPE);//系统配置中读取系统配置类型
//        if (null != dataList && "2".equals(dataList.getSettingValue())) {
//            organizationFlag = true;
//        }
//        return new ModelAndView("organize_facilities/facilities_config")
//                .addObject("isCustomer", isCustomer)
//                .addObject("organizationFlag", organizationFlag);
////                .addObject("currency_code_name", systemConfigService.getCurrencyName(schema_name))
//
//    }
//
//    /**
//     * 按条件查询组织列表
//     */
//    @RequestMapping("/find-org_customer-list")
//    public ResponseModel queryworksheet(HttpServletRequest request, HttpServletResponse response) {
//        String condition = "and a.org_type in(" + SqlConstant.FACILITY_OUTSIDE + ") ";
//        return facilitiesService.FacilitiesListforTable(request, response, condition);
//
//    }
//
//    /**
//     * 添加组织界面
//     */
//
//    @RequestMapping("/add_org_customer_view")
//    public ModelAndView AddFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        Boolean isCustomer = true;//是否厂商
//        Boolean organizationFlag = false;//是否从地址管理中获取位置信息
//        String schema_name = authService.getCompany(request).getSchema_name();
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ADDRESS_TYPE);//系统配置中读取系统配置类型
//        if (null != dataList && "2".equals(dataList.getSettingValue())) {
//            organizationFlag = true;
//        }
//        return new ModelAndView("organize_facilities/add_facilities_config").addObject("isCustomer", isCustomer).addObject("organizationFlag", organizationFlag);
//    }
//
//    /**
//     * 组织详情
//     */
//
//    @RequestMapping("/detail_org_customer_view")
//    public ModelAndView DetailFacilitiesData(HttpServletRequest request, HttpServletResponse response) {
//        Boolean isCustomer = true;//是否厂商
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        Boolean organizationFlag = false;//是否从地址管理中获取位置信息
//        SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, SystemConfigConstant.ADDRESS_TYPE);//系统配置中读取系统配置类型
//        if (null != dataList && "2".equals(dataList.getSettingValue())) {
//            organizationFlag = true;
//        }
//        Facility facility_data = facilitiesService.FacilitiesById(schema_name, Integer.parseInt(id));
//        User loginUser = AuthService.getLoginUser(request);
//        boolean feeFacilityRight = pagePermissionService.getPermissionByKey(schema_name, loginUser.getId(), "facilities", "facilities_fee");
//        Map<String, Boolean> pagePermissionInfo = new HashMap<String, Boolean>();
//        pagePermissionInfo.put("feeFcyRight", feeFacilityRight);
//        pagePermissionInfo.put("orgCostTag", feeFacilityRight);
//        return new ModelAndView("organize_facilities/detail_facilities_config")
//                .addObject("pagePermissionInfo", net.sf.json.JSONObject.fromObject(pagePermissionInfo))
//                .addObject("facility_data", JSON.toJSON(facility_data))
//                .addObject("isCustomer", isCustomer).addObject("organizationFlag", organizationFlag);
////                .addObject("currency_code_name", systemConfigService.getCurrencyName(schema_name))
//    }
//
//
//    //按权限查询所属场地,供工单列表、设备列表、查询、统计分析等功能使用
//
//    /**
//     * 获得所有的位置
//     */
//    @RequestMapping("find_all_facility_by_cloud_permission")
//    public ResponseModel queryFacilityByUserPermission(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            String businessType = request.getParameter("businessType");
//
//            Boolean isAllFacility = userService.isAllUserFunctionPermission(schema_name, user.getId(), businessType);
//            //按用户权限，获取用户所能看见的位置
//            List<Facility> facilitiesList = facilitiesService.GetFacilityCenterListByUser(schema_name, isAllFacility, user);
//            return ResponseModel.ok(facilitiesList);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_ERR_LOC);
//            return ResponseModel.error(tmpMsg + ex.getMessage());
//        }
//    }
}