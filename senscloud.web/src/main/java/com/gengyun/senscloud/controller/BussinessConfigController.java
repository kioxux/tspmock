package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.BussinessConfigModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.BussinessConfigService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 *
 */
@Api(tags = "业务配置")
@RestController
public class BussinessConfigController {
    @Resource
    BussinessConfigService bussinessConfigService;

    @ApiOperation(value = "新增系统维护配置", notes = ResponseConstant.RSP_DESC_ADD_SYSTEM_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "systemConfigType", "type_name", "data_order", "business_type_id", "is_use"
            , "running_status", "level_name", "type_code", "parent_id", "unit_name", "finished_name", "is_finished", "currency_name", "currency_code"})
    @Transactional
    @RequestMapping(value = "/addSystemConfig", method = RequestMethod.POST)
    public ResponseModel addSystemConfig(MethodParam methodParam, @RequestParam Map<String, Object> pm) {
        bussinessConfigService.newSystemConfig(methodParam, pm);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询系统维护配置列表", notes = ResponseConstant.RSP_DESC_SEARCH_SYSTEM_CONFIG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "systemConfigType"})
    @RequestMapping(value = "/searchSystemConfigList", method = RequestMethod.POST)
    public ResponseModel searchSystemConfigList(MethodParam methodParam, BussinessConfigModel bussinessConfigModel) {
        return ResponseModel.ok(bussinessConfigService.getSystemConfigList(methodParam, bussinessConfigModel));
    }

    @ApiOperation(value = "编辑系统维护配置", notes = ResponseConstant.RSP_DESC_EDIT_SYSTEM_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "systemConfigType", "type_name", "data_order", "business_type_id", "is_use"
            , "running_status", "level_name", "type_code", "parent_id", "unit_name", "finished_name", "is_finished", "currency_name", "currency_code"})
    @Transactional
    @RequestMapping(value = "/editSystemConfig", method = RequestMethod.POST)
    public ResponseModel editSystemConfig(MethodParam methodParam, @RequestParam Map<String, Object> pm) {
        bussinessConfigService.modifySystemConfig(methodParam, pm);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除系统维护配置列表", notes = ResponseConstant.RSP_DESC_REMOVE_SYSTEM_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "systemConfigType"})
    @Transactional
    @RequestMapping(value = "/removeSystemConfig", method = RequestMethod.POST)
    public ResponseModel removeSystemConfig(MethodParam methodParam, BussinessConfigModel bussinessConfigModel) {
        bussinessConfigService.cutSystemConfigById(methodParam, bussinessConfigModel);
        return ResponseModel.okMsgForOperate();
    }
    @ApiOperation(value = "查询系统维护配置字段", notes = ResponseConstant.RSP_DESC_SEARCH_SYSTEM_CONFIG_FIELD)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "systemConfigType"})
    @RequestMapping(value = "/searchSystemConfigField", method = RequestMethod.POST)
    public ResponseModel searchSystemConfigField(MethodParam methodParam, BussinessConfigModel bussinessConfigModel) {
        return ResponseModel.ok(bussinessConfigService.getSystemConfigField(methodParam, bussinessConfigModel));
    }
}
