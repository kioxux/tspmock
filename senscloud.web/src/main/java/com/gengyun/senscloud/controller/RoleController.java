package com.gengyun.senscloud.controller;


import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.RoleModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.RoleService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "角色管理")
@RestController
public class RoleController {
    @Resource
    RoleService roleService;

    @ApiOperation(value = "获取角色模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchRoleListPermission", method = RequestMethod.POST)
    public ResponseModel searchRoleListPermission(MethodParam methodParam) {
        return ResponseModel.ok(roleService.getRoleListPermission(methodParam));
    }

    @ApiOperation(value = "新增角色", notes = ResponseConstant.RSP_DESC_ADD_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "name", "description", "sys_role"})
    @Transactional //支持事务
    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    public ResponseModel addRole(MethodParam methodParam, RoleModel roleModel) {
        roleService.newRole(methodParam, roleModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除角色", notes = ResponseConstant.RSP_DESC_REMOVE_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeRoleById", method = RequestMethod.POST)
    public ResponseModel removeRoleById(MethodParam methodParam, RoleModel roleModel) {
        roleService.cutRoleById(methodParam, roleModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新角色信息", notes = ResponseConstant.RSP_DESC_EDIT_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "name", "description", "sys_role"})
    @Transactional //支持事务
    @RequestMapping(value = "/editRoleById", method = RequestMethod.POST)
    public ResponseModel editRoleById(MethodParam methodParam, RoleModel roleModel) {
        roleService.modifyRoleById(methodParam, roleModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新角色数据权限信息", notes = ResponseConstant.RSP_DESC_EDIT_DATA_PERMISSION_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "assetPositionIds", "assetCategoryIds", "workTypeRoleIds", "newWorkRoleIds"})
    @Transactional //支持事务
    @RequestMapping(value = "/editDataPermissionRole", method = RequestMethod.POST)
    public ResponseModel editDataPermissionRole(MethodParam methodParam, RoleModel roleModel) {
        roleService.modifyDataPermissionRoleById(methodParam, roleModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新角色功能权限信息", notes = ResponseConstant.RSP_DESC_EDIT_PERMISSION_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "permissionIds"})
    @Transactional //支持事务
    @RequestMapping(value = "/editPermissionRole", method = RequestMethod.POST)
    public ResponseModel editPermissionRoleById(MethodParam methodParam, RoleModel roleModel) {
        roleService.modifyPermissionRoleById(methodParam, roleModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询角色列表", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "pageSize", "pageNumber"})
    @RequestMapping(value = "/searchRoleList", method = RequestMethod.POST)
    public ResponseModel searchRoleList(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRoleList(methodParam, roleModel));
    }

    @ApiOperation(value = "查询角色详情", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRoleInfo", method = RequestMethod.POST)
    public ResponseModel searchRoleInfo(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRoleInfo(methodParam, roleModel));
    }

    @ApiOperation(value = "查询角色功能权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRolePermission", method = RequestMethod.POST)
    public ResponseModel searchRolePermission(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRolePermission(methodParam, roleModel));
    }

    @ApiOperation(value = "查询角色设备位置数据权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_ASSET_POSITION_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRoleAssetPositionPermission", method = RequestMethod.POST)
    public ResponseModel searchRoleAssetPositionPermission(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRoleAssetPositionPermission(methodParam, roleModel));
    }

    @ApiOperation(value = "查询角色设备类型数据权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_ASSET_TYPE_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRoleAssetTypePermission", method = RequestMethod.POST)
    public ResponseModel searchRoleAssetTypePermission(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRoleAssetTypePermission(methodParam, roleModel));
    }

    @ApiOperation(value = "查询角色工单类型数据权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_WORK_TYPE_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRoleWorkTypePermission", method = RequestMethod.POST)
    public ResponseModel searchRoleWorkTypePermission(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRoleWorkTypePermission(methodParam, roleModel));
    }

    @ApiOperation(value = "获取创建申请单数据权限", notes = ResponseConstant.RSP_DESC_SEARCH_NEW_WORK_TYPE_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchNewWorkTypePermission", method = RequestMethod.POST)
    public ResponseModel searchNewWorkTypePermission(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getNewWorkTypePermission(methodParam, roleModel));
    }

    @ApiOperation(value = "查询角色库房数据权限", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_STOCK_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchRoleStockPermission", method = RequestMethod.POST)
    public ResponseModel searchRoleStockPermission(MethodParam methodParam, RoleModel roleModel) {
        return ResponseModel.ok(roleService.getRoleStockPermission(methodParam, roleModel));
    }

    @ApiOperation(value = "克隆角色", notes = ResponseConstant.RSP_DESC_SEARCH_ROLE_CLONE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/cloneRole", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel cloneRole(MethodParam methodParam, RoleModel roleModel) {
        roleService.cloneRole(methodParam, roleModel);
        return ResponseModel.okMsgForOperate();
    }
}
