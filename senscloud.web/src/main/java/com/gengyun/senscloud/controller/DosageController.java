package com.gengyun.senscloud.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.mapper.BatchReportMapper;
//import com.gengyun.senscloud.service.*;
//import net.sf.json.JSONArray;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 批报详情
// * User: zys
// * Date: 2019/11/28
// * Time: 上午11:20
// */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dosage")
public class DosageController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AddressManageService addressManageService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    BatchReportMapper batchReportMapper;
//
//    @Autowired
//    ResolvingMetaService resolvingMetaService;
//
//    @Autowired
//    DosageService dosageService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("monitor_counter")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("address_add", "rpAddFlag");
//            put("address_edit", "rpEditFlag");
//            put("address_delete", "rpDeleteFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "monitor_count/asset_dosage/asset_dosage_detail", "monitor");
//    }
//
//    /**
//     *进入设备批报详情
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("monitor_counter")
//    @RequestMapping("/find_asset_dosage")
//    public ModelAndView queryAssetDosage(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String asset_code=request.getParameter("asset_code");
//            Map<String,Object> Asset=batchReportMapper.findVmiAssetBydAssetCode(schemaName,asset_code);
//
//            String category_id=String.valueOf(Asset.get("category_id"));
//            Map<String, Object> metaMonitor = (Map) resolvingMetaService.monitorMeta(schemaName).get(category_id);
//            if (null == metaMonitor || metaMonitor.isEmpty()) {
//                return pagePermissionService.getUserFunctionPermissionForPage(request,  new HashMap<String, String>(),
//                        "monitor_count/asset_dosage/asset_dosage_detail", "monitor");
//            }
//            List<String> dosageList = (List) metaMonitor.get("dosageList");
//            Map<String,Object> nameValueMap=(Map)metaMonitor.get("nameValueMap");
//            List<String> countList = (List) metaMonitor.get("countList");
//            List<String> newCountList=new ArrayList<>();
//            for(String count:countList){
//                if(dosageList.contains(count)){
//                    newCountList.add(count);
//                }
//            }
//            newCountList.add("report_times");
//            return pagePermissionService.getUserFunctionPermissionForPage(request,  new HashMap<String, String>(),
//                    "monitor_count/asset_dosage/asset_dosage_detail", "monitor").addObject("assetData",JSON.toJSON(Asset))
//                    .addObject("reportColumns", JSONArray.fromObject(dosageList)).addObject("reportTimeColumns",JSONArray.fromObject(newCountList)).addObject("nameValueMap",  JSON.toJSON(nameValueMap));
//        } catch (Exception ex) {
//            return pagePermissionService.getUserFunctionPermissionForPage(request,  new HashMap<String, String>(),
//                    "monitor_count/asset_dosage/asset_dosage_detail", "monitor");
//        }
//    }
//
//    /**
//     * 查询批报数据
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_use")
//    @RequestMapping("/find_report_data")
//    @Transactional //支持事务
//    public String FindReportData( HttpServletRequest request,HttpSession session) {
//        try {
//            return dosageService.getReportData(request,session);
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 按时间查询报告
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_use")
//    @RequestMapping("/find_report_time_data")
//    @Transactional //支持事务
//    public String FindReportTimeData( HttpServletRequest request, HttpSession session) {
//        try {
//            return dosageService.getReportTimeData(request,session);
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//    /**
//     * 按时间查询报告
//     * @param request
//     * @return
//     */
//    @MenuPermission("asset_use")
//    @RequestMapping("/find_report_month_year_data")
//    @Transactional //支持事务
//    public String FindReportMonthAndYearData( HttpServletRequest request, HttpSession session) {
//        try {
//            return dosageService.getReportMonthAndYearData(request,session);
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//
}
