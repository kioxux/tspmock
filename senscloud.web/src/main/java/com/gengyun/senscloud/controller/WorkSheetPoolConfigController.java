package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.IFacility;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.model.UserFunctionData;
//import com.gengyun.senscloud.model.WorkSheetPoolConfigModel;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.LinkedHashMap;
//import java.util.List;
//
///**
// * 工单派工配置
// * Created by Dong wudang on 2018/11/20.
// */
//
//@RestController
//@RequestMapping("/work_sheet_pool_config")
public class WorkSheetPoolConfigController {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    WorkSheetPoolConfigService workSheetPoolConfigService;
//
//    @Autowired
//    SerialNumberService serialNumberService;
//
//    @Autowired
//    RepairService repairService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    /**
//     * 功能：默认
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/index")
//    public ModelAndView workSheetPoolIndex(HttpServletRequest request, HttpServletResponse response){
//        ModelAndView mv = new ModelAndView();
//        User loginUser = AuthService.getLoginUser(request);
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String facilitiesSelect = "";
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility  = userService.isAllUserFunctionPermission(schema_name, loginUser.getId(), "work_sheet_pool_config");
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        mv.addObject("facilitiesSelect",facilitiesSelect);
//        mv.setViewName("work_sheet_pool_config/index");
//        return mv;
//    }
//    //新增工单派工配置
//    @RequestMapping("/addWorkSheetPoolConfig")
//    @Transactional
//    public ResponseModel addWorkSheetPoolConfig(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        WorkSheetPoolConfigModel workSheetPoolConfigModel = new WorkSheetPoolConfigModel();
//        User user = authService.getLoginUser(request);
//        try{
//            String id_str = serialNumberService.generateMaxBusinessCode(schema_name, "work_sheet_config");
//            String pool_name_str = request.getParameter("pool_name");//工单池名称
//            String role_id = request.getParameter("role_id");//负责人
//            String distribute_type_str = request.getParameter("distribute_type");//1：派单 2：抢单
//            String distribute_sms_minutes_str = request.getParameter("distribute_sms_minutes");//分配短信提醒分钟
//            String receive_sms_minute_str = request.getParameter("receive_sms_minute");//接受短信提醒分钟
//            String member = request.getParameter("member");//设置成员
//            String facility = request.getParameter("facility");//设置位置
//
//
//            String pool_name = pool_name_str;
//
//            Integer distribute_type = Integer.parseInt(distribute_type_str);
//            Integer distribute_sms_minutes = Integer.parseInt(distribute_sms_minutes_str);
//            Integer receive_sms_minute = Integer.parseInt(receive_sms_minute_str);
//            Timestamp create_time = new Timestamp(System.currentTimeMillis());
//            Boolean isuse = true;
//
//            workSheetPoolConfigModel.setId(id_str);
//            workSheetPoolConfigModel.setPool_name(pool_name);
//            workSheetPoolConfigModel.setRole_id(role_id);
//            workSheetPoolConfigModel.setDistribute_type(distribute_type);
//            workSheetPoolConfigModel.setDistribute_sms_minutes(distribute_sms_minutes);
//            workSheetPoolConfigModel.setReceive_sms_minute(receive_sms_minute);
//            workSheetPoolConfigModel.setCreate_time(create_time);
//            workSheetPoolConfigModel.setCreate_user_account(user.getAccount());
//            workSheetPoolConfigModel.setIsuse(isuse);
//
//            int doAdd = workSheetPoolConfigService.addWorkSheetPoolConfig(schema_name,workSheetPoolConfigModel);
//            if(doAdd>0){
//                //保存位置和负责人
//                addWorkSheetPoolFacilityOrMember(schema_name,id_str,member,"role");
//                addWorkSheetPoolFacilityOrMember(schema_name,id_str,facility,"facility");
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_SUCC));//添加成功
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ADD_FAIL));//添加失败！
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.ERROR_ADDPROC));//添加出现错误
//            return model;
//        }
//    }
//
//    //新增工单派工配置
//    @RequestMapping("/updateWorkSheetPoolConfig")
//    @Transactional
//    public ResponseModel updateWorkSheetPoolConfig(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        WorkSheetPoolConfigModel workSheetPoolConfigModel = new WorkSheetPoolConfigModel();
//        User user = authService.getLoginUser(request);
//        try{
//            String pool_id_str = request.getParameter("pool_id");//工单池名称
//            String pool_name_str = request.getParameter("pool_name");//工单池名称
//            String role_id = request.getParameter("role_id");//负责人
//            String distribute_type_str = request.getParameter("distribute_type");//1：派单 2：抢单
//            String distribute_sms_minutes_str = request.getParameter("distribute_sms_minutes");//分配短信提醒分钟
//            String receive_sms_minute_str = request.getParameter("receive_sms_minute");//接受短信提醒分钟
//            String member = request.getParameter("member");//设置成员
//            String facility = request.getParameter("facility");//设置位置
//
//
//            String pool_name = pool_name_str;
//
//            Integer distribute_type = Integer.parseInt(distribute_type_str);
//            Integer distribute_sms_minutes = Integer.parseInt(distribute_sms_minutes_str);
//            Integer receive_sms_minute = Integer.parseInt(receive_sms_minute_str);
//            Timestamp create_time = new Timestamp(System.currentTimeMillis());
//            Boolean isuse = true;
//
//            workSheetPoolConfigModel.setId(pool_id_str);
//            workSheetPoolConfigModel.setPool_name(pool_name);
//            workSheetPoolConfigModel.setRole_id(role_id);
//            workSheetPoolConfigModel.setDistribute_type(distribute_type);
//            workSheetPoolConfigModel.setDistribute_sms_minutes(distribute_sms_minutes);
//            workSheetPoolConfigModel.setReceive_sms_minute(receive_sms_minute);
//            workSheetPoolConfigModel.setCreate_time(create_time);
//            workSheetPoolConfigModel.setCreate_user_account(user.getAccount());
//            workSheetPoolConfigModel.setIsuse(isuse);
//
//            int doUpdate = workSheetPoolConfigService.updateWorkSheetPoolConfig(schema_name,workSheetPoolConfigModel);
//            if(doUpdate>0){
//                //保存位置和负责人
//                addWorkSheetPoolFacilityOrMember(schema_name,pool_id_str,member,"role");
//                addWorkSheetPoolFacilityOrMember(schema_name,pool_id_str,facility,"facility");
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_SUCCESS));//更新成功
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_UPDATE_FAIL));//更新失败！
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.UPDATE_ERROR));//更新失败！
//            return model;
//        }
//    }
//
//    //删除工单派工配置列表数据
//    @RequestMapping("/delWorkSheetPoolConfig")
//    public ResponseModel delWorkSheetPoolConfig(HttpServletRequest request, HttpServletResponse response){
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try{
//            String id_str = request.getParameter("id");
//            String isuse_str = request.getParameter("isuse");
//            Boolean isuse =false;
//            isuse = Boolean.parseBoolean(isuse_str);
//            int dpUpdate = workSheetPoolConfigService.delWorkSheetPoolConfig(schema_name,id_str,isuse);
//            if(dpUpdate > 0){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.DELETE_SUCCESS));
//            }else{
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.ERROR_DELETUSER));
//            return model;
//        }
//    }
//
//    //查询工单派工配置列表
//    @RequestMapping("/selectWorkSheetPoolConfig")
//    public String selectWorkSheetPoolConfig(HttpServletRequest request, HttpServletResponse response){
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        JSONObject result = new JSONObject();
//        try{
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String role_id_name_search = request.getParameter("role_id_name_search");
//            String pool_name_search = request.getParameter("pool_name_search");
//            String condition = "";
//            if (role_id_name_search != null && !role_id_name_search.isEmpty() && !role_id_name_search.equals("-1")) {
//                condition = " and w.role_id = '" + role_id_name_search + "'";
//            }
//            if (pool_name_search != null && !pool_name_search.isEmpty() && !pool_name_search.equals("")) {
//                condition = " and (w.pool_name like '%" + pool_name_search + "%' or u.account like '%"+pool_name_search+"%' or u.user_code like '%"+pool_name_search+"%' or u.username like '%"+pool_name_search+"%')";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<WorkSheetPoolConfigModel> workSheetPoolConfigModelsList = workSheetPoolConfigService.selectWorkSheetPoolConfig(schema_name,condition, pageSize, begin);
//            int count = workSheetPoolConfigService.selectWorkSheetPoolConfigCount(schema_name,condition);
//            result.put("rows", workSheetPoolConfigModelsList);
//            result.put("total", count);
//
//            return result.toString();
//        }catch (Exception e){
//            return "{}";
//        }
//    }
//
//    //查询工单派默认工单池有效
//    @RequestMapping("/selectWorkSheetPoolConfigValid")
//    public ResponseModel selectWorkSheetPoolConfigValid(HttpServletRequest request, HttpServletResponse response){
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        Boolean isAllFacility = false;
//        Boolean isSelfFacility = false;
//        String condition = "";
//        ResponseModel model = new ResponseModel();
//        try{
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "workSheetPool");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("work_shee_pool_all_facility")) {
//                        isAllFacility = true;
//                        break;
//                    } else if (functionData.getFunctionName().equals("work_shee_pool_self_facility")) {
//                        isSelfFacility = true;
//                    }
//                }
//            }
//            if (isAllFacility) {
//                condition = "  ";
//            } else if (isSelfFacility) {
//                LinkedHashMap<String, IFacility> facilityList = user.getFacilities();
//                String facilityIds = "";
//                if (facilityList != null && !facilityList.isEmpty()) {
//                    for (String key : facilityList.keySet()) {
//                        facilityIds += facilityList.get(key).getId() + ",";
//                    }
//                }
//                if (!facilityIds.isEmpty() && !facilityIds.equals("")) {
//                    facilityIds = facilityIds.substring(0, facilityIds.length() - 1);
//                    condition = " and f.facility_id in (" + facilityIds + ") ";
//                }
//            }
//
//            List<WorkSheetPoolConfigModel> workSheetPoolConfigModelsList = workSheetPoolConfigService.selectWorkSheetPoolConfigValid(schema_name ,condition );
//            if(workSheetPoolConfigModelsList!=null && workSheetPoolConfigModelsList.size()>0){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//                model.setContent(workSheetPoolConfigModelsList);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_SU));
//            }else {
//                model.setCode(StatusConstant.NO_DATE_RETURN);
//                model.setContent(null);
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_NO_DATA));//未查询到数据
//            }
//            return model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.EDITING_WO_POOL_QUERY_ERROR_PLEASE_CONTACT_ADMIN));//工单池列表查询出现异常，请刷新重试
//            return model;
//        }
//    }
//
//    /**
//     * 功能：根据工单池id查询工单池详情
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/selectWorkSheetPoolConfigById")
//    public ResponseModel selectWorkSheetPoolConfigById(HttpServletRequest request, HttpServletResponse response){
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel model = new ResponseModel();
//        try{
//            String id = request.getParameter("id");
//            WorkSheetPoolConfigModel workSheetPoolConfigModels = workSheetPoolConfigService.selectWorkSheetPoolConfigById(schema_name, id);
//            model.setCode(StatusConstant.SUCCES_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_SU));//详情查询成功
//            model.setContent(workSheetPoolConfigModels);
//           return  model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SERCAH_ERROR));//查询异常
//            return model;
//        }
//    }
//
//
//    /**
//     * 更新操作删除过往负责角色和位置
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/delWorkSheetPoolFacilityOrMember")
//    public ResponseModel delWorkSheetPoolFacilityOrMember(HttpServletRequest request, HttpServletResponse response){
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel model = new ResponseModel();
//        try{
//            String pool_id = request.getParameter("pool_id");
//            int doDelRole = workSheetPoolConfigService.delWorkSheetPoolRoleByPoolId(schema_name,pool_id);
//            int doDelFacility = workSheetPoolConfigService.delWorkSheetPoolFacilityByPoolId(schema_name,pool_id);
//            if(doDelRole>0 && doDelFacility >0){
//                model.setCode(StatusConstant.SUCCES_RETURN);
//            }
//            return  model;
//        }catch (Exception e){
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_PL_WK_RO_ERROR));//计划工单更新位置和角色出现异常
//            return model;
//        }
//    }
//
////    @RequestMapping("/selectUserLeadingOfficial")
////    public ResponseModel selectUserLeadingOfficial(HttpServletRequest request, HttpServletResponse response){
////        String schema_name = authService.getCompany(request).getSchema_name();
////        User user = authService.getLoginUser(request);
////        ResponseModel model = new ResponseModel();
////        try{
////            String id = request.getParameter("id");
////            WorkSheetPoolConfigModel workSheetPoolConfigModels = workSheetPoolConfigService.selectUserLeadingOfficial(schema_name, id);//根据权限和
////            model.setCode(StatusConstant.SUCCES_RETURN);
////            model.setMsg("详情查询成功");
////            model.setContent(workSheetPoolConfigModels);
////            return  model;
////        }catch (Exception e){
////            model.setCode(StatusConstant.ERROR_RETURN);
////            model.setMsg("工单池详情查询出现异常");
////            return model;
////        }
////    }
//
//
//    /**
//     * 功能：保存工单池位置
//     * @param pool_id
//     * @param member
//     * @return
//     */
//    @Transactional
//    private Boolean addWorkSheetPoolFacilityOrMember(String schema_name,String pool_id,String member,String status){
//       String[] member_arr = member.split(",");
//       Boolean doAdd = false;
//       if(member_arr.length>0 && !member_arr[0].equals("")){
//        for(int i=0;i<member_arr.length;i++){
//            int doUpdate = 0;
//            if(status.equals("role")){
//                doUpdate = workSheetPoolConfigService.addWorkSheetPoolRole(schema_name,pool_id,member_arr[i]);
//            }else if(status.equals("facility")){
//                doUpdate = workSheetPoolConfigService.addWorkSheetPoolFacility(schema_name,pool_id,Integer.parseInt(member_arr[i]));
//            }
//
//            if(doUpdate>0){
//                doAdd = true;
//            }
//        }
//       }
//
//       return doAdd;
//    }
//
//

}
