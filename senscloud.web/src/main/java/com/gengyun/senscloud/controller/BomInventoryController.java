package com.gengyun.senscloud.controller;
//
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.flowable.CustomTaskEntityImpl;
//import com.gengyun.senscloud.mapper.BomInventoryMapper;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.bom.BomInventoryService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.WorkflowService;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.*;
//
///**
// * 备件盘点
// */
//@RestController
//@RequestMapping("/bom_inventory")
public class BomInventoryController {
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    private BomInventoryService bomInventoryService;
//
//    @Autowired
//    private BomInventoryMapper bomInventoryMapper;
//
//    @Autowired
//    private SelectOptionService selectOptionService;
//
//    @Autowired
//    private WorkflowService workflowService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            Map<String, Object> workType = selectOptionService.getOptionByCode(schemaName, "work_type_by_business_type_id", SensConstant.BUSINESS_NO_35);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("inventory_add", "rpAddFlag");
//                put("inventory_delete", "rpDeleteFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_inventory/index", "bom_inventory")
//                    .addObject("workTypeId", workType.get("code"))
//                    .addObject("relation", workType.get("relation"));
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_inventory/index");
//        }
//    }
//
//    /**
//     * 备件盘点位置页面
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("/inventory_stock")
//    public ModelAndView inventoryStockList(HttpServletRequest request) {
//        try {
//            User user = AuthService.getLoginUser(request);
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String inventoryCode = request.getParameter("inventoryCode");
//            Map<String, Object> map = bomInventoryService.queryBomInventoryByCode(schemaName, inventoryCode);
//            Map<String, String> permissionInfo = new HashMap<String, String>() {{
//                put("inventory_edit", "rpEditFlag");
//                put("pc_inventory", "pcInventoryFlag");
//            }};
//            return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bom/bom_inventory/inventory_stock", "bom_inventory")
//                .addObject("inventoryCode", inventoryCode)
//                .addObject("inventory_name", (String)map.get("inventory_name"))
//                .addObject("userAccount", user.getAccount());
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_inventory/inventory_stock");
//        }
//    }
//
//    /**
//     * 盘点备件列表页面
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("inventory_stock_detail")
//    public ModelAndView inventoryStockDetailList(HttpServletRequest request) {
//        try {
//            User user = AuthService.getLoginUser(request);
//            String schemaName = authService.getCompany(request).getSchema_name();
//            String inventoryCode = request.getParameter("inventoryCode");
//            String subInventoryCode = request.getParameter("subInventoryCode");
//            String stockCode = request.getParameter("stockCode");
//            //获取流程信息
//            List<CustomTaskEntityImpl> taskList = workflowService.getTasks(schemaName, "", null, Arrays.asList(subInventoryCode), null, null, false, null, null, null, null, null);
//            String taskId = "";
//            String assignee = "";
//            if(taskList != null && taskList.size() >0){
//                taskId = taskList.get(0).getId();
//                assignee = taskList.get(0).getAssignee();
//            }
//            ModelAndView mav = new ModelAndView("bom/bom_inventory/inventory_stock_detail");
//            Map<String, Object> map = bomInventoryService.queryBomInventoryByCode(schemaName, inventoryCode);
//            Map<String, Object> stock = bomInventoryMapper.queryBomInventoryStockByCode(schemaName, subInventoryCode);
//            if(map != null ){
//                mav.addObject("inventoryName", (String)map.get("inventory_name"));
//            }
//            return mav.addObject("inventoryCode", inventoryCode)
//                    .addObject("subInventoryCode", subInventoryCode)
//                    .addObject("stockCode", stockCode)
//                    .addObject("taskId", taskId)
//                    .addObject("isShowBtn", user.getAccount().equals(assignee))
//                    .addObject("stockStatus", stock.get("status"))
//                    .addObject("stockName", stock.get("stock_name"));
//        } catch (Exception e) {
//            return new ModelAndView("bom/bom_inventory/inventory_stock_detail");
//        }
//    }
//
//    /**
//     * 查询备件盘点列表
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("findInventoryList")
//    public String findInventoryList(HttpServletRequest request) {
//        try {
//            return bomInventoryService.findBomInventoryList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询盘点位置列表
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("findInventoryStockList")
//    public String findInventoryStockList(HttpServletRequest request) {
//        try {
//            return bomInventoryService.findInventoryStockList(request);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 查询盘点备件列表
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("findInventoryStockDetailList")
//    public String findInventoryStockDetailList(HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return bomInventoryService.findInventoryStockDetailList(request, schemaName);
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 备件盘点作废
//     * @param request
//     * @return
//     */
//    @MenuPermission("inventory_delete")
//    @Transactional
//    @RequestMapping("invalidInventory")
//    public ResponseModel invalidInventory(HttpServletRequest request){
//        try {
//            return bomInventoryService.invalidInventory(request);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.errorMsg(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 根据code查询盘点位置详情数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("/findSingleBidInfo")
//    public ResponseModel findSingleBidInfo(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String subWorkCode = request.getParameter("subWorkCode");
//            Map<String, Object> map = bomInventoryService.queryBomInventoryStockByCode(schemaName, subWorkCode);
//            return ResponseModel.ok(map);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     *  盘点分配页面
//     * @param request
//     * @return
//     */
//    @MenuPermission("inventory_edit")
//    @RequestMapping("/detail-bomInventoryStock")
//    public ModelAndView bomInventoryStockDetail(HttpServletRequest request) {
//        try {
//            return bomInventoryService.getDetailInfo(request, "work/worksheet_detail/worksheet_detail");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 查询库房在库备件总数(模板页面联动获取库房备件数)
//     * @param request
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("/countStockBomQuantity")
//    public ResponseModel countStockBomQuantity(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            String stockCodes = request.getParameter("stockCodes");
//            String bomTypes = request.getParameter("bomTypes");
//            if(StringUtils.isBlank(stockCodes))
//                return ResponseModel.ok(0);
//
//            int quantity = bomInventoryService.countStockBomQuantity(schemaName, stockCodes, bomTypes);
//            return ResponseModel.ok(quantity);
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    /**
//     * 启动盘点提交
//     * @param request
//     * @return
//     */
//    @MenuPermission("inventory_add")
//    @RequestMapping("/addInventory")
//    @Transactional
//    public ResponseModel addInventory(@RequestParam(name = "flow_id") String processDefinitionId, HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return bomInventoryService.addInventory(schemaName, request, processDefinitionId, paramMap);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_START_INV_FAIL));
//        }
//    }
//
//    /**
//     * PC盘点提交
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("pc_inventory")
//    @RequestMapping("pcInventory")
//    @Transactional
//    public ResponseModel pcInventory(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return bomInventoryService.pcInventory(schemaName, request, paramMap);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_PC_INVENTORY_FAIL));
//        }
//    }
//
//    /**
//     * 盘点分配提交
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("inventory_edit")
//    @RequestMapping("inventoryAllot")
//    @Transactional
//    public ResponseModel inventoryAllot(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return bomInventoryService.inventoryAllot(schemaName, request, paramMap);
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            return ResponseModel.errorMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_INVENTORY_ALLOT_FAIL));
//        }
//    }
//
//    /**
//     * 盘点盈余备件的入库
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("stockInBom")
//    @Transactional
//    public ResponseModel stockInBom(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomInventoryService.stockInBom(schemaName, request, paramMap);
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 盘点盈余备件的报废
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("stockDiscard")
//    @Transactional
//    public ResponseModel stockDiscard(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomInventoryService.stockDiscard(schemaName, request, paramMap);
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
//
//    /**
//     * 盘点完成
//     * @param request
//     * @param paramMap
//     * @return
//     */
//    @MenuPermission("bom_inventory")
//    @RequestMapping("finishInventory")
//    @Transactional
//    public ResponseModel finishInventory(HttpServletRequest request, @RequestParam Map<String, Object> paramMap) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        try {
//            return bomInventoryService.finishInventory(schemaName, request, paramMap);
//        } catch (Exception e) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_FAIL);
//            return ResponseModel.errorMsg(tmpMsg);//失败
//        }
//    }
}
