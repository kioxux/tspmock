package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.StatisticService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "统计分析配置")
@RestController
public class StatisticConfigController {

    @Resource
    StatisticService statisticService;

    @ApiOperation(value = "获取统计分析配置列表模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_STATISTIC_CONFIG_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchStatisticConfigListPermission", method = RequestMethod.POST)
    public ResponseModel searchStatisticConfigListPermission(MethodParam methodParam) {
        return ResponseModel.ok(statisticService.getStatisticConfigListPermission(methodParam));
    }

    @ApiOperation(value = "查询统计分析配置列表", notes = ResponseConstant.RSP_DESC_SEARCH_STATISTIC_CONFIG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber","keywordSearch","groupName"})
    @RequestMapping(value = "/searchStatisticConfigList", method = RequestMethod.POST)
    public ResponseModel searchStatisticConfigList(MethodParam methodParam, SearchParam param) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(statisticService.getStatisticConfigList(methodParam, param));
    }

    @ApiOperation(value = "添加统计分析配置", notes = ResponseConstant.RSP_DESC_ADD_STATISTIC_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","export_url", "client_page", "description","is_show_dashboard","options","query","query_conditions","table_columns","table_query","title","group_name"})
    @Transactional //支持事务
    @RequestMapping(value = "/addStatistic", method = RequestMethod.POST)
    public ResponseModel addStatistic(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") StatisticAdd statisticAdd) {
        statisticService.newStatistic(methodParam, paramMap,true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑统计分析配置", notes = ResponseConstant.RSP_DESC_EDIT_STATISTIC_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "export_url","id","client_page", "description","is_show_dashboard","options","query","query_conditions","table_columns","table_query","title","group_name"})
    @Transactional //支持事务
    @RequestMapping(value = "/editStatistic", method = RequestMethod.POST)
    public ResponseModel editStatistic(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") StatisticAdd statisticAdd,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        statisticService.newStatistic(methodParam, paramMap,false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取统计配置明细信息", notes = ResponseConstant.RSP_DESC_GET_STATISTIC_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchStatisticInfo", method = RequestMethod.POST)
    public ResponseModel searchStatisticInfo(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(statisticService.getStatisticInfoById(methodParam, id));
    }

    @ApiOperation(value = "删除选中统计配置", notes = ResponseConstant.RSP_DESC_REMOVE_STATISTIC_CONFIG_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/removeSelectStatistic", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectStatistic(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        statisticService.cutStatisticByIds(methodParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "显示为看板", notes = ResponseConstant.RSP_DESC_CHANGE_USE_STATISTIC_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "is_show_dashboard"})
    @Transactional //支持事务
    @RequestMapping(value = "/editStatisticShow", method = RequestMethod.POST)
    public ResponseModel editStatisticShow(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") StatisticAdd stockAdd, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        statisticService.modifyStatisticShowForV3(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "前端报表信息-获取统计配置明细信息", notes = ResponseConstant.RSP_DESC_GET_STATISTIC_CONFIG)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchStatisticInfoFront", method = RequestMethod.POST)
    public ResponseModel searchStatisticInfoFront(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(statisticService.getStatisticInfoNoQueryById(methodParam, id));
    }
}
