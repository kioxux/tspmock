package com.gengyun.senscloud.controller.bulletinBoard;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 科华安全日历
 */
@RestController
@RequestMapping("/calendar")
public class CalendarController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    CalendarService calendarService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("calendar_add", "caAddFlag");
//            put("calendar_edit", "caEditFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "bulletin_board/calendar/index", "calendar");
//    }
//
//    /**
//     * 查询列表页列表数据（分页）
//     *
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     */
//    @RequestMapping("/find-calendar-list")
//    public String queryAddressList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                   @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                   @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                   @RequestParam(name = "sortName", required = false) String sortName,
//                                   @RequestParam(name = "sortOrder", required = false) String sortOrder, @RequestParam("beginTime") String beginTime, @RequestParam("endTime") String endTime,
//                                   @Param("accident_type") String accident_type,
//                                   HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = calendarService.query(schemaName, searchKey, pageSize, pageNumber, sortName, sortOrder,accident_type,beginTime,endTime);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/addCalendar")
//    @Transactional //支持事务
//    public ResponseModel addAddress(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "calendar", "calendar_edit");
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return calendarService.addCalendar(request, loginUser, schemaName, paramMap);
//            } else {
//                return ResponseModel.error("");
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);
//        }
//    }
//    /**
//     * 进入编辑页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/editView")
//    public ModelAndView editView(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String id = request.getParameter("id");
//        if(null!=id&&!"".equals(id)){
//            return new ModelAndView("bulletin_board/calendar/edit_calendar").addObject("calendar", JSON.toJSON(calendarService.queryCalendayId(schema_name,Integer.parseInt(id))));
//        }else{
//            return new ModelAndView("bulletin_board/calendar/edit_calendar");
//        }
//    }
//    /**
//     * 更新数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @RequestMapping("/editCalendar")
//    @Transactional //支持事务
//    public ResponseModel editAddress(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "calendar", "calendar_edit");
//            if (permissionFlag) {
//                return calendarService.updateCalendar(request, loginUser, schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);
//        }
//    }
//
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping("/deleteCalendar")
//    @Transactional //支持事务
//    public ResponseModel deleteAddress(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag =true;
//            if (permissionFlag) {
//                return calendarService.deleteById(schemaName, request, loginUser);
//            } else {
//                return ResponseModel.error("");
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo( LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);
//        }
//    }
}
