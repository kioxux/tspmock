package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.TaskTempLateModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.TaskTempLateService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 任务模板管理
 */
@Api(tags = "任务模板管理")
@RestController
public class TaskTempLateController {
    @Resource
    TaskTempLateService tasktempLateService;

    @ApiOperation(value = "获取任务模板模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_SYSTEM_CONFIG_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchTaskTempLatePermission", method = RequestMethod.POST)
    public ResponseModel searchTaskTempLatePermission(MethodParam methodParam) {
        return ResponseModel.ok(tasktempLateService.getTaskTempLatePermission(methodParam));
    }

    @ApiOperation(value = "新增任务模板", notes = ResponseConstant.RSP_DESC_ADD_TASK_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_name", "is_use", "remark"})
    @Transactional
    @RequestMapping(value = "/addTaskTempLate", method = RequestMethod.POST)
    public ResponseModel addTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return ResponseModel.ok(tasktempLateService.newTaskTempLate(methodParam, taskTempLateModel));
    }

    @ApiOperation(value = "编辑任务模板", notes = ResponseConstant.RSP_DESC_EDIT_TASK_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_code", "task_template_name", "isuse", "remark"})
    @Transactional
    @RequestMapping(value = "/editTaskTempLate", method = RequestMethod.POST)
    public ResponseModel editTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.modifyTempLate(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除任务模板", notes = ResponseConstant.RSP_DESC_REMOVE_TASK_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_codes"})
    @Transactional
    @RequestMapping(value = "/removeTaskTempLate", method = RequestMethod.POST)
    public ResponseModel removeTaskTempLate(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.cutTaskTempLate(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "任务模板详情", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_TEMPLATE_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_code"})
    @RequestMapping(value = "/searchTaskTempLateInfo", method = RequestMethod.POST)
    public ResponseModel searchTaskTempLateInfo(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return ResponseModel.ok(tasktempLateService.getTaskTempLateInfo(methodParam, taskTempLateModel));
    }

    @ApiOperation(value = "分页查询任务模板", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_TEMPLATE_PAGE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "isUseSearch"})
    @RequestMapping(value = "/searchTaskTempLatePage", method = RequestMethod.POST)
    public ResponseModel searchTaskTempLatePage(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(tasktempLateService.getTaskTempLatePage(methodParam, taskTempLateModel));
    }

    @ApiOperation(value = "新增任务模板任务项关联", notes = ResponseConstant.RSP_DESC_ADD_TASK_TEMPLATE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_item_codes", "task_template_code"})
    @Transactional
    @RequestMapping(value = "/addTaskTempLateItem", method = RequestMethod.POST)
    public ResponseModel addTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.newTaskTempLateItem(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除任务模板任务项关联", notes = ResponseConstant.RSP_DESC_REMOVE_TASK_TEMPLATE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_ids"})
    @Transactional
    @RequestMapping(value = "/removeTaskTempLateItem", method = RequestMethod.POST)
    public ResponseModel removeTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.cutTaskTempLateItem(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑任务模板任务项组名称", notes = ResponseConstant.RSP_DESC_EDIT_TASK_TEMPLATE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_ids", "group_name"})
    @Transactional
    @RequestMapping(value = "/editTaskTempLateItem", method = RequestMethod.POST)
    public ResponseModel editTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.modifyTaskTempLateItem(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "向上移动任务模板任务项", notes = ResponseConstant.RSP_DESC_MOVE_UP_TASK_TEMPLATE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_id"})
    @Transactional
    @RequestMapping(value = "/moveUpTaskTempLateItem", method = RequestMethod.POST)
    public ResponseModel moveUpTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.moveUpTaskTempLateItem(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "向下移动任务模板任务项", notes = ResponseConstant.RSP_DESC_MOVE_MOVE_TASK_TEMPLATE_ITEM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_id"})
    @Transactional
    @RequestMapping(value = "/moveDownTaskTempLateItem", method = RequestMethod.POST)
    public ResponseModel moveDownTaskTempLateItem(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        tasktempLateService.moveDownTaskTempLateItem(methodParam, taskTempLateModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "任务模板任务项关联列表", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_TEMPLATE_ITEM_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_code", "keywordSearch"})
    @RequestMapping(value = "/searchTaskTempLateItemList", method = RequestMethod.POST)
    public ResponseModel searchTaskTempLateItemList(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return ResponseModel.ok(tasktempLateService.getTaskTempLateItemList(methodParam, taskTempLateModel));
    }

    @ApiOperation(value = "查询任务模板列表", notes = ResponseConstant.RSP_DESC_SEARCH_TASK_TEMPLATE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"})
    @RequestMapping(value = "/searchTaskTempLateList", method = RequestMethod.POST)
    public ResponseModel searchTaskTempLateList(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return ResponseModel.ok(tasktempLateService.getTaskTempLateList(methodParam, taskTempLateModel));
    }

    @ApiOperation(value = "获取选中任务模板导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_TASK_TEMP_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "task_template_codes"})
    @RequestMapping(value = "/getExportTaskTempCode", method = RequestMethod.POST)
    public ResponseModel getExportTaskTempCode(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return ResponseModel.okExport(tasktempLateService.getExportTaskTempInfo(methodParam, taskTempLateModel, false));
    }

    @ApiOperation(value = "获取全部任务模板导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_TASK_TEMP_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch", "isUseSearch"})
    @RequestMapping(value = "/getExportAllTaskTempCode", method = RequestMethod.POST)
    public ResponseModel getExportAllTaskTempCodes(MethodParam methodParam, TaskTempLateModel taskTempLateModel) {
        return ResponseModel.okExport(tasktempLateService.getExportTaskTempInfo(methodParam, taskTempLateModel, true));
    }
}
