package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.business.AddressManageService;
//import com.gengyun.senscloud.service.PagePermissionService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 地址管理
// * User: sps
// * Date: 2018/11/20
// * Time: 上午11:20
// */
//@RestController
//@RequestMapping("/address_manage")
public class AddressManageController {
//    @Autowired
//    private AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    AddressManageService addressManageService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    /**
//     * 进入主页，获取按钮权限
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("address_manage")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView index(HttpServletRequest request) {
//        Map<String, String> permissionInfo = new HashMap<String, String>() {{
//            put("address_add", "rpAddFlag");
//            put("address_edit", "rpEditFlag");
//            put("address_delete", "rpDeleteFlag");
//        }};
//        return pagePermissionService.getUserFunctionPermissionForPage(request, permissionInfo, "address_manage/index", "address_manage");
//    }
//
//    /**
//     * 查询列表页列表数据（分页）
//     *
//     * @param searchKey
//     * @param pageSize
//     * @param pageNumber
//     * @param sortName
//     * @param sortOrder
//     * @param request
//     * @return
//     */
//    @MenuPermission("address_manage")
//    @RequestMapping("/find-address-list")
//    public String queryAddressList(@RequestParam(name = "keyWord", required = false) String searchKey,
//                                   @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                   @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                   @RequestParam(name = "sortName", required = false) String sortName,
//                                   @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                   HttpServletRequest request) {
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            JSONObject pageResult = addressManageService.query(schemaName, searchKey, pageSize, pageNumber, sortName, sortOrder);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("address_manage")
//    @RequestMapping("/addAddress")
//    @Transactional //支持事务
//    public ResponseModel addAddress(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "address_manage", "address_add");
//            if (permissionFlag) {
//                paramMap.put("create_user_account", loginUser.getAccount());
//                return addressManageService.addAddress(request, loginUser, schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//    /**
//     * 更新数据
//     *
//     * @param paramMap
//     * @param request
//     * @return
//     */
//    @MenuPermission("address_manage")
//    @RequestMapping("/editAddress")
//    @Transactional //支持事务
//    public ResponseModel editAddress(@RequestParam Map<String, Object> paramMap, HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "address_manage", "address_edit");
//            if (permissionFlag) {
//                return addressManageService.updateAddress(request, loginUser, schemaName, paramMap);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
//
//
//    /**
//     * 删除数据
//     *
//     * @param request
//     * @return
//     */
//    @MenuPermission("address_manage")
//    @RequestMapping("/deleteAddress")
//    @Transactional //支持事务
//    public ResponseModel deleteAddress(HttpServletRequest request) {
//        String schemaName = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        try {
//            boolean permissionFlag = pagePermissionService.getPermissionByKey(schemaName, loginUser.getId(), "address_manage", "address_delete");
//            if (permissionFlag) {
//                return addressManageService.deleteById(loginUser, schemaName, request);
//            } else {
//                String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_INS_AUTH);
//                return ResponseModel.error(tmpMsg);//权限不足 msg_ins_auth
//            }
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // 手动回滚
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_BK);
//            return ResponseModel.error(tmpMsg);//操作失败 msg_oper_fail
//        }
//    }
}
