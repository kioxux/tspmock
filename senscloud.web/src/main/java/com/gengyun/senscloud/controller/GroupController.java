package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.GroupModel;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.GroupService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@Api(tags = "部门岗位管理")
@RestController
public class GroupController {
    @Resource
    GroupService groupService;

    @ApiOperation(value = "获取部门岗位模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_GROUP_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchGroupListPermission", method = RequestMethod.POST)
    public ResponseModel searchGroupListPermission(MethodParam methodParam) {
        return ResponseModel.ok(groupService.getGroupListPermission(methodParam));
    }

    @ApiOperation(value = "新增部门", notes = ResponseConstant.RSP_DESC_ADD_GROUP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "group_name", "group_code", "parent_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addGroup", method = RequestMethod.POST)
    public ResponseModel addGroup(MethodParam methodParam, GroupModel groupModel) {
        groupService.newGroup(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "新增岗位", notes = ResponseConstant.RSP_DESC_ADD_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_name", "group_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addPosition", method = RequestMethod.POST)
    public ResponseModel addPosition(MethodParam methodParam, GroupModel groupModel) {
        groupService.newPosition(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "新增岗位角色", notes = ResponseConstant.RSP_DESC_EDIT_ADD_POSITION_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "position_id", "roleIds"})
    @Transactional //支持事务
    @RequestMapping(value = "/addPositionRole", method = RequestMethod.POST)
    public ResponseModel addPositionRole(MethodParam methodParam, GroupModel groupModel) {
        groupService.newPositionRole(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新部门", notes = ResponseConstant.RSP_DESC_EDIT_GROUP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "group_id", "group_name", "group_code", "parent_id",  "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/editGroup", method = RequestMethod.POST)
    public ResponseModel editGroup(MethodParam methodParam, GroupModel groupModel) {
        groupService.modifyGroup(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "更新岗位", notes = ResponseConstant.RSP_DESC_EDIT_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_id", "position_name", "group_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/editPosition", method = RequestMethod.POST)
    public ResponseModel editPosition(MethodParam methodParam, GroupModel groupModel) {
        groupService.modifyPosition(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除部门", notes = ResponseConstant.RSP_DESC_REMOVE_GROUP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "group_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeGroup", method = RequestMethod.POST)
    public ResponseModel removeGroup(MethodParam methodParam, GroupModel groupModel) {
        groupService.cutGroup(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除岗位角色", notes = ResponseConstant.RSP_DESC_EDIT_REMOVE_POSITION_ROLE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "position_id", "roleId"})
    @Transactional //支持事务
    @RequestMapping(value = "/removePositionRole", method = RequestMethod.POST)
    public ResponseModel removePositionRole(MethodParam methodParam, GroupModel groupModel) {
        groupService.cutPositionRole(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除岗位", notes = ResponseConstant.RSP_DESC_REMOVE_POSITION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "position_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removePosition", method = RequestMethod.POST)
    public ResponseModel removePosition(MethodParam methodParam, GroupModel groupModel) {
        groupService.cutPosition(methodParam, groupModel);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询部门详情", notes = ResponseConstant.RSP_DESC_SEARCH_GROUP_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "group_id"})
    @RequestMapping(value = "/searchGroupInfo", method = RequestMethod.POST)
    public ResponseModel searchGroupInfo(MethodParam methodParam, GroupModel groupModel) {
        return ResponseModel.ok(groupService.getGroupInfo(methodParam, groupModel));
    }

    @ApiOperation(value = "查询部门列表", notes = ResponseConstant.RSP_DESC_SEARCH_GROUP_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchGroupList", method = RequestMethod.POST)
    public ResponseModel searchGroupList(MethodParam methodParam, GroupModel groupModel) {
        return ResponseModel.ok(groupService.getGroupList(methodParam, groupModel));
    }

    @ApiOperation(value = "查询部门列表下拉框", notes = ResponseConstant.RSP_DESC_SEARCH_GROUP_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","id","is_out"})
    @RequestMapping(value = "/searchSelectGroupList", method = RequestMethod.POST)
    public ResponseModel searchSelectGroupList(MethodParam methodParam, GroupModel groupModel) {
        return ResponseModel.ok(groupService.getGroupSelectList(methodParam, groupModel));
    }

    @ApiOperation(value = "查询岗位详情", notes = ResponseConstant.RSP_DESC_SEARCH_POSITION_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "position_id", "roleSearch"})
    @RequestMapping(value = "/searchPositionInfo", method = RequestMethod.POST)
    public ResponseModel searchPositionInfo(MethodParam methodParam, GroupModel groupModel) {
        return ResponseModel.ok(groupService.getPositionInfo(methodParam, groupModel));
    }

    @ApiOperation(value = "查询岗位角列表", notes = ResponseConstant.RSP_DESC_EDIT_SEARCH_POSITION_ROLE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "position_id"})
    @RequestMapping(value = "/searchPositionRoleList", method = RequestMethod.POST)
    public ResponseModel searchPositionRoleList(MethodParam methodParam, GroupModel groupModel) {
        return ResponseModel.ok(groupService.getPositionRoleList(methodParam, groupModel));
    }

    @ApiOperation(value = "新增岗位角色的可选角色列表", notes = ResponseConstant.RSP_DESC_EDIT_SEARCH_ADD_POSITION_ROLE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "position_id,keywordSearch"})
    @RequestMapping(value = "/searchAddPositionRoleList", method = RequestMethod.POST)
    public ResponseModel searchAddPositionRoleList(MethodParam methodParam, GroupModel groupModel) {
        return ResponseModel.ok(groupService.getAddPositionRoleList(methodParam, groupModel));
    }
}
