package com.gengyun.senscloud.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.StatusConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Constants;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.util.*;
//
//@RestController
//@RequestMapping("/work_task")
public class WorkTaskController {
//    @Autowired
//    AuthService authService;
//    @Autowired
//    FacilitiesService facilitiesService;
//    @Autowired
//    UserService userService;
//    @Autowired
//    WorkTaskService workTaskService;
//    @Autowired
//    SystemConfigService systemConfigService;
//    @Autowired
//    SerialNumberService serialNumberService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    BomService bomService;
//    @Autowired
//    PagePermissionService pagePermissionService;
//
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping({"/", "/index"})
//    public ModelAndView selectStockData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String client_customer_code = authService.getCompany(request).getClient_customer_code();
//        User loginUser = AuthService.getLoginUser(request);
//        String status = request.getParameter("status");
//        String facilitiesSelect = "";
//        Boolean exportFlag = false;
//        Boolean invalidFlag = false;
//        Boolean distributeFlag = false;
//        Boolean addFlag = false;
//        Boolean auditpcFlag = false;
//        Boolean dealpcFlag = false;
//        Boolean applyForBomFlag = false;//申领备件状态
//        try {
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "repair");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
//                for (UserFunctionData functionData : userFunctionList) {
//                    //如果可以查看全部位置，则不限制
//                    if (functionData.getFunctionName().equals("repair_all_facility")) {
//                        isAllFacility = true;
//                    } else if (functionData.getFunctionName().equals("repair_export")) {
//                        exportFlag = true;
//                    } else if (functionData.getFunctionName().equals("repair_invalid")) {
//                        invalidFlag = true;
//                    } else if (functionData.getFunctionName().equals("repair_distribute")) {
//                        distributeFlag = true;
//                    } else if (functionData.getFunctionName().equals("repair_add")) {
//                        addFlag = true;
//                    } else if (functionData.getFunctionName().equals("repair_audit_pc")) {
//                        auditpcFlag = true;
//                    } else if (functionData.getFunctionName().equals("repair_do_pc")) {
//                        dealpcFlag = true;
//                    }
//                }
//            }
//
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, Constants.APPLY_FOR_BOM);
//            if (dataList != null) {
//                if (dataList.getSettingValue().equals("1")) {
//                    applyForBomFlag = true;
//                }
//            }
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        return new ModelAndView("work_task/index")
//                .addObject("facilitiesSelect", facilitiesSelect)
//                .addObject("exportFlag", exportFlag)
//                .addObject("rpAddFlag", addFlag)
//                .addObject("rpInvalidFlag", invalidFlag)
//                .addObject("rpDistributeFlag", distributeFlag)
//                .addObject("rpAuditpcFlag", auditpcFlag).addObject("rpDealpcFlag", dealpcFlag)
//                .addObject("currentUser", loginUser.getAccount())
//                .addObject("departmentname", loginUser.getDepartmentName())
//                .addObject("client_customer_code", client_customer_code)
//                .addObject("searchStatus", status)
//                .addObject("applyForBomFlag", applyForBomFlag);
//
//    }
//
//    //任务列表
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/find-worktask-list")
//    public String queryworksheet(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        JSONObject result = new JSONObject();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
////        String searchStatus = request.getParameter("status");
//        String check_method = request.getParameter("check_method");
//
//        String keyWord = request.getParameter("keyWord");
//        List<WorkTask> workTaskList = null;
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition="";
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(t.task_item_code) like upper('%" + keyWord + "%') or upper(t.task_item_name) like upper('%" + keyWord + "%') )";
//            }
////            if (searchStatus != null && !searchStatus.isEmpty() && !searchStatus.equals("0")) {
////                condition += " and (t.check_way ='" + searchStatus + "' ) ";
////            }
//            if(StringUtils.isNotBlank(check_method)){
//                condition += " and t.check_method like '%"+check_method+"%' ";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            workTaskList = workTaskService.getWorkTaskList(schema_name, condition, pageSize, begin);
//            int total = workTaskService.getWorkTaskListCount(schema_name, condition);
//            result.put("rows", workTaskList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    //添加、编辑任务项
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/addAndUpdateWorkTask")
//    @Transactional
//    public ResponseModel addWorkTask(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            return workTaskService.addAndUpdateWorkTask(schema_name, request);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo(LangConstant.MSG_SUB_EXCEP));
//        }
//    }
//
//    //修改任务项
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/update-worktask")
//    public ResponseModel findworkbycode(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        try {
//            String task_item_code = request.getParameter("worktaskcode");
//            String schema_name = authService.getCompany(request).getSchema_name();
//            WorkTask worktask = workTaskService.findByID(schema_name, task_item_code);
//            // yzj 0830，清除customer表，导致bug，大家各自修改  --  修改好删除此注释
////            List<CustomersData> customerList = customerService.findAllCustomerList3(schema_name);
//            int code = 0;
//            String msg = "";
//            Object content = null;
//            if (worktask != null) {
//                code = StatusConstant.SUCCES_RETURN;
//                msg = selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_SU);
//                content = worktask;
//            } else {
//                code = StatusConstant.NO_DATE_RETURN;
//                msg = selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_DE);
//            }
//
//            return remodel(code, content, msg);
//        } catch (Exception ex) {
//            model.setCode(StatusConstant.ERROR_RETURN);
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SEACH_DE));
//            return model;
//        }
//
//
//    }
//
////    //修改任务
////    @RequestMapping("/updateWorkTask")
////    @Transactional
////    public ResponseModel updateWorkTask(HttpServletRequest request, HttpServletResponse response) {
////        ResponseModel model = new ResponseModel();
////        String schema_name = authService.getCompany(request).getSchema_name();
////        User user = authService.getLoginUser(request);
////
////        try {
////            String task_item_name = request.getParameter("task_item_name");//任务名称
////            String check_way = request.getParameter("check_way");//检查方法
////            String result_type = request.getParameter("result_type");//结果类型
////            String file_ids = request.getParameter("file_ids");//指导图片
////            String task_item_code = request.getParameter("task_item_code");//是否启用
////            String requirements = request.getParameter("requirements");//任务要求
////            String work_hours = request.getParameter("work_hours");//任务要求
////            String order = request.getParameter("order");//排序序号
////            if (work_hours == null || "".equals(work_hours)) {
////                work_hours = "0.0";
////            }
////            Timestamp createTimeStamp = new Timestamp(System.currentTimeMillis());
////            WorkTask workTask = new WorkTask();
////            workTask.setTask_item_code(task_item_code);
////            workTask.setCheck_way(Integer.parseInt(check_way));
////            workTask.setCreate_time(createTimeStamp);
////            workTask.setCreate_user_account(user.getAccount());
////            workTask.setFile_ids(file_ids);
////            workTask.setOrder(Integer.parseInt(order));
////            workTask.setIsuse(true);
////            workTask.setResult_type(Integer.parseInt(result_type));
////            workTask.setTask_item_name(task_item_name);
////            workTask.setRequirements(requirements);
////            workTask.setWork_hours(Float.parseFloat(work_hours));
////            int i = workTaskService.updateworktask(schema_name, workTask);
////            int s = 1;
////            if (i > 0 && s > 0) {
////                model = remodel(1, "", selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
////            } else if (i > 0 && s <= 0) {
////                model = remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
////            } else if (i <= 0 && s > 0) {
////                model = remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
////            } else {
////                model = remodel(1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
////            }
////            return model;
////        } catch (Exception e) {
////            model.setCode(-1);
////            model.setContent("");
////            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
////            return model;
////        }
////    }
//
//    //移动任务项
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/move-worktask")
//    public ResponseModel movebyCode(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        try {
//            //任务项编号
//            String task_item_code = request.getParameter("worktaskcode");
//            String schema_name = authService.getCompany(request).getSchema_name();
//            //移动方向
//            String direction = request.getParameter("direction");
//            WorkTask worktask = workTaskService.findByID(schema_name, task_item_code);
//            if (direction == null || direction.equals("") || worktask == null) {
//                model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_BUSINESS_NO_EMPTY));
//                return model;
//            }
//            List<WorkTask> list = new ArrayList<WorkTask>();
//            WorkTask worktasknow = null;
//            String condtion = "";
//            if (direction.equals("upper")) {
//                condtion = " and t.order<" + worktask.getOrder();
//                list = workTaskService.getAllTemplateTaskList(schema_name, condtion);
//                if (list.isEmpty() || list.size() == 0) {
//                    return remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_MOVE_UP));
//                }
//                Collections.sort(list, new Comparator<WorkTask>() {
//                    @Override
//                    public int compare(WorkTask o1, WorkTask o2) {
//                        //给查出来的list重新排序
//                        return o1.getOrder() - o2.getOrder();
//                    }
//                });
//                worktasknow = list.get(list.size() - 1);
//            } else {
//                condtion = " and t.order>" + worktask.getOrder();
//                list = workTaskService.getAllTemplateTaskList(schema_name, condtion);
//                if (list.isEmpty() || list.size() == 0) {
//                    model.setCode(-1);
//                    model.setContent("");
//                    model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_MOVE_DOWN));
//                    return remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_ERROR_MOVE_DOWN));
//                }
//                Collections.sort(list, new Comparator<WorkTask>() {
//                    @Override
//                    public int compare(WorkTask o1, WorkTask o2) {
//                        //给查出来的list重新排序
//                        return o1.getOrder() - o2.getOrder();
//                    }
//                });
//                worktasknow = list.get(0);
//            }
//            int oder = worktask.getOrder();
//            int ordernew = worktasknow.getOrder();
//            worktask.setOrder(worktasknow.getOrder());
//            worktasknow.setOrder(oder);
//            int i = 0;
//            int s = 0;
//            i = workTaskService.updateworktask(schema_name, worktasknow);
//            s = workTaskService.updateworktask(schema_name, worktask);
//            if (i > 0 && s > 0) {
//                model = remodel(1, "", selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//            } else if (i > 0 && s <= 0) {
//                model = remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            } else if (i <= 0 && s > 0) {
//                model = remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            } else {
//                model = remodel(1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            }
//            return model;
//        } catch (Exception e) {
//            model.setCode(-1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            return model;
//        }
//
//    }
//
//    //拖动换行
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/changeorder")
//    public ResponseModel chanageorder(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel model = new ResponseModel();
//        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//        String keyWord = request.getParameter("keyWord");
//        String beginTime = request.getParameter("beginTime");
//        String endTime = request.getParameter("endTime");
//        String jsondata = request.getParameter("jsondata");
//        List<WorkTask> tasklist = JSON.parseArray(jsondata, WorkTask.class);
//        System.out.println(jsondata);
//        List<WorkTask> workTaskList = null;
//        try {
//            User user = authService.getLoginUser(request);
//            //根据人员的角色，判断获取值的范围
//            String condition = "";
////            String account = user.getAccount();
////            Boolean isAllFacility = false;
////            Boolean isSelfFacility = false;
////            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, user.getId(), "repair");
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(t.task_item_code) like upper('%" + keyWord + "%')  )";
//            }
//            if (beginTime != null && !beginTime.isEmpty() && !beginTime.equals("")) {
//                Timestamp beginDate = Timestamp.valueOf(beginTime + " 00:00:00");
//                condition += " and t.create_time >='" + beginDate + "'";
//            }
//            if (endTime != null && !endTime.isEmpty() && !endTime.equals("")) {
//                Timestamp end = Timestamp.valueOf(endTime + " 23:59:59");
//                condition += " and t.create_time <'" + end + "'";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            WorkTask wot = null;
//            workTaskList = workTaskService.getWorkTaskList(schema_name, condition, pageSize, begin);
//            for (int i = 0; i < tasklist.size(); i++) {
//                wot = tasklist.get(i);
//                wot.setOrder(workTaskList.get(i).getOrder());
//
//                workTaskService.updateworktask(schema_name, wot);
//            }
//            model.setCode(1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SAVE_FAIL));
//            return model;
//        } catch (Exception ex) {
//            model.setCode(-1);
//            model.setContent("");
//            model.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            return model;
//        }
//
//
//    }
//
//    private ResponseModel remodel(int code, Object content, String msg) {
//        ResponseModel remodel = new ResponseModel();
//        remodel.setCode(code);
//        remodel.setMsg(msg);
//        remodel.setContent(content);
//        return remodel;
//
//
//    }
//    //禁用任务项
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/invalid-task")
//    @Transactional
//    public ResponseModel invalidTemplate(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel model = new ResponseModel();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User user = authService.getLoginUser(request);
//        try {
//            String task_item_code = request.getParameter("code");
//            Boolean isuse = Boolean.valueOf(request.getParameter("isuse"));
//            WorkTask workTask = workTaskService.findByID(schema_name, task_item_code);
//            workTask.setIsuse(isuse);
//            int i = workTaskService.updateworktask(schema_name, workTask);
//            if (i > 0) {
//                model = remodel(1, "", selectOptionService.getLanguageInfo( LangConstant.SAVED_A));
//            } else {
//                model = remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            }
//            return model;
//        } catch (Exception e) {
//            model = remodel(-1, "", selectOptionService.getLanguageInfo( LangConstant.MSG_SUB_EXCEP));
//            return model;
//        }
//    }
//
//    /**
//     * 跳转到任务项新增页面
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/toAddWorktask")
//    public ModelAndView toAddWorktask(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            List<BomData> bomList = bomService.getBomDataDistinctDistinct(schema_name);
//            return new ModelAndView("work_task/add_and_update_task").addObject("bomList", bomList);
//        } catch (Exception ex) {
//            return new ModelAndView("work_task/add_and_update_task");
//        }
//    }
//
//    /**
//     * 跳转到任务项编辑页面
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/toUpdateWorktask")
//    public ModelAndView toUpdateWorktask(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String taskItemCode = request.getParameter("taskItemCode");
//            List<BomData> bomList = bomService.getBomDataDistinctDistinct(schema_name);
//            return new ModelAndView("work_task/add_and_update_task")
//                    .addObject("bomList", bomList)
//                    .addObject("taskItemCode", taskItemCode);
//        } catch (Exception ex) {
//            return new ModelAndView("work_task/add_and_update_task");
//        }
//    }
//
//    /**
//     * 查询任务项-备件列表
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/find_task_item_bom_list")
//    public String findTaskItemBomList(HttpServletRequest request) {
//        JSONObject result = new JSONObject();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String taskItemCode = request.getParameter("taskItemCode");
//            if (StringUtils.isBlank(taskItemCode)) {
//                return "{}";
//            }
//            List<Map<String, Object>> taskItemBomList = workTaskService.queryTaskItemBomList(schema_name, taskItemCode);
//            result.put("rows", taskItemBomList);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     *  添加任务项备件
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/add_task_item_bom")
//    public ResponseModel addTaskItemBom(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            return workTaskService.addTaskItemBom(schema_name, request);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_AD_BOM_DE));//新增备件失败
//        }
//    }
//
//    /**
//     * 通过code查询任务项详情
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("getTaskItemDetailByCode")
//    public ResponseModel getTaskItemDetailByCode(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String taskItemCode = request.getParameter("taskItemCode");
//            WorkTask taskItem = workTaskService.getTaskItemDetailByCode(schema_name, taskItemCode);
//            if(taskItem != null)
//                return ResponseModel.ok(taskItem);
//
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.DATA_LOAD_WRONG));//数据加载失败
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.DATA_LOAD_WRONG));//数据加载失败
//        }
//    }
//
//    /**
//     * 删除任务项bom
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("task_item_bom_delete")
//    public ResponseModel deleteTaskItemBom(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String id = request.getParameter("id");
//            int result = workTaskService.deleteTaskItemBom(schema_name, Long.parseLong(id));
//            if(result > 0)
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));//删除成功
//
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));//删除失败
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));//删除失败
//        }
//    }
//
//    /**
//     * 查询任务项-工具列表
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/find_task_item_tool_list")
//    public String findTaskItemToolList(HttpServletRequest request) {
//        JSONObject result = new JSONObject();
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String taskItemCode = request.getParameter("taskItemCode");
//            if (StringUtils.isBlank(taskItemCode)) {
//                return "{}";
//            }
//            List<Map<String, Object>> taskItemToolList = workTaskService.queryTaskItemToolList(schema_name, taskItemCode);
//            result.put("rows", taskItemToolList);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    /**
//     *  添加任务项工具
//     * @param request
//     * @param response
//     * @return
//     */
//    @Transactional
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/add_task_item_tool")
//    public ResponseModel addTaskItemTool(HttpServletRequest request, HttpServletResponse response) {
//        //保存任务项备件
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return workTaskService.addTaskItemTool(schemaName, request);
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_AD_BOM_DE));//新增备件失败
//        }
//    }
//
//    /**
//     * 新增工具
//     * @param request
//     * @return
//     */
//    @Transactional
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("/doAddTool")
//    public ResponseModel doAddTool(HttpServletRequest request){
//        try {
//            String schemaName = authService.getCompany(request).getSchema_name();
//            return workTaskService.addTool(schemaName, request);
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_OPERATE_ERROR));//操作异常
//        }
//    }
//
//    /**
//     * 删除任务项工具
//     * @param request
//     * @return
//     */
//    @MenuPermission("workSheetManage_task")
//    @RequestMapping("task_item_tool_delete")
//    public ResponseModel deleteTaskItemTool(HttpServletRequest request) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            String id = request.getParameter("id");
//            int result = workTaskService.deleteTaskItemTool(schema_name, Long.parseLong(id));
//            if(result > 0)
//                return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_DEL_OK));//删除成功
//
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));//删除失败
//        } catch (Exception e) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.FAIL_DELE));//删除失败
//        }
//    }
//
//    /**
//     * 跳转到任务项详情页面
//     * @param request
//     * @return
//     */
//    @RequestMapping("/toWorktaskDetail")
//    public ModelAndView toWorktaskDetail(HttpServletRequest request) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String taskItemCode = request.getParameter("taskItemCode");
//            return new ModelAndView("work_task/detail")
//                    .addObject("taskItemCode", taskItemCode);
//        } catch (Exception ex) {
//            return new ModelAndView("work_task/detail");
//        }
//    }
}



