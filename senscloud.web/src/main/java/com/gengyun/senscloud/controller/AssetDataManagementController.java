package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.asset.AssetDataService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "设备管理")
@RestController
public class AssetDataManagementController {
    @Resource
    AssetDataService assetDataService;
    @Resource
    LogsService logsService;


    @ApiOperation(value = "获取设备模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_LIST_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/getAssetListPermission", method = RequestMethod.POST)
    public ResponseModel searchAssetListPermission(MethodParam methodParam) {
        return ResponseModel.ok(assetDataService.getAssetListPermission(methodParam));
    }

    @ApiOperation(value = "获取设备列表（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "pageSize", "pageNumber", "statusSearch", "keywordSearch", "positionsSearch", "assetTypesSearch", "assetModelSearch", "runStatusSearch", "customerSearch", "levelSearch", "startDateSearch", "endDateSearch", "warrantiesTypeSearch", "facilitiesSearch", "orgTypeSearch", "duplicateAssetSearch", "startCreateDateSearch", "endCreateDateSearch"})
    @RequestMapping(value = "/getAssetList", method = RequestMethod.POST)
    public ResponseModel searchAssetList(MethodParam methodParam, AssetSearchParam asParam) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(assetDataService.getAssetListForPage(methodParam, asParam));
    }

    @ApiOperation(value = "删除选中设备", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/deleteSelectAsset", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel deleteAssetBySelect(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        assetDataService.deleteAssetByIds(methodParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "展示选择设备二维码图片", notes = ResponseConstant.RSP_DESC_SHOW_ASSET_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/showAssetQRCode", method = RequestMethod.GET)
    public void showAssetQRCode(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        assetDataService.doShowAssetQRCode(methodParam);
    }

    @ApiOperation(value = "导出选中设备二维码图片", notes = ResponseConstant.RSP_DESC_EXPORT_ASSET_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/exportAssetQRCode", method = RequestMethod.POST)
    public void exportAssetQRCode(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        assetDataService.doExportAssetQRCode(methodParam, asParam);
    }

    @ApiOperation(value = "导出全部设备二维码图片", notes = ResponseConstant.RSP_DESC_EXPORT_ALL_ASSET_QR_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "statusSearch", "keywordSearch",
            "positionsSearch", "assetTypesSearch", "assetModelSearch", "runStatusSearch", "customerSearch", "levelSearch",
            "startDateSearch", "endDateSearch", "warrantiesTypeSearch", "facilitiesSearch", "orgTypeSearch", "duplicateAssetSearch"})
    @RequestMapping(value = "/exportAllAssetQRCode", method = RequestMethod.POST)
    public void exportAllAssetQRCode(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.doExportAssetQRCode(methodParam, asParam);
    }

    @ApiOperation(value = "获取设备导入模板文件码", notes = ResponseConstant.RSP_DESC_GET_IMPORT_ASSET_TEMPLATE_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "assetTypesSearch"})
    @RequestMapping(value = "/getImportAssetTemplateCode", method = RequestMethod.GET)
    public ResponseModel getImportAssetTemplateCode(MethodParam methodParam, AssetSearchParam asParam) {
        return ResponseModel.okExport(assetDataService.getImportAssetCode(methodParam, asParam));
    }

    @ApiOperation(value = "获取选中设备导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ASSET_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "ids"})
    @RequestMapping(value = "/getExportAssetCode", method = RequestMethod.POST)
    public ResponseModel getExportAssetCode(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String ids) {
        return ResponseModel.okExport(assetDataService.getExportAssetCode(methodParam, asParam));
    }

    @ApiOperation(value = "获取全部设备导出码", notes = ResponseConstant.RSP_DESC_GET_EXPORT_ALL_ASSET_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "statusSearch", "keywordSearch", "positionsSearch",
            "assetTypesSearch", "assetModelSearch", "runStatusSearch", "customerSearch", "levelSearch", "startDateSearch", "endDateSearch",
            "warrantiesTypeSearch", "facilitiesSearch", "orgTypeSearch", "duplicateAssetSearch"})
    @RequestMapping(value = "/getExportAllAssetCode", method = RequestMethod.POST)
    public ResponseModel getExportAllAssetCode(MethodParam methodParam, AssetSearchParam asParam) {
        return ResponseModel.okExport(assetDataService.getExportAssetCode(methodParam, asParam));
    }

    @ApiOperation(value = "新增设备", notes = ResponseConstant.RSP_DESC_ADD_ASSET)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, ignoreParameters = {"AddAsset"}, includeParameters = {"token", "asset_code", "asset_name", "position_code", "asset_model_id", "category_id", "importment_level_id", "enable_time", "use_year", "remark", "buy_date", "supplier_id", "manufacturer_id", "tax_rate", "price", "tax_price", "install_date", "install_price", "buy_currency_id", "install_currency_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAsset", method = RequestMethod.POST)
    public ResponseModel addAsset(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetAdd assetAdd) {
        assetDataService.doAddAsset(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑设备", notes = ResponseConstant.RSP_DESC_EDIT_ASSET)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, ignoreParameters = {"EditAsset"}, includeParameters = {"token", "properties", "id", "sectionType", "asset_code", "asset_name", "position_code", "asset_model_id", "category_id", "importment_level_id", "enable_time", "use_year", "remark", "buy_date", "supplier_id", "manufacturer_id", "tax_rate", "price", "tax_price", "install_date", "install_price", "buy_currency_id", "install_currency_id", "asset_icon", "running_status_id", "status", "parent_id", "guarantee_status_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/editAsset", method = RequestMethod.POST)
    public ResponseModel editAsset(MethodParam methodParam, AssetSearchParam asParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetInfo assetInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        assetDataService.doEditAsset(methodParam, asParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "设备地图打点", notes = ResponseConstant.RSP_DESC_EDIT_ASSET_MAP)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, ignoreParameters = {"EditAsset"}, includeParameters = {"token", "id", "location"})
    @Transactional //支持事务
    @RequestMapping(value = "/editAssetMap", method = RequestMethod.POST)
    public ResponseModel editAssetMap(MethodParam methodParam, AssetSearchParam asParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") AssetInfo assetInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        assetDataService.editAssetMap(methodParam, asParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取设备明细信息（含手机端）", notes = ResponseConstant.RSP_DESC_GET_ASSET_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getAssetInfo", method = RequestMethod.POST)
    public ResponseModel getAssetInfo(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetDetailById(methodParam, asParam));
    }

    @ApiOperation(value = "根据设备编码获取设备明细信息（含手机端）", notes = ResponseConstant.RSP_DESC_GET_ASSET_INFO_BY_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "codeSearch"})
    @RequestMapping(value = "/getAssetInfoByCode", method = RequestMethod.POST)
    public ResponseModel getAssetInfoByCode(MethodParam methodParam, AssetSearchParam asParam) {
        return ResponseModel.ok(assetDataService.getAssetDetailByCode(methodParam, asParam));
    }

    @ApiOperation(value = "根据设备编码获取设备主键（含手机端）", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_ID_BY_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "codeSearch"})
    @RequestMapping(value = "/searchAssetIdByCode", method = RequestMethod.POST)
    public ResponseModel searchAssetIdByCode(MethodParam methodParam, AssetSearchParam asParam) {
        return ResponseModel.ok(assetDataService.getAssetIdByCode(methodParam, asParam));
    }

    @ApiOperation(value = "获取设备下一步工作信息", notes = ResponseConstant.RSP_DESC_GET_ASSET_WORK_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getAssetWorkInfo", method = RequestMethod.POST)
    public ResponseModel getAssetWorkInfo(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetWorkInfo(methodParam));
    }

    @ApiOperation(value = "获取设备历史工作信息（含手机端）", notes = ResponseConstant.RSP_DESC_GET_ASSET_HISTORY_WORK)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch", "startDateSearch", "endDateSearch"})
    @RequestMapping(value = "/getAssetHistoryWork", method = RequestMethod.POST)
    public ResponseModel getAssetHistoryWork(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetHistoryWorkInfo(methodParam, asParam));
    }

    @ApiOperation(value = "获取设备附件文档信息", notes = ResponseConstant.RSP_DESC_GET_ASSET_FILE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getAssetFileList", method = RequestMethod.POST)
    public ResponseModel getAssetFileList(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetFileList(methodParam, asParam));
    }

    @ApiOperation(value = "获得设备所有的日志", notes = ResponseConstant.RSP_DESC_SEARCH_ASSET_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getAssetLogList", method = RequestMethod.POST)
    public ResponseModel getAssetLogList(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(logsService.getLog(methodParam, SensConstant.BUSINESS_NO_2000, methodParam.getDataId()));
    }

    @ApiOperation(value = "获取设备故障履历信息", notes = ResponseConstant.RSP_DESC_GET_ASSET_REPAIR_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getAssetRepairInfo", method = RequestMethod.POST)
    public ResponseModel getAssetRepairInfo(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getFaultRecordList(methodParam, asParam));
    }

    @ApiOperation(value = "设备附件删除", notes = ResponseConstant.RSP_DESC_ASSET_FILE_UPDATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "file_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetFile", method = RequestMethod.POST)
    public ResponseModel removeAssetFile(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.doModifyAssetFile(methodParam, asParam, false);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "设备附件新增", notes = ResponseConstant.RSP_DESC_ASSET_FILE_UPDATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "file_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetFile", method = RequestMethod.POST)
    public ResponseModel addAssetFile(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.doModifyAssetFile(methodParam, asParam, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "设备厂商新增", notes = ResponseConstant.RSP_DESC_ADD_ASSET_FACILITY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "org_id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetFacilities", method = RequestMethod.POST)
    public ResponseModel addAssetFacilities(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.doModifyAssetFacilities(methodParam, asParam, true);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获得设备厂商列表", notes = ResponseConstant.RSP_DESC_GET_ASSET_FACILITY_LIST_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getAssetFacilityList", method = RequestMethod.POST)
    public ResponseModel getAssetFacilityList(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetFacilityList(methodParam, asParam));
    }

    @ApiOperation(value = "设备厂商删除", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_FACILITY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "org_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetFacility", method = RequestMethod.POST)
    public ResponseModel removeAssetFacility(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.cutAssetFacility(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获得设备关联人员列表", notes = ResponseConstant.RSP_DESC_GET_ASSET_DUTY_MAN_LIST_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getAssetDutyManList", method = RequestMethod.POST)
    public ResponseModel getAssetDutyManList(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetDutyManList(methodParam, asParam));
    }

    @ApiOperation(value = "设备关联人员新增", notes = ResponseConstant.RSP_DESC_ADD_ASSET_DUTY_MAN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "user_id", "duty_id", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetDutyMan", method = RequestMethod.POST)
    public ResponseModel addAssetDutyMan(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.newAssetDutyMan(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "设备关联人员删除", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_DUTY_MAN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "duty_man_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetDutyMan", method = RequestMethod.POST)
    public ResponseModel removeAssetDutyMan(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.cutAssetDutyMan(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获得设备的备件列表", notes = ResponseConstant.RSP_DESC_GET_ASSET_BOM_LIST_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getAssetBomList", method = RequestMethod.POST)
    public ResponseModel getAssetBomList(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        return ResponseModel.ok(assetDataService.getAssetBomList(methodParam, asParam));
    }

    @ApiOperation(value = "设备关联备件新增", notes = ResponseConstant.RSP_DESC_ADD_ASSET_BOM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "bom_id", "id", "use_count"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetBom", method = RequestMethod.POST)
    public ResponseModel addAssetBom(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.newAssetBom(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "设备关联备件删除", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_BOM)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "asset_bom_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetBom", method = RequestMethod.POST)
    public ResponseModel removeAssetBom(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.removeAssetBom(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获得设备的备件使用记录列表", notes = ResponseConstant.RSP_DESC_GET_BOM_USAGE_RECORD_LIST_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "bom_id"})
    @RequestMapping(value = "/getBomUsageRecordList", method = RequestMethod.POST)
    public ResponseModel getBomUsageRecordList(MethodParam methodParam, AssetSearchParam asParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String bom_id) {
        return ResponseModel.ok(assetDataService.getBomUsageRecordList(methodParam, bom_id));
    }

    @ApiOperation(value = "子设备新增", notes = ResponseConstant.RSP_DESC_ADD_ASSET_PARENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "select_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/addAssetParent", method = RequestMethod.POST)
    public ResponseModel addAssetParent(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.newAssetParent(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "子设备删除", notes = ResponseConstant.RSP_DESC_REMOVE_ASSET_PARENT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "select_id", "id"})
    @Transactional //支持事务
    @RequestMapping(value = "/removeAssetParent", method = RequestMethod.POST)
    public ResponseModel removeAssetParent(MethodParam methodParam, AssetSearchParam asParam) {
        assetDataService.removeAssetParent(methodParam, asParam);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "查询子设备列表", notes = ResponseConstant.RSP_DESC_GET_ASSET_PARENT_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id", "keywordSearch"})
    @RequestMapping(value = "/getAssetParentList", method = RequestMethod.POST)
    public ResponseModel getAssetParentList(MethodParam methodParam, AssetSearchParam asParam) {
        return ResponseModel.ok(assetDataService.getAssetParentList(methodParam, asParam));
    }

    @ApiOperation(value = "获得备件导入记录列表", notes = ResponseConstant.RSP_DESC_GET_BOM_IMPORT_RECORD_LIST_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "bom_id"})
    @RequestMapping(value = "/getBomImportRecordList", method = RequestMethod.POST)
    public ResponseModel getBomImportRecordList(MethodParam methodParam, BomSearchParam bsParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String bom_id) {
        return ResponseModel.ok(assetDataService.getBomImportRecordList(methodParam, bom_id));
    }

    @ApiOperation(value = "获得特定设备类型", notes = ResponseConstant.RSP_DESC_GET_SPECIAL_ASSET_TYPE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token","asssetTypeTag"})
    @RequestMapping(value = "/getSpecialAssetType", method = RequestMethod.POST)
    public ResponseModel getSpecialAssetType(MethodParam methodParam, AssetSearchParam asParam) {
        return ResponseModel.ok(assetDataService.getAssetTypeIds(methodParam, asParam));
    }

}