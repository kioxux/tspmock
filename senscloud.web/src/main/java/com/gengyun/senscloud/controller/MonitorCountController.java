package com.gengyun.senscloud.controller;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.common.SensConstant;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.MonitorCountService;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.business.UserService;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/monitor_counter")
public class MonitorCountController {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    MonitorCountService monitorCountService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @Autowired
//    UserService userService;
//
//    @RequestMapping({"/", "/index"})
//    public ModelAndView monitorCount(HttpServletRequest request, HttpServletResponse response) {
//        return new ModelAndView("monitor_count/index");
//    }
//    /**
//     * 按条件查询用量列表
//     */
//    @RequestMapping("/find_monitor_countlist")
//    public String queryworksheet(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject result = new JSONObject();
//        String schema_name = authService.getCompany(request).getSchema_name();
//        try {
//            Map<String, String[]> requestMap = request.getParameterMap();
//            String keyWord = request.getParameter("keyWord");
//            String timecondition = request.getParameter("timeCondition");
//            String facilityId = request.getParameter("facilityId");
//            String data_type = request.getParameter("data_type");
//            String[] facilityIdArray = null;
//            if(StringUtils.isNotBlank(facilityId)){
//                facilityIdArray = facilityId.split(",");
//            }
//            int pageSize = Integer.parseInt(request.getParameter("pageSize"));
//            int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
//            String condition = "";
//            String timecondition1="";
//            String timecondition2="";
//            String timecondition3="";
//            if(null!=data_type&&!"0".equals(data_type)){
//                SimpleDateFormat stf=new SimpleDateFormat(Constants.DATE_FMT);
//                String nowYear= stf.format(new Date());
//                if(null!=timecondition||timecondition.equals(nowYear)){
//                    timecondition1+="and substring(m.gather_time from 1 for 7)<(to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')) ";
//                    timecondition2+=" and substring(m.gather_time from 1 for 4)=(to_char(now() , '" + SqlConstant.SQL_DATE_FMT_YEAR + "')) ";
//                    timecondition3+="and h.mon=to_number((to_char(now(), 'MM')), '99')-1";
//                }else{
//                    timecondition1+="and substring(m.gather_time from 1 for 4)<'"+(Integer.parseInt(timecondition)+1)+"'";
//                    timecondition2+=" and substring(m.gather_time from 1 for 4)='"+timecondition+"'";
//                    timecondition3+="and h.mon=12";
//                }
//                condition += "and a.strcode NOT IN (select h.asset_code from(select count(1) as mon,s.asset_code from " +
//                        "(select DISTINCT m.asset_code,m.gather_time,substring(m.gather_time from 1 for 7) as times, " +
//                        "substring(m.gather_time from 1 for 4)as years " +
//                        "from "+schema_name+"._sc_asset_monitor_current m " +
//                        "where m.monitor_value_type=20 " + timecondition1 + timecondition2 +
//                        "GROUP BY m.asset_code,m.gather_time)s " +
//                        "GROUP BY s.asset_code)h " +
//                        "where 1=1 " + timecondition3+")";
//            }
//            if (keyWord != null && !keyWord.isEmpty()) {
//                condition += " and (upper(a.strcode) like upper('%" + keyWord + "%') or upper(a.strname) like upper('%" + keyWord + "%'))";
//            }
//            if(facilityIdArray != null && facilityIdArray.length > 0 && !(facilityIdArray.length == 1 &&"-1".equals(facilityIdArray[0]))) {
//                condition += " and (";
//                for (String facilityid : facilityIdArray) {
//                    condition += " f.facility_no like '" + facilityid + "%' or a.position_code like '" + facilityid + "%' or";
//
//                }
//                condition = condition.substring(0, condition.length() - 2);
//                condition += ") ";
//            }
//            User user=AuthService.getLoginUser(request);
//            //全部位置 所在位置权限判断来源于 设备的权限
//            String rootPermission="asset_data_management";
//            boolean isAll = userService.isAllUserFunctionPermission(schema_name, user.getId(), rootPermission);
//            //若没有所有位置的权限且没有筛选的组织根据用户的权限 筛选所能看见得设备
//            if(!isAll&&RegexUtil.isNull(facilityId)){
//                condition+="AND f.id in(select f.id from "+schema_name+"._sc_facilities f " +
//                        "LEFT JOIN "+schema_name+"._sc_group_org gr on gr.org_id=f.id " +
//                        "LEFT JOIN "+schema_name+"._sc_user_group up on up.group_id=gr.group_id " +
//                        "WHERE up.user_id='"+user.getId()+"')";
//            }
//            int begin = pageSize * (pageNumber - 1);
//            List<Map<String, Object>> facilitiesList = monitorCountService.getAssetMonitorCountList(schema_name, condition, timecondition, pageSize, begin);
//            int total = monitorCountService.getAssetMonitorCountListCount(schema_name, condition, timecondition);
//            result.put("rows", facilitiesList);
//            result.put("total", total);
//            return result.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//    /**
//     * 添加计数器的界面
//     */
//    @RequestMapping("/add_monitor_count")
//    public ModelAndView AddMonitorCountDataView(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String asset_id = request.getParameter("asset_id");
//        String asset_code = request.getParameter("asset_code");
//        String facility_id = request.getParameter("facility_id");
//        String condition = "";
//        if (asset_id != null && !asset_id.isEmpty()) {
//            condition += " and a._id='" + asset_id + "'";
//            List<String> facilitiesList = monitorCountService.getOraganzationList(schema_name, condition);
//            return new ModelAndView("monitor_count/edit_monitor_count").addObject("asset_id", asset_id).addObject("asset_code", asset_code).addObject("facilitiesList", JSONArray.toJSONString(facilitiesList));
//        }else if(StringUtils.isNotBlank(facility_id)) {
//            return new ModelAndView("monitor_count/edit_monitor_count").addObject("facilitiesList", JSONArray.toJSONString(Arrays.asList(facility_id)));
//        }
//        return new ModelAndView("monitor_count/edit_monitor_count");
//    }
//    /**
//     * 按照选中组织找设备
//     */
//    @RequestMapping("/find_asset_by_organization")
//    public String findAssetByOrganization(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            String facilityids = (String)request.getParameter("facilityId");
//            String condition = "";
//            if (facilityids != null && !facilityids.isEmpty()) {
//                condition += " AND CAST(f.id AS VARCHAR) ='" + facilityids + "' OR a.position_code = '" + facilityids + "' ";
//                List<Map<String, Object>> facilitiesList = monitorCountService.getAssetList(schema_name, condition);
//                return JSONArray.toJSONString(facilitiesList);
//            }
//            return JSONArray.toJSONString("");
//        } catch (Exception e) {
//            return "{}";
//        }
//    }
//    /**
//     * 按设备查找监控记录
//     */
//    @RequestMapping("/find_monitor_count_by_id")
//    public String findMonitorCountByID(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        Map<String, String[]> requestMap = request.getParameterMap();
//        String asset_id = request.getParameter("asset_id");
//        String timecondition = request.getParameter("timeCondition");
//
//        String condition = "";
//        if (asset_id != null && !asset_id.isEmpty()) {
//            condition += " and a._id ='" + asset_id + "'";
//            List<Map<String, Object>> facilitiesList = monitorCountService.getAssetMonitorCountList(schema_name, condition, timecondition, 1, 0);
//            return JSONArray.toJSONString(facilitiesList);
//        }
//        return "{}";
//    }
//
//    /**
//     * 插入监控记录
//     */
//    @RequestMapping("/add_monitor")
//    public String AddMonitorCountData(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        String asset_code = request.getParameter("assetCode");
//        String remark = request.getParameter("remark");
//        String liststr = request.getParameter("timeValue");
//        List<Map<String, Object>> datalist = (List) JSONArray.parseArray(liststr);
//        try {
//            for (Map<String, Object> data : datalist) {
//                String newTime = String.valueOf(data.get("count_time"));
//                monitorCountService.deleteMonitorCountData(schema_name, asset_code, newTime);
//                data.put("asset_code", asset_code);
//                data.put("monitor_name", "Month_Dosing_Accumulate_Total");
//                data.put("count_time", newTime);
//                data.put("remark", remark);
//                data.put("schema_name", schema_name);
//                data.put("monitor_value_type", 20);
//                data.put("source_type", SensConstant.DOSAGE_SUPPLEMENT);
//                data.put("create_time", new Timestamp(System.currentTimeMillis()));
//                monitorCountService.InsertMonitorCountData(data);
//            }
//            return "ok";
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "error";
//        }
//    }
////    //导出设备的监控数据
////    @RequestMapping("/findAssetpatch")
////    public ResponseModel findAssetpatch(HttpServletRequest request, HttpServletResponse response){
////        String schema_name = authService.getCompany(request).getSchema_name();
////        Map<String, String[]> requestMap = request.getParameterMap();
////        String[] facility_id = requestMap.get("facilityId[]");
////        String timecondition = request.getParameter("timeCondition");
////        String keyWord = request.getParameter("keyWord");
////        SimpleDateFormat stf=new SimpleDateFormat(Constants.DATE_FMT);
////        String nowYear= stf.format(new Date());
////        String facilityids = "";
////        String condition = "";
////        String timecondition1="";
////        String timecondition2="";
////        String timecondition3="";
////        try {
////            if(facility_id != null) {
////                for (String facilityid : facility_id) {
////                    facilityids += "'" + facilityid + "',";
////                }
////            }
////            if(null!=timecondition||timecondition.equals(nowYear)){
////                timecondition1+="and substring(m.gather_time from 1 for 7)<(to_char(now(), '" + SqlConstant.SQL_DATE_FMT_YEAR_MON_2 + "')) ";
////                timecondition2+=" and substring(m.gather_time from 1 for 4)=(to_char(now() , '" + SqlConstant.SQL_DATE_FMT_YEAR + "')) ";
////                timecondition3+="and h.mon=to_number((to_char(now(), 'MM')), '99')-1";
////            }else{
////                timecondition1+="and substring(m.gather_time from 1 for 4)<'"+(Integer.parseInt(timecondition)+1)+"'";
////                timecondition2+=" and substring(m.gather_time from 1 for 4)='"+timecondition+"'";
////                timecondition3+="and h.mon=12";
////            }
////            if (keyWord != null && !keyWord.isEmpty()) {
////                condition += " and (upper(a.strcode) like upper('%" + keyWord + "%') or upper(a.strname) like upper('%" + keyWord + "%'))";
////            }
////
////            if ( null!=facilityids  && !"".equals(facilityids) && !"'-1',".equals(facilityids)) {
////                facilityids = facilityids.substring(0, facilityids.length() - 1);
////                condition += " and ( CAST(f.id AS VARCHAR) in(" + facilityids + ") or CAST(f.parentid AS VARCHAR) in(" + facilityids + ") or A.position_code in (" + facilityids + ") )";
////            }
//////            List<Map<String, Object>> AssetList = monitorCountService.findAssetpatch(schema_name, condition,timecondition1,timecondition2,timecondition3);
////            List<Map<String, Object>> AssetList = monitorCountService.findFacilityFromAssetpatch(schema_name, condition,timecondition1,timecondition2,timecondition3);
////            return ResponseModel.ok(AssetList);
////
////        } catch (Exception e) {
////            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
////            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
////        }
////
////    }

}



