package com.gengyun.senscloud.controller;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.model.*;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.WorkColumnService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WorkTemplateService;
import com.gengyun.senscloud.util.RegexUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "工单模板")
@RestController
public class WorkTemplateController {

    @Resource
    WorkTemplateService workTemplateService;
    @Resource
    SelectOptionService selectOptionService;

    @Resource
    WorkColumnService workColumnService;

    @ApiOperation(value = "获取工单模板列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_TEMPLATE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch"}) //"pageSize", "pageNumber",
    @RequestMapping(value = "/searchWorkTemplateList", method = RequestMethod.POST)
    public ResponseModel searchWorkTemplateList(MethodParam methodParam, WorkTemplateSearchParam param) {
        return ResponseModel.ok(workTemplateService.getWorkTemplateListForPage(methodParam, param));
    }

    @ApiOperation(value = "新增模板基础信息", notes = ResponseConstant.RSP_DESC_ADD_WORK_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token", "work_template_name", "remark"})
    @Transactional //支持事务
    @RequestMapping(value = "/addWorkTemplate", method = RequestMethod.POST)
    public ResponseModel addWorkTemplate(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateAdd workTemplateAdd ) {
        return ResponseModel.ok(workTemplateService.newWorkTemplate(methodParam, paramMap));
    }

    @ApiOperation(value = "获取模板基本信息", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_template_code"})
    @RequestMapping(value = "/searchWorkTemplateInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkTemplateInfo(MethodParam methodParam, WorkTemplateSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_template_code) {
        return ResponseModel.ok(workTemplateService.getWorkTemplateInfoByWorkTemplateCode(methodParam, bParam));
    }

    @ApiOperation(value = "获取模板字段信息列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_template_code","keywordSearch","field_view_types"})
    @RequestMapping(value = "/searchWorkTemplateColumnList", method = RequestMethod.POST)
    public ResponseModel searchWorkTemplateColumnList(MethodParam methodParam, WorkTemplateSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_template_code) {
        return ResponseModel.ok(workTemplateService.getWorkTemplateColumnListByWorkTemplateCode(methodParam, bParam));
    }

    @ApiOperation(value = "删除工单模板", notes = ResponseConstant.RSP_DESC_REMOVE_WORK_TEMPLATE_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_template_code"})
    @RequestMapping(value = "/removeSelectWorkTemplate", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectWorkTemplate(MethodParam methodParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String work_template_code) {
        workTemplateService.cutWorkTemplateByWorkTemplateCode(methodParam,work_template_code);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "新增工单模板字段", notes = ResponseConstant.RSP_DESC_ADD_WORK_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"add"},includeParameters = {"token", "field_form_codes", "work_template_code"})
    @Transactional //支持事务
    @RequestMapping(value = "/addWorkTemplateColumn", method = RequestMethod.POST)
    public ResponseModel addWorkTemplateColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateColumnAdd add ) {
        workTemplateService.newWorkTemplateColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "是否处理工单模板字段", notes = ResponseConstant.RSP_DESC_ADD_WORK_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,includeParameters = {"token", "field_form_codes", "work_template_code"})
    @Transactional //支持事务
    @RequestMapping(value = "/isHandleWorkTemplateColumn", method = RequestMethod.POST)
    public ResponseModel isHandleWorkTemplateColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateColumnAdd add) {
        workTemplateService.isHandleWorkTemplateColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "删除工单模板字段", notes = ResponseConstant.RSP_DESC_REMOVE_WORK_TEMPLATE_COLUMN_BY_SELECT)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_template_code","field_form_code"})
    @RequestMapping(value = "/removeSelectWorkTemplateColumn", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel removeSelectWorkTemplateColumn(MethodParam methodParam,WorkTemplateSearchParam param, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String work_template_code,
                                                        @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String field_form_code) {
        workTemplateService.cutWorkTemplateColumnByWorkTemplateCode(methodParam,param);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "编辑工单模板基本信息", notes = ResponseConstant.RSP_DESC_EDIT_WORK_TEMPLATE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workInfo"},includeParameters = {"token", "work_template_code", "remark", "work_template_name"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkTemplate", method = RequestMethod.POST)
    public ResponseModel editWorkTemplate(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateAdd workInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_template_code) {
        workTemplateService.modifyWorkTemplate(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }
    @ApiOperation(value = "编辑工单模板字段", notes = ResponseConstant.RSP_DESC_EDIT_WORK_TEMPLATE_COLUMN)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnInfo"},includeParameters = {"token","field_value","data_key", "id","work_template_code", "field_form_code", "field_code", "field_name", "field_right", "is_required", "field_remark", "field_view_type", "field_section_type", "extList","linkList"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkTemplateColumn", method = RequestMethod.POST)
    public ResponseModel editWorkTemplateColumn(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateColumnAdd workColumnInfo, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String id) {
        workTemplateService.modifyWorkTemplateColumn(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "获取工单模板字段明细信息", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_COLUMN_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_template_code", "field_form_code"})
    @RequestMapping(value = "/searchWorkTemplateColumnInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkTemplateColumnInfo(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam String work_template_code,
                                                      @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String field_form_code) {
        return ResponseModel.ok(workTemplateService.getWorkTemplateColumnInfo(methodParam, bParam));
    }

    @ApiOperation(value = "获取工单模板字段关联条件明细信息", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_COLUMN_LINKAGE_CONDITION_INFO)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchWorkTemplateColumnLinkageInfo", method = RequestMethod.POST)
    public ResponseModel searchWorkTemplateColumnLinkageInfo(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam Integer id) {
        return ResponseModel.ok(workTemplateService.getWorkTemplateColumnLinkageInfo(methodParam, bParam));
    }


    @ApiOperation(value = "克隆字段", notes = ResponseConstant.RSP_DESC_GET_WORK_TEMPLATE_COLUMN_CLONE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "work_template_code"})
    @RequestMapping(value = "/addWorkTemplateClone", method = RequestMethod.POST)
    @Transactional //支持事务
    public ResponseModel addWorkTemplateClone(MethodParam methodParam, WorkColumnSearchParam bParam, @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam String work_template_code) {
        return ResponseModel.ok(workTemplateService.newWorkTemplateClone(methodParam, bParam));
    }

//    @ApiOperation(value = "根据字段主键获取属性值及字段控件类型", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_TEMPLATE_COLUMN_SPECIAL_PROPERTY)
//    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
//    @RequestMapping(value = "/searchWorkTemplateColumnSpecialProperty", method = RequestMethod.POST)
//    public ResponseModel searchWorkTemplateColumnSpecialProperty(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam Integer id) {
//        return ResponseModel.ok(workTemplateService.getWorkTemplateColumnSpecialProperty(methodParam, bParam));
//    }

    @ApiOperation(value = "根据字段id获取基本及特殊属性", notes = ResponseConstant.RSP_DESC_SEARCH_COLUMN_PROPERTY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchColumnProperty", method = RequestMethod.POST)
    public ResponseModel searchColumnProperty(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_IDS, required = true) @RequestParam Integer id) {
        return ResponseModel.ok(workTemplateService.getColumnProperty(methodParam, bParam));
    }

    @ApiOperation(value = "根据控件类型获取特殊属性key和value", notes = ResponseConstant.RSP_DESC_SEARCH_COLUMN_SPECIAL_PROPERTY)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectKey"})
    @RequestMapping(value = "/searchSpecialPropertyByWidgetType", method = RequestMethod.POST)
    public ResponseModel searchSpecialPropertyByWidgetType(MethodParam methodParam, WorkTemplateSearchParam bParam,@SuppressWarnings("unused") @ApiParam(value = "控件类型", required = true) @RequestParam String selectKey) {
        return ResponseModel.ok(workTemplateService.getSpecialPropertyByWidgetType(methodParam, bParam));
    }

    @ApiOperation(value = "根据code获取静态字典参数数据", notes = ResponseConstant.RSP_DESC_SEARCH_SELECT_OPTION_LIST_BY_KEY_AND_CODE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "selectValue"}, ignoreParameters = {"SearchSelectOptionList"})
    @RequestMapping(value = "/getOptionInfoByKeyAndCode", method = RequestMethod.POST)
    public ResponseModel searchSelectOptionInfoByKeyAndCode(MethodParam methodParam, SelectModel selectModel, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(selectOptionService.getSelectOptionInfoByKeyAndCode(methodParam, selectModel.getSelectValue()));
    }

    @ApiOperation(value = "查询新增字段查询字段库字段列表", notes = ResponseConstant.RSP_DESC_SEARCH_WORK_COLUMN_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "keywordSearch","work_template_code" })
    @RequestMapping(value = "/searchWorkColumnListForTemplate", method = RequestMethod.POST)
    public ResponseModel searchWorkColumnListForTemplate(MethodParam methodParam, WorkColumnSearchParam param) {
        return ResponseModel.ok(workColumnService.getWorkColumnListForTemplate(methodParam, param));
    }

    @ApiOperation(value = "编辑模板字段是否显示、上移、下移信息", notes = ResponseConstant.RSP_DESC_EDIT_WORK_TEMPLATE_COLUMN_USE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,ignoreParameters = {"workColumnAdd"},includeParameters = {"token", "id", "field_is_show","type"})
    @Transactional //支持事务
    @RequestMapping(value = "/editWorkTemplateColumnUse", method = RequestMethod.POST)
    public ResponseModel editWorkTemplateColumnUse(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") WorkTemplateColumnAdd workTemplateAdd,
                                                   @SuppressWarnings("unused") @ApiParam(value = ResponseConstant.DESC_ID, required = true) @RequestParam Integer id) {
        if(RegexUtil.optIsPresentStr(workTemplateAdd.getType()) && ("4".equals(workTemplateAdd.getType())||"5".equals(workTemplateAdd.getType()))){
            workTemplateService.modifyWorkTemplateOrder(methodParam, paramMap);
        }else {
            workTemplateService.modifyWorkTemplateColumnUse(methodParam, paramMap);
        }
        return ResponseModel.okMsgForOperate();
    }

}
