package com.gengyun.senscloud.controller.pollute;

import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.ResponseConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PolluteFeeModel;
import com.gengyun.senscloud.response.ResponseModel;
import com.gengyun.senscloud.service.pollute.PolluteFeeService;
import com.gengyun.senscloud.service.system.LogsService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "缴费管理")
@RestController
public class PolluteFeeConstroller {

    @Resource
    PolluteFeeService polluteFeeService;

    @Resource
    LogsService logsService;

    @ApiOperation(value = "获取缴费模块权限", notes = ResponseConstant.RSP_DESC_SEARCH_POLLUTE_FEE_PERMISSION)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token"})
    @RequestMapping(value = "/searchPolluteFeePermission", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeePermission(MethodParam methodParam) {
        return ResponseModel.ok(polluteFeeService.getPolluteFeePermission(methodParam));
    }

    @ApiOperation(value = "查询缴费记录列表", notes = ResponseConstant.RSP_DESC_SEARCH_POLLUTE_FEE_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "pageSize", "pageNumber", "keywordSearch", "title", "inner_code", "status", "begin_date", "end_date"})
    @RequestMapping(value = "/searchPolluteFeeList", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PolluteFeeModel model) {
        methodParam.setNeedPagination(true);
        return ResponseModel.ok(polluteFeeService.findPolluteFeeList(methodParam, paramMap));
    }

    @ApiOperation(value = "客户缴费统计信息", notes = ResponseConstant.RSP_DESC_SEARCH_POLLUTE_FEE_STATISTICS)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "begin_date", "end_date"})
    @RequestMapping(value = "/searchPolluteFeeStatistics", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeStatistics(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PolluteFeeModel model) {
        return ResponseModel.ok(polluteFeeService.findPolluteFeeStatistics(methodParam, paramMap));
    }

    @ApiOperation(value = "缴费日历", notes = ResponseConstant.RSP_DESC_CHECK_METER_CALENDAR)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "begin_date", "end_date"})
    @RequestMapping(value = "/searchPolluteFeeCalendar", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeCalendar(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PolluteFeeModel model) {
        return ResponseModel.ok(polluteFeeService.findsearchPolluteFeeCalendar(methodParam, paramMap));
    }

    @ApiOperation(value = "查询缴费明细", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_DETAIL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @RequestMapping(value = "/searchPolluteFeeDetailInfo", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeDetailInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap, @SuppressWarnings("unused") PolluteFeeModel model) {
        return ResponseModel.ok(polluteFeeService.findPolluteFeeDetailInfo(methodParam, paramMap));
    }

    @ApiOperation(value = "编辑缴费信息", notes = ResponseConstant.RSP_DESC_EDIT_POLLUTE_FEE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "final_amount", "remark", "isSubmit"})
    @Transactional //支持事务
    @RequestMapping(value = "/editPolluteFee", method = RequestMethod.POST)
    public ResponseModel editPolluteFee(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        polluteFeeService.modifyPolluteFee(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "审核缴费信息", notes = ResponseConstant.RSP_DESC_EDIT_POLLUTE_FEE)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id", "pass(1:通过;0:不通过)"})
    @RequestMapping(value = "/auditPolluteFee", method = RequestMethod.POST)
    public ResponseModel auditPolluteFee(MethodParam methodParam, PolluteFeeModel model) {
        polluteFeeService.auditPolluteFee(methodParam, model);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "自动计费周期设置", notes = ResponseConstant.RSP_DESC_AUTO_CHECK_METER_SETING)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "id"})
    @Transactional
    @RequestMapping(value = "/autoPolluteFeeSeting", method = RequestMethod.POST)
    public ResponseModel autoPolluteFeeSeting(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        polluteFeeService.autoPolluteFeeSeting(methodParam, paramMap);
        return ResponseModel.okMsgForOperate();
    }

    @ApiOperation(value = "自动计费周期查询", notes = ResponseConstant.RSP_DESC_AUTO_CHECK_METER_SEARCH)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token", "group_title"})
    @RequestMapping(value = "/autoPolluteFeeSearch", method = RequestMethod.POST)
    public ResponseModel searchPolluteFeeSetingInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(polluteFeeService.findPolluteFeeSetingInfo(methodParam, paramMap));
    }

    @ApiOperation(value = "获得缴费记录所有的日志", notes = ResponseConstant.RSP_DESC_SEARCH_POLLUTE_FEE_LOG_LIST)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR, includeParameters = {"token", "id"})
    @RequestMapping(value = "/getPolluteFeeLogList", method = RequestMethod.POST)
    public ResponseModel getPolluteFeeLogList(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(logsService.getLogByLogType(methodParam, SensConstant.BUSINESS_NO_6004, paramMap));
    }

    @ApiOperation(value = "重新计算缴费信息", notes = ResponseConstant.RSP_DESC_SEARCH_CHECK_METER_DETAIL)
    @ApiOperationSupport(author = Constants.JAVA_AUTHOR,
            includeParameters = {"token"})
    @RequestMapping(value = "/recalculatePolluteFeeDetailInfo", method = RequestMethod.POST)
    public ResponseModel recalculatePolluteFeeDetailInfo(MethodParam methodParam, @RequestParam Map<String, Object> paramMap) {
        return ResponseModel.ok(polluteFeeService.recalculatePolluteFeeDetailInfo(methodParam, paramMap));
    }
}
