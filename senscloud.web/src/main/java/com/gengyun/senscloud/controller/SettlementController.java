package com.gengyun.senscloud.controller;

//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.*;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.*;
//import com.gengyun.senscloud.util.Constants;
//import com.github.pagehelper.Page;
//import com.github.pagehelper.PageHelper;
//import org.apache.commons.lang.StringUtils;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.transaction.interceptor.TransactionAspectSupport;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.math.BigDecimal;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.UUID;
//
//@RestController
public class SettlementController {
//    @Autowired
//    AuthService authService;
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    FacilitiesService facilitiesService;
//
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @Autowired
//    SettlementService settlementService;
//
//    @Autowired
//    SelectOptionService selectOptionService;
//
//    @RequestMapping("/settlement-management")
//    public ModelAndView settlementManagement(HttpServletRequest request, HttpServletResponse response) {
//        String client_customer_code = "";
//        User loginUser = AuthService.getLoginUser(request);
//        String status = request.getParameter("status");
//        String facilitiesSelect = "";
////        Boolean exportFlag = false;
////        Boolean invalidFlag = false;
////        Boolean distributeFlag = false;
//        Boolean addFlag = false;
//        Boolean auditpcFlag = false;
//        Boolean dealpcFlag = false;
////        Boolean applyForBomFlag = false;//申领备件状态
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            client_customer_code = authService.getCompany(request).getClient_customer_code();
//            ResponseModel result = new ResponseModel();
//
//            //按当前登录用户，设备管理的权限，判断可看那些位置
//            Boolean isAllFacility = false;
//            List<UserFunctionData> userFunctionList = userService.getUserFunctionPermission(schema_name, loginUser.getId(), "settlement");
//            if (userFunctionList != null || !userFunctionList.isEmpty()) {
////                for (UserFunctionData functionData : userFunctionList) {
//                //如果可以查看全部位置，则不限制
////                    if (functionData.getFunctionName().equals("settlement_all_facility")) {
////                        isAllFacility = true;
////                    } else if (functionData.getFunctionName().equals("settlement_export")) {
////                        exportFlag = true;
////                    } else if (functionData.getFunctionName().equals("settlement_invalid")) {
////                        invalidFlag = true;
////                    } else if (functionData.getFunctionName().equals("settlement_distribute")) {
////                        distributeFlag = true;
////                    } else if (functionData.getFunctionName().equals("settlement_add")) {
//                addFlag = true;
////                    } else if (functionData.getFunctionName().equals("settlement_audit_pc")) {
//                auditpcFlag = true;
////                    } else if (functionData.getFunctionName().equals("settlement_do_pc")) {
//                dealpcFlag = true;
////                    }
////                }
//            }
//
//            //获取权限树
//            facilitiesSelect = facilitiesService.getFacilityRootByUser(schema_name, isAllFacility, loginUser);
//            SystemConfigData dataList = systemConfigService.getSystemConfigData(schema_name, Constants.APPLY_FOR_BOM);
//            if(dataList!=null){
//                if(dataList.getSettingValue().equals("1")){
////                    applyForBomFlag = true;
//                }
//            }
//        } catch (Exception ex) {
//            facilitiesSelect = "";
//        }
//        return new ModelAndView("settlement/settlement_list")
//                .addObject("facilitiesSelect", facilitiesSelect)
////                .addObject("exportFlag", exportFlag)
//                .addObject("rpAddFlag", addFlag)
////                .addObject("rpInvalidFlag", invalidFlag)
////                .addObject("rpDistributeFlag", distributeFlag)
//                .addObject("rpAuditpcFlag", auditpcFlag).addObject("rpDealpcFlag", dealpcFlag)
//                .addObject("currentUser", loginUser.getAccount())
//                .addObject("departmentname",loginUser.getDepartmentName())
////                .addObject("client_customer_code",client_customer_code)
////                .addObject("customerList", customerList)
//                .addObject("searchStatus", status);
////                .addObject("applyForBomFlag",applyForBomFlag)
//    }
//
//    @RequestMapping("/find-settlement-list")
//    public String getSettlementList(@RequestParam(name = "title", required = false) String title,
//                                    @RequestParam(name = "pageSize", required = false) Integer pageSize,
//                                    @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
//                                    @RequestParam(name = "sortName", required = false) String sortName,
//                                    @RequestParam(name = "sortOrder", required = false) String sortOrder,
//                                    HttpServletRequest request, HttpServletResponse response) {
//        try {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            if (pageNumber == null) {
//                pageNumber = 1;
//            }
//            if (pageSize == null) {
//                pageSize = 20;
//            }
//            String orderBy = null;
//            if (StringUtils.isNotEmpty(sortName)) {
//                if (StringUtils.isEmpty(sortOrder)) {
//                    sortOrder = "asc";
//                }
//                orderBy = sortName + " " + sortOrder;
//            }
//            Page page = PageHelper.startPage(pageNumber, pageSize, orderBy);
//            User loginUser = AuthService.getLoginUser(request);
//            List<Settlement> result = settlementService.find(schema_name, title, null, loginUser.getAccount());
//            JSONObject pageResult = new JSONObject();
//            pageResult.put("total", page.getTotal());
//            pageResult.put("rows", result);
//            return pageResult.toString();
//        } catch (Exception ex) {
//            return "{}";
//        }
//    }
//
//    @RequestMapping("/save-settlement")
//    public ResponseModel saveSettlement(@RequestBody Settlement settlement, HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        User loginUser = AuthService.getLoginUser(request);
//        String user = null;
//        if (loginUser != null) {
//            user = loginUser.getAccount();
//        }
//        String result = selectOptionService.getLanguageInfo( LangConstant.MSG_DE_DATA_NOT_NULL);
//        if (settlement == null || StringUtils.isEmpty(settlement.getSettlement_date())) {
//            return ResponseModel.error(result);
//        }
//        settlement.setCreate_user_account(user);
//        try {
//            if (settlement.getId() == null) {
//                SystemConfigData scd = systemConfigService.getSystemConfigData(schema_name, "calcuate_money");
//                String calcuateMoney = scd.getSettingValue();
//                BigDecimal monoey = new BigDecimal(0);
//                if (!StringUtils.isEmpty(calcuateMoney)) {
//                    monoey = new BigDecimal(calcuateMoney);
//                }
//                if (monoey.compareTo(settlement.getTotal_amount()) == 1) {
//                    settlement.setAuditor("yaozhijun1");
//                } else {
//                    settlement.setAuditor("yuejian");
//                }
//                settlement.setId(UUID.randomUUID().toString());
//                settlement.setStatus("0");
//                settlementService.insert(schema_name, settlement);
//                // 子表数据
//                settlementService.insertSubBatch(schema_name, settlement);
//            } else {
//                // settlementService.update(schema_name, settlement);
//            }
//            return ResponseModel.ok(selectOptionService.getLanguageInfo( LangConstant.MSG_SUCC_A));
//        } catch (Exception ex) {
//            return ResponseModel.error(selectOptionService.getLanguageInfo( LangConstant.MSG_EDIT_FA));
//        }
//    }
//
//    @RequestMapping("/find-single-settlement")
//    public ResponseModel querySingleSettlement(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String id = request.getParameter("id");
//            List<Settlement> list = settlementService.find(schema_name, null, id, null);
//            List<SettlementItem> subList = settlementService.findSubList(schema_name, id);
//            Map<String, Object> rstInfo = new HashMap<String, Object>();
//            rstInfo.put("info", list.get(0));
//            rstInfo.put("subList", subList);
//            result.setContent(rstInfo);
//            result.setCode(1);
//            result.setMsg("");
//            return result;
//        } catch (Exception ex) {
//
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_DATA_FAIL);
//            return ResponseModel.error(tmpMsg);//获取失败 MSG_DATA_FAIL
//        }
//    }
//
//    //点击审核触发事件
//    @RequestMapping("/settlement-audit")
//    @Transactional
//    public ResponseModel settlementAudit(HttpServletRequest request, HttpServletResponse response) {
//        String schema_name = authService.getCompany(request).getSchema_name();
//        ResponseModel result = new ResponseModel();
//        try {
//            String id = request.getParameter("id");
//            String status = request.getParameter("status");
//            String audit_desc = request.getParameter("audit_desc");
//            User user = authService.getLoginUser(request);
//            Settlement settlement = new Settlement();
//            settlement.setId(id);
//            settlement.setAudit_desc(audit_desc);
//            settlement.setStatus(status);
//            int count = settlementService.audit(schema_name, settlement);
//            if (count > 0) {
//                result.setCode(1);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_SURE_SUCC));
//                result.setContent(id);
//            } else {
//                result.setCode(0);
//                result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_APPROVAL_FAILED));
//            }
//            return result;
//        } catch (Exception ex) {
//            // 手动回滚
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            result.setCode(0);
//            result.setMsg(selectOptionService.getLanguageInfo( LangConstant.MSG_DO_FAIL));
//            return result;
//        }
//    }
}
