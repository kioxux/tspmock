package com.gengyun.senscloud.controller;
//
//import com.fitit100.util.RegexUtil;
//import com.gengyun.senscloud.auth.AuthService;
//import com.gengyun.senscloud.auth.MenuPermission;
//import com.gengyun.senscloud.common.LangConstant;
//import com.gengyun.senscloud.model.SystemConfigData;
//import com.gengyun.senscloud.model.User;
//import com.gengyun.senscloud.response.ResponseModel;
//import com.gengyun.senscloud.service.system.SelectOptionService;
//import com.gengyun.senscloud.service.system.SystemConfigService;
//import com.gengyun.senscloud.service.UserInfoEditService;
//import com.gengyun.senscloud.util.AESUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@RestController
public class UserInfoEditController {
//    @Autowired
//    private AuthService authService;
//    @Autowired
//    UserInfoEditService userInfoEditService;
//    @Autowired
//    SelectOptionService selectOptionService;
//    @Autowired
//    SystemConfigService systemConfigService;
//
//    @RequestMapping("/user_info_edit")
//    public ModelAndView userMobileEdit(HttpServletRequest request, HttpServletResponse response) {
//        //修改个人手机，以及密码
//        return new ModelAndView("user_info_edit");
////                .addObject("user_name","姚志军")
////                .addObject("user_account","yaozhijun")
////                .addObject("user_mobile","18051098665");
//    }
//
//    @RequestMapping("/get_user_info_for_edit")
//    public ResponseModel getUserInfo() {
//        return ResponseModel.ok(userInfoEditService.getUserInfoForEdit());
//    }
//
//    @RequestMapping("/get_user_mobile_edit")
//    public ResponseModel getUserMobileEdit(HttpServletRequest request, HttpServletResponse response) {
//        //获取个人信息及手机
//        ResponseModel result = new ResponseModel();
//        String account = request.getParameter("account");
//        String mobile = request.getParameter("mobile");
//        if (account == null || account.isEmpty() || mobile == null || mobile.isEmpty()) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ACCOUNT_OR_MOBILE_IS_NULL));//用户帐号或电话不能为空。
//        } else {
//            String schema_name = authService.getCompany(request).getSchema_name();
//            User user = authService.getLoginUser(request);
//
//            if (!user.getAccount().equals(account)) {
//                //修改的信息，不是当前登录人
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CB));//非法操作。
//            } else {
//                //是否需要验证手机号码
//                SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "is_validate_mobile");
//                if ((null == data || data.getSettingValue().equals("2")) && RegexUtil.isMobile(mobile) == false) {
//                    //电话号码格式不对
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_USER_MOBILE_WRONG));//用户电话格式不对。
//                } else {
//                    //看电话号码是否重复
//                    int count = userInfoEditService.existUserMobile(schema_name, mobile, account);
//                    if (count <= 0) {
//                        //更新电话号码
//                        int updateStatus = userInfoEditService.updateUserMobile(schema_name, mobile, account, authService.getCompany(request).getId());
//                        if (updateStatus > 0) {
//                            result.setCode(1);
//                            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_MOBILE_SUCC));//用户电话更新成功。
//                        } else {
//                            result.setCode(-1);
//                            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_MOBILE_FAIL));//用户电话更新失败。
//                        }
//                    } else {
//                        //电话号码重复
//                        result.setCode(-1);
//                        result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_USER_MOBILE_ALREADY_USE));//用户电话已经被注册。
//                    }
//                }
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    @RequestMapping("/get_u_p_edit")
//    public ResponseModel getUserPasswordEdit(HttpServletRequest request, HttpServletResponse response) {
//        ResponseModel result = new ResponseModel();
//        //获取个人信息及密码
//        String account = request.getParameter("account");
//        String oldPassword = request.getParameter("oldpd");
//        String newPassword = request.getParameter("newpd");
//        if (account == null || account.isEmpty() || oldPassword == null || oldPassword.isEmpty() || newPassword == null || newPassword.isEmpty()) {
//            result.setCode(-1);
//            result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_ACCOUNT_OR_PASSWORD_IS_NULL));//用户帐号或密码不能为空。
//        } else {
//            //比较旧密码是否一致
//            try {
//                String schema_name = authService.getCompany(request).getSchema_name();
//                User user = authService.getLoginUser(request);
//                account = AESUtils.decryptAES(account);
//                if (!user.getAccount().equals(account)) {
//                    //修改的信息，不是当前登录人
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_CB));//非法操作。
//                }
//
//                oldPassword = AESUtils.decryptAES(oldPassword);
//                newPassword = AESUtils.decryptAES(newPassword);
//
//                // 检查密码是否为强验证密码
//                SystemConfigData data = systemConfigService.getSystemConfigData(schema_name, "is_validate_strong_password");
//                if ((null != data && data.getSettingValue().equals("1")) && RegexUtil.isStrongPwd(newPassword) == false) {
//                    //密码非强密码，需修改
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.STRONG_PASSWORD));//强验证，必填字母数字及特殊字符，且以字母开头，8位以上。
//                    return result;
//                }
//
//                String oldPasswordEncode = User.encryptPassword(oldPassword);
//                String newPasswordEncode = User.encryptPassword(newPassword);
//
//                int count = userInfoEditService.checkUserPassword(schema_name, oldPasswordEncode, account);
//                if (count > 0) {
//                    //更新新密码
//                    int updateStatus = userInfoEditService.updateUserPassword(schema_name, newPasswordEncode, account);
//                    if (updateStatus > 0) {
//                        result.setCode(1);
//                        result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_PASSWORD_SUCC));//用户密码更新成功。
//                    } else {
//                        result.setCode(-1);
//                        result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_UPDATE_USER_PASSWORD_FAIL));//用户密码更新失败。
//                    }
//                } else {
//                    //旧密码输入错误
//                    result.setCode(-1);
//                    result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_OLD_PASSWORD_WRONG));//用户旧密码输入错误。
//                }
//            } catch (Exception ex) {
//                result.setCode(-1);
//                result.setMsg(selectOptionService.getLanguageInfo(LangConstant.MSG_PASSWORD_WRONG));//用户密码输入格式不对。
//            }
//        }
//        //返回保存结果
//        return result;
//    }
//
//    /**
//     * 保存用户个人配置信息
//     *
//     * @return
//     */
//    @RequestMapping("/saveUserConfig")
//    public ResponseModel saveUserConfig() {
//        try {
//            userInfoEditService.saveUserConfig();
//            return ResponseModel.ok("ok");
//        } catch (Exception ex) {
//            String tmpMsg = selectOptionService.getLanguageInfo(LangConstant.MSG_SAVE_FAIL);
//            return ResponseModel.errorMsg(tmpMsg); // 保存失败
//        }
//    }
}
