package com.gengyun.senscloud.listener.excel.read;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.system.CommonUtilService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * excel数据导入基础类
 */
public class BaseExcelListener extends AnalysisEventListener {

    private static final Logger logger = LoggerFactory.getLogger(BaseExcelListener.class);

    public static final String HANDLE_TYPE_CHECK = "check";//excel数据校验
    public static final String HANDLE_TYPE_SAVE = "save";//excel数据导入
    public static final String msgSplit = "；";
    public static final String EXCEL_ROW_NO = "excelRowNo";
    public List<LinkedHashMap> dataList = new ArrayList<>();
    int EXCEL_BATCH_COUNT = 5000; // 导入每批数量
    public int count = 0;
    public String sheetName;
    public String type;
    public String schemaName;
    public String account;
    public List<ImportLog> logList;
    public Map<String, String> msgInfo;
    public MethodParam methodParam;
    public CommonUtilService commonUtilService;
    public int headRowNumber = 0;//指定从第几行还是读取数据，不指定默认从第二行开始(默认值1)
    public LinkedHashMap columns;

    public BaseExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super();
        this.commonUtilService = SpringContextHolder.getBean("commonUtilServiceImpl");
        this.type = type;
        this.schemaName = schemaName;
        this.account = account;
        this.logList = logList;
        this.msgInfo = new HashMap<>();
        this.methodParam = methodParam;
    }

    /**
     * When analysis one row trigger invoke function.
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(Object data, AnalysisContext context) {
        if (RegexUtil.optIsPresentStr(sheetName)) {
            sheetName = context.readSheetHolder().getSheetName();
        }
        int rowNo = context.readRowHolder().getRowIndex();
        if (rowNo == 0 && headRowNumber == 0) {
            //如果监听器配置的是从第一列开始读取，则此invoke方法第一次进来的为列名数据，不用记录到dataList做处理
            columns = (LinkedHashMap) data;
        } else {
            LinkedHashMap map = (LinkedHashMap) data;
            map.put(EXCEL_ROW_NO, rowNo);
            dataList.add(map);
            handleData(EXCEL_BATCH_COUNT, context.readSheetHolder());
        }
    }

    /**
     * if have something to do after all analysis
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        handleData(0, context.readSheetHolder());
        sheetName = null;
    }

    /**
     * 修改导入每批数量
     *
     * @param count
     * @return
     */
    public BaseExcelListener setExcelBatchCount(int count) {
        this.EXCEL_BATCH_COUNT = count;
        return this;
    }

    public int getHeadRowNumber() {
        return headRowNumber;
    }

    public void setHeadRowNumber(int headRowNumber) {
        this.headRowNumber = headRowNumber;
    }

    /**
     * 处理数据
     *
     * @param baseSize
     */
    private void handleData(int baseSize, ReadSheetHolder sheetHolder) {
        if (dataList.size() > baseSize) {
            if (HANDLE_TYPE_SAVE.equals(type)) {
                if (logList.size() == 0) {
                    saveData(dataList, sheetHolder);
                }
            } else if (HANDLE_TYPE_CHECK.equals(type)) {
                checkData(dataList, sheetHolder);
            }
            dataList.clear();
        }
    }

    public int getCount() {
        return count;
    }

    /**
     * 校验数据
     *
     * @param dataList
     */
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
    }

    /**
     * 存储数据
     *
     * @param dataList
     */
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
    }

    /**
     * 添加日志
     *
     * @param no     编号
     * @param msgBff 消息
     */
    public void addLog(Object no, StringBuffer msgBff) {
        String rowNo = String.valueOf(RegexUtil.optIntegerOrVal(no, 0) + 1);
        String msg = RegexUtil.optStrOrBlank(msgBff);
        msg = msg.startsWith(msgSplit) ? msg.substring(1) : msg;
        ImportLog log = new ImportLog();
        log.setNum(rowNo);
        log.setLevel(msgInfo.get("msgError"));
        log.setMessage(msg);
        logList.add(log);
    }
}
