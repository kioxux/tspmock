package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.CloudDataService;
import com.gengyun.senscloud.service.CustomersService;
import com.gengyun.senscloud.service.FacilitiesService;
import com.gengyun.senscloud.util.RegexUtil;

import java.util.*;
import java.util.stream.Collectors;

public class CustomerExcelListener extends BaseExcelListener {

    private CustomersService customersService;
    private FacilitiesService facilitiesService;
    private CloudDataService cloudDataService;



    public CustomerExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        customersService = SpringContextHolder.getBean("customersServiceImpl");
        facilitiesService = SpringContextHolder.getBean("facilitiesServiceImpl");
        cloudDataService = SpringContextHolder.getBean("cloudDataServiceImpl");

        msgInfo = new HashMap<>();
        msgInfo.put("msgError", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ)); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            msgInfo.put("msgErrorFieldType", msgErrorFieldType);
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
            msgInfo.put("msgErrorCustomerCodeNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCustomerNameNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorCustomerLevelData", msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorCustomerTypeData", msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorUnloadingGroupData", msgColumnNo + "（F）" + msgErrorData);
            /*msgInfo.put("msgErrorAccountNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorAccountRepeat", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_JQ) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件型号重复
            msgInfo.put("msgErrorAccountExist", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_JQ) + "已存在");
            msgInfo.put("msgErrorGroupNull", msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorGroupData", msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorPositionNull", msgColumnNo + "（D）" + msgErrorNull);
            msgInfo.put("msgErrorPositionData", msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorSexNull", msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorMobileNull", msgColumnNo + "（F）" + msgErrorNull);
            msgInfo.put("msgErrorMobileRepeat", msgColumnNo + "（F）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAT_F) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件型号重复
            msgInfo.put("msgErrorMobileExist", msgColumnNo + "（F）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAT_F) + "已存在");*/
        }

    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Map<String,Object> customerLevels = new HashMap();
        customerLevels.put("data_type","customer_level");
        List<Map<String,Object>> customerLevelMap = cloudDataService.getCloudDataListByDataType(methodParam,customerLevels);
        Map<String,Object> customerTypes = new HashMap();
        customerTypes.put("data_type","customer_type");
        List<Map<String,Object>> customerTypeMap = cloudDataService.getCloudDataListByDataType(methodParam,customerTypes);
        Map<String,Object> unloadingGroups = new HashMap();
        unloadingGroups.put("data_type","unloading_group");
        List<Map<String,Object>> unloadingGroupMap = cloudDataService.getCloudDataListByDataType(methodParam,unloadingGroups);
        Map<String,Object> sewageDestinations = new HashMap();
        unloadingGroups.put("data_type","sewage_destination");
        List<Map<String,Object>> sewageDestinationsMap = cloudDataService.getCloudDataListByDataType(methodParam,sewageDestinations);
        for (LinkedHashMap row : dataList) {
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
            String customer_code = RegexUtil.optStrOrBlank(row.get(0));
            String customer_name = RegexUtil.optStrOrBlank(row.get(1));
            if (RegexUtil.optIsBlankStr(customer_code)) {
                msg.append(msgInfo.get("msgErrorCustomerCodeNull"));
            }
            if (RegexUtil.optIsBlankStr(customer_name)) {
                msg.append(msgInfo.get("msgErrorCustomerNameNull"));
            }
            if(RegexUtil.optIsPresentStr(row.get(2)) && RegexUtil.optIsBlankStr(getCloudDataCode(customerLevelMap,String.valueOf(row.get(2))))){
                msg.append(msgInfo.get("msgErrorCustomerLevelData"));
            }
            if(RegexUtil.optIsPresentStr(row.get(3)) && RegexUtil.optIsBlankStr(getCloudDataCode(customerTypeMap,String.valueOf(row.get(3))))){
                msg.append(msgInfo.get("msgErrorCustomerTypeData"));
            }
            if(RegexUtil.optIsPresentStr(row.get(5)) && RegexUtil.optIsBlankStr(getCloudDataCode(unloadingGroupMap,String.valueOf(row.get(5))))){
                msg.append(msgInfo.get("msgErrorUnloadingGroupData"));
            }
            if(RegexUtil.optIsPresentStr(row.get(8)) && RegexUtil.optIsBlankStr(getCloudDataCode(sewageDestinationsMap,String.valueOf(row.get(8))))){
                msg.append(msgInfo.get("msgErrorUnloadingGroupData"));
            }
            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        List<Map<String, Object>> newList = new ArrayList<>();
        Map<String,Object> customerLevels = new HashMap();
        customerLevels.put("data_type","customer_level");
        List<Map<String,Object>> customerLevelMap = cloudDataService.getCloudDataListByDataType(methodParam,customerLevels);
        Map<String,Object> customerTypes = new HashMap();
        customerTypes.put("data_type","customer_type");
        List<Map<String,Object>> customerTypeMap = cloudDataService.getCloudDataListByDataType(methodParam,customerTypes);
        Map<String,Object> unloadingGroups = new HashMap();
        unloadingGroups.put("data_type","unloading_group");
        List<Map<String,Object>> unloadingGroupMap = cloudDataService.getCloudDataListByDataType(methodParam,unloadingGroups);
        for (LinkedHashMap row : dataList) {
            Map<String, Object> map = new HashMap();
            map.put("inner_code", RegexUtil.optStrOrBlank(row.get(0))); //客户编号
            map.put("title", RegexUtil.optStrOrBlank(row.get(1))); //客户名称
            map.put("important_level_id", getCloudDataCode(customerLevelMap,RegexUtil.optStrOrBlank(row.get(2)))); //重要级别
            map.put("customer_type_id", getCloudDataCode(customerTypeMap,RegexUtil.optStrOrBlank(row.get(3)))); //客户类型
            map.put("address", RegexUtil.optStrOrBlank(row.get(4))); //所在地址
            map.put("unloading_group_id", getCloudDataCode(unloadingGroupMap,RegexUtil.optStrOrBlank(row.get(5)))); //并管排污分组
            map.put("position_code", RegexUtil.optStrOrBlank(row.get(6))); //所在片区
            map.put("pipeline_code", RegexUtil.optStrOrBlank(row.get(7))); //管道编号
            map.put("sewage_destination", getCloudDataCode(unloadingGroupMap,RegexUtil.optStrOrBlank(row.get(8)))); //污水去向
            map.put("main_products", RegexUtil.optStrOrBlank(row.get(9))); //主要产品
            map.put("remark", RegexUtil.optStrOrBlank(row.get(10))); //详细描述
            map.put("province", RegexUtil.optStrOrBlank(row.get(11))); //省
            map.put("city", RegexUtil.optStrOrBlank(row.get(12))); //市
            map.put("zone", RegexUtil.optStrOrBlank(row.get(13))); //区
            map.put("contact_mobile", RegexUtil.optStrOrBlank(row.get(14))); //联系人电话
            map.put("contact_name", RegexUtil.optStrOrBlank(row.get(15))); //联系人姓名
            //map.put("parent_id", RegexUtil.optStrOrBlank(row.get(16))); //所属企业
            newList.add(map);
            count++;//记录导入条数

        }
        for (Map<String, Object> map : newList) {
            map.put("org_type_id", 1);
            Map<String, Object> oldCustomer = facilitiesService.findByCode(methodParam.getSchemaName(), (String) map.get("inner_code"));
            if (RegexUtil.optIsPresentMap(oldCustomer)) {
                map.put("id", oldCustomer.get("id"));
                customersService.modifyCustomers(methodParam, map);
            } else
                customersService.newCustomer(methodParam, map);
            if (RegexUtil.optIsPresentStr(map.get("contact_mobile"))) {
                Map<String, Object> searchMap = new HashMap();
                searchMap.put("org_id", map.get("id"));
                searchMap.put("keywordSearch", map.get("contact_mobile"));
                List<Map<String, Object>> list = facilitiesService.getOrganizationContactList(methodParam, searchMap);
                Map<String, Object> contactMap = new HashMap();
                contactMap.put("org_id", map.get("id"));
                contactMap.put("contact_name", map.get("contact_name"));
                contactMap.put("contact_mobile", map.get("contact_mobile"));
                if (RegexUtil.optIsPresentList(list)) {
                    contactMap.put("id", Integer.valueOf(list.get(0).get("id").toString()));
                    facilitiesService.modifyOrganizationContact(methodParam, contactMap);
                } else
                    facilitiesService.newOrganizationContact(methodParam, contactMap);
            }
        }

    }

    /**
     * 查找编号
     * @param map
     * @param name
     * @return
     */
    private String getCloudDataCode(List<Map<String,Object>> map,String name){
        return RegexUtil.optStrOrBlank(RegexUtil.optNotNullListOrNew(map.stream()
                .filter(k->{
                    return name.equals(k.get("name"));
                }).collect(Collectors.toList()))
                .stream()
                .findFirst()
                .orElseGet(HashMap<String,Object>::new).get("code"));

    }
}
