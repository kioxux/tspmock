package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.UserModel;
import com.gengyun.senscloud.service.GroupService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 人员导入监听器
 */
public class UserExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(UserExcelListener.class);

    private UserService userService;
    private GroupService groupService;
    private Map<String, Map<String, Object>> accountListMap;
    private Map<String, Map<String, Object>> mobileListMap;
    private Map<String, Map<String, Object>> groupListMap;
    private Map<String, Map<String, Object>> positionListMap;
    private Map<String, Map<String, Object>> accountMobileListMap;

    public UserExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        userService = SpringContextHolder.getBean("userServiceImpl");
        groupService = SpringContextHolder.getBean("groupServiceImpl");
        accountListMap = userService.queryAccountListMap(schemaName);
        accountMobileListMap = userService.getAccountMobileListMap(schemaName);
        mobileListMap = userService.queryMobileListMap(schemaName);
        groupListMap = groupService.getGroupListMap(schemaName);
        positionListMap = groupService.getPositionListMap(schemaName);
        msgInfo = new HashMap<>();
        msgInfo.put("msgError", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ)); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            msgInfo.put("msgErrorFieldType", msgErrorFieldType);
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
            msgInfo.put("msgErrorUserNameNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorAccountNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorAccountRepeat", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_JQ) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件型号重复
            msgInfo.put("msgErrorAccountExist", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_JQ) + "已存在");
            msgInfo.put("msgErrorGroupNull", msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorGroupData", msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorPositionNull", msgColumnNo + "（D）" + msgErrorNull);
            msgInfo.put("msgErrorPositionData", msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorSexNull", msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorMobileNull", msgColumnNo + "（F）" + msgErrorNull);
            msgInfo.put("msgErrorMobileRepeat", msgColumnNo + "（F）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAT_F) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件型号重复
            msgInfo.put("msgErrorMobileExist", msgColumnNo + "（F）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAT_F) + "已存在");
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Set<String> mobileSet = new HashSet<>();
        Set<String> accountSet = new HashSet<>();
        String userName = null;//姓名
        String account = null;//账号
        String group = null;//部门
        String position = null;//岗位
        String sex = null;//性别
        String mobile = null;//手机号
        String user_code = null;//工号
        String nfc_code = null;//nfc号
        String email = null;//邮箱
        String joinDate = null;//入职日期
        String hourFee = null;//工时成本
        String remark = null;//备注
        String account_mobile = null;
        for (LinkedHashMap row : dataList) {
            mobileSet.add(RegexUtil.optStrOrBlank(row.get(5)));
            accountSet.add(RegexUtil.optStrOrBlank(row.get(1)));
        }
//        Map<String, Map<String, Object>> mobileMap = new HashMap<>();//查询手机号在系统中是否已录入
//        if (mobileSet.size() > 0) {
//            mobileMap = userService.getUserByMobile(schemaName, new ArrayList<>(mobileSet));
//        }
//        Map<String, Map<String, Object>> accountMap = new HashMap<>();
//        if (accountSet.size() > 0) {
//            accountMap = userService.getUserByAccount(schemaName, new ArrayList<>(accountSet));
//        }

        mobileSet.clear();
        accountSet.clear();//先清空，记录每一行的数据，校验上传数据中是否有重复
        for (LinkedHashMap row : dataList) {
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
            userName = RegexUtil.optStrOrBlank(row.get(0));
            account = RegexUtil.optStrOrBlank(row.get(1));
            group = RegexUtil.optStrOrBlank(row.get(2));
            position = RegexUtil.optStrOrBlank(row.get(3));
            sex = RegexUtil.optStrOrBlank(row.get(4));
            mobile = RegexUtil.optStrOrBlank(row.get(5));
            user_code = RegexUtil.optStrOrBlank(row.get(6));
            nfc_code = RegexUtil.optStrOrBlank(row.get(7));
            email = RegexUtil.optStrOrBlank(row.get(8));
            joinDate = RegexUtil.optStrOrBlank(row.get(9));
            hourFee = RegexUtil.optStrOrBlank(row.get(10));
            remark = RegexUtil.optStrOrBlank(row.get(11));
            account_mobile = account + "-" + mobile;

            if (RegexUtil.optIsBlankStr(userName)) {
                msg.append(msgInfo.get("msgErrorUserNameNull"));
            }
            if (RegexUtil.optIsBlankStr(account)) {
                msg.append(msgInfo.get("msgErrorAccountNull"));
            } else {
                if (accountSet.contains(account)) {
                    msg.append(msgInfo.get("msgErrorAccountRepeat"));
                } else if (accountListMap.containsKey(account)) {
                    if(!accountMobileListMap.containsKey(account_mobile)) {
                        msg.append(msgInfo.get("msgErrorAccountExist"));
                    }
                } else {
                    accountSet.add(account);
                }
            }
            if (RegexUtil.optIsBlankStr(mobile)) {
                msg.append(msgInfo.get("msgErrorMobileNull"));
            } else {
                if (mobileSet.contains(mobile)) {
                    msg.append(msgInfo.get("msgErrorMobileRepeat"));
                } else if (mobileListMap.containsKey(mobile)) {
                    if(!accountMobileListMap.containsKey(account_mobile)) {
                        msg.append(msgInfo.get("msgErrorMobileExist"));
                    }
                } else {
                    mobileSet.add(mobile);
                }
            }
            if (RegexUtil.optIsBlankStr(group)) {
                msg.append(msgInfo.get("msgErrorGroupNull"));
            } else if (!groupListMap.containsKey(group)) {
                msg.append(msgInfo.get("msgErrorGroupData"));
            }
            String gPName = group + "/" + position;

            if (RegexUtil.optIsBlankStr(position)) {
                msg.append(msgInfo.get("msgErrorPositionNull"));
            } else if (!positionListMap.containsKey(gPName)) {
                msg.append(msgInfo.get("msgErrorPositionData"));
            }
            if (RegexUtil.optIsBlankStr(sex) || (RegexUtil.optIsPresentStr(sex) && !"男女".contains(sex))) {
                msg.append(msgInfo.get("msgErrorSexNull"));
            }
            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
        }
//        if (null != mobileMap) {
//            mobileMap.clear();
//        }
//        if (null != accountMap) {
//            accountMap.clear();
//        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        List<UserModel> userList = new ArrayList<>();
        List<UserModel> upUserList = new ArrayList<>();
        for (LinkedHashMap row : dataList) {
            UserModel userModel = new UserModel();
            String userName = RegexUtil.optStrOrBlank(row.get(0));
            String account = RegexUtil.optStrOrBlank(row.get(1));
            String group = RegexUtil.optStrOrBlank(row.get(2));
            String position = RegexUtil.optStrOrBlank(row.get(3));
            String sex = RegexUtil.optStrOrBlank(row.get(4));
            String mobile = RegexUtil.optStrOrBlank(row.get(5));
            String user_code = RegexUtil.optStrOrNull(row.get(6));
            String nfc_code = RegexUtil.optStrOrNull(row.get(7));
            String email = RegexUtil.optStrOrNull(row.get(8));
            String joinDate = RegexUtil.optStrOrBlank(row.get(9));
            String hourFee = RegexUtil.optStrOrNull(row.get(10));
            String remark = RegexUtil.optStrOrNull(row.get(11));
            String gPName = group + "/" + position;
            String account_mobile = account + "-" + mobile;
            userModel.setAccount(account);
            userModel.setEmail(email);
            userModel.setGender_tag("男".equals(sex)?"1":"2");
            userModel.setHour_fee(hourFee);
            userModel.setUser_name(userName);
            userModel.setPassword("65f1f03bfb3137b18731da2ade91abe6");//123
            userModel.setUser_code(user_code);
            userModel.setMobile(mobile);
            userModel.setPositionIds(String.valueOf(positionListMap.get(gPName).get("id")));
            userModel.setIs_use("1");
            userModel.setIs_charge(false);
            userModel.setNfc_code(nfc_code);
            userModel.setRemark(remark);
            if(accountMobileListMap.containsKey(account_mobile)){
                String id = RegexUtil.optStrOrNull(accountMobileListMap.get(account_mobile).get("id"));
                userModel.setId(id);
                userModel.setSys_user("2");
                upUserList.add(userModel);
            }else {
                userList.add(userModel);
            }
            SimpleDateFormat date_sdf = new SimpleDateFormat("yyyy-MM-dd" );
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date = null;
            try {
                date = sdf.parse(joinDate);
                String join_date = date_sdf.format(date);
                userModel.setJoin_date(join_date);
            } catch (ParseException e) {
            }
            count++;//记录导入条数
        }
        if (RegexUtil.optIsPresentList(userList)) {
            for (UserModel userModel : userList) {
                userService.newUser(methodParam, userModel);
            }
        }
        if(RegexUtil.optIsPresentList(upUserList)){
            for (UserModel userModel : upUserList) {
                userService.modifyUser(methodParam, userModel);
            }
        }

    }

}
