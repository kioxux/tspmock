package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.pollute.PolluteFeeService;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 缴费信息导入监听器
 */
public class PolluteFeeExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(PolluteFeeExcelListener.class);

    private PolluteFeeService polluteFeeService;

    public PolluteFeeExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);

        polluteFeeService = SpringContextHolder.getBean("polluteFeeServiceImpl");

        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
            String msgErrorDate = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DATE_T);//日期
            String msgCustomField = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_G);//自定义字段
            String msgErrorField = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_S) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//列表列错误
            //  列表列
            msgInfo.put("msgErrorCusNameNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCusNameData", msgColumnNo + "（A）" + msgErrorData);
            msgInfo.put("msgErrorBelongMonthNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorBelongMonth", msgColumnNo + "（B）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorBeginDateNull", msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorBeginDate", msgColumnNo + "（C）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorEndDateNull", msgColumnNo + "（D）" + msgErrorNull);
            msgInfo.put("msgErrorEndDate", msgColumnNo + "（D）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorFinalFlowNull", msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorFinalFlow", msgErrorFieldType + msgErrorTmp + "（E）" + msgErrorTmpVal);
            msgInfo.put("msgErrorExceedTimesNull", msgColumnNo + "（F）" + msgErrorNull);
            msgInfo.put("msgErrorExceedTimes", msgErrorFieldType + msgErrorTmp + "（F）" + msgErrorTmpVal);
            msgInfo.put("msgErrorPaymentValueNull", msgColumnNo + "（G）" + msgErrorNull);
            msgInfo.put("msgErrorPaymentValue", msgErrorFieldType + msgErrorTmp + "（G）" + msgErrorTmpVal);
            msgInfo.put("msgErrorExcFlowNull", msgColumnNo + "（H）" + msgErrorNull);
            msgInfo.put("msgErrorExcFlow", msgErrorFieldType + msgErrorTmp + "（H）" + msgErrorTmpVal);
            msgInfo.put("msgErrorPriceNull", msgColumnNo + "（I）" + msgErrorNull);
            msgInfo.put("msgErrorPrice", msgErrorFieldType + msgErrorTmp + "（I）" + msgErrorTmpVal);
            msgInfo.put("msgErrorRatioNull", msgColumnNo + "（J）" + msgErrorNull);
            msgInfo.put("msgErrorRatio", msgErrorFieldType + msgErrorTmp + "（J）" + msgErrorTmpVal);
            msgInfo.put("msgErrorRepeatTimesNull", msgColumnNo + "（K）" + msgErrorNull);
            msgInfo.put("msgErrorRepeatTimes", msgErrorFieldType + msgErrorTmp + "（K）" + msgErrorTmpVal);
            msgInfo.put("msgErrorRepeatFeeNull", msgColumnNo + "（L）" + msgErrorNull);
            msgInfo.put("msgErrorRepeatFee", msgErrorFieldType + msgErrorTmp + "（L）" + msgErrorTmpVal);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Set<String> customerSet = new HashSet<>();
        for (LinkedHashMap row : dataList) {
            customerSet.add(RegexUtil.optStrOrBlank(row.get(0)));
        }
        Map<String, Map<String, Object>> customerMap = new HashMap<>();//查询导入的客户在系统中是否已录入
        if (customerSet.size() > 0) {
            customerMap = polluteFeeService.queryCusNameByName(schemaName, new ArrayList<>(customerSet));
        }
        for (LinkedHashMap row : dataList) {
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
            int i = 0;
            String cusName = RegexUtil.optStrOrBlank(row.get(i++));//客户名称
            String belongMonth = RegexUtil.optStrOrBlank(row.get(i++));//所属月份
            String beginDate = RegexUtil.optStrOrBlank(row.get(i++));//缴费开始日期
            String endDate = RegexUtil.optStrOrBlank(row.get(i++));//缴费结束日期
            String finalFlow = RegexUtil.optStrOrBlank(row.get(i++));//污水处理量
            String exceedTimes = RegexUtil.optStrOrBlank(row.get(i++));//超标次数
            String paymentValue = RegexUtil.optStrOrBlank(row.get(i++));//基值
            String excFlow = RegexUtil.optStrOrBlank(row.get(i++));//超标污水量
            String price = RegexUtil.optStrOrBlank(row.get(i++));//污水处理单价
            String ratio = RegexUtil.optStrOrBlank(row.get(i++));//超标系数
            String repeatTimes = RegexUtil.optStrOrBlank(row.get(i++));//复测次数
            String repeatFee = RegexUtil.optStrOrBlank(row.get(i));//复测采样分析费
            //客户信息不存在
            if (RegexUtil.optIsBlankStr(cusName)) {
                msg.append(msgInfo.get("msgErrorCusNameNull"));
            } else if (!customerMap.containsKey(cusName)) {
                msg.append(msgInfo.get("msgErrorCusNameData"));
            }
            //所属月份不存在
            if (RegexUtil.optIsBlankStr(belongMonth)) {
                msg.append(msgInfo.get("msgErrorBelongMonthNull"));
            } else if (RegexUtil.optIsPresentStr(belongMonth) && parseMonth(belongMonth) == null) {
                msg.append(msgInfo.get("msgErrorBelongMonth") + "(" + belongMonth + ")");
            }
            //缴费开始日期
            if (RegexUtil.optIsBlankStr(beginDate)) {
                msg.append(msgInfo.get("msgErrorBeginDateNull"));
            } else if (RegexUtil.optIsPresentStr(beginDate) && parseDate(beginDate) == null) {
                msg.append(msgInfo.get("msgErrorBeginDate") + "(" + beginDate + ")");
            }
            //缴费结束日期
            if (RegexUtil.optIsBlankStr(endDate)) {
                msg.append(msgInfo.get("msgErrorEndDateNull"));
            } else if (RegexUtil.optIsPresentStr(endDate) && parseDate(endDate) == null) {
                msg.append(msgInfo.get("msgErrorEndDate") + "(" + endDate + ")");
            }
            //污水处理量
            if (RegexUtil.optIsBlankStr(finalFlow)) {
                msg.append(msgInfo.get("msgErrorFinalFlowNull"));
            } else if (RegexUtil.optIsPresentStr(finalFlow) && !RegexUtil.isNumeric(finalFlow)) {
                msg.append(msgInfo.get("msgErrorFinalFlow") + "(" + finalFlow + ")");
            }
            //超标次数
            if (RegexUtil.optIsBlankStr(exceedTimes)) {
                msg.append(msgInfo.get("msgErrorExceedTimesNull"));
            } else if (RegexUtil.optIsPresentStr(exceedTimes) && !RegexUtil.isNumeric(exceedTimes)) {
                msg.append(msgInfo.get("msgErrorExceedTimes") + "(" + exceedTimes + ")");
            }
            //基值
            if (RegexUtil.optIsBlankStr(paymentValue)) {
                msg.append(msgInfo.get("msgErrorPaymentValueNull"));
            } else if (RegexUtil.optIsPresentStr(paymentValue) && !RegexUtil.isNumeric(paymentValue)) {
                msg.append(msgInfo.get("msgErrorPaymentValue") + "(" + paymentValue + ")");
            }
            //超标污水量
            if (RegexUtil.optIsBlankStr(excFlow)) {
                msg.append(msgInfo.get("msgErrorExcFlowNull"));
            } else if (RegexUtil.optIsPresentStr(excFlow) && !RegexUtil.isNumeric(excFlow)) {
                msg.append(msgInfo.get("msgErrorExcFlow") + "(" + excFlow + ")");
            }
            //污水处理单价
            if (RegexUtil.optIsBlankStr(price)) {
                msg.append(msgInfo.get("msgErrorPriceNull"));
            } else if (RegexUtil.optIsPresentStr(price) && !RegexUtil.isNumeric(price)) {
                msg.append(msgInfo.get("msgErrorPrice") + "(" + price + ")");
            }
            //超标系数
            if (RegexUtil.optIsBlankStr(ratio)) {
                msg.append(msgInfo.get("msgErrorRatioNull"));
            } else if (RegexUtil.optIsPresentStr(ratio) && !RegexUtil.isNumeric(ratio)) {
                msg.append(msgInfo.get("msgErrorRatio") + "(" + ratio + ")");
            }
            //复测次数
            if (RegexUtil.optIsBlankStr(repeatTimes)) {
                msg.append(msgInfo.get("msgErrorRepeatTimesNull"));
            } else if (RegexUtil.optIsPresentStr(repeatTimes) && !RegexUtil.isNumeric(repeatTimes)) {
                msg.append(msgInfo.get("msgErrorRepeatTimes") + "(" + repeatTimes + ")");
            }
            //复测采样分析费
            if (RegexUtil.optIsBlankStr(repeatFee)) {
                msg.append(msgInfo.get("msgErrorRepeatFeeNull"));
            } else if (RegexUtil.optIsPresentStr(repeatFee) && !RegexUtil.isNumeric(repeatFee)) {
                msg.append(msgInfo.get("msgErrorRepeatFee") + "(" + repeatFee + ")");
            }
            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
        }
        if (null != customerMap) {
            customerMap.clear();
        }
        customerSet.clear();
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Set<String> customerSet = new HashSet<>();
        for (LinkedHashMap row : dataList) {
            customerSet.add(RegexUtil.optStrOrBlank(row.get(0)));
        }
        Map<String, Map<String, Object>> customerMap = new HashMap<>();//查询导入的客户在系统中是否已录入
        if (customerSet.size() > 0) {
            customerMap = polluteFeeService.queryCusNameByName(schemaName, new ArrayList<>(customerSet));
        }
        customerSet.clear();
        for (LinkedHashMap row : dataList) {
            Map<String, Object> param = new HashMap<>();
            Map<String, Object> paramNormalItem = new HashMap<>();
            Map<String, Object> paramExcItem = new HashMap<>();
            //污水处理量
            BigDecimal final_flow = new BigDecimal(RegexUtil.optStrOrVal(row.get(4), "0"));
            //超标次数
            BigDecimal exceed_times = new BigDecimal(RegexUtil.optStrOrVal(row.get(5), "0"));
            //基值
            BigDecimal payment_value = new BigDecimal(RegexUtil.optStrOrVal(row.get(6), "0"));
            //超标水量
            BigDecimal exc_flow = new BigDecimal(RegexUtil.optStrOrVal(row.get(7), "0"));
            //污水处理单价
            BigDecimal price = new BigDecimal(RegexUtil.optStrOrVal(row.get(8), "0"));
            //超标系数
            BigDecimal ratio = new BigDecimal(RegexUtil.optStrOrVal(row.get(9), "0"));
            //复测次数
            BigDecimal repeat_times = new BigDecimal(RegexUtil.optStrOrVal(row.get(10), "0"));
            //复测采样分析费用
            BigDecimal repeat_price = new BigDecimal(RegexUtil.optStrOrVal(row.get(11), "0"));
            //复测总费用
            BigDecimal repeat_fee = repeat_times.multiply(repeat_price).setScale(2,BigDecimal.ROUND_HALF_UP);
            //污水处理服务费
            BigDecimal normal_fee;
            if (final_flow.compareTo(payment_value) > 0) {
                normal_fee = final_flow.multiply(price).setScale(2,BigDecimal.ROUND_HALF_UP);
            } else {
                normal_fee = payment_value.multiply(price).setScale(2,BigDecimal.ROUND_HALF_UP);
            }
            //处理服务补偿费
            BigDecimal exc_fee = BigDecimal.ZERO;
            if (repeat_times.compareTo(BigDecimal.valueOf(3)) < 0) {
                exc_fee = exc_flow.multiply(price).multiply(ratio).add(repeat_fee).setScale(2,BigDecimal.ROUND_HALF_UP);
            } else if (repeat_times.compareTo(BigDecimal.valueOf(3)) >= 0) {
                exc_fee = final_flow.multiply(price).multiply(ratio).add(repeat_fee).setScale(2,BigDecimal.ROUND_HALF_UP);
            }
            param.put("facility_id", customerMap.get(RegexUtil.optStrOrBlank(row.get(0))).get("facility_id"));
            param.put("belong_month", row.get(1));
            param.put("begin_date", row.get(2));
            param.put("end_date", row.get(3));
            param.put("final_flow", final_flow);
            param.put("exceed_times", exceed_times);
            param.put("payment_value", payment_value);
            param.put("amount", normal_fee.add(exc_fee));
            param.put("normal_amount", normal_fee);
            param.put("exceed_amount", exc_fee);
            param.put("final_amount", normal_fee.add(exc_fee));
            param.put("create_user_id", methodParam.getUserId());

            // 对每个客户的污水处理费用明细情况进行业务保存
            paramNormalItem.put("fee_type", "0");
            paramNormalItem.put("fee_item_id", 1);
            paramNormalItem.put("facility_id", customerMap.get(RegexUtil.optStrOrBlank(row.get(0))).get("facility_id"));
            paramNormalItem.put("belong_month", row.get(1));
            if (final_flow.compareTo(payment_value) > 0) {
                paramNormalItem.put("final_flow",  final_flow);
            } else {
                paramNormalItem.put("final_flow",  payment_value);
            }
            paramNormalItem.put("price", price);
            paramNormalItem.put("amount", normal_fee);
            paramNormalItem.put("create_user_id", methodParam.getUserId());

            paramExcItem.put("fee_type", "1");
            paramExcItem.put("fee_item_id", 2);
            paramExcItem.put("facility_id", customerMap.get(RegexUtil.optStrOrBlank(row.get(0))).get("facility_id"));
            paramExcItem.put("belong_month", row.get(1));
            paramExcItem.put("final_flow", final_flow);
            paramExcItem.put("price", price);
            paramExcItem.put("ratio", ratio);
            paramExcItem.put("repeat_count", repeat_times);
            paramExcItem.put("repeat_fee", repeat_price);
            paramExcItem.put("amount", exc_fee);
            paramExcItem.put("create_user_id", methodParam.getUserId());
            int polluteCount = polluteFeeService.findPolluteFeeByCondition(schemaName, param);
            if (polluteCount == 0) {
                polluteFeeService.insertPolluteFee(schemaName, param);
                paramNormalItem.put("fee_id", param.get("id"));
                polluteFeeService.insertPolluteFeeNormalDetail(schemaName, paramNormalItem);

                paramExcItem.put("fee_id", param.get("id"));
                polluteFeeService.insertPolluteFeeExcDetail(schemaName, paramExcItem);
            } else {
                int isUpdate = polluteFeeService.findPolluteFeeByUpdate(schemaName, param);
                if (isUpdate == 0) {
                    polluteFeeService.updatePolluteFee(schemaName, param);
                    polluteFeeService.updatePolluteFeeNormalDetail(schemaName, paramNormalItem);
                    polluteFeeService.updatePolluteFeeExcDetail(schemaName, paramExcItem);
                }
            }
            count++;//记录导入条数
        }
        customerMap.clear();
    }

    /**
     * 字符串转日期格式
     *
     * @param value
     * @return
     */
    private Date parseDate(String value) {
        if (RegexUtil.optIsPresentStr(value)) {
            try {
                if (value.contains("-")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("/")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("年")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
                    return simpleDateFormat.parse(value);
                }
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    private Date parseMonth(String value) {
        if (RegexUtil.optIsPresentStr(value)) {
            try {
                if (value.contains("-")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("/")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("年")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月");
                    return simpleDateFormat.parse(value);
                }
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }
}
