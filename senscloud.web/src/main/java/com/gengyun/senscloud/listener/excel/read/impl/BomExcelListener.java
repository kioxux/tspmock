package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.common.StatusConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.FacilitiesService;
import com.gengyun.senscloud.service.bom.BomService;
import com.gengyun.senscloud.service.bom.BomStockListService;
import com.gengyun.senscloud.service.dynamic.DynamicCommonService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * 备件导入(建卡+入库)监听器
 */
public class BomExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(BomExcelListener.class);

    private BomStockListService bomStockListService;
    private SelectOptionService selectOptionService;
    private DynamicCommonService dynamicCommonService;
    private FacilitiesService facilitiesService;
    private BomService bomService;
    private Map<String, Map<String, Object>> supplierList;
    private Map<String, Map<String, Object>> manufacturerList;
    private Map<String, Map<String, Object>> currencyMap = new HashMap<>();
    private String BOM_CATEGORY_ONE_THING = "一物一码";
    private String BOM_CATEGORY_ONE_CLASS = "一类一码";

    public BomExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        bomStockListService = SpringContextHolder.getBean("bomStockListServiceImpl");
        selectOptionService = SpringContextHolder.getBean("selectOptionServiceImpl");
        dynamicCommonService = SpringContextHolder.getBean("dynamicCommonServiceImpl");
        bomService = SpringContextHolder.getBean("bomServiceImpl");
        facilitiesService = SpringContextHolder.getBean("facilitiesServiceImpl");
        supplierList = facilitiesService.queryOuterSupplierList(schemaName);
        manufacturerList = facilitiesService.queryOuterManufacturerList(schemaName);

        List<Map<String, Object>> currencyList = selectOptionService.getSelectOptionList(methodParam, "currency", null);
        if (RegexUtil.optIsPresentList(currencyList)) {
            currencyList.forEach(c -> currencyMap.put(RegexUtil.optStrOrBlank(c.get("name")), c));
        }

        msgInfo = new HashMap<>();
        msgInfo.put("msgError", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ)); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            msgInfo.put("msgErrorFieldType", msgErrorFieldType);
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
//            msgInfo.put("msgErrorBomCategoryNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorBomNameNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorMtCodeNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorMtCodeRepeat", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BA) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件型号重复
            msgInfo.put("msgErrorMtCodeExist", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BA) + "已存在");
            msgInfo.put("msgErrorBomTypeNull", msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorBomTypeData", msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorBomUnitNull", msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorBomUnitData", msgColumnNo + "（E）" + msgErrorData);
            msgInfo.put("msgErrorQuantity", msgErrorFieldType + msgErrorTmp + "（F）" + msgErrorTmpVal);
            msgInfo.put("msgErrorShowPrice", msgErrorFieldType + msgErrorTmp + "（G）" + msgErrorTmpVal);
            msgInfo.put("msgErrorCurrencyData", msgColumnNo + "（H）" + msgErrorData);
            msgInfo.put("msgErrorSupplierData", msgColumnNo + "（I）" + msgErrorData);
            msgInfo.put("msgErrorSupplyCycle", msgErrorFieldType + msgErrorTmp + "（J）" + msgErrorTmpVal);
            msgInfo.put("msgErrorManufacturerData", msgColumnNo + "（K）" + msgErrorData);
            msgInfo.put("msgErrorServiceLife", msgErrorFieldType + msgErrorTmp + "（L）" + msgErrorTmpVal);
            msgInfo.put("msgErrorStockNull", msgColumnNo + "（M）" + msgErrorNull);
            msgInfo.put("msgErrorStockData", msgColumnNo + "（M）" + msgErrorData);
            msgInfo.put("msgErrorSecQuantity", msgErrorFieldType + msgErrorTmp + "（O）" + msgErrorTmpVal);
            msgInfo.put("msgErrorMaxSecQuantity", msgErrorFieldType + msgErrorTmp + "（P）" + msgErrorTmpVal);
        }
    }
    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Set<String> materialCodeSet = new HashSet<>();
        Set<String> stockSet = new HashSet<>();
        Set<String> bomTypeSet = new HashSet<>();
        Set<String> bomUnitSet = new HashSet<>();
        String bomCategory = null;//备件类型：一物一码、一类一码
        String bomName = null;//备件名称
        String materialCode = null;//物料编码
        String bomType = null;//备件类型
        String bomModel = null;//规格型号
        String bomUnit = null;//单位
        String quantity = null;//数量
        String showPrice = null;//单价
        String currency = null;//货币
        String supplier = null;//供应商
        String supplyCycle = null;//供货周期
        String manufacturer = null;//制造商
        String serviceLife = null;//使用寿命
        String bomStock = null;//库房
        String securityQuantity = null;//最小库存
        String maxSecurityQuantity = null;//最大库存
        for (LinkedHashMap row : dataList) {
            materialCodeSet.add(RegexUtil.optStrOrBlank(row.get(1)));
            bomTypeSet.add(RegexUtil.optStrOrBlank(row.get(2)));
            bomUnitSet.add(RegexUtil.optStrOrBlank(row.get(4)));
            stockSet.add(RegexUtil.optStrOrBlank(row.get(12)));
        }
//        Map<String, Map<String, Object>> materialCodeMap = new HashMap<>();//查询导入的物料编码在系统中是否已录入
//        if(materialCodeSet.size() > 0) {
//            materialCodeMap = bomService.queryBomByMaterialCodes(schemaName, new ArrayList<>(materialCodeSet));
//        }
        Map<String, Map<String, Object>> stockMap = new HashMap<>();
        if (stockSet.size() > 0) {
            stockMap = bomStockListService.queryStockByCode(schemaName, new ArrayList<>(stockSet));
        }
        Map<String, Map<String, Object>> bomTypeMap = new HashMap<>();
        if (bomTypeSet.size() > 0) {
            bomTypeMap = bomStockListService.queryBomTypeByCode(schemaName, new ArrayList<>(bomTypeSet));
        }
        Map<String, Map<String, Object>> bomUnitMap = new HashMap<>();
        if (bomUnitSet.size() > 0) {
            bomUnitMap = bomStockListService.queryBomUnitByCode(schemaName, new ArrayList<>(bomUnitSet));
        }
        bomTypeSet.clear();
        bomUnitSet.clear();
        stockSet.clear();
        materialCodeSet.clear();//先清空，记录每一行的物料编码，校验上传数据中是否有重复
        for (LinkedHashMap row : dataList) {
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
//            bomCategory = RegexUtil.optStrOrBlank(row.get(0));
            bomName = RegexUtil.optStrOrBlank(row.get(0));
            materialCode = RegexUtil.optStrOrBlank(row.get(1));
            bomType = RegexUtil.optStrOrBlank(row.get(2));
            bomUnit = RegexUtil.optStrOrBlank(row.get(4));
            quantity = RegexUtil.optStrOrBlank(row.get(5));
            showPrice = RegexUtil.optStrOrBlank(row.get(6));
            currency = RegexUtil.optStrOrBlank(row.get(7));
            supplier = RegexUtil.optStrOrBlank(row.get(8));
            supplyCycle = RegexUtil.optStrOrBlank(row.get(9));
            manufacturer = RegexUtil.optStrOrBlank(row.get(10));
            serviceLife = RegexUtil.optStrOrBlank(row.get(11));
            bomStock = RegexUtil.optStrOrBlank(row.get(12));
            securityQuantity = RegexUtil.optStrOrBlank(row.get(14));
            maxSecurityQuantity = RegexUtil.optStrOrBlank(row.get(15));
//            if (RegexUtil.optIsBlankStr(bomCategory) ||
//                    (RegexUtil.optIsPresentStr(bomCategory) && !BOM_CATEGORY_ONE_CLASS.equals(bomCategory) && !BOM_CATEGORY_ONE_THING.equals(bomCategory))) {
//                msg.append(msgInfo.get("msgErrorBomCategoryNull"));
//            }
            if (RegexUtil.optIsBlankStr(bomName)) {
                msg.append(msgInfo.get("msgErrorBomNameNull"));
            }
            if (RegexUtil.optIsBlankStr(materialCode)) {
                msg.append(msgInfo.get("msgErrorMtCodeNull"));
            }else {
//                if(materialCodeSet.contains(materialCode)){
//                    msg.append(msgInfo.get("msgErrorMtCodeRepeat"));
////                }else if(materialCodeMap.containsKey(materialCode)) {
////                    msg.append(msgInfo.get("msgErrorMtCodeExist"));
//                }else {
//                    materialCodeSet.add(materialCode);
//                }

                materialCodeSet.add(materialCode);
            }
            if (RegexUtil.optIsBlankStr(bomType)) {
                msg.append(msgInfo.get("msgErrorBomTypeNull"));
            } else if (!bomTypeMap.containsKey(bomType)) {
                msg.append(msgInfo.get("msgErrorBomTypeData"));
            }
            if (RegexUtil.optIsBlankStr(bomUnit)) {
                msg.append(msgInfo.get("msgErrorBomUnitNull"));
            } else if (!bomUnitMap.containsKey(bomUnit)) {
                msg.append(msgInfo.get("msgErrorBomUnitData"));
            }
            if (RegexUtil.optIsPresentStr(quantity) && !RegexUtil.isInteger(quantity)) {
                msg.append(msgInfo.get("msgErrorQuantity"));
            }
            if (RegexUtil.optIsPresentStr(showPrice) && !RegexUtil.isNumeric(showPrice)) {
                msg.append(msgInfo.get("msgErrorShowPrice") + "(" + showPrice + ")");
            }
            if (RegexUtil.optIsPresentStr(currency) && !currencyMap.containsKey(currency)) {
                msg.append(msgInfo.get("msgErrorCurrencyData"));
            }
            if (RegexUtil.optIsPresentStr(supplier) && !supplierList.containsKey(supplier)) {
                msg.append(msgInfo.get("msgErrorSupplierData"));
            }
            if (RegexUtil.optIsPresentStr(supplyCycle) && !RegexUtil.isInteger(supplyCycle)) {
                msg.append(msgInfo.get("msgErrorSupplyCycle") + "(" + supplyCycle + ")");
            }
            if (RegexUtil.optIsPresentStr(manufacturer) && !manufacturerList.containsKey(manufacturer)) {
                msg.append(msgInfo.get("msgErrorManufacturerData"));
            }
            if (RegexUtil.optIsPresentStr(serviceLife) && !RegexUtil.isInteger(serviceLife)) {
                msg.append(msgInfo.get("msgErrorServiceLife") + "(" + serviceLife + ")");
            }
            if(RegexUtil.optIsPresentStr(quantity) && RegexUtil.isInteger(quantity) && NumberUtils.toInt(quantity) > 0){//只有数量大于0时，才校验库房
                if (RegexUtil.optIsBlankStr(bomStock)) {
                    msg.append(msgInfo.get("msgErrorStockNull"));
                } else if (!stockMap.containsKey(bomStock)) {
                    msg.append(msgInfo.get("msgErrorStockData"));
                }
            }
            if (RegexUtil.optIsPresentStr(securityQuantity) && !RegexUtil.isNumeric(securityQuantity)) {
                msg.append(msgInfo.get("msgErrorSecQuantity") + "(" + securityQuantity + ")");
            }
            if (RegexUtil.optIsPresentStr(maxSecurityQuantity) && !RegexUtil.isNumeric(maxSecurityQuantity)) {
                msg.append(msgInfo.get("msgErrorMaxSecQuantity") + "(" + maxSecurityQuantity + ")");
            }
            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
        }
        if (null != stockMap) {
            stockMap.clear();
        }
        if (null != bomTypeMap) {
            bomTypeMap.clear();
        }
        if (null != bomUnitMap) {
            bomUnitMap.clear();
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Date now = new Date();
        Set<String> stockSet = new HashSet<>();
        Set<String> bomTypeSet = new HashSet<>();
        Set<String> bomUnitSet = new HashSet<>();
        Set<String> materialCodeSet = new HashSet<>();
        for (LinkedHashMap row : dataList) {
            bomTypeSet.add(RegexUtil.optStrOrBlank(row.get(2)));
            bomUnitSet.add(RegexUtil.optStrOrBlank(row.get(4)));
            stockSet.add(RegexUtil.optStrOrBlank(row.get(12)));
            materialCodeSet.add(RegexUtil.optStrOrBlank(row.get(1)));
        }
        List<LinkedHashMap> rawDataList = (List<LinkedHashMap>) SerializationUtils.clone((Serializable) dataList);
        Map<String, Map<String, Object>> stockMap = bomStockListService.queryStockByCode(schemaName, new ArrayList<>(stockSet));
        Map<String, Map<String, Object>> bomTypeMap = bomStockListService.queryBomTypeByCode(schemaName, new ArrayList<>(bomTypeSet));
        Map<String, Map<String, Object>> bomUnitMap = bomStockListService.queryBomUnitByCode(schemaName, new ArrayList<>(bomUnitSet));
        stockSet.clear();
        bomTypeSet.clear();
        bomUnitSet.clear();
        List<Map<String, Object>> bomList = new ArrayList<>();
        for (LinkedHashMap row : dataList) {
            String bomCategory = RegexUtil.optStrOrBlank(row.get(0));
            row.put(2, String.valueOf(bomTypeMap.get(RegexUtil.optStrOrBlank(row.get(2))).get("id")));
            row.put(4, String.valueOf(bomUnitMap.get(RegexUtil.optStrOrBlank(row.get(4))).get("id")));
            row.put(5, RegexUtil.optIsPresentStr(row.get(5))?NumberUtils.toInt(RegexUtil.optStrOrBlank(row.get(5))): 0);
            row.put(6, RegexUtil.optIsPresentStr(row.get(6))?new BigDecimal(String.valueOf(row.get(6))): 0);
            row.put(7, RegexUtil.optIsPresentStr(row.get(7))?currencyMap.get(row.get(7)).get("code"): 0);
            row.put(8, RegexUtil.optIsPresentStr(row.get(8))?supplierList.get(row.get(8)).get("id"): 0);
            row.put(9, RegexUtil.optIsPresentStr(row.get(9))?Integer.valueOf(String.valueOf(row.get(9))): 0);
            row.put(10, RegexUtil.optIsPresentStr(row.get(10))?manufacturerList.get(row.get(10)).get("id"): 0);
            row.put(11, RegexUtil.optIsPresentStr(row.get(11))?Integer.valueOf(String.valueOf(row.get(11))): 0);
            row.put(12, RegexUtil.optIsPresentStr(row.get(12))?stockMap.get(row.get(12)).get("id"): 0);
            row.put(13,RegexUtil.optStrOrBlank(row.get(13)));
            row.put(14, RegexUtil.optIsPresentStr(row.get(14))?Integer.valueOf(String.valueOf(row.get(14))): 0);
            row.put(15, RegexUtil.optIsPresentStr(row.get(15))?Integer.valueOf(String.valueOf(row.get(15))): 0);
            row.put(16, RegexUtil.optIsPresentStr(row.get(16))?row.get(16):""); //备注remark
            bomList.add(getBomBean(row, now, bomCategory));
            count++;//记录导入条数
        }
        stockMap.clear();
        bomTypeMap.clear();
        bomUnitMap.clear();
        if (RegexUtil.optIsPresentList(bomList)) {
            //批量新增、更新备件信息（按物料编码判断是否为新增备件）
            bomService.batchInsertBom(schemaName, bomList);
            //查询导入的物料编码在系统中生成的ID
            int i = 0;
            Map<String, Map<String, Object>> materialCodeMap = bomService.queryBomByMaterialCodes(schemaName, new ArrayList<>(materialCodeSet));
            if(materialCodeMap != null && materialCodeMap.size() > 0){
                List<Map<String, Object>> bomStockList = new ArrayList<>();
                List<Map<String, Object>> bomStockDetailList = new ArrayList<>();
                List<Map<String, Object>> bomSuppliesList = new ArrayList<>();
                List<Map<String, Object>> bomImportList = new ArrayList<>();
                for (LinkedHashMap row : dataList) {
                    String bomCode;
                    if(BOM_CATEGORY_ONE_THING.equals(RegexUtil.optStrOrBlank(row.get(0)))){
                        //一物一码的备件，编码单独生成
                        bomCode = dynamicCommonService.getBomCode(methodParam);
                    }else {
                        bomCode = String.valueOf(row.get(1));//一类一码的备件，备件编码跟物料编码相同
                    }
                    String bomId = RegexUtil.optStrOrBlank(bomList.get(i).get("id"));
                    if(NumberUtils.toInt(RegexUtil.optStrOrNull(row.get(5))) > 0){//数量大于0时，需要做备件入库操作（不走流程，直接入库）
                        //long bomId = (Long)materialCodeMap.get(String.valueOf(row.get(1))).get("id");
                        bomStockList.add(getBomStockBean(row, bomId));
                        bomStockDetailList.add(getBomStockDetailBean(row, bomCode, bomId));
                    }
                    bomSuppliesList.add(getSuppliesBean(row, bomCode, bomId));
                    bomImportList.add(getBomImportBean(row, bomId, rawDataList.get(i)));
                    i++;
                }
                if (RegexUtil.optIsPresentList(bomStockList)) {
                    //commonUtilService.doBatchInsertSql(schemaName, "_sc_bom_stock", SqlConstant._sc_bom_stock_columns, bomStockList);
                    bomService.batchInsertOrUpdateBomStock(schemaName,bomStockList);
                }
                if (RegexUtil.optIsPresentList(bomStockDetailList)) {
                    //commonUtilService.doBatchInsertSql(schemaName, "_sc_bom_stock_detail", SqlConstant._sc_bom_stock_detail_columns, bomStockDetailList);
                    bomService.batchInsertOrUpdateBomStockDetail(schemaName,bomStockDetailList);
                }
                if (RegexUtil.optIsPresentList(bomSuppliesList)) {
                    commonUtilService.doBatchInsertSql(schemaName, "_sc_bom_supplies", SqlConstant._sc_bom_supplies_columns, bomSuppliesList);
                }
                if (RegexUtil.optIsPresentList(bomImportList)) {
                    bomService.batchInsertBomImport(schemaName,bomImportList);
                }
            }
        }
    }

    private Map<String, Object> getBomBean(LinkedHashMap row, Date now, String bomCategory) {
        Map<String, Object> bomBean = new HashMap<>();
        bomBean.put("bom_name", row.get(0));
        bomBean.put("material_code", row.get(1));
        bomBean.put("type_id", row.get(2));
        bomBean.put("bom_model", row.get(3) == null?" ":row.get(3));
        bomBean.put("unit_id", row.get(4));
        bomBean.put("show_price", row.get(6));
        bomBean.put("currency_id", row.get(7));
        bomBean.put("manufacturer_id", row.get(10));
        bomBean.put("service_life", row.get(11));
        bomBean.put("code_classification", BOM_CATEGORY_ONE_THING.equals(bomCategory)?2:1);
        bomBean.put("data_order", 0);
        bomBean.put("is_from_service_supplier", 0);
        bomBean.put("is_use", StatusConstant.IS_USE_YES);
        bomBean.put("create_time", now);
        bomBean.put("create_user_id", methodParam.getUserId());
        bomBean.put("status", StatusConstant.STATUS_ENABLE);
        bomBean.put("remark",row.get(16));
        bomBean.put("id", SenscloudUtil.generateUUIDStr());
        return bomBean;
    }

    private Map<String, Object> getBomStockBean(LinkedHashMap row, String bomId) {
        Map<String, Object> bomStockBean = new HashMap<>();
        bomStockBean.put("bom_id", bomId);
        bomStockBean.put("quantity", row.get(5));
        bomStockBean.put("show_price", row.get(6));
        bomStockBean.put("currency_id", row.get(7));
        bomStockBean.put("stock_id", row.get(12));
        bomStockBean.put("store_position", row.get(13) == null?"":row.get(13));
        bomStockBean.put("security_quantity", row.get(14));
        bomStockBean.put("max_security_quantity", row.get(15));
        return bomStockBean;
    }

    private Map<String, Object> getBomStockDetailBean(LinkedHashMap row, String bomCode, String bomId) {
        Map<String, Object> bomStockDetailBean = new HashMap<>();
        bomStockDetailBean.put("bom_id", bomId);
        bomStockDetailBean.put("bom_code", bomCode);
        bomStockDetailBean.put("quantity", row.get(5));
        bomStockDetailBean.put("stock_id", row.get(12));
        bomStockDetailBean.put("store_position", row.get(13));
        return bomStockDetailBean;
    }

    private Map<String, Object> getSuppliesBean(LinkedHashMap row, String bomCode, String bomId) {
        Map<String, Object> suppliesBean = new HashMap<>();
        suppliesBean.put("bom_id", bomId);
        suppliesBean.put("bom_code", bomCode);
        suppliesBean.put("is_use", StatusConstant.IS_USE_YES);
        suppliesBean.put("supplier_id", row.get(8));
        suppliesBean.put("supply_period", row.get(9));
        return suppliesBean;
    }

    private Map<String, Object> getBomImportBean(LinkedHashMap row, String bomId, LinkedHashMap rawRow) {
        Map<String, Object> bomImportBean = new HashMap<>();
        bomImportBean.put("id", SenscloudUtil.generateUUIDStr());
        bomImportBean.put("bom_id", bomId);
        bomImportBean.put("stock_id", row.get(12));
        bomImportBean.put("create_user_id", methodParam.getUserId());
        bomImportBean.put("bom_name", row.get(0));
        bomImportBean.put("material_code", row.get(1));
        bomImportBean.put("bom_type", rawRow.get(2));
        bomImportBean.put("bom_model", rawRow.get(3));
        bomImportBean.put("bom_unit", rawRow.get(4));
        bomImportBean.put("quantity", row.get(5));
        bomImportBean.put("show_price", row.get(6));
        bomImportBean.put("currency", rawRow.get(7));
        bomImportBean.put("supplier", rawRow.get(8));
        bomImportBean.put("supply_cycle", rawRow.get(9));
        bomImportBean.put("manufacturer", rawRow.get(10));
        bomImportBean.put("service_life", rawRow.get(11));
        bomImportBean.put("bom_stock", rawRow.get(12));
        bomImportBean.put("security_quantity", rawRow.get(14));
        bomImportBean.put("max_security_quantity", rawRow.get(15));
        bomImportBean.put("remark", rawRow.get(16));
        bomImportBean.put("store_position", row.get(13));
        return bomImportBean;
    }

}
