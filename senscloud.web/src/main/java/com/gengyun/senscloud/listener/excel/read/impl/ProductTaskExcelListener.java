package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.dynamic.DynamicCommonService;
import com.gengyun.senscloud.service.dynamic.ProductTaskService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * 设备导入监听器
 */
public class ProductTaskExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(ProductTaskExcelListener.class);

    private ProductTaskService productTaskService;
    private DynamicCommonService dynamicCommonService;

    public ProductTaskExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        productTaskService = SpringContextHolder.getBean("productTaskServiceImpl");
        dynamicCommonService = SpringContextHolder.getBean("dynamicCommonServiceImpl");
        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
            String msgErrorDate = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DATE_T);//日期
            msgInfo.put("msgErrorAssetNameNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorAssetCodeNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorAssetCodeRepeat", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_AE) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件编号重复
            msgInfo.put("msgErrorAssetCodeExist", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_AE) + "已存在");
            msgInfo.put("msgErrorAssetTypeNull", msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorAssetTypeData", msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorAssetModelNull", msgColumnNo + "（D）" + msgErrorNull);
            msgInfo.put("msgErrorAssetModelData", msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorPositionCodeNull", msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorPositionCodeData", msgColumnNo + "（E）" + msgErrorData);
            msgInfo.put("msgErrorAssetStatusNull", msgColumnNo + "（F）" + msgErrorNull);
            msgInfo.put("msgErrorAssetStatusError", msgColumnNo + "（F）" +
                    commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_MR) +
                    commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_MP) + msgError);//资产状态错误
            msgInfo.put("msgErrorLevelData", msgColumnNo + "（G）" + msgErrorData);
            msgInfo.put("msgErrorEnableTime", msgColumnNo + "（H）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorUseYear", msgErrorFieldType + msgErrorTmp + "（I）" + msgErrorTmpVal);
            msgInfo.put("msgErrorBuyDate", msgColumnNo + "（J）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorSupplierData", msgColumnNo + "（K）" + msgErrorData);
            msgInfo.put("msgErrorManufacturerData", msgColumnNo + "（L）" + msgErrorData);
            msgInfo.put("msgErrorTaxPrice", msgErrorFieldType + msgErrorTmp + "（M）" + msgErrorTmpVal);
            msgInfo.put("msgErrorCurrencyData", msgColumnNo + "（N）" + msgErrorData);
            msgInfo.put("msgErrorTaxRate", msgErrorFieldType + msgErrorTmp + "（O）" + msgErrorTmpVal);
            msgInfo.put("msgErrorInstallDate", msgColumnNo + "（P）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorInstallPrice", msgErrorFieldType + msgErrorTmp + "（Q）" + msgErrorTmpVal);
            msgInfo.put("msgErrorInstallCurrencyData", msgColumnNo + "（R）" + msgErrorData);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        for (LinkedHashMap row : dataList) {
            //任务单号不能重复
            String task_code = row.get(4).toString();//任务单号
            Map<String, Object> well = productTaskService.findProductTaskWellByTaskCode(methodParam, task_code);
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
            if (RegexUtil.optNotNull(well).isPresent()) {
                msg.append("任务单号：" + task_code + "重复");
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        List<Map<String, Object>> productTaskWellList = new ArrayList<>();
        List<Map<String, Object>> productTaskWellItemList = new ArrayList<>();
        Date now = new Date();
        String product_task_code = dynamicCommonService.getProductTaskCode(methodParam);
        Map<String, Object> productTaskMap = new HashMap<>();
        productTaskMap.put("product_task_code", product_task_code);
        productTaskMap.put("create_time", now);
        productTaskMap.put("create_user_id", methodParam.getUserId());
        productTaskService.newProductTask(methodParam, productTaskMap);
        for (LinkedHashMap row : dataList) {
            //新增生产作业下井表
            Map<String, Object> productTaskWellMap = new HashMap<>();
            String task_code = row.get(4).toString();//任务单号
            String well_no = row.get(7).toString();//井号
            Integer go_down_count = Integer.valueOf(row.get(29).toString());//下井次数
            String task_well_code = SenscloudUtil.generateUUIDStr();
            productTaskWellMap.put("task_well_code", task_well_code);
            productTaskWellMap.put("product_task_code", product_task_code);
            productTaskWellMap.put("task_code", task_code);
            productTaskWellMap.put("well_no", well_no);
            productTaskWellMap.put("go_down_count", go_down_count);
            productTaskWellMap.put("create_time", now);
            productTaskWellMap.put("create_user_id", methodParam.getUserId());
            productTaskWellList.add(productTaskWellMap);
            //新增生产作业下井次数表
            for (int i = 0; i <= go_down_count; i++) {
                String task_well_item_code = SenscloudUtil.generateUUIDStr();
                Map<String, Object> productTaskWellItemMap = new HashMap<>();
                productTaskWellItemMap.put("task_well_item_code", task_well_item_code);
                productTaskWellItemMap.put("task_well_code", task_well_code);
                productTaskWellItemList.add(productTaskWellItemMap);
            }
        }
        productTaskService.newProductTaskWell(methodParam, productTaskWellList);
        productTaskService.newProductTaskWellItem(methodParam, productTaskWellItemList);
    }
}
