package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.dynamic.DynamicCommonService;
import com.gengyun.senscloud.service.dynamic.ProductTaskService;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 设备导入监听器
 */
public class ProductTaskExcelListenerFor40 extends BaseExcelListener {
//    private static final Logger logger = LoggerFactory.getLogger(ProductTaskExcelListenerFor40.class);

    private ProductTaskService productTaskService;
    private DynamicCommonService dynamicCommonService;

    public ProductTaskExcelListenerFor40(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        productTaskService = SpringContextHolder.getBean("productTaskServiceImpl");
        dynamicCommonService = SpringContextHolder.getBean("dynamicCommonServiceImpl");
        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
            String msgErrorDate = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DATE_T);//日期
            msgInfo.put("msgErrorFGSNull", msgColumnNo + "（B）" + msgErrorNull);//A
            msgInfo.put("msgErrorXMBNull", msgColumnNo + "（C）" + msgErrorNull);//b
//            msgInfo.put("msgErrorAssetCodeRepeat", msgColumnNo + "（C）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_AE) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件编号重复
//            msgInfo.put("msgErrorAssetCodeExist", msgColumnNo + "（c）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_AE) + "已存在");
            msgInfo.put("msgErrorDWNull", msgColumnNo + "（D）" + msgErrorNull);//C
//            msgInfo.put("msgErrorAssetTypeData", msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorJFDWNull", msgColumnNo + "（I）" + msgErrorNull);//d
//            msgInfo.put("msgErrorAssetModelData", msgColumnNo + "（I）" + msgErrorData);
            msgInfo.put("msgErrorRWDHNull", msgColumnNo + "（J）" + msgErrorNull);//e
            msgInfo.put("msgErrorRWDHRepeat", msgColumnNo + "（J）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_CGI) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件编号重复
//            msgInfo.put("msgErrorPositionCodeData", msgColumnNo + "（J）" + msgErrorData);
            msgInfo.put("msgErrorRWDLXNull", msgColumnNo + "（K）" + msgErrorNull);//f
//            msgInfo.put("msgErrorAssetStatusError", msgColumnNo + "（K）" +
//                    commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_MR) +
//                    commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_MP) + msgError);//资产状态错误
            msgInfo.put("msgErrorZYMDNull", msgColumnNo + "（L）" + msgErrorNull);//e
//            msgInfo.put("msgErrorLevelData", msgColumnNo + "（L）" + msgErrorData);//g
            msgInfo.put("msgErrorJHNull", msgColumnNo + "（M）" + msgErrorNull);//e
//            msgInfo.put("msgErrorEnableTime", msgColumnNo + "（M）" + msgErrorDate + msgError);//h
            msgInfo.put("msgErrorJXNull", msgColumnNo + "（P）" + msgErrorNull);//e
//            msgInfo.put("msgErrorUseYear", msgErrorFieldType + msgErrorTmp + "（P）" + msgErrorTmpVal);//i
            msgInfo.put("msgErrorSGGYNull", msgColumnNo + "（S）" + msgErrorNull);//e
//            msgInfo.put("msgErrorManufacturerData", msgColumnNo + "（S）" + msgErrorData);//l
            msgInfo.put("msgErrorSFYYNull", msgColumnNo + "（T）" + msgErrorNull);//e
//            msgInfo.put("msgErrorTaxPrice", msgErrorFieldType + msgErrorTmp + "（T）" + msgErrorTmpVal);//m
            msgInfo.put("msgErrorQSLBNull", msgColumnNo + "（W）" + msgErrorNull);//e
//            msgInfo.put("msgErrorInstallDate", msgColumnNo + "（W）" + msgErrorDate + msgError);//p
            msgInfo.put("msgErrorZTCCNull", msgColumnNo + "（X）" + msgErrorNull);//e
//            msgInfo.put("msgErrorInstallPrice", msgErrorFieldType + msgErrorTmp + "（X）" + msgErrorTmpVal);//q
            msgInfo.put("msgErrorZDXDNull", msgColumnNo + "（Y）" + msgErrorNull);//e
//            msgInfo.put("msgErrorInstallCurrencyData", msgColumnNo + "（Y）" + msgErrorData);//r
            msgInfo.put("msgErrorTGCCNull", msgColumnNo + "（Z）" + msgErrorNull);//f
            msgInfo.put("msgErrorDMYQXLNull", msgColumnNo + "（AC）" + msgErrorNull);//f
            msgInfo.put("msgErrorZYXMNull", msgColumnNo + "（AG）" + msgErrorNull);//f
            msgInfo.put("msgErrorZSNull", msgColumnNo + "（AI）" + msgErrorNull);//f
            msgInfo.put("msgErrorXJCSNull", msgColumnNo + "（AK）" + msgErrorNull);//f
            msgInfo.put("msgErrorXJCSNumber", msgErrorFieldType + msgErrorTmp + "（AK）" + msgErrorTmpVal);
            msgInfo.put("msgErrorCGCSNull", msgColumnNo + "（AL）" + msgErrorNull);//f
            msgInfo.put("msgErrorJXYQGZCSNull", msgColumnNo + "（AN）" + msgErrorNull);//f
            msgInfo.put("msgErrorCLJDCNull", msgColumnNo + "（AP）" + msgErrorNull);//f
            msgInfo.put("msgErrorCLJDDNull", msgColumnNo + "（AQ）" + msgErrorNull);//f
            msgInfo.put("msgErrorCLMNull", msgColumnNo + "（AR）" + msgErrorNull);//f
            msgInfo.put("msgErrorQXSJSKNull", msgColumnNo + "（AV）" + msgErrorNull);//f
            msgInfo.put("msgErrorSHYXKSNull", msgColumnNo + "（AW）" + msgErrorNull);//f
            msgInfo.put("msgErrorSKZHDNull", msgColumnNo + "（BB）" + msgErrorNull);//f
            msgInfo.put("msgErrorSKDNull", msgColumnNo + "（BE）" + msgErrorNull);//f
            msgInfo.put("msgErrorQXNull", msgColumnNo + "（BG）" + msgErrorNull);//f
            msgInfo.put("msgErrorDCGLSNull", msgColumnNo + "（BM）" + msgErrorNull);//f
            msgInfo.put("msgErrorSJDJSJNull", msgColumnNo + "（BU）" + msgErrorNull);//f
            msgInfo.put("msgErrorSJDJSJ", msgColumnNo + "（BU）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorCFSJNull", msgColumnNo + "（BV）" + msgErrorNull);//f
            msgInfo.put("msgErrorCFSJ", msgColumnNo + "（BV）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorFHSJNull", msgColumnNo + "（BW）" + msgErrorNull);//f
            msgInfo.put("msgErrorFHSJ", msgColumnNo + "（BW）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorAZSJNull", msgColumnNo + "（BX）" + msgErrorNull);//f
            msgInfo.put("msgErrorAZSJ", msgColumnNo + "（BX）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorCCSJNull", msgColumnNo + "（BY）" + msgErrorNull);//f
            msgInfo.put("msgErrorCCSJ", msgColumnNo + "（BY）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorZZSJNull", msgColumnNo + "（CA）" + msgErrorNull);//f
            msgInfo.put("msgErrorZSJNull", msgColumnNo + "（CB）" + msgErrorNull);//f
            msgInfo.put("msgErrorGZSJNull", msgColumnNo + "（CC）" + msgErrorNull);//f
            msgInfo.put("msgErrorBZNull", msgColumnNo + "（CM）" + msgErrorNull);//f
            msgInfo.put("msgErrorRWDBZNull", msgColumnNo + "（CN）" + msgErrorNull);//f

        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        for (LinkedHashMap row : dataList) {
            //任务单号不能重复
            String fgs =RegexUtil.optStrOrVal(row.get(1),"");
            String xmb = RegexUtil.optStrOrVal(row.get(2),"");
            String dw = RegexUtil.optStrOrVal(row.get(3),"");
            String jfdw = RegexUtil.optStrOrVal(row.get(8),"");
            String task_code = RegexUtil.optStrOrVal(row.get(9),"");//任务单号
            String rwdlx = RegexUtil.optStrOrVal(row.get(10),"");
            String zymd = RegexUtil.optStrOrVal(row.get(11),"");
            String jh = RegexUtil.optStrOrVal(row.get(12),"");
            String jx = RegexUtil.optStrOrVal(row.get(15),"");
            String sggy = RegexUtil.optStrOrVal(row.get(18),"");
            String sfyy = RegexUtil.optStrOrVal(row.get(19),"");
            String qslb = RegexUtil.optStrOrVal(row.get(22),"");
            String ztcc = RegexUtil.optStrOrVal(row.get(23),"");
            String zdxd = RegexUtil.optStrOrVal(row.get(24),"");
            String tgcc = RegexUtil.optStrOrVal(row.get(25),"");
            String dmyqxl =RegexUtil.optStrOrVal(row.get(28),"");
            String zyxm = RegexUtil.optStrOrVal(row.get(32),"");
            String js = RegexUtil.optStrOrVal(row.get(34),"");
            String xjcs = RegexUtil.optStrOrVal(row.get(36),"");
            String cgcs = RegexUtil.optStrOrVal(row.get(37),"");
            String jxyqgzcs = RegexUtil.optStrOrVal(row.get(39),"");
            String cljdc = RegexUtil.optStrOrVal(row.get(41),"");
            String cljdd = RegexUtil.optStrOrVal(row.get(42),"");
            String clm = RegexUtil.optStrOrVal(row.get(43),"");
            String qxsjsk = RegexUtil.optStrOrVal(row.get(47),"");
            String shyxks = RegexUtil.optStrOrVal(row.get(48),"");
            String skzhd = RegexUtil.optStrOrVal(row.get(53),"");
            String skd = RegexUtil.optStrOrVal(row.get(56),"");
            String qx = RegexUtil.optStrOrVal(row.get(58),"");
            String dcgls = RegexUtil.optStrOrVal(row.get(64),"");
            String sjdjsj = RegexUtil.optStrOrVal(row.get(72),"");
            String cfsj = RegexUtil.optStrOrVal(row.get(73),"");
            String fhsj = RegexUtil.optStrOrVal(row.get(74),"");
            String azsj = RegexUtil.optStrOrVal(row.get(75),"");
            String ccsj = RegexUtil.optStrOrVal(row.get(76),"");
            String zzsj = RegexUtil.optStrOrVal(row.get(78),"");
            String zsj = RegexUtil.optStrOrVal(row.get(79),"");
            String gzsj = RegexUtil.optStrOrVal(row.get(80),"");
            String rwdbz = RegexUtil.optStrOrVal(row.get(91),"");
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
            if (RegexUtil.optIsBlankStr(task_code)) {
                msg.append(msgInfo.get("msgErrorRWDHNull"));
            } else {
                Map<String, Object> well = productTaskService.findProductTaskWellByTaskCode(methodParam, task_code);
                if (RegexUtil.optNotNull(well).isPresent()) {
                    msg.append(msgInfo.get("msgErrorRWDHRepeat"));
                }
            }
            if (RegexUtil.optIsBlankStr(jh)) {
                msg.append(msgInfo.get("msgErrorJHNull"));
            }
            if (RegexUtil.optIsBlankStr(xjcs)) {
                msg.append(msgInfo.get("msgErrorXJCSNull"));
            }else{
                try{
                    Integer.valueOf(xjcs);
                }catch (Exception e){
                    msg.append(msgInfo.get("msgErrorXJCS"));
                }
            }
            if (RegexUtil.optIsPresentStr(sjdjsj) && parseDate(sjdjsj) == null) {
                msg.append(msgInfo.get("msgErrorSJDJSJ") + "(" + sjdjsj + ")");
            }
            if (RegexUtil.optIsPresentStr(cfsj) && parseDate(cfsj) == null) {
                msg.append(msgInfo.get("msgErrorCFSJ") + "(" + cfsj + ")");
            }
            if (RegexUtil.optIsPresentStr(fhsj) && parseDate(fhsj) == null) {
                msg.append(msgInfo.get("msgErrorFHSJ") + "(" + fhsj + ")");
            }
            if (RegexUtil.optIsPresentStr(azsj) && parseDate(azsj) == null) {
                msg.append(msgInfo.get("msgErrorAZSJ") + "(" + azsj + ")");
            }
            if (RegexUtil.optIsPresentStr(ccsj) && parseDate(ccsj) == null) {
                msg.append(msgInfo.get("msgErrorCCSJ") + "(" + ccsj + ")");
            }
            if(rwdbz.length()>=255){
                msg.append("任务单备注：过长");
            }

            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        List<Map<String, Object>> productTaskWellList = new ArrayList<>();
        List<Map<String, Object>> productTaskWellItemList = new ArrayList<>();
        Date now = new Date();
        String product_task_code = dynamicCommonService.getProductTaskCode(methodParam);
        Map<String, Object> productTaskMap = new HashMap<>();
        productTaskMap.put("product_task_code", product_task_code);
        productTaskMap.put("create_time", now);
        productTaskMap.put("create_user_id", methodParam.getUserId());
        productTaskService.newProductTask(methodParam, productTaskMap);
        for (LinkedHashMap row : dataList) {
            //新增生产作业下井表
            Map<String, Object> productTaskWellMap = new HashMap<>();
            String branch_office = RegexUtil.optStrOrNull(row.get(1));//分公司
            String project_department = RegexUtil.optStrOrNull(row.get(2));//项目部
            String ranks = RegexUtil.optStrOrNull(row.get(3));//队伍
            String party_a =RegexUtil.optStrOrNull(row.get(8));//甲方单位
            String task_code = RegexUtil.optStrOrNull(row.get(9));//任务单号
            String task_type = RegexUtil.optStrOrNull(row.get(10));//任务单类型
            String operation_purpose =  RegexUtil.optStrOrNull(row.get(11));//作业目的
            String well_no = RegexUtil.optStrOrNull(row.get(12));//井号
            String well_type = RegexUtil.optStrOrNull(row.get(15));//井型
            String construction_technology = RegexUtil.optStrOrNull(row.get(18));//施工工艺
            String is_use_source = RegexUtil.optStrOrNull(row.get(19));//是否用源
            String bridge_plug_type = RegexUtil.optStrOrNull(row.get(22));//桥塞类别
            String bit_size = RegexUtil.optStrOrNull(row.get(23));//钻头尺寸
            String maximum_slope = RegexUtil.optStrOrNull(row.get(24));//最大斜度
            String casing_size = RegexUtil.optStrOrNull(row.get(25));//套管尺寸
            String ground_instrument_series = RegexUtil.optStrOrNull(row.get(28));//地面仪器系列
            String operation_items = RegexUtil.optStrOrNull(row.get(32));//作业项目
            String well_depth = RegexUtil.optStrOrNull(row.get(34));//井深
            Integer go_down_count = Integer.valueOf(row.get(36).toString());//下井次数
            String success_times = RegexUtil.optStrOrNull(row.get(37));//成功次数
            String downhole_instrument_failures_times = RegexUtil.optStrOrNull(row.get(39));//井下仪器故障次数
            String survey_section_from = RegexUtil.optStrOrNull(row.get(41));//测量井段从
            String survey_section_to = RegexUtil.optStrOrNull(row.get(42));//测量井段到
            String measuring_meter = RegexUtil.optStrOrNull(row.get(43));//测量米
            String several_cores_are_designed =  RegexUtil.optStrOrNull(row.get(47));//取心设计数颗
            String core_harvested_number = RegexUtil.optStrOrNull(row.get(48));//收获岩心颗数
            String total_shooting_thickness = RegexUtil.optStrOrNull(row.get(53));//射开总厚度
            String perforation_section = RegexUtil.optStrOrNull(row.get(56));//射孔段
            String gun_type = RegexUtil.optStrOrNull(row.get(58));//枪型
            String one_way_kilometers = RegexUtil.optStrOrNull(row.get(64));//单程公里数
            Date actual_arrival_time = parseDate(RegexUtil.optStrOrBlank(row.get(72)));//实际到井时间
            Date departure_time = parseDate(RegexUtil.optStrOrBlank(row.get(73)));//出发时间
            Date return_time = parseDate(RegexUtil.optStrOrBlank(row.get(74)));//返回时间
            Date installation_time = parseDate(RegexUtil.optStrOrBlank(row.get(75)));//安装时间
            Date demolition_time = parseDate(RegexUtil.optStrOrBlank(row.get(76)));//拆除时间
            String well_occupation_time = RegexUtil.optStrOrNull(row.get(78));//占井时间
            String total_time = RegexUtil.optStrOrNull(row.get(79));//总时间
            String breakdown_time = RegexUtil.optStrOrNull(row.get(80));//故障时间
            String task_notes = RegexUtil.optStrOrNull(row.get(91));//任务单备注


            String task_well_code = SenscloudUtil.generateUUIDStr();
            productTaskWellMap.put("task_well_code", task_well_code);
            productTaskWellMap.put("product_task_code", product_task_code);
            productTaskWellMap.put("task_code", task_code);
            productTaskWellMap.put("well_no", well_no);
            productTaskWellMap.put("go_down_count", go_down_count);
            productTaskWellMap.put("create_time", now);
            productTaskWellMap.put("create_user_id", methodParam.getUserId());
            productTaskWellMap.put("branch_office",branch_office);
            productTaskWellMap.put("project_department",project_department);
            productTaskWellMap.put("ranks",ranks);
            productTaskWellMap.put("party_a",party_a);
            productTaskWellMap.put("task_type",task_type);
            productTaskWellMap.put("operation_purpose",operation_purpose);
            productTaskWellMap.put("well_type",well_type);
            productTaskWellMap.put("construction_technology",construction_technology);
            productTaskWellMap.put("is_use_source",is_use_source);
            productTaskWellMap.put("bridge_plug_type",bridge_plug_type);
            productTaskWellMap.put("bit_size",bit_size);
            productTaskWellMap.put("maximum_slope",maximum_slope);
            productTaskWellMap.put("casing_size",casing_size);
            productTaskWellMap.put("ground_instrument_series",ground_instrument_series);
            productTaskWellMap.put("success_times",success_times);
            productTaskWellMap.put("downhole_instrument_failures_times",downhole_instrument_failures_times);
            productTaskWellMap.put("survey_section_from",survey_section_from);
            productTaskWellMap.put("survey_section_to",survey_section_to);
            productTaskWellMap.put("measuring_meter",measuring_meter);
            productTaskWellMap.put("several_cores_are_designed",several_cores_are_designed);
            productTaskWellMap.put("core_harvested_number",core_harvested_number);
            productTaskWellMap.put("total_shooting_thickness",total_shooting_thickness);
            productTaskWellMap.put("operation_items",operation_items);
            productTaskWellMap.put("well_depth",well_depth);
            productTaskWellMap.put("perforation_section",perforation_section);
            productTaskWellMap.put("gun_type",gun_type);
            productTaskWellMap.put("one_way_kilometers",one_way_kilometers);
            productTaskWellMap.put("actual_arrival_time",actual_arrival_time);
            productTaskWellMap.put("departure_time",departure_time);
            productTaskWellMap.put("return_time",return_time);
            productTaskWellMap.put("installation_time",installation_time);
            productTaskWellMap.put("demolition_time",demolition_time);
            productTaskWellMap.put("well_occupation_time",well_occupation_time);
            productTaskWellMap.put("total_time",total_time);
            productTaskWellMap.put("breakdown_time",breakdown_time);
            productTaskWellMap.put("task_notes",task_notes);

            productTaskWellList.add(productTaskWellMap);
            //新增生产作业下井次数表
            for (int i = 0; i < go_down_count; i++) {
                String task_well_item_code = SenscloudUtil.generateUUIDStr();
                Map<String, Object> productTaskWellItemMap = new HashMap<>();
                productTaskWellItemMap.put("task_well_item_code", task_well_item_code);
                productTaskWellItemMap.put("task_well_code", task_well_code);
                productTaskWellItemList.add(productTaskWellItemMap);
            }
            count++;
        }
        productTaskService.newProductTaskWell(methodParam, productTaskWellList);
        productTaskService.newProductTaskWellItem(methodParam, productTaskWellItemList);
    }


    /**
     * 字符串转日期格式
     *
     * @param value
     * @return
     */
    private Date parseDate(String value) {
        if (RegexUtil.optIsPresentStr(value)) {
            try {
                if (value.contains("-")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("/")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("年")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
                    return simpleDateFormat.parse(value);
                }
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }
}
