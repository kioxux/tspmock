package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.mapper.WorkflowManageMapper;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 工单模板导入
 */
public class WorkFlowExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(WorkFlowExcelListener.class);
    private WorkflowManageMapper workflowManageMapper;
    private SerialNumberService serialNumberService;
    Map<String, Object> paramMap = new HashMap<>();
    LinkedHashMap workTemplateCodeMap = new LinkedHashMap();
    String maxWorkFlowNodeId;
    String maxWorkFlowNodeColumnId;
    String maxWorkFlowNodeColumnLinkageId;
    String date;
    String sheetName1 = "工单流程信息";
    String sheetName2 = "工单流程节点信息";
    String sheetName3 = "工单流程及节点字段信息";
    String sheetName4 = "工单流程及节点字段特殊属性信息";
    String sheetName5 = "工单流程及节点字段联动信息";
    String sheetName6 = "工单流程及节点字段联动条件信息";
    String sheetName7 = "工单模板信息";
    String sheetName8 = "工单模板字段联动条件信息";
    String sheetName9 = "工单模板字段联动信息";
    String sheetName10 = "工单模板字段特殊属性信息";
    String sheetName11 = "工单模板字段信息";
    String sheetName12 = "工单数据源信息";

    public WorkFlowExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        workflowManageMapper = SpringContextHolder.getBean("workflowManageMapper");
        serialNumberService = SpringContextHolder.getBean("serialNumberServiceImpl");
        maxWorkFlowNodeId = commonUtilService.getMaxId(methodParam, "id", "_sc_work_flow_node");
        maxWorkFlowNodeColumnId = commonUtilService.getMaxId(methodParam, "id", "_sc_work_flow_node_column");
        maxWorkFlowNodeColumnLinkageId = commonUtilService.getMaxId(methodParam, "id", "_sc_work_flow_node_column_linkage");
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        date = format.format(new Date());

        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String sheet1 = sheetName1 + "-"; //sheet页
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorDate = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DATE_T);//日期
            String msgErrorNotExist = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DC);//信息不存在
            String msgErrorExist = commonUtilService.getLanguageInfoByUserLang(methodParam, "工单流程已存在");//信息已存在
            //sheet 1
            msgInfo.put("msgErrorWorkFlow", sheet1 + msgColumnNo + "（B）" + msgErrorExist);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        String sheetName = sheetHolder.getSheetName();
        if (sheetName1.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    item.put(fieldName, row.get(i));
                }
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                List<Map<String, Object>> work_flow = RegexUtil.optNotNullListOrNew(workflowManageMapper.findWorkFlowInfo(schemaName, item));
                if (!work_flow.isEmpty()) {
                    msg.append(msgInfo.get("msgErrorWorkFlow"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        String sheetName = sheetHolder.getSheetName();
        if (sheetName1.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    item.put(fieldName, row.get(i));
                }
                paramMap.put("pref_id", item.get("pref_id"));
                workflowManageMapper.insertWorkFlowInfo(schemaName, item);
                count++;//记录导入条数
            }
        } else if (sheetName2.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    if ("id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else {
                        item.put(fieldName, row.get(i));
                    }
                }
                workflowManageMapper.insertWorkFlowNode(schemaName, item);
            }
            this.modifyTblSeq("_sc_work_flow_node", "_sc_work_flow_node_id_seq");
        } else if (sheetName3.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    if ("id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else if ("word_flow_node_id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else {
                        item.put(fieldName, row.get(i));
                    }
                }
                workflowManageMapper.insertWorkFlowNodeColum(schemaName, item);
            }
            this.modifyTblSeq("_sc_work_flow_node_column", "_sc_work_flow_node_column_id_seq");
        } else if (sheetName4.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    if ("column_id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else {
                        item.put(fieldName, RegexUtil.optStrOrBlank(row.get(i)));
                    }
                }
                workflowManageMapper.insertWorkFlowNodeColumExt(schemaName, item);
            }
        } else if (sheetName5.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    if ("id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnLinkageId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else if ("column_id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else {
                        item.put(fieldName, row.get(i));
                    }
                }
                workflowManageMapper.insertWorkFlowNodeColumLinkage(schemaName, item);
            }
            this.modifyTblSeq("_sc_work_flow_node_column_linkage", "_sc_work_flow_node_column_linkage_id_seq");
        } else if (sheetName6.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    if ("linkage_id".equals(fieldName)) {
                        item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnLinkageId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                    } else {
                        item.put(fieldName, row.get(i));
                    }
                }
                workflowManageMapper.insertWorkFlowNodeColumLinkageCondition(schemaName, item);
            }
        } else if (sheetName7.equals(sheetName)) {
            List<LinkedHashMap> workTemplatelist = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                LinkedHashMap item = new LinkedHashMap();
                for (int i = 0; i < columns.size(); i++) {
                    String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                    if ("remark".equals(fieldName)) {
                        item.put(fieldName, RegexUtil.optNotBlankStrOpt(row.get(i)).map(rm -> date.concat("-").concat(rm)).orElse(date));
                    } else if ("work_template_code".equals(fieldName)) {
                        String uuid = serialNumberService.generateMaxBsCodeByType(methodParam, "work_template");
                        item.put(fieldName, uuid);
                        workTemplateCodeMap.put(row.get(i), uuid);
                    } else {
                        item.put(fieldName, row.get(i));
                    }
                }
                workTemplatelist.add(item);
            }
            List<LinkedHashMap<String, Object>> workFlowInfo = workflowManageMapper.findWorkFlowInfo(methodParam.getSchemaName(), paramMap);
            List<LinkedHashMap<String, Object>> workFlowNodeColumLinkageCodition = workflowManageMapper.findWorkFlowNodeColumLinkageCondition(methodParam.getSchemaName(), paramMap);
            for (LinkedHashMap flowInfo : workFlowInfo) {
                if (workTemplateCodeMap.containsKey(flowInfo.get("work_template_code"))) {
                    flowInfo.put("work_template_code", workTemplateCodeMap.get(flowInfo.get("work_template_code")));
                    workflowManageMapper.updateWorkFlowInfo(schemaName, flowInfo);
                }
            }
            for (LinkedHashMap flowNodeColumLinkageCodition : workFlowNodeColumLinkageCodition) {
                if (workTemplateCodeMap.containsKey(flowNodeColumLinkageCodition.get("sub_page_template_code"))) {
                    flowNodeColumLinkageCodition.put("sub_page_template_code", workTemplateCodeMap.get(flowNodeColumLinkageCodition.get("sub_page_template_code")));
                }
                if (workTemplateCodeMap.containsKey(flowNodeColumLinkageCodition.get("edit_sub_page_template_code"))) {
                    flowNodeColumLinkageCodition.put("edit_sub_page_template_code", workTemplateCodeMap.get(flowNodeColumLinkageCodition.get("edit_sub_page_template_code")));
                }
                if (workTemplateCodeMap.containsKey(flowNodeColumLinkageCodition.get("scan_fail_return_template_code"))) {
                    flowNodeColumLinkageCodition.put("scan_fail_return_template_code", workTemplateCodeMap.get(flowNodeColumLinkageCodition.get("scan_fail_return_template_code")));
                }
                workflowManageMapper.updateFlowNodeColumLinkageCodition(schemaName, flowNodeColumLinkageCodition);
            }
            workflowManageMapper.insertOrUpdateWorkTemplate(schemaName, workTemplatelist);
        } else if (sheetName8.equals(sheetName)) {
            if (!dataList.isEmpty()) {
                //workflowManageMapper.deleteWorkTemplateColumLinkageCondition(schemaName, paramMap);
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("linkage_id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnLinkageId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.deleteWorkTemplateColumLinkageConditionBy(schemaName, item);
                }
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("linkage_id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnLinkageId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else if ("sub_page_template_code".equals(fieldName)) {
                            RegexUtil.optNotBlankStrOpt(row.get(i)).map(c -> RegexUtil.optStrOrVal(workTemplateCodeMap.get(c), c)).ifPresent(nc -> item.put(fieldName, nc));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.insertWorkFlowNodeColumLinkageCondition(schemaName, item);
                }
            }
        } else if (sheetName9.equals(sheetName)) {
            if (!dataList.isEmpty()) {
                //workflowManageMapper.deleteWorkTemplateColumLinkage(schemaName, paramMap);
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnLinkageId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.deleteWorkTemplateColumLinkageBy(schemaName, item);
                }
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnLinkageId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else if ("column_id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else if ("node_id".equals(fieldName)) {
                            item.put(fieldName, workTemplateCodeMap.get(row.get(i)));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.insertWorkFlowNodeColumLinkage(schemaName, item);
                }
                this.modifyTblSeq("_sc_work_flow_node_column_linkage", "_sc_work_flow_node_column_linkage_id_seq");
            }
        } else if (sheetName10.equals(sheetName)) {
            if (!dataList.isEmpty()) {
                //workflowManageMapper.deleteWorkTemplateColumExt(schemaName, paramMap);
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("column_id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.deleteWorkTemplateColumExtBy(schemaName, item);
                }
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("column_id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else if ("node_id".equals(fieldName)) {
                            item.put(fieldName, workTemplateCodeMap.get(row.get(i)));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.insertWorkFlowNodeColumExt(schemaName, item);
                }
            }
        } else if (sheetName11.equals(sheetName)) {
            if (!dataList.isEmpty()) {
                //workflowManageMapper.deleteWorkTemplateColum(schemaName, paramMap);
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.deleteWorkTemplateColumBy(schemaName, item);
                }
                for (LinkedHashMap row : dataList) {
                    LinkedHashMap item = new LinkedHashMap();
                    for (int i = 0; i < columns.size(); i++) {
                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                        if ("id".equals(fieldName)) {
                            item.put(fieldName, new BigDecimal(maxWorkFlowNodeColumnId).add(RegexUtil.optBigDecimalOrVal(row.get(i), "0", null).abs()));
                        } else if ("node_id".equals(fieldName)) {
                            item.put(fieldName, workTemplateCodeMap.get(row.get(i)));
                        } else {
                            item.put(fieldName, row.get(i));
                        }
                    }
                    workflowManageMapper.insertWorkFlowNodeColum(schemaName, item);
                }
                this.modifyTblSeq("_sc_work_flow_node_column", "_sc_work_flow_node_column_id_seq");
            }
        } else if (sheetName12.equals(sheetName)) {
            //暂时不处理字典数据
//            if (dataList.isEmpty()) {
//                workflowManageMapper.deleteDataSource(schemaName, paramMap);
//                for (LinkedHashMap row : dataList) {
//                    LinkedHashMap item = new LinkedHashMap();
//                    for (int i = 0; i < columns.size(); i++) {
//                        String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
//                        item.put(fieldName, row.get(i));
//                    }
//                    workflowManageMapper.insertDataSource(schemaName, item);
//                }
//            }
        }
    }

    /**
     * 更新序列
     *
     * @param table 表名
     * @param seq   序列名
     */
    private void modifyTblSeq(String table, String seq) {
        commonUtilService.modifySeqByTblForDefault(methodParam, seq, table);
        int maxId = RegexUtil.optNotBlankStrOpt(commonUtilService.getMaxId(methodParam, "id", table)).map(Integer::valueOf).map(v -> v + 1).orElse(1);
        commonUtilService.modifySeqStartByTbl(methodParam, seq, maxId);
    }
}
