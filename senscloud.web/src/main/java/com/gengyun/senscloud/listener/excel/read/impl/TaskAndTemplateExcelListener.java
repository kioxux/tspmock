package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.TaskItemService;
import com.gengyun.senscloud.service.TaskTempLateService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.SerialNumberService;
import com.gengyun.senscloud.util.RegexUtil;

import java.util.*;

/**
 * 任务项、任务模板导入
 */
public class TaskAndTemplateExcelListener extends BaseExcelListener {
    private SerialNumberService serialNumberService;
    private TaskItemService taskItemService;
    private TaskTempLateService taskTemplateService;
    private MethodParam methodParam;
    private Map<String, Map<String, Object>> resultTypeMap = new HashMap<>();
    private Map<String, Map<String, Object>> isEnableMap = new HashMap<>();
    private String sheetName1;

    public TaskAndTemplateExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        this.methodParam = methodParam;
        SelectOptionService selectOptionService = SpringContextHolder.getBean("selectOptionServiceImpl");
        serialNumberService = SpringContextHolder.getBean("serialNumberServiceImpl");
        taskItemService = SpringContextHolder.getBean("taskItemServiceImpl");
        taskTemplateService = SpringContextHolder.getBean("taskTempLateServiceImpl");
        RegexUtil.optNotNullList(selectOptionService.getStaticSelectList(methodParam, "resultTypes"))
                .ifPresent(l -> l.forEach(r -> RegexUtil.optNotBlankStrOpt(r.get("text")).ifPresent(s -> resultTypeMap.put(s, r))));
        RegexUtil.optNotNullList(selectOptionService.getStaticSelectList(methodParam, "is_enable"))
                .ifPresent(l -> l.forEach(r -> RegexUtil.optNotBlankStrOpt(r.get("text")).ifPresent(s -> isEnableMap.put(s, r))));

        sheetName1 = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAQ_Q); // 任务项
        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ); // 错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String sheet1 = ""; // sheet页sheetName1 + "-"
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorNotExist = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DC); // 信息不存在
            msgInfo.put("msgErrorItemNoData", sheet1 + msgColumnNo + "（A）" + msgErrorNotExist);
            msgInfo.put("msgErrorItemNameNull", sheet1 + msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorResultTypeNull", sheet1 + msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorResultTypeData", sheet1 + msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorTempNoData", sheet1 + msgColumnNo + "（F）" + msgErrorNotExist);
            msgInfo.put("msgErrorTempNameNull", sheet1 + msgColumnNo + "（G）" + msgErrorNull);
            msgInfo.put("msgErrorTempStatusNull", sheet1 + msgColumnNo + "（H）" + msgErrorNull);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        String sheetName = sheetHolder.getSheetName();
        if (sheetName1.equals(sheetName)) {
            HashSet<String> itemNoSet = new HashSet<>();
            HashSet<String> tempNoSet = new HashSet<>();
            for (LinkedHashMap row : dataList) {
                RegexUtil.optNotBlankStrOpt(row.get(0)).map(String::trim).ifPresent(itemNoSet::add);
                RegexUtil.optNotBlankStrOpt(row.get(5)).map(String::trim).ifPresent(tempNoSet::add);
            }
            int num = Constants.EXCEL_BATCH_COUNT;
            List<String> itemNoList = new ArrayList<>();
            int insLen = itemNoSet.size();
            if (insLen > 0) {
                List<String> itemNoExcelList = new ArrayList<>(itemNoSet);
                int p = insLen / num + (insLen % num == 0 ? 0 : 1);
                for (int i = 0; i < p; i++) {
                    int begin = i * num;
                    int end = (i + 1) * num - 1;
                    end = (end < (insLen - 1) ? end : (insLen - 1));
                    RegexUtil.optNotNullListStr(taskItemService.getItemCodesByCodes(schemaName, itemNoExcelList.subList(begin, end + 1))).ifPresent(itemNoList::addAll);
                }
            }
            List<String> tempNoList = new ArrayList<>();
            int tnsLen = tempNoSet.size();
            if (tnsLen > 0) {
                List<String> tempNoExcelList = new ArrayList<>(tempNoSet);
                int p = tnsLen / num + (tnsLen % num == 0 ? 0 : 1);
                for (int i = 0; i < p; i++) {
                    int begin = i * num;
                    int end = (i + 1) * num - 1;
                    end = (end < (tnsLen - 1) ? end : (tnsLen - 1));
                    RegexUtil.optNotNullListStr(taskTemplateService.getTempCodesByCodes(schemaName, tempNoExcelList.subList(begin, end + 1))).ifPresent(tempNoList::addAll);
                }
            }
            itemNoSet.clear();
            tempNoSet.clear();
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                int i = 0;
                String itemNo = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String itemName = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String resultType = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String itemDsp = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String groupName = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempNo = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempName = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempStatus = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempDsp = RegexUtil.optStrOrBlankWithTrim(row.get(i));
                // 任务项信息不存在
                if (!"".equals(itemNo) && !itemNoList.contains(itemNo)) {
                    msg.append(msgInfo.get("msgErrorItemNoData"));
                }
                // 任务项内容存在时，任务项名称必填
                if ((!"".equals(itemNo) || !"".equals(resultType) || !"".equals(itemDsp) || !"".equals(groupName)) && "".equals(itemName)) {
                    msg.append(msgInfo.get("msgErrorItemNameNull"));
                }
                // 任务项内容存在时，结果类型必填
                if ((!"".equals(itemNo) || !"".equals(itemName) || !"".equals(itemDsp) || !"".equals(groupName)) && "".equals(resultType)) {
                    msg.append(msgInfo.get("msgErrorResultTypeNull"));
                }
                // 结果类型信息不存在，需要在系统中添加
                if (!"".equals(resultType) && !resultTypeMap.containsKey(resultType)) {
                    msg.append(msgInfo.get("msgErrorResultTypeData"));
                }
                // 模板信息不存在
                if (!"".equals(tempNo) && !tempNoList.contains(tempNo)) {
                    msg.append(msgInfo.get("msgErrorTempNoData"));
                }
                // 任务模板内容存在时，模板名称必填
                if ((!"".equals(groupName) || !"".equals(tempNo) || !"".equals(tempStatus) || !"".equals(tempDsp)) && "".equals(tempName)) {
                    msg.append(msgInfo.get("msgErrorTempNameNull"));
                }
                // 模板状态为空或格式有误
                if (!"".equals(tempStatus) && !isEnableMap.containsKey(tempStatus)) {
                    msg.append(msgInfo.get("msgErrorTempStatusNull"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
            itemNoList.clear();
            tempNoList.clear();
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        if (sheetName1.equals(sheetHolder.getSheetName())) {
            HashSet<String> itemNoSet = new HashSet<>();
            HashSet<String> tempNoSet = new HashSet<>();
            HashSet<String> midNoSet = new HashSet<>();
            for (LinkedHashMap row : dataList) {
                StringBuffer sb = new StringBuffer();
                RegexUtil.optNotBlankStrOpt(row.get(5)).map(String::trim).ifPresent(s -> {
                    tempNoSet.add(s);
                    sb.append(s).append("-");
                });
                RegexUtil.optNotBlankStrOpt(row.get(4)).map(String::trim).map(s -> s.concat("-")).ifPresent(sb::append);
                RegexUtil.optNotBlankStrOpt(row.get(0)).map(String::trim).ifPresent(s -> {
                    itemNoSet.add(s);
                    sb.append(s);
                });
                RegexUtil.optNotBlankStrOpt(sb).ifPresent(midNoSet::add);
            }
            int num = Constants.EXCEL_BATCH_COUNT;
            Map<String, Map<String, Object>> itemMap = new HashMap<>();
            int insLen = itemNoSet.size();
            if (insLen > 0) {
                List<String> itemNoExcelList = new ArrayList<>(itemNoSet);
                int p = insLen / num + (insLen % num == 0 ? 0 : 1);
                for (int i = 0; i < p; i++) {
                    int begin = i * num;
                    int end = (i + 1) * num - 1;
                    end = (end < (insLen - 1) ? end : (insLen - 1));
                    RegexUtil.optNotNullMapObj(taskItemService.findItemsByItemCode(schemaName, itemNoExcelList.subList(begin, end + 1))).ifPresent(itemMap::putAll);
                }
            }
            Map<String, Map<String, Object>> tempMap = new HashMap<>();
            int tnsLen = tempNoSet.size();
            if (tnsLen > 0) {
                List<String> tempNoExcelList = new ArrayList<>(tempNoSet);
                int p = tnsLen / num + (tnsLen % num == 0 ? 0 : 1);
                for (int i = 0; i < p; i++) {
                    int begin = i * num;
                    int end = (i + 1) * num - 1;
                    end = (end < (tnsLen - 1) ? end : (tnsLen - 1));
                    RegexUtil.optNotNullMapObj(taskTemplateService.getTaskTemplatesByCodes(schemaName, tempNoExcelList.subList(begin, end + 1))).ifPresent(tempMap::putAll);
                }
            }
            List<String> midDbList = new ArrayList<>();
            int mnsLen = midNoSet.size();
            if (mnsLen > 0) {
                List<String> midNoExcelList = new ArrayList<>(midNoSet);
                int p = mnsLen / num + (mnsLen % num == 0 ? 0 : 1);
                for (int i = 0; i < p; i++) {
                    int begin = i * num;
                    int end = (i + 1) * num - 1;
                    end = (end < (mnsLen - 1) ? end : (mnsLen - 1));
                    RegexUtil.optNotNullListStr(taskTemplateService.getTaskTemplateItemByInfo(schemaName, midNoExcelList.subList(begin, end + 1))).ifPresent(midDbList::addAll);
                }
            }
            itemNoSet.clear();
            tempNoSet.clear();
            midNoSet.clear();

            List<Map<String, Object>> itemList = new ArrayList<>();
            List<Map<String, Object>> tempList = new ArrayList<>();
            List<Map<String, Object>> midList = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                int i = 0;
                String itemNo = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String itemName = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String resultType = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String itemDsp = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String groupName = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                int checkInt = 0;
                if ((!"".equals(itemName) || !"".equals(resultType) || !"".equals(itemDsp) || !"".equals(groupName)) && "".equals(itemNo)) {
                    itemNo = serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.WORK_TASK); // 新增
                }
                if (!"".equals(itemNo)) {
                    Map<String, Object> item = RegexUtil.optMapOrNew(itemMap.get(itemNo));
                    item.put("task_item_code", itemNo);
                    item.put("task_item_name", itemName);
                    item.put("result_type", RegexUtil.optNotNullMap(resultTypeMap.get(resultType)).map(rt -> RegexUtil.optIntegerOrNull(rt.get("value"),
                            LangConstant.TITLE_CATEGORY_AE)).orElse(null));
                    item.put("requirements", itemDsp);
                    item.put("create_user_id", methodParam.getUserId());
                    itemList.add(item);
                    checkInt++;
                }
                String tempNo = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempName = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempStatus = RegexUtil.optStrOrBlankWithTrim(row.get(i++));
                String tempDsp = RegexUtil.optStrOrBlankWithTrim(row.get(i));
                if ((!"".equals(groupName) || !"".equals(tempName) || !"".equals(tempStatus) || !"".equals(tempDsp)) && "".equals(tempNo)) {
                    tempNo = serialNumberService.generateMaxBsCodeByType(methodParam, SqlConstant.TEMPLATE_TASK); // 新增
                }
                if (!"".equals(tempNo)) {
                    Map<String, Object> temp = RegexUtil.optMapOrNew(tempMap.get(tempNo));
                    temp.put("task_template_code", tempNo);
                    temp.put("task_template_name", tempName);
                    temp.put("remark", tempDsp);
                    temp.put("is_use", isEnableMap.get(tempStatus).get("value"));
                    temp.put("create_user_id", methodParam.getUserId());
                    tempList.add(temp);
                    checkInt++;
                }
                if (checkInt > 1 && !"".equals(groupName)) {
                    String tmpStr = tempNo + "-" + groupName + "-" + itemNo;
                    if (!midDbList.contains(tmpStr)) {
                        Map<String, Object> mid = new HashMap<>();
                        mid.put("task_template_code", tempNo);
                        mid.put("task_item_code", itemNo);
                        mid.put("group_name", groupName);
                        midList.add(mid);
                    }
                }
                count++; // 记录导入条数
            }
            if (itemList.size() > 0) {
                taskItemService.batchInsertItem(schemaName, itemList);
            }
            if (tempList.size() > 0) {
                taskTemplateService.batchInsertTaskTemp(schemaName, tempList);
            }
            if (midList.size() > 0) {
                taskTemplateService.batchInsertTaskTempItem(schemaName, midList);
            }
        }
    }
}
