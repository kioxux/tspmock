package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.AssetBomData;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.asset.AssetInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * easyexcel导入监听器——设备部件导入
 */
public class AssetBomExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(AssetBomExcelListener.class);

    private AssetInfoService assetInfoService;

    public AssetBomExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        assetInfoService = SpringContextHolder.getBean("assetInfoServiceImpl");

//        String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.CLM_NM); // 列号
//        String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.MSG_ERROR_A); // 为空或格式有误
//        msgInfo.put("msgError", commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.ERROR_E)); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
//            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.MSG_ERROR_C); // 类型错误，应该为：
//            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.MSG_ERROR_D); // （数字格式）列号
//            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.MSG_INFO_A); // 值
//            msgInfo.put("msgErrorAssetCodeNull", msgColumnNo + "（A）" + msgErrorNull);
//            msgInfo.put("msgErrorBomCodeNull", msgColumnNo + "（B）" + msgErrorNull);
//            msgInfo.put("msgErrorMtCodeNull", msgColumnNo + "（C）" + msgErrorNull);
//            msgInfo.put("msgErrorUseDaysNull", msgColumnNo + "（D）" + msgErrorNull);
//            msgInfo.put("msgErrorUseDays", msgErrorFieldType + msgErrorTmp + "（D）" + msgErrorTmpVal);
//            msgInfo.put("msgErrorMaintenanceDaysNull", msgColumnNo + "（E）" + msgErrorNull);
//            msgInfo.put("msgErrorMaintenanceDays", msgErrorFieldType + msgErrorTmp + "（E）" + msgErrorTmpVal);
//            msgInfo.put("msgErrorUnitTypeNull", msgColumnNo + "（F）" + msgErrorNull);
//            msgInfo.put("msgErrorQuantityNull", msgColumnNo + "（G）" + msgErrorNull);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        StringBuffer msg = null;
        String assetCode = null;
        String bomCode = null;
        String materialCode = null;
        String useDays = null;
        String maintenanceDays = null;
        String unitType = null;
        String quantity = null;
        String day = "天";//天
        String hour = "小时";//小时
        for (Object row : dataList) {
            msg = new StringBuffer("");
//            assetCode = row.getColumn1();
//            bomCode = row.getColumn2();
//            materialCode = row.getColumn3();
//            useDays = row.getColumn4();
//            maintenanceDays = row.getColumn5();
//            unitType = row.getColumn6();
//            quantity = row.getColumn7();
//            if (RegexUtil.optIsBlankStr(assetCode)) {
//                msg.append(msgInfo.get("msgErrorAssetCodeNull"));
//            }
//            if (RegexUtil.optIsBlankStr(bomCode)) {
//                msg.append(msgInfo.get("msgErrorBomCodeNull"));
//            }
//            if (RegexUtil.optIsBlankStr(materialCode)) {
//                msg.append(msgInfo.get("msgErrorMtCodeNull"));
//            }
//            if (RegexUtil.optIsBlankStr(useDays)) {
//                msg.append(msgInfo.get("msgErrorUseDaysNull"));
//            }else if (!RegexUtil.isNumeric(useDays)) {
//                msg.append(msgInfo.get("msgErrorUseDays") + "(" + useDays + ")");
//            }
//            if (RegexUtil.optIsBlankStr(maintenanceDays)) {
//                msg.append(msgInfo.get("msgErrorMaintenanceDaysNull"));
//            }else if (!RegexUtil.isNumeric(maintenanceDays)) {
//                msg.append(msgInfo.get("msgErrorMaintenanceDays") + "(" + maintenanceDays + ")");
//            }
//            if (RegexUtil.optIsBlankStr(unitType) || (!day.equals(unitType) && !hour.equals(unitType))) {
//                msg.append(msgInfo.get("msgErrorUnitTypeNull"));
//            }
//            if (RegexUtil.optIsBlankStr(quantity) || !RegexUtil.isInteger(quantity)) {
//                msg.append(msgInfo.get("msgErrorQuantityNull"));
//            }
//            String msgStr = msg.toString();
//            if (msgStr.startsWith(msgSplit)) {
//                msgStr = msgStr.substring(1);
//                addLog(String.valueOf(row.getExcelRowNo() + 1), msgStr);
//            }
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        String day = "天";//天
        String hour = "小时";//小时
        List<AssetBomData> assetBomDatas = new ArrayList<>();
        for (Object m : dataList) {
//            try {
//                int unitType = hour.equals(m.getColumn6())?1:(day.equals(m.getColumn6())?2:0);
//                assetBomDatas.add(new AssetBomData(m.getColumn1(), account, m.getColumn2(), m.getColumn3(), Float.parseFloat(m.getColumn4()), Float.parseFloat(m.getColumn5()), unitType));
//                count++;//记录处理总条数
//            } catch (NumberFormatException e) {
//                logger.error("", e);
//                addLog(String.valueOf(m.getExcelRowNo()), "更新失败");//更新失败
//            }
        }
//        if(!assetBomDatas.isEmpty()){
//            try {
//                //注意：考虑到性能问题，此处采用批量更新/插入处理，插入或者更新由sql语句判断
//                assetInfoService.insertBatchAssetBom(schemaName, assetBomDatas);
//            } catch (Exception e) {
//                logger.error("", e);
//                for(BaseExcelModel m : dataList){
//                    addLog(String.valueOf(m.getExcelRowNo()), "更新失败");//更新失败
//                }
//            }
//        }
//        assetBomDatas.clear();
    }
}
