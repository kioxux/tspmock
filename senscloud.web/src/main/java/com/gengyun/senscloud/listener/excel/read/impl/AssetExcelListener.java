package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SqlConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.FacilitiesService;
import com.gengyun.senscloud.service.asset.*;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.RegexUtil;
import com.gengyun.senscloud.util.ScdSesRecordUtil;
import com.gengyun.senscloud.util.SenscloudUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 设备导入监听器
 */
public class AssetExcelListener extends BaseExcelListener {
    private static final Logger logger = LoggerFactory.getLogger(AssetExcelListener.class);

    private Map<String, Map<String, Object>> assetStatusMap = new HashMap<>();
    private Map<String, Map<String, Object>> suppliesList;
    private Map<String, Map<String, Object>> currencyMap = new HashMap<>();
    private Map<String, Map<String, Object>> levelMap = new HashMap<>();
    private FacilitiesService facilitiesService;
    private SelectOptionService selectOptionService;
    private AssetInfoService assetInfoService;
    private AssetPositionService assetPositionService;
    private AssetCategoryService assetCategoryService;
    private AssetModelService assetModelService;
    private AssetDataService assetDataService;
    private String COLUMN_ASSET_ID = "唯一键";
    private String assetRunningStatus = null;

    public AssetExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);

        facilitiesService = SpringContextHolder.getBean("facilitiesServiceImpl");
        selectOptionService = SpringContextHolder.getBean("selectOptionServiceImpl");
        assetInfoService = SpringContextHolder.getBean("assetInfoServiceImpl");
        assetPositionService = SpringContextHolder.getBean("assetPositionServiceImpl");
        assetCategoryService = SpringContextHolder.getBean("assetCategoryServiceImpl");
        assetModelService = SpringContextHolder.getBean("assetModelServiceImpl");
        assetDataService = SpringContextHolder.getBean("assetDataServiceImpl");
        suppliesList = facilitiesService.queryOuterSuppliesList(schemaName);
        assetRunningStatus = assetDataService.searchAssetRunningStatusForRun(schemaName);

        List<Map<String, Object>> levelList = selectOptionService.getSelectOptionList(methodParam, "level", null);
        if (RegexUtil.optIsPresentList(levelList)) {
            levelList.forEach(l -> levelMap.put(RegexUtil.optStrOrBlank(l.get("text")), l));
        }

        List<Map<String, Object>> currencyList = selectOptionService.getSelectOptionList(methodParam, "currency", null);
        if (RegexUtil.optIsPresentList(currencyList)) {
            currencyList.forEach(c -> currencyMap.put(RegexUtil.optStrOrBlank(c.get("name")), c));
        }

        List<Map<String, Object>> statusList = selectOptionService.getSelectOptionList(methodParam, "asset_info_status", null);
        if (RegexUtil.optIsPresentList(statusList)) {
            statusList.forEach(s -> assetStatusMap.put(RegexUtil.optStrOrBlank(s.get("text")), s));
        }

        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorFieldType = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFK); // 类型错误，应该为：
            String msgErrorTmp = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AB_C); // （数字格式）列号
            String msgErrorTmpVal = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_KM); // 值
            String msgErrorDate = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DATE_T);//日期
            String msgCustomField = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_G);//自定义字段
            String msgErrorField = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_S) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//列表列错误
            //  列表列
            msgInfo.put("msgErrorAssetNameNull", msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorAssetCodeNull", msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorAssetCodeRepeat", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_AE) + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AJK)); //备件编号重复
            msgInfo.put("msgErrorAssetCodeExist", msgColumnNo + "（B）" + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_ASSET_AE) + "已存在");
            msgInfo.put("msgErrorAssetTypeNull", msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorAssetTypeData", msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorAssetModelNull", msgColumnNo + "（D）" + msgErrorNull);
            msgInfo.put("msgErrorAssetModelData", msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorPositionCodeNull", msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorPositionCodeData", msgColumnNo + "（E）" + msgErrorData);
            msgInfo.put("msgErrorAssetStatusNull", msgColumnNo + "（F）" + msgErrorNull);
            msgInfo.put("msgErrorAssetStatusError", msgColumnNo + "（F）" +
                    commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_MR) +
                    commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_MP) + msgError);//资产状态错误
            msgInfo.put("msgErrorLevelData", msgColumnNo + "（G）" + msgErrorData);
            msgInfo.put("msgErrorEnableTime", msgColumnNo + "（H）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorUseYear", msgErrorFieldType + msgErrorTmp + "（I）" + msgErrorTmpVal);
            msgInfo.put("msgErrorBuyDate", msgColumnNo + "（J）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorSupplierData", msgColumnNo + "（K）" + msgErrorData);
            msgInfo.put("msgErrorManufacturerData", msgColumnNo + "（L）" + msgErrorData);
            msgInfo.put("msgErrorTaxPrice", msgErrorFieldType + msgErrorTmp + "（M）" + msgErrorTmpVal);
            msgInfo.put("msgErrorCurrencyData", msgColumnNo + "（N）" + msgErrorData);
            msgInfo.put("msgErrorTaxRate", msgErrorFieldType + msgErrorTmp + "（O）" + msgErrorTmpVal);
            msgInfo.put("msgErrorInstallDate", msgColumnNo + "（P）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorInstallPrice", msgErrorFieldType + msgErrorTmp + "（Q）" + msgErrorTmpVal);
            msgInfo.put("msgErrorInstallCurrencyData", msgColumnNo + "（R）" + msgErrorData);
            msgInfo.put("msgCustomField", msgCustomField);
            msgInfo.put("msgErrorField", msgErrorField);
            msgInfo.put("msgCustomFieldData", msgErrorData);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Set<String> assetCodeSet = new HashSet<>();
        Set<String> assetTypeSet = new HashSet<>();
        Set<String> assetModelSet = new HashSet<>();
        Set<String> positionCodeSet = new HashSet<>();
        for (LinkedHashMap row : dataList) {
            assetCodeSet.add(RegexUtil.optStrOrBlank(row.get(1)));
            assetTypeSet.add(RegexUtil.optStrOrBlank(row.get(2)));
            assetModelSet.add(RegexUtil.optStrOrBlank(row.get(3)));
            positionCodeSet.add(RegexUtil.optStrOrBlank(row.get(4)));
        }
        Map<String, Map<String, Object>> assetCodeMap = new HashMap<>();//查询导入的设备编码在系统中是否已录入
        if (assetCodeSet.size() > 0) {
            assetCodeMap = assetInfoService.queryAssetCodeByCode(schemaName, new ArrayList<>(assetCodeSet));
        }
        Map<String, Map<String, Object>> assetTypeMap = new HashMap<>();//查询导入的设备类型在系统中是否已录入
        if (assetTypeSet.size() > 0) {
            assetTypeMap = assetCategoryService.queryAssetTypeByCode(schemaName, new ArrayList<>(assetTypeSet));
        }
        Map<String, Map<String, Object>> assetModelMap = new HashMap<>();//查询导入的设备型号在系统中是否已录入
        if (assetModelSet.size() > 0) {
            assetModelMap = assetModelService.queryAssetModelByCode(schemaName, new ArrayList<>(assetModelSet));
        }
        Map<String, Map<String, Object>> positionCodeMap = new HashMap<>();//查询导入的设备位置在系统中是否已录入
        if (positionCodeSet.size() > 0) {
            positionCodeMap = assetPositionService.queryPositionByPositionCodes(schemaName, new ArrayList<>(positionCodeSet));
        }
        assetTypeSet.clear();
        assetModelSet.clear();
        positionCodeSet.clear();
        assetCodeSet.clear();//先清空，记录每一行的设备编码，校验上传数据中是否有重复
        if (columns.size() > 18) {
            boolean flag = true;
            //如果导入模板列除了基础信息列以外，还有其它自定义列、设备更新ID列，则做特殊处理
            for (int i = 18; i < columns.size(); i++) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                if (!COLUMN_ASSET_ID.equalsIgnoreCase(fieldName)) {
                    if (!fieldName.contains("【")) {
                        flag = false;
                        msg.append(msgInfo.get("msgCustomField")).append("（").append(fieldName).append("）").append(msgInfo.get("msgErrorField"));
                        RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(0, msg));
                    } else {
                        String code = fieldName.substring(0, fieldName.indexOf("【"));
                        String name = fieldName.substring(fieldName.indexOf("【") + 1, fieldName.indexOf("】"));
                        if (code.length() < 1 || name.length() < 1) {
                            flag = false;
                            msg.append(msgInfo.get("msgCustomField")).append("（").append(fieldName).append("）").append(msgInfo.get("msgErrorField"));
                            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(0, msg));
                        }
                    }
                }
            }
            if (flag) {
                for (LinkedHashMap row : dataList) {
                    StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                    RegexUtil.optNotNullMapObj(assetTypeMap).ifPresent(am -> RegexUtil.optNotBlankStrOpt(row.get(2)).map(am::get).ifPresent(amm -> {
                        Map<String, String> map = getSelectKeyMapWithCache(methodParam, (Integer) amm.get("id")); //通过设备类型id，在缓存模式中查找自定义字段selectKey的Map
                        for (int i = 18; i < columns.size(); i++) {
                            String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                            if (!COLUMN_ASSET_ID.equalsIgnoreCase(fieldName)) {
                                String code = fieldName.substring(0, fieldName.indexOf("【"));
                                String value = RegexUtil.optStrOrBlank(row.get(i));
                                if (map.containsKey(code) && RegexUtil.optIsPresentStr(value)) {
                                    String getValue = RegexUtil.optNotBlankStrOpt(map.get(code)).map(s -> selectOptionService.getSelectOptionCodeByTextWithCache(methodParam, s, value)).orElse("");
                                    if (RegexUtil.optIsBlankStr(getValue)) {
                                        msg.append(msgInfo.get("msgCustomField")).append("（").append(fieldName).append("）").append(msgInfo.get("msgCustomFieldData"));
                                    }
                                }
                            }
                        }
                    }));
                    RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
                }
            }
        }
        for (LinkedHashMap row : dataList) {
            StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
            String assetName = RegexUtil.optStrOrBlank(row.get(0));//设备名称
            String assetCode = RegexUtil.optStrOrBlank(row.get(1));//设备编码
            String assetType = RegexUtil.optStrOrBlank(row.get(2));//设备类型
            String assetModel = RegexUtil.optStrOrBlank(row.get(3));//设备型号
            String positionCode = RegexUtil.optStrOrBlank(row.get(4));//所在位置
            String assetStatus = RegexUtil.optStrOrBlank(row.get(5));//设备状态（资产状态）
            String level = RegexUtil.optStrOrBlank(row.get(6));//重要级别
            String enableTime = RegexUtil.optStrOrBlank(row.get(7));//启用日期
            String useYear = RegexUtil.optStrOrBlank(row.get(8));//使用寿命
            String buyDate = RegexUtil.optStrOrBlank(row.get(9));//购买日期
            String supplier = RegexUtil.optStrOrBlank(row.get(10));//供应商
            String manufacturer = RegexUtil.optStrOrBlank(row.get(11));//制造商
            String price = RegexUtil.optStrOrBlank(row.get(12));//税前价格
            String buyCurrency = RegexUtil.optStrOrBlank(row.get(13));//货币单位
            String taxRate = RegexUtil.optStrOrBlank(row.get(14));//税率
            String installDate = RegexUtil.optStrOrBlank(row.get(15));//安装时间
            String installPrice = RegexUtil.optStrOrBlank(row.get(16));//安装价格
            String installCurrency = RegexUtil.optStrOrBlank(row.get(17));//货币单位
            if (RegexUtil.optIsBlankStr(assetName)) {
                msg.append(msgInfo.get("msgErrorAssetNameNull"));
            }
            if (RegexUtil.optIsBlankStr(assetCode)) {
                msg.append(msgInfo.get("msgErrorAssetCodeNull"));
            } else {
                if (assetCodeSet.contains(assetCode)) {
                    msg.append(msgInfo.get("msgErrorAssetCodeRepeat"));
                } else if (!(COLUMN_ASSET_ID.equalsIgnoreCase(RegexUtil.optStrOrBlank(columns.get(18)).trim()) && RegexUtil.optIsPresentStr(row.get(18)))
                        && assetCodeMap.containsKey(assetCode)) {
                    //判断最后一列有没有设备id列，如果没有则为新增数据，需要再判断数据库中是否已存在该设备编码
                    msg.append(msgInfo.get("msgErrorAssetCodeExist"));
                } else {
                    assetCodeSet.add(assetCode);
                }
            }
            if (RegexUtil.optIsBlankStr(assetType)) {
                msg.append(msgInfo.get("msgErrorAssetTypeNull"));
            } else if (!assetTypeMap.containsKey(assetType)) {
                msg.append(msgInfo.get("msgErrorAssetTypeData"));
            }
            if (RegexUtil.optIsPresentStr(assetModel) && !assetModelMap.containsKey(assetModel)) {
                msg.append(msgInfo.get("msgErrorAssetModelData"));
            }
            if (RegexUtil.optIsBlankStr(positionCode)) {
                msg.append(msgInfo.get("msgErrorPositionCodeNull"));
            } else if (!positionCodeMap.containsKey(positionCode)) {
                msg.append(msgInfo.get("msgErrorPositionCodeData"));
            }
            if (RegexUtil.optIsBlankStr(assetStatus)) {
                msg.append(msgInfo.get("msgErrorAssetStatusNull"));
            } else if (!assetStatusMap.containsKey(assetStatus)) {
                msg.append(msgInfo.get("msgErrorAssetStatusError"));
            }
            if (RegexUtil.optIsPresentStr(level) && !levelMap.containsKey(level)) {
                msg.append(msgInfo.get("msgErrorLevelData"));
            }
            if (RegexUtil.optIsPresentStr(enableTime) && parseDate(enableTime) == null) {
                msg.append(msgInfo.get("msgErrorEnableTime") + "(" + enableTime + ")");
            }
            if (RegexUtil.optIsPresentStr(useYear) && !RegexUtil.isNumeric(useYear)) {
                msg.append(msgInfo.get("msgErrorUseYear") + "(" + useYear + ")");
            }
            if (RegexUtil.optIsPresentStr(buyDate) && parseDate(buyDate) == null) {
                msg.append(msgInfo.get("msgErrorBuyDate") + "(" + buyDate + ")");
            }
            if (RegexUtil.optIsPresentStr(supplier) && !suppliesList.containsKey(supplier)) {
                msg.append(msgInfo.get("msgErrorSupplierData"));
            }
            if (RegexUtil.optIsPresentStr(manufacturer) && !suppliesList.containsKey(manufacturer)) {
                msg.append(msgInfo.get("msgErrorManufacturerData"));
            }
            if (RegexUtil.optIsPresentStr(price) && !RegexUtil.isNumeric(price)) {
                msg.append(msgInfo.get("msgErrorTaxPrice") + "(" + price + ")");
            }
            if (RegexUtil.optIsPresentStr(buyCurrency) && !currencyMap.containsKey(buyCurrency)) {
                msg.append(msgInfo.get("msgErrorCurrencyData"));
            }
            if (RegexUtil.optIsPresentStr(taxRate) && !RegexUtil.isNumeric(taxRate)) {
                msg.append(msgInfo.get("msgErrorTaxRate") + "(" + taxRate + ")");
            }
            if (RegexUtil.optIsPresentStr(installDate) && parseDate(installDate) == null) {
                msg.append(msgInfo.get("msgErrorInstallDate") + "(" + installDate + ")");
            }
            if (RegexUtil.optIsPresentStr(installPrice) && !RegexUtil.isNumeric(installPrice)) {
                msg.append(msgInfo.get("msgErrorInstallPrice") + "(" + installPrice + ")");
            }
            if (RegexUtil.optIsPresentStr(installCurrency) && !currencyMap.containsKey(installCurrency)) {
                msg.append(msgInfo.get("msgErrorInstallCurrencyData"));
            }
            RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));

        }
        if (null != assetCodeMap) {
            assetCodeMap.clear();
        }
        if (null != assetTypeMap) {
            assetTypeMap.clear();
        }
        if (null != positionCodeMap) {
            positionCodeMap.clear();
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        Date now = new Date();
        Set<String> assetCodeSet = new HashSet<>();
        Set<String> assetTypeSet = new HashSet<>();
        Set<String> assetModelSet = new HashSet<>();
        Set<String> positionCodeSet = new HashSet<>();
        for (LinkedHashMap row : dataList) {
            assetCodeSet.add(RegexUtil.optStrOrBlank(row.get(1)));
            assetTypeSet.add(RegexUtil.optStrOrBlank(row.get(2)));
            assetModelSet.add(RegexUtil.optStrOrBlank(row.get(3)));
            positionCodeSet.add(RegexUtil.optStrOrBlank(row.get(4)));
        }
        Map<String, Map<String, Object>> assetCodeMap = new HashMap<>();//查询导入的设备编码在系统中是否已录入
        if (assetCodeSet.size() > 0) {
            assetCodeMap = assetInfoService.queryAssetCodeByCode(schemaName, new ArrayList<>(assetCodeSet));
        }
        Map<String, Map<String, Object>> assetTypeMap = new HashMap<>();//查询导入的设备类型在系统中是否已录入
        if (assetTypeSet.size() > 0) {
            assetTypeMap = assetCategoryService.queryAssetTypeByCode(schemaName, new ArrayList<>(assetTypeSet));
        }
        Map<String, Map<String, Object>> assetModelMap = new HashMap<>();//查询导入的设备型号在系统中是否已录入
        if (assetModelSet.size() > 0) {
            assetModelMap = assetModelService.queryAssetModelByCode(schemaName, new ArrayList<>(assetModelSet));
        }
        Map<String, Map<String, Object>> positionCodeMap = new HashMap<>();//查询导入的设备位置在系统中是否已录入
        if (positionCodeSet.size() > 0) {
            positionCodeMap = assetPositionService.queryPositionByPositionCodes(schemaName, new ArrayList<>(positionCodeSet));
        }
        assetCodeSet.clear();
        assetTypeSet.clear();
        assetModelSet.clear();
        positionCodeSet.clear();
        List<Map<String, Object>> addAssetList = new ArrayList<>();
        List<Map<String, Object>> updateAssetList = new ArrayList<>();
        for (LinkedHashMap row : dataList) {
            row.put(2, assetTypeMap.get(RegexUtil.optStrOrBlank(row.get(2))).get("id"));
            row.put(3, row.get(3) == null ? null : assetModelMap.get(RegexUtil.optStrOrBlank(row.get(3))).get("id"));
            row.put(4, positionCodeMap.get(RegexUtil.optStrOrBlank(row.get(4))).get("position_code"));
            row.put(5, assetStatusMap.get(RegexUtil.optStrOrBlank(row.get(5))).get("value"));
            row.put(6, row.get(6) == null ? null : levelMap.get(RegexUtil.optStrOrBlank(row.get(6))).get("value"));
            row.put(7, parseDate(RegexUtil.optStrOrBlank(row.get(7))));
            row.put(8, RegexUtil.optIsPresentStr(row.get(8)) ? new BigDecimal(String.valueOf(row.get(8))) : 0);
            row.put(9, parseDate(RegexUtil.optStrOrBlank(row.get(9))));
            row.put(10, RegexUtil.optIsPresentStr(row.get(10)) ? suppliesList.get(row.get(10)).get("id") : 0);
            row.put(11, RegexUtil.optIsPresentStr(row.get(11)) ? suppliesList.get(row.get(11)).get("id") : 0);
            row.put(12, RegexUtil.optIsPresentStr(row.get(12)) ? new BigDecimal(String.valueOf(row.get(12))).setScale(2, BigDecimal.ROUND_HALF_UP) : 0);
            row.put(13, RegexUtil.optIsPresentStr(row.get(13)) ? currencyMap.get(row.get(13)).get("code") : 0);
            row.put(14, RegexUtil.optIsPresentStr(row.get(14)) ? new BigDecimal(String.valueOf(row.get(14))).setScale(5, BigDecimal.ROUND_HALF_UP) : 0);
            row.put(15, parseDate(RegexUtil.optStrOrBlank(row.get(15))));
            row.put(16, RegexUtil.optIsPresentStr(row.get(16)) ? new BigDecimal(String.valueOf(row.get(16))).setScale(2, BigDecimal.ROUND_HALF_UP) : 0);
            row.put(17, RegexUtil.optIsPresentStr(row.get(17)) ? currencyMap.get(row.get(17)).get("code") : 0);
            if (COLUMN_ASSET_ID.equalsIgnoreCase(RegexUtil.optStrOrBlank(columns.get(18)).trim()) && RegexUtil.optIsPresentStr(row.get(18))) {
                //如果第19列是设备id数据，则此条设备数据做更新处理
                updateAssetList.add(getAssetBean(row, now, false));
            } else {
                addAssetList.add(getAssetBean(row, now, true));
            }
            count++;//记录导入条数
        }
        assetCodeMap.clear();
        assetTypeMap.clear();
        assetModelMap.clear();
        positionCodeMap.clear();
        //批量新增设备数据
        if (RegexUtil.optIsPresentList(addAssetList)) {
            commonUtilService.doBatchInsertSql(schemaName, "_sc_asset", SqlConstant._sc_asset_columns, addAssetList);
        }
        assetDataService.addAssetOrgBatch(methodParam, addAssetList);
        //批量修改设备数据
        if (RegexUtil.optIsPresentList(updateAssetList)) {
            assetDataService.updateAssetBatch(methodParam, updateAssetList);
            assetDataService.addAssetOrgBatch(methodParam, updateAssetList);
        }
    }

    /**
     * 字符串转日期格式
     *
     * @param value
     * @return
     */
    private Date parseDate(String value) {
        if (RegexUtil.optIsPresentStr(value)) {
            try {
                if (value.contains("-")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("/")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    return simpleDateFormat.parse(value);
                } else if (value.contains("年")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
                    return simpleDateFormat.parse(value);
                }
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    private Map<String, Object> getAssetBean(LinkedHashMap row, Date now, boolean isNew) {
        Map<String, Object> assetBean = new HashMap<>();
        if (isNew) {
            assetBean.put("id", SenscloudUtil.generateUUIDStr());//如果是新增设备，则生成uuid作为主键
            assetBean.put("data_order", 0);
            assetBean.put("create_time", now);
            assetBean.put("create_user_id", methodParam.getUserId());
            assetBean.put("guarantee_status_id", 1);//默认"保内"
            assetBean.put("running_status_id", RegexUtil.optIsPresentStr(assetRunningStatus) ? Integer.valueOf(assetRunningStatus) : 0);
        }
        assetBean.put("asset_name", row.get(0));
        assetBean.put("asset_code", row.get(1));
        assetBean.put("category_id", row.get(2));
        assetBean.put("asset_model_id", row.get(3));
        assetBean.put("position_code", row.get(4));
        assetBean.put("status", row.get(5));
        assetBean.put("importment_level_id", row.get(6));
        assetBean.put("enable_time", row.get(7));
        assetBean.put("use_year", row.get(8));
        assetBean.put("buy_date", row.get(9));
        assetBean.put("supplier_id", row.get(10));
        assetBean.put("manufacturer_id", row.get(11));
        assetBean.put("price", row.get(12));
        assetBean.put("buy_currency_id", row.get(13));
        assetBean.put("tax_rate", row.get(14));
        assetBean.put("install_date", row.get(15));
        assetBean.put("install_price", row.get(16));
        assetBean.put("install_currency_id", row.get(17));
        //计算税费
        BigDecimal taxPrice = BigDecimal.ZERO;
        if (RegexUtil.optIsPresentStr(row.get(12)) && RegexUtil.optIsPresentStr(row.get(14))) {//如果有税前价格、税率，则自动计算税费
            taxPrice = new BigDecimal(String.valueOf(row.get(12))).multiply(new BigDecimal(String.valueOf(row.get(14)))).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
        }
        assetBean.put("tax_price", taxPrice);
        if (columns.size() > 18) {
            JSONArray array = new JSONArray();
//            Map<String, Object> customFields = new HashMap<>();
            //如果导入模板列除了基础信息列以外，还有其它自定义列、设备更新ID列，则做特殊处理
            //通过设备类型id，在缓存模式中查找自定义字段selectKey的Map
            Map<String, String> map = getSelectKeyMapWithCache(methodParam, (Integer) (row.get(2)));
            for (int i = 18; i < columns.size(); i++) {
                String fieldName = RegexUtil.optStrOrBlank(columns.get(i));
                if (COLUMN_ASSET_ID.equalsIgnoreCase(fieldName)) {
                    //如果是更新设备id列，不计入自定义字段数据
                    if (!isNew) {
                        assetBean.put("id", RegexUtil.optStrOrBlank(row.get(i)));//如果是修改设备数据，则把id填入
                    }
                } else {
                    JSONObject object = new JSONObject();
                    String code = fieldName.substring(0, fieldName.indexOf("【"));
                    String name = fieldName.substring(fieldName.indexOf("【") + 1, fieldName.indexOf("】"));
                    object.put("field_code", code);
                    object.put("field_name", name);
                    String selectText = RegexUtil.optStrOrBlank(row.get(i));
                    object.put("field_value", RegexUtil.optNotBlankStrOpt(map.get(code)).map(s -> selectOptionService.getSelectOptionCodeByTextWithCache(methodParam, s, selectText)).orElse(selectText));
                    object.put("field_remark", "");
                    object.put("field_source", "");
                    object.put("field_view_type", "input");
                    array.add(object);
                }
            }
            //自定义字段处理
            assetBean.put("properties", array.toString());//自定义字段
        }
        return assetBean;
    }

    /**
     * 根据设备类型查询出自定义字段下拉框的selectKey
     *
     * @param methodParam 系统变量
     * @param categoryId  设备类型id
     * @return code对应selectKey的字典
     */
    private Map<String, String> getSelectKeyMap(MethodParam methodParam, Integer categoryId) {
        Map<String, String> map = new HashMap<>();
        Map<String, Object> fields = DataChangeUtil.scdObjToMapOrNew(assetCategoryService.findAssetCategoryFields(methodParam, categoryId));
        RegexUtil.optNotBlankStrOpt(fields.get("value")).map(JSONArray::fromObject).filter(a -> a.size() > 0).ifPresent(a -> {
            for (int j = 0; j < a.size(); j++) {
                JSONObject jsonObject = a.getJSONObject(j);
                if ("select".equals(jsonObject.get("field_view_type"))) {
                    RegexUtil.optNotBlankStrOpt(jsonObject.get("field_code")).filter(c -> RegexUtil.optIsPresentStr(jsonObject.get("field_source"))).ifPresent(c ->
                            map.put(c, jsonObject.get("field_source").toString())
                    );
                }
            }
        });
        return map;
    }

    /**
     * 根据设备类型id查询selectKey字典（【缓存模式】）
     *
     * @param methodParam   系统变量
     * @param assetCategory 设备类型id
     * @return 返回selectKey字典
     */
    private Map<String, String> getSelectKeyMapWithCache(MethodParam methodParam, Integer assetCategory) {
        Object obj = ScdSesRecordUtil.getScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_ASSET_CATEGORY_FIELD + assetCategory);
        if (null == obj) {
            Map<String, String> map = getSelectKeyMap(methodParam, assetCategory);
            ScdSesRecordUtil.setScdSesDataByKey(methodParam.getToken(), Constants.SCD_SES_KEY_ASSET_CATEGORY_FIELD + assetCategory, map);
            return map;
        }
        return DataChangeUtil.scdObjToMapStr(obj);
    }
}
